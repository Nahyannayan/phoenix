Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studPromote_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            'Stream_holder.Visible = False
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100090") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    callStream_top_Bind()
                    'callStream_botom_Bind()
                    'ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)

                    'BindGrade()
                    'PopulateSection()

                    PopulatePromotedSection()
                    Dim cb As New CheckBox
                    For Each gvr As GridViewRow In gvStudPromote.Rows
                        cb = gvr.FindControl("chkSelect")
                        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()

                    ViewState("slno") = 0

                    tbPromote.Rows(4).Visible = False
                    tbPromote.Rows(5).Visible = False
                    tbPromote.Rows(6).Visible = False
                    tbPromote.Rows(7).Visible = False
                    'tbPromote.Rows(8).Visible = False
                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

        Else

            highlight_grid()

        End If
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'Stream_holder.Visible = False
        Try
            'tbPromote.Rows(4).Visible = True
            tbPromote.Rows(5).Visible = True
            tbPromote.Rows(6).Visible = True
            tbPromote.Rows(7).Visible = True
            tbPromote.Rows(8).Visible = True
            lblError.Text = ""
            hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
            hfGRD_ID_PROMOTED.Value = GetpromotedGrade()
            hfGRD_ID.Value = ddlGrade_top.SelectedValue
            hfSCT_ID.Value = ddlSection.SelectedValue.ToString
            GridBind()
            Dim maxGrade As String = GetMaxGrade()
            If ddlGrade_top.SelectedValue.ToString = maxGrade Then
                ViewState("MaxGrade") = True
            Else
                ViewState("MaxGrade") = False
            End If


            PopulatePromotedSection()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub




    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvReenrolStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#Region "PrivateMethods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function

    Function GetPromotedYear() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " select top 1 acd_id from academicyear_d where acd_startdt>" _
                                & "(select acd_startdt from academicyear_d where acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                & " and acd_bsu_id='" + Session("sbsuid") + "' and acd_clm_id=" + Session("clm") + ") and acd_bsu_id='" + Session("sbsuid") + "' and acd_clm_id=" + Session("clm") + " order by acd_startdt"
        Dim acdid As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return acdid
    End Function


    Private Sub PopulatePromotedSection()
        Dim acd_id As Integer
        acd_id = GetPromotedYear()
        hfACD_ID_PROMOTED.Value = acd_id
        ddlPromoteSection.Items.Clear()
        Dim GRM_ID_Select = "SELECT grm_id FROM grade_bsu_m WHERE" _
                                 & " grm_acd_id=" + acd_id.ToString + " and grm_grd_id='" + ddlGrade_botom.SelectedValue + "' and GRM_STM_ID= '" + ddlStream_botom.SelectedValue + "'"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlAction.SelectedValue = "Pass" Then
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + acd_id.ToString _
                                     & " AND GRM_GRD_ID=(SELECT TOP 1 GRD_ID FROM GRADE_M WHERE GRD_DISPLAYORDER>" _
                                     & " (SELECT GRD_DISPLAYORDER FROM GRADE_M WHERE GRD_ID='" + ddlGrade_botom.SelectedValue + "') ORDER BY GRD_DISPLAYORDER) and GRM_STM_ID ='" + ddlStream_botom.SelectedValue + "')" _
                                     & "  ORDER BY SCT_DESCR"
        ElseIf ddlAction.SelectedValue = "DoublePromote" Then
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + acd_id.ToString _
                                     & " AND GRM_GRD_ID=(SELECT TOP 1 A.GRD_ID FROM  GRADE_M  A INNER JOIN dbo.GRADE_M B ON A.GRD_DISPLAYORDER =B.GRD_DISPLAYORDER +2 " _
                                     & " WHERE B.GRD_ID='" + ddlGrade_botom.SelectedValue + "' ORDER BY B.GRD_DISPLAYORDER) and GRM_STM_ID =" + ddlStream_botom.SelectedValue + ")" _
                                     & " ORDER BY SCT_DESCR"
        Else
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRD_ID='" + ddlGrade_botom.SelectedValue.ToString + "' AND SCT_ACD_ID=" + acd_id.ToString _
                      & " and  [SCT_GRM_ID] in (" + GRM_ID_Select + ") ORDER BY SCT_DESCR "
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlPromoteSection.DataSource = ds
        ddlPromoteSection.DataTextField = "SCT_DESCR"
        ddlPromoteSection.DataValueField = "SCT_ID"
        ddlPromoteSection.DataBind()
        If ddlAction.SelectedValue.ToLower <> "retest" And ddlSection.SelectedValue = "0" Then
            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlPromoteSection.Items.Insert(0, li)
        End If
    End Sub
    Private Sub PopulatePromotedSection_botom_change()

        Dim acd_id As Integer
        acd_id = GetPromotedYear()
        hfACD_ID_PROMOTED.Value = acd_id
        ddlPromoteSection.Items.Clear()
        Dim GRM_ID_Select = "SELECT grm_id FROM grade_bsu_m WHERE" _
                                 & " grm_acd_id=" + acd_id.ToString + " and grm_grd_id='" + ddlGrade_botom.SelectedValue + "' and GRM_STM_ID= '" + ddlStream_botom.SelectedValue + "'"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If ddlAction.SelectedValue = "Pass" Then
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + acd_id.ToString _
                                     & " AND GRM_GRD_ID='" + ddlGrade_botom.SelectedValue + "' and GRM_STM_ID ='" + ddlStream_botom.SelectedValue + "')" _
                                     & "  ORDER BY SCT_DESCR"
        ElseIf ddlAction.SelectedValue = "DoublePromote" Then
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + acd_id.ToString _
                                     & " AND GRM_GRD_ID=(SELECT TOP 1 A.GRD_ID FROM  GRADE_M  A INNER JOIN dbo.GRADE_M B ON A.GRD_DISPLAYORDER =B.GRD_DISPLAYORDER +1 " _
                                     & " WHERE B.GRD_ID='" + ddlGrade_botom.SelectedValue + "' ORDER BY B.GRD_DISPLAYORDER) and GRM_STM_ID =" + ddlStream_botom.SelectedValue + ")" _
                                     & " ORDER BY SCT_DESCR"
        Else
            str_query = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRD_ID='" + ddlGrade_botom.SelectedValue.ToString + "' AND SCT_ACD_ID=" + acd_id.ToString _
                      & " and  [SCT_GRM_ID] in (" + GRM_ID_Select + ") ORDER BY SCT_DESCR "
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlPromoteSection.DataSource = ds
        ddlPromoteSection.DataTextField = "SCT_DESCR"
        ddlPromoteSection.DataValueField = "SCT_ID"
        ddlPromoteSection.DataBind()
        If ddlAction.SelectedValue.ToLower <> "retest" And ddlSection.SelectedValue = "0" Then
            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlPromoteSection.Items.Insert(0, li)
        End If
    End Sub
    Function GetMaxGrade() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TOP 1 GRD_ID FROM GRADE_M INNER JOIN GRADE_BSU_M ON GRM_GRD_ID=GRD_ID " _
                                 & " WHERE GRM_ACd_ID=" + hfACD_ID_PROMOTED.Value + " ORDER BY GRD_DISPLAYORDER DESC"
        Dim grdid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return grdid
    End Function
    Function GetpromotedGrade() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TOP 1 GRD_ID FROM GRADE_M WHERE GRD_DISPLAYORDER>" _
                                  & " (SELECT GRD_DISPLAYORDER FROM GRADE_M WHERE GRD_ID='" + ddlGrade_top.SelectedValue + "') ORDER BY GRD_DISPLAYORDER"
        Dim grdid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return grdid
    End Function
    Sub SaveData()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim xmlString As String = GetXMlString()
        Dim stuIds As String
        If xmlString = "" Then
            lblError.Text = "No Records  selected"
            Exit Sub
        End If
        'if the grade is max grade of the school then pass sectionid as zero
        If ViewState("MaxGrade") = False Or ddlAction.SelectedValue = "Fail" Then
            str_query = "exec studPROMOTE '" + xmlString + "','" + Session("sbsuid") + "'," _
                        & Session("clm") + ",'" + ddlAction.SelectedValue.ToString + "'," _
                        & ddlPromoteSection.SelectedValue.ToString + ",'" + Format(Now.Date, "yyyy-MM-dd") + "','" + Session("sUsr_name") + "'"
        Else
            str_query = "exec studPROMOTE '" + xmlString + "','" + Session("sbsuid") + "'," _
                            & Session("clm") + ",'" + ddlAction.SelectedValue.ToString + "',0" _
                            & ",'" + Format(Now.Date, "yyyy-MM-dd") + "','" + Session("sUsr_name") + "'"
        End If

        ' Dim transaction As SqlTransaction
        '  Using conn As SqlConnection = ConnectionManger.GetOASISConnection
        '   transaction = conn.BeginTransaction("SampleTransaction")
        Try
            stuIds = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "STU_ID(" + stuIds + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            'transaction.Commit()
            lblError.Text = "Record Saved Successfully"
        Catch myex As ArgumentException
            'transaction.Rollback()
            lblError.Text = myex.Message
            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        Catch ex As Exception
            'transaction.Rollback()
            lblError.Text = "Record could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'End Using
    End Sub

    Sub PromoteAllSections(ByVal xmlString As String)

        'Dim transaction As SqlTransaction
        'Using conn As SqlConnection = ConnectionManger.GetOASISConnection
        '    transaction = conn.BeginTransaction("SampleTransaction")
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "exec studPROMOTEALLSTUDENTSINGRADE " _
                                     & "'" + xmlString + "'," _
                                     & "'" + Session("sbsuid") + "'," _
                                     & Session("clm") + "," _
                                     & "'" + ddlAction.SelectedValue + "'," _
                                     & "'" + Format(Now.Date, "yyyy-MM-dd") + "'," _
                                     & "'" + Session("sUsr_name") + "'," _
                                     & hfACD_ID.Value + "," _
                                     & hfACD_ID_PROMOTED.Value + "," _
                                     & "'" + hfGRD_ID.Value + "'," _
                                     & "'" + hfGRD_ID_PROMOTED.Value + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            ' Transaction.Commit()
            lblError.Text = "Record Saved Successfully"
        Catch myex As ArgumentException
            '  Transaction.Rollback()
            lblError.Text = myex.Message
            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        Catch ex As Exception
            'Transaction.Rollback()
            lblError.Text = "Record could not be Saved"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        '  End Using
    End Sub
    Function GetXMlString() As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim lblsctId As Label
        Dim str As String = ""
        With gvStudPromote
            For i = 0 To .Rows.Count - 1
                chkSelect = .Rows(i).FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    lblStuId = .Rows(i).FindControl("lblStuId")
                    lblsctId = .Rows(i).FindControl("lblsctId")
                    str += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID><SCT_ID>" + lblsctId.Text + "</SCT_ID></ID>"
                End If
            Next
        End With
        If str <> "" Then
            Return "<IDS>" + str + "</IDS>"
        Else
            Return ""
        End If
    End Function
    Private Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim GRM_ID_Select = "SELECT grm_id FROM grade_bsu_m,stream_m WHERE" _
                                     & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and grm_grd_id='" + ddlGrade_top.SelectedValue + "' and STM_ID= '" + ddlStream_top.SelectedValue + "'"
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade_top.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " and  [SCT_GRM_ID] in (" + GRM_ID_Select + ") ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudPromote.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudPromote.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudPromote.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudPromote.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub highlight_grid()
        For i As Integer = 0 To gvStudPromote.Rows.Count - 1
            Dim row As GridViewRow = gvStudPromote.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

    Sub GridBind()
        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_PROM_NEXT", pParms)
            While reader.Read
                Session("Prom_Next_Acad") = Convert.ToString(reader("ACD_ID"))
            End While
        End Using
        If Session("Prom_Next_Acad").ToString = "" Or Session("Prom_Next_Acad") = Nothing Then
            lblError.Text = "check your Select Academic Year"

            Exit Sub
        End If


        'Dim str_query As String = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
        '                         & " B.GRM_DISPLAY,STU_SCT_ID,C.SCT_DESCR," _
        '                         & " REENROL_STATUS=(SELECT CASE when ((STP_bFEE_PENDING =1)and (isnull(STP_bREENROLL,0)=0))  THEN 'Pending' " _
        '                         & " ELSE CASE WHEN ((STP_bFEE_PENDING =0)and (isnull(STP_bREENROLL,0)=1)) THEN 'Reenrolled' " _
        '                         & " ELSE 'Not Reenrolled' END END " _
        '                         & " FROM STUDENT_PROMO_S WHERE STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND STP_bACTIVE='TRUE' AND  STP_STU_ID=A.STU_ID )," _
        '                         & " STATUS=ISNULL((CASE STU_bPROMOTED WHEN 'FALSE' THEN " _
        '                         & "(SELECT CASE STP_RESULT WHEN 'RETEST' THEN 'Retest' ELSE CASE STP_bDEMOTED WHEN 'FALSE' THEN '--' ELSE 'Demoted' END END FROM" _
        '                         & " STUDENT_PROMO_S WHERE STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND STP_bACTIVE='TRUE' AND " _
        '                         & " STP_STU_ID=A.STU_ID )" _
        '                         & " ELSE(SELECT STP_RESULT FROM STUDENT_PROMO_S WHERE " _
        '                         & " STP_STU_ID=A.STU_ID AND STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND STP_bACTIVE='TRUE')END),'--')" _
        '                         & "  ,isNULL(PROM_GRM.GRM_DISPLAY,'')+ ' ' + isNULL(PROM_SCT.SCT_DESCR,'')+ ' (' + isNULL(PROM_STM.STM_DESCR,'') +' )' as PromGradeSection " _
        '                         & " FROM STUDENT_M  AS A INNER JOIN" _
        '                         & " GRADE_BSU_M AS B ON A.STU_GRM_ID = B.GRM_ID" _
        '                         & " INNER JOIN  SECTION_M AS C ON" _
        '                         & " A.STU_SCT_ID = C.SCT_ID " _
        '                         & " LEFT JOIN STUDENT_PROMO_S PROM WITH (NOLOCK) ON A.STU_ID=PROM.STP_STU_ID AND PROM.STP_ACD_ID=" & Session("Prom_Next_Acad") & " " _
        '                         & " LEFT JOIN Section_M PROM_SCT WITH (NOLOCK) ON PROM.STP_SCT_ID=PROM_SCT.SCT_ID AND PROM_SCT.SCT_ACD_ID=" & Session("Prom_Next_Acad") & " " _
        '                         & " LEFT JOIN GRADE_BSU_M PROM_GRM WITH (NOLOCK) ON PROM.STP_GRM_ID=PROM_GRM.GRM_ID AND PROM_GRM.GRM_ACD_ID=" & Session("Prom_Next_Acad") & " " _
        '                         & " LEFT JOIN STREAM_M PROM_STM WITH (NOLOCK) ON PROM_GRM.GRM_STM_ID=PROM_STM.STM_ID " _
        '                         & " WHERE  STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString
        'If ddlStream.SelectedValue <> "0" Then
        '    str_query += " AND B.GRM_STM_ID='" + ddlStream.SelectedValue.ToString + "'"
        'End If
        'If ddlGrade.SelectedValue <> "0" Then
        '    str_query += " AND STU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        'End If

        'str_query += " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
        '                         & "  AND STU_CURRSTATUS='EN'" _
        '                         & " AND STU_ID NOT IN (SELECT TCM_STU_ID FROM TCM_M WHERE  TCM_CANCELDATE IS NULL)"


        'If hfSCT_ID.Value <> "0" Then
        '    str_query += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        'End If

        'If txtStuNo.Text <> "" Then
        '    str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        'End If
        'If txtName.Text <> "" Then
        '    str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        'End If


        ' AND C.SCT_GRM_ID=B.GRM_ID" 
        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
        Dim ddlgvStatus As New DropDownList
        Dim ddlgvReenrolStatus As New DropDownList

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedStatus As String = ""
        Dim selectedRe_Status As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim txtSearch As New TextBox

        If gvStudPromote.Rows.Count > 0 Then


            txtSearch = gvStudPromote.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NAME", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudPromote.HeaderRow.FindControl("txtFeeSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                If strFilter <> "" Then
                    strFilter += " AND "
                End If
                strFilter += GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strFee = txtSearch.Text

            ddlgvGrade = gvStudPromote.HeaderRow.FindControl("ddlgvGrade")
            If ddlgvGrade.Text <> "ALL" And ddlgvGrade.Text <> "" Then
                If strFilter <> "" Then
                    strFilter += "AND "
                End If
                strFilter += "grm_display='" + ddlgvGrade.Text + "'"
                selectedGrade = ddlgvGrade.Text
            End If

            ddlgvSection = gvStudPromote.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" Then
                If strFilter = "" Then
                    strFilter = "sct_descr='" + ddlgvSection.Text + "'"
                Else
                    strFilter = strFilter + " and sct_descr='" + ddlgvSection.Text + "'"
                End If
                selectedSection = ddlgvSection.Text
            End If


            ddlgvStatus = gvStudPromote.HeaderRow.FindControl("ddlgvStatus")
            If ddlgvStatus.Text <> "ALL" Then
                If strFilter = "" Then
                    strFilter = "status='" + ddlgvStatus.Text + "'"
                Else
                    strFilter = strFilter + " and status='" + ddlgvStatus.Text + "'"
                End If

                selectedStatus = ddlgvStatus.Text
            End If

            ddlgvReenrolStatus = gvStudPromote.HeaderRow.FindControl("ddlgvReenrolStatus")
            If ddlgvReenrolStatus.Text <> "ALL" Then
                If strFilter = "" Then
                    strFilter = "REENROL_STATUS='" + ddlgvReenrolStatus.Text + "'"
                Else
                    strFilter = strFilter + " and REENROL_STATUS='" + ddlgvReenrolStatus.Text + "'"
                End If

                selectedRe_Status = ddlgvReenrolStatus.Text
            End If

        End If

        'str_query += " ORDER BY C.SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim pSParms(9) As SqlClient.SqlParameter
        pSParms(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        pSParms(1) = New SqlClient.SqlParameter("@Prom_Next_Acad", Session("Prom_Next_Acad").ToString)
        pSParms(2) = New SqlClient.SqlParameter("@GRM_STM_ID", ddlStream_top.SelectedValue.ToString)
        pSParms(3) = New SqlClient.SqlParameter("@STU_GRD_ID", ddlGrade_top.SelectedValue.ToString)
        pSParms(4) = New SqlClient.SqlParameter("@STU_SCT_ID", ddlSection.SelectedValue.ToString)
        pSParms(5) = New SqlClient.SqlParameter("@STU_NO", txtStuNo.Text)
        pSParms(6) = New SqlClient.SqlParameter("@STU_Name", txtName.Text)


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[Get_Promote_Students]", pSParms)
        Dim dv As DataView = ds.Tables(0).DefaultView
        If strFilter <> "" Then
            dv.RowFilter = strFilter
        End If

        Dim dt As DataTable
        gvStudPromote.DataSource = dv

        gvStudPromote.DataBind()

        dt = dv.Table
        If gvStudPromote.Rows.Count > 0 Then
            txtSearch = New TextBox
            txtSearch = gvStudPromote.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = strName

            txtSearch = New TextBox
            txtSearch = gvStudPromote.HeaderRow.FindControl("txtFeeSearch")
            txtSearch.Text = strFee


            ddlgvGrade = gvStudPromote.HeaderRow.FindControl("ddlgvGrade")
            ddlgvSection = gvStudPromote.HeaderRow.FindControl("ddlgvSection")
            ddlgvStatus = gvStudPromote.HeaderRow.FindControl("ddlgvStatus")
            ddlgvReenrolStatus = gvStudPromote.HeaderRow.FindControl("ddlgvReenrolStatus")
            Dim dr As DataRow

            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")


            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")


            ddlgvStatus.Items.Clear()
            ddlgvStatus.Items.Add("ALL")

            ddlgvReenrolStatus.Items.Clear()
            ddlgvReenrolStatus.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    If Not IsDBNull(.Item(3)) Then
                        If ddlgvGrade.Items.FindByText(.Item(3)) Is Nothing Then
                            ddlgvGrade.Items.Add(.Item(3))
                        End If
                    End If
                    If Not IsDBNull(.Item(5)) Then
                        If ddlgvSection.Items.FindByText(.Item(5)) Is Nothing Then
                            ddlgvSection.Items.Add(.Item(5))
                        End If
                    End If
                    If Not IsDBNull(.Item(6)) Then
                        If ddlgvReenrolStatus.Items.FindByText(.Item(6)) Is Nothing Then
                            ddlgvReenrolStatus.Items.Add(.Item(6))
                        End If
                    End If
                    If Not IsDBNull(.Item(7)) Then
                        If ddlgvStatus.Items.FindByText(.Item(7)) Is Nothing Then
                            ddlgvStatus.Items.Add(.Item(7))
                        End If
                    End If
                End With

            Next
            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If


            If selectedSection <> "" Then
                ddlgvSection.Text = selectedSection
            End If

            If selectedStatus <> "" Then
                ddlgvStatus.Text = selectedStatus
            End If
            If selectedRe_Status <> "" Then
                ddlgvReenrolStatus.Text = selectedRe_Status
            End If
        End If

        If gvStudPromote.Rows.Count > 0 Then
            btnUpdate.Visible = True
            btnUpdate1.Visible = True
        Else
            btnUpdate.Visible = False
            btnUpdate1.Visible = False
        End If

        set_Menu_Img()
    End Sub


    Function getPromotedMisMatchSections()
        Dim strSec As String = ""
        Dim i As Integer
        Dim lblSection As Label
        Dim chkSelect As CheckBox
        For i = 0 To gvStudPromote.Rows.Count - 1
            chkSelect = gvStudPromote.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblSection = gvStudPromote.Rows(i).FindControl("lblSection")
                If strSec <> "" Then
                    strSec += ","
                End If
                strSec += "'" + lblSection.Text + "'"
            End If
        Next
        If strSec = "" Then
            strSec = "'0'"
        End If
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_DESCR FROM SECTION_M " _
                                 & " WHERE SCT_GRD_ID='" + hfGRD_ID.Value + "' AND SCT_ACD_ID=" + hfACD_ID.Value + " AND SCT_DESCR  " _
                                 & " NOT IN(SELECT SCT_DESCR FROM SECTION_M WHERE SCT_GRD_ID='" + hfGRD_ID_PROMOTED.Value + "' AND SCT_ACD_ID=" + hfACD_ID_PROMOTED.Value + ")" _
                                 & " AND SCT_DESCR IN(" + strSec + ")"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sections As String = ""
        While reader.Read
            If sections <> "" Then
                sections += " , "
            End If
            sections += reader.GetString(0)
        End While
        reader.Close()
        Return sections
    End Function

    Function getDetainedMisMatchSections()
        Dim strSec As String = ""
        Dim i As Integer
        Dim lblSection As Label
        Dim chkSelect As CheckBox
        For i = 0 To gvStudPromote.Rows.Count - 1
            chkSelect = gvStudPromote.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblSection = gvStudPromote.Rows(i).FindControl("lblSection")
                If strSec <> "" Then
                    strSec += ","
                End If
                strSec += "'" + lblSection.Text + "'"
            End If
        Next
        If strSec = "" Then
            strSec = "'0'"
        End If
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_DESCR FROM SECTION_M " _
                                 & " WHERE SCT_GRD_ID='" + hfGRD_ID.Value + "' AND SCT_ACD_ID=" + hfACD_ID.Value + " AND SCT_DESCR  " _
                                 & " NOT IN(SELECT SCT_DESCR FROM SECTION_M WHERE SCT_GRD_ID='" + hfGRD_ID.Value + "' AND SCT_ACD_ID=" + hfACD_ID_PROMOTED.Value + ")" _
                                 & " AND SCT_DESCR IN(" + strSec + ")"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim sections As String = ""
        While reader.Read
            If sections <> "" Then
                sections += " , "
            End If
            sections += reader.GetString(0)
        End While
        reader.Close()
        Return sections
    End Function

#End Region

    Protected Sub ddlStream_top_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream_top.SelectedIndexChanged
        'if the selected grade is the maximum grade of the school then disable section
        Try
            ddlStream_botom.SelectedValue = ddlStream_top.SelectedValue
            BindGrade_top()

            PopulateSection()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'If ddlGrade.SelectedIndex <> ddlGrade.Items.Count - 1 Then
        '    ddlPromoteSection.Enabled = True
        '    PopulatePromotedSection()
        'Else
        '    ddlPromoteSection.Enabled = False
        '    ddlPromoteSection.Items.Clear()
        'End If
    End Sub
    Protected Sub ddlStream_botom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream_botom.SelectedIndexChanged
        'if the selected grade is the maximum grade of the school then disable section
        Try
            BindGrade_botom()
            'PopulatePromotedSection()
            'PopulateSection()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'If ddlGrade.SelectedIndex <> ddlGrade.Items.Count - 1 Then
        '    ddlPromoteSection.Enabled = True
        '    PopulatePromotedSection()
        'Else
        '    ddlPromoteSection.Enabled = False
        '    ddlPromoteSection.Items.Clear()
        'End If
    End Sub

    Protected Sub ddlGrade_top_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade_top.SelectedIndexChanged
        'if the selected grade is the maximum grade of the school then disable section
        Try

            PopulateSection()
            ddlGrade_botom.SelectedValue = ddlGrade_top.SelectedValue
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'If ddlGrade.SelectedIndex <> ddlGrade.Items.Count - 1 Then
        '    ddlPromoteSection.Enabled = True
        '    PopulatePromotedSection()
        'Else
        '    ddlPromoteSection.Enabled = False
        '    ddlPromoteSection.Items.Clear()
        'End If
    End Sub
    Protected Sub ddlGrade_botom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade_botom.SelectedIndexChanged
        'if the selected grade is the maximum grade of the school then disable section
        Try
            If ddlStream_botom.SelectedValue <> ddlStream_top.SelectedValue Then
                If ddlStream_botom.SelectedValue Is Nothing Or ddlStream_botom.SelectedValue = "" Then
                Else
                    PopulatePromotedSection_botom_change()

                End If


            Else
                PopulatePromotedSection()
            End If

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'If ddlGrade.SelectedIndex <> ddlGrade.Items.Count - 1 Then
        '    ddlPromoteSection.Enabled = True
        '    PopulatePromotedSection()
        'Else
        '    ddlPromoteSection.Enabled = False
        '    ddlPromoteSection.Items.Clear()
        'End If
    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            If hfSCT_ID.Value = "0" And ddlPromoteSection.SelectedValue.ToString = "0" Then
                Dim str As String
                Dim strSections As String = ""
                Dim xmlString As String = GetXMlString()
                If xmlString = "" Then
                    lblError.Text = "No Records  selected"
                    Exit Sub
                End If
                If ddlAction.SelectedValue.ToLower = "pass" Then
                    strSections = getPromotedMisMatchSections()
                    str = "promoted"
                ElseIf ddlAction.SelectedValue.ToLower = "fail" Then
                    strSections = getDetainedMisMatchSections()
                    str = "detained"
                End If
                If strSections <> "" Then
                    Panel1.Visible = True
                    lblText.Text = "Sections " + strSections + " are not there in the " + str + " year.Those students won't be  " + str + " . Do you wish to proceed ?."
                    Exit Sub
                End If
                PromoteAllSections(xmlString)
            Else
                SaveData()
            End If

            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            'ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
            callStream_top_Bind()
            callStream_botom_Bind()
            'BindGrade()
            'PopulateSection()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        '  PopulatePromotedSection()
    End Sub

    Protected Sub ddlAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAction.SelectedIndexChanged
        Try
            PopulatePromotedSection()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudPromote_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudPromote.PageIndexChanging
        Try
            gvStudPromote.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            Dim xmlString As String = GetXMlString()
            If xmlString = "" Then
                lblError.Text = "No Records  selected"
                Exit Sub
            End If
            PromoteAllSections(xmlString)
            Panel1.Visible = False
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        Panel1.Visible = False
    End Sub

    Protected Sub btnUpdate1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate1.Click
        Try
            If hfSCT_ID.Value = "0" And ddlPromoteSection.SelectedValue.ToString = "0" Then
                Dim str As String
                Dim strSections As String = ""
                Dim xmlString As String = GetXMlString()
                If xmlString = "" Then
                    lblError.Text = "No Records  selected"
                    Exit Sub
                End If
                If ddlAction.SelectedValue.ToLower = "pass" Then
                    strSections = getPromotedMisMatchSections()
                    str = "promoted"
                ElseIf ddlAction.SelectedValue.ToLower = "fail" Then
                    strSections = getDetainedMisMatchSections()
                    str = "detained"
                End If
                If strSections <> "" Then
                    Panel1.Visible = True


                    lblText.Text = "Sections " + strSections + " are not there in the " + str + " year.Those students won't be  " + str + " . Do you wish to proceed ?."
                    Exit Sub
                End If
                PromoteAllSections(xmlString)
            Else
                SaveData()

            End If
            GridBind()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Public Sub callStream_top_Bind()
        Try
            ddlStream_top.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream_top.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlStream.Items.Add(di)
            ddlStream_top.DataSource = ds
            ddlStream_top.DataTextField = "STM_DESCR"
            ddlStream_top.DataValueField = "STM_ID"
            ddlStream_top.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream_top.Items.Count > 0 Then
                ddlStream_top.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        callStream_botom_Bind()
        ddlStream_top_SelectedIndexChanged(ddlStream_top, Nothing)

    End Sub
    Public Sub callStream_botom_Bind()
        Try
            ddlStream_botom.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter
            Dim acd_id As Integer
            acd_id = GetPromotedYear()
            PARAM(0) = New SqlParameter("@ACD_ID", acd_id)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream_botom.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlStream.Items.Add(di)
            ddlStream_botom.DataSource = ds
            ddlStream_botom.DataTextField = "STM_DESCR"
            ddlStream_botom.DataValueField = "STM_ID"
            ddlStream_botom.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream_botom.Items.Count > 0 Then
                ddlStream_botom.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        'ddlStream_botom_SelectedIndexChanged(ddlStream_botom, Nothing)

    End Sub
    Sub BindGrade_top()
        Try
            ddlGrade_top.Items.Clear()
            ddlGrade_top.Items.Clear()
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream_top.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade_top.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlGrade.Items.Add(di)
            ddlGrade_top.DataSource = ds
            ddlGrade_top.DataTextField = "grm_display"
            ddlGrade_top.DataValueField = "GRD_ID"
            ddlGrade_top.DataBind()
            'ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        BindGrade_botom()
        ddlGrade_top_SelectedIndexChanged(ddlGrade_top, Nothing)


    End Sub
    Sub BindGrade_botom()
        Try
            ddlGrade_botom.Items.Clear()
            ddlGrade_botom.Items.Clear()
            Dim acd_id As Integer
            acd_id = GetPromotedYear()
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", acd_id)
            PARAM(2) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream_botom.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade_botom.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlGrade.Items.Add(di)
            ddlGrade_botom.DataSource = ds
            ddlGrade_botom.DataTextField = "grm_display"
            ddlGrade_botom.DataValueField = "GRD_ID"
            ddlGrade_botom.DataBind()
            'ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


        ddlGrade_botom_SelectedIndexChanged(ddlGrade_botom, Nothing)


    End Sub
    Protected Sub chb_change_stream_CheckedChanged(sender As Object, e As EventArgs)
        If chb_change_stream.Checked = True Then
            Stream_holder.Visible = True
        Else
            Stream_holder.Visible = False
        End If
    End Sub
End Class
