Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studYearEnd
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
  
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
             Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                Using readerAcademic_Cutoff As SqlDataReader = AccessStudentClass.GetActive_BsuAndCutOff(Session("sBsuid"), Session("CLM"))
                    If readerAcademic_Cutoff.HasRows = True Then
                        While readerAcademic_Cutoff.Read
                            Session("Current_ACY_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ACY_ID"))
                            Session("Current_ACD_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ID"))
                        End While
                    End If
                End Using
                Using readerPrev_Next As SqlDataReader = AccessStudentClass.getNEXT_CURR_PREV(Session("sBsuid"), Session("CLM"), Session("Current_ACY_ID"))
                    If readerPrev_Next.HasRows = True Then
                        While readerPrev_Next.Read
                            Session("prev_ACD_ID") = Convert.ToString(readerPrev_Next("prev_ACD_ID"))
                            Session("next_ACD_ID") = Convert.ToString(readerPrev_Next("next_ACD_ID"))
                        End While
                    End If
                End Using

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

   

    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str_query As String = " exec studYEAREND " + Session("Current_ACD_ID").ToString + "," + Session("prev_ACD_ID").ToString + ",'" + Session("sUsr_name") + "'"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                transaction.Commit()
                lblError.Text = "Records Saved Successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    
    Protected Sub btnChange_Click1(ByVal sender As Object, ByVal e As System.EventArgs)
        btnChange.Enabled = False
        SaveData()
        btnChange.Enabled = True
    End Sub
End Class
