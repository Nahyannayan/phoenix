Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_studEnquiryList_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim strGlobal_Filter As String


    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Session("BSU_ENQ_SELECT") = "0"
            Session("CONTACTME_SELECT") = "ALL"

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Session("sBsuid") = "123006" Then
                    btnprintlabel.Visible = True
                End If
                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100070") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    PRINT_INDEMNITY()

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    Session("liUserList") = New List(Of String)
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    bindPrevSchools()
                    If Session("ACD_SELECT") IsNot Nothing Then
                        If ddlAcademicYear.Items.FindByValue(Session("ACD_SELECT")) IsNot Nothing Then
                            ddlAcademicYear.ClearSelection()
                            ddlAcademicYear.Items.FindByValue(Session("ACD_SELECT")).Selected = True
                        End If
                    End If

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    get_Del_visible()

                    If ViewState("isDELPERMIT") = 0 Then
                        btnReject.Visible = False
                    End If
                    bind_cur_status()
                    BIND_BSU_DETAILS()
                    GridBind()
                    ViewState("selGrade") = ""
                    ViewState("selStream") = ""
                    ViewState("selShift") = ""
                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else
            studClass.SetChk(gvStudEnquiry, Session("liUserList"))
        End If
    End Sub
    Sub bindPrevSchools()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_CORP")


            ddlPrevSchool_BSU.Items.Clear()
            ddlPrevSchool_BSU.DataSource = ds.Tables(0)
            ddlPrevSchool_BSU.DataTextField = "BSU_NAME"
            ddlPrevSchool_BSU.DataValueField = "BSU_ID"
            ddlPrevSchool_BSU.DataBind()
            ddlPrevSchool_BSU.Items.Add(New ListItem("ALL", "0"))
            ddlPrevSchool_BSU.Items.Add(New ListItem("ALL GEMS", "1"))
            ddlPrevSchool_BSU.Items.Add(New ListItem("ALL NON GEMS", "2"))

            ddlPrevSchool_BSU.ClearSelection()
            If Not ddlPrevSchool_BSU.Items.FindByText("ALL") Is Nothing Then
                ddlPrevSchool_BSU.Items.FindByText("ALL").Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub




    Sub PRINT_INDEMNITY()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "Select isnull(BSU_bINDEMNITY,0) from BUSINESSUNIT_M where bsu_id='" & Session("sBsuid") & "'"
        Dim Print_bIndemnity As Boolean = CBool(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query))

        ViewState("Indemnity") = Print_bIndemnity

    End Sub
    Protected Sub btnEnqid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    Protected Sub btnEnq_Date_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAppl_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvStaff_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvEx_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvGender_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvSib_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvSis_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub lnkApplName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("action") = "select"
    End Sub

    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Session("liUserList") = New List(Of String)
            SaveData("OPN")
            GridBind()
            lblError.Text = "The selected enquiries are successfully shortlisted"
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            Session("liUserList") = New List(Of String)
            Session("ACD_SELECT") = ddlAcademicYear.SelectedValue
            Session("ACD_SELECT_CLEAR") = "ACD"
            GridBind()
            EnqCount()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlPrevSchool_BSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrevSchool_BSU.SelectedIndexChanged
        Try

            Session("BSU_ENQ_SELECT") = ddlPrevSchool_BSU.SelectedItem.Value
            GridBind()
            EnqCount()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlContactMe_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContactMe.SelectedIndexChanged
        Try

            Session("CONTACTME_SELECT") = ddlContactMe.SelectedItem.Value
            GridBind()
            EnqCount()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Session("liUserList") = New List(Of String)
            SaveData("DEL")
            GridBind()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudEnquiry_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudEnquiry.PageIndexChanged
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If
        Dim chk As CheckBox
        Dim ENQID As String = String.Empty
        For Each rowItem As GridViewRow In gvStudEnquiry.Rows
            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
            ENQID = DirectCast(rowItem.FindControl("lblEnqId"), Label).Text
            If hash.ContainsValue(ENQID) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next

    End Sub

    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudEnquiry.PageIndexChanging
        Try
            gvStudEnquiry.PageIndex = e.NewPageIndex
            Dim hash As New Hashtable
            If Not Session("hashCheck") Is Nothing Then
                hash = Session("hashCheck")
            End If


            Dim chk As CheckBox
            Dim ENQID As String = String.Empty
            For Each rowItem As GridViewRow In gvStudEnquiry.Rows
                ' chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
                chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
                'DirectCast(row.FindControl("lblEnqId"), Label).Text)
                ENQID = DirectCast(rowItem.FindControl("lblEnqId"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(ENQID) = False Then
                        hash.Add(ENQID, DirectCast(rowItem.FindControl("lblEnqId"), Label).Text)
                    End If
                Else
                    If hash.Contains(ENQID) = True Then
                        hash.Remove(ENQID)
                    End If
                End If
            Next
            Session("hashCheck") = hash
            GridBind()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudEnquiry_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudEnquiry.RowCommand
        Try
            If e.CommandName = "View" Or e.CommandName = "Edit" Or e.CommandName = "Print" Or e.CommandName = "RePrint" Or e.CommandName = "Followup" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStudEnquiry.Rows(index), GridViewRow)
                Dim eqsid As New Label
                Dim enqid As New Label
                Dim enqno As New Label
                Dim lnkApplName As LinkButton
                Dim url As String
                'define the datamode to view if view is clicked
                ViewState("datamode") = "edit"
                'Encrypt the data that needs to be send through Query String
                eqsid = selectedRow.FindControl("lblEqsid")
                enqid = selectedRow.FindControl("lblEnqId")
                enqno = selectedRow.FindControl("lblEnqNo")
                lnkApplName = selectedRow.FindControl("lnkApplName")
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

                Dim lblGrade As Label = selectedRow.FindControl("lblGrade")
                Dim lblShift As Label = selectedRow.FindControl("lblShift")
                Dim lblStream As Label = selectedRow.FindControl("lblStream")
                Dim editString As String = Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text + "|" + ddlAcademicYear.SelectedValue + "|" + lblGrade.Text + "|" + lblShift.Text + "|" + lblStream.Text)
                If e.CommandName = "View" Then

                    url = String.Format("~\Students\stuEnquiry_Edit.aspx?MainMnu_code={0}&datamode={1}&enqid=" + Encr_decrData.Encrypt(enqid.Text) + "&enqno=" + Encr_decrData.Encrypt(enqno.Text) + "&eqsid=" + Encr_decrData.Encrypt(eqsid.Text) + "&editstring=" + editString + "&stgcomp=0", Encr_decrData.Encrypt(ViewState("MainMnu_code")), ViewState("datamode"))
                    Response.Redirect(url)
                ElseIf e.CommandName = "Edit" Then
                    url = String.Format("~\Students\studEnqStages_Edit.aspx?MainMnu_code={0}&datamode={1}&eqsid=" + Encr_decrData.Encrypt(eqsid.Text) _
                   & "&applname=" + Encr_decrData.Encrypt(lnkApplName.Text) _
                   & "&enqno=" + Encr_decrData.Encrypt(enqno.Text), Encr_decrData.Encrypt(ViewState("MainMnu_code")), ViewState("datamode"))
                    Response.Redirect(url)
                ElseIf e.CommandName = "Print" Then
                    If ViewState("Indemnity") = False Then
                        url = String.Format("../Students/stuEnquiryPrint.aspx?MainMnu_code={0}&datamode={1}&eqsid=" + Encr_decrData.Encrypt(eqsid.Text) _
                                                             & "&enqid=" + Encr_decrData.Encrypt(enqid.Text) + "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                                                            , Encr_decrData.Encrypt(ViewState("MainMnu_code")), ViewState("datamode"))
                        ' Response.Redirect(url)
                        ','','width = 100,height=100,top=20, left=0,fullscreen=yes, scrollbars=auto'
                        'Dim jscript As New StringBuilder()
                        'jscript.Append("<script>window.open('")
                        'jscript.Append(url)
                        'jscript.Append("');</script>")
                        ' Page.RegisterStartupScript("OpenWindows", jscript.ToString())
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + url + "','_blank');", True)
                    Else
                        CallReport_Indemnity(eqsid.Text, enqid.Text, ddlAcademicYear.SelectedValue)
                    End If

                ElseIf e.CommandName = "Followup" Then

                    Dim eqs_id = Encr_decrData.Encrypt(eqsid.Text)
                    Dim enq_id = Encr_decrData.Encrypt(enqid.Text)
                    Dim accyear = ddlAcademicYear.SelectedItem.Text
                    Dim accyearval = ddlAcademicYear.SelectedValue
                    Dim grade = lblGrade.Text
                    Dim shift = lblShift.Text
                    Dim stream = lblStream.Text
                    'Dim gender = lblGender.Text
                    editString = Encr_decrData.Encrypt(accyear + "|" + accyearval + "|" + grade + "|" + shift + "|" + stream)
                    Response.Redirect("~/Students/StuFollowUpDisplay.aspx?eqs_id=" & eqs_id & "&editstring=" & editString & "&enq_id=" & enq_id & "&MainMnu_code=T6V+LqgVMFI=&datamode=Zo4HhpVNpXc=")


                ElseIf e.CommandName = "RePrint" Then

                    Session("TEMP_ApplNo") = enqno.Text
                    Session("enq_id") = enqid.Text

                    Dim vid As String = Encr_decrData.Encrypt("S100070")
                    Dim reg As String = Encr_decrData.Encrypt("yes")

                    url = String.Format("../Students/StudEnqApplAck.aspx?reg=" & reg & "&vid=" & vid)
                    'url = String.Format("../Students/StudEnqApplAck.aspx?")
                    ' Response.Redirect(url)
                    ','','width = 100,height=100,top=20, left=0,fullscreen=yes, scrollbars=auto'
                    'Dim jscript As New StringBuilder()
                    'jscript.Append("<script>window.open('")
                    'jscript.Append(url)
                    'jscript.Append("');</script>")
                    'Page.RegisterStartupScript("OpenWindows", jscript.ToString())
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + url + "','_blank');", True)
                End If
            End If
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Sub CallReport_Indemnity(ByVal EQS_ID As String, ByVal ENQ_ID As String, ByVal ACD_ID As String)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@EQM_ENQID", ENQ_ID)
        param.Add("@BSU_ID", Session("sBsuid"))
        param.Add("@ACD_ID", ACD_ID)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("..\Students\Reports\RPT\rptIndemnity_Enquiry.rpt")
        End With

        Session("rptClass") = rptClass
        Dim jscript As New StringBuilder()

        'Dim URL As String = "../Reports/ASPX Report/rptReportViewer.aspx"
        'jscript.Append("<script>window.open('")
        'jscript.Append(URL)
        'jscript.Append("');</script>")
        'Page.RegisterStartupScript("OpenWindows", jscript.ToString())
        ReportLoadSelection()
    End Sub
    Protected Sub gvStudEnquiry_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudEnquiry.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lblEQS_bFOLLOWUP As New Label
            Dim lblReason As Label
            Dim lblTooltip As Label
            lblEQS_bFOLLOWUP = e.Row.FindControl("lblEQS_bFOLLOWUP")
            lblReason = e.Row.FindControl("lblReason")
            lblTooltip = e.Row.FindControl("lblTooltip")


            If lblEQS_bFOLLOWUP IsNot Nothing Then

                If lblEQS_bFOLLOWUP.Text = "True" Then

                    Dim ib As ImageButton = e.Row.Cells(19).Controls(0)
                    If lblReason.Text = "" Then
                        ib.ImageUrl = "~\images\tick.gif"
                    Else
                        ib.ImageUrl = "~\images\" & lblReason.Text & ""
                        ib.ToolTip = lblTooltip.Text
                    End If
                Else
                    Dim ib As ImageButton = e.Row.Cells(19).Controls(0)
                    ib.ImageUrl = "~\images\cross.gif"
                End If
            End If


            Dim lbl As Label = DirectCast(e.Row.FindControl("lblEQM_ENQTYPE"), Label)
            Dim lblEQS_RFS_ID As Label = DirectCast(e.Row.FindControl("lblEQS_RFS_ID"), Label)
            Dim lblEqsId As Label = DirectCast(e.Row.FindControl("lblEqsId"), Label)
            Dim chkSelect As CheckBox = DirectCast(e.Row.FindControl("chkSelect"), CheckBox)


            'If chkDues.Checked = True Then
            chkSelect.Attributes.Add("onclick", "javascript:Showdata('" & lblEqsId.Text & "',this)")
            'End If

            If lblEQS_RFS_ID.Text = "REFER" Then
                e.Row.BackColor = System.Drawing.Color.Cornsilk
            End If
            If lbl.Text = "O" Then
                e.Row.ForeColor = System.Drawing.Color.Green
            ElseIf lbl.Text = "R" Then
                e.Row.ForeColor = System.Drawing.Color.FromArgb(27, 128, 182)
            End If
        End If
    End Sub

    Protected Sub gvStudEnquiry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudEnquiry.SelectedIndexChanged
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String
            Dim stgId As Integer
            If ViewState("action") = "select" Then
                ViewState("action") = ""
                Dim lbleqsId As Label
                With gvStudEnquiry.SelectedRow
                    lbleqsId = .FindControl("lbleqsid")
                    str_query = "SELECT ISNULL(EQS_DOC_STG_ID,0) FROM ENQUIRY_SCHOOLPRIO_S WHERE EQS_ID=" + lbleqsId.Text
                    stgId = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                    Dim chkSelect As CheckBox
                    chkSelect = .FindControl("chkSelect")
                    If stgId >= 2 Then
                        chkSelect.Enabled = True
                    Else
                        chkSelect.Enabled = False
                    End If
                End With
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

#Region "Private Methods"

    Private Sub get_Del_visible()
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        pParms(1) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "ENQ.GET_NOT_DEL_RIGHTUSERS", pParms)
        If ds.Tables(0).Rows.Count >= 1 Then
            ViewState("isDELPERMIT") = ds.Tables(0).Rows(0).Item("PERMITS")
        End If

    End Sub
    Private Sub SetChk()
        Dim i As Integer
        Dim chk As CheckBox
        If gvStudEnquiry.Rows.Count > 0 Then
            For i = 0 To gvStudEnquiry.Rows.Count - 1
                chk = gvStudEnquiry.Rows(i).FindControl("chkSelect")
                If chk.Checked = True Then
                    If list_add(chk.ClientID + "-" + gvStudEnquiry.PageIndex.ToString) = False Then
                        chk.Checked = True
                        gvStudEnquiry.Rows(i).BackColor = Drawing.Color.FromName("#f6deb2")
                    End If
                Else
                    If list_exist(chk.ClientID + "-" + gvStudEnquiry.PageIndex.ToString) = True Then
                        chk.Checked = True
                        gvStudEnquiry.Rows(i).BackColor = Drawing.Color.FromName("#f6deb2")
                    End If
                    list_remove(chk.ClientID + "-" + gvStudEnquiry.PageIndex.ToString)
                End If

            Next

        End If
    End Sub

    Protected Function GetNavigateUrl(ByVal eqsid As String) As String
        'Dim str As String = "javascript:var popup = window.showModalDialog('studJoinDocuments.aspx?eqsid=" + eqsid + "', '','dialogHeight:400px;dialogWidth:705px;scroll:yes;resizable:no;');"
        'Dim str As String = "javascript:var popup =Popup('studJoinDocuments.aspx?eqsid=" + eqsid + "');"
        Dim str As String = "javascript:ShowWindowWithClose('studJoinDocuments.aspx?eqsid=" + eqsid + "',  'search', '55%', '85%'); return false;"
        Return str
    End Function

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Private Sub GridBind()
        Dim lstrTop10 As String

        If gvStudEnquiry.Rows.Count > 0 Then
            If Session("ACD_SELECT_CLEAR") <> "ACD" Then
                lstrTop10 = ""
                Session("ACD_SELECT_CLEAR") = ""
            Else
                lstrTop10 = "Top 10"
                Session("ACD_SELECT_CLEAR") = ""
            End If
        Else
            lstrTop10 = "Top 10"
            Session("ACD_SELECT_CLEAR") = ""
        End If

        If ViewState("ACK1") Then
            gvStudEnquiry.Columns(17).Visible = True
        Else
            gvStudEnquiry.Columns(17).Visible = False
        End If



        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString


            Dim str_query As String = "SELECT " & lstrTop10 & " row_number()OVER(ORDER BY eqm_enqdate,eqs_applno) AS RowNumber, EQS_ID,EQS_APPLNO,EQM_ENQID,EQM_ENQDATE, GRM_DISPLAY, isNULL(EQM_APPL_CONTACTME,'NO') as SHF_DESCR,STM_DESCR, Left(EQM_APPLGENDER,1) as EQM_APPLGENDER,isnull(EFR_ICONS,'')EFR_ICONS,EFR_REASON," _
                                    & " APPL_NAME=isnull(EQM_APPLFIRSTNAME,'') + ' ' + ISNULL(EQM_APPLMIDNAME, '') + ' ' + ISNULL(EQM_APPLLASTNAME, '') , " _
                                    & " IMGGEMS=CASE EQM_bSTAFFGEMS WHEN 'TRUE' THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END  ," _
                                    & " IMGEX=CASE EQM_bEXSTUDENT WHEN 'TRUE' THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END , " _
                                    & " IMGSIB=CASE EQM_bAPPLSIBLING WHEN 'TRUE' THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END," _
                                    & " IMGSIS=CASE  WHEN ISNULL(EQM_PREVSCHOOL_NURSERY,0)<>0 AND ISNULL(EQM_PREVSCHOOL_NURSERY,0)<>''  AND ISNULL(EQM_PREVSCHOOL_NURSERY,'')<>4 THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END, " _
                                    & " ISNULL((CASE WHEN EQS_DOC_STG_ID>=2 THEN 'TRUE' ELSE 'FALSE' END),'TRUE') AS CHK ,isnull(EQM_ENQTYPE,'O') as EQM_ENQTYPE,case when isnull(EQS_RFS_ID,0)<>0 then 'REFER' else 'NON' end  as EQS_RFS_ID,CASE when isnull(EQS_bSEN,0)=1  THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END  as EQS_bSEN ,case when rtrim(ltrim(EQS_SEN))='' then 0 else 1 end as bSEN, EQS_SEN,DBO.GETCAL_AGE( EQM_APPLDOB,ACD_AGE_CUTOFF) 	as Appl_Age " _
                                    & " ,isnull(B.EQS_bFOLLOWUP,0) as EQS_bFOLLOWUP,isNULL(PREVSCHOOL.BSU_SHORTNAME,'')  as Prev_School_GEMS FROM   ENQUIRY_M AS A INNER JOIN " _
                                    & " ENQUIRY_SCHOOLPRIO_S AS B ON A.EQM_ENQID = B.EQS_EQM_ENQID INNER JOIN" _
                                    & " GRADE_BSU_M AS C ON B.EQS_GRM_ID = C.GRM_ID INNER JOIN " _
                                    & " SHIFTS_M AS D ON B.EQS_SHF_ID = D.SHF_ID INNER JOIN " _
                                    & " STREAM_M AS E ON B.EQS_STM_ID = E.STM_ID INNER JOIN " _
                                    & " ACADEMICYEAR_D AS ACD ON B.EQS_ACD_ID = ACD.ACD_ID LEFT JOIN " _
                                    & " BUSINESSUNIT_M  AS PREVSCHOOL ON A.EQM_PREVSCHOOL_BSU_ID = PREVSCHOOL.BSU_ID " _
                                    & " LEFT OUTER JOIN ENQUIRY_FOLLOW_UP_REASON AS FR ON FR.ENR_ENQ_ID=B.eqs_eqm_enqid  AND FR.ENR_bACTIVE=1" _
                                    & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=FR.ENR_EFR_ID " _
                                    & " WHERE EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                    & " AND EQS_BSU_ID='" + Session("sbsuid") + "' " _
                                    & " AND EQS_STATUS<>'DEL'  AND EQS_CURRSTATUS=1 "



            Dim strFilter As String = ""
            Dim strSidsearch As String()
            Dim strSearch As String

            Dim enqSearch As String = ""
            Dim nameSearch As String = ""
            Dim dateSearch As String = ""

            Dim ddlgvShift As New DropDownList
            Dim ddlgvStream As New DropDownList
            Dim ddlgvStaff As New DropDownList
            Dim ddlgvEx As New DropDownList
            Dim ddlgvSib As New DropDownList
            Dim ddlgvSis As New DropDownList
            Dim ddlgvGender As New DropDownList

            Dim selectedGrade As String = ""
            Dim selectedShift As String = ""
            Dim selectedStream As String = ""
            Dim selectedStaff As String = ""
            Dim selectedEx As String = ""
            Dim selectedSib As String = ""
            Dim selectedSis As String = ""
            Dim selectedGender As String = ""
            Dim txtSearch As New TextBox



            If Session("BSU_ENQ_SELECT") = "0" Then
                strFilter = ""
            ElseIf Session("BSU_ENQ_SELECT") = "1" Then
                strFilter = strFilter + " and EQM_PREVSCHOOL_BSU_ID IN (SELECT BSU_ID From BusinessUnit_M WITH (NOLOCK))"
            ElseIf Session("BSU_ENQ_SELECT") = "2" Then
                strFilter = strFilter + " and EQM_PREVSCHOOL_BSU_ID NOT IN (SELECT BSU_ID From BusinessUnit_M WITH (NOLOCK))"
            Else
                strFilter = strFilter + " and EQM_PREVSCHOOL_BSU_ID='" + Session("BSU_ENQ_SELECT") + "'"
            End If


            If Session("CONTACTME_SELECT") = "ALL" Then
                strFilter = strFilter + ""
            ElseIf Session("CONTACTME_SELECT") = "YES" Then
                strFilter = strFilter + " and EQM_APPL_CONTACTME='YES'"
            ElseIf Session("CONTACTME_SELECT") = "NO" Then
                strFilter = strFilter + " and isNULL(EQM_APPL_CONTACTME,'NO')='NO'"
            End If
            If ddlFollowupStatus.SelectedValue <> "0" Then
                str_query += " AND EFR_ID=" & ddlFollowupStatus.SelectedValue
            End If

            If gvStudEnquiry.Rows.Count > 0 Then



                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("eqs_applno", txtSearch.Text, strSearch)
                enqSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("convert(varchar,eqm_enqdate,106)", txtSearch.Text.Replace("/", " "), strSearch)
                dateSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("grm_display", txtSearch.Text, strSearch)
                selectedGrade = txtSearch.Text

                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                If ddlgvShift.Text <> "ALL" And ddlgvShift.Text <> "" Then
                    strFilter = strFilter + " and shf_descr='" + ddlgvShift.Text + "'"
                    selectedShift = ddlgvShift.Text
                End If

                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")
                If ddlgvStream.Text <> "ALL" And ddlgvStream.Text <> "" Then

                    strFilter = strFilter + " and stm_descr='" + ddlgvStream.Text + "'"
                    selectedStream = ddlgvStream.Text
                End If

                ddlgvGender = gvStudEnquiry.HeaderRow.FindControl("ddlgvGender")
                If ddlgvGender.Text <> "ALL" And ddlgvGender.Text <> "" Then

                    strFilter = strFilter + " and Left(EQM_APPLGENDER,1)='" + ddlgvGender.Text + "'"
                    selectedGender = ddlgvGender.Text
                End If



                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
                strSidsearch = h_Selected_menu_3.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("isnull(eqm_applfirstname,' ')+' '+isnull(eqm_applmidname,' ')+' '+isnull(eqm_appllastname,' ')", txtSearch.Text, strSearch)
                nameSearch = txtSearch.Text

                ddlgvStaff = gvStudEnquiry.HeaderRow.FindControl("ddlgvStaff")
                If ddlgvStaff.Text <> "ALL" Then
                    strFilter += " and EQM_bSTAFFGEMS=" + IIf(ddlgvStaff.Text = "YES", "'true'", "'false'")
                    selectedStaff = ddlgvStaff.Text
                End If

                ddlgvEx = gvStudEnquiry.HeaderRow.FindControl("ddlgvEx")
                If ddlgvEx.Text <> "ALL" Then
                    strFilter += " and EQM_bEXSTUDENT=" + IIf(ddlgvEx.Text = "YES", "'true'", "'false'")
                    selectedEx = ddlgvEx.Text
                End If

                ddlgvSib = gvStudEnquiry.HeaderRow.FindControl("ddlgvSib")
                If ddlgvSib.Text <> "ALL" Then
                    strFilter += " and EQM_bAPPLSIBLING=" + IIf(ddlgvSib.Text = "YES", "'true'", "'false'")
                    selectedSib = ddlgvSib.Text
                End If

                ddlgvSis = gvStudEnquiry.HeaderRow.FindControl("ddlgvSis")
                If ddlgvSis.Text <> "ALL" Then
                    If ddlgvSis.Text = "YES" Then
                        strFilter += " AND ISNULL(EQM_PREVSCHOOL_NURSERY,0)<>0 AND ISNULL(EQM_PREVSCHOOL_NURSERY,0)<>'' AND ISNULL(EQM_PREVSCHOOL_NURSERY,0)<>4"
                    ElseIf ddlgvSis.Text = "NO" Then
                        strFilter += " AND (ISNULL(EQM_PREVSCHOOL_NURSERY,0)=0 OR ISNULL(EQM_PREVSCHOOL_NURSERY,0)='' OR ISNULL(EQM_PREVSCHOOL_NURSERY,0)=4)"
                    End If
                    selectedSis = ddlgvSis.Text
                End If

                If strFilter.Trim <> "" Then
                    str_query = str_query + strFilter
                    strGlobal_Filter = strFilter
                Else
                    strGlobal_Filter = ""
                End If
            End If
            EnqCount()
            Dim ds As DataSet
            str_query += " order by eqm_enqdate desc ,eqs_applno desc"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudEnquiry.DataSource = ds

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.Tables(0).Rows(0).Item(12) = "true"
                ds.Tables(0).Rows(0)("CHK") = False
                ds.Tables(0).Rows(0)("bSEN") = False
                'ds.Tables(0).Rows(0)("EQS_bSEN") = False
                gvStudEnquiry.DataBind()
                Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
                gvStudEnquiry.Rows(0).Cells.Clear()
                gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
                gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudEnquiry.DataBind()
            End If
            'Catch ex As Exception

            ' End Try
            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
            txtSearch.Text = enqSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
            txtSearch.Text = dateSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
            txtSearch.Text = nameSearch

            ddlgvStaff = gvStudEnquiry.HeaderRow.FindControl("ddlgvStaff")
            ddlgvStaff.Text = selectedStaff

            ddlgvEx = gvStudEnquiry.HeaderRow.FindControl("ddlgvEx")
            ddlgvEx.Text = selectedEx

            ddlgvGender = gvStudEnquiry.HeaderRow.FindControl("ddlgvGender")
            ddlgvGender.Text = selectedGender

            ddlgvSib = gvStudEnquiry.HeaderRow.FindControl("ddlgvSib")
            ddlgvSib.Text = selectedSib

            ddlgvSis = gvStudEnquiry.HeaderRow.FindControl("ddlgvSis")
            ddlgvSis.Text = selectedSis

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = selectedGrade

            Dim dt As DataTable = ds.Tables(0)

            If gvStudEnquiry.Rows.Count > 0 Then
                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")

                Dim dr As DataRow
                ddlgvShift.Items.Clear()
                ddlgvShift.Items.Add("ALL")

                ddlgvStream.Items.Clear()
                ddlgvStream.Items.Add("ALL")

                For Each dr In dt.Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr
                        ''check for duplicate values and add to dropdownlist 

                        If ddlgvShift.Items.FindByText(.Item(6)) Is Nothing Then
                            ddlgvShift.Items.Add(.Item(6))
                        End If
                        If ddlgvStream.Items.FindByText(.Item(7)) Is Nothing Then
                            ddlgvStream.Items.Add(.Item(7))
                        End If
                    End With
                Next

                If selectedShift <> "" Then
                    ddlgvShift.Text = selectedShift
                End If

                If selectedStream <> "" Then
                    ddlgvStream.Text = selectedStream
                End If
            End If
            studClass.SetChk(gvStudEnquiry, Session("liUserList"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub EnqCount()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STAFF=(SELECT COUNT(EQS_ID) FROM ENQUIRY_SCHOOLPRIO_S " _
                                  & " AS A  INNER JOIN ENQUIRY_M AS B " _
                                  & " ON A.EQS_EQM_ENQID=B.EQM_ENQID " _
                                  & " INNER JOIN GRADE_BSU_M AS C ON A.EQS_GRM_ID = C.GRM_ID " _
                                  & " INNER JOIN SHIFTS_M AS D ON A.EQS_SHF_ID = D.SHF_ID  " _
                                  & " INNER JOIN STREAM_M AS E ON A.EQS_STM_ID = E.STM_ID " _
                                  & " WHERE EQM_bSTAFFGEMS='TRUE'" _
                                  & " AND EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND EQS_BSU_ID='" + Session("sbsuid") + "'  " + strGlobal_Filter _
                                  & "  AND EQS_STATUS<>'DEL'  AND EQS_CURRSTATUS=1), " _
                                  & "EXSTUDENT=(SELECT COUNT(EQS_ID) FROM ENQUIRY_SCHOOLPRIO_S " _
                                   & " AS A  INNER JOIN ENQUIRY_M AS B " _
                                  & " ON A.EQS_EQM_ENQID=B.EQM_ENQID " _
                                  & " INNER JOIN GRADE_BSU_M AS C ON A.EQS_GRM_ID = C.GRM_ID " _
                                    & " INNER JOIN SHIFTS_M AS D ON A.EQS_SHF_ID = D.SHF_ID  " _
                                  & " INNER JOIN STREAM_M AS E ON A.EQS_STM_ID = E.STM_ID " _
                                  & " WHERE EQM_bEXSTUDENT='TRUE'" _
                                  & " AND EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND EQS_BSU_ID='" + Session("sbsuid") + "' " + strGlobal_Filter _
                                  & "  AND EQS_STATUS<>'DEL'  AND EQS_CURRSTATUS=1), " _
                                  & "SIBLING=(SELECT COUNT(EQS_ID) FROM ENQUIRY_SCHOOLPRIO_S " _
                                  & " AS A  INNER JOIN ENQUIRY_M AS B " _
                                  & " ON A.EQS_EQM_ENQID=B.EQM_ENQID " _
                                   & " INNER JOIN GRADE_BSU_M AS C ON A.EQS_GRM_ID = C.GRM_ID " _
                                                                     & " INNER JOIN SHIFTS_M AS D ON A.EQS_SHF_ID = D.SHF_ID  " _
                                  & " INNER JOIN STREAM_M AS E ON A.EQS_STM_ID = E.STM_ID " _
                                  & " WHERE EQM_bAPPLSIBLING='TRUE'" _
                                  & " AND EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND EQS_BSU_ID='" + Session("sbsuid") + "' " + strGlobal_Filter _
                                  & "  AND EQS_STATUS<>'DEL'  AND EQS_CURRSTATUS=1) ," _
                                  & "SIS=(SELECT COUNT(EQS_ID) FROM ENQUIRY_SCHOOLPRIO_S " _
                                  & " AS A  INNER JOIN ENQUIRY_M AS B " _
                                  & " ON A.EQS_EQM_ENQID=B.EQM_ENQID " _
                                 & " INNER JOIN GRADE_BSU_M AS C ON A.EQS_GRM_ID = C.GRM_ID " _
                                                                   & " INNER JOIN SHIFTS_M AS D ON A.EQS_SHF_ID = D.SHF_ID  " _
                                  & " INNER JOIN STREAM_M AS E ON A.EQS_STM_ID = E.STM_ID " _
                                & " WHERE  ISNULL(EQM_PREVSCHOOL_NURSERY,0)<>0 AND ISNULL(EQM_PREVSCHOOL_NURSERY,0)<>'' and ISNULL(EQM_PREVSCHOOL_NURSERY,0)<>4" _
                                  & " AND EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND EQS_BSU_ID='" + Session("sbsuid") + "' " + strGlobal_Filter _
                                  & " AND EQS_STATUS<>'DEL'  AND EQS_CURRSTATUS=1) , " _
                                  & " GP=(SELECT COUNT(EQS_ID) FROM ENQUIRY_SCHOOLPRIO_S " _
                                  & " AS A  INNER JOIN ENQUIRY_M AS B " _
                                  & " ON A.EQS_EQM_ENQID=B.EQM_ENQID " _
                                   & " INNER JOIN GRADE_BSU_M AS C ON A.EQS_GRM_ID = C.GRM_ID " _
                                                                     & " INNER JOIN SHIFTS_M AS D ON A.EQS_SHF_ID = D.SHF_ID  " _
                                  & " INNER JOIN STREAM_M AS E ON A.EQS_STM_ID = E.STM_ID " _
                                & " WHERE EQM_bSTAFFGEMS='False'  AND  EQM_bEXSTUDENT='False'   AND EQM_bAPPLSIBLING='False' AND (isNULL(eqm_prevschool_NURSERY,'')='' OR  eqm_prevschool_NURSERY=0 OR isNULL(eqm_prevschool_NURSERY,'')=4 )" _
                                  & " AND EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " AND EQS_BSU_ID='" + Session("sbsuid") + "' " + strGlobal_Filter _
                                  & "  AND EQS_STATUS<>'DEL'  AND EQS_CURRSTATUS=1)"

        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            lblStaff.Text = reader.GetValue(0).ToString
            lblEx.Text = reader.GetValue(1).ToString
            lblSib.Text = reader.GetValue(2).ToString
            lblSis.Text = reader.GetValue(3).ToString
            lblGP.Text = reader.GetValue(4).ToString
        End While
        reader.Close()
    End Sub

    Public Function returnpath(ByVal bYes As Boolean) As String
        Try
            'Dim Flag_status As Boolean '= Convert.ToBoolean(Status)
            If bYes = True Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getmenuid(Optional ByVal p_imgsrc As String = "", Optional ByVal imagename As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudEnquiry.HeaderRow.FindControl(imagename)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Private Sub SaveData(ByVal mode As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim i As Integer
            Dim str_query As String
            Dim chkSelect As New CheckBox
            Dim eqsID As New Label
            Dim st As String = "", totstr As String = ""

            Dim chk As CheckBox
            Dim ENQID As String = String.Empty
            Dim hash As New Hashtable
            If Not Session("hashCheck") Is Nothing Then
                hash = Session("hashCheck")
            End If
            For Each rowItem As GridViewRow In gvStudEnquiry.Rows

                chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

                ENQID = DirectCast(rowItem.FindControl("lblEqsId"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(ENQID) = False Then
                        hash.Add(ENQID, DirectCast(rowItem.FindControl("lblEqsId"), Label).Text)
                    End If
                Else
                    If hash.Contains(ENQID) = True Then
                        hash.Remove(ENQID)
                    End If
                End If



            Next

            Session("hashCheck") = hash


            If Not Session("hashCheck") Is Nothing Then
                hash = Session("hashCheck")
                Dim hashloop As DictionaryEntry

                For Each hashloop In hash
                    eqsID.Text = hashloop.Value

                    If mode = "DEL" Then
                        st = check_followup_remark(eqsID.Text)

                        If st = "" Then
                            str_query = "exec studSaveStudEnqAcceptReject " + eqsID.Text + ",'" + mode + "'," + ddlAcademicYear.SelectedValue.ToString
                            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                        End If

                        If totstr = "" Then
                            totstr = st
                        Else
                            totstr = totstr & "," & st
                        End If



                    Else
                        str_query = "exec studSaveStudEnqAcceptReject " + eqsID.Text + ",'" + mode + "'," + ddlAcademicYear.SelectedValue.ToString
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    End If

                Next
                Session.Remove("hashCheck")

            Else
                lblError.Text = "No record selected for shortlisting"

            End If
            If mode = "DEL" Then
                If totstr <> "" Then
                    lblError.Text = "Enq Nos " & totstr & " cannot be delete. It doesnot have valid follow up status with remarks"
                Else
                    lblError.Text = "Records removed successfully"
                End If

            End If


            'For i = 0 To gvStudEnquiry.Rows.Count - 1
            '    chkSelect = gvStudEnquiry.Rows(i).Cells(0).FindControl("chkSelect")
            '    If chkSelect.Checked = True Then
            '        eqsID = gvStudEnquiry.Rows(i).Cells(1).FindControl("lblEqsId")
            '        str_query = "exec studSaveStudEnqAcceptReject " + eqsID.Text + ",'" + mode + "'," + ddlAcademicYear.SelectedValue.ToString
            '        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            '    End If
            'Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Function check_followup_remark(ByVal eqsid As Integer) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim str As String = "", applnno As String = "", v As Integer
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@eqs_id", eqsid)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "enq.check_enq_followup_remark", param)


        If ds.Tables(0).Rows.Count >= 1 Then
            v = ds.Tables(0).Rows(0).Item("VALID")
            applnno = ds.Tables(0).Rows(0).Item("EQS_APPLNO")
        End If
        If v = 0 Then
            str = applnno
        Else
            str = ""
        End If
        Return str
    End Function

#End Region

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim ChkState As Boolean = False

        Dim chk As CheckBox
        Dim ENQID As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If
        For Each rowItem As GridViewRow In gvStudEnquiry.Rows

            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

            ENQID = DirectCast(rowItem.FindControl("lblEnqId"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(ENQID) = False Then
                    hash.Add(ENQID, DirectCast(rowItem.FindControl("lblEnqId"), Label).Text)
                End If
            Else
                If hash.Contains(ENQID) = True Then
                    hash.Remove(ENQID)
                End If
            End If
        Next

        Session("hashCheck") = hash

        If Not Session("hashCheck") Is Nothing Then
            CallReport()
        Else
            lblError.Text = "No Enquiry selected for print"
        End If
    End Sub
    

    Private Sub CallReport()

        Dim EQS_EQM_ENQIDs As New StringBuilder
        Dim hash As New Hashtable

        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                EQS_EQM_ENQIDs.Append(hashloop.Value)
                EQS_EQM_ENQIDs.Append("|")
            Next
        End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@EQS_EQM_ENQIDs", EQS_EQM_ENQIDs.ToString)
        param.Add("@STU_BSU_ID", Session("sBsuid"))
        param.Add("ACD_YEAR", ddlAcademicYear.SelectedItem.Text)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("..\Students\Reports\RPT\rptENQUIRY_NEW_LIST.rpt")
        End With
        Session.Remove("hashCheck")
        Session("rptClass") = rptClass
        ' Dim jscript As New StringBuilder()

        'Dim URL As String = "../Reports/ASPX Report/rptReportViewer.aspx"
        'jscript.Append("<script>window.open('")
        'jscript.Append(URL)
        'jscript.Append("');</script>")
        'Page.RegisterStartupScript("OpenWindows", jscript.ToString())
        ReportLoadSelection()
    End Sub

    Protected Sub btnprintlabel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("ENQ_ID") = ""
        Dim chk As CheckBox
        Dim ENQID As String
        For Each rowItem As GridViewRow In gvStudEnquiry.Rows
            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
            ENQID = DirectCast(rowItem.FindControl("lblEnqId"), Label).Text
            If chk.Checked = True Then
                ViewState("ENQ_ID") += ENQID + ","
            End If
        Next
        If ViewState("ENQ_ID").ToString.Trim = "" Then
            lblError.Text = "No Records Selected To Print....!!!"
        Else
            h_print.Value = "print"
            printlabel()
        End If
    End Sub

    Sub printlabel()
        Try
            Dim param As New Hashtable
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")

            param.Add("@EQS_EQM_ENQID", ViewState("ENQ_ID"))
            'param.Add("UserName", Session("sUsr_name"))
            'param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = param
                .reportPath = Server.MapPath("~/Students/Reports/RPT/rptEnqLabel.rpt")
            End With
            Session("rptClass") = rptClass
            '' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

            'Dim url As String, strRedirect As String
            ' strRedirect = "~/Reports/ASPX Report/rptReportViewer.aspx"
            'strRedirect = "~/Reports/ASPX Report/RptViewerModal.aspx"
            'url = String.Format("{0}", strRedirect)
            'ResponseHelper.Redirect(url, "_blank", "")
            ' Session("ReportSource") = rptClass
            'ReportLoadSelection()
        Catch ex As Exception
            lblError.Text = "Request cannot be completed"

        End Try
    End Sub

    Protected Sub chkDues_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDues.CheckedChanged
        GridBind()
    End Sub
    Sub bind_cur_status()
        ddlFollowupStatus.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFR_ID,EFR_REASON   FROM ENQ_FOLLOW_UP_REASON_M  "



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlFollowupStatus.DataSource = ds
        ddlFollowupStatus.DataTextField = "EFR_REASON"
        ddlFollowupStatus.DataValueField = "EFR_ID"
        ddlFollowupStatus.DataBind()
        ddlFollowupStatus.Items.Add(New ListItem("ALL", "0"))
        ddlFollowupStatus.Items.FindByText("ALL").Selected = True
    End Sub

    Protected Sub ddlFollowupStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFollowupStatus.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub lbtnResend1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim transaction As SqlTransaction
        Dim calltransaction As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim str As String = ""
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try


                Dim lblEnqId As New Label
                lblEnqId = TryCast(sender.FindControl("lblEnqId"), Label)
                Dim lblEqsId As New Label
                lblEqsId = TryCast(sender.FindControl("lblEqsId"), Label)

                str = lblEnqId.Text + "|" + Encr_decrData.Encrypt(lblEnqId.Text + "|" + lblEqsId.Text) + ","

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim pParms(4) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@delimitedStr", str)
                pParms(1) = New SqlClient.SqlParameter("@ENQ_ID", lblEnqId.Text)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENQ.RESEND_EMAIL_ACK1", pParms)
                Dim ReturnFlag As Integer = pParms(2).Value

                If ReturnFlag <> 0 Then
                    calltransaction = "1"

                Else
                    calltransaction = "0"

                    lblError.Text = "Applicant mail resend sucessfully!!! "

                End If


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Request could not be processed "
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else

                    errorMessage = ""
                    transaction.Commit()
                    GridBind()
                End If
            End Try



        End Using
    End Sub
    Private Sub BIND_BSU_DETAILS()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter

        param(0) = New SqlParameter("@BSU_ID", Session("sbsuid"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ENQ.GET_EMAIL_RESEND_BSU", param)
            While datareader.Read
                ViewState("ACK1") = Convert.ToString(datareader("ERT_ACK1"))
                ViewState("ACK2") = Convert.ToString(datareader("ERT_ACK2"))


            End While
        End Using
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


End Class
