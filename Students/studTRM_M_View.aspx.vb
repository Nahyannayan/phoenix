Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports UtilityObj
Partial Class Students_studTRM_M_View
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Dim str_err As String = ""
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code for Term Master
                If USR_NAME = "" Or MainMnu_code <> "S050010" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("Y_DESCR") = "All"
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    'Get the Active year
                    'Dim temp_Year As String = String.Empty
                    Using readerActive_year As SqlDataReader = AccessStudentClass.GetActive_Year(Session("sBsuid"), Session("CLM"))
                        While readerActive_year.Read

                            ViewState("Y_DESCR") = Convert.ToString(readerActive_year("Y_DESCR"))
                        End While

                    End Using
                    callYEAR_DESCRBind(ViewState("Y_DESCR"))
                    Call gridbind()
                    FillNewAcadamicYear()

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Sub FillNewAcadamicYear()
        ddlAcdYearCopy.DataSource = FeeCommon.GetAcademicYear(Session("sBsuid"), "NEW")
        ddlAcdYearCopy.DataTextField = "ACY_DESCR"
        ddlAcdYearCopy.DataValueField = "ACD_ID"
        ddlAcdYearCopy.DataBind()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String



        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))


    End Sub

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAcademic.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAcademic.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAcademic.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAcademic.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvAcademic.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAcademic.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""


            Dim str_filter_TERM_DESCR As String = String.Empty

            Dim str_filter_STARTDT As String = String.Empty
            Dim str_filter_ENDDT As String = String.Empty

            Dim ds As New DataSet



            str_Sql = "Select TRM_ID,Y_DESCR,TRM_DESCR,STARTDT,ENDDT from(SELECT TRM_M.TRM_DESCRIPTION as TRM_DESCR , TRM_M.TRM_STARTDATE as STARTDT, TRM_M.TRM_ENDDATE as ENDDT," & _
 " TRM_M.TRM_BSU_ID as BSU_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR, TRM_M.TRM_ACD_ID as ACD_ID,TRM_M.TRM_ID as TRM_ID,ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID  FROM ACADEMICYEAR_D " & _
" INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
 " TRM_M ON ACADEMICYEAR_D.ACD_ID = TRM_M.TRM_ACD_ID)a where  a.CLM_ID='" & Session("CLM") & "' and a.BSU_ID='" & Session("sBsuid") & "' AND A.ACD_ID<>''"



            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim ddlY_DESCRH As New DropDownList
            Dim str_search As String

            Dim str_TRM_DESCR As String = String.Empty
            Dim str_STARTDT As String = String.Empty
            Dim str_ENDDT As String = String.Empty



            If gvAcademic.Rows.Count > 0 Then

                Dim str_Sid_search() As String








                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAcademic.HeaderRow.FindControl("txtTRM_DESCR")
                str_TRM_DESCR = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_TERM_DESCR = "  AND  NOT a.TRM_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_TERM_DESCR = " AND a.TRM_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAcademic.HeaderRow.FindControl("txtSTARTDT")

                str_STARTDT = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STARTDT = " AND a.STARTDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STARTDT = "  AND  NOT a.STARTDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STARTDT = " AND a.STARTDT  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STARTDT = " AND a.STARTDT NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STARTDT = " AND a.STARTDT LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STARTDT = " AND a.STARTDT NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAcademic.HeaderRow.FindControl("txtENDDT")

                str_ENDDT = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_ENDDT = " AND a.ENDDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_ENDDT = "  AND  NOT a.ENDDT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_ENDDT = " AND a.ENDDT  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_ENDDT = " AND a.ENDDT  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_ENDDT = " AND a.ENDDT LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_ENDDT = " AND a.ENDDT NOT LIKE '%" & txtSearch.Text & "'"
                End If


            End If



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & ViewState("str_filter_Year") & str_filter_TERM_DESCR & str_filter_STARTDT & str_filter_ENDDT & "  order by  a.Y_DESCR")

            If ds.Tables(0).Rows.Count > 0 Then

                gvAcademic.DataSource = ds.Tables(0)
                gvAcademic.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not


                gvAcademic.DataSource = ds.Tables(0)
                Try
                    gvAcademic.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAcademic.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAcademic.Rows(0).Cells.Clear()
                gvAcademic.Rows(0).Cells.Add(New TableCell)
                gvAcademic.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAcademic.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAcademic.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvAcademic.HeaderRow.FindControl("txtTRM_DESCR")
            txtSearch.Text = str_TRM_DESCR
            txtSearch = gvAcademic.HeaderRow.FindControl("txtSTARTDT")
            txtSearch.Text = str_STARTDT
            txtSearch = gvAcademic.HeaderRow.FindControl("txtENDDT")
            txtSearch.Text = str_ENDDT




            ddlY_DESCRH = gvAcademic.HeaderRow.FindControl("ddlY_DESCRH ")



            callYEAR_DESCRBind(ViewState("Y_DESCR"))



            Call ddlYear_state(ddlY_DESCRH.SelectedItem.Text)





            set_Menu_Img()




        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub




    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lblTRM_ID As New Label
            Dim url As String
            Dim viewid As String

            lblTRM_ID = TryCast(sender.FindControl("lblTRM_ID"), Label)
            viewid = lblTRM_ID.Text


            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            MainMnu_code = Request.QueryString("MainMnu_code")

            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studTRM_M.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            MainMnu_code = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studTRM_M.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
        End Try
    End Sub
    Sub ddlYear_state(ByVal selText As String)
        Try

            Dim ItemTypeCounter As Integer
            Dim ddlY_DESCRH As New DropDownList
            ddlY_DESCRH = gvAcademic.HeaderRow.FindControl("ddlY_DESCRH")
            For ItemTypeCounter = 0 To ddlY_DESCRH.Items.Count - 1
                'keep loop until you get the counter for default Grade into  the SelectedIndex

                If ddlY_DESCRH.Items(ItemTypeCounter).Text = selText Then
                    ddlY_DESCRH.SelectedIndex = ItemTypeCounter
                End If
            Next
            '  ddlOpenonLineH.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlACY_DESCRH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("Y_DESCR") = sender.SelectedItem.Text
        callYEAR_DESCRBind(ViewState("Y_DESCR"))
        gridbind()
    End Sub
    Public Sub callYEAR_DESCRBind(Optional ByVal p_selected As String = "All")

        Try

            Dim ddlY_DESCRH As New DropDownList
            ddlY_DESCRH = gvAcademic.HeaderRow.FindControl("ddlY_DESCRH")

            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))

                ddlY_DESCRH.Items.Clear()
                di = New ListItem("All", "All")
                ddlY_DESCRH.Items.Add(di)
                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACY_ID"))
                        ddlY_DESCRH.Items.Add(di)

                    End While
                End If
            End Using

            If p_selected <> "All" Then
                ViewState("str_filter_Year") = " AND a.Y_DESCR = '" & p_selected & "'"
            Else
                ViewState("str_filter_Year") = " AND a.Y_DESCR <>''"
            End If
            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlY_DESCRH.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not ViewState("Y_DESCR") Is Nothing Then
                    If ddlY_DESCRH.Items(ItemTypeCounter).Text = ViewState("Y_DESCR") Then
                        ddlY_DESCRH.SelectedIndex = ItemTypeCounter
                    End If
                End If

            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearchTRM_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSTARTDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchENDDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub gvAcademic_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAcademic.PageIndexChanging
        gvAcademic.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub ChkCopy_CheckedChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        If ChkCopy.Checked Then
            TrNew.Visible = True
        Else
            TrNew.Visible = False
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtOldTerms As New DataTable
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("mainDBO").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()

        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Dim pParms(4) As SqlParameter
        Dim pParms1(1) As SqlParameter
        Dim NewTermid As Integer


        If ddlAcdYearCopy.Items.Count = 0 Then
            'lblError.Text = "Please Set New Acadamic Year..!"
            usrMessageBar.ShowNotification("Please Set New Acadamic Year..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If ddlAcademic.Items.Count = 0 Then
            'lblError.Text = "Please Set New Acadamic Year..!"
            usrMessageBar.ShowNotification("Please Set New Acadamic Year..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        Dim startDate1 As Date = Mainclass.getDataValue("select ACD_STARTDT from ACADEMICYEAR_D where ACD_ID =" & ddlAcademic.SelectedItem.Value & " and ACD_BSU_ID='" & Session("sBsuid") & "'", "OASISConnectionString")
        Dim startDate2 As Date = Mainclass.getDataValue("select ACD_STARTDT from ACADEMICYEAR_D where ACD_ID =" & ddlAcdYearCopy.SelectedItem.Value & " and ACD_BSU_ID='" & Session("sBsuid") & "'", "OASISConnectionString")

        If DateDiff(DateInterval.Year, startDate1, startDate2) <> 1 Or startDate1 > startDate2 Or DateDiff(DateInterval.Year, startDate1, startDate2) = 0 Then
            'lblError.Text = "You can only copy the terms to the next academic year..!"
            usrMessageBar.ShowNotification("You can only copy the terms to the next academic year..!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        If Mainclass.getDataValue("select COUNT(*) from TRM_M  where TRM_ACD_ID =" & ddlAcdYearCopy.SelectedItem.Value, "OASISConnectionString") <> 0 Then
            'lblError.Text = "Can not copy.Already Terms Defined...!"
            usrMessageBar.ShowNotification("Can not copy.Already Terms Defined...!", UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        End If

        dtOldTerms = Mainclass.getDataTable("select *from TRM_M where TRM_ACD_ID=" & ddlAcademic.SelectedItem.Value & "", ConnectionManger.GetOASISConnectionString)
        For Each mRow As DataRow In dtOldTerms.Rows
            pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            pParms(2) = Mainclass.CreateSqlParameter("@NEW_ACA_YAER", ddlAcdYearCopy.SelectedItem.Value, SqlDbType.Int)
            pParms(3) = Mainclass.CreateSqlParameter("@OLD_TERM_ID", mRow("TRM_ID"), SqlDbType.Int)
            pParms(4) = New SqlClient.SqlParameter("@NEW_TRM_ID", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.Output
            str_err = Mainclass.ExecuteParamQRY(objConn, stTrans, "[dbo].[SaveCopyStudentTerm]", pParms)

            If str_err <> "0" Then
                Exit For
            End If
            NewTermid = pParms(4).Value

            FillTermMonths(mRow("TRM_STARTDATE"), mRow("TRM_ENDDATE"), Session("sBsuid"), NewTermid, ddlAcdYearCopy.SelectedItem.Value, stTrans)

            FillTermWeeks(mRow("TRM_STARTDATE"), mRow("TRM_ENDDATE"), Session("sBsuid"), NewTermid, stTrans)

            pParms1(0) = Mainclass.CreateSqlParameter("@TRM_ID", NewTermid, SqlDbType.VarChar)
            str_err = Mainclass.ExecuteParamQRY(objConn, stTrans, "[dbo].[UpdateTermsWeeks]", pParms1)


            If str_err <> "0" Then
                Exit For
            End If
        Next

        If str_err = "0" Then
            stTrans.Commit()
            'lblError.Text = getErrorMessage(str_err)
            usrMessageBar.ShowNotification(getErrorMessage(str_err), UserControls_usrMessageBar.WarningType.Danger)
        Else
            stTrans.Rollback()
            'lblError.Text = getErrorMessage(str_err)
            usrMessageBar.ShowNotification(getErrorMessage(str_err), UserControls_usrMessageBar.WarningType.Danger)
        End If

    End Sub
    Private Sub FillTermMonths(ByVal Sdate As Date, ByVal Edate As Date, ByVal BsuId As String, ByVal NewTermid As Integer, ByVal New_Acd_id As Integer, ByRef Trans As SqlTransaction)

        Dim frmdt As DateTime = DateAdd(DateInterval.Year, 1, Sdate)
        Dim todt As DateTime = DateAdd(DateInterval.Year, 1, Edate)
        Dim tempdt As DateTime = todt
        Dim i As Integer = 1
        Dim pParms(6) As SqlParameter
        Dim retval As Integer
        Dim TempToDate As Date

        While (frmdt < todt)
            TempToDate = frmdt.AddDays(DateTime.DaysInMonth(frmdt.Year, frmdt.Month) - frmdt.Day)
            pParms(1) = Mainclass.CreateSqlParameter("@AMS_ACD_ID", New_Acd_id, SqlDbType.Int)
            pParms(2) = Mainclass.CreateSqlParameter("@AMS_MONTH", frmdt.Month, SqlDbType.Int)
            pParms(3) = Mainclass.CreateSqlParameter("@AMS_TRM_ID", NewTermid, SqlDbType.Int)
            pParms(4) = Mainclass.CreateSqlParameter("@AMS_ORDER", i, SqlDbType.Int)
            pParms(5) = Mainclass.CreateSqlParameter("@AMS_DTFROM", frmdt, SqlDbType.DateTime)
            pParms(6) = Mainclass.CreateSqlParameter("@AMS_DTTO", TempToDate, SqlDbType.DateTime)

            retval = SqlHelper.ExecuteNonQuery(Trans, CommandType.StoredProcedure, "[dbo].[SaveCopyStudentTerm_Months]", pParms)
            frmdt = TempToDate.AddDays(1)
            i = i + 1
        End While
    End Sub
    Private Sub FillTermWeeks(ByVal Sdate As Date, ByVal Edate As Date, ByVal BsuId As String, ByVal NewTermid As Integer, ByRef trans As SqlTransaction)
        Dim frmdt As DateTime = DateAdd(DateInterval.Year, 1, Sdate)
        Dim todt As DateTime = DateAdd(DateInterval.Year, 1, Edate)
        Dim pParms(3) As SqlParameter
        Dim retval As Integer


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            cmd.Parameters.AddWithValue("@FROMDT", frmdt)
            cmd.Parameters.AddWithValue("@TODT", todt)
            cmd.Parameters.AddWithValue("@BSU_ID", Session("sBSUID"))
            cmd.Parameters.AddWithValue("@ACD_ID", Session("Current_ACD_ID"))
            cmd.CommandText = "[FEES].[F_GetWEEKSForPeriod]"

            Dim drRead As SqlDataReader = cmd.ExecuteReader()

            While (drRead.Read())
                pParms(1) = Mainclass.CreateSqlParameter("@AWS_TRM_ID", NewTermid, SqlDbType.Int)
                pParms(2) = Mainclass.CreateSqlParameter("@AWS_DTFROM", Format(drRead("FRMDT"), OASISConstants.DateFormat), SqlDbType.DateTime)
                pParms(3) = Mainclass.CreateSqlParameter("@AWS_DTTO", Format(drRead("TODT"), OASISConstants.DateFormat), SqlDbType.DateTime)
                retval = SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[dbo].[SaveCopyStudentTerm_Weeks]", pParms)
            End While

        End Using

    End Sub





End Class

