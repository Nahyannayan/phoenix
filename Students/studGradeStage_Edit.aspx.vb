Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collection
Partial Class Students_studGradeStage_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            '   Try


            'Dim MainMnu_code As String = String.Empty
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S050035") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'GetActive_ACD_4_Grade


                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                lblAcademicYear.Text = Encr_decrData.Decrypt(Request.QueryString("academicyear").Replace(" ", "+"))
                hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                hfSTM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                lblStream.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))

                BindList()
            End If
            'Catch ex As Exception

            '    lblError.Text = "Request could not be processed "
            'End Try

        End If


    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindList()
        lstRequired.Items.Clear()
        lstNotRequired.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT PRG_ID,PRO_DESCRIPTION, PRG_bREQUIRED" _
                                 & " FROM PROCESSFO_GRADE_M INNER JOIN" _
                                 & " PROCESSFO_SYS_M ON PROCESSFO_GRADE_M.PRG_STG_ID = PROCESSFO_SYS_M.PRO_ID" _
                                 & " WHERE PRG_GRD_ID='" + hfGRD_ID.Value.ToString + "' AND PRG_STM_ID=" + hfSTM_ID.Value _
                                 & " AND PRG_ACD_ID=" + hfACD_ID.Value + " AND PRG_STG_ID BETWEEN 3 AND 6 ORDER BY PRG_STG_ORDER"

        Dim chk As CheckBox
        Dim txt As TextBox


        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim i As Integer = 0
        Dim li As ListItem

        While reader.Read
            li = New ListItem
            li.Value = reader.GetValue(0)
            li.Text = reader.GetString(1)
            If reader.GetBoolean(2) = True Then
                lstRequired.Items.Add(li)
            Else
                lstNotRequired.Items.Add(li)
            End If

        End While
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim lstCount As Integer

        Dim strReqs As String()
        Dim strNotReqs As String()
        Dim strReq As String()
        Dim strNotReq As String()
        If lstValuesReq.Value.Length <> 0 Then
            strReqs = lstValuesReq.Value.Split("|")
            For i = 0 To strReqs.Length - 1
                strReq = strReqs(i).Split("$")
                str_query = "exec studUPDATEGRADESTAGES " + strReq(1) + ",'true'," + (3 + i).ToString
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next

        End If
        If lstValuesNotReq.Value.Length <> 0 Then
            strNotReqs = lstValuesNotReq.Value.Split("|")
            lstCount = strReqs.Length
            For i = 0 To strNotReqs.Length - 1
                strNotReq = strNotReqs(i).Split("$")
                str_query = "exec studUPDATEGRADESTAGES " + strNotReq(1) + ",'false'," + (lstCount + 3 + i).ToString
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        BindList()
        LBLERROR.TEXT = "Record saved successfully"
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
End Class
