﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.HttpContext
Imports System.Globalization
Imports GemBox.Spreadsheet
Partial Class Students_StudentDashboardstatistics_View
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Try
                If Not Request.QueryString("Acyid") Is Nothing AndAlso Not Request.QueryString("ActLvl") Is Nothing AndAlso Not Request.QueryString("LvlBsu") Is Nothing Then
                    Dim Acyids As String = Convert.ToString(Request.QueryString("Acyid"))
                    Dim ActLvl As String = Convert.ToString(Request.QueryString("ActLvl"))
                    Dim LvlBsu As String = Convert.ToString(Request.QueryString("LvlBsu"))
                    ViewState("Acyids") = Acyids
                    ViewState("regionID") = ActLvl
                    ViewState("Stat") = LvlBsu
                    BindBsuInfo(Acyids, ActLvl, LvlBsu)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        tblSecondTable.Visible = False
        tblFirstTable.Visible = True
    End Sub
    Sub imgExport1_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
        Dim lblBsuId As New Label()
        lblBsuId = TryCast(sender.FindControl("lblBsuId"), Label)
        ViewState("Actual_BSU") = lblBsuId.Text

    End Sub
    Protected Sub gvBsuWiseGrid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBsuWiseGrid.PageIndexChanging
        gvBsuWiseGrid.PageIndex = e.NewPageIndex
        BindBsuInfo(ViewState("bsuCity"), ViewState("regionID"), ViewState("Stat"))
    End Sub
    Private Sub BindBsuInfo(ByVal Acyids As String, ByVal ActLvl As String, ByVal LvlBsu As String)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim DS As New DataSet
            Dim param(8) As SqlParameter
            Dim param1(1) As SqlParameter
            Dim bsuCityCode As String = String.Empty
            param1(0) = New SqlParameter("@EMR_ID", 1)

            bsuCityCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "CORP.GET_CITY_CODE_BY_ID", param1)

            Dim fromdat As Date = Date.Today
            Dim toDate As Date = Date.Today
            If Not Session("FusionFromDt") Is Nothing Then
                fromdat = Convert.ToDateTime(Session("FusionFromDt"))
            End If

            If Not Session("FusionToDt") Is Nothing Then
                toDate = Convert.ToDateTime(Session("FusionToDt"))
            End If
            'param(0) = New SqlParameter("@ACT_LVL", ActLvl)

            param(0) = New SqlParameter("@ACT_LVL", "10")
            param(1) = New SqlParameter("@LVL_BSU", Session("LVL_BSU"))
            param(2) = New SqlParameter("@FromDate", fromdat)
            param(3) = New SqlParameter("@ToDate", toDate)
            param(4) = New SqlParameter("@BSU_CITY", bsuCityCode)
            param(5) = New SqlParameter("@Stat", "")
            param(6) = New SqlParameter("@BSU_ID", Convert.ToString(Session("sBsuid")))
            If Not Session("strAcyId") Is Nothing Then
                param(7) = New SqlParameter("@ACY_IDS", Acyids)
            End If
            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[CORP].[STUDENT_DASHBOARD_STATISTICS]", param)
            gvBsuWiseGrid.DataSource = DS.Tables(0)
            gvBsuWiseGrid.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboardpopupbsu", ex.Message)
        End Try
    End Sub
End Class
