Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports CURRICULUM

Partial Class UpdateGUSI
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                        'Else
                        '    Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed "
            End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub ddlSearchBy_SelectedIndexChanged(sender As Object, e As EventArgs)
        If ddlSearchBy.SelectedValue = "0" Then
            tblGUSI.Visible = True
            tblStudentDetails.Visible = False
            tblStuNo.Visible = False
        ElseIf ddlSearchBy.SelectedValue = "1" Then
            tblGUSI.Visible = False
            tblStudentDetails.Visible = False
            tblStuNo.Visible = True
        Else
            tblGUSI.Visible = False
            tblStudentDetails.Visible = True
            tblStuNo.Visible = False
        End If
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        Try
            lblError.Text = ""
            'If h_STU_IDs.Value.Trim.Length = 0 Then
            '    lblError.Text = "Please enter the student."
            '    txtStudIDs.Focus()
            '    Exit Sub
            'End If

            If ddlSearchBy.SelectedValue = "0" And txtGUSI.Text.Trim.Length = 0 Then
                lblError.Text = "Please enter the GUSI number."
                Exit Sub
            End If

            If ddlSearchBy.SelectedValue = "1" And txtStuNo.Text.Trim.Length = 0 Then
                lblError.Text = "Please enter the Student number."
                Exit Sub
            End If
            If ddlSearchBy.SelectedValue = "2" And (txtFName.Text.Trim.Length = 0 Or txtDOB.Text.Trim.Length = 0 Or txtEmail.Text.Trim.Length = 0 Or txtMobile.Text.Trim.Length = 0) Then
                lblError.Text = "Please enter the all the fields."
                Exit Sub
            End If

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(9) As SqlParameter


            PARAM(0) = New SqlParameter("@USR_NAME", Session("sUsr_name"))
            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@SEARCHBY", ddlSearchBy.SelectedValue)
            PARAM(3) = New SqlParameter("@GUIS", txtGUSI.Text.Trim)

            PARAM(4) = New SqlParameter("@STU_FNAME", txtFName.Text.Trim)
            PARAM(5) = New SqlParameter("@STU_DOB", txtDOB.Text.Trim)
            PARAM(6) = New SqlParameter("@STU_MOBILE", txtMobile.Text.Trim)
            PARAM(7) = New SqlParameter("@STU_EMAIL", txtEmail.Text.Trim)
            PARAM(8) = New SqlParameter("@STU_NO", txtStuNo.Text.Trim)

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_STUDENT_GUSI", PARAM)
            gvStudent.DataSource = dsDetails
            gvStudent.DataBind()

            If dsDetails IsNot Nothing Then
                If dsDetails.Tables(0).Rows.Count > 0 Then
                    btnSave.Visible = True
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub txtStudIDs_TextChanged(sender As Object, e As EventArgs)
        'txtStudIDs.Text = ""
        If h_STU_IDs.Value <> "" Then
            grdStudent.Visible = True
            GridBindStudents(h_STU_IDs.Value)
        Else
            grdStudent.Visible = False
        End If

    End Sub
    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        grdStudent.DataSource = ReportFunctions.GetSelectedStudents(vSTU_IDs)
        grdStudent.DataBind()
    End Sub
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub
    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grdStudent.PageIndex = e.NewPageIndex
        GridBindStudents(h_STU_IDs.Value)
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If h_STU_IDs.Value.Trim = "" Then
            lblError.Text = "Please Select The student."
            Exit Sub
        End If
        If h_stu_id_old.Value.Trim = "" Then
            lblError.Text = "Please Select The student."
            Exit Sub
        End If

        Dim tran As SqlTransaction
        Dim callTransaction As Integer = 0
        Using CONN As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            CONN.Open()
            tran = CONN.BeginTransaction("SampleTransaction")
            Try
                Dim param(10) As SqlParameter
                param(0) = New SqlParameter("@USR_NAME", Session("sUsr_name"))
                param(1) = New SqlParameter("@STU_ID", h_STU_IDs.Value)
                param(2) = New SqlParameter("@STU_ID_OLD", h_stu_id_old.Value)
                param(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.SmallInt)
                param(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(tran, CommandType.StoredProcedure, "[dbo].[UPDATE_GUSI_NUMBER]", param)
                Dim ReturnFlag As Integer = param(3).Value
                If ReturnFlag = -1 Then
                    callTransaction = "-1"
                ElseIf ReturnFlag <> 0 Then
                    callTransaction = "1"
                Else
                    callTransaction = "0"
                End If
            Catch ex As Exception
                callTransaction = "1"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If callTransaction = "-1" Then
                    tran.Rollback()
                ElseIf callTransaction <> "0" Then
                    tran.Rollback()
                Else
                    tran.Commit()
                End If
                CONN.Close()
            End Try

            If callTransaction <> "0" Then
                lblError.Text = "Error!"
            Else
                lblError.Text = "Data has been save successfully"
            End If

        End Using
    End Sub
End Class

