Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studHeadofYear_View
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ViewState("datamode") = "add"
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                GridBind()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            '     ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studHeadofYear.aspx?MainMnu_code={0}&datamode={1}&mode=add", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudTutor_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudTutor.PageIndexChanging
        gvStudTutor.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvStudTutor_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudTutor.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStudTutor.Rows(index), GridViewRow)
                Dim url As String
                'define the datamode to view if view is clicked
                ViewState("datamode") = "view"
                Dim lblHodId As New Label
                With selectedRow
                    lblHodId = .FindControl("LblHODID")
                End With
                'Encrypt the data that needs to be send through Query String
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

                Dim editString As String = lblHodId.Text
                editString = Encr_decrData.Encrypt(editString)
                url = String.Format("~\Students\studHeadofYear.aspx?MainMnu_code={0}&datamode={1}&mode=edit&editstring=" + editString, ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Private Sub GridBind()
        Dim dv As New DataView
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim arrGrade As New ArrayList
        Dim arrSection As New ArrayList
        'Dim str_query As String = "SELECT sec_id,sct_descr,sec_grm_id,emp_fname+' '+emp_mname+' '+emp_lname as emp_name,sec_emp_id,sec_sct_id FROM " _
        '                        & "section_tutor_d,section_m,employee_m WHERE " _
        '                        & "section_tutor_d.sec_sct_id = section_m.sct_id " _
        '                        & "And section_tutor_d.sec_emp_id = employee_m.emp_id " _
        '                        & "And sec_grm_id = sct_grm_id And sec_acd_id = sct_acd_id " _
        '                        & "AND sec_acd_id=" + ViewState("SelectedYear").ToString + " AND sec_bsu_id='" + Session("sbsuid").ToString + "' and sec_todt is null"

        Dim str_query As String = "SELECT HODID,GRADEID,GRD_DESCR," _
                                 & "EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,''), STAFF_ID FROM HEADOFYEAR " _
                                 & "AS A INNER JOIN GRADE_M AS B ON A.GRADEID = B.GRD_ID INNER JOIN " _
                                 & "EMPLOYEE_M AS F ON A.STAFF_ID = F.EMP_ID " _
                                 & "WHERE accyear=" + ddlAcademicYear.SelectedValue + " AND BSU_ID=" + Session("sbsuid") + ""
        'B.GRM_ID
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStudTutor.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStudTutor.DataBind()
            Dim columnCount As Integer = gvStudTutor.Rows(0).Cells.Count
            gvStudTutor.Rows(0).Cells.Clear()
            gvStudTutor.Rows(0).Cells.Add(New TableCell)
            gvStudTutor.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudTutor.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudTutor.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudTutor.DataBind()
        End If
        Dim dt As DataTable = ds.Tables(0)
    End Sub

End Class
