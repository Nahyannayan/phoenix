﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports Lesnikowski.Barcode
Partial Class Students_stuLatePassReason
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S050039") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'GetActive_ACD_4_Grade


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    gridbind()
                End If
            Catch ex As Exception

                ' lblm.Text = "Request could not be processed "
            End Try

        End If

    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub gridbind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As New DataSet
        Dim str_query As String = "SELECT LR_ID,LR_DESCR  FROM STU.STU_LATE_REASONS "



        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            gvReason.DataSource = ds.Tables(0)
            gvReason.DataBind()

        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvReason.DataSource = ds.Tables(0)
            Try
                gvReason.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvReason.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvReason.Rows(0).Cells.Clear()
            gvReason.Rows(0).Cells.Add(New TableCell)
            gvReason.Rows(0).Cells(0).ColumnSpan = columnCount
            gvReason.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvReason.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        End If
    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divReason.Visible = False
    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divReason.Visible = False
    End Sub

    Protected Sub lnkadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkadd.Click
        divReason.Visible = True
        btnSav.Text = "Save"
        ViewState("LR_ID") = "0"
        txtReason.Text = ""
    End Sub

    Protected Sub gvReason_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReason.RowCommand
        If e.CommandName = "edit" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvReason.Rows(index), GridViewRow)

            Dim lblLRID As Label

            lblLRID = selectedRow.FindControl("lblLRID")
            ViewState("LR_ID") = lblLRID.Text
            divReason.Visible = True
            dispayitems(ViewState("LR_ID"))
            If btnSav.Text = "Save" Then btnSav.Text = "Update"

        End If
    End Sub
    Sub dispayitems(ByVal id As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""
            Dim subj As String = ""

            str_query = "SELECT LR_ID,LR_DESCR  FROM STU.STU_LATE_REASONS" _
                        & " WHERE LR_ID=" & id
            If str_query <> "" Then
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count >= 1 Then


                    txtReason.Text = ds.Tables(0).Rows(0).Item("LR_DESCR")
                    



                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblUerror.Text = "Request could not be processed"
        End Try
    End Sub

    Sub save_reasons(ByVal id As Integer)
        Dim Status As Integer
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                param(0) = New SqlParameter("@LR_ID", id)
                param(1) = New SqlParameter("@LR_DESCR", txtReason.Text)


                param(3) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(3).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.SAVE_LATEPASS_REASON", param)
                Status = param(3).Value





            Catch ex As Exception

                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If Status <> 0 Then
                    ' UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    transaction.Rollback()
                    lblUerror.Text = "Error "
                Else
                    If btnSav.Text = "save" Then
                        lblUerror.Text = "Saved Successfully."
                    Else
                        lblUerror.Text = "Updated Successfully."
                    End If
                    transaction.Commit()
                    gridbind()
                End If
            End Try

        End Using
    End Sub

    Protected Sub gvReason_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvReason.RowEditing

    End Sub

    Protected Sub btnSav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSav.Click
        save_reasons(ViewState("LR_ID"))
    End Sub


    

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        ' Create_print()
        Response.Redirect("~/Students/stuLateBarcode.aspx", False)
    End Sub
End Class
