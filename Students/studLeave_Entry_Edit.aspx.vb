Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class Students_studLeave_Entry_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        Call Student_Detail_Leaves()
                        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        getStart_EndDate()
                        GetTCISSUE_DATE()
                        bindAPD_ID()
                        Session("Leave_Approval") = CreateDataTable()
                        setModifyFooter()
                        GetEmpID()
                        btnUpdate.Visible = False
                        disable_control()
                    End If

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
    Sub bindAPD_ID()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim sqlstr As String
        sqlstr = "select APD_ID,APD_PARAM_DESCR from ATTENDANCE_PARAM_D where APD_ACD_ID = '" & ViewState("ACD_ID") & "' " & _
" and APD_bSHOW=1 AND APD_PARAM_DESCR NOT IN ('PRESENT','ABSENT')" & _
" AND APD_PARAM_DESCR not like '%late%'" & _
" ORDER BY ISNULL(APD_DISPLAYORDER,5) "

        Dim DS As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlstr)
        ddlAPD_ID.Items.Clear()
        ddlAPD_ID.DataSource = DS.Tables(0)
        ddlAPD_ID.DataTextField = "APD_PARAM_DESCR"
        ddlAPD_ID.DataValueField = "APD_ID"
        ddlAPD_ID.DataBind()
        ddlAPD_ID.Items.Add(New ListItem("-- SELECT --", "0"))
        ddlAPD_ID.ClearSelection()
        ddlAPD_ID.Items.FindByValue("0").Selected = True


    End Sub

    Sub Student_Detail_Leaves()

        Using readerSTUD_Detail_Leaves As SqlDataReader = AccessStudentClass.GetStudent_Detail_Leaves(ViewState("viewid"))
            While readerSTUD_Detail_Leaves.Read


                ViewState("STU_ID") = Convert.ToString(readerSTUD_Detail_Leaves("STU_ID"))
                ViewState("ACD_ID") = Convert.ToString(readerSTUD_Detail_Leaves("ACD_ID"))
                ViewState("GRD_ID") = Convert.ToString(readerSTUD_Detail_Leaves("GRD_ID"))
                ViewState("SCT_ID") = Convert.ToString(readerSTUD_Detail_Leaves("SCT_ID"))
                ViewState("STM_ID") = Convert.ToString(readerSTUD_Detail_Leaves("STM_ID"))
                ViewState("SHF_ID") = Convert.ToString(readerSTUD_Detail_Leaves("SHF_ID"))

                lblAcdYear.Text = Convert.ToString(readerSTUD_Detail_Leaves("AcdYear"))
                lblGrade.Text = Convert.ToString(readerSTUD_Detail_Leaves("GRD_DISP"))
                lblMOE.Text = Convert.ToString(readerSTUD_Detail_Leaves("BLUEID"))
                lblSName.Text = Convert.ToString(readerSTUD_Detail_Leaves("STUDNAME"))
                lblSection.Text = Convert.ToString(readerSTUD_Detail_Leaves("SCT_DESCR"))
                lblShift.Text = Convert.ToString(readerSTUD_Detail_Leaves("SHF_DESCR"))
                lblStream.Text = Convert.ToString(readerSTUD_Detail_Leaves("STM_DESCR"))
                lblStudID.Text = Convert.ToString(readerSTUD_Detail_Leaves("FEE_ID"))

                lblAcdYear.Enabled = False
                lblGrade.Enabled = False
                lblMOE.Enabled = False
                lblSName.Enabled = False
                lblSection.Enabled = False
                lblShift.Enabled = False
                lblStream.Enabled = False
                lblStudID.Enabled = False

            End While
        End Using
    End Sub
    Sub disable_control()
        txtTo.Enabled = False
        txtFrom.Enabled = False
        imgCalendar.Enabled = False
        ImageButton1.Enabled = False
        btnAddDetail.Enabled = False
        txtRemarks.Enabled = False
        gvHolidayDetail.Enabled = False
        ddlAPD_ID.Enabled = False
    End Sub
    Sub enable_control()
        txtTo.Enabled = True
        txtFrom.Enabled = True
        imgCalendar.Enabled = True
        ImageButton1.Enabled = True
        btnAddDetail.Enabled = True
        txtRemarks.Enabled = True
        gvHolidayDetail.Enabled = True
        ddlAPD_ID.Enabled = True
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.String"))
            Dim FROMDT As New DataColumn("FROMDT", System.Type.GetType("System.DateTime"))
            Dim TODT As New DataColumn("TODT", System.Type.GetType("System.DateTime"))
            Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim LEAVESTATUS As New DataColumn("LEAVESTATUS", System.Type.GetType("System.String"))
            Dim APD_ID As New DataColumn("APD_ID", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(FROMDT)
            dtDt.Columns.Add(TODT)
            dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(LEAVESTATUS)
            dtDt.Columns.Add(APD_ID)
            dtDt.Columns.Add(STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function
    Private Sub setModifyFooter()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT  SLA_ID, SLA_FROMDT, SLA_TODT, SLA_REMARKS, SLA_APPRLEAVE ,SLA_APD_ID,(SELECT APD_PARAM_DESCR FROM ATTENDANCE_PARAM_D WHERE APD_ID=SLA_APD_ID) AS PARAM_DESCR FROM STUDENT_LEAVE_APPROVAL where " & _
 " SLA_STU_ID='" & ViewState("STU_ID") & "' and  SLA_ACD_ID='" & ViewState("ACD_ID") & "' and  SLA_GRD_ID='" & ViewState("GRD_ID") & "' and " & _
 " SLA_SCT_ID='" & ViewState("SCT_ID") & "' and  SLA_STM_ID='" & ViewState("STM_ID") & "' and  SLA_SHF_ID='" & ViewState("SHF_ID") & "'"


            Session("Leave_Approval").Rows.Clear()
            ViewState("SRNO") = 1
            Using Userreader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                While Userreader.Read
                    Dim rDt As DataRow
                    rDt = Session("Leave_Approval").NewRow
                    rDt("SRNO") = ViewState("SRNO")
                    rDt("ID") = Convert.ToString(Userreader("SLA_ID"))
                    rDt("FROMDT") = Convert.ToString(Userreader("SLA_FROMDT"))
                    rDt("TODT") = Convert.ToString(Userreader("SLA_TODT"))
                    rDt("REMARKS") = Convert.ToString(Userreader("SLA_REMARKS"))
                    rDt("LEAVESTATUS") = Convert.ToString(Userreader("PARAM_DESCR"))
                    rDt("APD_ID") = Convert.ToString(Userreader("SLA_APD_ID"))
                    rDt("STATUS") = Convert.ToString(Userreader("SLA_APPRLEAVE"))
                    Session("Leave_Approval").Rows.Add(rDt)
                    ViewState("SRNO") = ViewState("SRNO") + 1
                End While
                gridbind()
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub gridbind()
        Try
            gvHolidayDetail.DataSource = Session("Leave_Approval")
            gvHolidayDetail.DataBind()
            Call DisableControls()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub DisableControls()

        For j As Integer = 0 To gvHolidayDetail.Rows.Count - 1

            Dim row As GridViewRow = gvHolidayDetail.Rows(j)

            Dim lblStatus As New Label

            lblStatus = DirectCast(row.FindControl("lblStatus"), Label)
            Dim Stud_ID_status As String = lblStatus.Text
            If UCase(Stud_ID_status) = "PENDING" Then
                DirectCast(row.FindControl("lblStatus"), Label).Visible = False
            Else
                If UCase(Stud_ID_status) = "APPROVED" Then
                    lblStatus.ForeColor = Drawing.Color.Green
                ElseIf UCase(Stud_ID_status) = "NOT APPROVED" Then
                    lblStatus.ForeColor = Drawing.Color.Red
                End If
                DirectCast(row.FindControl("lblEdit"), LinkButton).Visible = False
            End If

        Next
    End Sub

    Sub gridbind_add()
        Try
            Dim i As Integer
            Dim Acd_SDate As Date = ViewState("ACD_STARTDT")
            Dim Acd_EDate As Date = ViewState("ACD_ENDDT")
            Dim From_date As Date = txtFrom.Text
            Dim To_Date As Date = txtTo.Text

            If (From_date >= Acd_SDate And From_date <= Acd_EDate) And (To_Date >= Acd_SDate And To_Date <= Acd_EDate) Then

                For i = 0 To Session("Leave_Approval").Rows.Count - 1

                    If Session("Leave_Approval").Rows(i)("Status") = "PENDING" Then

                        lblError.Text = "No new leave approval can be added,since pending approval exist for the current student!!!"
                        Exit Sub
                    End If
                    Dim SFromDate As Date = Session("Leave_Approval").Rows(i)("FROMDT")
                    Dim SToDate As Date = Session("Leave_Approval").Rows(i)("TODT")

                    If From_date >= SFromDate And From_date <= SToDate Then
                        lblError.Text = "From Date is already set for leave approval!!!"
                        Exit Sub
                    ElseIf To_Date >= SFromDate And To_Date <= SToDate Then
                        lblError.Text = "To Date is already set for leave approval!!!"
                        Exit Sub
                    End If

                    If ViewState("TC_ISSUEDT") <> "" Then
                        If ViewState("TC_ISSUEDT") >= From_date And ViewState("TC_ISSUEDT") <= To_Date Then
                            lblError.Text = "Leave entered for this student must be within the Last Attendance date(Last Attendance Date :)" & ViewState("TC_ISSUEDT")
                            Exit Sub
                        End If
                    End If
                Next

                Dim rDt As DataRow


                rDt = Session("Leave_Approval").NewRow
                rDt("SRNO") = ViewState("SRNO")
                rDt("ID") = 0
                rDt("FROMDT") = txtFrom.Text
                rDt("TODT") = txtTo.Text
                rDt("STATUS") = "PENDING"
                rDt("REMARKS") = txtRemarks.Text
                If ddlAPD_ID.SelectedValue = "0" Then
                    rDt("LEAVESTATUS") = ""
                Else
                    rDt("LEAVESTATUS") = ddlAPD_ID.SelectedItem.Text
                End If

                rDt("APD_ID") = ddlAPD_ID.SelectedValue
                Session("Leave_Approval").Rows.Add(rDt)
                gridbind()
                'gvHolidayDetail.DataSource = Session("Leave_Approval")
                'gvHolidayDetail.DataBind()

            Else
                lblError.Text = "From Date to To Date must be with in the academic year"
            End If

        Catch ex As Exception
            lblError.Text = "Check Student Leave date"
        End Try
    End Sub

    Protected Sub btnAddDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind_add()
    End Sub

    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If

        If txtTo.Text.Trim <> "" Then

            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>To Date format is Invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid To date</div>"
                    End If
                End If
            End If
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function
    Sub GetEmpID()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub getStart_EndDate()
        Dim ACD_ID As String = ViewState("ACD_ID")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query = "select ACD_StartDt,ACD_ENDDT  from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        ViewState("ACD_STARTDT") = ds.Tables(0).Rows(0).Item("ACD_StartDt")
        ViewState("ACD_ENDDT") = ds.Tables(0).Rows(0).Item("ACD_ENDDT")
    End Sub

    Sub GetTCISSUE_DATE()

        Dim sqlString As String = " SELECT    replace(convert(varchar(11), T.TCM_LASTATTDATE, 106), ' ', '/') FROM    STUDENT_M  AS S left outer join TCM_M AS T ON S.STU_ID=T.TCM_STU_ID AND(T.TCM_CANCELDATE IS NULL) where " & _
 " S.STU_ID='" & ViewState("STU_ID") & "' and  S.STU_ACD_ID='" & ViewState("ACD_ID") & "' and  S.STU_GRD_ID='" & ViewState("GRD_ID") & "' and " & _
 " S.STU_SCT_ID='" & ViewState("SCT_ID") & "' and  S.STU_STM_ID='" & ViewState("STM_ID") & "' and S.STU_SHF_ID='" & ViewState("SHF_ID") & "'"


        Dim sdate As Object
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        sdate = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sqlString)
        If IsDBNull(sdate) = False Then
            If IsDate(CStr(sdate)) = True Then
                ViewState("TC_ISSUEDT") = CStr(sdate)
            Else
                ViewState("TC_ISSUEDT") = ""
            End If
        Else
            ViewState("TC_ISSUEDT") = ""
        End If


    End Sub

    Protected Sub gvHolidayDetail_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        btnSave.Enabled = False
        gvHolidayDetail.SelectedIndex = e.NewEditIndex


        Dim row As GridViewRow = gvHolidayDetail.Rows(e.NewEditIndex)
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblID"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)

        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("Leave_Approval").Rows.Count - 1
            If iIndex = Session("Leave_Approval").Rows(iEdit)("ID") Then

                txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Session("Leave_Approval").Rows(iEdit)("FROMDT"))
                txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Session("Leave_Approval").Rows(iEdit)("TODT"))
                txtRemarks.Text = Session("Leave_Approval").Rows(iEdit)("REMARKS")
                txtFrom.Enabled = True
                txtTo.Enabled = True
                txtRemarks.Enabled = True
                If Not ddlAPD_ID.Items.FindByValue(Session("Leave_Approval").Rows(iEdit)("APD_ID")) Is Nothing Then
                    ddlAPD_ID.ClearSelection()
                    ddlAPD_ID.Items.FindByValue(Session("Leave_Approval").Rows(iEdit)("APD_ID")).Selected = True
                End If

                Exit For
            End If
        Next

        btnAddDetail.Visible = False
        btnUpdate.Visible = True
        gridbind()
    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If serverDateValidate() = "0" Then

            Dim Acd_SDate As Date = ViewState("ACD_STARTDT")
            Dim Acd_EDate As Date = ViewState("ACD_ENDDT")
            Dim From_date As Date = txtFrom.Text
            Dim To_Date As Date = txtTo.Text

            If (From_date >= Acd_SDate And From_date <= Acd_EDate) And (To_Date >= Acd_SDate And To_Date <= Acd_EDate) Then
                btnSave.Enabled = True
                Dim row As GridViewRow = gvHolidayDetail.Rows(gvHolidayDetail.SelectedIndex)
                Dim idRow As New Label
                idRow = TryCast(row.FindControl("lblID"), Label)
                Dim iEdit As Integer = 0
                Dim iIndex As Integer = 0
                iIndex = CInt(idRow.Text)
                'loop through the data table row  for the selected rowindex item in the grid view

                For iEdit = 0 To Session("Leave_Approval").Rows.Count - 1

                    Dim SFromDate As Date = Session("Leave_Approval").Rows(iEdit)("FROMDT")
                    Dim SToDate As Date = Session("Leave_Approval").Rows(iEdit)("TODT")
                    If iIndex <> Session("Leave_Approval").Rows(iEdit)("ID") Then
                        If From_date >= SFromDate And From_date <= SToDate Then
                            lblError.Text = "Date is already set for leave approval!!!"
                            Exit Sub
                        ElseIf To_Date >= SFromDate And To_Date <= SToDate Then
                            lblError.Text = "Date is already set for leave approval!!!"
                            Exit Sub
                        End If

                    End If
                    If ViewState("TC_ISSUEDT") <> "" Then
                        If ViewState("TC_ISSUEDT") >= From_date And ViewState("TC_ISSUEDT") <= To_Date Then
                            lblError.Text = "Leave entered for this student must be within the Last Attendance Date(Last Attendance Date :)" & ViewState("TC_ISSUEDT")
                            Exit Sub
                        End If
                    End If
                Next
                For iEdit = 0 To Session("Leave_Approval").Rows.Count - 1
                    If iIndex = Session("Leave_Approval").Rows(iEdit)("ID") Then

                        Session("Leave_Approval").Rows(iEdit)("FROMDT") = txtFrom.Text
                        Session("Leave_Approval").Rows(iEdit)("TODT") = txtTo.Text
                        Session("Leave_Approval").Rows(iEdit)("REMARKS") = txtRemarks.Text

                        If ddlAPD_ID.SelectedValue = "0" Then
                            Session("Leave_Approval").Rows(iEdit)("LEAVESTATUS") = ""
                        Else
                            Session("Leave_Approval").Rows(iEdit)("LEAVESTATUS") = ddlAPD_ID.SelectedItem.Text
                        End If

                        Session("Leave_Approval").Rows(iEdit)("APD_ID") = ddlAPD_ID.SelectedValue

                        txtFrom.Enabled = False
                        txtTo.Enabled = False
                        txtRemarks.Enabled = False
                        ddlAPD_ID.Enabled = False
                        Exit For
                    End If
                Next
                btnUpdate.Visible = False
                btnAddDetail.Visible = True

                gvHolidayDetail.SelectedIndex = -1
                gridbind()
            Else
                lblError.Text = "From Date to To Date must be with in the academic year"
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = CallTransaction(errorMessage)
        If str_err = "0" Then
            disable_control()
            lblError.Text = "Record updated successfully"
        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Private Function CallTransaction(ByRef errorMessage As String) As String
        Dim Status As Integer
        Dim RecordStatus As String = String.Empty
        Dim SLA_APD_ID As String = String.Empty
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                If Session("Leave_Approval").Rows.Count <> 0 Then

                    For i As Integer = 0 To Session("Leave_Approval").Rows.Count - 1
                        If Session("Leave_Approval").Rows(i)("Status").ToString = "PENDING" Then



                            Dim FROMDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", Session("Leave_Approval").Rows(i)("FROMDT"))
                            Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", Session("Leave_Approval").Rows(i)("TODT"))


                            Status = AccessStudentClass.SaveSTUDLEAVE_ENTRY(Session("Leave_Approval").Rows(i)("ID"), _
                                  ViewState("EMP_ID"), ViewState("viewid"), ViewState("ACD_ID"), ViewState("GRD_ID"), ViewState("SCT_ID"), _
                                    ViewState("STM_ID"), ViewState("SHF_ID"), FROMDT, _
                                  TODT, Session("Leave_Approval").Rows(i)("REMARKS"), Session("Leave_Approval").Rows(i)("APD_ID"), _
                                  Session("Leave_Approval").Rows(i)("Status"), transaction)
                            If Status <> 0 Then
                                CallTransaction = "1"
                                errorMessage = "Error while updating records"
                                Return "1"
                            Else
                                Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), Session("Leave_Approval").Rows(i)("ID"), "edit", Page.User.Identity.Name.ToString, Me.Page)
                                If Status <> 0 Then
                                    CallTransaction = "1"
                                    errorMessage = "Could not process audit request"
                                    Return "1"
                                End If
                            End If
                        End If
                    Next
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"

                Else
                    CallTransaction = "1"
                    errorMessage = "Add leave approval detail for the current student!!!"
                End If



            Catch ex As Exception

                errorMessage = "Record could not be Updated"
            Finally
                If CallTransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        enable_control()
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            disable_control()
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
End Class
