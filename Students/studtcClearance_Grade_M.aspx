<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studtcClearance_Grade_M.aspx.vb" Inherits="Students_studtcClearance_Grade_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }
</script>
       <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>TC Clearance - Grade
        </div>
        <div class="card-body">
            <div class="table-responsive ">
<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 100%">
        <tr>
            <td  >
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                ValidationGroup="groupM1" style="text-align: left" Width="342px" />
                &nbsp;&nbsp;

            </td>
        </tr>
        <tr>
             <td align="center"  valign="bottom" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                SkinID="error"   style="text-align: center" ></asp:Label>
             </td>
        </tr>
        <tr>
            <td   valign="top">
                <table align="center"  cellpadding="5" cellspacing="0"
                style="width: 100%">
                   
                    <tr>
                        <td align="left"  width="20%"  >
                            <span class="field-label">Select Academic Year</span></td>
                        
                        <td align="left"   width="30%"  >
                            <asp:DropDownList id="ddlAcademicYear" runat="server"  >
                            </asp:DropDownList></td>
                    
                        <td align="left"   width="20%" >
                            <span class="field-label">Select Grade</span></td>
                        
                        <td align="left"    width="30%">
                            <asp:DropDownList id="ddlGrade" runat="server"  >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left"  >
                            <span class="field-label">Select Stream</span></td>
                       
                        <td align="left"   >
                            <asp:DropDownList id="ddlStream" runat="server"  >
                            </asp:DropDownList></td>
                   
                        <td align="left"  >
                            <span class="field-label">Clearance Type</span></td>
                       
                        <td align="left"   >
                            <asp:DropDownList id="ddlClearance" runat="server"  ></asp:DropDownList></td>          
                    </tr>
            
                                       
                     </table>
               </td>
        </tr>
       
        <tr>
            <td align="center" valign="bottom">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />&nbsp;
                <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" ValidationGroup="groupM1" TabIndex="7" /></td>
        </tr>
        <tr>
            <td  valign="bottom"  >
                <asp:HiddenField ID="hfTCG_ID" runat="server" />&nbsp;
          </td>
          
        </tr>
    </table>
                </div>
            </div>
        </div>
</asp:Content>

