﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="stuAddNotifications.aspx.vb" Inherits="Students_stuAddNotifications" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="../Scripts/jquery.ui.timepicker.css" rel="stylesheet" />
     <script type="text/javascript" src="../Scripts/jquery.ui.timepicker.js"></script>
     <link type="text/css" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
      <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
     <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script> 
    <style>
        .ui-timepicker-table td a{
          width: fit-content!important;
        }

        .ui-widget-header {
            border: 1px solid buttonface!important;
            background: #8DC24C 50% 50% repeat-x!important;
            color: White!important;
            border-style: Solid!important;
        }
        .ui-timepicker {
        background-color:white!important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            <asp:Label ID="lblheading" runat="server" Text="Add New Notification" /> </div>
            <div class="card-body">
                <div class="table-responsive m-auto">
                    <asp:Label ID="lblError" runat="server" />
                    <asp:HiddenField ID="viewidUpdate" runat="server" />
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td width="20%">
                                    <asp:Label ID="Label2" runat="server" Text="Academic Year" CssClass="field-label" />
                                </td>
                                <td width="30%">
                                    <asp:DropDownList ID="ddlACDYear" runat="server" OnSelectedIndexChanged="ddlACDYear_SelectedIndexChanged" AutoPostBack="true" TabIndex="1" />

                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="Adhoc" CssClass="field-label" />
                                </td>
                                <td>
                                     <asp:CheckBox ID="chkAdhoc" runat="server" AutoPostBack="true" OnCheckedChanged="chkAdhoc_CheckedChanged" />
                                </td>
                            </tr>
                            <tr>

                                <td width="20%">
                                    <asp:Label ID="lblDateNot" runat="server" Text="Notification Display Date" CssClass="field-label" />
                                </td>
                                <td width="30%">
                                    <asp:UpdatePanel runat="server" ID="updatepanel1">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtNotificationDt" runat="server" placeholder="DD-MMM-YYYY" TabIndex="3" ClientIDMode="Static" onfocus="click_me();false;" onClick="click_me();false;" required="required" CssClass="myTextBoxStyleS_CAL"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td colspan="2">
                                    <table width="100%" runat="server" id="tblTreeView" visible="false">
                                        <tr>
                                            <td width="40%">
                                                <asp:Label ID="lblSlctGrade" runat="server" Text="Grade And Sections" CssClass="field-label" /></td>
                                            <td width="60%">
                                                <asp:TreeView ID="trGrades" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked()" ForeColor="Black"
                                                    runat="server" TabIndex="2">
                                                </asp:TreeView>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" runat="server" id="tblStudentView" visible="false">
                                        <tr>
                                            <td width="40%">
                                   <asp:Label ID="Label3" runat="server" Text="Student's" CssClass="field-label" />   
                                </td>
                               <td width="60%">
                                    <asp:TextBox ID="txtSTU_IDs" runat="server" OnTextChanged="txtSTU_IDs_TextChanged" formnovalidate></asp:TextBox>
                                            <asp:ImageButton ID="imgParticipants2" runat="server" ImageUrl="~/Images/cal.gif"
                                                 OnClientClick="openStudents();return false;" formnovalidate></asp:ImageButton>
                                            <asp:GridView ID="gvStudents" runat="server" AutoGenerateColumns="False"
                                                Width="100%" PageSize="5"
                                                EnableModelValidation="True" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("STU_ID") %>' Font-Bold="False"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Student Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTrainerName" runat="server" Text='<%# bind("FullName") %>'></asp:Label>
                                                        </ItemTemplate>                                                        
                                                        <ItemStyle Width="80%" />
                                                    </asp:TemplateField>
                                                    
                                                    <asp:CommandField DeleteText="Remove" ShowDeleteButton="True">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:CommandField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader_new" />
                                            </asp:GridView> 
                                            <asp:HiddenField ID="h_Stu_Ids" runat="server" />

                                </td> 
                                        </tr>
                                    </table>
                                </td>
                               
                            </tr>
                            
                            <tr>
                                <td width="20%">  
                                    <asp:Label ID="Label5" runat="server" Text="Notification Title" CssClass="field-label" />
                                </td>
                                <td width="30%">
                                    <textarea id="txtNotificationTitle" runat="server" required="required" TabIndex="5"  class="textareaS_CAL"  rows="1" placeholder="Enter notification title.." maxlength="60" />
                                </td>
                                <td width="20%">
                                    <asp:Label ID="lbltxtCategory" runat="server" Text="Notification category" CssClass="field-label" />
                                </td>
                                <td width="30%">
                                    <asp:DropDownList ID="ddlCategory" runat="server" required="required" TabIndex="6" >
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     <asp:Label ID="lblText" runat="server" Text="Notification Description" CssClass="field-label" /> 
                                </td>
                                <td>
                                     <textarea id="txtNotificationText" runat="server" required="required" class="textareaS_CAL" TabIndex="7"  placeholder="Enter notification text..." maxlength="500" />
                                </td>
                               
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnSave" CssClass="button" runat="server" OnClientClick="if ( !confirm('All Your Data Will Be Saved And You Won't Be Able To Change Further!! Are you sure you want to proceed ? ')) return false;" OnClick="btnSave_Click" Text="Submit" />
                                    <asp:Button ID="btnCancel" CssClass="button" runat="server" OnClick="btnCancel_Click" Text="Cancel" formnovalidate/>
                                    <asp:Button ID="btnAdd" CssClass="button" runat="server" OnClientClick="if ( !confirm('Are you sure you want to proceed ? ')) return false;" OnClick="btnAdd_Click" Text="Add New" />

                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="4">
                                     
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
      
    </div>
    <script type="text/javascript" language="javascript">

        function CallFromStudentSelection(obj) {
            //alert(obj);
            NameandCode = obj.split('||');
            var trainers = document.getElementById('<%=h_Stu_Ids.ClientID%>').value + ',' + NameandCode[0]
            document.getElementById('<%=txtSTU_IDs.ClientID%>').value = NameandCode[0];
            document.getElementById('<%=h_Stu_Ids.ClientID%>').value = trainers;

            //alert(trainers);
            __doPostBack('<%= txtSTU_IDs.ClientID%>', 'TextChanged');
        }

        function openStudents() {

            var cUrl = ""


            cUrl = "stuNotification_add_students.aspx";

            $.fancybox({
                'type': 'iframe',
                'width': '50%',
                'height': '80%',
                'hideOnOverlayClick': false,
                'hideOnContentClick': false,
                'overlayOpacity': 0.7,
                'enableEscapeButton': false,
                'href': cUrl, 
            });


        }
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $('#<%= txtNotificationDt.ClientID%>').datepicker({
                        changeMonth: true,
                        changeYear: true,
                        yearRange: '1950:2050',
                        dateFormat: "dd/M/yy"
                    });

                   <%-- $('#<%= txtNotificationTime.ClientID%>').timepicker({

                    });--%>
                }
            });

            function pageLoad() {
                $('#<%= txtNotificationDt.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });
           <%-- $('#<%= txtNotificationTime.ClientID%>').timepicker({

            });--%>
        }

        function click_me() {
            $('#<%= txtNotificationDt.ClientID%>').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2050',
                dateFormat: "dd/M/yy"
            });
           <%-- $('#<%= txtNotificationTime.ClientID%>').timepicker({

            });--%>
        }

        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>
</asp:Content>
