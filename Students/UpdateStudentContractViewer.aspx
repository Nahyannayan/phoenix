﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="UpdateStudentContractViewer.aspx.vb" Inherits="Students_UpdateStudentContractViewer" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script type="text/javascript" language="javascript">

        function ChangeAllCheckBoxStates(checkState) {
            var chk_state = document.getElementById("ChkSelAll").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }


        function ViewDetail(url, text, w, h) {
            var sFeatures;
            sFeatures = "dialogWidth: 625px; ";
            sFeatures += "dialogHeight: 420px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            result = radopen(url, "pop_up")
            //if (result == '' || result == undefined) {
            //    return false;
            //}
            //NameandCode = result.split('___');
            //return false;
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-5"></i>

            <asp:Label ID="lblHead" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">
                    <tr valign="top">
                        <td valign="top" align="left">
                            <asp:HyperLink ID="hlAddNew" runat="server" Visible="False">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table align="center" width="100%">

                    <tr valign="top">

                        <td align="left" valign="middle" width="8%"><span class="field-label">Contract Updated ?</span></td>
                        <td align="left" valign="middle" width="15%">
                            <asp:RadioButtonList ID="rblFilter" RepeatDirection="Horizontal" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblFilter_SelectedIndexChanged">
                                
                                <asp:ListItem  Value="YES">Yes</asp:ListItem>
                                <asp:ListItem Selected="True" Value="NO">No</asp:ListItem>
                                
                            </asp:RadioButtonList>
                        </td>

                        <td align="left" valign="middle" width="8%"><span class="field-label">Grade </span></td>
                        <td align="left" valign="middle" width="15%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" SkinID="DropDownListNormal"
                                >
                                </asp:DropDownList>
                        </td>

                        
                      <%--<td align="left" valign="middle" width="8%" ><span class="field-label" >Section </span></td>
                        <td align="left" valign="middle" width="15%">
                            <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" SkinID="DropDownListNormal"
                                >
                                </asp:DropDownList>
                        </td>--%>
                        <td align="left" valign="middle" width="10%"><span class="field-label">Contract Signed for </span></td>
                        <td align="left" valign="middle" width="19%">
                            <asp:DropDownList ID="ddlAcaYear" runat="server" AutoPostBack="True" SkinID="DropDownListNormal"
                                OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                
                            </asp:DropDownList>
                            
                        </td>
                        

                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="8">
                            <asp:GridView ID="gvJournal" runat="server" AutoGenerateColumns="False" EmptyDataText="No Data Found" CssClass="table table-bordered table-row"
                                Width="100%" AllowPaging="True" PageSize="25" SkinID="GridViewView">
                                <Columns>
                                    <asp:TemplateField HeaderText="" >
                                        <HeaderTemplate>
                                            <input id="ChkSelAll" name="ChkSelAll" onclick="ChangeAllCheckBoxStates(true);" type="checkbox" value="Check All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                            <asp:HiddenField ID="hf_STP" runat="server" Value='<%# Bind("ID")%>' />
                                            <asp:HiddenField ID="hf_STU" runat="server" Value='<%# Bind("STU_ID")%>' />
                                            <asp:HiddenField ID="hf_ACD_ID" runat="server" Value='<%# Bind("ACD_ID")%>' />
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Contract Updating Date" >
                                        <HeaderTemplate>
                                          Contract Updating Date  
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                          <asp:TextBox ID="txtCUpdateDate" runat="server" Width="90px" Enabled="false" Style="text-align: left"
                                                                                Text='<%#Eval("stp_Contract_updated_date", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                                            <asp:ImageButton ID="imgFChqDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" Format="dd-MMM-yyyy" runat="server"
                                                                                TargetControlID="txtCUpdateDate" PopupButtonID="imgFChqDate">
                                                                            </ajaxToolkit:CalendarExtender>  
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Student Number">
                                        <HeaderTemplate>
                                            Student Id
                                            <br />
                                            <asp:TextBox ID="txtStuNo" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnEmpSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpno" runat="server" Text='<%# Bind("STUNO")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Student Name">
                                        <HeaderTemplate>
                                            Student Name<br />
                                            <asp:TextBox ID="txtStuName" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnEmpNameSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Date of Join">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOJG" runat="server" Text='<%# Bind("DOJ", "{0:dd/MMM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Academic Year">
                                        <HeaderTemplate>
                                            Academic Year
                                            <br />
                                            <asp:TextBox ID="txtEmpBsu" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnEmpbsuSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif"
                                                ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblempbsu" runat="server" Text='<%# Bind("ACY_DESCR")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade
                                            <br />
                                            <asp:TextBox ID="txtGrade" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDeptSearch" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                OnClick="ImageButton1_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("GRADE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <HeaderTemplate>
                                            Section
                                            <br />
                                            <asp:TextBox ID="txtSection" runat="server" Width="75%"></asp:TextBox>
                                            <asp:ImageButton ID="btnDesigSearch" OnClick="ImageButton1_Click" runat="server"
                                                ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesig" runat="server" Text='<%# Bind("Section")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="" Visible="false"  >
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbDetail" OnClick="lbDetail_Click" runat="server">Detail</asp:LinkButton>
                                            
                                            
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Message" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblErrorMessage" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="left" valign="top" colspan="3">
                            <asp:Label ID="lblAlert" runat="server" CssClass="text-danger"></asp:Label>
                        </td>
                    </tr>
                    <tr id="UpdatePanel" runat ="server">
                        <td align="center" valign="top" colspan="8">
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Update Contract" />
                        </td>
                    </tr>
                </table>



                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_selected_menu_8" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />

            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>
</asp:Content>
