﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="encEmailText.aspx.vb" Inherits="Students_encEmailText" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Email Text Setting
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table width="100%">

                    <tr align="left">
                        <td width="15%"><span class="field-label">Business Unit</span>
                        </td>

                        <td width="35%">
                            <asp:DropDownList ID="ddlBSU" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td width="15%"><span class="field-label">Academic Year</span>
                        </td>
                        <td width="35%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>


                    <tr align="left" id="tr_Status" runat="server">
                        <td width="15%"><span class="field-label">Grade</span>
                        </td>

                        <td width="35%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true"></asp:DropDownList>

                        </td>
                        <td width="15%"><span class="field-label">Stream</span>
                        </td>

                        <td width="35%">
                            <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr id="Tr1" align="left" runat="server">
                        <td><span class="field-label">Subject</span>
                        </td>

                        <td>
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">
                            <asp:LinkButton ID="lnkCopy" runat="server" Text="Copy To"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="Tr2" align="left" runat="server">
                        <td><span class="field-label">Text</span>
                        </td>

                        <td colspan="2">
                            <telerik:RadEditor ID="txtText" runat="server" EditModes="All" Height="600px">
                                <Tools>
                                    <telerik:EditorToolGroup>
                                        <telerik:EditorTool Name="Bold" />
                                        <telerik:EditorTool Name="Italic" />
                                        <telerik:EditorTool Name="InsertOrderedList" />
                                        <telerik:EditorTool Name="InsertUnorderedList" />
                                    </telerik:EditorToolGroup>
                                </Tools>
                            </telerik:RadEditor>
                        </td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">&nbsp;
                <asp:Label ID="lblerror" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                </table>

                <div id="divEmail" runat="server" class="panel-cover" visible="false">
                    <div>
                        <asp:Button ID="btClose" type="button" runat="server"
                            Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                            ForeColor="White" Text="X"></asp:Button>
                        <div class="holderInner">
                            <div>
                                <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <br>
                            <br>
                            <table align="center" cellpadding="0" cellspacing="0">

                                <tr>
                                    <td align="left" width="15%"><span class="field-label">Business Unit</span>
                                    </td>

                                    <td align="left" width="35%">
                                        <asp:DropDownList ID="ddlBSuCopy" runat="server" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                    <td align="left" width="15%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="35%">
                                        <asp:DropDownList ID="ddlAcyear" runat="server" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGradeEmail" runat="server" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Stream</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlStreamEmail" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Subject</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubjectEmail" runat="server"></asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnUpdate" Text="Copy" CssClass="button" runat="server" />
                                        <asp:Button ID="btnUClose" Text="Close" CssClass="button" runat="server" />
                                    </td>
                                </tr>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

