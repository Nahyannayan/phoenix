﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false"   MasterPageFile="~/mainMasterPage.master" MaintainScrollPositionOnPostback="true" CodeFile="studlistview_new.aspx.vb" Inherits="Students_studlistview_new" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
<script type="text/javascript">

        function change(a)
            {

            var val = document.getElementById(a.id).value
            var ValidChars = "0123456789";
            var IsNumber=true;
            var Char;
               for (i = 0; i < val.length && IsNumber == true; i++) 
                  { 
                  Char = val.charAt(i); 
                  if (ValidChars.indexOf(Char) == -1) 
                     {
                     IsNumber = false;
                     document.getElementById(a.id).value="";
                     document.getElementById(a.id).focus();
                     }
                  }
                  
               return IsNumber;
            }
    

    function alerting(a)
    {
    a.blur()
    alert('Select a Date from Calendar')
    }
    


        </script>
    <div>
        <asp:Panel id="Panel1" runat="server">
                    &nbsp;<table style="width: 100%">
                <tr>
                    <td align="left" class="title">
                        &nbsp; &nbsp; &nbsp;  Edit Student Details (Arabic)</td>
                </tr>
                <tr>
                    <td align="center" class="matters">
                    </td>
                </tr>
                <tr>
                    <td align="center" class="matters">
                        (Fields marked <span style="color: #ff0000">*</span> are mandatory)</td>
                </tr>
                <tr>
                    <td align="center" class="matters" >
                        <asp:Label ID="lblpassportname" runat="server"></asp:Label>&nbsp;
                        <asp:Label ID="lblgrade" runat="server"></asp:Label>&nbsp;
                        Admn.No . &nbsp;<asp:Label ID="lbladmissionnumber" runat="server"></asp:Label>&nbsp;
                        Accademic Year :<asp:Label ID="lblaccyear" runat="server"></asp:Label></td>
                </tr>
            </table>
            <table align="center" border="0" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                width="55%">
            </table>
            <table align="center" border="1" bordercolor="#1b80b6" cellpadding="2" cellspacing="0"
                width="90%" id="TABLE1" onclick="return TABLE1_onclick()">
                <tr>
                    <td colspan="4" style="height: 21px">
                    </td>
                </tr>
                <tr>
                    <td align="left" class="matters" style="width: 70px; height: 26px">
                        Stage</td>
                    <td align="center" class="repcoldetail" style="table-layout: fixed; font-size: 12pt;
                        width: 169px; border-collapse: separate; height: 26px">
                                    <asp:Label id="Lbstage" runat="server" ForeColor="#404040" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td align="right" class="repcoldetail" style="font-size: 12pt; width: 207px; height: 26px">
                        <asp:DropDownList ID="DropStage" runat="server">
                                        </asp:DropDownList></td>
                    <td align="right" class="matters" style="font-size: 12pt; width: 153px; height: 26px">
                        <span style="color: #000080"><span style="color: #000080">
                            <br />
                            :</span><span style="color: #000080">المرحلة</span>&nbsp;</span></td>
                </tr>
                <tr style="font-size: 12pt">
                    <td align="left" class="matters" style="width: 70px; height: 44px;">
                        &nbsp;Grade</td>
                    <td align="center" class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">                    <asp:Label id="LBgrade" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label>&nbsp;</td>
                    <td align="right" class="repcoldetail" style="height: 44px; width: 207px;">
                        <font color="navy" size="3">
                            <div dir="rtl">
                                <p align="right">
                                    &nbsp;<asp:DropDownList ID="DropGrade" runat="server">
                                    </asp:DropDownList>
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
                                    &nbsp;&nbsp;</p>
                            </div>
                        </font>
                    </td>
                    <td class="matters" style="width: 153px;" align="right">
                        <p align="center" style="text-align: right">
                            <font color="navy" size="3">: الصف &nbsp;&nbsp;&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td align="middle" class="matters" colspan="4">
                        <p align="center">
                            <font color="navy" size="3">(بيانات التلميذ) </font>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Passport Name<span style="color: #ff0000">*</span></td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        &nbsp;<asp:Label id="LBpassportname" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <font color="navy" size="4">
                            <div dir="rtl">
                                <p align="right">
                                    &nbsp;<asp:TextBox ID="TextPassportName" dir="rtl" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextPassportName"
                                        Display="Dynamic" ErrorMessage="(Required)" SetFocusOnError="True"></asp:RequiredFieldValidator></p>
                            </div>
                        </font>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3"><span style="color: red">*</span>: (الاسم (طبقا لجواز السفر</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Nationality</td>
                    <td align="middle" class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        &nbsp;<asp:Label id="LBnationality" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td align="middle" class="repcoldetail" style="width: 207px">
                        <font color="navy" size="4">
                            <div dir="rtl">
                                <p align="right">
                                    &nbsp;<asp:DropDownList ID="DropNationality" runat="server">
                                    </asp:DropDownList></p>
                            </div>
                        </font>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">: الجنسية&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Relegion</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        &nbsp;<asp:Label id="LBreligion" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:DropDownList ID="DropRelegion" runat="server">
                                </asp:DropDownList></p>
                        </div>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">: الديانة &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Place Of Birth</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBplaceofbirth" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextPlaceOfBirth" dir="rtl" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">: مكان الميلاد&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;DOB</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="lbdob" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <p align="right">
                            <asp:ImageButton ID="ImageButton4"
                                runat="server" ImageUrl="~/Images/calendar.gif"  />
                            <asp:TextBox ID="TextDOB"  runat="server"></asp:TextBox>&nbsp;
                            <ajaxToolkit:CalendarExtender ID="CE1" TargetControlID="TextDOB" PopupButtonID="ImageButton4" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
                        </p>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">:تاريخ الميلااد&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Passport Number</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBpassportnumber" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <p align="right">
                            <asp:TextBox ID="TextPassportNumber" dir="rtl" runat="server"></asp:TextBox>&nbsp;
                        </p>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">:رقم جواز السفرد&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Passport Issue</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBpassportissue" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextPassportIssue" dir="rtl" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">: صادر من&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="matters" style="width: 70px">
                        &nbsp;Passport Issue Date</td>
                    <td align="center" class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBPassportIssueDate" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td align="right" class="repcoldetail" style="width: 207px"><asp:ImageButton
                                ID="ImageButton8" runat="server" ImageUrl="~/Images/calendar.gif"  />
                       
                        <asp:TextBox ID="TextPassportissuedate" runat="server"></asp:TextBox>&nbsp;</td>
                         <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="TextPassportissuedate" PopupButtonID="ImageButton8" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
                    <td class="matters" style="width: 153px" align="right">
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Passport Expiry Date</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBPassportExpiryDate" runat="server" Height="11px" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="matters" style="width: 207px">
                        <p align="right">
                        </p>
                        <p align="right">
                            <asp:ImageButton
                                ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:TextBox ID="TextPassportExpirity"  runat="server"></asp:TextBox>
                             <ajaxToolkit:CalendarExtender ID="CalendarExtender2" TargetControlID="TextPassportExpirity" PopupButtonID="ImageButton1" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
                            &nbsp;&nbsp;
                        </p>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">: بتاريخ &nbsp;&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Visa Number</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBvisanumber" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="matters" style="width: 207px">
                        <p align="right">
                            <asp:TextBox ID="TextVisaNumber"  dir="rtl" runat="server"></asp:TextBox>&nbsp;
                        </p>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">: إقامة رقم &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Visa From</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBvisafrom" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <p align="right">
                            <asp:ImageButton
                                ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"  />
                            <asp:TextBox ID="TextVisaFrom"  runat="server"></asp:TextBox>&nbsp;
                             <ajaxToolkit:CalendarExtender ID="CalendarExtender3" TargetControlID="TextVisaFrom" PopupButtonID="ImageButton2" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
                        </p>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">: صادرة بتاريخ&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        &nbsp;Visa To</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBvisato" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="matters" style="width: 207px">
                        <p align="right">
                        </p>
                        <p align="right">
                            <asp:ImageButton
                                ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <asp:TextBox ID="TextVisaTo"  runat="server"></asp:TextBox>&nbsp;
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="TextVisaTo" PopupButtonID="ImageButton3" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
                        </p>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">: صالحة لغاية&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td align="middle" class="matters" colspan="4">
                        <p align="center">
                            <font color="navy" size="3">(بيانات ولي الأمـر) </font>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Primary Contact Name</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBPrimaryContactName" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextPrimaryConName" dir="rtl" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">:الاسم &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Primary Contact</td>
                    <td align="middle" class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBlPrimaryContactName" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td align="middle" class="repcoldetail" style="width: 207px">
                        <font color="navy" size="4">
                            <div dir="rtl">
                                <p align="right">
                                    &nbsp;<asp:DropDownList ID="DropPrimaryContact" runat="server">
                                    </asp:DropDownList></p>
                            </div>
                        </font>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <font color="navy" size="3">
                            <p align="right">
                                : صلة القرابة &nbsp;</p>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Occupation</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LbOccupation" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="matters" style="width: 207px">
                        <p align="right">
                        </p>
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextOccupation" dir="rtl" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td align="right" class="matters" style="width: 153px">
                        <p align="right">
                            <font color="navy" size="3">:الوظيفة &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Address 1</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="Lbaddress" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="matters" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextAddress1" dir="rtl" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">:عنوان السكن&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Post Box Number</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBpostboxnumber" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="matters" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextPBNumber" onchange="javascript:change(this)" dir="rtl" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td align="right" class="matters" style="width: 153px">
                        <p align="right">
                            <font color="navy" size="3">:ص.ب&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Telephone Number</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="lbtelephonenumber" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="matters" style="width: 207px">
                        <p align="right">
                        </p>
                        <p align="right">
                        </p>
                        <p align="right">
                            <table>
                                <tr>
                                    <td style="width: 100px; height: 26px">
                                        </td>
                                    <td style="width: 100px; height: 26px">
                                        <asp:TextBox ID="TextTelCode1" onchange="javascript:change(this)" dir="rtl" runat="server" Width="30%"></asp:TextBox>
                                        <asp:TextBox ID="TextTelCode2" onchange="javascript:change(this)" dir="rtl" runat="server" Width="30%"></asp:TextBox></td>
                                    <td style="width: 100px; height: 26px">
                                        <asp:TextBox ID="TextTelNumber" onchange="javascript:change(this)" dir="rtl" runat="server"></asp:TextBox></td>
                                </tr>
                            </table>
                            
                        </p>
                    </td>
                    <td align="right" class="matters" style="width: 153px">
                        <p align="right">
                            <font color="navy" size="3">: ت&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Office Address</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="Lboffficeaddress" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextOfficeAddress" dir="rtl" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td align="right" class="matters" style="width: 153px">
                        <p align="right">
                            <font color="navy" size="3">:عنوان العمل&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Office PO Box</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="lbofficepobox" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <p align="right">
                            <asp:TextBox ID="TextOfficePOBox" onchange="javascript:change(this)"  dir="rtl" runat="server"></asp:TextBox>&nbsp;
                        </p>
                    </td>
                    <td align="right" class="matters" style="width: 153px">
                        <p align="right">
                            <font color="navy" size="3">:ص.ب&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Office Phone</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBofficephone" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <p align="right">
                            <table>
                                <tr>
                                    <td style="width: 100px">
                                        </td>
                                    <td style="width: 100px">
                                        <asp:TextBox ID="TextOffCode1" onchange="javascript:change(this)" dir="rtl" runat="server" Width="30%"></asp:TextBox>
                                        <asp:TextBox ID="TextOffCode2" onchange="javascript:change(this)" dir="rtl" runat="server" Width="30%"></asp:TextBox></td>
                                    <td style="width: 100px">
                                        <asp:TextBox ID="TextOfficePhone" onchange="javascript:change(this)" dir="rtl" runat="server"></asp:TextBox></td>
                                </tr>
                            </table>
                           </p>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            <font color="navy" size="3">: ت&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Previous School 1</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="lbpreviousschool1" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextPreviosSchool1" dir="rtl" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            1.<font color="navy" size="3"> صـادرة من &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Leave Date 1</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="lbLeavedate1" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <p align="right"><asp:ImageButton
                                ID="ImageButton5" runat="server" ImageUrl="~/Images/calendar.gif"  />
                            <asp:TextBox ID="TextLeaveDate1"  runat="server"></asp:TextBox>&nbsp;
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender5" TargetControlID="TextLeaveDate1" PopupButtonID="ImageButton5" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
                        </p>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            1. <font color="navy" size="3">تاريخ ترك الدراسة &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" style="height: 28px; width: 70px;" align="left">
                        Academic Year 1</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBAcadamicyear1" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="height: 28px; width: 207px;">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextAccdYear1" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td class="matters" style="width: 153px;" align="right">
                        <p align="right">
                            1. <font color="navy" size="3">الـعـام الدراسي &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Syllabus 1</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBSyllabus1" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextSyllabus1" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            1. <font color="navy" size="3">المنهاج &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Certificate 1</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBCertificate1" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <div dir="rtl">
                            <p align="right">
                                &nbsp;<asp:TextBox ID="TextCertificate1" runat="server"></asp:TextBox></p>
                        </div>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            1. <font color="navy" size="3">اسم الـشهــادة &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td  class="matters" align="left" style="width: 70px">
                        Previous School 2</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBPreviousSchool2" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                       
                            <p align="right">
                                <asp:TextBox ID="TextPreviosSchool2" dir="rtl" runat="server"></asp:TextBox>&nbsp;</p>
                       
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            2.<font color="navy" size="3"> صـادرة من &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Leave Date 2</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBLeaveDate2" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                     <div dir="rtl">
                        <p align="right">
                            &nbsp;<asp:TextBox ID="TextLeaveDate2" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton
                                ID="ImageButton6" runat="server" ImageUrl="~/Images/calendar.gif"  /></p>
                           <ajaxToolkit:CalendarExtender ID="CalendarExtender6" TargetControlID="TextLeaveDate2" PopupButtonID="ImageButton6" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
                            </div>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            2. <font color="navy" size="3">تاريخ ترك الدراسة &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Academic Year 2</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBAcademicYear2" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        
                            <p align="right">
                                <asp:TextBox ID="TextAccdYear2" dir="rtl" runat="server"></asp:TextBox>&nbsp;</p>
                     
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            2. <font color="navy" size="3">الـعـام الدراسي &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Syllabus 2</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBSyllabus2" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                      
                            <p align="right">
                                <asp:TextBox ID="TextSyllabus2" dir="rtl" runat="server"></asp:TextBox>&nbsp;</p>
                       
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            2. <font color="navy" size="3">المنهاج&nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Certificate 2</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBCertificate2" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                       
                            <p align="right">
                                <asp:TextBox ID="TextCertificate2" dir="rtl" runat="server"></asp:TextBox>&nbsp;</p>
                       
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            2. <font color="navy" size="3">اسم الـشهــادة &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Previous School 3</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBPreviousSchool3" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        
                            <p align="right">
                                <asp:TextBox ID="TextPreviosSchool3" dir="rtl" runat="server"></asp:TextBox>&nbsp;</p>
                      
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            3.<font color="navy" size="3"> صـادرة من &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Leave Date 3</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBLeaveDate3" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                        <p align="right"><asp:ImageButton
                                ID="ImageButton7" runat="server" ImageUrl="~/Images/calendar.gif"  />
                            <asp:TextBox ID="TextLeaveDate3"  runat="server"></asp:TextBox>&nbsp;</p>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender7" TargetControlID="TextLeaveDate3" PopupButtonID="ImageButton7" Format="dd/MMM/yyyy" runat="server"></ajaxToolkit:CalendarExtender>
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            3. <font color="navy" size="3">تاريخ ترك الدراسة &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Academic Year 3</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBAcademicYear3" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                       
                            <p align="right">
                                <asp:TextBox ID="TextAccdYear3" dir="rtl" runat="server"></asp:TextBox>&nbsp;</p>
                       
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            3. <font color="navy" size="3">الـعـام الدراسي &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" style="height: 33px; width: 70px;" align="left">
                        Syllabus 3</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBSyllabus3" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="height: 33px; width: 207px;">
                       
                            <p align="right">
                                <asp:TextBox ID="TextSyllabus3" dir="rtl" runat="server"></asp:TextBox>&nbsp;</p>
                       
                    </td>
                    <td class="matters" style="width: 153px;" align="right">
                        <p align="right">
                            3. <font color="navy" size="3">المنهاج &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" align="left" style="width: 70px">
                        Certificate 3</td>
                    <td class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                        <asp:Label id="LBCertificate3" runat="server" Font-Names="Verdana" Font-Size="Small">&nbsp;</asp:Label></td>
                    <td class="repcoldetail" style="width: 207px">
                      
                            <p align="right">
                                <asp:TextBox ID="TextCertificate3" dir="rtl" runat="server"></asp:TextBox>&nbsp;</p>
                      
                    </td>
                    <td class="matters" style="width: 153px" align="right">
                        <p align="right">
                            3. <font color="navy" size="3">اسم الـشهــادة &nbsp;</font></p>
                    </td>
                </tr>
                <tr>
                    <td class="matters" style="width: 70px">
                    </td>
                    <td align="center" class="repcoldetail" style="border-collapse: separate; table-layout: fixed; width: 169px;">
                    </td>
                    <td align="center" class="repcoldetail" style="width: 207px">
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
                    <td class="matters" style="width: 153px" align="right">
                    </td>
                </tr>
                <tr height="35">
                    <td colspan="4">
                        <center>
                            &nbsp;
                            <asp:Button ID="btnsave"  runat="server" Text="SAVE" CssClass="button" />
                            <asp:Button ID="btnreset" runat="server" Text="RESET" CssClass="button" />&nbsp;
                        </center>
                        <center>
                            <asp:Label ID="lblEmessage" runat="server" ForeColor="Red"></asp:Label>
                            <asp:HiddenField ID="Hiddenoption" runat="server" /><asp:HiddenField ID="Hiddenstuid" runat="server" />
                        </center>
                    </td>
                </tr>
            </table>
        
        </asp:Panel>

        <asp:Panel id="Panel2" CssClass="matters" runat="server" Visible="false">Click here to <asp:LinkButton
                id="lnkPrint" runat="server" OnClick="lnkPrint_Click">Print</asp:LinkButton><br />Click here to go  <asp:LinkButton
                    id="lnkBack" runat="server" OnClick="lnkBack_Click">Back</asp:LinkButton> to list </asp:Panel>
        </div>
</asp:Content>
    