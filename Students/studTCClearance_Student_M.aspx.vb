
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studTCClearance_Student_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100255") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    Session("liUserList") = New List(Of String)
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"
                    txtApr.Text = Format(Now.Date, "dd/MMM/yyyy")
                    GridBind()
                    lblTitle.Text = "Clearances"


                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

        Else


            studClass.SetChk(gvStud, Session("liUserList"))
        End If
    End Sub
    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Try
                Dim lblStuId As Label = e.Row.FindControl("lblStuId")
                Dim lblLibClearance As Label = e.Row.FindControl("lblLibClearance")
                Dim lblLabClearance As Label = e.Row.FindControl("lblLabClearance")
                Dim library As String = lblLibClearance.Text
                Dim lab As String = lblLabClearance.Text
                Dim imgFee As Image = e.Row.FindControl("imgFee")
                Dim imgLib As Image = e.Row.FindControl("imgLib")
                Dim imgLab As Image = e.Row.FindControl("imgLab")
                Dim remarks As String = ""
                remarks = getFeeRemarks(lblStuId.Text)
                If remarks <> "" Then
                    imgFee.ImageUrl = "~/Images/tick.gif"
                Else
                    imgFee.ImageUrl = "~/images/cross.gif"
                End If

                If library <> -1 Then
                    If library <> 0 Then
                        imgLib.ImageUrl = "~/images/cross.gif"
                        imgLib.Visible = True
                        lblLibClearance.Visible = False
                    Else
                        imgLib.ImageUrl = "~/Images/tick.gif"
                        imgLib.Visible = True
                        lblLibClearance.Visible = False
                    End If

                Else
                    lblLibClearance.Text = "NA"
                    lblLibClearance.Visible = True
                    imgLib.Visible = False
                End If

                If lab <> -1 Then
                    If lab <> 0 Then
                        imgLab.ImageUrl = "~/images/cross.gif"
                        imgLab.Visible = True
                        lblLabClearance.Visible = False
                    Else
                        imgLab.ImageUrl = "~/Images/tick.gif"
                        imgLab.Visible = True
                        lblLabClearance.Visible = False
                    End If

                Else
                    lblLabClearance.Text = "NA"
                    lblLabClearance.Visible = True
                    imgLab.Visible = False
                End If

            Catch ex As Exception

            End Try

                '    Dim gvClearance As GridView
                '    Dim lblTcmId As Label

                '    gvClearance = e.Row.FindControl("gvClearance")
                '    lblTcmId = e.Row.FindControl("lblTcmId")
                '    If lblTcmId.Text <> "" Then
                '        BindClearances(lblTcmId.Text, gvClearance)
                '    End If
        End If
    End Sub

    Private Function getFeeRemarks(ByVal IntSTU_ID As Integer) As String

        Dim STUDID As Integer = 0
        Dim iReturnValue As Integer
        Dim cmd As SqlCommand
        Dim Remarks As String = ""
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try
                cmd = New SqlCommand("OASIS_FEES.FEES.CheckPendingApprovals", conn)
                cmd.CommandType = CommandType.StoredProcedure

                Dim sqlpSTUD_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
                sqlpSTUD_ID.Value = IntSTU_ID
                cmd.Parameters.Add(sqlpSTUD_ID)

                Dim sqlReturnValue As New SqlParameter("@REMARKS", SqlDbType.VarChar, 200)
                sqlReturnValue.Direction = ParameterDirection.Output
                cmd.Parameters.Add(sqlReturnValue)

                iReturnValue = cmd.ExecuteNonQuery()
                If iReturnValue <> 0 Then
                    If Not IsDBNull(sqlReturnValue.Value) Then
                        Remarks = sqlReturnValue.Value
                    End If

                End If
            Catch ex As Exception
                Remarks = "Error"
            End Try
        End Using
        Return Remarks

    End Function

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Session("liUserList") = New List(Of String)
        lblError.Text = ""
        SaveData()
        GridBind()
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        'str_query = "SELECT STU_ID,STU_NO,STU_GRD_ID,STU_SCT_ID,TCM_ID,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
        '            & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " _
        '            & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
        '            & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
        '            & " INNER JOIN TCM_M AS D ON A.STU_ID=D.TCM_STU_ID " _
        '            & " WHERE STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString _
        '            & " AND TCM_TCSO='TC' AND TCM_CANCELDATE IS NULL AND ISNULL(TCM_bCLEARANCE,'FALSE')='FALSE'" _
        '            & " AND STU_CURRSTATUS<>'CN' AND ISNULL(TCM_bPREAPPROVED,'FALSE')='TRUE'"

        str_query = "getStudentTCClearabceList " + ddlAcademicYear.SelectedValue.ToString


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        If gvStud.Rows.Count > 0 Then


            txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            txtSearch = gvStud.HeaderRow.FindControl("txtSection")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SCT_DESCR", txtSearch.Text, strSearch)
            selectedSection = txtSearch.Text

            If strFilter <> "" Then
                ' str_query += strFilter
            End If

        End If
        'If ViewState("MainMnu_code") <> "S100254" Then
        '    str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        'End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim dv As DataView = ds.Tables(0).DefaultView
        If strFilter <> "" Then
            dv.RowFilter = "1=1 " + strFilter
        End If
        gvStud.DataSource = dv
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStud.DataBind()
        End If


        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
        txtSearch.Text = selectedGrade

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtSection")
        txtSearch.Text = selectedSection

        set_Menu_Img()

        studClass.SetChk(gvStud, Session("liUserList"))
    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Session("liUserList") = New List(Of String)
        GridBind()
    End Sub


    Protected Sub gvStudTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvStudTPT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        'If e.CommandName = "view" Then
        '    Dim url As String
        '    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        '    Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
        '    Dim lblStuId As Label
        '    Dim lblStuName As Label
        '    Dim lblStuNo As Label
        '    With selectedRow
        '        lblStuId = .FindControl("lblStuId")
        '        lblStuName = .FindControl("lblStuName")
        '        lblStuNo = .FindControl("lblStuNo")
        '    End With
        '    If ViewState("MainMnu_code") = "S100250" Then
        '        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        '        url = String.Format("~\Students\studStrikeOffRecommend_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        '    ElseIf ViewState("MainMnu_code") = "S100251" Then
        '        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        '        url = String.Format("~\Students\studStrikeOffApproval_M.aspx?MainMnu_code={0}&datamode={1}&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        '    ElseIf ViewState("MainMnu_code") = "S100252" Then
        '        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        '        url = String.Format("~\Students\studStrikeOffRequest_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        '    ElseIf ViewState("MainMnu_code") = "S100253" Then
        '        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        '        url = String.Format("~\Students\studStrikeOffCancelRequest_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        '    ElseIf ViewState("MainMnu_code") = "S100254" Then
        '        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        '        url = String.Format("~\Students\studStrikeOffCancel_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) + "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) + "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        '    End If
        '    Response.Redirect(url)
        'End If
    End Sub

    Sub BindClearances(ByVal tcmId As String, ByVal gvClearance As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCS_ID,TCC_DESCR,TCC_FEE_ID FROM STUDENT_TCCLEARANCE_S AS A INNER JOIN" _
                                & " TCCLEARANCE_M AS B ON A.TCS_TCC_ID=B.TCC_ID WHERE TCS_TCM_ID=" + tcmId
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvClearance.DataSource = ds
        gvClearance.DataBind()
    End Sub

    Function getClearance_XML(ByVal gvClearance As GridView) As String
        Dim str As String = ""
        Dim i As Integer
        Dim lblTcsId As Label
        Dim lblFeeId As Label
        Dim txtFee As TextBox
        For i = 0 To gvClearance.Rows.Count - 1
            With gvClearance.Rows(i)
                lblTcsId = .FindControl("lblTcsId")
                lblFeeId = .FindControl("lblFeeId")
                txtFee = .FindControl("txtFee")

                str += "<ID><TCS_ID>" + lblTcsId.Text + "</TCS_ID><FEE_ID>" + lblFeeId.Text + "</FEE_ID><FEE_AMT>" + Val(txtFee.Text).ToString + "</FEE_AMT></ID>"
            End With
        Next

        If str = "" Then
            str = "<ID><TCS_ID>0</TCS_ID><FEE_ID>0</FEE_ID><FEE_AMT>0</FEE_AMT></ID>"
        End If
        str = "<IDS>" + str + "</IDS>"
        Return str
    End Function

    Sub SaveData()
        Dim str_query As String
        Dim strLibrary As String = ""
        Dim strLab As String = ""
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim lblTcmId As Label
        Dim lblGrdId As Label
        Dim lblSctId As Label
        Dim lblStuNo As Label
        Dim lblLibClearance As Label
        Dim lblLabClearance As Label
        Dim gvClearance As GridView
        Dim str_XML As String = ""
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim transaction As SqlTransaction

        For i = 0 To gvStud.Rows.Count - 1
            With gvStud.Rows(i)

                chkSelect = .FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    lblStuId = .FindControl("lblStuId")
                    lblTcmId = .FindControl("lblTcmId")
                    lblGrdId = .FindControl("lblGrdId")
                    lblSctId = .FindControl("lblSctId")
                    lblStuNo = .FindControl("lblStuNo")
                    lblLibClearance = .FindControl("lblLibClearance")
                    lblLabClearance = .FindControl("lblLabClearance")
                    If lblLibClearance.Text = "0" Then
                        lblError.Text += "Library Clearance Pending for For Student No: " + lblStuNo.Text
                        Continue For
                    End If
                    If lblLabClearance.Text = "0" Then
                        lblError.Text += "Lab Clearance Pending for For Student No: " + lblStuNo.Text
                        Continue For
                    End If

                  
                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                        transaction = conn.BeginTransaction("SampleTransaction")
                        Try

                            Dim FeeRemarks As String = ""
                            FeeRemarks = SetClearanceStatus(CInt(lblStuId.Text), conn, transaction)

                            If FeeRemarks <> "" Then
                                transaction.Rollback()
                                lblError.Text += "For Student No: " + lblStuNo.Text + " " + FeeRemarks
                                ' conn.Close()
                                'Exit Sub
                            Else

                                UtilityObj.InsertAuditdetails(transaction, "edit", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + lblTcmId.Text, "TCM_M_CLEAR")
                                UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_TCCLEARANCE_S", "TCS_ID", "TCS_TCM_ID", "TCS_TCM_ID=" + lblTcmId.Text)

                                'str_query = "exec saveSTUDENTTCCLEARANCE_M " _
                                '          & lblStuId.Text + "," _
                                '          & lblTcmId.Text + "," _
                                '          & "'" + Session("sbsuid") + "'," _
                                '          & ddlAcademicYear.SelectedValue.ToString + "," _
                                '          & lblSctId.Text + "," _
                                '          & "'" + lblGrdId.Text + "'," _
                                '          & "'" + Format(Date.Parse(txtApr.Text), "yyyy-MM-dd") + "'," _
                                '          & "," _
                                '          & "'" + Session("sUsr_name") + "'"

                                str_query = "exec saveSTUDENTTCCLEARANCE_M " _
                                            & lblTcmId.Text



                                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "TCM_ID(" + lblTcmId.Text.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

                                If flagAudit <> 0 Then
                                    Throw New ArgumentException("Could not process your request")
                                End If
                                transaction.Commit()

                                If lblError.Text = "" Then
                                    lblError.Text = "Record Saved Successfully"
                                End If
                            End If

                        Catch myex As ArgumentException
                            transaction.Rollback()
                            lblError.Text = myex.Message
                            UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                        Catch ex As Exception
                            transaction.Rollback()
                            lblError.Text = "Record could not be Saved"
                            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                        End Try
                    End Using

                    '   If Session("sbsuid") = "131001" Or Session("sbsuid") = "131002" Or Session("sbsuid") = "123016" Then
                    str_query = "EXEC INSERTEMAILS_TC " _
                        & " @BSU_ID ='" + Session("SBSUID") + "'," _
                        & " @STU_ID ='" + lblStuId.Text + "'," _
                        & " @EMAILTYPE='TC_CLEARANCE'," _
                        & " @SENDER='SCHOOL'"

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    'End If
                End If
            End With
        Next
    End Sub
    Private Function SetClearanceStatus(ByVal IntSTU_ID As Integer, ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction) As String

        Dim STUDID As Integer = 0
        Dim iReturnValue As Integer
        Dim cmd As SqlCommand
        Dim Remarks As String = ""
        
        Try
            

            cmd = New SqlCommand("OASIS_FEES.FEES.CheckPendingApprovals", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpSTUD_ID As New SqlParameter("@STU_ID", SqlDbType.BigInt)
            sqlpSTUD_ID.Value = IntSTU_ID
            cmd.Parameters.Add(sqlpSTUD_ID)

            Dim sqlReturnValue As New SqlParameter("@REMARKS", SqlDbType.VarChar, 200)
            sqlReturnValue.Direction = ParameterDirection.Output
            cmd.Parameters.Add(sqlReturnValue)

            iReturnValue = cmd.ExecuteNonQuery()
            If iReturnValue <> 0 Then
                If Not IsDBNull(sqlReturnValue.Value) Then
                    Remarks = sqlReturnValue.Value
                End If

            End If


        Catch ex As Exception
            Remarks = "Error"
        End Try
        Return Remarks
        
    End Function
#End Region

  
End Class
