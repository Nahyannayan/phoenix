<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studtcClearance_M.aspx.vb" Inherits="Students_studtcClearance_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_delete() {

            if (confirm("You are about to delete this record.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>TC Clearance Master
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="height: 79px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" Style="text-align: left" Width="342px" />
                            &nbsp;&nbsp;

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom" style="height: 20px">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Width="133px" Style="text-align: center" Height="24px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td   valign="top">
                            <table align="center"  cellpadding="5" cellspacing="0"
                                style="width: 100%">
                                

                                <tr>
                                    <td align="left"  ><span class="field-label">Clearance Type</span></td>
                                   
                                    <td align="left"   colspan="4">
                                        <asp:TextBox ID="txtClearance" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                                    </td>
                                
                                    <td align="left"  ><span class="field-label">Fee Type</span></td>
                                    
                                    <td align="left"  colspan="4" >
                                        <asp:DropDownList ID="ddlFeeType" runat="server" Width="155px">
                                        </asp:DropDownList></td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center"  valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" TabIndex="5" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" TabIndex="6" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" />
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom" style="height: 52px">
                            <asp:HiddenField ID="hfTCC_ID" runat="server" />
                            <asp:RequiredFieldValidator ID="rfCl" runat="server" ErrorMessage="Please enter the field Clearance"
                                ControlToValidate="txtClearance" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        </td>

                    </tr>
                </table>
                </div>
            </div>
        </div>
</asp:Content>

