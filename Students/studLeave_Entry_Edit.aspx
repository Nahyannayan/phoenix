<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studLeave_Entry_Edit.aspx.vb" Inherits="Students_studLeave_Entry_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Leave Approval Entry"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--<table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%">
               LEAVE APPROVAL ENTRY</td>
        </tr>
    </table>--%>
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <%--<td align="center" >
                &nbsp;Fields Marked<span style="font-size: 8pt; color: #800000"> </span>with(<span
                    style="font-size: 8pt; color: #800000">*</span>)are mandatory</td>--%>
                        <td align="center" class="text-danger font-small" valign="middle">Fields Marked with ( * ) are mandatory
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr>
                        <td class="subheader_img" colspan="6">
                            <asp:Literal id="ltLabel" runat="server" Text="Leave Approval Entry"></asp:Literal></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblAcdYear" runat="server" CssClass="field-value"></asp:Label></td>

                                    <td align="left" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student Id(Fees)</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblStudID" runat="server" CssClass="field-value"></asp:Label></td>

                                    <td align="left"><span class="field-label">MOE Reg. No</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblMOE" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Full Name in English </span>
                                        <br />
                                        <span class="field-label">(First,Middle,Last)</span></td>
                                    <td align="left" colspan="3">
                                        <asp:Label ID="lblSName" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Stream</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblStream" runat="server" CssClass="field-value"></asp:Label></td>

                                    <td align="left"><span class="field-label">Shift</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblShift" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>

                                    <td align="left"><span class="field-label">Section</span></td>
                                    <td align="left">
                                        <asp:Label ID="lblSection" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg-lite" colspan="4">Add Detail</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">From Date</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" runat="server">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                            CssClass="error" Display="Dynamic" ErrorMessage="From Date required" ForeColor=""
                                            ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                                    <td align="left"><span class="field-label">To Date</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtTo" runat="server">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvTo" runat="server" ControlToValidate="txtTo" CssClass="error"
                                            Display="Dynamic" ErrorMessage="To Date required" ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Leave Status</span></td>

                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlAPD_ID" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span><span class="text-danger font-small">*</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" SkinID="MultiText" TextMode="MultiLine"
                                            Width="220px">
                                        </asp:TextBox><asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Remarks field can not be left empty"
                                            ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnAddDetail" runat="server" CssClass="button" Text="Add Detail" OnClick="btnAddDetail_Click" ValidationGroup="AttGroup" />&nbsp;
                            <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update Detail" OnClick="btnUpdate_Click" ValidationGroup="AttGroup" /></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvHolidayDetail" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                Width="100%" OnRowEditing="gvHolidayDetail_RowEditing">
                                <RowStyle CssClass="table table-bordered table-row" />
                                <EmptyDataRowStyle />
                                <Columns>
                                    <asp:BoundField DataField="SRNO" HeaderText="Sl.No"></asp:BoundField>
                                    <asp:BoundField DataField="FROMDT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="From Date" HtmlEncode="False">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TODT" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="To Date" HtmlEncode="False">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="leaveStatus" HeaderText="Leave Status"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Status" ShowHeader="False">
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" CausesValidation="True" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CausesValidation="False" CommandName="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CausesValidation="False" CommandName="Edit"></asp:LinkButton>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# bind("STATUS") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="APD_ID" HeaderText="APD_ID" Visible="False"></asp:BoundField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnEdit_Click" Text="Add/Edit" />&nbsp;
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />

                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtTo">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

