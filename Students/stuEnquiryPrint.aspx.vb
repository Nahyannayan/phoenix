Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Partial Class Students_stuEnquiryPrint
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            hfENQ_ID.Value = Encr_decrData.Decrypt(Request.QueryString("enqid").Replace(" ", "+"))

            HiddenBsuId.Value = Session("sbsuid")
            HiddenAcademicyear.Value = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
            BindBsuDetails()
            PopulateEnquiryData()
            PopulatePrevData()
        End If

    End Sub
    Public Sub BindBsuDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "select BSU_NAME,BSU_ADDRESS,BSU_POBOX,BSU_CITY,BSU_TEL,BSU_FAX,BSU_EMAIL,BSU_MOE_LOGO,BSU_URL from BUSINESSUNIT_M where BSU_ID='" & HiddenBsuId.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        BsuName.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_NAME").ToString()
        'bsuAddress.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_ADDRESS").ToString()
        'bsupostbox.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_POBOX").ToString()
        'bsucity.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_CITY").ToString()
        'bsutelephone.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_TEL").ToString()
        'bsufax.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_FAX").ToString()
        'bsuemail.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_EMAIL").ToString()
        'bsuwebsite.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_URL").ToString()
        imglogo.ImageUrl = ds.Tables(0).Rows(0).Item("BSU_MOE_LOGO").ToString()
    End Sub
    Private Sub PopulateEnquiryData()

        'Try

        bind_qus()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "exec GETENQUIRY_PRINT '" + hfENQ_ID.Value + "','" + Session("sbsuid") + "'"
        Dim reader As SqlDataReader
 
        Dim primarycontact As String = ""
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem

        While reader.Read

            '''''Main

            ltEnqDate.Text = DateFormat(Convert.ToDateTime(reader("EQM_ENQDATE")))

            If Not IsDBNull(reader("EQS_REGNDATE")) Then
                ltRegDate.Text = DateFormat(Convert.ToDateTime(reader("EQS_REGNDATE")))
            End If

            ltStuId.Text = Convert.ToString(reader("EQS_STU_NO"))

            ltName.Text = Convert.ToString(reader("APPLNAME")).Trim.Replace("''", "'")
            ltDob.Text = DateFormat(Convert.ToDateTime(reader("EQM_APPLDOB")))
            If Convert.ToString(reader("EQM_APPLGENDER")).Trim = "M" Then
                ltGender.Text = "Male"
            Else
                ltGender.Text = "Female"
            End If

            primarycontact = Convert.ToString(reader("EQM_PRIMARYCONTACT"))
            If primarycontact = "F" Then
                ltPri_cont.Text = "Father"
            ElseIf primarycontact = "M" Then
                ltPri_cont.Text = "Mother"
            Else
                ltPri_cont.Text = "Guardian"
            End If
            ltReg.Text = Convert.ToString(reader("RLG_DESCR"))
            ltPOB.Text = Convert.ToString(reader("EQM_APPLPOB"))
            ltCOB.Text = Convert.ToString(reader("CTY_DESCR"))
            ltNat.Text = Convert.ToString(reader("Nat"))

            ltFpreferedcontact.Text = Convert.ToString(reader("EQM_PREFCONTACT"))
            ltEmgNo.Text = Convert.ToString(reader("EQM_EMGCONTACT"))
            ltEmployer_ShortEnq.Text = Convert.ToString(reader("EMPLOYER_SHORTENQ"))
            ltPassNo.Text = Convert.ToString(reader("APPLPASPRTNO"))
            ltIssPlace.Text = Convert.ToString(reader("APPLPASPRTISSPLACE"))
            ltIssDate.Text = DateFormat(Convert.ToDateTime(reader("APPLPASPRTISSDATE")))
            ltExpDate.Text = DateFormat(Convert.ToDateTime(reader("APPLPASPRTEXPDATE")))
            ltVisaNo.Text = Convert.ToString(reader("APPLVISANO"))
            ltIssPlace_V.Text = Convert.ToString(reader("APPLVISAISSPLACE"))
            ltIssDate_V.Text = DateFormat(Convert.ToDateTime(reader("APPLVISAISSDATE")))
            ltExpr_Date_V.Text = DateFormat(Convert.ToDateTime(reader("APPLVISAEXPDATE")))
            ltIssAuth.Text = Convert.ToString(reader("APPLVISAISSAUTH"))

            ltFirstLang.Text = Convert.ToString(reader("EQM_FIRSTLANG"))
            ltOthLang.Text = Convert.ToString(reader("EQM_OTHLANG"))




            Dim PREVSCHOOL_NURSERY As String = Convert.ToString(reader("PREVSCHOOL_NURSERY"))
            Dim FlagCurrGEMS_Sch As Boolean = Convert.ToBoolean(reader("EQM_bPREVSCHOOLGEMS"))

            If Trim(PREVSCHOOL_NURSERY) = "" Then


                trCurrSch_1.Visible = True
                trCurrSch_2.Visible = True
                trCurrSch_3.Visible = True
                trCurrSch_4.Visible = True
                trCurrSch_5.Visible = True
                trCurrSch_6.Visible = True
                trPrevSCH_id.Visible = True


                ltCurr_School.Text = Convert.ToString(reader("EQM_PREVSCHOOL"))
                ltCurr_City.Text = Convert.ToString(reader("EQM_PREVSCHOOL_CITY"))
                ltCurr_Country.Text = Convert.ToString(reader("PCTY_DESCR"))
                ltCurr_Grade.Text = Convert.ToString(reader("PREVSCHOOL_GRD_ID"))
                ltCurr_Instr.Text = Convert.ToString(reader("EQM_PREVSCHOOL_MEDIUM"))
                ltCurr_FromDT.Text = DateFormat(Convert.ToDateTime(reader("PREVSCHOOL_LASTATTDATE")))
                ltCurr_Curr.Text = Convert.ToString(reader("PREVSCHOOL_CLM_DESCR"))
                ltCurr_Head.Text = Convert.ToString(reader("EQM_PREVSCHOOL_HEAD_NAME"))
                ltCurr_Stu_id.Text = Convert.ToString(reader("PREVSCHOOL_REG_ID"))
                ltCurr_Addr.Text = Convert.ToString(reader("EQM_PREVSCHOOL_ADDRESS"))
                ltCurr_City.Text = Convert.ToString(reader("EQM_PREVSCHOOL_CITY"))
                ltCurr_Country.Text = Convert.ToString(reader("PCTY_DESCR"))
                ltCurr_Sch_ph.Text = Convert.ToString(reader("EQM_PREVSCHOOL_PHONE"))
                ltCurr_Sch_fax.Text = Convert.ToString(reader("EQM_PREVSCHOOL_FAX"))
                ltCurr_ToDT.Text = DateFormat(Convert.ToDateTime(reader("EQM_PREVSCHOOL_FROMDT")))

            Else

                ltCurr_School.Text = StrConv(PREVSCHOOL_NURSERY, VbStrConv.ProperCase)
                trCurrSch_1.Visible = False
                trCurrSch_2.Visible = False
                trCurrSch_3.Visible = False
                trCurrSch_4.Visible = False
                trCurrSch_5.Visible = False
                trCurrSch_6.Visible = False
                trPrevSCH_id.Visible = False
            End If
            trF_GEMS.Visible = False
            trM_GEMS.Visible = False
            ltM_GEMS.Text = "No"
            ltF_GEMS.Text = "No"
       



            If Convert.ToString(reader("XBSU_FNAME")).Trim = "" Then
                ltF_ExGEms.Text = "No"
                ltFStaff_ACD.Text = ""
                ltFStaff_GEMS_School.Text = ""
                trF_ExGEMS.Visible = False
            Else
                trF_ExGEMS.Visible = True
                ltF_ExGEms.Text = "Yes"
                ltFStaff_ACD.Text = Convert.ToString(reader("F_ACD_YEAR"))
                ltFStaff_GEMS_School.Text = Convert.ToString(reader("XBSU_FNAME"))
            End If

            'If primarycontact = "F" Then
            '    If Convert.ToBoolean(reader("EQM_bSTAFFGEMS")) = True Then
            '        trF_GEMS.Visible = True

            '        ltF_GEMS.Text = "Yes"
            '        ltFStaffID.Text = Convert.ToString(reader("EMP_NO"))
            '        ltFStaff_BSU.Text = Convert.ToString(reader("STBSU_NAME"))
            '    End If
            'ElseIf primarycontact = "M" Then
            '    If Convert.ToBoolean(reader("EQM_bSTAFFGEMS")) = True Then
            '        trM_GEMS.Visible = True

            '        ltM_GEMS.Text = "Yes"
            '        ltMStaffID.Text = Convert.ToString(reader("EMP_NO"))
            '        ltMStaff_BSU.Text = Convert.ToString(reader("STBSU_NAME"))
            '    End If
            'End If


            If Convert.ToBoolean(reader("EQP_bFGEMSSTAFF")) = True Then
                trF_GEMS.Visible = True
                ltF_GEMS.Text = "Yes"
                ltFStaffID.Text = Convert.ToString(reader("F_EMP_NO"))
                ltFStaff_BSU.Text = Convert.ToString(reader("F_BSU_NAME"))
            Else

                ltF_GEMS.Text = "No"


            End If


            If Convert.ToBoolean(reader("EQP_bMGEMSSTAFF")) = True Then
                trM_GEMS.Visible = True
                ltM_GEMS.Text = "Yes"
                ltMStaffID.Text = Convert.ToString(reader("M_EMP_NO"))
                ltMStaff_BSU.Text = Convert.ToString(reader("M_BSU_NAME"))
            Else

                ltM_GEMS.Text = "No"


            End If


            If Convert.ToString(reader("XBSU_MNAME")).Trim = "" Then
                ltM_ExGEms.Text = "No"
                ltMStaff_ACD.Text = ""
                ltMStaff_GEMS_School.Text = ""
                trM_ExGEMS.Visible = False
            Else
                ltM_ExGEms.Text = "Yes"
                ltMStaff_ACD.Text = Convert.ToString(reader("M_ACD_YEAR"))
                ltMStaff_GEMS_School.Text = Convert.ToString(reader("XBSU_MNAME"))
                trM_ExGEMS.Visible = True
            End If






            If Trim(StrConv(Convert.ToString(reader("EQM_FILLED_BY")), VbStrConv.Uppercase)) = "P" Then
                ltFilled_by.Text = "Parent"
            Else
                ltFilled_by.Text = "Relocation Agent"


            End If

            'fathers details------------------------------

            ltFather_name.Text = Convert.ToString(reader("EQP_FATHERNAME")).Trim.Replace("''", "'")

            ltFnationality.Text = Convert.ToString(reader("F_Nationality1"))
            ltFNat2.Text = Convert.ToString(reader("F_Nationality2"))
            ltFpobox.Text = Convert.ToString(reader("EQP_FCOMPOBOX"))
            ltFofficetelephone.Text = Convert.ToString(reader("EQP_FOFFPHONE"))
            ltFhometelphone.Text = Convert.ToString(reader("EQP_FRESPHONE"))
            ltFfax.Text = Convert.ToString(reader("EQP_FFAX"))
            ltFmobile.Text = Convert.ToString(reader("EQP_FMOBILE"))
            ltFemail.Text = Convert.ToString(reader("EQP_FEMAIL"))


            ltFAdd1.Text = Convert.ToString(reader("EQP_FPRMADDR1"))
            ltFAdd2.Text = Convert.ToString(reader("EQP_FPRMADDR2"))


            ltFAddCity.Text = Convert.ToString(reader("EQP_FPRMCITY"))
            ltFAdd_Country.Text = Convert.ToString(reader("F_CTY_DESCRPER"))
            ltFAddPhone.Text = Convert.ToString(reader("EQP_FPRMPHONE"))

            ltFAdd_Zip.Text = Convert.ToString(reader("F_pobox"))

            ltFApart.Text = Convert.ToString(reader("EQP_FCOMAPARTNO"))
            ltFBuild.Text = Convert.ToString(reader("EQP_FCOMBLDG"))
            ltFStreet.Text = Convert.ToString(reader("EQP_FCOMSTREET"))
            ltFArea.Text = Convert.ToString(reader("EQP_FCOMAREA"))
            ltFemirate.Text = Convert.ToString(reader("F_EMR_DESCR"))

            ltFOcc.Text = Convert.ToString(reader("EQP_FOCC"))
            ltFComp.Text = Convert.ToString(reader("EQP_FCOMPANY"))
            ltFCurr_Contry.Text = Convert.ToString(reader("F_CTY_DESCRCURR"))

            'fathers details ends------------------------------

            'Mothers details STARTs------------------------------

            ltMother_name.Text = Convert.ToString(reader("EQP_MOTHERNAME")).Trim.Replace("''", "'")

            ltMnationality.Text = Convert.ToString(reader("M_Nationality1"))
            ltMNat2.Text = Convert.ToString(reader("M_Nationality2"))
            ltMpobox.Text = Convert.ToString(reader("EQP_MCOMPOBOX"))
            ltMofficetelephone.Text = Convert.ToString(reader("EQP_MOFFPHONE"))
            ltMhometelphone.Text = Convert.ToString(reader("EQP_MRESPHONE"))
            ltMfax.Text = Convert.ToString(reader("EQP_MFAX"))
            ltMmobile.Text = Convert.ToString(reader("EQP_MMOBILE"))
            ltMemail.Text = Convert.ToString(reader("EQP_MEMAIL"))


            ltMAdd1.Text = Convert.ToString(reader("EQP_MPRMADDR1"))
            ltMAdd2.Text = Convert.ToString(reader("EQP_MPRMADDR2"))


            ltMAddCity.Text = Convert.ToString(reader("EQP_MPRMCITY"))
            ltMAdd_Country.Text = Convert.ToString(reader("M_CTY_DESCRPER"))
            ltMAddPhone.Text = Convert.ToString(reader("EQP_MPRMPHONE"))

            ltMAdd_Zip.Text = Convert.ToString(reader("M_pobox"))

            ltMApart.Text = Convert.ToString(reader("EQP_MCOMAPARTNO"))
            ltMBuild.Text = Convert.ToString(reader("EQP_MCOMBLDG"))
            ltMStreet.Text = Convert.ToString(reader("EQP_MCOMSTREET"))
            ltMArea.Text = Convert.ToString(reader("EQP_MCOMAREA"))
            ltMemirate.Text = Convert.ToString(reader("M_EMR_DESCR"))

            ltMOcc.Text = Convert.ToString(reader("EQP_MOCC"))
            ltMComp.Text = Convert.ToString(reader("EQP_MCOMPANY"))
            ltMCurr_Contry.Text = Convert.ToString(reader("M_CTY_DESCRCURR"))


            'gUARDIAN details STARTs------------------------------




            ltGuardian_name.Text = Convert.ToString(reader("EQP_GuardianNAME")).Trim.Replace("''", "'")

            ltGnationality.Text = Convert.ToString(reader("G_Nationality1"))
            ltGNat2.Text = Convert.ToString(reader("G_Nationality2"))
            ltGpobox.Text = Convert.ToString(reader("EQP_GCOMPOBOX"))
            ltGofficetelephone.Text = Convert.ToString(reader("EQP_GOFFPHONE"))
            ltGhometelphone.Text = Convert.ToString(reader("EQP_GRESPHONE"))
            ltGfax.Text = Convert.ToString(reader("EQP_GFAX"))
            ltGmobile.Text = Convert.ToString(reader("EQP_GMOBILE"))
            ltGemail.Text = Convert.ToString(reader("EQP_GEMAIL"))


            ltGAdd1.Text = Convert.ToString(reader("EQP_GPRMADDR1"))
            ltGAdd2.Text = Convert.ToString(reader("EQP_GPRMADDR2"))


            ltGAddCity.Text = Convert.ToString(reader("EQP_GPRMCITY"))
            ltGAdd_Country.Text = Convert.ToString(reader("G_CTY_DESCRPER"))
            ltGAddPhone.Text = Convert.ToString(reader("EQP_GPRMPHONE"))

            ltGAdd_Zip.Text = Convert.ToString(reader("G_pobox"))

            ltGApart.Text = Convert.ToString(reader("EQP_GCOMAPARTNO"))
            ltGBuild.Text = Convert.ToString(reader("EQP_GCOMBLDG"))
            ltGStreet.Text = Convert.ToString(reader("EQP_GCOMSTREET"))
            ltGArea.Text = Convert.ToString(reader("EQP_GCOMAREA"))
            ltGemirate.Text = Convert.ToString(reader("G_EMR_DESCR"))

            ltGOcc.Text = Convert.ToString(reader("EQP_GOCC"))
            ltGComp.Text = Convert.ToString(reader("EQP_GCOMPANY"))






            ltFamily_detail.Text = Convert.ToString(reader("EQP_FAMILY_NOTE"))



            ltEmail_promo.Text = Convert.ToString(reader("EQM_bRCVMAIL"))
            ltMob_promo.Text = Convert.ToString(reader("EQM_bRCVSMS"))
            ltPhoto_promo.Text = Convert.ToString(reader("EQM_bRCVPUBL"))


            ltHth_No.Text = Convert.ToString(reader("EQM_HEALTHCARDNO"))
            ltB_grp.Text = Convert.ToString(reader("EQM_BLOODGROUP"))
            ltAlg_Detail.Text = Convert.ToString(reader("EQS_bALLERGIES"))
            divAlg.InnerText = Convert.ToString(reader("EQS_ALLERGIES"))

            ltSp_med.Text = Convert.ToString(reader("EQS_bRCVSPMEDICATION"))
            divSp_med.InnerText = Convert.ToString(reader("EQS_SPMEDICN"))

            ltPhy_edu.Text = Convert.ToString(reader("EQS_bPRESTRICTIONS"))
            divPhy_edu.InnerText = Convert.ToString(reader("EQS_PRESTRICTIONS"))

            ltHth_Info.Text = Convert.ToString(reader("EQS_bHRESTRICTIONS"))
            divHth_Info.InnerText = Convert.ToString(reader("EQS_HRESTRICTIONS"))

            ltLS_Ther.Text = Convert.ToString(reader("EQS_bTHERAPHY"))
            divLS_Ther.InnerText = Convert.ToString(reader("EQS_THERAPHY"))

            ltSEN.Text = Convert.ToString(reader("EQS_bSEN"))
            divSEN.InnerText = Convert.ToString(reader("EQS_SEN"))

            ltEAL.Text = Convert.ToString(reader("EQS_bEAL"))
            divEAL.InnerText = Convert.ToString(reader("EQS_EAL"))

            ltSEA.Text = Convert.ToString(reader("EQS_bENRICH"))
            divSEA.InnerText = Convert.ToString(reader("EQS_ENRICH"))

            ltMus_Prof.Text = Convert.ToString(reader("EQS_bMUSICAL"))
            divMus_Prof.InnerText = Convert.ToString(reader("EQS_MUSICAL"))

            ltSport.Text = Convert.ToString(reader("EQS_bSPORTS"))
            divSport.InnerText = Convert.ToString(reader("EQS_SPORTS"))
            ltPrev_sch.Text = Convert.ToString(reader("EQS_bBEHAVIOUR"))
            divPrev_sch.InnerText = Convert.ToString(reader("EQS_BEHAVIOUR"))

            ltProEng_R.Text = Convert.ToString(reader("EQS_ENG_READING"))
            ltProEng_W.Text = Convert.ToString(reader("EQS_ENG_WRITING"))
            ltProEng_S.Text = Convert.ToString(reader("EQS_ENG_SPEAKING"))













            ltAppNo.Text = Convert.ToString(reader("EQS_APPLNO"))
            ltAcdYear.Text = Convert.ToString(reader("ACY_DECR"))
            ltGrade.Text = Convert.ToString(reader("GRD_DESCR"))
            ltShift.Text = Convert.ToString(reader("SHF_DESCR"))
            ltStream.Text = Convert.ToString(reader("STM_DESCR"))
            ltCurr.Text = Convert.ToString(reader("CLM_DESCR"))
            ltTenJoinDate.Text = DateFormat(Convert.ToDateTime(reader("EQS_DOJ")))
            ltAppNo.Text = Convert.ToString(reader("EQS_APPLNO"))

            If Convert.ToBoolean(reader("EQS_bAPPLSIBLING")) = True Then
                trSib_1.Visible = True
                trSib_2.Visible = True

                ltSib.Text = "Yes"
                ltSibName.Text = Convert.ToString(reader("SIBNAME"))
                ltSibFeeID.Text = Convert.ToString(reader("SIBLINGFEEID"))
                ltSibGEMS.Text = Convert.ToString(reader("SIBBSU"))
            Else
                trSib_1.Visible = False
                trSib_2.Visible = False
                ltSib.Text = "No"
                ltSibName.Text = ""
                ltSibFeeID.Text = ""
                ltSibGEMS.Text = ""
            End If

            If Convert.ToBoolean(reader("EQS_bTPTREQD")) = True Then
                trTranSch_1.Visible = True
                trTranSch_2.Visible = True
                trTranSch_3.Visible = True

                ltMain_Loc.Text = Convert.ToString(reader("LOC_DESCR"))
                ltSub_Loc.Text = Convert.ToString(reader("SBL_DESCR"))
                ltPick_Up.Text = Convert.ToString(reader("PNT_DESCR"))
                ltTransOpt.Text = Convert.ToString(reader("EQS_TPTREMARKS"))

            Else
                trTranSch_1.Visible = False
                trTranSch_2.Visible = False
                trTranSch_3.Visible = False
                ltMain_Loc.Text = ""
                ltSub_Loc.Text = ""
                ltPick_Up.Text = ""
                ltTransOpt.Text = ""

            End If





        End While
        reader.Close()

        ltAcdYear.Text = HiddenAcademicyear.Value
        ''Contact Details

        hideGuardian_info(ltGuardian_name.Text)
       

       
    End Sub
    Sub bind_qus()
        Dim conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@ENQID", hfENQ_ID.Value)
       
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETENQUIRY_QUS_PRINT", param)
        rptQus.DataSource = ds
        rptQus.DataBind()
    End Sub
    Sub hideGuardian_info(ByVal Guard_name As String)

        If Guard_name.Trim = "" Then

            trGr1.Visible = False
            trGr2.Visible = False
            trGr3.Visible = False
            trGr4.Visible = False
            trGr5.Visible = False
            trGr6.Visible = False
            trGr7.Visible = False
            trGr8.Visible = False
            trGr9.Visible = False
            trGr10.Visible = False
            trGr11.Visible = False
            trGr12.Visible = False
            trGr13.Visible = False
            trGr14.Visible = False
            trGr15.Visible = False
            trGr16.Visible = False
          
        End If
    End Sub

    Function DateFormat(ByVal fDate As DateTime) As String
        Return Format(fDate, "dd/MMM/yyyy").Replace("01/Jan/1900", "")
    End Function

    Private Sub PopulatePrevData()
        Try


            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = "exec GetPrev_school_print '" + hfENQ_ID.Value + "','" + Session("sbsuid") + "'"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                gvSchool.DataSource = ds
                gvSchool.DataBind()
            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvSchool.DataSource = ds.Tables(0)
                Try
                    gvSchool.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvSchool.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvSchool.Rows(0).Cells.Clear()
                gvSchool.Rows(0).Cells.Add(New TableCell)
                gvSchool.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSchool.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSchool.Rows(0).Cells(0).Text = "Record not available "



            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "PopulatePrevData")

        End Try
    End Sub
End Class
