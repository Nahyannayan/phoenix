<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="studGradeStage_Edit.aspx.vb" Inherits="Students_studGradeStage_Edit" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function moveOptionUp() {
            var obj = document.getElementById("<%=lstRequired.ClientId%>")
            if (!hasOptions(obj)) { return; }
            for (i = 0; i < obj.options.length; i++) {
                if (obj.options[i].selected) {
                    if (i != 0 && !obj.options[i - 1].selected) {
                        swapOptions(obj, i, i - 1);
                        obj.options[i - 1].selected = true;
                    }
                }
            }
            persistReqOptionsList();
        }
        function hasOptions(obj) {
            if (obj != null && obj.options != null) { return true; }
            return false;
        }


        function moveOptionDown() {
            var obj = document.getElementById("<%=lstRequired.ClientId%>")
            if (!hasOptions(obj)) { return; }
            for (i = obj.options.length - 1; i >= 0; i--) {
                if (obj.options[i].selected) {
                    if (i != (obj.options.length - 1) && !obj.options[i + 1].selected) {
                        swapOptions(obj, i, i + 1);
                        obj.options[i + 1].selected = true;
                    }
                }
            }
            persistReqOptionsList();
        }


        function removeSelectedOptions() {
            var from = document.getElementById("<%=lstRequired.ClientId%>")

    if (!hasOptions(from)) { return; }
    if (from.type == "select-one") {
        from.options[from.selectedIndex] = null;
    }
    else {
        for (var i = (from.options.length - 1) ; i >= 0; i--) {
            var o = from.options[i];
            if (o.selected) {
                from.options[i] = null;
            }
        }
    }
    from.selectedIndex = -1;
    persistReqOptionsList();
}


function RemoveOption() {

}
function swapOptions(obj, i, j) {
    var o = obj.options;
    var i_selected = o[i].selected;
    var j_selected = o[j].selected;
    var temp = new Option(o[i].text, o[i].value, o[i].defaultSelected, o[i].selected);
    var temp2 = new Option(o[j].text, o[j].value, o[j].defaultSelected, o[j].selected);
    o[i] = temp2;
    o[j] = temp;
    o[i].selected = j_selected;
    o[j].selected = i_selected;
}


//to retain values in the trips listbox after postback the list items are stored in an input box	
function persistReqOptionsList() {
    var listBox1 = document.getElementById("<%=lstRequired.ClientId%>");
    var optionsList = '';
    if (listBox1.options.length > 0) {
        for (var i = 0; i < listBox1.options.length; i++) {
            var optionText = listBox1.options[i].text;
            var optionValue = listBox1.options[i].value;

            if (optionsList.length > 0)
                optionsList += '|';
            optionsList += optionText + "$" + optionValue;
        }
    }
    document.getElementById("<%=lstValuesReq.ClientId%>").value = optionsList;
    return false;
}


function persistNotReqOptionsList() {
    var listBox1 = document.getElementById("<%=lstNotRequired.ClientId%>");
    var optionsList = '';
    if (listBox1.options.length > 0) {
        for (var i = 0; i < listBox1.options.length; i++) {
            var optionText = listBox1.options[i].text;
            var optionValue = listBox1.options[i].value;

            if (optionsList.length > 0)
                optionsList += '|';
            optionsList += optionText + "$" + optionValue;
        }
    }
    document.getElementById("<%=lstValuesNotReq.ClientId%>").value = optionsList;
    return false;
}

function moveSelectedLeftToRight() {
    var from = document.getElementById("<%=lstNotRequired.ClientId%>");
     var to = document.getElementById("<%=lstRequired.ClientId%>");
     if (!hasOptions(from)) { return; }
     for (var i = 0; i < from.options.length; i++) {
         var o = from.options[i];
         if (o.selected) {
             if (!hasOptions(to)) { var index = 0; } else { var index = to.options.length; }
             to.options[index] = new Option(o.text, o.value, false, false);
         }
     }
     // Delete them from original
     for (var i = (from.options.length - 1) ; i >= 0; i--) {
         var o = from.options[i];
         if (o.selected) {
             from.options[i] = null;
         }
     }
     persistReqOptionsList();
     persistNotReqOptionsList();
 }
 function moveSelectedRighttoLeft() {
     var to = document.getElementById("<%=lstNotRequired.ClientId%>");
    var from = document.getElementById("<%=lstRequired.ClientId%>");
    if (!hasOptions(from)) { return; }
    for (var i = 0; i < from.options.length; i++) {
        var o = from.options[i];
        if (o.selected) {
            if (!hasOptions(to)) { var index = 0; } else { var index = to.options.length; }
            to.options[index] = new Option(o.text, o.value, false, false);
        }
    }
    // Delete them from original
    for (var i = (from.options.length - 1) ; i >= 0; i--) {
        var o = from.options[i];
        if (o.selected) {
            from.options[i] = null;
        }
    }
    persistReqOptionsList();
    persistNotReqOptionsList();
}


    </script>




    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label4" runat="server" Text="Stage Setup"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <%--    <tr class="subheader_img">
          <td align="left" colspan="12" style="height: 16px" valign="middle" >
         <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
          Stage Setup</span></font></td>
         </tr>--%>

                                <tr>
                                    <td align="left" width="10%">
                                        <asp:Label ID="Label1" runat="server" Text="Academic Year" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="25%">
                                        <asp:Label ID="lblAcademicYear" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="10%">
                                        <asp:Label ID="Label2" runat="server" Text="Grade" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="25%">
                                        <asp:Label ID="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" width="10%">
                                        <asp:Label ID="Label3" runat="server" Text="Stream" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblStream" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>


                         <%--       <tr>
                                    <td colspan="6" class="title-bg">Details</td>
                                </tr>--%>



                                <tr>
                                    <td align="center" colspan="6">

                                        <table id="Table4" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                            <tr>


                                                <td align="left">
                                                    <table id="Table3" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="title-bg" align="left"  colspan="2">Stages Not Required</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:ListBox ID="lstNotRequired" runat="server" 
                                                                    SelectionMode="Multiple"   Width="192px"></asp:ListBox></td>

                                                            <td>
                                                    <input id="btnRight" type="button" value=">>" onclick="moveSelectedLeftToRight()" class="button"  /><br />
                                                    <br />
                                                    <input id="btnLeft" type="button" value="<<" onclick="moveSelectedRighttoLeft()" class="button"  /><br />

                                                </td> 
                                                        </tr>
                                                         
                                                         
                                                    </table>
                                                </td>
                                               
                                                <td align="left">
                                                    <table id="Table5" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr >
                                                            <td class="title-bg" align="left" colspan="2">
                                                                Stages Required</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:ListBox ID="lstRequired" runat="server"  SelectionMode="Multiple"                                                                    
                                                                    Width="188px"></asp:ListBox></td>

                                                             <td  >
                                                  <input id="btnUp" type="button" value="  Up   " onclick="moveOptionUp()" class="button"   /><br />
                                                    <br />
                                                    <input id="btnDown" type="button" value="Down" onclick="moveOptionDown()" class="button"  /><br />
                                                   
                                                </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                               

                                            </tr>

                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6">

                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" />
                                    </td>
                                </tr>
                            </table>

                            <input id="lstValuesReq" name="lstValues" runat="server" type="hidden" style="left: 274px; width: 74px; position: absolute; top: 161px; height: 10px" />
                            <input id="lstValuesNotReq" name="lstValuesNotReq" runat="server" type="hidden" style="left: 274px; width: 74px; position: absolute; top: 161px; height: 10px" />

                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                            <asp:HiddenField ID="hfSTM_ID" runat="server" />

                        </td>
                    </tr>

                </table>




            </div>
        </div>
    </div>
</asp:Content>

