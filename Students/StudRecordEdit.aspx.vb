Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class StudRecordEdit
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
    Protected Sub mnuMaster_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs)

        Try
            mvMaster.ActiveViewIndex = Int32.Parse(e.Item.Value)

            Dim i As Integer = Int32.Parse(e.Item.Value)

            'mnuMaster.Items(0).ImageUrl = "~/Images/Tab_studEdit/btnMain.jpg"
            'mnuMaster.Items(1).ImageUrl = "~/Images/Tab_studEdit/btnPassport.jpg"
            'mnuMaster.Items(2).ImageUrl = "~/Images/Tab_studEdit/btnParent.jpg"
            'mnuMaster.Items(3).ImageUrl = "~/Images/Tab_studEdit/btnPrevSchool.jpg"
            'mnuMaster.Items(4).ImageUrl = "~/Images/Tab_studEdit/btnTransport.jpg"
            'mnuMaster.Items(5).ImageUrl = "~/Images/Tab_studEdit/btnHealth.jpg"


            Select Case i
                Case 0
                    'mnuMaster.Items(0).ImageUrl = "~/Images/Tab_studEdit/btnMain2.jpg"

                Case 1
                    'mnuMaster.Items(1).ImageUrl = "~/Images/Tab_studEdit/btnPassport2.jpg"

                Case 2
                    'mnuMaster.Items(2).ImageUrl = "~/Images/Tab_studEdit/btnParent2.jpg"

                Case 3
                    'mnuMaster.Items(3).ImageUrl = "~/Images/Tab_studEdit/btnPrevSchool2.jpg"

                Case 4
                    'mnuMaster.Items(4).ImageUrl = "~/Images/Tab_studEdit/btnTransport2.jpg"

                Case 5
                    'mnuMaster.Items(5).ImageUrl = "~/Images/Tab_studEdit/btnHealth2.jpg"

            End Select
            Call menuShow()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "mnuMaster_MenuItemClick")
        End Try
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    'Protected Sub FeeSponsor_Update(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If e.ToString = "1" Then
    '        mpxFeeSponsor.Show()
    '    Else
    '        mpxFeeSponsor.Hide()
    '    End If
    'End Sub
    Protected Sub student_show(ByVal an As Integer)
        If an = 1 Then
            mpxFeeSponsor.Show()
        Else
            mpxFeeSponsor.Hide()
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.MaintainScrollPositionOnPostBack = True
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        'AddHandler FeeSponsor1.BtnClick, AddressOf FeeSponsor_Update
        AddHandler FeeSponsor1.student_showpop, AddressOf student_show
        UpLoadPhoto()
        ''added by nahyan on 7thDec2014
        UpLoadFatherPhoto()
        UpLoadMotherPhoto()
        UpLoadGuardianPhoto()
        'If Not Session("Flag_SPONSOR") Is Nothing Then
        '    If Session("Flag_SPONSOR") = "1" Then
        '        ModalPopupExtender1.Show()
        '    Else
        '        ModalPopupExtender1.Hide()
        '    End If

        'End If

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100015" And ViewState("MainMnu_code") <> "S100190" And ViewState("MainMnu_code") <> "S100450") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("datamode") = "edit"
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'txtAcadYear.Attributes.Add("readonly", "readonly")
                    'txtCurr.Attributes.Add("readonly", "readonly")


                    FUUploadEmpPhoto.Attributes.Add("onblur", "javascript:UploadPhoto();")
                    FUUploadFatherPhoto.Attributes.Add("onblur", "javascript:UploadFatherPhoto();")
                    FUUploadMotherPhoto.Attributes.Add("onblur", "javascript:UploadMotherPhoto();")
                    FLUGuradian.Attributes.Add("onblur", "javascript:UploadGuardianPhoto();")
                    Call control_Disable()
                    'get the menucode to confirm the user is accessing the valid page


                    mvMaster.ActiveViewIndex = 0
                    'mnuMaster.Items(0).ImageUrl = "~/Images/Tab_studEdit/btnMain2.jpg"

                    chkbFGEMSEMP.Attributes.Add("onclick", "Chk_Enable_F(this)")
                    chkbMGEMSEMP.Attributes.Add("onclick", "Chk_Enable_M(this)")


                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    '  Call call_Function()
                    Call BindEmirate_info(ddlFCOMEmirate)
                    Call BindEmirate_info(ddlMCOMEmirate)
                    Call BindEmirate_info(ddlGCOMEmirate)
                    Call bindLanguage_dropDown()
                    Call call_Function()
                    Call Student_M_Details(ViewState("viewid"))
                    check_Visibility()
                    Call HidePrevious_school(hfGRD_ID.Value)
                    Call HideBSU_CustomFlds(Session("sBSUID"))
                    Session("Fee_SLIB_ID") = h_SliblingID.Value
                    Session("TYPE_FEE_SPON") = "S"
                    Call Student_D_Details(h_SliblingID.Value)
                    Call Student_Transport()

                    Call BindServices()

                    btnCancel.Text = "Back"

                    rdFather_CheckedChanged(Nothing, Nothing)
                    rdMother_CheckedChanged(Nothing, Nothing)
                    rdGuardian_CheckedChanged(Nothing, Nothing)

                    If ViewState("MainMnu_code") = "S100190" Or ViewState("MainMnu_code") = "S100450" Then
                        mnuMaster.Items(1).Enabled = False
                        mnuMaster.Items(2).Enabled = False
                        mnuMaster.Items(4).Enabled = False
                        mnuMaster.Items(5).Enabled = False
                    Else
                        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
                        Dim sqlqry = "select * from users_m join employee_m on emp_id=usr_emp_id where USR_NAME='" & Session("sUsr_name") & "' and emp_des_id in(298,340,315)"
                        Using reader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sqlqry)
                            If reader.HasRows = True Then
                                Session("tabsuperuser_edit") = True
                            Else

                                Session("tabsuperuser_edit") = False
                            End If

                        End Using

                        If Session("sBusper") = True Then
                            Session("tabsuperuser_edit") = True
                        End If


                        Dim MNu_code As String = Session("MainMnu_code_editstud")
                        Dim RoleID As String = String.Empty
                        Using reader_Rol_ID As SqlDataReader = AccessRoleUser.GetRoleID_user(Session("sUsr_name"))
                            If reader_Rol_ID.HasRows = True Then
                                While reader_Rol_ID.Read()
                                    RoleID = Convert.ToString(reader_Rol_ID("USR_ROL_ID"))
                                End While
                            End If
                        End Using
                        Dim T_code As String = String.Empty
                        Dim T_right As String = String.Empty
                        Dim ht_tab As New Hashtable()
                        Using readertab_Access As SqlDataReader = AccessRoleUser.GetTabRights(RoleID, Session("sBsuid"), ViewState("MainMnu_code"))
                            If readertab_Access.HasRows = True Then
                                While readertab_Access.Read()
                                    T_code = CInt(Convert.ToString(readertab_Access("TAB_CODE"))) - 40
                                    T_right = Convert.ToString(readertab_Access("TAR_RIGHT"))
                                    ht_tab.Add(T_code, T_right)
                                End While
                            End If
                        End Using
                        Session("tab_Right_User_edit") = ht_tab
                        Call menuShow()





                    End If
                End If
                UtilityObj.beforeLoopingControls(Me.Page)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Sub HidePrevious_school(ByVal GRD_ID As String)
        If GRD_ID = "KG1" Or GRD_ID = "PK" Then
            trSch_Add.Visible = False
            trSch_City.Visible = False
            trSch_FRMDT.Visible = False
            trSch_Head.Visible = False
            trSch_Lang.Visible = False
            trSch_Ph.Visible = False
            trSch_name.Visible = True
            trSch_ID.Visible = True
        Else
            trSch_Add.Visible = True
            trSch_City.Visible = True
            trSch_FRMDT.Visible = True
            trSch_Head.Visible = True
            trSch_Lang.Visible = True
            trSch_Ph.Visible = True
            trSch_name.Visible = True
            trSch_ID.Visible = True
        End If

    End Sub
    Sub HideBSU_CustomFlds(ByVal BSU_ID As String)
        If BSU_ID = "800017" Then
            tr_MinInfo.Visible = True
            trGFatherName.Visible = True
            trNatNo.Visible = True
            tr_DOB_POB_Arabic.Visible = True
            tr_ParentName_Arabic.Visible = True
            tr_ParentAddress_Arabic.Visible = True
            tr_ParentOcc_Arabic.Visible = True
            tr_BC_DETAILS.Visible = True
        Else
            tr_MinInfo.Visible = False
            trGFatherName.Visible = False
            trNatNo.Visible = False
            tr_DOB_POB_Arabic.Visible = False
            tr_ParentName_Arabic.Visible = False
            tr_ParentAddress_Arabic.Visible = False
            tr_ParentOcc_Arabic.Visible = False
            tr_BC_DETAILS.Visible = False
        End If

    End Sub

    Private Sub check_Visibility()
        Dim i As Integer = 0
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select isnull(EQV_STAR_ID,'') EQV_STAR_ID,isnull(EQV_CONTROL_ID,'') EQV_CONTROL_ID,ISNULL(EVD_bVISIBLE,'FALSE')EVD_bVISIBLE," _
                                      & " ISNULL(EQV_VALID_CONTROL,'')EQV_VALID_CONTROL,ISNULL(EVD_bVALIDATE,'FALSE')EVD_bVALIDATE  from ONLINE_ENQ.ENQUIRY_VISIBLE_M " _
                                      & " INNER JOIN ONLINE_ENQ.ENQUIRY_VISIBLE_D ON EQV_ID=EVD_EQV_ID " _
                                      & " WHERE EVD_BSU_ID='" + Session("sBSUID") + "'and EQV_CONTROL_ID='StudRecordEdit'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        If ds.Tables(0).Rows.Count >= 1 Then

            While i < ds.Tables(0).Rows.Count

                Dim S As String = ds.Tables(0).Rows(i).Item("EQV_STAR_ID")
                Dim V As String = ds.Tables(0).Rows(i).Item("EQV_VALID_CONTROL")
                If S <> "" Then
                    Dim ctrl As Control = FindControl(S)
                    ctrl.Visible = ds.Tables(0).Rows(i).Item("EVD_bVISIBLE")
                End If
                If V <> "" Then
                    Dim VALID_ctrl As Control = FindControl(V)
                    VALID_ctrl.Visible = ds.Tables(0).Rows(i).Item("EVD_bVALIDATE")
                End If

                i += 1
            End While
        End If




    End Sub

    Sub menuShow()
        If Session("tabsuperuser_edit") = False Then
            Dim i As Integer
            Dim FirstFlag As Boolean = True
            '0 to 6 is mnuMaster
            For i = 0 To mnuMaster.Items.Count - 1
                Dim str1 As String = i
                If Session("tab_Right_User_edit").ContainsKey(str1) Then
                    If FirstFlag And (Session("tab_Right_User_edit").Item(str1) = "1" Or Session("tab_Right_User_edit").Item(str1) = "2") Then
                        'mvMaster.ActiveViewIndex = i
                        FirstFlag = False
                    End If
                    If Session("tab_Right_User_edit").Item(str1) = "1" Then
                        mnuMaster.Items(i).Selectable = True
                        SetPanelEnabled(i, False)
                    ElseIf Session("tab_Right_User_edit").Item(str1) = "2" Then
                        mnuMaster.Items(i).Selectable = True
                        SetPanelEnabled(i, True)
                    End If
                Else
                    mnuMaster.Items(i).ImageUrl = ""
                End If
            Next


        End If
    End Sub
    Private Sub SetPanelEnabled(ByVal index As Integer, ByVal enable As Boolean)
        Select Case index
            Case 0
                plMain_Vw.Enabled = enable
            Case 1

                plPassport_Vw.Enabled = enable

            Case 2
                plparent_Vw.Enabled = enable
            Case 3
                plPrevious_Vw.Enabled = enable
            Case 4
                Panel1.Enabled = enable
            Case 5
                plHealth_vw.Enabled = enable
        End Select
    End Sub
    Sub call_Function()
        Call GetReligion_info()
        Call GetCountry_info()
        Call GetNational_info()
        'Call GetEmirate_info()
        Call GetBusinessUnits_info_staff()
        Call getColor_House()
        Call getTransfer()
        Call GetCURRICULUM_M()
        Call GetGrade_Display_BSU()
        Call GetCompany_Name()

    End Sub

    Private Sub BindEmirate_info(ByVal ddlEmirate As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M order by EMR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlEmirate.DataSource = ds
        ddlEmirate.DataTextField = "EMR_DESCR"
        ddlEmirate.DataValueField = "EMR_CODE"
        ddlEmirate.DataBind()
    End Sub
    Private Sub bindLanguage_dropDown()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT LNG_ID,LNG_DESCR  FROM LANGUAGE_M order by LNG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlFLang.DataSource = ds
        ddlFLang.DataTextField = "LNG_DESCR"
        ddlFLang.DataValueField = "LNG_ID"
        ddlFLang.DataBind()
        chkOLang.DataSource = ds
        chkOLang.DataTextField = "LNG_DESCR"
        chkOLang.DataValueField = "LNG_ID"
        chkOLang.DataBind()
        ddlFLang.Items.Add(New ListItem("", ""))
        ddlFLang.ClearSelection()
        ddlFLang.Items.FindByText("").Selected = True
    End Sub

    Sub Student_D_Details(ByVal Slib_ID As String)

        Try


            'Dim temp_FNation1, temp_FNation2, temp_FComCountry, temp_FPRM_Country, temp_F_BSU_ID, temp_MNationality1, temp_MNationality2, temp_MCOMCountry As String
            'Dim temp_MPRMCOUNTRY, temp_GPRMCOUNTRY, temp_PSchool_Country1, temp_M_BSU_ID, temp_GCOMCountry, temp_GNationality1, temp_GNationality2 As String
            'Dim temp_PBoard_Attend1, temp_PSchool_Year1, temp_PSchool_Country2, temp_PBoard_Attend2, temp_PSchool_Year2, temp_PSchool_Country3, temp_PBoard_Attend3, temp_PSchool_Year3 As String
            Dim temp_FOFFPhone, temp_FRESPhone, temp_FFax, temp_FMobile, temp_FPRMPHONE, temp_MFAX, temp_MOFFPHONE, temp_MRESPHONE, temp_MMOBILE, temp_MPRMPHONE, temp_GOFFPHONE, temp_GResPhone, temp_GFax, temp_GMOBILE, temp_GPRMPHONE As String
            'Dim temp_bFGEMSEMP, temp_bMGEMSEMP As Boolean

            Dim arInfo As String() = New String(2) {}
            Dim splitter As Char = "-"
            Dim STR As String = "EXEC DBO.GETSTUDENT_D_DETAILS " + Slib_ID + ""
            Dim CONN As String = ConnectionManger.GetOASISConnectionString

            Using readerStudent_D_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.Text, STR)
                If readerStudent_D_Detail.HasRows = True Then
                    While readerStudent_D_Detail.Read

                        'handle the null value returned from the reader incase  convert.tostring
                        txtPAddr_Arabic.Text = Convert.ToString(readerStudent_D_Detail("STS_PADDR_ARABIC"))
                        txtPOcc_Arabic.Text = Convert.ToString(readerStudent_D_Detail("STS_POCC_ARABIC"))
                        txtPName_Arabic.Text = Convert.ToString(readerStudent_D_Detail("STS_PNAME_ARABIC"))
                        txtFFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_FFIRSTNAME"))
                        txtFMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_FMIDNAME"))
                        txtFLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_FLASTNAME"))

                        'txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR1"))
                        'txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR2"))

                        txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTREET"))

                        txtFBldg.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMBLDG"))
                        txtFAptNo.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAPARTNO"))


                        txtFCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPOBOX"))


                        'txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE"))
                        txtFPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMADDR1"))
                        txtFPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMADDR2"))
                        txtFPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMPOBOX"))
                        'temp_Nationality = Convert.ToString(readerStudent_D_Detail("SNATIONALITY"))
                        txtFPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMCITY"))
                        txtFOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_FOCC"))

                        If Not ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))) Is Nothing Then
                            ddlFCOMEmirate.ClearSelection()
                            ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))).Selected = True
                        Else
                            ddlFCOMEmirate.ClearSelection()
                            ddlFCOMEmirate.Items.FindByValue(Session("BSU_CITY")).Selected = True
                        End If


                        'code added by dhanya 29 apr 08
                        txtFEmail.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMAIL"))
                        txtFComp_Add.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY_ADDR"))


                        txtMFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_MFIRSTNAME"))
                        txtMMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_MMIDNAME"))
                        txtMLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_MLASTNAME"))


                        'txtMCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMADDR1"))
                        'txtMCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMADDR2"))

                        txtMCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMSTREET"))

                        txtMBldg.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMBLDG"))
                        txtMAptNo.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAPARTNO"))

                        txtMCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPOBOX"))

                        'txtMCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE"))
                        txtMPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMADDR1"))
                        txtMPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMADDR2"))
                        txtMPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMPOBOX"))
                        txtMPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMCITY"))
                        txtMOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_MOCC"))

                        If Not ddlMCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))) Is Nothing Then
                            ddlMCOMEmirate.ClearSelection()
                            ddlMCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))).Selected = True
                        Else
                            ddlMCOMEmirate.ClearSelection()
                            ddlMCOMEmirate.Items.FindByValue(Session("BSU_CITY")).Selected = True
                        End If


                        'code aaded by dhanya 29 apr 08
                        txtMEmail.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMAIL"))
                        txtMComp_Add.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY_ADDR"))

                        txtGFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_GFIRSTNAME"))
                        txtGMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_GMIDNAME"))
                        txtGLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_GLASTNAME"))

                        'txtGCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMADDR1"))
                        'txtGCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMADDR2"))

                        txtGCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMSTREET"))

                        txtGBldg.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMBLDG"))
                        txtGAptNo.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAPARTNO"))

                        txtGCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPOBOX"))

                        'txtGCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE"))
                        txtGPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMADDR1"))
                        txtGPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMADDR2"))
                        txtGPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMPOBOX"))
                        txtGPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMCITY"))
                        txtGOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_GOCC"))

                        If Not ddlGCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))) Is Nothing Then
                            ddlGCOMEmirate.ClearSelection()
                            ddlGCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))).Selected = True
                        Else
                            ddlGCOMEmirate.ClearSelection()
                            ddlGCOMEmirate.Items.FindByValue(Session("BSU_CITY")).Selected = True
                        End If


                        'code added by dhanya 29 apr 08
                        txtGEmail.Text = Convert.ToString(readerStudent_D_Detail("STS_GEMAIL"))
                        txtGComp_Add.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY_ADDR"))


                        'code commented by dhanya 13 jul 08--moving columns to student_m
                        'txtPSchool_Name1.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHI"))
                        'txtPSchool_City1.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHI_CITY"))
                        'txtPSchool_Name2.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHII"))
                        'txtPSchool_City2.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHII_CITY"))
                        'txtPSchool_Name3.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHIII"))
                        'txtPSchool_City3.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHIII_CITY"))
                        ''Viewstate settings


                        If Not ddlFNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))) Is Nothing Then
                            ddlFNationality1.ClearSelection()
                            ddlFNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))).Selected = True
                        End If
                        If Not ddlFNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2"))) Is Nothing Then
                            ddlFNationality2.ClearSelection()
                            ddlFNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2"))).Selected = True
                        End If
                        If Not ddlMNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))) Is Nothing Then
                            ddlMNationality1.ClearSelection()
                            ddlMNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))).Selected = True
                        End If
                        If Not ddlMNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))) Is Nothing Then
                            ddlMNationality2.ClearSelection()
                            ddlMNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))).Selected = True
                        End If

                        If Not ddlGNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))) Is Nothing Then
                            ddlGNationality1.ClearSelection()
                            ddlGNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))).Selected = True
                        End If
                        If Not ddlGNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))) Is Nothing Then
                            ddlGNationality2.ClearSelection()
                            ddlGNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))).Selected = True
                        End If

                        If Not ddlF_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))) Is Nothing Then
                            ddlF_BSU_ID.ClearSelection()
                            ddlF_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))).Selected = True
                        End If
                        If Not ddlM_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))) Is Nothing Then
                            ddlM_BSU_ID.ClearSelection()
                            ddlM_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))).Selected = True
                        End If


                        'keep loop until you get the Country to Not Available into  the SelectedIndex

                        If Not ddlF_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID"))) Is Nothing Then
                            ddlF_BSU_ID.ClearSelection()
                            ddlF_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID"))).Selected = True
                        End If
                        If Not ddlM_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID"))) Is Nothing Then
                            ddlM_BSU_ID.ClearSelection()
                            ddlM_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID"))).Selected = True
                        End If




                        txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA"))
                        txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMCITY"))
                        If Not ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))) Is Nothing Then
                            ddlFCOMCountry.ClearSelection()
                            ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))).Selected = True
                            ddlFCOMCountry_SelectedIndexChanged(ddlFCOMCountry, Nothing)
                        End If


                        If Not ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE_ID"))) Is Nothing Then
                            ddlFCity.ClearSelection()
                            ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE_ID"))).Selected = True
                            ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

                        End If



                        If Not ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))) Is Nothing Then
                            ddlFArea.ClearSelection()
                            ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))).Selected = True
                        End If



                        txtMCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA"))
                        txtMCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMCITY"))
                        If Not ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))) Is Nothing Then
                            ddlMCOMCountry.ClearSelection()
                            ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))).Selected = True
                            ddlMCOMCountry_SelectedIndexChanged(ddlMCOMCountry, Nothing)
                        End If

                        If Not ddlMCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE_ID"))) Is Nothing Then
                            ddlMCity.ClearSelection()
                            ddlMCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE_ID"))).Selected = True
                            ddlMCity_SelectedIndexChanged(ddlMCity, Nothing)
                        End If

                        If Not ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))) Is Nothing Then
                            ddlMArea.ClearSelection()
                            ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))).Selected = True
                        End If


                        txtGCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA"))
                        txtGCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMCITY"))
                        If Not ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))) Is Nothing Then
                            ddlGCOMCountry.ClearSelection()
                            ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))).Selected = True
                            ddlGCOMCountry_SelectedIndexChanged(ddlGCOMCountry, Nothing)
                        End If


                        If Not ddlGCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE_ID"))) Is Nothing Then
                            ddlGCity.ClearSelection()
                            ddlGCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE_ID"))).Selected = True
                            ddlGCity_SelectedIndexChanged(ddlGCity, Nothing)
                        End If


                        If Not ddlGArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA_ID"))) Is Nothing Then
                            ddlGArea.ClearSelection()
                            ddlGArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA_ID"))).Selected = True
                        End If



                        If Not ddlFPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))) Is Nothing Then
                            ddlFPRM_Country.ClearSelection()
                            ddlFPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))).Selected = True
                        End If


                        If Not ddlMPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))) Is Nothing Then
                            ddlMPRM_Country.ClearSelection()
                            ddlMPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))).Selected = True
                        End If


                        If Not ddlGPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))) Is Nothing Then
                            ddlGPRM_Country.ClearSelection()
                            ddlGPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))).Selected = True
                        End If

                        If Not ddlFCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))) Is Nothing Then
                            ddlFCompany_Name.ClearSelection()
                            ddlFCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))).Selected = True
                        End If
                        If Not ddlMCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))) Is Nothing Then
                            ddlMCompany_Name.ClearSelection()
                            ddlMCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))).Selected = True
                        End If

                        If Not ddlGCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_G_COMP_ID"))) Is Nothing Then
                            ddlGCompany_Name.ClearSelection()
                            ddlGCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_G_COMP_ID"))).Selected = True
                        End If


                        txtFComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY"))
                        txtMComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY"))
                        txtGComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY"))

                        'radio button setting

                        chkbFGEMSEMP.Checked = Convert.ToBoolean(readerStudent_D_Detail("STS_bFGEMSEMP"))
                        If chkbFGEMSEMP.Checked = False Then
                            ddlF_BSU_ID.Enabled = False
                        End If
                        chkbMGEMSEMP.Checked = Convert.ToBoolean(readerStudent_D_Detail("STS_bMGEMSEMP"))
                        If chkbMGEMSEMP.Checked = False Then
                            ddlM_BSU_ID.Enabled = False
                        End If


                        txtFamily_NOTE.Text = Convert.ToString(readerStudent_D_Detail("STS_FAMILY_NOTE"))


                        'phone settings
                        temp_FOFFPhone = Convert.ToString(readerStudent_D_Detail("STS_FOFFPHONE"))
                        arInfo = temp_FOFFPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFOffPhone_Country.Text = arInfo(0)
                            txtFOffPhone_Area.Text = arInfo(1)
                            txtFOffPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFOffPhone_Area.Text = arInfo(0)
                            txtFOffPhone_No.Text = arInfo(1)

                        Else
                            txtFOffPhone_No.Text = temp_FOFFPhone
                        End If


                        temp_FRESPhone = Convert.ToString(readerStudent_D_Detail("STS_FRESPHONE"))
                        arInfo = temp_FRESPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFResPhone_Country.Text = arInfo(0)
                            txtFResPhone_Area.Text = arInfo(1)
                            txtFResPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFResPhone_Area.Text = arInfo(0)
                            txtFResPhone_No.Text = arInfo(1)

                        Else
                            txtFResPhone_No.Text = temp_FRESPhone
                        End If

                        temp_FFax = Convert.ToString(readerStudent_D_Detail("STS_FFAX"))
                        arInfo = temp_FFax.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFFax_Country.Text = arInfo(0)
                            txtFFax_Area.Text = arInfo(1)
                            txtFFax_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFFax_Area.Text = arInfo(0)
                            txtFFax_No.Text = arInfo(1)
                        Else
                            txtFFax_No.Text = temp_FFax
                        End If
                        temp_FMobile = Convert.ToString(readerStudent_D_Detail("STS_FMOBILE"))
                        arInfo = temp_FMobile.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFMobile_Country.Text = arInfo(0)
                            txtFMobile_Area.Text = arInfo(1)
                            txtFMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFMobile_Area.Text = arInfo(0)
                            txtFMobile_No.Text = arInfo(1)

                        Else
                            txtFMobile_No.Text = temp_FMobile
                        End If

                        temp_FPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_FPRMPHONE"))
                        arInfo = temp_FPRMPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFPPRM_Country.Text = arInfo(0)
                            txtFPPRM_Area.Text = arInfo(1)
                            txtFPPRM_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFPPRM_Area.Text = arInfo(0)
                            txtFPPRM_Area.Text = arInfo(1)

                        Else
                            txtFPPRM_No.Text = temp_FPRMPHONE
                        End If
                        temp_MOFFPHONE = Convert.ToString(readerStudent_D_Detail("STS_MOFFPHONE"))
                        arInfo = temp_MOFFPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMOffPhone_Country.Text = arInfo(0)
                            txtMOffPhone_Area.Text = arInfo(1)
                            txtMOffPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMOffPhone_Area.Text = arInfo(0)
                            txtMOffPhone_No.Text = arInfo(1)

                        Else
                            txtMOffPhone_No.Text = temp_MOFFPHONE
                        End If
                        temp_MRESPHONE = Convert.ToString(readerStudent_D_Detail("STS_MRESPHONE"))
                        arInfo = temp_MRESPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMResPhone_Country.Text = arInfo(0)
                            txtMResPhone_Area.Text = arInfo(1)
                            txtMResPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMResPhone_Area.Text = arInfo(0)
                            txtMResPhone_No.Text = arInfo(1)

                        Else
                            txtMResPhone_No.Text = temp_MRESPHONE
                        End If
                        temp_MFAX = Convert.ToString(readerStudent_D_Detail("STS_MFAX"))
                        arInfo = temp_MFAX.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMFax_Country.Text = arInfo(0)
                            txtMFax_Area.Text = arInfo(1)
                            txtMFax_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMFax_Area.Text = arInfo(0)
                            txtMFax_No.Text = arInfo(1)

                        Else
                            txtMFax_No.Text = temp_MFAX
                        End If

                        temp_MMOBILE = Convert.ToString(readerStudent_D_Detail("STS_MMOBILE"))
                        arInfo = temp_MMOBILE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMMobile_Country.Text = arInfo(0)
                            txtMMobile_Area.Text = arInfo(1)
                            txtMMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMMobile_Area.Text = arInfo(0)
                            txtMMobile_No.Text = arInfo(1)

                        Else
                            txtMMobile_No.Text = temp_MMOBILE
                        End If
                        temp_MPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_MPRMPHONE"))
                        arInfo = temp_MPRMPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMPPermant_Country.Text = arInfo(0)
                            txtMPPermant_Area.Text = arInfo(1)
                            txtMPPermant_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMPPermant_Area.Text = arInfo(0)
                            txtMPPermant_No.Text = arInfo(1)

                        Else
                            txtMPPermant_No.Text = temp_MPRMPHONE
                        End If
                        temp_GOFFPHONE = Convert.ToString(readerStudent_D_Detail("STS_GOFFPHONE"))
                        arInfo = temp_GOFFPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGOffPhone_Country.Text = arInfo(0)
                            txtGOffPhone_Area.Text = arInfo(1)
                            txtGOffPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGOffPhone_Area.Text = arInfo(0)
                            txtGOffPhone_No.Text = arInfo(1)
                        Else
                            txtGOffPhone_No.Text = temp_GOFFPHONE
                        End If
                        temp_GResPhone = Convert.ToString(readerStudent_D_Detail("STS_GRESPHONE"))
                        arInfo = temp_GResPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGResPhone_Country.Text = arInfo(0)
                            txtGResPhone_Area.Text = arInfo(1)
                            txtGResPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGResPhone_Area.Text = arInfo(0)
                            txtGResPhone_No.Text = arInfo(1)
                        Else
                            txtGResPhone_No.Text = temp_GResPhone
                        End If
                        temp_GFax = Convert.ToString(readerStudent_D_Detail("STS_GFAX"))
                        arInfo = temp_GFax.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGFax_Country.Text = arInfo(0)
                            txtGFax_Area.Text = arInfo(1)
                            txtGFax_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGFax_Area.Text = arInfo(0)
                            txtGFax_Area.Text = arInfo(1)
                        Else
                            txtGFax_No.Text = temp_GFax
                        End If
                        temp_GMOBILE = Convert.ToString(readerStudent_D_Detail("STS_GMOBILE"))
                        arInfo = temp_GMOBILE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGMobile_Country.Text = arInfo(0)
                            txtGMobile_Area.Text = arInfo(1)
                            txtGMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGMobile_Area.Text = arInfo(0)
                            txtGMobile_No.Text = arInfo(1)
                        Else
                            txtGMobile_No.Text = temp_GMOBILE
                        End If
                        temp_GPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_GPRMPHONE"))
                        arInfo = temp_GPRMPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGPPERM_Country.Text = arInfo(0)
                            txtGPPERM_Area.Text = arInfo(1)
                            txtGPPERM_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGPPERM_Area.Text = arInfo(0)
                            txtGPPERM_No.Text = arInfo(1)
                        Else
                            txtGPPERM_No.Text = temp_GPRMPHONE
                        End If

                        txtFEMIRATES_ID.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATES_ID"))
                        txtFEMIRATES_ID_EXPDATE.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATES_ID_EXPDATE"))
                        txtMEMIRATES_ID.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATES_ID"))
                        txtMEMIRATES_ID_EXPDATE.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATES_ID_EXPDATE"))
                        txtGEMIRATES_ID.Text = Convert.ToString(readerStudent_D_Detail("STS_GEMIRATES_ID"))
                        txtGEMIRATES_ID_EXPDATE.Text = Convert.ToString(readerStudent_D_Detail("STS_GEMIRATES_ID_EXPDATE"))

                        txtFEmiratesId_ENG.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_NAME"))
                        txtMEmiratesId_ENG.Text = Convert.ToString(readerStudent_D_Detail("STS_mEMIRATESID_NAME"))
                        txtFEmiratesId_ARB.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_ARABIC_NAME"))
                        txtMEmiratesId_ARB.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATESID_ARABIC_NAME"))


                        ''added father image on 7Dec2014 by nahyan

                        Dim FPhotoPath = String.Empty
                        If Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_PHOTO")) <> "" Then

                            FPhotoPath = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_PHOTO"))
                            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                            Dim strImagePath As String = String.Empty
                            If FPhotoPath <> "" Then
                                strImagePath = connPath & FPhotoPath
                                ' Save_Error_Log_Enquiry("EMiratesID get filepath", strImagePath)
                                imgFather.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
                            End If
                        End If

                        ViewState("FATHER_PHOTO_PATH") = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_PHOTO"))
                        ViewState("tempFatherimgpath") = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_PHOTO"))


                        Dim MPhotoPath = String.Empty
                        If Convert.ToString(readerStudent_D_Detail("STS_MEMIRATESID_PHOTO")) <> "" Then

                            MPhotoPath = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATESID_PHOTO"))
                            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                            Dim strImagePath As String = String.Empty
                            If MPhotoPath <> "" Then
                                strImagePath = connPath & MPhotoPath
                                ' Save_Error_Log_Enquiry("EMiratesID get filepath", strImagePath)
                                imgMother.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
                            End If
                        End If

                        ViewState("MOTHER_PHOTO_PATH") = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATESID_PHOTO"))
                        ViewState("tempMotherimgpath") = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATESID_PHOTO"))



                        Dim GPhotoPath = String.Empty
                        If Convert.ToString(readerStudent_D_Detail("STS_GEMIRATESID_PHOTO")) <> "" Then

                            GPhotoPath = Convert.ToString(readerStudent_D_Detail("STS_GEMIRATESID_PHOTO"))
                            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                            Dim strImagePath2 As String = String.Empty
                            If GPhotoPath <> "" Then
                                strImagePath2 = connPath & GPhotoPath
                                ' Save_Error_Log_Enquiry("EMiratesID get filepath", strImagePath)
                                imgGuardian.ImageUrl = strImagePath2 & "?" & DateTime.Now.Ticks.ToString()
                            End If
                        End If

                        ViewState("GUARDIAN_PHOTO_PATH") = Convert.ToString(readerStudent_D_Detail("STS_GEMIRATESID_PHOTO"))
                        ViewState("tempGuardianimgpath") = Convert.ToString(readerStudent_D_Detail("STS_GEMIRATESID_PHOTO"))


                    End While

                Else


                End If


            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "student D")
        End Try

    End Sub

    Sub Student_M_Details(ByVal Stud_No As String)



        Dim temp_Gender, temp_PrimContact As String
        Dim temp_SMS, temp_Mail, temp_bActive As Boolean
        'Dim temp_HOUSE, temp_Blood, temp_Type, temp_Rlg_ID, temp_Nationality, temp_COB, temp_MINLIST, temp_MINLISTTYPE, temp_PrefContact As String
        Dim arInfo As String() = New String(2) {}
        Dim Temp_Phone_Split As String = String.Empty
        Dim splitter As Char = "-"
        'arInfo = info.Split(splitter)
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim STR As String = "exec [dbo].[GETSTUDENT_M_DETAILS] " + ViewState("viewid") + "," + Session("sBsuid") + ""
        Try


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.Text, STR)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        'handle the null value returned from the reader incase  convert.tostring

                        txtSTU_KNOWNNAME.Text = Convert.ToString(readerStudent_Detail("STU_KNOWNNAME"))
                        txtShift_ID.Text = Convert.ToString(readerStudent_Detail("SHF_DESCR"))
                        txtStream.Text = Convert.ToString(readerStudent_Detail("STM_DESCR"))
                        txtJoin_Shift.Text = Convert.ToString(readerStudent_Detail("SHF_DESCR_JOIN"))
                        txtJoin_Stream.Text = Convert.ToString(readerStudent_Detail("STM_DESCR_JOIN"))
                        txtStud_No.Text = Convert.ToString(readerStudent_Detail("STU_NO"))
                        txtACD_ID_Join.Text = Convert.ToString(readerStudent_Detail("ACD_ID_JOIN_Y"))
                        txtACD_ID.Text = Convert.ToString(readerStudent_Detail("ACD_ID_Y"))
                        txtGRD_ID.Text = Convert.ToString(readerStudent_Detail("GRD_ID"))
                        txtGRD_ID_Join.Text = Convert.ToString(readerStudent_Detail("GRD_ID_JOIN"))
                        txtSCT_ID.Text = Convert.ToString(readerStudent_Detail("SCT_ID"))
                        txtSCT_ID_JOIN.Text = Convert.ToString(readerStudent_Detail("SCT_DESCR_JOIN"))
                        txtFname_E.Text = Convert.ToString(readerStudent_Detail("SFIRSTNAME_E"))
                        txtMname_E.Text = Convert.ToString(readerStudent_Detail("SMIDNAME_E"))
                        txtLname_E.Text = Convert.ToString(readerStudent_Detail("SLASTNAME_E"))
                        txtFname_A.Text = Convert.ToString(readerStudent_Detail("SFIRSTNAME_A"))
                        txtMname_A.Text = Convert.ToString(readerStudent_Detail("SMIDNAME_A"))
                        txtLname_A.Text = Convert.ToString(readerStudent_Detail("SLASTNAME_A"))
                        hfGRD_ID.value = Convert.ToString(readerStudent_Detail("STU_GRD_ID"))

                        '***********************************************************************
                        'txtEQM_ENQID.Text = Convert.ToString(readerStudent_Detail("EQM_ENQID"))
                        '***********************************************************************
                        txtEQM_ENQID.Text = Convert.ToString(readerStudent_Detail("EQS_APPLNO"))


                        txtFee_ID.Text = Convert.ToString(readerStudent_Detail("FEE_ID"))
                        txtMOE_No.Text = Convert.ToString(readerStudent_Detail("BLUEID"))
                        txtPob.Text = Convert.ToString(readerStudent_Detail("POB"))
                        txtFee_Spon.Text = Convert.ToString(readerStudent_Detail("SFEESPONSOR"))
                        txtHthOth_Note.Text = Convert.ToString(readerStudent_Detail("SHEALTH"))

                        ' txtPickup_BusNo.Text = Convert.ToString(readerStudent_Detail("PICKUP_BUSNO"))
                        ' txtDropOff_BusNo.Text = Convert.ToString(readerStudent_Detail("DROPOFF_BUSNO"))
                        'PICKUP_BUSNO, STUDENT_M.STU_DROPOFF_BUSNO AS DROPOFF_BUSNO

                        'code commented by dhanya
                        'txtPickup.Text = Convert.ToString(readerStudent_Detail("SPICKUP"))
                        'txtDropoff.Text = Convert.ToString(readerStudent_Detail("SDROPOFF"))

                        txtPassport_Name.Text = Convert.ToString(readerStudent_Detail("SPASPRTNAME"))
                        txtPNo.Text = Convert.ToString(readerStudent_Detail("SPASPRTNO"))
                        txtPIssPlace.Text = Convert.ToString(readerStudent_Detail("SPASPRTISSPLACE"))
                        txtVNo.Text = Convert.ToString(readerStudent_Detail("STU_VISANO"))


                        txtVIssPlace.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSPLACE"))
                        txtVIssAuth.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSAUTH"))

                        txtHCard_No.Text = Convert.ToString(readerStudent_Detail("STU_HCNO"))
                        'CODE ADDDED BY LIJ0 07/JUN/2009
                        txtStud_Comment.Text = Convert.ToString(readerStudent_Detail("STU_COMMENT"))

                        Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
                        Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                        Dim strImagePath As String = String.Empty
                        If strPath <> "" Then
                            strImagePath = connPath & strPath
                            imgEmpImage.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
                        End If

                        'code added by dhanya 28 apr 08
                        ViewState("EMP_IMAGE_PATH") = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))


                        ViewState("temp_PathPhoto") = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
                        txtCurrStatus.Text = Convert.ToString(readerStudent_Detail("S_CURRSTATUS"))


                        h_SliblingID.Value = Convert.ToString(readerStudent_Detail("SSIBLING_ID"))



                        'Setting date
                        If IsDate(readerStudent_Detail("SDOJ")) = True Then
                            txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("SDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("MINDOJ")) = True Then
                            txtMINDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("MINDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("DOB")) = True Then
                            txtDob.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("DOB"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_PASPRTISSDATE")) = True Then
                            txtPIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTISSDATE"))))
                        End If
                        If IsDate(readerStudent_Detail("STU_PASPRTEXPDATE")) = True Then
                            txtPExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTEXPDATE"))))
                        End If

                        '************************************************
                        'If IsDate(readerStudent_Detail("STU_VISAISSDATE")) = True Then
                        '    txtVIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAISSDATE"))))
                        'End If
                        'If IsDate(readerStudent_Detail("STU_VISAEXPDATE")) = True Then
                        '    txtVExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAEXPDATE"))))
                        'End If
                        '************************************************
                        'code modified by dhanya 28 apr 08
                        If IsDate(readerStudent_Detail("STU_VISAISSDATE")) = True Then
                            txtVIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAISSDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_VISAEXPDATE")) = True Then
                            txtVExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAEXPDATE")))).Replace("01/Jan/1900", "")
                        End If


                        'setting radio buttons
                        temp_Gender = Convert.ToString(readerStudent_Detail("GENDER"))
                        If UCase(temp_Gender) = "F" Then
                            rdFemale.Checked = True

                        ElseIf UCase(temp_Gender) = "M" Then
                            rdMale.Checked = True

                        End If

                        temp_PrimContact = Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT"))

                        If UCase(temp_PrimContact) = "F" Then
                            rdFather.Checked = True
                        ElseIf UCase(temp_PrimContact) = "M" Then
                            rdMother.Checked = True
                        Else
                            rdGuardian.Checked = True
                        End If

                        'If IsDBNull(readerStudent_Detail("SbUSETPT")) = False Then

                        '    temp_Trans = Convert.ToBoolean(readerStudent_Detail("SbUSETPT"))
                        '    If temp_Trans Then
                        '        rdUSETPT_YES.Checked = True
                        '    Else
                        '        rdUSETPT_No.Checked = True
                        '    End If
                        'Else
                        '    rdUSETPT_No.Checked = True
                        'End If




                        'temp_bActive = Convert.ToBoolean(readerStudent_Detail("STU_bActive"))
                        'If temp_bActive Then
                        '    rdStu_bActive_Yes.Checked = True
                        'Else
                        '    rdStu_bActive_No.Checked = True
                        'End If
                        'CODE ADDED BY DHANYA 28 APR 08 




                        If Convert.ToBoolean(readerStudent_Detail("STU_bRCVPUBL")) = True Then
                            rbPubYES.Checked = True
                        Else
                            rbPubNo.Checked = True
                        End If





                        If Convert.ToBoolean(readerStudent_Detail("SbRCVSPMEDICATION")) = True Then
                            rbHthSM_Yes.Checked = True
                        Else
                            rbHthSM_No.Checked = True
                        End If
                        txtHthSM_Note.Text = Convert.ToString(readerStudent_Detail("SPMEDICATION"))

                        'txtNotes.Text = Convert.ToString(reader("EQS_REMARKS"))
                        'txtHthLS_Note.Text = Convert.ToString(readerStudent_Detail("STU_REMARKS"))
                        'txtHthPER_Note.Text = Convert.ToString(readerStudent_Detail("SPHYSICAL"))

                        If Convert.ToBoolean(readerStudent_Detail("STU_bHEALTH")) = True Then
                            rbHthOther_yes.Checked = True
                        Else
                            rbHthOther_No.Checked = True
                        End If
                        txtHthOth_Note.Text = Convert.ToString(readerStudent_Detail("SHEALTH"))

                        If Convert.ToBoolean(readerStudent_Detail("STU_bPHYSICAL")) = True Then
                            rbHthPER_Yes.Checked = True
                        Else
                            rbHthPER_No.Checked = True
                        End If
                        txtHthPER_Note.Text = Convert.ToString(readerStudent_Detail("SPHYSICAL"))


                        If Convert.ToBoolean(readerStudent_Detail("STU_bALLERGIES")) = True Then
                            rbHthAll_Yes.Checked = True
                        Else
                            rbHthAll_No.Checked = True
                        End If
                        txtHthAll_Note.Text = Convert.ToString(readerStudent_Detail("STU_ALLERGIES"))

                        If Convert.ToBoolean(readerStudent_Detail("STU_bTHERAPHY")) = True Then
                            rbHthLS_Yes.Checked = True
                        Else
                            rbHthLS_No.Checked = True
                        End If
                        txtHthLS_Note.Text = Convert.ToString(readerStudent_Detail("STU_THERAPHY"))

                        If Convert.ToBoolean(readerStudent_Detail("STU_bSEN")) = True Then
                            rbHthSE_Yes.Checked = True
                        Else
                            rbHthSE_No.Checked = True
                        End If
                        txtHthSE_Note.Text = Convert.ToString(readerStudent_Detail("STU_SEN_REMARK"))

                        If Convert.ToBoolean(readerStudent_Detail("STU_bEAL")) = True Then
                            rbHthEAL_Yes.Checked = True
                        Else
                            rbHthEAL_No.Checked = True
                        End If
                        txtHthEAL_Note.Text = Convert.ToString(readerStudent_Detail("STU_EAL_REMARK"))

                        If Convert.ToBoolean(readerStudent_Detail("STU_bBEHAVIOUR")) = True Then
                            rbHthBehv_Yes.Checked = True
                        Else
                            rbHthBehv_No.Checked = True
                        End If
                        txtHthBehv_Note.Text = Convert.ToString(readerStudent_Detail("STU_BEHAVIOUR"))


                        If Convert.ToBoolean(readerStudent_Detail("STU_bENRICH")) = True Then
                            rbHthEnr_Yes.Checked = True
                        Else
                            rbHthEnr_No.Checked = True
                        End If
                        txtHthEnr_note.Text = Convert.ToString(readerStudent_Detail("STU_ENRICH"))

                        If Convert.ToBoolean(readerStudent_Detail("STU_bMUSICAL")) = True Then
                            rbHthMus_Yes.Checked = True
                        Else
                            rbHthMus_No.Checked = True
                        End If
                        txtHthMus_Note.Text = Convert.ToString(readerStudent_Detail("STU_MUSICAL"))


                        If Convert.ToBoolean(readerStudent_Detail("STU_bSPORTS")) = True Then
                            rbHthSport_Yes.Checked = True
                        Else
                            rbHthSport_No.Checked = True
                        End If
                        txtHthSport_note.Text = Convert.ToString(readerStudent_Detail("STU_SPORTS"))



                        If Convert.ToBoolean(readerStudent_Detail("STU_bCommInt")) = True Then
                            rbHthComm_Yes.Checked = True
                        Else
                            rbHthComm_No.Checked = True
                        End If
                        txtHthCommInt_Note.Text = Convert.ToString(readerStudent_Detail("STU_CommInt"))


                        If Convert.ToBoolean(readerStudent_Detail("STU_bDisabled")) = True Then
                            rbHthDisabled_YES.Checked = True
                        Else
                            rbHthDisabled_No.Checked = True
                        End If
                        txtHthDisabled_Note.Text = Convert.ToString(readerStudent_Detail("STU_Disabled"))








                        temp_Mail = Convert.ToBoolean(readerStudent_Detail("STU_bRCVMAIL"))
                        If temp_Mail Then
                            rdRcvMail_Yes.Checked = True
                        Else
                            rdRcvMail_No.Checked = True
                        End If

                        temp_SMS = Convert.ToBoolean(readerStudent_Detail("STU_bRCVSMS"))
                        If temp_SMS Then
                            rbRcvSms_Yes.Checked = True
                        Else
                            rbRcvSms_No.Checked = True
                        End If

                        Temp_Phone_Split = Convert.ToString(readerStudent_Detail("SEMGCONTACT"))





                        'setting the phone
                        arInfo = Temp_Phone_Split.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtEmerg_Country.Text = arInfo(0)
                            txtEmerg_Area.Text = arInfo(1)
                            txtEmerg_PhoneNo.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtEmerg_Area.Text = arInfo(0)
                            txtEmerg_PhoneNo.Text = arInfo(1)

                        Else

                            txtEmerg_PhoneNo.Text = Temp_Phone_Split

                        End If

                        'ISNULL(STUDENT_M.STU_PREVSCHI, '') AS STU_PREVSCHI, ISNULL(STUDENT_M.STU_PREVSCHI_CITY, '') AS STU_PREVSCHI_CITY,
                        ' ISNULL(STUDENT_M.STU_PREVSCHI_COUNTRY, 0)  AS STU_PREVSCHI_COUNTRY, ISNULL(STUDENT_M.STU_PREVSCHI_CLM, 0) AS STU_PREVSCHI_CLM, 
                        ' ISNULL(STUDENT_M.STU_PREVSCHI_GRADE, '') AS STU_PREVSCHI_GRADE,ISNULL(STU_PREVSCHI_MEDIUM,'') AS STU_PREVSCHI_MEDIUM,
                        'ISNULL(STUDENT_M.STU_PREVSCHI_FROMDATE, '01/01/1900') AS STU_PREVSCHI_FROMDATE,
                        ' ISNULL(STUDENT_M.STU_PREVSCHI_LASTATTDATE, '01/01/1900') AS STU_PREVSCHI_LASTATTDATE,
                        'STU_PREVSCHI_HEAD_NAME,STU_PREVSCHI_ADDRESS,STU_PREVSCHI_PHONE,STU_PREVSCHI_FAX,STU_PREVSCHI_REG_ID,



                        'code added bu dhanya 13 jul 08
                        txtPSchool_Name1.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHI"))
                        txtPSchool_Name2.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHII"))
                        txtPSchool_Name3.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII"))

                        txtPSchool_HEAD_NAME1.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_HEAD_NAME"))
                        txtPSchool_HEAD_NAME2.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_HEAD_NAME"))
                        txtPSchool_HEAD_NAME3.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_HEAD_NAME"))

                        txtPSchool_REG_ID1.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_REG_ID"))
                        txtPSchool_REG_ID2.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_REG_ID"))
                        txtPSchool_REG_ID3.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_REG_ID"))

                        txtPSchool_City1.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_CITY"))
                        txtPSchool_City2.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_CITY"))
                        txtPSchool_City3.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_CITY"))


                        txtPSchool_Medium1.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_MEDIUM"))
                        txtPSchool_Medium2.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_MEDIUM"))
                        txtPSchool_Medium3.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_MEDIUM"))

                        txtPSchool_Ph1.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_PHONE"))
                        txtPSchool_Ph2.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_PHONE"))
                        txtPSchool_Ph3.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_PHONE"))

                        txtPSchool_Fax1.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_FAX"))
                        txtPSchool_fax2.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_FAX"))
                        txtPSchool_fax3.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_FAX"))

                        txtPSchool_ADDRESS1.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_ADDRESS"))
                        txtPSchool_ADDRESS2.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_ADDRESS"))
                        txtPSchool_ADDRESS3.Text = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_ADDRESS"))


                        If IsDate(readerStudent_Detail("STU_PREVSCHI_FROMDATE")) = True Then
                            txtPFRM_DT1.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PREVSCHI_FROMDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_PREVSCHI_LASTATTDATE")) = True Then
                            txtPTO_DT1.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PREVSCHI_LASTATTDATE")))).Replace("01/Jan/1900", "")
                        End If

                        If IsDate(readerStudent_Detail("STU_PREVSCHII_FROMDATE")) = True Then
                            txtPFRM_DT2.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PREVSCHII_FROMDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_PREVSCHII_LASTATTDATE")) = True Then
                            txtPTO_DT2.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PREVSCHII_LASTATTDATE")))).Replace("01/Jan/1900", "")
                        End If

                        If IsDate(readerStudent_Detail("STU_PREVSCHIII_FROMDATE")) = True Then
                            txtPFRM_DT3.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PREVSCHIII_FROMDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_PREVSCHIII_LASTATTDATE")) = True Then
                            txtPTo_DT3.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PREVSCHIII_LASTATTDATE")))).Replace("01/Jan/1900", "")
                        End If

                        If Not ddlPSchool_Country1.Items.FindByValue(readerStudent_Detail("STU_PREVSCHI_COUNTRY")) Is Nothing Then
                            ddlPSchool_Country1.ClearSelection()
                            ddlPSchool_Country1.Items.FindByValue(readerStudent_Detail("STU_PREVSCHI_COUNTRY")).Selected = True
                        End If

                        If Not ddlPSchool_Country2.Items.FindByValue(readerStudent_Detail("STU_PREVSCHII_COUNTRY")) Is Nothing Then
                            ddlPSchool_Country2.ClearSelection()
                            ddlPSchool_Country2.Items.FindByValue(readerStudent_Detail("STU_PREVSCHII_COUNTRY")).Selected = True
                        End If

                        If Not ddlPSchool_Country3.Items.FindByValue(readerStudent_Detail("STU_PREVSCHIII_COUNTRY")) Is Nothing Then
                            ddlPSchool_Country3.ClearSelection()
                            ddlPSchool_Country3.Items.FindByValue(readerStudent_Detail("STU_PREVSCHIII_COUNTRY")).Selected = True
                        End If
                        If Not ddlPBoard_Attend1.Items.FindByValue(readerStudent_Detail("STU_PREVSCHI_CLM")) Is Nothing Then
                            ddlPBoard_Attend1.ClearSelection()
                            ddlPBoard_Attend1.Items.FindByValue(readerStudent_Detail("STU_PREVSCHI_CLM")).Selected = True
                        End If
                        If Not ddlPBoard_Attend2.Items.FindByValue(readerStudent_Detail("STU_PREVSCHII_CLM")) Is Nothing Then
                            ddlPBoard_Attend2.ClearSelection()
                            ddlPBoard_Attend2.Items.FindByValue(readerStudent_Detail("STU_PREVSCHII_CLM")).Selected = True
                        End If
                        If Not ddlPBoard_Attend3.Items.FindByValue(readerStudent_Detail("STU_PREVSCHIII_CLM")) Is Nothing Then
                            ddlPBoard_Attend3.ClearSelection()
                            ddlPBoard_Attend3.Items.FindByValue(readerStudent_Detail("STU_PREVSCHIII_CLM")).Selected = True
                        End If

                        If Not ddlPSchool_Year1.Items.FindByValue(readerStudent_Detail("STU_PREVSCHI_GRADE")) Is Nothing Then
                            ddlPSchool_Year1.ClearSelection()
                            ddlPSchool_Year1.Items.FindByValue(readerStudent_Detail("STU_PREVSCHI_GRADE")).Selected = True
                        End If
                        If Not ddlPSchool_Year2.Items.FindByValue(readerStudent_Detail("STU_PREVSCHII_GRADE")) Is Nothing Then
                            ddlPSchool_Year2.ClearSelection()
                            ddlPSchool_Year2.Items.FindByValue(readerStudent_Detail("STU_PREVSCHII_GRADE")).Selected = True
                        End If
                        If Not ddlPSchool_Year3.Items.FindByValue(readerStudent_Detail("STU_PREVSCHIII_GRADE")) Is Nothing Then
                            ddlPSchool_Year3.ClearSelection()
                            ddlPSchool_Year3.Items.FindByValue(readerStudent_Detail("STU_PREVSCHIII_GRADE")).Selected = True
                        End If


                        If Not ddlReligion.Items.FindByValue(Convert.ToString(readerStudent_Detail("RLG_ID"))) Is Nothing Then
                            ddlReligion.ClearSelection()
                            ddlReligion.Items.FindByValue(Convert.ToString(readerStudent_Detail("RLG_ID"))).Selected = True
                        End If


                        If Not ddlTran_Type.Items.FindByValue(Convert.ToString(readerStudent_Detail("STFRTYPE"))) Is Nothing Then
                            ddlTran_Type.ClearSelection()
                            ddlTran_Type.Items.FindByValue(Convert.ToString(readerStudent_Detail("STFRTYPE"))).Selected = True
                        End If

                        If Not ddlJoinResult.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_JOIN_RESULT"))) Is Nothing Then
                            ddlJoinResult.ClearSelection()
                            ddlJoinResult.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_JOIN_RESULT"))).Selected = True
                        End If

                        If Not ddlNationality_Birth.Items.FindByValue(Convert.ToString(readerStudent_Detail("SNATIONALITY"))) Is Nothing Then
                            ddlNationality_Birth.ClearSelection()
                            ddlNationality_Birth.Items.FindByValue(Convert.ToString(readerStudent_Detail("SNATIONALITY"))).Selected = True
                        End If
                        If Not ddlCountry_Birth.Items.FindByValue(Convert.ToString(readerStudent_Detail("COB"))) Is Nothing Then
                            ddlCountry_Birth.ClearSelection()
                            ddlCountry_Birth.Items.FindByValue(Convert.ToString(readerStudent_Detail("COB"))).Selected = True
                        End If


                        If Not ddlHouse_BSU.Items.FindByValue(Convert.ToString(readerStudent_Detail("HOUSE"))) Is Nothing Then
                            ddlHouse_BSU.ClearSelection()
                            ddlHouse_BSU.Items.FindByValue(Convert.ToString(readerStudent_Detail("HOUSE"))).Selected = True
                        End If









                        'ViewState("temp_PSchool_Country1") = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_COUNTRY"))
                        'ViewState("temp_PBoard_Attend1") = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_CLM"))
                        'ViewState("temp_PSchool_Year1") = Convert.ToString(readerStudent_Detail("STU_PREVSCHI_GRADE"))
                        'ViewState("temp_PSchool_Country2") = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_COUNTRY"))
                        'ViewState("temp_PBoard_Attend2") = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_CLM"))
                        'ViewState("temp_PSchool_Year2") = Convert.ToString(readerStudent_Detail("STU_PREVSCHII_GRADE"))
                        'ViewState("temp_PSchool_Country3") = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_COUNTRY"))
                        'ViewState("temp_PBoard_Attend3") = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_CLM"))
                        'ViewState("temp_PSchool_Year3") = Convert.ToString(readerStudent_Detail("STU_PREVSCHIII_GRADE"))
                        'viewState setting
                        'ViewState("temp_Type") = Convert.ToString(readerStudent_Detail("STFRTYPE"))
                        'ViewState("temp_Rlg_ID") = Convert.ToString(readerStudent_Detail("RLG_ID"))
                        'ViewState("temp_Nationality") = Convert.ToString(readerStudent_Detail("SNATIONALITY"))
                        'ViewState("temp_COB") = Convert.ToString(readerStudent_Detail("COB"))
                        ' ViewState("temp_HOUSE") = Convert.ToString(readerStudent_Detail("HOUSE"))
                        ' ViewState("temp_MINLIST") = Convert.ToString(readerStudent_Detail("MINLIST"))
                        'ViewState("temp_MINLISTTYPE") = Convert.ToString(readerStudent_Detail("SMINLISTTYPE"))
                        'ViewState("temp_PrefContact") = Convert.ToString(readerStudent_Detail("STU_PREFCONTACT"))
                        'ViewState("temp_Blood") = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        'code added by dhanya
                        ViewState("ACD_ID") = Convert.ToString(readerStudent_Detail("ACD_ID"))


                        If Not ddlMini_list.Items.FindByValue(Convert.ToString(readerStudent_Detail("MINLIST"))) Is Nothing Then
                            ddlMini_list.ClearSelection()
                            ddlMini_list.Items.FindByValue(Convert.ToString(readerStudent_Detail("MINLIST"))).Selected = True
                        End If
                        If Not ddlMini_list_Type.Items.FindByValue(Convert.ToString(readerStudent_Detail("SMINLISTTYPE"))) Is Nothing Then
                            ddlMini_list_Type.ClearSelection()
                            ddlMini_list_Type.Items.FindByValue(Convert.ToString(readerStudent_Detail("SMINLISTTYPE"))).Selected = True
                        End If
                        If Not ddlPrefContact.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_PREFCONTACT"))) Is Nothing Then
                            ddlPrefContact.ClearSelection()
                            ddlPrefContact.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_PREFCONTACT"))).Selected = True
                        End If

                        If Not ddlBgroup.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))) Is Nothing Then
                            ddlBgroup.ClearSelection()
                            ddlBgroup.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))).Selected = True
                        End If





                        If Not ddlFLang.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_FIRSTLANG"))) Is Nothing Then
                            ddlFLang.ClearSelection()
                            ddlFLang.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_FIRSTLANG"))).Selected = True
                        End If

                        Dim arInfoLang() As String
                        Dim splitterLang As Char = "|"
                        Dim new1 As String = String.Empty
                        arInfoLang = Convert.ToString(readerStudent_Detail("STU_OTHLANG")).Trim.Replace("''", "'").Split(splitterLang)
                        If arInfoLang.Length > 0 Then
                            For i As Integer = 0 To arInfoLang.Length - 1
                                If arInfoLang(i).Trim <> "" Then
                                    If Not chkOLang.Items.FindByValue(arInfoLang(i)) Is Nothing Then
                                        chkOLang.Items.FindByValue(arInfoLang(i)).Selected = True
                                    End If
                                End If
                            Next
                        End If


                        Dim STU_ENG_READING As String = Convert.ToString(readerStudent_Detail("STU_ENG_READING"))
                        Dim STU_ENG_WRITING As String = Convert.ToString(readerStudent_Detail("STU_ENG_WRITING"))
                        Dim STU_ENG_SPEAKING As String = Convert.ToString(readerStudent_Detail("STU_ENG_SPEAKING"))

                        If STU_ENG_WRITING = "1" Then
                            rbWExc.Checked = True
                        ElseIf STU_ENG_WRITING = "2" Then
                            rbWGood.Checked = True
                        ElseIf STU_ENG_WRITING = "3" Then
                            rbWFair.Checked = True
                        ElseIf STU_ENG_WRITING = "4" Then
                            rbWPoor.Checked = True

                        End If


                        If STU_ENG_SPEAKING = "1" Then
                            rbSExc.Checked = True
                        ElseIf STU_ENG_SPEAKING = "2" Then
                            rbSGood.Checked = True
                        ElseIf STU_ENG_SPEAKING = "3" Then
                            rbSFair.Checked = True
                        ElseIf STU_ENG_SPEAKING = "4" Then
                            rbSPoor.Checked = True
                        End If

                        If STU_ENG_READING = "1" Then
                            rbRExc.Checked = True
                        ElseIf STU_ENG_READING = "2" Then
                            rbRGood.Checked = True
                        ElseIf STU_ENG_READING = "3" Then
                            rbRFair.Checked = True
                        ElseIf STU_ENG_READING = "4" Then
                            rbRPoor.Checked = True
                        End If

                        txtGFather.Text = Convert.ToString(readerStudent_Detail("STU_GFATHERNAME"))
                        txtNatNo.Text = Convert.ToString(readerStudent_Detail("STU_NAT_NO"))
                        txtRefugNo.Text = Convert.ToString(readerStudent_Detail("STU_RFG_NO"))
                        txtDob_Arabic.Text = Convert.ToString(readerStudent_Detail("STU_DOB_ARABIC"))
                        txtPob_Arabic.Text = Convert.ToString(readerStudent_Detail("STU_POB_ARABIC"))
                        txtBC_NO.Text = Convert.ToString(readerStudent_Detail("STU_BCERT_NO"))
                        txtBC_POI.Text = Convert.ToString(readerStudent_Detail("STU_BCERT_ISSUE"))
                        txtBC_DOI.Text = Convert.ToString(readerStudent_Detail("STU_BCERT_DATE"))
                        txtFamilyName.Text = Convert.ToString(readerStudent_Detail("STU_FAMILY_NAME_ARABIC"))

                        txtEmiratesID.Text = Convert.ToString(readerStudent_Detail("Emirates_ID"))
                        txtEMIRATES_IDExp_date.Text = Convert.ToString(readerStudent_Detail("STU_APPLEMIRATES_ID_EXPDATE"))
                        txtSEmiratesId_ENG.Text = Convert.ToString(readerStudent_Detail("STU_EMIRATESID_NAME"))
                        txtSEmiratesId_ARB.Text = Convert.ToString(readerStudent_Detail("STU_EMIRATESID_ARABIC_NAME"))
                        txtPremisesId.Text = Convert.ToString(readerStudent_Detail("stu_PremisesID"))
                        txtStu_Email.text = Convert.ToString(readerStudent_Detail("STU_EMAIL"))


                        If Convert.ToBoolean(readerStudent_Detail("BSU_bHide_PremisesID")) = False Then
                            trPremisesId.Visible = True
                        Else
                            trPremisesId.Visible = False
                        End If

                    End While

                Else
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Student M")

        End Try
    End Sub


    Sub DropDownList_RecordSelect()

        ''ViewState("temp_MINLISTTYPE")

        'For ItemTypeCounter As Integer = 0 To ddlMini_list_Type.Items.Count - 1
        '    'keep loop until you get the School to Not Available into  the SelectedIndex
        '    If UCase(ddlMini_list_Type.Items(ItemTypeCounter).Value) = UCase(ViewState("temp_MINLISTTYPE")) Then
        '        ddlMini_list_Type.SelectedIndex = ItemTypeCounter
        '    End If
        'Next

        'For ItemTypeCounter As Integer = 0 To ddlPrefContact.Items.Count - 1
        '    'keep loop until you get the School to Not Available into  the SelectedIndex
        '    If UCase(ddlPrefContact.Items(ItemTypeCounter).Value) = UCase(ViewState("temp_PrefContact")) Then
        '        ddlPrefContact.SelectedIndex = ItemTypeCounter
        '    End If
        'Next
        'For ItemTypeCounter As Integer = 0 To ddlBgroup.Items.Count - 1
        '    'keep loop until you get the School to Not Available into  the SelectedIndex
        '    If UCase(ddlBgroup.Items(ItemTypeCounter).Value) = UCase(ViewState("temp_Blood")) Then
        '        ddlBgroup.SelectedIndex = ItemTypeCounter
        '    End If
        'Next

        'For ItemTypeCounter As Integer = 0 To ddlMini_list.Items.Count - 1
        '    'keep loop until you get the School to Not Available into  the SelectedIndex
        '    If UCase(ddlMini_list.Items(ItemTypeCounter).Value) = UCase(ViewState("temp_MINLIST")) Then
        '        ddlMini_list.SelectedIndex = ItemTypeCounter
        '    End If
        'Next





    End Sub


    'code commented by dhanya 29 apr 08
    '*******************************************************************************************
    'Sub GetGrade_Display_BSU()
    '    Try

    '        Using Grade_Display_BSU_reader As SqlDataReader = AccessStudentClass.GetGrade_Display_BSU(Session("sBsuid"))

    '            Dim di_Grade_BSU As ListItem
    '            ddlPSchool_Year1.Items.Clear()
    '            ddlPSchool_Year2.Items.Clear()
    '            ddlPSchool_Year3.Items.Clear()
    '            'di_Religion = New ListItem("--", "--")
    '            ' ddlReligion.Items.Add(di_Religion)
    '            If Grade_Display_BSU_reader.HasRows = True Then
    '                While Grade_Display_BSU_reader.Read
    '                    di_Grade_BSU = New ListItem(Grade_Display_BSU_reader("GRM_DISPLAY"), Grade_Display_BSU_reader("GRD_ID"))
    '                    ddlPSchool_Year1.Items.Add(New ListItem(Grade_Display_BSU_reader("GRM_DISPLAY"), Grade_Display_BSU_reader("GRD_ID")))
    '                    ddlPSchool_Year2.Items.Add(New ListItem(Grade_Display_BSU_reader("GRM_DISPLAY"), Grade_Display_BSU_reader("GRD_ID")))
    '                    ddlPSchool_Year3.Items.Add(New ListItem(Grade_Display_BSU_reader("GRM_DISPLAY"), Grade_Display_BSU_reader("GRD_ID")))

    '                End While

    '                For ItemTypeCounter As Integer = 0 To ddlPSchool_Year1.Items.Count - 1
    '                    'keep loop until you get the School to Not Available into  the SelectedIndex
    '                    If ddlPSchool_Year1.Items(ItemTypeCounter).Value = ViewState("temp_PSchool_Year1") Then
    '                        ddlPSchool_Year1.SelectedIndex = ItemTypeCounter
    '                    End If

    '                    If ddlPSchool_Year2.Items(ItemTypeCounter).Value = ViewState("temp_PSchool_Year2") Then
    '                        ddlPSchool_Year2.SelectedIndex = ItemTypeCounter
    '                    End If
    '                    If ddlPSchool_Year3.Items(ItemTypeCounter).Value = ViewState("temp_PSchool_Year3") Then
    '                        ddlPSchool_Year3.SelectedIndex = ItemTypeCounter
    '                    End If
    '                Next
    '            End If
    '        End Using
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, "GetGrade_BSU_info")
    '    End Try
    'End Sub
    '*********************************************************************************

    'code modified by dhanya 29 apr 08
    Sub GetGrade_Display_BSU()
        Try

            Using Grade_Display_BSU_reader As SqlDataReader = AccessStudentClass.GetGrade_M()

                Dim di_Grade_BSU As ListItem
                ddlPSchool_Year1.Items.Clear()
                ddlPSchool_Year2.Items.Clear()
                ddlPSchool_Year3.Items.Clear()

                ddlPSchool_Year1.Items.Add(New ListItem("--", "--"))
                ddlPSchool_Year2.Items.Add(New ListItem("--", "--"))
                ddlPSchool_Year3.Items.Add(New ListItem("--", "--"))

                If Grade_Display_BSU_reader.HasRows = True Then

                    While Grade_Display_BSU_reader.Read
                        di_Grade_BSU = New ListItem(Grade_Display_BSU_reader("GRD_DISPLAY"), Grade_Display_BSU_reader("GRD_ID"))
                        ddlPSchool_Year1.Items.Add(New ListItem(Grade_Display_BSU_reader("GRD_DISPLAY"), Grade_Display_BSU_reader("GRD_ID")))
                        ddlPSchool_Year2.Items.Add(New ListItem(Grade_Display_BSU_reader("GRD_DISPLAY"), Grade_Display_BSU_reader("GRD_ID")))
                        ddlPSchool_Year3.Items.Add(New ListItem(Grade_Display_BSU_reader("GRD_DISPLAY"), Grade_Display_BSU_reader("GRD_ID")))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetGrade_BSU_info")
        End Try
    End Sub

    Sub GetCURRICULUM_M()
        Try

            Using AllCURRICULUM_reader As SqlDataReader = AccessStudentClass.GetCURRICULUM_M()

                Dim di_CURRICULUM As ListItem
                ddlPBoard_Attend1.Items.Clear()
                ddlPBoard_Attend2.Items.Clear()
                ddlPBoard_Attend3.Items.Clear()
                'di_Religion = New ListItem("--", "--")
                ' ddlReligion.Items.Add(di_Religion)
                If AllCURRICULUM_reader.HasRows = True Then
                    While AllCURRICULUM_reader.Read
                        di_CURRICULUM = New ListItem(AllCURRICULUM_reader("CLM_DESCR"), AllCURRICULUM_reader("CLM_ID"))
                        ddlPBoard_Attend1.Items.Add(New ListItem(AllCURRICULUM_reader("CLM_DESCR"), AllCURRICULUM_reader("CLM_ID")))
                        ddlPBoard_Attend2.Items.Add(New ListItem(AllCURRICULUM_reader("CLM_DESCR"), AllCURRICULUM_reader("CLM_ID")))
                        ddlPBoard_Attend3.Items.Add(New ListItem(AllCURRICULUM_reader("CLM_DESCR"), AllCURRICULUM_reader("CLM_ID")))

                    End While

                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCURRICULUM_info")
        End Try
    End Sub

    Sub GetReligion_info()
        Try

            Using AllReligion_reader As SqlDataReader = AccessStudentClass.GetReligion()


                ddlReligion.Items.Clear()
                'di_Religion = New ListItem("--", "--")
                ' ddlReligion.Items.Add(di_Religion)
                If AllReligion_reader.HasRows = True Then
                    While AllReligion_reader.Read
                        ddlReligion.Items.Add(New ListItem(AllReligion_reader("RLG_DESCR"), AllReligion_reader("RLG_ID")))

                    End While


                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetReligion_info")
        End Try
    End Sub


    Sub GetCompany_Name()
        Try
            Using GetCompany_Name_reader As SqlDataReader = AccessStudentClass.GetCompany_Name()
                ddlFCompany_Name.Items.Clear()
                ddlMCompany_Name.Items.Clear()
                ddlGCompany_Name.Items.Clear()

                ddlFCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlMCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlGCompany_Name.Items.Add(New ListItem("Other", "0"))

                If GetCompany_Name_reader.HasRows = True Then
                    While GetCompany_Name_reader.Read
                        ddlFCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlMCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlGCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                    End While
                End If

            End Using


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, " GetCompany_Name()")
        End Try
    End Sub


    Sub GetBusinessUnits_info_staff()
        Try


            Using AllStaff_BSU_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_staff()

                Dim di_Staff_BSU As ListItem
                ddlM_BSU_ID.Items.Clear()
                ddlF_BSU_ID.Items.Clear()
                'di_Religion = New ListItem("--", "--")
                ddlM_BSU_ID.Items.Add(New ListItem("", ""))
                ddlF_BSU_ID.Items.Add(New ListItem("", ""))
                If AllStaff_BSU_reader.HasRows = True Then
                    While AllStaff_BSU_reader.Read

                        di_Staff_BSU = New ListItem(AllStaff_BSU_reader("bsu_name"), AllStaff_BSU_reader("bsu_id"))
                        ddlM_BSU_ID.Items.Add(New ListItem(AllStaff_BSU_reader("bsu_name"), AllStaff_BSU_reader("bsu_id")))
                        ddlF_BSU_ID.Items.Add(New ListItem(AllStaff_BSU_reader("bsu_name"), AllStaff_BSU_reader("bsu_id")))

                    End While



                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_staff()")
        End Try
    End Sub

    Sub GetCountry_info()
        Try
            ddlFCOMCountry.Items.Clear()
            ddlFPRM_Country.Items.Clear()
            ddlMCOMCountry.Items.Clear()
            ddlGCOMCountry.Items.Clear()
            ddlGPRM_Country.Items.Clear()
            ddlPSchool_Country1.Items.Clear()
            ddlPSchool_Country2.Items.Clear()
            ddlPSchool_Country3.Items.Clear()

            ddlCountry_Birth.Items.Add(New ListItem("", ""))
            ddlFCOMCountry.Items.Add(New ListItem("", ""))
            ddlFPRM_Country.Items.Add(New ListItem("", ""))
            ddlMCOMCountry.Items.Add(New ListItem("", ""))
            ddlMPRM_Country.Items.Add(New ListItem("", ""))
            ddlGCOMCountry.Items.Add(New ListItem("", ""))
            ddlGPRM_Country.Items.Add(New ListItem("", ""))
            ddlPSchool_Country1.Items.Add(New ListItem("", ""))
            ddlPSchool_Country2.Items.Add(New ListItem("", ""))
            ddlPSchool_Country3.Items.Add(New ListItem("", ""))





            Using AllCountry_reader As SqlDataReader = AccessRoleUser.GetCountry()
                Dim di_Country As ListItem

                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID"))
                        ddlCountry_Birth.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlFCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlFPRM_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlMCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlMPRM_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlGCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlGPRM_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlPSchool_Country1.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlPSchool_Country2.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlPSchool_Country3.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))

                        'ddlCountry_Birth.Items.Add(di_Country)
                        'ddlFCOMCountry.Items.Add(di_Country)
                        'ddlFPRM_Country.Items.Add(di_Country)
                        'ddlMCOMCountry.Items.Add(di_Country)
                        'ddlMPRM_Country.Items.Add(di_Country)
                        'ddlGCOMCountry.Items.Add(di_Country)
                        'ddlGPRM_Country.Items.Add(di_Country)
                        'ddlPSchool_Country1.Items.Add(di_Country)
                        'ddlPSchool_Country2.Items.Add(di_Country)
                        'ddlPSchool_Country3.Items.Add(di_Country)
                    End While
                    'For ItemTypeCounter As Integer = 0 To ddlCountry_Birth.Items.Count - 1
                    'keep loop until you get the Country to Not Available into  the SelectedIndex
                    'ddlCountry_Birth.SelectedIndex = -1
                    'ddlFCOMCountry.SelectedIndex = -1
                    'ddlFPRM_Country.SelectedIndex = -1
                    'ddlMCOMCountry.SelectedIndex = -1
                    'ddlMPRM_Country.SelectedIndex = -1
                    'ddlGCOMCountry.SelectedIndex = -1
                    'ddlGPRM_Country.SelectedIndex = -1
                    'ddlPSchool_Country1.SelectedIndex = -1
                    'ddlPSchool_Country2.SelectedIndex = -1
                    'ddlPSchool_Country3.SelectedIndex = -1

                    'ddlCountry_Birth.Items.FindByValue(ViewState("temp_COB")).Selected = True
                    'ddlFCOMCountry.Items.FindByValue(ViewState("temp_FComCountry")).Selected = True
                    'ddlFPRM_Country.Items.FindByValue(ViewState("temp_FPRM_Country")).Selected = True
                    'ddlMCOMCountry.Items.FindByValue(ViewState("temp_MCOMCountry")).Selected = True
                    'ddlMPRM_Country.Items.FindByValue(ViewState("temp_MPRMCOUNTRY")).Selected = True
                    'ddlGCOMCountry.Items.FindByValue(ViewState("temp_GCOMCountry")).Selected = True
                    'ddlGPRM_Country.Items.FindByValue(ViewState("temp_GPRMCOUNTRY")).Selected = True
                    'ddlPSchool_Country1.Items.FindByValue(ViewState("temp_PSchool_Country1")).Selected = True
                    'ddlPSchool_Country2.Items.FindByValue(ViewState("temp_PSchool_Country2")).Selected = True
                    'ddlPSchool_Country3.Items.FindByValue(ViewState("temp_PSchool_Country3")).Selected = True


                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub
    Private Sub bind_FCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlFCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFCity.Items.Clear()
                ddlFCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlFCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlFCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FState")
        End Try
    End Sub
    Private Sub bind_MCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlMCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlMCity.Items.Clear()
                ddlMCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlMCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlMCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_MState")
        End Try
    End Sub
    Private Sub bind_GCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlGCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlGCity.Items.Clear()
                ddlGCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlGCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlGCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_GState")
        End Try
    End Sub
    Private Sub bind_FArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlFCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFArea.Items.Clear()
                ddlFArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlFArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlFArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Private Sub bind_MArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlMCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlMArea.Items.Clear()
                ddlMArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlMArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlMArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Private Sub bind_GArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlGCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlGArea.Items.Clear()
                ddlGArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlGArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlGArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Protected Sub ddlFCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFCOMCountry.SelectedIndexChanged
        bind_FCITY()
        ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

    End Sub
    Protected Sub ddlMCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlMCOMCountry.SelectedIndexChanged
        bind_MCITY()
        ddlMCity_SelectedIndexChanged(ddlMCity, Nothing)
    End Sub
    Protected Sub ddlGCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGCOMCountry.SelectedIndexChanged
        bind_GCITY()
        ddlGCity_SelectedIndexChanged(ddlGCity, Nothing)
    End Sub
    Protected Sub ddlFCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFCity.SelectedIndexChanged
        bind_FArea()
    End Sub
    Protected Sub ddlMCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlMCity.SelectedIndexChanged
        bind_MArea()
    End Sub
    Protected Sub ddlGCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGCity.SelectedIndexChanged
        bind_GArea()
    End Sub


    Sub getTransfer()

        Try
            Using Transfer_reader As SqlDataReader = AccessStudentClass.GetTransfer()
                Dim di_Transfer As ListItem

                ddlTran_Type.Items.Clear()
                'di_Transfer = New ListItem("--", "--")
                'ddlTran_Type.Items.Add(di_Transfer)
                If Transfer_reader.HasRows = True Then
                    While Transfer_reader.Read()
                        Dim name As String = Transfer_reader("TFR_DESCR").ToString()
                        Dim id As String = Transfer_reader("TFR_CODE").ToString()
                        If Session("BSU_COUNTRY_ID") <> "172" Then
                            If Transfer_reader("TFR_CODE") = ("E") Then
                                name = "OTHER CITY"
                            End If
                        End If
                        ddlTran_Type.Items.Add(New ListItem(name, id))
                    End While


                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetTransfer")
        End Try
    End Sub
    Sub getColor_House()

        Try
            Using AllHouse_reader As SqlDataReader = AccessStudentClass.GetBSU_House(Session("sBsuid"))
                Dim di_House As ListItem
                ddlHouse_BSU.Items.Clear()
                di_House = New ListItem("", "")
                ddlHouse_BSU.Items.Add(di_House)
                If AllHouse_reader.HasRows = True Then
                    While AllHouse_reader.Read()
                        di_House = New ListItem(AllHouse_reader("ID"), AllHouse_reader("HideID"))
                        ddlHouse_BSU.Items.Add(di_House)
                    End While

                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetHOUSEBSU_info")
        End Try

    End Sub
    'Getting national info
    Sub GetNational_info()
        Try
            Using AllNational_reader As SqlDataReader = AccessStudentClass.GetNational()
                Dim di_National As ListItem
                ddlNationality_Birth.Items.Clear()
                ddlFNationality1.Items.Clear()
                ddlFNationality2.Items.Clear()
                ddlMNationality1.Items.Clear()
                ddlMNationality2.Items.Clear()
                ddlGNationality1.Items.Clear()
                ddlGNationality2.Items.Clear()
                ddlNationality_Birth.Items.Add(New ListItem("", ""))
                ddlFNationality1.Items.Add(New ListItem("", ""))
                ddlFNationality2.Items.Add(New ListItem("", ""))
                ddlMNationality1.Items.Add(New ListItem("", ""))
                ddlMNationality2.Items.Add(New ListItem("", ""))
                ddlGNationality1.Items.Add(New ListItem("", ""))
                ddlGNationality2.Items.Add(New ListItem("", ""))

                If AllNational_reader.HasRows = True Then
                    While AllNational_reader.Read()
                        di_National = New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID"))
                        ddlNationality_Birth.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlFNationality1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlFNationality2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlMNationality1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlMNationality2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlGNationality1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlGNationality2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                    End While


                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetNational_info")
        End Try
    End Sub
    'Getting Emirates info
    Sub GetEmirate_info()
        Try
            Using AllEmirate_reader As SqlDataReader = AccessStudentClass.GetEmirate()
                Dim di_Emirate As ListItem
                ddlFCOMEmirate.Items.Clear()
                ddlMCOMEmirate.Items.Clear()
                ddlGCOMEmirate.Items.Clear()

                di_Emirate = New ListItem(" ", " ")
                ddlFCOMEmirate.Items.Add(di_Emirate)
                ddlMCOMEmirate.Items.Add(di_Emirate)
                ddlGCOMEmirate.Items.Add(di_Emirate)
                If AllEmirate_reader.HasRows = True Then
                    While AllEmirate_reader.Read
                        di_Emirate = New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE"))
                        ddlFCOMEmirate.Items.Add(New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE")))
                        ddlMCOMEmirate.Items.Add(New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE")))
                        ddlGCOMEmirate.Items.Add(New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE")))
                    End While



                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmirate_info")
        End Try
    End Sub
    Private Sub control_Disable()
        txtStud_No.Attributes.Add("Readonly", "Readonly")
        txtEQM_ENQID.Attributes.Add("Readonly", "Readonly")
        txtACD_ID_Join.Attributes.Add("Readonly", "Readonly")
        txtDOJ.Attributes.Add("Readonly", "Readonly")
        'txtMINDOJ.Attributes.Add("Readonly", "Readonly")
        txtGRD_ID_Join.Attributes.Add("Readonly", "Readonly")
        txtSCT_ID_JOIN.Attributes.Add("Readonly", "Readonly")
        txtACD_ID.Attributes.Add("Readonly", "Readonly")
        txtGRD_ID.Attributes.Add("Readonly", "Readonly")
        txtSCT_ID.Attributes.Add("Readonly", "Readonly")
        txtFee_ID.Attributes.Add("Readonly", "Readonly")
        txtCurrStatus.Attributes.Add("Readonly", "Readonly")
        ' txtHouse.Attributes.Add("Readonly", "Readonly")
        ' txtPickup.Attributes.Add("Readonly", "Readonly")
        ' txtDropoff.Attributes.Add("Readonly", "Readonly")
        'txtPickup_BusNo.Attributes.Add("Readonly", "Readonly")
        'txtDropOff_BusNo.Attributes.Add("Readonly", "Readonly")
        txtPar_Sib.Attributes.Add("Readonly", "Readonly")
        txtShift_ID.Attributes.Add("Readonly", "Readonly")
        txtStream.Attributes.Add("Readonly", "Readonly")
        txtJoin_Shift.Attributes.Add("Readonly", "Readonly")
        txtJoin_Stream.Attributes.Add("Readonly", "Readonly")

        txtShift_ID.Enabled = False
        txtStream.Enabled = False
        txtJoin_Shift.Enabled = False
        txtJoin_Stream.Enabled = False
        'rdTYes.Enabled = False
        'rdTNo.Enabled = False

        'txtPLocation.Enabled = False
        'txtPSubLocation.Enabled = False
        'txtPPickUpPoint.Enabled = False

        'txtDLocation.Enabled = False
        'txtDSubLocation.Enabled = False
        'txtDPickUpPoint.Enabled = False

        'txtTptRemarks.Enabled = False

        ' rdUSETPT_YES.Enabled = False
        ''  rdUSETPT_No.Enabled = False
        'rdStu_bActive_Yes.Enabled = False
        'rdStu_bActive_No.Enabled = False

        txtStud_No.Enabled = False
        txtEQM_ENQID.Enabled = False
        txtACD_ID_Join.Enabled = False
        txtDOJ.Enabled = False
        'txtMINDOJ.Enabled = False
        txtGRD_ID_Join.Enabled = False
        txtSCT_ID_JOIN.Enabled = False
        txtACD_ID.Enabled = False
        txtGRD_ID.Enabled = False
        txtSCT_ID.Enabled = False
        txtFee_ID.Enabled = False
        txtCurrStatus.Enabled = False
        '  txtHouse.Enabled = False
        'txtPickup.Enabled = False
        'txtDropoff.Enabled = False
        'txtPickup_BusNo.Enabled = False
        ' txtPar_Sib.Enabled = False
        ddlHouse_BSU.Enabled = False



    End Sub
    Private Sub BindReligion()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select RLG_ID,RLG_DESCR from RELIGION_M"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlReligion.DataSource = dr
        ddlReligion.DataTextField = "RLG_DESCR"
        ddlReligion.DataValueField = "RLG_ID"
        ddlReligion.DataBind()
    End Sub
    Sub ImgHeightnWidth(ByVal strImgPath As String)

        Dim actWidth, actHeight, imgWith, imgHeight As Integer
        Dim i As System.Drawing.Image = System.Drawing.Image.FromFile(strImgPath)
        imgWith = 202 'imgEmpImage.Width.Value
        imgHeight = 189 'imgEmpImage.Height.Value
        actWidth = i.Width
        actHeight = i.Height
        i.Dispose()
        Dim itempUnit As Integer
        If actWidth < imgWith And actHeight < imgHeight Then
            imgEmpImage.Height = actHeight
            imgEmpImage.Width = actWidth
            Exit Sub
        End If
        If actWidth < actHeight Then
            itempUnit = actWidth * imgHeight / actHeight
            If (imgWith > itempUnit) Then
                imgEmpImage.Width = itempUnit
            End If
        Else
            itempUnit = actHeight * imgWith / actWidth
            If (imgHeight > itempUnit) Then
                imgEmpImage.Height = itempUnit
            End If
        End If

    End Sub
    Public Function GetBSUEmpFilePath() As String

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select BSU_EMP_FILEPATH FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBSUID") & "'"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString


    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim status As Integer
        Dim transaction As SqlTransaction
        Dim ACD_ID As String = ViewState("Eid")
        Dim BSU_ID As String = Session("sBsuid")


        If Page.IsValid = True Then
            If ViewState("datamode") = "edit" Then

                If Final_validation() = True Then
                    Using conn2 As SqlConnection = ConnectionManger.GetOASISConnection
                        Dim STU_EMIRATES_ID As String = txtEmiratesID.Text.Replace("'", "")
                        Dim SEMIRATESID_ENG As String = txtSEmiratesId_ENG.Text.Replace("'", "")
                        Dim SEMIRATESID_ARB As String = txtSEmiratesId_ARB.Text.Replace("'", "")
                        Dim FEMIRATESID_ENG As String = txtFEmiratesId_ENG.Text.Replace("'", "")
                        Dim FEMIRATESID_ARB As String = txtFEmiratesId_ARB.Text.Replace("'", "")
                        Dim MEMIRATESID_ENG As String = txtMEmiratesId_ENG.Text.Replace("'", "")
                        Dim MEMIRATESID_ARB As String = txtMEmiratesId_ARB.Text.Replace("'", "")

                        Dim pParmsNew(3) As SqlClient.SqlParameter
                        pParmsNew(1) = New SqlClient.SqlParameter("@STU_ID", ViewState("viewid"))
                        pParmsNew(2) = New SqlClient.SqlParameter("@STU_EMIRATES_ID", STU_EMIRATES_ID)
                        SqlHelper.ExecuteNonQuery(conn2, CommandType.StoredProcedure, "SaveSTUDENT_M_EmiratesID", pParmsNew)
                    End Using


                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                        transaction = conn.BeginTransaction("SampleTransaction")
                        Try


                            status = callTrans_Student_M(transaction)

                            If status <> 0 Then
                                Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                            End If
                            status = callTrans_Student_D(transaction)

                            If status <> 0 Then
                                Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                            End If

                            'status = callTrans_Student_Services_D(transaction)
                            'If status <> 0 Then
                            '    Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                            'End If

                            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "Edit", Page.User.Identity.Name.ToString, Me.Page)

                            If flagAudit <> 0 Then
                                Throw New ArgumentException("Could not process your request")
                            End If
                            transaction.Commit()
                            ' populateGrade_Edit()
                            Dim msg As String = String.Empty
                            lblError.Text = "Record Updated Successfully"
                            'msg = "Record Updated Successfully"
                            'Dim url As String = String.Empty

                            ViewState("datamode") = "none"
                            ''Encrypt the data that needs to be send through Query String
                            'ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")

                            'ViewState("datamode") = Encr_decrData.Encrypt("edit")
                            'msg = Encr_decrData.Encrypt(msg)
                            'url = String.Format("~\Students\StudRecordView.aspx?MainMnu_code={0}&datamode={1}& msg={2}", ViewState("MainMnu_code"), ViewState("datamode"), msg)
                            'Response.Redirect(url)





                            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                        Catch myex As ArgumentException
                            transaction.Rollback()
                            lblError.Text = myex.Message
                        Catch ex As Exception
                            transaction.Rollback()
                            lblError.Text = "Record could not be Updated"

                        End Try
                    End Using
                End If
            End If
        End If
        lblError.Focus()

        'Dim sqlpEMD_PHOTO As New SqlParameter("@EMD_PHOTO", SqlDbType.VarChar, 200)
        'If ViewState("temp_PathPhoto") Is Nothing Then
        '    sqlpEMD_PHOTO.Value = ""
        'Else
        '    sqlpEMD_PHOTO.Value = ViewState("EMP_IMAGE_PATH")
        'End If
        ''sqlpEMD_PHOTO.Value = ViewState("EMP_IMAGE_PATH")

        'cmd.Parameters.Add(sqlpEMD_PHOTO)
        ''''''''''''EmpFilepathvirtual
        'Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
        'Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString

        'ViewState("EMPPHOTOFILEPATH") = GetBSUEmpFilePath()
        'ViewState("EMPPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & "\EMPPHOTO"
        'If h_EmpImagePath.Value <> "" Then
        '    Dim dirInfoBSUID As DirectoryInfo = Nothing
        '    Dim dirInfoEMPLOYEE As DirectoryInfo = Nothing
        '    If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH")) Then
        '        dirInfoBSUID = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH"))
        '    End If
        '    If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH") & "\" & STU_ID) Then
        '        dirInfoEMPLOYEE = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH") & "\" & STU_ID)
        '    End If
        '    'Dim fs As New FileInfo(Server.MapPath("../Temp/Image" & FUUploadEmpPhoto.FileName))
        '    Dim fs As New FileInfo(h_EmpImagePath.Value)
        '    Dim strFilePath As String = ViewState("EMPPHOTOFILEPATH") & "\" & STU_ID & "\EMPPHOTO" & fs.Extension
        '    'FUUploadEmpPhoto.PostedFile.SaveAs(strFilePath)
        '    'Dim strOriginFilepath As String = Server.MapPath("../Temp/EMPPHOTO" & fs.Extension)
        '    File.Copy(ViewState("tempimgpath"), strFilePath, True)
        '    'File.Delete(strOriginFilepath)
        '    imgEmpImage.ImageUrl = str_imgvirtual & "/" & Session("sbsuid") & "/empphoto/" & STU_ID & "/EMPPHOTO" & fs.Extension
        '    imgEmpImage.AlternateText = "No Image found"
        '    sqlpEMD_PHOTO.Value = imgEmpImage.ImageUrl
        'End If

    End Sub
    Function Final_validation() As Boolean
        Dim IS_ERROR As Boolean

        Dim commString As String = "The following fields needs to be validated: <UL>"
        If txtDOJ.Text.Trim <> "" Then
            Dim strfDate As String = txtDOJ.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                IS_ERROR = True
                commString = commString & "<LI>Invalid  date of join"
            Else
                txtDOJ.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtDOJ.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                'check for the leap year date
                If Not IsDate(dateTime1) Then
                    IS_ERROR = True

                    commString = commString & "<LI>Invalid  date of join"
                End If
            End If
        End If

        If txtMINDOJ.Text.Trim <> "" Then
            Dim strfDate As String = txtMINDOJ.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                IS_ERROR = True

                commString = commString & "<LI>Invalid  Ministry date of join"
            Else
                txtMINDOJ.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtMINDOJ.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                'check for the leap year date
                If Not IsDate(dateTime1) Then
                    IS_ERROR = True

                    commString = commString & "<LI>Invalid Ministry date of join"
                End If
            End If
        End If

        If txtEMIRATES_IDExp_date.Text.Trim <> "" Then
            Dim strfEmirExpDate As String = txtEMIRATES_IDExp_date.Text.Trim
            Dim str_EmirExperr As String = DateFunctions.checkdate(strfEmirExpDate)
            If str_EmirExperr <> "" Then
                commString = commString & "<LI>Emirates ID expiry date format is invalid"
                IS_ERROR = True
            Else
                txtEMIRATES_IDExp_date.Text = strfEmirExpDate
            End If
        End If

        '-----------------------------------Fathers EMIRATES_ID_EXPDATE date-----------------------------
        If txtFEMIRATES_ID_EXPDATE.Text.Trim <> "" Then
            Dim strFEMIRATES_ID_EXPDATE As String = txtFEMIRATES_ID_EXPDATE.Text.Trim
            Dim str_FEMIRATES_ID_EXPDATE As String = DateFunctions.checkdate(strFEMIRATES_ID_EXPDATE)
            If str_FEMIRATES_ID_EXPDATE <> "" Then
                commString = commString & "<LI>Please verify father's Emirates ID expiry date"
                IS_ERROR = True
            Else
                txtFEMIRATES_ID_EXPDATE.Text = strFEMIRATES_ID_EXPDATE
            End If
        End If
        '-----------------------------------Passport expire date date-----------------------------
        If txtMEMIRATES_ID_EXPDATE.Text.Trim <> "" Then
            Dim strMEMIRATES_ID_EXPDATE As String = txtMEMIRATES_ID_EXPDATE.Text.Trim
            Dim str_MEMIRATES_ID_EXPDATE As String = DateFunctions.checkdate(strMEMIRATES_ID_EXPDATE)
            If str_MEMIRATES_ID_EXPDATE <> "" Then
                commString = commString & "<LI>Please verify mother's Emirates ID expiry date"
                IS_ERROR = True
            Else
                txtMEMIRATES_ID_EXPDATE.Text = strMEMIRATES_ID_EXPDATE
            End If
        End If

        '-----------------------------------Visa Issue date date-----------------------------
        If txtGEMIRATES_ID_EXPDATE.Text.Trim <> "" Then
            Dim strGEMIRATES_ID_EXPDATE As String = txtGEMIRATES_ID_EXPDATE.Text.Trim
            Dim str_GEMIRATES_ID_EXPDATE As String = DateFunctions.checkdate(strGEMIRATES_ID_EXPDATE)
            If str_GEMIRATES_ID_EXPDATE <> "" Then
                commString = commString & "<LI>Please verify guardian's Emirates ID expiry date"
                IS_ERROR = True
            Else
                txtGEMIRATES_ID_EXPDATE.Text = strGEMIRATES_ID_EXPDATE
            End If
        End If



        If IS_ERROR = True Then
            lblError.Text = commString & "</UL>"
            Final_validation = False

        Else
            Final_validation = True

        End If

    End Function
    'Function SaveServices(ByVal trans As SqlTransaction) As Integer
    '    'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
    '    ' Dim str_query As String
    '    Dim i As Integer
    '    Dim lblSvcId As Label
    '    Dim SSV_FROMDATE As String = String.Format("{0:" & OASISConstants.DateFormat & "}", Date.Today)
    '    Dim rdY As RadioButton
    '    Try

    '        Dim flag As Integer
    '        Dim Status As Integer
    '        With gvServices
    '            For i = 0 To .Rows.Count - 1
    '                lblSvcId = .Rows(i).FindControl("lblSvcId")
    '                rdY = .Rows(i).FindControl("rdYes")
    '                If rdY.Checked = True Then
    '                    flag = 1
    '                Else
    '                    flag = 0
    '                End If


    '                '  str_query = "exec studSAVESTUSERVICES '" + Session("sbsuid").ToString + "'," + ViewState("viewid") + "," + Session("Current_ACD_ID") + "," + lblSvcId.Text.ToString + ",0,0,0," + rdY.Checked.ToString
    '                'SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    '                If flag = 1 Then
    '                    Status = AccessStudentClass.studSAVESTUDSERVICES(Session("sbsuid").ToString, Session("Current_ACD_ID").ToString, ViewState("viewid"), lblSvcId.Text.ToString, SSV_FROMDATE, trans)

    '                End If

    '            Next
    '        End With

    '        Return status

    '    Catch ex As Exception

    '        Return -1
    '    End Try

    'End Function

    Function callTrans_Student_M(ByVal trans As SqlTransaction) As Integer
        Try


            Dim status As Integer
            Dim STU_ID As String = ViewState("viewid")
            Dim STU_BSU_ID As String = Session("sBsuid")
            Dim STU_TFRTYPE As String = ddlTran_Type.SelectedItem.Value
            Dim STU_JOIN_RESULT As String = ddlJoinResult.SelectedItem.Value
            Dim STU_BLUEID As String = txtMOE_No.Text
            Dim STU_FIRSTNAME As String = txtFname_E.Text
            Dim STU_MIDNAME As String = txtMname_E.Text
            Dim STU_LASTNAME As String = txtLname_E.Text
            Dim STU_FIRSTNAMEARABIC As String = txtFname_A.Text
            Dim STU_MIDNAMEARABIC As String = txtMname_A.Text
            Dim STU_LASTNAMEARABIC As String = txtLname_A.Text
            Dim STU_DOB As String = txtDob.Text
            Dim STU_GENDER As String
            If rdFemale.Checked = True Then
                STU_GENDER = "F"
            Else
                STU_GENDER = "M"
            End If
            Dim STU_RLG_ID As String = ddlReligion.SelectedItem.Value
            Dim STU_NATIONALITY As String = ddlNationality_Birth.SelectedItem.Value
            Dim STU_POB As String = txtPob.Text
            Dim STU_COB As String = ddlCountry_Birth.SelectedItem.Value
            Dim STU_HOUSE As String = ddlHouse_BSU.SelectedItem.Value
            Dim STU_MINLIST As String = ddlMini_list.SelectedItem.Value
            Dim STU_MINLISTTYPE As String
            If ddlMini_list.SelectedItem.Value <> "Regular" Then
                STU_MINLISTTYPE = ddlMini_list_Type.SelectedItem.Value
            Else
                STU_MINLISTTYPE = ""
            End If

            Dim STU_FEESPONSOR As String = txtFee_Spon.Text
            Dim STU_EMGCONTACT As String
            ' If txtEmerg_Country.Text = "" Or txtEmerg_Area.Text = "" Or txtEmerg_PhoneNo.Text = "" Then
            '  STU_EMGCONTACT = ""
            ' Else
            STU_EMGCONTACT = txtEmerg_Country.Text & "-" & txtEmerg_Area.Text & "-" & txtEmerg_PhoneNo.Text
            ' End If
            'Dim STU_bDAYCARE As Boolean = rdDayCare_Yes.Checked
            'Dim STU_bCATERING As Boolean = rdYes_Cat.Checked

            Dim STU_PASPRTNAME As String = txtPassport_Name.Text
            Dim STU_PASPRTNO As String = txtPNo.Text
            Dim STU_PASPRTISSPLACE As String = txtPIssPlace.Text
            Dim STU_PASPRTISSDATE As String = txtPIssDate.Text
            Dim STU_PASPRTEXPDATE As String = txtPExpDate.Text
            Dim STU_VISANO As String = txtVNo.Text
            Dim STU_VISAISSPLACE As String = txtVIssPlace.Text
            Dim STU_VISAISSDATE As String = txtVIssDate.Text
            Dim STU_VISAEXPDATE As String = txtVExpDate.Text
            Dim STU_VISAISSAUTH As String = txtVIssAuth.Text
            Dim STU_MINDOJ As String = txtMINDOJ.Text
            Dim STU_PRIMARYCONTACT As String
            Dim STU_KNOWNNAME As String = txtSTU_KNOWNNAME.Text
            If rdFather.Checked = True Then
                STU_PRIMARYCONTACT = "F"

            ElseIf rdMother.Checked = True Then
                STU_PRIMARYCONTACT = "M"
            Else
                STU_PRIMARYCONTACT = "G"
            End If
            Dim STU_PREFCONTACT As String = ddlPrefContact.SelectedItem.Value
            Dim STU_PHOTOPATH As String = String.Empty

            Dim C As Boolean = rbPubYES.Checked
            Dim STU_SIBLING_ID As String = String.Empty
            If Trim(txtPar_Sib.Text) = "" Then
                STU_SIBLING_ID = h_SliblingID.Value

            Else
                STU_SIBLING_ID = h_sliblingID_Copy.Value
            End If


            'Dim sqlpEMD_PHOTO As New SqlParameter("@EMD_PHOTO", SqlDbType.VarChar, 200)
            'sqlpEMD_PHOTO.Value = ViewState("EMP_IMAGE_PATH")
            '' cmd.Parameters.Add(sqlpEMD_PHOTO)


            'ViewState("EMPPHOTOFILEPATH") = GetBSUEmpFilePath()
            'If h_EmpImagePath.Value <> "" Then
            '    Dim dirInfoBSUID As DirectoryInfo = Nothing
            '    Dim dirInfoEMPLOYEE As DirectoryInfo = Nothing
            '    'If Server.MapPath(ViewState("EMPPHOTOFILEPATH")) Then
            '    '    Response.Write("dddddd")
            '    'End If
            '    If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH")) Then
            '        dirInfoBSUID = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH"))
            '    End If
            '    If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH") & "\" & STU_ID) Then
            '        dirInfoEMPLOYEE = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH") & "\" & STU_ID)
            '    End If
            '    'Dim fs As New FileInfo(Server.MapPath("../Temp/Image" & FUUploadEmpPhoto.FileName))
            '    Dim fs As New FileInfo(h_EmpImagePath.Value)
            '    Dim strFilePath As String = Server.MapPath(ViewState("EMPPHOTOFILEPATH") & "\" & STU_ID & "\EMPPHOTO" & fs.Extension)

            '    'FUUploadEmpPhoto.PostedFile.SaveAs(strFilePath)
            '    Dim strOriginFilepath As String = Server.MapPath("../Temp/EMPPHOTO" & fs.Extension)
            '    File.Copy(strOriginFilepath, strFilePath, True)
            '    File.Delete(strOriginFilepath)
            '    sqlpEMD_PHOTO.Value = strFilePath
            'End If
            Dim PHOTO_PATH As String
            If ViewState("EMP_IMAGE_PATH") Is Nothing Then
                PHOTO_PATH = ""
            Else
                PHOTO_PATH = ViewState("EMP_IMAGE_PATH")
            End If
            'sqlpEMD_PHOTO.Value = ViewState("EMP_IMAGE_PATH")

            '''''''''''EmpFilepathvirtual
            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString

            ViewState("EMPPHOTOFILEPATH") = GetBSUEmpFilePath()
            ViewState("EMPPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & ""
            If h_EmpImagePath.Value <> "" Then
                ViewState("EMPPHOTOFILEPATH") = GetBSUEmpFilePath()
                ViewState("EMPPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & ""
                Dim dirInfoBSUID As DirectoryInfo = Nothing
                Dim dirInfoEMPLOYEE As DirectoryInfo = Nothing
                If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH")) Then
                    dirInfoBSUID = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH"))
                End If
                If Not Directory.Exists(ViewState("EMPPHOTOFILEPATH") & "\" & STU_ID) Then
                    dirInfoEMPLOYEE = Directory.CreateDirectory(ViewState("EMPPHOTOFILEPATH") & "\" & STU_ID)
                End If
                'Dim fs As New FileInfo(Server.MapPath("../Temp/Image" & FUUploadEmpPhoto.FileName))
                Dim fs As New FileInfo(h_EmpImagePath.Value)
                Dim strFilePath As String = ViewState("EMPPHOTOFILEPATH") & "\" & STU_ID & "\STUPHOTO" & fs.Extension
                'FUUploadEmpPhoto.PostedFile.SaveAs(strFilePath)
                'Dim strOriginFilepath As String = Server.MapPath("../Temp/EMPPHOTO" & fs.Extension)
                File.Copy(ViewState("tempimgpath"), strFilePath, True)

                'File.Delete(strOriginFilepath) 
                Dim strFileNameDB As String = "/" & Session("sbsuid") & "/" & STU_ID & "/STUPHOTO" & fs.Extension
                imgEmpImage.ImageUrl = str_imgvirtual & strFileNameDB & "?" & DateTime.Now.Ticks.ToString()
                imgEmpImage.AlternateText = "No Image found"
                PHOTO_PATH = strFileNameDB
            End If

            Dim STU_PREVSCHI As String = txtPSchool_Name1.Text
            Dim STU_PREVSCHI_CITY As String = txtPSchool_City1.Text
            Dim STU_PREVSCHI_COUNTRY As String = ddlPSchool_Country1.SelectedItem.Value
            Dim STU_PREVSCHI_CLM As String = ddlPBoard_Attend1.SelectedItem.Value
            Dim STU_PREVSCHI_GRADE As String = ddlPSchool_Year1.SelectedItem.Value
            Dim STU_PREVSCHI_LASTATTDATE As String = txtPTO_DT1.Text
            Dim STU_PREVSCHII As String = txtPSchool_Name2.Text
            Dim STU_PREVSCHII_CITY As String = txtPSchool_City2.Text
            Dim STU_PREVSCHII_COUNTRY As String = ddlPSchool_Country2.SelectedItem.Value
            Dim STU_PREVSCHII_CLM As String = ddlPBoard_Attend2.SelectedItem.Value
            Dim STU_PREVSCHII_GRADE As String = ddlPSchool_Year2.SelectedItem.Value
            Dim STU_PREVSCHII_LASTATTDATE As String = txtPTO_DT2.Text
            Dim STU_PREVSCHIII As String = txtPSchool_Name3.Text
            Dim STU_PREVSCHIII_CITY As String = txtPSchool_City3.Text
            Dim STU_PREVSCHIII_COUNTRY As String = ddlPSchool_Country3.SelectedItem.Value
            Dim STU_PREVSCHIII_CLM As String = ddlPBoard_Attend3.SelectedItem.Value
            Dim STU_PREVSCHIII_GRADE As String = ddlPSchool_Year3.SelectedItem.Value
            Dim STU_PREVSCHIII_LASTATTDATE As String = txtPTo_DT3.Text.Trim

            Dim STU_PREVSCHI_MEDIUM As String = txtPSchool_Medium1.Text
            Dim STU_PREVSCHII_MEDIUM As String = txtPSchool_Medium2.Text
            Dim STU_PREVSCHIII_MEDIUM As String = txtPSchool_Medium3.Text

            'CODE ADDED BY LIJO 07/JUN/2009
            Dim STU_COMMENT As String = txtStud_Comment.Text
            Dim STU_REMARKS As String = txtHthLS_Note.Text

            '---------------New fields added
            Dim STU_BLOODGROUP As String = ddlBgroup.SelectedItem.Value
            Dim STU_HCNO As String = txtHCard_No.Text

            Dim STU_bSEN As Boolean = rbHthSE_Yes.Checked
            Dim STU_SEN_REMARK As String = txtHthSE_Note.Text
            Dim STU_bEAL As Boolean = rbHthEAL_Yes.Checked
            Dim STU_EAL_REMARK As String = txtHthEAL_Note.Text
            Dim STU_bHEALTH As Boolean = rbHthOther_yes.Checked
            Dim STU_HEALTH As String = txtHthOth_Note.Text
            Dim STU_bPHYSICAL As Boolean = rbHthPER_Yes.Checked
            Dim STU_PHYSICAL As String = txtHthPER_Note.Text
            Dim STU_bRCVSPMEDICATION As Boolean = rbHthSM_Yes.Checked
            Dim STU_SPMEDICATION As String = txtHthSM_Note.Text
            Dim STU_bALLERGIES As Boolean = rbHthAll_Yes.Checked
            Dim STU_ALLERGIES As String = txtHthAll_Note.Text

            Dim STU_bTHERAPHY As Boolean = rbHthLS_Yes.Checked
            Dim STU_THERAPHY As String = txtHthLS_Note.Text
            Dim STU_bBEHAVIOUR As Boolean = rbHthBehv_Yes.Checked
            Dim STU_BEHAVIOUR As String = txtHthBehv_Note.Text
            Dim STU_bENRICH As Boolean = rbHthEnr_Yes.Checked
            Dim STU_ENRICH As String = txtHthEnr_note.Text
            Dim STU_bMUSICAL As Boolean = rbHthMus_Yes.Checked
            Dim STU_MUSICAL As String = txtHthMus_Note.Text
            Dim STU_bSPORTS As Boolean = rbHthSport_Yes.Checked
            Dim STU_SPORTS As String = txtHthSport_note.Text


            Dim STU_bCommInt As Boolean = rbHthComm_Yes.Checked
            Dim STU_CommInt As String = txtHthCommInt_Note.Text
            Dim STU_bDisabled As Boolean = rbHthDisabled_YES.Checked
            Dim STU_Disabled As String = txtHthDisabled_Note.Text



            Dim STU_bRCVPUBL As Boolean = rbPubYES.Checked
            Dim STU_bRCVSMS As Boolean = rbRcvSms_Yes.Checked
            Dim STU_bRCVMAIL As Boolean = rdRcvMail_Yes.Checked
            Dim STU_FIRSTLANG As String = ddlFLang.SelectedValue
            Dim STU_OTHLANG As String = String.Empty

            For Each item As ListItem In chkOLang.Items
                If (item.Selected) Then

                    STU_OTHLANG = STU_OTHLANG + item.Value + "|"
                End If
            Next

            Dim STU_ENG_READING As String
            Dim STU_ENG_WRITING As String
            Dim STU_ENG_SPEAKING As String

            If rbWExc.Checked = True Then
                STU_ENG_WRITING = "1"
            ElseIf rbWGood.Checked = True Then
                STU_ENG_WRITING = "2"
            ElseIf rbWFair.Checked = True Then
                STU_ENG_WRITING = "3"
            ElseIf rbWPoor.Checked = True Then
                STU_ENG_WRITING = "4"
            Else
                STU_ENG_WRITING = "0"
            End If

            If rbSExc.Checked = True Then
                STU_ENG_SPEAKING = "1"
            ElseIf rbSGood.Checked = True Then
                STU_ENG_SPEAKING = "2"
            ElseIf rbSFair.Checked = True Then
                STU_ENG_SPEAKING = "3"
            ElseIf rbSPoor.Checked = True Then
                STU_ENG_SPEAKING = "4"
            Else
                STU_ENG_SPEAKING = "0"
            End If


            If rbRExc.Checked = True Then
                STU_ENG_READING = "1"
            ElseIf rbRGood.Checked = True Then
                STU_ENG_READING = "2"
            ElseIf rbRFair.Checked = True Then
                STU_ENG_READING = "3"
            ElseIf rbRPoor.Checked = True Then
                STU_ENG_READING = "4"
            Else
                STU_ENG_READING = "0"
            End If

            Dim STU_GFATHERNAME As String = txtGFather.Text
            Dim STU_NAT_NO As String = txtNatNo.Text
            Dim STU_REFG_NO As String = txtRefugNo.Text


            Dim STU_DOB_ARABIC As String = txtDob_Arabic.Text
            Dim STU_POB_ARABIC As String = txtPob_Arabic.Text
            Dim STU_BCERT_NO As String = txtBC_NO.Text
            Dim STU_BCERT_ISSUE As String = txtBC_DOI.Text
            Dim STU_BCERT_DATE As String = txtBC_POI.Text
            Dim STU_FAMILY_NAME_ARABIC As String = txtFamilyName.Text
            Dim STU_EMIRATES_ID As String = txtEmiratesID.Text.Replace("'", "")
            Dim STU_EMIRATES_ID_EXPDATE As String = txtEMIRATES_IDExp_date.Text
            Dim STU_EMAIL As String = txtStu_Email.Text.Trim
            Dim SEMIRATES_ID_ENG As String = txtSEmiratesId_ENG.Text.Replace("'", "")
            Dim SEMIRATES_ID_ARB As String = txtSEmiratesId_ARB.Text.Replace("'", "")



            Dim pParms(131) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", STU_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@STU_TFRTYPE", STU_TFRTYPE)
            pParms(3) = New SqlClient.SqlParameter("@STU_BLUEID", STU_BLUEID)
            pParms(4) = New SqlClient.SqlParameter("@STU_FIRSTNAME", STU_FIRSTNAME)
            pParms(5) = New SqlClient.SqlParameter("@STU_MIDNAME", STU_MIDNAME)
            pParms(6) = New SqlClient.SqlParameter("@STU_LASTNAME", STU_LASTNAME)
            pParms(7) = New SqlClient.SqlParameter("@STU_FIRSTNAMEARABIC", STU_FIRSTNAMEARABIC)
            pParms(8) = New SqlClient.SqlParameter("@STU_MIDNAMEARABIC", STU_MIDNAMEARABIC)
            pParms(9) = New SqlClient.SqlParameter("@STU_LASTNAMEARABIC", STU_LASTNAMEARABIC)
            pParms(10) = New SqlClient.SqlParameter("@STU_DOB", STU_DOB)
            pParms(11) = New SqlClient.SqlParameter("@STU_GENDER", STU_GENDER)
            pParms(12) = New SqlClient.SqlParameter("@STU_RLG_ID", STU_RLG_ID)
            pParms(13) = New SqlClient.SqlParameter("@STU_NATIONALITY", STU_NATIONALITY)
            pParms(14) = New SqlClient.SqlParameter("@STU_POB", STU_POB)
            pParms(15) = New SqlClient.SqlParameter("@STU_COB", STU_COB)
            pParms(16) = New SqlClient.SqlParameter("@STU_HOUSE_ID", STU_HOUSE)
            pParms(17) = New SqlClient.SqlParameter("@STU_MINLIST", STU_MINLIST)
            pParms(18) = New SqlClient.SqlParameter("@STU_MINLISTTYPE", STU_MINLISTTYPE)
            pParms(19) = New SqlClient.SqlParameter("@STU_FEESPONSOR", STU_FEESPONSOR)
            pParms(20) = New SqlClient.SqlParameter("@STU_EMGCONTACT", STU_EMGCONTACT)

            pParms(21) = New SqlClient.SqlParameter("@STU_PASPRTNAME", STU_PASPRTNAME)
            pParms(22) = New SqlClient.SqlParameter("@STU_PASPRTNO", STU_PASPRTNO)
            pParms(23) = New SqlClient.SqlParameter("@STU_PASPRTISSPLACE", STU_PASPRTISSPLACE)
            pParms(24) = New SqlClient.SqlParameter("@STU_PASPRTISSDATE", STU_PASPRTISSDATE)
            pParms(25) = New SqlClient.SqlParameter("@STU_PASPRTEXPDATE", STU_PASPRTEXPDATE)
            pParms(26) = New SqlClient.SqlParameter("@STU_VISANO", STU_VISANO)
            pParms(27) = New SqlClient.SqlParameter("@STU_VISAISSPLACE", STU_VISAISSPLACE)
            pParms(28) = New SqlClient.SqlParameter("@STU_VISAISSDATE", STU_VISAISSDATE)
            pParms(29) = New SqlClient.SqlParameter("@STU_VISAEXPDATE", STU_VISAEXPDATE)
            pParms(30) = New SqlClient.SqlParameter("@STU_VISAISSAUTH", STU_VISAISSAUTH)
            pParms(31) = New SqlClient.SqlParameter("@STU_PRIMARYCONTACT", STU_PRIMARYCONTACT)
            pParms(32) = New SqlClient.SqlParameter("@STU_PREFCONTACT", STU_PREFCONTACT)
            pParms(33) = New SqlClient.SqlParameter("@STU_SIBLING_ID", STU_SIBLING_ID)
            pParms(34) = New SqlClient.SqlParameter("@STU_PREVSCHI", STU_PREVSCHI)
            pParms(35) = New SqlClient.SqlParameter("@STU_PREVSCHI_CITY", STU_PREVSCHI_CITY)
            pParms(36) = New SqlClient.SqlParameter("@STU_PREVSCHI_COUNTRY", STU_PREVSCHI_COUNTRY)
            pParms(37) = New SqlClient.SqlParameter("@STU_PREVSCHI_CLM", STU_PREVSCHI_CLM)
            pParms(38) = New SqlClient.SqlParameter("@STU_PREVSCHI_GRADE", STU_PREVSCHI_GRADE)
            If STU_PREVSCHI_LASTATTDATE.Trim = "" Then
                pParms(39) = New SqlClient.SqlParameter("@STU_PREVSCHI_LASTATTDATE", System.DBNull.Value)
            Else
                pParms(39) = New SqlClient.SqlParameter("@STU_PREVSCHI_LASTATTDATE", STU_PREVSCHI_LASTATTDATE)
            End If
            pParms(40) = New SqlClient.SqlParameter("@STU_PREVSCHII", STU_PREVSCHII)
            pParms(41) = New SqlClient.SqlParameter("@STU_PREVSCHII_CITY", STU_PREVSCHII_CITY)
            pParms(42) = New SqlClient.SqlParameter("@STU_PREVSCHII_COUNTRY", STU_PREVSCHII_COUNTRY)
            pParms(43) = New SqlClient.SqlParameter("@STU_PREVSCHII_CLM", STU_PREVSCHII_CLM)
            pParms(44) = New SqlClient.SqlParameter("@STU_PREVSCHII_GRADE", STU_PREVSCHII_GRADE)
            If STU_PREVSCHII_LASTATTDATE.Trim = "" Then
                pParms(45) = New SqlClient.SqlParameter("@STU_PREVSCHII_LASTATTDATE", System.DBNull.Value)
            Else
                pParms(45) = New SqlClient.SqlParameter("@STU_PREVSCHII_LASTATTDATE", STU_PREVSCHII_LASTATTDATE)
            End If
            pParms(46) = New SqlClient.SqlParameter("@STU_PREVSCHIII", STU_PREVSCHIII)
            pParms(47) = New SqlClient.SqlParameter("@STU_PREVSCHIII_CITY", STU_PREVSCHIII_CITY)
            pParms(48) = New SqlClient.SqlParameter("@STU_PREVSCHIII_COUNTRY", STU_PREVSCHIII_COUNTRY)
            pParms(49) = New SqlClient.SqlParameter("@STU_PREVSCHIII_CLM", STU_PREVSCHIII_CLM)
            pParms(50) = New SqlClient.SqlParameter("@STU_PREVSCHIII_GRADE", STU_PREVSCHIII_GRADE)
            If STU_PREVSCHIII_LASTATTDATE.Trim = "" Then
                pParms(51) = New SqlClient.SqlParameter("@STU_PREVSCHIII_LASTATTDATE", System.DBNull.Value)
            Else
                pParms(51) = New SqlClient.SqlParameter("@STU_PREVSCHIII_LASTATTDATE", STU_PREVSCHIII_LASTATTDATE)
            End If
            pParms(52) = New SqlClient.SqlParameter("@STU_PREVSCHI_MEDIUM", STU_PREVSCHI_MEDIUM)
            pParms(53) = New SqlClient.SqlParameter("@STU_PREVSCHII_MEDIUM", STU_PREVSCHII_MEDIUM)
            pParms(54) = New SqlClient.SqlParameter("@STU_PREVSCHIII_MEDIUM", STU_PREVSCHIII_MEDIUM)
            If STU_MINDOJ.Trim = "" Then
                pParms(55) = New SqlClient.SqlParameter("@STU_MINDOJ", System.DBNull.Value)
            Else
                pParms(55) = New SqlClient.SqlParameter("@STU_MINDOJ", STU_MINDOJ)
            End If
            pParms(56) = New SqlClient.SqlParameter("@user", Session("sUsr_name"))
            pParms(57) = New SqlClient.SqlParameter("@STU_COMMENT", STU_COMMENT)
            pParms(58) = New SqlClient.SqlParameter("@STU_KNOWNNAME", STU_KNOWNNAME)
            pParms(59) = New SqlClient.SqlParameter("@STU_REMARKS", STU_REMARKS)


            pParms(60) = New SqlClient.SqlParameter("@STU_PHOTOPATH", PHOTO_PATH)
            pParms(61) = New SqlClient.SqlParameter("@STU_bRCVSMS", STU_bRCVSMS)
            pParms(62) = New SqlClient.SqlParameter("@STU_bRCVMAIL", STU_bRCVMAIL)
            pParms(63) = New SqlClient.SqlParameter("@STU_bRCVPUBL", STU_bRCVPUBL)
            pParms(64) = New SqlClient.SqlParameter("@STU_BLOODGROUP", STU_BLOODGROUP)
            pParms(65) = New SqlClient.SqlParameter("@STU_HCNO", STU_HCNO)

            pParms(66) = New SqlClient.SqlParameter("@STU_bRCVSPMEDICATION", STU_bRCVSPMEDICATION)
            pParms(67) = New SqlClient.SqlParameter("@STU_SPMEDICATION", STU_SPMEDICATION)
            pParms(68) = New SqlClient.SqlParameter("@STU_bSEN", STU_bSEN)
            pParms(69) = New SqlClient.SqlParameter("@STU_SEN_REMARK", STU_SEN_REMARK)

            pParms(70) = New SqlClient.SqlParameter("@STU_bEAL", STU_bEAL)
            pParms(71) = New SqlClient.SqlParameter("@STU_EAL_REMARK", STU_EAL_REMARK)

            pParms(72) = New SqlClient.SqlParameter("@STU_bHEALTH", STU_bHEALTH)
            pParms(73) = New SqlClient.SqlParameter("@STU_HEALTH", STU_HEALTH)

            pParms(74) = New SqlClient.SqlParameter("@STU_bPHYSICAL", STU_bPHYSICAL)
            pParms(75) = New SqlClient.SqlParameter("@STU_PHYSICAL", STU_PHYSICAL)

            pParms(76) = New SqlClient.SqlParameter("@STU_bALLERGIES", STU_bALLERGIES)
            pParms(77) = New SqlClient.SqlParameter("@STU_ALLERGIES", STU_ALLERGIES)

            pParms(78) = New SqlClient.SqlParameter("@STU_bTHERAPHY", STU_bTHERAPHY)
            pParms(79) = New SqlClient.SqlParameter("@STU_THERAPHY", STU_THERAPHY)

            pParms(80) = New SqlClient.SqlParameter("@STU_bBEHAVIOUR", STU_bBEHAVIOUR)
            pParms(81) = New SqlClient.SqlParameter("@STU_BEHAVIOUR", STU_BEHAVIOUR)

            pParms(82) = New SqlClient.SqlParameter("@STU_bENRICH", STU_bENRICH)
            pParms(83) = New SqlClient.SqlParameter("@STU_ENRICH", STU_ENRICH)

            pParms(84) = New SqlClient.SqlParameter("@STU_bMUSICAL", STU_bMUSICAL)
            pParms(85) = New SqlClient.SqlParameter("@STU_MUSICAL", STU_MUSICAL)

            pParms(86) = New SqlClient.SqlParameter("@STU_bSPORTS", STU_bSPORTS)
            pParms(87) = New SqlClient.SqlParameter("@STU_SPORTS", STU_SPORTS)

            pParms(88) = New SqlClient.SqlParameter("@STU_FIRSTLANG", STU_FIRSTLANG)
            pParms(89) = New SqlClient.SqlParameter("@STU_OTHLANG", STU_OTHLANG)

            pParms(90) = New SqlClient.SqlParameter("@STU_ENG_READING", STU_ENG_READING)
            pParms(91) = New SqlClient.SqlParameter("@STU_ENG_WRITING", STU_ENG_WRITING)
            pParms(92) = New SqlClient.SqlParameter("@STU_ENG_SPEAKING", STU_ENG_SPEAKING)
            pParms(93) = New SqlClient.SqlParameter("@STU_GFATHERNAME", STU_GFATHERNAME)
            pParms(94) = New SqlClient.SqlParameter("@STU_NAT_NO", STU_NAT_NO)
            pParms(95) = New SqlClient.SqlParameter("@STU_RFG_NO", STU_REFG_NO)
            pParms(96) = New SqlClient.SqlParameter("@STU_DOB_ARABIC", STU_DOB_ARABIC)
            pParms(97) = New SqlClient.SqlParameter("@STU_POB_ARABIC", STU_POB_ARABIC)
            pParms(98) = New SqlClient.SqlParameter("@STU_BCERT_NO", STU_BCERT_NO)
            pParms(99) = New SqlClient.SqlParameter("@STU_BCERT_ISSUE", STU_BCERT_ISSUE)
            pParms(100) = New SqlClient.SqlParameter("@STU_BCERT_DATE", STU_BCERT_DATE)
            pParms(101) = New SqlClient.SqlParameter("@STU_FAMILY_NAME_ARABIC", STU_FAMILY_NAME_ARABIC)

            pParms(102) = New SqlClient.SqlParameter("@STU_bCommInt", STU_bCommInt)
            pParms(103) = New SqlClient.SqlParameter("@STU_CommInt", STU_CommInt)

            pParms(104) = New SqlClient.SqlParameter("@STU_bDisabled", STU_bDisabled)
            pParms(105) = New SqlClient.SqlParameter("@STU_Disabled", STU_Disabled)

            pParms(106) = New SqlClient.SqlParameter("@STU_JOIN_RESULT", STU_JOIN_RESULT)

            pParms(107) = New SqlClient.SqlParameter("@STU_EMIRATES_ID", txtEmiratesID.Text)

            pParms(108) = New SqlClient.SqlParameter("@stu_PremisesID", txtPremisesId.Text)

            pParms(109) = New SqlClient.SqlParameter("@STU_APPLEMIRATES_ID_EXPDATE", IIf(STU_EMIRATES_ID_EXPDATE = "", System.DBNull.Value, STU_EMIRATES_ID_EXPDATE))
            pParms(110) = New SqlClient.SqlParameter("@STU_EMAIL", IIf(STU_EMAIL = "", System.DBNull.Value, STU_EMAIL))

            pParms(120) = New SqlClient.SqlParameter("@STU_EMIRATES_ID_ENG", SEMIRATES_ID_ENG)
            pParms(121) = New SqlClient.SqlParameter("@STU_EMIRATES_ID_ARB", SEMIRATES_ID_ARB)

            pParms(111) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(111).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDENT_M", pParms)
            Dim ReturnFlag As Integer = pParms(111).Value
            Return ReturnFlag
        Catch ex As Exception
            Return 1000
        End Try




        '       status = AccessStudentClass.SaveStudSTUDENT_M(STU_ID, STU_BSU_ID, STU_TFRTYPE, STU_BLUEID, STU_FIRSTNAME, STU_MIDNAME, STU_LASTNAME, _
        'STU_FIRSTNAMEARABIC, STU_MIDNAMEARABIC, STU_LASTNAMEARABIC, STU_DOB, STU_GENDER, STU_RLG_ID, STU_NATIONALITY, STU_POB, STU_COB, _
        'STU_HOUSE, STU_MINLIST, STU_MINLISTTYPE, STU_FEESPONSOR, STU_EMGCONTACT, STU_HEALTH, _
        'STU_PHYSICAL, STU_bRCVSPMEDICATION, STU_SPMEDICATION, STU_PASPRTNAME, STU_PASPRTNO, STU_PASPRTISSPLACE, STU_PASPRTISSDATE, STU_PASPRTEXPDATE, _
        'STU_VISANO, STU_VISAISSPLACE, STU_VISAISSDATE, STU_VISAEXPDATE, STU_VISAISSAUTH, STU_BLOODGROUP, STU_HCNO, STU_PRIMARYCONTACT, _
        'STU_PREFCONTACT, PHOTO_PATH, STU_bRCVSMS, STU_bRCVMAIL, STU_SIBLING_ID, STU_PREVSCHI, STU_PREVSCHI_CITY, STU_PREVSCHI_COUNTRY, _
        'STU_PREVSCHI_CLM, STU_PREVSCHI_GRADE, STU_PREVSCHI_LASTATTDATE, STU_PREVSCHII, STU_PREVSCHII_CITY, STU_PREVSCHII_COUNTRY, _
        'STU_PREVSCHII_CLM, STU_PREVSCHII_GRADE, STU_PREVSCHII_LASTATTDATE, STU_PREVSCHIII, _
        'STU_PREVSCHIII_CITY, STU_PREVSCHIII_COUNTRY, STU_PREVSCHIII_CLM, STU_PREVSCHIII_GRADE, _
        'STU_PREVSCHIII_LASTATTDATE, STU_PREVSCHI_MEDIUM, STU_PREVSCHII_MEDIUM, STU_PREVSCHIII_MEDIUM, STU_MINDOJ, , STU_COMMENT, STU_KNOWNNAME, STU_REMARKS, _
        'STU_bSEN, STU_bEAL, STU_SEN_REMARK, STU_EAL_REMARK, STU_FIRSTLANG, STU_OTHLANG, STU_bRCVPUBL, trans)

        '       Return status
        '   Catch ex As Exception
        '       Return 1000
        '   End Try
    End Function
    Private Sub UpLoadPhoto()
        If FUUploadEmpPhoto.FileName <> "" And h_EmpImagePath.Value <> FUUploadEmpPhoto.FileName Then

            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            'imgEmpImage.ImageUrl = FUUploadEmpPhoto.PostedFile.FileName
            Dim fs As New FileInfo(FUUploadEmpPhoto.PostedFile.FileName)

            If Not Directory.Exists(str_img & "\temp") Then
                Directory.CreateDirectory(str_img & "\temp")
            End If

            Dim str_tempfilename As String = AccountFunctions.GetRandomString() & FUUploadEmpPhoto.FileName

            Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
            If FUUploadEmpPhoto.FileName <> "" Then
                FUUploadEmpPhoto.PostedFile.SaveAs(strFilepath)
                ImgHeightnWidth(strFilepath)
                ' Dim strFilepath As String = Server.UrlPathEncode(FUUploadEmpPhoto.PostedFile.FileName)
                'Dim strFilepath As String = "file://" & FUUploadEmpPhoto.PostedFile.FileName.Replace("\", "/")
                Try
                    If Not FUUploadEmpPhoto.PostedFile.ContentType.Contains("image") Then
                        Throw New Exception
                    End If
                    'Dim bt As New Drawing.Bitmap(FUUploadEmpPhoto.PostedFile.FileName)
                    imgEmpImage.ImageUrl = str_imgvirtual & "/temp/" & str_tempfilename & "?" & DateTime.Now.Ticks.ToString()
                    ViewState("tempimgpath") = strFilepath
                    imgEmpImage.AlternateText = "No Image found"
                Catch ex As Exception
                    'File.Delete(strFilepath)
                    h_EmpImagePath.Value = ""
                    UtilityObj.Errorlog("No Image found")
                    imgEmpImage.ImageUrl = Server.MapPath("~/Images/Photos/no_image.gif")
                    imgEmpImage.AlternateText = "No Image found"
                End Try
            End If

        End If
    End Sub


    Private Sub UpLoadFatherPhoto()
        If FUUploadFatherPhoto.FileName <> "" And h_FImagePath.Value <> FUUploadFatherPhoto.FileName Then

            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            'imgEmpImage.ImageUrl = FUUploadEmpPhoto.PostedFile.FileName
            Dim fs As New FileInfo(FUUploadFatherPhoto.PostedFile.FileName)

            If Not Directory.Exists(str_img & "\temp") Then
                Directory.CreateDirectory(str_img & "\temp")
            End If

            Dim str_tempfilename As String = AccountFunctions.GetRandomString() & FUUploadFatherPhoto.FileName

            Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
            If FUUploadFatherPhoto.FileName <> "" Then
                FUUploadFatherPhoto.PostedFile.SaveAs(strFilepath)
                ImgHeightnWidth(strFilepath)
                ' Dim strFilepath As String = Server.UrlPathEncode(FUUploadEmpPhoto.PostedFile.FileName)
                'Dim strFilepath As String = "file://" & FUUploadEmpPhoto.PostedFile.FileName.Replace("\", "/")
                Try
                    If Not FUUploadFatherPhoto.PostedFile.ContentType.Contains("image") Then
                        Throw New Exception
                    End If
                    'Dim bt As New Drawing.Bitmap(FUUploadEmpPhoto.PostedFile.FileName)
                    imgFather.ImageUrl = str_imgvirtual & "/temp/" & str_tempfilename & "?" & DateTime.Now.Ticks.ToString()
                    ViewState("tempFatherimgpath") = strFilepath
                    imgFather.AlternateText = "No Image found"
                Catch ex As Exception
                    'File.Delete(strFilepath)
                    h_FImagePath.Value = ""
                    UtilityObj.Errorlog("No Image found")
                    imgFather.ImageUrl = Server.MapPath("~/Images/Photos/no_image.gif")
                    imgFather.AlternateText = "No Image found"
                End Try
            End If

        End If
    End Sub

    Private Sub UpLoadMotherPhoto()
        If FUUploadMotherPhoto.FileName <> "" And h_MImagePath.Value <> FUUploadMotherPhoto.FileName Then

            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            'imgEmpImage.ImageUrl = FUUploadEmpPhoto.PostedFile.FileName
            Dim fs As New FileInfo(FUUploadMotherPhoto.PostedFile.FileName)

            If Not Directory.Exists(str_img & "\temp") Then
                Directory.CreateDirectory(str_img & "\temp")
            End If

            Dim str_tempfilename As String = AccountFunctions.GetRandomString() & FUUploadMotherPhoto.FileName

            Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
            If FUUploadMotherPhoto.FileName <> "" Then
                FUUploadMotherPhoto.PostedFile.SaveAs(strFilepath)
                ImgHeightnWidth(strFilepath)
                ' Dim strFilepath As String = Server.UrlPathEncode(FUUploadEmpPhoto.PostedFile.FileName)
                'Dim strFilepath As String = "file://" & FUUploadEmpPhoto.PostedFile.FileName.Replace("\", "/")
                Try
                    If Not FUUploadMotherPhoto.PostedFile.ContentType.Contains("image") Then
                        Throw New Exception
                    End If
                    'Dim bt As New Drawing.Bitmap(FUUploadEmpPhoto.PostedFile.FileName)
                    imgMother.ImageUrl = str_imgvirtual & "/temp/" & str_tempfilename & "?" & DateTime.Now.Ticks.ToString()
                    ViewState("tempMotherimgpath") = strFilepath
                    imgMother.AlternateText = "No Image found"
                Catch ex As Exception
                    'File.Delete(strFilepath)
                    h_MImagePath.Value = ""
                    UtilityObj.Errorlog("No Image found")
                    imgMother.ImageUrl = Server.MapPath("~/Images/Photos/no_image.gif")
                    imgMother.AlternateText = "No Image found"
                End Try
            End If

        End If
    End Sub

    Private Sub UpLoadGuardianPhoto()
        If FLUGuradian.FileName <> "" And h_GImagePath.Value <> FLUGuradian.FileName Then

            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            'imgEmpImage.ImageUrl = FUUploadEmpPhoto.PostedFile.FileName
            Dim fs As New FileInfo(FLUGuradian.PostedFile.FileName)

            If Not Directory.Exists(str_img & "\temp") Then
                Directory.CreateDirectory(str_img & "\temp")
            End If

            Dim str_tempfilename As String = AccountFunctions.GetRandomString() & FLUGuradian.FileName

            Dim strFilepath As String = str_img & "\temp\" & str_tempfilename
            If FLUGuradian.FileName <> "" Then
                FLUGuradian.PostedFile.SaveAs(strFilepath)
                ImgHeightnWidth(strFilepath)
                ' Dim strFilepath As String = Server.UrlPathEncode(FUUploadEmpPhoto.PostedFile.FileName)
                'Dim strFilepath As String = "file://" & FUUploadEmpPhoto.PostedFile.FileName.Replace("\", "/")
                Try
                    If Not FLUGuradian.PostedFile.ContentType.Contains("image") Then
                        Throw New Exception
                    End If
                    'Dim bt As New Drawing.Bitmap(FUUploadEmpPhoto.PostedFile.FileName)
                    imgGuardian.ImageUrl = str_imgvirtual & "/temp/" & str_tempfilename & "?" & DateTime.Now.Ticks.ToString()
                    ViewState("tempGuardianimgpath") = strFilepath
                    imgGuardian.AlternateText = "No Image found"
                Catch ex As Exception
                    'File.Delete(strFilepath)
                    h_GImagePath.Value = ""
                    UtilityObj.Errorlog("No Image found")
                    imgGuardian.ImageUrl = Server.MapPath("~/Images/Photos/no_image.gif")
                    imgGuardian.AlternateText = "No Image found"
                End Try
            End If

        End If
    End Sub

    Function callTrans_Student_D(ByVal trans As SqlTransaction) As Integer

        Try
            Dim status As Integer

            Dim STS_STU_ID As String = String.Empty
            'If Trim(txtPar_Sib.Text) = "" Then
            '    STS_STU_ID = ViewState("viewid")
            'Else
            STS_STU_ID = h_SliblingID.Value
            ' End If

            Dim STS_FFIRSTNAME As String = txtFFirstName.Text
            Dim STS_FMIDNAME As String = txtFMidName.Text
            Dim STS_FLASTNAME As String = txtFLastName.Text


            ''by nahyan on 7thDec2014 to add parent photo

            Dim PHOTO_PATH_FATHER As String
            If ViewState("FATHER_PHOTO_PATH") Is Nothing Then
                PHOTO_PATH_FATHER = ""
            Else
                PHOTO_PATH_FATHER = ViewState("FATHER_PHOTO_PATH")
            End If
            'sqlpEMD_PHOTO.Value = ViewState("EMP_IMAGE_PATH")

            '''''''''''EmpFilepathvirtual
            Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString

            ViewState("EMPFATHERPHOTOFILEPATH") = GetBSUEmpFilePath()
            ViewState("EMPFATHERPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & ""
            If h_FImagePath.Value <> "" Then
                ViewState("EMPFATHERPHOTOFILEPATH") = GetBSUEmpFilePath()
                ViewState("EMPFATHERPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & ""
                Dim dirInfoBSUID As DirectoryInfo = Nothing
                Dim dirInfoEMPLOYEE As DirectoryInfo = Nothing
                If Not Directory.Exists(ViewState("EMPFATHERPHOTOFILEPATH")) Then
                    dirInfoBSUID = Directory.CreateDirectory(ViewState("EMPFATHERPHOTOFILEPATH"))
                End If
                If Not Directory.Exists(ViewState("EMPFATHERPHOTOFILEPATH") & "\" & STS_STU_ID & "\c\EMIRATESID\FATHER\") Then
                    dirInfoEMPLOYEE = Directory.CreateDirectory(ViewState("EMPFATHERPHOTOFILEPATH") & "\" & STS_STU_ID & "\STUPHOTO\EMIRATESID\FATHER\")
                End If

                Dim fs1 As New FileInfo(h_FImagePath.Value)
                Dim strFilePath As String = ViewState("EMPFATHERPHOTOFILEPATH") & "\" & STS_STU_ID & "\STUPHOTO\EMIRATESID\FATHER\PARENTPHOTO" & fs1.Extension

                File.Copy(ViewState("tempFatherimgpath"), strFilePath, True)

                'File.Delete(strOriginFilepath) 
                Dim strFileNameDB As String = "/" & Session("sbsuid") & "/" & STS_STU_ID & "/STUPHOTO/EMIRATESID/FATHER/PARENTPHOTO" & fs1.Extension
                imgFather.ImageUrl = str_imgvirtual & strFileNameDB & "?" & DateTime.Now.Ticks.ToString()
                imgFather.AlternateText = "No Image found"
                PHOTO_PATH_FATHER = strFileNameDB
            End If


            Dim PHOTO_PATH_MOTHER As String
            If ViewState("MOTHER_PHOTO_PATH") Is Nothing Then
                PHOTO_PATH_MOTHER = ""
            Else
                PHOTO_PATH_MOTHER = ViewState("MOTHER_PHOTO_PATH")
            End If
            'sqlpEMD_PHOTO.Value = ViewState("EMP_IMAGE_PATH")



            ViewState("EMPMOTHERPHOTOFILEPATH") = GetBSUEmpFilePath()
            ViewState("EMPMOTHERPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & ""
            If h_MImagePath.Value <> "" Then
                ViewState("EMPMOTHERPHOTOFILEPATH") = GetBSUEmpFilePath()
                ViewState("EMPMOTHERPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & ""
                Dim dirInfoBSUID1 As DirectoryInfo = Nothing
                Dim dirInfoEMPLOYEE1 As DirectoryInfo = Nothing
                If Not Directory.Exists(ViewState("EMPMOTHERPHOTOFILEPATH")) Then
                    dirInfoBSUID1 = Directory.CreateDirectory(ViewState("EMPMOTHERPHOTOFILEPATH"))
                End If
                If Not Directory.Exists(ViewState("EMPMOTHERPHOTOFILEPATH") & "\" & STS_STU_ID & "\STUPHOTO\EMIRATESID\MOTHER\") Then
                    dirInfoEMPLOYEE1 = Directory.CreateDirectory(ViewState("EMPMOTHERPHOTOFILEPATH") & "\" & STS_STU_ID & "\STUPHOTO\EMIRATESID\MOTHER\")
                End If

                Dim fs2 As New FileInfo(h_MImagePath.Value)
                Dim strFilePath As String = ViewState("EMPMOTHERPHOTOFILEPATH") & "\" & STS_STU_ID & "\STUPHOTO\EMIRATESID\MOTHER\PARENTPHOTO" & fs2.Extension

                File.Copy(ViewState("tempMotherimgpath"), strFilePath, True)

                'File.Delete(strOriginFilepath) 
                Dim strFileNameDB1 As String = "/" & Session("sbsuid") & "/" & STS_STU_ID & "/STUPHOTO/EMIRATESID/MOTHER/PARENTPHOTO" & fs2.Extension
                imgMother.ImageUrl = str_imgvirtual & strFileNameDB1 & "?" & DateTime.Now.Ticks.ToString()
                imgMother.AlternateText = "No Image found"
                PHOTO_PATH_MOTHER = strFileNameDB1
            End If




            Dim PHOTO_PATH_GUARDIAN As String
            If ViewState("GUARDIAN_PHOTO_PATH") Is Nothing Then
                PHOTO_PATH_GUARDIAN = ""
            Else
                PHOTO_PATH_GUARDIAN = ViewState("GUARDIAN_PHOTO_PATH")
            End If
            'sqlpEMD_PHOTO.Value = ViewState("EMP_IMAGE_PATH")



            ViewState("EMPGUARDIANPHOTOFILEPATH") = GetBSUEmpFilePath()
            ViewState("EMPGUARDIANPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & ""
            If h_GImagePath.Value <> "" Then
                ViewState("EMPGUARDIANPHOTOFILEPATH") = GetBSUEmpFilePath()
                ViewState("EMPGUARDIANPHOTOFILEPATH") = str_img & "\" & Session("sbsuid") & ""
                Dim dirInfoBSUID2 As DirectoryInfo = Nothing
                Dim dirInfoEMPLOYEE2 As DirectoryInfo = Nothing
                If Not Directory.Exists(ViewState("EMPGUARDIANPHOTOFILEPATH")) Then
                    dirInfoBSUID2 = Directory.CreateDirectory(ViewState("EMPGUARDIANPHOTOFILEPATH"))
                End If
                If Not Directory.Exists(ViewState("EMPGUARDIANPHOTOFILEPATH") & "\" & STS_STU_ID & "\STUPHOTO\EMIRATESID\GUARDIAN\") Then
                    dirInfoEMPLOYEE2 = Directory.CreateDirectory(ViewState("EMPGUARDIANPHOTOFILEPATH") & "\" & STS_STU_ID & "\STUPHOTO\EMIRATESID\GUARDIAN\")
                End If

                Dim fs3 As New FileInfo(h_GImagePath.Value)
                Dim strFilePath As String = ViewState("EMPGUARDIANPHOTOFILEPATH") & "\" & STS_STU_ID & "\STUPHOTO\EMIRATESID\GUARDIAN\PARENTPHOTO" & fs3.Extension

                File.Copy(ViewState("tempGuardianimgpath"), strFilePath, True)

                'File.Delete(strOriginFilepath) 
                Dim strFileNameDB2 As String = "/" & Session("sbsuid") & "/" & STS_STU_ID & "/STUPHOTO/EMIRATESID/GUARDIAN/PARENTPHOTO" & fs3.Extension
                imgGuardian.ImageUrl = str_imgvirtual & strFileNameDB2 & "?" & DateTime.Now.Ticks.ToString()
                imgGuardian.AlternateText = "No Image found"
                PHOTO_PATH_GUARDIAN = strFileNameDB2
            End If



            ''nahyan code ends here on 7thDec2014

            Dim STS_FNATIONALITY As String = ddlFNationality1.SelectedItem.Value
            Dim STS_FNATIONALITY2 As String = ddlFNationality2.SelectedItem.Value

            Dim STS_FCOMADDR1 As String = txtFCOMADDR1.Text


            Dim STS_FCOMPOBOX As String = txtFCOMPOBOX.Text

            Dim STS_FCOMCOUNTRY As String = ddlFCOMCountry.SelectedItem.Value
            Dim STS_FOFFPHONE As String

            STS_FOFFPHONE = txtFOffPhone_Country.Text & "-" & txtFOffPhone_Area.Text & "-" & txtFOffPhone_No.Text

            Dim STS_FRESPHONE As String

            STS_FRESPHONE = txtFResPhone_Country.Text & "-" & txtFResPhone_Area.Text & "-" & txtFResPhone_No.Text


            Dim STS_FFAX As String

            STS_FFAX = txtFFax_Country.Text & "-" & txtFFax_Area.Text & "-" & txtFFax_No.Text



            Dim STS_FMOBILE As String

            STS_FMOBILE = txtFMobile_Country.Text & "-" & txtFMobile_Area.Text & "-" & txtFMobile_No.Text


            Dim STS_FPRMADDR1 As String = txtFPRMAddr1.Text
            Dim STS_FPRMADDR2 As String = txtFPRMAddr2.Text
            Dim STS_FPRMPOBOX As String = txtFPRMPOBOX.Text
            Dim STS_FPRMCITY As String = txtFPRM_City.Text
            Dim STS_FPRMCOUNTRY As String = ddlFPRM_Country.SelectedItem.Value
            Dim STS_FPRMPHONE As String

            STS_FPRMPHONE = txtFPPRM_Country.Text & "-" & txtFPPRM_Area.Text & "-" & txtFPPRM_No.Text

            Dim STS_FOCC As String = txtFOcc.Text



            Dim STS_bFGEMSEMP As Boolean = chkbFGEMSEMP.Checked
            Dim STS_F_BSU_ID As String
            If chkbFGEMSEMP.Checked Then
                STS_F_BSU_ID = ddlF_BSU_ID.SelectedItem.Value
            Else
                STS_F_BSU_ID = ""
            End If

            'code added by dhanya 29 apr 08
            Dim STS_FCOMPANY_ADDR As String = txtFComp_Add.Text
            Dim STS_FEMAIL As String = txtFEmail.Text


            Dim STS_MFIRSTNAME As String = txtMFirstName.Text
            Dim STS_MMIDNAME As String = txtMMidName.Text
            Dim STS_MLASTNAME As String = txtMLastName.Text
            Dim STS_MNATIONALITY As String = ddlMNationality1.SelectedItem.Value
            Dim STS_MNATIONALITY2 As String = ddlMNationality2.SelectedItem.Value

            Dim STS_MCOMADDR1 As String = txtMCOMADDR1.Text


            Dim STS_MCOMPOBOX As String = txtMCOMPOBOX.Text

            Dim STS_MCOMCOUNTRY As String = ddlMCOMCountry.SelectedItem.Value
            Dim STS_MOFFPHONE As String
            'If txtMOffPhone_Country.Text = "" Or txtMOffPhone_Area.Text = "" Or txtMOffPhone_No.Text = "" Then
            '    STS_MOFFPHONE = ""
            'Else
            STS_MOFFPHONE = txtMOffPhone_Country.Text & "-" & txtMOffPhone_Area.Text & "-" & txtMOffPhone_No.Text
            'End If

            Dim STS_MRESPHONE As String
            'If txtMResPhone_Country.Text = "" Or txtMResPhone_Area.Text = "" Or txtMResPhone_No.Text = "" Then
            '    STS_MRESPHONE = ""
            'Else
            STS_MRESPHONE = txtMResPhone_Country.Text & "-" & txtMResPhone_Area.Text & "-" & txtMResPhone_No.Text
            'End If

            Dim STS_MFAX As String
            'If txtMFax_Country.Text = "" Or txtMFax_Area.Text = "" Or txtMFax_No.Text = "" Then
            '    STS_MFAX = ""
            'Else
            STS_MFAX = txtMFax_Country.Text & "-" & txtMFax_Area.Text & "-" & txtMFax_No.Text
            '  End If

            Dim STS_MMOBILE As String
            'If txtMMobile_Country.Text = "" Or txtMMobile_Area.Text = "" Or txtMMobile_No.Text = "" Then
            '    STS_MMOBILE = ""
            'Else
            STS_MMOBILE = txtMMobile_Country.Text & "-" & txtMMobile_Area.Text & "-" & txtMMobile_No.Text
            'End If

            Dim STS_MPRMADDR1 As String = txtMPRMAddr1.Text

            Dim STS_MPRMADDR2 As String = txtMPRMAddr2.Text
            Dim STS_MPRMPOBOX As String = txtMPRMPOBOX.Text
            Dim STS_MPRMCITY As String = txtMPRM_City.Text
            Dim STS_MPRMCOUNTRY As String = ddlMPRM_Country.SelectedItem.Value
            Dim STS_MPRMPHONE As String
            'If txtMPPermant_Country.Text = "" Or txtMPPermant_Area.Text = "" Or txtMPPermant_No.Text = "" Then
            '    STS_MPRMPHONE = ""
            'Else
            STS_MPRMPHONE = txtMPPermant_Country.Text & "-" & txtMPPermant_Area.Text & "-" & txtMPPermant_No.Text
            'End If
            Dim STS_MOCC As String = txtMOcc.Text



            Dim STS_bMGEMSEMP As Boolean = chkbMGEMSEMP.Checked
            Dim STS_M_BSU_ID As String
            If chkbMGEMSEMP.Checked Then
                STS_M_BSU_ID = ddlM_BSU_ID.SelectedItem.Value
            Else
                STS_M_BSU_ID = ""
            End If



            'code added by dhanya 29 apr 08
            Dim STS_MCOMPANY_ADDR As String = txtMComp_Add.Text
            Dim STS_MEMAIL As String = txtMEmail.Text




            Dim STS_GFIRSTNAME As String = txtGFirstName.Text
            Dim STS_GMIDNAME As String = txtGMidName.Text
            Dim STS_GLASTNAME As String = txtGLastName.Text
            Dim STS_GNATIONALITY As String = ddlGNationality1.SelectedItem.Value
            Dim STS_GNATIONALITY2 As String = ddlGNationality2.SelectedItem.Value
            Dim STS_GCOMADDR1 As String = txtGCOMADDR1.Text

            Dim STS_GCOMPOBOX As String = txtGCOMPOBOX.Text


            Dim STS_GCOMCOUNTRY As String = ddlGCOMCountry.SelectedItem.Value
            Dim STS_GOFFPHONE As String
            'If txtGOffPhone_Country.Text = "" Or txtGOffPhone_Area.Text = "" Or txtGOffPhone_No.Text = "" Then
            '    STS_GOFFPHONE = ""
            'Else
            STS_GOFFPHONE = txtGOffPhone_Country.Text & "-" & txtGOffPhone_Area.Text & "-" & txtGOffPhone_No.Text
            'End If


            Dim STS_GRESPHONE As String
            'If txtGResPhone_Country.Text = "" Or txtGResPhone_Area.Text = "" Or txtGResPhone_No.Text = "" Then
            '    STS_GRESPHONE = ""
            'Else
            STS_GRESPHONE = txtGResPhone_Country.Text & "-" & txtGResPhone_Area.Text & "-" & txtGResPhone_No.Text
            ' End If

            Dim STS_GFAX As String
            'If txtGFax_Country.Text = "" Or txtGFax_Area.Text = "" Or txtGFax_No.Text = "" Then
            '    STS_GFAX = ""
            'Else
            STS_GFAX = txtGFax_Country.Text & "-" & txtGFax_Area.Text & "-" & txtGFax_No.Text
            ' End If

            Dim STS_GMOBILE As String
            'If txtGMobile_Country.Text = "" Or txtGMobile_Area.Text = "" Or txtGMobile_No.Text = "" Then
            '    STS_GMOBILE = ""
            'Else
            STS_GMOBILE = txtGMobile_Country.Text & "-" & txtGMobile_Area.Text & "-" & txtGMobile_No.Text
            ' End If
            Dim STS_GPRMADDR1 As String = txtGPRMAddr1.Text
            Dim STS_GPRMADDR2 As String = txtGPRMAddr2.Text
            Dim STS_GPRMPOBOX As String = txtGPRMPOBOX.Text
            Dim STS_GPRMCITY As String = txtGPRM_City.Text
            Dim STS_GPRMCOUNTRY As String = ddlGPRM_Country.SelectedItem.Value
            Dim STS_GPRMPHONE As String
            'If txtGPPERM_Country.Text = "" Or txtGPPERM_Area.Text = "" Or txtGPPERM_No.Text = "" Then
            '    STS_GPRMPHONE = ""
            'Else
            STS_GPRMPHONE = txtGPPERM_Country.Text & "-" & txtGPPERM_Area.Text & "-" & txtGPPERM_No.Text
            ' End If

            'code added by dhanya 29 apr 08
            Dim STS_GCOMPANY_ADDR As String = txtGComp_Add.Text
            Dim STS_GEMAIL As String = txtGEmail.Text
            Dim STS_GOCC As String = txtGOcc.Text
            'code added by Lijo 25 May 08 field added(STS_F_COMP_ID ,STS_M_COMP_ID,STS_G_COMP_ID)
            Dim STS_FCOMPANY As String
            Dim STS_MCOMPANY As String
            Dim STS_GCOMPANY As String
            Dim STS_F_COMP_ID As String
            Dim STS_M_COMP_ID As String
            Dim STS_G_COMP_ID As String












            Dim STS_FCOMAREA_ID As String = ddlFArea.SelectedValue ' IIf(ddlFArea.SelectedValue = 0, "-1", ddlFArea.SelectedValue)
            Dim STS_FCOMSTATE_ID As String = ddlFCity.SelectedValue ' IIf(ddlFCity.SelectedValue = 0, "-1", ddlFCity.SelectedValue)

            Dim STS_MCOMAREA_ID As String = ddlMArea.SelectedValue ' IIf(ddlMArea.SelectedValue = 0, "-1", ddlMArea.SelectedValue)
            Dim STS_MCOMSTATE_ID As String = ddlMCity.SelectedValue ' IIf(ddlMCity.SelectedValue = 0, "-1", ddlMCity.SelectedValue)

            Dim STS_GCOMAREA_ID As String = ddlGArea.SelectedValue ' IIf(ddlGArea.SelectedValue = 0, "-1", ddlGArea.SelectedValue)
            Dim STS_GCOMSTATE_ID As String = ddlGCity.SelectedValue 'IIf(ddlGCity.SelectedValue = 0, "-1", ddlGCity.SelectedValue)

            Dim STS_FCOMAREA As String = String.Empty
            Dim STS_FCOMADDR2 As String = String.Empty
            Dim STS_FCOMCITY As String = String.Empty
            Dim STS_FCOMSTATE As String = String.Empty

            If ddlFArea.SelectedValue = "0" Then
                STS_FCOMAREA = txtFCOMADDR2.Text
                STS_FCOMADDR2 = txtFCOMADDR2.Text
                STS_FCOMCITY = txtFCOMCity.Text
                STS_FCOMSTATE = txtFCOMCity.Text

            ElseIf ddlFArea.SelectedValue = "-1" Then
                STS_FCOMAREA = txtFCOMADDR2.Text
                STS_FCOMADDR2 = txtFCOMADDR2.Text
                STS_FCOMCITY = txtFCOMCity.Text
                STS_FCOMSTATE = txtFCOMCity.Text

            Else
                STS_FCOMAREA = ddlFArea.SelectedItem.Text
                STS_FCOMADDR2 = ddlFArea.SelectedItem.Text
                STS_FCOMCITY = ddlFCity.SelectedItem.Text
                STS_FCOMSTATE = ddlFCity.SelectedItem.Text
            End If

            Dim STS_MCOMAREA As String = String.Empty
            Dim STS_MCOMADDR2 As String = String.Empty

            Dim STS_MCOMCITY As String = String.Empty
            Dim STS_MCOMSTATE As String = String.Empty

            If ddlMArea.SelectedValue = "0" Then
                STS_MCOMAREA = txtMCOMADDR2.Text
                STS_MCOMADDR2 = txtMCOMADDR2.Text
                STS_MCOMCITY = txtMCOMCity.Text
                STS_MCOMSTATE = txtMCOMCity.Text

            ElseIf ddlMArea.SelectedValue = "-1" Then
                STS_MCOMAREA = txtMCOMADDR2.Text
                STS_MCOMADDR2 = txtMCOMADDR2.Text
                STS_MCOMCITY = txtMCOMCity.Text
                STS_MCOMSTATE = txtMCOMCity.Text


            Else
                If ddlMArea.SelectedValue <> "" Then
                    STS_MCOMAREA = ddlMArea.SelectedItem.Text
                    STS_MCOMADDR2 = ddlMArea.SelectedItem.Text
                    STS_MCOMCITY = ddlMCity.SelectedItem.Text
                    STS_MCOMSTATE = ddlMCity.SelectedItem.Text
                End If

            End If




            Dim STS_GCOMAREA As String = String.Empty
            Dim STS_GCOMADDR2 As String = String.Empty
            Dim STS_GCOMCITY As String = String.Empty
            Dim STS_GCOMSTATE As String = String.Empty


            If ddlGArea.SelectedValue = "0" Then
                STS_GCOMAREA = txtGCOMADDR2.Text
                STS_GCOMADDR2 = txtGCOMADDR2.Text
                STS_GCOMCITY = txtGCOMCity.Text
                STS_GCOMSTATE = txtGCOMCity.Text

            ElseIf ddlGArea.SelectedValue = "-1" Then
                STS_GCOMAREA = txtGCOMADDR2.Text
                STS_GCOMADDR2 = txtGCOMADDR2.Text
                STS_GCOMCITY = txtGCOMCity.Text
                STS_GCOMSTATE = txtGCOMCity.Text



            Else
                If ddlGArea.SelectedValue <> "" Then
                    STS_GCOMAREA = ddlGArea.SelectedItem.Text
                    STS_GCOMADDR2 = ddlGArea.SelectedItem.Text
                    STS_GCOMCITY = ddlGCity.SelectedItem.Text
                    STS_GCOMCITY = ddlGCity.SelectedItem.Text
                End If

            End If












            If ddlFCompany_Name.SelectedItem.Text = "Other" Then
                STS_FCOMPANY = txtFComp_Name.Text
                STS_F_COMP_ID = "0"
            Else
                STS_FCOMPANY = ""
                STS_F_COMP_ID = ddlFCompany_Name.SelectedItem.Value
            End If

            If ddlMCompany_Name.SelectedItem.Text = "Other" Then
                STS_MCOMPANY = txtMComp_Name.Text
                STS_M_COMP_ID = "0"
            Else
                STS_MCOMPANY = ""
                STS_M_COMP_ID = ddlMCompany_Name.SelectedItem.Value
            End If

            If ddlGCompany_Name.SelectedItem.Text = "Other" Then
                STS_GCOMPANY = txtGComp_Name.Text
                STS_G_COMP_ID = "0"
            Else
                STS_GCOMPANY = ""
                STS_G_COMP_ID = ddlGCompany_Name.SelectedItem.Value
            End If

            Dim STS_FCOMSTREET As String = txtFCOMADDR1.Text

            Dim STS_FCOMBLDG As String = txtFBldg.Text
            Dim STS_FCOMAPARTNO As String = txtFAptNo.Text
            Dim STS_FEMIR As String = ddlFCOMEmirate.SelectedValue

            Dim STS_MCOMSTREET As String = txtMCOMADDR1.Text

            Dim STS_MCOMBLDG As String = txtMBldg.Text
            Dim STS_MCOMAPARTNO As String = txtMAptNo.Text
            Dim STS_MEMIR As String = ddlMCOMEmirate.SelectedValue

            Dim STS_GCOMSTREET As String = txtGCOMADDR1.Text

            Dim STS_GCOMBLDG As String = txtGBldg.Text
            Dim STS_GCOMAPARTNO As String = txtGAptNo.Text
            Dim STS_GEMIR As String = ddlGCOMEmirate.SelectedValue

            Dim STS_FAMILY_NOTE As String = txtFamily_NOTE.Text

            Dim STS_PNAME_ARABIC As String = txtPName_Arabic.Text
            Dim STS_PADDR_ARABIC As String = txtPAddr_Arabic.Text
            Dim STS_POCC_ARABIC As String = txtPOcc_Arabic.Text

            Dim STS_FEMIRATES_ID As String = txtFEMIRATES_ID.Text
            Dim STS_MEMIRATES_ID As String = txtMEMIRATES_ID.Text
            Dim STS_GEMIRATES_ID As String = txtGEMIRATES_ID.Text
            Dim STS_FEMIRATES_ID_EXPDATE As String = txtFEMIRATES_ID_EXPDATE.Text
            Dim STS_MEMIRATES_ID_EXPDATE As String = txtMEMIRATES_ID_EXPDATE.Text
            Dim STS_GEMIRATES_ID_EXPDATE As String = txtGEMIRATES_ID_EXPDATE.Text

            Dim FEMIRATES_ID_ENG As String = txtFEmiratesId_ENG.Text.Replace("'", "")
            Dim FEMIRATES_ID_ARB As String = txtFEmiratesId_ARB.Text.Replace("'", "")

            Dim MEMIRATES_ID_ENG As String = txtMEmiratesId_ENG.Text.Replace("'", "")
            Dim MEMIRATES_ID_ARB As String = txtMEmiratesId_ARB.Text.Replace("'", "")



            Dim pParms(131) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STS_STU_ID", STS_STU_ID)
            pParms(1) = New SqlClient.SqlParameter("@STS_FFIRSTNAME", STS_FFIRSTNAME)
            pParms(2) = New SqlClient.SqlParameter("@STS_FMIDNAME", STS_FMIDNAME)
            pParms(3) = New SqlClient.SqlParameter("@STS_FLASTNAME", STS_FLASTNAME)
            pParms(4) = New SqlClient.SqlParameter("@STS_FNATIONALITY", STS_FNATIONALITY)
            pParms(5) = New SqlClient.SqlParameter("@STS_FNATIONALITY2", STS_FNATIONALITY2)
            pParms(6) = New SqlClient.SqlParameter("@STS_FCOMADDR1", STS_FCOMADDR1)
            pParms(7) = New SqlClient.SqlParameter("@STS_FCOMADDR2", STS_FCOMADDR2)
            pParms(8) = New SqlClient.SqlParameter("@STS_FCOMPOBOX", STS_FCOMPOBOX)
            pParms(9) = New SqlClient.SqlParameter("@STS_FCOMCITY", STS_FCOMCITY)
            pParms(10) = New SqlClient.SqlParameter("@STS_FCOMSTATE", STS_FCOMSTATE)
            pParms(11) = New SqlClient.SqlParameter("@STS_FCOMCOUNTRY", STS_FCOMCOUNTRY)
            pParms(12) = New SqlClient.SqlParameter("@STS_FOFFPHONE", STS_FOFFPHONE)
            pParms(13) = New SqlClient.SqlParameter("@STS_FRESPHONE", STS_FRESPHONE)
            pParms(14) = New SqlClient.SqlParameter("@STS_FFAX", STS_FFAX)
            pParms(15) = New SqlClient.SqlParameter("@STS_FMOBILE", STS_FMOBILE)
            pParms(16) = New SqlClient.SqlParameter("@STS_FPRMADDR1", STS_FPRMADDR1)
            pParms(17) = New SqlClient.SqlParameter("@STS_FPRMADDR2", STS_FPRMADDR2)
            pParms(18) = New SqlClient.SqlParameter("@STS_FPRMPOBOX", STS_FPRMPOBOX)
            pParms(19) = New SqlClient.SqlParameter("@STS_FPRMCITY", STS_FPRMCITY)
            pParms(20) = New SqlClient.SqlParameter("@STS_FPRMCOUNTRY", STS_FPRMCOUNTRY)
            pParms(21) = New SqlClient.SqlParameter("@STS_FPRMPHONE", STS_FPRMPHONE)
            pParms(22) = New SqlClient.SqlParameter("@STS_FOCC", STS_FOCC)
            pParms(23) = New SqlClient.SqlParameter("@STS_FCOMPANY", STS_FCOMPANY)
            pParms(24) = New SqlClient.SqlParameter("@STS_bFGEMSEMP", STS_bFGEMSEMP)
            pParms(25) = New SqlClient.SqlParameter("@STS_F_BSU_ID", STS_F_BSU_ID)
            pParms(26) = New SqlClient.SqlParameter("@STS_MFIRSTNAME", STS_MFIRSTNAME)
            pParms(27) = New SqlClient.SqlParameter("@STS_MMIDNAME", STS_MMIDNAME)
            pParms(28) = New SqlClient.SqlParameter("@STS_MLASTNAME", STS_MLASTNAME)
            pParms(29) = New SqlClient.SqlParameter("@STS_MNATIONALITY", STS_MNATIONALITY)
            pParms(30) = New SqlClient.SqlParameter("@STS_MNATIONALITY2", STS_MNATIONALITY2)
            pParms(31) = New SqlClient.SqlParameter("@STS_MCOMADDR1", STS_MCOMADDR1)
            pParms(32) = New SqlClient.SqlParameter("@STS_MCOMADDR2", STS_MCOMADDR2)
            pParms(33) = New SqlClient.SqlParameter("@STS_MCOMPOBOX", STS_MCOMPOBOX)
            pParms(34) = New SqlClient.SqlParameter("@STS_MCOMCITY", STS_MCOMCITY)
            pParms(35) = New SqlClient.SqlParameter("@STS_MCOMSTATE", STS_MCOMSTATE)
            pParms(36) = New SqlClient.SqlParameter("@STS_MCOMCOUNTRY", STS_MCOMCOUNTRY)
            pParms(37) = New SqlClient.SqlParameter("@STS_MOFFPHONE", STS_MOFFPHONE)
            pParms(38) = New SqlClient.SqlParameter("@STS_MRESPHONE", STS_MRESPHONE)
            pParms(39) = New SqlClient.SqlParameter("@STS_MFAX", STS_MFAX)
            pParms(40) = New SqlClient.SqlParameter("@STS_MMOBILE", STS_MMOBILE)
            pParms(41) = New SqlClient.SqlParameter("@STS_MPRMADDR1", STS_MPRMADDR1)
            pParms(42) = New SqlClient.SqlParameter("@STS_MPRMADDR2", STS_MPRMADDR2)
            pParms(43) = New SqlClient.SqlParameter("@STS_MPRMPOBOX", STS_MPRMPOBOX)
            pParms(44) = New SqlClient.SqlParameter("@STS_MPRMCITY", STS_MPRMCITY)
            pParms(45) = New SqlClient.SqlParameter("@STS_MPRMCOUNTRY", STS_MPRMCOUNTRY)
            pParms(46) = New SqlClient.SqlParameter("@STS_MPRMPHONE", STS_MPRMPHONE)
            pParms(47) = New SqlClient.SqlParameter("@STS_MOCC", STS_MOCC)
            pParms(48) = New SqlClient.SqlParameter("@STS_MCOMPANY", STS_MCOMPANY)
            pParms(49) = New SqlClient.SqlParameter("@STS_bMGEMSEMP", STS_bMGEMSEMP)
            pParms(50) = New SqlClient.SqlParameter("@STS_M_BSU_ID", STS_M_BSU_ID)
            pParms(51) = New SqlClient.SqlParameter("@STS_GFIRSTNAME", STS_GFIRSTNAME)
            pParms(52) = New SqlClient.SqlParameter("@STS_GMIDNAME", STS_GMIDNAME)
            pParms(53) = New SqlClient.SqlParameter("@STS_GLASTNAME", STS_GLASTNAME)
            pParms(54) = New SqlClient.SqlParameter("@STS_GNATIONALITY", STS_GNATIONALITY)
            pParms(55) = New SqlClient.SqlParameter("@STS_GNATIONALITY2", STS_GNATIONALITY2)
            pParms(56) = New SqlClient.SqlParameter("@STS_GCOMADDR1", STS_GCOMADDR1)
            pParms(57) = New SqlClient.SqlParameter("@STS_GCOMADDR2", STS_GCOMADDR2)
            pParms(58) = New SqlClient.SqlParameter("@STS_GCOMPOBOX", STS_GCOMPOBOX)
            pParms(59) = New SqlClient.SqlParameter("@STS_GCOMCITY", STS_GCOMCITY)
            pParms(60) = New SqlClient.SqlParameter("@STS_GCOMSTATE", STS_GCOMSTATE)
            pParms(61) = New SqlClient.SqlParameter("@STS_GCOMCOUNTRY", STS_GCOMCOUNTRY)
            pParms(62) = New SqlClient.SqlParameter("@STS_GOFFPHONE", STS_GOFFPHONE)
            pParms(63) = New SqlClient.SqlParameter("@STS_GRESPHONE", STS_GRESPHONE)
            pParms(64) = New SqlClient.SqlParameter("@STS_GFAX", STS_GFAX)
            pParms(65) = New SqlClient.SqlParameter("@STS_GMOBILE", STS_GMOBILE)
            pParms(66) = New SqlClient.SqlParameter("@STS_GPRMADDR1", STS_GPRMADDR1)
            pParms(67) = New SqlClient.SqlParameter("@STS_GPRMADDR2", STS_GPRMADDR2)

            pParms(68) = New SqlClient.SqlParameter("@STS_GPRMPOBOX", STS_GPRMPOBOX)
            pParms(69) = New SqlClient.SqlParameter("@STS_GPRMCITY", STS_GPRMCITY)
            pParms(70) = New SqlClient.SqlParameter("@STS_GPRMCOUNTRY", STS_GPRMCOUNTRY)
            pParms(71) = New SqlClient.SqlParameter("@STS_GPRMPHONE", STS_GPRMPHONE)
            pParms(72) = New SqlClient.SqlParameter("@STS_GOCC", STS_GOCC)
            pParms(73) = New SqlClient.SqlParameter("@STS_GCOMPANY", STS_GCOMPANY)
            pParms(74) = New SqlClient.SqlParameter("@STS_FCOMPANY_ADDR", STS_FCOMPANY_ADDR)
            pParms(75) = New SqlClient.SqlParameter("@STS_FEMAIL", STS_FEMAIL)
            pParms(76) = New SqlClient.SqlParameter("@STS_MCOMPANY_ADDR", STS_MCOMPANY_ADDR)
            pParms(77) = New SqlClient.SqlParameter("@STS_MEMAIL", STS_MEMAIL)
            pParms(78) = New SqlClient.SqlParameter("@STS_GCOMPANY_ADDR", STS_GCOMPANY_ADDR)
            pParms(79) = New SqlClient.SqlParameter("@STS_GEMAIL", STS_GEMAIL)
            pParms(80) = New SqlClient.SqlParameter("@STS_F_COMP_ID", STS_F_COMP_ID)
            pParms(81) = New SqlClient.SqlParameter("@STS_M_COMP_ID", STS_M_COMP_ID)
            pParms(82) = New SqlClient.SqlParameter("@STS_G_COMP_ID", STS_G_COMP_ID)

            pParms(83) = New SqlClient.SqlParameter("@STS_FCOMSTREET", STS_FCOMSTREET)
            pParms(84) = New SqlClient.SqlParameter("@STS_FCOMAREA", STS_FCOMAREA)
            pParms(85) = New SqlClient.SqlParameter("@STS_FCOMBLDG", STS_FCOMBLDG)
            pParms(86) = New SqlClient.SqlParameter("@STS_FCOMAPARTNO", STS_FCOMAPARTNO)


            pParms(87) = New SqlClient.SqlParameter("@STS_MCOMSTREET", STS_MCOMSTREET)
            pParms(88) = New SqlClient.SqlParameter("@STS_MCOMAREA", STS_MCOMAREA)
            pParms(89) = New SqlClient.SqlParameter("@STS_MCOMBLDG", STS_MCOMBLDG)
            pParms(90) = New SqlClient.SqlParameter("@STS_MCOMAPARTNO", STS_MCOMAPARTNO)

            pParms(91) = New SqlClient.SqlParameter("@STS_GCOMSTREET", STS_GCOMSTREET)
            pParms(92) = New SqlClient.SqlParameter("@STS_GCOMAREA", STS_GCOMAREA)
            pParms(93) = New SqlClient.SqlParameter("@STS_GCOMBLDG", STS_GCOMBLDG)
            pParms(94) = New SqlClient.SqlParameter("@STS_GCOMAPARTNO", STS_GCOMAPARTNO)

            pParms(95) = New SqlClient.SqlParameter("@STS_FEMIR", STS_FEMIR)
            pParms(96) = New SqlClient.SqlParameter("@STS_MEMIR", STS_MEMIR)
            pParms(97) = New SqlClient.SqlParameter("@STS_GEMIR", STS_GEMIR)
            pParms(98) = New SqlClient.SqlParameter("@STS_FAMILY_NOTE", STS_FAMILY_NOTE)
            pParms(99) = New SqlClient.SqlParameter("@STS_PNAME_ARABIC", STS_PNAME_ARABIC)
            pParms(100) = New SqlClient.SqlParameter("@STS_PADDR_ARABIC", STS_PADDR_ARABIC)
            pParms(101) = New SqlClient.SqlParameter("@STS_POCC_ARABIC", STS_POCC_ARABIC)


            pParms(102) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID", STS_FEMIRATES_ID)
            pParms(103) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID", STS_MEMIRATES_ID)
            pParms(104) = New SqlClient.SqlParameter("@STS_GEMIRATES_ID", STS_GEMIRATES_ID)

            pParms(105) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID_EXPDATE", IIf(STS_FEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_FEMIRATES_ID_EXPDATE))
            pParms(106) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID_EXPDATE", IIf(STS_MEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_MEMIRATES_ID_EXPDATE))
            pParms(107) = New SqlClient.SqlParameter("@STS_GEMIRATES_ID_EXPDATE", IIf(STS_GEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_GEMIRATES_ID_EXPDATE))

            pParms(108) = New SqlClient.SqlParameter("@STS_FCOMAREA_ID", STS_FCOMAREA_ID)
            pParms(109) = New SqlClient.SqlParameter("@STS_FCOMSTATE_ID", STS_FCOMSTATE_ID)
            pParms(110) = New SqlClient.SqlParameter("@STS_MCOMAREA_ID", STS_MCOMAREA_ID)

            pParms(111) = New SqlClient.SqlParameter("@STS_MCOMSTATE_ID", STS_MCOMSTATE_ID)
            pParms(112) = New SqlClient.SqlParameter("@STS_GCOMAREA_ID", STS_GCOMAREA_ID)
            pParms(113) = New SqlClient.SqlParameter("@STS_GCOMSTATE_ID", STS_GCOMSTATE_ID)

            pParms(122) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID_ENG", FEMIRATES_ID_ENG)
            pParms(123) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID_ENG", MEMIRATES_ID_ENG)


            pParms(124) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID_ARB", FEMIRATES_ID_ARB)
            pParms(125) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID_ARB", MEMIRATES_ID_ARB)

            pParms(126) = New SqlClient.SqlParameter("@STS_FEMIRATESID_PHOTO", PHOTO_PATH_FATHER)
            pParms(127) = New SqlClient.SqlParameter("@STS_MEMIRATESID_PHOTO", PHOTO_PATH_MOTHER)
            pParms(128) = New SqlClient.SqlParameter("@STS_GEMIRATESID_PHOTO", PHOTO_PATH_GUARDIAN)

            pParms(130) = New SqlClient.SqlParameter("@SESSION_USER", Session("sUsr_name"))


            pParms(114) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(114).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDENT_D", pParms)
            Dim ReturnFlag As Integer = pParms(114).Value
            Return ReturnFlag

        Catch ex As Exception
            Return 1000
        End Try



    End Function


    'code added by dhanya 28 apr 08
    Function callTrans_Student_Services_D(ByVal trans As SqlTransaction) As Integer
        Try
            Dim lblSvcId As Label
            Dim rdYes As RadioButton
            Dim SSV_SVC_ID As Integer
            Dim SSV_BSU_ID As String
            Dim SSV_ACD_ID As Integer
            Dim SSV_STU_ID As Integer
            Dim status As Integer
            Dim mode As String
            For i As Integer = 0 To gvServices.Rows.Count - 1
                lblSvcId = gvServices.Rows(i).FindControl("lblSvcId")
                rdYes = gvServices.Rows(i).FindControl("rdYes")
                SSV_SVC_ID = CType(lblSvcId.Text, Integer)
                SSV_BSU_ID = Session("sBsuid").ToString
                SSV_ACD_ID = CType(ViewState("ACD_ID"), Integer)
                SSV_STU_ID = CType(ViewState("viewid"), Integer)
                If rdYes.Checked = True Then
                    mode = "add"
                Else
                    mode = "delete"
                End If
                status = AccessStudentClass.SaveStudSTUDENT_SERVICES_D(SSV_SVC_ID, SSV_BSU_ID, SSV_ACD_ID, SSV_STU_ID, mode, trans)
            Next
            Return status
        Catch ex As Exception
            Return -1
        End Try
    End Function


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        ViewState("datamode") = "edit"
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"

            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect(ViewState("ReferrerUrl"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub CopySibling()
        If h_SliblingID.Value.Trim <> "" Then
            Dim temp_FOFFPhone, temp_FRESPhone, temp_FFax, temp_FMobile, temp_FPRMPHONE, temp_MFAX, temp_MOFFPHONE, temp_MRESPHONE, temp_MMOBILE, temp_MPRMPHONE, temp_GOFFPHONE, temp_GResPhone, temp_GFax, temp_GMOBILE, temp_GPRMPHONE As String


            Dim arInfo As String() = New String(2) {}
            Dim splitter As Char = "-"


            Using readerStudent_D_Detail As SqlDataReader = AccessStudentClass.GetStudent_D(h_SliblingID.Value)
                If readerStudent_D_Detail.HasRows = True Then
                    While readerStudent_D_Detail.Read

                        'handle the null value returned from the reader incase  convert.tostring
                        txtFFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_FFIRSTNAME"))
                        txtFMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_FMIDNAME"))
                        txtFLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_FLASTNAME"))
                        txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR1"))
                        txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR2"))
                        txtFCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPOBOX"))
                        txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMCITY"))
                        'txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE"))
                        txtFPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMADDR1"))
                        txtFPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMADDR2"))
                        txtFPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMPOBOX"))
                        'temp_Nationality = Convert.ToString(readerStudent_D_Detail("SNATIONALITY"))
                        txtFPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMCITY"))
                        txtFOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_FOCC"))
                        txtFComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY"))
                        txtMFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_MFIRSTNAME"))
                        txtMMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_MMIDNAME"))
                        txtMLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_MLASTNAME"))
                        txtMCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMADDR1"))
                        txtMCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMADDR2"))
                        txtMCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPOBOX"))
                        txtMCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMCITY"))
                        'txtMCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE"))
                        txtMPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMADDR1"))
                        txtMPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMADDR2"))
                        txtMPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMPOBOX"))
                        txtMPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMCITY"))
                        txtMOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_MOCC"))
                        txtMComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY"))
                        txtGFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_GFIRSTNAME"))
                        txtGMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_GMIDNAME"))
                        txtGLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_GLASTNAME"))
                        txtGCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMADDR1"))
                        txtGCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMADDR2"))
                        txtGCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPOBOX"))
                        txtGCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMCITY"))
                        'txtGCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE"))
                        txtGPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMADDR1"))
                        txtGPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMADDR2"))
                        txtGPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMPOBOX"))
                        txtGPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMCITY"))
                        txtGOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_GOCC"))


                        txtGComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY"))
                        'txtPSchool_Name1.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHI"))
                        'txtPSchool_City1.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHI_CITY"))
                        'txtPSchool_Name2.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHII"))
                        'txtPSchool_City2.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHII_CITY"))
                        'txtPSchool_Name3.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHIII"))
                        'txtPSchool_City3.Text = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHIII_CITY"))
                        ' ViewState("temp_FNation1") = Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))

                        If Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY")) <> "" Then
                            If Not ddlFNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))) Is Nothing Then
                                ddlFNationality1.SelectedIndex = -1
                                ddlFNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))).Selected = True
                            End If

                        End If

                        ' ViewState("temp_FNation2") = Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2"))
                        If Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2")) <> "" Then
                            If Not ddlFNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2"))) Is Nothing Then
                                ddlFNationality2.SelectedIndex = -1
                                ddlFNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2"))).Selected = True
                            End If
                        End If



                        'ViewState("temp_FComCountry") = Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))
                        If Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY")) <> "" Then
                            If Not ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))) Is Nothing Then
                                ddlFCOMCountry.SelectedIndex = -1
                                ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))).Selected = True
                            End If
                        End If
                        'ViewState("temp_FPRM_Country") = Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))



                        If Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY")) <> "" Then
                            If Not ddlFPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))) Is Nothing Then
                                ddlFPRM_Country.SelectedIndex = -1
                                ddlFPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))).Selected = True
                            End If
                        End If

                        'If Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY")) <> "" Then
                        '    ddlFPRM_Country.SelectedIndex = -1
                        '    ddlFPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))).Selected = True

                        'End If

                        'ViewState("temp_F_BSU_ID") = Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID"))
                        If Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID")) <> "" Then
                            If Not ddlFPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID"))) Is Nothing Then
                                ddlF_BSU_ID.SelectedIndex = -1
                                ddlF_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID"))).Selected = True
                            End If
                        End If
                        ''added father image on 7Dec2014 by nahyan

                        Dim FPhotoPath = String.Empty
                        If Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_PHOTO")) <> "" Then

                            FPhotoPath = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_PHOTO"))
                            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                            Dim strImagePath As String = String.Empty
                            If FPhotoPath <> "" Then
                                strImagePath = connPath & FPhotoPath
                                ' Save_Error_Log_Enquiry("EMiratesID get filepath", strImagePath)
                                imgFather.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
                            End If
                        End If

                        ViewState("FATHER_PHOTO_PATH") = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_PHOTO"))
                        ViewState("tempFatherimgpath") = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATESID_PHOTO"))

                        'If Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID")) <> "" Then
                        '    ddlF_BSU_ID.SelectedIndex = -1
                        '    ddlF_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID"))).Selected = True

                        'End If

                        ' ViewState("temp_MNationality1") = Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))
                        'If Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY")) <> "" Then
                        '    ddlMNationality1.SelectedIndex = -1
                        '    ddlMNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))).Selected = True
                        'End If

                        If Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY")) <> "" Then
                            If Not ddlMNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))) Is Nothing Then
                                ddlMNationality1.SelectedIndex = -1
                                ddlMNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))).Selected = True
                            End If
                        End If


                        ''ViewState("temp_MNationality2") = Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))
                        'If Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2")) <> "" Then
                        '    ddlMNationality2.SelectedIndex = -1
                        '    ddlMNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))).Selected = True
                        'End If
                        If Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2")) <> "" Then
                            If Not ddlMNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))) Is Nothing Then
                                ddlMNationality2.SelectedIndex = -1
                                ddlMNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))).Selected = True
                            End If
                        End If


                        'ViewState("temp_MCOMCountry") = Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))
                        'If Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY")) <> "" Then
                        '    ddlMCOMCountry.SelectedIndex = -1
                        '    ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))).Selected = True

                        'End If
                        If Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY")) <> "" Then
                            If Not ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))) Is Nothing Then
                                ddlMCOMCountry.SelectedIndex = -1
                                ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))).Selected = True
                            End If
                        End If


                        '' ViewState("temp_MPRMCOUNTRY") = Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))
                        'If Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY")) <> "" Then
                        '    ddlMPRM_Country.SelectedIndex = -1
                        '    ddlMPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))).Selected = True
                        'End If

                        If Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY")) <> "" Then
                            If Not ddlMPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))) Is Nothing Then
                                ddlMPRM_Country.SelectedIndex = -1
                                ddlMPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))).Selected = True
                            End If
                        End If

                        ''ViewState("temp_M_BSU_ID") = Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID"))
                        'If Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID")) <> "" Then
                        '    ddlM_BSU_ID.SelectedIndex = -1
                        '    ddlM_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID"))).Selected = True
                        'End If

                        If Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID")) <> "" Then
                            If Not ddlM_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID"))) Is Nothing Then
                                ddlM_BSU_ID.SelectedIndex = -1
                                ddlM_BSU_ID.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID"))).Selected = True
                            End If
                        End If


                        ''added mother image on 7Dec2014 by nahyan

                        Dim MPhotoPath = String.Empty
                        If Convert.ToString(readerStudent_D_Detail("STS_MEMIRATESID_PHOTO")) <> "" Then

                            MPhotoPath = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATESID_PHOTO"))
                            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
                            Dim strImagePath As String = String.Empty
                            If MPhotoPath <> "" Then
                                strImagePath = connPath & MPhotoPath
                                ' Save_Error_Log_Enquiry("EMiratesID get filepath", strImagePath)
                                imgMother.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
                            End If
                        End If

                        ''ViewState("temp_GNationality1") = Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))
                        'If Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY")) <> "" Then
                        '    ddlGNationality1.SelectedIndex = -1
                        '    ddlGNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))).Selected = True
                        'End If


                        If Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY")) <> "" Then
                            If Not ddlGNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))) Is Nothing Then
                                ddlGNationality1.SelectedIndex = -1
                                ddlGNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))).Selected = True
                            End If
                        End If


                        '' ViewState("temp_GNationality2") = Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))
                        'If Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2")) <> "" Then
                        '    ddlGNationality2.SelectedIndex = -1
                        '    ddlGNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))).Selected = True
                        'End If


                        If Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2")) <> "" Then
                            If Not ddlGNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))) Is Nothing Then
                                ddlGNationality2.SelectedIndex = -1
                                ddlGNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))).Selected = True
                            End If
                        End If

                        ' ViewState("temp_GCOMCountry") = Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))

                        'If Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY")) <> "" Then
                        '    ddlGCOMCountry.SelectedIndex = -1
                        '    ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))).Selected = True
                        'End If

                        If Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY")) <> "" Then
                            If Not ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))) Is Nothing Then
                                ddlGCOMCountry.SelectedIndex = -1
                                ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))).Selected = True
                            End If
                        End If



                        ''ViewState("temp_GPRMCOUNTRY") = Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))
                        'If Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY")) <> "" Then
                        '    ddlGPRM_Country.SelectedIndex = -1
                        '    ddlGPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))).Selected = True
                        'End If
                        If Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY")) <> "" Then
                            If Not ddlGPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))) Is Nothing Then
                                ddlGPRM_Country.SelectedIndex = -1
                                ddlGPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))).Selected = True
                            End If
                        End If


                        ' ViewState("temp_PSchool_Country1") = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHI_COUNTRY"))
                        ' ViewState("temp_PBoard_Attend1") = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHI_CLM"))
                        ' ViewState("temp_PSchool_Year1") = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHI_GRADE"))
                        ' ViewState("temp_PSchool_Country2") = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHII_COUNTRY"))
                        '  ViewState("temp_PBoard_Attend2") = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHII_CLM"))
                        ' ViewState("temp_PSchool_Year2") = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHII_GRADE"))
                        ' ViewState("temp_PSchool_Country3") = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHIII_COUNTRY"))
                        ' ViewState("temp_PBoard_Attend3") = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHIII_CLM"))
                        ' ViewState("temp_PSchool_Year3") = Convert.ToString(readerStudent_D_Detail("STS_PREVSCHIII_GRADE"))


                        'radio button setting

                        chkbFGEMSEMP.Checked = Convert.ToBoolean(readerStudent_D_Detail("STS_bFGEMSEMP")) '
                        If chkbFGEMSEMP.Checked = False Then
                            ddlF_BSU_ID.Enabled = False
                        End If
                        chkbMGEMSEMP.Checked = Convert.ToBoolean(readerStudent_D_Detail("STS_bMGEMSEMP"))
                        If chkbMGEMSEMP.Checked = False Then
                            ddlM_BSU_ID.Enabled = False
                        End If



                        'phone settings
                        temp_FOFFPhone = Convert.ToString(readerStudent_D_Detail("STS_FOFFPHONE"))
                        arInfo = temp_FOFFPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFOffPhone_Country.Text = arInfo(0)
                            txtFOffPhone_Area.Text = arInfo(1)
                            txtFOffPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFOffPhone_Area.Text = arInfo(0)
                            txtFOffPhone_No.Text = arInfo(1)

                            txtFOffPhone_No.Text = temp_FOFFPhone
                        End If


                        temp_FRESPhone = Convert.ToString(readerStudent_D_Detail("STS_FRESPHONE"))
                        arInfo = temp_FRESPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFResPhone_Country.Text = arInfo(0)
                            txtFResPhone_Area.Text = arInfo(1)
                            txtFResPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFResPhone_Area.Text = arInfo(0)
                            txtFResPhone_No.Text = arInfo(1)

                        Else
                            txtFResPhone_No.Text = temp_FRESPhone
                        End If

                        temp_FFax = Convert.ToString(readerStudent_D_Detail("STS_FFAX"))
                        arInfo = temp_FFax.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFFax_Country.Text = arInfo(0)
                            txtFFax_Area.Text = arInfo(1)
                            txtFFax_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFFax_Area.Text = arInfo(0)
                            txtFFax_No.Text = arInfo(1)

                        Else
                            txtFFax_No.Text = temp_FFax
                        End If
                        temp_FMobile = Convert.ToString(readerStudent_D_Detail("STS_FMOBILE"))
                        arInfo = temp_FMobile.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFMobile_Country.Text = arInfo(0)
                            txtFMobile_Area.Text = arInfo(1)
                            txtFMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFMobile_Area.Text = arInfo(0)
                            txtFMobile_No.Text = arInfo(1)

                        Else
                            txtFMobile_No.Text = temp_FMobile
                        End If

                        temp_FPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_FPRMPHONE"))
                        arInfo = temp_FPRMPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFPPRM_Country.Text = arInfo(0)
                            txtFPPRM_Area.Text = arInfo(1)
                            txtFPPRM_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFPPRM_Area.Text = arInfo(0)
                            txtFPPRM_No.Text = arInfo(1)

                        Else
                            txtFPPRM_No.Text = temp_FPRMPHONE
                        End If
                        temp_MOFFPHONE = Convert.ToString(readerStudent_D_Detail("STS_MOFFPHONE"))
                        arInfo = temp_MOFFPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMOffPhone_Country.Text = arInfo(0)
                            txtMOffPhone_Area.Text = arInfo(1)
                            txtMOffPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMOffPhone_Area.Text = arInfo(0)
                            txtMOffPhone_No.Text = arInfo(1)

                        Else
                            txtMOffPhone_No.Text = temp_MOFFPHONE
                        End If
                        temp_MRESPHONE = Convert.ToString(readerStudent_D_Detail("STS_MRESPHONE"))
                        arInfo = temp_MRESPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMResPhone_Country.Text = arInfo(0)
                            txtMResPhone_Area.Text = arInfo(1)
                            txtMResPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMResPhone_Area.Text = arInfo(0)
                            txtMResPhone_No.Text = arInfo(1)

                        Else
                            txtMResPhone_No.Text = temp_MRESPHONE
                        End If
                        temp_MFAX = Convert.ToString(readerStudent_D_Detail("STS_MFAX"))
                        arInfo = temp_MFAX.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMFax_Country.Text = arInfo(0)
                            txtMFax_Area.Text = arInfo(1)
                            txtMFax_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMFax_Area.Text = arInfo(0)
                            txtMFax_No.Text = arInfo(1)

                        Else
                            txtMFax_No.Text = temp_MFAX
                        End If

                        temp_MMOBILE = Convert.ToString(readerStudent_D_Detail("STS_MMOBILE"))
                        arInfo = temp_MMOBILE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMMobile_Country.Text = arInfo(0)
                            txtMMobile_Area.Text = arInfo(1)
                            txtMMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMMobile_Area.Text = arInfo(0)
                            txtMMobile_No.Text = arInfo(1)

                        Else
                            txtMMobile_No.Text = temp_MMOBILE
                        End If
                        temp_MPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_MPRMPHONE"))
                        arInfo = temp_MPRMPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtMPPermant_Country.Text = arInfo(0)
                            txtMPPermant_Area.Text = arInfo(1)
                            txtMPPermant_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtMPPermant_Area.Text = arInfo(0)
                            txtMPPermant_No.Text = arInfo(1)

                        Else
                            txtMPPermant_No.Text = temp_MPRMPHONE
                        End If
                        temp_GOFFPHONE = Convert.ToString(readerStudent_D_Detail("STS_GOFFPHONE"))
                        arInfo = temp_GOFFPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGOffPhone_Country.Text = arInfo(0)
                            txtGOffPhone_Area.Text = arInfo(1)
                            txtGOffPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGOffPhone_Area.Text = arInfo(0)
                            txtGOffPhone_No.Text = arInfo(1)

                        Else
                            txtGOffPhone_No.Text = temp_GOFFPHONE
                        End If
                        temp_GResPhone = Convert.ToString(readerStudent_D_Detail("STS_GRESPHONE"))
                        arInfo = temp_GResPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGResPhone_Country.Text = arInfo(0)
                            txtGResPhone_Area.Text = arInfo(1)
                            txtGResPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGResPhone_Area.Text = arInfo(0)
                            txtGResPhone_No.Text = arInfo(1)

                        Else
                            txtGResPhone_No.Text = temp_GResPhone
                        End If
                        temp_GFax = Convert.ToString(readerStudent_D_Detail("STS_GFAX"))
                        arInfo = temp_GFax.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGFax_Country.Text = arInfo(0)
                            txtGFax_Area.Text = arInfo(1)
                            txtGFax_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGFax_Area.Text = arInfo(0)
                            txtGFax_No.Text = arInfo(1)

                        Else
                            txtGFax_No.Text = temp_GFax
                        End If
                        temp_GMOBILE = Convert.ToString(readerStudent_D_Detail("STS_GMOBILE"))
                        arInfo = temp_GMOBILE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGMobile_Country.Text = arInfo(0)
                            txtGMobile_Area.Text = arInfo(1)
                            txtGMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGMobile_Area.Text = arInfo(0)
                            txtGMobile_No.Text = arInfo(1)

                        Else
                            txtGMobile_No.Text = temp_GMOBILE
                        End If
                        temp_GPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_GPRMPHONE"))
                        arInfo = temp_GPRMPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtGPPERM_Country.Text = arInfo(0)
                            txtGPPERM_Area.Text = arInfo(1)
                            txtGPPERM_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtGPPERM_Area.Text = arInfo(0)
                            txtGPPERM_No.Text = arInfo(1)

                        Else
                            txtGPPERM_No.Text = temp_GPRMPHONE
                        End If

                        'Date setting format


                        'If IsDate(readerStudent_D_Detail("STS_PREVSCHI_LASTATTDATE")) = True Then
                        '    txtPSchool_LDate1.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_D_Detail("STS_PREVSCHI_LASTATTDATE"))))
                        'End If

                        'If IsDate(readerStudent_D_Detail("STS_PREVSCHII_LASTATTDATE")) = True Then
                        '    txtPSchool_LDate2.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_D_Detail("STS_PREVSCHII_LASTATTDATE"))))
                        'End If

                        'If IsDate(readerStudent_D_Detail("STS_PREVSCHIII_LASTATTDATE")) = True Then
                        '    txtPSchool_LDate3.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_D_Detail("STS_PREVSCHIII_LASTATTDATE"))))
                        'End If

                    End While

                Else


                End If


            End Using
        End If
    End Sub

    Protected Sub imgbtnSibling_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnSibling.Click
        Call CopySibling()
    End Sub

    Sub BindServices()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("viewid"))
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GETSERVICE_DETAILS", param)
            If ds.Tables(0).Rows.Count > 0 Then

                gvServices.DataSource = ds.Tables(0)
                gvServices.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ' ds.Tables(0).Rows(0)(4) = True

                gvServices.DataSource = ds.Tables(0)
                Try
                    gvServices.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvServices.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvServices.Rows(0).Cells.Clear()
                gvServices.Rows(0).Cells.Add(New TableCell)
                gvServices.Rows(0).Cells(0).ColumnSpan = columnCount
                gvServices.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvServices.Rows(0).Cells(0).Text = "No record found."
            End If

        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try

        'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        'Dim str_query As String = " SELECT SVC_ID,SVC_DESCRIPTION, STATUS=(SELECT CASE  WHEN B.SVC_ID IN (SELECT ISNULL(SSV_SVC_ID,0)" & _
        '                          " FROM STUDENT_SERVICES_D WHERE SSV_STU_ID='" & ViewState("viewid") & "' AND SSV_TODATE IS NULL) THEN 1 " & _
        '                          " ELSE 0 END)FROM  SERVICES_BSU_M AS A INNER JOIN " & _
        '                          " SERVICES_SYS_M AS B ON A.SVB_SVC_ID = B.SVC_ID " & _
        '                          " WHERE SVB_BSU_ID='" & Session("sBsuid") & "' AND SVB_BAVAILABLE=1"
        'Dim dt As New DataTable
        'dt.Columns.Add("SVC_ID", GetType(Integer))
        'dt.Columns.Add("SVC_DESCRIPTION", GetType(String))
        'dt.Columns.Add("YES", GetType(Boolean))
        'dt.Columns.Add("NO", GetType(Boolean))
        'Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        'Dim flag As Boolean = False
        'Dim dr As DataRow
        'While reader.Read
        '    dr = dt.NewRow
        '    With dr
        '        If reader.GetValue(0) <> 1 Then  '''' if transport service is not stored in the grid
        '            .Item(0) = reader.GetValue(0)
        '            .Item(1) = reader.GetValue(1)
        '            If reader.GetValue(2) = 1 Then
        '                .Item(2) = True
        '                .Item(3) = False
        '            Else
        '                .Item(2) = False
        '                .Item(3) = True
        '            End If
        '            dt.Rows.Add(dr)
        '        Else
        '            ViewState("transportservice") = 1 '' to check if transprt service is available for that bsu
        '            If reader.GetValue(2) = 1 Then
        '                ViewState("enqservice") = 1
        '            End If
        '        End If
        '    End With
        'End While
        'reader.Close()


        'gvServices.DataSource = dt
        'gvServices.DataBind()
    End Sub
    Sub Student_Transport()


        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("viewid"))
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.TRANSPORT_DETAILS", param)
            If ds.Tables(0).Rows.Count > 0 Then
                ltPickUp_Loc.Text = ds.Tables(0).Rows(0)("PAREA").ToString
                ltDropOff_Loc.Text = ds.Tables(0).Rows(0)("DAREA").ToString


                ltPickUp_SubLoc.Text = ds.Tables(0).Rows(0)("PICKUP").ToString
                ltDropOff_SubLoc.Text = ds.Tables(0).Rows(0)("DROPOFF").ToString


                ltPickUp_BusNo.Text = ds.Tables(0).Rows(0)("ONWARDBUS").ToString
                ltDropOff_BusNo.Text = ds.Tables(0).Rows(0)("RETURNBUS").ToString



                ltPickUp_Trip.Text = ds.Tables(0).Rows(0)("ONWARDTRIP").ToString
                ltDropOff_Trip.Text = ds.Tables(0).Rows(0)("RETURNTRIP").ToString
          
            End If
        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try




        'Using readerStudent_Transport As SqlDataReader = AccessStudentClass.GetStudentID_Transport(ViewState("viewid"), Session("Current_ACD_ID"))

        '    If readerStudent_Transport.HasRows = True Then
        '        '*************************************************************
        '        'While readerStudent_Transport.Read
        '        '    ViewState("tran_ID") = Convert.ToString(readerStudent_Transport("SVC_ID"))
        '        '    rdUSETPT_YES.Checked = Convert.ToBoolean(readerStudent_Transport("STU_bUSETPT"))
        '        '    txtLocation.Text = Convert.ToString(readerStudent_Transport("LOC_DESC"))
        '        '    txtSubLocation.Text = Convert.ToString(readerStudent_Transport("SBL_DESC"))
        '        '    txtPickUpPoint.Text = Convert.ToString(readerStudent_Transport("PNT_DESC"))
        '        '    txtTptRemarks.Text = Convert.ToString(readerStudent_Transport("SSV_REMARKS"))
        '        'End While
        '        '***************************************************************

        '        'query modified by dhanya 28 apr 08

        '        While readerStudent_Transport.Read
        '            ViewState("tran_ID") = Convert.ToString(readerStudent_Transport("SVC_ID"))

        '            '        rdUSETPT_YES.Checked = Convert.ToBoolean(readerStudent_Transport("STU_bUSETPT"))
        '            rdTYes.Checked = Convert.ToBoolean(readerStudent_Transport("STU_bUSETPT"))

        '            txtPLocation.Text = Convert.ToString(readerStudent_Transport("PLOC"))
        '            txtPSubLocation.Text = Convert.ToString(readerStudent_Transport("PSBL"))
        '            txtPPickUpPoint.Text = Convert.ToString(readerStudent_Transport("PPICKUP"))

        '            txtDLocation.Text = Convert.ToString(readerStudent_Transport("DLOC"))
        '            txtDSubLocation.Text = Convert.ToString(readerStudent_Transport("DSBL"))
        '            txtDPickUpPoint.Text = Convert.ToString(readerStudent_Transport("DPICKUP"))

        '            txtTptRemarks.Text = Convert.ToString(readerStudent_Transport("REMARKS"))

        '            'txtPickup.Text = Convert.ToString(readerStudent_Transport("PPICKUP"))
        '            ' txtDropoff.Text = Convert.ToString(readerStudent_Transport("DPICKUP"))
        '        End While


        '    Else
        '        ViewState("tran_ID") = ""
        '        'rdUSETPT_YES.Checked = False
        '    End If
        'End Using
        'If ViewState("tran_ID") = "" Then
        '    rdTNo.Checked = True
        'Else
        '    rdTYes.Checked = True
        'End If

    End Sub

  
   
    
    Protected Sub rdFather_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdFather.CheckedChanged
        If rdFather.Checked Then
            rfvFFirstName.Visible = True
            rfvMFirstName.Visible = False
            rfvGFirstName.Visible = False
        Else
            rfvFFirstName.Visible = False

        End If
    End Sub
    Protected Sub rdMother_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdMother.CheckedChanged
        If rdMother.Checked Then
            rfvFFirstName.Visible = False
            rfvMFirstName.Visible = True
            rfvGFirstName.Visible = False
        Else
            rfvMFirstName.Visible = False

        End If
    End Sub

    Protected Sub rdGuardian_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGuardian.CheckedChanged
        If rdGuardian.Checked Then
            rfvFFirstName.Visible = False
            rfvMFirstName.Visible = False
            rfvGFirstName.Visible = True
        Else
            rfvGFirstName.Visible = False

        End If
    End Sub
End Class
