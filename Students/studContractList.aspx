<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studContractList.aspx.vb" Inherits="Students_studContractList" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Contract Student"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="tbl_test" runat="server">
                                <tr class="matters">
                                    <td align="left" width="20%"><span class="field-label">Select Academic year</span></td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server"
                                            CssClass="listbox" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr style="font-family: Verdana">
                                    <td align="left" class="matters" colspan="4" valign="top">
                                        <asp:GridView ID="gvStudentRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-row table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCONId" runat="server" Text='<%# Bind("CON_ID") %>' ></asp:Label>
                                                        <asp:Label ID="lblSTUDID" runat="server" Text='<%# Bind("STU_ID") %>' ></asp:Label>
                                                        <asp:Label ID="lblContract" runat="server" Text='<%# Bind("BCONTRACT") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student No">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("STU_DOJ", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblSTUD_IDH" runat="server" Text="Student ID" CssClass="gridheader_text"></asp:Label><br />
                                                        <asp:TextBox ID="txtSTUD_ID" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStud_ID" OnClick="btnSearchStud_ID_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" ></asp:ImageButton>&nbsp;
                    
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTUD_ID" runat="server" Text='<%# Bind("STU_No") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("STU_No") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStud_NameH" runat="server" Text="Student Name" CssClass="gridheader_text"></asp:Label><br />
                                                        <asp:TextBox ID="txtStud_Name" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStud_Name" OnClick="btnSearchStud_Name_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" ></asp:ImageButton>&nbsp;

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblStud_Name" OnClick="lblStud_Name_Click" runat="server" Text='<%# Bind("STU_PASPRTNAME") %>'
                                                            CommandName="Select"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left" ></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("SName") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblGradeH" runat="server" Text="Grade Section" CssClass="gridheader_text"></asp:Label><br />
                                                        <asp:TextBox ID="txtStud_Grade" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchGrade" OnClick="btnSearchGrade_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" ></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                            <asp:Label ID="lblGradeH" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contract">

                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="img" runat="server" ImageUrl='<%# BIND("CON_BCONTRACT") %>' OnClick="btnimg_Click" HorizontalAlign="Center" ></asp:ImageButton>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>


                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>


</asp:Content>
