﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudentDashboardstatistics_F.aspx.vb"
    Inherits="Dashboard_StudentDashboardstatistics_F" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../cssfiles/title.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <div>
            <table width="100%" id="tblFirstTable" runat="server">
                <tr id="trGridv">
                    <td align="left" valign="top">
                        <asp:GridView ID="gvBsuWiseGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CssClass="gridstyle" EmptyDataText="Record not available !!!" 
                            PageSize="50" Width="40%">
                            <Columns>
                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBsuId" runat="server" Text='<%# Bind("bsu_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="School">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkCustom" runat="server" Text='<%# bind("BSU_NAME") %>' OnClick="lnkCustomised_Click">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enquiry">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnq" runat="server" Text='<%# bind("ENQS") %>'></asp:Label>
                                        (<asp:Label ID="lblEnqLast" runat="server" Text='<%# bind("ENQS_LAST") %>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Open Enquiry">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOpenEnq" runat="server" Text='<%# bind("OPN_ENQ") %>'></asp:Label>
                                        (<asp:Label ID="lblOpenEnqLast" runat="server" Text='<%# bind("OPN_ENQ_LAST") %>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Registered">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReg" runat="server" Text='<%# bind("REGS") %>'></asp:Label>
                                        (<asp:Label ID="lblRegLast" runat="server" Text='<%# bind("REGS_LAST") %>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Offered">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnrolles" runat="server" Text='<%# bind("STUDS") %>'></asp:Label>
                                        (<asp:Label ID="lblEnrollesLast" runat="server" Text='<%# bind("STUDS_LAST") %>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enrolled">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTC" runat="server" Text='<%# bind("TCS") %>'></asp:Label>
                                         (<asp:Label ID="lblTCLast" runat="server" Text='<%# bind("TCS_LAST") %>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle BackColor="Aqua" />
                            <HeaderStyle CssClass="gridheader_pop" Height="55px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                        <asp:HiddenField ID="hdnID" runat="server" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        <asp:GridView ID="grvConsolidated" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                             EmptyDataText="Record not available !!!" 
                            PageSize="50" Width="600px" >
                            <Columns>
                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBsuId" runat="server" Text='<%# Bind("bsu_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No." ItemStyle-Width="10px" >
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                    </ItemTemplate>
                                   
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="School">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkConsolodated" runat="server" Text='<%# bind("BSU_NAME") %>'
                                            OnClick="lnkConsolodated_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                    
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Actuals">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCURRENT" runat="server" Text='<%# bind("CURR_ACTUALS") %>'></asp:Label>
                                    </ItemTemplate>
                                    
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Budget">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCURR_BUDGET" runat="server" Text='<%# bind("CURR_BUDGET") %>'></asp:Label>
                                    </ItemTemplate>
                                   
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Leavers cur(prev)"   >
                                    <ItemTemplate>
                                        <asp:Label ID="lblLEAVERS" runat="server" Text='<%# bind("LEAVERS") %>'></asp:Label>
                                        (<asp:Label ID="lblLastLEAVERS" runat="server" Text='<%# bind("LASTYRLEAVERS") %>'></asp:Label>)
                                    </ItemTemplate>
                                 
                                    
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Net Leavers cur(prev)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNETLEAVERS" runat="server" Text='<%# bind("NETLEAVERS") %>'></asp:Label>
                                        (<asp:Label ID="lblNETLEAVERS1" runat="server" Text='<%# bind("NETLEAVERSPREV") %>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="New Joiners" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblENROLLMENTS" runat="server" Text='<%# bind("ENROLLED") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="New Admission(Curr Yr)" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblENROLLMENTSCurr" runat="server" Text='<%# Bind("ENROLLED_NEW_CURRENT")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="New Admission(Next Yr)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblENROLLMENTSFuture" runat="server" Text='<%# Bind("ENROLLED_NEW_FUTURE")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Likely Actuals cur(prev)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblACTUALS" runat="server" Text='<%# bind("LIKELYACTUALS") %>'></asp:Label>
                                        (<asp:Label ID="lblACTUALS1" runat="server" Text='<%# bind("LIKELYACTUALPREV") %>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Budget Apr/Sep">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBUDGET" runat="server" Text='<%# bind("LIKELYBUDGET") %>'></asp:Label>
                                    </ItemTemplate>
                                     
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Export" Visible="false">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgExport" runat="server" ImageUrl="~/Images/excelD.jpg" OnClick="imgExport1_Click"
                                            Width="20px" Height="20px" Visible="false" />
                                    </ItemTemplate>
                                    
                                 
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle BackColor="Aqua" />
                            <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 50%">
                        <asp:Button ID="btnExportBsuData" runat="server" Text="Export" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td align="left" style="width: 50%">
                        <asp:Button ID="btnExportActual" runat="server" Text="Export Actual" />
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" id="tblSecondTable" visible="false" runat="server">
                <tr id="tr1">
                    <td align="left" valign="top">
                        <asp:GridView ID="grvBSUWISEGRD" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CssClass="gridstyle" EmptyDataText="Record not available !!!" 
                            PageSize="50" Width="40%">
                            <Columns>
                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBsuId" runat="server" Text='<%# Bind("bsu_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="School">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBsuShort" runat="server" Text='<%# bind("BSU_SHORTNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Grade">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnq" runat="server" Text='<%# bind("GRM_DISPLAY") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enquiry">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnq" runat="server" Text='<%# bind("ENQS") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Open Enquiry">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOpenEnq" runat="server" Text='<%# bind("OPN_ENQ") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Registered">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReg" runat="server" Text='<%# bind("REGS") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Offered">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnrolles" runat="server" Text='<%# bind("OFFERD") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enrolled">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTC" runat="server" Text='<%# bind("ENR") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle BackColor="Aqua" />
                            <HeaderStyle CssClass="gridheader_pop" Height="55px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        <asp:GridView ID="grvConsolidatedGRD" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                             EmptyDataText="Record not available !!!"  HeaderStyle-Height="30"
                            PageSize="50" Width="720px">
                            <Columns>
                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBsuId" runat="server" Text='<%# Bind("bsu_id") %>'></asp:Label>
                                    </ItemTemplate>
                                   
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No." ItemStyle-Width="10px">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                    </ItemTemplate>
                                    
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="School">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBsuShorta" runat="server" Text='<%# bind("BSU_SHORTNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Grade">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnq" runat="server" Text='<%# bind("GRM_DISPLAY") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Actuals">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCURRENT" runat="server" Text='<%# bind("CURR_ACTUALS") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Budget">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCURR_BUDGET" runat="server" Text='<%# bind("CURR_BUDGET") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Leavers cur(prev)" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblLEAVERS1" runat="server" Text='<%# bind("LEAVERS") %>'></asp:Label>
                                        (<asp:Label ID="lblLastLEAVERS1" runat="server" Text='<%# bind("LASTYRLEAVERS") %>'></asp:Label>)
                                    </ItemTemplate>
                                   
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Net Leavers cur(prev)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNETLEAVERS" runat="server" Text='<%# bind("NETLEAVERS") %>'></asp:Label>
                                        (<asp:Label ID="lblNETLEAVERS1" runat="server" Text='<%# bind("NETLEAVERSPREV") %>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="New Joiners" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblENROLLMENTS" runat="server" Text='<%# bind("ENROLLED") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="New Admission(Curr Yr)" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblENROLLMENTSCurr" runat="server" Text='<%# Bind("ENROLLED_NEW_CURRENT")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="New Admission(Next Yr)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblENROLLMENTSFuture" runat="server" Text='<%# Bind("ENROLLED_NEW_FUTURE")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Likely Actuals cur(prev)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblACTUALS" runat="server" Text='<%# bind("LIKELYACTUALS") %>'></asp:Label>
                                        (<asp:Label ID="lblACTUALS1" runat="server" Text='<%# bind("LIKELYACTUALPREV") %>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Budget Apr/Sep">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBUDGET" runat="server" Text='<%# bind("LIKELYBUDGET") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Export" Visible="false">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgExport" runat="server" ImageUrl="~/Images/excelD.jpg" OnClick="imgExport1_Click"
                                            Width="20px" Height="20px" Visible="false" />
                                    </ItemTemplate>
                                   
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle BackColor="Aqua" />
                            <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnExportExcel" runat="server" Text="Export Excel" CssClass="button" />
                        &nbsp;<asp:Button ID="btnBack" runat="server" Text="BACK" CssClass="button" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
