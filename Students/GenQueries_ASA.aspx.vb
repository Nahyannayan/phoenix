Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports ResponseHelper
Partial Class GenQueries_ASA
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        tr_Acad.Visible = False
        tr_Acad_List.Visible = False
        tr_Grade.Visible = False
        tr_Grade_List.Visible = False
        tr_AsOnDate.Visible = False
        tr_ToDate.Visible = False


        If Page.IsPostBack = False Then
            txtAsOnDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200900" And ViewState("MainMnu_code") <> "LB20477" And ViewState("MainMnu_code") <> "H020477" And ViewState("MainMnu_code") <> "IR11025" And ViewState("MainMnu_code") <> "TP00477" And ViewState("MainMnu_code") <> "ER20477") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling pageright class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                If Session("USR_MIS") <> "2" Then
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                BindQuery()
                BindAcad()
                BindGrade()

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindQuery()
        ddlQuery.Items.Add(New ListItem("----Select----", "0"))
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
            pParms(1) = New SqlClient.SqlParameter("@MODULE_ID", "S1ASA")
            pParms(2) = New SqlClient.SqlParameter("@USER_ID", Session("sUsr_name"))
            Using readerQuery As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetBSU_Queries_Menus", pParms)
                If readerQuery.HasRows = True Then
                    While readerQuery.Read
                        ddlQuery.Items.Add(New ListItem(readerQuery("QRY_DESCR"), readerQuery("QRY_ID")))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Function GetACDFilter() As String
        Dim i As Integer
        Dim str As String = ""
        If ViewState("ACAD") = "1" Then
            str += "<ID><ACD_ID>" + ddlACD_ID.SelectedItem.Value + "</ACD_ID></ID>"
        ElseIf ViewState("ACAD") = "2" Then
            With lstACAD
                For i = 0 To .Items.Count - 1
                    If .Items(i).Selected = True Then
                        str += "<ID><ACD_ID>" + .Items(i).Value + "</ACD_ID></ID>"
                    End If
                Next
            End With
        End If
        Return "<IDS>" + str + "</IDS>"
    End Function
    Function GetGRDFilter() As String
        Dim i As Integer
        Dim str As String = ""
        If ViewState("GRADES") = "1" Then
            str += "<ID><GRD_ID>" + ddlGRD_ID.SelectedItem.Value + "</GRD_ID></ID>"
        ElseIf ViewState("GRADES") = "2" Then
            With lstGrades
                For i = 0 To .Items.Count - 1
                    If .Items(i).Selected = True Then
                        str += "<ID><GRD_ID>" + .Items(i).Value + "</GRD_ID></ID>"
                    End If
                Next
            End With
        End If
        Return "<IDS>" + str + "</IDS>"
    End Function


    Private Sub BindAcad()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_ACD")
        lstACAD.DataSource = ds
        lstACAD.DataTextField = "ACY_DESCR"
        lstACAD.DataValueField = "ACY_ID"
        lstACAD.DataBind()

        ddlACD_ID.DataSource = ds
        ddlACD_ID.DataTextField = "ACY_DESCR"
        ddlACD_ID.DataValueField = "ACY_ID"
        ddlACD_ID.DataBind()

        For ItemTypeCounter As Integer = 0 To ddlACD_ID.Items.Count - 1
            'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
            If Not Session("Current_ACY_ID") Is Nothing Then
                If ddlACD_ID.Items(ItemTypeCounter).Value = Session("Current_ACY_ID") Then
                    ddlACD_ID.SelectedIndex = ItemTypeCounter
                End If
            End If
        Next
    End Sub
    Private Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As New DataSet
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_Grades", pParms)
        lstGrades.DataSource = ds
        lstGrades.DataTextField = "GRD_DESCR"
        lstGrades.DataValueField = "GRD_ID"
        lstGrades.DataBind()

        ddlGRD_ID.DataSource = ds
        ddlGRD_ID.DataTextField = "GRD_DESCR"
        ddlGRD_ID.DataValueField = "GRD_ID"
        ddlGRD_ID.DataBind()
    End Sub

    Protected Sub ddlQuery_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQuery.SelectedIndexChanged
        BindControls()
    End Sub
    Private Sub BindControls()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@QRY_ID", ddlQuery.SelectedItem.Value)


        Using readerStudent_D_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetQuery_Controls", pParms)
            If readerStudent_D_Detail.HasRows = True Then
                While readerStudent_D_Detail.Read
                    ViewState("DATSETS") = readerStudent_D_Detail("QFL_OUTPUT_TYPE")

                    If Convert.ToString(readerStudent_D_Detail("QFL_ACAD_TYPE")) = "1" Then
                        tr_Acad.Visible = True
                        tr_Acad_List.Visible = False
                        ViewState("ACAD") = Convert.ToString(readerStudent_D_Detail("QFL_ACAD_TYPE"))
                    ElseIf Convert.ToString(readerStudent_D_Detail("QFL_ACAD_TYPE")) = "2" Then
                        tr_Acad.Visible = False
                        tr_Acad_List.Visible = True
                        ViewState("ACAD") = Convert.ToString(readerStudent_D_Detail("QFL_ACAD_TYPE"))
                    End If

                    If Convert.ToString(readerStudent_D_Detail("QFL_GRADE_TYPE")) = "1" Then
                        tr_Grade.Visible = True
                        tr_Grade_List.Visible = False
                        ViewState("GRADES") = Convert.ToString(readerStudent_D_Detail("QFL_GRADE_TYPE"))
                    ElseIf Convert.ToString(readerStudent_D_Detail("QFL_GRADE_TYPE")) = "2" Then
                        tr_Grade.Visible = False
                        tr_Grade_List.Visible = True
                        ViewState("GRADES") = Convert.ToString(readerStudent_D_Detail("QFL_GRADE_TYPE"))
                    End If

                    If Convert.ToString(readerStudent_D_Detail("QFL_DATE_TYPE")) = "1" Then
                        tr_AsOnDate.Visible = True
                        tr_ToDate.Visible = False
                    ElseIf Convert.ToString(readerStudent_D_Detail("QFL_DATE_TYPE")) = "2" Then
                        tr_AsOnDate.Visible = True
                        tr_ToDate.Visible = True
                    End If
                End While
            End If
        End Using
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If ddlQuery.SelectedItem.Value = "51" Then
            ''not using --nahyan on 9thsep2015
            CallReport_GPS()
        ElseIf ViewState("DATSETS") > 1 Then
            CallReport_MULTI()
        Else
            ''not using --nahyan on 9thsep2015
            CallReport()
        End If

    End Sub
    ''not using --nahyan 9th sep 2015
    Sub CallReport()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim lstrExportType As Integer
        If txtToDate.Text = "" Then
            txtToDate.Text = txtAsOnDate.Text
        End If



        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_XML", GetACDFilter)
        pParms(1) = New SqlClient.SqlParameter("@GRD_XML", GetGRDFilter)
        pParms(2) = New SqlClient.SqlParameter("@FromDate", Format(Date.Parse(txtAsOnDate.Text.Trim), "dd/MMM/yyyy"))
        pParms(3) = New SqlClient.SqlParameter("@ToDate", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        pParms(4) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        pParms(5) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
        pParms(6) = New SqlClient.SqlParameter("@QRY_ID", ddlQuery.SelectedItem.Value)
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Export_OASIS_QUERY", pParms)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then

            Dim title1 As String = String.Empty

            If ddlQuery.SelectedItem.Text.Length > 30 Then
                title1 = ddlQuery.SelectedItem.Text.Substring(0, 30)
            Else
                title1 = ddlQuery.SelectedItem.Text
            End If
            Dim ws As ExcelWorksheet = ef.Worksheets.Add(title1)
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            '  ws.HeadersFooters.AlignWithMargins = True
            If ddlQuery.SelectedItem.Value = 11 Then
                Dim dtEXCEL2 As New DataTable
                dtEXCEL2 = ds1.Tables(1)

                If dtEXCEL2.Rows.Count > 0 Then
                    Dim ws2 As ExcelWorksheet = ef.Worksheets.Add(title1 & "2")
                    ws2.InsertDataTable(dtEXCEL2, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    ws2.HeadersFooters.AlignWithMargins = True
                End If
            End If

            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If

    End Sub
    Sub CallReport_MULTI()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim lstrExportType As Integer
        If txtToDate.Text = "" Then
            txtToDate.Text = txtAsOnDate.Text
        End If



        Dim pParms(2) As SqlClient.SqlParameter
        'pParms(0) = New SqlClient.SqlParameter("@ACD_XML", GetACDFilter)
        'pParms(1) = New SqlClient.SqlParameter("@GRD_XML", GetGRDFilter)
        'pParms(2) = New SqlClient.SqlParameter("@FromDate", Format(Date.Parse(txtAsOnDate.Text.Trim), "dd/MMM/yyyy"))
        'pParms(3) = New SqlClient.SqlParameter("@ToDate", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        'pParms(4) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        pParms(0) = New SqlClient.SqlParameter("@BSU", Session("sBSUID"))
        'pParms(6) = New SqlClient.SqlParameter("@QRY_ID", ddlQuery.SelectedItem.Value)
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OASIS_SERVICES..SERVICES_EXPORT", pParms)

        Dim dtEXCEL As New DataTable
        'Dim CellStyle As CellStyle = New CellStyle
        'CellStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center
        'CellStyle.VerticalAlignment = VerticalAlignmentStyle.Center
        'CellStyle.Font.Name = "Verdana"
        'CellStyle.Font.Weight = ExcelFont.BoldWeight
        'CellStyle.Font.Color = System.Drawing.ColorTranslator.FromHtml("#f2f2f2")
        'CellStyle.FillPattern.SetSolid(System.Drawing.ColorTranslator.FromHtml("#455798"))
        'CellStyle.Borders.SetBorders(MultipleBorders.Left Or MultipleBorders.Bottom Or MultipleBorders.Right Or MultipleBorders.Top, System.Drawing.ColorTranslator.FromHtml("#a6a6a6"), LineStyle.Thin)


        dtEXCEL = ds1.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim title2 As String = String.Empty

            If ds1.Tables(1).Rows(0).Item("SHEET1_NAME").ToString().Length > 30 Then
                title2 = ds1.Tables(1).Rows(0).Item("SHEET1_NAME").ToString().Substring(0, 30)
            Else
                title2 = ds1.Tables(1).Rows(0).Item("SHEET1_NAME").ToString()
            End If

            Dim Sheet1_Name As String = title2
            Dim ws As ExcelWorksheet = ef.Worksheets.Add(Sheet1_Name)
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            '  ws.HeadersFooters.AlignWithMargins = True
            'For rowLoop As Integer = 1 To 1
            '    For colLoop As Integer = 1 To 10
            '        ws.Cells(rowLoop, colLoop).Style = CellStyle
            '    Next
            'Next


            If ds1.Tables(2).Rows.Count > 0 Then
                Dim dtEXCEL2 As New DataTable
                dtEXCEL2 = ds1.Tables(2)

                If dtEXCEL2.Rows.Count > 0 Then
                    Dim title3 As String = String.Empty

                    If ds1.Tables(3).Rows(0).Item("SHEET2_NAME").ToString().Length > 30 Then
                        title3 = ds1.Tables(3).Rows(0).Item("SHEET2_NAME").ToString().Substring(0, 30)
                    Else
                        title3 = ds1.Tables(3).Rows(0).Item("SHEET2_NAME").ToString()
                    End If

                    Dim Sheet2_Name As String = title3
                    Dim ws2 As ExcelWorksheet = ef.Worksheets.Add(Sheet2_Name)
                    ws2.InsertDataTable(dtEXCEL2, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    ' ws2.HeadersFooters.AlignWithMargins = True
                End If
            End If

            If ds1.Tables(4).Rows.Count > 0 Then
                Dim dtEXCEL3 As New DataTable
                dtEXCEL3 = ds1.Tables(4)

                If dtEXCEL3.Rows.Count > 0 Then
                    Dim title4 As String = String.Empty

                    If ds1.Tables(5).Rows(0).Item("SHEET3_NAME").ToString().Length > 30 Then
                        title4 = ds1.Tables(5).Rows(0).Item("SHEET3_NAME").ToString().Substring(0, 30)
                    Else
                        title4 = ds1.Tables(5).Rows(0).Item("SHEET3_NAME").ToString()
                    End If

                    Dim Sheet3_Name As String = title4
                    Dim ws3 As ExcelWorksheet = ef.Worksheets.Add(Sheet3_Name)
                    ws3.InsertDataTable(dtEXCEL3, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    ' ws3.HeadersFooters.AlignWithMargins = True
                End If
            End If

            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave


            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub

    Sub CallReport_GPS()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
        Dim str_query As String
        Dim lstrExportType As Integer



        Dim pParms(10) As SqlClient.SqlParameter


        pParms(1) = New SqlClient.SqlParameter("@bsu_id", Session("sBsuid"))
        pParms(2) = New SqlClient.SqlParameter("@date", Format(Date.Parse(txtAsOnDate.Text.Trim), "dd/MMM/yyyy"))
        pParms(7) = New SqlClient.SqlParameter("@option", 2)
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GPS_LCD_DISPLAY_EXPORT", pParms)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            '   ws.HeadersFooters.AlignWithMargins = True


            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If

    End Sub
End Class
