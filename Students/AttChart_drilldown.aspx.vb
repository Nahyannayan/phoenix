Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports InfoSoftGlobal
Partial Class Students_AttChart_drilldown
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CreateCharts_Main()
    End Sub
    Private Sub CreateCharts_Main()
        'In this example, we show how to connect FusionCharts to a database.
        'For the sake of ease, we've used an Access database which is present in
        '../App_Data/FactoryDB.mdb. It just contains two tables, which are linked to each
        'other. 

        'Database Objects - Initialization
        Dim GRM_ID As String = Request.QueryString("vid")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim TODT As String = Session("TO_Dt")
        Dim FROMDT As String = Session("FROM_Dt")
        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("ACD_SEL_ATT"))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        param(2) = New SqlClient.SqlParameter("@FROMDT", FROMDT)
        param(3) = New SqlClient.SqlParameter("@TODT", TODT)
        param(4) = New SqlClient.SqlParameter("@GRM_ID", GRM_ID)

        Dim strXML As String
        Dim sct_descr As String = String.Empty
        Dim sct_per As String = String.Empty
        'strXML will be used to store the entire XML document generated


        Dim arr() As String
        ' arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE", "46ffb3", "b6ce6b", "acefc4", "cfefac", "efe3ac", "e6acef"}

        arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE", "46ffb3", "b6ce6b", "acefc4", "cfefac", "efe3ac", _
"e6acef", "efdc07", "73d1f8", "ee735b", "f704dd", "b8f149", "f1cb49", "a849f1", "07daf9", "91b8be", _
"d2cad6", "aae6db", "d7e6aa", "d2e6aa", "aad9e6", "aac2e6", "8176c0", "974594", "ed4e61", _
"52ed4e", "0a937e", "80269e", "9e266d", "f4e8b0", "8a008c", "288c00", "8e006e", "70e2bb", _
"b3bec3", "9bc101", "9b01c1", "b68db6", "aae6bf", "b9cf75", "d5aff0", "f0baaf", "aae6db", "d7e6aa", "d2e6aa", "aad9e6", "aac2e6", "8176c0", "974594", "ed4e61"}
        Dim i As Integer = 0



        strXML = ""
        strXML = strXML & "<graph caption='Section Wise' xAxisName='Section' yaxismaxvalue='100' yaxisminvalue='0'  yAxisName='Percent' decimalPrecision='0' formatNumberScale='0' rotateNames='0'>"

        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETATT_DASHBOARD_CHART_DRILL]", param)

            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read


                    sct_descr = readerStudent_Detail("SCT_DES").ToString
                    sct_per = readerStudent_Detail("PERC").ToString

                    strXML = strXML & "<set name=" + "'" & sct_descr & "' value=" + "'" & sct_per & "' color=" + "'" & arr(i) & "' />"
                    i = i + 1
                End While
                'Dim strline As String = "<trendlines><line startvalue='75' displayValue='Good-above 75' color='FF0000' thickness='1' isTrendZone='0'/><line startvalue='50' displayValue='Bad-below 50' color='009999' thickness='1' isTrendZone='0'/></trendlines>"
                strXML = strXML & "</graph>"
                ltDrilldown.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column2D.swf", "", strXML, "myNext", "650", "400", False)
            Else
                'lblerror.Text = "No Records Found "

                strXML = ""
                strXML = strXML & "<graph caption='Attendance Section Wise' xAxisName='Section' yAxisName='Percent' decimalPrecision='0' formatNumberScale='0'>"
                strXML = strXML & "</graph>"
                ltDrilldown.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column2D.swf", "", strXML, "myNext", "650", "400", False)
            End If
        End Using



    End Sub

End Class

