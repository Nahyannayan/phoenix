﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.IO
Imports System.Diagnostics
Imports System.Threading
Imports Microsoft.VisualBasic
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Drawing
Imports System.Drawing.Printing
Partial Class Students_studStaffLunchOrder
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ' Dim STR As String = Encr_decrData.Decrypt("wr92h2HbWq4=")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S101302") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    txtStaffId.Focus()
                    'txtItems.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
                    ' ddlreason.Attributes.Add("onKeyPress", "doClick('" + btnSav.ClientID + "',event)")
                    href1.Attributes.Add("class", "cssStepBtnActive")
                    href2.Attributes.Add("class", "cssStepBtnInActive")

                    trOrder.Visible = True
                    trPass.Visible = False

                    'gridbind(44, "ORDER")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try


        Else


        End If
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSav)
        ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnTest)
        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnOrder)

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub txtStaffId_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStaffId.TextChanged
        lblError.Text = ""

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT count(EMP_ID) cnt FROM EMPLOYEE_M WHERE EMPNO='" & txtStaffId.Text & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count >= 1 Then
            If ds.Tables(0).Rows(0).Item("cnt") = 0 Then
                lblError.Text = "Employee No Does not Exists...."
                txtStaffId.Text = ""
            End If
        End If

        'If txtStaffId.Text <> "" Then
        trDets.Visible = True
        ' bindItems()
        Displayitems()
        bindReasons()

        'ddlreason.Focus()
        ' System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "ShowLeaveDetails", "setfocus();", True)
        'Page.ClientScript.RegisterStartupScript(Me.GetType(), "satrtup", "setfocus();")
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.SetFocus(ddlreason)
        'Check_order()

        ' Else
        ' trDets.Visible = False
        ' End If

    End Sub
    Protected Sub btnTest_Click(ByVal sender As Object, ByVal e As EventArgs)
        lblError.Text = ""
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT count(EMP_ID) cnt FROM EMPLOYEE_M WHERE EMPNO='" & txtStaffId.Text & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count >= 1 Then
            If ds.Tables(0).Rows(0).Item("cnt") = 0 Then
                lblError.Text = "Employee No Does not Exists...."
                txtStaffId.Text = ""
            End If
        End If

        ' If txtStaffId.Text <> "" Then
        trDets.Visible = True
        ' bindItems()
        bindReasons()
        Displayitems()

        'ddlreason.Focus()
        ' System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "ShowLeaveDetails", "setfocus();", True)
        'Page.ClientScript.RegisterStartupScript(Me.GetType(), "satrtup", "setfocus();")
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.SetFocus(ddlreason)
        'Check_order()

        ' Else
        ' trDets.Visible = False
        ' End If
    End Sub
    Public Sub bindReasons()
        ddlreason.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SBR_ID,SBR_REASON   FROM STAFFSTAYBACKREASON_M"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlreason.DataSource = ds
        ddlreason.DataTextField = "SBR_REASON"
        ddlreason.DataValueField = "SBR_ID"
        ddlreason.DataBind()
        If (Not ddlreason.Items Is Nothing) AndAlso (ddlreason.Items.Count > 1) Then
            ddlreason.Items.Add(New ListItem("--", "0"))
            ddlreason.Items.FindByText("--").Selected = True
        End If
    End Sub
    'Public Sub bindItems()
    '    ddlItems.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT SLM_ID,SLM_ITEM   FROM STAFFLUNCHMENU_M"
    '    Dim ds As DataSet
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    '    ddlItems.DataSource = ds
    '    ddlItems.DataTextField = "SLM_ITEM"
    '    ddlItems.DataValueField = "SLM_ID"
    '    ddlItems.DataBind()
    '    If (Not ddlItems.Items Is Nothing) AndAlso (ddlItems.Items.Count > 1) Then
    '        ddlItems.Items.Add(New ListItem("--", "0"))
    '        ddlItems.Items.FindByText("--").Selected = True
    '    End If
    'End Sub

    'Protected Sub btnAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItem.Click


    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    If lstItem.Items.Count > 1 Then
    '        ddlItems.Attributes.Add("onKeyPress", "")
    '        smScriptManager.SetFocus(ddlreason)
    '    Else
    '        lstItem.Items.Add(New ListItem(ddlItems.SelectedItem.Text, ddlItems.SelectedValue))
    '        If lstItem.Items.Count > 1 Then
    '            smScriptManager.SetFocus(ddlreason)
    '        Else
    '            smScriptManager.SetFocus(ddlItems)
    '        End If

    '    End If


    'End Sub

    Protected Sub btnSav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSav.Click

        Dim Status As Integer
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction
        Dim sls_id As Integer

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try



                param(0) = New SqlParameter("@SLP_ACD_ID", ViewState("ACD_ID"))
                param(1) = New SqlParameter("@SLP_EMP_ID", ViewState("EMP_ID"))
                param(2) = New SqlParameter("@SLP_SBR_ID", ddlreason.SelectedValue)
                param(3) = New SqlParameter("SLP_SLM_ID", txtItems.Text)
                

                param(4) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(4).Direction = ParameterDirection.ReturnValue
                param(5) = New SqlClient.SqlParameter("@SLP_ID", SqlDbType.VarChar, 200)
                param(5).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "DBO.SAVE_STAFFLUNCHPASS_S", param)
                Status = param(4).Value

                sls_id = param(5).Value



            Catch ex As Exception

                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If Status <> 0 Then
                    ' UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    transaction.Rollback()
                    lblError.Text = "Do not allow adding more than one with the same date  "
                Else
                    lblError.Text = "Saved Successfully."
                    transaction.Commit()


                    ' Response.Redirect("studLatePrint.aspx?ID=" & sls_id & "&TYPE=2", False)
                    ' Response.Write("<Script> window.open('studLatePrint.aspx?ID=" & sls_id & "&TYPE=2') </Script>")



                    clear()
                    txtStaffId.Text = ""
                    txtStaffId.Focus()
                End If
            End Try

        End Using


    End Sub
    Sub clear()
        ddlreason.SelectedValue = "0"
        txtItems.Text = ""
        lblName.Text = ""
    End Sub
    Public Sub Displayitems()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""

            Dim param(3) As SqlParameter

            param(0) = New SqlParameter("@EMPNO", txtStaffId.Text)
            param(1) = New SqlParameter("@TYPE", "STAFF")

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_STAFF_PASS_DETS", param)


            If ds.Tables(0).Rows.Count >= 1 Then

                lblName.Text = ds.Tables(0).Rows(0).Item("EMP_NAME")
                lblname1.Text = ds.Tables(0).Rows(0).Item("EMP_NAME")
                ViewState("EMP_ID") = ds.Tables(0).Rows(0).Item("EMP_ID")
         
                ViewState("ACD_ID") = ds.Tables(0).Rows(0).Item("ACD_ID")

             
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub

    Sub Check_order()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""


            str_query = "SELECT SLP_ID  FROM  dbo.STAFFLUNCHPASS_S  inner join EMPLOYEE_M on EMP_ID=SLP_EMP_ID " _
                        & " where EMPNO=" & txtStaffId.Text & " and SLP_bCOLLECT=0"


            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count >= 1 Then

                href2.Attributes.Add("class", "cssStepBtnActive")
                href1.Attributes.Add("class", "cssStepBtnInActive")

                trOrder.Visible = False
                trPass.Visible = True

                txtPass.Focus()
                ' gridbind(txtStaffId.Text, "DETAIL")

            Else
                href1.Attributes.Add("class", "cssStepBtnActive")
                href2.Attributes.Add("class", "cssStepBtnInActive")

                trOrder.Visible = True
                trPass.Visible = False

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub

    Sub gridbind(ByVal id As String, ByVal type As String)


        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""

            Dim param(3) As SqlParameter

            If type = "ORDER" Then
                param(0) = New SqlParameter("@EMPNO", Val(id))
            Else
                param(0) = New SqlParameter("@EMPNO", id)
            End If

            param(1) = New SqlParameter("@TYPE", type)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_STAFF_PASS_DETS", param)

            gvPass.DataSource = ds
            gvPass.DataBind()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

        End Try
    End Sub

    Protected Sub href1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles href1.Click
        href1.Attributes.Add("class", "cssStepBtnActive")
        href2.Attributes.Add("class", "cssStepBtnInActive")

        trOrder.Visible = True
        trPass.Visible = False

    End Sub

    Protected Sub href2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles href2.Click
        href2.Attributes.Add("class", "cssStepBtnActive")
        href1.Attributes.Add("class", "cssStepBtnInActive")

        trOrder.Visible = False
        trPass.Visible = True

        'gridbind(txt)
    End Sub
 

    Protected Sub gvPass_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPass.RowCommand
        If e.CommandName = "Delivered" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvPass.Rows(index), GridViewRow)
            Dim lblID As Label

            lblID = selectedRow.FindControl("lblID")

            update_data(lblID.Text)
            'gridbind()

        End If
    End Sub

    Protected Sub gvPass_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPass.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblAlert As Label

            lblAlert = e.Row.FindControl("lblAlert")
            If lblAlert.Text = "True" Then
                Dim ib As ImageButton = e.Row.Cells(5).Controls(0)
                ib.ImageUrl = "~\images\tick.gif"
                ib.Enabled = False
            Else
                Dim ib As ImageButton = e.Row.Cells(5).Controls(0)
                ib.ImageUrl = "~\images\cross.gif"
                ib.Enabled = True

            End If


        End If
    End Sub

    Sub update_data(ByVal id As Integer)
        Dim Status As Integer
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try



                param(0) = New SqlParameter("@SLP_ID", id)

                ' param(1) = New SqlParameter("@EMP_NO", txtStaffId.Text)


                param(1) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(1).Direction = ParameterDirection.ReturnValue
              
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "DBO.UPDATE_STAFFLUNCHPASS_S", param)
                Status = param(1).Value





            Catch ex As Exception

                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If Status <> 0 Then

                    transaction.Rollback()

                Else

                    transaction.Commit()

                    lblError.Text = "Items Delivered..."
                    
                End If

            End Try

        End Using


    End Sub

    Protected Sub btnOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOrder.Click
        If txtPass.Text <> "" Then
            update_data(Val(txtPass.Text))
            ' gridbind(Val(txtPass.Text), "ORDER")

            txtPass.Text = ""
            txtPass.Focus()
          
        End If
        txtPass.Text = ""
        txtPass.Focus()
        lblError.Text = "Items Delivered..."
    End Sub

  
  
End Class
