Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studTCEntry_Approval
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100086") Then

                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    ' GridBind()
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = "0"
                    ddlGrade.Items.Insert(0, li)


                    PopulateSection()

                    If ViewState("MainMnu_code") = "S100086" Then
                        lblTitle.Text = "TC APPROVAL"
                        'ElseIf ViewState("MainMnu_code") = "S100256" Then
                        '    lblTitle.Text = "REQUEST FOR CHANGE OF TC"
                        'ElseIf ViewState("MainMnu_code") = "S100257" Then
                        '    lblTitle.Text = "CHANGE TC APPROVAL"
                    End If
                    tblTC.Rows(4).Visible = False
                    tblTC.Rows(5).Visible = False
                    tblTC.Rows(6).Visible = False
                    trApprove.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else



        End If
    End Sub
    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub lblType_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim url As String
        '      Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        '   Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
        Dim lblStuId As Label
        Dim lblStuName As Label
        Dim lblStuNo As Label
        Dim lblGrade As Label
        Dim lblSection As Label
        Dim lblDoj As Label
        Dim lblSctId As Label
        Dim lblGrdId As Label
        With sender
            lblStuId = .FindControl("lblStuId")
            lblStuName = .FindControl("lblStuName")
            lblStuNo = .FindControl("lblStuNo")
            lblGrade = .FindControl("lblGrade")
            lblSection = .FindControl("lblSection")
            lblDoj = .FindControl("lblDoj")
            lblSctId = .FindControl("lblSctId")
            lblGrdId = .FindControl("lblGrdId")
        End With



        If ViewState("MainMnu_code") = "S100086" Then
            'ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            'url = String.Format("~\Students\studTc_Student_ChangeReq_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
            '       & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
            '       & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
            '       & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
            '       & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
            '       & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
            '       & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            'ElseIf ViewState("MainMnu_code") = "S100085" Then
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            url = String.Format("~\Students\studTCEntry_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
            & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
            & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
            & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
            & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
            & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
            & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            'ElseIf ViewState("MainMnu_code") = "S100257" Then
            '    ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            '    url = String.Format("~\Students\studTc_Student_ChangeApprove_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
            '    & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
            '    & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
            '    & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
            '    & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
            '    & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
            '    & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) _
            '    & "&sctid=" + Encr_decrData.Encrypt(lblSctId.Text) _
            '    & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        End If
        Response.Redirect(url)
    End Sub

    Function getLastDateColor(ByVal lDate As String)
        If lDate <> "" Then
            If CDate(lDate) < Now.Date Then
                Return System.Drawing.Color.Red
            End If
        End If
        Return System.Drawing.Color.Navy
    End Function

#Region "Private Methods"

    Private Sub PopulateSection()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"


        ddlSection.Items.Clear()
        If ddlGrade.SelectedValue = "All" Then
            ddlSection.Items.Insert(0, li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                     & " ORDER BY SCT_DESCR "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If ViewState("MainMnu_code") = "S100086" Then
            str_query = "SELECT D.TCM_ID,STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,''))," _
                       & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID, " _
                       & " TYPE='view',CASE WHEN ISNULL(TCM_EXITFEEDBACK_PARENT,'')='' THEN 'Not Completed' ELSE 'Completed' END AS EXITFORM,TCM_LASTATTDATE " _
                       & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                       & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                       & " INNER JOIN TCM_M AS D ON A.STU_ID=D.TCM_STU_ID AND TCM_TCSO='TC' AND TCM_CANCELDATE IS NULL" _
                       & " WHERE STU_ACD_ID = " + hfACD_ID.Value _
                       & " AND STU_CURRSTATUS<>'CN'" _
                       & " AND isNULL(D.TCM_bPREAPPROVED,'FALSE')='FALSE'" _
                       & " AND D.TCM_bPREAPPROVEDT IS NULL"


        End If


        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID= " + hfSCT_ID.Value
        End If
        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID= '" + hfGRD_ID.Value + "'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        If rdComplete.Checked = True Then
            str_query += " AND TCM_EXITSTATUS='COMPLETED'"
        ElseIf rdPending.Checked = True Then
            str_query += " AND TCM_EXITSTATUS='PENDING'"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""

        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""




        If ddlEntry.SelectedValue.ToUpper <> "ALL" Then
            str_query += " AND ISNULL(TCM_ENTRY_TYPE,'SCHOOL')='" + ddlEntry.SelectedValue + "'"
        End If
        If ViewState("MainMnu_code") <> "S100254" Then
            str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            ViewState("norecord") = "1"
        Else
            gvStud.DataBind()
            ViewState("norecord") = "0"
        End If




    End Sub


    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region



    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        'GridBind()
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)


        PopulateSection()
        If ViewState("norecord") = "1" Then
            tblTC.Rows(4).Visible = False
            tblTC.Rows(5).Visible = False
            tblTC.Rows(6).Visible = False
        End If
    End Sub


    Protected Sub gvStudTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvStudTPT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        If e.CommandName = "view" Then
            Dim url As String
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
            Dim lblStuId As Label
            Dim lblStuName As Label
            Dim lblStuNo As Label
            Dim lblGrade As Label
            Dim lblSection As Label
            Dim lblDoj As Label
            Dim lblSctId As Label
            Dim lblGrdId As Label
            With selectedRow
                lblStuId = .FindControl("lblStuId")
                lblStuName = .FindControl("lblStuName")
                lblStuNo = .FindControl("lblStuNo")
                lblGrade = .FindControl("lblGrade")
                lblSection = .FindControl("lblSection")
                lblDoj = .FindControl("lblDoj")
                lblSctId = .FindControl("lblSctId")
                lblGrdId = .FindControl("lblGrdId")
            End With



            If ViewState("MainMnu_code") = "S100086" Then

                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Students\studTCEntry_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
                & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
                & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
                & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
                & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))

            End If
            Response.Redirect(url)
        End If
    End Sub



    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
        If ViewState("norecord") = "1" Then
            tblTC.Rows(4).Visible = False
            tblTC.Rows(5).Visible = False
            tblTC.Rows(6).Visible = False
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        tblTC.Rows(4).Visible = True
        tblTC.Rows(5).Visible = True
        tblTC.Rows(6).Visible = True
        hfACD_ID.Value = ddlAcademicYear.SelectedValue
        hfGRD_ID.Value = ddlGrade.SelectedValue
        hfSCT_ID.Value = ddlSection.SelectedValue
        hfSTUNO.Value = txtStuNo.Text
        hfNAME.Value = txtName.Text
        txtApprovedDate.Text = Format(Date.Now(), "dd/MMM/yyyy")
        trApprove.Visible = True
        GridBind()
    End Sub


    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim SCTID As Integer = 0
            Dim STUID As Integer = 0
            Dim GRDID As String = ""
            Dim TCMID As Integer = 0
            Dim gvFeeDetails As GridView
            Dim str_Xml As String = ""

            'If rbRequested.Checked = True Then
            For Each GvRow As GridViewRow In gvStud.Rows

                STUID = TryCast(GvRow.FindControl("lblStuId"), Label).Text
                SCTID = TryCast(GvRow.FindControl("lblSctid"), Label).Text
                GRDID = TryCast(GvRow.FindControl("lblGrdid"), Label).Text
                TCMID = TryCast(GvRow.FindControl("lblTCMID"), Label).Text
                gvFeeDetails = GvRow.FindControl("gvFeeDetails")
                str_Xml = getClearance_XML(gvFeeDetails)

                If TryCast(GvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                    SetTCApproval(1, ddlAcademicYear.SelectedValue, STUID, SCTID, GRDID, TCMID, str_Xml)
                End If
            Next
            'End If
            'refresh Grid
            GridBind()

        Catch ex As Exception

        End Try
    End Sub
    Private Function SetTCApproval(ByVal Approv As Integer, ByVal ACDID As Integer, ByVal STUID As Integer, ByVal SCTID As Integer, ByVal GRDID As String, ByVal TCMID As Integer, ByVal str_xml As String)
        Dim cmd As SqlCommand
        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim stTrans As SqlTransaction
        Dim errNo As Integer
        Dim FromDate As DateTime
        Dim flagAudit As Integer
        Dim strQuery As String

        Try

            FromDate = Date.Today()

            objConn.Close()
            objConn.Open()
            stTrans = objConn.BeginTransaction
            UtilityObj.InsertAuditdetails(stTrans, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + STUID.ToString)
            UtilityObj.InsertAuditdetails(stTrans, "edit", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + TCMID.ToString, "TCM_M_APPROVE")
            cmd = New SqlCommand("SaveTCAPPROVAL", objConn, stTrans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpTCM_ID As New SqlParameter("@TCM_ID", SqlDbType.Int)
            sqlpTCM_ID.Value = TCMID
            cmd.Parameters.Add(sqlpTCM_ID)

            Dim sqlpBSU_ID As New SqlParameter("@TCM_BSU_ID", SqlDbType.VarChar, 20)
            sqlpBSU_ID.Value = Session("SBsuid")
            cmd.Parameters.Add(sqlpBSU_ID)

            Dim sqlpTCM_ACD_ID As New SqlParameter("@TCM_ACD_ID", SqlDbType.Int)
            sqlpTCM_ACD_ID.Value = ACDID
            cmd.Parameters.Add(sqlpTCM_ACD_ID)

            Dim sqlpTCM_STU_ID As New SqlParameter("@TCM_STU_ID", SqlDbType.BigInt)
            sqlpTCM_STU_ID.Value = STUID
            cmd.Parameters.Add(sqlpTCM_STU_ID)

            Dim sqlpTCM_SCT_ID As New SqlParameter("@TCM_SCT_ID", SqlDbType.Int)
            sqlpTCM_SCT_ID.Value = SCTID
            cmd.Parameters.Add(sqlpTCM_SCT_ID)



            Dim sqlpTCM_APPROVDT As New SqlParameter("@TCM_APPROVDT", SqlDbType.DateTime)
            sqlpTCM_APPROVDT.Value = Format(CDate(txtApprovedDate.Text), OASISConstants.DateFormat)
            cmd.Parameters.Add(sqlpTCM_APPROVDT)




            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            cmd.ExecuteNonQuery()

            flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "Approved: TCM_ID(" + TCMID.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If

            If retValParam.Value <> 0 Then
                stTrans.Rollback()
                lblError.Text = UtilityObj.getErrorMessage(retValParam.Value)
                objConn.Close()
            Else
                strQuery = "exec saveSTUDENTTCFEESCHARGING " _
                                    & STUID.ToString() + "," _
                                    & TCMID.ToString() + "," _
                                    & "'" + Session("sbsuid") + "'," _
                                    & ddlAcademicYear.SelectedValue.ToString + "," _
                                    & SCTID.ToString() + "," _
                                    & "'" + GRDID + "'," _
                                    & "'" + Format(Date.Parse(txtApprovedDate.Text), "yyyy-MM-dd") + "'," _
                                    & "'" + str_xml + "'," _
                                    & "'" + Session("sUsr_name") + "'"


                SqlHelper.ExecuteNonQuery(stTrans, CommandType.Text, strQuery)
                'stTrans.Rollback()
                stTrans.Commit()
                If Approv = 1 Then
                    lblError.Text = "TC Withdrawal Approved..."
                End If
                'If Approv = 0 Then
                '    lblError.Text = "Student Service Discontinue Rejected..."
                'End If
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If

        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = ex.Message
        Finally
            objConn.Close()
        End Try

        'If Session("sbsuid") = "131001" Or Session("sbsuid") = "131002" Or Session("sbsuid") = "123016" Then
        '    Try

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Try
            strQuery = "EXEC INSERTEMAILS_TC " _
                    & " @BSU_ID ='" + Session("SBSUID") + "'," _
                    & " @STU_ID ='" + STUID.ToString + "'," _
                    & " @EMAILTYPE='TC_APPROVAL'," _
                    & " @SENDER='SCHOOL'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)

            strQuery = "EXEC INSERTEMAILS_TC " _
                   & " @BSU_ID ='" + Session("SBSUID") + "'," _
                   & " @STU_ID ='" + STUID.ToString + "'," _
                   & " @EMAILTYPE='TC_APPROVAL'," _
                   & " @SENDER='TRANSPORT'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, strQuery)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'End If
    End Function


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            'Call ClearAllFields()
            ViewState("datamode") = "none"
            Session("sSERV_PROV") = Nothing
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
    Function getClearance_XML(ByVal gvFeeDetails As GridView) As String
        Dim str As String = ""
        Dim i As Integer
        Dim lblTcsId As Label
        Dim lblFeeId As Label
        Dim txtFee As TextBox
        For i = 0 To gvFeeDetails.Rows.Count - 1
            With gvFeeDetails.Rows(i)
                lblTcsId = .FindControl("lblTCSID")
                lblFeeId = .FindControl("lblFeeId")
                txtFee = .FindControl("txtFee")

                str += "<ID><TCS_ID>" + lblTcsId.Text + "</TCS_ID><FEE_ID>" + lblFeeId.Text + "</FEE_ID><FEE_AMT>" + Val(txtFee.Text).ToString + "</FEE_AMT></ID>"
            End With
        Next

        If str = "" Then
            str = "<ID><TCS_ID>0</TCS_ID><FEE_ID>0</FEE_ID><FEE_AMT>0</FEE_AMT></ID>"
        End If
        str = "<IDS>" + str + "</IDS>"
        Return str
    End Function
    Sub BindClearances(ByVal tcmId As String, ByVal gvFeeDetails As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCS_ID,TCC_DESCR,TCC_FEE_ID FROM STUDENT_TCCLEARANCE_S AS A INNER JOIN" _
                                & " TCCLEARANCE_M AS B ON A.TCS_TCC_ID=B.TCC_ID WHERE TCS_TCM_ID=" + tcmId
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvFeeDetails.DataSource = ds
        gvFeeDetails.DataBind()
    End Sub

    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gvFeeDetails As GridView
            Dim lblTcmId As Label

            gvFeeDetails = e.Row.FindControl("gvFeeDetails")
            lblTcmId = e.Row.FindControl("lblTCMID")
            If lblTcmId.Text <> "" Then
                BindClearances(lblTcmId.Text, gvFeeDetails)
            End If
        End If
    End Sub


End Class
