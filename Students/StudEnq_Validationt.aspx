<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudEnq_Validationt.aspx.vb" Inherits="Students_StudStaff_AuthEdit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function ChangeAllCheckBoxStates() {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked;


            ChangeCheckBoxState(lstrChk);
        }



        function ChangeCheckBoxState(checkState) {
            var tableBody = document.getElementById('<%=chkCategory.ClientID %>').childNodes[0];
            //alert('called me')
            for (var i = 0; i < tableBody.childNodes.length; i++) {
                var currentTd = tableBody.childNodes[i].childNodes[0];
                var listControl = currentTd.childNodes[0];
                listControl.checked = checkState;



                // if ( listControl.checked == true )
                //alert('#' + i + ': is checked');
            }
        }
    </script>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Enquiry Validation Settings
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <%-- <table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%">
                ENQUIRY VALIDATION SETTINGS</td>
        </tr>
    </table>--%>

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" colspan="4" class="title-bg">

                                        <span class="field-label">Apply Enquiry Validation</span></td>
                                </tr>
                                <tr>
                                    <td align="left" width="25%"><span class="field-label">Set For</span></td>

                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                            <asp:ListItem Value="1">Online Enquiry</asp:ListItem>
                                            <asp:ListItem Value="2">Enquiry in School</asp:ListItem>
                                            <asp:ListItem Value="3">Enquiry Edit</asp:ListItem>
                                        </asp:DropDownList></td>

                                    <td align="left" colspan="2" width="50%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="25%"></td>
                                    <td align="left"  width="25%">
                                        <asp:Panel ID="Panel1" runat="server">
                                            <input id="Checkbox1" name="chkAL" onclick="ChangeAllCheckBoxStates();" 
                                                title="Select" type="checkbox" value="Check All" />Select All
                                        </asp:Panel>


                                    </td>
                                      <td align="left" colspan="2" width="50%"></td>
                                </tr>

                                <tr>
                                    <td align="left" width="25%"></td>
                                    <td align="left"  colspan="2" width="50%">
                                        <div class="checkbox-list">
                                            <asp:Panel ID="plChkBUnit" runat="server" HorizontalAlign="left">
                                                <asp:CheckBoxList ID="chkCategory" runat="server" CellPadding="0" CellSpacing="0"
                                                    RepeatColumns="1" RepeatDirection="Horizontal">
                                                </asp:CheckBoxList>
                                            </asp:Panel>
                                        </div>

                                    </td>
                                     <td align="left" width="25%"></td>
                                </tr>

                            </table>



                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" OnClick="btnAdd_Click" Text="Add/Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel1" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

