Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studEnqDocProceed_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100200") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    ViewState("datamode") = "add"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    hfEQS_ID.Value = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
                    GetInfo()
                    GridBind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try


        End If


    End Sub

    Protected Sub gvStage_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStage.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblStgid As New Label
                lblStgid = e.Row.FindControl("lblStgId")
                Dim chkProceed As New CheckBox
                chkProceed = e.Row.FindControl("chkProceed")
                If GetProceed(lblStgid.Text) = True Then
                    chkProceed.Checked = True
                Else
                    chkProceed.Checked = False
                End If

                If Val(e.Row.RowIndex + 1) <= Val(hfEQS_CURRSTATUSORDER.Value) Then
                    '  e.Row.Cells(1).BackColor = Drawing.Color.DarkGreen
                    ' e.Row.Cells(2).BackColor = Drawing.Color.DarkGreen
                    e.Row.Cells(1).ForeColor = Drawing.Color.DarkGreen
                    e.Row.Cells(2).ForeColor = Drawing.Color.DarkGreen
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            lblError.Text = "Records saved successfully"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub UpdateEnquiryStage(ByVal mode As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "EXEC updateENQUIRYDOCUMENTSTAGE " + hfEQS_ID.Value + ",'" + mode + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT PRO_DESCRIPTION,PRA_STG_ID FROM PROCESSFO_SYS_M AS A " _
                               & " INNER JOIN PROCESSFO_APPLICANT_S AS B ON A.PRO_ID=B.PRA_STG_ID" _
                               & " WHERE PRA_EQS_ID=" + hfEQS_ID.Value + "AND PRA_bREQUIRED='TRUE' AND PRA_STG_ID<=7 ORDER BY PRA_STG_ORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvStage.DataSource = ds
        gvStage.DataBind()
    End Sub
    Sub GetInfo()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim currStatus As Integer
        Dim str_query As String = "SELECT EQS_CURRSTATUS FROM ENQUIRY_SCHOOLPRIO_S WHERE EQS_ID=" + hfEQS_ID.Value
        currStatus = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        str_query = "SELECT PRA_STG_ORDER FROM PROCESSFO_APPLICANT_S WHERE PRA_EQS_ID=" + hfEQS_ID.Value + " AND PRA_STG_ID=" + currStatus.ToString
        hfEQS_CURRSTATUSORDER.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Function GetProceed(ByVal stgId As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct DCE_bPROCEED FROM DOCREQD_M AS A INNER JOIN" _
                                & " ENQUIRY_JOINDOCUMENTS_S AS B ON A.DOC_ID=B.DCE_DOC_ID" _
                                & " WHERE  DCE_EQS_ID=" + hfEQS_ID.Value + " AND DCE_STG_ID=" + stgId

        Dim bProceed As Boolean
        bProceed = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bProceed
    End Function

    Sub SaveData()
        Dim chkProceed As CheckBox

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim i As Integer

        For i = 0 To gvStage.Rows.Count - 1
            chkProceed = gvStage.Rows(i).FindControl("chkProceed")
            str_query = "UPDATE ENQUIRY_JOINDOCUMENTS_S SET DCE_bPROCEED='" + chkProceed.Checked.ToString + "'" _
                & " WHERE DCE_EQS_ID=" + hfEQS_ID.Value + " AND DCE_STG_ID=" + chkProceed.ToolTip
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next
        UpdateEnquiryStage("Proceed")
    End Sub
#End Region
End Class
