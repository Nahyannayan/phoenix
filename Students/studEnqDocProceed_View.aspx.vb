Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_studEnqDocProceed_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100200") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    Session("liUserList") = New List(Of String)
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    GridBind()

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()


                    ViewState("selGrade") = ""
                    ViewState("selStream") = ""
                    ViewState("selShift") = ""

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try



        Else



            studClass.SetChk(gvStudEnquiry, Session("liUserList"))
        End If


    End Sub
    Protected Sub btnEnqid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnEnq_Date_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnAppl_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvStaff_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvEx_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvSib_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub lnkApplName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("action") = "select"
    End Sub
    Private Sub SetChk()
        Dim i As Integer
        Dim chk As CheckBox
        If gvStudEnquiry.Rows.Count > 0 Then
            For i = 0 To gvStudEnquiry.Rows.Count - 1
                chk = gvStudEnquiry.Rows(i).FindControl("chkSelect")
                If chk.Checked = True Then
                    If list_add(chk.ClientID + "-" + gvStudEnquiry.PageIndex.ToString) = False Then
                        chk.Checked = True
                        gvStudEnquiry.Rows(i).BackColor = Drawing.Color.FromName("#f6deb2")
                    End If
                Else
                    If list_exist(chk.ClientID + "-" + gvStudEnquiry.PageIndex.ToString) = True Then
                        chk.Checked = True
                        gvStudEnquiry.Rows(i).BackColor = Drawing.Color.FromName("#f6deb2")
                    End If
                    list_remove(chk.ClientID + "-" + gvStudEnquiry.PageIndex.ToString)
                End If

            Next

        End If
    End Sub
   
    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Private Sub GridBind()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT  EQS_ID,EQS_APPLNO,EQM_ENQID,EQM_ENQDATE, GRM_DISPLAY, SHF_DESCR,STM_DESCR, " _
                                    & " APPL_NAME=isnull(EQM_APPLFIRSTNAME,'') + ' ' + ISNULL(EQM_APPLMIDNAME, '') + ' ' + ISNULL(EQM_APPLLASTNAME, ''), " _
                                    & " IMGGEMS=CASE EQM_bSTAFFGEMS WHEN 'TRUE' THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END  ," _
                                    & " IMGEX=CASE EQM_bEXSTUDENT WHEN 'TRUE' THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END , " _
                                    & " IMGSIB=CASE EQM_bAPPLSIBLING WHEN 'TRUE' THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END," _
                                    & " CHK=CASE WHEN EQS_DOC_STG_ID>=2 THEN 'TRUE' ELSE 'FALSE' END" _
                                    & " FROM   ENQUIRY_M AS A INNER JOIN " _
                                    & " ENQUIRY_SCHOOLPRIO_S AS B ON A.EQM_ENQID = B.EQS_EQM_ENQID INNER JOIN" _
                                    & " GRADE_BSU_M AS C ON B.EQS_GRM_ID = C.GRM_ID INNER JOIN " _
                                    & " SHIFTS_M AS D ON B.EQS_SHF_ID = D.SHF_ID INNER JOIN " _
                                    & " STREAM_M AS E ON B.EQS_STM_ID = E.STM_ID " _
                                    & " WHERE EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                    & " AND EQS_BSU_ID='" + Session("sbsuid") + "' " _
                                    & " AND EQS_STATUS<>'DEL'  "


            Dim strFilter As String = ""
            Dim strSidsearch As String()
            Dim strSearch As String

            Dim enqSearch As String = ""
            Dim nameSearch As String = ""
            Dim dateSearch As String = ""

            Dim ddlgvGrade As New DropDownList
            Dim ddlgvShift As New DropDownList
            Dim ddlgvStream As New DropDownList
            Dim ddlgvStaff As New DropDownList
            Dim ddlgvEx As New DropDownList
            Dim ddlgvSib As New DropDownList


            Dim selectedGrade As String = ""
            Dim selectedShift As String = ""
            Dim selectedStream As String = ""
            Dim selectedStaff As String = ""
            Dim selectedEx As String = ""
            Dim selectedSib As String = ""
            Dim txtSearch As New TextBox



            If gvStudEnquiry.Rows.Count > 0 Then


                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = GetSearchString("eqs_applno", txtSearch.Text, strSearch)
                enqSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("convert(varchar,eqm_enqdate,106)", txtSearch.Text.Replace("/", " "), strSearch)
                dateSearch = txtSearch.Text

                ddlgvGrade = gvStudEnquiry.HeaderRow.FindControl("ddlgvGrade")
                If ddlgvGrade.Text <> "ALL" And ddlgvGrade.Text <> "" Then
                    strFilter += " and grm_display='" + ddlgvGrade.Text + "'"
                    selectedGrade = ddlgvGrade.Text
                End If

                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                If ddlgvShift.Text <> "ALL" And ddlgvShift.Text <> "" Then
                    strFilter = strFilter + " and shf_descr='" + ddlgvShift.Text + "'"
                    selectedShift = ddlgvShift.Text
                End If

                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")
                If ddlgvStream.Text <> "ALL" And ddlgvStream.Text <> "" Then

                    strFilter = strFilter + " and stm_descr='" + ddlgvStream.Text + "'"
                    selectedStream = ddlgvStream.Text
                End If



                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
                strSidsearch = h_Selected_menu_3.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("isnull(eqm_applfirstname,' ')+' '+isnull(eqm_applmidname,' ')+' '+isnull(eqm_appllastname,' ')", txtSearch.Text, strSearch)
                nameSearch = txtSearch.Text

               
                If strFilter.Trim <> "" Then
                    str_query = str_query + strFilter
                End If
            End If

            Dim ds As DataSet
            str_query += " order by eqm_enqdate desc "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudEnquiry.DataSource = ds

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudEnquiry.DataBind()
                Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
                gvStudEnquiry.Rows(0).Cells.Clear()
                gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
                gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudEnquiry.DataBind()
            End If

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
            txtSearch.Text = enqSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
            txtSearch.Text = dateSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
            txtSearch.Text = nameSearch

         

            Dim dt As DataTable = ds.Tables(0)

            If gvStudEnquiry.Rows.Count > 0 Then
                ddlgvGrade = gvStudEnquiry.HeaderRow.FindControl("ddlgvGrade")
                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")

                Dim dr As DataRow

                ddlgvGrade.Items.Clear()
                ddlgvGrade.Items.Add("ALL")

                ddlgvShift.Items.Clear()
                ddlgvShift.Items.Add("ALL")


                ddlgvStream.Items.Clear()
                ddlgvStream.Items.Add("ALL")

                For Each dr In dt.Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr
                        ''check for duplicate values and add to dropdownlist 

                        If ddlgvGrade.Items.FindByText(.Item(4)) Is Nothing Then
                            ddlgvGrade.Items.Add(.Item(4))
                        End If


                        If ddlgvShift.Items.FindByText(.Item(5)) Is Nothing Then
                            ddlgvShift.Items.Add(.Item(5))
                        End If


                        If ddlgvStream.Items.FindByText(.Item(6)) Is Nothing Then
                            ddlgvStream.Items.Add(.Item(6))
                        End If

                    End With
                Next

                If selectedGrade <> "" Then
                    ddlgvGrade.Text = selectedGrade
                End If


                If selectedShift <> "" Then
                    ddlgvShift.Text = selectedShift
                End If

                If selectedStream <> "" Then
                    ddlgvStream.Text = selectedStream
                End If
            End If


            studClass.SetChk(gvStudEnquiry, Session("liUserList"))
            '  highlight_grid()
            '   set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


    Public Function returnpath(ByVal bYes As Boolean) As String
        Try
            'Dim Flag_status As Boolean '= Convert.ToBoolean(Status)
            If bYes = True Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getmenuid(Optional ByVal p_imgsrc As String = "", Optional ByVal imagename As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudEnquiry.HeaderRow.FindControl(imagename)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

   

    'Sub highlight_grid()
    '    For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1
    '        Dim row As GridViewRow = gvStudEnquiry.Rows(i)
    '        Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
    '        If isSelect Then
    '            row.BackColor = Drawing.Color.FromName("#f6deb2")
    '        End If
    '    Next
    'End Sub


   
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Session("liUserList") = New List(Of String)
        GridBind()
    End Sub

   
    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudEnquiry.PageIndexChanging
        gvStudEnquiry.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvStudEnquiry_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudEnquiry.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStudEnquiry.Rows(index), GridViewRow)
                Dim eqsid As New Label
                Dim enqid As New Label
                Dim enqno As New Label
                Dim url As String
                'define the datamode to view if view is clicked
                ViewState("datamode") = "edit"
                'Encrypt the data that needs to be send through Query String
                eqsid = selectedRow.Cells(1).FindControl("lblEqsid")
                enqid = selectedRow.Cells(5).FindControl("lblEnqId")
                enqno = selectedRow.Cells(5).FindControl("lblEnqNo")
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))


                url = String.Format("~\Students\studEnqDocProceed_M.aspx?MainMnu_code={0}&datamode={1}&eqsid=" + Encr_decrData.Encrypt(eqsid.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudEnquiry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudEnquiry.SelectedIndexChanged
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim stgId As Integer
        If ViewState("action") = "select" Then
            ViewState("action") = ""
            Dim lbleqsId As Label
            With gvStudEnquiry.SelectedRow
                lbleqsId = .FindControl("lbleqsid")
                str_query = "SELECT ISNULL(EQS_DOC_STG_ID,0) FROM ENQUIRY_SCHOOLPRIO_S WHERE EQS_ID=" + lbleqsId.Text
                stgId = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                Dim chkSelect As CheckBox
                chkSelect = .FindControl("chkSelect")
                If stgId >= 2 Then
                    chkSelect.Enabled = True
                Else
                    chkSelect.Enabled = False
                End If
            End With
        End If
    End Sub
End Class
