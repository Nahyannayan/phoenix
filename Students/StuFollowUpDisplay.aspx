<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="StuFollowUpDisplay.aspx.vb" Inherits="Students_StuFollowUpDisplay" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
<script type = "text/javascript">
    function Confirm_test() {
       
        if (confirm("Are you sure you want to Delete the enquiry ?")) {
            document.getElementById('<%= hdnValue.ClientID%>').value = "Yes";
        } else {
            document.getElementById('<%= hdnValue.ClientID%>').value = "No";
        }
        
    }
    function redirect_test(val) {

        window.location.href = val;

    }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Follow Up
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <div>
                    <asp:HiddenField ID="hdnValue" runat="server" />
                    <table align="left">
                        <tr>
                            <td align="left"  colspan="3" >
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                                    EnableViewState="False" ForeColor="" ValidationGroup="groupM1" />
                                <span style="color: #c00000"></span>
                            </td>
                        </tr>
                    </table>


                    <table cellpadding="5" cellspacing="0"
                        width="100%">
                        <%-- <tr class="subheader_img">
                <td align="left" colspan="6" style="width: 777px" valign="middle">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">Follow Up</font></td>
            </tr>--%>
                        <tr>
                            <td align="left"  rowspan="1">
                                <table align="left"  cellpadding="5" cellspacing="0"
                                    style="width: 100%">
                                    <tr>
                                        <td align="left" width="20%" ><span class="field-label">Enquiry No</span></td>
                                        
                                        <td  align="left">
                                            <asp:TextBox ID="txtEnqNo" runat="server" ReadOnly="True" ></asp:TextBox></td>
                                        <td align="left" width="20%" ><span class="field-label">Academic Year</span></td>
                                        
                                        <td  style="text-align: left">
                                            <asp:TextBox ID="txtAcadYear" runat="server" ReadOnly="True"></asp:TextBox>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Grade</span></td>
                                        
                                        <td  style="text-align: left">
                                            <asp:TextBox ID="txtGrade" runat="server" ReadOnly="True"></asp:TextBox>
                                        </td>
                                        <td align="left" width="20%" ><span class="field-label">Shift</span></td>                                      
                                        <td >
                                            <asp:TextBox ID="txtShift" runat="server" ReadOnly="True" ></asp:TextBox></td>
                                       
                                    </tr>
                                    <tr>
                                         <td align="left" width="20%"><span class="field-label">Stream</span></td>                                       
                                        <td >
                                            <asp:TextBox ID="txtStream" runat="server" ReadOnly="True"></asp:TextBox>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <br /><hr />
                                        </td>
                                    </tr>
                                </table>


                                <table align="left" cellpadding="5" cellspacing="0"
                                    style="width: 100%" id="TABLE1">
                                    <tr>
                                        <td align="left" width="20%"  ><span class="field-label">Enquiry Date</span></td>
                                        
                                        <td align="left" >
                                            <asp:TextBox ID="txtenqdate" runat="server" ReadOnly="True" ></asp:TextBox></td>
                                        <td align="left" width="20%" ><span class="field-label">Mobile</span> </td>
                                        
                                        <td  align="left">
                                            <asp:TextBox ID="txtmobile" runat="server" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"><span class="field-label">Applicant Name</span></td>
                                       
                                        <td  colspan="0" align="left">
                                            <asp:TextBox ID="txtappname" runat="server" ReadOnly="True" ></asp:TextBox></td>
                                        <td align="left" width="20%" ><span class="field-label">Email</span></td>
                                        
                                        <td   style="text-align: left">
                                            <asp:TextBox ID="txtemail" runat="server" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%" ><span class="field-label">Primary Contact </span></td>
                                        
                                        <td    align="left">
                                            <asp:TextBox ID="txtprimarycaontact" runat="server" ReadOnly="true" ></asp:TextBox>
                                        </td>
                                        <td colspan="2"></td>

                                    </tr>
                                     <tr>
                                        <td colspan="4">
                                            <br /><hr />
                                        </td>
                                    </tr>
                                </table>

                              

                                <table align="left"  cellpadding="5" cellspacing="0"
                                    style="width: 100%" id="TABLE3">
                                    <tr>
                                        <td align="left" width="20%" ><span class="field-label">Current Status</span></td>
                                       
                                        <td  style="text-align: left" width="30%">
                                            <asp:DropDownList ID="ddlCurStatus" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </td>
                                        <td align="left"  width="20%"><span class="field-label">Status Reason</span></td>
                                       
                                        <td  style="text-align: left">
                                            <asp:DropDownList ID="ddlReason" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%" ><span class="field-label">Remarks</span></td>
                                        
                                        <td  style="text-align: left" >
                                            <asp:TextBox ID="txtReasonRemark" runat="server" MaxLength="300" SkinID="MultiText"
                                                TextMode="MultiLine" >
                                            </asp:TextBox>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <br /><hr />
                                        </td>
                                    </tr>
                                </table>

                                

                                
                                <table align="left" cellpadding="5" cellspacing="0"
                                    style="width: 100%">
                                    <tr>
                                        <td align="left" width="20%" ><span class="field-label">Mode </span></td>
                                       
                                        <td  align="left " width="30%">
                                            <asp:DropDownList ID="ddmode" runat="server">
                                                <asp:ListItem Text="" Selected="true" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Telephone" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Email" Value="2"></asp:ListItem>
                                            </asp:DropDownList></td>
                                        <td align="left" width="20%"><span class="field-label">Alert On</span> </td>
                                       
                                        <td  style="text-align: left" width="30%">
                                            <asp:DropDownList ID="ddalertdays" runat="server" Visible="false">
                                            </asp:DropDownList>

                                            <asp:TextBox ID="txtAob" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="imgDOB" runat="server" ImageUrl="~/Images/calendar.gif" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtAob"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                            <br />
                                            (dd/mmm/yyyy)
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%"  ><span class="field-label">Remarks</span> </td>
                                        
                                        <td  colspan="0" width="30%"  align="left">

                                            <asp:TextBox ID="txtremarks" runat="server" MaxLength="300" SkinID="MultiText"
                                                TextMode="MultiLine">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfv_txtremarks" runat="server" ControlToValidate="txtremarks"
                                                Display="Dynamic" ErrorMessage="Please enter the Remarks." InitialValue=" "
                                                ValidationGroup="groupM1" EnableClientScript="false" Visible="false">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                </table>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="center"  >
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" /><asp:Button
                        ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click" Text="Cancel" />
                                <br />
                                <asp:Label ID="lblmessage" runat="server" ForeColor="Red" ></asp:Label><asp:HiddenField ID="HiddenENQID" runat="server" />

                                <asp:GridView ID="grdReason" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-bordered table-row"
>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Log Date">
                                            <ItemTemplate>
                                                <%#Eval("ENR_LOG_DATE", "{0:dd/MMM/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Current Status">
                                            <ItemTemplate>
                                                <%#Eval("EFR_REASON")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reason">
                                            <ItemTemplate>
                                                <%#Eval("EFD_REASON")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks">
                                            <ItemTemplate>
                                                <%#Eval("ENR_REMARKS")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="User">
                                            <ItemTemplate>
                                                <%#Eval("ENR_USER_ID")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <HeaderStyle CssClass="gridheader_pop"  />
                                    <RowStyle CssClass="griditem"  />
                                    <SelectedRowStyle CssClass="Green" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>

                                <asp:GridView ID="grdFollowuphis" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Log Date">
                                            <ItemTemplate>
                                                <%#Eval("FOL_FOLL_UP_DATE", "{0:dd/MMM/yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mode">
                                            <ItemTemplate>
                                                <%#Eval("MODE_DESCR")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks">
                                            <ItemTemplate>
                                                <%#Eval("FOL_REMARKS")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Alert Date">
                                            <ItemTemplate>
                                                <%#Eval("alertdate")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="User">
                                            <ItemTemplate>
                                                <%#Eval("FOL_USER_ID")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <HeaderStyle CssClass="gridheader_pop" />
                                    <RowStyle CssClass="griditem" />
                                    <SelectedRowStyle CssClass="Green" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>


                            </td>
                        </tr>
                    </table>
                    
     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txtAOB" runat="server" PopupButtonID="imgDOB" Format="dd/MMM/yyyy">
     </ajaxToolkit:CalendarExtender>

                </div>
                <asp:HiddenField ID="hdnEnq_id" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>
