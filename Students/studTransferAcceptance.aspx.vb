﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studTransferAcceptance
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Try


            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                'Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200118") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    GridBind()
                End If

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If GrdView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = GrdView.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If GrdView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = GrdView.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT row_number() over (ORDER BY A.F_GRD_DISPLAYORDER,A.F_SCT_DESCR,A.STU_NAME)as SINO,STS_ID,STU_ID,STU_NO,STU_NAME,F_GRD_DISPLAYORDER,F_GRM_DISPLAY,F_GRM_ID,F_STS_SCT_ID,F_SCT_DESCR,F_STS_ACD_ID,F_BSU_NAME,F_ACY_DESCR,F_BSU_ID,T_GRD_DISPLAYORDER,T_GRM_DISPLAY,T_GRM_ID,T_STS_SCT_ID,T_SCT_DESCR,T_STS_ACD_ID,T_BSU_NAME,T_ACY_DESCR,T_BSU_ID FROM(SELECT TRANS.STS_ID,STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ') AS STU_NAME,GRD_FROM.GRD_DISPLAYORDER as F_GRD_DISPLAYORDER, GRADE_BSU_FROM.GRM_DISPLAY AS F_GRM_DISPLAY,GRADE_BSU_FROM.GRM_ID AS F_GRM_ID, TRANS.STS_SCT_ID AS F_STS_SCT_ID,SECTION_M_FROM.SCT_DESCR AS F_SCT_DESCR,TRANS.STS_ACD_ID AS F_STS_ACD_ID,BSU_FROM.BSU_NAME AS F_BSU_NAME,ACY_FROM.ACY_DESCR AS F_ACY_DESCR,TRANS.STS_BSU_ID AS  F_BSU_ID,GRD_TO.GRD_DISPLAYORDER as T_GRD_DISPLAYORDER, GRADE_BSU_TO.GRM_DISPLAY AS T_GRM_DISPLAY,GRADE_BSU_TO.GRM_ID AS T_GRM_ID, TRANS.STS_TO_SCT_ID AS T_STS_SCT_ID,SECTION_M_TO.SCT_DESCR AS T_SCT_DESCR,TRANS.STS_TO_ACD_ID AS T_STS_ACD_ID,BSU_TO.BSU_NAME AS T_BSU_NAME,ACY_TO.ACY_DESCR AS T_ACY_DESCR,TRANS.STS_TO_BSU_ID AS  T_BSU_ID FROM STUDENT_TRANSFER_S TRANS INNER JOIN STUDENT_M STU ON STU.STU_ID= TRANS.STS_STU_ID INNER JOIN GRADE_M  GRD_FROM ON TRANS.STS_GRD_ID=GRD_FROM.GRD_ID INNER JOIN GRADE_BSU_M  GRADE_BSU_FROM ON TRANS.STS_GRM_ID = GRADE_BSU_FROM.GRM_ID INNER JOIN SECTION_M SECTION_M_FROM ON TRANS.STS_SCT_ID = SECTION_M_FROM.SCT_ID INNER JOIN BUSINESSUNIT_M BSU_FROM ON TRANS.STS_BSU_ID=BSU_FROM.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_FROM ON TRANS.STS_ACD_ID=ACD_FROM.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_FROM ON ACY_FROM.ACY_ID=ACD_FROM.ACD_ACY_ID INNER JOIN GRADE_M  GRD_TO ON TRANS.STS_TO_GRD_ID=GRD_TO.GRD_ID INNER JOIN GRADE_BSU_M  GRADE_BSU_TO ON TRANS.STS_TO_GRM_ID = GRADE_BSU_TO.GRM_ID INNER JOIN SECTION_M SECTION_M_TO ON TRANS.STS_TO_SCT_ID = SECTION_M_TO.SCT_ID INNER JOIN BUSINESSUNIT_M BSU_TO ON TRANS.STS_TO_BSU_ID=BSU_TO.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_TO ON TRANS.STS_TO_ACD_ID=ACD_TO.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_TO ON ACY_TO.ACY_ID=ACD_TO.ACD_ACY_ID AND ISNULL(STU.STU_LEAVEDATE,'')='' AND STU.STU_CurrStatus<>'CN'AND TRANS.STS_bCANCEL_TRANS='FALSE' AND TRANS.STS_bApproved='True' AND TRANS.STS_TO_bApproved='False' )A WHERE A.T_BSU_ID='" & Session("sBsuid") & "'"

        'Dim txtStuNo As TextBox
        'Dim txtName As TextBox
        'If txtStuNo.Text <> "" Then
        '    str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        'End If

        'If txtName.Text <> "" Then
        '    str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        'End If


        ' AND C.SCT_GRM_ID=B.GRM_ID" 
        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedStatus As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim txtSearch As New TextBox

        If GrdView.Rows.Count > 0 Then


            txtSearch = GrdView.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter += " AND "
                strFilter += GetSearchString("STU_NAME", txtSearch.Text, strSearch)
            End If
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = GrdView.HeaderRow.FindControl("txtFeeSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then

                strFilter += " AND "

                strFilter += GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strFee = txtSearch.Text

        End If

        str_query += strFilter + " ORDER BY A.F_GRD_DISPLAYORDER,A.F_SCT_DESCR,A.STU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'Dim dv As DataView = ds.Tables(0).DefaultView
        'If strFilter <> "" Then
        '    dv.RowFilter = strFilter
        'End If

        Dim dt As DataTable
        If ds.Tables(0).Rows.Count = 0 Then
            GrdView.DataSource = ds
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            GrdView.DataBind()
            Dim columnCount As Integer = GrdView.Rows(0).Cells.Count
            GrdView.Rows(0).Cells.Clear()
            GrdView.Rows(0).Cells.Add(New TableCell)
            GrdView.Rows(0).Cells(0).ColumnSpan = columnCount
            GrdView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            GrdView.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

        Else
            GrdView.DataSource = ds
            GrdView.DataBind()

        End If

        set_Menu_Img()
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim CurBsUnit As String = Session("sBsuid")
        Dim CurACDID As String = "" ' ddlAca_Year.SelectedValue
        Dim lstrTRF_GENFEEID As String


        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", CurBsUnit)

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_TRF_GENFEEID", pParms)
            While reader.Read
                lstrTRF_GENFEEID = Convert.ToString(reader("BSU_TRF_GENFEEID"))
            End While
        End Using



        'ANDACD_ACY_ID=20 ANDACD_ACY_ID=20
        Dim con As New SqlConnection(str_conn)
        Dim transaction As SqlTransaction
        con.Open()
        For Each gr As GridViewRow In GrdView.Rows

            Dim Return_msg As String = ""

            Dim btransfer As CheckBox = TryCast(gr.FindControl("chkSelect"), CheckBox)
            Dim HF_sts_id As HiddenField = TryCast(gr.FindControl("HF_sts_id"), HiddenField)
            If btransfer.Checked = True Then
                Try
                    transaction = con.BeginTransaction("trans")
                    Dim HF_T0_BSU_ID As HiddenField = TryCast(gr.FindControl("HF_T0_BSU_ID"), HiddenField)
                    Dim stu_id As String = TryCast(gr.FindControl("HF_stu_id"), HiddenField).Value
                    Dim stu_no As String = TryCast(gr.FindControl("lblFeeId"), Label).Text
                    Dim param(10) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@STS_ID", HF_sts_id.Value)
                    param(1) = New SqlClient.SqlParameter("@STU_NO", stu_no)
                    param(2) = New SqlClient.SqlParameter("@STU_ID", stu_id)
                    param(3) = New SqlClient.SqlParameter("@BSU_ID", HF_T0_BSU_ID.Value)
                    param(7) = New SqlClient.SqlParameter("@STS_ACCEPT_USR", Session("sUsr_name"))
                    param(4) = New SqlClient.SqlParameter("@Return_msg", SqlDbType.VarChar, 400)
                    param(4).Direction = ParameterDirection.Output

                    If lstrTRF_GENFEEID = "1" Then
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.STUDENT_TRANSFER_Accept_DIFF_FEE_ID", param)
                    Else
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.STUDENT_TRANSFER_Accept", param)
                    End If

                    Return_msg = param(4).Value.ToString
                    If Return_msg = "SUSSESS" Then
                        transaction.Commit()
                    Else
                        ViewState("FAILED_TRANSFER_STUNO") += stu_no + ","
                    End If
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Transfer Failed...!!!!"
                End Try
            End If
        Next
        If ViewState("FAILED_TRANSFER_STUNO") <> "" Then
            lblError.Text = ViewState("FAILED_TRANSFER_STUNO") + "transfer Failed for these student no"
        End If
        GridBind()
    End Sub
    
End Class
