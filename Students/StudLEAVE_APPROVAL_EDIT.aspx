<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudLEAVE_APPROVAL_EDIT.aspx.vb" Inherits="Students_studLeave_Entry_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

   <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Leave Approval Edit
        </div>
        <div class="card-body">
            <div class="table-responsive">

<%--<table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%">
               LEAVE APPROVAL EDIT</td>
        </tr>
    </table>--%>
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" >
                
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"  ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
               
            </td>
        </tr>
        <tr >
          <%--  <td align="center" >
                &nbsp;Fields Marked<span style="font-size: 8pt; color: #800000"> </span>with(<span
                    style="font-size: 8pt; color: #800000">*</span>)are mandatory</td>--%>
              <td align="center" class="text-danger font-small" valign="middle">
                Fields Marked with ( * ) are mandatory
            </td>
        </tr>
        <tr>
            <td class="matters"  >
                <table align="center" border="0" cellpadding="5" cellspacing="0"  width="100%">
               <%--     <tr>
                        <td colspan="6">
                            <asp:Literal id="ltLabel" runat="server" Text="Leave Approval Edit"></asp:Literal></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%">
                           <span class="field-label">  Academic Year</span></td>
                       
                        <td align="left" colspan="3">
                            <asp:Label id="lblAcdYear" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" >
                          <span class="field-label">   Full Name in English</span><br /><span class="field-label"> (First,Middle,Last)</span></td>
                       
                        <td align="left" colspan="3" >
                            <asp:Label id="lblSName" runat="server" CssClass="field-value"></asp:Label>
                           </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label"> Grade</span></td>
                       
                        <td align="left">
                            <asp:Label id="lblGrade" runat="server" CssClass="field-value"></asp:Label></td>
                        <td align="left" >
                             <span class="field-label">Section</span></td>
                       
                        <td align="left" >
                            <asp:Label id="lblSection" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                   <tr >
                       <td align="left"  colspan="4" class="title-bg-lite">
                            Edit Detail</td>
                    </tr>
                    <tr>
                        <td align="left" >
                           <span class="field-label">  From Date</span><font class="error" color="red">*</font></td>
                        
                        <td align="left" >
                            <asp:TextBox id="txtFrom" runat="server" >
                            </asp:TextBox>
                            <asp:ImageButton id="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif">
                            </asp:ImageButton>
                            <asp:RequiredFieldValidator id="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                CssClass="error" Display="Dynamic" ErrorMessage="From Date required" ForeColor=""
                                ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                        <td align="left" >
                           <span class="field-label">  To Date</span><font class="error" color="red">*</font></td>
                        
                        <td align="left" >
                            <asp:TextBox id="txtTo" runat="server" >
                            </asp:TextBox>
                            <asp:ImageButton id="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif">
                            </asp:ImageButton>
                            <asp:RequiredFieldValidator id="rfvTo" runat="server" ControlToValidate="txtTo" CssClass="error"
                                Display="Dynamic" ErrorMessage="To Date required" ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label"> Leave Status</span></td>
                      
                        <td align="left" colspan="3" >
                            <asp:DropDownList id="ddlAPD_ID" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label"> Remarks</span><font class="error" color="red">*</font></td>
                       
                        <td align="left" colspan="3" >
                            <asp:TextBox id="txtRemarks" runat="server" SkinID="MultiText" TextMode="MultiLine"
                                Width="220px">
                            </asp:TextBox><asp:RequiredFieldValidator id="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                CssClass="error" Display="Dynamic" ErrorMessage="Remarks field can not be left empty"
                                ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" >
                           <span class="field-label">  Status</span></td>
                       
                        <td align="left" colspan="3" >
                            <asp:RadioButton id="rbApproved" runat="server" GroupName="approve" Text="Approve" CssClass="field-label">
                            </asp:RadioButton>
                            <asp:RadioButton id="rbNotApproved" runat="server" GroupName="approve" Text="Reject" CssClass="field-label">
                            </asp:RadioButton></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button id="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    onclick="btnEdit_Click" Text="Edit" /><asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                <asp:Button id="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /></td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgCalendar" TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="ImageButton1" TargetControlID="txtTo">
    </ajaxToolkit:CalendarExtender>

                
            </div>
        </div>
    </div>
</asp:Content>

