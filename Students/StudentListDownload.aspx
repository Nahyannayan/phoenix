﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="StudentListDownload.aspx.vb" Inherits="StudentListDownload" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Student Photo Download"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <div align="center" class="matters">
                    <table width="100%">
                        <tr>
                            <td width="10%">
                                <span class="field-label">Business Unit</span>
                            </td>
                            <td width="40%">
                                <asp:DropDownList ID="ddbsu" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td width="20%">
                                <span class="field-label">Download For </span>
                            </td>
                            <td width="30%">
                                <asp:DropDownList ID="ddlCat" runat="server">
                                    <asp:ListItem Value="Student">Student</asp:ListItem>
                                    <asp:ListItem Value="Father">Father</asp:ListItem>
                                    <asp:ListItem Value="Mother">Mother</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnDownload" runat="server" Text="Download" CssClass="button" OnClick="btnupload_Click" Width="134px" ValidationGroup="vg1" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblmessage" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblStatus" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:CheckBox ID="CheckError" runat="server" AutoPostBack="True" CssClass="field-label"
                                    Text="Show Error List" Visible="False" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select a Business Unit" ValidationGroup="vg1"
        ControlToValidate="ddbsu" Display="None" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <%# Eval("File_Name") %>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ValidationGroup="vg1"
        ShowSummary="False" />
    <asp:HiddenField ID="HiddenPostBack" Value="0" runat="server" />
</asp:Content>
