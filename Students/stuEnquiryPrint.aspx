<%@ Page Language="VB" AutoEventWireup="false" CodeFile="stuEnquiryPrint.aspx.vb" Inherits="Students_stuEnquiryPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd Xhtml 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">

<!DOCTYPE html PUBLIC "-//W3C//Dtd html 4.0 transitional//EN">
<!-- saved from url=(0038)http://live.gemsoasis.com/printenq.php -->
<html>
<head>
    <title>:::: GEMS :::: Enquiry Details ::::</title>
    <%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <style type="text/css">
        .style2 {
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            width: 3px;
        }

        .style3 {
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            width: 3px;
        }

        .style4 {
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            width: 3px;
        }

        .style5 {
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
        }

        .style6 {
            /*font-family: Verdana, Arial, Helvetica, sans-serif;*/
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            width: 3px;
        }
    </style>



</head>

<body topmargin="1px">
    <form id="Form1" runat="server">
        <table cellspacing="0" cellpadding="5" width="90%" align="center" border="0">
            <tbody>
                <tr>
                    <td align="center" class="title" colspan="4" valign="middle">
                        <table cellspacing="0" cellpadding="0" align="center" border="0" width="100%">
                            <tbody>
                                <tr>
                                    <td valign="top" align="left" width="15%">
                                        <asp:Image ID="imglogo" runat="server" Height="50" />
                                    </td>
                                    <td align="left" width="100%">
                                        <table width="70%" border="0" align="center">
                                            <tbody>
                                                <tr>
                                                    <td align="center"><span id="BsuName" runat="server" class="field-label"></span></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <%-- <tr>
     
    <td class=mattersmis align="center" colSpan=2 height=18><IMG 
      src="../Images/colourbar.jpg" width="100%" height="3px"></td></tr>--%>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr class="title-bg-lite">
                    <td valign="middle" align="center" colspan="4">Enquiry 
      Details </td>
                </tr>
            </tbody>
        </table>
        <table style="width: 90%" align="center" class="table table-bordered table-row">
            <thead>
                <tr>
                    <th colspan="4">Applicant Details
                    </th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td width="160px" class="text-block">Name</td>
                    
                    <td colspan="3">
                        <span class="text-capitalize">
                            <strong><asp:Literal ID="ltName" runat="server"></asp:Literal></strong></span></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Application No</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltAppNo" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Enquiry Date</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltEnqDate" runat="server"></asp:Literal></td>
                </tr>

                <tr>
                    <td width="160px" class="text-block">Registration Date</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltRegDate" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Student Id</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltStuId" runat="server"></asp:Literal></td>
                </tr>


                <tr>
                    <td width="160px" class="text-block">Academic Year</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltAcdYear" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Tentative Joining Date</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltTenJoinDate" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Grade</td>
                    
                    <td width="160px">
                        <asp:Literal ID="ltGrade" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Shift</td>
                    
                    <td width="160px">
                        <asp:Literal ID="ltShift" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Curriculum</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Stream</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltStream" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Gender</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGender" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Religion</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltReg" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Date of Birth</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltDob" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Place of Birth</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltPOB" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Country of Birth</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCOB" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Nationality</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltNat" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Emergency Contact No.</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltEmgNo" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Employer</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltEmployer_ShortEnq" runat="server"></asp:Literal></td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4">Passport Details 
                    </td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Passport No</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltPassNo" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Issue Place</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltIssPlace" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Issue Date</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltIssDate" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Expiry Date</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltExpDate" runat="server"></asp:Literal></td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4" >Visa Details 
                    </td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Visa No</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltVisaNo" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Issue Place</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltIssPlace_V" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Issue Date</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltIssDate_V" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Expiry Date</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltExpr_Date_V" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Issuing Authority</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltIssAuth" runat="server"></asp:Literal></td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4">Language Details </td>
                </tr>

                <tr>
                    <td width="160px" class="text-block">First Language</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFirstLang" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Other Languages</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltOthLang" runat="server"></asp:Literal></td>
                </tr>

                <tr>
                    <td width="160px" class="text-block">Proficiency in English</td>
                    
                    <td width="160px" colspan="3">
                        <table style="width:100%" border="0">
                            <tr>
                                <td class="text-block">Reading</td>
                                <td>:</td>
                                <td>

                                    <asp:Literal ID="ltProEng_R" runat="server"></asp:Literal></td>
                                <td class="text-block">Writing
                                </td>
                                <td>:</td>
                                <td>
                                    <asp:Literal ID="ltProEng_W" runat="server"></asp:Literal>
                                </td>
                                <td class="text-block">Speaking</td>
                                <td>:</td>
                                <td>
                                    <asp:Literal ID="ltProEng_S" runat="server"></asp:Literal></td>
                            </tr>
                        </table>
                    </td>
                </tr>




                <tr class="title-bg-lite">
                    <td colspan="4">Sibling Details </td>
                </tr>
                <!-- Grade & Section -->
                <!-- Name -->

                <tr>
                    <td colspan="4">
                        <span class="text-info">If any of your child is already studying in GEMS &nbsp;school,
                  please provide one of the sibling details</span>

                        <asp:Literal ID="ltSib" runat="server"></asp:Literal>&nbsp;</td>
                </tr>
                <tr id="trSib_1" runat="server">
                    <td width="160px" class="text-block">Sibling Name</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltSibName" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Sibling Fee ID</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltSibFeeID" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trSib_2" runat="server">
                    <td width="160px" class="text-block">GEMS School</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltSibGEMS" runat="server"></asp:Literal></td>
                </tr>







                <tr>
                    <td colspan="4" class="title-bg-lite">Primary Contact Details </td>
                </tr>
                <!-- Grade & Section -->
                <!-- Name -->
                <tr>
                    <td width="160px" class="text-block">Primary Contact</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltPri_cont" runat="server"></asp:Literal></td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4" class="text-block">Father's Details
                    </td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Name</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltFather_name" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Nationality</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltFnationality" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Nationality2</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFNat2" runat="server"></asp:Literal></td>
                </tr>
                <!-- Full Name In Passport -->
                <!-- Genmder -->
                <tr>
                    <td colspan="4">
                        <span class="text-info">Is father of Student an employee with G E M S ?</span> &nbsp;
                            <asp:Literal
                                ID="ltF_GEMS" runat="server"></asp:Literal>&nbsp;</td>
                </tr>
                <tr id="trF_GEMS" runat="server">
                    <td width="160px" class="text-block">Staff ID</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFStaffID" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Business unit</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFStaff_BSU" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <span class="text-info">Is father of Student an Ex-Student of a  G E M S School?</span>&nbsp;
                  
                      <asp:Literal ID="ltF_ExGEms" runat="server"></asp:Literal>&nbsp;</td>
                </tr>
                <tr id="trF_ExGEMS" runat="server">
                    <td width="160px" class="text-block">Academic Year</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFStaff_ACD" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">GEMS School</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFStaff_GEMS_School" runat="server"></asp:Literal></td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4" class="text-block">Permanent Address
                    </td>
                </tr>
                <!-- Student Id -->
                <tr>
                    <td width="160px" class="text-block">Address Line 1</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFAdd1" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Address Line 2</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFAdd2" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Address City/State</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFAddCity" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Address Country</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFAdd_Country" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Phone</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFAddPhone" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">P.O Box/Zip Code</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFAdd_Zip" runat="server"></asp:Literal></td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4">Current Address        </td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Apartment No</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFApart" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Building</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFBuild" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Street</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFStreet" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Area</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFArea" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">P.O Box</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltFpobox" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">City/Emirate</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltFemirate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Country</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltFCurr_Contry" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Home Tel No.</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltFhometelphone" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Office Tel No.</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltFofficetelephone" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Mobile No</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltFmobile" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Email</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltFemail" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Fax No</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltFfax" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Prefered Contact</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltFpreferedcontact" runat="server"></asp:Label>
                    </td>
                </tr>
                <!-- OverSeas Contact-->
                <tr>
                    <td width="160px" class="text-block">Occupation</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFOcc" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Company</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltFComp" runat="server"></asp:Literal></td>
                </tr>


                <tr class="title-bg-lite">
                    <td colspan="4">Mother's Details
                    </td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Name</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltMother_name" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Nationality</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltMnationality" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Nationality2</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMNat2" runat="server"></asp:Literal></td>
                </tr>
                <!-- Full Name In Passport -->
                <!-- Genmder -->
                <tr>
                    <td colspan="4">
                        <span class="text-info">Is mother of Student an employee with G E M S ?</span> &nbsp;
                            <asp:Literal
                                ID="ltM_GEMS" runat="server"></asp:Literal>&nbsp;</td>
                </tr>
                <tr id="trM_GEMS" runat="server">
                    <td width="160px" class="text-block">Staff ID</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMStaffID" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Business unit</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMStaff_BSU" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <span class="text-info">Is mother of Student an Ex-Student of a  G E M S School?</span>&nbsp;
                  
                      <asp:Literal ID="ltM_ExGEms" runat="server"></asp:Literal>&nbsp;</td>
                </tr>
                <tr id="trM_ExGEMS" runat="server">
                    <td width="160px" class="text-block">Academic Year</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMStaff_ACD" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">GEMS School</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMStaff_GEMS_School" runat="server"></asp:Literal></td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4">Permanent Address
                    </td>
                </tr>
                <!-- Student Id -->
                <tr>
                    <td width="160px" class="text-block">Address Line 1</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMAdd1" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Address Line 2</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMAdd2" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Address City/State</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMAddCity" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Address Country</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMAdd_Country" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Phone</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMAddPhone" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">P.O Box/Zip Code</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMAdd_Zip" runat="server"></asp:Literal></td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4">Current Address        </td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Apartment No</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMApart" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Building</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMBuild" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Street</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMStreet" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Area</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMArea" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">P.O Box</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltMpobox" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">City/Emirate</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltMemirate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Country</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltMCurr_Contry" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Home Tel No.</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltMhometelphone" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Office Tel No.</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltMofficetelephone" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Mobile No</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltMmobile" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Email</td>
                    
                    <td width="160px">&nbsp;<asp:Label ID="ltMemail" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Fax No</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltMfax" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Prefered Contact</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltMpreferedcontact" runat="server"></asp:Label>
                    </td>
                </tr>
                <!-- OverSeas Contact-->
                <tr>
                    <td width="160px" class="text-block">Occupation</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMOcc" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Company</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMComp" runat="server"></asp:Literal></td>
                </tr>


                <tr id="trGr1" runat="server" class="title-bg-lite">
                    <td colspan="4">Guardian's Details
                    </td>
                </tr>
                <tr id="trGr2" runat="server">
                    <td width="160px" class="text-block">Name</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltGuardian_name" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trGr3" runat="server">
                    <td width="160px" class="text-block">Nationality</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltGnationality" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Nationality2</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGNat2" runat="server"></asp:Literal></td>
                </tr>
                <!-- Full Name In Passport -->
                <!-- Genmder -->

                <tr id="trGr4" runat="server" class="title-bg-lite">
                    <td colspan="4">Permanent Address
                    </td>
                </tr>
                <!-- Student Id -->
                <tr id="trGr5" runat="server">
                    <td width="160px" class="text-block">Address Line 1</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGAdd1" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Address Line 2</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGAdd2" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trGr6" runat="server">
                    <td width="160px" class="text-block">Address City/State</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGAddCity" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Address Country</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGAdd_Country" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trGr7" runat="server">
                    <td width="160px" class="text-block">Phone</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGAddPhone" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">P.O Box/Zip Code</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGAdd_Zip" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trGr8" runat="server" class="title-bg-lite">
                    <td colspan="4" class="text-block">Current Address        </td>
                </tr>
                <tr id="trGr9" runat="server">
                    <td width="160px" class="text-block">Apartment No</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGApart" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Building</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGBuild" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trGr10" runat="server">
                    <td width="160px" class="text-block">Street</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGStreet" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Area</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGArea" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trGr11" runat="server">
                    <td width="160px" class="text-block">P.O Box</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltGpobox" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">City/Emirate</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltGemirate" runat="server"></asp:Label></td>
                </tr>
                <tr id="trGr12" runat="server">
                    <td width="160px" class="text-block">Country</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltGCurr_Contry" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trGr13" runat="server">
                    <td width="160px" class="text-block">Home Tel No.</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltGhometelphone" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Office Tel No.</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltGofficetelephone" runat="server"></asp:Label></td>
                </tr>
                <tr id="trGr14" runat="server">
                    <td width="160px" class="text-block">Mobile No</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltGmobile" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Email</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltGemail" runat="server"></asp:Label></td>
                </tr>
                <tr id="trGr15" runat="server">
                    <td width="160px" class="text-block">Fax No</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltGfax" runat="server"></asp:Label></td>
                    <td width="160px" class="text-block">Prefered Contact</td>
                    
                    <td width="160px">

                        <asp:Label ID="ltGpreferedcontact" runat="server"></asp:Label>
                    </td>
                </tr>
                <!-- OverSeas Contact-->
                <tr id="trGr16" runat="server">
                    <td width="160px" class="text-block">Occupation</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGOcc" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Company</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltGComp" runat="server"></asp:Literal></td>
                </tr>



                <tr class="title-bg-lite">
                    <td colspan="4">School Details</td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4">Current School Details</td>
                </tr>

                <tr>
                    <td width="160px" class="text-block">School Name</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_School" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Student Id</td>
                    
                    <td width="160px">&nbsp;<asp:Literal ID="ltCurr_Stu_id" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trCurrSch_1" runat="server">
                    <td width="160px" class="text-block">Name of the head teacher</td>
                    
                    <td width="160px" colspan="3">

                        <asp:Literal ID="ltCurr_Head" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trCurrSch_2" runat="server">
                    <td width="160px" class="text-block">Grade</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_Grade" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Curriculum</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_Curr" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trCurrSch_3" runat="server">
                    <td width="160px" class="text-block">Language of Instruction</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_Instr" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Address</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_Addr" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trCurrSch_4" runat="server">
                    <td width="160px" class="text-block">City</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_City" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Country</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_Country" runat="server"></asp:Literal></td>
                </tr>

                <tr id="trCurrSch_5" runat="server">
                    <td width="160px" class="text-block">School Phone</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_Sch_ph" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">School Fax</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_Sch_fax" runat="server"></asp:Literal></td>
                </tr>

                <tr id="trCurrSch_6" runat="server">
                    <td width="160px" class="text-block">From Date</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_FromDT" runat="server"></asp:Literal></td>

                    <td width="160px" class="text-block">To Date</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltCurr_ToDT" runat="server"></asp:Literal></td>
                </tr>

                <tr id="trPrevSCH_id" runat="server" class="title-bg-lite">
                    <td colspan="4">Previous School Details</td>
                </tr>

                <tr>
                    <td colspan="4" align="center">
                        <asp:GridView ID="gvSchool" runat="server" AutoGenerateColumns="False"
                            EmptyDataText="No record added yet" CssClass="table table-bordered table-row"
                            EnableModelValidation="True"
                            Width="100%">
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="School Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSCH_NAME" runat="server" Text='<%# Bind("SCH_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="185px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Head Teacher">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSCH_HEAD" runat="server" Text='<%# Bind("SCH_HEAD") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Grade">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSCH_GRADE" runat="server" Text='<%# BIND("SCH_GRADE") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Curriculum">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# bind("CLM_DESCR") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Language">
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# bind("LANG_INST") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="School Ph. No">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# bind("SCH_PHONE") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="From Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSCH_FROMDT" runat="server" Text='<%# bind("SCH_FROMDT") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="To Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSchName" runat="server" Text='<%# bind("SCH_TODT") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblToDt" runat="server" Text='<%# bind("SCH_TODT") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="Khaki" />
                            <HeaderStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>



                <tr class="title-bg-lite">
                    <td colspan="4">Health Information</td>
                </tr>
                <tr>
                    <td width="160px" class="text-block">Health Card No / Medical Insurance No</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltHth_No" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Blood Group</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltB_grp" runat="server"></asp:Literal></td>
                </tr>
                <tr class="title-bg-lite">
                    <td colspan="4">Health Restriction</td>
                </tr>
                <tr>
                    <td colspan="4">Does your child have any allergies? &nbsp;<asp:Literal ID="ltAlg_Detail"
                        runat="server" EnableViewState="False"></asp:Literal>

                        <div id="divAlg" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">Does your child have any prescribed Special Medication? 
                        <asp:Literal ID="ltSp_med" runat="server"></asp:Literal>
                        <div id="divSp_med" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>
                </tr>


                <tr>
                    <td colspan="4">Is there any Physical Education Restrictions for your child?       
                        <asp:Literal ID="ltPhy_edu" runat="server"></asp:Literal>
                        <div id="divPhy_edu" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">Any other information related to health issue of your child the school should be aware of?
        
            <asp:Literal ID="ltHth_Info" runat="server"></asp:Literal>
                        <div id="divHth_Info" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>
                </tr>

                <tr class="title-bg-lite">
                    <td colspan="4">Applicant's disciplinary, social, physical or psychological detail</td>
                </tr>

                <tr>
                    <td colspan="4">Has the child received any sort of learning support or theraphy?
        
            <asp:Literal ID="ltLS_Ther" runat="server"></asp:Literal>
                        <div id="divLS_Ther" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">Does the child require any special education needs?
        
            <asp:Literal ID="ltSEN" runat="server"></asp:Literal>
                        <div id="divSEN" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">Does the student require English support as Additional Language program (EAL) ?
          
              <asp:Literal ID="ltEAL" runat="server"></asp:Literal>
                        <div id="divEAL" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">Has your child's behaviour been any cause for concern in previous schools ?
                        <asp:Literal ID="ltPrev_sch" runat="server"></asp:Literal>
                        <div id="divPrev_sch" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>

                </tr>

                <tr class="title-bg-lite">
                    <td colspan="4">Gifted and talented</td>
                </tr>


                <tr>
                    <td colspan="4">Has your child ever been selected for specific enrichment activities? 
                        <asp:Literal ID="ltSEA"
                            runat="server"></asp:Literal>
                        <div id="divSEA" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">Is your child musically proficient? 
                        <asp:Literal
                            ID="ltMus_Prof" runat="server"></asp:Literal>
                        <div id="divMus_Prof" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>

                </tr>

                <tr>
                    <td colspan="4">Has your child represented a school or country in sport? 
                        <asp:Literal
                            ID="ltSport" runat="server"></asp:Literal>
                        <div id="divSport" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                    </td>
                </tr>




                <tr id="trTranSch_1" runat="server" class="title-bg-lite">
                    <td colspan="4">Transport Information</td>
                </tr>


                <tr id="trTranSch_2" runat="server">
                    <td width="160px" class="text-block">Main Location</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltMain_Loc" runat="server"></asp:Literal></td>
                    <td width="160px" class="text-block">Sub-Location</td>
                    
                    <td width="160px">

                        <asp:Literal ID="ltSub_Loc" runat="server"></asp:Literal></td>
                </tr>
                <tr id="trTranSch_3" runat="server">
                    <td width="160px" class="text-block">Pick Up Point</td>
                    
                    <td colspan="3">

                        <asp:Literal ID="ltPick_Up" runat="server"></asp:Literal>&nbsp;
                  <asp:Literal ID="ltTransOpt" runat="server"></asp:Literal></td>
                </tr>

                <tr class="title-bg-lite">
                    <td colspan="4">Other Information</td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Repeater ID="rptQus" runat="server">
                            <ItemTemplate>
                                <div style="color: #1b80b6;">
                                    <%# Eval("QUS")%>&nbsp;<%#Eval("ANS")%><hr style="color: #1b80b6;">
                                </div>

                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>


                <tr>
                    <td colspan="4">Are there any family circumstances of which you feel we should be aware of?
(eg Deceased parent /divorced/separated / adopted /others if so please give detail)<br />
                       <asp:Literal ID="ltFamily_detail" runat="server"></asp:Literal></td>
                </tr>

                <tr>
                    <td colspan="4">The application is filled by 
                        <asp:Literal
                            ID="ltFilled_by" runat="server"></asp:Literal></td>
                </tr>

                <tr>
                    <td colspan="4">G E M S may use the above Email Address to send Newsletters and promotional mails.<asp:Literal
                        ID="ltEmail_promo" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td colspan="4">G E M S may use the above Mobile Number to send text messages and alerts.<asp:Literal
                        ID="ltMob_promo" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td colspan="4">GEMS has permission to include child in publication/promotion photos and videos.<asp:Literal
                        ID="ltPhoto_promo" runat="server"></asp:Literal></td>
                </tr>




            </tbody>
        </table>
        <asp:HiddenField ID="hfENQ_ID" runat="server" />
        <asp:HiddenField ID="hfEQS_ID" runat="server" />
        <asp:HiddenField ID="HiddenBsuId" runat="server" />
        <asp:HiddenField ID="HiddenAcademicyear" runat="server" />

    </form>
</body>

</html>
