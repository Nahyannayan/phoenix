<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowStudent.aspx.vb" Inherits="ShowStudent" Theme="General" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" >
    <link href="../cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="../cssfiles/jquery-ui.structure.min.css" rel="stylesheet">
    <script language="javascript" type="text/javascript" src="../chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        function listen_window() {
        }
        function GetRadWindow() {

            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

    </script>
</head>
<body class="matter" onload="listen_window();">
    <form id="form1" runat="server">
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data" AllowPaging="True" PageSize="20" CssClass="table table-bordered table-row">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Student#<br />
                                    <asp:TextBox ID="txtCode" runat="server" ></asp:TextBox>
                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="ImageButton1_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Student Name<br />
                                    <asp:TextBox ID="txtName" runat="server" width="75%" ></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchName" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchName_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                   <asp:LinkButton ID="lbCodeSubmit" runat="server" OnClick="LinkButton1_Click" Text='<%# Bind("STU_NAME") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Grade<br />
                                    <asp:TextBox ID="txtGrade" runat="server" width="75%" ></asp:TextBox>
                                    <asp:ImageButton ID="btnSearchControl" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchControl_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Parent Name<br />
                                    <asp:TextBox ID="txtPName" runat="server" width="75%" ></asp:TextBox></td>
                                                                <asp:ImageButton ID="btnBankACSearch" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif"
                                                                    OnClick="ImageButton1_Click" />

                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Parent Mobile">
                                <HeaderTemplate>
                                    Parent Mobile<br />
                                    <asp:TextBox ID="txtMobile" runat="server"  width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnNarration" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                        OnClick="btnSearchName_Click" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("PARENT_MOBILE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                    <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                    <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
