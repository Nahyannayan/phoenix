﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studLibLabClearance_M.aspx.vb" Inherits="Students_studLibLabClearance_M"
    Title="Untitled Page" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <style>

        .RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}

.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
    </style>
    <script language="javascript" type="text/javascript">

        function getLibrarian() {
            var sFeatures;
            sFeatures = "dialogWidth: 445px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Students/studShowEmpDetailWithEmail.aspx';


            result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }

            NameandCode = result.split('|');
      <%--      document.getElementById("<%=txtLibrarian.ClientID %>").value = NameandCode[0];--%>
        document.getElementById("<%=txtLibEmail.ClientID %>").value = NameandCode[2];
        document.getElementById("<%=hfEMP_ID_LIB.ClientID %>").value = NameandCode[1];

        // return false;

    }


    function getLabAsst() {
        var sFeatures;
        sFeatures = "dialogWidth: 445px; ";
        sFeatures += "dialogHeight: 310px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        url = '../Students/studShowEmpDetailWithEmail.aspx';


        result = window.showModalDialog(url, "", sFeatures);

        if (result == '' || result == undefined) {
            return false;
        }

        NameandCode = result.split('|');
  <%--      document.getElementById("<%=txtLabAsst.ClientID %>").value = NameandCode[0];--%>
        document.getElementById("<%=txtLabEmail.ClientID %>").value = NameandCode[2];
        document.getElementById("<%=hfEMP_ID_LAB.ClientID %>").value = NameandCode[1];

        return false;

    }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Library & Lab Clearance Setup
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Grade</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" colspan="2"></td>

                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Select Librarian  </span>
                                    </td>

                                    <td align="left" >
                                        <%--<asp:TextBox ID="txtLibrarian" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgSub" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="javascript:getLibrarian();return false;" />--%>
                                          <telerik:RadComboBox RenderMode="Lightweight" ID="ddlLibrarian" runat="server"  AutoPostBack="true" AllowCustomText="true" Filter="Contains" MarkFirstMatch="true"  
                     OnSelectedIndexChanged="ddlLibrarian_SelectedIndexChanged"  Width="100%" >
                </telerik:RadComboBox>

                                    </td>
                                    <td align="left" >
                                        <span class="field-label">Email ID</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtLibEmail" runat="server" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span class="field-label">Select Lab Asst</span>
                                    </td>

                                    <td align="left" >
                                      <%--  <asp:TextBox ID="txtLabAsst" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgSub0" runat="server" ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="return getLabAsst();" />--%>
                                         <telerik:RadComboBox RenderMode="Lightweight" ID="ddlLabAsst" runat="server"  AutoPostBack="true" AllowCustomText="true" Filter="Contains" MarkFirstMatch="true"
                     OnSelectedIndexChanged="ddlLabAsst_SelectedIndexChanged"  Width="100%" >
                </telerik:RadComboBox>
                                    </td>
                                    <td align="left" >
                                        <span class="field-label">Email ID</span>
                                    </td>

                                    <td align="left" >
                                        <asp:TextBox ID="txtLabEmail" runat="server" ></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" ValidationGroup="groupM1" />
                                    </td>
                                </tr>
                                <tr id="trGrid" runat="server">
                                    <td align="left" colspan="4">
                                        <table id="Table4" runat="server" align="center" border="0"  cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" >
                                                    <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="No records to edit." width="100%"
                                                        PageSize="20" BorderStyle="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="lib_emp_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLibEmpId" runat="server" Text='<%# Bind("SCL_LIB_EMP_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="lab_emp_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLabEmpId" runat="server" Text='<%# Bind("SCL_LAB_EMP_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Grade">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("SCL_GRD_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Librarian">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLibrarian" runat="server" Text='<%# Bind("EMP_NAME_LIBRARIAN") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Librarian Email">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLibEmail" runat="server" Text='<%# Bind("SCL_LIB_EMAIL") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Lab Asst">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLabAsst" runat="server" Text='<%# Bind("EMP_NAME_LAB") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Lab Asst Email">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLabEmail" runat="server" Text='<%# Bind("SCL_LAB_EMAIL") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                            <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("UNIQUEID") %>'
                                                                        CommandName="delete" Text="Delete"></asp:LinkButton>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Details will be deleted permanently.Are you sure you want to continue?"
                                                                        runat="server">
                                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle  />
                                                        <RowStyle CssClass="griditem" />
                                                        <SelectedRowStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <EmptyDataRowStyle  />
                                                        <EditRowStyle />
                                                    </asp:GridView>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfEMP_ID_LAB" runat="server" />
                <asp:HiddenField ID="hfEMP_ID_LIB" runat="server" />
                <asp:HiddenField ID="hfEMP_ID_LAB_ORG" runat="server" />
                <asp:HiddenField ID="hfEMP_ID_LIB_ORG" runat="server" />



            </div>
        </div>
    </div>

</asp:Content>
