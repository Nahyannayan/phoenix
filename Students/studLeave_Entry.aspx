<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studLeave_Entry.aspx.vb" Inherits="Students_studLeave_Entry" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Leave Approval Entry"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

  
<%--<table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%">
               LEAVE APPROVAL ENTRY</td>
        </tr>
    </table>--%>
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" >
             
                    <div align="left">
                        <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                            ></asp:Label></div>
                    <div align="left">
                        <asp:ValidationSummary id="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"  ValidationGroup="AttGroup">
                        </asp:ValidationSummary></div>
                
            </td>
        </tr>
        <tr >
            <%--<td align="center" class="matters" style="height: 22px" valign="middle">
                &nbsp;Fields Marked with(<span style="font-size: 8pt; color: #800000">*</span>)are mandatory</td>--%>
             <td align="center" class="text-danger font-small" valign="middle">
                Fields Marked with ( * ) are mandatory
            </td>
        </tr>
        <tr>
            <td >
                <table align="center" border="0"  cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr>
                        <td  colspan="4">
                         
                                <asp:Literal id="ltLabel" runat="server" Text="Leave Approval Entry"></asp:Literal></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="15%">
                            <span class="field-label"> Academic Year</span><span class="text-danger font-small">*</span></td>
                       
                        <td align="left" width="35%">
                            <asp:DropDownList id="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                            </asp:DropDownList></td>

                          <td align="left" >
                           <span class="field-label">  As on</span></td>
                       
                        <td align="left"  >
                            <asp:TextBox id="txtDate" runat="server" >
                            </asp:TextBox>&nbsp;<asp:ImageButton id="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator
                                    id="rfvDate" runat="server" ControlToValidate="txtDate" CssClass="error" Display="Dynamic"
                                    ErrorMessage="As on  Date required" ForeColor="" ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                    </tr>
                   <%-- <tr>
                      
                    </tr>--%>
                    <tr>
                        <td align="left" >
                           <span class="field-label">  Stream</span><span class="text-danger font-small">*</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList id="ddlStream" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" >
                            <span class="field-label"> Shift</span><span class="text-danger font-small">*</span></td>
                      
                        <td align="left" >
                            <asp:DropDownList id="ddlShift" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" >
                           <span class="field-label">  Grade</span><span class="text-danger font-small">*</span></td>
                    
                        <td align="left" >
                            <asp:DropDownList id="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" >
                            <span class="field-label"> Section</span><span class="text-danger font-small">*</span></td>
                        
                        <td align="left" >
                            <asp:DropDownList id="ddlSection" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
           
                <asp:Button id="btnSubmit" runat="server" CausesValidation="False" CssClass="button"
                     Text="Submit" OnClick="btnSubmit_Click" ValidationGroup="AttGroup" /></td>
        </tr>
        <tr>
            <td >
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:GridView id="gvInfo" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                    DataKeyNames="SRNO"  Width="100%" UseAccessibleHeader="False">
                    <rowstyle cssclass="griditem" />
                    <columns>
<asp:TemplateField HeaderText="Sl.No"><ItemTemplate>
<asp:Label id="lblsNo" runat="server" Text='<%# Bind("SRNO") %>' __designer:wfdid="w15"></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student ID"><EditItemTemplate>
<asp:TextBox id="TextBox5" runat="server" ></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblStudNo" runat="server" Text='<%# bind("STU_NO") %>' ></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student ID_1" Visible="False"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="lblStud_ID" runat="server" Text='<%# Bind("STU_ID") %>' ></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Student Name"><EditItemTemplate>
&nbsp; 
</EditItemTemplate>
<ItemTemplate>
&nbsp; <asp:Label id="lblStudName" runat="server" Text='<%# Bind("STUDNAME") %>' ></asp:Label> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="View"><EditItemTemplate>
<asp:TextBox id="TextBox4" runat="server" ></asp:TextBox> 
</EditItemTemplate>
<ItemTemplate>
&nbsp;<asp:LinkButton id="lblView" onclick="lblView_Click" runat="server" >View</asp:LinkButton>
</ItemTemplate>
</asp:TemplateField>
</columns>
                    <selectedrowstyle  />
                    <headerstyle  horizontalalign="Center" verticalalign="Middle" />
                    <alternatingrowstyle cssclass="griditem_alternative"  />
                </asp:GridView></td>
        </tr>
        <tr>
            <td >
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgCalendar" TargetControlID="txtDate">
    </ajaxToolkit:CalendarExtender>
                
            </div>
        </div>
    </div>
</asp:Content>

