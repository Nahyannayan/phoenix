<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studStrikeOffCancel_M.aspx.vb" Inherits="Students_studStrikeOffCancel_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Strike Off Cancellation
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" />
                            &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">Fields Marked with (<span class="text-danger">*</span>) are mandatory      
                        </td>
                    </tr>
                    <tr>
                        <td width="100%">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Student Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtName" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">SEN</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSEN" runat="server" ReadOnly="True"></asp:TextBox></td>
                                </tr>

                                <tr>

                                    <td align="left">
                                        <span class="field-label">Last Attendance Date</span></td>


                                    <td align="left">
                                        <asp:TextBox ID="txtLast" runat="server" ReadOnly="True">
                                        </asp:TextBox>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <span class="field-label">Recommended Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRec" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRecRemarks" runat="server" TextMode="MultiLine"  ReadOnly="True">
                                        </asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td align="left">
                                        <span class="field-label">Approval Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtApr" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtAprRemarks" runat="server" TextMode="MultiLine"  ReadOnly="True">
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Strike Off</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStrike" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStrikeRemarks" runat="server" TextMode="MultiLine"  ReadOnly="True"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Cancel Request Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtReqDate" runat="server" ReadOnly="True">
                                        </asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtReqRemarks" runat="server" TextMode="MultiLine"  ReadOnly="True">
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Fee Pending(AED)</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFee" runat="server" ReadOnly="True">
                                        </asp:TextBox></td>
                                    <td align="left">
                                        <asp:LinkButton ID="LinkButton1" runat="server" Visible="False">LinkButton</asp:LinkButton></td>
                                </tr>



                                <tr>
                                    <td align="left" colspan="4" class="title-bg-lite">Details</td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Cancel Date</span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                        <%-- <asp:ImageButton ID="imgStrike" runat="server" ImageUrl="~/Images/calendar.gif" />--%></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Cancel Reason</span><span class="text-danger">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>



                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center">&nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">&nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDate"
                    Display="None" ErrorMessage="Please enter data in the field Cancel Request  Date" ValidationGroup="groupM1"
                    Width="23px"></asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfTCM_ID" runat="server" />
                            <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <%--<ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="txtDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>--%>
                &nbsp;
               <%-- <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                    Format="dd/MMM/yyyy" PopupButtonID="imgStrike" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>--%>
                            <asp:HiddenField ID="hfMode" runat="server" />
                            &nbsp; &nbsp;
                &nbsp;
                &nbsp;&nbsp;
                &nbsp;&nbsp;
                <asp:HiddenField ID="HF_STK_ID" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

