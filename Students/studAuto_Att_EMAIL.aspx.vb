Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Globalization

Partial Class studAutoAttEMAIL
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Dim Session("dsEvents") As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")



        If Page.IsPostBack = False Then

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state
            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050119") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                plEmailContent.Visible = False
                txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                bindHrs()
                bindMin()
                bindGrades()
                bindParamAtt()
                bindEMAIL_SCHOOL_SCHEDULE()
                bindEmail_Content()
            End If
        End If
        'Dim ToolkitScriptManager1 As AjaxControlToolkit.ToolkitScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me.Page)
        ' ToolkitScriptManager1.RegisterPostBackControl(lbtnEdit)
    End Sub
    Private Sub bindGrades()
        chkGrd.Items.Clear()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(2) = New SqlParameter("@INFO_TYPE", "GRADE")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ATT.GETSCHEDULE_ATT_EMAIL_DATABINDS", param)
        chkGrd.DataSource = ds.Tables(0)
        chkGrd.DataTextField = "GRM_DISPLAY"
        chkGrd.DataValueField = "GRM_GRD_ID"
        chkGrd.DataBind()

    End Sub
    Private Sub bindParamAtt()
        chkparam.Items.Clear()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        param(2) = New SqlParameter("@INFO_TYPE", "ATT_PARAM")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ATT.GETSCHEDULE_ATT_EMAIL_DATABINDS", param)
        chkparam.DataSource = ds.Tables(0)
        chkparam.DataTextField = "APD_PARAM_DESCR"
        chkparam.DataValueField = "APD_ID"
        chkparam.DataBind()

    End Sub
    Private Sub bindEmail_Content()

        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ATT.GETSCHEDULE_ATT_EMAIL_CONTENT", param)
            If datareader.HasRows = True Then
                While datareader.Read
                    txtEmailContent.Content = Convert.ToString(datareader("SAE_EMAILCONTENT"))
                End While
            Else
                txtEmailContent.Content = ""
            End If
        End Using

    End Sub
    Private Sub bindHrs()
        For i As Integer = 8 To 18
            If i < 10 Then

                ddlHrs.Items.Add(New ListItem("0" + i.ToString, "0" + i.ToString))
            Else
                ddlHrs.Items.Add(New ListItem(i, i))
            End If

        Next

        ddlHrs.DataBind()



    End Sub
    Private Sub bindMin()
        For i As Integer = 0 To 60 Step 5
            If i < 10 Then

                ddlMin.Items.Add(New ListItem("0" + i.ToString, "0" + i.ToString))
            Else

                ddlMin.Items.Add(New ListItem(i, i))
            End If

        Next
        ddlMin.DataBind()


    End Sub
    Private Sub bindEMAIL_SCHOOL_SCHEDULE()
        Dim arInfo As String() = New String(1) {}
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ATT.GETSCHEDULE_ATT_EMAIL_BSU", param)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read
                    Dim GRDS As String = Convert.ToString(DATAREADER("SAE_GRD_IDS"))
                    Dim ATT_PARAMETERS As String = Convert.ToString(DATAREADER("SAE_PARAMETERS"))
                    txtFrom.Text = Convert.ToString(DATAREADER("FROMDT"))
                    txtTo.Text = Convert.ToString(DATAREADER("TODT"))
                    chkActive.Checked = Convert.ToBoolean(DATAREADER("SAE_bENABLED"))
                    Dim TIME As String = Convert.ToString(DATAREADER("SAE_TIME"))
                    arInfo = TIME.Split(":")
                    If Not ddlHrs.Items.FindByValue(arInfo(0)) Is Nothing Then
                        ddlHrs.ClearSelection()
                        ddlHrs.Items.FindByValue(arInfo(0)).Selected = True
                    End If
                    If Not ddlMin.Items.FindByValue(arInfo(1)) Is Nothing Then
                        ddlMin.ClearSelection()
                        ddlMin.Items.FindByValue(arInfo(1)).Selected = True
                    End If
                    chkGrd.ClearSelection()
                    For Each item As ListItem In chkGrd.Items
                        If GRDS.Contains(item.Value) = True Then
                            item.Selected = True
                        End If
                    Next
                    chkparam.ClearSelection()
                    For Each item As ListItem In chkparam.Items
                        If ATT_PARAMETERS.Contains(item.Text) = True Then
                            item.Selected = True
                        End If
                    Next


                End While
            End If
        End Using
    End Sub

    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From date is invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From date is invalid</div>"
                End If
            End If
        End If

        If txtTo.Text.Trim <> "" Then

            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>To date format is invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to from date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid to date</div>"
                    End If
                End If
            End If
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_err As String = String.Empty
        Dim GRDS As String = String.Empty
        Dim ATT_PARAM As String = String.Empty
        Dim errorMessage As String = String.Empty

        For Each ITEM As ListItem In chkGrd.Items
            If ITEM.Selected = True Then
                GRDS += ITEM.Value + "|"
            End If
        Next
        For Each ITEM As ListItem In chkparam.Items
            If ITEM.Selected = True Then
                ATT_PARAM += ITEM.Text + "|"
            End If
        Next
        If serverDateValidate() = "0" Then


            If GRDS.Trim = "" Or ATT_PARAM.Trim = "" Then
                lblError.Text = "Grade & attendance parameters  needs to selected !!!"
            Else
                str_err = CallTransaction(errorMessage, GRDS, ATT_PARAM)

                If str_err = "0" Then

                    lblError.Text = "Record updated successfully"
                Else
                    lblError.Text = errorMessage
                End If
            End If




        End If

    End Sub



    Private Function CallTransaction(ByRef errorMessage As String, ByVal GRDS As String, ByVal ATT_PARAM As String) As String
        Dim status As String = String.Empty
        Dim str As String = String.Empty
        Dim BSU_ID As String = Session("sBsuid")
        Dim FROMDT As String = txtFrom.Text
        Dim TODT As String = txtTo.Text


        Dim TIME As String = ddlHrs.SelectedValue + ":" + ddlMin.SelectedValue
        Dim USR_ID As String = Session("sUsr_id")
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                status = ""

                Dim pParms(12) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@SAE_BSU_ID", BSU_ID)
                pParms(1) = New SqlClient.SqlParameter("@SAE_GRD_IDS", GRDS)
                pParms(2) = New SqlClient.SqlParameter("@SAE_FROMDT", FROMDT)
                pParms(3) = New SqlClient.SqlParameter("@SAE_TODT", TODT)
                pParms(4) = New SqlClient.SqlParameter("@SAE_bENABLED", chkActive.Checked)
                pParms(5) = New SqlClient.SqlParameter("@SAE_TIME", TIME)
                pParms(6) = New SqlClient.SqlParameter("@SAE_PARAMETERS", ATT_PARAM)
                pParms(7) = New SqlClient.SqlParameter("@USR_ID", USR_ID)
                pParms(8) = New SqlClient.SqlParameter("@SAE_EMAILCONTENT", txtEmailContent.Content)

                pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(9).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ATT].[SAVESCHEDULE_ATT_EMAIL]", pParms)
                Dim ReturnFlag As Integer = pParms(9).Value


                If ReturnFlag <> 0 Then
                    CallTransaction = "1"
                    errorMessage = "Error occured while processing info."
                    status = "1"
                End If

                CallTransaction = "0"

            Catch ex As Exception

                errorMessage = "Record could not be Updated"
            Finally
                If CallTransaction <> "0" Then
                    errorMessage = "Error occured while processing info."
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using





    End Function




    Protected Sub imgEmailContentClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEmailContentClose.Click

        plEmailContent.Visible = False
    End Sub

    Protected Sub lbtnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnEdit.Click
        plEmailContent.Visible = True

    End Sub

    Protected Sub btnEmailContentCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailContentCancel.Click
        bindEmail_Content()
        plEmailContent.Visible = False
    End Sub
End Class
