﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_EnrWeeklyanalysis_F
    Inherits BasePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            If Request.QueryString("BSU") IsNot Nothing AndAlso Request.QueryString("AsonDate") IsNot Nothing Then
                Dim BSU As String = Request.QueryString("BSU").ToString
                Dim ASondate As String = Request.QueryString("AsonDate").ToString
                ViewState("BSU") = BSU
                ViewState("ASondate") = ASondate
                BindBsuIdByCode(BSU)

                BindGrid(ViewState("bsuId"), ASondate)

            End If
        End If
    End Sub
    Protected Sub gvEnroll_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvEnroll.PageIndex = e.NewPageIndex
        BindGrid(ViewState("BSU"), ViewState("ASondate"))
    End Sub

    Private Sub BindGrid(ByVal Bsu As String, ByVal AsonDate As String)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(5) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable


            Dim strBsu As String = String.Empty
            strBsu = "<IDS>"
           
                strBsu += "<ID><BSU_ID>" + Bsu + "</BSU_ID></ID>"

            strBsu = strBsu + "</IDS>"
            param(0) = New SqlClient.SqlParameter("@BSU_XML", strBsu)
           
            param(1) = New SqlClient.SqlParameter("@RPT_TYPE", "")
          
            param(2) = New SqlClient.SqlParameter("@SCHOOL_TYPE", "")

            param(3) = New SqlClient.SqlParameter("@ASON_DATE", AsonDate)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[QRY].[WEEKLY_ANALYSIS]", param)
            If ds.Tables.Count <= 0 AndAlso ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEnroll.DataSource = ds
                gvEnroll.DataBind()
                Dim columnCount As Integer = gvEnroll.Rows(0).Cells.Count
                gvEnroll.Rows(0).Cells.Clear()
                gvEnroll.Rows(0).Cells.Add(New TableCell)
                gvEnroll.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEnroll.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEnroll.Rows(0).Cells(0).Text = "Currently there is no record Exists"
            Else


                DT = ds.Tables(0)

                gvEnroll.DataSource = DT
                gvEnroll.DataBind()


            End If
        Catch ex As Exception
            Dim msg As String = ex.Message
        End Try

    End Sub

    Sub BindBsuIdByCode(ByVal BSUCODE As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(1) As SqlClient.SqlParameter
        str_query = "SELECT * FROM dbo.Businessunit_m where BSU_SHORTNAME='" & BSUCODE & "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)
            ViewState("bsuId") = Dt.Rows(0)("BSU_ID").ToString

        End If
    End Sub
End Class
