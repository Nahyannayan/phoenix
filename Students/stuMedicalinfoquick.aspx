﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="stuMedicalinfoquick.aspx.vb" Inherits="Students_stuMedicalinfoquick" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="">
        <tr>
            <td colspan="3">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
          
          <tr>
                <td align="left" width="50%" colspan="2"><span class="field-label">Student Name</span><br />

               
                     <asp:Literal ID="ltStuname" runat="server"></asp:Literal></td>
                <td align="left" width="25%"><span class="field-label">Student No</span>
                    <br />
                    <asp:Literal ID="ltStuNo" runat="server"></asp:Literal></td>
                 <td align="left" width="25%"><span class="field-label">Grade & Section</span>
                    <br />
                       <asp:Literal ID="ltGradeSection" runat="server"></asp:Literal></td>
            </tr>

          <tr>
                <%--<td align="left" width="30%"><span class="field-label"></span></td>

                <td align="left" width="20%">
                    <asp:RadioButton ID="rbMedical_Yes" runat="server" GroupName="Medical"
                        Text="Yes" />
                    <asp:RadioButton ID="rbMedical_No" runat="server" GroupName="Medical" Text="No" /></td>--%>
                <td align="left" width="50%" colspan="2">Medical Condition
                    <br />
                    <asp:TextBox ID="txtMedical_Note" runat="server" TextMode="MultiLine"
                        Rows="4" Columns="6" MaxLength="255"></asp:TextBox></td>
                 <td align="left" width="50%" colspan="2">Action Taken
                    <br />
                    <asp:TextBox ID="txtMedical_Action" runat="server" TextMode="MultiLine"
                        Rows="4" Columns="6" MaxLength="255"></asp:TextBox></td>
            </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="btnSave" runat="server" Text="UpdateRecords" CssClass="button" />
            </td>
        </tr>
        </table>

        <table width="100%">
            <tr>
                <td>
                    <asp:GridView ID="gvStudMEdInfo" runat="server" Width="95%" CssClass="table table-bordered table-row"
                            AutoGenerateColumns="false" EmptyDataText="No Info available.">

                            <Columns>

                                <asp:BoundField DataField="STU_ID" HeaderText="Fee ID" HtmlEncode="False" Visible="false">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>

                                <asp:BoundField DataField="STU_BMEDICAL" HeaderText="MEDICAL ISSUE" HtmlEncode="False" Visible="false">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>

                               

                                <asp:BoundField DataField="STU_MED_NOTES" HeaderText="MEDICAL CONDITION" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:BoundField>

                                <asp:BoundField DataField="STU_MED_ACTIONS" HeaderText="ACTION TAKEN" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>
                                 <asp:BoundField DataField="STU_MED_DATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="DATE" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>
                               
                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <HeaderStyle />
                        </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
