﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studEnroll_Popup.aspx.vb" Inherits="Students_studEnroll_Popup" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
<link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css" />

<link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>:::GEMS OASIS:::Enrollement stage:::</title>
    <base target="_self" />
    <script language="javascript" type="text/javascript">
        function getStudents(mode, status) {
            var sFeatures;
            sFeatures = "dialogWidth: 845px; ";
            sFeatures += "dialogHeight: 510px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
           
            if (status == 'SIB') {
               
                if (document.getElementById('TabContainer1_TabPanel2_RBSiblingEnq').checked == true) {
                   
                    url = '../Students/ShowEnquiryStudents.aspx?id=' + status;
                   
                }
                else {
                  
                    url = '../Students/ShowSiblingInfo.aspx?id=' + mode + '&TYPE=' + status;
                  
                }

               // if (document.getElementById('TabContainer1$TabPanel2$txtSibno').value == '') {
                   
                    if (mode == 'SE') {
                      
                       
                        var oWnd = radopen(url, "pop_sib");
                        //result = window.showModalDialog(url, "", sFeatures);

                        //if (result == '' || result == undefined) {
                        //    return false;
                        //}
                        //NameandCode = result.split('___');

                        //document.getElementById('TabContainer1$TabPanel2$txtSibno').value = NameandCode[0];
                        //document.getElementById('TabContainer1$TabPanel2$HF_SIBL_ID').value = NameandCode[1];
                        //if (NameandCode.length > 2) {
                        //    document.getElementById('TabContainer1$TabPanel2$HF_SIBLING_TYPE').value = NameandCode[2];
                        //}
                    }
                //}
            }
            else {
                if (document.getElementById('TabContainer1_TabPanel3_RBTwinsEnq').checked == true) {
                    url = '../Students/ShowEnquiryStudents.aspx?id=' + status;
                }
                else {
                    url = '../Students/ShowSiblingInfo.aspx?id=' + mode + '&TYPE=' + status;
                }
               // if (document.getElementById('TabContainer1$TabPanel3$txtTwinsNo').value == '') {
                    if (mode == 'SE') {
                        var oWnd = radopen(url, "pop_twin");
                        //result = window.showModalDialog(url, "", sFeatures);

                        //if (result == '' || result == undefined) {
                        //    return false;
                        //}
                        //NameandCode = result.split('___');
                        //document.getElementById('TabContainer1$TabPanel3$txtTwinsNo').value = NameandCode[0];
                        //document.getElementById('TabContainer1$TabPanel3$HF_Twins_ID').value = NameandCode[1];

                    }
               // }
            }

        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function OnClientCloseSib(oWnd, args) {
            //get the transferred arguments
          
            var arg = args.get_argument();

            if (arg) {
               
                NameandCode = arg.Result.split('___');
                
                document.getElementById('TabContainer1_TabPanel2_txtSibno').value = NameandCode[0];
                document.getElementById('TabContainer1_TabPanel2_HF_SIBL_ID').value = NameandCode[1];
                if (NameandCode.length > 2) {
                    document.getElementById('TabContainer1_TabPanel2_HF_SIBLING_TYPE').value = NameandCode[2];

                     <%-- __doPostBack('<% =txtSibno.ClientID%>', 'TextChanged');--%>
                }
            }
        }
            function OnClientCloseTwin(oWnd, args) {
                //get the transferred arguments
                
                var arg = args.get_argument();

                if (arg) {
                    
                    NameandCode = arg.Result.split('___');
                   
                    document.getElementById('TabContainer1_TabPanel3_txtTwinsNo').value = NameandCode[0];
                    document.getElementById('TabContainer1_TabPanel3_HF_Twins_ID').value = NameandCode[1];

                    <%-- __doPostBack('<% =txtTwinsNo.ClientID%>', 'TextChanged');--%>
                }
            }
            function confirm_clear() {

                if (confirm("You are about to clear the siblings related to this enquiry.Do you want to proceed?") == true)
                    return true;
                else
                    return false;

            }
        
    </script>
</head>
    

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_sib" runat="server" Behaviors="Close,Move"   OnClientClose="OnClientCloseSib" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows> 
         <Windows> 
            <telerik:RadWindow ID="pop_twin" runat="server" Behaviors="Close,Move"  OnClientClose="OnClientCloseTwin"  
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>         
    </telerik:RadWindowManager>

<body>
    <form id="form1" runat="server">
        <div>

            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                Width="100%">
                <ajaxToolkit:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1">
                    <HeaderTemplate>
                        General
                    </HeaderTemplate>
                    <ContentTemplate>
                        <center>
                            <table class="bluetable" width="100%" cellpadding="0" cellspacing="0">
                                <tr align="left" style="display: none">
                                    <td>Transfer Type</td>

                                    <td>
                                        <asp:DropDownList ID="ddlTranType" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>SEN</td>

                                    <td>
                                        <asp:RadioButtonList ID="rbSEN" runat="server" CssClass="field-label"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Value="True" Selected="True">Yes</asp:ListItem>
                                            <asp:ListItem Value="False">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>Comments</td>

                                    <td>
                                        <asp:TextBox ID="txtComments" runat="server" EnableTheming="False" 
                                            TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSaveComments" runat="server" CssClass="button" Text="Save"
                                             />
                                        <asp:Button ID="btnClearcomments" runat="server" CssClass="button" Text="Clear"
                                             />
                                        <br />
                                        <asp:Label ID="lblError" runat="server" CssClass="error"
                                            EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                    <HeaderTemplate>
                        Set Siblings
                    </HeaderTemplate>
                    <ContentTemplate>
                        <center>
                            <table class="bluetable" width="100%" cellpadding="0" cellspacing="0">
                                <tr align="left">
                                    <td>Student Status</td>

                                    <td>
                                        <asp:RadioButton ID="RBSiblingEnq" runat="server" CssClass="field-label"
                                            GroupName="SIB" Text="Enquiry" AutoPostBack="true" />
                                        <asp:RadioButton ID="RBSiblingStu" runat="server" Checked="True" GroupName="SIB" CssClass="field-label"
                                            Text="Student" AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>Pick Sibling </td>

                                    <td>
                                        <asp:TextBox ID="txtSibno" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnSib" runat="server"
                                            ImageUrl="~/Images/forum_search.gif"
                                            OnClientClick="getStudents('SE','SIB');return  false;" />
                                        <asp:HiddenField ID="HF_SIBL_ID" runat="server" />
                                        <asp:HiddenField ID="HF_SIBLING_TYPE" runat="server" />
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSaveSib" runat="server" CssClass="button" Text="Save"
                                             />
                                        <asp:Button ID="btnClearSib" runat="server" CssClass="button" Text="Clear"
                                             OnClientClick="return confirm_clear();" />
                                        <br />
                                        <asp:Label ID="lblErrorsib" runat="server" CssClass="error"
                                            EnableViewState="False"></asp:Label>
                                        <br />
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="center" colspan="2">
                                        <asp:GridView ID="gvSibling" runat="server" AutoGenerateColumns="False"
                                            SkinID="GridViewView" Width="100%" CssClass="table table-bordered table-row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="NO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LBL_NO" runat="server" Text='<%# Bind("NUMBER") %>'></asp:Label>
                                                        <asp:HiddenField ID="HF_ID" runat="server" Value='<%# Bind("ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="STATUS" HeaderText="STATUS" />
                                                <asp:BoundField DataField="APPL_NAME" HeaderText="NAME" />
                                                <asp:BoundField DataField="GRM_DISPLAY" HeaderText="GRADE" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3">
                    <HeaderTemplate>
                        Set Twins
                    </HeaderTemplate>
                    <ContentTemplate>
                        <center>
                            <table class="bluetable" width="100%"  cellpadding="0" cellspacing="0">
                                <tr align="left">
                                    <td>Student Status</td>

                                    <td>
                                        <asp:RadioButton ID="RBTwinsEnq" runat="server" Checked="True"
                                            GroupName="twins" Text="Enquiry" CssClass="field-label" />
                                        <asp:RadioButton ID="RBTwinsStu" runat="server" GroupName="twins"
                                            Text="Student" CssClass="field-label" />
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>Pick Twin Student </td>

                                    <td>
                                        <asp:TextBox ID="txtTwinsNo" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server"
                                            ImageUrl="~/Images/forum_search.gif" OnClientClick="getStudents('SE','Twins');return  false;" />
                                        <asp:HiddenField ID="HF_Twins_ID" runat="server" />
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSaveTwin" runat="server" CssClass="button" Text="Save"
                                             />
                                        <asp:Button ID="btnClearTwin" runat="server" CssClass="button" Text="Clear"
                                             />
                                        <br />
                                        <asp:Label ID="lblErrorTwin" runat="server" CssClass="error"
                                            EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="center" colspan="2">
                                        <asp:GridView ID="gvTwins" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            SkinID="GridViewView" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="NO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LBL_NO0" runat="server" Text='<%# Bind("NUMBER") %>'></asp:Label>
                                                        <asp:HiddenField ID="HF_ID0" runat="server" Value='<%# Bind("ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="STATUS" HeaderText="STATUS" />
                                                <asp:BoundField DataField="APPL_NAME" HeaderText="NAME" />
                                                <asp:BoundField DataField="GRM_DISPLAY" HeaderText="GRADE" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>






        </div>
        <asp:HiddenField ID="HF_EQS_ID" runat="server" />
    </form>
</body>
</html>
