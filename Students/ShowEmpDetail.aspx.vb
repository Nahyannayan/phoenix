Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class ShowEmpDetail
    Inherits BasePage
    'Protected PageTitle As System.Web.UI.HtmlControls.HtmlGenericControl
    Dim SearchMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Try
                SearchMode = Request.QueryString("id")
                If SearchMode = "E" Or SearchMode = "EN" Then
                    Page.Title = "Employee Info"

                ElseIf SearchMode = "B" Then
                    Page.Title = "Business Unit Info"

                ElseIf SearchMode = "C" Then
                    Page.Title = "Category Info"
                ElseIf SearchMode = "D" Then
                    Page.Title = "Deduction Info"

                End If
                Page.DataBind()

                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                gridbind()
                desgnationbind()
            Catch ex As Exception

            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub desgnationbind()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select distinct des_id,des_descr from empdesignation_m join employee_m on des_id=emp_des_id WHERE DES_FLAG = 'sd' and emp_bsu_id='" & Session("sBsuid") & "'"


        ''SELECT ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(EMP_LNAME,'') AS STAFF , EMP_ID FROM EMPLOYEE_M ''WHERE EMP_BSU_ID='" & Hiddenbsuid.Value & "' " ' & _
        '" AND EMP_DES_ID IN (SELECT DES_ID FROM EMPDESIGNATION_M WHERE DES_FLAG='SD')"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim ddl As DropDownList = TryCast(gvEmpInfo.HeaderRow.FindControl("ddldesignation"), DropDownList)

        ddl.DataSource = ds
        ddl.DataTextField = "des_descr"
        ddl.DataValueField = "des_id"
        ddl.DataBind()
        Dim list As New ListItem
        list.Text = "ALL"
        list.Value = "0"
        ddl.Items.Insert(0, list)
        ddl.SelectedValue = ViewState("desg_id")

    End Sub
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            SearchMode = Request.QueryString("id")
            Dim str_filter_code As String
            Dim str_filter_name As String
            'Select * from(Select BSU_ID as ID,BSU_NAME as E_Name from businessunit_m)a where a.id!='100'
            'Select * from(Select emp_ID as ID,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name from Employee_M)a where a.id<>''
            'Get all the employees
            If SearchMode = "E" Then
                str_Sql = "Select ID,E_Name from(Select emp_ID as ID,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,EMP_bACTIVE as bActive from Employee_M)a where a.bActive=1 and  a.id<>'' "
                'Getting all the employee for that BSU
                ''INCLUDED EMPNO AS NEW FIELD
            ElseIf SearchMode = "EN" Then
                str_Sql = "Select E_Name,ID,DES_DESCR,emp_des_id,EMPNO from (SELECT ISNULL(EMP_FNAME,'')+'  '+isnull(emp_mname,'')+ ' ' + ISNULL(EMP_LNAME,'') AS E_Name , EMP_ID as ID,DES_DESCR,emp_des_id,EMPNO FROM EMPLOYEE_M join EMPDESIGNATION_M on emp_des_id=des_id WHERE EMP_BSU_ID='" & Session("sBsuid") & "' and EMP_bACTIVE=1 and EMP_STATUS<>4  and des_flag='sd')a WHERE a.ID <> ''"

                '' str_Sql = "Select ID,E_Name from(Select emp_ID as ID,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'')  as E_Name,emp_bsu_ID as BSU,EMP_bACTIVE as bActive from Employee_M)a where a.bActive=1 and a.BSU='" & Session("sBsuid") & "' and a.id<>'' "
                'Get all the BSU
            ElseIf SearchMode = "B" Then
                str_Sql = "Select ID, E_Name from(Select BSU_ID as ID,BSU_NAME as E_Name from businessunit_m)a where a.ID<>'' "
                'Get Category
            ElseIf SearchMode = "C" Then
                str_Sql = "Select ID, E_Name from(Select ECT_ID as ID,ECT_DESCR as E_Name from EMPCATEGORY_M)a where a.ID<>''"
                'Get Deduction Type
            ElseIf SearchMode = "DT" Then
                str_Sql = "Select ID, E_Name from(Select ERN_ID as ID,ERN_DESCR as E_Name,ERN_TYP from EMPSALCOMPO_M)a where  a.ERN_TYP=0 and a.ID<>''"

                'Get Earning Type
            ElseIf SearchMode = "ET" Then
                str_Sql = "Select ID, E_Name from(Select ERN_ID as ID,ERN_DESCR as E_Name,ERN_TYP from EMPSALCOMPO_M)a where  a.ERN_TYP=1 and a.ID<>''"
            End If

            Dim ds As New DataSet

            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox
            Dim str_txtCode, str_txtName As String
            Dim str_search As String
            str_txtCode = ""
            str_txtName = ""

            str_filter_code = ""
            str_filter_name = ""

            Dim str_filter_desgId As String = ""

            If gvEmpInfo.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                If ViewState("desg_id_flag") = 1 Then
                    Dim desg_id As String = TryCast(gvEmpInfo.HeaderRow.FindControl("ddldesignation"), DropDownList).SelectedValue
                    ViewState("desg_id") = desg_id
                    If desg_id <> "0" Then
                        str_filter_desgId = "and emp_des_id='" & desg_id & "'"
                    End If

                End If



                If SearchMode = "E" Or SearchMode = "EN" Then
                    'str_img = h_selected_menu_1.Value()
                    str_Sid_search = h_selected_menu_1.Value.Split("__")
                    str_search = str_Sid_search(0)
                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                    str_txtCode = txtSearch.Text
                    ''code
                    If str_search = "LI" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_code = " AND a.ID LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_code = " AND a.ID NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                    ''name
                    str_Sid_search = h_Selected_menu_2.Value.Split("__")
                    str_search = str_Sid_search(0)

                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                    str_txtName = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_name = " AND a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_name = "  AND  NOT a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_name = " AND a.E_Name  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_name = " AND a.E_Name  NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_name = " AND a.E_Name LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                    End If



                ElseIf SearchMode = "B" Then

                    'str_img = h_selected_menu_1.Value()
                    str_Sid_search = h_selected_menu_1.Value.Split("__")
                    str_search = str_Sid_search(0)
                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                    str_txtCode = txtSearch.Text
                    ''code

                    If str_search = "LI" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_code = " AND a.ID LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_code = " AND a.ID NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                    ''name
                    str_Sid_search = h_Selected_menu_2.Value.Split("__")
                    str_search = str_Sid_search(0)

                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                    str_txtName = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_name = " AND a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_name = "  AND  NOT a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_name = " AND a.E_Name  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_name = " AND a.E_Name  NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_name = " AND a.E_Name LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                ElseIf SearchMode = "C" Or SearchMode = "DT" Or SearchMode = "ET" Then

                    'str_img = h_selected_menu_1.Value()
                    str_Sid_search = h_selected_menu_1.Value.Split("__")
                    str_search = str_Sid_search(0)
                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                    str_txtCode = txtSearch.Text
                    ''code

                    If str_search = "LI" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_code = " AND a.ID LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_code = " AND a.ID NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                    ''name
                    str_Sid_search = h_Selected_menu_2.Value.Split("__")
                    str_search = str_Sid_search(0)

                    txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                    str_txtName = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_name = " AND a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_name = "  AND  NOT a.E_Name LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_name = " AND a.E_Name  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_name = " AND a.E_Name  NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_name = " AND a.E_Name LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                    End If
                End If


            End If





            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name & str_filter_desgId & "order by des_descr,e_name")



            gvEmpInfo.DataSource = ds.Tables(0)
            ' gvEmpInfo.TemplateControl.FindControl("label1"). = ds.Tables(0).Columns("emp_ID")



            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEmpInfo.DataBind()
                Dim columnCount As Integer = gvEmpInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.


                gvEmpInfo.Rows(0).Cells.Clear()
                gvEmpInfo.Rows(0).Cells.Add(New TableCell)
                gvEmpInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEmpInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEmpInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

                'gvEmpInfo.HeaderRow.Visible = True
            Else
                gvEmpInfo.DataBind()
                desgnationbind()


            End If



            If SearchMode = "E" Or SearchMode = "EN" Then
                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Employee ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Employee Name"
                Page.Title = "Employee Info"


            ElseIf SearchMode = "B" Then

                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Business ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Business Unit Name"
                Page.Title = "Business Unit Info"

            ElseIf SearchMode = "C" Then

                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Category ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Category Name"
                Page.Title = "Category Info"

            ElseIf SearchMode = "DT" Then

                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Deduction ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Description"
                Page.Title = "Deduction Info"

            ElseIf SearchMode = "ET" Then

                lblID = gvEmpInfo.HeaderRow.FindControl("lblID")
                lblID.Text = "Earning ID"
                lblName = gvEmpInfo.HeaderRow.FindControl("lblName")
                lblName.Text = "Description"
                Page.Title = "Earning Info"
            End If


            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
            txtSearch.Text = str_txtCode
            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            set_Menu_Img()

            'Page.Title = "Employee Info"

        Catch ex As Exception
            ' Errorlog(ex.Message)
        End Try





    End Sub
   
  
    Protected Sub btnSearchEmpId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    

    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    'Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim lblcode As New LinkButton

    '    Dim lbClose As New LinkButton


    '    lbClose = sender

    '    lblcode = sender.Parent.FindControl("LinkButton2")

    '    ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
    '    Dim l_Str_Msg As String = lbClose.Text & "___" & lblcode.Text
    '    l_Str_Msg = l_Str_Msg.Replace("'", "\'")

    '    If (Not lblcode Is Nothing) Then
    '        '   Response.Write(lblcode.Text)
    '        Response.Write("<script language='javascript'> function listen_window(){")
    '        Response.Write("window.returnValue = '" & l_Str_Msg & "';")


    '        Response.Write("window.close();")
    '        Response.Write("} </script>")
    '        h_SelectedId.Value = "Close"
    '    End If
    'End Sub

   
    Protected Sub gvEmpInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpInfo.PageIndexChanging
        gvEmpInfo.PageIndex = e.NewPageIndex        
        gridbind()
    End Sub

   
    'Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim lblcode As New LinkButton

    '    Dim lbClose As New LinkButton


    '    lbClose = sender.Parent.FindControl("LinkButton1")


    '    lblcode = sender
    '    ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
    '    Dim l_Str_Msg As String = lbClose.Text & "___" & lblcode.Text
    '    l_Str_Msg = l_Str_Msg.Replace("'", "\'")

    '    If (Not lblcode Is Nothing) Then
    '        '   Response.Write(lblcode.Text)
    '        Response.Write("<script language='javascript'> function listen_window(){")
    '        Response.Write("window.returnValue = '" & l_Str_Msg & "';")


    '        Response.Write("window.close();")
    '        Response.Write("} </script>")
    '        h_SelectedId.Value = "Close"
    '    End If
    'End Sub

    Protected Sub gvEmpInfo_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvEmpInfo.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim lblCode As LinkButton = DirectCast(e.Row.FindControl("LinkButton2"), LinkButton)
            Dim lbCodeSubmit As LinkButton = DirectCast(e.Row.FindControl("LinkButton1"), LinkButton)
            Dim l_Str_Msg As String = lbCodeSubmit.Text & "___" & lblCode.Text
            l_Str_Msg = l_Str_Msg.Replace("'", "\'")
            Dim Req_data As String = Request.QueryString("type")
            If (Not lblCode Is Nothing) And Req_data = "WitnessType" Then
                lbCodeSubmit.Attributes.Add("onClick", "return SetValuetoParent2('" & l_Str_Msg & "');")
            Else
                lbCodeSubmit.Attributes.Add("onClick", "return SetValuetoParent('" & l_Str_Msg & "');")
            End If

        End If
    End Sub

    Protected Sub ddldesignation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("desg_id_flag") = 1
        gridbind()
    End Sub
End Class
