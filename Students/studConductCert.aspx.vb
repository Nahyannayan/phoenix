Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Students_studConductCert
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_sql As String = ""
                ViewState("CurBsUnit") = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'ViewState("CurACDID") = Session("Current_ACD_ID")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then
                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                End If
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100088") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, ViewState("CurBsUnit"), ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    BindAcademicYear()
                    BindShift()
                    BindStream()
                    BindGrade()
                    Call GridBind()
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        Else
            studClass.SetChk(gvStudCondCert, Session("liUserList"))
        End If
    End Sub
    Public Sub BindAcademicYear()
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
        bindShift()
    End Sub
    Public Sub BindGrade()
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
        ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
        ddlSection.Items.Clear()
        ddlSection.Items.Insert(0, "All")
        ddlSection.DataBind()
    End Sub
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudCondCert.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudCondCert.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudCondCert.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudCondCert.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudCondCert.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudCondCert.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvStudCondCert.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudCondCert.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim GradeSearch As String = ""
        Dim SectionSearch As String = ""
        Dim txtSearch As New TextBox
        Dim str_query As String

        gvStudCondCert.Enabled = True
        btnPrint.Enabled = True
        If gvStudCondCert.Rows.Count > 0 Then

            txtSearch = gvStudCondCert.HeaderRow.FindControl("txtStuNoSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudCondCert.HeaderRow.FindControl("txtStuNameSearch")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_PASPRTNAME,'')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            txtSearch = gvStudCondCert.HeaderRow.FindControl("txtGradeSearch")
            strSidsearch = h_Selected_menu_3.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            GradeSearch = txtSearch.Text

            txtSearch = gvStudCondCert.HeaderRow.FindControl("txtSectionSearch")
            strSidsearch = h_Selected_menu_4.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SCT_DESCR", txtSearch.Text, strSearch)
            SectionSearch = txtSearch.Text

            If ddlShift.SelectedIndex <> 0 Then

                str_query = " SELECT STU_NO, STU_ID,CONVERT(VARCHAR(100),STU_DOJ,104)AS STU_DOJ,ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR," & _
                            " CONVERT(VARCHAR(100),STU_DOB,104)AS STU_DOB,STU_NAME=(ISNULL(STU_PASPRTNAME,''))," & _
                            " SHF_DESCR,STM_DESCR, CONVERT(VARCHAR(100),ACD_ENDDT,104)AS ACD_ENDDT " & _
                            " FROM STUDENT_M SM INNER JOIN GRADE_BSU_M GBM ON SM.STU_GRM_ID=GBM.GRM_ID INNER JOIN SECTION_M SC" & _
                            " ON SM.STU_SCT_ID=SC.SCT_ID INNER JOIN SHIFTS_M SH ON SH.SHF_ID=GBM.GRM_SHF_ID INNER JOIN STREAM_M ST " & _
                            " ON ST.STM_ID=GBM.GRM_STM_ID INNER JOIN GRADE_M GM ON GM.GRD_ID=GBM.GRM_GRD_ID INNER JOIN ACADEMICYEAR_D ACA ON ACA.ACD_ID=SM.STU_ACD_ID" & _
                            " WHERE STU_BSU_ID= '" & ViewState("CurBsUnit") & "'  AND STU_ACD_ID='" & ddlAcademicYear.SelectedValue & "' "


                strFilter = " AND SHF_ID='" & ddlShift.SelectedValue & "'  "

            Else
                str_query = " SELECT STU_NO, STU_ID,CONVERT(VARCHAR(100),STU_DOJ,104)AS STU_DOJ,ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR," & _
                            " CONVERT(VARCHAR(100),STU_DOB,104)AS STU_DOB,STU_NAME=(ISNULL(STU_PASPRTNAME,''))," & _
                            " SHF_DESCR,STM_DESCR, CONVERT(VARCHAR(100),ACD_ENDDT,104)AS ACD_ENDDT " & _
                            " FROM STUDENT_M SM INNER JOIN GRADE_BSU_M GBM ON SM.STU_GRM_ID=GBM.GRM_ID INNER JOIN SECTION_M SC" & _
                            " ON SM.STU_SCT_ID=SC.SCT_ID INNER JOIN SHIFTS_M SH ON SH.SHF_ID=GBM.GRM_SHF_ID INNER JOIN STREAM_M ST " & _
                            " ON ST.STM_ID=GBM.GRM_STM_ID INNER JOIN GRADE_M GM ON GM.GRD_ID=GBM.GRM_GRD_ID INNER JOIN ACADEMICYEAR_D ACA ON ACA.ACD_ID=SM.STU_ACD_ID" & _
                            " WHERE STU_BSU_ID= '" & ViewState("CurBsUnit") & "'  AND STU_ACD_ID='" & ddlAcademicYear.SelectedValue & "' "
            End If
            If ddlStream.SelectedIndex <> 0 Then
                strFilter = strFilter + " AND STM_ID='" & ddlStream.SelectedValue & "'"
            End If
            If ddlGrade.SelectedIndex <> 0 Then
                strFilter = strFilter + " AND STU_GRD_ID='" & ddlGrade.SelectedValue & "'"
            End If
            If ddlSection.SelectedIndex <> 0 Then
                strFilter = strFilter + " AND STU_SCT_ID='" & ddlSection.SelectedValue & "'"
            End If
        Else
            str_query = " SELECT STU_NO, STU_ID,CONVERT(VARCHAR(100),STU_DOJ,104)AS STU_DOJ,ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR," & _
                        " CONVERT(VARCHAR(100),STU_DOB,104)AS STU_DOB,STU_NAME=(ISNULL(STU_PASPRTNAME,''))," & _
                        " SHF_DESCR,STM_DESCR, CONVERT(VARCHAR(100),ACD_ENDDT,104)AS ACD_ENDDT " & _
                        " FROM STUDENT_M SM INNER JOIN GRADE_BSU_M GBM ON SM.STU_GRM_ID=GBM.GRM_ID INNER JOIN SECTION_M SC" & _
                        " ON SM.STU_SCT_ID=SC.SCT_ID INNER JOIN SHIFTS_M SH ON SH.SHF_ID=GBM.GRM_SHF_ID INNER JOIN STREAM_M ST " & _
                        " ON ST.STM_ID=GBM.GRM_STM_ID INNER JOIN GRADE_M GM ON GM.GRD_ID=GBM.GRM_GRD_ID INNER JOIN ACADEMICYEAR_D ACA ON ACA.ACD_ID=SM.STU_ACD_ID" & _
                        " WHERE STU_BSU_ID= '" & ViewState("CurBsUnit") & "'  AND STU_ACD_ID='" & ddlAcademicYear.SelectedValue & "' "
        End If
        If strFilter <> "" Then
            str_query += strFilter + "ORDER BY GRD_DISPLAYORDER,SCT_DESCR,STU_NAME"
        Else
            str_query = str_query + "ORDER BY GRD_DISPLAYORDER,SCT_DESCR,STU_NAME"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStudCondCert.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStudCondCert.DataBind()
            Dim columnCount As Integer = gvStudCondCert.Rows(0).Cells.Count
            gvStudCondCert.Rows(0).Cells.Clear()
            gvStudCondCert.Rows(0).Cells.Add(New TableCell)
            gvStudCondCert.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudCondCert.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudCondCert.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            gvStudCondCert.Enabled = False
            btnPrint.Enabled = False
        Else
            gvStudCondCert.DataBind()
        End If
        txtSearch = New TextBox
        txtSearch = gvStudCondCert.HeaderRow.FindControl("txtStuNoSearch")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStudCondCert.HeaderRow.FindControl("txtStuNameSearch")
        txtSearch.Text = stuNameSearch

        txtSearch = New TextBox
        txtSearch = gvStudCondCert.HeaderRow.FindControl("txtGradeSearch")
        txtSearch.Text = GradeSearch

        txtSearch = New TextBox
        txtSearch = gvStudCondCert.HeaderRow.FindControl("txtSectionSearch")
        txtSearch.Text = SectionSearch

    End Sub
   
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Private Sub PopulateSection()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            'Dim ACD_ID As String = Session("Current_ACD_ID")
            Dim ACD_ID As String = ddlAcademicYear.SelectedValue
            Dim GRD_ID As String
            Dim str_Sql As String
            If ddlGrade.SelectedIndex <> 0 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = ""
            End If

            If ViewState("GRD_ACCESS") > 0 Then

                str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
                          " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID " & _
                          " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND " & _
                          " (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "') AND SECTION_M.SCT_ID IN(SELECT  ID  FROM  dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  GRADE_SECTION_ACCESS  " & _
                          " WHERE (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|'))  order by SECTION_M.SCT_DESCR "
            Else

                str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
                          " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID " & _
                          " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND " & _
                          " (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "')  order by SECTION_M.SCT_DESCR "

            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSection.Items.Clear()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
                ddlSection.Items.Insert(0, "All")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"
        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If
    End Function
    Protected Sub gvStudCondCert_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudCondCert.PageIndexChanging
        Try
            gvStudCondCert.PageIndex = e.NewPageIndex
            Call GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
        Dim txtSearchStuNo As New TextBox
        txtSearchStuNo = gvStudCondCert.HeaderRow.FindControl("txtStuNoSearch")
        txtSearchStuNo.Text = ""
        lblError.Text = ""
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
        Dim txtSearchStuName As New TextBox
        txtSearchStuName = gvStudCondCert.HeaderRow.FindControl("txtStuNameSearch")
        txtSearchStuName.Text = ""
        lblError.Text = ""
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
        Dim txtSearchGrade As New TextBox
        txtSearchGrade = gvStudCondCert.HeaderRow.FindControl("txtGradeSearch")
        txtSearchGrade.Text = ""
        lblError.Text = ""
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
        Dim txtSearchSection As New TextBox
        txtSearchSection = gvStudCondCert.HeaderRow.FindControl("txtSectionSearch")
        txtSearchSection.Text = ""
        lblError.Text = ""
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If ddlGrade.SelectedIndex <> 0 Then
            Call PopulateSection()
            Call Subjects()
            Call Stream_Subjects()
            Call GridBind()
        Else
            ddlSection.Items.Clear()
            ddlSection.Items.Insert(0, "All")
            ComSub.Visible = False
            OptSub.Visible = False
        End If
    End Sub
    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        If ddlGrade.SelectedIndex <> 0 Then
            lblError.Text = ""
            If gvStudCondCert.Rows.Count > 0 Then
                Call GridBind()
            End If
        End If
    End Sub
    Function getStu_XML()
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim str As String = ""
        For i = 0 To gvStudCondCert.Rows.Count - 1
            chkSelect = gvStudCondCert.Rows(i).FindControl("chkSelect")
            If chkSelect.Checked = True Then
                lblStuId = gvStudCondCert.Rows(i).FindControl("lblStuId")
                str += "<ID><STU_ID>" + lblStuId.Text + "</STU_ID></ID>"
            End If
        Next
        If str <> "" Then
            str = "<IDS>" + str + "</IDS>"
        End If
        Return str
    End Function
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Call PrintConductCert()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub PrintConductCert()

        Dim OptSubFilter As String = ""
        Dim ComSubFilter As String = ""
        Dim stu_XML As String = getStu_XML()
        If stu_XML = "" Then
            lblError.Text = "No records selected"
            Exit Sub
        End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@pBSU_ID", Session("sbsuid"))
        param.Add("@pExamDate", txtExamDate.Text)
        param.Add("@pResultDate", txtResultDate.Text)
        param.Add("@pCharacter", txtCharacter.Text)
       
        If chkbxStream.Items.Count > 0 Then

            Dim temp As String = ""
            Dim i = 0
            Dim flag As Integer

            chkbxStream = DirectCast(chkbxStream, CheckBoxList)
            For i = 0 To chkbxStream.Items.Count - 1
                Dim filter As String = ""
                If chkbxStream.Items(i).Selected Then
                    If Trim(chkbxStream.Items(i).Value) <> "0" Then
                        filter += chkbxStream.Items(i).Text & ","
                    End If
                End If
                temp += filter
            Next
            If temp <> "" Then
                'If chkbxSubjects.Items.Count > 0 Then
                '    For j As Integer = 0 To chkbxSubjects.Items.Count - 1
                '        If chkbxSubjects.Items(j).Selected = True Then
                '            flag = 1
                '        End If
                '    Next
                'End If
                If flag > 0 Then
                    OptSubFilter += temp
                Else
                    OptSubFilter += temp
                    OptSubFilter = OptSubFilter.Remove(OptSubFilter.LastIndexOf(","))
                    OptSubFilter = OptSubFilter + "."
                End If
            End If
        End If

        If chkbxSubjects.Items.Count > 0 Then

            Dim temp As String = ""
            Dim i = 0
            chkbxSubjects = DirectCast(chkbxSubjects, CheckBoxList)
            For i = 0 To chkbxSubjects.Items.Count - 1
                Dim filter As String = ""
                If chkbxSubjects.Items(i).Selected Then
                    If Trim(chkbxSubjects.Items(i).Value) <> "0" Then
                        filter += chkbxSubjects.Items(i).Text & ","
                    End If
                End If
                temp += filter
            Next
            If temp <> "" Then
                ComSubFilter += temp
                'ComSubFilter = ComSubFilter.Remove(ComSubFilter.LastIndexOf(","))
                'ComSubFilter = ComSubFilter + "."
            End If
        End If
        param.Add("@pSubFilter", OptSubFilter)
        param.Add("@pSubjects", ComSubFilter)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            param.Add("@pSTU_XML", stu_XML)
            .reportPath = Server.MapPath("../Students/Reports/RPT/rptStuConductCer.rpt") '"../Students/Reports/RPT/rptStuConductCer.rpt"
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlStream.SelectedIndex <> 0 Then
            If ddlGrade.SelectedIndex <> 0 Then
                Call Stream_Subjects()
                Call Subjects()
                Call GridBind()
            Else
                ComSub.Visible = False
                OptSub.Visible = False
                Call GridBind()
            End If
        Else
            ddlGrade.SelectedIndex = 0
            ComSub.Visible = False
            OptSub.Visible = False
            Call GridBind()
        End If
    End Sub
    Sub Stream_Subjects()
        Try
            chkbxStream.Items.Clear()

            If ddlStream.SelectedIndex <> 0 Then
                Dim Stream_id As String = String.Empty
                Stream_id = ddlStream.SelectedValue
                Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim sql_query As String = String.Empty
                Dim ds As DataSet
                Dim Acd_id As String = String.Empty
                Dim Grade_id As String = String.Empty

                If ddlAcademicYear.SelectedIndex <> -1 Then
                    Acd_id = ddlAcademicYear.SelectedValue
                Else
                    Acd_id = 0
                End If
                If ddlGrade.SelectedIndex <> 0 Then
                    Grade_id = ddlGrade.SelectedValue
                Else
                    Grade_id = ""
                End If

                If Grade_id <> "" Then
                    sql_query = "SELECT SBG_STM_ID,SBG_DESCR FROM vw_CURR_ELECTIVES " & _
                                " WHERE SBG_ACD_ID='" & Acd_id & "'AND SBG_GRD_ID='" & Grade_id & "' AND" & _
                                " SBG_STM_ID='" & Stream_id & "' AND SBG_BSU_ID='" & ViewState("CurBsUnit") & " ' ORDER BY SBG_ORDER"
                Else
                    sql_query = "SELECT SBG_STM_ID,SBG_DESCR FROM vw_CURR_ELECTIVES " & _
                                " WHERE SBG_ACD_ID='" & Acd_id & "'AND SBG_STM_ID='" & Stream_id & "' AND SBG_BSU_ID='" & ViewState("CurBsUnit") & " ' ORDER BY SBG_ORDER"
                End If

                ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
                chkbxStream.DataSource = ds
                chkbxStream.DataTextField = "SBG_DESCR"
                chkbxStream.DataValueField = "SBG_STM_ID"
                chkbxStream.DataBind()
                If ds.Tables(0).Rows.Count > 0 Then
                    OptSub.Visible = True
                Else
                    OptSub.Visible = False
                End If
            Else
                OptSub.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            BindShift()
            BindStream()
            BindGrade()
            Call GridBind()
            'ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Public Sub BindShift()
        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = String.Empty
        Dim ds As DataSet
        Dim Acd_id As String = String.Empty
        If ddlAcademicYear.SelectedIndex <> -1 Then
            Acd_id = ddlAcademicYear.SelectedValue
        Else
            Acd_id = 0
        End If
        sql_query = "select SHF_ID,SHF_DESCR from SHIFTS_M WHERE SHF_ID IN(select distinct " _
        & " grm_shf_id from grade_bsu_m where grm_acd_id='" & Acd_id & "')"
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        ddlShift.DataSource = ds
        ddlShift.DataTextField = "SHF_DESCR"
        ddlShift.DataValueField = "SHF_ID"
        ddlShift.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlShift.Items.Insert(0, li)
    End Sub
    Public Sub BindStream()
        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = String.Empty
        Dim ds As DataSet
        Dim Acd_id As String = String.Empty
        If ddlAcademicYear.SelectedIndex <> -1 Then
            Acd_id = ddlAcademicYear.SelectedValue
        Else
            Acd_id = 0
        End If
        sql_query = "SELECT STM_ID,STM_DESCR  FROM STREAM_M WHERE STM_ID IN( " _
        & " select distinct grm_STM_id from grade_bsu_m where grm_acd_id='" & Acd_id & "')"
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()

        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlStream.Items.Insert(0, li)
    End Sub
    Public Sub BindSection()
        Dim bsu_id As String = Session("sBsuid").ToString '' "125016"
        Dim acd_id As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        Dim SHF_ID As String = String.Empty

        If ddlAcademicYear.SelectedIndex <> -1 Then
            acd_id = ddlAcademicYear.SelectedValue
        Else
            acd_id = "0"
        End If

        If ddlGrade.SelectedIndex <> 0 Then
            GRD_ID = ddlGrade.SelectedValue
        Else
            GRD_ID = ""
        End If
        If ddlShift.SelectedIndex <> 0 Then
            SHF_ID = ddlShift.SelectedValue
        Else
            SHF_ID = "0"
        End If
        If ddlStream.SelectedIndex <> 0 Then
            STM_ID = ddlStream.SelectedValue
        Else
            STM_ID = "0"
        End If

        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query = "SELECT [SCT_ID],[SCT_DESCR] FROM [OASIS].[dbo].[SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [OASIS].[dbo].[GRADE_BSU_M] where [GRM_BSU_ID]='" & bsu_id & "' and [GRM_ACD_ID]='" & acd_id & "' and GRM_GRD_ID='" & GRD_ID & "' and GRM_SHF_ID='" & SHF_ID & "' and GRM_STM_ID='" & STM_ID & "') order by SCT_DESCR"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()

        If ds.Tables(0).Rows.Count = 0 Then
            lblError.Text = "No Section Under Selected Grade"
        Else
            lblError.Text = ""
        End If
    End Sub
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Call GridBind()
    End Sub
    Sub Subjects()

        Dim acd_id As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim STM_ID As String = String.Empty

        If ddlAcademicYear.SelectedIndex <> -1 Then
            acd_id = ddlAcademicYear.SelectedValue
        Else
            acd_id = "0"
        End If

        If ddlGrade.SelectedIndex <> 0 Then
            GRD_ID = ddlGrade.SelectedValue
        Else
            GRD_ID = ""
        End If
        If ddlStream.SelectedIndex <> 0 Then
            STM_ID = ddlStream.SelectedValue
        Else
            STM_ID = "0"
        End If
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim ds As DataSet
            Try
                Dim pParams(4) As SqlClient.SqlParameter
                pParams(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
                pParams(1) = New SqlClient.SqlParameter("@ACD_ID", acd_id)
                pParams(2) = New SqlClient.SqlParameter("@STM_ID", STM_ID)
                pParams(3) = New SqlClient.SqlParameter("@GRD_ID", GRD_ID)

                ds = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "GET_STM_COMMONSUBJ", pParams)
                chkbxSubjects.DataSource = ds
                chkbxSubjects.DataTextField = "SBG_DESCR"
                chkbxSubjects.DataValueField = "SBG_DESCR"
                chkbxSubjects.DataBind()
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    chkbxSubjects.Items(i).Selected = True
                Next

                If ds.Tables(0).Rows.Count > 0 Then
                    ComSub.Visible = True
                Else
                    ComSub.Visible = False
                End If
           
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End Using
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class