﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="stuNotification_add_students.aspx.vb" Inherits="stuNotification_add_students" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<base target="_self" />
    <title></title>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <style>
  .GridPager a, .GridPager span
    {
        display: block;
        height: 15px;
        width: 15px;
        font-weight: bold;
        text-align: center;
        text-decoration: none;
    }
    .GridPager a
    {
        background-color: #f5f5f5; 
        color: #969696;
        border: 1px solid #969696;
    }
    .GridPager span
    {
        background-color: #A1DCF2;
        color: #000;
        border: 1px solid #3AC0F2;
    }

  </style>
     <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
 <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>


   
  <script type="text/javascript">
      $(document).ready(function () {
          $('[id$=chkHeader]').click(function () {
              $("[id$='chkChild']").attr('checked', this.checked);
          });
      });
      function fancyClose() {
          window.close();
        
      } 


      function setStudentsToParent() {

          
          if (parent.$.fancybox) {

              setTimeout(function () { parent.CallFromStudentSelection($("#hdnSelectedTrainersSelection").val()); parent.$.fancybox.close(); }, 500);


              return false;
          }
          return true;

      }
          </script>
</head>
<body>
    <form id="form1" runat="server">
         <asp:HiddenField ID="hdnSelectedTrainersSelection" runat="server"/>
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Tellal PD Add Trainers
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
   
        <table  id="tblCategory" cellspacing="2" cellpadding="2" width="100%" >
        <tr id="trLabelError">
            <td align="left" class="matters" valign="bottom">
                <div id="lblError" runat="server">
                </div>
            </td>
        </tr>
        <tr id="trAdd">
            <td>
                <div style="text-align: right; margin: 5px; font-weight: bold;">
                 
                   </div>
            </td>
        </tr>
        <tr id="trGridv" runat="server">
            <td align="center">
                <table width="100%">
                    <tr class="subheader_img">
                        <td>
                           
                        </td>
                    </tr>
                     <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnAddPDC"    Text="ADD" runat="server" CssClass="button" ValidationGroup="rfAdd" />
                 <input type="button" class="button" id="btnCancel1" title="CLOSE" value="CLOSE" onclick="fancyClose()" />
               
            </td>
        </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvPDC" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" HeaderStyle-Height="30"
                                PageSize="15" Width="100%" OnPageIndexChanging="gvPDC_PageIndexChanging" DataKeyNames ="STU_ID">
                                <Columns>
                                  <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkHeader" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkChild" runat="server" />
                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("STU_ID") %>' Visible="false"></asp:Label> 
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField> 
                                    <asp:TemplateField HeaderText="Name">
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" class="gridheader_text" align="center">
                                                           Full Name
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gridheader_text" align="left">
                                                            <asp:TextBox ID="txtFullName" runat="server" Width="160px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 18px" valign="middle">
                                                            <asp:ImageButton ID="btnSearchFullName" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                                ImageAlign="Top" OnClick="btnSearchFullName_Click"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFullName" runat="server" Text='<%# bind("FullName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="STU.No." ItemStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStu_no" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField> 
                                </Columns>
                                
                                <RowStyle CssClass="griditem" Height="25px" />
                                <SelectedRowStyle BackColor="Aqua" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <asp:HiddenField ID="hdnSelected" runat="server" />
                        </td>
                    </tr>

                        
                </table>
            </td>
        </tr>
        </table>
    </div>
            </div></div>
    </form>
</body>
</html>
