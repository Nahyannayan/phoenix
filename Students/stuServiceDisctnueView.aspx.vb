Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class stuServiceDisctnueView
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                hlAddnew.PostBackUrl = "stuServiceDisctnue.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("add")
                lblError.Text = " "
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                gvJournal.Attributes.Add("bordercolor", "#1b80b6")
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "S100017" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    ddlACDYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
                    ddlACDYear.DataTextField = "ACY_DESCR"
                    ddlACDYear.DataValueField = "ACD_ID"
                    ddlACDYear.DataBind()
                    ddlACDYear.SelectedValue = Session("Current_ACD_ID")

                    ddlService.DataSource = GetSERVICES_Details(1)
                    ddlService.DataTextField = "SVC_DESCRIPTION"
                    ddlService.DataValueField = "SVC_ID"
                    ddlService.DataBind()

                    Dim lstItem As New ListItem("ALL", "0")
                    ddlService.Items.Insert(0, lstItem)

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gridbind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Sub gridbind()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""


            If ddlACDYear.SelectedValue > 0 Then
                str_Filter = " AND SSD_ACD_ID=" & ddlACDYear.SelectedValue
            End If

            If ddlService.SelectedValue > 0 Then
                str_Filter += " AND SVC_ID=" & ddlService.SelectedValue
            End If

            If txtFroDate.Text.Trim() <> "" Then
                str_Filter += " AND SSD_REQUESTED_DT>='" & txtFroDate.Text & "'"
            End If

            If txtToDate.Text.Trim() <> "" Then
                str_Filter += " AND SSD_REQUESTED_DT<='" & txtToDate.Text & "'"
            End If


            str_Sql = "SELECT SSD_ID,SSD_STU_ID,STU_NO,STU_NAME,vv_ACADEMICYEAR_M.ACY_DESCR,SSD_SVC_ID,SVC_DESCRIPTION," & _
                     "CONVERT(VARCHAR,SSD_REQUESTED_DT,103)AS SSD_REQUESTED_DT, CONVERT(VARCHAR,SSD_SERVICE_DIS_DT,103)AS SSD_SERVICE_DIS_DT, " & _
                     "(CASE WHEN SSD_bAPPROVE_STATUS='A' THEN 'APPROVED' " & _
                        "WHEN SSD_bAPPROVE_STATUS='N' THEN 'REQUESTED'" & _
                        "WHEN SSD_bAPPROVE_STATUS='R' THEN 'REJECTED' " & _
                        "ELSE 'NO STATUS' END ) AS STATUSDIS " & _
                     "FROM STUDENT_SERVICE_DISCONTINUE_REQ  " & _
                     "INNER JOIN SERVICES_SYS_M ON SSD_SVC_ID=SVC_ID " & _
                     "INNER JOIN VW_OSO_STUDENT_M ON STU_ID=SSD_STU_ID " & _
                     "INNER JOIN VV_ACADEMICYEAR_D ON ACD_ID=SSD_ACD_ID " & _
                     "INNER JOIN vv_ACADEMICYEAR_M ON ACY_ID=ACD_ACY_ID " & _
                     " WHERE SSD_bAPPROVE_STATUS IN ('A','N','R') AND SVC_ID<>1 " & str_Filter

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            gvJournal.DataSource = ds.Tables(0)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count

                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetSERVICES_Details(Optional ByVal SVCNOt As Integer = 1) As DataTable
        Dim sql_query As String = "SELECT SVB_ID,SVC_ID, SVC_DESCRIPTION FROM SERVICES_BSU_M " & _
                                  " INNER JOIN SERVICES_SYS_M ON SVB_SVC_ID=SVC_ID WHERE " & _
                                  " SVB_ACD_ID=" & ddlACDYear.SelectedValue & " AND SVB_BSU_ID='" & Session("sBsuId") & "'" & _
                                  " AND SVB_bAvailable='True'"

        If SVCNOt >= 1 Then
            sql_query += " AND SVC_ID<>" & SVCNOt.ToString
        End If
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(6).Text <> "REQUESTED" Then
                    TryCast(e.Row.FindControl("lnkView"), LinkButton).Enabled = False
                    TryCast(e.Row.FindControl("LinkButton1"), LinkButton).Enabled = False
                End If

            End If
        Catch ex As Exception
            'Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvJournal_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Try
            Dim strSql As String = ""
            Dim IntResult As Int16 = 0
            lblError.Text = ""

            If e.CommandName = "Delete" Then
                strSql = "DELETE FROM STUDENT_SERVICE_DISCONTINUE_REQ WHERE SSD_ID=" & e.CommandArgument & " AND SSD_bApprove_Status='N'"
                IntResult = SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, CommandType.Text, strSql)

                If IntResult = 0 Then
                    lblError.Text = " This Discountinue Request is Approved. Can't be delete."
                End If
                gridbind()
            ElseIf e.CommandName = "View" Then
                'To Display the Recoed Retails
                Response.Redirect("stuServiceDisctnue.aspx" & "?MainMnu_code=" & Request.QueryString("MainMnu_code") & "&datamode=" & Encr_decrData.Encrypt("edit") & "&SSDID=" & e.CommandArgument & "")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvJournal_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Filter
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlACDYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ddlService.Items.Clear()
            ddlService.DataSource = GetSERVICES_Details(1)
            ddlService.DataTextField = "SVC_DESCRIPTION"
            ddlService.DataValueField = "SVC_ID"
            ddlService.DataBind()

            Dim lstItem As New ListItem("ALL", "0")
            ddlService.Items.Insert(0, lstItem)
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlService_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            gridbind()
        Catch ex As Exception

        End Try
    End Sub
End Class
