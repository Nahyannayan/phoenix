Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studGradeJoinDocs
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050035") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control buttons based on the rights

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                    hfSTM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))

                    GridBind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnRight_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("action") = "right"

    End Sub

    Protected Sub btnLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("action") = "left"

    End Sub

    Protected Sub chkProceed_CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkProceed As CheckBox = DirectCast(sender, CheckBox)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String

            str_query = "UPDATE GRADE_JOINDOCUMENTS_M SET DCG_bPROCEED='" + chkProceed.Checked.ToString + "'" _
                        & " WHERE DCG_GRD_ID='" + hfGRD_ID.Value + "' AND DCG_STM_ID=" + hfSTM_ID.Value _
                        & " AND DCG_ACD_ID=" + hfACD_ID.Value + " AND DCG_STG_ID=" + chkProceed.ToolTip

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private methods"



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT PRO_DESCRIPTION,PRG_STG_ID FROM PROCESSFO_SYS_M AS A " _
                               & " INNER JOIN PROCESSFO_GRADE_M AS B ON A.PRO_ID=B.PRG_STG_ID" _
                               & " WHERE PRG_ACD_ID=" + hfACD_ID.Value + "  AND" _
                               & " PRG_GRD_ID='" + hfGRD_ID.Value + "' AND PRG_STM_ID=" + hfSTM_ID.Value _
                               & " AND PRG_bREQUIRED='TRUE' AND PRG_STG_ID<=7 ORDER BY PRG_STG_ORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvStage.DataSource = ds
        gvStage.DataBind()

    End Sub

    Function BindRequiredList(ByVal lstRequired As ListBox, ByVal stgId As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DOC_DESCR,DOC_ID,DCG_bPROCEED FROM DOCREQD_M AS A INNER JOIN" _
                                & " GRADE_JOINDOCUMENTS_M AS B ON A.DOC_ID=B.DCG_DOC_ID" _
                                & " WHERE  DCG_ACD_ID=" + hfACD_ID.Value + "  AND" _
                                & " DCG_GRD_ID='" + hfGRD_ID.Value + "' AND DCG_STM_ID=" + hfSTM_ID.Value _
                                & " AND DCG_STG_ID=" + stgId
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstRequired.DataSource = ds
        lstRequired.DataTextField = "DOC_DESCR"
        lstRequired.DataValueField = "DOC_ID"
        lstRequired.DataBind()
        If ds.Tables(0).Rows.Count <> 0 Then
            Return ds.Tables(0).Rows(0).Item(2)
        Else
            Return False
        End If
    End Function

    Sub BindNotRequiredList(ByVal lstNotRequired As ListBox, ByVal stgId As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DOC_DESCR,DOC_ID FROM DOCREQD_M AS A INNER JOIN" _
                                & " BSU_JOINDOCUMENTS_M AS B ON A.DOC_ID=B.DCB_DOC_ID " _
                                & " WHERE DCB_ACD_ID=" + hfACD_ID.Value + " AND DCB_STG_ID=" + stgId _
                                & " AND DCB_DOC_ID NOT IN (SELECT DCG_DOC_ID FROM" _
                                & " GRADE_JOINDOCUMENTS_M  WHERE  DCG_ACD_ID=" + hfACD_ID.Value + "  AND" _
                                & " DCG_GRD_ID='" + hfGRD_ID.Value + "' AND DCG_STM_ID=" + hfSTM_ID.Value _
                                & " AND DCG_STG_ID=" + stgId + ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstNotRequired.DataSource = ds
        lstNotRequired.DataTextField = "DOC_DESCR"
        lstNotRequired.DataValueField = "DOC_ID"
        lstNotRequired.DataBind()
    End Sub

    Sub UpdateListItems(ByVal lstFrom As ListBox, ByVal lstTo As ListBox, ByVal stgId As String, ByVal mode As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim li As ListItem
        Dim lstTemp As New ListBox
        For Each li In lstFrom.Items
            If li.Selected = True Then
                lstTo.Items.Add(li)
                lstTemp.Items.Add(li)

                str_query = "exec updateGRADEJOINDOCUMENTS " _
                        & hfACD_ID.Value + "," _
                        & "'" + hfGRD_ID.Value + "'," _
                        & hfSTM_ID.Value + "," _
                        & stgId + "," _
                        & li.Value.ToString + "," _
                        & "'" + mode + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next

        For Each li In lstTemp.Items
            lstFrom.Items.Remove(li)
        Next

        lstTo.ClearSelection()
        lstFrom.ClearSelection()
    End Sub

    Function GetIds(ByVal lst As ListBox) As String
        Dim i As Integer
        Dim str As String = ""
        For i = 0 To lst.Items.Count - 1
            If str <> "" Then
                str += ","
            End If
            str += lst.Items(i).Value.ToString
        Next
        Return str
    End Function

    'Sub SaveData()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String
    '    Dim i As Integer
    '    Dim lblStgid As New Label
    '    Dim lstRequired As New ListBox
    '    Dim lstNotRequired As New ListBox
    '    Dim strReqd As String
    '    Dim strNonReqd As String
    '    Dim chkProceed As New CheckBox
    '    For i = 0 To gvStage.Rows.Count - 1
    '        With gvStage.Rows(i)
    '            lstRequired = .FindControl("lstRequired")
    '            lstNotRequired = .FindControl("lstNotRequired")
    '            chkProceed = .FindControl("chkProceed")
    '            strReqd = GetIds(lstRequired)
    '            strNonReqd = GetIds(lstNotRequired)

    '            str_query = "exec updateGRADEJOINDOCUMENTS " _
    '                      & "'" + hfACD_ID.Value + "'" _
    '                      & "'" + hfGRD_ID.Value + "'" _
    '                      & "'" + hfSTM_ID.Value + "'" _
    '                      & "'" + lblStgid.Text + "'" _
    '                      & "'" + strReqd + "'" _
    '                      & "'" + strNonReqd + "'"

    '            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    '            str_query = "UPDATE GRADE_JOINDOCUMENTS_M SET DCG_bPROCEED='" + chkProceed.Checked.ToString + "'" _
    '                    & " WHERE DCG_GRD_ID='" + hfGRD_ID.Value + " AND DCG_STM_ID=" + hfSTM_ID.Value _
    '                    & " AND DCG_ACD_ID=" + hfACD_ID.Value + " AND DCG_STG_ID=" + lblStgid.Text

    '            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    '        End With
    '    Next

    'End Sub
#End Region

   

    Protected Sub gvStage_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStage.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblStgid As New Label

                lblStgid = e.Row.FindControl("lblStgId")

                Dim lstRequired As New ListBox
                Dim lstNotRequired As New ListBox
                Dim bProceed As Boolean
                Dim chkProceed As New CheckBox

                lstRequired = e.Row.FindControl("lstRequired")
                lstNotRequired = e.Row.FindControl("lstNotRequired")

                bProceed = BindRequiredList(lstRequired, lblStgid.Text)
                BindNotRequiredList(lstNotRequired, lblStgid.Text)
                chkProceed = e.Row.FindControl("chkProceed")
                chkProceed.Checked = bProceed

                If lstRequired.Items.Count = 0 And lstNotRequired.Items.Count = 0 Then
                    e.Row.Visible = False
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStage.SelectedIndexChanged
        Try
            Dim lstRequired As New ListBox
            Dim lblStgid As New Label

            Dim lstNotRequired As New ListBox
            With gvStage.SelectedRow
                lstRequired = .FindControl("lstRequired")
                lstNotRequired = .FindControl("lstNotRequired")
                lblStgid = .FindControl("lblStgId")
            End With
            If ViewState("action") = "right" Then
                UpdateListItems(lstNotRequired, lstRequired, lblStgid.Text, "add")
                ViewState("action") = ""
            End If
            If ViewState("action") = "left" Then
                UpdateListItems(lstRequired, lstNotRequired, lblStgid.Text, "delete")
                ViewState("action") = ""
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lnkBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBack.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
End Class
