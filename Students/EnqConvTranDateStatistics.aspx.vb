﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.HttpContext
Imports InfosoftGlobal
Imports System.Globalization
Partial Class Students_EnqConvTranDateStatistics
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public arr_FCColors(20) As String
    Public FC_ColorCounter As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'dbbarENQ.Style.Add("width", "60.40%")
        If Page.IsPostBack = False Then
            Try
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")



                Dim dt As Date = Date.Today.AddDays(-30)
                Dim dt1 As Date = Date.Today

                txtIncFromDT.Text = dt.ToString("dd/MMM/yyyy", CultureInfo.InvariantCulture)
                txtIncDateTo.Text = dt1.ToString("dd/MMM/yyyy", CultureInfo.InvariantCulture)

                ''added by nahyan on 22 of june2015 for global

                If Convert.ToString(Session("sBsuid")) = "800030" Then
                    ViewState("REGION_ID") = "2"
                Else
                    ViewState("REGION_ID") = "1"
                End If

                BIND_ACY_IDS()
                BIND_REGION_TAB()
                ApplyCss()
                BIND_CHART()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

            ''added by nahyan on 22 of june2015 for global

            If Convert.ToString(Session("sBsuid")) = "800030" Then
                ViewState("REGION_ID") = "2"
            Else
                ViewState("REGION_ID") = "1"
            End If
            BIND_REGION_TAB()
            ApplyCss()

            BIND_CHART()
        Catch ex As Exception
            UtilityObj.Errorlog("search stu dashboard", (ex.Message))
        End Try

    End Sub
    Protected Sub btnResetDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetDate.Click
        Dim dt As Date = Date.Today.AddDays(-30)
        Dim dt1 As Date = Date.Today

        txtIncFromDT.Text = dt.ToString("dd/MMM/yyyy", CultureInfo.InvariantCulture)
        txtIncDateTo.Text = dt1.ToString("dd/MMM/yyyy", CultureInfo.InvariantCulture)

        Try

            ''added by nahyan on 22 of june2015 for global

            If Convert.ToString(Session("sBsuid")) = "800030" Then
                ViewState("REGION_ID") = "2"
            Else
                ViewState("REGION_ID") = "1"
            End If
            BIND_REGION_TAB()
            ApplyCss()

            BIND_CHART()
        Catch ex As Exception
            UtilityObj.Errorlog("search stu dashboard", (ex.Message))
        End Try
    End Sub
    'Protected Sub chkPeriod_CheckedChanged(sender As Object, e As EventArgs) Handles chkPeriod.CheckedChanged
    '    '  Re_Bind_Info()
    'End Sub
    Protected Sub chklACY_IDs_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chklACY_IDs.SelectedIndexChanged

        Dim ACD_YEARS As String = String.Empty
        Dim strYear As String = String.Empty
        For Each item As ListItem In chklACY_IDs.Items
            If item.Selected = True Then
                ACD_YEARS += item.Value + "|"
                strYear += item.Text + ","

            End If
        Next
        If strYear.Trim = "" Then
            For Each item As ListItem In chklACY_IDs.Items
                item.Selected = True
                ACD_YEARS += item.Value + "|"
                strYear += item.Text + ","
                Exit For
            Next
        End If


        txtFilterYear.Text = strYear.TrimEnd(",")
        HDNaCYiD.Value = ACD_YEARS

        BIND_REGION_TAB()
        BIND_CHART()
        ' Re_Bind_Info()

    End Sub

    Private Sub BIND_REGION_TAB()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim FILTER_INFO As String = String.Empty
            If txtIncFromDT.Text.Trim <> "" And txtIncDateTo.Text.Trim = "" Then
                FILTER_INFO += " AND CONVERT(datetime,REPLACE(CONVERT(VARCHAR(12),IM_DT_FROM,106),' ','/'))  >= '" & txtIncFromDT.Text.Trim & "'"
            ElseIf txtIncFromDT.Text.Trim = "" And txtIncDateTo.Text.Trim <> "" Then
                FILTER_INFO += " AND CONVERT(datetime,REPLACE(CONVERT(VARCHAR(12),IM_DT_FROM,106),' ','/'))  <= '" & txtIncDateTo.Text.Trim & "'"
            ElseIf txtIncFromDT.Text.Trim <> "" And txtIncDateTo.Text.Trim <> "" Then
                FILTER_INFO += " AND CONVERT(datetime,REPLACE(CONVERT(VARCHAR(12),IM_DT_FROM,106),' ','/'))  BETWEEN '" & txtIncFromDT.Text.Trim & "' AND '" & txtIncDateTo.Text.Trim & "'"
            End If
            Dim DS As New DataSet
            Dim param(6) As SqlParameter

            param(0) = New SqlParameter("@ACT_LVL", "0")
            param(1) = New SqlParameter("@LVL_BSU", "0")
            param(2) = New SqlParameter("@FromDate", Convert.ToDateTime(txtIncFromDT.Text))
            param(3) = New SqlParameter("@ToDate", Convert.ToDateTime(txtIncDateTo.Text))
            param(4) = New SqlParameter("@ACY_IDS", HDNaCYiD.Value)
            param(5) = New SqlParameter("@BSU_ID", Convert.ToString(Session("sBsuid")))
            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[CORP].[STUDENT_DASHBOARD_STATISTICS_TRANWISE]", param)
            rptTabDB.DataSource = DS.Tables(0)
            rptTabDB.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboard", ex.Message)
        End Try
    End Sub

    Private Function Get_City_ID_By_CODE(ByVal bsuCity As String) As String
        Dim cityId As String = String.Empty
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(24) As SqlParameter
            param(0) = New SqlParameter("@EMR_CODE", bsuCity)
            cityId = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "CORP.GET_CITY_ID_BY_CODE", param)


        Catch ex As Exception
            UtilityObj.Errorlog("Get_City_ID_By_CODE", ex.Message)
        End Try
        Return cityId
    End Function

    Private Sub ApplyCss()
        Try


            For Each item As RepeaterItem In rptTabDB.Items
                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                    Dim hfWidthENQ As HiddenField = DirectCast(item.FindControl("hfWidthENQ"), HiddenField)
                    Dim dbbarENQ As HtmlControl = DirectCast(item.FindControl("dbbarENQ"), HtmlControl)

                    Dim hfwidthREG As HiddenField = DirectCast(item.FindControl("hfwidthREG"), HiddenField)
                    Dim dbbarREG As HtmlControl = DirectCast(item.FindControl("dbbarREG"), HtmlControl)

                    Dim hfwidthTC As HiddenField = DirectCast(item.FindControl("hfwidthTC"), HiddenField)
                    Dim dbbarTC As HtmlControl = DirectCast(item.FindControl("dbbarTC"), HtmlControl)

                    Dim hfZoneID As HiddenField = DirectCast(item.FindControl("hfZoneID"), HiddenField)
                    Dim divTabHolder As HtmlControl = DirectCast(item.FindControl("divTabHolder"), HtmlControl)
                    Dim divRightArrow As HtmlControl = DirectCast(item.FindControl("divRightArrow"), HtmlControl)
                    Dim lbtnZoneID As LinkButton = DirectCast(item.FindControl("lbtnZoneID"), LinkButton)

                    dbbarENQ.Style.Add("width", hfWidthENQ.Value + "%")
                    dbbarREG.Style.Add("width", hfwidthREG.Value + "%")
                    dbbarTC.Style.Add("width", hfwidthTC.Value + "%")

                    If ViewState("REGION_ID") = "0" Then
                        divTitle.InnerText = lbtnZoneID.CommandName
                        divTitle.InnerText = divTitle.InnerText & " -  (" & txtIncFromDT.Text & " - " & txtIncDateTo.Text & " )"
                        ViewState("REGION_ID") = hfZoneID.Value
                        divTabHolder.Attributes.Add("class", "dbactiveCat")
                        divRightArrow.Attributes.Add("class", "arrRightActive")
                    ElseIf ViewState("REGION_ID") = hfZoneID.Value Then
                        divTitle.InnerText = lbtnZoneID.CommandName
                        divTitle.InnerText = divTitle.InnerText & " -  (" & txtIncFromDT.Text & " - " & txtIncDateTo.Text & " )"
                        divTabHolder.Attributes.Add("class", "dbactiveCat")
                        divRightArrow.Attributes.Add("class", "arrRightActive")
                    Else
                        divTabHolder.Attributes.Add("class", "dbinactiveCat")
                        divRightArrow.Attributes.Add("class", "arrRightInactive")
                    End If


                End If
            Next
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub rptTabDB_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptTabDB.ItemCommand
        If e.CommandName <> "Page" Then
            divTitle.InnerText = e.CommandName
            ViewState("REGION_ID") = e.CommandArgument
            divTitle.InnerText = divTitle.InnerText & " -  (" & txtIncFromDT.Text & " - " & txtIncDateTo.Text & " )"
            BIND_CHART()
            divChart.Visible = True




            ApplyCss()
        End If
    End Sub

    Private Sub BIND_CHART()
        Try
            Dim arr() As String
            arr = New String() {"b3bec3", "9bc101", "9b01c1", "b68db6", "aae6bf", "b9cf75", "d5aff0", "f0baaf", "aae6db", "d7e6aa", "d2e6aa", "aad9e6", "aac2e6", "8176c0", "974594", "ed4e61", _
    "e6acef", "efdc07", "73d1f8", "ee735b", "f704dd", "b8f149", "f1cb49", "a849f1", "07daf9", "91b8be", _
     "52ed4e", "0a937e", "80269e", "9e266d", "f4e8b0", "8a008c", "288c00", "8e006e", "70e2bb", _
    "d2cad6", "aae6db", "d7e6aa", "d2e6aa", "aad9e6", "aac2e6", "8176c0", "974594", "ed4e61", _
    "BD1550", "E97F02", "008C9E", "8FBE00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE", "46ffb3", "b6ce6b", "acefc4", "cfefac", "efe3ac"
    }
            Dim i As Integer = 0
            Dim Enq_Xml As String = String.Empty
            Dim Reg_XML As String = String.Empty
            Dim Stud_Xml As String = String.Empty
            Dim TC_Xml As String = String.Empty
            Enq_Xml = "<chart caption='ENQUIRIES' showvalues='1' showpercentvalues='0'  skipOverlapLabels='0' enableSmartLabels='1' formatNumber='0' formatNumberScale='0'  divLineColor='e8e8e8' divLineThickness='1' showCanvasBg='0' placeValuesInside='1' chartRightMargin='1' chartTopMargin='1' baseFont='Helvetica' baseFontSize ='10' baseFontColor ='666666'>"
            Reg_XML = "<chart caption='REGISTRATIONS' showvalues='1' showpercentvalues='0'  skipOverlapLabels='0' enableSmartLabels='1' formatNumber='0' formatNumberScale='0'  divLineColor='e8e8e8' divLineThickness='1' showCanvasBg='0' placeValuesInside='1' chartRightMargin='1' chartTopMargin='1' baseFont='Helvetica' baseFontSize ='10' baseFontColor ='666666'>"
            Stud_Xml = "<chart caption='OFFERED' showvalues='1' showpercentvalues='0'  skipOverlapLabels='0' enableSmartLabels='1' formatNumber='0' formatNumberScale='0'  divLineColor='e8e8e8' divLineThickness='1' showCanvasBg='0' placeValuesInside='1' chartRightMargin='1' chartTopMargin='1' baseFont='Helvetica' baseFontSize ='10' baseFontColor ='666666'>"
            TC_Xml = "<chart caption='ENROLLED' showvalues='1' showpercentvalues='0'  skipOverlapLabels='0' enableSmartLabels='1' formatNumber='0' formatNumberScale='0' divLineColor='e8e8e8' divLineThickness='1' showCanvasBg='0' placeValuesInside='1' chartRightMargin='1' chartTopMargin='1' baseFont='Helvetica' baseFontSize ='10' baseFontColor ='666666'>"
            Dim ChartStyle As String = String.Empty
            Dim varcolor As String = "FF8E46"
            ChartStyle = "<styles>" & _
       "<definition>" & _
           "<style name='lblFont' type='font' font='Helvetica' size='10' color='666666' />" & _
           "<style name='capFont' type='font' font='Helvetica' size='16' color='363636'  bold='0' underline='0' letterSpacing='2' />" & _
           "<style name='valFont' type='font' font='Helvetica' size='12' color='555152' />" & _
             "<style name='plotShadow' type='Shadow' distance='0' angle='0' />" & _
             "<style name='myAxisTitlesFont' type='font' font='Arial' size='11' bold='1' color='ff0000'/>" & _
       "</definition>" & _
       "<application>" & _
          "<apply toObject='DataLabels' styles='lblFont' />" & _
           "<apply toObject='DataValues' styles='valFont' />" & _
            "<apply toObject='Caption' styles='capFont' />" & _
            "<apply toObject='DataPlot' styles='plotShadow' />" & _
             "<apply toObject='XAxisName' styles='myAxisTitlesFont' />" & _
       "</application>" & _
    "</styles>"
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(6) As SqlParameter
            param(0) = New SqlParameter("@ACT_LVL", "2")
            param(1) = New SqlParameter("@LVL_BSU", ViewState("REGION_ID"))
            param(2) = New SqlParameter("@FromDate", Convert.ToDateTime(txtIncFromDT.Text))
            param(3) = New SqlParameter("@ToDate", Convert.ToDateTime(txtIncDateTo.Text))
            param(4) = New SqlParameter("@ACY_IDS", HDNaCYiD.Value)
            param(5) = New SqlParameter("@BSU_ID", Convert.ToString(Session("sBsuid")))
            Dim isSliced As Integer = 1

            Session.Remove("FusionFromDt")
            Session("FusionFromDt") = Convert.ToDateTime(txtIncFromDT.Text)
            Session.Remove("FusionToDt")
            Session("FusionToDt") = Convert.ToDateTime(txtIncDateTo.Text)
            Session("strAcyId") = HDNaCYiD.Value

            ''generating 

            Using readerDashboard As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "[CORP].[STUDENT_DASHBOARD_STATISTICS_TRANWISE]", param)
                If readerDashboard.HasRows = True Then
                    While readerDashboard.Read
                        Dim bsuId As String = String.Empty
                        bsuId = Get_City_ID_By_CODE(readerDashboard("BSU_ID").ToString)

                        Enq_Xml = Enq_Xml & "<set label='" & readerDashboard("BSU_NAME").ToString & "' value='" & readerDashboard("ENQS").ToString & "' color='" & arr(i) & "' link='javascript:AddDetails(" & bsuId & "," & ViewState("REGION_ID") & ",1);' />"
                        Reg_XML = Reg_XML & "<set label='" & readerDashboard("BSU_NAME").ToString & "' value='" & readerDashboard("REGS").ToString & "' color='" & arr(i) & "' link='javascript:AddDetails(" & bsuId & "," & ViewState("REGION_ID") & ",2);' />"
                        Stud_Xml = Stud_Xml & "<set label='" & readerDashboard("BSU_NAME").ToString & "' value='" & readerDashboard("STUDS").ToString & "' color='" & arr(i) & "' link='javascript:AddDetails(" & bsuId & "," & ViewState("REGION_ID") & ",3);'/>"
                        TC_Xml = TC_Xml & "<set label='" & readerDashboard("BSU_NAME").ToString & "' value='" & readerDashboard("TCS").ToString & "' color='" & arr(i) & "' link='javascript:AddDetails(" & bsuId & "," & ViewState("REGION_ID") & ",4);'/>"
                        i = i + 1
                        isSliced = 0
                    End While
                End If
            End Using
            Enq_Xml = Enq_Xml & ChartStyle & "</chart>"
            Reg_XML = Reg_XML & ChartStyle & "</chart>"
            Stud_Xml = Stud_Xml & ChartStyle & "</chart>"
            TC_Xml = TC_Xml & ChartStyle & "</chart>"
            ' Enq_Xml = Enq_Xml & "</chart>"
            'Reg_XML = Reg_XML & "</chart>"
            'Stud_Xml = Stud_Xml & "</chart>"
            'TC_Xml = TC_Xml & "</chart>"
            ltPieEnq.Text = FusionCharts.RenderChart("Column3D", "", Enq_Xml, "Chart1ID", "300", "220", False, True)
            ltPieReg.Text = FusionCharts.RenderChart("Column3D", "", Reg_XML, "enq", "300", "220", False, True)
            ltPieStud.Text = FusionCharts.RenderChart("Column3D", "", Stud_Xml, "stud", "300", "220", False, True)
            ltPieTC.Text = FusionCharts.RenderChart("Column3D", "", TC_Xml, "tc", "300", "220", False, True)
            ' ltPieEnq.Text = FusionCharts.RenderChartHTML("../FusionCharts/Column3D.swf", "", Enq_Xml, "enq", "300", "220", False)
            ' ltPieReg.Text = FusionCharts.RenderChartHTML("../FusionCharts/Column3D.swf", "", Reg_XML, "enq", "300", "220", False)
            ' ltPieStud.Text = FusionCharts.RenderChartHTML("../FusionCharts/Column3D.swf", "", Stud_Xml, "stud", "300", "220", False)
            ' ltPieTC.Text = FusionCharts.RenderChartHTML("../FusionCharts/Column3D.swf", "", TC_Xml, "tc", "300", "220", False)
        Catch ex As Exception
            UtilityObj.Errorlog("BindChart", ex.Message)
        End Try
    End Sub
    Private Sub BIND_ACY_IDS()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim DS As New DataSet
            Dim strYear As String = String.Empty
            Dim ACY_IDS As String = String.Empty

            Dim param(2) As SqlParameter

            'param(0) = New SqlParameter("@BIND_TYPE", "ACY_IDS")

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "CORP.GET_ACADEMICYEAR_FOR_DASHBOARD")
            chklACY_IDs.DataSource = DS.Tables(0)
            chklACY_IDs.DataTextField = "ACY_DESCR"
            chklACY_IDs.DataValueField = "ACD_ACY_ID"
            chklACY_IDs.DataBind()
            chklACY_IDs.ClearSelection()
            For Each item As ListItem In chklACY_IDs.Items
                item.Selected = True
                strYear += item.Text + ","
                ACY_IDS += item.Value + "|"

            Next
            txtFilterYear.Text = strYear.TrimEnd(",")
            HDNaCYiD.Value = ACY_IDS



        Catch ex As Exception

        End Try
    End Sub
End Class
