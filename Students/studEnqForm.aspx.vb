Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports UtilityObj
Imports System
Imports System.Text.RegularExpressions
Imports System.IO
Partial Class studEnqForm
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private m_bIsTerminating As Boolean = False

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    Page.MaintainScrollPositionOnPostBack = True
    'End Sub
    'Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
    '    Dim viewState As String = Request.Form("__VSTATE")
    '    Dim bytes As Byte() = Convert.FromBase64String(viewState)
    '    bytes = Compressor.Decompress(bytes)
    '    Dim formatter As New LosFormatter()
    '    Return formatter.Deserialize(Convert.ToBase64String(bytes))
    'End Function

    'Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
    '    Dim formatter As New LosFormatter()
    '    Dim writer As New StringWriter()
    '    formatter.Serialize(writer, viewState)
    '    Dim viewStateString As String = writer.ToString()
    '    Dim bytes As Byte() = Convert.FromBase64String(viewStateString)
    '    bytes = Compressor.Compress(bytes)
    '    ClientScript.RegisterHiddenField("__VSTATE", Convert.ToBase64String(bytes))
    'End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.MaintainScrollPositionOnPostBack = False
        If Page.IsPostBack = False Then
            Try
                hometab.Visible = True
                Call Display_Msg()
                maintab.Visible = False
                divNote.Visible = False
                divRef.Visible = False

                Call GEMS_BSU_4_Student()

                Call callAll_fun_OnPageLoad()
                Call add_keyPress_check()


                mnuRegMaster.Items(0).ImageUrl = "~/Images/FormRegImg/Schools_2.jpg"
                chkMHide.Checked = False
                chkGHide.Checked = True
                chksms.Checked = True
                chkEAdd.Checked = True
                chkPubl.Checked = True
                rdPri_Father.Checked = True
                rbParentF.Checked = True
                tbReloc.Visible = False
                rdMale.Checked = True
                rdFemale.Checked = False
                rbHthAll_No.Checked = True
                rbHthSM_No.Checked = True
                rbHthPER_No.Checked = True
                rbHthOther_No.Checked = True
                rbHthLS_No.Checked = True
                rbHthSE_no.Checked = True
                rbHthEAL_No.Checked = True
                rbHthMus_No.Checked = True
                rbHthBehv_No.Checked = True
                rbHthEnr_No.Checked = True
                rbHthSport_No.Checked = True
                rbMealNo.Checked = True
                btnUpdate.Visible = False
                ltSchoolType.Text = "Current School"
                ViewState("btnStatus") = ""
                'ViewState("btnStateCheck") = ""
                ViewState("V_Staff_Check") = 0
                ViewState("V_Sibl_Check") = 0
                ViewState("V_Sister_check") = 0
                ViewState("V_GEMS_STU_check") = 0
                hfValid_ID.Value = "0"
                ViewState("TABLE_School_Pre") = CreateDataTable()
                ViewState("TABLE_School_Pre").Rows.Clear()
                ViewState("id") = 1
                 ViewState("ENQTYPE")="O"
                gridbind()
                divConf.Visible = False
                divDecl.Visible = True
                Dim OASISUsr As String = String.Empty
                If Not Session("sUsr_id") Is Nothing Then
                    If Session("sUsr_id").ToString.Trim <> "" Then
                        OASISUsr = Session("sUsr_id")
                    End If

                End If
                Call disable_Rem_MenuList()
                txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                If ((Not (Request.QueryString("MainMnu_code")) Is Nothing) And (OASISUsr <> "")) Then
                    ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                    Session("Registrar") = "S100065"
                    ViewState("ENQTYPE") = "R"
                    If ViewState("MainMnu_code") = "S100065" Then

                        If Not Session("Online_sBsuid") Is Nothing Then
                            Dim CurBsUnit As String = Session("Online_sBsuid")
                            If Not ddlGEMSSchool.Items.FindByValue(Session("Online_sBsuid")) Is Nothing Then
                                ddlGEMSSchool.ClearSelection()
                                ddlGEMSSchool.Items.FindByValue(Session("Online_sBsuid")).Selected = True
                            End If
                        End If



                        If Not ddlGemsSchools.Items.FindByText("OTHER") Is Nothing Then
                            ddlGemsSchools.ClearSelection()
                            ddlGemsSchools.Items.FindByText("OTHER").Selected = True
                        End If

                        ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)
                    End If

                ElseIf Not (Request.QueryString("RID")) Is Nothing Then
                    ViewState("RFS_ID") = Request.QueryString("RID")
                    ViewState("school_id") = Request.QueryString("SID")
                    BINDUSER_ref()
                    If Not ddlGEMSSchool.Items.FindByValue(ViewState("school_id")) Is Nothing Then
                        ddlGEMSSchool.ClearSelection()
                        ddlGEMSSchool.Items.FindByValue(ViewState("school_id")).Selected = True
                        If Not ddlGemsSchools.Items.FindByValue(ViewState("school_id")) Is Nothing Then
                            ddlGemsSchools.ClearSelection()
                            ddlGemsSchools.Items.FindByValue(ViewState("school_id")).Selected = True
                        End If
                    End If
                    ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)




                Else
                    Session("Registrar") = ""
                    ViewState("ENQTYPE") = "O"
                    If Not ddlGEMSSchool.Items.FindByValue(Session("Online_sBsuid")) Is Nothing Then
                        ddlGEMSSchool.ClearSelection()
                        ddlGEMSSchool.Items.FindByValue(Session("Online_sBsuid")).Selected = True
                        If Not ddlGemsSchools.Items.FindByValue(Session("Online_sBsuid")) Is Nothing Then
                            ddlGemsSchools.ClearSelection()
                            ddlGemsSchools.Items.FindByValue(Session("Online_sBsuid")).Selected = True
                        End If
                    End If
                    ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)
                End If


                '  Call callAll_fun_OnPageLoad()


                If Not Session("Applic") Is Nothing Then
                    If Session("Applic") = "1" Then
                        Call Applic_info()
                    End If
                End If

                Call ControlTable_row()

                Dim Active_count As New ArrayList
                ViewState("Active_count") = Active_count
                ltMessage.Visible = False
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
            End Try

        End If


    End Sub
#Region "NO PRIMARY CONTACT"
    Protected Sub btnVwDecl_Finish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwDecl_Finish.Click
        Dim bDeclare As Boolean = True
        Dim commString As String = "The following fields needs to be validated: <UL>"

        ltAppName.Text = Trim(txtFname.Text) & " " & Trim(txtMname.Text) & " " & Trim(txtLname.Text)
        ltSchool.Text = ddlGEMSSchool.SelectedItem.Text
        ltCurr.Text = ddlCurri.SelectedItem.Text
        ltAcd.Text = ddlAca_Year.SelectedItem.Text
        ltGrade.Text = ddlGrade.SelectedItem.Text
        ltDob.Text = txtDob.Text
        ltNat.Text = ddlNational.SelectedItem.Text
        LtPref_Cont.Text = ddlFPref_contact.SelectedItem.Text

        If rdPri_Father.Checked Then
            ltPrim_cont.Text = rdPri_Father.Text
        ElseIf rbPri_Mother.Checked Then
            ltPrim_cont.Text = rbPri_Mother.Text
        ElseIf rdPri_Guard.Checked Then
            ltPrim_cont.Text = rdPri_Guard.Text
        End If






        If chkagree.Checked = False Then
            commString = commString & "<LI>Please tick the declaration box to confirm that you agree to the above declaration."
            bDeclare = False
        End If

        Captcha1.ValidateCaptcha(txtCapt.Text.Trim())

        If Captcha1.UserValidated = False Then

            commString = commString & "<LI>The characters you entered didn't match the word verification. Please try again."
            bDeclare = False
        End If



        If bDeclare = True Then

            divConf.Visible = True
            divDecl.Visible = False
            lblError.Text = ""
        Else

            divConf.Visible = False
            divDecl.Visible = True
 lblError.Text = commString & "</UL>"
        End If




        ' ViewState("btnStateCheck") = " declare finish "

        'Dim mp As AjaxControlToolkit.ModalPopupExtender = TryCast(Page.FindControl("mpe"), AjaxControlToolkit.ModalPopupExtender)
        'If mp IsNot Nothing Then
        '    mp.Show()
        'End If


    End Sub
    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        If Final_validation() = True Then

            'If (Request.QueryString("MainMnu_code")) Is Nothing Then
            If ViewState("ENQTYPE") = "O" Then
                If ViewState("OpenFor") = 1 Then

                    If OpenOnlineCheck() = True Then
                        Call SAVEENQUIRY_DATA()
                    End If
                Else

                    Call SAVEENQUIRY_DATA()

                End If

            Else
                Call SAVEENQUIRY_DATA()

            End If

        End If
    End Sub

    Protected Sub btnConf_Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConf_Prev.Click
        lblError.Text = ""
        divConf.Visible = False
        divDecl.Visible = True
    End Sub


    Function Valid_Health() As Boolean
        If ViewState("btnStatus") = "" Then
            Valid_Health = False
        Else
            Valid_Health = True
        End If


    End Function
    Function Valid_Declaration() As Boolean

        Dim V_Decl As Boolean

        If chkagree.Checked = True Then
            V_Decl = True

        End If
        Valid_Declaration = V_Decl


    End Function
    'Protected Sub btnPrevSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevSave1.Click, btnPrevSave2.Click
    '    'btnPrevedit.Visible = False
    '    ' btnPrevClose1.Visible = False
    '    System.Threading.Thread.Sleep(80)
    Sub SAVEENQUIRY_DATA()
        Dim Final_Result As Integer
        Dim Dup_count As Integer

        Dim EQM_APPLPASPRTNO As String = txtPassport.Text
        Dim EQS_ACY_ID As String = ddlAca_Year.SelectedItem.Value
        Dim EQS_BSU_ID As String = ddlGEMSSchool.SelectedItem.Value
        Dim EQS_GRD_ID As String = ddlGrade.SelectedItem.Value
        Dim EQS_SHF_ID As String = ddlShift.SelectedItem.Value
        Dim EQS_STM_ID As String = ddlStream.SelectedItem.Value
        Dim CLM_ID As String = ddlCurri.SelectedItem.Value
        Dim URL_Enquiry As String = String.Empty

        Dup_count = AccessStudentClass.GetDuplicate_Enquiry_M(EQM_APPLPASPRTNO, EQS_SHF_ID, EQS_STM_ID, EQS_BSU_ID, EQS_GRD_ID, EQS_ACY_ID, CLM_ID)
        If Dup_count = 0 Then


            Final_Result = Enquiry_transaction()


            If Final_Result = 0 Then


                Session("TEMP_ApplNo") = ViewState("TEMP_ApplNo")
                Session("Enq_ID") = ViewState("Enq_ID")
                If Not ViewState("TABLE_School_Pre") Is Nothing Then

                    ViewState("TABLE_School_Pre").Rows.Clear()
                    ViewState("id") = 1
                    gridbind()
                End If
                Response.Redirect("~\Students\StudEnqApplAck.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                m_bIsTerminating = True
                Exit Sub

            End If

        Else
            lblError.Text = "Record already exist"

        End If
    End Sub
    Protected Overloads Overrides Sub RaisePostBackEvent(ByVal sourceControl As IPostBackEventHandler, ByVal eventArgument As String)
        If m_bIsTerminating = False Then
            MyBase.RaisePostBackEvent(sourceControl, eventArgument)
        End If
    End Sub
    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        If m_bIsTerminating = False Then
            MyBase.Render(writer)
        End If
    End Sub
    Private Sub BINDUSER_ref()
        Dim areacode As String = String.Empty
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select TOP 1 M.RFM_CODE,S.RFS_REF_EMAIL from REF.REFERRAL_M as M inner join REF.REFERRAL_S AS S " & _
                                " on S.RFS_RFM_ID=M.RFM_ID WHERE RFS_ID='" & ViewState("RFS_ID") & "'"

        Using USER_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If USER_reader.HasRows Then
                While USER_reader.Read
                    txtRefCode.Text = Convert.ToString(USER_reader("RFM_CODE"))
                    txtREFEmail.Text = Convert.ToString(USER_reader("RFS_REF_EMAIL"))
                    rbRefYes.Checked = True
                    rbRefNo.Checked = False
                End While



            End If
        End Using
    End Sub
    Sub add_keyPress_check()





        'lblf.Attributes.Add("onclick", "Hide_me(1)")
        'lblM.Attributes.Add("onclick", "Hide_me(2)")
        'lblG.Attributes.Add("onclick", "Hide_me(3)")
        'rdPri_Father.Attributes.Add("onclick", "Hide_row(1)")
        'rbPri_Mother.Attributes.Add("onclick", "Hide_row(2)")
        'rdPri_Guard.Attributes.Add("onclick", "Hide_row(3)")
        rbRefYes.Attributes.Add("onclick", "ValidatorsRef()")
        rbRefNo.Attributes.Add("onclick", "ValidatorsRef()")
        chkFHide.Attributes.Add("onclick", "Hide_me()")
        chkMHide.Attributes.Add("onclick", "Hide_me()")
        chkGHide.Attributes.Add("onclick", "Hide_me()")

        chkFStaff_GEMS.Attributes.Add("onclick", "fill_Staff(this)")
        chkFExStud_Gems.Attributes.Add("onclick", "fill_Stud(this)")
        chkMStaff_GEMS.Attributes.Add("onclick", "fill_MStaff(this)")
        chkMExStud_Gems.Attributes.Add("onclick", "fill_MStud(this)")

        chkSibling.Attributes.Add("onclick", "fill_Sibl(this)")
        chkTran_Req.Attributes.Add("onclick", "fill_Tran(this)")
        txtPassIss_date.Attributes.Add("onblur", "fillPassport()")
        txtPassExp_Date.Attributes.Add("onfocus", "fillPassport()")
        chkOLang.Attributes.Add("onclick", "GetSelectedItem()")
        ' chkOLang.Attributes.Add("onclick", "javascript:Cond1(event);")
        ddlFLang.Attributes.Add("onChange", "chkFirst_lang()")
        txtVisaIss_date.Attributes.Add("onblur", "fillVisa()")
        txtVisaExp_date.Attributes.Add("onfocus", "fillVisa()")
        txtFPhone_Oversea_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFPhone_Oversea_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFPhone_Oversea_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFHPhone_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFHPhone_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFHPhone.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFM_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFM_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFMobile_Pri.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFOPhone_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFOPhone_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFOPhone.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFFaxNo_country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFFaxNo_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtFFaxNo.Attributes.Add("onkeypress", "return isNumberKey(event)")

        txtMPhone_Oversea_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMPhone_Oversea_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMPhone_Oversea_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMHPhone_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMHPhone_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMHPhone.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMM_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMM_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMMobile_Pri.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMOPhone_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMOPhone_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMOPhone.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMFaxNo_country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMFaxNo_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMFaxNo.Attributes.Add("onkeypress", "return isNumberKey(event)")

        txtGPhone_Oversea_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGPhone_Oversea_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGPhone_Oversea_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGHPhone_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGHPhone_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGHPhone.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGM_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGM_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGMobile_Pri.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGOPhone_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGOPhone_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGOPhone.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGFaxNo_country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGFaxNo_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtGFaxNo.Attributes.Add("onkeypress", "return isNumberKey(event)")
        'txtFPoBox_Pri.Attributes.Add("onkeypress", "return isNumberKey(event)")
        'txtMPoBox_Pri.Attributes.Add("onkeypress", "return isNumberKey(event)")
        'txtGPoBox_Pri.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtStud_Contact_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtStud_Contact_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtStud_Contact_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMAgent_country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMAgent_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtMAgent_No.Attributes.Add("onkeypress", "return isNumberKey(event)")



        txtSCHPhone_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHPhone_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHPhone_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHFax_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHFax_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
        txtSCHFax_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")




    End Sub

    Sub country_code(ByVal CID As String)
        If CID.Trim = "172" Then
            txtStud_Contact_Country.Text = 971
            txtFPhone_Oversea_Country.Text = 971
            txtFHPhone_Country.Text = 971
            txtFOPhone_Country.Text = 971
            txtFFaxNo_country.Text = 971
            txtMPhone_Oversea_Country.Text = 971
            txtMHPhone_Country.Text = 971
            txtMOPhone_Country.Text = 971
            txtMFaxNo_country.Text = 971
            txtGPhone_Oversea_Country.Text = 971
            txtGHPhone_Country.Text = 971
            txtGOPhone_Country.Text = 971
            txtGFaxNo_country.Text = 971
            txtMAgent_country.Text = 971
        Else
            txtStud_Contact_Country.Text = ""
            txtFPhone_Oversea_Country.Text = ""
            txtFHPhone_Country.Text = ""
            txtFOPhone_Country.Text = ""
            txtFFaxNo_country.Text = ""
            txtMPhone_Oversea_Country.Text = ""
            txtMHPhone_Country.Text = ""
            txtMOPhone_Country.Text = ""
            txtMFaxNo_country.Text = ""
            txtGPhone_Oversea_Country.Text = ""
            txtGHPhone_Country.Text = ""
            txtGOPhone_Country.Text = ""
            txtGFaxNo_country.Text = ""
            txtMAgent_country.Text = ""
        End If

    End Sub
    Sub BSU_Enq_validation()
        Try
            Dim Temp_Rfv As New RequiredFieldValidator
            Dim Enq_hash As New Hashtable
            If Not Session("BSU_Enq_Valid") Is Nothing Then
                Enq_hash = Session("BSU_Enq_Valid")
                Dim hashloop As DictionaryEntry
                For Each hashloop In Enq_hash
                    If hashloop.Value <> "" Then
                        If Not Page.FindControl(hashloop.Value) Is Nothing Then
                            Temp_Rfv = Page.FindControl(hashloop.Value)
                            Temp_Rfv.EnableClientScript = True
                            Temp_Rfv.Enabled = True
                            Temp_Rfv.Visible = True
                        End If
                    End If
                Next
            End If
            Enq_hash.Clear()
            Session("BSU_Enq_Valid") = Nothing
            Dim BSU_ID As String = ddlGEMSSchool.SelectedValue
            Using Enq_validation_reader As SqlDataReader = AccessStudentClass.GetEnquiry_Validation(BSU_ID, "1")
                While Enq_validation_reader.Read
                    If Enq_validation_reader.HasRows Then
                        Enq_hash.Add(Enq_validation_reader("EQV_CODE"), Enq_validation_reader("CONTROL_ID"))
                        If Convert.ToString(Enq_validation_reader("CONTROL_ID")) <> "" Then
                            If Not Page.FindControl(Enq_validation_reader("CONTROL_ID")) Is Nothing Then
                                Temp_Rfv = Page.FindControl(Enq_validation_reader("CONTROL_ID"))
                                Temp_Rfv.EnableClientScript = False
                                Temp_Rfv.Enabled = False
                                Temp_Rfv.Visible = False
                            End If
                        End If
                    End If
                End While
            End Using
            Session("BSU_Enq_Valid") = Enq_hash
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "BSU_Enq_validation")
        End Try
    End Sub
    Sub Applic_info()
        Dim arInfo() As String
        Dim splitter As Char = "|"
        Dim new1 As String = String.Empty

        If Not Session("Enq_ID") Is Nothing Then

            Try
                Dim mobNumber As String()
                Dim hNumber As String()
                Dim oNumber As String()
                Dim overNumber As String()
                Dim fNumber As String()
                Dim conn As String = ConnectionManger.GetOASISConnectionString
                Dim param(1) As sqlparameter
                param(0) = New sqlparameter("@EQM_ENQID", Session("Enq_ID"))

                Using Applic_info_reader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETAPPL_DETAILS", param)
                    While Applic_info_reader.Read
                        If Applic_info_reader("EQM_PRIMARYCONTACT") = "F" Then
                            rdPri_Father.Checked = True
                        ElseIf Applic_info_reader("EQM_PRIMARYCONTACT") = "M" Then
                            rbPri_Mother.Checked = True
                        ElseIf Applic_info_reader("EQM_PRIMARYCONTACT") = "G" Then
                            rdPri_Guard.Checked = True
                        End If
                        If Not ddlFPref_contact.Items.FindByText(Applic_info_reader("EQM_PREFCONTACT")) Is Nothing Then
                            ddlFPref_contact.ClearSelection()
                            ddlFPref_contact.Items.FindByText(Applic_info_reader("EQM_PREFCONTACT")).Selected = True
                        End If
                        If Not ddlFCont_Status.Items.FindByText(Applic_info_reader("EQP_FSALUT")) Is Nothing Then
                            ddlFCont_Status.ClearSelection()
                            ddlFCont_Status.Items.FindByText(Applic_info_reader("EQP_FSALUT")).Selected = True
                        End If
                        If Not ddlMCont_Status.Items.FindByText(Applic_info_reader("EQP_MSALUT")) Is Nothing Then
                            ddlMCont_Status.ClearSelection()
                            ddlMCont_Status.Items.FindByText(Applic_info_reader("EQP_MSALUT")).Selected = True
                        End If
                        If Not ddlGCont_Status.Items.FindByText(Applic_info_reader("EQP_GSALUT")) Is Nothing Then
                            ddlGCont_Status.ClearSelection()
                            ddlGCont_Status.Items.FindByText(Applic_info_reader("EQP_GSALUT")).Selected = True
                        End If


                        txtFPri_Fname.Text = Applic_info_reader("EQP_FFIRSTNAME")
                        txtFPri_Mname.Text = Applic_info_reader("EQP_FMIDNAME")
                        txtFPri_Lname.Text = Applic_info_reader("EQP_FLASTNAME")
                        If Not ddlFPri_National1.Items.FindByValue(Applic_info_reader("EQP_FNATIONALITY")) Is Nothing Then
                            ddlFPri_National1.ClearSelection()
                            ddlFPri_National1.Items.FindByValue(Applic_info_reader("EQP_FNATIONALITY")).Selected = True
                        End If
                        If Not ddlFPri_National2.Items.FindByValue(Applic_info_reader("EQP_FNATIONALITY2")) Is Nothing Then
                            ddlFPri_National2.ClearSelection()
                            ddlFPri_National2.Items.FindByValue(Applic_info_reader("EQP_FNATIONALITY2")).Selected = True
                        End If
                        mobNumber = Applic_info_reader("EQP_FMOBILE").ToString.Replace(" ", "-").Split("-")
                        If mobNumber.Length = 3 Then
                            txtFM_Country.Text = mobNumber(0)
                            txtFM_Area.Text = mobNumber(1)
                            txtFMobile_Pri.Text = mobNumber(2)
                        ElseIf mobNumber.Length = 2 Then
                            txtFM_Area.Text = mobNumber(0)
                            txtFMobile_Pri.Text = mobNumber(1)
                        ElseIf mobNumber.Length = 1 Then
                            txtFMobile_Pri.Text = mobNumber(0)
                        End If

                        txtFPoBox_Pri.Text = Applic_info_reader("EQP_FPRMPOBOX")
                        txtFEmail_Pri.Text = StrConv(Applic_info_reader("EQP_FEMAIL"), VbStrConv.Lowercase)
                        txtFAdd1_overSea.Text = Applic_info_reader("EQP_FPRMADDR1")
                        txtFAdd2_overSea.Text = Applic_info_reader("EQP_FPRMADDR2")


                        If Not ddlFOverSeas_Add_Country.Items.FindByValue(Applic_info_reader("EQP_FPRMCOUNTRY")) Is Nothing Then
                            ddlFOverSeas_Add_Country.ClearSelection()
                            ddlFOverSeas_Add_Country.Items.FindByValue(Applic_info_reader("EQP_FPRMCOUNTRY")).Selected = True
                        End If


                        txtFApartNo.Text = Applic_info_reader("EQP_FCOMAPARTNO")
                        txtFBldg.Text = Applic_info_reader("EQP_FCOMBLDG")
                        txtFStreet.Text = Applic_info_reader("EQP_FCOMSTREET")
                        txtFArea.Text = Applic_info_reader("EQP_FCOMAREA")
                        txtFCity_pri.Text = Applic_info_reader("EQP_FCOMCITY")

                        'If Not ddlCity_pri.Items.FindByValue(Applic_info_reader("STS_FCOMCITY")) Is Nothing Then
                        '    ddlCity_pri.ClearSelection()
                        '    ddlCity_pri.Items.FindByValue(Applic_info_reader("STS_FCOMCITY")).Selected = True
                        'End If

                        If Not ddlFCountry_Pri.Items.FindByValue(Applic_info_reader("EQP_FCOMCOUNTRY")) Is Nothing Then
                            ddlFCountry_Pri.ClearSelection()
                            ddlFCountry_Pri.Items.FindByValue(Applic_info_reader("EQP_FCOMCOUNTRY")).Selected = True
                        End If

                        overNumber = Applic_info_reader("EQP_FPRMPHONE").ToString.Replace(" ", "-").Split("-")

                        If overNumber.Length = 3 Then
                            txtFPhone_Oversea_Country.Text = overNumber(0)
                            txtFPhone_Oversea_Area.Text = overNumber(1)
                            txtFPhone_Oversea_No.Text = overNumber(2)
                        ElseIf overNumber.Length = 2 Then
                            txtFPhone_Oversea_Area.Text = overNumber(0)
                            txtFPhone_Oversea_No.Text = overNumber(1)
                        ElseIf overNumber.Length = 1 Then
                            txtFPhone_Oversea_No.Text = overNumber(0)
                        End If




                        hNumber = Applic_info_reader("EQP_FRESPHONE").ToString.Replace(" ", "-").Split("-")
                        If hNumber.Length = 3 Then
                            txtFHPhone_Country.Text = hNumber(0)
                            txtFHPhone_Area.Text = hNumber(1)
                            txtFHPhone.Text = hNumber(2)
                        ElseIf hNumber.Length = 2 Then
                            txtFHPhone_Area.Text = hNumber(0)
                            txtFHPhone.Text = hNumber(1)
                        ElseIf hNumber.Length = 1 Then
                            txtFHPhone.Text = hNumber(0)
                        End If

                        oNumber = Applic_info_reader("EQP_FOFFPHONE").ToString.Replace(" ", "-").Split("-")
                        If oNumber.Length = 3 Then
                            txtFOPhone_Country.Text = oNumber(0)
                            txtFOPhone_Area.Text = oNumber(1)
                            txtFOPhone.Text = oNumber(2)
                        ElseIf oNumber.Length = 2 Then
                            txtFOPhone_Area.Text = oNumber(0)
                            txtFOPhone.Text = oNumber(1)
                        ElseIf oNumber.Length = 1 Then
                            txtFOPhone.Text = oNumber(0)
                        End If


                        fNumber = Applic_info_reader("EQP_FFAX").ToString.Replace(" ", "-").Split("-")
                        If fNumber.Length = 3 Then
                            txtFFaxNo_country.Text = fNumber(0)
                            txtFFaxNo_Area.Text = fNumber(1)
                            txtFFaxNo.Text = fNumber(2)
                        ElseIf fNumber.Length = 2 Then
                            txtFFaxNo_Area.Text = fNumber(0)
                            txtFFaxNo.Text = fNumber(1)
                        ElseIf fNumber.Length = 1 Then
                            txtFFaxNo.Text = fNumber(0)
                        End If

                        txtFPoboxLocal.Text = Applic_info_reader("EQP_FCOMPOBOX")
                        txtFOccup.Text = Applic_info_reader("EQP_FOCC")
                        txtFComp.Text = Applic_info_reader("EQP_FCOMPANY")
                        If Not ddlFCompany.Items.FindByValue(Applic_info_reader("EQP_FCOMP_ID")) Is Nothing Then
                            ddlFCompany.ClearSelection()
                            ddlFCompany.Items.FindByValue(Applic_info_reader("EQP_FCOMP_ID")).Selected = True
                        End If
                        txtFOverSeas_Add_City.Text = Applic_info_reader("EQP_FPRMCITY")
                        chkFExStud_Gems.Checked = Convert.ToBoolean(Applic_info_reader("EQP_bFGEMSSTAFF"))

                        If chkFExStud_Gems.Checked = True Then

                            txtFExStudName_Gems.Text = Session("ExStudName_Gems")
                            If Not ddlFExYear.Items.FindByValue(Applic_info_reader("EQP_FACD_YEAR")) Is Nothing Then
                                ddlFExYear.ClearSelection()
                                ddlFExYear.Items.FindByValue(Applic_info_reader("EQP_FACD_YEAR")).Selected = True
                            End If
                            If Not ddlFExStud_Gems.Items.FindByValue(Applic_info_reader("EQM_EXUNIT")) Is Nothing Then
                                ddlFExStud_Gems.ClearSelection()
                                ddlFExStud_Gems.Items.FindByValue(Applic_info_reader("EQM_EXUNIT")).Selected = True
                            End If

                        End If
                        chkFStaff_GEMS.Checked = Convert.ToBoolean(Applic_info_reader("EQM_bSTAFFGEMS"))
                        If chkFStaff_GEMS.Checked = True Then

                            If Not ddlFStaff_BSU.Items.FindByValue(Session("STAFFUNIT")) Is Nothing Then
                                ddlFStaff_BSU.ClearSelection()
                                ddlFStaff_BSU.Items.FindByValue(Session("STAFFUNIT")).Selected = True
                            End If
                            txtFStaffID.Text = Session("StaffID_ENQ")
                            txtFStaff_Name.Text = Session("STAFF_NAME")

                        End If


                        'arInfo = Session("str_Aboutus").Trim.Replace("''", "'").Split(splitter)
                        'If arInfo.Length > 0 Then
                        '    For i As Integer = 0 To arInfo.Length - 1
                        '        If arInfo(i).Trim <> "" Then
                        '            chkAboutUs.Items.FindByValue(arInfo(i).Trim).Selected = True
                        '        End If
                        '    Next
                        'End If

                        If Not ddlFEmirate.Items.FindByValue(Applic_info_reader("EQP_FCOMPOBOX_EMIR")) Is Nothing Then
                            ddlFEmirate.ClearSelection()
                            ddlFEmirate.Items.FindByValue(Applic_info_reader("EQP_FCOMPOBOX_EMIR")).Selected = True
                        End If






                        txtMPri_Fname.Text = Applic_info_reader("EQP_MFIRSTNAME")
                        txtMPri_Mname.Text = Applic_info_reader("EQP_MMIDNAME")
                        txtMPri_Lname.Text = Applic_info_reader("EQP_MLASTNAME")

                        If Not ddlMPri_National1.Items.FindByValue(Applic_info_reader("EQP_MNATIONALITY")) Is Nothing Then
                            ddlMPri_National1.ClearSelection()
                            ddlMPri_National1.Items.FindByValue(Applic_info_reader("EQP_MNATIONALITY")).Selected = True
                        End If
                        If Not ddlMPri_National2.Items.FindByValue(Applic_info_reader("EQP_MNATIONALITY2")) Is Nothing Then
                            ddlMPri_National2.ClearSelection()
                            ddlMPri_National2.Items.FindByValue(Applic_info_reader("EQP_MNATIONALITY2")).Selected = True
                        End If
                        mobNumber = Applic_info_reader("EQP_MMOBILE").ToString.Replace(" ", "-").Split("-")
                        If mobNumber.Length = 3 Then
                            txtMM_Country.Text = mobNumber(0)
                            txtMM_Area.Text = mobNumber(1)
                            txtMMobile_Pri.Text = mobNumber(2)
                        ElseIf mobNumber.Length = 2 Then
                            txtMM_Area.Text = mobNumber(0)
                            txtMMobile_Pri.Text = mobNumber(1)
                        ElseIf mobNumber.Length = 1 Then
                            txtMMobile_Pri.Text = mobNumber(0)
                        End If

                        txtMPoBox_Pri.Text = Applic_info_reader("EQP_MPRMPOBOX")
                        txtMEmail_Pri.Text = Applic_info_reader("EQP_MEMAIL").ToString.ToLower
                        txtMAdd1_overSea.Text = Applic_info_reader("EQP_MPRMADDR1")
                        txtMAdd2_overSea.Text = Applic_info_reader("EQP_MPRMADDR2")


                        If Not ddlMOverSeas_Add_Country.Items.FindByValue(Applic_info_reader("EQP_MPRMCOUNTRY")) Is Nothing Then
                            ddlMOverSeas_Add_Country.ClearSelection()
                            ddlMOverSeas_Add_Country.Items.FindByValue(Applic_info_reader("EQP_MPRMCOUNTRY")).Selected = True
                        End If


                        txtMApartNo.Text = Applic_info_reader("EQP_MCOMAPARTNO")
                        txtMBldg.Text = Applic_info_reader("EQP_MCOMBLDG")
                        txtMStreet.Text = Applic_info_reader("EQP_MCOMSTREET")
                        txtMArea.Text = Applic_info_reader("EQP_MCOMAREA")
                        txtMCity_pri.Text = Applic_info_reader("EQP_MCOMCITY")

                        'If Not ddlCity_pri.Items.FindByValue(Applic_info_reader("STS_FCOMCITY")) Is Nothing Then
                        '    ddlCity_pri.ClearSelection()
                        '    ddlCity_pri.Items.FindByValue(Applic_info_reader("STS_FCOMCITY")).Selected = True
                        'End If

                        If Not ddlMCountry_Pri.Items.FindByValue(Applic_info_reader("EQP_MCOMCOUNTRY")) Is Nothing Then
                            ddlMCountry_Pri.ClearSelection()
                            ddlMCountry_Pri.Items.FindByValue(Applic_info_reader("EQP_MCOMCOUNTRY")).Selected = True
                        End If

                        overNumber = Applic_info_reader("EQP_MPRMPHONE").ToString.Replace(" ", "-").Split("-")


                        If overNumber.Length = 3 Then
                            txtMPhone_Oversea_Country.Text = overNumber(0)
                            txtMPhone_Oversea_Area.Text = overNumber(1)
                            txtMPhone_Oversea_No.Text = overNumber(2)
                        ElseIf overNumber.Length = 2 Then
                            txtMPhone_Oversea_Area.Text = overNumber(0)
                            txtMPhone_Oversea_No.Text = overNumber(1)
                        ElseIf overNumber.Length = 1 Then
                            txtMPhone_Oversea_No.Text = overNumber(0)
                        End If




                        hNumber = Applic_info_reader("EQP_MRESPHONE").ToString.Replace(" ", "-").Split("-")
                        If hNumber.Length = 3 Then
                            txtMHPhone_Country.Text = hNumber(0)
                            txtMHPhone_Area.Text = hNumber(1)
                            txtMHPhone.Text = hNumber(2)
                        ElseIf hNumber.Length = 2 Then
                            txtMHPhone_Area.Text = hNumber(0)
                            txtMHPhone.Text = hNumber(1)
                        ElseIf hNumber.Length = 1 Then
                            txtMHPhone.Text = hNumber(0)
                        End If

                        oNumber = Applic_info_reader("EQP_MOFFPHONE").ToString.Replace(" ", "-").Split("-")
                        If oNumber.Length = 3 Then
                            txtMOPhone_Country.Text = oNumber(0)
                            txtMOPhone_Area.Text = oNumber(1)
                            txtMOPhone.Text = oNumber(2)
                        ElseIf oNumber.Length = 2 Then
                            txtMOPhone_Area.Text = oNumber(0)
                            txtMOPhone.Text = oNumber(1)
                        ElseIf oNumber.Length = 1 Then
                            txtMOPhone.Text = oNumber(0)
                        End If


                        fNumber = Applic_info_reader("EQP_MFAX").ToString.Replace(" ", "-").Split("-")
                        If fNumber.Length = 3 Then
                            txtMFaxNo_country.Text = fNumber(0)
                            txtMFaxNo_Area.Text = fNumber(1)
                            txtMFaxNo.Text = fNumber(2)
                        ElseIf fNumber.Length = 2 Then
                            txtMFaxNo_Area.Text = fNumber(0)
                            txtMFaxNo.Text = fNumber(1)
                        ElseIf fNumber.Length = 1 Then
                            txtMFaxNo.Text = fNumber(0)
                        End If

                        txtMPoboxLocal.Text = Applic_info_reader("EQP_MCOMPOBOX")
                        txtMOccup.Text = Applic_info_reader("EQP_MOCC")
                        txtMComp.Text = Applic_info_reader("EQP_MCOMPANY")
                        If Not ddlMCompany.Items.FindByValue(Applic_info_reader("EQP_MCOMP_ID")) Is Nothing Then
                            ddlMCompany.ClearSelection()
                            ddlMCompany.Items.FindByValue(Applic_info_reader("EQP_MCOMP_ID")).Selected = True
                        End If
                        txtMOverSeas_Add_City.Text = Applic_info_reader("EQP_MPRMCITY")
                        chkMExStud_Gems.Checked = Convert.ToBoolean(Applic_info_reader("EQP_bMGEMSSTAFF"))

                        If chkMExStud_Gems.Checked = True Then

                            txtMExStudName_Gems.Text = Session("ExStudName_Gems")
                            If Not ddlMExYear.Items.FindByValue(Applic_info_reader("EQP_MACD_YEAR")) Is Nothing Then
                                ddlMExYear.ClearSelection()
                                ddlMExYear.Items.FindByValue(Applic_info_reader("EQP_MACD_YEAR")).Selected = True
                            End If
                            If Not ddlMExStud_Gems.Items.FindByValue(Applic_info_reader("EQM_EXUNIT")) Is Nothing Then
                                ddlMExStud_Gems.ClearSelection()
                                ddlMExStud_Gems.Items.FindByValue(Applic_info_reader("EQM_EXUNIT")).Selected = True
                            End If
                        End If

                        chkMStaff_GEMS.Checked = Convert.ToBoolean(Applic_info_reader("EQM_bSTAFFGEMS"))
                        If chkMStaff_GEMS.Checked = True Then
                            If Not ddlMStaff_BSU.Items.FindByValue(Session("STAFFUNIT")) Is Nothing Then
                                ddlMStaff_BSU.ClearSelection()
                                ddlMStaff_BSU.Items.FindByValue(Session("STAFFUNIT")).Selected = True
                            End If
                            txtMStaffID.Text = Session("StaffID_ENQ")
                            txtMStaff_Name.Text = Session("STAFF_NAME")
                        End If


                        'arInfo = Session("str_Aboutus").Trim.Replace("''", "'").Split(splitter)
                        'If arInfo.Length > 0 Then
                        '    For i As Integer = 0 To arInfo.Length - 1
                        '        If arInfo(i).Trim <> "" Then
                        '            chkAboutUs.Items.FindByValue(arInfo(i).Trim).Selected = True
                        '        End If
                        '    Next
                        'End If

                        If Not ddlMEmirate.Items.FindByValue(Applic_info_reader("EQP_MCOMPOBOX_EMIR")) Is Nothing Then
                            ddlMEmirate.ClearSelection()
                            ddlMEmirate.Items.FindByValue(Applic_info_reader("EQP_MCOMPOBOX_EMIR")).Selected = True
                        End If
                        '-------------------GUARDIAN---------------------------------------------

                        If Not ddlGEmirate.Items.FindByValue(Applic_info_reader("EQP_GCOMPOBOX_EMIR")) Is Nothing Then
                            ddlGEmirate.ClearSelection()
                            ddlGEmirate.Items.FindByValue(Applic_info_reader("EQP_GCOMPOBOX_EMIR")).Selected = True
                        End If


                        txtGPri_Fname.Text = Applic_info_reader("EQP_GFIRSTNAME")
                        txtGPri_Mname.Text = Applic_info_reader("EQP_GMIDNAME")
                        txtGPri_Lname.Text = Applic_info_reader("EQP_GLASTNAME")
                        If Not ddlGPri_National1.Items.FindByValue(Applic_info_reader("EQP_GNATIONALITY")) Is Nothing Then
                            ddlGPri_National1.ClearSelection()
                            ddlGPri_National1.Items.FindByValue(Applic_info_reader("EQP_GNATIONALITY")).Selected = True
                        End If
                        If Not ddlGPri_National2.Items.FindByValue(Applic_info_reader("EQP_GNATIONALITY2")) Is Nothing Then
                            ddlGPri_National2.ClearSelection()
                            ddlGPri_National2.Items.FindByValue(Applic_info_reader("EQP_GNATIONALITY2")).Selected = True
                        End If
                        mobNumber = Applic_info_reader("EQP_GMOBILE").ToString.Replace(" ", "-").Split("-")
                        If mobNumber.Length = 3 Then
                            txtGM_Country.Text = mobNumber(0)
                            txtGM_Area.Text = mobNumber(1)
                            txtGMobile_Pri.Text = mobNumber(2)
                        ElseIf mobNumber.Length = 2 Then
                            txtGM_Area.Text = mobNumber(0)
                            txtGMobile_Pri.Text = mobNumber(1)
                        ElseIf mobNumber.Length = 1 Then
                            txtGMobile_Pri.Text = mobNumber(0)
                        End If

                        txtGPoBox_Pri.Text = Applic_info_reader("EQP_GPRMPOBOX")
                        txtGEmail_Pri.Text = Applic_info_reader("EQP_GEMAIL").ToString.ToLower
                        txtGAdd1_overSea.Text = Applic_info_reader("EQP_GPRMADDR1")
                        txtGAdd2_overSea.Text = Applic_info_reader("EQP_GPRMADDR2")


                        If Not ddlGOverSeas_Add_Country.Items.FindByValue(Applic_info_reader("EQP_GPRMCOUNTRY")) Is Nothing Then
                            ddlGOverSeas_Add_Country.ClearSelection()
                            ddlGOverSeas_Add_Country.Items.FindByValue(Applic_info_reader("EQP_GPRMCOUNTRY")).Selected = True
                        End If


                        txtGApartNo.Text = Applic_info_reader("EQP_GCOMAPARTNO")
                        txtGBldg.Text = Applic_info_reader("EQP_GCOMBLDG")
                        txtGStreet.Text = Applic_info_reader("EQP_GCOMSTREET")
                        txtGArea.Text = Applic_info_reader("EQP_GCOMAREA")
                        txtGCity_pri.Text = Applic_info_reader("EQP_GCOMCITY")



                        If Not ddlGCountry_Pri.Items.FindByValue(Applic_info_reader("EQP_GCOMCOUNTRY")) Is Nothing Then
                            ddlGCountry_Pri.ClearSelection()
                            ddlGCountry_Pri.Items.FindByValue(Applic_info_reader("EQP_GCOMCOUNTRY")).Selected = True
                        End If

                        overNumber = Applic_info_reader("EQP_GPRMPHONE").ToString.Replace(" ", "-").Split("-")


                        If overNumber.Length = 3 Then
                            txtGPhone_Oversea_Country.Text = overNumber(0)
                            txtGPhone_Oversea_Area.Text = overNumber(1)
                            txtGPhone_Oversea_No.Text = overNumber(2)
                        ElseIf overNumber.Length = 2 Then
                            txtGPhone_Oversea_Area.Text = overNumber(0)
                            txtGPhone_Oversea_No.Text = overNumber(1)
                        ElseIf overNumber.Length = 1 Then
                            txtGPhone_Oversea_No.Text = overNumber(0)
                        End If




                        hNumber = Applic_info_reader("EQP_GRESPHONE").ToString.Replace(" ", "-").Split("-")
                        If hNumber.Length = 3 Then
                            txtGHPhone_Country.Text = hNumber(0)
                            txtGHPhone_Area.Text = hNumber(1)
                            txtGHPhone.Text = hNumber(2)
                        ElseIf hNumber.Length = 2 Then
                            txtGHPhone_Area.Text = hNumber(0)
                            txtGHPhone.Text = hNumber(1)
                        ElseIf hNumber.Length = 1 Then
                            txtGHPhone.Text = hNumber(0)
                        End If

                        oNumber = Applic_info_reader("EQP_GOFFPHONE").ToString.Replace(" ", "-").Split("-")
                        If oNumber.Length = 3 Then
                            txtGOPhone_Country.Text = oNumber(0)
                            txtGOPhone_Area.Text = oNumber(1)
                            txtGOPhone.Text = oNumber(2)
                        ElseIf oNumber.Length = 2 Then
                            txtGOPhone_Area.Text = oNumber(0)
                            txtGOPhone.Text = oNumber(1)
                        ElseIf oNumber.Length = 1 Then
                            txtGOPhone.Text = oNumber(0)
                        End If


                        fNumber = Applic_info_reader("EQP_GFAX").ToString.Replace(" ", "-").Split("-")
                        If fNumber.Length = 3 Then
                            txtGFaxNo_country.Text = fNumber(0)
                            txtGFaxNo_Area.Text = fNumber(1)
                            txtGFaxNo.Text = fNumber(2)
                        ElseIf fNumber.Length = 2 Then
                            txtGFaxNo_Area.Text = fNumber(0)
                            txtGFaxNo.Text = fNumber(1)
                        ElseIf fNumber.Length = 1 Then
                            txtGFaxNo.Text = fNumber(0)
                        End If

                        txtGPoboxLocal.Text = Applic_info_reader("EQP_GCOMPOBOX")
                        txtGOccup.Text = Applic_info_reader("EQP_GOCC")
                        txtGComp.Text = Applic_info_reader("EQP_GCOMPANY")
                        If Not ddlGCompany.Items.FindByValue(Applic_info_reader("EQP_GCOMP_ID")) Is Nothing Then
                            ddlGCompany.ClearSelection()
                            ddlGCompany.Items.FindByValue(Applic_info_reader("EQP_GCOMP_ID")).Selected = True
                        End If
                        txtGOverSeas_Add_City.Text = Applic_info_reader("EQP_GPRMCITY")


                        'chkGExStud_Gems.Checked = Convert.ToBoolean(Applic_info_reader("EQP_bGGEMSSTAFF"))

                        'If chkGExStud_Gems.Checked = True Then

                        '    txtFExStudName_Gems.Text = Session("ExStudName_Gems")
                        '    If Not ddlFExYear.Items.FindByValue(Applic_info_reader("EQP_GACD_YEAR")) Is Nothing Then
                        '        ddlFExYear.ClearSelection()
                        '        ddlFExYear.Items.FindByValue(Applic_info_reader("EQP_GACD_YEAR")).Selected = True
                        '    End If
                        '    If Not ddlFExStud_Gems.Items.FindByValue(Applic_info_reader("EQM_EXUNIT")) Is Nothing Then
                        '        ddlFExStud_Gems.ClearSelection()
                        '        ddlFExStud_Gems.Items.FindByValue(Applic_info_reader("EQM_EXUNIT")).Selected = True
                        '    End If
                        'End If
                        ' chkFStaff_GEMS.Checked = Convert.ToBoolean(Applic_info_reader("EQM_bSTAFFGEMS"))
                        'If chkFStaff_GEMS.Checked = True Then
                        '    If Not ddlFStaff_BSU.Items.FindByValue(Session("STAFFUNIT")) Is Nothing Then
                        '        ddlFStaff_BSU.ClearSelection()
                        '        ddlFStaff_BSU.Items.FindByValue(Session("STAFFUNIT")).Selected = True
                        '    End If
                        '    txtGStaffID.Text = Session("StaffID_ENQ")
                        '    txtFStaff_Name.Text = Session("STAFF_NAME")
                        'End If

                        If Not Session("str_Aboutus") Is Nothing Then
                            arInfo = Session("str_Aboutus").Trim.Replace("''", "'").Split(splitter)
                            If arInfo.Length > 0 Then
                                For i As Integer = 0 To arInfo.Length - 1
                                    If arInfo(i).Trim <> "" Then
                                        chkAboutUs.Items.FindByValue(arInfo(i).Trim).Selected = True
                                    End If
                                Next
                            End If
                        End If
                    End While

                End Using
            Catch ex As Exception

            End Try


        Else

            lblError.Text = "Error while loading previous Applicant info.Please re-enter Primary Contact Details "
        End If

    End Sub
    Sub AboutUs()
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim AboutUSString As String = "select MODE_ID,MODE_DESCR from ENQUIRY_MODE_M"

        Dim ds1 As New DataSet

        ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, AboutUSString)

        conn.Close()
        Dim row As DataRow
        For Each row In ds1.Tables(0).Rows
            chkAboutUs.Items.Add(New ListItem(row("MODE_DESCR"), row("MODE_ID").ToString.Trim))
        Next
    End Sub
    Sub EnquryOpen_for()
        Dim ACY_ID As String = String.Empty
        Dim CLM_ID As String = String.Empty
        Dim BSU_ID As String = String.Empty
        Dim GRD_ID As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            ACY_ID = ""
        Else
            ACY_ID = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            CLM_ID = ""
        Else
            CLM_ID = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            BSU_ID = ""
        Else
            BSU_ID = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlGrade.SelectedIndex = -1 Then
            GRD_ID = ""
        Else
            GRD_ID = ddlGrade.SelectedItem.Value
        End If
        Dim separator As String() = New String() {"|"}

        Dim varsib_BSU As String = String.Empty
        Dim varstaff_BSU As String = String.Empty
        Dim varsister_BSU As String = String.Empty
        Dim varGEMS_STUD_BSU As String = String.Empty
        Dim varGender_BSU As String = String.Empty
        Dim msg_line1 As String = String.Empty
        Dim msg_line2 As String = String.Empty
        Dim str_conn_New As String = ConnectionManger.GetOASISConnectionString

        Dim BSU_IDs As String = String.Empty
        rdFemale.Enabled = True
        rdMale.Enabled = True

        Using readerEnquryOpen_for As SqlDataReader = AccessStudentClass.GetEnquiry_open_for(GRD_ID, ACY_ID, CLM_ID, BSU_ID)
            If readerEnquryOpen_for.HasRows = True Then
                ViewState("OpenFor") = 1
                While readerEnquryOpen_for.Read

                    If Convert.ToString(readerEnquryOpen_for("EQO_DESC")) = "GEMS Staff" Then
                        ViewState("S_STAFF_GEMS") = Convert.ToString(readerEnquryOpen_for("EQS_BSU_IDS"))
                        varstaff_BSU = Convert.ToString(readerEnquryOpen_for("EQO_DESC"))
                        msg_line1 = Convert.ToString(readerEnquryOpen_for("EQS_MSG1"))
                        msg_line2 = Convert.ToString(readerEnquryOpen_for("EQS_MSG2"))

                        Dim strSplitArr As String() = ViewState("S_STAFF_GEMS").Split(separator, StringSplitOptions.RemoveEmptyEntries)

                        For Each arrStr As String In strSplitArr
                            BSU_IDs += "'" & arrStr & "'" & ","
                        Next
                        BSU_IDs = BSU_IDs.TrimEnd(",")
                        BSU_IDs = BSU_IDs.TrimStart(",")
                        Call OpenStaff_BSU_Populate(BSU_IDs)
                    End If

                    If Convert.ToString(readerEnquryOpen_for("EQO_DESC")) = "Sibling" Then
                        ViewState("S_GEMS_SIBIL") = Convert.ToString(readerEnquryOpen_for("EQS_BSU_IDS"))
                        varsib_BSU = Convert.ToString(readerEnquryOpen_for("EQO_DESC"))
                        msg_line1 = Convert.ToString(readerEnquryOpen_for("EQS_MSG1"))
                        msg_line2 = Convert.ToString(readerEnquryOpen_for("EQS_MSG2"))
                        Dim strSplitArr As String() = ViewState("S_GEMS_SIBIL").Split(separator, StringSplitOptions.RemoveEmptyEntries)

                        For Each arrStr As String In strSplitArr
                            BSU_IDs += "'" & arrStr & "'" & ","

                        Next
                        BSU_IDs = BSU_IDs.TrimEnd(",")
                        BSU_IDs = BSU_IDs.TrimStart(",")
                        Call OpenSib_BSU_Populate(BSU_IDs)
                        ddlSib_BSU.Enabled = True
                    End If


                    If Convert.ToString(readerEnquryOpen_for("EQO_DESC")) = "GEMS Student" Then
                        ViewState("S_GEMS_STUD") = Convert.ToString(readerEnquryOpen_for("EQS_BSU_IDS"))
                        varGEMS_STUD_BSU = Convert.ToString(readerEnquryOpen_for("EQO_DESC"))
                        msg_line1 = Convert.ToString(readerEnquryOpen_for("EQS_MSG1"))
                        msg_line2 = Convert.ToString(readerEnquryOpen_for("EQS_MSG2"))
                        Dim strSplitArr As String() = ViewState("S_GEMS_STUD").Split(separator, StringSplitOptions.RemoveEmptyEntries)

                        For Each arrStr As String In strSplitArr
                            BSU_IDs += "'" & arrStr & "'" & ","

                        Next
                        BSU_IDs = BSU_IDs.TrimEnd(",")
                        BSU_IDs = BSU_IDs.TrimStart(",")
                        Call OpenGEMS_STUD_BSU_Populate(BSU_IDs)
                    End If

                    If Convert.ToString(readerEnquryOpen_for("EQO_DESC")) = "Female Applicant" Then
                        ViewState("S_GENDER") = Convert.ToString(readerEnquryOpen_for("EQS_BSU_IDS"))
                        varGender_BSU = Convert.ToString(readerEnquryOpen_for("EQO_DESC"))
                        msg_line1 = Convert.ToString(readerEnquryOpen_for("EQS_MSG1"))
                        msg_line2 = Convert.ToString(readerEnquryOpen_for("EQS_MSG2"))
                        rdFemale.Enabled = False
                        rdMale.Enabled = False
                        rdFemale.Checked = True
                        rdMale.Checked = False
                    ElseIf Convert.ToString(readerEnquryOpen_for("EQO_DESC")) = "Male Applicant" Then
                        ViewState("S_GENDER") = Convert.ToString(readerEnquryOpen_for("EQS_BSU_IDS"))
                        varGender_BSU = Convert.ToString(readerEnquryOpen_for("EQO_DESC"))
                        msg_line1 = Convert.ToString(readerEnquryOpen_for("EQS_MSG1"))
                        msg_line2 = Convert.ToString(readerEnquryOpen_for("EQS_MSG2"))
                        rdFemale.Enabled = False
                        rdMale.Enabled = False
                        rdMale.Checked = True
                        rdFemale.Checked = False
                    End If

                End While
            Else
                ViewState("OpenFor") = 0

                Call GetBusinessUnits_info_student()
                If Not ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedValue) Is Nothing Then
                    ddlSib_BSU.ClearSelection()
                    ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedValue).Selected = True
                    ddlSib_BSU.Enabled = True
                Else
                    ddlSib_BSU.Enabled = True
                End If
            End If

            Dim strOpenFor As String = String.Empty
            Dim strDetail As String = String.Empty
            Dim orCheck As Integer = 0
            If varstaff_BSU <> "" Then
                orCheck = 1
                ViewState("V_STAFF") = varstaff_BSU
                strDetail = "Staff ID(in Primary Contact)"
                strOpenFor = varstaff_BSU & ","
            Else
                ViewState("V_STAFF") = ""
                ViewState("S_STAFF_GEMS") = ""
            End If

            If varsib_BSU <> "" Then
                orCheck += 1
                ViewState("V_SIB") = varsib_BSU
                strOpenFor += varsib_BSU & ","
                If orCheck > 1 Then
                    strDetail += " or Sibling Fee ID(in Student Details)"
                Else
                    strDetail += "Sibling Fee ID(in Student Details)"
                End If
            Else
                ViewState("V_SIB") = ""
                ViewState("S_GEMS_SIBIL") = ""

            End If

            If varGEMS_STUD_BSU <> "" Then
                orCheck += 1
                ViewState("V_GEMS_STU") = varGEMS_STUD_BSU
                strOpenFor += varGEMS_STUD_BSU & ","
                If orCheck > 1 Then
                    strDetail += " or Student Id(in Current School Details)"
                Else
                    strDetail += "Student Id(in Current School Details)"
                End If
            Else
                ViewState("V_GEMS_STU") = ""
                ViewState("S_GEMS_STUD") = ""
            End If
            If varsister_BSU <> "" Then
                orCheck += 1
                ViewState("V_SISTER") = varsister_BSU
                strOpenFor += varsister_BSU & ","
                If orCheck > 1 Then
                    strDetail += " or School Name(in Current School)"
                Else
                    strDetail += "School Name(in Current School)"
                End If
            Else
                ViewState("V_SISTER") = ""
                ViewState("S_SISTER_GEMS") = ""
            End If

            If varGender_BSU <> "" Then
                orCheck += 1
                ViewState("V_GENDER") = varGender_BSU
                strOpenFor += varGender_BSU & ","
                If orCheck > 1 Then
                    strDetail += " or Valid Gender(in Applicant Info)"
                Else
                    strDetail += "Valid Gender(in Applicant Info)"
                End If
            Else
                ViewState("V_GENDER") = ""
                ViewState("S_GENDER") = ""
            End If







            ViewState("orCheck") = orCheck
            strOpenFor = strOpenFor.TrimEnd(",")
            strOpenFor = strOpenFor.TrimStart(",")
            strDetail = strDetail.TrimStart(",")
            strDetail = strDetail.TrimEnd(",")

            Dim flag As Integer = 0
            If varstaff_BSU <> "" Then
                flag = 1
            ElseIf varsib_BSU <> "" Then
                flag = 1
            ElseIf varsister_BSU <> "" Then
                flag = 1
            ElseIf varGEMS_STUD_BSU <> "" Then
                flag = 1
            ElseIf varGender_BSU <> "" Then
                flag = 1
            Else
                flag = 0
            End If
            If flag = 1 Then

                'If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
                If ViewState("ENQTYPE") = "R" Then
                    ltMessage.Visible = False
                Else
                    ltMessage.Visible = True
                    ltMessage.Text = "<marquee direction='left'  behavior='SCROLL' scrolldelay='180' VSPACE='5'><font color='#1B80B6' size=1px' face='Verdana, Times New Roman'><FONT COLOR='RED' >*</FONT><B> " & msg_line1 & "</B><div><I> &nbsp;&nbsp;" & msg_line2 & " </I></div></font></marquee>"
                    ViewState("ERROR_DETAIL") = msg_line2
                End If

            Else
                ltMessage.Visible = False
            End If

            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            pParms(1) = New SqlClient.SqlParameter("@ACY_ID", ACY_ID)
            pParms(2) = New SqlClient.SqlParameter("@CLM_ID", CLM_ID)

            Using reader_EQS As SqlDataReader = SqlHelper.ExecuteReader(str_conn_New, CommandType.StoredProcedure, "Get_ENQMARQUEE", pParms)
                While reader_EQS.Read
                    Dim lstrEQS_DESCR = Convert.ToString(reader_EQS("EQS_DESCR"))
                    ltMessage.Visible = True
                    If lstrEQS_DESCR <> "" Then
                        ltMessage.Text = "<marquee direction='left'  behavior='SCROLL' scrolldelay='180' VSPACE='5'><font color='#1B80B6' size=1px' face='Verdana, Times New Roman'><FONT COLOR='RED' >*</FONT><B> " & lstrEQS_DESCR & "</B><div><I> &nbsp;&nbsp;" & msg_line2 & " </I></div></font></marquee>"
                    End If
                End While
            End Using

        End Using
    End Sub
    Function Status_VW_ACAD() As Integer
        Try
            Dim VW_ACAD As Integer = 0

            'And ViewState("TERM_Valid") = "1"
            CheckTermDate()
            If ViewState("Acd_Valid") = "1" Then

                If ddlAca_Year.SelectedIndex = -1 Or ddlGrade.SelectedIndex = -1 Or ddlCurri.SelectedIndex = -1 Or ddlShift.SelectedIndex = -1 Then   'And ddlTerm.Items.Count > 0 
                    VW_ACAD = -1 'not completed

                Else
                    ViewState("Table_row") = ddlGrade.SelectedItem.Value
                    Call ControlTable_row()
                    VW_ACAD = 1 'completed
                End If
            End If
            'code modified by lijo on 9/06/08
            If ViewState("Acd_Valid") = "1" Then
                If txtDOJ.Text.Trim <> "" Then
                    Dim strfDate As String = txtDOJ.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        VW_ACAD = -1
                        lblError.Text = "Invalid tentative date of join"
                    Else
                        txtDOJ.Text = strfDate
                        Dim dateTime1 As String
                        dateTime1 = Date.ParseExact(txtDOJ.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If Not IsDate(dateTime1) Then
                            VW_ACAD = -1
                            lblError.Text = "Invalid tentative date of join"
                        Else
                            Dim TRM_SDate As Date = ViewState("TRM_STARTDT")
                            Dim TRM_EDate As Date = ViewState("TRM_ENDDT")
                            Dim TENT_date As Date = txtDOJ.Text
                            If Not (TENT_date >= TRM_SDate And TENT_date <= TRM_EDate) Then
                                VW_ACAD = -1
                                lblError.Text = "Tentative date of join  must be with in the Term Date"
                            End If
                        End If
                    End If
                End If
            Else
                VW_ACAD = -1 'not completed
            End If


            Return VW_ACAD
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Status_VW_ACAD")
            Status_VW_ACAD = -1
        End Try
    End Function
    Sub callAll_fun_OnPageLoad()
        Try
            Call Populate_YearList()
            Call GetBusinessUnits_info_student()
            Call GetPRENURSERY()
            Call GetBusinessUnits_info_staff()
            Call GetReligion_info()
            'Call GetCity_info()
            Call GetCountry_info()
            Call GetNational_info()
            'Call GetTelePhone_info_Mobile("M")
            'Call GetTelePhone_info("L")
            Call GetCurriculum_Info()
            Call GetGrade_info()
            Call GetMain_Loc()
            Call GetSub_Main_Loc()
            Call GetSub_PickUp_point()
            Call GetCompany_Name()
            Call GetEmirate_info()
            Call bindLanguage_dropDown()
            Call AboutUs()
            Call bindETHNICITY()
            If Not ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedValue) Is Nothing Then
                ddlSib_BSU.ClearSelection()
                ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedValue).Selected = True
                ddlSib_BSU.Enabled = True
            Else
                ddlSib_BSU.Enabled = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "callAll_fun_OnPageLoad")
        End Try

    End Sub
    Private Sub bindETHNICITY()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select EC_ID,EC_NAME from dbo.ETHNICITY_M order by EC_NAME"
        Dim sqlread As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)

        If sqlread.HasRows = True Then
            ddlEthnicity.DataSource = sqlread
            ddlEthnicity.DataTextField = "EC_NAME"
            ddlEthnicity.DataValueField = "EC_ID"
            ddlEthnicity.DataBind()
        End If
        ddlEthnicity.Items.Add(New ListItem("Other", "0"))
        ddlEthnicity.ClearSelection()
        ddlEthnicity.Items.FindByText("Other").Selected = True
    End Sub

    Private Sub bindLanguage_dropDown()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT LNG_ID,LNG_DESCR  FROM LANGUAGE_M order by LNG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        ddlFLang.DataSource = ds
        ddlFLang.DataTextField = "LNG_DESCR"
        ddlFLang.DataValueField = "LNG_ID"
        ddlFLang.DataBind()
        chkOLang.DataSource = ds
        chkOLang.DataTextField = "LNG_DESCR"
        chkOLang.DataValueField = "LNG_ID"
        chkOLang.DataBind()


        ddlFLang.Items.Add(New ListItem("", ""))
        ddlFLang.ClearSelection()
        ddlFLang.Items.FindByText("").Selected = True



    End Sub
    Sub GetCompany_Name()
        Try

            Using GetComp_Name_reader As SqlDataReader = AccessStudentClass.GetCompany_Name()

                ddlFCompany.Items.Clear()
                ddlFCompany.Items.Add(New ListItem("Other", "0"))
                ddlMCompany.Items.Clear()
                ddlMCompany.Items.Add(New ListItem("Other", "0"))
                ddlGCompany.Items.Clear()
                ddlGCompany.Items.Add(New ListItem("Other", "0"))

                If GetComp_Name_reader.HasRows = True Then
                    While GetComp_Name_reader.Read
                        ddlFCompany.Items.Add(New ListItem(GetComp_Name_reader("comp_Name"), GetComp_Name_reader("comp_ID")))
                        ddlMCompany.Items.Add(New ListItem(GetComp_Name_reader("comp_Name"), GetComp_Name_reader("comp_ID")))
                        ddlGCompany.Items.Add(New ListItem(GetComp_Name_reader("comp_Name"), GetComp_Name_reader("comp_ID")))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCompany_Name()")
        End Try
    End Sub
    'Getting Business Unit info
    Sub GetBusinessUnits_info_staff()
        Try
            Dim str_query As String = " exec GetSchool_list_staff"
            Dim conn As String = ConnectionManger.GetOASISConnectionString

            Using AllStaff_BSU_reader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_query)
                ddlFStaff_BSU.Items.Clear()
                ddlMStaff_BSU.Items.Clear()

                If AllStaff_BSU_reader.HasRows = True Then
                    While AllStaff_BSU_reader.Read
                        ddlFStaff_BSU.Items.Add(New ListItem(AllStaff_BSU_reader("bsu_name"), AllStaff_BSU_reader("bsu_id")))
                        ddlMStaff_BSU.Items.Add(New ListItem(AllStaff_BSU_reader("bsu_name"), AllStaff_BSU_reader("bsu_id")))

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_staff()")
        End Try
    End Sub
    'Getting Religion info
    'code modified on 12/AUG/2008
    Sub GetReligion_info()
        Try
            Dim BSU_ID As String = String.Empty
            If ddlGEMSSchool.SelectedIndex <> -1 Then

                BSU_ID = ddlGEMSSchool.SelectedItem.Value
            Else
                BSU_ID = ""
            End If

            Using AllReligion_reader As SqlDataReader = AccessStudentClass.GetStud_BSU_Religion(BSU_ID)

                Dim di_Religion As ListItem
                ddlReligion.Items.Clear()
                ddlReligion.Items.Add(New ListItem("", ""))
                If AllReligion_reader.HasRows = True Then
                    While AllReligion_reader.Read

                        ddlReligion.Items.Add(New ListItem(AllReligion_reader("RLG_DESCR"), AllReligion_reader("RLG_ID")))

                    End While
                Else
                    di_Religion = New ListItem("", "")
                    ddlReligion.Items.Add(di_Religion)
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetReligion_info")
        End Try
    End Sub
    Sub GetCountry_info()
        Try


            Using AllCountry_reader As SqlDataReader = AccessRoleUser.GetCountry()
                Dim di_Country As ListItem
                ddlCountry.Items.Clear()
                ddlPre_Country.Items.Clear()
                ddlFCountry_Pri.Items.Clear()
                ddlFOverSeas_Add_Country.Items.Clear()

                ddlMCountry_Pri.Items.Clear()
                ddlMOverSeas_Add_Country.Items.Clear()

                ddlGCountry_Pri.Items.Clear()
                ddlGOverSeas_Add_Country.Items.Clear()

                ddlPre_Country.Items.Add(New ListItem("", ""))
                ddlCountry.Items.Add(New ListItem("", ""))
                ddlFCountry_Pri.Items.Add(New ListItem("", ""))
                ddlFOverSeas_Add_Country.Items.Add(New ListItem("", ""))

                ddlMCountry_Pri.Items.Add(New ListItem("", ""))
                ddlMOverSeas_Add_Country.Items.Add(New ListItem("", ""))


                ddlGCountry_Pri.Items.Add(New ListItem("", ""))
                ddlGOverSeas_Add_Country.Items.Add(New ListItem("", ""))

                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID"))

                        ddlPre_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlFCountry_Pri.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlFOverSeas_Add_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlMCountry_Pri.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlMOverSeas_Add_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlGCountry_Pri.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlGOverSeas_Add_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))


                    End While
                End If

                ddlFCountry_Pri.ClearSelection()
                ddlFCountry_Pri.Items.FindByValue("172").Selected = True
                ddlMCountry_Pri.ClearSelection()
                ddlMCountry_Pri.Items.FindByValue("172").Selected = True
                ddlGCountry_Pri.ClearSelection()
                ddlGCountry_Pri.Items.FindByValue("172").Selected = True

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub
    'Getting national info
    Sub GetNational_info()
        Try
            Using AllNational_reader As SqlDataReader = AccessStudentClass.GetNational()
                Dim di_National As ListItem
                ddlFPri_National1.Items.Clear()
                ddlFPri_National2.Items.Clear()
                ddlMPri_National1.Items.Clear()
                ddlMPri_National2.Items.Clear()
                ddlGPri_National1.Items.Clear()
                ddlGPri_National2.Items.Clear()


                ddlNational.Items.Clear()



                ddlFPri_National1.Items.Add(New ListItem("", ""))
                ddlFPri_National2.Items.Add(New ListItem("", ""))
                ddlMPri_National1.Items.Add(New ListItem("", ""))
                ddlMPri_National2.Items.Add(New ListItem("", ""))
                ddlGPri_National1.Items.Add(New ListItem("", ""))
                ddlGPri_National2.Items.Add(New ListItem("", ""))
                ddlNational.Items.Add(New ListItem("", ""))


                If AllNational_reader.HasRows = True Then
                    While AllNational_reader.Read()

                        ddlFPri_National1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlFPri_National2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlMPri_National1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlMPri_National2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlGPri_National1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlGPri_National2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))


                        ddlNational.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetNational_info")
        End Try
    End Sub
    ' Getting Emirates info
    Sub GetEmirate_info()
        Try
            Using AllEmirate_reader As SqlDataReader = AccessStudentClass.GetEmirate()

                ddlFEmirate.Items.Clear()
                ddlMEmirate.Items.Clear()
                ddlGEmirate.Items.Clear()
                If AllEmirate_reader.HasRows = True Then
                    While AllEmirate_reader.Read

                        ddlFEmirate.Items.Add(New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE")))
                        ddlMEmirate.Items.Add(New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE")))
                        ddlGEmirate.Items.Add(New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE")))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmirate_info")
        End Try
    End Sub
    Sub GetCurriculum_Info()
        Try


            Using AllCurriculum_reader As SqlDataReader = AccessStudentClass.GetCURRICULUM_M()

                Dim di_Curriculum As ListItem
                ddlPre_Curriculum.Items.Clear()

                If AllCurriculum_reader.HasRows = True Then
                    While AllCurriculum_reader.Read
                        di_Curriculum = New ListItem(AllCurriculum_reader("CLM_DESCR"), AllCurriculum_reader("CLM_ID"))
                        ddlPre_Curriculum.Items.Add(di_Curriculum)

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_staff()")
        End Try
    End Sub
    'Getting Grade Info
    Sub GetPRENURSERY()
        'ddlPreSchool_Nursery
        Try
            ' Dim defaultNursery As String = "--"


            Dim di As ListItem
            Using PRENURSERYreader As SqlDataReader = AccessStudentClass.GetPRENURSERY_M()
                ddlPreSchool_Nursery.Items.Clear()


                If PRENURSERYreader.HasRows = True Then
                    While PRENURSERYreader.Read
                        di = New ListItem(PRENURSERYreader("SCH_DESCR"), PRENURSERYreader("SCH_CODE"))
                        ddlPreSchool_Nursery.Items.Add(di)
                    End While
                End If
            End Using

            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlPreSchool_Nursery.Items.Count - 1
                If ddlPreSchool_Nursery.Items(ItemTypeCounter).Value = "4" Then
                    ddlPreSchool_Nursery.SelectedIndex = ItemTypeCounter
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetPRENURSERY()")
        End Try





    End Sub
    'SCH_CODE,SCH_DESCR
    Sub GetGrade_info()
        Try


            Using AllGrade_reader As SqlDataReader = AccessStudentClass.GetGrade_M()

                'Dim di_Grade As ListItem
                ddlPre_Grade.Items.Clear()
                If AllGrade_reader.HasRows = True Then
                    While AllGrade_reader.Read
                        ' di_Grade = New ListItem(AllGrade_reader("GRD_ID"), AllGrade_reader("GRD_ID"))
                        ddlPre_Grade.Items.Add(New ListItem(AllGrade_reader("GRD_ID"), AllGrade_reader("GRD_ID")))

                    End While
                End If
            End Using


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetGrade_info")
        End Try
    End Sub
    Sub GetMain_Loc()
        Try

            Dim GEMSSchool As String = String.Empty

            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If

            Using Main_Loc_reader As SqlDataReader = AccessStudentClass.getMain_Location(GEMSSchool)
                Dim di_Main_Loc As ListItem
                ddlMainLocation.Items.Clear()
                If Main_Loc_reader.HasRows = True Then
                    While Main_Loc_reader.Read
                        di_Main_Loc = New ListItem(Main_Loc_reader("LOC_DESCRIPTION"), Main_Loc_reader("LOC_ID"))
                        ddlMainLocation.Items.Add(di_Main_Loc)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetMain_Loc")

        End Try
    End Sub
    Sub GetSub_Main_Loc()
        Try
            Dim MainLocation As String = String.Empty

            Dim GEMSSchool As String = String.Empty

            If ddlMainLocation.SelectedIndex = -1 Then
                MainLocation = ""
            Else
                MainLocation = ddlMainLocation.SelectedItem.Value
            End If

            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If


            Using Sub_Main_Loc_reader As SqlDataReader = AccessStudentClass.getSubMain_Location(GEMSSchool, MainLocation)
                Dim di_Sub_Main_Loc As ListItem
                ddlSubLocation.Items.Clear()
                If Sub_Main_Loc_reader.HasRows = True Then
                    While Sub_Main_Loc_reader.Read
                        di_Sub_Main_Loc = New ListItem(Sub_Main_Loc_reader("SBL_DESCRIPTION"), Sub_Main_Loc_reader("SBL_ID"))
                        ddlSubLocation.Items.Add(di_Sub_Main_Loc)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetSub_Main_Loc")

        End Try
    End Sub
    Sub GetSub_PickUp_point()
        Try

            Dim SubLocation As String = String.Empty

            Dim GEMSSchool As String = String.Empty

            If ddlSubLocation.SelectedIndex = -1 Then
                SubLocation = ""
            Else
                SubLocation = ddlSubLocation.SelectedItem.Value
            End If

            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If


            Using PickUp_point_reader As SqlDataReader = AccessStudentClass.getPickUP_Points(GEMSSchool, SubLocation)
                Dim di_PickUp As ListItem
                ddlPickup.Items.Clear()
                ' di_PickUp = New ListItem("Other", "")
                ' ddlPickup.Items.Add(di_PickUp)
                If PickUp_point_reader.HasRows = True Then
                    While PickUp_point_reader.Read
                        di_PickUp = New ListItem(PickUp_point_reader("PNT_DESCRIPTION"), PickUp_point_reader("PNT_ID"))
                        ddlPickup.Items.Add(di_PickUp)
                    End While
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Sub_PickUp_point")

        End Try
    End Sub
    Sub ControlTable_row()
        If (ViewState("Table_row") = "KG1") Or (ViewState("Table_row") = "PK") Or (ViewState("Table_row") = "PN1") Then
            'rdGemsGr.Checked = True
            ' ddlPreSchool_Nursery.SelectedIndex = 0
            ViewState("TABLE_School_Pre").Rows.Clear()
            ltSchoolType.Text = "Current School"

            ViewState("id") = 1
            gridbind()
            trSch1.Visible = True
            trSch2.Visible = False
            trSch3.Visible = False
            trSch4.Visible = False
            trSch5.Visible = False
            trSch6.Visible = False
            trSch7.Visible = False
            trSch8.Visible = False
            trSch9.Visible = False
            trSch10.Visible = False
            trSch11.Visible = False
        Else
            '  ddlGemsGr_SelectedIndexChanged(ddlGemsGr, Nothing)

            ddlGemsSchools.Visible = True
            'ddlGemsGr.ClearSelection()
            If Not ddlPre_Grade.Items.FindByText(GetPrevGrade()) Is Nothing Then
                ddlPre_Grade.ClearSelection()
                ddlPre_Grade.Items.FindByText(GetPrevGrade()).Selected = True
            End If
            '' txtPre_School.Text = ""
            'txtPre_School.Visible = False
            'rfv_Pre_school.Visible = False



            ViewState("rowstate") = "1"
            trSch1.Visible = False
            trSch2.Visible = True
            trSch3.Visible = True
            trSch4.Visible = True
            trSch5.Visible = True
            trSch6.Visible = True
            trSch7.Visible = True
            trSch8.Visible = True
            trSch9.Visible = True
            trSch10.Visible = True
            trSch11.Visible = True
            '  rfvFeeID_App.Visible = False

            ' If (Request.QueryString("MainMnu_code")) Is Nothing Then
            If ViewState("ENQTYPE") = "O" Then
                If ViewState("V_GEMS_STU") <> "" Then
                    If ViewState("orCheck") >= 1 Then

                        ''r1.Visible = False
                        ''r3.Visible = False
                        ''r12.Visible = True
                        ViewState("rowstate") = "2"
                        ' rfvFeeID_App.Visible = True
                    End If
                End If
            End If


        End If
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ' Call GetTerm_4ACD(ddlAca_Year.SelectedValue, ddlGrade.SelectedValue)
        'check if grade is open or not
        ViewState("Table_row") = ddlGrade.SelectedItem.Value

        If "PK|KG1".Contains(ddlGrade.SelectedValue) Then
            currPre_SchTitle.InnerText = "Current School Details"
        Else
            currPre_SchTitle.InnerText = "Current /Previous School Details"
        End If


        Call ControlTable_row()
        'fifth shift is called
        Call GetAllGrade_Shift()

        Call EnquryOpen_for()

        Call GetSchoolButton_Enable()



    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        'forth Grade is called
        Call GetAllGrade_Bsu()
        Call GetTerm_4ACD()
        '  Call ControlTable_row()
        Call GetSchoolButton_Enable()
        ' Call GEMS_BSU_AcadStart()

    End Sub
    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged

        Dim trm_ID As String = String.Empty

        If ddlTerm.SelectedIndex = -1 Then
            trm_ID = ""
        Else
            trm_ID = ddlTerm.SelectedItem.Value
        End If
        'Call GEMS_BSU_AcadStart()
        If Not Session("termSTDT_online") Is Nothing Then
            If ddlTerm.SelectedIndex <> -1 Then
                txtDOJ.Text = Session("termSTDT_online")(ddlTerm.SelectedIndex)
            End If
        End If

        ' Dim sdate As String = AccessStudentClass.GetTermDate(trm_ID)
        ' txtDOJ.Text = ""
        'txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
    End Sub
    Protected Sub mnuRegMaster_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles mnuRegMaster.MenuItemClick
        Dim str_error_cond As String = String.Empty
        Try

            mvRegMain.ActiveViewIndex = Int32.Parse(e.Item.Value)

            If mvRegMain.ActiveViewIndex = 1 Then
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                               "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                                "<script language=javascript>fill_Tran();</script>")

            End If

            If mvRegMain.ActiveViewIndex = 2 Then
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                               "<script language=javascript>fill_Staffs();</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                               "<script language=javascript>fill_Studs();</script>")


                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                            "<script language=javascript>fill_MStaffs();</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                               "<script language=javascript>fill_MStuds();</script>")

            End If




            ViewState("VW_ACAD") = Status_VW_ACAD()

            If ViewState("VW_ACAD") = "-1" Or ViewState("VW_ACAD") Is Nothing Then
                mnuRegMaster.Items(0).ImageUrl = "~/Images/FormRegImg/Schools_1.jpg"
                divRef.Visible = False
                maintab.Visible = True
            ElseIf ViewState("VW_ACAD") = "1" Then
                divRef.Visible = False
                maintab.Visible = True
                btnVwAcad_Next.Enabled = True

                mnuRegMaster.Items(0).ImageUrl = "~/Images/FormRegImg/Schools_3.jpg"
            End If

            If ViewState("Valid_Appl") = "1" Then
                mnuRegMaster.Items(1).ImageUrl = "~/Images/FormRegImg/APPLICANT_3.jpg"
            ElseIf ViewState("Valid_Appl") = "-1" Or ViewState("Valid_Appl") Is Nothing Then
                mnuRegMaster.Items(1).ImageUrl = "~/Images/FormRegImg/APPLICANT_1.jpg"
            End If
            If ViewState("Valid_Prim_cont") = "1" Then
                mnuRegMaster.Items(2).ImageUrl = "~/Images/FormRegImg/PRIMARY_3.jpg"
            ElseIf ViewState("Valid_Prim_cont") = "-1" Or ViewState("Valid_Prim_cont") Is Nothing Then
                mnuRegMaster.Items(2).ImageUrl = "~/Images/FormRegImg/PRIMARY_1.jpg"
            End If
            If ViewState("Valid_Prev_school") = "1" Then
                mnuRegMaster.Items(3).ImageUrl = "~/Images/FormRegImg/PREVIOUS_3.jpg"

            ElseIf ViewState("Valid_Prev_school") = "-1" Or ViewState("Valid_Prev_school") Is Nothing Then

                mnuRegMaster.Items(3).ImageUrl = "~/Images/FormRegImg/PREVIOUS_1.jpg"
            End If
            If Valid_Health() Then
                mnuRegMaster.Items(4).ImageUrl = "~/Images/FormRegImg/OtherDetails_3.jpg"
            Else
                mnuRegMaster.Items(4).ImageUrl = "~/Images/FormRegImg/OtherDetails_1.jpg"
            End If


            If Valid_Declaration() Then

                divConf.Visible = False
                divDecl.Visible = True
                mnuRegMaster.Items(5).ImageUrl = "~/Images/FormRegImg/Declaration_3.jpg"
            Else
                mnuRegMaster.Items(5).ImageUrl = "~/Images/FormRegImg/Declaration_1.jpg"
                divConf.Visible = False
                divDecl.Visible = True
            End If

            Select Case e.Item.Value
                Case 0
                    hf_status.Value = 0
                    mnuRegMaster.Items(0).ImageUrl = "~/Images/FormRegImg/Schools_2.jpg"
                Case 1
                    hf_status.Value = 0
                    mnuRegMaster.Items(1).ImageUrl = "~/Images/FormRegImg/APPLICANT_2.jpg"
                Case 2
                    hf_status.Value = 1
                    mnuRegMaster.Items(2).ImageUrl = "~/Images/FormRegImg/PRIMARY_2.jpg"
                Case 3
                    hf_status.Value = 0
                    mnuRegMaster.Items(3).ImageUrl = "~/Images/FormRegImg/PREVIOUS_2.jpg"

                Case 4
                    hf_status.Value = 0
                    mnuRegMaster.Items(4).ImageUrl = "~/Images/FormRegImg/OtherDetails_2.jpg"
                Case 5
                    hf_status.Value = 0
                    mnuRegMaster.Items(5).ImageUrl = "~/Images/FormRegImg/Declaration_2.jpg"
            End Select
            Call Enable_Rem_MenuList()

            If ddlGEMSSchool.SelectedIndex <> -1 Then
                str_error_cond = str_error_cond + ddlGEMSSchool.SelectedItem.Text
            End If
            If ddlGrade.SelectedIndex <> -1 Then
                str_error_cond = str_error_cond + "Grade: " + ddlGrade.SelectedItem.Text
            End If

            str_error_cond = str_error_cond '+ ViewState("btnStateCheck")


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "mnuRegMaster_MenuItemClick" & str_error_cond)
        End Try
    End Sub
    Sub mnuReg_Select(ByVal Mindex As Integer)
        Dim str_error_cond As String = String.Empty
        Try
            If Mindex = 1 Then
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                               "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                              "<script language=javascript>fill_Tran();</script>")
            End If


            If mvRegMain.ActiveViewIndex = 2 Then
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                               "<script language=javascript>fill_Staffs();</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                               "<script language=javascript>fill_Studs();</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                            "<script language=javascript>fill_MStaffs();</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                               "<script language=javascript>fill_MStuds();</script>")

            End If

            mvRegMain.ActiveViewIndex = Mindex
            If ViewState("VW_ACAD") = "-1" Or ViewState("VW_ACAD") Is Nothing Then
                mnuRegMaster.Items(0).ImageUrl = "~/Images/FormRegImg/Schools_1.jpg"
            ElseIf ViewState("VW_ACAD") = "1" Then
                btnVwAcad_Next.Enabled = True
                mnuRegMaster.Items(0).ImageUrl = "~/Images/FormRegImg/Schools_3.jpg"
            End If

            If ViewState("Valid_Appl") = "1" Then
                mnuRegMaster.Items(1).ImageUrl = "~/Images/FormRegImg/APPLICANT_3.jpg"
            ElseIf ViewState("Valid_Appl") = "-1" Or ViewState("Valid_Appl") Is Nothing Then
                mnuRegMaster.Items(1).ImageUrl = "~/Images/FormRegImg/APPLICANT_1.jpg"
            End If
            If ViewState("Valid_Prim_cont") = "1" Then
                mnuRegMaster.Items(2).ImageUrl = "~/Images/FormRegImg/PRIMARY_3.jpg"
            ElseIf ViewState("Valid_Prim_cont") = "-1" Or ViewState("Valid_Prim_cont") Is Nothing Then
                mnuRegMaster.Items(2).ImageUrl = "~/Images/FormRegImg/PRIMARY_1.jpg"
            End If


            If ViewState("Valid_Prev_school") = "1" Then
                mnuRegMaster.Items(3).ImageUrl = "~/Images/FormRegImg/PREVIOUS_3.jpg"

            ElseIf ViewState("Valid_Prev_school") = "-1" Or ViewState("Valid_Prev_school") Is Nothing Then
                mnuRegMaster.Items(3).ImageUrl = "~/Images/FormRegImg/PREVIOUS_1.jpg"

            End If


            If Valid_Health() Then
                mnuRegMaster.Items(4).ImageUrl = "~/Images/FormRegImg/OtherDetails_3.jpg"
            Else
                mnuRegMaster.Items(4).ImageUrl = "~/Images/FormRegImg/OtherDetails_1.jpg"
            End If

            If Valid_Declaration() Then
                mnuRegMaster.Items(5).ImageUrl = "~/Images/FormRegImg/Declaration_3.jpg"
            Else
                mnuRegMaster.Items(5).ImageUrl = "~/Images/FormRegImg/Declaration_1.jpg"
            End If

            Select Case Mindex

                Case 0
                    hf_status.Value = 0
                    mnuRegMaster.Items(0).ImageUrl = "~/Images/FormRegImg/Schools_2.jpg"
                Case 1
                    hf_status.Value = 0
                    mnuRegMaster.Items(1).ImageUrl = "~/Images/FormRegImg/APPLICANT_2.jpg"
                Case 2
                    hf_status.Value = 1
                    mnuRegMaster.Items(2).ImageUrl = "~/Images/FormRegImg/PRIMARY_2.jpg"
                Case 3
                    hf_status.Value = 0
                    mnuRegMaster.Items(3).ImageUrl = "~/Images/FormRegImg/PREVIOUS_2.jpg"

                Case 4
                    hf_status.Value = 0
                    mnuRegMaster.Items(4).ImageUrl = "~/Images/FormRegImg/OtherDetails_2.jpg"
                Case 5
                    hf_status.Value = 0
                    mnuRegMaster.Items(5).ImageUrl = "~/Images/FormRegImg/Declaration_2.jpg"
            End Select
            Call Enable_Rem_MenuList()

            If ddlGEMSSchool.SelectedIndex <> -1 Then
                str_error_cond = str_error_cond + ddlGEMSSchool.SelectedItem.Text
            End If
            If ddlGrade.SelectedIndex <> -1 Then
                str_error_cond = str_error_cond + "Grade: " + ddlGrade.SelectedItem.Text
            End If

            str_error_cond = str_error_cond '+ ViewState("btnStateCheck")



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "mnuReg_Select" & str_error_cond)
        End Try
    End Sub
    Sub Enable_Rem_MenuList()
        Try
            Dim count_Active As Integer = mvRegMain.ActiveViewIndex
            'to allow the user to move to the next menu one after the other and storing the all selected menus
            If ViewState("Active_count") IsNot Nothing Then
                If ViewState("Active_count").Contains(count_Active) = False Then
                    ViewState("Active_count").Add(count_Active)
                    mnuRegMaster.Items(count_Active).Selectable = True
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Enable_Rem_MenuList")
        End Try
    End Sub
    Sub disable_Rem_MenuList()
        Try
            For i As Integer = 0 To mnuRegMaster.Items.Count - 1
                If i = 0 Then
                    mnuRegMaster.Items(i).Selectable = True
                    btnVwAcad_Next.Enabled = False
                Else
                    mnuRegMaster.Items(i).Selectable = False
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "disable_Rem_MenuList")
        End Try
    End Sub
    Protected Sub btnVwAcad_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwAcad_Next.Click
        '  mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex + 1


        ViewState("VW_ACAD") = Status_VW_ACAD()
        'ViewState("btnStateCheck") = " First Screen "
        If ViewState("VW_ACAD") = "1" Then

            If ViewState("bRefer_Friend") = True Then
                divRef.Visible = True
                maintab.Visible = False
                vsRef_content.Visible = True
                rfvRef_Email.Enabled = True
                rfvRef_code.Enabled = True
                rbRefNo.Checked = False
                rbRefYes.Checked = True
            Else
                vsRef_content.Visible = False
                rfvRef_Email.Enabled = False
                rfvRef_code.Enabled = False
                rbRefNo.Checked = True
                rbRefYes.Checked = False
                mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex + 1
                Call mnuReg_Select(mvRegMain.ActiveViewIndex)

            End If


        Else
            divRef.Visible = False
            maintab.Visible = True
            mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex
            Call mnuReg_Select(mvRegMain.ActiveViewIndex)
        End If


        ' Call VW_ACAD()

        ' Call mnuReg_Select(mvRegMain.ActiveViewIndex)
    End Sub
    Protected Sub btnRefPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefPrev.Click
        divRef.Visible = False
        maintab.Visible = True
    End Sub

    Protected Sub btnRefNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefNext.Click
        If rbRefYes.Checked = True Then

            If RefCodeIsvalid() = True Then
                mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex + 1
                Call mnuReg_Select(mvRegMain.ActiveViewIndex)
                vsRef_content.Visible = False
            Else
                lblError.Text = "<UL><LI>Please verify the ref. code and email address you have entered.</LI></UL>"
            End If

        ElseIf rbRefNo.Checked = True Then
            mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex + 1
            Call mnuReg_Select(mvRegMain.ActiveViewIndex)
            vsRef_content.Visible = False
        End If
    End Sub

    Function RefCodeIsvalid() As Boolean
        Dim refcode As String = txtRefCode.Text.Trim
        Dim Bsu_id As String = ddlGEMSSchool.SelectedValue
        Dim email As String = txtREFEmail.Text.Trim

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = " select TOP 1 D.RFD_ID, M.RFM_CODE,S.RFS_REF_EMAIL from REF.REFERRAL_M as M inner join REF.REFERRAL_S AS S " & _
 " on S.RFS_RFM_ID=M.RFM_ID INNER JOIN REF.REFERRAL_D AS D ON D.RFD_RFS_ID=S.RFS_ID " & _
 " WHERE RFM_CODE='" & refcode & "' and RFS_REF_EMAIL='" & email & "' and RFD_BSU_ID='" & Bsu_id & "'"

        Using USER_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If USER_reader.HasRows Then


                While USER_reader.Read
                    ViewState("RFD_ID") = Convert.ToString(USER_reader("RFD_ID"))
                End While
                Return True

            Else
                Return False
            End If
        End Using

    End Function

    Public Function GetPrevGrade() As String
        Dim GRD As String = String.Empty
        If ddlGrade.SelectedIndex <> -1 Then
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = "select GRD_DISPLAY from GRADE_M where GRD_DISPLAYORDER=(SELECT GRD_DISPLAYORDER FROM GRADE_M where GRD_DISPLAY='" & ddlGrade.SelectedItem.Value & "')-1"
            Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString
        Else
            Return ""
        End If

    End Function
    Protected Sub btnVwApp_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwApp_Next.Click
        ViewState("Valid_Appl") = "1"
        If Page.IsValid Then



            Call Valid_Appl()
            'ViewState("btnStateCheck") = " Appl Info Next  "
            If ViewState("Valid_Appl") = "1" Then




                If chkSibling.Checked = True Then
                    Call SiblingInfo()
                End If
                mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex + 1
                Call mnuReg_Select(mvRegMain.ActiveViewIndex)

            Else
                mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex
                Call mnuReg_Select(mvRegMain.ActiveViewIndex)
            End If
        Else
            ViewState("Valid_Appl") = "-1"
            If mvRegMain.ActiveViewIndex = 1 Then
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                               "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                              "<script language=javascript>fill_Tran();</script>")

            End If
        End If
    End Sub
    Protected Sub btnVwPri_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwPri_Next.Click
        If Page.IsValid Then
            Call Valid_Prim_Cont()
            'ViewState("btnStateCheck") = " prim cont next  "

            If ViewState("Valid_Prim_cont") = "1" Then
                mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex + 1
                Call mnuReg_Select(mvRegMain.ActiveViewIndex)
            End If
        Else
            ViewState("Valid_Prim_cont") = "-1"
            If mvRegMain.ActiveViewIndex = 2 Then
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                              "<script language=javascript>fill_Staffs();</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                               "<script language=javascript>fill_Studs();</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                             "<script language=javascript>fill_MStaffs();</script>")
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                               "<script language=javascript>fill_MStuds();</script>")
            End If
        End If


    End Sub
    Protected Sub btnVwPrev_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwPrev_Next.Click
        If Page.IsValid Then
            'ViewState("btnStateCheck") = " current school next "
            If Check_KG1() = True Then
                ViewState("Valid_Prev_school") = "1"
                callAutoAdd()
                mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex + 1
                Call mnuReg_Select(mvRegMain.ActiveViewIndex)
            Else
                ViewState("Valid_Prev_school") = "-1"
            End If

        Else
            ViewState("Valid_Prev_school") = "-1"
        End If

    End Sub
    Protected Sub btnVwApp_Pre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwApp_Pre.Click
        mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex - 1
        'ViewState("btnStateCheck") = " Appl info prev "
        Call mnuReg_Select(mvRegMain.ActiveViewIndex)
    End Sub
    Protected Sub btnVwPrev_Pre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwPrev_Pre.Click
        mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex - 1
        'ViewState("btnStateCheck") = " current school prev  "
        Call mnuReg_Select(mvRegMain.ActiveViewIndex)
    End Sub
    Protected Sub btnVwPri_Pre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwPri_Pre.Click
        mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex - 1
        'ViewState("btnStateCheck") = " prim cont prev  "
        Call mnuReg_Select(mvRegMain.ActiveViewIndex)
    End Sub
    Protected Sub btnVwHealth_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwHealth_Next.Click
        ViewState("btnStatus") = "1"


        mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex + 1
        Call mnuReg_Select(mvRegMain.ActiveViewIndex)
    End Sub
    Protected Sub btnVwHealth_Pre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwHealth_Pre.Click
        mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex - 1
        Call mnuReg_Select(mvRegMain.ActiveViewIndex)
    End Sub
    Protected Sub btnVwDecl_Pre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVwDecl_Pre.Click
        mvRegMain.ActiveViewIndex = mvRegMain.ActiveViewIndex - 1
        ' ViewState("btnStateCheck") = " declare prev  "
        Call mnuReg_Select(mvRegMain.ActiveViewIndex)
    End Sub
    Sub SiblingInfo()
        Try
            Dim mobNumber As String()
            Dim hNumber As String()
            Dim oNumber As String()
            Dim fNumber As String()
            Using sibl_reader As SqlDataReader = AccessStudentClass.GetSiblingDetails(txtSib_ID.Text, ddlSib_BSU.SelectedValue)
                While sibl_reader.Read
                    If sibl_reader("STU_PRIMARYCONTACT") = "F" Then
                        rdPri_Father.Checked = True
                    ElseIf sibl_reader("STU_PRIMARYCONTACT") = "M" Then
                        rbPri_Mother.Checked = True
                    ElseIf sibl_reader("STU_PRIMARYCONTACT") = "G" Then
                        rdPri_Guard.Checked = True
                    End If

                    If Not ddlFPref_contact.Items.FindByText(sibl_reader("STU_PRIMARYCONTACT")) Is Nothing Then
                        ddlFPref_contact.ClearSelection()
                        ddlFPref_contact.Items.FindByText(sibl_reader("STU_PRIMARYCONTACT")).Selected = True
                    End If




                    txtFPri_Fname.Text = sibl_reader("STS_FFIRSTNAME")
                    txtFPri_Fname.Enabled = False

                    txtFPri_Mname.Text = sibl_reader("STS_FMIDNAME")
                    txtFPri_Mname.Enabled = False
                    txtFPri_Lname.Text = sibl_reader("STS_FLASTNAME")
                    txtFPri_Lname.Enabled = False
                    If Not ddlFPri_National1.Items.FindByValue(sibl_reader("STS_FNATIONALITY")) Is Nothing Then
                        ddlFPri_National1.ClearSelection()
                        ddlFPri_National1.Items.FindByValue(sibl_reader("STS_FNATIONALITY")).Selected = True
                    End If
                    If Not ddlFPri_National2.Items.FindByValue(sibl_reader("STS_FNATIONALITY2")) Is Nothing Then
                        ddlFPri_National2.ClearSelection()
                        ddlFPri_National2.Items.FindByValue(sibl_reader("STS_FNATIONALITY2")).Selected = True
                    End If
                    mobNumber = sibl_reader("STS_FMOBILE").ToString.Split(" ")
                    If mobNumber.Length = 3 Then
                        txtFM_Country.Text = mobNumber(0)
                        txtFM_Area.Text = mobNumber(1)
                        txtFMobile_Pri.Text = mobNumber(2)
                    ElseIf mobNumber.Length = 2 Then
                        txtFM_Area.Text = mobNumber(0)
                        txtFMobile_Pri.Text = mobNumber(1)
                    ElseIf mobNumber.Length = 1 Then
                        txtFMobile_Pri.Text = mobNumber(0)
                    End If

                    txtFPoBox_Pri.Text = sibl_reader("STS_FPRMPOBOX")
                    txtFEmail_Pri.Text = sibl_reader("STS_FEMAIL").ToString.ToLower
                    txtFAdd1_overSea.Text = sibl_reader("STS_FPRMADDR1")
                    txtFAdd2_overSea.Text = sibl_reader("STS_FPRMADDR2")


                    If Not ddlFOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_FPRMCOUNTRY")) Is Nothing Then
                        ddlFOverSeas_Add_Country.ClearSelection()
                        ddlFOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_FPRMCOUNTRY")).Selected = True
                    End If

                    txtFStreet.Text = sibl_reader("STS_FCOMADDR1")
                    txtFArea.Text = sibl_reader("STS_FCOMADDR2")
                    txtFCity_pri.Text = sibl_reader("STS_FCOMCITY")
                    'If Not ddlCity_pri.Items.FindByValue(sibl_reader("STS_FCOMCITY")) Is Nothing Then
                    '    ddlCity_pri.ClearSelection()
                    '    ddlCity_pri.Items.FindByValue(sibl_reader("STS_FCOMCITY")).Selected = True
                    'End If

                    If Not ddlFCountry_Pri.Items.FindByValue(sibl_reader("STS_FCOMCOUNTRY")) Is Nothing Then
                        ddlFCountry_Pri.ClearSelection()
                        ddlFCountry_Pri.Items.FindByValue(sibl_reader("STS_FCOMCOUNTRY")).Selected = True
                    End If

                    hNumber = sibl_reader("STS_FRESPHONE").ToString.Replace(" ", "-").Split("-")
                    If hNumber.Length = 3 Then
                        txtFHPhone_Country.Text = hNumber(0)
                        txtFHPhone_Area.Text = hNumber(1)
                        txtFHPhone.Text = hNumber(2)
                    ElseIf hNumber.Length = 2 Then
                        txtFHPhone_Area.Text = hNumber(0)
                        txtFHPhone.Text = hNumber(1)
                    ElseIf hNumber.Length = 1 Then
                        txtFHPhone.Text = hNumber(0)
                    End If

                    oNumber = sibl_reader("STS_FOFFPHONE").ToString.Replace(" ", "-").Split("-")
                    If oNumber.Length = 3 Then
                        txtFOPhone_Country.Text = oNumber(0)
                        txtFOPhone_Area.Text = oNumber(1)
                        txtFOPhone.Text = oNumber(2)
                    ElseIf oNumber.Length = 2 Then
                        txtFOPhone_Area.Text = oNumber(0)
                        txtFOPhone.Text = oNumber(1)
                    ElseIf oNumber.Length = 1 Then
                        txtFOPhone.Text = oNumber(0)
                    End If


                    fNumber = sibl_reader("STS_FFAX").ToString.Replace(" ", "-").Split("-")
                    If fNumber.Length = 3 Then
                        txtFFaxNo_country.Text = fNumber(0)
                        txtFFaxNo_Area.Text = fNumber(1)
                        txtFFaxNo.Text = fNumber(2)
                    ElseIf fNumber.Length = 2 Then
                        txtFFaxNo_Area.Text = fNumber(0)
                        txtFFaxNo.Text = fNumber(1)
                    ElseIf fNumber.Length = 1 Then
                        txtFFaxNo.Text = fNumber(0)
                    End If
                    txtFPoboxLocal.Text = sibl_reader("STS_FCOMPOBOX")
                    txtFOccup.Text = sibl_reader("STS_FOCC")
                    If Not ddlFCompany.Items.FindByValue(sibl_reader("STS_F_COMP_ID")) Is Nothing Then
                        ddlFCompany.ClearSelection()
                        ddlFCompany.Items.FindByValue(sibl_reader("STS_F_COMP_ID")).Selected = True
                    End If



                    txtMPri_Fname.Text = sibl_reader("STS_MFIRSTNAME")
                    txtMPri_Mname.Text = sibl_reader("STS_MMIDNAME")
                    txtMPri_Lname.Text = sibl_reader("STS_MLASTNAME")
                    If Not ddlMPri_National1.Items.FindByValue(sibl_reader("STS_MNATIONALITY")) Is Nothing Then
                        ddlMPri_National1.ClearSelection()
                        ddlMPri_National1.Items.FindByValue(sibl_reader("STS_MNATIONALITY")).Selected = True
                    End If
                    If Not ddlMPri_National2.Items.FindByValue(sibl_reader("STS_MNATIONALITY2")) Is Nothing Then
                        ddlMPri_National2.ClearSelection()
                        ddlMPri_National2.Items.FindByValue(sibl_reader("STS_MNATIONALITY2")).Selected = True
                    End If

                    mobNumber = sibl_reader("STS_MMOBILE").ToString.Split(" ")
                    If mobNumber.Length = 3 Then
                        txtMM_Country.Text = mobNumber(0)
                        txtMM_Area.Text = mobNumber(1)
                        txtMMobile_Pri.Text = mobNumber(2)
                    ElseIf mobNumber.Length = 2 Then
                        txtMM_Area.Text = mobNumber(0)
                        txtMMobile_Pri.Text = mobNumber(1)
                    ElseIf mobNumber.Length = 1 Then
                        txtMMobile_Pri.Text = mobNumber(0)
                    End If

                    txtMPoBox_Pri.Text = sibl_reader("STS_MPRMPOBOX")
                    txtMEmail_Pri.Text = sibl_reader("STS_MEMAIL").ToString.ToLower
                    txtMAdd1_overSea.Text = sibl_reader("STS_MPRMADDR1")
                    txtMAdd2_overSea.Text = sibl_reader("STS_MPRMADDR2")

                    If Not ddlMOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_MPRMCOUNTRY")) Is Nothing Then
                        ddlMOverSeas_Add_Country.ClearSelection()
                        ddlMOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_MPRMCOUNTRY")).Selected = True
                    End If
                    txtMStreet.Text = sibl_reader("STS_MCOMADDR1")
                    txtMArea.Text = sibl_reader("STS_MCOMADDR2")
                    txtMCity_pri.Text = Convert.ToString(sibl_reader("STS_MCOMCITY"))

                    'If Not ddlCity_pri.Items.FindByValue(sibl_reader("STS_MCOMCITY")) Is Nothing Then
                    '    ddlCity_pri.ClearSelection()
                    '    ddlCity_pri.Items.FindByValue(sibl_reader("STS_MCOMCITY")).Selected = True
                    'End If

                    If Not ddlMCountry_Pri.Items.FindByValue(sibl_reader("STS_MCOMCOUNTRY")) Is Nothing Then
                        ddlMCountry_Pri.ClearSelection()
                        ddlMCountry_Pri.Items.FindByValue(sibl_reader("STS_MCOMCOUNTRY")).Selected = True
                    End If

                    hNumber = sibl_reader("STS_MRESPHONE").Replace(" ", "-").Split("-")
                    If hNumber.Length = 3 Then
                        txtMHPhone_Country.Text = hNumber(0)
                        txtMHPhone_Area.Text = hNumber(1)
                        txtMHPhone.Text = hNumber(2)
                    ElseIf hNumber.Length = 2 Then
                        txtMHPhone_Area.Text = hNumber(0)
                        txtMHPhone.Text = hNumber(1)
                    ElseIf hNumber.Length = 1 Then
                        txtMHPhone.Text = hNumber(0)
                    End If

                    oNumber = sibl_reader("STS_MOFFPHONE").Replace(" ", "-").Split("-")
                    If oNumber.Length = 3 Then
                        txtMOPhone_Country.Text = oNumber(0)
                        txtMOPhone_Area.Text = oNumber(1)
                        txtMOPhone.Text = oNumber(2)
                    ElseIf oNumber.Length = 2 Then
                        txtMOPhone_Area.Text = oNumber(0)
                        txtMOPhone.Text = oNumber(1)
                    ElseIf oNumber.Length = 1 Then
                        txtMOPhone.Text = oNumber(0)
                    End If



                    fNumber = sibl_reader("STS_MFAX").Replace(" ", "-").Split("-")
                    If fNumber.Length = 3 Then
                        txtMFaxNo_country.Text = fNumber(0)
                        txtMFaxNo_Area.Text = fNumber(1)
                        txtMFaxNo.Text = fNumber(2)
                    ElseIf fNumber.Length = 2 Then
                        txtMFaxNo_Area.Text = fNumber(0)
                        txtMFaxNo.Text = fNumber(1)
                    ElseIf fNumber.Length = 1 Then
                        txtMFaxNo.Text = fNumber(0)
                    End If

                    txtMPoboxLocal.Text = sibl_reader("STS_MCOMPOBOX")
                    txtMOccup.Text = sibl_reader("STS_MOCC")
                    If Not ddlMCompany.Items.FindByValue(sibl_reader("STS_M_COMP_ID")) Is Nothing Then
                        ddlMCompany.ClearSelection()
                        ddlMCompany.Items.FindByValue(sibl_reader("STS_M_COMP_ID")).Selected = True
                    End If


                    txtGPri_Fname.Text = sibl_reader("STS_GFIRSTNAME")
                    txtGPri_Mname.Text = sibl_reader("STS_GMIDNAME")
                    txtGPri_Lname.Text = sibl_reader("STS_GLASTNAME")
                    If Not ddlGPri_National1.Items.FindByValue(sibl_reader("STS_GNATIONALITY")) Is Nothing Then
                        ddlGPri_National1.ClearSelection()
                        ddlGPri_National1.Items.FindByValue(sibl_reader("STS_GNATIONALITY")).Selected = True
                    End If
                    If Not ddlGPri_National2.Items.FindByValue(sibl_reader("STS_GNATIONALITY2")) Is Nothing Then
                        ddlGPri_National2.ClearSelection()
                        ddlGPri_National2.Items.FindByValue(sibl_reader("STS_GNATIONALITY2")).Selected = True
                    End If

                    mobNumber = sibl_reader("STS_GMOBILE").ToString.Split(" ")
                    If mobNumber.Length = 3 Then
                        txtGM_Country.Text = mobNumber(0)
                        txtGM_Area.Text = mobNumber(1)
                        txtGMobile_Pri.Text = mobNumber(2)
                    ElseIf mobNumber.Length = 2 Then
                        txtGM_Area.Text = mobNumber(0)
                        txtGMobile_Pri.Text = mobNumber(1)
                    ElseIf mobNumber.Length = 1 Then
                        txtGMobile_Pri.Text = mobNumber(0)
                    End If

                    txtGPoBox_Pri.Text = sibl_reader("STS_GCOMPOBOX")
                    txtGEmail_Pri.Text = sibl_reader("STS_GEMAIL").ToString.ToLower
                    txtGAdd1_overSea.Text = sibl_reader("STS_GPRMADDR1")
                    txtGAdd2_overSea.Text = sibl_reader("STS_GPRMADDR2")
                    txtGOverSeas_Add_City.Text = sibl_reader("STS_FPRMCITY")

                    If Not ddlGOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_GPRMCOUNTRY")) Is Nothing Then
                        ddlGOverSeas_Add_Country.ClearSelection()
                        ddlGOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_GPRMCOUNTRY")).Selected = True
                    End If

                    txtGStreet.Text = sibl_reader("STS_GCOMADDR1")
                    txtGArea.Text = sibl_reader("STS_GCOMADDR2")
                    txtGCity_pri.Text = sibl_reader("STS_GCOMCITY")
                    'If Not ddlCity_pri.Items.FindByValue(sibl_reader("STS_GCOMCITY")) Is Nothing Then
                    '    ddlCity_pri.ClearSelection()
                    '    ddlCity_pri.Items.FindByValue(sibl_reader("STS_GCOMCITY")).Selected = True
                    'End If

                    If Not ddlGCountry_Pri.Items.FindByValue(sibl_reader("STS_GCOMCOUNTRY")) Is Nothing Then
                        ddlGCountry_Pri.ClearSelection()
                        ddlGCountry_Pri.Items.FindByValue(sibl_reader("STS_GCOMCOUNTRY")).Selected = True
                    End If

                    hNumber = sibl_reader("STS_GRESPHONE").ToString.Split(" ")
                    If hNumber.Length = 3 Then
                        txtGHPhone_Country.Text = hNumber(0)
                        txtGHPhone_Area.Text = hNumber(1)
                        txtGHPhone.Text = hNumber(2)
                    ElseIf hNumber.Length = 2 Then
                        txtGHPhone_Area.Text = hNumber(0)
                        txtGHPhone.Text = hNumber(1)
                    ElseIf hNumber.Length = 1 Then
                        txtGHPhone.Text = hNumber(0)
                    End If

                    oNumber = sibl_reader("STS_GOFFPHONE").ToString.Split(" ")
                    If oNumber.Length = 3 Then
                        txtGOPhone_Country.Text = oNumber(0)
                        txtGOPhone_Area.Text = oNumber(1)
                        txtGOPhone.Text = oNumber(2)
                    ElseIf oNumber.Length = 2 Then
                        txtGOPhone_Area.Text = oNumber(0)
                        txtGOPhone.Text = oNumber(1)
                    ElseIf oNumber.Length = 1 Then
                        txtGOPhone.Text = oNumber(0)
                    End If

                    fNumber = sibl_reader("STS_GFAX").ToString.Split(" ")
                    If fNumber.Length = 3 Then
                        txtGFaxNo_country.Text = fNumber(0)
                        txtGFaxNo_Area.Text = fNumber(1)
                        txtGFaxNo.Text = fNumber(2)
                    ElseIf fNumber.Length = 2 Then
                        txtGFaxNo_Area.Text = fNumber(0)
                        txtGFaxNo.Text = fNumber(1)
                    ElseIf fNumber.Length = 1 Then
                        txtGFaxNo.Text = fNumber(0)
                    End If

                    txtGPoboxLocal.Text = sibl_reader("STS_GCOMPOBOX")
                    txtGOccup.Text = sibl_reader("STS_GOCC")
                    If Not ddlGCompany.Items.FindByValue(sibl_reader("STS_G_COMP_ID")) Is Nothing Then
                        ddlGCompany.ClearSelection()
                        ddlGCompany.Items.FindByValue(sibl_reader("STS_G_COMP_ID")).Selected = True
                    End If


                End While

            End Using
        Catch ex As Exception

        End Try
    End Sub
    Sub Valid_Appl()

        Dim CommStr As String = String.Empty
        CommStr = "<UL>"

        If Trim(txtFname.Text) <> "" Then
            If Not Regex.Match(Trim(txtFname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = "<LI>Invalid Student first name"
                ViewState("Valid_Appl") = "-1"
            End If
        End If


        If Trim(txtMname.Text) <> "" Then
            If Not Regex.Match(Trim(txtMname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = CommStr & "<LI>Invalid Student middle name"
                ViewState("Valid_Appl") = "-1"
            End If
        End If
        If Trim(txtLname.Text) <> "" Then
            If Not Regex.Match(Trim(txtLname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = CommStr & "<LI>Invalid Student last name"
                ViewState("Valid_Appl") = "-1"
            End If
        End If
        'some reason----------------------------------
        'If (Request.QueryString("MainMnu_code")) Is Nothing Then
        If ViewState("ENQTYPE") = "O" Then
            If ViewState("V_SIB") <> "" Then
                If ViewState("orCheck") >= 1 Then
                    chkSibling.Checked = True
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                End If
            End If
        End If

        If ViewState("V_GENDER") = "Female Applicant" And rdFemale.Checked = False Then
            CommStr = CommStr & "<LI>Online enquiry is opened for female applicant only"
            ViewState("Valid_Appl") = "-1"
        ElseIf ViewState("V_GENDER") = "Male Applicant" And rdMale.Checked = False Then
            CommStr = CommStr & "<LI>Online enquiry is opened for male applicant only"
            ViewState("Valid_Appl") = "-1"
        End If


        '--------------for SIBILING check (MAIN)-------------
        If chkSibling.Checked = True Then
            '--------------for SIBILING_Name check (A)-------------
            If Trim(txtSib_Name.Text) = "" Then
                CommStr = CommStr & "<LI>Sibling name cannot be left empty"
                ViewState("Valid_Appl") = "-1"
                If mvRegMain.ActiveViewIndex = 1 Then
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                   "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                                                  "<script language=javascript>fill_Tran('" & chkTran_Req.ClientID & "');</script>")
                End If

            End If
            '--------------(A) End Here-------------

            '--------------for STUDENT FEE_ID check (B)-------------

            If Trim(txtSib_ID.Text) = "" Then
                CommStr = CommStr & "<LI>Sibling Fee ID required"
                ViewState("Valid_Appl") = "-1"
                If mvRegMain.ActiveViewIndex = 1 Then
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                   "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                                                 "<script language=javascript>fill_Tran('" & chkTran_Req.ClientID & "');</script>")
                End If
            Else

                '--------------for open for Staff check  (C)-------------
                '  If (Request.QueryString("MainMnu_code")) Is Nothing Then
                If ViewState("ENQTYPE") = "O" Then
                    If ViewState("V_SIB") <> "" Then
                        Dim SIB_BSUIDs As String = ViewState("S_GEMS_SIBIL")

                        '--------------for Staff_ID check from Selected BSU_IDs orCheck=1(D1)-------------


                        If SIB_BSUIDs.IndexOf(ddlSib_BSU.SelectedItem.Value) < 0 Then
                            CommStr = CommStr & "<LI>Online Enquiry is not open for the Sibling from " & StrConv(ddlSib_BSU.SelectedItem.Text, VbStrConv.ProperCase)
                            ViewState("Valid_Appl") = "-1"
                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                        Else
                            Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)

                                If ValidSibl_reader.HasRows = False Then
                                    CommStr = CommStr & "<LI>Sibling ID is not valid for the selected School"
                                    ViewState("Valid_Appl") = "-1"
                                    If mvRegMain.ActiveViewIndex = 1 Then
                                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                                                          "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                                                                      "<script language=javascript>fill_Tran('" & chkTran_Req.ClientID & "');</script>")
                                    End If
                                End If

                            End Using
                        End If
                        '--------------(D1) End Here-------------


                    Else
                        ' --------------for open only for Staff check  (D2)-------------

                        Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)

                            If ValidSibl_reader.HasRows = False Then
                                CommStr = CommStr & "<LI>Sibling Fee ID is not valid  for the selected School"
                                ViewState("Valid_Appl") = "-1"
                                If mvRegMain.ActiveViewIndex = 1 Then
                                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                                                                  "<script language=javascript>fill_Tran('" & chkTran_Req.ClientID & "');</script>")
                                End If
                            End If

                        End Using
                        '--------------(D2) End Here-------------
                    End If
                    '-----------------------------------(C)End Here-----------------------------



                Else
                    '-----------------------code for registrar----------------------
                    Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)

                        If ValidSibl_reader.HasRows = False Then
                            CommStr = CommStr & "<LI>Sibling Fee ID is not valid  for the selected School"
                            ViewState("Valid_Appl") = "-1"
                            If mvRegMain.ActiveViewIndex = 1 Then
                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                   "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                                                              "<script language=javascript>fill_Tran('" & chkTran_Req.ClientID & "');</script>")
                            End If
                        End If

                    End Using

                    '-----------------code for registrar end here-------------------

                End If


            End If
            '-----------------------------------(B)End Here-----------------------------

        End If
        '-----------------------------------(MAIN)End Here-----------------------------



        If txtDob.Text.Trim <> "" Then
            Dim strfDate As String = txtDob.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                ViewState("Valid_Appl") = "-1"
                CommStr = CommStr & "<li>Invalid Date of birth"
            Else
                txtDob.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtDob.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                'check for the leap year date
                If Not IsDate(dateTime1) Then
                    ViewState("Valid_Appl") = "-1"
                    CommStr = CommStr & "<li>Invalid Date of birth"
                End If
                Dim cutoff As DateTime = hfcutoff.Value
                Dim cutoff_Dob As DateTime = txtDob.Text
                If cutoff_Dob > cutoff Then
                    ViewState("Valid_Appl") = "-1"
                    CommStr = CommStr & "<li>Applicant is under-age as on the cutoff date" & String.Format("{0:" & OASISConstants.DateFormat & "}", cutoff)
                End If

            End If
        End If

        If txtPassIss_date.Text.Trim <> "" Then
            Dim strfDate As String = txtPassIss_date.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                ViewState("Valid_Appl") = "-1"
                CommStr = CommStr & "<li>Passport Issue Date format is Invalid"

            Else
                txtPassIss_date.Text = strfDate
                Dim DateTime2 As String
                Dim dateTime1 As String
                Dim strfDate1 As String = txtDob.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    txtDob.Text = strfDate1
                    DateTime2 = Date.ParseExact(txtDob.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(dateTime1) = True Then
                        If DateTime.Compare(dateTime1, DateTime2) < 0 Then
                            ViewState("Valid_Appl") = "-1"
                            CommStr = CommStr & "<li>Passport Issue Date entered is not a valid date and must be greater than Date of Birth"
                        End If
                    Else
                        ViewState("Valid_Appl") = "-1"
                        CommStr = CommStr & "<li>Invalid Passport Issue Date"
                    End If

                End If
            End If
        End If

        If txtPassExp_Date.Text.Trim <> "" Then

            Dim strfDate As String = txtPassExp_Date.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                ViewState("Valid_Appl") = "-1"
                CommStr = CommStr & "<li>Passport Expiry Date format is Invalid"
            Else
                txtPassExp_Date.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtPassIss_date.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                            ViewState("Valid_Appl") = "-1"
                            CommStr = CommStr & "<li>Passport Expiry  Date entered is not a valid date and must be greater than Passport Issue Date"
                        End If
                    Else
                        ViewState("Valid_Appl") = "-1"
                        CommStr = CommStr & "<li>Invalid Passport Expiry  Date"
                    End If
                End If
            End If
        End If

        If txtEMIRATES_ID.Text.Trim <> "" Then
            If txtVisaExp_date.Text.Trim = "" Then
                ViewState("Valid_Appl") = "-1"
                CommStr = CommStr & "<LI>Visa Issue Date is not a vaild date"
            End If

            If txtVisaExp_date.Text.Trim = "" Then
                ViewState("Valid_Appl") = "-1"
                CommStr = CommStr & "<li>Visa Expiry Date format is Invalid"
            End If
        End If


        If txtVisaIss_date.Text.Trim <> "" Then

            If IsDate(txtVisaIss_date.Text) = False Then
                ViewState("Valid_Appl") = "-1"
                CommStr = CommStr & "<LI>Visa Issue Date is not a vaild date"
            Else

                Dim strfDate As String = txtVisaIss_date.Text.Trim

                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ViewState("Valid_Appl") = "-1"
                    CommStr = CommStr & "<li>Visa Issue Date format is Invalid"

                End If
            End If
        End If

        If txtVisaExp_date.Text.Trim <> "" Then
            Dim strfDate As String = txtVisaExp_date.Text.Trim

            Dim str_err As String = DateFunctions.checkdate(strfDate)

            If str_err <> "" Then
                ViewState("Valid_Appl") = "-1"
                CommStr = CommStr & "<li>Visa Expiry Date format is Invalid"
            Else
                txtVisaExp_date.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtVisaIss_date.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    txtVisaIss_date.Text = strfDate1
                    DateTime2 = Date.ParseExact(txtVisaExp_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtVisaIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date

                    If IsDate(DateTime2) Then
                        If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                            ViewState("Valid_Appl") = "-1"
                            CommStr = CommStr & "<li>Visa Expiry Date entered is not a valid date and must be greater than Visa Issue Date"
                        End If
                    Else
                        ViewState("Valid_Appl") = "-1"
                        CommStr = CommStr & "<li>Invalid Visa Expiry Date"
                    End If
                End If
            End If
        End If

        Dim Enq_hash As New Hashtable
        If Not Session("BSU_Enq_Valid") Is Nothing Then
            Enq_hash = Session("BSU_Enq_Valid")
        End If

        If Enq_hash.ContainsKey("FLANG") = False Then
            If ddlFLang.SelectedItem.Text.Trim = "" Then
                CommStr = CommStr & "<LI>Please choose the first language."
                ViewState("Valid_Appl") = "-1"
            End If
        End If

        lblError.Text = CommStr & "</UL>"




    End Sub
    Function Check_KG1() As Boolean
        Dim Check_Flag As Boolean = True
        Dim ErrorString As String

        Dim Enq_hash As New Hashtable
        If Not Session("BSU_Enq_Valid") Is Nothing Then
            Enq_hash = Session("BSU_Enq_Valid")
        End If


        ErrorString = "<UL>"
        If (ddlGrade.SelectedItem.Value = "KG1" Or ddlGrade.SelectedItem.Value = "PK" Or ddlGrade.SelectedItem.Value = "PN1") Then
            If Enq_hash.ContainsKey("CSNY") = False Then
                If ddlPreSchool_Nursery.SelectedItem.Text = "--" Then
                    ErrorString = ErrorString & "<LI>Select current school name" & "</UL>"
                    Check_Flag = False
                End If
            End If

        Else
            'If Enq_hash.ContainsKey("CURRN") = False Then
            '    If ddlPre_Curriculum.SelectedItem.Text = "--" Then
            '        ErrorString = ErrorString & "<LI>Curriculum needs to be selected"
            '        Check_Flag = False
            '    End If
            'End If
            Check_Flag = checkGemsStud_Valid(ErrorString)
        End If
        lblError.Text = ErrorString
        Check_KG1 = Check_Flag
    End Function
    Function checkGemsStud_Valid(Optional ByRef ErrorString As String = "") As Boolean

        Dim Check_Flag As Boolean = True

        Dim DateValid_L_Date As Date

        Dim Appl_GEMS_STUD As Boolean = False
        'some reason----------------------------------
        'If (Request.QueryString("MainMnu_code")) Is Nothing Then
        If ViewState("ENQTYPE") = "O" Then
            If ViewState("V_GEMS_STU") <> "" Then
                If ViewState("orCheck") >= 1 Then
                    Appl_GEMS_STUD = True
                End If
            End If
        End If
        If hfValid_ID.Value = "0" Then


            '--------------for GEMS STUDENTS check (MAIN)-------------
            If Appl_GEMS_STUD = True Then
                '-----------------------code not for registrar----------------------
                '--------------for STUDENT FEE_ID check (B)-------------
                If Trim(txtFeeID_GEMS.Text) = "" Then
                    ErrorString = ErrorString & "<LI>Student Id required"
                    Check_Flag = False
                Else
                    '--------------for open for Staff check  (C)-------------
                    ' If (Request.QueryString("MainMnu_code")) Is Nothing Then
                    If ViewState("ENQTYPE") = "O" Then
                        If ViewState("V_GEMS_STU") <> "" Then
                            Dim GEMS_STU_BSUIDs As String = ViewState("S_GEMS_STUD")
                            '--------------for Staff_ID check from Selected BSU_IDs orCheck=1(D1)-------------
                            If GEMS_STU_BSUIDs.IndexOf(ddlGemsSchools.SelectedItem.Value) < 0 Then
                                ErrorString = ErrorString & "<LI>Online Enquiry is not open for the Applicant from " & StrConv(ddlGemsSchools.SelectedItem.Text, VbStrConv.ProperCase)
                                Check_Flag = False
                            Else
                                Using ValidGEMS_STU_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtFeeID_GEMS.Text, ddlGemsSchools.SelectedItem.Value)

                                    If ValidGEMS_STU_reader.HasRows = False Then
                                        ErrorString = ErrorString & "<LI>Student Id is not valid for the selected School"
                                        Check_Flag = False
                                        hfValid_ID.Value = "0"
                                    Else
                                        hfValid_ID.Value = "1"
                                    End If
                                End Using
                            End If
                            '--------------(D1) End Here-------------
                        Else
                            ' --------------for open only for Staff check  (D2)-------------
                            Using ValidGEMS_STU_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtFeeID_GEMS.Text, ddlGemsSchools.SelectedItem.Value)
                                If ValidGEMS_STU_reader.HasRows = False Then
                                    ErrorString = ErrorString & "<LI>Student Id is not valid  for the selected School"
                                    Check_Flag = False
                                    hfValid_ID.Value = "0"
                                Else
                                    hfValid_ID.Value = "1"
                                End If
                            End Using
                            '--------------(D2) End Here-------------
                        End If
                        '-----------------------------------(C)End Here-----------------------------
                        '-----------------------code not for registrar----------------------
                    Else
                        '-----------------------code for registrar----------------------
                        Using ValidGEMS_STU_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtFeeID_GEMS.Text, ddlGemsSchools.SelectedItem.Value)
                            If ValidGEMS_STU_reader.HasRows = False Then
                                ErrorString = ErrorString & "<LI>Student Id is not valid  for the selected School"
                                Check_Flag = False
                                hfValid_ID.Value = "0"
                            Else
                                hfValid_ID.Value = "1"
                            End If
                        End Using
                        '-----------------code for registrar end here-------------------
                    End If
                End If
                '-----------------------------------(B)End Here-----------------------------
            End If
            '-----------------------------------(MAIN)End Here-----------------------------
        End If



        If txtSchFrom_dt.Text.Trim <> "" Then
            If IsDate(txtSchFrom_dt.Text) = False Then
                ErrorString = ErrorString & "<LI>School from date is not a vaild date"

                Check_Flag = False
            End If
        ElseIf txtSchFrom_dt.Text.Trim <> "" And IsDate(txtSchFrom_dt.Text) = True Then
            'code modified 4 date
            Dim strfDate As String = txtSchFrom_dt.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                Check_Flag = False
                ErrorString = ErrorString & "<LI>School from date is not a vaild date"
            Else
                txtSchFrom_dt.Text = strfDate
                DateValid_L_Date = Date.ParseExact(txtSchFrom_dt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                If IsDate(DateValid_L_Date) = False Then
                    ErrorString = ErrorString & "<LI>School from date is not a vaild date"
                    Check_Flag = False
                End If
            End If
        ElseIf txtSchFrom_dt.Text.Trim <> "" And IsDate(txtSchTo_dt.Text) = False Then
            ErrorString = ErrorString & "<LI>School from date format is invalid"

            Check_Flag = False
        End If


        If txtSchTo_dt.Text.Trim <> "" Then
            If IsDate(txtSchTo_dt.Text) = False Then
                ErrorString = ErrorString & "<LI>School to date is not a vaild date"

                Check_Flag = False
            End If
        ElseIf txtSchTo_dt.Text.Trim <> "" And IsDate(txtSchTo_dt.Text) = True Then
            'code modified 4 date
            Dim strfDate As String = txtSchTo_dt.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                Check_Flag = False
                ErrorString = ErrorString & "<LI>School to date is not a vaild date"
            Else
                txtSchTo_dt.Text = strfDate
                DateValid_L_Date = Date.ParseExact(txtSchTo_dt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                If IsDate(DateValid_L_Date) = False Then
                    ErrorString = ErrorString & "<LI>School to date is not a vaild date"
                    Check_Flag = False
                End If
            End If
        ElseIf txtSchTo_dt.Text.Trim <> "" And IsDate(txtSchTo_dt.Text) = False Then
            ErrorString = ErrorString & "<LI>School to date format is invalid"

            Check_Flag = False
        End If



        lblError.Text = ErrorString
        checkGemsStud_Valid = Check_Flag
    End Function
    Sub OpenStaff_BSU_Populate(ByVal BSU_IDs As String)
        'Dim Bsu As String = Replace(BSU_IDs, "|", ",")
        'Bsu = Bsu.TrimEnd(",")
        'Bsu = Bsu.TrimStart(",")
        Dim ds1 As DataSet = AccessStudentClass.GetOpen_BSU_Staff(BSU_IDs)
        'BSU_ID,BSU_NAME

        If ds1.Tables(0).Rows.Count > 0 Then

            ddlFStaff_BSU.Items.Clear()
            ddlFStaff_BSU.DataSource = ds1.Tables(0)
            ddlFStaff_BSU.DataTextField = "BSU_NAME"
            ddlFStaff_BSU.DataValueField = "BSU_ID"
            ddlFStaff_BSU.DataBind()
            ddlMStaff_BSU.Items.Clear()
            ddlMStaff_BSU.DataSource = ds1.Tables(0)
            ddlMStaff_BSU.DataTextField = "BSU_NAME"
            ddlMStaff_BSU.DataValueField = "BSU_ID"
            ddlMStaff_BSU.DataBind()

        End If

    End Sub
    Sub OpenSib_BSU_Populate(ByVal BSU_IDs As String)

        Dim ds1 As DataSet = AccessStudentClass.GetOpen_BSU_Sib(BSU_IDs)
        'BSU_ID,BSU_NAME

        If ds1.Tables(0).Rows.Count > 0 Then
            ddlSib_BSU.Items.Clear()
            ddlSib_BSU.DataSource = ds1.Tables(0)
            ddlSib_BSU.DataTextField = "BSU_NAME"
            ddlSib_BSU.DataValueField = "BSU_ID"
            ddlSib_BSU.DataBind()
        End If

    End Sub
    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Length < 1 Then
            Return False
        Else
            Dim strRegex As String = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" '"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            Dim re As New Regex(strRegex)
            If re.IsMatch(inputEmail) Then
                Return (True)
            Else
                Return (False)
            End If
        End If
    End Function
    'Protected Sub rdGemsGr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGemsGr.CheckedChanged
    '    ddlGemsSchools.Visible = True
    '    'txtPre_School.Text = ""
    '    'txtPre_City.Text = ""
    '    txtPre_School.Visible = False
    '    rfv_Pre_school.Visible = False
    '    'ddlGemsGr_SelectedIndexChanged(ddlGemsGr, Nothing)
    'End Sub
    'Protected Sub rdOther_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdOther.CheckedChanged
    '    '  ddlCont_Status.SelectedIndex = 0
    '    ddlGemsSchools.Visible = False
    '    'txtPre_School.Text = ""
    '    ' txtPre_City.Text = ""
    '    txtPre_School.Visible = True
    'End Sub
    Protected Sub ddlGEMSSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGEMSSchool.SelectedIndexChanged

        Call CUSTOM_LABEL()
        Call GetMain_Loc()
        Call BSU_Enq_validation()
        ddlMainLocation_SelectedIndexChanged(ddlMainLocation, Nothing)
        'Second Curriculum 
        Call CURRICULUM_BSU_4_Student()
        ' Call GetTerm_4ACD()
        Call GetSchoolButton_Enable()
        Call GetReligion_info()
        'new code added
        Call Refer_friend_View()
        If Not ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedValue) Is Nothing Then
            ddlSib_BSU.ClearSelection()
            ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedValue).Selected = True
            'ddlSib_BSU.Enabled = True
        Else
            'ddlSib_BSU.Enabled = True
        End If

        If Not ddlGemsSchools.Items.FindByText("OTHER") Is Nothing Then
            ddlGemsSchools.ClearSelection()
            ddlGemsSchools.Items.FindByText("OTHER").Selected = True
        End If

        ltAboutUs.Text = StrConv(ddlGEMSSchool.SelectedItem.Text, VbStrConv.ProperCase) & "? [please tick]"
    End Sub
    Private Sub CUSTOM_LABEL()

        Dim BSU_ID As String = ddlGEMSSchool.SelectedValue
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", BSU_ID)
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "GETENQUIRY_CUSTOM_LABEL", PARAM)
            If DATAREADER.HasRows Then
                While DATAREADER.Read

                    ltGrade_lb.Text = Convert.ToString(DATAREADER("ECL_GRADE"))
                    ltStream_lb.Text = Convert.ToString(DATAREADER("ECL_STREAM"))
                    If Convert.ToBoolean(DATAREADER("ECL_bSHOWMEAL")) Then
                        trShift.Visible = True
                        ddlShift.Visible = False
                    Else
                        trShift.Visible = False
                        ddlShift.Visible = False
                    End If

                End While

            Else
                ltGrade_lb.Text = "Grade"

                ltStream_lb.Text = "Stream"
                trShift.Visible = False

            End If

        End Using


    End Sub

    Private Sub Refer_friend_View()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Query As String = "select count(bsu_id)  from businessunit_m where  ISNULL(BSU_bREFERRAL,0)=1  and bsu_id ='" & ddlGEMSSchool.SelectedValue & "'"

        Dim IfExists As Integer = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Query))
        If IfExists = 0 Then
            ViewState("bRefer_Friend") = False
        Else
            ViewState("bRefer_Friend") = True
        End If


    End Sub
    Protected Sub ddlCurri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurri.SelectedIndexChanged
        'third Academic year
        Call GetAcademicYear_Bsu()
        ' Call GetTerm_4ACD()
        Call GetSchoolButton_Enable()
        'new code added
    End Sub
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        'sixth stream is called
        Call GetStudent_Stream()
        Call GetSchoolButton_Enable()
    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Call GetSchoolButton_Enable()
    End Sub
    Protected Sub ddlMainLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMainLocation.SelectedIndexChanged
        Call GetSub_Main_Loc()
        Call GetSub_PickUp_point()
        If mvRegMain.ActiveViewIndex = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                           "<script language=javascript>fill_Sibls();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                                        "<script language=javascript>fill_Tran();</script>")

        End If
    End Sub
    Protected Sub ddlSubLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubLocation.SelectedIndexChanged
        Call GetSub_PickUp_point()
        If mvRegMain.ActiveViewIndex = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                           "<script language=javascript>fill_Sibls();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                                        "<script language=javascript>fill_Tran();</script>")

        End If
    End Sub
    Sub Populate_YearList()
        'Year list can be extended
        Dim intYear As Integer
        For intYear = DateTime.Now.Year - 40 To DateTime.Now.Year + 5
            ddlFExYear.Items.Add(intYear)
            ddlMExYear.Items.Add(intYear)
        Next
        ddlFExYear.Items.FindByValue(DateTime.Now.Year).Selected = True
        ddlMExYear.Items.FindByValue(DateTime.Now.Year).Selected = True
    End Sub
    Protected Sub rbRelocAgent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbRelocAgent.CheckedChanged
        If rbRelocAgent.Checked = True Then
            tbReloc.Visible = True
        End If

    End Sub
    Protected Sub rbParentF_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbParentF.CheckedChanged
        If rbParentF.Checked = True Then
            tbReloc.Visible = False
        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If InStr(Request.ServerVariables("http_user_agent"), "AppleWebKit") Then
            Page.ClientTarget = "uplevel"
        End If


        ScriptManager1.EnablePartialRendering = True
    End Sub
    Sub GetSchoolButton_Enable()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim Grade As String = String.Empty
        Dim SHF_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlGrade.SelectedIndex = -1 Then
            Grade = ""
        Else
            Grade = ddlGrade.SelectedItem.Value
        End If


        If ddlShift.SelectedIndex = -1 Then
            SHF_ID = ""
        Else
            SHF_ID = ddlShift.SelectedItem.Value
        End If
        If ddlStream.SelectedIndex = -1 Then
            STM_ID = ""
        Else
            STM_ID = ddlStream.SelectedItem.Value
        End If
        'If rdGemsGr.Checked = True Then
        '    rdGemsGr_CheckedChanged(rdGemsGr, Nothing)
        'Else
        '    rdOther_CheckedChanged(rdOther, Nothing)
        'End If

        ' If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
        If ViewState("ENQTYPE") = "R" Then
            Using EnableButton_reader As SqlDataReader = AccessStudentClass.getPick_Stream_Registrar(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID, STM_ID)

                If EnableButton_reader.HasRows = True Then

                    ViewState("Acd_Valid") = "1" 'if open for registration
                    btnVwAcad_Next.Enabled = True
                Else
                    ViewState("Acd_Valid") = "-1" 'if open for registration
                    btnVwAcad_Next.Enabled = False
                End If

            End Using

        Else
            Using EnableButton_reader As SqlDataReader = AccessStudentClass.getPick_Stream(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID, STM_ID)

                If EnableButton_reader.HasRows = True Then

                    ViewState("Acd_Valid") = "1" 'if open for registration
                    btnVwAcad_Next.Enabled = True
                Else
                    ViewState("Acd_Valid") = "-1" 'if open for registration
                    btnVwAcad_Next.Enabled = False
                End If

            End Using


        End If




    End Sub
    Sub GetBusinessUnits_info_student()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Using AllStudent_BSU_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSchool_list")


                ddlFExStud_Gems.Items.Clear()
                ddlMExStud_Gems.Items.Clear()
                ddlSib_BSU.Items.Clear()
                ddlGemsSchools.Items.Clear()

                'di_Religion = New ListItem("--", "--")
                ' ddlReligion.Items.Add(di_Religion)

                If AllStudent_BSU_reader.HasRows = True Then
                    While AllStudent_BSU_reader.Read
                        ddlFExStud_Gems.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))
                        ddlMExStud_Gems.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))

                        ddlSib_BSU.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))
                        ddlGemsSchools.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))
                    End While
                End If
            End Using

            ddlGemsSchools.Items.Add(New ListItem("OTHER", ""))
            If Not ddlGemsSchools.Items.FindByText("OTHER") Is Nothing Then
                ddlGemsSchools.ClearSelection()
                ddlGemsSchools.Items.FindByText("OTHER").Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_student()")
        End Try
    End Sub
    'first GEMS BSU is Called
    Sub Display_Msg()
        Dim str_conn2 As String = ConnectionManger.GetOASISConnectionString
        Dim pParms2(3) As SqlClient.SqlParameter
        Dim lstrPWD As String
        If Session("Online_sBsuid") = "" Then
            Session("Online_sBsuid") = Session("sBSUID")
        End If
        pParms2(0) = New SqlClient.SqlParameter("@BSU_ID", Session("Online_sBsuid"))

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn2, CommandType.StoredProcedure, "Get_BSU_MSG_TYPE", pParms2)
            While reader.Read
                ltMain.text = Convert.ToString(reader("BSU_ENQ_MSG"))
            End While
        End Using

        

    End Sub
    Sub GEMS_BSU_4_Student()


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim param() As SqlClient.SqlParameter
        'param(0) = New SqlClient.SqlParameter("@BSU_ENQ_GROUP", 1)



        Using BSU_4_Student_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSchool_list")
            ddlGEMSSchool.Items.Clear()
            If BSU_4_Student_reader.HasRows = True Then
                While BSU_4_Student_reader.Read
                    ddlGEMSSchool.Items.Add(New ListItem(BSU_4_Student_reader("bsu_name"), BSU_4_Student_reader("bsu_id")))
                End While
                'ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using

    End Sub
    'Sub GEMS_BSU_AcadStart()
    '    Dim Aca_Year As String = String.Empty
    '    Dim Curri As String = String.Empty
    '    Dim GEMSSchool As String = String.Empty

    '    If ddlAca_Year.SelectedIndex = -1 Then
    '        Aca_Year = ""
    '    Else
    '        Aca_Year = ddlAca_Year.SelectedItem.Value
    '    End If
    '    If ddlCurri.SelectedIndex = -1 Then
    '        Curri = ""
    '    Else
    '        Curri = ddlCurri.SelectedItem.Value
    '    End If

    '    If ddlGEMSSchool.SelectedIndex = -1 Then
    '        GEMSSchool = ""
    '    Else
    '        GEMSSchool = ddlGEMSSchool.SelectedItem.Value

    '    End If
    '    Using BSU_AcadStart_reader As SqlDataReader = AccessStudentClass.GetBSU_M_AcadStart(GEMSSchool, Aca_Year, Curri)



    '        While BSU_AcadStart_reader.Read
    '            txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", BSU_AcadStart_reader("ACD_STARTDT"))
    '        End While
    '    End Using

    'End Sub
    Sub CURRICULUM_BSU_4_Student()
        Dim str_sql As String = String.Empty
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim COUNTRY_ID As String = String.Empty

        Dim GEMSSchool As String = String.Empty
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        str_sql = " SELECT  distinct  CURRICULUM_M.CLM_ID as CLM_ID, CURRICULUM_M.CLM_DESCR as CLM_DESCR,BSU_COUNTRY_ID " & _
" FROM  ACADEMICYEAR_D INNER JOIN  CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID  INNER JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID= ACADEMICYEAR_D.ACD_BSU_ID  where " & _
" ACADEMICYEAR_D.ACD_BSU_ID='" & GEMSSchool & "' order by CURRICULUM_M.CLM_DESCR"



        Using CURRICULUM_BSU_4_Student_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.Text, str_sql)

            ddlCurri.Items.Clear()
            If CURRICULUM_BSU_4_Student_reader.HasRows = True Then
                While CURRICULUM_BSU_4_Student_reader.Read

                    ddlCurri.Items.Add(New ListItem(CURRICULUM_BSU_4_Student_reader("CLM_DESCR"), CURRICULUM_BSU_4_Student_reader("CLM_ID")))
                    COUNTRY_ID = Convert.ToString(CURRICULUM_BSU_4_Student_reader("BSU_COUNTRY_ID"))
                End While
                lblErrorInfo.Visible = False
                ViewState("Acd_Valid") = "1" 'if open for registration
                'ddlCurri.SelectedIndex = 0
                Call country_code(COUNTRY_ID)
                ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            Else

                ViewState("Acd_Valid") = "-1" 'if not open for registration
                lblErrorInfo.Visible = True
                'If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
                If ViewState("ENQTYPE") = "R" Then
                    lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Record currently not available.Please contact system Admin.</div>"
                Else
                    lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Online Registration is CLOSED for this Search query.Please contact the school.</div>"
                End If
                Call country_code(COUNTRY_ID)
                ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using

    End Sub
    Sub GetAcademicYear_Bsu()
        Try


            Dim Curri As String = String.Empty
            Dim GEMSSchool As String = String.Empty


            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If
            If ddlCurri.SelectedIndex = -1 Then
                Curri = ""
            Else
                Curri = ddlCurri.SelectedItem.Value
            End If

            'If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
            If ViewState("ENQTYPE") = "R" Then
                Using OpenBsu_reader As SqlDataReader = AccessStudentClass.GetActive_BSU_Year_Registrar(GEMSSchool, Curri)
                    Dim di_Aca_Year As ListItem
                    ddlAca_Year.Items.Clear()

                    If OpenBsu_reader.HasRows = True Then
                        While OpenBsu_reader.Read
                            di_Aca_Year = New ListItem(OpenBsu_reader("ACY_DESCR"), OpenBsu_reader("ACY_ID"))
                            ddlAca_Year.Items.Add(di_Aca_Year)

                        End While
                        lblErrorInfo.Visible = False

                        ViewState("Acd_Valid") = "1" 'if open for registration

                        Call showActive_year()


                        ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)


                    Else

                        ViewState("Acd_Valid") = "-1" 'if not open for registration
                        lblErrorInfo.Visible = True

                        lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Record currently not available.Please contact system Admin.</div>"
                        ' ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
                        ddlAca_Year.Items.Clear()
                        ddlGrade.Items.Clear()
                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If
                End Using

            Else

                Using OpenBsu_reader As SqlDataReader = AccessStudentClass.GetActive_BSU_Year(GEMSSchool, Curri)
                    Dim di_Aca_Year As ListItem
                    ddlAca_Year.Items.Clear()

                    If OpenBsu_reader.HasRows = True Then
                        While OpenBsu_reader.Read
                            di_Aca_Year = New ListItem(OpenBsu_reader("ACY_DESCR"), OpenBsu_reader("ACY_ID"))
                            ddlAca_Year.Items.Add(di_Aca_Year)

                        End While
                        lblErrorInfo.Visible = False
                        ViewState("Acd_Valid") = "1" 'if open for registration
                        ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)


                    Else

                        ViewState("Acd_Valid") = "-1" 'if not open for registration
                        lblErrorInfo.Visible = True

                        lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Online Registration is CLOSED for this Search query.Please contact the school.</div>"
                        ' ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
                        ddlAca_Year.Items.Clear()
                        ddlGrade.Items.Clear()
                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If
                End Using
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetAcademicYear_Bsu")
        End Try
    End Sub
    Sub showActive_year()
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty


        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        Using readerAcademic_Cutoff As SqlDataReader = AccessStudentClass.GetActive_BsuAndCutOff(GEMSSchool, Curri)
            If readerAcademic_Cutoff.HasRows = True Then
                While readerAcademic_Cutoff.Read
                    If Not ddlAca_Year.Items.FindByValue(Convert.ToString(readerAcademic_Cutoff("ACD_ACY_ID"))) Is Nothing Then
                        ddlAca_Year.ClearSelection()
                        ddlAca_Year.Items.FindByValue(Convert.ToString(readerAcademic_Cutoff("ACD_ACY_ID"))).Selected = True
                    End If

                End While
            End If
        End Using
    End Sub
    Sub GetAllGrade_Bsu()
        Try
            Dim Aca_Year As String = String.Empty
            Dim Curri As String = String.Empty
            Dim GEMSSchool As String = String.Empty
            Dim lstrGrade As String

            If ddlGrade.SelectedIndex = -1 Then
                lstrGrade = ""
            Else
                lstrGrade = ddlGrade.SelectedItem.Value
            End If


            If ddlAca_Year.SelectedIndex = -1 Then
                Aca_Year = ""
            Else
                Aca_Year = ddlAca_Year.SelectedItem.Value
            End If
            If ddlCurri.SelectedIndex = -1 Then
                Curri = ""
            Else
                Curri = ddlCurri.SelectedItem.Value
            End If
            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If


            'If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
            If ViewState("ENQTYPE") = "R" Then
                Using AllGrade_reader As SqlDataReader = AccessStudentClass.GetAllGrade_Bsu_Registrar(Aca_Year, GEMSSchool, Curri)
                    Dim di_Grade As ListItem
                    ddlGrade.Items.Clear()

                    If AllGrade_reader.HasRows = True Then
                        While AllGrade_reader.Read
                            di_Grade = New ListItem(AllGrade_reader("GRM_Display"), AllGrade_reader("GRD_ID"))
                            ddlGrade.Items.Add(di_Grade)
                        End While
                        If Not ddlGrade.Items.FindByValue(lstrGrade) Is Nothing Then
                            ddlGrade.ClearSelection()
                            ddlGrade.Items.FindByValue(lstrGrade).Selected = True
                        End If



                        lblErrorInfo.Visible = False
                        ViewState("Acd_Valid") = "1" 'if open for registration
                        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
                    Else

                        lblErrorInfo.Visible = True

                        lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Record currently not available.Please contact system Admin.</div>"
                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If


                End Using


            Else
                Using AllGrade_reader As SqlDataReader = AccessStudentClass.GetAllGrade_Bsu(Aca_Year, GEMSSchool, Curri)
                    Dim di_Grade As ListItem
                    ddlGrade.Items.Clear()

                    If AllGrade_reader.HasRows = True Then
                        While AllGrade_reader.Read
                            di_Grade = New ListItem(AllGrade_reader("GRM_Display"), AllGrade_reader("GRD_ID"))
                            ddlGrade.Items.Add(di_Grade)

                        End While
                        ddlGrade.ClearSelection()

                        If Not ddlGrade.Items.FindByValue(lstrGrade) Is Nothing Then
                            ddlGrade.ClearSelection()
                            ddlGrade.Items.FindByValue(lstrGrade).Selected = True
                        End If
                        lblErrorInfo.Visible = False
                        ViewState("Acd_Valid") = "1" 'if open for registration
                        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
                    Else

                        lblErrorInfo.Visible = True

                        lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Online Registration is CLOSED for this Search query.Please contact the school.</div>"
                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If

                End Using
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetAllGrade_Bsu")
        End Try
    End Sub
    'Getting all the grade of currently open BSU
    Sub GetAllGrade_Shift()

        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim Grade As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlGrade.SelectedIndex = -1 Then
            Grade = ""
        Else
            Grade = ddlGrade.SelectedItem.Value
        End If

        'If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
        If ViewState("ENQTYPE") = "R" Then
            Using AllGrade_Shift_reader As SqlDataReader = AccessStudentClass.getAllShift_4_Grade_Registrar(Aca_Year, Curri, GEMSSchool, Grade)
                Dim di_Grade_Shift As ListItem
                Dim Shift_count As Integer
                ddlShift.Items.Clear()

                If AllGrade_Shift_reader.HasRows = True Then
                    While AllGrade_Shift_reader.Read
                        di_Grade_Shift = New ListItem(AllGrade_Shift_reader("SHF_DESCR"), AllGrade_Shift_reader("SHF_ID"))
                        ddlShift.Items.Add(di_Grade_Shift)
                        Shift_count += 1
                        hfcutoff.Value = AllGrade_Shift_reader("CUTOFF")
                    End While
                    'code modified on 20th apr-2008
                    'If Shift_count > 1 Then
                    '    ddlShift.Enabled = True
                    'Else
                    '    ddlShift.Enabled = False
                    'End If
                    'end

                    lblErrorInfo.Visible = False
                    ViewState("Acd_Valid") = "1" 'if open for registration
                    ddlShift_SelectedIndexChanged(ddlShift, Nothing)
                Else
                    lblErrorInfo.Visible = True

                    lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Record currently not available.Please contact system Admin.</div>"

                    ddlStream.Items.Clear()
                    ddlTerm.Items.Clear()
                End If

            End Using

        Else

            Using AllGrade_Shift_reader As SqlDataReader = AccessStudentClass.getAllShift_4_Grade(Aca_Year, Curri, GEMSSchool, Grade)
                Dim di_Grade_Shift As ListItem
                Dim Shift_count As Integer
                ddlShift.Items.Clear()

                If AllGrade_Shift_reader.HasRows = True Then
                    While AllGrade_Shift_reader.Read
                        di_Grade_Shift = New ListItem(AllGrade_Shift_reader("SHF_DESCR"), AllGrade_Shift_reader("SHF_ID"))
                        ddlShift.Items.Add(di_Grade_Shift)
                        Shift_count += 1
                        hfcutoff.Value = AllGrade_Shift_reader("CUTOFF")
                    End While
                    'code modified on 20th apr-2008
                    'If Shift_count > 1 Then
                    '    ddlShift.Enabled = True
                    'Else
                    '    ddlShift.Enabled = False
                    'End If
                    'end

                    lblErrorInfo.Visible = False
                    ViewState("Acd_Valid") = "1" 'if open for registration
                    ddlShift_SelectedIndexChanged(ddlShift, Nothing)
                Else
                    lblErrorInfo.Visible = True

                    lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Online Registration is CLOSED for this Search query.Please contact the school.</div>"

                    ddlStream.Items.Clear()
                    ddlTerm.Items.Clear()
                End If

            End Using

        End If
    End Sub
    Sub GetStudent_Stream()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim Grade As String = String.Empty
        Dim SHF_ID As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If


        If ddlGrade.SelectedIndex = -1 Then
            Grade = ""
        Else
            Grade = ddlGrade.SelectedItem.Value
        End If

        If ddlShift.SelectedIndex = -1 Then
            SHF_ID = ""
        Else
            SHF_ID = ddlShift.SelectedItem.Value
        End If

        'If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
        If ViewState("ENQTYPE") = "R" Then
            Using Student_Stream_reader As SqlDataReader = AccessStudentClass.getPick_Stream_Only_Registrar(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID)
                Dim di_Stream As ListItem
                Dim Stream_Count As Integer
                ddlStream.Items.Clear()

                If Student_Stream_reader.HasRows = True Then
                    While Student_Stream_reader.Read
                        di_Stream = New ListItem(Student_Stream_reader("STM_DESCR"), Student_Stream_reader("STM_ID"))
                        ddlStream.Items.Add(di_Stream)
                        Stream_Count = Stream_Count + 1
                    End While
                    'Code Modified 20-apr-2008
                    If Stream_Count > 1 Then
                        trStream.Visible = True
                    Else
                        trStream.Visible = False
                    End If
                    'End
                    'End
                    lblErrorInfo.Visible = False
                    ViewState("Acd_Valid") = "1" 'if open for registration
                Else
                    lblErrorInfo.Visible = True

                    lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Record currently not available.Please contact system Admin.</div>"
                    ddlTerm.Items.Clear()
                End If

            End Using


        Else
            Using Student_Stream_reader As SqlDataReader = AccessStudentClass.getPick_Stream_Only(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID)
                Dim di_Stream As ListItem
                Dim Stream_Count As Integer
                ddlStream.Items.Clear()

                If Student_Stream_reader.HasRows = True Then
                    While Student_Stream_reader.Read
                        di_Stream = New ListItem(Student_Stream_reader("STM_DESCR"), Student_Stream_reader("STM_ID"))
                        ddlStream.Items.Add(di_Stream)
                        Stream_Count = Stream_Count + 1
                    End While
                    'Code Modified 20-apr-2008
                    If Stream_Count > 1 Then
                        trStream.Visible = True
                    Else
                        trStream.Visible = False
                    End If
                    'End
                    lblErrorInfo.Visible = False
                    ViewState("Acd_Valid") = "1" 'if open for registration
                Else
                    lblErrorInfo.Visible = True

                    lblErrorInfo.Text = "<div style='border: 1px solid #1B80B6;width: 522px; text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #800000;padding:5pt;'>Online Registration is CLOSED for this Search query.Please contact the school.</div>"
                    ddlTerm.Items.Clear()
                End If

            End Using
        End If
    End Sub
    Sub CheckTermDate()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim TRMID As String = String.Empty

        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value
        End If

        If ddlTerm.SelectedIndex = -1 Then
            TRMID = ""
        Else
            TRMID = ddlTerm.SelectedItem.Value
        End If
        Using Term_4ACD_reader As SqlDataReader = AccessStudentClass.GetTerm_CheckDate(Aca_Year, Curri, GEMSSchool, TRMID)

            If Term_4ACD_reader.HasRows = True Then
                While Term_4ACD_reader.Read

                    If IsDate(Term_4ACD_reader("Trm_startDate")) = True Then
                        ViewState("TRM_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("Trm_startDate")))).Replace("01/Jan/1900", "")
                    End If

                    If IsDate(Term_4ACD_reader("trm_endDate")) = True Then
                        ViewState("TRM_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("trm_endDate")))).Replace("01/Jan/1900", "")
                    End If

                End While


            End If
        End Using

    End Sub
    'getting the term for which currently opened BSU_ID academic year ID
    Sub GetTerm_4ACD()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim termDate()
        Dim i As Integer = 0
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value
        End If
        Using Term_4ACD_reader As SqlDataReader = AccessStudentClass.GetTerm_4ACD(Aca_Year, Curri, GEMSSchool)
            Dim di_Term As ListItem
            ddlTerm.Items.Clear()
            If Term_4ACD_reader.HasRows = True Then
                While Term_4ACD_reader.Read
                    di_Term = New ListItem(Term_4ACD_reader("TDescr"), Term_4ACD_reader("TRM_ID"))
                    ddlTerm.Items.Add(di_Term)
                    If IsDate(Term_4ACD_reader("Trm_startDate")) = True Then
                        ViewState("TRM_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("Trm_startDate")))).Replace("01/Jan/1900", "")
                        ReDim Preserve termDate(i)
                        termDate(i) = ViewState("TRM_STARTDT")
                        i = i + 1
                    End If
                    If IsDate(Term_4ACD_reader("trm_endDate")) = True Then
                        ViewState("TRM_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("trm_endDate")))).Replace("01/Jan/1900", "")
                    End If

                End While
            Else
                ViewState("TRM_STARTDT") = ""
                ViewState("TRM_ENDDT") = ""
            End If
        End Using
        Session("termSTDT_online") = termDate
        ddlTerm_SelectedIndexChanged(ddlTerm, Nothing)
    End Sub
    Function ValidStaff_IDCheck() As Boolean
        Dim BSU_ID As String = String.Empty
        Dim Flag As Boolean = False

        If rdPri_Father.Checked = True Then
            If ddlFStaff_BSU.SelectedIndex = -1 Then
                BSU_ID = ""
            Else
                BSU_ID = ddlFStaff_BSU.SelectedItem.Value
            End If
        ElseIf rbPri_Mother.Checked = True Then
            If ddlFStaff_BSU.SelectedIndex = -1 Then
                BSU_ID = ""
            Else
                BSU_ID = ddlFStaff_BSU.SelectedItem.Value
            End If
        End If
        Using staffIDdatareader As SqlDataReader = AccessStudentClass.CheckStaffID_BSU(BSU_ID)
            While staffIDdatareader.Read
                Flag = Convert.ToBoolean(staffIDdatareader("BSU_bCheck_StaffID"))
            End While
        End Using
        ValidStaff_IDCheck = Flag
    End Function
    Sub OpenGEMS_STUD_BSU_Populate(ByVal BSU_IDs As String)

        Dim ds1 As DataSet = AccessStudentClass.GetOpen_BSU_Sib(BSU_IDs)
        'BSU_ID,BSU_NAME

        If ds1.Tables(0).Rows.Count > 0 Then
            ddlGemsSchools.Items.Clear()
            ddlGemsSchools.DataSource = ds1.Tables(0)
            ddlGemsSchools.DataTextField = "BSU_NAME"
            ddlGemsSchools.DataValueField = "BSU_ID"
            ddlGemsSchools.DataBind()
        End If

    End Sub
    Function Enquiry_transaction() As Integer
        Dim str As String = String.Empty
        Dim status As Integer
        Dim TEMP_ApplNo As String = String.Empty
        Dim TEMP_EQM_ENQID As Integer
        Dim EQM_PREVSCHOOL_GRD_ID As String = String.Empty
        Dim EQM_EQP_ID As String = String.Empty
        Dim EQM_EQMID As String = String.Empty
        Dim EQM_PREVSCHOOL_NURSERY As String = String.Empty
        Dim EQM_PREVSCHOOL_REG_ID As String = String.Empty
        Dim EQM_APPLNO As String = String.Empty
        Dim EQM_PREVSCHOOL_BSU_ID As String = String.Empty
        Dim EQM_PREVSCHOOL As String = String.Empty
        Dim EQM_PREVSCHOOL_CITY As String = String.Empty
        Dim EQM_PREVSCHOOL_CTY_ID As String = String.Empty
        Dim EQM_PREVSCHOOL_CLM_ID As String = String.Empty
        Dim EQM_PREVSCHOOL_MEDIUM As String = String.Empty
        Dim EQM_PREVSCHOOL_LASTATTDATE As Date
        Dim EQM_bPREVSCHOOLGEMS As Boolean
        Dim EQM_SIBLINGFEEID As String = String.Empty
        Dim EQM_SIBLINGSCHOOL As String = String.Empty
        Dim EQM_EX_STU_ID As String = String.Empty
        Dim EQM_EXUNIT As String = String.Empty
        Dim EQM_MODE_ID As String = String.Empty
        Dim EQM_ENQTYPE As String = ViewState("ENQTYPE")
        Dim EQM_STATUS As String = String.Empty
        Dim EQM_PREFCONTACT As String = String.Empty
        Dim EQM_PRIMARYCONTACT As String = String.Empty
        Dim EQM_STAFFUNIT As String = String.Empty
        Dim EQM_STAFF_EMP_ID As String = String.Empty
        Dim EQM_ENQDATE As String = String.Format("{0:" & OASISConstants.DateFormat & "}", Date.Today)
        Dim EQM_APPLFIRSTNAME As String = Trim(txtFname.Text)
        Dim EQM_APPLMIDNAME As String = Trim(txtMname.Text)
        Dim EQM_APPLLASTNAME As String = Trim(txtLname.Text)
        Dim ENQ_APPLPASSPORTNAME As String = Trim(txtFname.Text) & " " & Trim(txtMname.Text) & " " & Trim(txtLname.Text)
        Dim EQM_APPLDOB As Date = txtDob.Text
        Dim EQM_APPLGENDER As String = String.Empty
        If rdMale.Checked = True Then
            EQM_APPLGENDER = "M"
        ElseIf rdFemale.Checked = True Then
            EQM_APPLGENDER = "F"
        End If
        Dim EQM_REL_ID As String = ddlReligion.SelectedItem.Value
        Dim EQM_APPLNATIONALITY As String = ddlNational.SelectedItem.Value
        Dim EQM_APPLPOB As String = txtPob.Text
        Dim EQM_APPLCOB As String = ddlCountry.SelectedItem.Value
        Dim EQM_SAUDI_ID As String = String.Empty
        Dim EQM_APPLPASPRTNO As String = txtPassport.Text
        Dim EQM_APPLPASPRTISSDATE As Date
        If txtPassIss_date.Text <> "" Then
            EQM_APPLPASPRTISSDATE = txtPassIss_date.Text
        End If
        Dim EQM_APPLPASPRTEXPDATE As Date
        If txtPassExp_Date.Text <> "" Then
            EQM_APPLPASPRTEXPDATE = txtPassExp_Date.Text
        End If
        Dim EQM_APPLPASPRTISSPLACE As String = txtPassportIssue.Text
        Dim EQM_APPLVISANO As String = txtVisaNo.Text
        Dim EQM_APPLEMIRATES_ID As String = txtEMIRATES_ID.Text
        Dim EQM_APPLVISAISSDATE As Date = Date.MinValue
        Session("PassportNo_Enq") = txtPassport.Text
        If txtVisaIss_date.Text <> "" Then
            EQM_APPLVISAISSDATE = txtVisaIss_date.Text
        End If
        Dim EQM_APPLVISAEXPDATE As Date = Date.MinValue
        If txtVisaExp_date.Text <> "" Then
            EQM_APPLVISAEXPDATE = txtVisaExp_date.Text
        End If
        Dim EQM_APPLVISAISSPLACE As String = txtIss_Place.Text
        Dim EQM_APPLVISAISSAUTH As String = txtIss_Auth.Text
        Dim EQM_BLOODGROUP As String = ddlBgroup.SelectedValue
        Dim EQM_HEALTHCARDNO As String = txtHthNo.Text.Trim


        'check 4 is it KG1 or not 
        ' code modified on 22-apr-2008
        'EQM_PREVSCHOOL_NURSERY
        If (ViewState("Table_row") = "KG1") Or (ViewState("Table_row") = "PK") Or (ViewState("Table_row") = "PN1") Then
            EQM_PREVSCHOOL_NURSERY = ddlPreSchool_Nursery.SelectedItem.Value
            EQM_PREVSCHOOL_REG_ID = txtRegNo.Text
        Else
            If ViewState("rowstate") = "2" = True Then
                If ViewState("V_GEMS_STU") <> "" Then
                    If ViewState("orCheck") >= 1 Then '-----ddlGemsSchools
                        EQM_PREVSCHOOL_NURSERY = ddlGemsSchools.SelectedItem.Value
                        EQM_PREVSCHOOL_REG_ID = txtFeeID_GEMS.Text
                        'r1.Visible = False
                        'r3.Visible = False
                        'r12.Visible = True
                        'rfvFeeID_App.Visible = True
                    End If
                End If
            End If
            If ViewState("rowstate") = "1" Then
                If ddlGemsSchools.SelectedItem.Text = "OTHER" Then
                    EQM_bPREVSCHOOLGEMS = False
                    EQM_PREVSCHOOL = "" '' txtPre_School.Text
                    EQM_PREVSCHOOL_CITY = "" ''txtPre_City.Text
                Else
                    EQM_bPREVSCHOOLGEMS = True
                    EQM_PREVSCHOOL = "" '' txtPre_School.Text
                    EQM_PREVSCHOOL_CITY = "" ''txtPre_City.Text
                End If


                'If rdGemsGr.Checked = True Then
                '    EQM_PREVSCHOOL = ddlGemsSchools.SelectedItem.Text
                '    EQM_PREVSCHOOL_BSU_ID = ddlGemsSchools.SelectedItem.Value
                '    EQM_PREVSCHOOL_CITY = txtPre_City.Text
                'ElseIf rdOther.Checked = True Then

            End If
        End If


        ' ''EQM_PREVSCHOOL_CTY_ID = ddlPre_Country.SelectedItem.Value
        ' ''EQM_PREVSCHOOL_CLM_ID = ddlPre_Curriculum.SelectedItem.Value
        ' ''EQM_PREVSCHOOL_MEDIUM = txtMedInst.Text

        'If txtLast_Att.Text <> "" Then
        '    EQM_PREVSCHOOL_LASTATTDATE = txtLast_Att.Text
        'End If
        ''    EQM_PREVSCHOOL_GRD_ID = ddlPre_Grade.SelectedItem.Value
        ''End If


        Dim EQM_bAPPLSIBLING As Boolean = chkSibling.Checked
        Session("bAPPLSIBLING") = chkSibling.Checked

        Session("SIBLINGFEEID") = ""
        Session("SIBLINGSCHOOL") = ""

        If EQM_bAPPLSIBLING = True Then
            Dim query_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Sib_query As String = "SELECT  STU_FEE_ID  FROM STUDENT_M WHERE (STU_BSU_ID = '" & ddlSib_BSU.SelectedValue & "') AND ((STU_FEE_ID = '" & txtSib_ID.Text & "') OR (STU_NO = '" & txtSib_ID.Text & "'))"

            EQM_SIBLINGFEEID = CStr(SqlHelper.ExecuteScalar(query_conn, CommandType.Text, Sib_query))
            Session("SIBLINGFEEID") = EQM_SIBLINGFEEID
            EQM_SIBLINGSCHOOL = ddlSib_BSU.SelectedItem.Value
            Session("SIBLINGSCHOOL") = EQM_SIBLINGSCHOOL
            Session("Sibling_FeeID") = UCase(txtSib_Name.Text) & "/" & EQM_SIBLINGFEEID

        Else
            Session("Sibling_FeeID") = ""
        End If
        Session("ExStudName_Gems") = ""
        Dim EQM_bEXSTUDENT As String = chkFExStud_Gems.Checked
        If EQM_bEXSTUDENT = True Then
            Session("ExStudName_Gems") = txtFExStudName_Gems.Text
            'code modified by removing txtExStudFeeID_Gems.Text
            EQM_EX_STU_ID = "" 'txtExStudFeeID_Gems.Text
            EQM_EXUNIT = ddlFExStud_Gems.SelectedItem.Value
        End If


        Session("StaffID_ENQ") = ""
        Session("STAFF_NAME") = ""
        Dim EQM_bSTAFFGEMS As Boolean = False
        Dim STAFFID As String = String.Empty
        Dim STAFF_BSU_ID As String = String.Empty
        Dim STAFF_NAME As String = String.Empty
        If chkFStaff_GEMS.Checked Then
            EQM_bSTAFFGEMS = True
            STAFFID = txtFStaffID.Text
            STAFF_BSU_ID = ddlFStaff_BSU.SelectedItem.Value
            STAFF_NAME = txtFStaff_Name.Text
        ElseIf chkMStaff_GEMS.Checked Then
            STAFFID = txtMStaffID.Text
            STAFF_BSU_ID = ddlMStaff_BSU.SelectedItem.Value
            EQM_bSTAFFGEMS = True
            STAFF_NAME = txtMStaff_Name.Text
        End If



        If EQM_bSTAFFGEMS = True Then
            EQM_STAFFUNIT = STAFF_BSU_ID
            Session("STAFFUNIT") = STAFF_BSU_ID
            Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(STAFFID, STAFF_BSU_ID)
                Session("StaffID_ENQ") = STAFFID
                Session("STAFF_NAME") = STAFF_NAME
                If ValidStaff_reader.HasRows = True Then
                    While ValidStaff_reader.Read
                        EQM_STAFF_EMP_ID = Convert.ToString(ValidStaff_reader("EMP_ID"))
                    End While
                End If
            End Using

        End If




        ''Session("EQM_REMARKS") = txtNote.Text
        ''Session("EQM_bRCVSPMEDICATION") = rdMisc_Yes.Checked
        Dim EQM_REMARKS As String = ""

        Dim EQM_bRCVSPMEDICATION As Boolean
        Dim EQM_SPMEDICN As String = String.Empty

        Dim EQM_bRCVMAIL As Boolean = chkEAdd.Checked
        Dim EQM_bRCVSMS As Boolean = chksms.Checked
        Dim EQM_bRCVPUBL As Boolean = chkPubl.Checked
        EQM_PREFCONTACT = ddlFPref_contact.SelectedItem.Value

        If rdPri_Father.Checked Then
            EQM_PRIMARYCONTACT = "F"
        ElseIf rbPri_Mother.Checked Then
            EQM_PRIMARYCONTACT = "M"
        ElseIf rdPri_Guard.Checked Then
            EQM_PRIMARYCONTACT = "G"
        End If

        Dim EQM_EMGCONTACT As String = String.Empty
        'If txtStud_Contact_Country.Text <> "" And txtStud_Contact_Area.Text <> "" And txtStud_Contact_No.Text <> "" Then
        EQM_EMGCONTACT = txtStud_Contact_Country.Text & "-" & txtStud_Contact_Area.Text & "-" & txtStud_Contact_No.Text
        'Else
        'EQM_EMGCONTACT = ""
        'End If

        Dim EQM_FILLED_BY As String = String.Empty
        Dim EQM_AGENT_NAME As String = String.Empty
        Dim EQM_AGENT_MOB As String = String.Empty
        If rbParentF.Checked = True Then
            EQM_FILLED_BY = "P"

        ElseIf rbRelocAgent.Checked = True Then
            EQM_FILLED_BY = "A"
            EQM_AGENT_NAME = txtAgentName.Text
            EQM_AGENT_MOB = txtMAgent_country.Text & "-" & txtMAgent_Area.Text & "-" & txtMAgent_No.Text
        End If






        Dim EQM_TRM_ID As String = String.Empty
        If ddlTerm.SelectedIndex = -1 Then
            EQM_TRM_ID = ""
        Else
            EQM_TRM_ID = ddlTerm.SelectedItem.Value
        End If


        Dim EQM_FIRSTLANG As String = ddlFLang.SelectedValue
        Dim EQM_OTHLANG As String = String.Empty

        For Each item As ListItem In chkOLang.Items
            If (item.Selected) Then

                EQM_OTHLANG = EQM_OTHLANG + item.Value + "|"
            End If
        Next

        Dim EQM_bMEAL As Boolean

        If rbMealYes.Checked = True Then
            EQM_bMEAL = True
        ElseIf rbMealNo.Checked = True Then
            EQM_bMEAL = False
        End If
        Dim EQM_MODE_NOTE As String = txtMODE_NOTE.Text.Trim 'is choosen other

        Dim trans As SqlTransaction

        'code to check for duplicate entry in the enquiry form


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            trans = conn.BeginTransaction("SampleTransaction")

            Try


                status = AccessStudentClass.SaveStudENQUIRY_M_NEW(EQM_EQMID, UCase(EQM_ENQDATE), UCase(EQM_APPLFIRSTNAME), UCase(EQM_APPLMIDNAME), UCase(EQM_APPLLASTNAME), UCase(ENQ_APPLPASSPORTNAME), _
                        UCase(EQM_APPLDOB), UCase(EQM_APPLGENDER), EQM_REL_ID, UCase(EQM_APPLNATIONALITY), UCase(EQM_APPLPOB), UCase(EQM_APPLCOB), UCase(EQM_APPLPASPRTNO), UCase(EQM_APPLPASPRTISSDATE), _
                          UCase(EQM_APPLPASPRTEXPDATE), UCase(EQM_APPLPASPRTISSPLACE), UCase(EQM_APPLVISANO), UCase(EQM_APPLVISAISSDATE), UCase(EQM_APPLVISAEXPDATE), _
                          UCase(EQM_APPLVISAISSPLACE), UCase(EQM_APPLVISAISSAUTH), EQM_bPREVSCHOOLGEMS, EQM_PREVSCHOOL_BSU_ID, UCase(EQM_PREVSCHOOL_NURSERY), UCase(EQM_PREVSCHOOL_REG_ID), UCase(EQM_PREVSCHOOL), _
                          UCase(EQM_PREVSCHOOL_CITY), EQM_PREVSCHOOL_CTY_ID, UCase(EQM_PREVSCHOOL_GRD_ID), EQM_PREVSCHOOL_CLM_ID, EQM_PREVSCHOOL_MEDIUM, UCase(EQM_PREVSCHOOL_LASTATTDATE), _
                          EQM_bAPPLSIBLING, UCase(EQM_SIBLINGFEEID), UCase(EQM_SIBLINGSCHOOL), EQM_bEXSTUDENT, UCase(EQM_EXUNIT), UCase(EQM_EX_STU_ID), _
                          EQM_bSTAFFGEMS, UCase(EQM_STAFFUNIT), EQM_STAFF_EMP_ID, EQM_bRCVSPMEDICATION, UCase(EQM_SPMEDICN), UCase(EQM_REMARKS), _
                          EQM_EQP_ID, EQM_bRCVSMS, EQM_bRCVMAIL, UCase(EQM_PRIMARYCONTACT), UCase(EQM_PREFCONTACT), UCase(EQM_MODE_ID), _
                         UCase(EQM_ENQTYPE), UCase(EQM_STATUS), EQM_EMGCONTACT, EQM_TRM_ID, EQM_FILLED_BY, UCase(EQM_AGENT_NAME), EQM_AGENT_MOB, EQM_FIRSTLANG, EQM_OTHLANG, EQM_bRCVPUBL, EQM_SAUDI_ID, _
                         EQM_BLOODGROUP, EQM_HEALTHCARDNO, ddlFee_sponsor.SelectedValue, EQM_bMEAL, EQM_APPLEMIRATES_ID, EQM_MODE_NOTE, TEMP_EQM_ENQID, trans)

                If status <> 0 Then
                    Throw New ArgumentException("Record could not be Inserted")

                Else

                    Dim EQP_ID As String = TEMP_EQM_ENQID
                    Dim EQP_FFIRSTNAME As String = String.Empty
                    Dim EQP_FMIDNAME As String = String.Empty
                    Dim EQP_FLASTNAME As String = String.Empty
                    Dim EQP_FNATIONALITY As String = String.Empty
                    Dim EQP_FNATIONALITY2 As String = String.Empty



                    Dim EQP_FCOMSTREET As String = String.Empty
                    Dim EQP_FCOMAREA As String = String.Empty
                    Dim EQP_FCOMBLDG As String = String.Empty
                    Dim EQP_FCOMAPARTNO As String = String.Empty
                    Dim EQP_FCOMPOBOX As String = String.Empty
                    Dim EQP_FCOMCITY As String = String.Empty
                    Dim EQP_FCOMSTATE As String = String.Empty
                    Dim EQP_FCOMCOUNTRY As String = String.Empty
                    Dim EQP_FOFFPHONECODE As String = String.Empty
                    Dim EQP_FOFFPHONE As String = String.Empty
                    Dim EQP_FRESPHONECODE As String = String.Empty
                    Dim EQP_FRESPHONE As String = String.Empty
                    Dim EQP_FFAXCODE As String = String.Empty
                    Dim EQP_FFAX As String = String.Empty
                    Dim EQP_FMOBILECODE As String = String.Empty
                    Dim EQP_FMOBILE As String = String.Empty
                    Dim EQP_FPRMADDR1 As String = String.Empty
                    Dim EQP_FPRMADDR2 As String = String.Empty
                    Dim EQP_FPRMPOBOX As String = String.Empty
                    'field added
                    Dim EQP_FCOMPOBOX_EMIR As String = String.Empty
                    Dim EQP_FPRMCITY As String = String.Empty
                    Dim EQP_FPRMCOUNTRY As String = String.Empty
                    Dim EQP_FPRMPHONE As String = String.Empty
                    Dim EQP_FOCC As String = String.Empty
                    Dim EQP_FCOMPANY As String = String.Empty
                    Dim EQP_FEMAIL As String = String.Empty
                    'FILED ADDED
                    Dim EQP_FACD_YEAR As String = String.Empty
                    Dim EQP_FMODE_ID As String = String.Empty

                    Dim EQP_BSU_ID_STAFF As String = String.Empty
                    Dim EQP_bFGEMSSTAFF As Boolean
                    Dim EQP_MFIRSTNAME As String = String.Empty
                    Dim EQP_MMIDNAME As String = String.Empty
                    Dim EQP_MLASTNAME As String = String.Empty
                    Dim EQP_MNATIONALITY As String = String.Empty
                    Dim EQP_MNATIONALITY2 As String = String.Empty
                    Dim EQP_MCOMSTREET As String = String.Empty
                    Dim EQP_MCOMAREA As String = String.Empty
                    Dim EQP_MCOMBLDG As String = String.Empty
                    Dim EQP_MCOMAPARTNO As String = String.Empty
                    Dim EQP_MCOMPOBOX As String = String.Empty
                    Dim EQP_MCOMCITY As String = String.Empty
                    Dim EQP_MCOMSTATE As String = String.Empty
                    Dim EQP_MCOMCOUNTRY As String = String.Empty
                    Dim EQP_MOFFPHONECODE As String = String.Empty
                    Dim EQP_MOFFPHONE As String = String.Empty
                    Dim EQP_MRESPHONECODE As String = String.Empty
                    Dim EQP_MRESPHONE As String = String.Empty
                    Dim EQP_MFAXCODE As String = String.Empty
                    Dim EQP_MFAX As String = String.Empty
                    Dim EQP_MMOBILECODE As String = String.Empty
                    Dim EQP_MMOBILE As String = String.Empty
                    Dim EQP_MPRMADDR1 As String = String.Empty
                    Dim EQP_MPRMADDR2 As String = String.Empty
                    Dim EQP_MPRMPOBOX As String = String.Empty
                    'field added
                    Dim EQP_MCOMPOBOX_EMIR As String = String.Empty
                    Dim EQP_MMODE_ID As String = String.Empty

                    Dim EQP_MPRMCITY As String = String.Empty
                    Dim EQP_MPRMCOUNTRY As String = String.Empty
                    Dim EQP_MPRMPHONE As String = String.Empty
                    Dim EQP_MOCC As String = String.Empty
                    Dim EQP_MCOMPANY As String = String.Empty
                    Dim EQP_MEMAIL As String = String.Empty
                    Dim EQP_MBSU_ID As String = String.Empty
                    'FIELD ADDED
                    Dim EQP_MACD_YEAR As String = String.Empty

                    Dim EQP_bMGEMSSTAFF As Boolean
                    Dim EQP_GFIRSTNAME As String = String.Empty
                    Dim EQP_GMIDNAME As String = String.Empty
                    Dim EQP_GLASTNAME As String = String.Empty
                    Dim EQP_GNATIONALITY As String = String.Empty
                    Dim EQP_GNATIONALITY2 As String = String.Empty


                    Dim EQP_GCOMSTREET As String = String.Empty
                    Dim EQP_GCOMAREA As String = String.Empty
                    Dim EQP_GCOMBLDG As String = String.Empty
                    Dim EQP_GCOMAPARTNO As String = String.Empty

                    Dim EQP_GCOMPOBOX As String = String.Empty
                    Dim EQP_GCOMCITY As String = String.Empty
                    Dim EQP_GCOMSTATE As String = String.Empty
                    Dim EQP_GCOMCOUNTRY As String = String.Empty
                    Dim EQP_GOFFPHONECODE As String = String.Empty
                    Dim EQP_GOFFPHONE As String = String.Empty
                    Dim EQP_GRESPHONECODE As String = String.Empty
                    Dim EQP_GRESPHONE As String = String.Empty
                    Dim EQP_GFAXCODE As String = String.Empty
                    Dim EQP_GFAX As String = String.Empty
                    Dim EQP_GMOBILECODE As String = String.Empty
                    Dim EQP_GMOBILE As String = String.Empty
                    Dim EQP_GPRMADDR1 As String = String.Empty
                    Dim EQP_GPRMADDR2 As String = String.Empty
                    Dim EQP_GPRMPOBOX As String = String.Empty

                    'field added
                    Dim EQP_GCOMPOBOX_EMIR As String = String.Empty
                    Dim EQP_GMODE_ID As String = String.Empty

                    Dim EQP_GPRMCITY As String = String.Empty
                    Dim EQP_GPRMCOUNTRY As String = String.Empty
                    Dim EQP_GPRMPHONE As String = String.Empty
                    Dim EQP_GOCC As String = String.Empty
                    Dim EQP_GCOMPANY As String = String.Empty
                    Dim EQP_GEMAIL As String = String.Empty
                    Dim EQP_GBSU_ID As String = String.Empty
                    'FIELD ADDED
                    Dim EQP_GACD_YEAR As String = String.Empty
                    Dim EQP_bGGEMSSTAFF As Boolean
                    Dim EQP_FCOMP_ID As String = String.Empty
                    Dim EQP_MCOMP_ID As String = String.Empty
                    Dim EQP_GCOMP_ID As String = String.Empty

                    Dim str_Aboutus As String = String.Empty
                    For Each item As ListItem In chkAboutUs.Items
                        If (item.Selected) Then

                            str_Aboutus = str_Aboutus & item.Value
                            str_Aboutus = str_Aboutus & "|"
                        End If
                    Next

                    Session("str_Aboutus") = str_Aboutus
                    EQP_FFIRSTNAME = txtFPri_Fname.Text
                    EQP_FMIDNAME = txtFPri_Mname.Text
                    EQP_FLASTNAME = txtFPri_Lname.Text
                    EQP_FNATIONALITY = ddlFPri_National1.SelectedItem.Value
                    EQP_FNATIONALITY2 = ddlFPri_National2.SelectedItem.Value
                    'Field added 19/aug/2008
                    EQP_FCOMSTREET = txtFStreet.Text
                    EQP_FCOMAREA = txtFArea.Text
                    EQP_FCOMBLDG = txtFBldg.Text
                    EQP_FCOMAPARTNO = txtFApartNo.Text
                    EQP_FCOMPOBOX = txtFPoboxLocal.Text
                    'Field added 19/aug/2008
                    EQP_FCOMPOBOX_EMIR = ddlFEmirate.SelectedItem.Value
                    EQP_FCOMCITY = txtFCity_pri.Text
                    EQP_FCOMSTATE = txtFCity_pri.Text
                    EQP_FCOMCOUNTRY = ddlFCountry_Pri.SelectedItem.Value
                    EQP_FOFFPHONECODE = txtFOPhone_Country.Text
                    'If txtOPhone_Country.Text <> "" And txtOPhone_Area.Text <> "" And txtOPhone.Text <> "" Then
                    EQP_FOFFPHONE = txtFOPhone_Country.Text & "-" & txtFOPhone_Area.Text & "-" & txtFOPhone.Text
                    'Else
                    '    EQP_FOFFPHONE = txtOPhone.Text
                    'End If
                    EQP_FRESPHONECODE = txtFHPhone_Country.Text
                    'If txtFHPhone_Country.Text <> "" And txtFHPhone_Area.Text <> "" And txtFHPhone.Text <> "" Then
                    EQP_FRESPHONE = txtFHPhone_Country.Text & "-" & txtFHPhone_Area.Text & "-" & txtFHPhone.Text
                    'Else
                    '    EQP_FRESPHONE = txtFHPhone.Text
                    'End If
                    EQP_FFAXCODE = txtFFaxNo_country.Text
                    'If txtFaxNo_country.Text <> "" And txtFaxNo_Area.Text <> "" And txtFaxNo.Text <> "" Then
                    EQP_FFAX = txtFFaxNo_country.Text & "-" & txtFFaxNo_Area.Text & " " & txtFFaxNo.Text
                    'Else
                    '    EQP_FFAX = txtFaxNo.Text
                    'End If
                    EQP_FMOBILECODE = txtFM_Country.Text
                    'If txtM_Country.Text <> "" And txtM_Area.Text <> "" And txtMobile_Pri.Text <> "" Then
                    EQP_FMOBILE = txtFM_Country.Text & "-" & txtFM_Area.Text & "-" & txtFMobile_Pri.Text
                    'Else
                    '    EQP_FMOBILE = txtMobile_Pri.Text
                    'End If
                    EQP_FPRMADDR1 = txtFAdd1_overSea.Text
                    EQP_FPRMADDR2 = txtFAdd2_overSea.Text
                    EQP_FPRMPOBOX = txtFPoBox_Pri.Text
                    EQP_FPRMCITY = txtFOverSeas_Add_City.Text
                    EQP_FPRMCOUNTRY = ddlFOverSeas_Add_Country.SelectedItem.Value
                    'If txtFPhone_Oversea_Country.Text <> "" And txtFPhone_Oversea_Area.Text <> "" And txtFPhone_Oversea_No.Text <> "" Then
                    EQP_FPRMPHONE = txtFPhone_Oversea_Country.Text & "-" & txtFPhone_Oversea_Area.Text & "-" & txtFPhone_Oversea_No.Text
                    'Else
                    '   EQP_FPRMPHONE = txtFPhone_Oversea_No.Text
                    ' End If
                    EQP_FOCC = txtFOccup.Text

                    'code updated 25-may-2008
                    If ddlFCompany.SelectedItem.Text = "Other" Then
                        EQP_FCOMP_ID = "0"
                        EQP_FCOMPANY = txtFComp.Text
                    Else
                        EQP_FCOMP_ID = ddlFCompany.SelectedItem.Value
                        EQP_FCOMPANY = ""
                    End If

                    EQP_FEMAIL = txtFEmail_Pri.Text.ToString.ToLower
                    EQP_bFGEMSSTAFF = chkFExStud_Gems.Checked
                    If EQP_bFGEMSSTAFF Then
                        EQP_FACD_YEAR = ddlFExYear.SelectedItem.Value
                        EQP_BSU_ID_STAFF = ddlFExStud_Gems.SelectedItem.Value
                    End If
                    EQP_FMODE_ID = str_Aboutus.ToString


                    EQP_MFIRSTNAME = txtMPri_Fname.Text
                    EQP_MMIDNAME = txtMPri_Mname.Text
                    EQP_MLASTNAME = txtMPri_Lname.Text
                    EQP_MNATIONALITY = ddlMPri_National1.SelectedItem.Value
                    EQP_MNATIONALITY2 = ddlMPri_National2.SelectedItem.Value

                    EQP_MCOMSTREET = txtMStreet.Text
                    EQP_MCOMAREA = txtMArea.Text
                    EQP_MCOMBLDG = txtMBldg.Text
                    EQP_MCOMAPARTNO = txtMApartNo.Text

                    EQP_MCOMCITY = txtMCity_pri.Text
                    EQP_MCOMSTATE = txtMCity_pri.Text
                    EQP_MCOMCOUNTRY = ddlMCountry_Pri.SelectedItem.Value

                    EQP_MCOMPOBOX = txtMPoboxLocal.Text
                    'Field added 19/aug/2008
                    EQP_MCOMPOBOX_EMIR = ddlMEmirate.SelectedItem.Value

                    EQP_MOFFPHONECODE = txtMOPhone_Country.Text

                    ' If txtFOPhone_Country.Text <> "" And txtMOPhone_Area.Text <> "" And txtMOPhone.Text <> "" Then

                    EQP_MOFFPHONE = txtMOPhone_Country.Text & "-" & txtMOPhone_Area.Text & "-" & txtMOPhone.Text
                    'ELse
                    'EQP_MOFFPHONE = txtFOPhone.Text
                    'End If
                    EQP_MRESPHONECODE = txtMHPhone_Country.Text
                    'If txtFHPhone_Country.Text <> "" And txtFHPhone_Area.Text <> "" And txtFHPhone.Text <> "" Then
                    EQP_MRESPHONE = txtMHPhone_Country.Text & "-" & txtMHPhone_Area.Text & "-" & txtMHPhone.Text
                    'Else
                    '    EQP_MRESPHONE = txtFHPhone.Text
                    'End If
                    EQP_MFAXCODE = txtMFaxNo_country.Text
                    ' If txtMFaxNo_country.Text <> "" And txtMFaxNo_Area.Text <> "" And txtMFaxNo.Text <> "" Then
                    EQP_MFAX = txtMFaxNo_country.Text & "-" & txtMFaxNo_Area.Text & "-" & txtMFaxNo.Text
                    'Else
                    '    EQP_MFAX = txtFFaxNo.Text
                    'End If
                    EQP_MMOBILECODE = txtMM_Country.Text
                    'If txtM_Country.Text <> "" And txtM_Area.Text <> "" And txtMobile_Pri.Text <> "" Then
                    EQP_MMOBILE = txtMM_Country.Text & "-" & txtMM_Area.Text & "-" & txtMMobile_Pri.Text
                    'Else
                    '    EQP_MMOBILE = txtMobile_Pri.Text
                    'End If
                    EQP_MPRMADDR1 = txtMAdd1_overSea.Text
                    EQP_MPRMADDR2 = txtMAdd2_overSea.Text
                    EQP_MPRMPOBOX = txtMPoBox_Pri.Text
                    EQP_MPRMCITY = txtMOverSeas_Add_City.Text
                    EQP_MPRMCOUNTRY = ddlMOverSeas_Add_Country.SelectedItem.Value
                    ' If txtFPhone_Oversea_Country.Text <> "" And txtFPhone_Oversea_Area.Text <> "" And txtFPhone_Oversea_No.Text <> "" Then
                    EQP_MPRMPHONE = txtMPhone_Oversea_Country.Text & "-" & txtMPhone_Oversea_Area.Text & "-" & txtMPhone_Oversea_No.Text
                    'Else
                    '    EQP_MPRMPHONE = txtFPhone_Oversea_No.Text
                    'End If
                    EQP_MOCC = txtMOccup.Text

                    'code updated 25-may-2008
                    If ddlMCompany.SelectedItem.Text = "Other" Then
                        EQP_MCOMP_ID = ""
                        EQP_MCOMPANY = txtMComp.Text
                    Else
                        EQP_MCOMP_ID = ddlMCompany.SelectedItem.Value
                        EQP_MCOMPANY = ""
                    End If
                    EQP_MEMAIL = txtMEmail_Pri.Text.ToString.ToLower


                    EQP_bMGEMSSTAFF = chkMExStud_Gems.Checked
                    If EQP_bMGEMSSTAFF Then
                        EQP_MACD_YEAR = ddlMExYear.SelectedItem.Value
                        EQP_BSU_ID_STAFF = ddlMExStud_Gems.SelectedItem.Value
                    End If
                    EQP_MMODE_ID = str_Aboutus.ToString

                    EQP_GFIRSTNAME = txtGPri_Fname.Text
                    EQP_GMIDNAME = txtGPri_Mname.Text
                    EQP_GLASTNAME = txtGPri_Lname.Text
                    EQP_GNATIONALITY = ddlGPri_National1.SelectedItem.Value
                    EQP_GNATIONALITY2 = ddlGPri_National2.SelectedItem.Value

                    EQP_GCOMSTREET = txtGStreet.Text
                    EQP_GCOMAREA = txtGArea.Text
                    EQP_GCOMBLDG = txtGBldg.Text
                    EQP_GCOMAPARTNO = txtGApartNo.Text

                    EQP_GCOMPOBOX = txtGPoboxLocal.Text
                    EQP_GCOMCITY = txtGCity_pri.Text
                    EQP_GCOMSTATE = txtGCity_pri.Text
                    EQP_GCOMCOUNTRY = ddlGCountry_Pri.SelectedItem.Value
                    EQP_GOFFPHONECODE = txtGOPhone_Country.Text
                    ' If txtFOPhone_Country.Text <> "" And txtFOPhone_Area.Text <> "" And txtFOPhone.Text <> "" Then
                    EQP_GOFFPHONE = txtGOPhone_Country.Text & "-" & txtGOPhone_Area.Text & "-" & txtGOPhone.Text
                    'Else
                    '    EQP_GOFFPHONE = txtFOPhone.Text
                    'End If
                    EQP_GRESPHONECODE = txtGHPhone_Country.Text
                    'If txtFHPhone_Country.Text <> "" And txtFHPhone_Area.Text <> "" And txtFHPhone.Text <> "" Then

                    EQP_GRESPHONE = txtGHPhone_Country.Text & "-" & txtGHPhone_Area.Text & "-" & txtGHPhone.Text
                    'Else
                    '    EQP_GRESPHONE = txtFHPhone.Text
                    'End If
                    EQP_GFAXCODE = txtGFaxNo_country.Text
                    'If txtFaxNo_country.Text <> "" And txtFaxNo_Area.Text <> "" And txtFaxNo.Text <> "" Then

                    EQP_GFAX = txtGFaxNo_country.Text & "-" & txtGFaxNo_Area.Text & "-" & txtGFaxNo.Text
                    'Else
                    '    EQP_GFAX = txtFaxNo.Text
                    'End If
                    EQP_GMOBILECODE = txtGM_Country.Text



                    ' If txtM_Country.Text <> "" And txtM_Area.Text <> "" And txtMobile_Pri.Text <> "" Then
                    EQP_GMOBILE = txtGM_Country.Text & "-" & txtGM_Area.Text & "-" & txtGMobile_Pri.Text
                    'Else
                    '    EQP_GMOBILE = txtMobile_Pri.Text
                    'End If

                    EQP_GPRMADDR1 = txtGAdd1_overSea.Text
                    EQP_GPRMADDR2 = txtGAdd2_overSea.Text
                    EQP_GPRMPOBOX = txtGPoBox_Pri.Text
                    'Field added 19/aug/2008
                    EQP_GCOMPOBOX_EMIR = ddlGEmirate.SelectedItem.Value

                    EQP_GPRMCITY = txtGOverSeas_Add_City.Text
                    EQP_GPRMCOUNTRY = ddlGOverSeas_Add_Country.SelectedItem.Value
                    ' If txtFPhone_Oversea_Country.Text <> "" And txtFPhone_Oversea_Area.Text <> "" And txtFPhone_Oversea_No.Text <> "" Then
                    EQP_GPRMPHONE = txtGPhone_Oversea_Country.Text & " " & txtGPhone_Oversea_Area.Text & " " & txtGPhone_Oversea_No.Text
                    'Else
                    '    EQP_GPRMPHONE = txtFPhone_Oversea_No.Text
                    'End If
                    EQP_GOCC = txtGOccup.Text
                    'code updated 25-may-2008

                    If ddlGCompany.SelectedItem.Text = "Other" Then
                        EQP_GCOMP_ID = ""
                        EQP_GCOMPANY = txtGComp.Text
                    Else
                        EQP_GCOMP_ID = ddlGCompany.SelectedItem.Value
                        EQP_GCOMPANY = ""
                    End If
                    EQP_GEMAIL = txtGEmail_Pri.Text.ToString.ToLower

                    EQP_bGGEMSSTAFF = False
                    If EQP_bGGEMSSTAFF Then
                        EQP_GACD_YEAR = ""
                        EQP_BSU_ID_STAFF = ""
                    End If
                    EQP_GMODE_ID = str_Aboutus.ToString


                    Dim EQP_FAMILY_CHK As String = String.Empty
                    If chkFam_Par_sep.Checked = True Then
                        EQP_FAMILY_CHK += "PAR_SEP|"
                    End If
                    If chkFam_Par_div.Checked = True Then
                        EQP_FAMILY_CHK += "PAR_DIV|"
                    End If

                    If chkFam_Fath_Des.Checked = True Then
                        EQP_FAMILY_CHK += "FAT_DES|"
                    End If
                    If chkFam_Moth_Des.Checked = True Then
                        EQP_FAMILY_CHK += "MOT_DES|"
                    End If
                    If chkFam_Oth.Checked = True Then
                        EQP_FAMILY_CHK += "OTH|"
                    End If

                    Dim EQP_FAMILY_NOTE As String = txtFamily_NOTE.Text.Trim 'is choosen other
                    Dim EQP_FAMILY_LEGAL As String = txtStu_Leg.Text.Trim
                    Dim EQP_FAMILY_LIVING As String = txtStu_Living.Text.Trim

                    status = AccessStudentClass.SaveStudENQUIRY_PARENT_M_NEW(TEMP_EQM_ENQID, UCase(EQP_FFIRSTNAME), UCase(EQP_FMIDNAME), UCase(EQP_FLASTNAME), UCase(EQP_FNATIONALITY), UCase(EQP_FNATIONALITY2), _
                                                   UCase(EQP_FCOMSTREET), UCase(EQP_FCOMAREA), UCase(EQP_FCOMBLDG), UCase(EQP_FCOMAPARTNO), EQP_FCOMPOBOX, UCase(EQP_FCOMCITY), UCase(EQP_FCOMSTATE), UCase(EQP_FCOMCOUNTRY), EQP_FOFFPHONECODE, EQP_FOFFPHONE, _
                                                   EQP_FRESPHONECODE, EQP_FRESPHONE, EQP_FFAXCODE, EQP_FFAX, EQP_FMOBILECODE, EQP_FMOBILE, UCase(EQP_FPRMADDR1), UCase(EQP_FPRMADDR2), EQP_FPRMPOBOX, _
                                                   UCase(EQP_FPRMCITY), UCase(EQP_FPRMCOUNTRY), EQP_FPRMPHONE, UCase(EQP_FOCC), UCase(EQP_FCOMPANY), EQP_FEMAIL, EQP_bFGEMSSTAFF, EQP_BSU_ID_STAFF, EQP_FACD_YEAR, _
                                                   UCase(EQP_MFIRSTNAME), UCase(EQP_MMIDNAME), UCase(EQP_MLASTNAME), UCase(EQP_MNATIONALITY), UCase(EQP_MNATIONALITY2), UCase(EQP_MCOMSTREET), UCase(EQP_MCOMAREA), UCase(EQP_MCOMBLDG), UCase(EQP_MCOMAPARTNO), EQP_MCOMPOBOX, _
                                                   UCase(EQP_MCOMCITY), UCase(EQP_MCOMSTATE), UCase(EQP_MCOMCOUNTRY), EQP_MOFFPHONECODE, EQP_MOFFPHONE, EQP_MRESPHONECODE, EQP_MRESPHONE, _
                                                   EQP_MFAXCODE, EQP_MFAX, EQP_MMOBILECODE, EQP_MMOBILE, UCase(EQP_MPRMADDR1), UCase(EQP_MPRMADDR2), EQP_MPRMPOBOX, _
                                                   UCase(EQP_MPRMCITY), UCase(EQP_MPRMCOUNTRY), EQP_MPRMPHONE, UCase(EQP_MOCC), UCase(EQP_MCOMPANY), EQP_bMGEMSSTAFF, _
                                                   EQP_MBSU_ID, EQP_MACD_YEAR, UCase(EQP_GFIRSTNAME), UCase(EQP_GMIDNAME), UCase(EQP_GLASTNAME), UCase(EQP_GNATIONALITY), UCase(EQP_GNATIONALITY2), _
                                                   UCase(EQP_GCOMSTREET), UCase(EQP_GCOMAREA), UCase(EQP_GCOMBLDG), UCase(EQP_GCOMAPARTNO), EQP_GCOMPOBOX, UCase(EQP_GCOMCITY), UCase(EQP_GCOMSTATE), UCase(EQP_GCOMCOUNTRY), EQP_GOFFPHONECODE, _
                                                   EQP_GOFFPHONE, EQP_GRESPHONECODE, EQP_GRESPHONE, EQP_GFAXCODE, EQP_GFAX, EQP_GMOBILECODE, EQP_GMOBILE, UCase(EQP_GPRMADDR1), _
                                                   UCase(EQP_GPRMADDR2), EQP_GPRMPOBOX, UCase(EQP_GPRMCITY), UCase(EQP_GPRMCOUNTRY), EQP_GPRMPHONE, UCase(EQP_GOCC), UCase(EQP_GCOMPANY), EQP_bGGEMSSTAFF, _
                                                   EQP_GBSU_ID, EQP_GACD_YEAR, EQP_GEMAIL, EQP_MEMAIL, UCase(EQP_FCOMP_ID), UCase(EQP_MCOMP_ID), UCase(EQP_GCOMP_ID), EQP_FCOMPOBOX_EMIR, EQP_MCOMPOBOX_EMIR, EQP_GCOMPOBOX_EMIR, EQP_FMODE_ID, EQP_MMODE_ID, EQP_GMODE_ID, EQP_FAMILY_NOTE, _
                                      ddlFCont_Status.SelectedValue, ddlMCont_Status.SelectedValue, ddlGCont_Status.SelectedValue, EQP_FAMILY_CHK, EQP_FAMILY_LEGAL, EQP_FAMILY_LIVING, trans)



                    If status <> 0 Then
                        Throw New ArgumentException("Record could not be Inserted")

                    Else



                        Dim EQS_EQM_ENQID As String = TEMP_EQM_ENQID
                        Dim EQS_ACY_ID As String = ddlAca_Year.SelectedItem.Value
                        Dim EQS_BSU_ID As String = ddlGEMSSchool.SelectedItem.Value
                        Dim EQS_GRD_ID As String = ddlGrade.SelectedItem.Value
                        Dim EQS_SHF_ID As String = ddlShift.SelectedItem.Value
                        Dim EQS_bTPTREQD As Boolean = chkTran_Req.Checked
                        Dim EQS_LOC_ID As String = String.Empty
                        Dim EQS_SBL_ID As String = String.Empty
                        Dim EQS_PNT_ID As String = String.Empty
                        Dim EQS_TPTREMARKS As String = String.Empty
                        Dim EQS_STATUS As String = "NEW"
                        Dim EQS_STM_ID As String = ddlStream.SelectedItem.Value
                        Dim EQS_CLM_ID As Integer = ddlCurri.SelectedItem.Value
                        Dim EQS_DOJ As String = txtDOJ.Text
                        Dim EQS_RFD_ID As String = String.Empty
                        '-----Newly added fields--------------

                        Dim EQS_bALLERGIES As Boolean = IIf(rbHthAll_Yes.Checked = True, True, False)

                        Dim EQS_ALLERGIES As String = txtHthAll_Note.Text.Trim
                        Dim EQS_bRCVSPMEDICATION As Boolean = IIf(rbHthSM_Yes.Checked = True, True, False)
                        Dim EQS_SPMEDICN As String = txtHthSM_Note.Text
                        Dim EQS_bPRESTRICTIONS As Boolean = IIf(rbHthPER_Yes.Checked = True, True, False)
                        Dim EQS_PRESTRICTIONS As String = txtHthPER_Note.Text
                        Dim EQS_bHRESTRICTIONS As Boolean = IIf(rbHthOther_yes.Checked = True, True, False)
                        Dim EQS_HRESTRICTIONS As String = txtHthOth_Note.Text
                        Dim EQS_bTHERAPHY As Boolean = IIf(rbHthLS_Yes.Checked = True, True, False)
                        Dim EQS_THERAPHY As String = txtHthLS_Note.Text
                        Dim EQS_bSPEDUCATION As Boolean = IIf(rbHthSE_Yes.Checked = True, True, False)
                        Dim EQS_SPEDUCATION As String = txtHthSE_Note.Text
                        Dim EQS_bEAL As Boolean = IIf(rbHthEAL_Yes.Checked = True, True, False)
                        Dim EQS_EAL As String = txtHthEAL_Note.Text
                        Dim EQS_bMUSICAL As Boolean = IIf(rbHthMus_Yes.Checked = True, True, False)
                        Dim EQS_MUSICAL As String = txtHthMus_Note.Text
                        Dim EQS_bENRICH As Boolean = IIf(rbHthEnr_Yes.Checked = True, True, False)
                        Dim EQS_ENRICH As String = txtHthEnr_note.Text
                        Dim EQS_bBEHAVIOUR As Boolean = IIf(rbHthBehv_Yes.Checked = True, True, False)
                        Dim EQS_BEHAVIOUR As String = txtHthBehv_Note.Text
                        Dim EQS_bSPORTS As Boolean = IIf(rbHthSport_Yes.Checked = True, True, False)
                        Dim EQS_SPORTS As String = txtHthSport_note.Text
                        Dim EQS_ETHNICITY As String = ddlEthnicity.SelectedValue
                        Dim EQS_ETHNICITY_OTH As String = txtEthnicity.Text.Trim
                        Dim EQS_ENG_READING As String = String.Empty
                        Dim EQS_ENG_WRITING As String = String.Empty
                        Dim EQS_ENG_SPEAKING As String = String.Empty
                        Dim EQS_bREP_GRD As Boolean = IIf(rbRepGRD_Yes.Checked = True, True, False)
                        Dim EQS_REP_GRD As String = txtRep_GRD.Text.Trim

                        Dim EQS_bCommInt As Boolean = IIf(rbHthComm_Yes.Checked = True, True, False)
                        Dim EQS_CommInt As String = txtHthCommInt_Note.Text

                        Dim EQS_bDisabled As Boolean = IIf(rbHthDisabled_YES.Checked = True, True, False)
                        Dim EQS_Disabled As String = txtHthDisabled_Note.Text

                        If rbWExc.Checked = True Then
                            EQS_ENG_WRITING = 1
                        ElseIf rbWGood.Checked = True Then
                            EQS_ENG_WRITING = 2
                        ElseIf rbWFair.Checked = True Then
                            EQS_ENG_WRITING = 3
                        ElseIf rbWPoor.Checked = True Then
                            EQS_ENG_WRITING = 4
                        Else
                            EQS_ENG_WRITING = 0
                        End If


                        If rbSExc.Checked = True Then
                            EQS_ENG_SPEAKING = 1
                        ElseIf rbSGood.Checked = True Then
                            EQS_ENG_SPEAKING = 2
                        ElseIf rbSFair.Checked = True Then
                            EQS_ENG_SPEAKING = 3
                        ElseIf rbSPoor.Checked = True Then
                            EQS_ENG_SPEAKING = 4
                        Else
                            EQS_ENG_SPEAKING = 0
                        End If

                        If rbRExc.Checked = True Then
                            EQS_ENG_READING = 1
                        ElseIf rbRGood.Checked = True Then
                            EQS_ENG_READING = 2
                        ElseIf rbRFair.Checked = True Then
                            EQS_ENG_READING = 3
                        ElseIf rbRPoor.Checked = True Then
                            EQS_ENG_READING = 4
                        Else
                            EQS_ENG_READING = 0
                        End If





                      

                        '------------------previous school Fields
                        Dim Prev_sch_id As String = String.Empty
                        Dim prev_SCH_name As String = String.Empty
                        Dim prev_sch_head As String = String.Empty
                        Dim prev_SCH_FEE_ID As String = String.Empty
                        Dim prev_SCH_GRADE As String = String.Empty
                        Dim prev_SCH_LEARN_INS As String = String.Empty
                        Dim prev_SCH_ADDR As String = String.Empty
                        Dim prev_SCH_CURR As String = String.Empty
                        Dim prev_SCH_CITY As String = String.Empty
                        Dim prev_SCH_COUNTRY As String = String.Empty
                        Dim prev_SCH_PHONE As String = String.Empty
                        Dim prev_SCH_FAX As String = String.Empty
                        Dim prev_SCH_FROMDT As String = String.Empty
                        Dim prev_SCH_TODT As String = String.Empty
                        Dim prev_SCH_TYPE As String = String.Empty

                        '-----------end of newly added field-----------------






                        Session("ENQ_RFD_ID") = EQS_RFD_ID


                        If rbRefYes.Checked = True Then
                            EQS_RFD_ID = ViewState("RFD_ID")
                        End If



                        If EQS_bTPTREQD Then
                            If ddlMainLocation.SelectedIndex <> -1 Then
                                EQS_LOC_ID = ddlMainLocation.SelectedItem.Value
                            End If
                            If ddlSubLocation.SelectedIndex <> -1 Then
                                EQS_SBL_ID = ddlSubLocation.SelectedItem.Value
                            End If
                            If ddlPickup.SelectedIndex <> -1 Then
                                EQS_PNT_ID = ddlPickup.SelectedItem.Value
                            End If


                            EQS_TPTREMARKS = UCase(Trim(txtOthers.Text))

                        End If


                        status = AccessStudentClass.SaveStudENQUIRY_SCHOOLPRIO_S_NEW(EQS_EQM_ENQID, EQS_ACY_ID, EQS_BSU_ID, UCase(EQS_GRD_ID), EQS_SHF_ID, _
       EQS_bTPTREQD, EQS_LOC_ID, EQS_SBL_ID, EQS_PNT_ID, EQS_STM_ID, EQM_bAPPLSIBLING, UCase(EQM_SIBLINGFEEID), _
UCase(EQM_SIBLINGSCHOOL), EQS_TPTREMARKS, UCase(EQS_STATUS), UCase(EQM_REMARKS), EQS_CLM_ID, UCase(EQS_DOJ), EQS_RFD_ID, _
EQS_bALLERGIES, UCase(EQS_ALLERGIES), EQS_bRCVSPMEDICATION, UCase(EQS_SPMEDICN), EQS_bPRESTRICTIONS, UCase(EQS_PRESTRICTIONS), EQS_bHRESTRICTIONS, _
EQS_HRESTRICTIONS, EQS_bTHERAPHY, EQS_THERAPHY, EQS_bSPEDUCATION, _
 EQS_SPEDUCATION, EQS_bEAL, EQS_EAL, EQS_bMUSICAL, _
 EQS_MUSICAL, EQS_bENRICH, EQS_ENRICH, EQS_bBEHAVIOUR, _
 EQS_BEHAVIOUR, EQS_bSPORTS, EQS_SPORTS, EQS_ETHNICITY, _
  EQS_ETHNICITY_OTH, EQS_ENG_READING, EQS_ENG_WRITING, EQS_ENG_SPEAKING, _
TEMP_ApplNo, EQS_bREP_GRD, EQS_REP_GRD, EQS_bCommInt, EQS_CommInt, EQS_bDisabled, EQS_Disabled, trans)

                        If status <> 0 Then

                            Throw New ArgumentException(getErrorMessage(status))
                        End If

                        If Not ViewState("TABLE_School_Pre") Is Nothing Then
                            If ViewState("Table_row").ToString.Contains("PK|KG1") = False Then


                                If ViewState("TABLE_School_Pre").Rows.Count > 0 Then
                                    For i As Integer = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1


                                        Prev_sch_id = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("sch_id"))
                                        prev_SCH_name = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_NAME"))
                                        prev_sch_head = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_HEAD"))
                                        prev_SCH_FEE_ID = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_FEE_ID"))
                                        prev_SCH_GRADE = ViewState("TABLE_School_Pre").Rows(i)("SCH_GRADE")
                                        prev_SCH_LEARN_INS = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_LEARN_INS"))
                                        prev_SCH_ADDR = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_ADDR"))
                                        prev_SCH_CURR = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_CURR"))
                                        prev_SCH_CITY = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_CITY"))
                                        prev_SCH_COUNTRY = ViewState("TABLE_School_Pre").Rows(i)("SCH_COUNTRY")
                                        prev_SCH_PHONE = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_PHONE"))
                                        prev_SCH_FAX = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_FAX"))
                                        prev_SCH_FROMDT = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_FROMDT"))
                                        prev_SCH_TODT = Replace_InvalidXML(ViewState("TABLE_School_Pre").Rows(i)("SCH_TODT"))
                                        prev_SCH_TYPE = ViewState("TABLE_School_Pre").Rows(i)("SCH_TYPE")

                                        str += String.Format("<School sch_id='{0}' SCH_NAME='{1}' SCH_HEAD='{2}' SCH_FEE_ID='{3}' SCH_GRADE='{4}' SCH_LEARN_INS='{5}' SCH_ADDR='{6}' SCH_CURR='{7}' " & _
                                                             " SCH_CITY='{8}' SCH_COUNTRY='{9}' SCH_PHONE='{10}' SCH_FAX='{11}'  SCH_FROMDT='{12}' SCH_TODT='{13}'  SCH_TYPE='{14}' />", _
                                                            Prev_sch_id, prev_SCH_name, prev_sch_head, _
                                                      prev_SCH_FEE_ID, prev_SCH_GRADE, prev_SCH_LEARN_INS, _
                                                      prev_SCH_ADDR, prev_SCH_CURR, prev_SCH_CITY, prev_SCH_COUNTRY, _
                                        prev_SCH_PHONE, prev_SCH_FAX, _
                                        prev_SCH_FROMDT, prev_SCH_TODT, prev_SCH_TYPE)
                                    Next
                                    If str <> "" Then
                                        str = "<Schools>" + str + "</Schools>"

                                    End If



                                    Dim pParms(5) As SqlClient.SqlParameter
                                    pParms(0) = New SqlClient.SqlParameter("@STR", str)
                                    pParms(1) = New SqlClient.SqlParameter("@EPS_EQM_ENQID", EQS_EQM_ENQID)
                                    pParms(2) = New SqlClient.SqlParameter("@EPS_BSU_ID", EQS_BSU_ID)
                                    pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                                    pParms(3).Direction = ParameterDirection.ReturnValue
                                    SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveENQUIRY_PREVIOUS_SCHOOL", pParms)
                                    status = pParms(3).Value
                                    If status <> 0 Then
                                        Throw New ArgumentException(getErrorMessage(status))
                                    End If
                                End If

                            End If
                        End If



                    End If

                End If
                'return the ApplNo & Enq_ID
                ViewState("TEMP_ApplNo") = TEMP_ApplNo
                ViewState("Enq_ID") = TEMP_EQM_ENQID

                trans.Commit()

                lblError.Text = "Record Inserted Successfully"
                'Adding transaction info
                status = 0
                Return 0
            Catch myex As ArgumentException
                status = -1
                trans.Rollback()

                UtilityObj.Errorlog(myex.Message, "Save Enquiry_myex PN(" & txtPassport.Text & " ) BSU(" & ddlGEMSSchool.SelectedValue & ") str(" & str.ToString & ") ")


                lblError.Text = "Record could not be saved"
                Return -1
            Catch ex As Exception
                status = -1
                trans.Rollback()

                UtilityObj.Errorlog(ex.Message, "Save Enquiry PN(" & txtPassport.Text & " ) BSU(" & ddlGEMSSchool.SelectedValue & ") str(" & str.ToString & ")")
                lblError.Text = "Record could not be saved"
                Return -1
            Finally
               
                OnlineEnqAudit_log()

            End Try


        End Using


    End Function

    Private Sub OnlineEnqAudit_log()
        Try
            Dim clientIp1 As String = Request.UserHostAddress()
            Dim ClientBrw As String = String.Empty
            Dim mcName As String = String.Empty
            Dim MAC_ID As String = IRDataTables.GetMACAddress
            IRDataTables.GetClientInfo(ClientBrw, clientIp1, mcName)
            Dim clientIp2 As String = String.Empty
            Dim serverVar As New StringBuilder
            Dim Enq_ID As Integer
            Dim ApplNo As Integer
            Dim OASISUsr As String = String.Empty

            If Not ViewState("Enq_ID") Is Nothing Then
                Enq_ID = IIf(ViewState("Enq_ID").ToString.Trim = "", 0, ViewState("Enq_ID"))
            End If
            If Not ViewState("TEMP_ApplNo") Is Nothing Then
                ApplNo = IIf(ViewState("TEMP_ApplNo").ToString.Trim = "", 0, ViewState("TEMP_ApplNo"))
            End If

            If Not Session("sUsr_id") Is Nothing Then
                If Session("sUsr_id").ToString.Trim <> "" Then
                    OASISUsr = Session("sUsr_id")
                End If

            End If
            clientIp2 = Request.ServerVariables("HTTP_X_FORWARDED_FOR")

            If clientIp2 = "" Or clientIp2 Is Nothing Then
                clientIp2 = Request.ServerVariables("REMOTE_ADDR")
            End If
            Dim item
            For Each item In Request.ServerVariables
                serverVar.Append("<" & item & ">" & Request.ServerVariables(item) & "</" & item & ">")

            Next
            If Enq_ID <> 0 Then



                Dim conn As String = ConnectionManger.GetOASISConnectionString
                Dim param(12) As SqlParameter
                param(0) = New SqlParameter("@OEA_EQS_BSU_ID", ddlGEMSSchool.SelectedItem.Value)
                param(1) = New SqlParameter("@OEA_EQM_ENQID", Enq_ID)
                param(2) = New SqlParameter("@OEA_EQS_APPLNO", ApplNo)
                param(3) = New SqlParameter("@OEA_USR_ID", OASISUsr)
                param(4) = New SqlParameter("@OEA_CLIENTIP1", clientIp1)
                param(5) = New SqlParameter("@OEA_CLIENTIP2", clientIp2)
                param(6) = New SqlParameter("@OEA_CLIENTBRW", ClientBrw)
                param(7) = New SqlParameter("@OEA_MCNAME", mcName)
                param(8) = New SqlParameter("@OEA_MAC_ID", MAC_ID)
                param(9) = New SqlParameter("@OEA_SERVER_VARIABLE", serverVar.ToString)

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "ENQ.OnlineEnquiryAudit", param)

            End If
        Catch ex As Exception

        End Try

    End Sub
    Function Replace_InvalidXML(ByVal str As String) As String
        str = str.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("""", "&quot;").Replace("'", "&apos;")
        Return str
    End Function
    Function OpenOnlineCheck() As Boolean
        Dim NO_error As Boolean = True
        Dim OpenCheck As Integer = 0
        Dim commString As String = "The following condition required for online Student Registration: <UL>"

        If ViewState("orCheck") >= 1 Then


            If ViewState("V_GENDER") <> "" Then
                If ViewState("V_GENDER") = "Female Applicant" And rdFemale.Checked = False Then
                    commString = commString & "<LI>Online enquiry is opened for female applicant "
                    NO_error = False
                ElseIf ViewState("V_GENDER") = "Male Applicant" And rdMale.Checked = False Then

                    commString = commString & "<LI>Online enquiry is opened for male applicant "
                    NO_error = False
                End If


            ElseIf ViewState("V_SIB") <> "" Then
                If chkSibling.Checked = True And txtSib_ID.Text <> "" Then
                    If ViewState("Valid_Appl") = "1" Then
                        NO_error = True
                    Else
                        commString = commString & "<LI>Sibling Fee ID required in Student Details"
                        NO_error = False
                    End If
                Else
                    commString = commString & "<LI>Sibling Fee ID required in Student Details"
                    NO_error = False
                End If
                'ElseIf ViewState("V_GEMS_STU") <> "" Then

                '    If txtFeeID_GEMS.Text <> "" Then
                '        If ViewState("Valid_Prev_school") = "1" Then
                '            NO_error = True
                '        Else
                '            commString = commString & "<LI>Student Id required in Current School Details "
                '            NO_error = False
                '        End If
                '    Else
                '        commString = commString & "<LI>Student Id required in Current School Details"
                '        NO_error = False
                '    End If



            ElseIf ViewState("V_STAFF") <> "" Then

                If rdPri_Father.Checked = True And chkFStaff_GEMS.Checked = True And txtFStaffID.Text <> "" Then
                    If ViewState("Valid_Prim_cont") = "1" Then
                        NO_error = True
                    Else
                        commString = commString & "<LI>Staff ID required in Primary Contact "
                        NO_error = False
                    End If
                ElseIf rbPri_Mother.Checked = True And chkMStaff_GEMS.Checked = True And txtMStaffID.Text <> "" Then
                    If ViewState("Valid_Prim_cont") = "1" Then
                        NO_error = True
                    Else
                        commString = commString & "<LI>Staff ID required in Primary Contact "
                        NO_error = False
                    End If
                Else
                    commString = commString & "<LI>Staff  ID required in Primary Contact "
                    NO_error = False
                End If

            ElseIf ViewState("V_SISTER") <> "" Then

                Dim SISTER_BSUIDs As String = ViewState("S_SISTER_GEMS")

                If SISTER_BSUIDs.IndexOf(ddlPreSchool_Nursery.SelectedItem.Value) < 0 Then

                    commString = commString & "<LI>Select valid Sister Concern School "
                    NO_error = False
                End If
            End If
        ElseIf ViewState("orCheck") >= 1 Then

            If ViewState("V_SIB") <> "" Then
                If chkSibling.Checked = True And txtSib_ID.Text <> "" Then
                    If ViewState("Valid_Appl") = "1" Then
                        OpenCheck = 1
                    End If
                End If
            End If

            If ViewState("V_GEMS_STU") <> "" Then
                If txtFeeID_GEMS.Text <> "" Then
                    If ViewState("Valid_Prev_school") = "1" Then
                        OpenCheck += 1
                    End If
                End If
            End If



            If ViewState("V_STAFF") <> "" Then
                If rdPri_Father.Checked = True And chkFStaff_GEMS.Checked = True And txtFStaffID.Text <> "" Then
                    If ViewState("Valid_Prim_cont") = "1" Then
                        OpenCheck += 1
                    End If

                ElseIf rbPri_Mother.Checked = True And chkMStaff_GEMS.Checked = True And txtMStaffID.Text <> "" Then
                    If ViewState("Valid_Prim_cont") = "1" Then
                        OpenCheck += 1
                    End If
                End If


            End If

            If ViewState("V_SISTER") <> "" Then

                Dim SISTER_BSUIDs As String = ViewState("S_SISTER_GEMS")

                If SISTER_BSUIDs.IndexOf(ddlPreSchool_Nursery.SelectedValue) >= 0 Then

                    OpenCheck += 1

                End If
            End If

        End If



        If ViewState("orCheck") >= 1 Then
            If NO_error = True Then
                OpenOnlineCheck = True
            Else
                lblError.Text = commString & "</UL>"
                OpenOnlineCheck = False
            End If

            'ElseIf ViewState("orCheck") > 1 Then
            '    If OpenCheck > 0 Then
            '        OpenOnlineCheck = True
            '    Else
            '        lblError.Text = ViewState("ERROR_DETAIL") & "</UL>"
            '        OpenOnlineCheck = False
            '    End If

        End If

    End Function
#End Region

    Private Sub span_control_hide()
        '  FeeSp.Visible = False
        Fadd1.Visible = False
        FaddCity.Visible = False
        FaddCty.Visible = False
        FPhNo.Visible = False
        Fpobox.Visible = False
        Focc.Visible = False
        Fcomp.Visible = False

        Madd1.Visible = False
        MaddCity.Visible = False
        MaddCty.Visible = False
        MPhNo.Visible = False
        Mpobox.Visible = False
        Mocc.Visible = False
        Mcomp.Visible = False

        Gadd1.Visible = False
        GaddCity.Visible = False
        GaddCty.Visible = False
        GPhNo.Visible = False
        Gpobox.Visible = False
        Gocc.Visible = False
        Gcomp.Visible = False

    End Sub


    Sub Valid_Prim_Cont()
        span_control_hide()
        Dim CommStr As String = String.Empty
        CommStr = "<UL>"
        'code modified here
        ViewState("Valid_Prim_cont") = "1"




        ' ----FATHERS  CONTACT---
        If Trim(txtFPri_Fname.Text) <> "" Then
            If Not Regex.Match(Trim(txtFPri_Fname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = "<LI>Invalid fathers first name"
                ViewState("Valid_Prim_cont") = "-1"
            End If
        End If

        If Trim(txtFPri_Mname.Text) <> "" Then
            If Not Regex.Match(Trim(txtFPri_Mname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers middle name"
                ViewState("Valid_Prim_cont") = "-1"

            End If
        End If
        If Trim(txtFPri_Lname.Text) <> "" Then
            If Not Regex.Match(Trim(txtFPri_Lname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers last name"
                ViewState("Valid_Prim_cont") = "-1"
            End If
        End If






        ' ----motherS  CONTACT---
        If Trim(txtMPri_Fname.Text) <> "" Then
            If Not Regex.Match(Trim(txtMPri_Fname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = "<LI>Invalid mothers first name"
                ViewState("Valid_Prim_cont") = "-1"
            End If
        End If

        If Trim(txtMPri_Mname.Text) <> "" Then
            If Not Regex.Match(Trim(txtMPri_Mname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers middle name"
                ViewState("Valid_Prim_cont") = "-1"

            End If
        End If
        If Trim(txtMPri_Lname.Text) <> "" Then
            If Not Regex.Match(Trim(txtMPri_Lname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers last name"
                ViewState("Valid_Prim_cont") = "-1"
            End If
        End If
        ' ----GuardianS  CONTACT---
        If Trim(txtGPri_Fname.Text) <> "" Then
            If Not Regex.Match(Trim(txtGPri_Fname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = "<LI>Invalid guardians first name"
                ViewState("Valid_Prim_cont") = "-1"
            End If
        End If

        If Trim(txtGPri_Mname.Text) <> "" Then
            If Not Regex.Match(Trim(txtGPri_Mname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians middle name"
                ViewState("Valid_Prim_cont") = "-1"

            End If
        End If
        If Trim(txtGPri_Lname.Text) <> "" Then
            If Not Regex.Match(Trim(txtGPri_Lname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians last name"
                ViewState("Valid_Prim_cont") = "-1"
            End If
        End If

        '----father contact---
        Dim Enq_hash As New Hashtable
        If Not Session("BSU_Enq_Valid") Is Nothing Then
            Enq_hash = Session("BSU_Enq_Valid")
        End If

        If Enq_hash.ContainsKey("FEE_SP") = False Then
            If ddlFee_sponsor.SelectedItem.Text.Trim = "" Then
                CommStr = CommStr & "<LI>Fee sponsor detail required."
                ViewState("Valid_Prim_cont") = "-1"

            End If
        End If
        'Primary contact oversea address 1 validation----------new validation added

        If Enq_hash.ContainsKey("PCOADD1") = False Then
            If rdPri_Father.Checked Then
                If txtFAdd1_overSea.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent address 1 required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Fadd1.Visible = True
                    Madd1.Visible = False
                    Gadd1.Visible = False


                End If

            ElseIf rbPri_Mother.Checked Then
                If txtMAdd1_overSea.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent address 1 required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Fadd1.Visible = False
                    Madd1.Visible = True
                    Gadd1.Visible = False
                End If

            ElseIf rdPri_Guard.Checked Then
                If txtGAdd1_overSea.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent address 1 required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Fadd1.Visible = False
                    Madd1.Visible = False
                    Gadd1.Visible = True
                End If
            End If

        End If

        'Primary contact oversea city name validation----------new validation added
        If Enq_hash.ContainsKey("PCOST") = False Then
            If rdPri_Father.Checked Then
                If txtFOverSeas_Add_City.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent city name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FaddCity.Visible = True
                    MaddCity.Visible = False
                    GaddCity.Visible = False
                End If
            ElseIf rbPri_Mother.Checked Then
                If txtMOverSeas_Add_City.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent city name required "
                    ViewState("Valid_Prim_cont") = "-1"

                    FaddCity.Visible = False
                    MaddCity.Visible = True
                    GaddCity.Visible = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGOverSeas_Add_City.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent city name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FaddCity.Visible = False
                    MaddCity.Visible = False
                    GaddCity.Visible = True
                End If
            End If
        End If



        'Primary contact oversea country name validation----------new validation added
        If Enq_hash.ContainsKey("PCOCTY") = False Then
            If rdPri_Father.Checked Then
                If ddlFOverSeas_Add_Country.SelectedItem.Text = "" Or ddlFOverSeas_Add_Country.SelectedItem.Text = "-" Then
                    CommStr = CommStr & "<LI>Primary contact permanent country name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FaddCty.Visible = True
                    MaddCty.Visible = False
                    GaddCty.Visible = False

                End If
            ElseIf rbPri_Mother.Checked Then
                If ddlMOverSeas_Add_Country.SelectedItem.Text = "" Or ddlMOverSeas_Add_Country.SelectedItem.Text = "-" Then
                    CommStr = CommStr & "<LI>Primary contact permanent country name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FaddCty.Visible = False
                    MaddCty.Visible = True
                    GaddCty.Visible = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If ddlGOverSeas_Add_Country.SelectedItem.Text = "" Or ddlGOverSeas_Add_Country.SelectedItem.Text = "-" Then
                    CommStr = CommStr & "<LI>Primary contact permanent country name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FaddCty.Visible = False
                    MaddCty.Visible = False
                    GaddCty.Visible = True
                End If
            End If
        End If

        'Primary contact oversea phone no validation----------new validation added
        If Enq_hash.ContainsKey("PCOPH") = False Then
            If rdPri_Father.Checked Then
                If txtFPhone_Oversea_Country.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent phone country code required "
                    ViewState("Valid_Prim_cont") = "-1"

                    FPhNo.Visible = True
                    MPhNo.Visible = False
                    GPhNo.Visible = False

                End If
                If txtFPhone_Oversea_Area.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent phone area code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FPhNo.Visible = True
                    MPhNo.Visible = False
                    GPhNo.Visible = False
                End If
                If txtFPhone_Oversea_No.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent phone no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FPhNo.Visible = True
                    MPhNo.Visible = False
                    GPhNo.Visible = False
                End If

            ElseIf rbPri_Mother.Checked Then
                If txtMPhone_Oversea_Country.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent phone country code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FPhNo.Visible = False
                    MPhNo.Visible = True
                    GPhNo.Visible = False
                End If
                If txtMPhone_Oversea_Area.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent phone area code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FPhNo.Visible = False
                    MPhNo.Visible = True
                    GPhNo.Visible = False
                End If
                If txtMPhone_Oversea_No.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent phone no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FPhNo.Visible = False
                    MPhNo.Visible = True
                    GPhNo.Visible = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGPhone_Oversea_Country.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent phone country code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FPhNo.Visible = False
                    MPhNo.Visible = False
                    GPhNo.Visible = True
                End If
                If txtGPhone_Oversea_Area.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent phone area code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FPhNo.Visible = False
                    MPhNo.Visible = False
                    GPhNo.Visible = True
                End If
                If txtGPhone_Oversea_No.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent phone no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    FPhNo.Visible = False
                    MPhNo.Visible = False
                    GPhNo.Visible = True
                End If
            End If
        End If
        'Primary contact oversea PO Box no validation----------new validation added
        If Enq_hash.ContainsKey("PCOPO") = False Then
            If rdPri_Father.Checked Then
                If txtFPoBox_Pri.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent PO Box no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Fpobox.Visible = True
                    Mpobox.Visible = False
                    Gpobox.Visible = False
                End If
            ElseIf rbPri_Mother.Checked Then
                If txtMPoBox_Pri.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent PO Box no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Fpobox.Visible = False
                    Mpobox.Visible = True
                    Gpobox.Visible = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGPoBox_Pri.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact permanent PO Box no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Fpobox.Visible = False
                    Mpobox.Visible = False
                    Gpobox.Visible = True
                End If
            End If
        End If
        'Primary contact occupation  validation----------new validation added
        If Enq_hash.ContainsKey("PCOCC") = False Then
            If rdPri_Father.Checked Then
                If txtFOccup.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact occupation required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Focc.Visible = True
                    Mocc.Visible = False
                    Gocc.Visible = False
                End If
            ElseIf rbPri_Mother.Checked Then
                If txtMOccup.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact occupation required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Focc.Visible = False
                    Mocc.Visible = True
                    Gocc.Visible = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGOccup.Text.Trim = "" Then
                    CommStr = CommStr & "<LI>Primary contact occupation required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Focc.Visible = False
                    Mocc.Visible = False
                    Gocc.Visible = True
                End If
            End If
        End If

        'Primary contact company name  validation----------new validation added
        If Enq_hash.ContainsKey("PCCOMP") = False Then
            If rdPri_Father.Checked Then
                If txtFComp.Text.Trim = "" And ddlFCompany.SelectedItem.Text = "Other" Then
                    CommStr = CommStr & "<LI>Primary contact company name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Fcomp.Visible = True
                    Mcomp.Visible = False
                    Gcomp.Visible = False
                End If
            ElseIf rbPri_Mother.Checked Then
                If txtMComp.Text.Trim = "" And ddlMCompany.SelectedItem.Text = "Other" Then
                    CommStr = CommStr & "<LI>Primary contact company name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Fcomp.Visible = False
                    Mcomp.Visible = True
                    Gcomp.Visible = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGComp.Text.Trim = "" And ddlGCompany.SelectedItem.Text = "Other" Then
                    CommStr = CommStr & "<LI>Primary contact company name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Fcomp.Visible = False
                    Mcomp.Visible = False
                    Gcomp.Visible = True
                End If
            End If
        End If



        If ddlFPref_contact.SelectedItem.Text = "Home Phone" Then
            If txtFHPhone_Country.Text.Trim = "" Or txtFHPhone_Area.Text.Trim = "" Or txtFHPhone.Text.Trim = "" Then
                If rdPri_Father.Checked = True Then
                    CommStr = CommStr & "<LI>Fathers home contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                End If


            ElseIf Not Regex.Match(txtFHPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers home phone country code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtFHPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers home phone area code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtFHPhone.Text, "^[0-9]{1,10}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers home phone no."
                ViewState("Valid_Prim_cont") = "-1"

            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Office Phone" Then
            If txtFOPhone_Country.Text.Trim = "" Or txtFOPhone_Area.Text.Trim = "" Or txtFOPhone.Text.Trim = "" Then
                If rdPri_Father.Checked = True Then
                    CommStr = CommStr & "<LI>Fathers office contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                End If

            ElseIf Not Regex.Match(txtFOPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers office phone country code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtFOPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers office phone area code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtFOPhone.Text, "^[0-9]{1,10}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers office phone no."
                ViewState("Valid_Prim_cont") = "-1"

            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Mobile" Then
            If txtFM_Country.Text.Trim = "" Or txtFM_Area.Text.Trim = "" Or txtFMobile_Pri.Text.Trim = "" Then
                If rdPri_Father.Checked = True Then
                    CommStr = CommStr & "<LI>Fathers mobile number required"
                    ViewState("Valid_Prim_cont") = "-1"
                End If
            ElseIf Not Regex.Match(txtFM_Country.Text, "^[0-9]{1,3}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers mobile country code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtFM_Area.Text, "^[0-9]{1,4}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers mobile phone area code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtFMobile_Pri.Text, "^[0-9]{1,10}$").Success Then
                CommStr = CommStr & "<LI>Invalid fathers mobile phone no."
                ViewState("Valid_Prim_cont") = "-1"

            End If
        End If


        '----Mother contact---

        If ddlFPref_contact.SelectedItem.Text = "Home Phone" Then
            If txtMHPhone_Country.Text.Trim = "" Or txtMHPhone_Area.Text.Trim = "" Or txtMHPhone.Text.Trim = "" Then
                If rbPri_Mother.Checked = True Then
                    CommStr = CommStr & "<LI>Mothers home contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                End If
            ElseIf Not Regex.Match(txtMHPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers home phone country code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtMHPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers home phone area code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtMHPhone.Text, "^[0-9]{1,10}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers home phone no."
                ViewState("Valid_Prim_cont") = "-1"

            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Office Phone" Then
            If txtMOPhone_Country.Text.Trim = "" Or txtMOPhone_Area.Text.Trim = "" Or txtMOPhone.Text.Trim = "" Then
                If rbPri_Mother.Checked = True Then
                    CommStr = CommStr & "<LI>Mothers office contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                End If

            ElseIf Not Regex.Match(txtMOPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers office phone country code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtMOPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers office phone area code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtMOPhone.Text, "^[0-9]{1,10}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers office phone no."
                ViewState("Valid_Prim_cont") = "-1"

            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Mobile" Then
            If txtMM_Country.Text.Trim = "" Or txtMM_Area.Text.Trim = "" Or txtMMobile_Pri.Text.Trim = "" Then
                If rbPri_Mother.Checked = True Then
                    CommStr = CommStr & "<LI>Mothers mobile number required"
                    ViewState("Valid_Prim_cont") = "-1"
                End If
            ElseIf Not Regex.Match(txtMM_Country.Text, "^[0-9]{1,3}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers mobile country code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtMM_Area.Text, "^[0-9]{1,4}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers mobile phone area code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtMMobile_Pri.Text, "^[0-9]{1,10}$").Success Then
                CommStr = CommStr & "<LI>Invalid mothers mobile phone no."
                ViewState("Valid_Prim_cont") = "-1"

            End If
        End If

        '----Guardain contact---

        If ddlFPref_contact.SelectedItem.Text = "Home Phone" Then
            If txtGHPhone_Country.Text.Trim = "" Or txtGHPhone_Area.Text.Trim = "" Or txtGHPhone.Text.Trim = "" Then
                If rdPri_Guard.Checked = True Then
                    CommStr = CommStr & "<LI>Guardians home contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                End If
            ElseIf Not Regex.Match(txtGHPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians home phone country code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtGHPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians home phone area code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtGHPhone.Text, "^[0-9]{1,10}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians home phone no."
                ViewState("Valid_Prim_cont") = "-1"

            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Office Phone" Then
            If txtGOPhone_Country.Text.Trim = "" Or txtGOPhone_Area.Text.Trim = "" Or txtGOPhone.Text.Trim = "" Then
                If rdPri_Guard.Checked = True Then
                    CommStr = CommStr & "<LI>Guardians office contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                End If

            ElseIf Not Regex.Match(txtGOPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians office phone country code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtGOPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians office phone area code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtGOPhone.Text, "^[0-9]{1,10}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians office phone no."
                ViewState("Valid_Prim_cont") = "-1"

            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Mobile" Then
            If txtGM_Country.Text.Trim = "" Or txtGM_Area.Text.Trim = "" Or txtGMobile_Pri.Text.Trim = "" Then
                If rdPri_Guard.Checked = True Then
                    CommStr = CommStr & "<LI>Guardians mobile number required"
                    ViewState("Valid_Prim_cont") = "-1"
                End If

            ElseIf Not Regex.Match(txtGM_Country.Text, "^[0-9]{1,3}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians mobile country code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtGM_Area.Text, "^[0-9]{1,4}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians mobile phone area code"
                ViewState("Valid_Prim_cont") = "-1"

            ElseIf Not Regex.Match(txtGMobile_Pri.Text, "^[0-9]{1,10}$").Success Then
                CommStr = CommStr & "<LI>Invalid guardians mobile phone no."
                ViewState("Valid_Prim_cont") = "-1"

            End If
        End If





        ' If (Request.QueryString("MainMnu_code")) Is Nothing Then
        If ViewState("ENQTYPE") = "O" Then
            If ViewState("V_STAFF") <> "" Then
                If ViewState("orCheck") >= 1 Then
                    If rdPri_Father.Checked Then
                        chkFStaff_GEMS.Checked = True
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                                      "<script language=javascript>fill_Staffs();</script>")
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                                       "<script language=javascript>fill_Studs();</script>")
                    ElseIf rbPri_Mother.Checked() Then
                        chkMStaff_GEMS.Checked = True
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                                      "<script language=javascript>fill_MStaffs();</script>")
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                                       "<script language=javascript>fill_MStuds();</script>")
                    Else
                        chkFStaff_GEMS.Checked = True
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                                      "<script language=javascript>fill_Staffs();</script>")
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                                       "<script language=javascript>fill_Studs();</script>")
                    End If
                End If
            End If
        End If



        '--------------for FATHER Staff check (MAIN)-------------
        If chkFStaff_GEMS.Checked = True Then
            '--------------for Staff_Name check (A)-------------
            If Trim(txtFStaff_Name.Text) = "" Then
                CommStr = CommStr & "<LI>Father staff name required"
                ViewState("Valid_Prim_cont") = "-1"
                If mvRegMain.ActiveViewIndex = 2 Then
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                                   "<script language=javascript>fill_Staffs();</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                                   "<script language=javascript>fill_Studs();</script>")
                End If

            End If
            '--------------(A) End Here-------------

            If ValidStaff_IDCheck() = True Then
                '--------------for Staff_ID check (B)-------------
                If Trim(txtFStaffID.Text) = "" Then
                    CommStr = CommStr & "<LI>Father staff ID required"
                    ViewState("Valid_Prim_cont") = "-1"
                    If mvRegMain.ActiveViewIndex = 2 Then
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                                  "<script language=javascript>fill_Staffs();</script>")
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                                       "<script language=javascript>fill_Studs();</script>")
                    End If

                Else


                    '--------------for open for Staff check  (C)-------------

                    ' If (Request.QueryString("MainMnu_code")) Is Nothing Then

                    If ViewState("ENQTYPE") = "O" Then
                        If ViewState("V_STAFF") <> "" Then
                            Dim Staff_BSUIDs As String = ViewState("S_STAFF_GEMS")

                            '--------------for Staff_ID check from Selected BSU_IDs orCheck=1(D1)-------------

                            If Staff_BSUIDs.IndexOf(ddlFStaff_BSU.SelectedItem.Value) < 0 Then

                                CommStr = CommStr & "<LI>Online Enquiry is not open for the Staff from " & ddlFStaff_BSU.SelectedItem.Text
                                ViewState("Valid_Prim_cont") = "-1"
                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                                      "<script language=javascript>fill_Staffs();</script>")
                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                                               "<script language=javascript>fill_Studs();</script>")
                            Else
                                Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtFStaffID.Text, ddlFStaff_BSU.SelectedItem.Value)

                                    If ValidStaff_reader.HasRows = False Then
                                        CommStr = CommStr & "<LI>Father staff ID is not valid for the selected Business Unit"
                                        ViewState("Valid_Prim_cont") = "-1"
                                        If mvRegMain.ActiveViewIndex = 2 Then
                                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                                        "<script language=javascript>fill_Staffs();</script>")
                                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                                                           "<script language=javascript>fill_Studs();</script>")
                                        End If
                                    End If

                                End Using
                            End If
                            '--------------(D1) End Here-------------
                        Else
                            '--------------for open only for Staff check  (D2)-------------

                            Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtFStaffID.Text, ddlFStaff_BSU.SelectedItem.Value)

                                If ValidStaff_reader.HasRows = False Then
                                    CommStr = CommStr & "<LI>Father staff ID is not valid for the selected Business Unit"
                                    ViewState("Valid_Prim_cont") = "-1"
                                    If mvRegMain.ActiveViewIndex = 2 Then
                                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                                       "<script language=javascript>fill_Staffs();</script>")
                                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                                                       "<script language=javascript>fill_Studs();</script>")
                                    End If
                                End If

                            End Using
                            '--------------(D2) End Here-------------
                        End If
                        '-----------------------------------(C)End Here-----------------------------



                    Else

                        Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtFStaffID.Text, ddlFStaff_BSU.SelectedItem.Value)

                            If ValidStaff_reader.HasRows = False Then
                                CommStr = CommStr & "<LI>Father staff ID is not valid for the selected Business Unit"
                                ViewState("Valid_Prim_cont") = "-1"
                                If mvRegMain.ActiveViewIndex = 2 Then
                                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                                   "<script language=javascript>fill_Staffs();</script>")
                                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                                                   "<script language=javascript>fill_Studs();</script>")
                                End If
                            End If

                        End Using


                    End If

                End If
                '-----------------------------------(B)End Here-----------------------------
            End If
        End If
        '-----------------------------------(MAIN)End Here-----------------------------





        '--------------for Mother Staff check (MAIN)-------------
        If chkMStaff_GEMS.Checked = True Then
            '--------------for Staff_Name check (A)-------------
            If Trim(txtMStaff_Name.Text) = "" Then
                CommStr = CommStr & "<LI>Mothers staff name required"
                ViewState("Valid_Prim_cont") = "-1"
                If mvRegMain.ActiveViewIndex = 2 Then
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                                   "<script language=javascript>fill_MStaffs();</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                                   "<script language=javascript>fill_MStuds();</script>")
                End If

            End If
            '--------------(A) End Here-------------

            If ValidStaff_IDCheck() = True Then
                '--------------for Staff_ID check (B)-------------
                If Trim(txtMStaffID.Text) = "" Then
                    CommStr = CommStr & "<LI>Mother staff ID required"
                    ViewState("Valid_Prim_cont") = "-1"
                    If mvRegMain.ActiveViewIndex = 2 Then
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                                  "<script language=javascript>fill_MStaffs();</script>")
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                                       "<script language=javascript>fill_MStuds();</script>")
                    End If

                Else


                    '--------------for open for Staff check  (C)-------------

                    'If (Request.QueryString("MainMnu_code")) Is Nothing Then
                    If ViewState("ENQTYPE") = "O" Then

                        If ViewState("V_STAFF") <> "" Then
                            Dim Staff_BSUIDs As String = ViewState("S_STAFF_GEMS")

                            '--------------for Staff_ID check from Selected BSU_IDs orCheck=1(D1)-------------

                            If Staff_BSUIDs.IndexOf(ddlMStaff_BSU.SelectedItem.Value) < 0 Then

                                CommStr = CommStr & "<LI>Online Enquiry is not open for the Staff from " & ddlMStaff_BSU.SelectedItem.Text
                                ViewState("Valid_Prim_cont") = "-1"
                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                                      "<script language=javascript>fill_MStaffs();</script>")
                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                                               "<script language=javascript>fill_MStuds();</script>")
                            Else
                                Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtMStaffID.Text, ddlMStaff_BSU.SelectedItem.Value)

                                    If ValidStaff_reader.HasRows = False Then
                                        CommStr = CommStr & "<LI>Mother staff ID is not valid for the selected Business Unit"
                                        ViewState("Valid_Prim_cont") = "-1"
                                        If mvRegMain.ActiveViewIndex = 2 Then
                                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                                        "<script language=javascript>fill_MStaffs();</script>")
                                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                                                           "<script language=javascript>fill_MStuds();</script>")
                                        End If
                                    End If

                                End Using
                            End If
                            '--------------(D1) End Here-------------
                        Else
                            '--------------for open only for Staff check  (D2)-------------

                            Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtMStaffID.Text, ddlMStaff_BSU.SelectedItem.Value)

                                If ValidStaff_reader.HasRows = False Then
                                    CommStr = CommStr & "<LI>Mother staff ID is not valid for the selected Business Unit"
                                    ViewState("Valid_Prim_cont") = "-1"
                                    If mvRegMain.ActiveViewIndex = 2 Then
                                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                                       "<script language=javascript>fill_MStaffs();</script>")
                                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                                                       "<script language=javascript>fill_MStuds();</script>")
                                    End If
                                End If

                            End Using
                            '--------------(D2) End Here-------------
                        End If
                        '-----------------------------------(C)End Here-----------------------------


                    Else

                        Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtMStaffID.Text, ddlMStaff_BSU.SelectedItem.Value)

                            If ValidStaff_reader.HasRows = False Then
                                CommStr = CommStr & "<LI>Mother staff ID is not valid for the selected Business Unit"
                                ViewState("Valid_Prim_cont") = "-1"
                                If mvRegMain.ActiveViewIndex = 2 Then
                                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                                   "<script language=javascript>fill_MStaffs();</script>")
                                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                                                   "<script language=javascript>fill_MStuds();</script>")
                                End If
                            End If

                        End Using


                    End If

                End If
                '-----------------------------------(B)End Here-----------------------------
            End If
        End If
        '-----------------------------------(MAIN)End Here-----------------------------

        '--------------for ExStud_Gems simple check (K)-------------

        If chkFExStud_Gems.Checked = True Then
            If Trim(txtFExStudName_Gems.Text) = "" Then
                CommStr = CommStr & "<LI>Ex-Student detail cannot left empty under father contact information detail"
                ViewState("Valid_Prim_cont") = "-1"

                If mvRegMain.ActiveViewIndex = 2 Then
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_STAFF", _
                                   "<script language=javascript>fill_Staffs();</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_EXSTUD", _
                                   "<script language=javascript>fill_Studs();</script>")
                End If

            End If



        ElseIf chkMExStud_Gems.Checked = True Then
            If Trim(txtMExStudName_Gems.Text) = "" Then
                CommStr = CommStr & "<LI>Ex-Student detail cannot left empty under mother contact information detail"
                ViewState("Valid_Prim_cont") = "-1"

                If mvRegMain.ActiveViewIndex = 2 Then
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MSTAFF", _
                                   "<script language=javascript>fill_MStaffs();</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "PRIM_MEXSTUD", _
                                   "<script language=javascript>fill_MStuds();</script>")
                End If

            End If




        End If
        '-----------------------------------(K)End Here-----------------------------
        If ViewState("Valid_Prim_cont") = "-1" Then
            lblError.Text = CommStr & "</UL>"
            ViewState("Valid_Prim_cont") = "-1"
        Else
            ViewState("Valid_Prim_cont") = "1"
        End If

    End Sub

    Function Final_validation() As Boolean
        Dim Final_error As Boolean = True
        Dim DateValid_DOB As Date
        Dim DateValid_PIss_D As Date
        Dim DateValid_PExp_D As Date
        'Dim DateValid_VIss_D As Date
        'Dim DateValid_VExp_D As Date

        Dim commString As String = "The following fields needs to be validated: <UL>"

        'Select Schools validation
        Dim ChkCounter As Integer = 0


        If ddlAca_Year.SelectedIndex = -1 Or ddlGrade.SelectedIndex = -1 Or ddlCurri.SelectedIndex = -1 Or ddlShift.SelectedIndex = -1 Then    'And ddlTerm.Items.Count > 0 

            commString = commString & "<LI>Online Registration is CLOSED for this Search query"

            Final_error = False
            ViewState("Acd_Valid") = -1 'not completed

        Else
            ViewState("Acd_Valid") = 1 'completed
        End If

        If txtDOJ.Text.Trim <> "" Then
            Dim strfDate As String = txtDOJ.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                Final_error = False
                ViewState("Acd_Valid") = -1
                commString = commString & "<LI>Invalid tentative date of join"
            Else
                txtDOJ.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtDOJ.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                'check for the leap year date
                If Not IsDate(dateTime1) Then
                    Final_error = False
                    ViewState("Acd_Valid") = -1
                    commString = commString & "<LI>Invalid tentative date of join"
                End If
            End If
        End If


        If Trim(txtFname.Text) <> "" Then
            If Not Regex.Match(Trim(txtFname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid Student first name"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If

        If Trim(txtMname.Text) <> "" Then
            If Not Regex.Match(Trim(txtMname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid Student middle name"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If

        If Trim(txtLname.Text) <> "" Then
            If Not Regex.Match(Trim(txtLname.Text), "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid Student last name"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If




        'If (Request.QueryString("MainMnu_code")) Is Nothing Then
        If ViewState("ENQTYPE") = "O" Then
            If ViewState("V_SIB") <> "" Then
                If ViewState("orCheck") >= 1 Then
                    chkSibling.Checked = True
                    'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                    '                   "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                End If
            End If

        End If




        '--------------for SIBILING check (MAIN)-------------
        If chkSibling.Checked = True Then
            '--------------for SIBILING_Name check (A)-------------
            If Trim(txtSib_Name.Text) = "" Then
                commString = commString & "<LI>Sibling name cannot be left empty"
                ViewState("Valid_Appl") = "-1"
                Final_error = False

            End If

            '--------------(A) End Here-------------

            '--------------for STUDENT FEE_ID check (B)-------------
            If Trim(txtSib_ID.Text) = "" Then
                commString = commString & "<LI>Sibling Fee ID required"
                ViewState("Valid_Appl") = "-1"
                Final_error = False


            Else


                '  If (Request.QueryString("MainMnu_code")) Is Nothing Then
                If ViewState("ENQTYPE") = "O" Then
                    If ViewState("V_SIB") <> "" Then
                        Dim SIB_BSUIDs As String = ViewState("S_GEMS_SIBIL")

                        '--------------for Staff_ID check from Selected BSU_IDs orCheck=1(D1)-------------


                        If SIB_BSUIDs.IndexOf(ddlSib_BSU.SelectedItem.Value) < 0 Then
                            commString = commString & "<LI>Online Enquiry is not open for the Sibling from " & StrConv(ddlSib_BSU.SelectedItem.Text, VbStrConv.ProperCase)
                            ViewState("Valid_Appl") = "-1"
                            Final_error = False
                            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                            '           "<script language=javascript>fill_Sibls('" & chkSibling.ClientID & "');</script>")
                        Else
                            Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)

                                If ValidSibl_reader.HasRows = False Then
                                    commString = commString & "<LI>Sibling ID is not valid for the selected School"
                                    ViewState("Valid_Appl") = "-1"
                                    Final_error = False

                                End If

                            End Using
                        End If
                        '--------------(D1) End Here-------------


                    Else
                        '--------------for open only for Staff check  (D2)-------------

                        Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)

                            If ValidSibl_reader.HasRows = False Then
                                commString = commString & "<LI>Sibling Fee ID is not valid for the selected School"
                                ViewState("Valid_Appl") = "-1"
                                Final_error = False
                                'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                '   "<script language=javascript>fill_Sibls();</script>")
                                'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                                '                              "<script language=javascript>fill_Tran();</script>")

                            End If

                        End Using
                        '--------------(D2) End Here-------------
                    End If
                    '-----------------------------------(C)End Here-----------------------------


                Else
                    '------------code for registrar-------------------

                    Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)

                        If ValidSibl_reader.HasRows = False Then
                            commString = commString & "<LI>Sibling Fee ID is not valid for the selected School"
                            ViewState("Valid_Appl") = "-1"
                            Final_error = False
                            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                            '   "<script language=javascript>fill_Sibls();</script>")
                            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
                            '                              "<script language=javascript>fill_Tran();</script>")

                        End If

                    End Using

                    '------------code for registrar end here-------------------

                End If


            End If
            '-----------------------------------(B)End Here-----------------------------

        End If
        '-----------------------------------(MAIN)End Here-----------------------------


        'modified code to be added here

        Dim Enq_hash As New Hashtable
        If Not Session("BSU_Enq_Valid") Is Nothing Then
            Enq_hash = Session("BSU_Enq_Valid")
        End If



        If Enq_hash.ContainsKey("SFN") = False Then
            If txtFname.Text.Trim = "" Then
                commString = commString & "<LI>Student name cannot be left empty"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If

        End If


        If Enq_hash.ContainsKey("SRE") = False Then
            If ddlReligion.SelectedItem.Text = "" Then
                commString = commString & "<LI>Please Select Religion"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If





        If Enq_hash.ContainsKey("COB") = False Then
            If ddlCountry.SelectedItem.Text = "" Then
                commString = commString & "<LI>Please select Country of Birth"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If



        If ViewState("V_GENDER") <> "" Then
            If ViewState("orCheck") >= 1 Then
                If ViewState("V_GENDER") = "Female Applicant" And rdFemale.Checked = False Then
                    commString = commString & "<LI>Online enquiry is opened for female applicant only"
                    ViewState("Valid_Appl") = "-1"
                    Final_error = False
                ElseIf ViewState("V_GENDER") = "Male Applicant" And rdMale.Checked = False Then
                    commString = commString & "<LI>Online enquiry is opened for male applicant only"
                    ViewState("Valid_Appl") = "-1"
                    Final_error = False
                End If

            End If
        End If


        If Enq_hash.ContainsKey("NAT") = False Then

            If ddlNational.SelectedItem.Text = "" Then
                commString = commString & "<LI>Please select Student Nationality"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If

        If Enq_hash.ContainsKey("DOB") = False Then
            If txtDob.Text.Trim = "" Then
                commString = commString & "<LI>Student date of birth required"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            ElseIf IsDate(txtDob.Text) = False Then
                commString = commString & "<LI>Date of birth format is invalid"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            ElseIf txtDob.Text.Trim <> "" And IsDate(txtDob.Text) = True Then
                'code modified 4 date
                Dim strfDate As String = txtDob.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ViewState("Valid_Appl") = "-1"
                    Final_error = False
                    commString = commString & "<li>Invalid Date of birth"
                Else
                    txtDob.Text = strfDate
                    DateValid_DOB = Date.ParseExact(txtDob.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    If IsDate(DateValid_DOB) = False Then
                        commString = commString & "<LI>Invalid Date of birth"
                        ViewState("Valid_Appl") = "-1"
                        Final_error = False
                    End If
                    Dim cutoff As DateTime = hfcutoff.Value
                    Dim cutoff_Dob As DateTime = txtDob.Text
                    If cutoff_Dob > cutoff Then
                        ViewState("Valid_Appl") = "-1"
                        commString = commString & "<li>Applicant is under-age as on the cutoff date" & String.Format("{0:" & OASISConstants.DateFormat & "}", cutoff)
                        Final_error = False
                    End If
                End If
            ElseIf txtDob.Text.Trim <> "" And IsDate(txtDob.Text) = False Then
                commString = commString & "<LI>Date of birth format is invalid"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If

        If Enq_hash.ContainsKey("POB") = False Then
            If txtPob.Text.Trim = "" Then
                commString = commString & "<LI>Student place of birth required"
                ViewState("Valid_Appl") = "-1"
                Final_error = False

            End If
        End If


        'fIRST LANGUAGE validation----------new validation added

        If Enq_hash.ContainsKey("FLANG") = False Then
            If ddlFLang.SelectedItem.Text.Trim = "" Then
                commString = commString & "<LI>Please choose the first language."
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If

        'If txtStud_Contact_No.Text.Trim = "" Then
        '    commString = commString & "<LI>Student contact cannot be left empty"
        '    ViewState("Valid_Appl") = "-1"
        '    Final_error = False
        'End If

        ' End of select validation of Application Info


        'Validation  for passport/visa  Details
        If Enq_hash.ContainsKey("PPN") = False Then
            If txtPassport.Text.Trim = "" Then
                commString = commString & "<LI>Passport No. required"
                'ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If





        If Enq_hash.ContainsKey("PPIP") = False Then
            If txtPassportIssue.Text.Trim = "" Then
                commString = commString & "<LI>Passport issue place required"
                'ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            End If
        End If
        '----Passport issue date-----
        If Enq_hash.ContainsKey("PPID") = False Then
            If txtPassIss_date.Text.Trim = "" Then
                commString = commString & "<LI>Passport issue date required"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            ElseIf IsDate(txtPassIss_date.Text) = False Then
                commString = commString & "<LI>Passport Issue Date is not a vaild date"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            ElseIf txtPassIss_date.Text.Trim <> "" And IsDate(txtPassIss_date.Text) = True Then
                'code modified 4 date
                Dim strfDate As String = txtPassIss_date.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ViewState("Valid_Appl") = "-1"
                    Final_error = False
                    commString = commString & "<li>Passport Issue Date format is Invalid"
                Else
                    txtPassIss_date.Text = strfDate
                    DateValid_PIss_D = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    If IsDate(DateValid_PIss_D) = False Then
                        commString = commString & "<LI>Passport Issue Date Invalid"
                        ViewState("Valid_Appl") = "-1"
                        Final_error = False
                    End If
                End If

            ElseIf txtPassIss_date.Text.Trim <> "" And IsDate(txtPassIss_date.Text) = False Then
                commString = commString & "<LI>Date of birth format is invalid"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            ElseIf IsDate(txtPassIss_date.Text) = True Then
                If IsDate(txtDob.Text) = True Then
                    'code modified 4 date
                    Dim strfDate As String = txtPassIss_date.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        ViewState("Valid_Appl") = "-1"
                        Final_error = False
                        commString = commString & "<li>Passport Issue Date format is Invalid"
                    Else
                        txtPassIss_date.Text = strfDate
                        Dim strfDate1 As String = txtDob.Text.Trim
                        Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                        If str_err1 <> "" Then
                        Else
                            txtDob.Text = strfDate1
                            DateValid_DOB = Date.ParseExact(txtDob.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            DateValid_PIss_D = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            If DateTime.Compare(DateValid_DOB, DateValid_PIss_D) > 0 Then
                                commString = commString & "<LI>Passport Issue Date entered is not a valid date and must be greater than Date of Birth"
                                ViewState("Valid_Appl") = "-1"
                                Final_error = False
                            End If
                        End If
                    End If
                End If

            End If
        End If


        'validating passport expire  date

        'DateValid_PExp_D = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
        '---------Passport expiry date----------
        If Enq_hash.ContainsKey("PPED") = False Then
            If txtPassExp_Date.Text.Trim = "" Then
                commString = commString & "<LI>Passport expiry date required"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            ElseIf IsDate(txtPassExp_Date.Text) = False Then
                commString = commString & "<LI>Passport Expiry Date is not a vaild date"

                ViewState("Valid_Appl") = "-1"
                Final_error = False
            ElseIf txtPassExp_Date.Text.Trim <> "" And IsDate(txtPassExp_Date.Text) = True Then

                Dim strfDate As String = txtPassExp_Date.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ViewState("Valid_Appl") = "-1"
                    Final_error = False
                    commString = commString & "<li>Passport Expiry Date format is Invalid"
                Else
                    txtPassExp_Date.Text = strfDate

                    DateValid_PExp_D = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    If IsDate(DateValid_PExp_D) = False Then
                        commString = commString & "<LI>Date of birth format is invalid"
                        ViewState("Valid_Appl") = "-1"
                        Final_error = False
                    End If

                End If

            ElseIf IsDate(txtPassExp_Date.Text) = True Then


                If IsDate(txtPassIss_date.Text) = True Then

                    Dim strfDate As String = txtPassExp_Date.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        ViewState("Valid_Appl") = "-1"
                        Final_error = False
                        commString = commString & "<li>Passport Expiry Date format is Invalid"
                    Else
                        txtPassExp_Date.Text = strfDate
                        Dim strfDate1 As String = txtPassIss_date.Text.Trim
                        Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                        If str_err1 <> "" Then
                        Else
                            txtPassIss_date.Text = strfDate1
                            DateValid_PExp_D = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            DateValid_PIss_D = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            If DateTime.Compare(DateValid_PIss_D, DateValid_PExp_D) > 0 Then
                                commString = commString & "<LI>Passport Expiry  Date entered must be greater than Passport Issue Date"
                                ViewState("Valid_Appl") = "-1"
                                Final_error = False
                            End If

                        End If
                    End If

                End If


            End If
        End If


        If txtVisaIss_date.Text.Trim <> "" Then
            If IsDate(txtVisaIss_date.Text) = False Then
                commString = commString & "<LI>Visa Issue Date is not a vaild date"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            Else

                Dim strfDate As String = txtVisaIss_date.Text.Trim

                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ViewState("Valid_Appl") = "-1"
                    Final_error = False
                    commString = commString & "<li>Visa Issue Date format is Invalid"

                End If
            End If
        End If




        If txtVisaExp_date.Text.Trim <> "" Then
            ' DateValid_VExp_D = Date.ParseExact(txtVisaExp_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            If IsDate(txtVisaExp_date.Text) = False Then
                commString = commString & "<LI>Visa Expiry Date is not a vaild date"
                'ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = False
            ElseIf IsDate(txtVisaExp_date.Text) = True Then
                If IsDate(txtVisaIss_date.Text) = True Then

                    Dim strfDate As String = txtVisaExp_date.Text.Trim

                    Dim str_err As String = DateFunctions.checkdate(strfDate)

                    If str_err <> "" Then
                        ViewState("Valid_Appl") = "-1"
                        Final_error = False
                        commString = commString & "<li>Visa Expiry Date format is Invalid"
                    Else
                        txtVisaExp_date.Text = strfDate
                        Dim DateTime2 As Date
                        Dim dateTime1 As Date
                        Dim strfDate1 As String = txtVisaIss_date.Text.Trim
                        Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                        If str_err1 <> "" Then
                        Else
                            txtVisaIss_date.Text = strfDate1
                            DateTime2 = Date.ParseExact(txtVisaExp_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            dateTime1 = Date.ParseExact(txtVisaIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            'check for the leap year date

                            If IsDate(DateTime2) Then
                                If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                    ViewState("Valid_Appl") = "-1"
                                    Final_error = False
                                    commString = commString & "<li>Visa Expiry Date entered  must be greater than Visa Issue Date"
                                End If
                            Else
                                ViewState("Valid_Appl") = "-1"
                                Final_error = False
                                commString = commString & "<li>Invalid Visa Expiry Date"
                            End If
                        End If
                    End If


                End If
            End If
        End If

        ' End for passport/visa  Details



        'Validation  for primary School Details



        'Fee Sponsor validation----------new validation added

        If Enq_hash.ContainsKey("FEE_SP") = False Then
            If ddlFee_sponsor.SelectedItem.Text.Trim = "" Then
                commString = commString & "<LI>Fee sponsor detail required."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If

        End If


        If Enq_hash.ContainsKey("PCFN") = False Then
            If rdPri_Father.Checked Then
                If txtFPri_Fname.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact name cannot be left empty"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf rbPri_Mother.Checked Then
                If txtMPri_Fname.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact name cannot be left empty"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGPri_Fname.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact name cannot be left empty"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If
        End If

        If Enq_hash.ContainsKey("PCN") = False Then
            If rdPri_Father.Checked Then
                If ddlFPri_National1.SelectedItem.Text = "" Or ddlFPri_National1.SelectedItem.Text = "-" Then
                    commString = commString & "<LI>Primary contact Nationality required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf rbPri_Mother.Checked Then
                If ddlMPri_National1.SelectedItem.Text = "" Or ddlMPri_National1.SelectedItem.Text = "-" Then
                    commString = commString & "<LI>Primary contact Nationality required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf rdPri_Guard.Checked Then
                If ddlGPri_National1.SelectedItem.Text = "" Or ddlGPri_National1.SelectedItem.Text = "-" Then
                    commString = commString & "<LI>Primary contact Nationality required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If



        End If


        'father validation
        If txtFPri_Fname.Text <> "" Then
            If Not Regex.Match(txtFPri_Fname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid fathers contact first name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If

        If txtFPri_Mname.Text <> "" Then
            If Not Regex.Match(txtFPri_Mname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid fathers contact middle name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If
        If txtFPri_Lname.Text <> "" Then
            If Not Regex.Match(txtFPri_Lname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid fathers contact last name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If
        'mother validation
        If txtMPri_Fname.Text <> "" Then
            If Not Regex.Match(txtMPri_Fname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid mothers contact first name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If

        If txtMPri_Mname.Text <> "" Then
            If Not Regex.Match(txtMPri_Mname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid mothers contact middle name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If
        If txtMPri_Lname.Text <> "" Then
            If Not Regex.Match(txtMPri_Lname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid mothers contact last name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If

        'Guardian validation

        If txtGPri_Fname.Text <> "" Then
            If Not Regex.Match(txtGPri_Fname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid guardian  contact first name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If

        If txtGPri_Mname.Text <> "" Then
            If Not Regex.Match(txtGPri_Mname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid Guardian contact middle name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If
        If txtGPri_Lname.Text <> "" Then
            If Not Regex.Match(txtGPri_Lname.Text, "^[a-zA-Z'.\s-\\\/]{1,100}$").Success Then
                commString = commString & "<LI>Invalid Guardian contact last name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If

        'Primary contact oversea address 1 validation----------new validation added

        If Enq_hash.ContainsKey("PCOADD1") = False Then
            If rdPri_Father.Checked Then
                If txtFAdd1_overSea.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent address 1 required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf rbPri_Mother.Checked Then
                If txtMAdd1_overSea.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent address 1 required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf rdPri_Guard.Checked Then
                If txtGAdd1_overSea.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent address 1 required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If

        End If

        'Primary contact oversea city name validation----------new validation added
        If Enq_hash.ContainsKey("PCOST") = False Then
            If rdPri_Father.Checked Then
                If txtFOverSeas_Add_City.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent city name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rbPri_Mother.Checked Then
                If txtMOverSeas_Add_City.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent city name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGOverSeas_Add_City.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent city name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If
        End If



        'Primary contact oversea country name validation----------new validation added
        If Enq_hash.ContainsKey("PCOCTY") = False Then
            If rdPri_Father.Checked Then
                If ddlFOverSeas_Add_Country.SelectedItem.Text = "" Or ddlFOverSeas_Add_Country.SelectedItem.Text = "-" Then
                    commString = commString & "<LI>Primary contact permanent country name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rbPri_Mother.Checked Then
                If ddlMOverSeas_Add_Country.SelectedItem.Text = "" Or ddlMOverSeas_Add_Country.SelectedItem.Text = "-" Then
                    commString = commString & "<LI>Primary contact permanent country name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If ddlGOverSeas_Add_Country.SelectedItem.Text = "" Or ddlGOverSeas_Add_Country.SelectedItem.Text = "-" Then
                    commString = commString & "<LI>Primary contact permanent country name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If
        End If

        'Primary contact oversea phone no validation----------new validation added
        If Enq_hash.ContainsKey("PCOPH") = False Then
            If rdPri_Father.Checked Then
                If txtFPhone_Oversea_Country.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent phone country code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
                If txtFPhone_Oversea_Area.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent phone area code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
                If txtFPhone_Oversea_No.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent phone no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf rbPri_Mother.Checked Then
                If txtMPhone_Oversea_Country.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent phone country code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
                If txtMPhone_Oversea_Area.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent phone area code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
                If txtMPhone_Oversea_No.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent phone no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGPhone_Oversea_Country.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent phone country code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
                If txtGPhone_Oversea_Area.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent phone area code required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
                If txtGPhone_Oversea_No.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent phone no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If
        End If
        'Primary contact oversea PO Box no validation----------new validation added
        If Enq_hash.ContainsKey("PCOPO") = False Then
            If rdPri_Father.Checked Then
                If txtFPoBox_Pri.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent PO Box no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rbPri_Mother.Checked Then
                If txtMPoBox_Pri.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent PO Box no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGPoBox_Pri.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact permanent PO Box no required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If
        End If
        'Primary contact occupation  validation----------new validation added
        If Enq_hash.ContainsKey("PCOCC") = False Then
            If rdPri_Father.Checked Then
                If txtFOccup.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact occupation required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rbPri_Mother.Checked Then
                If txtMOccup.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact occupation required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGOccup.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact occupation required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If
        End If

        'Primary contact company name  validation----------new validation added
        If Enq_hash.ContainsKey("PCCOMP") = False Then
            If rdPri_Father.Checked Then
                If txtFComp.Text.Trim = "" And ddlFCompany.SelectedItem.Text = "Other" Then
                    commString = commString & "<LI>Primary contact company name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rbPri_Mother.Checked Then
                If txtMComp.Text.Trim = "" And ddlMCompany.SelectedItem.Text = "Other" Then
                    commString = commString & "<LI>Primary contact company name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rdPri_Guard.Checked Then
                If txtGComp.Text.Trim = "" And ddlGCompany.SelectedItem.Text = "Other" Then
                    commString = commString & "<LI>Primary contact company name required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If
        End If


        '^[0-9]{1,3}$
        'father contact info

        If ddlFPref_contact.SelectedItem.Text = "Home Phone" Then
            If txtFHPhone_Country.Text.Trim = "" Or txtFHPhone_Area.Text.Trim = "" Or txtFHPhone.Text.Trim = "" Then
                If rdPri_Father.Checked = True Then
                    commString = commString & "<LI>Father home contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf Not Regex.Match(txtFHPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Invalid father home phone country code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtFHPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Invalid father home phone area code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtFHPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Invalid father home phone no."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Office Phone" Then
            If txtFOPhone_Country.Text.Trim = "" Or txtFOPhone_Area.Text.Trim = "" Or txtFOPhone.Text.Trim = "" Then
                If rdPri_Father.Checked = True Then
                    commString = commString & "<LI>Father office contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf Not Regex.Match(txtFOPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Invalid father office phone country code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtFOPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Invalid father office phone area code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtFOPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Invalid father office phone no."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Mobile" Then
            If txtFM_Country.Text.Trim = "" Or txtFM_Area.Text.Trim = "" Or txtFMobile_Pri.Text.Trim = "" Then
                If rdPri_Father.Checked = True Then
                    commString = commString & "<LI>Father mobile contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf Not Regex.Match(txtFM_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Invalid father mobile country code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtFM_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Invalid father mobile phone area code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtFMobile_Pri.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Invalid father mobile phone no."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If

        'father contact info

        If ddlFPref_contact.SelectedItem.Text = "Home Phone" Then
            If txtMHPhone_Country.Text.Trim = "" Or txtMHPhone_Area.Text.Trim = "" Or txtMHPhone.Text.Trim = "" Then
                If rbPri_Mother.Checked = True Then
                    commString = commString & "<LI>Mother home contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf Not Regex.Match(txtMHPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Invalid mother home phone country code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtMHPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Invalid mother home phone area code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtMHPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Invalid mother home phone no."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Office Phone" Then
            If txtMOPhone_Country.Text.Trim = "" Or txtMOPhone_Area.Text.Trim = "" Or txtMOPhone.Text.Trim = "" Then
                If rbPri_Mother.Checked = True Then
                    commString = commString & "<LI>Mother office contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf Not Regex.Match(txtMOPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Invalid mother office phone country code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtMOPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Invalid mother office phone area code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtMOPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Invalid mother office phone no."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Mobile" Then
            If txtMM_Country.Text.Trim = "" Or txtMM_Area.Text.Trim = "" Or txtMMobile_Pri.Text.Trim = "" Then
                If rbPri_Mother.Checked = True Then
                    commString = commString & "<LI>Mother mobile contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf Not Regex.Match(txtMM_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Invalid mother mobile country code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtMM_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Invalid mother mobile phone area code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtMMobile_Pri.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Invalid mother mobile phone no."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If
        'Guardian  contact info

        If ddlFPref_contact.SelectedItem.Text = "Home Phone" Then
            If txtGHPhone_Country.Text.Trim = "" Or txtGHPhone_Area.Text.Trim = "" Or txtGHPhone.Text.Trim = "" Then
                If rdPri_Guard.Checked = True Then
                    commString = commString & "<LI>Guardian home contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf Not Regex.Match(txtGHPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Invalid guardian home phone country code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtGHPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Invalid guardian home phone area code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtGHPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Invalid guardian home phone no."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Office Phone" Then
            If txtGOPhone_Country.Text.Trim = "" Or txtGOPhone_Area.Text.Trim = "" Or txtGOPhone.Text.Trim = "" Then
                If rdPri_Guard.Checked = True Then
                    commString = commString & "<LI>Guardian office contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf Not Regex.Match(txtGOPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Invalid guardian office phone country code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtGOPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Invalid guardian office phone area code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtGOPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Invalid guardian office phone no."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If

        ElseIf ddlFPref_contact.SelectedItem.Text = "Mobile" Then
            If txtGM_Country.Text.Trim = "" Or txtGM_Area.Text.Trim = "" Or txtGMobile_Pri.Text.Trim = "" Then
                If rdPri_Guard.Checked = True Then
                    commString = commString & "<LI>Guardian mobile contact number required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf Not Regex.Match(txtGM_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Invalid guardian mobile country code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtGM_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Invalid guardian mobile phone area code"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtGMobile_Pri.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Invalid guardian mobile phone no."
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If









        If Enq_hash.ContainsKey("PCEID") = False Then
            If rdPri_Father.Checked = True Then
                If txtFEmail_Pri.Text.Trim = "" Then
                    commString = commString & "<LI>Father e-mail ID required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False

                ElseIf isEmail(txtFEmail_Pri.Text) = False Then
                    commString = commString & "<LI>Father e-mail entered is not valid"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf rbPri_Mother.Checked = True Then
                If txtMEmail_Pri.Text.Trim = "" Then
                    commString = commString & "<LI>Mother e-mail ID required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False

                ElseIf isEmail(txtMEmail_Pri.Text) = False Then
                    commString = commString & "<LI>Mother e-mail entered is not valid"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            ElseIf rdPri_Guard.Checked = True Then
                If txtGEmail_Pri.Text.Trim = "" Then
                    commString = commString & "<LI>Guardian e-mail ID required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False

                ElseIf isEmail(txtGEmail_Pri.Text) = False Then
                    commString = commString & "<LI>Guardian e-mail entered is not valid"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If
            End If

        End If












        '  If (Request.QueryString("MainMnu_code")) Is Nothing Then
        If ViewState("ENQTYPE") = "O" Then

            If ViewState("V_STAFF") <> "" Then
                If ViewState("orCheck") >= 1 Then
                    If rdPri_Father.Checked = True Then
                        chkFStaff_GEMS.Checked = True
                    ElseIf rbPri_Mother.Checked = True Then
                        chkMStaff_GEMS.Checked = True
                    Else
                        chkFStaff_GEMS.Checked = True
                    End If

                End If

            End If
        End If



        '--------------for Staff check (MAIN)-------------
        If chkFStaff_GEMS.Checked = True Then
            '--------------for Staff_Name check (A)-------------
            If Trim(txtFStaff_Name.Text) = "" Then
                commString = commString & "<LI>Father staff name required"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False


            End If
            '--------------(A) End Here-------------


            If ValidStaff_IDCheck() = True Then




                '--------------for Staff_ID check (B)-------------

                If Trim(txtFStaffID.Text) = "" Then
                    commString = commString & "<LI>Father staff ID required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False


                Else


                    '--------------for open for Staff check  (C)-------------

                    '  If (Request.QueryString("MainMnu_code")) Is Nothing Then

                    If ViewState("ENQTYPE") = "O" Then

                        If ViewState("V_STAFF") <> "" Then
                            Dim Staff_BSUIDs As String = ViewState("S_STAFF_GEMS")

                            ' Call OpenStaff_BSU_Populate(Staff_BSUIDs)


                            '--------------for Staff_ID check from Selected BSU_IDs orCheck=1(D1)-------------

                            If Staff_BSUIDs.IndexOf(ddlFStaff_BSU.SelectedItem.Value) < 0 Then
                                commString = commString & "<LI>Online Enquiry is not open for the Staff from " & ddlFStaff_BSU.SelectedItem.Text
                                ViewState("Valid_Prim_cont") = "-1"
                                Final_error = False

                            Else
                                Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtFStaffID.Text, ddlFStaff_BSU.SelectedItem.Value)

                                    If ValidStaff_reader.HasRows = False Then

                                        commString = commString & "<LI>Father staff ID is not valid  for the selected Business Unit"
                                        ViewState("Valid_Prim_cont") = "-1"
                                        Final_error = False



                                    End If

                                End Using
                            End If
                            '--------------(D1) End Here-------------


                        Else
                            '--------------for open only for Staff check  (D2)-------------

                            Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtFStaffID.Text, ddlFStaff_BSU.SelectedItem.Value)

                                If ValidStaff_reader.HasRows = False Then
                                    commString = commString & "<LI>Father staff ID is not valid  for the selected Business Unit"
                                    ViewState("Valid_Prim_cont") = "-1"
                                    Final_error = False



                                End If

                            End Using
                            '--------------(D2) End Here-------------
                        End If
                        '-----------------------------------(C)End Here-----------------------------


                    Else
                        '-------------code for registrar------------
                        Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtFStaffID.Text, ddlFStaff_BSU.SelectedItem.Value)

                            If ValidStaff_reader.HasRows = False Then
                                commString = commString & "<LI>father staff ID is not valid  for the selected Business Unit"
                                ViewState("Valid_Prim_cont") = "-1"
                                Final_error = False



                            End If

                        End Using


                    End If
                    '-----------------------------------(B)End Here-----------------------------


                End If

            End If

        End If
        '-----------------------------------(MAIN)End Here-----------------------------




        '--------------for mother  Staff check (MAIN)-------------
        If chkMStaff_GEMS.Checked = True Then
            '--------------for Staff_Name check (A)-------------
            If Trim(txtMStaff_Name.Text) = "" Then
                commString = commString & "<LI>Mother staff name required"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False


            End If
            '--------------(A) End Here-------------


            If ValidStaff_IDCheck() = True Then




                '--------------for Staff_ID check (B)-------------

                If Trim(txtMStaffID.Text) = "" Then
                    commString = commString & "<LI>Mother staff ID required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False


                Else


                    '--------------for open for Staff check  (C)-------------

                    ' If (Request.QueryString("MainMnu_code")) Is Nothing Then

                    If ViewState("ENQTYPE") = "O" Then

                        If ViewState("V_STAFF") <> "" Then
                            Dim Staff_BSUIDs As String = ViewState("S_STAFF_GEMS")

                            ' Call OpenStaff_BSU_Populate(Staff_BSUIDs)


                            '--------------for Staff_ID check from Selected BSU_IDs orCheck=1(D1)-------------

                            If Staff_BSUIDs.IndexOf(ddlMStaff_BSU.SelectedItem.Value) < 0 Then
                                commString = commString & "<LI>Online Enquiry is not open for the Staff from " & ddlMStaff_BSU.SelectedItem.Text
                                ViewState("Valid_Prim_cont") = "-1"
                                Final_error = False

                            Else
                                Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtMStaffID.Text, ddlMStaff_BSU.SelectedItem.Value)

                                    If ValidStaff_reader.HasRows = False Then

                                        commString = commString & "<LI>Mother staff ID is not valid  for the selected Business Unit"
                                        ViewState("Valid_Prim_cont") = "-1"
                                        Final_error = False



                                    End If

                                End Using
                            End If
                            '--------------(D1) End Here-------------


                        Else
                            '--------------for open only for Staff check  (D2)-------------

                            Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtMStaffID.Text, ddlMStaff_BSU.SelectedItem.Value)

                                If ValidStaff_reader.HasRows = False Then
                                    commString = commString & "<LI>Mother staff ID is not valid  for the selected Business Unit"
                                    ViewState("Valid_Prim_cont") = "-1"
                                    Final_error = False



                                End If

                            End Using
                            '--------------(D2) End Here-------------
                        End If
                        '-----------------------------------(C)End Here-----------------------------


                    Else
                        '-------------code for registrar------------
                        Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtMStaffID.Text, ddlMStaff_BSU.SelectedItem.Value)

                            If ValidStaff_reader.HasRows = False Then
                                commString = commString & "<LI>Mother staff ID is not valid  for the selected Business Unit"
                                ViewState("Valid_Prim_cont") = "-1"
                                Final_error = False



                            End If

                        End Using


                    End If
                    '-----------------------------------(B)End Here-----------------------------


                End If

            End If

        End If
        '-----------------------------------(MAIN)End Here-----------------------------

























        '--------------for ExStud_Gems simple check (K)-------------


        If chkFExStud_Gems.Checked Then
            If Trim(txtFExStudName_Gems.Text) = "" Then
                commString = commString & "<LI>Ex-Student detail cannot left empty under father contact information detail"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        ElseIf chkMExStud_Gems.Checked Then
            If Trim(txtMExStudName_Gems.Text) = "" Then
                commString = commString & "<LI>Ex-Student detail cannot left empty under mother contact information detail"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If

        End If

        ' End of primary school details Application Info


        'validation for previous school Details
        If ddlGrade.SelectedIndex <> -1 Then
            If (ddlGrade.SelectedItem.Value = "KG1" Or ddlGrade.SelectedItem.Value = "PK" Or ddlGrade.SelectedItem.Value = "PN1") Then
                If Enq_hash.ContainsKey("CSNY") = False Then
                    If ddlPreSchool_Nursery.SelectedItem.Text = "--" Then
                        commString = commString & "<LI>Kindly select previous school name"
                        ViewState("Valid_Prev_school") = "-1"
                        Final_error = False
                    End If
                End If
            Else
                'If rdOther.Checked = True Then
                '    If Enq_hash.ContainsKey("CSN") = False Then
                '        If txtPre_School.Text.Trim = "" Then
                '            commString = commString & "<LI>Kindly enter previous school name"
                '            ViewState("Valid_Prev_school") = "-1"
                '            Final_error = False
                '        End If
                '    End If
                'End If
                Dim Appl_GEMS_STUD As Boolean = False
                'some reason----------------------------------
                ' If (Request.QueryString("MainMnu_code")) Is Nothing Then
                If ViewState("ENQTYPE") = "O" Then
                    If ViewState("V_GEMS_STU") <> "" Then
                        If ViewState("orCheck") >= 1 Then
                            Appl_GEMS_STUD = True
                        End If
                    End If
                End If


                '--------------for GEMS STUDENTS check (MAIN)-------------
                If Appl_GEMS_STUD = True Then
                    '-----------------------code not for registrar----------------------
                    '--------------for STUDENT FEE_ID check (B)-------------
                    'If Trim(txtFeeID_GEMS.Text) = "" Then
                    '    commString = commString & "<LI>Student Id required"
                    '    ViewState("Valid_Prev_school") = "-1"
                    '    Final_error = False
                    'Else
                    '--------------for open for Staff check  (C)-------------
                    ' If (Request.QueryString("MainMnu_code")) Is Nothing Then
                    If ViewState("ENQTYPE") = "O" Then
                        If ViewState("V_GEMS_STU") <> "" Then
                            'Dim GEMS_STU_BSUIDs As String = ViewState("S_GEMS_STUD")
                            ''--------------for Staff_ID check from Selected BSU_IDs orCheck=1(D1)-------------
                            '' If GEMS_STU_BSUIDs.IndexOf(ddlGemsSchools.SelectedItem.Value) < 0 Then
                            'commString = commString & "<LI>Online Enquiry is not open for the Applicant from " & StrConv(ddlGemsSchools.SelectedItem.Text, VbStrConv.ProperCase)
                            'ViewState("Valid_Prev_school") = "-1"
                            'Final_error = False
                            ' Else
                            Final_error = validateStu_id(commString)
                            'Using ValidGEMS_STU_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtFeeID_GEMS.Text, ddlGemsSchools.SelectedItem.Value)

                            '    If ValidGEMS_STU_reader.HasRows = False Then
                            '        commString = commString & "<LI>Student Id is not valid for the selected School"

                            '        ViewState("Valid_Prev_school") = "-1"
                            '        Final_error = False
                            '    End If
                            'End Using
                            '  End If
                            '--------------(D1) End Here-------------
                        Else
                            ' --------------for open only for Staff check  (D2)-------------
                            'Using ValidGEMS_STU_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtFeeID_GEMS.Text, ddlGemsSchools.SelectedItem.Value)
                            '    If ValidGEMS_STU_reader.HasRows = False Then
                            '        commString = commString & "<LI>Student Id is not valid  for the selected School"
                            '        ViewState("Valid_Prev_school") = "-1"
                            '        Final_error = False
                            '    End If
                            'End Using
                            Final_error = validateStu_id(commString)
                            '--------------(D2) End Here-------------
                        End If
                        '-----------------------------------(C)End Here-----------------------------
                        '-----------------------code not for registrar----------------------
                    Else
                        '-----------------------code for registrar----------------------
                        'Using ValidGEMS_STU_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtFeeID_GEMS.Text, ddlGemsSchools.SelectedItem.Value)
                        '    If ValidGEMS_STU_reader.HasRows = False Then
                        '        commString = commString & "<LI>Student Id is not valid  for the selected School"
                        '        ViewState("Valid_Prev_school") = "-1"
                        '        Final_error = False
                        '    End If
                        'End Using
                        Final_error = validateStu_id(commString)
                        '-----------------code for registrar end here-------------------
                    End If
                End If
                '-----------------------------------(B)End Here-----------------------------

                '-----------------------------------(MAIN)End Here-----------------------------















                ''If Enq_hash.ContainsKey("CSC") = False Then
                ''    If txtPre_City.Text.Trim = "" Then
                ''        commString = commString & "<LI>Previous city name cannot be left empty"
                ''        ViewState("Valid_Prev_school") = "-1"
                ''        Final_error = False
                ''    End If
                ''End If

                'If Enq_hash.ContainsKey("CURRN") = False Then
                '    If ddlPre_Curriculum.SelectedIndex <> -1 Then
                '        If ddlPre_Curriculum.SelectedItem.Text = "--" Then
                '            commString = commString & "<LI>Kindly select the curriculum"
                '            ViewState("Valid_Prev_school") = "-1"
                '            Final_error = False
                '        End If
                '    End If
                'End If

                'code modified by lijo on 26may08 post back disabled


                ''If txtLast_Att.Text.Trim <> "" Then
                ''    If IsDate(txtLast_Att.Text) = False Then
                ''        commString = commString & "<LI>Last Attendance Date is not a vaild date"
                ''        ViewState("Valid_Prev_school") = "-1"
                ''        Final_error = False
                ''    End If

                ''ElseIf txtLast_Att.Text.Trim <> "" And IsDate(txtLast_Att.Text) = True Then
                ''    'code modified 4 date
                ''    Dim strfDate As String = txtLast_Att.Text.Trim
                ''    Dim str_err As String = DateFunctions.checkdate(strfDate)
                ''    If str_err <> "" Then
                ''        ViewState("Valid_Prev_school") = "-1"
                ''        Final_error = False
                ''        commString = commString & "<LI>Last Attendance Date is not a vaild date"
                ''    Else
                ''        txtLast_Att.Text = strfDate
                ''        DateValid_DOB = Date.ParseExact(txtLast_Att.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                ''        If IsDate(DateValid_DOB) = False Then
                ''            commString = commString & "<LI>Last Attendance Date is not a vaild date"
                ''            ViewState("Valid_Prev_school") = "-1"
                ''            Final_error = False
                ''        End If
                ''    End If
                ''ElseIf txtLast_Att.Text.Trim <> "" And IsDate(txtLast_Att.Text) = False Then
                ''    commString = commString & "<LI>Last Attendance Date format is invalid"
                ''    ViewState("Valid_Prev_school") = "-1"
                ''    Final_error = False
                ''End If





            End If

        End If

        If rbRelocAgent.Checked = True Then
            If Enq_hash.ContainsKey("AGN") = False Then
                If txtAgentName.Text.Trim = "" Then
                    commString = commString & "<LI>Agent contact name cannot be left empty"
                    '  ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False

                End If
            End If

        End If

        If rbRelocAgent.Checked = True Then
            If txtMAgent_country.Text.Trim = "" Or txtMAgent_Area.Text.Trim = "" Or txtMAgent_No.Text.Trim = "" Then
                commString = commString & "<LI>Agent Mobile Contact number required"
                '  ViewState("Valid_Prim_cont") = "-1"
                Final_error = False

            ElseIf Not Regex.Match(txtMAgent_country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Enter valid Agent mobile country code"
                ' ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtMAgent_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Enter valid Agent mobile phone area code"
                ' ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            ElseIf Not Regex.Match(txtMAgent_No.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Enter valid Agent mobile phone no."
                ' ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If

        If chkagree.Checked = False Then
            commString = commString & "<LI>Please tick the declaration box to confirm that you agree to the above declaration."
            Final_error = False
        End If

        Captcha1.ValidateCaptcha(txtCapt.Text.Trim())

        If Captcha1.UserValidated = False Then

            commString = commString & "<LI>The characters you entered didn't match the word verification. Please try again."
            Final_error = False
        End If

        If rbRefYes.Checked Then
            If RefCodeIsvalid() = False Then
                commString = commString & "<LI>Please verify the Ref. Code and email you have entered."
                Final_error = False
            End If
        End If



        If Final_error Then
            Call mnuReg_Select(mvRegMain.ActiveViewIndex)
            Final_validation = Final_error
        Else
            Call mnuReg_Select(mvRegMain.ActiveViewIndex)
            lblError.Text = commString & "</UL>"
            Final_validation = Final_error
        End If


    End Function

    Function validateStu_id(ByRef commString As String) As Boolean
        Dim flag As Boolean = False
        For i As Integer = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
            Using ValidGEMS_STU_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(ViewState("TABLE_School_Pre").Rows(i)("SCH_FEE_ID"), ViewState("TABLE_School_Pre").Rows(i)("SCH_ID"))
                If ValidGEMS_STU_reader.HasRows = True Then

                    flag = True
                End If
            End Using
        Next
        If flag = False Then
            commString = commString & "<LI>Student Id is not valid  for the selected School"
            ViewState("Valid_Prev_school") = "-1"
            flag = False
        Else
            flag = True
        End If

        validateStu_id = flag

    End Function
    

    Sub gridbind()
        Try


            gvSchool.DataSource = ViewState("TABLE_School_Pre")
            gvSchool.DataBind()
            If Not ViewState("TABLE_School_Pre") Is Nothing Then
                If ViewState("TABLE_School_Pre").Rows.Count > 0 Then
                    ltSchoolType.Text = "Previous School"
                Else
                    ltSchoolType.Text = "Current School"

                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GridBind")
        End Try
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim SCH_ID As New DataColumn("SCH_ID", System.Type.GetType("System.String"))
            Dim SCH_NAME As New DataColumn("SCH_NAME", System.Type.GetType("System.String"))
            Dim SCH_HEAD As New DataColumn("SCH_HEAD", System.Type.GetType("System.String"))
            Dim SCH_FEE_ID As New DataColumn("SCH_FEE_ID", System.Type.GetType("System.String"))
            Dim SCH_GRADE As New DataColumn("SCH_GRADE", System.Type.GetType("System.String"))
            Dim SCH_LEARN_INS As New DataColumn("SCH_LEARN_INS", System.Type.GetType("System.String"))
            Dim SCH_ADDR As New DataColumn("SCH_ADDR", System.Type.GetType("System.String"))
            Dim SCH_CURR As New DataColumn("SCH_CURR", System.Type.GetType("System.String"))
            Dim SCH_CITY As New DataColumn("SCH_CITY", System.Type.GetType("System.String"))
            Dim SCH_COUNTRY As New DataColumn("SCH_COUNTRY", System.Type.GetType("System.String"))
            Dim SCH_PHONE As New DataColumn("SCH_PHONE", System.Type.GetType("System.String"))
            Dim SCH_FAX As New DataColumn("SCH_FAX", System.Type.GetType("System.String"))
            Dim SCH_FROMDT As New DataColumn("SCH_FROMDT", System.Type.GetType("System.String"))
            Dim SCH_TODT As New DataColumn("SCH_TODT", System.Type.GetType("System.String"))
            Dim SCH_TYPE As New DataColumn("SCH_TYPE", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(SCH_ID)
            dtDt.Columns.Add(SCH_NAME)
            dtDt.Columns.Add(SCH_HEAD)
            dtDt.Columns.Add(SCH_FEE_ID)
            dtDt.Columns.Add(SCH_GRADE)
            dtDt.Columns.Add(SCH_LEARN_INS)
            dtDt.Columns.Add(SCH_ADDR)
            dtDt.Columns.Add(SCH_CURR)
            dtDt.Columns.Add(SCH_CITY)
            dtDt.Columns.Add(SCH_COUNTRY)
            dtDt.Columns.Add(SCH_PHONE)
            dtDt.Columns.Add(SCH_FAX)
            dtDt.Columns.Add(SCH_FROMDT)
            dtDt.Columns.Add(SCH_TODT)
            dtDt.Columns.Add(SCH_TYPE)
            dtDt.Columns.Add(STATUS)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Create table")

            Return dtDt
        End Try
    End Function
    'Protected Sub gvRef_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSchool.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim DeleteBtn As LinkButton = DirectCast(e.Row.FindControl("DeleteBtn"), LinkButton)
    '        DeleteBtn.OnClientClick = "if(!confirm('Are you sure you want to delete this?')) return false;"
    '    End If
    'End Sub
    'Protected Sub gvRef_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
    '    Dim COND_ID As Integer = CInt(gvSchool.DataKeys(e.RowIndex).Value)

    '    Dim i As Integer = 0

    '    For i = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
    '        If ViewState("TABLE_School_Pre").rows(i)("ID") = COND_ID Then
    '            ViewState("TABLE_School_Pre").Rows(i).Delete()

    '        End If

    '    Next

    '    gridbind()
    'End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Try
            If checkGemsStud_Valid() = True Then

                Dim Ibsu As Integer = 0
                Dim SCH_NAME As String = String.Empty
                Dim sch_id As String = String.Empty
                If ddlGemsSchools.SelectedItem.Text = "OTHER" Then
                    SCH_NAME = txtSch_other.Text
                    sch_id = ""
                Else
                    SCH_NAME = ddlGemsSchools.SelectedItem.Text
                    sch_id = ddlGemsSchools.SelectedValue
                End If

                If ViewState("TABLE_School_Pre").Rows.Count > 4 Then
                    lblError.Text = "Add school details not more than four !!!"
                    Exit Sub
                End If

                For i As Integer = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
                    If ((UCase(Trim(ViewState("TABLE_School_Pre").Rows(i)("SCH_NAME"))).Trim = UCase(Trim(SCH_NAME.Trim)))) _
                     And ((UCase(Trim(ViewState("TABLE_School_Pre").Rows(i)("SCH_GRADE"))).Trim = UCase(Trim(ddlPre_Grade.SelectedValue.Trim)))) Then

                        lblError.Text = "Duplicate entry not allowed !!!"

                        Exit Sub

                        'ElseIf ViewState("TABLE_School_Pre").Rows(i)("SCH_TYPE").Trim = "Current School" Then
                        '    lblError.Text = "Duplicate current school type entry not allowed !!!"
                        '    Exit Sub
                    End If
                Next


                Dim rDt As DataRow

                rDt = ViewState("TABLE_School_Pre").NewRow
                rDt("ID") = ViewState("id")
                rDt("sch_id") = sch_id
                rDt("SCH_NAME") = SCH_NAME
                rDt("SCH_HEAD") = txtSchool_head.Text.Trim
                rDt("SCH_FEE_ID") = txtFeeID_GEMS.Text.Trim
                rDt("SCH_GRADE") = ddlPre_Grade.SelectedValue.Trim
                rDt("SCH_LEARN_INS") = txtLang_Instr.Text
                rDt("SCH_ADDR") = txtSchAddr.Text.Trim
                rDt("SCH_CURR") = ddlPre_Curriculum.SelectedValue
                rDt("SCH_CITY") = txtSchCity.Text.Trim
                rDt("SCH_COUNTRY") = ddlPre_Country.SelectedValue
                rDt("SCH_PHONE") = txtSCHPhone_Country.Text + "-" + txtSCHPhone_Area.Text + "-" + txtSCHPhone_No.Text
                rDt("SCH_FAX") = txtSCHFax_Country.Text + "-" + txtSCHFax_Area.Text + "-" + txtSCHFax_No.Text
                rDt("SCH_FROMDT") = txtSchFrom_dt.Text
                rDt("SCH_TODT") = txtSchTo_dt.Text
                rDt("SCH_TYPE") = ltSchoolType.Text
                rDt("STATUS") = "add"
                ViewState("id") = ViewState("id") + 1
                ViewState("TABLE_School_Pre").Rows.Add(rDt)
                gridbind()
                clearall()

            End If
        Catch ex As Exception
            lblError.Text = "Error in adding new record"
        End Try
    End Sub


    Sub callAutoAdd()
        Try

            If ViewState("Table_row").ToString.Contains("PK|KG1") = False Then
                Dim Ibsu As Integer = 0
                Dim SCH_NAME As String = String.Empty
                Dim sch_id As String = String.Empty
                If ddlGemsSchools.SelectedItem.Text = "OTHER" Then
                    SCH_NAME = txtSch_other.Text
                    sch_id = ""
                Else
                    SCH_NAME = ddlGemsSchools.SelectedItem.Text
                    sch_id = ddlGemsSchools.SelectedValue
                End If
                If Not ViewState("TABLE_School_Pre") Is Nothing Then
                    If ViewState("TABLE_School_Pre").Rows.Count > 0 Then

                        For i As Integer = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
                            If ((UCase(Trim(ViewState("TABLE_School_Pre").Rows(i)("SCH_NAME"))).Trim = UCase(Trim(SCH_NAME.Trim)))) _
                             And ((UCase(Trim(ViewState("TABLE_School_Pre").Rows(i)("SCH_GRADE"))).Trim = UCase(Trim(ddlPre_Grade.SelectedValue.Trim)))) Then
                                clearall()
                                Exit Sub

                            ElseIf (ddlGemsSchools.SelectedItem.Text = "OTHER") And (txtSch_other.Text.Trim = "") Then
                                clearall()
                                Exit Sub
                            End If
                        Next
                    End If
                Else

                    ViewState("TABLE_School_Pre") = CreateDataTable()
                    ViewState("TABLE_School_Pre").Rows.Clear()
                    ViewState("id") = 1
                End If
                If ViewState("TABLE_School_Pre").Rows.Count > 4 Then
                    Exit Sub
                End If
                Dim rDt As DataRow

                rDt = ViewState("TABLE_School_Pre").NewRow
                rDt("ID") = ViewState("id")
                rDt("sch_id") = sch_id
                rDt("SCH_NAME") = SCH_NAME
                rDt("SCH_HEAD") = txtSchool_head.Text.Trim
                rDt("SCH_FEE_ID") = txtFeeID_GEMS.Text.Trim
                rDt("SCH_GRADE") = ddlPre_Grade.SelectedValue.Trim
                rDt("SCH_LEARN_INS") = txtLang_Instr.Text
                rDt("SCH_ADDR") = txtSchAddr.Text.Trim
                rDt("SCH_CURR") = ddlPre_Curriculum.SelectedValue
                rDt("SCH_CITY") = txtSchCity.Text.Trim
                rDt("SCH_COUNTRY") = ddlPre_Country.SelectedValue
                rDt("SCH_PHONE") = txtSCHPhone_Country.Text + "-" + txtSCHPhone_Area.Text + "-" + txtSCHPhone_No.Text
                rDt("SCH_FAX") = txtSCHFax_Country.Text + "-" + txtSCHFax_Area.Text + "-" + txtSCHFax_No.Text
                rDt("SCH_FROMDT") = txtSchFrom_dt.Text
                rDt("SCH_TODT") = txtSchTo_dt.Text
                rDt("SCH_TYPE") = ltSchoolType.Text
                rDt("STATUS") = "add"
                ViewState("id") = ViewState("id") + 1
                ViewState("TABLE_School_Pre").Rows.Add(rDt)
                gridbind()
                clearall()
            Else
                If ViewState("TABLE_School_Pre") Is Nothing Then
                    ViewState("TABLE_School_Pre") = CreateDataTable()
                End If

                ViewState("TABLE_School_Pre").Rows.Clear()
                ViewState("id") = 1
                gridbind()
            End If


        Catch ex As Exception


            UtilityObj.Errorlog(ex.Message, "callAutoAdd PN(" & txtPassport.Text & " ) BSU(" & ddlGEMSSchool.SelectedValue & ")")
        End Try

    End Sub
    Protected Sub gvSchool_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvSchool.RowEditing
        btnVwPrev_Next.Enabled = False
        btnVwPrev_Pre.Enabled = False

        gvSchool.SelectedIndex = e.NewEditIndex


        Dim row As GridViewRow = gvSchool.Rows(e.NewEditIndex)
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)

        Dim arInfoLang() As String
        Dim splitterLang As Char = "-"


        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
            If iIndex = ViewState("TABLE_School_Pre").Rows(iEdit)("ID") Then

                If Not ddlGemsSchools.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("sch_id")) Is Nothing Then
                    ddlGemsSchools.ClearSelection()
                    ddlGemsSchools.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("sch_id")).Selected = True
                End If

                If ViewState("TABLE_School_Pre").Rows(iEdit)("sch_id") = "" Then
                    txtSch_other.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_NAME")
                Else
                    txtSch_other.Text = ""
                End If

                txtSchool_head.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_HEAD")
                txtFeeID_GEMS.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FEE_ID")

                If Not ddlPre_Grade.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_GRADE")) Is Nothing Then
                    ddlPre_Grade.ClearSelection()
                    ddlPre_Grade.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_GRADE")).Selected = True
                End If
                txtSchCity.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CITY")
                If Not ddlPre_Country.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_COUNTRY")) Is Nothing Then
                    ddlPre_Country.ClearSelection()
                    ddlPre_Country.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_COUNTRY")).Selected = True
                End If

                If Not ddlPre_Curriculum.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CURR")) Is Nothing Then
                    ddlPre_Curriculum.ClearSelection()
                    ddlPre_Curriculum.Items.FindByValue(ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CURR")).Selected = True
                End If


                txtSchAddr.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_ADDR")
                arInfoLang = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_PHONE").Trim.Split(splitterLang)
                txtSCHPhone_Country.Text = arInfoLang(0)
                txtSCHPhone_Area.Text = arInfoLang(1)
                txtSCHPhone_No.Text = arInfoLang(2)
                arInfoLang = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FAX").Trim.Split(splitterLang)
                txtSCHFax_Country.Text = arInfoLang(0)
                txtSCHFax_Area.Text = arInfoLang(1)
                txtSCHFax_No.Text = arInfoLang(2)


                txtSchFrom_dt.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FROMDT")
                txtSchTo_dt.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_TODT")
                ltSchoolType.Text = ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_TYPE")

                Exit For
            End If
        Next
        gvSchool.Columns(6).Visible = False
        btnAdd.Visible = False
        btnUpdate.Visible = True
        'gridbind()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        Dim row As GridViewRow = gvSchool.Rows(gvSchool.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        iIndex = CInt(idRow.Text)

        Dim SCH_NAME As String = String.Empty
        Dim sch_id As String = String.Empty
        If ddlGemsSchools.SelectedItem.Text = "OTHER" Then
            SCH_NAME = txtSch_other.Text
            sch_id = ""
        Else
            SCH_NAME = ddlGemsSchools.SelectedItem.Text
            sch_id = ddlGemsSchools.SelectedValue
        End If


        For iEdit = 0 To ViewState("TABLE_School_Pre").Rows.Count - 1
            If iIndex = ViewState("TABLE_School_Pre").Rows(iEdit)("ID") Then
                ViewState("TABLE_School_Pre").Rows(iEdit)("sch_id") = sch_id
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_NAME") = SCH_NAME
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_HEAD") = txtSchool_head.Text.Trim
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FEE_ID") = txtFeeID_GEMS.Text.Trim
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_GRADE") = ddlPre_Grade.SelectedValue.Trim
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_LEARN_INS") = txtLang_Instr.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_ADDR") = txtSchAddr.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CURR") = ddlPre_Curriculum.SelectedValue
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_CITY") = txtSchCity.Text.Trim
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_COUNTRY") = ddlPre_Country.SelectedValue
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_PHONE") = txtSCHPhone_Country.Text + "-" + txtSCHPhone_Area.Text + "-" + txtSCHPhone_No.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FAX") = txtSCHFax_Country.Text + "-" + txtSCHFax_Area.Text + "-" + txtSCHFax_No.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_FROMDT") = txtSchFrom_dt.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_TODT") = txtSchTo_dt.Text
                ViewState("TABLE_School_Pre").Rows(iEdit)("SCH_TYPE") = ltSchoolType.Text

                Exit For
            End If
        Next

        btnAdd.Visible = True

        btnUpdate.Visible = False
        btnVwPrev_Next.Enabled = True
        btnVwPrev_Pre.Enabled = True
        gvSchool.SelectedIndex = -1
        gridbind()
        gvSchool.Columns(6).Visible = True
        clearall()

    End Sub

    Sub clearall()
        txtSch_other.Text = ""
        txtSchool_head.Text = ""
        txtFeeID_GEMS.Text = ""
        If Not ddlGemsSchools.Items.FindByText("OTHER") Is Nothing Then
            ddlGemsSchools.ClearSelection()
            ddlGemsSchools.Items.FindByText("OTHER").Selected = True
        End If
        If Not ddlPre_Grade.Items.FindByText(GetPrevGrade()) Is Nothing Then
            ddlPre_Grade.ClearSelection()
            ddlPre_Grade.Items.FindByText(GetPrevGrade()).Selected = True
        End If
        ddlPre_Country.ClearSelection()
        ddlPre_Country.Items.FindByValue("172").Selected = True
        If Not ddlPre_Curriculum.Items.FindByText("--") Is Nothing Then
            ddlPre_Curriculum.ClearSelection()
            ddlPre_Curriculum.Items.FindByText("--").Selected = True
        End If

        txtLang_Instr.Text = ""
        txtSchCity.Text = ""
        txtSCHPhone_Country.Text = ""
        txtSCHPhone_Area.Text = ""
        txtSCHPhone_No.Text = ""
        txtSCHFax_Country.Text = ""
        txtSCHFax_Area.Text = ""
        txtSCHFax_No.Text = ""
        txtSchFrom_dt.Text = ""
        txtSchTo_dt.Text = ""
        txtSchAddr.Text = ""

    End Sub

    Protected Sub btnHome_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHome.Click
        ltMessage.Visible = True
        hometab.Visible = False
        maintab.Visible = True
        divNote.Visible = True
        divRef.Visible = False
    End Sub

    
  
End Class
