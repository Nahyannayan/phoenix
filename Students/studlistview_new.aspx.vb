Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection

Imports System.IO
Imports System.Text


Partial Class Students_studlistview_new
    Inherits System.Web.UI.Page

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkPrint)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkBack)

        If Not IsPostBack Then

            Try
                CheckMenuandUser()
                CheckFlag()
                BindReligion()
                BindNationality()
                BindRelation()
                BindContols()
                'TextDOB.Attributes.Add("onfocus", "javascript:alerting(this);")
                'TextPassportExpirity.Attributes.Add("onfocus", "javascript:alerting(this);")
                'TextVisaFrom.Attributes.Add("onfocus", "javascript:alerting(this);")
                'TextVisaTo.Attributes.Add("onfocus", "javascript:alerting(this);")
                'TextLeaveDate1.Attributes.Add("onfocus", "javascript:alerting(this);")
                'TextLeaveDate2.Attributes.Add("onfocus", "javascript:alerting(this);")
                'TextLeaveDate3.Attributes.Add("onfocus", "javascript:alerting(this);")
                'TextPassportissuedate.Attributes.Add("onfocus", "javascript:alerting(this);")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

        End If
    End Sub
    Public Sub CheckMenuandUser()
        Dim Encr_decrData As New Encryption64
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim USR_NAME As String = Session("sUsr_name")

        If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100080" And ViewState("MainMnu_code") <> "S100072") Then
            If Not Request.UrlReferrer Is Nothing Then
                Response.Redirect(Request.UrlReferrer.ToString())
            Else

                Response.Redirect("~\noAccess.aspx")
            End If
        End If

    End Sub
    Public Sub CheckFlag()
        Dim Encr_decrData As New Encryption64
        Dim flag As String = Encr_decrData.Decrypt(Request.QueryString("flag").Replace(" ", "+")) ''1 for Disable 
        If flag = "1" Then
            ImageButton1.Enabled = False
            ImageButton2.Enabled = False
            ImageButton3.Enabled = False
            ImageButton4.Enabled = False
            ImageButton8.Enabled = False

        End If
    End Sub
    Public Sub BindContols()
        Try


            Dim Encr_decrData As New Encryption64
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sBsuid As String = Session("sBsuid") '' "125016"
            Dim stuid As String = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+")) ''"1" ''
            Hiddenstuid.Value = stuid
            Dim str_query

            If (Session("SBsuid") = "131001" Or Session("SBsuid") = "131002" Or Session("SBsuid") = "133006") Then
                str_query = "select *,J.CTY_DESCR as STUNAT,J.CTY_ID as STUNAT_ID,isNULL(BSU_bMOEGRADE,'') as BSU_MOEGrade from STUDENT_M  INNER JOIN STUDENT_D ON STUDENT_M.STU_sibLING_ID=STUDENT_D.STS_STU_ID INNER JOIN Grade_M_MOE_SHJ ON STUDENT_M.STU_GRD_ID=Grade_M_MOE_SHJ.GRD_ID inner join ACADEMICYEAR_D on ACADEMICYEAR_D.ACD_ID =STU_ACD_ID inner join ACADEMICYEAR_M on ACADEMICYEAR_D.ACD_ACY_ID =ACADEMICYEAR_M.ACY_ID LEFT OUTER JOIN COUNTRY_M AS J  ON J.CTY_ID = STUDENT_M.STU_COB INNER JOIN BusinessUnit_M BSU ON Student_M.STU_BSU_ID=BSU.BSU_ID WHERE Student_M.STU_ID='" & stuid & "'"
            Else
                str_query = "select *,J.CTY_DESCR as STUNAT,J.CTY_ID as STUNAT_ID,isNULL(BSU_bMOEGRADE,'') as BSU_MOEGrade from STUDENT_M  INNER JOIN STUDENT_D ON STUDENT_M.STU_sibLING_ID=STUDENT_D.STS_STU_ID INNER JOIN GRADE_M ON STUDENT_M.STU_GRD_ID=GRADE_M.GRD_ID inner join ACADEMICYEAR_D on ACADEMICYEAR_D.ACD_ID =STU_ACD_ID inner join ACADEMICYEAR_M on ACADEMICYEAR_D.ACD_ACY_ID =ACADEMICYEAR_M.ACY_ID LEFT OUTER JOIN COUNTRY_M AS J  ON J.CTY_ID = STUDENT_M.STU_COB INNER JOIN BusinessUnit_M BSU ON Student_M.STU_BSU_ID=BSU.BSU_ID WHERE Student_M.STU_ID='" & stuid & "'"
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ''Bind Student Details
            If ds.Tables(0).Rows.Count > 0 Then


                StudentsDetails(ds)

                ''Get Primary Contact Code
                Dim primarycontactcode As String = ds.Tables(0).Rows(0).Item("STU_PRIMARYCONTACT").ToString().Trim()
                Hiddenoption.Value = primarycontactcode

                DropPrimaryContact.SelectedValue = ds.Tables(0).Rows(0).Item("STU_PRIMARYCONTACT").ToString().Trim()

                'Bind Students Parent Details

                If (primarycontactcode = "F") Then ''Father
                    LBlPrimaryContactName.Text = "Father"
                    FatherDetails(ds)
                ElseIf (primarycontactcode = "M") Then ''Mother
                    LBlPrimaryContactName.Text = "Mother"
                    MotherDetails(ds)
                ElseIf (primarycontactcode = "G") Then ''Guardian
                    LBlPrimaryContactName.Text = "Guardian"
                    GuardianDetails(ds)
                End If

                ''Bind Previous Schools
                PreviousSchools(ds)
            End If
            labelcheck()
        Catch ex As Exception
            lblEmessage.Text = "Error :" & ex.Message
        End Try
    End Sub
    Public Sub StudentsDetails(ByVal ds As DataSet)
        Dim lstrBSU_bMOEGrade As String

        If DropGrade.Items.Count = 0 Or DropStage.Items.Count = 0 Then
            DropStage.Items.Add(ds.Tables(0).Rows(0).Item("GRD_STAGE_ARABIC"))
            If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("GRD_STAGE")) Then
                Lbstage.Text = ""
            Else
                Lbstage.Text = (ds.Tables(0).Rows(0).Item("GRD_STAGE")).ToString()
            End If
            DropGrade.Items.Add(ds.Tables(0).Rows(0).Item("GRD_DISPLAY_ARABIC"))
            LBgrade.Text = (ds.Tables(0).Rows(0).Item("GRD_DISPLAY")).ToString()
            lblgrade.Text = ds.Tables(0).Rows(0).Item("GRD_DISPLAY_ARABIC").ToString()
        End If


        lstrBSU_bMOEGrade = ds.Tables(0).Rows(0).Item("BSU_MOEGrade").ToString()
        If lstrBSU_bMOEGrade = "True" Then
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@Grade", ds.Tables(0).Rows(0).Item("STU_BB_GRADE_DES").ToString())
            pParms(1) = New SqlClient.SqlParameter("@ACD_ID", ds.Tables(0).Rows(0).Item("STU_ACD_ID").ToString())
            Dim str_conn_MOE As String = ConnectionManger.GetOASISConnectionString
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn_MOE, CommandType.StoredProcedure, "GetMOE_GRADE_DETAILS", pParms)
                While reader.Read
                    DropStage.Items.Add(Convert.ToString(reader("GRD_STAGE_ARABIC")))
                    If Convert.IsDBNull(reader("GRD_STAGE")) Then
                        Lbstage.Text = ""
                    Else
                        Lbstage.Text = (reader("GRD_STAGE")).ToString()
                    End If
                    DropGrade.Items.Add(reader("GRD_DISPLAY_ARABIC"))
                    LBgrade.Text = (reader("GRD_DISPLAY")).ToString()
                    lblgrade.Text = reader("GRD_DISPLAY_ARABIC").ToString()
                End While
            End Using
        End If
        lbladmissionnumber.Text = ds.Tables(0).Rows(0).Item("STU_BLUEID").ToString()
        lblaccyear.Text = ds.Tables(0).Rows(0).Item("ACY_DESCR").ToString()
        If ds.Tables(0).Rows(0).Item("STU_RLG_ID").ToString().Trim() <> "0" Then
            DropRelegion.SelectedValue = ds.Tables(0).Rows(0).Item("STU_RLG_ID").ToString().Trim()
            Religion(ds.Tables(0).Rows(0).Item("STU_RLG_ID").ToString())
        End If
        If ds.Tables(0).Rows(0).Item("STU_NATIONALITY").ToString().Trim() <> "" Then
            DropNationality.SelectedValue = ds.Tables(0).Rows(0).Item("STU_NATIONALITY").ToString().Trim()
            Nationality(ds.Tables(0).Rows(0).Item("STU_NATIONALITY").ToString())

        End If

        TextDOB.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_DOB").ToString()).ToString("dd/MMM/yyyy")
        lbdob.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_DOB").ToString()).ToString("dd/MMM/yyyy")

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_POB_ARABIC")) Then
            If ds.Tables(0).Rows(0).Item("STUNAT_ID").ToString() = 172 Then
                TextPlaceOfBirth.Text = ds.Tables(0).Rows(0).Item("STU_POB").ToString() & "," & ds.Tables(0).Rows(0).Item("STUNAT").ToString()
                LBplaceofbirth.Text = ds.Tables(0).Rows(0).Item("STU_POB").ToString() & "," & ds.Tables(0).Rows(0).Item("STUNAT").ToString()
            Else
                TextPlaceOfBirth.Text = ds.Tables(0).Rows(0).Item("STUNAT").ToString()
                LBplaceofbirth.Text = ds.Tables(0).Rows(0).Item("STUNAT").ToString()
            End If

        Else
            TextPlaceOfBirth.Text = ds.Tables(0).Rows(0).Item("STUNAT").ToString()
            If ds.Tables(0).Rows(0).Item("STUNAT_ID").ToString() = 172 Then

                LBplaceofbirth.Text = ds.Tables(0).Rows(0).Item("STU_POB").ToString() & "," & ds.Tables(0).Rows(0).Item("STUNAT").ToString()
            Else

                LBplaceofbirth.Text = ds.Tables(0).Rows(0).Item("STUNAT").ToString()
            End If
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_FIRSTNAMEARABIC")) Then
            TextPassportName.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTNAME").ToString()
            lblpassportname.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTNAME").ToString()
            LBpassportname.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTNAME").ToString()
        Else
            TextPassportName.Text = ds.Tables(0).Rows(0).Item("STU_FIRSTNAMEARABIC").ToString()
            lblpassportname.Text = ds.Tables(0).Rows(0).Item("STU_FIRSTNAMEARABIC").ToString()
            LBpassportname.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTNAME").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PASPRTISSPLACE_ARBIC")) Then
            TextPassportIssue.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTISSPLACE").ToString()
            LBpassportissue.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTISSPLACE").ToString()
        Else
            TextPassportIssue.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTISSPLACE_ARBIC").ToString()
            LBpassportissue.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTISSPLACE").ToString()
        End If
        TextPassportNumber.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTNO").ToString()
        LBpassportnumber.Text = ds.Tables(0).Rows(0).Item("STU_PASPRTNO").ToString()

        If (ds.Tables(0).Rows(0).Item("STU_PASPRTEXPDATE").ToString() <> "") Then
            TextPassportExpirity.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PASPRTEXPDATE").ToString()).ToString("dd/MMM/yyyy")
            LBPassportExpiryDate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PASPRTEXPDATE").ToString()).ToString("dd/MMM/yyyy")
        End If
        TextVisaNumber.Text = ds.Tables(0).Rows(0).Item("STU_VISANO").ToString()
        LBvisanumber.Text = ds.Tables(0).Rows(0).Item("STU_VISANO").ToString()

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_VISAISSDATE")) Or ds.Tables(0).Rows(0).Item("STU_VISAISSDATE").ToString().Trim() = "" Then
        Else
            LBvisafrom.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_VISAISSDATE").ToString()).ToString("dd/MMM/yyyy")
            TextVisaFrom.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_VISAISSDATE").ToString()).ToString("dd/MMM/yyyy")

        End If
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_VISAEXPDATE")) Or ds.Tables(0).Rows(0).Item("STU_VISAEXPDATE").ToString().Trim() = "" Then
        Else
            LBvisato.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_VISAEXPDATE").ToString()).ToString("dd/MMM/yyyy")
            TextVisaTo.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_VISAEXPDATE").ToString()).ToString("dd/MMM/yyyy")
        End If
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PASPRTISSDATE")) Or ds.Tables(0).Rows(0).Item("STU_PASPRTISSDATE").ToString().Trim() = "" Then
        Else
            LBPassportIssueDate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PASPRTISSDATE").ToString()).ToString("dd/MMM/yyyy")
            TextPassportissuedate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PASPRTISSDATE").ToString()).ToString("dd/MMM/yyyy")
        End If


    End Sub
    Public Sub FatherDetails(ByVal ds As DataSet)
        Dim lstrComADDR As String
        If ds.Tables(0).Rows(0).Item("STS_PNAME_ARABIC").ToString() = "" Then
            TextPrimaryConName.Text = ds.Tables(0).Rows(0).Item("STS_FFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_FMIDNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_FLASTNAME").ToString()
            LBPrimaryContactName.Text = ds.Tables(0).Rows(0).Item("STS_FFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_FMIDNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_FLASTNAME").ToString()
        Else
            TextPrimaryConName.Text = ds.Tables(0).Rows(0).Item("STS_PNAME_ARABIC").ToString()
            LBPrimaryContactName.Text = ds.Tables(0).Rows(0).Item("STS_FFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_FMIDNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_FLASTNAME").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STS_POCC_ARABIC")) Then
            TextOccupation.Text = ds.Tables(0).Rows(0).Item("STS_FOCC").ToString()
            LbOccupation.Text = ds.Tables(0).Rows(0).Item("STS_FOCC").ToString()
        Else
            TextOccupation.Text = ds.Tables(0).Rows(0).Item("STS_POCC_ARABIC").ToString()
            LbOccupation.Text = ds.Tables(0).Rows(0).Item("STS_FOCC").ToString()
        End If
        lstrComADDR = ds.Tables(0).Rows(0).Item("STS_FCOMAPARTNO").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMBLDG").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMAREA").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMSTREET").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMCITY").ToString()
        If lstrComADDR = "" Then
            lstrComADDR = ds.Tables(0).Rows(0).Item("STS_FCOMADDR1").ToString()
        End If
        If lstrComADDR = "" Then
            lstrComADDR = ds.Tables(0).Rows(0).Item("STS_FCOMPOBox").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_FCOMCity").ToString()
        End If
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STS_PADDR_ARABIC")) Then
            TextAddress1.Text = lstrComADDR
            Lbaddress.Text = lstrComADDR
        Else
            TextAddress1.Text = ds.Tables(0).Rows(0).Item("STS_PADDR_ARABIC").ToString()
            Lbaddress.Text = ds.Tables(0).Rows(0).Item("STS_PADDR_ARABIC").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STS_POFFCADDR_ARABIC")) Then
            TextOfficeAddress.Text = ds.Tables(0).Rows(0).Item("STS_FCOMADDR2").ToString()
            Lboffficeaddress.Text = ds.Tables(0).Rows(0).Item("STS_FCOMADDR2").ToString()
        Else
            TextOfficeAddress.Text = ds.Tables(0).Rows(0).Item("STS_POFFCADDR_ARABIC").ToString()
            Lboffficeaddress.Text = ds.Tables(0).Rows(0).Item("STS_FCOMADDR2").ToString()
        End If

        TextPBNumber.Text = ds.Tables(0).Rows(0).Item("STS_FPRMPOBOX").ToString()
        LBpostboxnumber.Text = ds.Tables(0).Rows(0).Item("STS_FPRMPOBOX").ToString()

        ''Split Telephone Number
        TelephoneNumberSplit(ds.Tables(0).Rows(0).Item("STS_FPRMPHONE").ToString(), TextTelCode1, TextTelCode2, TextTelNumber)
        lbtelephonenumber.Text = ds.Tables(0).Rows(0).Item("STS_FPRMPHONE").ToString()
        TextOfficePOBox.Text = ds.Tables(0).Rows(0).Item("STS_FCOMPOBOX").ToString()
        lbofficepobox.Text = ds.Tables(0).Rows(0).Item("STS_FCOMPOBOX").ToString()
        ''Split Office Phone Number
        TelephoneNumberSplit(ds.Tables(0).Rows(0).Item("STS_FOFFPHONE").ToString(), TextOffCode1, TextOffCode2, TextOfficePhone)
        LBofficephone.Text = ds.Tables(0).Rows(0).Item("STS_FOFFPHONE").ToString()

    End Sub
    Public Sub MotherDetails(ByVal ds As DataSet)
        Dim lstrComADDR As String
        If ds.Tables(0).Rows(0).Item("STS_PNAME_ARABIC").ToString() = "" Then
            TextPrimaryConName.Text = ds.Tables(0).Rows(0).Item("STS_MFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_MMIDNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_MLASTNAME").ToString()

            LBPrimaryContactName.Text = ds.Tables(0).Rows(0).Item("STS_MFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_MMIDNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_MLASTNAME").ToString()
        Else
            TextPrimaryConName.Text = ds.Tables(0).Rows(0).Item("STS_PNAME_ARABIC").ToString()
            LBPrimaryContactName.Text = ds.Tables(0).Rows(0).Item("STS_MFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_MMIDNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_MLASTNAME").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STS_POCC_ARABIC")) Then
            TextOccupation.Text = ds.Tables(0).Rows(0).Item("STS_MOCC").ToString()
            LbOccupation.Text = ds.Tables(0).Rows(0).Item("STS_MOCC").ToString()
        Else
            TextOccupation.Text = ds.Tables(0).Rows(0).Item("STS_POCC_ARABIC").ToString()
            LbOccupation.Text = ds.Tables(0).Rows(0).Item("STS_MOCC").ToString()
        End If

        lstrComADDR = ds.Tables(0).Rows(0).Item("STS_MCOMAPARTNO").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMBLDG").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMAREA").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMSTREET").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMCITY").ToString()
        If lstrComADDR = "" Then
            lstrComADDR = ds.Tables(0).Rows(0).Item("STS_MCOMADDR1").ToString()
        End If
        If lstrComADDR = "" Then
            lstrComADDR = ds.Tables(0).Rows(0).Item("STS_MCOMPOBox").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMCity").ToString()
        End If
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STS_PADDR_ARABIC")) Then
            TextAddress1.Text = lstrComADDR
            Lbaddress.Text = lstrComADDR
        Else
            TextAddress1.Text = ds.Tables(0).Rows(0).Item("STS_PADDR_ARABIC").ToString()
            Lbaddress.Text = ds.Tables(0).Rows(0).Item("STS_PADDR_ARABIC").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STS_POFFCADDR_ARABIC")) Then
            TextOfficeAddress.Text = ds.Tables(0).Rows(0).Item("STS_MCOMADDR1").ToString()
            Lboffficeaddress.Text = ds.Tables(0).Rows(0).Item("STS_MCOMADDR1").ToString()
        Else
            TextOfficeAddress.Text = ds.Tables(0).Rows(0).Item("STS_POFFCADDR_ARABIC").ToString()
            Lboffficeaddress.Text = ds.Tables(0).Rows(0).Item("STS_MCOMADDR1").ToString()
        End If

        TextPBNumber.Text = ds.Tables(0).Rows(0).Item("STS_MPRMPOBOX").ToString()
        LBpostboxnumber.Text = ds.Tables(0).Rows(0).Item("STS_MPRMPOBOX").ToString()
        ''Split Telephone Number
        TelephoneNumberSplit(ds.Tables(0).Rows(0).Item("STS_MPRMPHONE").ToString(), TextTelCode1, TextTelCode2, TextTelNumber)

        lbtelephonenumber.Text = ds.Tables(0).Rows(0).Item("STS_MPRMPHONE").ToString()
        TextOfficePOBox.Text = ds.Tables(0).Rows(0).Item("STS_MCOMPOBOX").ToString()
        lbofficepobox.Text = ds.Tables(0).Rows(0).Item("STS_MCOMPOBOX").ToString()
        ''Split Office Phone Number
        TelephoneNumberSplit(ds.Tables(0).Rows(0).Item("STS_MOFFPHONE").ToString(), TextOffCode1, TextOffCode2, TextOfficePhone)

        LBofficephone.Text = ds.Tables(0).Rows(0).Item("STS_MOFFPHONE").ToString()
    End Sub
    Public Sub GuardianDetails(ByVal ds As DataSet)
        Dim lstrComADDR As String
        If ds.Tables(0).Rows(0).Item("STS_PNAME_ARABIC").ToString() = "" Then
            TextPrimaryConName.Text = ds.Tables(0).Rows(0).Item("STS_GFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_GMIDNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_GLASTNAME").ToString()
            LBPrimaryContactName.Text = ds.Tables(0).Rows(0).Item("STS_GFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_GMIDNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_GLASTNAME").ToString()
        Else
            TextPrimaryConName.Text = ds.Tables(0).Rows(0).Item("STS_PNAME_ARABIC").ToString()
            LBPrimaryContactName.Text = ds.Tables(0).Rows(0).Item("STS_GFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_GMIDNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STS_GLASTNAME").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STS_POCC_ARABIC")) Then
            TextOccupation.Text = ds.Tables(0).Rows(0).Item("STS_GOCC").ToString()
            LbOccupation.Text = ds.Tables(0).Rows(0).Item("STS_GOCC").ToString()
        Else
            TextOccupation.Text = ds.Tables(0).Rows(0).Item("STS_POCC_ARABIC").ToString()
            LbOccupation.Text = ds.Tables(0).Rows(0).Item("STS_GOCC").ToString()
        End If


        lstrComADDR = ds.Tables(0).Rows(0).Item("STS_MCOMAPARTNO").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMBLDG").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMAREA").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMSTREET").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMCITY").ToString()
        If lstrComADDR = "" Then
            lstrComADDR = ds.Tables(0).Rows(0).Item("STS_MCOMADDR1").ToString()
        End If
        If lstrComADDR = "" Then
            lstrComADDR = ds.Tables(0).Rows(0).Item("STS_MCOMPOBox").ToString() + " " + ds.Tables(0).Rows(0).Item("STS_MCOMCity").ToString()
        End If
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STS_PADDR_ARABIC")) Then
            TextAddress1.Text = lstrComADDR
            Lbaddress.Text = lstrComADDR
        Else
            TextAddress1.Text = ds.Tables(0).Rows(0).Item("STS_PADDR_ARABIC").ToString()
            Lbaddress.Text = ds.Tables(0).Rows(0).Item("STS_PADDR_ARABIC").ToString()
        End If


        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STS_POFFCADDR_ARABIC")) Then
            TextOfficeAddress.Text = ds.Tables(0).Rows(0).Item("STS_GCOMADDR1").ToString()
            Lboffficeaddress.Text = ds.Tables(0).Rows(0).Item("STS_GCOMADDR1").ToString()
        Else
            TextOfficeAddress.Text = ds.Tables(0).Rows(0).Item("STS_POFFCADDR_ARABIC").ToString()
            Lboffficeaddress.Text = ds.Tables(0).Rows(0).Item("STS_GCOMADDR1").ToString()
        End If

        TextPBNumber.Text = ds.Tables(0).Rows(0).Item("STS_GPRMPOBOX").ToString()
        LBpostboxnumber.Text = ds.Tables(0).Rows(0).Item("STS_GPRMPOBOX").ToString()
        ''Split Telephone Number
        TelephoneNumberSplit(ds.Tables(0).Rows(0).Item("STS_GPRMPHONE").ToString(), TextTelCode1, TextTelCode2, TextTelNumber)
        lbtelephonenumber.Text = ds.Tables(0).Rows(0).Item("STS_GPRMPHONE").ToString()

        TextOfficePOBox.Text = ds.Tables(0).Rows(0).Item("STS_GCOMPOBOX").ToString()
        lbofficepobox.Text = ds.Tables(0).Rows(0).Item("STS_GCOMPOBOX").ToString()
        ''Split Office Phone Number
        TelephoneNumberSplit(ds.Tables(0).Rows(0).Item("STS_GOFFPHONE").ToString(), TextOffCode1, TextOffCode2, TextOfficePhone)
        LBofficephone.Text = ds.Tables(0).Rows(0).Item("STS_GOFFPHONE").ToString()

    End Sub
    Public Sub PreviousSchools(ByVal ds As DataSet)
        ''School 1
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PREVSCHI_ARABIC")) Then
            TextPreviosSchool1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI").ToString()
        Else
            TextPreviosSchool1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI_ARABIC").ToString()
            'lbpreviousschool1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PREVSCHI")) Then
        Else
            lbpreviousschool1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PREVSCHI_LASTATTDATE")) Or ds.Tables(0).Rows(0).Item("STU_PREVSCHI_LASTATTDATE").ToString().Trim() = "" Then
        Else
            TextLeaveDate1.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PREVSCHI_LASTATTDATE")).ToString("dd/MMM/yyyy")
            lbLeavedate1.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PREVSCHI_LASTATTDATE")).ToString("dd/MMM/yyyy")
        End If
        'TextAccdYear1.Text = TrimDate(TextLeaveDate1.Text)
        TextAccdYear1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI_ACDYR").ToString()
        LBAcadamicyear1.Text = TrimDate(TextLeaveDate1.Text)
        If ds.Tables(0).Rows(0).Item("STU_PREVSCHI_CLM").ToString() <> "8" Then
            TextSyllabus1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI_CLM").ToString()

            If (ds.Tables(0).Rows(0).Item("STU_PREVSCHI_CLM").ToString() <> "") Then

                LBSyllabus1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI_CLM").ToString()
            End If

        End If

        TextCertificate1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI_CERTIFICATE").ToString()
        If (ds.Tables(0).Rows(0).Item("STU_PREVSCHI_CERTIFICATE").ToString() <> "") Then

            LBCertificate1.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHI_CERTIFICATE").ToString()
        End If


        ''School 2
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PREVSCHII_ARABIC")) Then
            TextPreviosSchool2.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHII").ToString()

        Else
            TextPreviosSchool2.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHII_ARABIC").ToString()

        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PREVSCHII")) Then
        Else
            LBPreviousSchool2.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHII").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PREVSCHII_LASTATTDATE")) Or ds.Tables(0).Rows(0).Item("STU_PREVSCHII_LASTATTDATE").ToString().Trim() = "" Then
        Else
            TextLeaveDate2.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PREVSCHII_LASTATTDATE")).ToString("dd/MMM/yyyy")
            LBLeaveDate2.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PREVSCHII_LASTATTDATE")).ToString("dd/MMM/yyyy")
        End If
        LBAcademicYear2.Text = TrimDate(TextLeaveDate2.Text)
        TextAccdYear2.Text = TrimDate(TextLeaveDate2.Text)
        If ds.Tables(0).Rows(0).Item("STU_PREVSCHII_CLM").ToString() <> "8" Then
            TextSyllabus2.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHII_CLM").ToString()

            If (ds.Tables(0).Rows(0).Item("STU_PREVSCHII_CLM").ToString() <> "") Then

                LBSyllabus2.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHII_CLM").ToString()
            End If

        End If
        TextCertificate2.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHII_CERTIFICATE").ToString()

        If (ds.Tables(0).Rows(0).Item("STU_PREVSCHII_CERTIFICATE").ToString() <> "") Then

            LBCertificate2.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHII_CERTIFICATE").ToString()
        End If

        ''School 3
        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_ARABIC")) Then
            TextPreviosSchool3.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHIII").ToString()

        Else
            TextPreviosSchool3.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_ARABIC").ToString()

        End If

        If (ds.Tables(0).Rows(0).Item("STU_PREVSCHIII").ToString <> "") Then
            LBPreviousSchool3.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHIII").ToString()
        End If

        If Convert.IsDBNull(ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_LASTATTDATE")) Or ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_LASTATTDATE").ToString().Trim() = "" Then
        Else
            TextLeaveDate3.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_LASTATTDATE")).ToString("dd/MMM/yyyy")
            LBLeaveDate3.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_LASTATTDATE")).ToString("dd/MMM/yyyy")
        End If
        TextAccdYear3.Text = TrimDate(TextLeaveDate3.Text)
        LBAcademicYear3.Text = TrimDate(TextLeaveDate3.Text)
        If ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_CLM").ToString() <> "8" Then
            TextSyllabus3.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_CLM").ToString()
            If (ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_CLM").ToString <> "") Then
                LBSyllabus3.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_CLM").ToString()
            End If
        End If
        TextCertificate3.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_CERTIFICATE").ToString()

        If (ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_CERTIFICATE").ToString <> "") Then
            LBCertificate3.Text = ds.Tables(0).Rows(0).Item("STU_PREVSCHIII_CERTIFICATE").ToString()
        End If

    End Sub
#Region "text"
    Sub labelcheck()
        If Lbstage.Text = "" Then
            Lbstage.Text = "&nbsp;"
        End If
        If LBgrade.Text = "" Then
            LBgrade.Text = "&nbsp;"
        End If
        If LBpassportname.Text = "" Then
            LBpassportname.Text = "&nbsp;"
        End If
        If LBnationality.Text = "" Then
            LBnationality.Text = "&nbsp;"
        End If
        If LBreligion.Text = "" Then
            LBreligion.Text = "&nbsp;"
        End If
        If LBplaceofbirth.Text = "" Then
            LBplaceofbirth.Text = "&nbsp;"
        End If
        If lbdob.Text = "" Then
            lbdob.Text = "&nbsp;"
        End If
        If LBpassportnumber.Text = "" Then
            LBpassportnumber.Text = "&nbsp;"
        End If
        If LBpassportissue.Text = "" Then
            LBpassportissue.Text = "&nbsp;"
        End If
        If LBPassportIssueDate.Text = "" Then
            LBPassportIssueDate.Text = "&nbsp;"
        End If
        If LBPassportExpiryDate.Text = "" Then
            LBPassportExpiryDate.Text = "&nbsp;"
        End If
        If LBvisanumber.Text = "" Then
            LBvisanumber.Text = "&nbsp;"
        End If
        If LBvisafrom.Text = "" Then
            LBvisafrom.Text = "&nbsp;"
        End If
        If LBvisato.Text = "" Then
            LBvisato.Text = "&nbsp;"
        End If
        If LBPrimaryContactName.Text = "" Then
            LBPrimaryContactName.Text = "&nbsp;"
        End If
        If LbOccupation.Text = "" Then
            LbOccupation.Text = "&nbsp;"
        End If
        If Lbaddress.Text = "" Then
            Lbaddress.Text = "&nbsp;"
        End If
        If LBpostboxnumber.Text = "" Then
            LBpostboxnumber.Text = "&nbsp;"
        End If
        If lbtelephonenumber.Text = "" Then
            lbtelephonenumber.Text = "&nbsp;"
        End If
        If Lboffficeaddress.Text = "" Then
            Lboffficeaddress.Text = "&nbsp;"
        End If
        If lbofficepobox.Text = "" Then
            lbofficepobox.Text = "&nbsp;"
        End If
        If LBofficephone.Text = "" Then
            LBofficephone.Text = "&nbsp;"
        End If
        If lbpreviousschool1.Text = "" Then
            lbpreviousschool1.Text = "&nbsp;"
        End If
        If lbLeavedate1.Text = "" Then
            lbLeavedate1.Text = "&nbsp;"
        End If
        If LBAcadamicyear1.Text = "" Then
            LBAcadamicyear1.Text = "&nbsp;"
        End If
        If LBSyllabus1.Text = "" Then
            LBSyllabus1.Text = "&nbsp;"
        End If
        If LBCertificate1.Text = "" Then
            LBCertificate1.Text = "&nbsp;"
        End If
        If LBPreviousSchool2.Text = "" Then
            LBPreviousSchool2.Text = "&nbsp;"
        End If
        If LBLeaveDate2.Text = "" Then
            LBLeaveDate2.Text = "&nbsp;"
        End If
        If LBAcademicYear2.Text = "" Then
            LBAcademicYear2.Text = "&nbsp;"
        End If
        If LBSyllabus2.Text = "" Then
            LBSyllabus2.Text = "&nbsp;"
        End If
        If LBCertificate2.Text = "" Then
            LBCertificate2.Text = "&nbsp;"
        End If
        If LBPreviousSchool3.Text = "" Then
            LBPreviousSchool3.Text = "&nbsp;"
        End If
        If LBLeaveDate3.Text = "" Then
            LBLeaveDate3.Text = "&nbsp;"
        End If
        If LBAcademicYear3.Text = "" Then
            LBAcademicYear3.Text = "&nbsp;"
        End If
        If LBSyllabus3.Text = "" Then
            LBSyllabus3.Text = "&nbsp;"
        End If
        If LBCertificate3.Text = "" Then
            LBCertificate3.Text = "&nbsp;"
        End If
        If Lbstage.Text = "" Then
            Lbstage.Text = "&nbsp;"
        End If

    End Sub
#End Region
    Public Function TrimDate(ByVal val As String) As String
        Dim split As String()
        Dim returnval As String = ""
        If val <> "" Then
            split = val.Split("/")
            returnval = split(2).ToString()
        End If
        Return returnval
    End Function
    Public Sub TelephoneNumberSplit(ByVal Number As String, ByVal Code1 As TextBox, ByVal Code2 As TextBox, ByVal Code3 As TextBox)
        Dim split As String()

        split = Number.Split("-")
        If split.Length = "3" Then
            Code1.Text = split(0)
            Code2.Text = split(1)
            Code3.Text = split(2)
        Else
            Code3.Text = split(0)
        End If


    End Sub
    Public Sub BindRelation()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select * from RELATIONTYPE_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DropPrimaryContact.DataSource = ds
        DropPrimaryContact.DataTextField = "RELN_DESCRIPTION_ARABIC"
        DropPrimaryContact.DataValueField = "RELN_CODE"
        DropPrimaryContact.DataBind()

    End Sub
    Public Sub BindReligion()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select * from RELIGION_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DropRelegion.DataSource = ds
        DropRelegion.DataValueField = "RLG_ID"
        DropRelegion.DataTextField = "RLG_ARABIC"
        DropRelegion.DataBind()
    End Sub
    Public Sub Religion(ByVal RLG_ID As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select * from RELIGION_M where RLG_ID='" & RLG_ID & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        LBreligion.Text = ds.Tables(0).Rows(0)("RLG_DESCR").ToString()
    End Sub
    Public Sub BindNationality()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select * from COUNTRY_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DropNationality.DataSource = ds
        DropNationality.DataTextField = "CTY_ARABIC"
        DropNationality.DataValueField = "CTY_ID"
        DropNationality.DataBind()
    End Sub
    Public Sub Nationality(ByVal cty_id As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select * from COUNTRY_M where CTY_ID='" & cty_id & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        LBnationality.Text = ds.Tables(0).Rows(0)("CTY_DESCR").ToString()
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim Encr_decrData As New Encryption64
        Try
            lblmessage.Text = ""
            If validateEntry() Then

                Dim pParms(37) As SqlClient.SqlParameter
                Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

                pParms(0) = New SqlClient.SqlParameter("@OPTION", DropPrimaryContact.SelectedValue)

                pParms(1) = New SqlClient.SqlParameter("@STU_ID", Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+")))
                pParms(2) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("sBsuid"))

                pParms(3) = New SqlClient.SqlParameter("@GRD_STAGE_ARABIC", DropStage.SelectedValue)
                pParms(4) = New SqlClient.SqlParameter("@GRD_DISPLAY_ARABIC", DropGrade.SelectedValue)
                If TextDOB.Text.Trim() <> "" Then
                    pParms(5) = New SqlClient.SqlParameter("@STU_DOB", Convert.ToDateTime(TextDOB.Text.Trim()))
                Else
                    pParms(5) = New SqlClient.SqlParameter("@STU_DOB", System.DBNull.Value)
                End If


                pParms(6) = New SqlClient.SqlParameter("@STU_RLG_ID", DropRelegion.SelectedValue)
                pParms(7) = New SqlClient.SqlParameter("@STU_NATIONALITY", DropNationality.SelectedValue)
                pParms(8) = New SqlClient.SqlParameter("@STU_POB", TextPlaceOfBirth.Text.Trim())
                pParms(9) = New SqlClient.SqlParameter("@STU_PASPRTNAME", TextPassportName.Text.Trim())
                pParms(10) = New SqlClient.SqlParameter("@STU_PASPRTNO", TextPassportNumber.Text.Trim())
                pParms(11) = New SqlClient.SqlParameter("@STU_PASPRTISSPLACE", TextPassportIssue.Text.Trim())
                If TextPassportExpirity.Text.Trim() <> "" Then
                    pParms(12) = New SqlClient.SqlParameter("@STU_PASPRTEXPDATE", Convert.ToDateTime(TextPassportExpirity.Text.Trim()))
                Else
                    pParms(12) = New SqlClient.SqlParameter("@STU_PASPRTEXPDATE", System.DBNull.Value)
                End If
                pParms(13) = New SqlClient.SqlParameter("@STU_VISANO", TextVisaNumber.Text.Trim())
                If TextVisaFrom.Text.Trim() <> "" Then
                    pParms(14) = New SqlClient.SqlParameter("@STU_VISAISSDATE", Convert.ToDateTime(TextVisaFrom.Text.Trim()))
                Else
                    pParms(14) = New SqlClient.SqlParameter("@STU_VISAISSDATE", System.DBNull.Value)
                End If
                If TextVisaTo.Text.Trim() <> "" Then
                    pParms(15) = New SqlClient.SqlParameter("@STU_VISAEXPDATE", Convert.ToDateTime(TextVisaTo.Text.Trim()))
                Else
                    pParms(15) = New SqlClient.SqlParameter("@STU_VISAEXPDATE", System.DBNull.Value)
                End If

                pParms(16) = New SqlClient.SqlParameter("@STS_FIRSTNAME", TextPrimaryConName.Text.Trim())
                pParms(17) = New SqlClient.SqlParameter("@STS_OCC", TextOccupation.Text.Trim())
                pParms(18) = New SqlClient.SqlParameter("@STS_PRMADDR1", TextAddress1.Text.Trim())
                pParms(19) = New SqlClient.SqlParameter("@STS_PRMPOBOX", TextPBNumber.Text.Trim())
                pParms(20) = New SqlClient.SqlParameter("@STS_PRMPHONE", TextTelCode1.Text.Trim() & "-" & TextTelCode2.Text.Trim() & "-" & TextTelNumber.Text.Trim())
                pParms(21) = New SqlClient.SqlParameter("@STS_COMADDR1", TextOfficeAddress.Text.Trim())
                pParms(22) = New SqlClient.SqlParameter("@STS_COMPOBOX", TextOfficePOBox.Text.Trim())
                pParms(23) = New SqlClient.SqlParameter("@STS_OFFPHONE", TextOffCode1.Text.Trim() & "-" & TextOffCode2.Text.Trim() & "-" & TextOfficePhone.Text.Trim())

                pParms(24) = New SqlClient.SqlParameter("@STU_PREVSCHI_ARABIC", TextPreviosSchool1.Text.Trim())
                If TextLeaveDate1.Text <> "" Then
                    pParms(25) = New SqlClient.SqlParameter("@STU_PREVSCHI_LASTATTDATE", Convert.ToDateTime(TextLeaveDate1.Text.Trim()))
                Else
                    pParms(25) = New SqlClient.SqlParameter("@STU_PREVSCHI_LASTATTDATE", System.DBNull.Value)
                End If
                pParms(26) = New SqlClient.SqlParameter("@STU_PREVSCHI_CLM", TextSyllabus1.Text.Trim())
                pParms(27) = New SqlClient.SqlParameter("@STU_PREVSCHI_CERTIFICATE", TextCertificate1.Text.Trim())
                If TextAccdYear1.Text <> "" Then
                    pParms(28) = New SqlClient.SqlParameter("@STU_PREVSCHI_ACDYR", TextAccdYear1.Text.Trim())
                Else
                    pParms(28) = New SqlClient.SqlParameter("@STU_PREVSCHI_ACDYR", System.DBNull.Value)
                End If
                pParms(29) = New SqlClient.SqlParameter("@STU_PREVSCHII_ARABIC", TextPreviosSchool2.Text.Trim())
                If TextLeaveDate2.Text <> "" Then
                    pParms(30) = New SqlClient.SqlParameter("@STU_PREVSCHII_LASTATTDATE", Convert.ToDateTime(TextLeaveDate2.Text.Trim()))
                Else
                    pParms(30) = New SqlClient.SqlParameter("@STU_PREVSCHII_LASTATTDATE", DBNull.Value)
                End If
                pParms(31) = New SqlClient.SqlParameter("@STU_PREVSCHII_CLM", TextSyllabus2.Text.Trim())
                pParms(32) = New SqlClient.SqlParameter("@STU_PREVSCHII_CERTIFICATE", TextCertificate2.Text.Trim())

                pParms(33) = New SqlClient.SqlParameter("@STU_PREVSCHIII_ARABIC", TextPreviosSchool3.Text.Trim())
                If TextAccdYear3.Text <> "" Then
                    pParms(34) = New SqlClient.SqlParameter("@STU_PREVSCHIII_LASTATTDATE", Convert.ToDateTime(TextLeaveDate3.Text.Trim()))
                Else
                    pParms(34) = New SqlClient.SqlParameter("@STU_PREVSCHIII_LASTATTDATE", System.DBNull.Value)
                End If
                pParms(35) = New SqlClient.SqlParameter("@STU_PREVSCHIII_CLM", TextSyllabus3.Text.Trim())
                pParms(36) = New SqlClient.SqlParameter("@STU_PREVSCHIII_CERTIFICATE", TextCertificate3.Text.Trim())
                If TextPassportissuedate.Text.Trim() <> "" Then
                    pParms(37) = New SqlClient.SqlParameter("@STU_PASPRTISSDATE", Convert.ToDateTime(TextPassportissuedate.Text.Trim()))
                Else
                    pParms(37) = New SqlClient.SqlParameter("@STU_PASPRTISSDATE", System.DBNull.Value)
                End If

                lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "UpdateStudentDetails(Arabic)", pParms)
                lblEmessage.Text = ""
                BindContols()

                If lblmessage.Text = "Updated" Then
                    Panel1.Visible = False
                    Panel2.Visible = True
                End If

            End If
        Catch ex As Exception
            lblmessage.Text = "Error." & ex.Message
        End Try
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        BindContols()
    End Sub
    Public Function validateEntry() As Boolean
        Try


            Dim validate As Boolean = True
            Dim Emessage As String = "<b>Following Fields needed attention <b><br>"
            If TextDOB.Text = "" Then
                Emessage = Emessage & "" & "'DOB' must be specified.<br>"
                validate = False
            End If
           

            lblEmessage.Text = Emessage
            Return validate
        Catch ex As Exception
            ''lblmessage.Text = "Error In Date (or) Date not Specified"...
            Return True
        End Try
    End Function

    Protected Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        Dim lstrPWD As String

        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBsuid"))

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_MOE_FILETYPE", pParms)
            While reader.Read
                lstrPWD = Convert.ToString(reader("BSU_MOE_FILETYPE"))
            End While
        End Using


        Dim Encr_decrData As New Encryption64
        If Session("SBsuid") = "165010" Then
            Response.Write("<Script> window.open('studMOEPrint_RAK.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        ElseIf lstrPWD = "13" Then
            Response.Write("<Script> window.open('studMOEPrint_Sharjah.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        ElseIf lstrPWD = "11_1" Then
            Response.Write("<Script> window.open('studMOEPrint_AUH.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        ElseIf lstrPWD = "11" Then
            Response.Write("<Script> window.open('studMOEPrint_CHS.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        ElseIf lstrPWD = "14" Then
            Response.Write("<Script> window.open('studMOEPrint_ALN.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        ElseIf lstrPWD = "12_1" Then
            Response.Write("<Script> window.open('studMOEPrint_KHDA.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")
        ElseIf lstrPWD = "00" Then
            Response.Write("<Script> window.open('studMOEPrint.aspx?stuid=" & Hiddenstuid.Value & "') </Script>")

        End If

    End Sub

    Protected Sub lnkBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
        If (Session("SBsuid") <> "131001" And Session("SBsuid") <> "131002" And Session("SBsuid") <> "133006") Then
            Response.Redirect("studministryapproval.aspx" & mInfo)
        Else
            Response.Redirect("studministryapproval_Sharjah.aspx" & mInfo)
        End If

    End Sub
End Class
