Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Globalization

Partial Class Students_studSetHoliday
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    'Dim Session("dsEvents") As New DataSet

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        calDatepicker.Attributes.Add("title", String.Empty)


        If Page.IsPostBack = False Then

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059001") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else


                ViewState("BSU_WEEKEND1") = ""
                ViewState("BSU_WEEKEND2") = ""

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Call bindAcademicYear()
                Call Populate_MonthList(Now.Date)
                Call Populate_YearList(Now.Date)
                Call getStart_EndDate()
                Call BindTree_GradeSection()
                Call CountSectionGrade()
                Call bindWeekEndstatus()
                calDatepicker.FirstDayOfWeek = WebControls.FirstDayOfWeek.Sunday

                If ViewState("datamode") = "view" Then

                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                    Call EditRecordBind(ViewState("viewid"))
                    Call SetState()
                ElseIf ViewState("datamode") = "add" Then

                    txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                    ddlAcdYear.ClearSelection()
                    ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                    calDatepicker.TodaysDate = Now.Date
                    Call ResetState()
                End If

                ltWeekend.Text = StrConv(ViewState("BSU_WEEKEND1"), VbStrConv.ProperCase) & "  &  " & StrConv(ViewState("BSU_WEEKEND2"), VbStrConv.ProperCase)
                ltWeekend1.Text = StrConv(ViewState("BSU_WEEKEND1"), VbStrConv.ProperCase)
                ltWeekend2.Text = StrConv(ViewState("BSU_WEEKEND2"), VbStrConv.ProperCase)
                ltWeekendlog1.Text = "Select if Log Book entry on " & ltWeekend1.Text & " is required."
                ltWeekendlog2.Text = "Select if Log Book entry on " & ltWeekend2.Text & " is required."
                txtGradeSection.Attributes.Add("readonly", "readonly")


                '  Session("School_Holiday") = CreateDataTable()


            End If
        End If

    End Sub
    Public Sub BindcheckNodes(ByVal nodeList As TreeNodeCollection, ByVal menuCode As String) 'in view mode bind it to the grid view
        If Not nodeList Is Nothing Then
            For Each nodec As TreeNode In nodeList

                Dim strNodeValue As String = nodec.Value

                If strNodeValue = menuCode Then

                    nodec.Checked = True
                End If
                BindcheckNodes(nodec.ChildNodes, menuCode)
            Next
        End If
    End Sub
    Sub BindTree_GradeSection() 'for orginating tree view for the grade and section
        Dim ACD_ID As String = String.Empty
        If ddlAcdYear.SelectedIndex = -1 Then
            ACD_ID = ""
        Else
            ACD_ID = ddlAcdYear.SelectedItem.Value
        End If
        Dim dtTable As DataTable = AccessStudentClass.PopulateGrade_Section(ACD_ID)
        ' PROCESS Filter
        Dim dvGRD_DESCR As New DataView(dtTable, "", "GRD_DESCR", DataViewRowState.OriginalRows)

        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim ienumGRD_DESCR As IEnumerator = dvGRD_DESCR.GetEnumerator
        Dim drGRD_DESCR As DataRowView
        While (ienumGRD_DESCR.MoveNext())
            'Processes List
            drGRD_DESCR = ienumGRD_DESCR.Current

            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drGRD_DESCR("GRD_DESCR") Then
                    contains = True
                End If
            End While
            Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("GRD_DESCR"), drGRD_DESCR("GRD_ID"))
            If contains Then
                Continue While
            End If

            Dim strGRADE_SECT As String = "GRD_DESCR = '" & _
            drGRD_DESCR("GRD_DESCR") & "'"
            Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "SCT_DESCR", DataViewRowState.OriginalRows)
            Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
            While (ienumGRADE_SECT.MoveNext())
                Dim drGRADE_SECT As DataRowView = ienumGRADE_SECT.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drGRADE_SECT("SCT_DESCR"), drGRADE_SECT("SCT_ID"))
                trNodeGRD_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeGRD_DESCR)
        End While
        tvGrade.Nodes.Clear()
        tvGrade.Nodes.Add(trSelectAll)
        tvGrade.DataBind()

    End Sub

    'Private Function CreateDataTable() As DataTable
    '    Dim dtDt As DataTable
    '    dtDt = New DataTable
    '    Try
    '        Dim HId As New DataColumn("id", System.Type.GetType("System.String"))
    '        Dim HSCH_ID As New DataColumn("SCH_ID", System.Type.GetType("System.String"))
    '        Dim HSCH_ACD_ID As New DataColumn("SCH_ACD_ID", System.Type.GetType("System.String"))

    '        Dim HSCH_DTFROM As New DataColumn("SCH_DTFROM", System.Type.GetType("System.DateTime"))
    '        Dim HSCH_DTTO As New DataColumn("SCH_DTTO", System.Type.GetType("System.DateTime"))
    '        Dim HSCH_REMARKS As New DataColumn("SCH_REMARKS", System.Type.GetType("System.String"))
    '        Dim HSCH_TYPE As New DataColumn("SCH_TYPE", System.Type.GetType("System.String"))
    '        Dim HSCH_DAY_SL As New DataColumn("SCH_DAY_SL", System.Type.GetType("System.String"))
    '        Dim HStatus As New DataColumn("Status", System.Type.GetType("System.String"))
    '        dtDt.Columns.Add(HId)
    '        dtDt.Columns.Add(HSCH_ID)
    '        dtDt.Columns.Add(HSCH_ACD_ID)
    '        dtDt.Columns.Add(HSCH_DTFROM)
    '        dtDt.Columns.Add(HSCH_DTTO)
    '        dtDt.Columns.Add(HSCH_REMARKS)
    '        dtDt.Columns.Add(HSCH_TYPE)
    '        dtDt.Columns.Add(HSCH_DAY_SL)
    '        dtDt.Columns.Add(HStatus)
    '        Return dtDt
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, "datatable")
    '        Return dtDt
    '    End Try
    'End Function

    Function TotDay() As Integer
        Dim NumberOfDays As Integer = 0
        Dim startDate, endDate As Date
        startDate = txtFrom.Text
        endDate = txtTo.Text
        ' regular

        Dim Difference As System.TimeSpan

        ' Default instance value of datetime is minvalue

        If Not startDate = Date.MinValue And endDate > startDate Then

            Difference = endDate.Subtract(startDate)

        End If

        ' Use the timespan to loop through and add

        Dim i As Integer = 0

        While i <= Difference.Days

            IncrementCount(NumberOfDays, startDate, i)

            i += 1

        End While

        TotDay = NumberOfDays

    End Function

    Public Sub IncrementCount(ByRef Days As Integer, ByVal Startdate As DateTime, ByVal DaysToIncrement As Integer)


        'need to check based on the working weekend also
        If Not (UCase(Startdate.AddDays(DaysToIncrement).DayOfWeek.ToString) = ViewState("BSU_WEEKEND1").ToUpper.Trim Or UCase(Startdate.AddDays(DaysToIncrement).DayOfWeek.ToString) = ViewState("BSU_WEEKEND2").ToUpper.Trim) Then
            Days += 1
        End If

    End Sub
    Sub SetState()
        calDatepicker.Enabled = False
        ddlAcdYear.Enabled = False
        ddlAttReq.Enabled = False
        txtRemarks.Enabled = False
        chkLogBook1.Enabled = False
        chkLogBook2.Enabled = False
        chkWeekend1.Enabled = False
        chkWeekend2.Enabled = False
        LinkButton1.Enabled = False
        txtFrom.Enabled = False
        txtTo.Enabled = False
        txtRemarks.Enabled = False

    End Sub

    Sub ResetState()
        calDatepicker.Enabled = True
        ddlAcdYear.Enabled = True
        ddlAttReq.Enabled = True
        txtRemarks.Enabled = True
        chkLogBook1.Enabled = True
        chkLogBook2.Enabled = True
        chkWeekend1.Enabled = True
        chkWeekend2.Enabled = True
        LinkButton1.Enabled = True
        txtFrom.Enabled = True
        txtTo.Enabled = True
        txtRemarks.Enabled = True
    End Sub
    Sub clearContent()
        txtRemarks.Text = ""
        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtGradeSection.Text = ""
        chkLogBook1.Checked = False
        chkLogBook2.Checked = False
        chkWeekend1.Checked = False
        chkWeekend2.Checked = False
        ddlAttReq.ClearSelection()
        ddlAttReq.SelectedIndex = 0
        ddlAcdYear.ClearSelection()
        ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        ClearAllCheck(tvGrade.Nodes)
    End Sub

    Sub Populate_MonthList(ByVal p_seldt As Date)
        ddlMonth.Items.Add("January")
        ddlMonth.Items.Add("February")
        ddlMonth.Items.Add("March")
        ddlMonth.Items.Add("April")
        ddlMonth.Items.Add("May")
        ddlMonth.Items.Add("June")
        ddlMonth.Items.Add("July")
        ddlMonth.Items.Add("August")
        ddlMonth.Items.Add("September")
        ddlMonth.Items.Add("October")
        ddlMonth.Items.Add("November")
        ddlMonth.Items.Add("December")
        ddlMonth.Items.FindByValue(MonthName(p_seldt.Month)).Selected = True
    End Sub

    Sub Populate_YearList(ByVal p_seldt As Date)
        'Year list can be extended
        Dim intYear As Integer
        For intYear = DateTime.Now.Year - 20 To DateTime.Now.Year + 20
            ddlyear.Items.Add(intYear)
        Next
        ddlyear.Items.FindByValue(p_seldt.Year).Selected = True
    End Sub

    Sub bindWeekEndstatus()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademicYear()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id in('" & Session("sBsuid") & "') and acd_clm_id='" & Session("CLM") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function validateDateRange() As String
        Dim CommStr As String = String.Empty
        Dim Acd_SDate As Date = ViewState("ACD_STARTDT")
        Dim Acd_EDate As Date = ViewState("ACD_ENDDT")
        Dim From_date As Date = txtFrom.Text


        If (From_date >= Acd_SDate And From_date <= Acd_EDate) Then
            If txtTo.Text.Trim <> "" Then
                Dim To_Date As Date = txtTo.Text
                If Not ((To_Date >= Acd_SDate And To_Date <= Acd_EDate)) Then
                    CommStr = CommStr & "<div>From Date to To Date must be with in the academic year</div>"
                End If
            End If
        Else
            CommStr = CommStr & "<div>From Date  must be with in the academic year</div>"
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If
    End Function
    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If

        If txtTo.Text.Trim <> "" Then

            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>To Date format is Invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid To date</div>"
                    End If
                End If
            End If
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function

    Sub getStart_EndDate()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query = "select ACD_StartDt,ACD_ENDDT  from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        If ds.Tables(0).Rows.Count > 0 Then
            ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_StartDt"))
            ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_ENDDT"))
        Else
            ViewState("ACD_STARTDT") = ""
            ViewState("ACD_ENDDT") = ""

        End If
    End Sub



    Sub bindevents()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = String.Empty
            If ddlAcdYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcdYear.SelectedItem.Value
            End If
            str_Sql = "SELECT SCH_ID, datediff(dd,SCH_DTFROM,SCH_DTTO) + 1 as totDay, SCH_DTFROM,SCH_DTTO,SCH_REMARKS,SCH_TYPE FROM " & _
        " SCHOOL_HOLIDAY_M  where SCH_bDELETED=0 and SCH_BSU_ID='" & Session("sBSUID") & "' and SCH_ACD_ID='" & ACD_ID & "'"
            Session("dsEvents") = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Protected Sub ddlyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlyear.SelectedIndexChanged, ddlMonth.SelectedIndexChanged
        calDatepicker.TodaysDate = CDate(ddlMonth.SelectedItem.Value & " 1, " & ddlyear.SelectedItem.Value)
        calDatepicker.DataBind()
    End Sub

    Protected Sub calDatepicker_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles calDatepicker.SelectionChanged
        txtFrom.Text = calDatepicker.SelectedDates(0).ToString("dd/MMM/yyyy")
        txtTo.Text = calDatepicker.SelectedDates(calDatepicker.SelectedDates.Count - 1).ToString("dd/MMM/yyyy")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_err As String = String.Empty

        Dim errorMessage As String = String.Empty
        If serverDateValidate() = "0" Then
            If validateDateRange() = "0" Then
                str_err = CallTransaction(errorMessage)

                If str_err = "0" Then
                   
                    lblError.Text = "Record updated successfully"
                Else
                    lblError.Text = errorMessage
                End If
            End If

        End If

    End Sub



    Private Function CallTransaction(ByRef errorMessage As String) As String
        Dim status As String = String.Empty
        Dim SCH_ID As String = String.Empty
        Dim SCH_DATE As Date
        Dim SCH_ACD_ID As String
        If ddlAcdYear.SelectedIndex = -1 Then
            SCH_ACD_ID = ""
        Else
            SCH_ACD_ID = ddlAcdYear.SelectedItem.Value
        End If
        Dim SCH_DTFROM As String = txtFrom.Text
        Dim SCH_DTTO As String = txtTo.Text
        Dim SCH_REMARKS As String = txtRemarks.Text
        Dim SCH_TYPE As String = ddlAttReq.SelectedItem.Value
        Dim SCH_DAY_SL As String = TotDay()
        Dim bDeleted As Boolean

        Dim bGRADEALL As Boolean
        If ViewState("AllGrades") = 1 Then
            bGRADEALL = True
        Else
            bGRADEALL = False
        End If
        Dim WEEKEND1_WORK As String
        If chkWeekend1.Checked = True Then
            WEEKEND1_WORK = ViewState("BSU_WEEKEND1")
        Else
            WEEKEND1_WORK = ""
        End If

        Dim bWEEKEND1_LOG_BOOK As Boolean
        If chkLogBook1.Checked = True Then
            bWEEKEND1_LOG_BOOK = True
        Else
            bWEEKEND1_LOG_BOOK = False
        End If

        Dim WEEKEND2_WORK As String
        If chkWeekend2.Checked = True Then
            WEEKEND2_WORK = ViewState("BSU_WEEKEND2")
        Else
            WEEKEND2_WORK = ""
        End If


        Dim bWEEKEND2_LOG_BOOK As Boolean
        If chkLogBook2.Checked = True Then
            bWEEKEND2_LOG_BOOK = True
        Else
            bWEEKEND2_LOG_BOOK = False
        End If


        Dim New_SCH_ID As String = String.Empty
        Dim bEdit As Boolean

        'check the data mode to do the required operation

        If ViewState("datamode") = "add" Then
            'only insert if applicable to particular grade & section
            If ViewState("AllGrades") = 1 Then
                If GetAddConfirmation_ALLGRADES() > 0 Then

                    errorMessage = "Attendance cannot be set for ALL GRADES since Attendance required for.. is already set between the given date range. Choose selected Grades"
                    Return "1"
                End If
            End If
            Dim transaction As SqlTransaction
            bEdit = False
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    SCH_DATE = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)


                    status = AccessStudentClass.SaveSTUDSCHOOL_HOLIDAY_M(SCH_ID, SCH_DATE, Session("sBsuid"), SCH_ACD_ID, _
                          SCH_DTFROM, SCH_DTTO, SCH_REMARKS, SCH_TYPE, SCH_DAY_SL, bDeleted, bGRADEALL, WEEKEND1_WORK, bWEEKEND1_LOG_BOOK, WEEKEND2_WORK, bWEEKEND2_LOG_BOOK, New_SCH_ID, bEdit, transaction)

                    If status <> 0 Then
                        CallTransaction = "1"
                        errorMessage = "Error while updating School Holidays"
                        Return "1"
                    Else

                        Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes

                        If tvGrade.CheckedNodes.Count > 0 Then
                            For Each node As TreeNode In tvGrade.CheckedNodes
                                If Not node Is Nothing Then
                                    If node.Text <> "Select All" Then
                                        If node.Parent.Text <> "Select All" Then
                                            status = AccessStudentClass.SaveSCHOOL_HOLIDAY_GRADE(New_SCH_ID, SCH_ACD_ID, node.Parent.Value, node.Value, SCH_DTFROM, SCH_DTTO, transaction)
                                            If status <> 0 Then
                                                CallTransaction = "1"
                                                errorMessage = UtilityObj.getErrorMessage(status)
                                                Return "1"
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        End If

                        If status = 0 Then
                            status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), New_SCH_ID, "add", Page.User.Identity.Name.ToString, Me.Page)
                            If status <> 0 Then
                                CallTransaction = "1"
                                errorMessage = "Could not process audit request"
                                Return "1"
                            End If

                        End If
                        '  End If

                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"
                    Call SetState()
                    Call clearContent()

                    CallTransaction = "0"


                Catch ex As Exception

                    errorMessage = "Record could not be Updated"
                Finally
                    If CallTransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using




        ElseIf ViewState("datamode") = "edit" Then

            Dim transaction As SqlTransaction
            bEdit = True
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    SCH_DATE = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)


                    status = AccessStudentClass.SaveSTUDSCHOOL_HOLIDAY_M(ViewState("viewid"), SCH_DATE, Session("sBsuid"), SCH_ACD_ID, _
                          SCH_DTFROM, SCH_DTTO, SCH_REMARKS, SCH_TYPE, SCH_DAY_SL, bDeleted, bGRADEALL, WEEKEND1_WORK, bWEEKEND1_LOG_BOOK, WEEKEND2_WORK, bWEEKEND2_LOG_BOOK, New_SCH_ID, bEdit, transaction)

                    If status <> 0 Then
                        CallTransaction = "1"
                        errorMessage = "Error while updating School Holidays"
                        Return "1"
                    Else
                        'check if applicable for all Grades & Section
                        status = AccessStudentClass.DeleteSCHOOL_HOILDAY_GRADE(ViewState("viewid"), transaction)
                        If status <> 0 Then
                            CallTransaction = "1"
                            errorMessage = UtilityObj.getErrorMessage(1000)
                            Return "1"
                        Else
                            '  If ViewState("AllGrades") = 0 Then 'only insert if applicable to particular grade & section
                            Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes

                            If tvGrade.CheckedNodes.Count > 0 Then
                                For Each node As TreeNode In tvGrade.CheckedNodes
                                    If Not node Is Nothing Then
                                        If node.Text <> "Select All" Then
                                            If node.Parent.Text <> "Select All" Then
                                                status = AccessStudentClass.SaveSCHOOL_HOLIDAY_GRADE(ViewState("viewid"), SCH_ACD_ID, node.Parent.Value, node.Value, SCH_DTFROM, SCH_DTTO, transaction)
                                                If status <> 0 Then
                                                    CallTransaction = "1"
                                                    errorMessage = UtilityObj.getErrorMessage(status)
                                                    Return "1"
                                                End If
                                            End If
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        If status = 0 Then
                            status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "edit", Page.User.Identity.Name.ToString, Me.Page)
                            If status <> 0 Then
                                CallTransaction = "1"
                                errorMessage = "Could not process audit request"
                                Return "1"
                            End If

                        End If

                    End If
                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    CallTransaction = "0"
                    Call SetState()
                    Call clearContent()

                    CallTransaction = "0"

                Catch ex As Exception

                    errorMessage = "Record could not be Updated"
                Finally
                    If CallTransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using



        End If

    End Function

    Function GetAddConfirmation_ALLGRADES() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim DTFROM As String = txtFrom.Text
        Dim DTTO As String = txtTo.Text
        Dim ACD_ID As String = String.Empty
        If ddlAcdYear.SelectedIndex = -1 Then
            ACD_ID = ""
        Else
            ACD_ID = ddlAcdYear.SelectedItem.Value
        End If

        str_Sql = "SELECT count(SCH_ID) FROM SCHOOL_HOLIDAY_M  where [SCH_ACD_ID]='" & ACD_ID & "'  and  [SCH_bDELETED]=0 and  " & _
"  ((SCH_DTFROM  between '" & DTFROM & "' and '" & DTTO & "' ) or  (SCH_DTTO  between '" & DTFROM & "' and  '" & DTTO & "'))"


        GetAddConfirmation_ALLGRADES = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

    End Function


    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        getStart_EndDate()
        Call BindTree_GradeSection()
    End Sub

    '    Sub tvGradeBind()
    '        Dim ds As New DataSet
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString



    '        Dim str_Sql As String = "declare  @temptable table(GRM_DISPLAY varchar(20), SCT_DESCR varchar(20), GRD_ID varchar(100), SCT_ID varchar(10),DISPLAYORDER int) " & _
    '    " insert @temptable (GRM_DISPLAY, SCT_DESCR, GRD_ID, SCT_ID,DISPLAYORDER) " & _
    '" SELECT  distinct    GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,SECTION_M.SCT_DESCR as SCT_DESCR, " & _
    ' "  GRADE_BSU_M.GRM_GRD_ID as  GRD_ID,SECTION_M.SCT_ID as SCT_ID,GRADE_M.GRD_DISPLAYORDER as DISPLAYORDER" & _
    '" FROM GRADE_BSU_M INNER JOIN  SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID AND " & _
    '" GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
    '" where  GRADE_BSU_M.GRM_ACD_ID='80' order by GRADE_M.GRD_DISPLAYORDER " & _
    ' " SELECT GRD_ID,GRM_DISPLAY, SCT_ID,SCT_DESCR FROM   @temptable "


    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

    '        ds.DataSetName = "GRADERIGHTS"
    '        ds.Tables(0).TableName = "GRADERIGHT"
    '        'Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("GRADERIGHT").Columns("SCT_ID"), ds.Tables("GRADERIGHT").Columns("GRD_ID"), True)
    '        'relation.Nested = False
    '        'ds.Relations.Add(relation)

    '        XmlDataSourceGrade.Data = ds.GetXml()
    '        tvGrade.DataBind()
    '    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Page.IsPostBack = False Then
            If Not tvGrade Is Nothing Then
                DisableAutoPostBack(tvGrade.Nodes)
            End If
        End If


    End Sub
    Sub DisableAutoPostBack(ByVal tnc As TreeNodeCollection)
        ' Loop over every node in the passed collection 
        For Each tn As TreeNode In tnc
            ' Set the node's NavigateUrl (which equates to A HREF) to javascript:void(0);, 
            ' effectively intercepting the click event and disabling PostBack. 
            tn.NavigateUrl = "javascript:void(0);"

            ' Set the node's SelectAction to Select in order to enable client-side 
            ' expansion/collapsing of parent nodes. Caution: SelectExpand causes 
            ' expand/collapse icon to disappear! 
            tn.SelectAction = TreeNodeSelectAction.[Select]

            ' If this node has children, recurse over them as well before returning 
            If tn.ChildNodes.Count > 0 Then

                DisableAutoPostBack(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub ClearAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = False

            If tn.ChildNodes.Count > 0 Then
                ClearAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub SelectAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = True

            If tn.ChildNodes.Count > 0 Then
                SelectAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub
    'Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    ClearAllCheck(tvGrade.Nodes)
    '    txtGradeSection.Text = ""
    '    'Session("ListOfSection").clear()
    'End Sub
    Protected Sub okButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        populateGrades()
    End Sub
    Public Sub populateGrades()

        Dim ListOfSection As New ArrayList
        ' Session("ListOfSection").clear()
        ' Session("ListOfSection") = ListOfSection

        Dim strAddSection As String = String.Empty
        Dim parentId As String = String.Empty
        Dim tempID As String = String.Empty
        Dim childCount As Integer
        Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
        Dim tempCount As Integer
        Dim temp As String = String.Empty
        If tvGrade.CheckedNodes.Count > 0 Then

            'For i As Integer = 0 To tvGrade.CheckedNodes.Count

            'Next

            For Each node As TreeNode In tvGrade.CheckedNodes
                If Not node Is Nothing Then
                    If node.Text <> "Select All" Then
                        If node.Parent.Text <> "Select All" Then

                            Dim popAttGrade As New Attendance(node.Parent.Text, node.Parent.Value, node.Text, node.Value)
                            childCount = node.Parent.ChildNodes.Count
                            ListOfSection.Add(popAttGrade)
                            If tempID = node.Parent.Text Then
                                strAddSection = strAddSection + node.Text + ","
                                ' tempCount = tempCount + 1
                            Else
                                strAddSection = strAddSection + "}" + node.Parent.Text + "{" + node.Text + ","
                                tempID = node.Parent.Text
                                'tempCount = 1
                            End If
                        End If
                    End If

                End If
            Next
            strAddSection = strAddSection.Replace(",}", "},").TrimStart("}") '.Remove(0, 1)
            txtGradeSection.Text = strAddSection.TrimEnd(",") + "}"
            'temp = ListOfSection.
            'For i As Integer = 0 To ListOfSection.Count - 1
            '    Console.WriteLine("Element {0} is ""{1}""", i, ListOfSection(i))
            'Next

            If ViewState("nodeCounts") = ListOfSection.Count Then
                ViewState("AllGrades") = 1
            Else
                ViewState("AllGrades") = 0
            End If
        Else
            txtGradeSection.Text = ""
        End If

    End Sub
    Sub CountSectionGrade() 'to monitor the add grade and the total grades are equal or not

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim ACD_ID As String = String.Empty
        If ddlAcdYear.SelectedIndex = -1 Then
            ACD_ID = ""
        Else
            ACD_ID = ddlAcdYear.SelectedItem.Value
        End If

        str_Sql = " SELECT  count(distinct SECTION_M.SCT_ID) as SCT_ID FROM GRADE_BSU_M INNER JOIN  SECTION_M ON " & _
" GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID AND  GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN  GRADE_M " & _
" ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID  where  GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' "
        'to check all the section from the grade is selected or not
        ViewState("nodeCounts") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

    End Sub


    Sub EditRecordBind(ByVal SCH_ID As String)
        Dim tempStatus As Boolean
        Using RecordBindreader As SqlDataReader = AccessStudentClass.GetSCHOOL_HOLIDAY_M(SCH_ID)

            While RecordBindreader.Read
                If RecordBindreader.HasRows Then
                    ddlAcdYear.ClearSelection()
                    ddlAcdYear.Items.FindByValue(Convert.ToString(RecordBindreader("SCH_ACD_ID"))).Selected = True
                    If IsDate(RecordBindreader("SCH_DTFROM")) = True Then
                        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((RecordBindreader("SCH_DTFROM"))))
                        calDatepicker.TodaysDate = Convert.ToDateTime((RecordBindreader("SCH_DTFROM")))
                        ddlyear.ClearSelection()
                        ddlyear.Items.FindByValue(Year(Convert.ToDateTime((RecordBindreader("SCH_DTFROM"))))).Selected = True
                        ddlMonth.ClearSelection()
                        Dim MonthDis As String = StrConv(MonthName(Month(Convert.ToDateTime(RecordBindreader("SCH_DTFROM")))), VbStrConv.ProperCase)
                        ddlMonth.Items.FindByValue(MonthDis).Selected = True
                    End If
                    If IsDate(RecordBindreader("SCH_DTTO")) = True Then
                        txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((RecordBindreader("SCH_DTTO"))))
                    End If

                    txtRemarks.Text = Convert.ToString(RecordBindreader("SCH_REMARKS"))
                    ddlAttReq.ClearSelection()
                    ddlAttReq.Items.FindByValue(Convert.ToString(RecordBindreader("SCH_TYPE")).Trim).Selected = True

                    tempStatus = Convert.ToBoolean(RecordBindreader("SCH_bGRADEALL"))
                    If Convert.ToString(RecordBindreader("SCH_WEEKEND1_WORK")) = ViewState("BSU_WEEKEND1") Then
                        chkWeekend1.Checked = True
                    Else
                        chkWeekend1.Checked = False
                    End If
                    If Convert.ToBoolean(RecordBindreader("SCH_bWEEKEND1_LOG_BOOK")) = True Then
                        chkLogBook1.Checked = True
                    Else
                        chkLogBook1.Checked = False
                    End If

                    If Convert.ToString(RecordBindreader("SCH_WEEKEND2_WORK")) = ViewState("BSU_WEEKEND2") Then
                        chkWeekend2.Checked = True
                    Else
                        chkWeekend2.Checked = False
                    End If
                    If Convert.ToBoolean(RecordBindreader("SCH_bWEEKEND2_LOG_BOOK")) = True Then
                        chkLogBook2.Checked = True
                    Else
                        chkLogBook2.Checked = False
                    End If
                End If
            End While
        End Using

        If tempStatus = True Then
            Call SelectAllCheck(tvGrade.Nodes)
            Call populateGrades_onLoad()
        Else
            Using GradeSectionreader As SqlDataReader = AccessStudentClass.GetSCHOOL_HOLIDAY_GRADE(SCH_ID)
                Dim htTab As New Hashtable

                While GradeSectionreader.Read
                    If GradeSectionreader.HasRows Then
                        Call checkme(tvGrade.Nodes, Convert.ToString(GradeSectionreader("SHG_SCT_ID")), htTab)
                    End If
                End While
                Dim chkdNodes As TreeNode
                Dim ienum As IDictionaryEnumerator = htTab.GetEnumerator
                While (ienum.MoveNext())
                    chkdNodes = ienum.Key
                    If chkdNodes.ChildNodes.Count = ienum.Value Then
                        chkdNodes.Checked = True
                    End If
                End While
            End Using
            Call populateGrades_onLoad()
        End If

    End Sub
    Public Sub checkme(ByVal nodeList As TreeNodeCollection, ByVal menuCode As String, ByRef htTab As Hashtable)

        If Not nodeList Is Nothing Then
            For Each nodec As TreeNode In nodeList
                '  If Not nodec.Parent Is Nothing Then
                Dim strNodeValue As String = nodec.Value
                '   For Each node As TreeNode In nodec.Parent.ChildNodes
                If strNodeValue = menuCode Then
                    'tvmenu.SelectedNode.Checked
                    ' Label2.Text += nodec.Value & " "
                    If Not nodec.Parent Is Nothing Then
                        If Not htTab.Contains(nodec.Parent) Then
                            htTab(nodec.Parent) = 1
                        Else
                            htTab(nodec.Parent) = htTab(nodec.Parent) + 1
                        End If
                    End If
                    nodec.Checked = True
                End If
                ' Next
                '  End If
                checkme(nodec.ChildNodes, menuCode, htTab)
            Next
        End If
    End Sub
    Public Sub populateGrades_onLoad()

        Dim ListOfSection As New ArrayList
        ' Session("ListOfSection").clear()
        ' Session("ListOfSection") = ListOfSection

        Dim strAddSection As String = String.Empty
        Dim parentId As String = String.Empty
        Dim tempID As String = String.Empty
        Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes


        If tvGrade.CheckedNodes.Count > 0 Then
            For Each node As TreeNode In tvGrade.CheckedNodes

                If Not node Is Nothing Then
                    If node.Text <> "Select All" Then
                        If node.Parent.Text <> "Select All" Then
                            Dim popAttGrade As New Attendance(node.Parent.Text, node.Parent.Value, node.Text, node.Value)
                            ListOfSection.Add(popAttGrade)
                            If tempID = node.Parent.Text Then
                                strAddSection = strAddSection + node.Text + ","
                            Else
                                strAddSection = strAddSection + "}" + node.Parent.Text + "{" + node.Text + ","
                                tempID = node.Parent.Text
                            End If
                        End If
                    End If

                End If
            Next
            strAddSection = strAddSection.Replace(",}", "},").TrimStart("}") '.Remove(0, 1)
            txtGradeSection.Text = strAddSection.TrimEnd(",") + "}"





            If ViewState("nodeCounts") = ListOfSection.Count Then
                ViewState("AllGrades") = 1
            Else
                ViewState("AllGrades") = 0
            End If
        Else
            ViewState("AllGrades") = 0
        End If

    End Sub

    'Private Function FIND_DAY(ByVal P_DATE As Date) As String
    '    Dim str_events As String = ""
    '    For i As Integer = 0 To dsEvents.Tables(0).Rows.Count - 1
    '        If P_DATE >= dsEvents.Tables(0).Rows(i)("SCH_DTFROM") And P_DATE <= dsEvents.Tables(0).Rows(i)("SCH_DTTO") Then
    '            Dim str1 As String = "<ul type='square' class='multiArrow'>"
    '            If str_events = "" Then
    '                str_events = "<ul type='square' class='multiArrow'><li><font size='-1px'>" & Left(dsEvents.Tables(0).Rows(i)("SCH_REMARKS"), 7) & " ..." & "</font></li>"
    '            Else
    '                str_events = str_events & "<li><font size='-1px'>" & Left(dsEvents.Tables(0).Rows(i)("SCH_REMARKS"), 7) & " ..." & "</font></li>"
    '            End If
    '        End If
    '    Next
    '    Return str_events
    'End Function

    Sub knap()
        calDatepicker.DataBind()

    End Sub
    Protected Sub calDatepicker_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles calDatepicker.DayRender
        Call bindevents()

        Dim day As CalendarDay = DirectCast(e.Day, CalendarDay)
        Dim cell As TableCell = DirectCast(e.Cell, TableCell)
        Dim str_color As String = ""
        'Dim link As String = "<a href='studShowHolidayDetail.aspx?SCH_ID="
        'If Not day.IsOtherMonth Then
        Dim dayStr As String = String.Empty 'FIND_DAY(day.Date)

        For i As Integer = 0 To Session("dsEvents").Tables(0).Rows.Count - 1
            If day.Date >= Session("dsEvents").Tables(0).Rows(i)("SCH_DTFROM") And day.Date <= Session("dsEvents").Tables(0).Rows(i)("SCH_DTTO") Then
                'Dim str1 As String = "<ul type='square' class='multiArrow'>"
                'Dim lb As New LinkButton
                'Dim tempStr As String = String.Empty
                'Dim link As New HyperLink()
                'link.Text = "<ul type='square' class='multiArrow'><li><font size='-1px'> " + Left(Session("dsEvents").Tables(0).Rows(i)("SCH_REMARKS"), 7).ToString().Trim() + "..." & "</font></li>"
                'link.NavigateUrl = "studShowHolidayDetail.aspx?SCH_ID=" + Session("dsEvents").Tables(0).Rows(i)("SCH_ID").ToString

                Dim btnLink As New LinkButton()



                If StrConv(Session("dsEvents").Tables(0).Rows(i)("SCH_TYPE"), VbStrConv.ProperCase).Trim = "All" Then
                    btnLink.Text = "<ul type='square' class='gradeAll'><li><font size='-1px'> " + StrConv(Left(Session("dsEvents").Tables(0).Rows(i)("SCH_REMARKS"), 50).ToString().Trim(), VbStrConv.ProperCase) + "" & "</font></li>"
                ElseIf StrConv(Session("dsEvents").Tables(0).Rows(i)("SCH_TYPE"), VbStrConv.ProperCase).Trim = "None" Then
                    btnLink.Text = "<ul type='square' class='gradeNone'><li><font size='-1px'> " + StrConv(Left(Session("dsEvents").Tables(0).Rows(i)("SCH_REMARKS"), 50).ToString().Trim(), VbStrConv.ProperCase) + "" & "</font></li>"
                ElseIf StrConv(Session("dsEvents").Tables(0).Rows(i)("SCH_TYPE"), VbStrConv.ProperCase).Trim = "Male" Then
                    btnLink.Text = "<ul type='square' class='gradeMale'><li><font size='-1px'> " + StrConv(Left(Session("dsEvents").Tables(0).Rows(i)("SCH_REMARKS"), 50).ToString().Trim(), VbStrConv.ProperCase) + "" & "</font></li>"
                ElseIf StrConv(Session("dsEvents").Tables(0).Rows(i)("SCH_TYPE"), VbStrConv.ProperCase).Trim = "Female" Then

                    btnLink.Text = "<ul type='square' class='gradeFemale'><li><font size='-1px'> " + StrConv(Left(Session("dsEvents").Tables(0).Rows(i)("SCH_REMARKS"), 50).ToString().Trim(), VbStrConv.ProperCase) + "" & "</font></li>"
                End If
                Dim url As String
                Dim SID As String = Encr_decrData.Encrypt(Session("dsEvents").Tables(0).Rows(i)("SCH_ID").ToString)
                url = String.Format("studShowHolidayDetail.aspx?ID_code={0}", SID)
                btnLink.OnClientClick = "javascript:ShowHDetails('" & url & "');return false;"   ' "alert('" & Session("dsEvents").Tables(0).Rows(i)("SCH_ID").ToString & "');"
                e.Cell.Controls.Add(btnLink)

            End If
        Next


        Select Case day.Date.DayOfWeek
            Case DayOfWeek.Sunday
                If ViewState("BSU_WEEKEND1").ToString.Trim = "SUNDAY" Or ViewState("BSU_WEEKEND2").ToString.Trim = "SUNDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                    dayStr = ""
                Else
                    If dayStr <> "" Then
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                        cell.Controls.Add(New LiteralControl("<BR>" + dayStr))
                        dayStr = ""
                    End If
                End If

            Case DayOfWeek.Monday
                If ViewState("BSU_WEEKEND1").ToString.Trim = "MONDAY" Or ViewState("BSU_WEEKEND2").ToString.Trim = "MONDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                    dayStr = ""
                Else
                    If dayStr <> "" Then
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                        cell.Controls.Add(New LiteralControl("<BR>" + dayStr))
                        dayStr = ""
                    End If
                End If

            Case DayOfWeek.Tuesday
                If ViewState("BSU_WEEKEND1").ToString.Trim = "TUESDAY" Or ViewState("BSU_WEEKEND2").ToString.Trim = "TUESDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                    dayStr = ""
                Else
                    If dayStr <> "" Then
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                        cell.Controls.Add(New LiteralControl("<BR>" + dayStr))
                        dayStr = ""
                    End If
                End If

            Case DayOfWeek.Wednesday
                If ViewState("BSU_WEEKEND1").ToString.Trim = "WEDNESDAY" Or ViewState("BSU_WEEKEND2").ToString.Trim = "WEDNESDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                    dayStr = ""
                Else
                    If dayStr <> "" Then
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                        cell.Controls.Add(New LiteralControl("<BR>" + dayStr))
                        dayStr = ""
                    End If
                End If

            Case DayOfWeek.Thursday
                If ViewState("BSU_WEEKEND1").ToString.Trim = "THURSDAY" Or ViewState("BSU_WEEKEND2").ToString.Trim = "THURSDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                    dayStr = ""
                Else
                    If dayStr <> "" Then
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                        cell.Controls.Add(New LiteralControl("<BR>" + dayStr))
                        dayStr = ""
                    End If
                End If

            Case DayOfWeek.Friday
                If ViewState("BSU_WEEKEND1").ToString.Trim = "FRIDAY" Or ViewState("BSU_WEEKEND2").ToString.Trim = "FRIDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                    dayStr = ""
                Else
                    If dayStr <> "" Then
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                        cell.Controls.Add(New LiteralControl("<BR>" + dayStr))
                        dayStr = ""
                    End If
                End If
            Case DayOfWeek.Saturday
                If ViewState("BSU_WEEKEND1").ToString.Trim = "SATURDAY" Or ViewState("BSU_WEEKEND2").ToString.Trim = "SATURDAY" Then
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                    dayStr = ""
                Else
                    If dayStr <> "" Then
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#eed549")
                        cell.Controls.Add(New LiteralControl("<BR>" + dayStr))
                        dayStr = ""
                    End If
                End If

        End Select

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Call SetState()
            Call clearContent()
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call ResetState()
        clearContent()
        Call CountSectionGrade()
        ViewState("datamode") = "add"

        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Call ResetState()
        ViewState("datamode") = "edit"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If ViewState("viewid") = 0 Then
            lblError.Text = "No records to delete"
            Exit Sub
        End If
        Dim str_err As String = String.Empty
        Dim DeleteMessage As String = String.Empty
        str_err = callDelete(DeleteMessage)
        If str_err = "0" Then
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            lblError.Text = "Record Deleted Successfully"

        Else
            lblError.Text = DeleteMessage
        End If
    End Sub

    Function callDelete(ByRef DeleteMessage As String) As Integer
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'delete needs to be modified based on the trigger

                Status = AccessStudentClass.DeleteSCHOOL_HOLIDAY_M(ViewState("viewid"), transaction)


                If Status = -1 Then
                    callDelete = "1"
                    DeleteMessage = "Record Does Not Exist "
                    Return "1"

                ElseIf Status <> 0 Then
                    callDelete = "1"
                    DeleteMessage = "Error Occured While Deleting."
                    Return "1"
                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        callDelete = "1"
                        DeleteMessage = "Could not complete your request"
                        Return "1"

                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("viewid") = 0
                    Call SetState()
                    Call clearContent()
                End If
            Catch ex As Exception
                callDelete = "1"
                DeleteMessage = "Error Occured While Saving."
            Finally
                If callDelete <> "0" Then
                    UtilityObj.Errorlog(DeleteMessage)
                    transaction.Rollback()
                Else
                    DeleteMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function


    'Private Sub CheckChildNode(ByVal currNode As TreeNode)
    '    'set the children check status to the same as the current node
    '    Dim checkStatus As Boolean = currNode.Checked
    '    For Each node As TreeNode In currNode.Nodes
    '        node.Checked = checkStatus
    '        CheckChildNode(node)
    '    Next
    'End Sub

    'Private Sub CheckParentNode(ByVal currNode As TreeNode)
    '    Dim parentNode As TreeNode = currNode.Parent
    '    If parentNode Is Nothing Then Exit Sub
    '    parentNode.Checked = True
    '    For Each node As TreeNode In parentNode.Nodes
    '        If Not node.Checked Then
    '            parentNode.Checked = False
    '            Exit For
    '        End If
    '    Next
    '    CheckParentNode(parentNode)
    'End Sub

    'Private Sub treeview_AfterCheck(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles treeview.AfterCheck
    '    RemoveHandler treeview.AfterCheck, AddressOf treeview_AfterCheck
    '    CheckChildNode(e.Node)
    '    CheckParentNode(e.Node)
    '    AddHandler treeview.AfterCheck, AddressOf treeview_AfterCheck
    'End Sub

  
End Class
