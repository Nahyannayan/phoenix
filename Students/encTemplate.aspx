﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="encTemplate.aspx.vb" Inherits="Students_encTemplate" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Email Template
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="center">&nbsp;
                              <asp:Label ID="lblerror" runat="server" EnableViewState="False" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>

                     <tr id="Tr1" align="left" runat="server">
                        <td width="15%"><span class="field-label">Business Unit</span> 
                        </td>

                        <td >
                            <asp:DropDownList ID="ddlBSU" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td width="15%" ><span class="field-label">Template Name</span> 
                        </td>

                        <td >
                            <asp:TextBox ID="txtTemplate" runat="server" ></asp:TextBox>
                        </td>
                    </tr>
                                                             

                    <tr id="Tr2" align="left" runat="server">
                        <td  width="15%"><span class="field-label">Template</span> 
                        </td>

                        <td >
                            <telerik:RadEditor ID="txtText" runat="server" EditModes="All" >
                                <Tools>
                                    <telerik:EditorToolGroup>
                                        <telerik:EditorTool Name="Bold" />
                                        <telerik:EditorTool Name="Italic" />
                                        <telerik:EditorTool Name="InsertOrderedList" />
                                        <telerik:EditorTool Name="InsertUnorderedList" />
                                    </telerik:EditorToolGroup>
                                </Tools>
                            </telerik:RadEditor>
                        </td>

                          <td width="15%"><span class="field-label">Merge Type</span> 
                        </td>

                        <td  align="left">
                            <asp:DropDownList ID="ddlMergeType" runat="server" AutoPostBack="true" ></asp:DropDownList></td>
                    </tr>
                   
                    <tr>
                        <td width="15%"><span class="field-label">Merge Field</span> 
                        </td>

                        <td  align="left">
                            <asp:DropDownList ID="ddlMergeField" runat="server" AutoPostBack="true" ></asp:DropDownList>
                            <asp:Button ID="btnInsert" runat="server" CssClass="button" Text="Insert" />
                        </td>
                        <td>

                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Save" />

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

