<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="GenQueries_ASA.aspx.vb" Inherits="GenQueries_ASA" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Customized Queries"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr  >
                        <td  >
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr  >
                        <td  >
                            <table width="100%">

                                <tr runat="server" id="tr1">
                                    <td align="left" class="matters" width="20%"><span class="field-label">Select Query</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlQuery" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                     <td width="20%"></td>
                                     <td width="30%"></td>
                                </tr>



                                <tr runat="server" id="tr_Acad">
                                    <td align="left" class="matters"><span class="field-label">Academic Year</span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlACD_ID" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                     <td></td>
                                     <td></td>

                                </tr>


                                <tr runat="server" id="tr_Acad_List">
                                    <td align="left" class="matters"><span class="field-label">Academic Year</span></td>
                                    <td align="left" class="matters">
                                         <div class="checkbox-list">
                                        <asp:CheckBoxList ID="lstACAD" runat="server" Style="vertical-align: middle; overflow: auto; text-align: left; border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;" BorderStyle="Solid" BorderWidth="1px" Height="149px" Width="165px" RepeatLayout="Flow">
                                        </asp:CheckBoxList>
                                             </div>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>




                                <tr runat="server" id="tr_Grade">
                                    <td align="left" class="matters"><span class="field-label">Grade</span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlGRD_ID" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>


                                <tr runat="server" id="tr_Grade_List">
                                    <td align="left" class="matters"><span class="field-label">Grade</span></td>
                                    <td align="left" class="matters">
                                        <div class="checkbox-list">

                                        <asp:CheckBoxList ID="lstGrades" runat="server" Style="vertical-align: middle; overflow: auto; text-align: left; border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;" BorderStyle="Solid" BorderWidth="1px" Height="149px" Width="165px" RepeatLayout="Flow">
                                        </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>




                                <tr runat="server" id="tr_AsOnDate">
                                    <td align="left" class="matters"><span class="field-label">As on/From Date</span></td>
                                    <td align="left" class="matters"  style="text-align: left;">
                                        <asp:TextBox ID="txtAsOnDate" runat="server" Width="123px"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />

                                        <br />
                                        <span  >(dd/mmm/yyyy)</span></td>
                                    <td></td>
                                    <td></td>
                                </tr>



                                <tr runat="server" id="tr_ToDate">
                                    <td align="left" class="matters"><span class="field-label">To Date</span></td>
                                    <td align="left" class="matters"  style="text-align: left;">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="123px"></asp:TextBox> 
                                        <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />

                                        <br />
                                        <span >(dd/mmm/yyyy)</span></td>
                                    <td></td>
                                    <td></td>
                                </tr>





                                <tr>
                                    <td align="left" class="matters" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Export to Excel"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                    TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgToDate"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>


            </div>
        </div>
    </div>

</asp:Content>

