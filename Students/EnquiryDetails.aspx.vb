﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Collections
Imports UtilityObj
Imports UserControls_usrMessageBar
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports NReco.PdfGenerator
Imports System.Net.Mail
Imports System.Reflection


Partial Class Students_NewASPX_EnquiryDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Popup_Message As String = ""
    Public SchoolHead As String = ""
    Public Allow_Permissions As Boolean = False
    Public Popup_Message_Status As WarningType
    Dim studClass As New studClass
#Region "Page Details Data"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnOfferLetterToPDF)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DocumentSave)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnRegisterSlipToPDF)
        If Not IsPostBack Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnOfferLetterToPDF)
            If Not Session("sbsuid") Is Nothing Then
                hfENQ_ID.Value = Encr_decrData.Decrypt(Request.QueryString("enqid").Replace(" ", "+"))

                HiddenBsuId.Value = Session("sbsuid")
                HiddenAcademicyear.Value = Encr_decrData.Decrypt(Request.QueryString("accyear").Replace(" ", "+"))
                BindBsuDetails()
                PopulateEnquiryData()
                PopulatePrevData()
            Else
                Response.Redirect("/login.aspx")
            End If

        End If
        Dim EQS_STATUS As String = Encr_decrData.Decrypt(HFSTATUS.Value)
        If EQS_STATUS = "ENR" Then
            Showand_hide_BtnSTATUS(EQS_STATUS)

        End If
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "progress_bar_active('" + EQS_STATUS + "');", True)
    End Sub
    Public Sub BindBsuDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "select BSU_NAME,BSU_ADDRESS,BSU_POBOX,BSU_CITY,BSU_TEL,BSU_FAX,BSU_EMAIL,BSU_MOE_LOGO,BSU_URL from BUSINESSUNIT_M where BSU_ID='" & HiddenBsuId.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        BsuName.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_NAME").ToString()
        'bsuAddress.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_ADDRESS").ToString()
        'bsupostbox.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_POBOX").ToString()
        'bsucity.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_CITY").ToString()
        'bsutelephone.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_TEL").ToString()
        'bsufax.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_FAX").ToString()
        'bsuemail.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_EMAIL").ToString()
        'bsuwebsite.InnerHtml = ds.Tables(0).Rows(0).Item("BSU_URL").ToString()
        'imglogo.ImageUrl = ds.Tables(0).Rows(0).Item("BSU_MOE_LOGO").ToString()
    End Sub
    Private Sub PopulateEnquiryData()

        'Try

        bind_qus()

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "exec GETENQUIRY_Details '" + hfENQ_ID.Value + "','" + Session("sbsuid") + "'"
        Dim reader As SqlDataReader

        Dim primarycontact As String = ""
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem

        While reader.Read

            '''''Main

            ltEnqDate.Text = DateFormat(Convert.ToDateTime(reader("EQM_ENQDATE")))

            If Not IsDBNull(reader("EQS_REGNDATE")) Then
                ltRegDate.Text = DateFormat(Convert.ToDateTime(reader("EQS_REGNDATE")))

            End If

            ltStuId.Text = Convert.ToString(reader("EQS_STU_NO"))

            ltName.Text = Convert.ToString(reader("APPLNAME")).Trim.Replace("''", "'")
            ltDob.Text = DateFormat(Convert.ToDateTime(reader("EQM_APPLDOB")))
            If Convert.ToString(reader("EQM_APPLGENDER")).Trim = "M" Then
                ltGender.Text = "Male"
            Else
                ltGender.Text = "Female"
            End If

            primarycontact = Convert.ToString(reader("EQM_PRIMARYCONTACT"))
            If primarycontact = "F" Then
                ltPri_cont.Text = "Father"
            ElseIf primarycontact = "M" Then
                ltPri_cont.Text = "Mother"
            Else
                ltPri_cont.Text = "Guardian"
            End If
            ltReg.Text = Convert.ToString(reader("RLG_DESCR"))
            ltPOB.Text = Convert.ToString(reader("EQM_APPLPOB"))
            ltCOB.Text = Convert.ToString(reader("CTY_DESCR"))
            ltNat.Text = Convert.ToString(reader("Nat"))

            ltFpreferedcontact.Text = Convert.ToString(reader("EQM_PREFCONTACT"))
            ltEmgNo.Text = Convert.ToString(reader("EQM_EMGCONTACT"))
            ltEmployer_ShortEnq.Text = Convert.ToString(reader("EMPLOYER_SHORTENQ"))
            ltPassNo.Text = Convert.ToString(reader("APPLPASPRTNO"))
            ltIssPlace.Text = Convert.ToString(reader("APPLPASPRTISSPLACE"))
            ltIssDate.Text = DateFormat(Convert.ToDateTime(reader("APPLPASPRTISSDATE")))
            ltExpDate.Text = DateFormat(Convert.ToDateTime(reader("APPLPASPRTEXPDATE")))
            ltVisaNo.Text = Convert.ToString(reader("APPLVISANO"))
            ltIssPlace_V.Text = Convert.ToString(reader("APPLVISAISSPLACE"))
            ltIssDate_V.Text = DateFormat(Convert.ToDateTime(reader("APPLVISAISSDATE")))
            ltExpr_Date_V.Text = DateFormat(Convert.ToDateTime(reader("APPLVISAEXPDATE")))
            ltIssAuth.Text = Convert.ToString(reader("APPLVISAISSAUTH"))

            ltFirstLang.Text = Convert.ToString(reader("EQM_FIRSTLANG"))
            ltOthLang.Text = Convert.ToString(reader("EQM_OTHLANG"))




            Dim PREVSCHOOL_NURSERY As String = Convert.ToString(reader("PREVSCHOOL_NURSERY"))
            Dim FlagCurrGEMS_Sch As Boolean = Convert.ToBoolean(reader("EQM_bPREVSCHOOLGEMS"))

            If Trim(PREVSCHOOL_NURSERY) = "" Then


                trCurrSch_1.Visible = True
                trCurrSch_2.Visible = True
                trCurrSch_3.Visible = True
                trCurrSch_4.Visible = True
                'trCurrSch_5.Visible = True
                'trCurrSch_6.Visible = True
                trPrevSCH_id.Visible = True


                ltCurr_School.Text = Convert.ToString(reader("EQM_PREVSCHOOL"))
                ltCurr_City.Text = Convert.ToString(reader("EQM_PREVSCHOOL_CITY"))
                ltCurr_Country.Text = Convert.ToString(reader("PCTY_DESCR"))
                ltCurr_Grade.Text = Convert.ToString(reader("PREVSCHOOL_GRD_ID"))
                ltCurr_Instr.Text = Convert.ToString(reader("EQM_PREVSCHOOL_MEDIUM"))
                ltCurr_FromDT.Text = DateFormat(Convert.ToDateTime(reader("PREVSCHOOL_LASTATTDATE")))
                ltCurr_Curr.Text = Convert.ToString(reader("PREVSCHOOL_CLM_DESCR"))
                ltCurr_Head.Text = Convert.ToString(reader("EQM_PREVSCHOOL_HEAD_NAME"))
                ltCurr_Stu_id.Text = Convert.ToString(reader("PREVSCHOOL_REG_ID"))
                ltCurr_Addr.Text = Convert.ToString(reader("EQM_PREVSCHOOL_ADDRESS"))
                ltCurr_City.Text = Convert.ToString(reader("EQM_PREVSCHOOL_CITY"))
                ltCurr_Country.Text = Convert.ToString(reader("PCTY_DESCR"))
                ltCurr_Sch_ph.Text = Convert.ToString(reader("EQM_PREVSCHOOL_PHONE"))
                ltCurr_Sch_fax.Text = Convert.ToString(reader("EQM_PREVSCHOOL_FAX"))
                ltCurr_ToDT.Text = DateFormat(Convert.ToDateTime(reader("EQM_PREVSCHOOL_FROMDT")))

            Else

                ltCurr_School.Text = StrConv(PREVSCHOOL_NURSERY, VbStrConv.ProperCase)
                trCurrSch_1.Visible = False
                trCurrSch_2.Visible = False
                trCurrSch_3.Visible = False
                trCurrSch_4.Visible = False
                'trCurrSch_5.Visible = False
                'trCurrSch_6.Visible = False
                trPrevSCH_id.Visible = False
            End If
            trF_GEMS.Visible = False
            trM_GEMS.Visible = False
            ltM_GEMS.Text = "No"
            ltF_GEMS.Text = "No"




            If Convert.ToString(reader("XBSU_FNAME")).Trim = "" Then
                ltF_ExGEms.Text = "No"
                ltFStaff_ACD.Text = ""
                ltFStaff_GEMS_School.Text = ""
                trF_ExGEMS.Visible = False
            Else
                trF_ExGEMS.Visible = True
                ltF_ExGEms.Text = "Yes"
                ltFStaff_ACD.Text = Convert.ToString(reader("F_ACD_YEAR"))
                ltFStaff_GEMS_School.Text = Convert.ToString(reader("XBSU_FNAME"))
            End If

            'If primarycontact = "F" Then
            '    If Convert.ToBoolean(reader("EQM_bSTAFFGEMS")) = True Then
            '        trF_GEMS.Visible = True

            '        ltF_GEMS.Text = "Yes"
            '        ltFStaffID.Text = Convert.ToString(reader("EMP_NO"))
            '        ltFStaff_BSU.Text = Convert.ToString(reader("STBSU_NAME"))
            '    End If
            'ElseIf primarycontact = "M" Then
            '    If Convert.ToBoolean(reader("EQM_bSTAFFGEMS")) = True Then
            '        trM_GEMS.Visible = True

            '        ltM_GEMS.Text = "Yes"
            '        ltMStaffID.Text = Convert.ToString(reader("EMP_NO"))
            '        ltMStaff_BSU.Text = Convert.ToString(reader("STBSU_NAME"))
            '    End If
            'End If


            If Convert.ToBoolean(reader("EQP_bFGEMSSTAFF")) = True Then
                trF_GEMS.Visible = True
                ltF_GEMS.Text = "Yes"
                ltFStaffID.Text = Convert.ToString(reader("F_EMP_NO"))
                ltFStaff_BSU.Text = Convert.ToString(reader("F_BSU_NAME"))
            Else

                ltF_GEMS.Text = "No"


            End If


            If Convert.ToBoolean(reader("EQP_bMGEMSSTAFF")) = True Then
                trM_GEMS.Visible = True
                ltM_GEMS.Text = "Yes"
                ltMStaffID.Text = Convert.ToString(reader("M_EMP_NO"))
                ltMStaff_BSU.Text = Convert.ToString(reader("M_BSU_NAME"))
            Else

                ltM_GEMS.Text = "No"


            End If


            If Convert.ToString(reader("XBSU_MNAME")).Trim = "" Then
                ltM_ExGEms.Text = "No"
                ltMStaff_ACD.Text = ""
                ltMStaff_GEMS_School.Text = ""
                trM_ExGEMS.Visible = False
            Else
                ltM_ExGEms.Text = "Yes"
                ltMStaff_ACD.Text = Convert.ToString(reader("M_ACD_YEAR"))
                ltMStaff_GEMS_School.Text = Convert.ToString(reader("XBSU_MNAME"))
                trM_ExGEMS.Visible = True
            End If






            If Trim(StrConv(Convert.ToString(reader("EQM_FILLED_BY")), VbStrConv.Uppercase)) = "P" Then
                ltFilled_by.Text = "Parent"
            Else
                ltFilled_by.Text = "Relocation Agent"


            End If

            'fathers details------------------------------

            ltFather_name.Text = Convert.ToString(reader("EQP_FATHERNAME")).Trim.Replace("''", "'")

            ltFnationality.Text = Convert.ToString(reader("F_Nationality1"))
            ltFNat2.Text = Convert.ToString(reader("F_Nationality2"))
            ltFpobox.Text = Convert.ToString(reader("EQP_FCOMPOBOX"))
            ltFofficetelephone.Text = Convert.ToString(reader("EQP_FOFFPHONE"))
            ltFhometelphone.Text = Convert.ToString(reader("EQP_FRESPHONE"))
            ltFfax.Text = Convert.ToString(reader("EQP_FFAX"))
            ltFmobile.Text = Convert.ToString(reader("EQP_FMOBILE"))
            ltFemail.Text = Convert.ToString(reader("EQP_FEMAIL"))


            ltFAdd1.Text = Convert.ToString(reader("EQP_FPRMADDR1"))
            ltFAdd2.Text = Convert.ToString(reader("EQP_FPRMADDR2"))


            ltFAddCity.Text = Convert.ToString(reader("EQP_FPRMCITY"))
            ltFAdd_Country.Text = Convert.ToString(reader("F_CTY_DESCRPER"))
            ltFAddPhone.Text = Convert.ToString(reader("EQP_FPRMPHONE"))

            ltFAdd_Zip.Text = Convert.ToString(reader("F_pobox"))

            ltFApart.Text = Convert.ToString(reader("EQP_FCOMAPARTNO"))
            ltFBuild.Text = Convert.ToString(reader("EQP_FCOMBLDG"))
            ltFStreet.Text = Convert.ToString(reader("EQP_FCOMSTREET"))
            ltFArea.Text = Convert.ToString(reader("EQP_FCOMAREA"))
            ltFemirate.Text = Convert.ToString(reader("F_EMR_DESCR"))

            ltFOcc.Text = Convert.ToString(reader("EQP_FOCC"))
            ltFComp.Text = Convert.ToString(reader("EQP_FCOMPANY"))
            ltFCurr_Contry.Text = Convert.ToString(reader("F_CTY_DESCRCURR"))

            'fathers details ends------------------------------

            'Mothers details STARTs------------------------------

            ltMother_name.Text = Convert.ToString(reader("EQP_MOTHERNAME")).Trim.Replace("''", "'")

            ltMnationality.Text = Convert.ToString(reader("M_Nationality1"))
            ltMNat2.Text = Convert.ToString(reader("M_Nationality2"))
            ltMpobox.Text = Convert.ToString(reader("EQP_MCOMPOBOX"))
            ltMofficetelephone.Text = Convert.ToString(reader("EQP_MOFFPHONE"))
            ltMhometelphone.Text = Convert.ToString(reader("EQP_MRESPHONE"))
            ltMfax.Text = Convert.ToString(reader("EQP_MFAX"))
            ltMmobile.Text = Convert.ToString(reader("EQP_MMOBILE"))
            ltMemail.Text = Convert.ToString(reader("EQP_MEMAIL"))


            ltMAdd1.Text = Convert.ToString(reader("EQP_MPRMADDR1"))
            ltMAdd2.Text = Convert.ToString(reader("EQP_MPRMADDR2"))


            ltMAddCity.Text = Convert.ToString(reader("EQP_MPRMCITY"))
            ltMAdd_Country.Text = Convert.ToString(reader("M_CTY_DESCRPER"))
            ltMAddPhone.Text = Convert.ToString(reader("EQP_MPRMPHONE"))

            ltMAdd_Zip.Text = Convert.ToString(reader("M_pobox"))

            ltMApart.Text = Convert.ToString(reader("EQP_MCOMAPARTNO"))
            ltMBuild.Text = Convert.ToString(reader("EQP_MCOMBLDG"))
            ltMStreet.Text = Convert.ToString(reader("EQP_MCOMSTREET"))
            ltMArea.Text = Convert.ToString(reader("EQP_MCOMAREA"))
            ltMemirate.Text = Convert.ToString(reader("M_EMR_DESCR"))

            ltMOcc.Text = Convert.ToString(reader("EQP_MOCC"))
            ltMComp.Text = Convert.ToString(reader("EQP_MCOMPANY"))
            ltMCurr_Contry.Text = Convert.ToString(reader("M_CTY_DESCRCURR"))


            'gUARDIAN details STARTs------------------------------


            ltGuardian_name.Text = Convert.ToString(reader("EQP_GuardianNAME")).Trim.Replace("''", "'")

            ltGnationality.Text = Convert.ToString(reader("G_Nationality1"))
            ltGNat2.Text = Convert.ToString(reader("G_Nationality2"))
            ltGpobox.Text = Convert.ToString(reader("EQP_GCOMPOBOX"))
            ltGofficetelephone.Text = Convert.ToString(reader("EQP_GOFFPHONE"))
            ltGhometelphone.Text = Convert.ToString(reader("EQP_GRESPHONE"))
            ltGfax.Text = Convert.ToString(reader("EQP_GFAX"))
            ltGmobile.Text = Convert.ToString(reader("EQP_GMOBILE"))
            ltGemail.Text = Convert.ToString(reader("EQP_GEMAIL"))


            ltGAdd1.Text = Convert.ToString(reader("EQP_GPRMADDR1"))
            ltGAdd2.Text = Convert.ToString(reader("EQP_GPRMADDR2"))


            ltGAddCity.Text = Convert.ToString(reader("EQP_GPRMCITY"))
            ltGAdd_Country.Text = Convert.ToString(reader("G_CTY_DESCRPER"))
            ltGAddPhone.Text = Convert.ToString(reader("EQP_GPRMPHONE"))

            ltGAdd_Zip.Text = Convert.ToString(reader("G_pobox"))

            ltGApart.Text = Convert.ToString(reader("EQP_GCOMAPARTNO"))
            ltGBuild.Text = Convert.ToString(reader("EQP_GCOMBLDG"))
            ltGStreet.Text = Convert.ToString(reader("EQP_GCOMSTREET"))
            ltGArea.Text = Convert.ToString(reader("EQP_GCOMAREA"))
            ltGemirate.Text = Convert.ToString(reader("G_EMR_DESCR"))

            ltGOcc.Text = Convert.ToString(reader("EQP_GOCC"))
            ltGComp.Text = Convert.ToString(reader("EQP_GCOMPANY"))

            ltFamily_detail.Text = Convert.ToString(reader("EQP_FAMILY_NOTE"))



            ltEmail_promo.Text = Convert.ToString(reader("EQM_bRCVMAIL"))
            ltMob_promo.Text = Convert.ToString(reader("EQM_bRCVSMS"))
            ltPhoto_promo.Text = Convert.ToString(reader("EQM_bRCVPUBL"))


            ltHth_No.Text = Convert.ToString(reader("EQM_HEALTHCARDNO"))
            ltB_grp.Text = Convert.ToString(reader("EQM_BLOODGROUP"))
            ltAlg_Detail.Text = Convert.ToString(reader("EQS_bALLERGIES"))
            divAlg.InnerText = Convert.ToString(reader("EQS_ALLERGIES"))

            ltSp_med.Text = Convert.ToString(reader("EQS_bRCVSPMEDICATION"))
            divSp_med.InnerText = Convert.ToString(reader("EQS_SPMEDICN"))

            ltPhy_edu.Text = Convert.ToString(reader("EQS_bPRESTRICTIONS"))
            divPhy_edu.InnerText = Convert.ToString(reader("EQS_PRESTRICTIONS"))

            ltHth_Info.Text = Convert.ToString(reader("EQS_bHRESTRICTIONS"))
            divHth_Info.InnerText = Convert.ToString(reader("EQS_HRESTRICTIONS"))

            ltLS_Ther.Text = Convert.ToString(reader("EQS_bTHERAPHY"))
            divLS_Ther.InnerText = Convert.ToString(reader("EQS_THERAPHY"))

            ltSEN.Text = Convert.ToString(reader("EQS_bSEN"))
            divSEN.InnerText = Convert.ToString(reader("EQS_SEN"))

            ltEAL.Text = Convert.ToString(reader("EQS_bEAL"))
            divEAL.InnerText = Convert.ToString(reader("EQS_EAL"))

            ltSEA.Text = Convert.ToString(reader("EQS_bENRICH"))
            divSEA.InnerText = Convert.ToString(reader("EQS_ENRICH"))

            ltMus_Prof.Text = Convert.ToString(reader("EQS_bMUSICAL"))
            divMus_Prof.InnerText = Convert.ToString(reader("EQS_MUSICAL"))

            ltSport.Text = Convert.ToString(reader("EQS_bSPORTS"))
            divSport.InnerText = Convert.ToString(reader("EQS_SPORTS"))
            ltHthComm.Text = Convert.ToString(reader("EQS_bCommInt"))
            divHthComm.InnerText = Convert.ToString(reader("EQS_CommInt"))
            ltHthDisabled.Text = Convert.ToString(reader("EQS_bDisabled"))
            divHthDisabled.InnerText = Convert.ToString(reader("EQS_Disabled"))
            ltPrev_sch.Text = Convert.ToString(reader("EQS_bBEHAVIOUR"))
            divPrev_sch.InnerText = Convert.ToString(reader("EQS_BEHAVIOUR"))

            ltProEng_R.Text = Convert.ToString(reader("EQS_ENG_READING"))
            ltProEng_W.Text = Convert.ToString(reader("EQS_ENG_WRITING"))
            ltProEng_S.Text = Convert.ToString(reader("EQS_ENG_SPEAKING"))

            ltAppNo.Text = Convert.ToString(reader("EQS_APPLNO"))
            ltAcdYear.Text = Convert.ToString(reader("ACY_DECR"))
            ltGrade.Text = Convert.ToString(reader("GRD_DESCR"))
            ltShift.Text = Convert.ToString(reader("SHF_DESCR"))
            ltStream.Text = Convert.ToString(reader("STM_DESCR"))
            ltCurr.Text = Convert.ToString(reader("CLM_DESCR"))
            ltTenJoinDate.Text = DateFormat(Convert.ToDateTime(reader("EQS_DOJ")))
            ltAppNo.Text = Convert.ToString(reader("EQS_APPLNO"))
            HFstuEqs.Value = Encr_decrData.Encrypt(Convert.ToString(reader("Eqs_id")))

            HiddenEQSID.Value = Convert.ToString(reader("Eqs_id"))
            HFEQS_ACD.Value = Encr_decrData.Encrypt(Convert.ToString(reader("EQS_ACD_ID")))
            Dim EQS_STATUS As String = Convert.ToString(reader("EQS_STATUS"))
            HFSTATUS.Value = Encr_decrData.Encrypt(EQS_STATUS)
            HFACCNO.Value = Convert.ToString(reader("EQS_ACCNO"))
            HFSTGCount.Value = Get_STG_Four_ORDER().ToString()
            Try
                HFREGNDATE.Value = Convert.ToString(reader("EQS_REGNDATE"))
            Catch ex As Exception

            End Try
            HFACCNO.Value = Convert.ToString(reader("EQS_ACCNO"))
            HFLinkRegFee.Value = Convert.ToString(reader("Link_RegFee_Pag"))

            Showand_hide_BtnSTATUS(EQS_STATUS)
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "progress_bar_active('" + EQS_STATUS + "');", True)

            If Convert.ToBoolean(reader("EQS_bAPPLSIBLING")) = True Then
                trSib_1.Visible = True
                'trSib_2.Visible = True

                ltSib.Text = "Yes"
                ltSibName.Text = Convert.ToString(reader("SIBNAME"))
                ltSibFeeID.Text = Convert.ToString(reader("SIBLINGFEEID"))
                ltSibGEMS.Text = Convert.ToString(reader("SIBBSU"))
            Else
                trSib_1.Visible = False
                'trSib_2.Visible = False
                ltSib.Text = "No"
                ltSibName.Text = ""
                ltSibFeeID.Text = ""
                ltSibGEMS.Text = ""
            End If

            If Convert.ToBoolean(reader("EQS_bTPTREQD")) = True Then
                trTranSch_1.Visible = True
                'trTranSch_2.Visible = True
                'trTranSch_3.Visible = True

                ltMain_Loc.Text = Convert.ToString(reader("LOC_DESCR"))
                ltSub_Loc.Text = Convert.ToString(reader("SBL_DESCR"))
                ltPick_Up.Text = Convert.ToString(reader("PNT_DESCR"))
                ltTransOpt.Text = Convert.ToString(reader("EQS_TPTREMARKS"))

            Else
                trTranSch_1.Visible = False
                'trTranSch_2.Visible = False
                'trTranSch_3.Visible = False
                ltMain_Loc.Text = ""
                ltSub_Loc.Text = ""
                ltPick_Up.Text = ""
                ltTransOpt.Text = ""

            End If





        End While
        reader.Close()

        ltAcdYear.Text = HiddenAcademicyear.Value
        ''Contact Details

        hideGuardian_info(ltGuardian_name.Text)



    End Sub
    Sub bind_qus()
        Dim conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@ENQID", hfENQ_ID.Value)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETENQUIRY_QUS_PRINT", param)
        rptQus.DataSource = ds
        rptQus.DataBind()
    End Sub
    Sub hideGuardian_info(ByVal Guard_name As String)

        If Guard_name.Trim = "" Then

            trGr1.Visible = False
            trGr2.Visible = False
            trGr3.Visible = False
            'trGr4.Visible = False
            'trGr5.Visible = False
            'trGr6.Visible = False
            'trGr7.Visible = False
            'trGr8.Visible = False
            'trGr9.Visible = False
            'trGr10.Visible = False
            'trGr11.Visible = False
            'trGr12.Visible = False
            'trGr13.Visible = False
            'trGr14.Visible = False
            'trGr15.Visible = False
            'trGr16.Visible = False

        End If
    End Sub

    Function DateFormat(ByVal fDate As DateTime) As String
        Return Format(fDate, "dd/MMM/yyyy").Replace("01/Jan/1900", "")
    End Function

    Private Sub PopulatePrevData()
        Try


            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query As String = "exec GetPrev_school_print '" + hfENQ_ID.Value + "','" + Session("sbsuid") + "'"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                gvSchool.DataSource = ds
                gvSchool.DataBind()
            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvSchool.DataSource = ds.Tables(0)
                Try
                    gvSchool.DataBind()
                Catch ex As Exception
                End Try

                'Dim columnCount As Integer = gvSchool.Rows(0).Cells.Count
                ''Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                'gvSchool.Rows(0).Cells.Clear()
                'gvSchool.Rows(0).Cells.Add(New TableCell)
                'gvSchool.Rows(0).Cells(0).ColumnSpan = columnCount
                'gvSchool.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                'gvSchool.Rows(0).Cells(0).Text = "Record not available "



            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "PopulatePrevData")

        End Try
    End Sub
#End Region

#Region "Short List Enquir"
    Protected Sub LbtnShortlistEnquiry_Click(sender As Object, e As EventArgs)
        Try
            Allow_Permissions = GetAllow_Permissions(btnShortlistConfirm)
            VisibleFalseAll()
            Details_holder.Visible = False
            Shortlist_Holder.Visible = True
            Dim str_Sql As String = "exec FEES.GET_STUDENTSHORTLIST_DUE  '" & Mainclass.cleanString(Encr_decrData.Decrypt(HFstuEqs.Value)) & "'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds
            If ds.Tables.Count > 0 And Mainclass.cleanString(Request.QueryString("noclose")) <> "1" Then
                If ds.Tables(0).Rows.Count = 0 Then
                    'Dim javaSr As String = "<script language=javascript>  window.close();</script>"
                    'Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)
                End If
            End If
            GridViewShowDetails.EmptyDataText = "There is No Possible Duplicate Records Found"
            GridViewShowDetails.DataBind()
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnShortlistConfirm_Click(sender As Object, e As EventArgs)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String
            Dim StatusMode_New As String = "OPN"
            Dim EQS_STATUS As String = Encr_decrData.Decrypt(HFSTATUS.Value)
            If EQS_STATUS = "NEW" Then
                str_query = "exec studSaveStudEnqAcceptReject " + Encr_decrData.Decrypt(HFstuEqs.Value) + ",'" + StatusMode_New + "'," + Encr_decrData.Decrypt(HFEQS_ACD.Value)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                Popup_Message = "Record saved successfully"
                Popup_Message_Status = WarningType.Success
                LbtnShortlistEnquiry.Enabled = False
                HFSTATUS.Value = Encr_decrData.Encrypt(StatusMode_New)
                Showand_hide_BtnSTATUS(StatusMode_New)
                ScriptManager.RegisterStartupScript(Me, Page.GetType, "ScriptOPN", "progress_bar_active('" + StatusMode_New + "');", True)
            Else
                Popup_Message = "Request could not be processed"
                Popup_Message_Status = WarningType.Danger
            End If

        Catch ex As Exception
            Popup_Message = "Request could not be processed"
            Popup_Message_Status = WarningType.Danger
        Finally
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            Details_holder.Visible = True
            Shortlist_Holder.Visible = False
        End Try
    End Sub
    Protected Sub btnShortlistCancel_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        Shortlist_Holder.Visible = False
        GridViewShowDetails.DataSource = New List(Of String)
        GridViewShowDetails.DataBind()
    End Sub

#End Region

#Region "Register"

    Protected Sub lbtnRegister_Click(sender As Object, e As EventArgs)
        Allow_Permissions = GetAllow_Permissions(RegisterSave)
        VisibleFalseAll()
        Details_holder.Visible = False
        Register_Holder.Visible = True
    End Sub

    Protected Sub RegisterSave_Click(sender As Object, e As EventArgs)
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim StatusMode_New As String = "REG"
                Dim EQS_STATUS As String = Encr_decrData.Decrypt(HFSTATUS.Value)
                If EQS_STATUS = "OPN" Then
                    Dim retFlag As Integer = 0
                    retFlag = updateStudEnquirySchoolPrio_S(transaction)
                    If retFlag <> 0 Then
                        Popup_Message = "Request could not be processed"
                        Popup_Message_Status = WarningType.Danger
                    Else
                        transaction.Commit()
                        Try
                            HFREGNDATE.Value = txtregdate.Text.ToString
                        Catch ex As Exception

                        End Try
                        Popup_Message = "Record saved successfully"
                        Popup_Message_Status = WarningType.Success
                        lbtnRegister.Enabled = False
                        HFSTATUS.Value = Encr_decrData.Encrypt(StatusMode_New)
                        Showand_hide_BtnSTATUS(StatusMode_New)
                        ScriptManager.RegisterStartupScript(Me, Page.GetType, "ScriptREG", "progress_bar_active('" + StatusMode_New + "');", True)

                    End If
                Else
                    Popup_Message = "Request could not be processed"
                    Popup_Message_Status = WarningType.Danger
                End If
            Catch ex As Exception
                Popup_Message = "Request could not be processed"
                Popup_Message_Status = WarningType.Danger
            Finally
                usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                Details_holder.Visible = True
                Register_Holder.Visible = False
                txtregdate.Text = ""
            End Try
        End Using
    End Sub
    Private Function updateStudEnquirySchoolPrio_S(ByVal transaction As SqlTransaction) As Integer
        Try
            Dim lstrReg As String
            lstrReg = ""
            Dim strReg As String
            'registration date can be updated once only as it may affect the reports if the  date is canged

            lstrReg = "REG"
            strReg = "'" + txtregdate.Text.ToString + "'"

            Dim EQS_ENG_READING As String = String.Empty
            Dim EQS_ENG_WRITING As String = String.Empty


            Dim param(70) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EQS_ID", Encr_decrData.Decrypt(HFstuEqs.Value).ToString)

            param(6) = New SqlClient.SqlParameter("@EQS_REG", lstrReg)
            param(7) = New SqlClient.SqlParameter("@EQS_REGNDATE", IIf(strReg = "NULL", System.DBNull.Value, txtregdate.Text.ToString))

            param(9) = New SqlClient.SqlParameter("@EQS_ACCNO", HFACCNO.Value)
            param(10) = New SqlClient.SqlParameter("@EQS_bALLERGIES", Convert_string_ToBoolean(ltAlg_Detail.Text))
            param(11) = New SqlClient.SqlParameter("@EQS_ALLERGIES", divAlg.InnerText)

            param(12) = New SqlClient.SqlParameter("@EQS_bRCVSPMEDICATION", Convert_string_ToBoolean(ltSp_med.Text))
            param(13) = New SqlClient.SqlParameter("@EQS_SPMEDICN", divSp_med.InnerText)

            param(14) = New SqlClient.SqlParameter("@EQS_bPRESTRICTIONS", Convert_string_ToBoolean(ltPhy_edu.Text))
            param(15) = New SqlClient.SqlParameter("@EQS_PRESTRICTIONS", divPhy_edu.InnerText)

            param(16) = New SqlClient.SqlParameter("@EQS_bHRESTRICTIONS", Convert_string_ToBoolean(ltHth_Info.Text))
            param(17) = New SqlClient.SqlParameter("@EQS_HRESTRICTIONS", divHth_Info.InnerText)


            param(18) = New SqlClient.SqlParameter("@EQS_bTHERAPHY", Convert_string_ToBoolean(ltLS_Ther.Text))
            param(19) = New SqlClient.SqlParameter("@EQS_THERAPHY", divLS_Ther.InnerText)

            param(20) = New SqlClient.SqlParameter("@EQS_bSEN", Convert_string_ToBoolean(ltSEN.Text))
            param(21) = New SqlClient.SqlParameter("@EQS_SEN", divSEN.InnerText)

            param(22) = New SqlClient.SqlParameter("@EQS_bEAL", Convert_string_ToBoolean(ltEAL.Text))
            param(23) = New SqlClient.SqlParameter("@EQS_EAL", divEAL.InnerText)

            param(24) = New SqlClient.SqlParameter("@EQS_bMUSICAL", Convert_string_ToBoolean(ltMus_Prof.Text))
            param(25) = New SqlClient.SqlParameter("@EQS_MUSICAL", divMus_Prof.InnerText)

            param(26) = New SqlClient.SqlParameter("@EQS_bENRICH", Convert_string_ToBoolean(ltSEA.Text))
            param(27) = New SqlClient.SqlParameter("@EQS_ENRICH", divSEA.InnerText)

            param(28) = New SqlClient.SqlParameter("@EQS_bBEHAVIOUR", Convert_string_ToBoolean(ltPrev_sch.Text))
            param(29) = New SqlClient.SqlParameter("@EQS_BEHAVIOUR", divPrev_sch.InnerText)
            param(30) = New SqlClient.SqlParameter("@EQS_bSPORTS", Convert_string_ToBoolean(ltSport.Text))
            param(31) = New SqlClient.SqlParameter("@EQS_SPORTS", divSport.InnerText)

            param(35) = New SqlClient.SqlParameter("@EQS_bCommInt", Convert_string_ToBoolean(ltHthComm.Text))
            param(36) = New SqlClient.SqlParameter("@EQS_CommInt", divHthComm.InnerText.Replace("'", "''"))
            param(37) = New SqlClient.SqlParameter("@EQS_bDisabled", Convert_string_ToBoolean(ltHthDisabled.Text))
            param(38) = New SqlClient.SqlParameter("@EQS_Disabled", divHthDisabled.InnerText.Replace("'", "''"))

            param(39) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_name"))


            param(40) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(40).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "updateStudEnquirySchoolPrioForREG", param)
            Dim ReturnFlag As Integer = param(40).Value
            Return ReturnFlag

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "updateStudEnquirySchoolPrio_S")
            Return -1


        End Try

    End Function
    Protected Sub RegisterCancel_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        Register_Holder.Visible = False
        txtregdate.Text = ""
    End Sub

#End Region

#Region "Register Slip Print"

    Protected Sub lbtnRegisterSlip_Print_Click(sender As Object, e As EventArgs)
        VisibleFalseAll()
        Details_holder.Visible = False
        RegisterSlip_Print_Holder.Visible = True
    End Sub
    Protected Sub btnPreviewRegisterSlip_Click(sender As Object, e As EventArgs)
        Dim HTMLStr As String = GetRegisterSlipAsHtml(HiddenEQSID.Value)
        DIV_RegisterSlip.InnerHtml = HTMLStr
        DIV_RegisterSlip.Visible = True

    End Sub
    Protected Sub btnRegisterSlipToPDF_Click(sender As Object, e As EventArgs)
        Dim HTMLStr As String = GetRegisterSlipAsHtml(HiddenEQSID.Value)
        Html2PdfPreview2(HTMLStr, HiddenEQSID.Value, "Register Slip")
    End Sub
    Protected Sub btnCancelPrintRegisterSlip_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        RegisterSlip_Print_Holder.Visible = False
        DIV_RegisterSlip.InnerHtml = ""
        DIV_RegisterSlip.Visible = False
    End Sub
    Public Function GetRegisterSlipAsHtml(EQS_ID As String) As String
        Dim result As String = ""
        Dim HTML As String = ScreenScrapeHtml(Server.MapPath("~\Students\RegistrationSlip.html"))
        Dim BSU_ID As String = String.Empty
        Dim IMG_IMAGE As String = String.Empty
        Dim BSU_NAME As String = String.Empty
        Dim BSU_POBOX As String = String.Empty
        Dim BSU_TEL As String = String.Empty
        Dim BSU_FAX As String = String.Empty
        Dim BSU_EMAIL As String = String.Empty
        Dim BSU_URL As String = String.Empty
        Dim STU_NO As String = String.Empty
        Dim EQS_REGNDATE As String = String.Empty
        Dim EQS_APPLNO As String = String.Empty
        Dim STU_NAME As String = String.Empty
        Dim ACY_DESCR As String = String.Empty
        Dim GRM_DISPLAY As String = String.Empty
        Dim PNAME As String = String.Empty

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_ID", EQS_ID)
        Using readerData As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "rptREGSLIP", pParms)
            While readerData.Read
                BSU_ID = Convert.ToString(readerData("EQS_BSU_ID"))
                STU_NO = Convert.ToString(readerData("STU_NO"))
                EQS_REGNDATE = Convert.ToString(readerData("EQS_REGNDATE"))
                EQS_APPLNO = Convert.ToString(readerData("EQS_APPLNO"))
                STU_NAME = Convert.ToString(readerData("STU_NAME"))
                ACY_DESCR = Convert.ToString(readerData("ACY_DESCR"))
                GRM_DISPLAY = Convert.ToString(readerData("GRM_DISPLAY"))
                PNAME = Convert.ToString(readerData("PNAME"))
                'Get School date 


            End While
        End Using
        Dim SParms(3) As SqlClient.SqlParameter
        SParms(0) = New SqlClient.SqlParameter("@IMG_BSU_ID", BSU_ID)
        SParms(1) = New SqlClient.SqlParameter("@IMG_TYPE", "logo")
        Using readerSchool As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "getBsuInFoWithImage", SParms)
            While readerSchool.Read
                Try
                    If IsDBNull(readerSchool("IMG_IMAGE")) Then
                        IMG_IMAGE = ""
                    Else
                        Dim imageUrl As String = "data:image/png;base64," & Convert.ToBase64String(CType(readerSchool("IMG_IMAGE"), Byte()))
                        IMG_IMAGE = imageUrl
                    End If
                Catch ex As Exception

                End Try

                BSU_NAME = Convert.ToString(readerSchool("BSU_NAME"))
                BSU_POBOX = Convert.ToString(readerSchool("BSU_POBOX"))
                BSU_FAX = Convert.ToString(readerSchool("BSU_FAX"))
                BSU_TEL = Convert.ToString(readerSchool("BSU_TEL"))
                BSU_EMAIL = Convert.ToString(readerSchool("BSU_EMAIL"))
                BSU_URL = Convert.ToString(readerSchool("BSU_URL"))

            End While
        End Using
        If Trim(STU_NO) <> "" Then
            HTML = HTML.Replace("@STU_NO", UCase(STU_NO))
        Else
            HTML = HTML.Replace("@STU_NO", "")
        End If
        If Trim(EQS_REGNDATE) <> "" Then
            Dim EQS_REGNDATE_str As String = ""
            Try
                EQS_REGNDATE_str = Convert.ToDateTime(EQS_REGNDATE).ToString("dd/MMM/yyyy")
            Catch ex As Exception

            End Try
            HTML = HTML.Replace("@EQS_REGNDATE", EQS_REGNDATE_str)
        Else
            HTML = HTML.Replace("@EQS_REGNDATE", "")
        End If
        If Trim(EQS_APPLNO) <> "" Then
            HTML = HTML.Replace("@EQS_APPLNO", EQS_APPLNO)
        Else
            HTML = HTML.Replace("@EQS_APPLNO", "")
        End If
        If Trim(STU_NAME) <> "" Then
            HTML = HTML.Replace("@STU_NAME", STU_NAME)
        Else
            HTML = HTML.Replace("@STU_NAME", "")
        End If
        If Trim(ACY_DESCR) <> "" Then
            HTML = HTML.Replace("@ACY_DESCR", ACY_DESCR)
        Else
            HTML = HTML.Replace("@ACY_DESCR", "")
        End If
        If Trim(GRM_DISPLAY) <> "" Then
            HTML = HTML.Replace("@GRM_DISPLAY", GRM_DISPLAY)
        Else
            HTML = HTML.Replace("@GRM_DISPLAY", "")
        End If
        If Trim(PNAME) <> "" Then
            HTML = HTML.Replace("@PNAME", PNAME)
        Else
            HTML = HTML.Replace("@PNAME", "")
        End If
        If Trim(IMG_IMAGE) <> "" Then
            HTML = HTML.Replace("@IMG_IMAGE", IMG_IMAGE)
        Else
            HTML = HTML.Replace("@IMG_IMAGE", "")
        End If
        If Trim(BSU_NAME) <> "" Then
            HTML = HTML.Replace("@BSU_NAME", BSU_NAME)
        Else
            HTML = HTML.Replace("@BSU_NAME", "")
        End If
        If Trim(BSU_POBOX) <> "" Then
            HTML = HTML.Replace("@BSU_POBOX", BSU_POBOX)
        Else
            HTML = HTML.Replace("@BSU_POBOX", "")
        End If
        If Trim(BSU_FAX) <> "" Then
            HTML = HTML.Replace("@BSU_FAX", BSU_FAX)
        Else
            HTML = HTML.Replace("@BSU_FAX", "")
        End If
        If Trim(BSU_TEL) <> "" Then
            HTML = HTML.Replace("@BSU_TEL ", BSU_TEL)
        Else
            HTML = HTML.Replace("@BSU_TEL", "")
        End If
        If Trim(BSU_EMAIL) <> "" Then
            HTML = HTML.Replace("@BSU_EMAIL", BSU_EMAIL)
        Else
            HTML = HTML.Replace("@BSU_EMAIL", "")
        End If
        If Trim(BSU_URL) <> "" Then
            HTML = HTML.Replace("@BSU_URL", BSU_URL)
        Else
            HTML = HTML.Replace("@BSU_URL", "")
        End If
        result = HTML
        Return result
    End Function

#End Region

#Region "Screening "
    Protected Sub lbtnScreening_Click(sender As Object, e As EventArgs)
        Allow_Permissions = GetAllow_Permissions(Screening_Save)
        VisibleFalseAll()
        Details_holder.Visible = False
        Screening_Holder.Visible = True
        Dim Encr_decrData As New Encryption64()
        HiddenEQSID.Value = Encr_decrData.Decrypt(HFstuEqs.Value)
        If studClass.GetBsuShowDocs(Session("sbsuid")) = True Then
            studClass.BindDocumentsList(lstNotSubmittedForScreening, "5", "false", HiddenEQSID.Value)
            studClass.BindDocumentsList(lstNotSubmittedForScreening, "5", "true", HiddenEQSID.Value)
            If lstNotSubmittedForScreening.Items.Count <> 0 Or lstNotSubmittedForScreening.Items.Count <> 0 Then
                trDoc_Scr.Visible = True
            Else
                trDoc_Scr.Visible = False
            End If
            lnkDocs_Scr.Visible = False
        Else
            hfURL.Value = "studJoinDocuments.aspx?eqsid=" + HiddenEQSID.Value
            lnkDocs_Scr.Visible = True
            trDoc_Scr.Visible = False
        End If
        If CHECK_MAIL() = "INVALID" Then
            tr11_Scr.Style.Add("display", "none")
            tr12_Scr.Style.Add("display", "none")
            tr13_Scr.Style.Add("display", "none")
        End If
        BindScreeningDecision()
        BindScreeningReason(ddlScreeningDecision.Items(0).Value)
        'CODE ADDED BY LIJO 23/NOV/2008

        GetInfo()
    End Sub
    Function CHECK_MAIL() As String
        Dim str As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT CASE WHEN BSU_bSCR_MAIL=1 THEN 'VALID' ELSE 'INVALID' END ISMAIL FROM businessunit_m  WHERE BSU_ID='" + Session("SBSUID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            str = ds.Tables(0).Rows(0).Item("ISMAIL")
        Else
            str = "INVALID"
        End If
        Return str
    End Function
    Sub BindScreeningDecision()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT APL_ID=CONVERT(VARCHAR(100),APL_ID)+'|'+CONVERT(VARCHAR(10),APL_DISABLE),APL_DESCR FROM APPLICATIONDECISION_M WHERE APL_BSU_ID='" + Session("SBSUID") + "' AND APL_TYPE='SCR'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlScreeningDecision.DataSource = ds
        ddlScreeningDecision.DataTextField = "APL_DESCR"
        ddlScreeningDecision.DataValueField = "APL_ID"
        ddlScreeningDecision.DataBind()
    End Sub
    Sub BindScreeningReason(ByVal aplid As String)
        Dim apl_id As String() = aplid.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT APR_ID,APR_DESCR FROM APPLDESC_REASON_M WHERE APR_APL_ID=" + apl_id(0)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlScreeningReason.DataSource = ds
        ddlScreeningReason.DataTextField = "APR_DESCR"
        ddlScreeningReason.DataValueField = "APR_ID"
        ddlScreeningReason.DataBind()
    End Sub


    Protected Sub ddlScreeningDecision_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlScreeningDecision.SelectedIndexChanged
        BindScreeningReason(ddlScreeningDecision.SelectedValue)


    End Sub

    Protected Sub btnRightForScreening_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRightForScreening.Click
        studClass.UpdateDocListItems(lstNotSubmittedForScreening, lstNotSubmittedForScreening, "true", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub

    Protected Sub btnLeftForScreening_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeftForScreening.Click
        studClass.UpdateDocListItems(lstNotSubmittedForScreening, lstNotSubmittedForScreening, "false", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub
    Protected Sub Screening_Save_Click(sender As Object, e As EventArgs)
        Try
            If ddlScreeningDecision.SelectedItem.Text = "CLEARED" And ddlScreeningReason.SelectedItem.Text = "CLEARED" Then


                Dim StatusMode_New As String = "SCR"
                Dim EQS_STATUS As String = Encr_decrData.Decrypt(HFSTATUS.Value)
                If EQS_STATUS = "REG" Then
                    Dim str_query = ""
                    Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString()
                    Dim eqs_id = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
                    Dim apr_id As String() = ddlScreeningDecision.SelectedValue.Split("|")
                    Dim ChkState As String = String.Empty
                    ChkState = "1"
                    str_query = "exec studDirectApproval_Screening " _
                                                      & "'" + Format(Now, "yyyy-MM-dd") + "'," _
                                                      & "'" + apr_id(1) + "'," _
                                                      & ddlScreeningReason.SelectedValue.ToString + "," _
                                                      & apr_id(0) + "," _
                                                      & HiddenEQSID.Value + "," _
                                                      & ChkState + ",'" _
                                                      & txtScrDate.Text + "','" _
                                                      & rdtime.DateInput.Text + "','" _
                                                      & txtDetails.Text + "','" _
                                                      & Request.QueryString("eqsid") + "'"

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    updateenquiry(Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+")), "SCR", 4, 4, True)

                    Popup_Message = "Record saved successfully"
                    Popup_Message_Status = WarningType.Success
                    lbtnScreening.Enabled = False
                    HFSTATUS.Value = Encr_decrData.Encrypt(StatusMode_New)
                    Showand_hide_BtnSTATUS(StatusMode_New)
                    ScriptManager.RegisterStartupScript(Me, Page.GetType, "ScriptREG", "progress_bar_active('" + StatusMode_New + "');", True)

                Else
                    Popup_Message = "Request could not be processed"
                    Popup_Message_Status = WarningType.Danger
                End If
                Details_holder.Visible = True
                Screening_Holder.Visible = False
            Else
                Popup_Message = "Screening Status  and Reason Must be Cleared to process"
                Popup_Message_Status = WarningType.Danger
            End If
        Catch ex As Exception
            Popup_Message = "Request could not be processed"
            Popup_Message_Status = WarningType.Danger
            Details_holder.Visible = True
            Screening_Holder.Visible = False
        Finally
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
        End Try

    End Sub


    Protected Sub Screening_Cancel_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        Screening_Holder.Visible = False
    End Sub

#End Region

#Region "Approval"
    Protected Sub lbtnApproval_Click(sender As Object, e As EventArgs)
        Allow_Permissions = GetAllow_Permissions(Approval_Save)
        VisibleFalseAll()
        Details_holder.Visible = False
        Approval_Holder.Visible = True
        Dim Encr_decrData As New Encryption64()
        HiddenEQSID.Value = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
        If studClass.GetBsuShowDocs(Session("sbsuid")) = True Then
            studClass.BindDocumentsList(lstNotSubmittedForApproval, "5", "false", HiddenEQSID.Value)
            studClass.BindDocumentsList(lstSubmittedForApproval, "5", "true", HiddenEQSID.Value)
            If lstNotSubmittedForApproval.Items.Count <> 0 Or lstSubmittedForApproval.Items.Count <> 0 Then
                trDocApp.Visible = True
            Else
                trDocApp.Visible = False
            End If
            lnkDocs_App.Visible = False
        Else
            hfURL.Value = "studJoinDocuments.aspx?eqsid=" + HiddenEQSID.Value
            lnkDocs_App.Visible = True
            trDocApp.Visible = False
        End If

        BindApprovalDecision()
        BindApprovaReason(ddlApprovalDecision.Items(0).Value)
        'CODE ADDED BY LIJO 23/NOV/2008
        enable_disble_ChkPrint(ddlApprovalDecision.Items(0).Value)
        GetInfo()

    End Sub

    Protected Sub btnRightForApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRightForApproval.Click
        studClass.UpdateDocListItems(lstNotSubmittedForApproval, lstSubmittedForApproval, "true", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub

    Protected Sub btnLeftForApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeftForApproval.Click
        studClass.UpdateDocListItems(lstSubmittedForApproval, lstNotSubmittedForApproval, "false", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub
    Sub BindApprovalDecision()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT APL_ID=CONVERT(VARCHAR(100),APL_ID)+'|'+CONVERT(VARCHAR(10),APL_DISABLE),APL_DESCR FROM APPLICATIONDECISION_M WHERE APL_BSU_ID='" + Session("SBSUID") + "' and apl_type='APR'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlApprovalDecision.DataSource = ds
        ddlApprovalDecision.DataTextField = "APL_DESCR"
        ddlApprovalDecision.DataValueField = "APL_ID"
        ddlApprovalDecision.DataBind()
    End Sub

    Sub BindApprovaReason(ByVal aplid As String)
        Dim apl_id As String() = aplid.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT APR_ID,APR_DESCR FROM APPLDESC_REASON_M WHERE APR_APL_ID=" + apl_id(0)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlApprovalReason.DataSource = ds
        ddlApprovalReason.DataTextField = "APR_DESCR"
        ddlApprovalReason.DataValueField = "APR_ID"
        ddlApprovalReason.DataBind()
    End Sub
    Protected Sub ddlApprovalDecision_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApprovalDecision.SelectedIndexChanged
        BindApprovaReason(ddlApprovalDecision.SelectedValue)

        enable_disble_ChkPrint(ddlApprovalDecision.SelectedValue)
    End Sub
    Sub enable_disble_ChkPrint(ByVal APL_ID As String)
        Dim APL_FLAG As String() = APL_ID.Split("|")
        If APL_FLAG(1) = 1 Then
            chkPrint.Checked = False
            chkPrint.Enabled = False
        Else
            chkPrint.Enabled = True
        End If
    End Sub
    Protected Sub Approval_Save_Click(sender As Object, e As EventArgs)
        Try
            If ddlApprovalDecision.SelectedItem.Text = "APPROVED" And ddlApprovalReason.SelectedItem.Text = "APPROVED" Then
                Dim StatusMode_New As String = "APR"
                Dim EQS_STATUS As String = Encr_decrData.Decrypt(HFSTATUS.Value)
                If EQS_STATUS = "SCR" Then
                    Dim Encr_decrData As New Encryption64()
                    Dim str_query = ""
                    Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString()
                    Dim eqs_id = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))

                    Dim apr_id As String() = ddlApprovalDecision.SelectedValue.Split("|")
                    Dim ChkState As String = String.Empty
                    If chkPrint.Checked Then
                        ChkState = "1"
                    Else
                        ChkState = "0"
                    End If



                    str_query = "exec studDirectApproval " _
                                              & "'" + Format(Now, "yyyy-MM-dd") + "'," _
                                              & "'" + apr_id(1) + "'," _
                                              & ddlApprovalReason.SelectedValue.ToString + "," _
                                              & apr_id(0) + "," _
                                              & HiddenEQSID.Value + "," _
                                              & chkPrint.Checked

                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

                    If chkPrint.Checked = True Then

                        Dim param As New Hashtable
                        Dim EQS_IDs As String
                        EQS_IDs = HiddenEQSID.Value + "|"
                        param.Add("@IMG_BSU_ID", Session("sBsuid"))
                        param.Add("@IMG_TYPE", "LOGO")
                        param.Add("@EQS_IDs", EQS_IDs)
                        param.Add("@BSU_ID", Session("sBsuid"))
                        param.Add("@BAL_APL_ID", apr_id(0))
                        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
                        param.Add("UserName", Session("sUsr_name"))

                        Dim rptClass As New rptClass
                        With rptClass
                            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                            .reportParameters = param

                            .reportPath = Server.MapPath("~\Students\Reports\RPT\rptOffer_letter.rpt")


                        End With
                        Session("rptClass") = rptClass
                        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

                    Else
                        updateenquiry(Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+")), "APR", 5, 5, True)
                    End If

                    Popup_Message = "Record saved successfully"
                    Popup_Message_Status = WarningType.Success
                    lbtnApproval.Enabled = False
                    HFSTATUS.Value = Encr_decrData.Encrypt(StatusMode_New)
                    Showand_hide_BtnSTATUS(StatusMode_New)
                    ScriptManager.RegisterStartupScript(Me, Page.GetType, "ScriptREG", "progress_bar_active('" + StatusMode_New + "');", True)

                Else
                    Popup_Message = "Request could not be processed"
                    Popup_Message_Status = WarningType.Danger
                End If
                Details_holder.Visible = True
                Approval_Holder.Visible = False
            Else
                Popup_Message = "Application decision   and Reason Must be Approved to process"
                Popup_Message_Status = WarningType.Danger
            End If
        Catch ex As Exception
            Popup_Message = "Request could not be processed"
            Popup_Message_Status = WarningType.Danger
            Details_holder.Visible = True
            Approval_Holder.Visible = False
        Finally
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
        End Try
    End Sub
    Protected Sub Approval_Cancel_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        Approval_Holder.Visible = False
    End Sub
#End Region

#Region "Offer Letter"
    Protected Sub lbtnOfferLetter_Click(sender As Object, e As EventArgs)
        Allow_Permissions = GetAllow_Permissions(btnOfferLetterConfirm)
        VisibleFalseAll()
        Details_holder.Visible = False
        OfferLetter_Holder.Visible = True

        CheckOfferIssued()
        GET_STUDENTSHORTLIST_DUE()
        '***********Code added by dhanya 16-07-08

        If studClass.GetBsuShowDocs(Session("sbsuid")) = True Then
            studClass.BindDocumentsList(lstNotSubmitted_OfferLetter, "6", "false", HiddenEQSID.Value)
            studClass.BindDocumentsList(lstSubmitted_OfferLetter, "6", "true", HiddenEQSID.Value)
            If lstNotSubmitted_OfferLetter.Items.Count <> 0 Or lstSubmitted_OfferLetter.Items.Count <> 0 Then
                trDoc_OfferLetter.Visible = True
            Else
                trDoc_OfferLetter.Visible = False
            End If
            lnkDocs_OfferLetter.Visible = False
        Else
            hfURL.Value = "studJoinDocuments.aspx?eqsid=" + HiddenEQSID.Value
            lnkDocs_OfferLetter.Visible = True
            trDoc_OfferLetter.Visible = False
        End If

    End Sub
    Public Sub CheckOfferIssued()
        Dim Decr_decrData As New Encryption64()
        ''Get the Enquiry Ids.
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString()
        Dim eqsid As String = Decr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
        HiddenEQSID.Value = eqsid
        Dim enqid As String = Request.QueryString("enqid")
        Dim str_query = "select PRA_bCOMPLETED from PROCESSFO_APPLICANT_S where pra_eqs_id='" & eqsid & "' and PRA_STG_ID='6'"
        Dim val As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()
        If val = "True" Then
            'Response.Redirect("~/Students/studPrintOffer.aspx?eqsid=" + Request.QueryString("eqsid") + "&enqid=" + enqid)
        End If

    End Sub
    Sub GET_STUDENTSHORTLIST_DUE()
        Try
            Dim str_Sql As String = "exec FEES.GET_STUDENTSHORTLIST_DUE  '" & HiddenEQSID.Value.ToString & "'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            GridViewShowDetails_OfferLetter.DataSource = ds

            GridViewShowDetails_OfferLetter.EmptyDataText = "No data Found"
            GridViewShowDetails_OfferLetter.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnRight_OfferLetter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRight_OfferLetter.Click
        studClass.UpdateDocListItems(lstNotSubmitted_OfferLetter, lstSubmitted_OfferLetter, "true", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub

    Protected Sub btnLeft_OfferLetter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeft_OfferLetter.Click
        studClass.UpdateDocListItems(lstSubmitted_OfferLetter, lstNotSubmitted_OfferLetter, "false", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub
    Protected Sub btnOfferLetterConfirm_Click(sender As Object, e As EventArgs)
        Try
            Dim StatusMode_New As String = "OFR"
            Dim EQS_STATUS As String = Encr_decrData.Decrypt(HFSTATUS.Value)
            If EQS_STATUS = "REG" Or EQS_STATUS = "APR" Then

                Dim Decr_decrData As New Encryption64()
                ''Get the Enquiry Ids.
                Dim eqsid As String = Request.QueryString("eqsid")
                Dim enqid As String = Request.QueryString("enqid")

                Dim Engchange As New studClass
                Dim dec As New Encryption64
                eqsid = dec.Decrypt(eqsid.Replace(" ", "+"))
                ''Change Enquirey Status
                Engchange.EnquiryChange(eqsid, "OFR", System.DateTime.Today, 6, 6, True)

                Popup_Message = "Record saved successfully"
                Popup_Message_Status = WarningType.Success
                lbtnOfferLetter.Visible = False
                HFSTATUS.Value = Encr_decrData.Encrypt(StatusMode_New)
                Showand_hide_BtnSTATUS(StatusMode_New)
                ScriptManager.RegisterStartupScript(Me, Page.GetType, "ScriptREG", "progress_bar_active('" + StatusMode_New + "');", True)
            Else
                Popup_Message = "Request could not be processed"
                Popup_Message_Status = WarningType.Danger
            End If
            Details_holder.Visible = True
            OfferLetter_Holder.Visible = False

        Catch ex As Exception
            Popup_Message = "Request could not be processed"
            Popup_Message_Status = WarningType.Danger
            Details_holder.Visible = True
            OfferLetter_Holder.Visible = False
        Finally
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
        End Try

    End Sub
    Protected Sub btnOfferLetterCancel_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        OfferLetter_Holder.Visible = False

    End Sub
#End Region

#Region "Offer Letter Print"
    Protected Sub lbtnOfferLetter_Print_Click(sender As Object, e As EventArgs)
        VisibleFalseAll()
        Details_holder.Visible = False
        Offer_Letter_Printing_Holder.Visible = True
        SchoolHead = GetSchoolHead()
        Try
            Dim Encr_decrData As New Encryption64()
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim str_query As String = ""
            Dim email As String = ""
            Dim prefer As String = ""
            Dim StrenqApplNo As String = ""

            StrenqApplNo = Session("enqApplNo")
            str_query = "SELECT EQS_ID FROM  ENQUIRY_SCHOOLPRIO_S WHERE EQS_APPLNO='" + StrenqApplNo + "' AND EQS_BSU_ID=" & Session("sBsuid") & ""
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                HiddenEQSID.Value = ds.Tables(0).Rows(0).Item("EQS_ID")
                BindOfferType()
                Offer_letter_Data()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Sub BindOfferType()

        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
            pParms(2) = New SqlClient.SqlParameter("@USER_ID", Session("sUsr_name"))
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetBSU_OfferLetters", pParms)

            ddlOFFR_TYPE.DataSource = ds
            ddlOFFR_TYPE.DataTextField = "BSO_TITLE"
            ddlOFFR_TYPE.DataValueField = "BSO_TITLE"
            ddlOFFR_TYPE.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub Offer_letter_Data()
        Try
            Using readerOFFER_LETTER As SqlDataReader = GetOffer_DOJDATA()

                If readerOFFER_LETTER.HasRows = True Then
                    While readerOFFER_LETTER.Read
                        txtDojOfferLetterPrint.Text = readerOFFER_LETTER("EQS_OFR_DOJ")
                        txtLDate.Text = readerOFFER_LETTER("EQS_OFR_LDATE")
                        txtNOTE.Text = readerOFFER_LETTER("EQS_EXTRADOC")
                    End While
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Private Function GetOffer_DOJDATA() As SqlDataReader

        Dim sqlOffer_LetterDATA As String = " Select replace(CONVERT(varchar,EQS_OFR_DOJ,106),' ','/') as EQS_OFR_DOJ,replace(CONVERT(varchar,EQS_OFR_LDATE,106),' ','/') as EQS_OFR_LDATE,EQS_EXTRADOC  From Enquiry_SchoolPrio_S WHERE EQS_ID=" & HiddenEQSID.Value & "  " &
"  AND EQS_BSU_ID='" & Session("sBsuid") & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlOffer_LetterDATA, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Protected Sub btnPreviewOfferLetter_Click(sender As Object, e As EventArgs)
        DIV_OfferLetter.InnerText = ""
        If CheckValid_DOJDate() = False Then
            lblError_OfferLetterPrint.Text = "Date of Join should be within the academic year"
            Exit Sub
        End If


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(13) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_BSU_ID", Session("sBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@EQS_ID", HiddenEQSID.Value)
        pParms(2) = New SqlClient.SqlParameter("@EQS_OFR_DOJ", txtDojOfferLetterPrint.Text)
        pParms(3) = New SqlClient.SqlParameter("@EQS_EXTRADOC", Replace(txtNOTE.Text, "'", ""))
        pParms(4) = New SqlClient.SqlParameter("@EQS_OFR_LDATE", txtLDate.Text)
        pParms(5) = New SqlClient.SqlParameter("@EQS_USR", Session("sUsr_name"))
        pParms(6) = New SqlClient.SqlParameter("@EQS_OFR_TYPE", ddlOFFR_TYPE.SelectedItem.Value)



        pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "STU.SaveBSU_OFFER_LETTER_EXTRA", pParms)
        PrintOfferLetter("PreviewHTML")
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "ScripOFLP", "OfferLetterPriview();", True)

    End Sub
    Protected Sub btnOfferLetterToPDF_Click(sender As Object, e As EventArgs)
        lblError_OfferLetterPrint.Text = ""

        'If Date.Parse(txtDojOfferLetterPrint.Text) < Now.Date Then
        '    lblError.Text = "Date of Join should not be less than current date"
        '    Exit Sub    
        'End If
        DIV_OfferLetter.InnerText = ""
        If CheckValid_DOJDate() = False Then
            lblError_OfferLetterPrint.Text = "Date of Join should be within the academic year"
            Exit Sub
        End If


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(13) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_BSU_ID", Session("sBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@EQS_ID", HiddenEQSID.Value)
        pParms(2) = New SqlClient.SqlParameter("@EQS_OFR_DOJ", txtDojOfferLetterPrint.Text)
        pParms(3) = New SqlClient.SqlParameter("@EQS_EXTRADOC", Replace(txtNOTE.Text, "'", ""))
        pParms(4) = New SqlClient.SqlParameter("@EQS_OFR_LDATE", txtLDate.Text)
        pParms(5) = New SqlClient.SqlParameter("@EQS_USR", Session("sUsr_name"))
        pParms(6) = New SqlClient.SqlParameter("@EQS_OFR_TYPE", ddlOFFR_TYPE.SelectedItem.Value)



        pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "STU.SaveBSU_OFFER_LETTER_EXTRA", pParms)

        PrintOfferLetter("PreviewPDF")
    End Sub
    Protected Sub btnEmailOfferLetter_Click(sender As Object, e As EventArgs)
        lblError_OfferLetterPrint.Text = ""
        lblError_OfferLetterPrint.Text = ""
        'If Date.Parse(txtDojOfferLetterPrint.Text) < Now.Date Then
        '    lblError.Text = "Date of Join should not be less than current date"
        '    Exit Sub
        'End If

        DIV_OfferLetter.InnerText = ""
        If CheckValid_DOJDate() = False Then
            lblError_OfferLetterPrint.Text = "Date of Join should be within the academic year"
            Exit Sub
        End If


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(13) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_BSU_ID", Session("sBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@EQS_ID", HiddenEQSID.Value)
        pParms(2) = New SqlClient.SqlParameter("@EQS_OFR_DOJ", txtDojOfferLetterPrint.Text)
        pParms(3) = New SqlClient.SqlParameter("@EQS_EXTRADOC", Replace(txtNOTE.Text, "'", ""))
        pParms(4) = New SqlClient.SqlParameter("@EQS_OFR_LDATE", txtLDate.Text)
        pParms(5) = New SqlClient.SqlParameter("@EQS_USR", Session("sUsr_name"))
        pParms(6) = New SqlClient.SqlParameter("@EQS_OFR_TYPE", ddlOFFR_TYPE.SelectedItem.Value)



        pParms(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "STU.SaveBSU_OFFER_LETTER_EXTRA", pParms)


        PrintOfferLetter("email")
    End Sub
#Region "Convert to PDF and send mail"
    Function CheckValid_DOJDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "Select Count(ACD_ID) from Enquiry_SchoolPrio_S A INNER JOIN AcademicYear_D B ON EQS_ACD_ID=ACD_ID WHERE EQS_ID=" & HiddenEQSID.Value & " AND '" & txtDojOfferLetterPrint.Text & "'  BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError_OfferLetterPrint.Text = "The service start date should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function

    Public Sub PrintOfferLetter(ByVal DisplayType As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = String.Empty
            Dim s As String = String.Empty
            If DisplayType = "PreviewHTML" Then
                s = GetSchoolHead()
            Else
                s = GetSchoolHead()
            End If

            Dim tnc As String = String.Empty
            Dim RegistrarEmail As String = String.Empty
            Dim savedPath As String = String.Empty

            Dim sEqsid As String = ""
            sEqsid = HiddenEQSID.Value
            'Session("EQSID") = "383559"
            'Session("sBSUID") = "123004"
            Dim stu_no As String = String.Empty
            Dim MailContent As New StringBuilder
            Dim parentName As String = String.Empty
            Dim LOG_USERNAME As String = String.Empty
            Dim LOG_PASSWORD As String = String.Empty
            Dim LOG_HOST As String = String.Empty
            Dim LOG_PORT As String = String.Empty
            Dim RegistrarName As String = String.Empty
            Dim Parentemail As String = String.Empty
            Dim Email_Subject As String = String.Empty
            Dim EMAILSTATUS As String = String.Empty
            Dim OnBehalf As String

            OnBehalf = "On Behalf of Principal/CEO"

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
            pParms(1) = New SqlClient.SqlParameter("@ROLE", "STAFF")
            pParms(2) = New SqlClient.SqlParameter("@EQS_ID", sEqsid)
            pParms(3) = New SqlClient.SqlParameter("@OFR_TYPE", ddlOFFR_TYPE.SelectedItem.Value)

            Using readerData As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GET_BSU_OFFER_PRINT_DETAILS", pParms)
                While readerData.Read
                    s += Convert.ToString(readerData("BSU_STUD_OFFRLETTER"))
                    tnc = Convert.ToString(readerData("BSU_OFFER_TNC"))
                    RegistrarEmail = Convert.ToString(readerData("BSU_OFR_REGISTRAR_EMAIL"))
                    s = s.Replace("##DATE##", Convert.ToString(readerData("CURRENTDATE")))
                    s = s.Replace("##AcademicYear##", Convert.ToString(readerData("AcademicYear")))
                    s = s.Replace("##PreviousAcademicYear##", Convert.ToString(readerData("previous_AcademicYear")))
                    s = s.Replace("##REGISTRAREMAIL##", RegistrarEmail)
                    s = s.Replace("##STUDENTNAME##", Convert.ToString(readerData("StudentName")))
                    s = s.Replace("##STUDENTFEEID##", Convert.ToString(readerData("EQS_STU_NO")))
                    tnc = tnc.Replace("##STUDENTNAME##", Convert.ToString(readerData("StudentName")))
                    tnc = tnc.Replace("##DOB##", Convert.ToString(readerData("EQM_APPLDOB")))
                    s = s.Replace("##GRADE##", Convert.ToString(readerData("Grade")))
                    s = s.Replace("##REGISTRAR##", Convert.ToString(readerData("RegistrarName")))
                    s = s.Replace("##SCHOOLNAME##", Convert.ToString(readerData("School")))
                    tnc = tnc.Replace("##SCHOOLNAME##", Convert.ToString(readerData("School")))
                    s = s.Replace("##Designation##", Convert.ToString(readerData("Designation")))
                    s = s.Replace("##Telephone##", Convert.ToString(readerData("Telephone")))
                    s = s.Replace("##Emirate##", Convert.ToString(readerData("Emirate")))
                    s = s.Replace("##PARENTNAME##", Convert.ToString(readerData("ParentName")))
                    tnc = tnc.Replace("##PARENTNAME##", Convert.ToString(readerData("ParentName")))
                    s = s.Replace("##STARTDATE##", Convert.ToString(readerData("EQS_OFR_DOJ")))
                    s = s.Replace("##EXTRADOCS##", Convert.ToString(readerData("EQS_EXTRADOC")))
                    s = s.Replace("##RETDATE##", Convert.ToString(readerData("RETDATE")))
                    s = s.Replace("##AMOUNT##", Convert.ToString(readerData("ADMIN_AMT")))
                    s = s.Replace("width=""90%""", " ").Replace("<br>                          </br>                          <br>                          </br>                          <br>", " ")

                    Parentemail = Convert.ToString(readerData("ParentEmail"))
                    Email_Subject = Convert.ToString(readerData("School")) + " - Offer Letter  " + Convert.ToString(readerData("Grade")) + " " + Convert.ToString(readerData("StudentName"))
                    Session("Offer_letter_BSU_NAME") = Convert.ToString(readerData("BSU_NAME"))
                    Session("Offer_letter_BSU_LOGO") = Convert.ToString(readerData("BSU_MOE_LOGO"))
                    'stu_no = Convert.ToString(readerData("EQS_STU_NO"))
                    stu_no = HiddenEQSID.Value
                    LOG_USERNAME = Convert.ToString(readerData("LOG_USERNAME"))
                    LOG_PASSWORD = Convert.ToString(readerData("LOG_PASSWORD"))
                    LOG_HOST = Convert.ToString(readerData("LOG_HOST"))
                    LOG_PORT = Convert.ToString(readerData("LOG_PORT"))
                    parentName = Convert.ToString(readerData("ParentName"))
                    RegistrarName = Convert.ToString(readerData("RegistrarName"))
                    OnBehalf = Convert.ToString(readerData("OnBehalf"))

                End While
            End Using

            If s <> "" Then

                If DisplayType = "PreviewHTML" Then
                    HTML_Preview(s, tnc)
                ElseIf DisplayType = "PreviewPDF" Then
                    Html2PdfPreview2(s + "<div style='page-break-after:always;'></div>" + tnc, stu_no)
                    'HTML2PDF_Preview(s, tnc, stu_no)
                Else


                    HTML2PDF_Email(s, tnc, stu_no, savedPath)

                    MailContent.Append("<table border='0' style=' font-family: Verdana;font-size: 12pt;color: #000000;'>")
                    MailContent.Append("<tr><td  >Dear Parent(s) </td></tr>")
                    MailContent.Append("<tr><td ><br />Please find enclosed an Offer Letter from our Principal.<br /><br /><br /></td></tr>")
                    MailContent.Append("<tr><td >Yours sincerely, </td></tr>")
                    MailContent.Append("<tr><td><br /><br /></td></tr>")
                    MailContent.Append("<tr><td >" & RegistrarName & "   (" & RegistrarEmail & ") </td></tr>")
                    MailContent.Append("<tr><td><br /><br /></td></tr>")
                    MailContent.Append("<tr><td >" & OnBehalf & " </td></tr>")

                    MailContent.Append("</table>")

                    'SendPDFEmails(FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String

                    EMAILSTATUS = SendPDFEmails(RegistrarEmail, Parentemail, Email_Subject, MailContent.ToString, LOG_USERNAME, LOG_PASSWORD, LOG_HOST, LOG_PORT, savedPath, s + "<div style='page-break-after:always;'></div>" + tnc, True)


                    OFFER_LETTER_PDF_LOG(Parentemail, EMAILSTATUS)
                    If File.Exists(savedPath) Then
                        File.Delete(savedPath)
                    End If

                    If EMAILSTATUS.Contains("Successfully") Then
                        lblError_OfferLetterPrint.Text = "Offer Letter succesfully emailed to " + Parentemail
                    Else
                        lblError_OfferLetterPrint.Text = "Error while sending email attachment to " + Parentemail
                    End If

                End If
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub HTML2PDF_Email(ByVal HTMLStr As String, ByVal HTMLTERM As String, ByVal stu_no As String, ByRef savedPath As String)
        Try
            Dim ATT_HTML As String = HTMLStr
            If HTMLTERM.Trim <> "" Then
                ATT_HTML += "<div style='page-break-after:always;'></div>"
                ATT_HTML += HTMLTERM

            End If
            Dim pdfBytes = ConvertHtmlToPDF(ATT_HTML)
            Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("OfferLetterFilepath").ToString()


            Dim pathSave As String = "Offer Letter-" & stu_no & ".pdf"
            If Not Directory.Exists(PhyPath & "\" & Session("sBSUID") & "\") Then
                ' Create the directory.
                Directory.CreateDirectory(PhyPath & "\" & Session("sBSUID") & "\")
            End If

            Dim path = PhyPath & "\" & Session("sBSUID") & "\" & pathSave
            System.IO.File.WriteAllBytes(path, pdfBytes)

            'Dim filename = "Question.pdf"

            'Dim tempFolder = System.IO.Path.GetTempPath()
            'filename = System.IO.Path.Combine(tempFolder, filename)

            'System.Diagnostics.Process.Start(filename)
            'Using myMemoryStream As New MemoryStream()
            '    Dim page As PdfPage
            '    Dim document As New Document(iTextSharp.text.PageSize.A4, 10, 10, 10, 10)
            '    Dim myPDFWriter As PdfWriter = PdfWriter.GetInstance(document, myMemoryStream)
            '    Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("OfferLetterFilepath").ToString()


            '    Dim pathSave As String = "Offer Letter-" & stu_no & ".pdf"
            '    If Not Directory.Exists(PhyPath & "\" & Session("sBSUID") & "\") Then
            '        ' Create the directory.
            '        Directory.CreateDirectory(PhyPath & "\" & Session("sBSUID") & "\")
            '    End If

            '    Dim path = PhyPath & "\" & Session("sBSUID") & "\" & pathSave
            '    myPDFWriter.PageEvent = page
            '    document.Open()

            '    ' Add to content to your PDF here...

            '    Dim htmlarraylist = HTMLWorker.ParseToList(New StringReader(HTMLStr), Nothing)
            '    For k As Integer = 0 To htmlarraylist.Count - 1
            '        document.Add(DirectCast(htmlarraylist(k), IElement))
            '    Next
            '    If HTMLTERM.Trim <> "" Then
            '        HTMLTERM = "<div style='page-break-after:always;'></div>" & HTMLTERM

            '        Dim htmlarraylist_terms = HTMLWorker.ParseToList(New StringReader(HTMLTERM), Nothing)
            '        For i As Integer = 0 To htmlarraylist_terms.Count - 1
            '            document.Add(DirectCast(htmlarraylist_terms(i), IElement))

            '        Next
            '    End If



            '    ' We're done adding stuff to our PDF.
            '    document.Close()

            '    Dim content As Byte() = myMemoryStream.ToArray()

            '    ' Write out PDF from memory stream.
            '    Using fs As FileStream = File.Create(path)
            '        fs.Write(content, 0, CInt(content.Length))
            '    End Using

            '    savedPath = path
            'End Using
            savedPath = path

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub HTML2PDF_Preview(ByVal HTMLStr As String, ByVal HTMLTERM As String, ByVal stu_no As String)
        Try
            Dim document As New Document(iTextSharp.text.PageSize.A4, 10, 10, 10, 10)
            Dim output = New MemoryStream()
            'Change the content type to PDF
            HttpContext.Current.Response.ContentType = "application/pdf"
            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'create an instance of your PDFpage class. This is the class we generated above.
            Dim page As PdfPage

            'create an instance of the PdfWriter and write to the Response.OutputStream.  directly to the browser
            Dim pdfWriter1 As PdfWriter = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream)

            'set the PageEvent of the pdfWriter instance to the instance of our PDFPage class
            pdfWriter1.PageEvent = page

            'open the document
            document.Open()
            Dim css As New StyleSheet()
            css.LoadTagStyle("HtmlTags.DIV", "HtmlTags.BORDER", "1")




            Dim htmlarraylist = HTMLWorker.ParseToList(New StringReader(HTMLStr), Nothing)
            For k As Integer = 0 To htmlarraylist.Count - 1
                document.Add(DirectCast(htmlarraylist(k), IElement))
            Next
            If HTMLTERM.Trim <> "" Then
                HTMLTERM = "<div style='page-break-after:always;'></div>" & HTMLTERM

                Dim htmlarraylist_terms = HTMLWorker.ParseToList(New StringReader(HTMLTERM), Nothing)
                For i As Integer = 0 To htmlarraylist_terms.Count - 1
                    document.Add(DirectCast(htmlarraylist_terms(i), IElement))

                Next
            End If

            document.Close()
            'Dim bytearray() As Byte
            'bytearray = output.ToArray

            HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename=Offer Letter-{0}.pdf", stu_no))
            HttpContext.Current.Response.BinaryWrite(output.ToArray)
            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.Close()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function SendPDFEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal ATT_HTML As String, ByVal HasAttachments As Boolean) As String
        Dim ReturnValue As String = ""
        Dim client As New System.Net.Mail.SmtpClient(Host, Port)
        Dim msg As New System.Net.Mail.MailMessage
        Dim mailfrom As New System.Net.Mail.MailAddress(FromEmailId, "Registrar")

        Try

            msg.From = mailfrom
            msg.To.Add(ToEmailId)

            If chkEmailCopy.Checked = True Then
                msg.Bcc.Add(FromEmailId)
            End If

            msg.Subject = Subject
            msg.Body = MailBody
            msg.Priority = Net.Mail.MailPriority.High
            msg.IsBodyHtml = True
            '' If Attachments.
            If HasAttachments Then
                'Dim pdfBytes = ConvertHtmlToPDF(ATT_HTML)
                'Dim Stream = New System.IO.MemoryStream(pdfBytes)
                'msg.Attachments.Add(New Attachment(Stream, "invoice.pdf"))
                Dim attach As New System.Net.Mail.Attachment(Templateid)
                msg.Attachments.Add(attach)

                'If Session("sBSUID") = "900201" Then
                '    Dim attach2 As New System.Net.Mail.Attachment("\\172.16.1.11\oasisphotos\OFFERLETTER\Extras\900201\PEI Student Contract.pdf")
                '    msg.Attachments.Add(attach2)
                'End If

            End If

            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            client.Send(msg)
            client = Nothing
            msg.Dispose()
            ReturnValue = "Successfully sent to " & ToEmailId
        Catch ex As Exception
            ReturnValue = "Error in Sending Mail :" & Date.Today.ToString & " " & ex.Message
            client = Nothing
            msg.Dispose()
        End Try
        Return ReturnValue
    End Function
    Private Sub OFFER_LETTER_PDF_LOG(ByVal PARENT_EMAIL As String, ByVal STATUS As String)
        Try


            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OL_BSU_ID", Session("sBSUID"))
            pParms(1) = New SqlClient.SqlParameter("@OL_EQS_ID", HiddenEQSID.Value)
            pParms(2) = New SqlClient.SqlParameter("@OL_STATUS", STATUS)
            pParms(3) = New SqlClient.SqlParameter("@OL_EMAIL_PAR", PARENT_EMAIL)
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "SAVEOFFER_LETTER_EMAIL_LOG", pParms)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub HTML_Preview(ByVal HTMLStr As String, ByVal HTMLTERM As String)
        DIV_OfferLetter.InnerHtml = HTMLStr
        DIV_OfferLetter.Visible = True
        DIV_TermsAndConditions.InnerHtml = HTMLTERM
        DIV_TermsAndConditions.Visible = True
    End Sub
    Private Sub Html2PdfPreview2(ByVal htmlContent As String, ByVal fileName As String, Optional ByVal type As String = "Normal")
        Try
            Dim date_pdf As String = DateTime.Now.ToString("dd-MM-yy-hh-mm-ss")
            Dim Contract_name As String = fileName

            WriteDocument(Contract_name + "-" + date_pdf + ".pdf", "application/pdf", ConvertHtmlToPDF(htmlContent, type))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function ConvertHtmlToPDF(htmlContent As String, Optional ByVal type As String = "Normal") As Byte()
        Try


            Dim nRecohtmltoPdfObj As HtmlToPdfConverter = New HtmlToPdfConverter()
            nRecohtmltoPdfObj.Orientation = PageOrientation.Portrait
            nRecohtmltoPdfObj.PageWidth = 210
            'nRecohtmltoPdfObj.PageHeight = IsNothing()
            nRecohtmltoPdfObj.Size = NReco.PdfGenerator.PageSize.A4

            'nRecohtmltoPdfObj.Margins = New PageMargins { Top = 2, Bottom = 14, Left = 8, Right = 8 }
            nRecohtmltoPdfObj.Margins = New PageMargins With {
                .Top = 10,
                .Bottom = 14,
                .Left = 8,
                .Right = 8
            }

            nRecohtmltoPdfObj.PageFooterHtml = ""
            nRecohtmltoPdfObj.PageHeaderHtml = ""

            'nRecohtmltoPdfObj.CustomWkHtmlArgs = "--margin-top 25 --header-spacing 25 --margin-left 5 --margin-right 5";
            'return nRecohtmltoPdfObj.GeneratePdfFromFile(url, null);
            If type = "Register Slip" Then
                Return nRecohtmltoPdfObj.GeneratePdf(CreatePDFScript_reg() + htmlContent + "</body></html>")
            Else
                Return nRecohtmltoPdfObj.GeneratePdf(CreatePDFScript() + htmlContent + "</body></html>")
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "PDFWriteNrecoEnq on GeneratePdf")
        End Try
    End Function
    Private Function CreatePDFScript() As String
        Return "<html lang=""en"" dir=""ltr""><head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8""/>" &
             "<link href='https://fonts.googleapis.com/css?family=Open+Sans&display=swap' rel='stylesheet'>" &
            "<style> table tr td { font-family: 'Open Sans', sans-serif !important;font-size: 22px !important; line-height: 25px !important;}</style>" &
            "</head><body style=""background-color: #fff; "" >"
    End Function
    Private Function CreatePDFScript_reg() As String
        Return "<html lang=""en"" dir=""ltr""><head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8""/>" &
             "<link href='https://fonts.googleapis.com/css?family=Open+Sans&display=swap' rel='stylesheet'>" &
              "</head><body  >"
    End Function
    Private Sub WriteDocument(ByVal fileName As String, ByVal contentType As String, ByVal content As Byte())
        Try


            HttpContext.Current.Response.Clear()
            Response.ContentType = contentType
            Response.AddHeader("content-disposition", "attachment; filename=" & fileName)
            Response.CacheControl = "No-cache"
            Response.BinaryWrite(content)
            Response.Flush()
            Response.SuppressContent = True
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "PDFWriteNrecoEnq on WriteDocument")
        End Try
    End Sub
#End Region

    Protected Sub btnCancelPrintOfferLetter_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        Offer_Letter_Printing_Holder.Visible = False
        DIV_OfferLetter.InnerHtml = ""
        DIV_OfferLetter.Visible = False
        DIV_TermsAndConditions.InnerHtml = ""
        DIV_TermsAndConditions.Visible = False
    End Sub
#End Region

#Region "Enrollment"
    Protected Sub lbtnEnrollment_Click(sender As Object, e As EventArgs)
        Allow_Permissions = GetAllow_Permissions(btnSaveEnroll)
        VisibleFalseAll()
        Details_holder.Visible = False
        Enrollment_Holder.Visible = True
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = ""
        If Session("BSU_BFlexiblePlan") = "False" Then
            Payment_Plan_DIV.Visible = False
        Else
            Payment_Plan_DIV.Visible = True
        End If
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
        GetPayPlan()
        txtDoj.Attributes.Add("OnTextChanged", "MoveText();")
        GetTransfer()
    End Sub
    Private Sub GetPayPlan()
        Dim li As New System.Web.UI.WebControls.ListItem
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT     FEES.SCHEDULE_M.SCH_ID, FEES.SCHEDULE_M.SCH_DESCR FROM         FEES.SCHEDULE_M INNER JOIN                       FEES.FEESETUP_M ON FEES.SCHEDULE_M.SCH_ID = FEES.FEESETUP_M.FSM_Collection_SCH_ID                       where FsM_FEE_ID=5 and FsM_ACD_ID= '" & ddlAcademicYear.SelectedItem.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlPayPlan.DataSource = ds
        ddlPayPlan.DataTextField = "SCH_DESCR"
        ddlPayPlan.DataValueField = "SCH_ID"
        ddlPayPlan.DataBind()


        '---- Get the default payment plan
        Using PayPlanReader As SqlDataReader = GetDefaultPayPlan(ddlAcademicYear.SelectedItem.Value)
            While PayPlanReader.Read
                If Convert.ToString(PayPlanReader("DefaultPlan")) = 1 Then
                    li.Text = Convert.ToString(PayPlanReader("SCH_DESCR"))
                    li.Value = Convert.ToString(PayPlanReader("SCH_ID"))
                    ddlPayPlan.Items(ddlPayPlan.Items.IndexOf(li)).Selected = True
                End If
            End While
        End Using

    End Sub
    Function GetDefaultPayPlan(ByVal ACD_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "OASIS_FEES.FEES.PaymentPlan", pParms)
        Return reader
    End Function
    Sub GetTransfer()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT Top 1 '' as TFR_CODE,'' as TFR_DESCR From TFRTYPE_M WHERE 1=1 UNION ALL SELECT TFR_CODE, TFR_DESCR FROM TFRTYPE_M order by TFR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If Session("BSU_COUNTRY_ID") = "172" Then
            ddlTranType.DataSource = ds
            ddlTranType.DataValueField = "TFR_CODE"
            ddlTranType.DataTextField = "TFR_DESCR"
            ddlTranType.DataBind()
        Else
            Dim table_32 As DataTable = ds.Tables(0)
            ddlTranType.Items.Clear()
            For i = 0 To table_32.Rows.Count() - 1
                Dim name As String = table_32.Rows(i).Item("TFR_DESCR").ToString()
                Dim id As String = table_32.Rows(i).Item("TFR_CODE").ToString()
                If id = "E" Then
                    name = "OTHER CITY"
                End If
                Dim newListItem As System.Web.UI.WebControls.ListItem
                newListItem = New System.Web.UI.WebControls.ListItem(name, id)
                ddlTranType.Items.Add(newListItem)

            Next
        End If



    End Sub
    Protected Sub btnSaveEnroll_Click(sender As Object, e As EventArgs)
        Try
            Dim StatusMode_New As String = "ENR"
            Dim EQS_STATUS As String = Encr_decrData.Decrypt(HFSTATUS.Value)
            If EQS_STATUS = "OFR" Then
                If CheckDOJ() = False Then
                    btnSaveEnroll.Enabled = True
                    Popup_Message = lblError_Enrollment.Text
                    Popup_Message_Status = WarningType.Danger
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                    Exit Sub
                End If

                If ddlTranType.SelectedItem.Value = "" Then
                    lblError_Enrollment.Text = "Please Select Transfer Type"
                    Popup_Message = lblError_Enrollment.Text
                    Popup_Message_Status = WarningType.Danger
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                    btnSaveEnroll.Enabled = True
                    Exit Sub
                End If

                If studClass.checkFeeClosingDate(Session("sbsuid"), ddlAcademicYear.SelectedValue, Date.Parse(txtDoj.Text)) = False Then
                    lblError_Enrollment.Text = "Date of join has to be a date higher than the fee closing date"
                    Popup_Message = lblError_Enrollment.Text
                    Popup_Message_Status = WarningType.Danger
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                    Exit Sub
                End If


                If SaveData() = True Then
                    If lblError_Enrollment.Text = "success" Then
                        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                        Popup_Message = "Record saved successfully"
                        Popup_Message_Status = WarningType.Success
                        lbtnEnrollment.Enabled = False
                        HFSTATUS.Value = Encr_decrData.Encrypt(StatusMode_New)
                        Showand_hide_BtnSTATUS(StatusMode_New)
                        ScriptManager.RegisterStartupScript(Me, Page.GetType, "ScriptOPN", "progress_bar_active('" + StatusMode_New + "');", True)
                        'Dim url As String = String.Format("~\Students\studPrintEnrollmentSlip2.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
                        'Response.Redirect(url)
                        'Details_holder.Visible = True
                        'Shortlist_Holder.Visible = False
                    Else
                        Popup_Message = lblError_Enrollment.Text
                        Popup_Message_Status = WarningType.Danger
                    End If
                Else
                    If String.IsNullOrEmpty(lblError_Enrollment.Text) Then
                        Popup_Message = "Request could not be processed"
                    Else
                        Popup_Message = lblError_Enrollment.Text
                    End If

                    Popup_Message_Status = WarningType.Danger
                End If

                usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)

            Else
                Popup_Message = "Request could not be processed"
                Popup_Message_Status = WarningType.Danger
                usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                Details_holder.Visible = True
                Shortlist_Holder.Visible = False
            End If
        Catch ex As Exception
            'lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Popup_Message = "Request could not be processed"
            Popup_Message_Status = WarningType.Danger
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            Details_holder.Visible = True
            Shortlist_Holder.Visible = False
        Finally

        End Try

    End Sub
    Function CheckDOJ() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND '" + txtDoj.Text + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError_Enrollment.Text = "The date of join should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function
    Function SaveData() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim ids As String()
        ids = GetEQSIDS.Split("|")
        Dim XMlString As String
        Dim applNos As String

        XMlString = ids(0)
        applNos = ids(1)

        Dim stuIds As String = ""
        Dim eqsIds As String = ""

        If XMlString = "" And applNos = "" Then
            lblError_Enrollment.Text = "No records selected"
            Return False
        ElseIf XMlString <> "" Then
            If AuditData() = True Then
                str_query = "exec studENROLL '" + XMlString + "','" + Format(Date.Parse(txtDoj.Text), "yyyy-MM-dd") + "','" + Format(Date.Parse(txtMDoj.Text), "yyyy-MM-dd") + "','" + ddlTranType.SelectedValue + "','" + Session("sbsuid") + "','" + Session("sUsr_name") + "','" + ddlPayPlan.SelectedValue + "','" + ddlJoinResult.SelectedItem.Value + "'"
                ' SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                Try
                    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
                    While reader.Read
                        Try
                            'HFstuIds.Value = reader.GetString(0)
                            ' eqsIds = reader.GetString(1)
                        Catch ex As Exception
                        End Try
                    End While
                    reader.Close()
                Catch ex As Exception
                    If ex.Message = "Error in DOJ" Then
                        lblError_Enrollment.Text = "Date of join must be bigger than  today date "
                        Return False
                    End If
                End Try

            Else
                lblError_Enrollment.Text = "Record coud not be saved"
                Return False
            End If
        End If

        If applNos <> "" Then
            Session("enrError") = "The following records  " + applNos + " were not updated,Since the date of join has to be higher or equal that the registration date "
            lblError_Enrollment.Text = "The following records  " + applNos + " were not updated,Since the date of join has to be higher or equal that the registration date "
        Else
            lblError_Enrollment.Text = "success"
        End If
        Return True


    End Function

    Function GetEQSIDS() As String


        Dim str As String = ""
        Dim str1 As String = ""
        Dim enqIds As String = ""
        If Date.Parse(HFREGNDATE.Value) <= Date.Parse(txtDoj.Text) Then
            str += "<ID><EQS_ID>" + HiddenEQSID.Value + "</EQS_ID><ENQ_ID>" + hfENQ_ID.Value + "</ENQ_ID></ID>"
            If enqIds <> "" Then
                enqIds += ","
            End If
            enqIds += hfENQ_ID.Value
        Else

            ' if the date of join is less than the registration date then it has to be validated
            If str1 <> "" Then
                str1 += ","
            End If

            str1 += ltAppNo.Text
        End If
        Session("ENQIDS") = enqIds
        If str <> "" Then
            Return "<IDS>" + str + "</IDS>" + "|" + str1
        Else
            Return "|" + str1
        End If
    End Function

    Function AuditData() As Boolean

        Dim str As String = ""

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try
                transaction = conn.BeginTransaction("SampleTransaction")
                UtilityObj.InsertAuditdetails(transaction, "edit", "ENQUIRY_SCHOOLPRIO_S", "EQS_ID", "EQS_EQM_ENQID", "EQS_ID=" + HiddenEQSID.Value)
                transaction.Commit()
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function
    Protected Sub btnCancelEnroll_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        Enrollment_Holder.Visible = False
    End Sub
#End Region

#Region "Follow Up"
    Protected Sub lbtnFollowUp_Click(sender As Object, e As EventArgs)
        VisibleFalseAll()
        Details_holder.Visible = False
        FollowUp_Holder.Visible = True
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Check_Alert", "Check_Alert();", True)
        'Dim EQS_STATUS As String = Encr_decrData.Decrypt(HFSTATUS.Value)
        'HFSTATUS.Value = Encr_decrData.Encrypt(EQS_STATUS)

        BindEnqDetails()
        'BindOtherDetails()
        BindGrid()
        bind_cur_status()
        bind_reason()
        bind_gridReason()
    End Sub
    Public Sub BindEnqDetails()
        Dim Encr_decrData As New Encryption64()
        'Dim editstring As String() = Encr_decrData.Decrypt(Request.QueryString("editstring").Replace(" ", "+")).Split("|")
        txtAcadYear.Text = ltAcdYear.Text
        txtGrade.Text = ltGrade.Text
        txtShift.Text = ltShift.Text
        txtStream.Text = ltStream.Text

        txtEnqNo.Text = ltAppNo.Text
        txtenqdate.Text = ltEnqDate.Text
        txtappname.Text = ltName.Text
        If ltPri_cont.Text = "Father" Then
            txtemail.Text = ltFemail.Text
            txtmobile.Text = ltFmobile.Text
            txtprimarycaontact.Text = ltFather_name.Text
        ElseIf ltPri_cont.Text = "Mother" Then
            txtemail.Text = ltMemail.Text
            txtmobile.Text = ltMmobile.Text
            txtprimarycaontact.Text = ltMother_name.Text
        Else
            txtemail.Text = ltGemail.Text
            txtmobile.Text = ltGmobile.Text
            txtprimarycaontact.Text = ltGuardian_name.Text
        End If

    End Sub

    Public Sub BindParentDetails(ByVal ds As DataSet, ByVal email As String, ByVal mobile As String, ByVal firstname As String, ByVal lastname As String)
        txtemail.Text = ds.Tables(0).Rows(0).Item(email).ToString()
        txtmobile.Text = ds.Tables(0).Rows(0).Item(mobile).ToString()
        txtprimarycaontact.Text = ds.Tables(0).Rows(0).Item(firstname).ToString() & " " & ds.Tables(0).Rows(0).Item(lastname).ToString()
    End Sub
    Public Sub BindGrid()
        Dim Encr_decrData As New Encryption64
        Dim id = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
        Dim bsu_id = Session("sBsuid").ToString
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select *,(case fol_mode when '1' then 'Telephone' else 'Email' end) as mode_descr ,replace(CONVERT(varchar,ALERT_DATE,106),' ','/') as alertdate from ENQUIRY_FOLLOW_UP " &
                        " where fol_eqs_id='" & id & "' and fol_bsu_id='" & bsu_id & "' and ALERT_DATE is not null ORDER BY FOL_ID DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        grdFollowuphis.DataSource = ds
        grdFollowuphis.EmptyDataText = "There is no tasks"
        grdFollowuphis.DataBind()

    End Sub
    Sub bind_cur_status()
        ddlCurStatus.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFR_ID,EFR_REASON   FROM ENQ_FOLLOW_UP_REASON_M  "



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCurStatus.DataSource = ds
        ddlCurStatus.DataTextField = "EFR_REASON"
        ddlCurStatus.DataValueField = "EFR_ID"
        ddlCurStatus.DataBind()
        ddlCurStatus.Items.Add(New System.Web.UI.WebControls.ListItem("--", "0"))
        ddlCurStatus.Items.FindByText("--").Selected = True
    End Sub
    Sub bind_reason()
        ddlReason.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFD_ID,EFD_REASON  FROM ENQ_FOLLOW_UP_REASON_D where EFD_EFR_ID=" & ddlCurStatus.SelectedValue



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReason.DataSource = ds
        ddlReason.DataTextField = "EFD_REASON"
        ddlReason.DataValueField = "EFD_ID"
        ddlReason.DataBind()

        If (Not ddlReason.Items Is Nothing) AndAlso (ddlReason.Items.Count = 0) Then
            ddlReason.Items.Add(New System.Web.UI.WebControls.ListItem("--", "0"))
            ddlReason.Items.FindByText("--").Selected = True
        End If

    End Sub
    Sub bind_gridReason()
        Dim Encr_decrData As New Encryption64
        Dim id = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
        Dim bsu_id = Session("sBsuid").ToString
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " SELECT  ENR_LOG_DATE,EFR_REASON,ENR_REMARKS,EFD_REASON,ENR_USER_ID   ,(CASE ENR_mode_descr	WHEN '1'	THEN 'Telephone'ELSE 'Email'END) AS mode_descr  FROM ENQUIRY_FOLLOW_UP_REASON " _
                        & " INNER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=ENR_EFR_ID " _
                        & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_D ON EFd_ID=ENR_EFD_ID " _
                        & " WHERE ENR_BSU_ID='" & bsu_id & "' AND ENR_EQS_ID =" & id & " order by enr_id desc"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        grdReason.DataSource = ds
        grdReason.EmptyDataText = "There is no Follow Ups "
        grdReason.DataBind()
    End Sub
    Protected Sub ddlCurStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurStatus.SelectedIndexChanged
        bind_reason()
    End Sub
    Protected Sub lbtnSaveFollowUp_Click(sender As Object, e As EventArgs)
        Try

            Dim str_query As String




            If txtAob.Text.Trim <> "" Then
                Dim strfDate As String = txtAob.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    lblmessage.Text = "Please check the alert date"
                    Exit Sub
                End If
            End If
            If txtReasonRemark.Text.Trim = "" Then
                lblmessage.Text = "Please enter Follow Up Remarks. "
                Exit Sub
            End If
            If txtAob.Text.Trim <> "" Then
                If txtremarks.Text.Trim = "" Then
                    lblmessage.Text = "Please enter Tasks Remarks. "
                    Exit Sub
                End If
            End If
            Dim Encr_decrData As New Encryption64()
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim bsu_id = Session("sBsuid") ''"125016" ''
            Dim eqs_id = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
            Dim enq_id = Encr_decrData.Decrypt(Request.QueryString("enqid").Replace(" ", "+"))
            Dim userid = Session("sUsr_name") ''"admin" ''

            SAVE_REASON(eqs_id, enq_id)
            If ddmode.SelectedValue <> "0" Then
                Dim pParms(17) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@bsu_id", bsu_id)
                pParms(1) = New SqlClient.SqlParameter("@eqs_id", eqs_id)
                pParms(2) = New SqlClient.SqlParameter("@enq_id", enq_id)
                pParms(3) = New SqlClient.SqlParameter("@user_id", userid)
                pParms(4) = New SqlClient.SqlParameter("@mode", Convert.ToInt16(ddmode.SelectedItem.Value))
                pParms(5) = New SqlClient.SqlParameter("@remarks", txtremarks.Text)
                pParms(6) = New SqlClient.SqlParameter("@alertdays", "1")
                pParms(11) = New SqlClient.SqlParameter("@ALERT_DATE", txtAob.Text)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "stu_enq_insert_followup", pParms)



                If txtAob.Text.Trim <> "" Then
                    Dim lstrEMPNAME
                    Dim lstrEMPEMAIL
                    Dim pParmss(3) As SqlClient.SqlParameter
                    pParmss(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
                    pParmss(1) = New SqlClient.SqlParameter("@EQS_ID", eqs_id)

                    Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetEMP_Name_Email", pParmss)
                        While reader.Read
                            lstrEMPNAME = Convert.ToString(reader("EMP_FNAME"))
                            lstrEMPEMAIL = Convert.ToString(reader("EMD_EMAIL"))
                        End While
                    End Using
                    BindGrid()
                    Dim MSG As String, SUBJECT As String
                    SUBJECT = "PHOENIX ALERT - Enquiry Followup : " + txtEnqNo.Text
                    MSG = "<table width=100% border=0 style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none;  border-bottom-style: none;font-size: 10pt;'>              <tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Enquiry Followup Alert : " + txtEnqNo.Text + "</td></tr> <tr><td>Please accept this email as confirmation for your enquiry follow up with details given below : <br></td></tr>  <tr><td> Enquiry Number: " + txtEnqNo.Text + " <br></td></tr> <tr><td> Academic Year: " + txtAcadYear.Text + " <br></td></tr> <tr><td> Grade: " + txtGrade.Text + " <br></td></tr>     <tr><td> Parent Name: " + txtprimarycaontact.Text + " <br></td></tr>   <tr><td> Mobile: " + txtmobile.Text + " <br></td></tr>    <tr><td> Email: " + txtemail.Text + " <br></td></tr>   <tr><td> Remarks:<br> " + txtremarks.Text + " </td></tr>          </table>                      <table width=100% border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none;  border-bottom-style: none;font-size: 10pt;'>    <tr><td><br><br>This is an Automated mail from GEMS PHOENIX. </td></tr>              <tr></tr><tr></tr>            </table>    "
                    Dim SendAppt As New AppointmentBLL(txtAob.Text + " 08:00AM", txtAob.Text + " 08:00AM", SUBJECT, MSG, "School", lstrEMPNAME, lstrEMPEMAIL, SUBJECT, "communication@gemsedu.com")
                    Try
                        SendAppt.EmailAppointment()
                    Catch ex As Exception

                    End Try

                End If
            End If

            If ((check_isDel(ddlCurStatus.SelectedValue) <> 0) And (txtReasonRemark.Text <> "")) Then

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Confirm_test", "Confirm_test();", True)



                If hdnValue.Value = "Yes" Then
                    str_query = "exec studSaveStudEnqAcceptReject " + eqs_id + ",'DEL',0 "
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If



                '        'Response.Redirect("~/Students/StuFollowUp.aspx?MainMnu_code=T6V+LqgVMFI=&datamode=Zo4HhpVNpXc=")
            End If

            lblmessage.Text = "Saved"
            Popup_Message = "Record saved successfully"
            Popup_Message_Status = WarningType.Success
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirect_test", "redirect_test('" & ViewState("ReferrerUrl") & "');", True)
            '  Response.Redirect("~/Students/StuFollowUp.aspx?MainMnu_code=T6V+LqgVMFI=&datamode=Zo4HhpVNpXc=")
            '  Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            lblmessage.Text = "Error"
            Popup_Message = "Request could not be processed"
            Popup_Message_Status = WarningType.Danger
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
        End Try
    End Sub
    Private Sub SAVE_REASON(ByVal EQSID As Integer, ByVal ENQID As Integer)

        Dim Status As Integer
        Dim param(9) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                param(0) = New SqlParameter("@ENR_BSU_ID", Session("sBsuid"))
                param(1) = New SqlParameter("@ENR_EQS_ID", EQSID)
                param(2) = New SqlClient.SqlParameter("@ENR_ENQ_ID", ENQID)
                param(3) = New SqlClient.SqlParameter("@ENR_EFR_ID", ddlCurStatus.SelectedValue)
                param(4) = New SqlClient.SqlParameter("@ENR_EFD_ID", ddlReason.SelectedValue)
                param(5) = New SqlClient.SqlParameter("@ENR_USER_ID", Session("sUsr_name"))
                param(6) = New SqlClient.SqlParameter("@ENR_REMARKS", txtReasonRemark.Text)
                param(8) = New SqlClient.SqlParameter("@ENR_mode_descr", ddmode.Text)

                param(7) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(7).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVE_ENQ_FOLLOW_UP_REASON", param)
                Status = param(7).Value
            Catch ex As Exception

                lblError.Text = "Record could not be updated"
            Finally
                If Status <> 0 Then
                    UtilityObj.Errorlog(lblError.Text)
                    transaction.Rollback()
                Else
                    lblError.Text = ""
                    transaction.Commit()





                    bind_gridReason()
                End If
            End Try

        End Using
    End Sub
    Function check_isDel(ByVal id As Integer) As Integer
        Dim i As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT isnull(EFR_bDEL,0) EFR_bDEL  FROM ENQ_FOLLOW_UP_REASON_M  where EFR_ID=" & id

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            i = ds.Tables(0).Rows(0).Item("EFR_bDEL")
        End If
        Return i
    End Function
    Protected Sub lbtnCancelFollowUp_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        FollowUp_Holder.Visible = False
    End Sub

#End Region

#Region "Document"
    Protected Sub lbtnDocument_Click(sender As Object, e As EventArgs)
        VisibleFalseAll()
        Details_holder.Visible = False
        Document_Holder.Visible = True
        BindStages()
        RepeaterBind()

    End Sub

    Protected Sub DocumentSave_Click(sender As Object, e As EventArgs)
        Try
            If Not ddlDocument.SelectedValue = "-1" Then
                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
                If FileUpload_stud.HasFile Then

                    Dim serverpath As String = WebConfigurationManager.AppSettings("UploadExcelFile").ToString + "enquiry\"
                    'Dim serverpath As String = Server.MapPath("~/oasisfiles/hr_files/enquiry/")
                    Dim filen As String()
                    Dim FileName As String = FileUpload_stud.PostedFile.FileName
                    filen = FileName.Split("\")
                    FileName = filen(filen.Length - 1)

                    Dim extention = GetExtension(FileName)
                    If extention.ToLower = "jpg" Or extention.ToLower = "jpeg" Or extention.ToLower = "png" Or extention.ToLower = "doc" Or extention.ToLower = "docx" Or extention.ToLower = "pdf" Then
                        Dim pParms(7) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@FILE_NAME", FileName)
                        pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)
                        pParms(2) = New SqlClient.SqlParameter("@EQM_ENQID", hfENQ_ID.Value)
                        pParms(3) = New SqlClient.SqlParameter("@EQM_DOC_ID", ddlDocument.SelectedValue)
                        pParms(4) = New SqlClient.SqlParameter("@Extension", extention)
                        pParms(5) = New SqlClient.SqlParameter("@Serverpath", WebConfigurationManager.AppSettings("UploadExcelFilePathVirtual").ToString + "enquiry/")
                        Dim uploadid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_DELETE_FILE_UPLOAD_MASTER", pParms)
                        FileUpload_stud.SaveAs(serverpath + uploadid + "." + extention)
                        RepeaterBind()
                        Popup_Message = "Record saved successfully"
                        Popup_Message_Status = WarningType.Success
                    Else
                        Popup_Message = "File extention must be <br/> jpg or jpeg or png or doc or docx or pdf"
                        Popup_Message_Status = WarningType.Danger
                    End If
                Else
                    Popup_Message = "You must select Document to upload  "
                    Popup_Message_Status = WarningType.Danger
                End If
            Else
                Popup_Message = "there is no Document assign for that stage "
                Popup_Message_Status = WarningType.Danger
            End If
        Catch ex As Exception
            Popup_Message = "Request could not be processed"
            Popup_Message_Status = WarningType.Danger
        Finally
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
        End Try

    End Sub
    Protected Sub DocumentCancel_Click(sender As Object, e As EventArgs)
        Details_holder.Visible = True
        Document_Holder.Visible = False
    End Sub
    Protected Sub Repeater_Stage_ItemDataBound(sender As Object, e As ListViewItemEventArgs)
        'If e.Item.ItemType = ListItemType.Item Then
        Dim lblStgid As New Label
        lblStgid = e.Item.FindControl("lblStgId")

        Dim Repeater_Stage_Document As New ListView
        Repeater_Stage_Document = e.Item.FindControl("Repeater_Stage_Document")
        BindDocuments(hfENQ_ID.Value, lblStgid.Text, Repeater_Stage_Document)
        'End If
    End Sub
    Protected Sub Repeater_Stage_Document_ItemDataBound(sender As Object, e As ListViewItemEventArgs)
        Dim lblcollected As New Label
        lblcollected = e.Item.FindControl("lblcollected")
        Dim upload_div As New HtmlGenericControl
        upload_div = e.Item.FindControl("upload_div")
        Dim Pending_div As New HtmlGenericControl
        Pending_div = e.Item.FindControl("Pending_div")
        If lblcollected.Text = "True" Then
            upload_div.Visible = True
            Pending_div.Visible = False
        Else
            upload_div.Visible = False
            Pending_div.Visible = True
        End If
    End Sub
    Private Sub RepeaterBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Encr_decrData.Decrypt(HFEQS_ACD.Value))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[SV_ENQ].[GETENQ_STAGE]", param)
        Repeater_Stage.DataSource = ds
        Repeater_Stage.DataBind()
    End Sub
    Private Sub BindDocuments(ByVal ENQID As String, ByVal StgId As String, ByVal Repeater_Stage_Document As ListView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EQM_ENQID", ENQID)
        param(1) = New SqlClient.SqlParameter("@STG_ID", StgId)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[SV_ENQ].[GETENQ_DOC_LIST_ALL]", param)
        Repeater_Stage_Document.DataSource = ds
        Repeater_Stage_Document.DataBind()
    End Sub
    Protected Sub ddlEnqStatus_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindddlDocuments(ddlEnqStatus.SelectedValue)
    End Sub
    Sub BindStages()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Encr_decrData.Decrypt(HFEQS_ACD.Value))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[SV_ENQ].[GETENQ_STAGE]", param)
        ddlEnqStatus.DataSource = ds
        ddlEnqStatus.DataTextField = "PRO_DESCRIPTION"
        ddlEnqStatus.DataValueField = "PRB_STG_ID"
        ddlEnqStatus.DataBind()
        BindddlDocuments(ddlEnqStatus.SelectedValue)
    End Sub
    Sub BindddlDocuments(ByVal STG_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Encr_decrData.Decrypt(HFEQS_ACD.Value))
        param(1) = New SqlClient.SqlParameter("@STG_ID", STG_ID)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[SV_ENQ].[GETENQ_STAGE_Document]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlDocument.Items.Clear()
            ddlDocument.DataSource = ds
            ddlDocument.DataTextField = "DOC_DESCR"
            ddlDocument.DataValueField = "DOC_ID"
            ddlDocument.DataBind()
        Else
            Dim item As New System.Web.UI.WebControls.ListItem
            item.Text = "There is no Document "
            item.Value = "-1"
            ddlDocument.Items.Clear()
            ddlDocument.Items.Add(item)
        End If

    End Sub
#End Region

#Region "Functions"
    Private Sub Showand_hide_BtnSTATUS(EQS_STATUS As String)
        EnabledFalseAll()
        VisibleFalseAll()
        If HFSTGCount.Value = "5" Then
            lbtnScreening.Visible = False
            lbtnApproval.Visible = False
        Else
            lbtnScreening.Visible = True
            lbtnApproval.Visible = True
        End If
        If EQS_STATUS = "NEW" Or EQS_STATUS = "OPN" Then
            lbtnRegister.Visible = True
            lbtnRegisterSlip_Print.Visible = False
        Else
            If HFLinkRegFee.Value = "False" Then
                lbtnRegister.Visible = False
                lbtnRegisterSlip_Print.Visible = True
            Else
                lbtnRegister.Visible = True
                lbtnRegisterSlip_Print.Visible = False
            End If
        End If

        If EQS_STATUS = "NEW" Then
            LbtnShortlistEnquiry.Enabled = True
            HFStageID.Value = 1
        ElseIf EQS_STATUS = "OPN" Then
            lbtnRegister.Enabled = True
            HFStageID.Value = 2
        ElseIf EQS_STATUS = "REG" Then
            If HFSTGCount.Value = "5" Then
                lbtnOfferLetter.Enabled = True
            Else
                lbtnScreening.Enabled = True
            End If
            HFStageID.Value = 3

        ElseIf EQS_STATUS = "SCR" Then
            lbtnApproval.Enabled = True
            HFStageID.Value = 4
        ElseIf EQS_STATUS = "APR" Then
            lbtnOfferLetter.Enabled = True
            HFStageID.Value = 5
        ElseIf EQS_STATUS = "OFR" Then
            HFStageID.Value = 6
            lbtnEnrollment.Enabled = True
            lbtnOfferLetter.Visible = False
            lbtnOfferLetter_Print.Visible = True
        ElseIf EQS_STATUS = "ENR" Then
            HFStageID.Value = 7
            AfterEnrollment()
            Div_EnrollmentDone.Visible = True
            If String.IsNullOrWhiteSpace(HFstuIds.Value) Then
                A_Student.Visible = False
            Else
                A_Student.Visible = True
                A_Student.HRef = ResolveUrl("~/StudProfileGeneral.aspx?id=" + HFstuIds.Value + "&status=EN")
            End If
        End If

    End Sub
    Private Sub EnabledFalseAll()
        LbtnShortlistEnquiry.Enabled = False
        lbtnRegister.Enabled = False
        lbtnScreening.Enabled = False
        lbtnApproval.Enabled = False
        lbtnOfferLetter.Enabled = False
        lbtnOfferLetter_Print.Visible = False
        lbtnEnrollment.Enabled = False

    End Sub
    Private Sub btnVisibleFalseAll()
        LbtnShortlistEnquiry.Visible = False
        lbtnRegister.Visible = False
        lbtnScreening.Visible = False
        lbtnApproval.Visible = False
        lbtnOfferLetter.Visible = False
        lbtnOfferLetter_Print.Visible = False
        lbtnEnrollment.Visible = False
        lbtnFollowUp.Visible = False
        lbtnDocument.Visible = False
        lbtnRegisterSlip_Print.Visible = False
        Div_EnrollmentDone.Visible = False
    End Sub
    Private Sub VisibleFalseAll()
        Shortlist_Holder.Visible = False
        Register_Holder.Visible = False
        Screening_Holder.Visible = False
        Approval_Holder.Visible = False
        OfferLetter_Holder.Visible = False
        Offer_Letter_Printing_Holder.Visible = False
        Enrollment_Holder.Visible = False
        FollowUp_Holder.Visible = False
        Document_Holder.Visible = False
        RegisterSlip_Print_Holder.Visible = False

    End Sub
    Private Sub AfterEnrollment()
        VisibleFalseAll()
        btnVisibleFalseAll()
        Details_holder.Visible = False
    End Sub
    Private Function Convert_string_ToBoolean(ByVal value As String) As Boolean
        Dim flag As Boolean = False
        If value.ToLower().Trim() = "yes" Then
            flag = True
        Else
            flag = False
        End If
        Return flag
    End Function

    Private Function Get_STG_Four_ORDER() As Integer
        Dim STG As Integer = 0
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@pra_eqs_id", Encr_decrData.Decrypt(HFstuEqs.Value).ToString)
            param(1) = New SqlClient.SqlParameter("@PRA_STG_ID", SqlDbType.Int)
            param(1).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENQ.Get_STG_ORDER_Four", param)
            STG = param(1).Value
        End Using
        Return STG
    End Function
    Public Sub updateenquiry(ByVal eqsid As String, ByVal eqsstatus As String, ByVal currentstatus As Integer, ByVal stgid As Integer, ByVal pra_completed As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString()
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_ID", eqsid)
        pParms(1) = New SqlClient.SqlParameter("@EQS_STATUS", eqsstatus)
        pParms(2) = New SqlClient.SqlParameter("@CURRENT_STATUS", currentstatus)
        pParms(3) = New SqlClient.SqlParameter("@PRA_STG_ID", stgid)
        pParms(4) = New SqlClient.SqlParameter("@PRA_bCOMPLETED", pra_completed)
        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "StudentEnqChange", pParms)
        Dim returnflag As Integer = pParms(5).Value

    End Sub

    Sub GetInfo()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EQS_APL_ID,0),ISNULL(EQS_APL_DISABLE,'false'),ISNULL(EQS_APR_ID,0) FROM ENQUIRY_SCHOOLPRIO_S " _
                                & " WHERE EQS_ID=" + HiddenEQSID.Value
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim apl_id As String
        Dim apr_id As Integer
        While reader.Read
            apl_id = reader.GetValue(0).ToString + "|" + IIf(reader.GetBoolean(1) = True, "1", "0")
            apr_id = reader.GetValue(2)
        End While
        reader.Close()
        ddlScreeningDecision.ClearSelection()
        If apl_id <> "0|0" And apl_id <> "0|1" Then
            ddlScreeningDecision.Items.FindByValue(apl_id).Selected = True
            BindScreeningReason(apl_id)
        End If
        ddlScreeningReason.ClearSelection()
        If apr_id <> 0 Then
            ddlScreeningReason.Items.FindByValue(apr_id).Selected = True
        End If
    End Sub
    Private Function GetExtension(ByVal FileName As String) As String

        Dim Extension As String

        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)

        Return Extension


    End Function
    Public Function GetExtensionIcon(ByVal FileName As String) As String
        Dim Extension As String = GetExtension(FileName)
        Dim ExtensionIcon As String = "<i class=""fa fa-file-photo-o i-style-ex""></i>"
        If Extension.ToLower = "jpg" Or Extension.ToLower = "jpeg" Or Extension.ToLower = "png" Then
            ExtensionIcon = "<i class=""fa fa-file-photo-o i-style-ex""></i>"
        ElseIf Extension.ToLower = "doc" Or Extension.ToLower = "docx" Then
            ExtensionIcon = "<i class=""fa fa-file-word-o i-style-ex""></i>"
        ElseIf Extension.ToLower = "pdf" Then
            ExtensionIcon = "<i class=""fa fa-file-pdf-o i-style-ex""></i>"
        End If


        Return ExtensionIcon


    End Function
    Private Function GetAllow_Permissions(btn As LinkButton) As Boolean
        Dim flag As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EQM_ENQID", hfENQ_ID.Value)
        param(1) = New SqlClient.SqlParameter("@STG_ID", HFStageID.Value)
        param(2) = New SqlClient.SqlParameter("@Allow_Permissions", SqlDbType.Bit)
        param(2).Direction = ParameterDirection.Output
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[SV_ENQ].[GETENQ_DOC_Permissions]", param)
        Try
            flag = param(2).Value
        Catch ex As Exception

        End Try
        If flag = False Then
            Popup_Message = "You Cant proceed to the next stage until <br> you upload your pending document for that stage "
            Popup_Message_Status = WarningType.Danger
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            btn.Visible = False
        Else
            btn.Visible = True
        End If
        Return flag
    End Function

    Public Function GetSchoolHead() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim result As String = ""
        Dim IMG_IMAGE As String = String.Empty
        Dim BSU_NAME As String = String.Empty
        Dim BSU_POBOX As String = String.Empty
        Dim BSU_TEL As String = String.Empty
        Dim BSU_FAX As String = String.Empty
        Dim BSU_EMAIL As String = String.Empty
        Dim BSU_URL As String = String.Empty
        Dim html As String = ScreenScrapeHtml(Server.MapPath("~\Students\SchoolHeader.html"))
        Dim SParms(3) As SqlClient.SqlParameter
        SParms(0) = New SqlClient.SqlParameter("@IMG_BSU_ID", Session("sBSUID"))
        SParms(1) = New SqlClient.SqlParameter("@IMG_TYPE", "logo")
        Using readerSchool As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "getBsuInFoWithImage", SParms)
            While readerSchool.Read
                Try
                    If IsDBNull(readerSchool("IMG_IMAGE")) Then
                        IMG_IMAGE = ""
                    Else
                        Dim imageUrl As String = "data:image/png;base64," & Convert.ToBase64String(CType(readerSchool("IMG_IMAGE"), Byte()))
                        IMG_IMAGE = imageUrl
                    End If
                Catch ex As Exception

                End Try

                BSU_NAME = Convert.ToString(readerSchool("BSU_NAME"))
                BSU_POBOX = Convert.ToString(readerSchool("BSU_POBOX"))
                BSU_FAX = Convert.ToString(readerSchool("BSU_FAX"))
                BSU_TEL = Convert.ToString(readerSchool("BSU_TEL"))
                BSU_EMAIL = Convert.ToString(readerSchool("BSU_EMAIL"))
                BSU_URL = Convert.ToString(readerSchool("BSU_URL"))

            End While
        End Using
        If Trim(IMG_IMAGE) <> "" Then
            html = html.Replace("@IMG_IMAGE", IMG_IMAGE)
        Else
            html = html.Replace("@IMG_IMAGE", "")
        End If
        If Trim(BSU_NAME) <> "" Then
            html = html.Replace("@BSU_NAME", BSU_NAME)
        Else
            html = html.Replace("@BSU_NAME", "")
        End If
        If Trim(BSU_POBOX) <> "" Then
            html = html.Replace("@BSU_POBOX", BSU_POBOX)
        Else
            html = html.Replace("@BSU_POBOX", "")
        End If
        If Trim(BSU_FAX) <> "" Then
            html = html.Replace("@BSU_FAX", BSU_FAX)
        Else
            html = html.Replace("@BSU_FAX", "")
        End If
        If Trim(BSU_TEL) <> "" Then
            html = html.Replace("@BSU_TEL ", BSU_TEL)
        Else
            html = html.Replace("@BSU_TEL", "")
        End If
        If Trim(BSU_EMAIL) <> "" Then
            html = html.Replace("@BSU_EMAIL", BSU_EMAIL)
        Else
            html = html.Replace("@BSU_EMAIL", "")
        End If
        If Trim(BSU_URL) <> "" Then
            html = html.Replace("@BSU_URL", BSU_URL)
        Else
            html = html.Replace("@BSU_URL", "")
        End If
        result = html

        Return result
    End Function 'ScreenScrapeHtml
    Public Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml
#End Region

    Protected Sub btnOfferLetterToPDF_Init(sender As Object, e As EventArgs)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnOfferLetterToPDF)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DocumentSave)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lbtnDocument)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnRegisterSlipToPDF)
    End Sub




End Class
