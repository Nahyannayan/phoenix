﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studStaffLunchOrder.aspx.vb" Inherits="Students_studStaffLunchOrder"
    Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="../cssfiles/TabMenu.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .btnvisible {
            display: none;
        }

        .cssStepBtnActive {
            border: 1px solid #52a1c6 !important;
            background: #69b3d6 !important;
            display: block !important;
            float: left !important;
            text-decoration: none !important;
            font-family: Helvetica, sans-serif !important;
            color: #fff;
            margin: 2px !important;
            padding: 2px !important;
            width: 120px !important;
            height: 50px !important;
            cursor: pointer;
        }

            .cssStepBtnActive:hover {
                border: 1px solid #52a1c6 !important;
                background: #69b3d6 !important;
                display: block !important;
                float: left !important;
                text-decoration: none !important;
                font-family: Helvetica, sans-serif !important;
                color: #fff;
                margin: 2px !important;
                padding: 2px !important;
                width: 120px !important;
                height: 50px !important;
                cursor: pointer;
            }

        .cssStepBtnInActive {
            border: 1px solid #e8e8e8 !important;
            background: #fbfbfb !important;
            display: block !important;
            float: left !important;
            text-decoration: none !important;
            font-family: Helvetica, sans-serif !important;
            color: #BCBDAC;
            margin: 2px !important;
            padding: 2px !important;
            width: 120px !important;
            height: 50px !important;
            cursor: pointer;
        }

            .cssStepBtnInActive:hover {
                border: 1px solid #e8e8e8 !important;
                background: #fbfbfb !important;
                display: block !important;
                float: left !important;
                text-decoration: none !important;
                font-family: Helvetica, sans-serif !important;
                color: #BCBDAC;
                margin: 2px !important;
                padding: 2px !important;
                width: 120px !important;
                height: 50px !important;
                cursor: pointer;
            }
    </style>

    <script type="text/javascript">


        function doClick(buttonName, e) {
            //the purpose of this function is to allow the enter key to 
            //point to the correct button to click.
            var key;

            if (window.event)
                key = window.event.keyCode;     //IE
            else
                key = e.which;     //firefox

            if (key == 13) {
                //Get the button the user wants to have clicked
                var btn = document.getElementById(buttonName);
                alert(btn);
                if (btn != null) { //If we find the button click it
                    btn.click();
                    event.keyCode = 0
                }
            }
        }
        function checkItem(o) {
            var i = o.value.length;

            if (i > 7) //trap enter
            {

                document.getElementById('<%= btnTest.ClientID %>').click();
                event.keyCode = 9;
                return event.keyCode; //convert to Tab key
                //}
            }
        }
        function CHECK_DROPDOWN_item(DD) {
            //            var key = String.fromCharCode(event.keyCode);
            //            var s = DD.value
            //            alert(key)
            //            if (s == '')
            //                DD.value = s
            //            else if (event.keyCode != 13) 
            //            DD.value = s+ ',' ;

            if (event.keyCode == 13) {

                document.getElementById('<%= btnSav.ClientID %>').click();
            }

        }

        function CHECK_DROPDOWN(DD) {
            var key = String.fromCharCode(event.keyCode);

            DD.value = key;
            document.getElementById('<%= txtItems.ClientID %>').focus();

        }
        function chk_key(txt) {
            var i = txt.value.length;

            if (i > 9) {

                document.getElementById('<%= btnOrder.ClientID %>').click();
            }


        }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Staff Lunch Order"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <asp:Button ID="btnTest" runat="server" OnClick="btnTest_Click" CssClass="btnvisible" />
                <asp:Button ID="btnOrder" runat="server" OnClick="btnOrder_Click" CssClass="btnvisible" />
                <table id="tbl_ShowScreen" runat="server" align="center" border="0"
                    cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="8">
                            <table id="Table1" runat="server" align="center"
                                cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblAccText" runat="server" Text="Staff Id :" CssClass="field-label"></asp:Label>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStaffId" runat="server" onkeyup="checkItem(this);"
                                            OnTextChanged="txtStaffId_TextChanged" AutoPostBack="true">
                                        </asp:TextBox>&nbsp;
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr id="trDets" runat="server">
                                    <td colspan="4">
                                        <asp:LinkButton ID="href1" runat="server" CssClass="">
                 <div class="cssStepNo" >Order</div>
                                    
                
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="href2" runat="server" CssClass="">
                 <div class="cssStepNo" >Delivery</div> </asp:LinkButton>

                                    </td>
                                </tr>
                                <tr id="trOrder" runat="server" visible="false">
                                    <td colspan="4">
                                        <table runat="server" align="center"
                                            cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%">
                                                    <span class="field-label">Staff Name</span>
                                                </td>

                                                <td align="left" width="30%">
                                                    <asp:Label ID="lblName" runat="server" CssClass="field-value"></asp:Label>
                                                </td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="30%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <span class="field-label">Reason for Stay Back</span>
                                                </td>

                                                <td align="left">
                                                    <asp:DropDownList ID="ddlreason" runat="server" AutoPostBack="true" onkeyup="CHECK_DROPDOWN(this);">
                                                    </asp:DropDownList>
                                                </td>

                                                <td align="left">
                                                    <span class="field-label">Food Items</span>
                                                </td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtItems" runat="server" Width="400px" AutoPostBack="true" onkeydown="CHECK_DROPDOWN_item(this);"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Button ID="btnSav" runat="server" Text="Save" CssClass="button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trPass" visible="false">

                                    <td colspan="4">
                                        <asp:Label ID="lblname1" runat="server"></asp:Label><br />
                                        <span class="field-label">Enter Order No:</span>
                                        <asp:TextBox ID="txtPass" runat="server" AutoPostBack="true" onkeydown="chk_key(this);"></asp:TextBox><br />
                                        <asp:GridView ID="gvPass" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" Visible="true">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("SLP_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<%# Bind("SLP_DATE","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reason">
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblreason" runat="server" Text='<%# Bind("SBR_REASON") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Items">
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTime" runat="server" Text='<%# Bind("SLP_SLM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delivered" Visible="false">
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAlert" runat="server" Text='<%# Bind("SLP_bCOLLECT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="Delivered" ImageUrl="~\images\tick.gif" ButtonType="Image"
                                                    HeaderText="Delivered">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="Delivered Date">
                                                    <ItemStyle HorizontalAlign="center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblddate" runat="server" Text='<%# Bind("SLP_COLLDATE","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle />
                                            <RowStyle CssClass="griditem" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                            <EmptyDataRowStyle />
                                        </asp:GridView>
                                    </td>
                                </tr>



                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
