Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Configuration
Imports ActivityFunctions
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports System.Web.Security

Partial Class Students_studTerm_GradeWise
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"

                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C300026") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("datamode") = "add" Then

                    Else

                    End If
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    GridBind()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub

#Region "Private Functions "
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT TRM_ID,TRM_ACD_ID,TRM_DESCRIPTION,TRM_STARTDATE,TRM_ENDDATE FROM " _
                                & " TERM_MASTER WHERE TRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
                               
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String


        Dim ddlgvTerm As New DropDownList
        Dim selectedTerm As String = ""

        If gvTerms.Rows.Count > 0 Then
            ddlgvTerm = gvTerms.HeaderRow.FindControl("ddlgvTerm")
            If ddlgvTerm.Text <> "ALL" And ddlgvTerm.Text <> "" Then
                strFilter += " AND TRM_DESCRIPTION='" + ddlgvTerm.Text + "'"
                selectedTerm = ddlgvTerm.Text
            End If

            If strFilter.Trim <> "" Then
                str_query += strFilter
            End If

        End If

        str_query += " order by 1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvTerms.DataSource = ds
        gvTerms.DataBind()

        Dim dt As DataTable = ds.Tables(0)

        If gvTerms.Rows.Count > 0 Then
            ddlgvTerm = gvTerms.HeaderRow.FindControl("ddlgvTerm")

            Dim dr As DataRow
            ddlgvTerm.Items.Clear()
            ddlgvTerm.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    ''check for duplicate values and add to dropdownlist 
                    If ddlgvTerm.Items.FindByText(.Item(2)) Is Nothing Then
                        ddlgvTerm.Items.Add(.Item(2))
                    End If
                End With
            Next

            If selectedTerm <> "" Then
                ddlgvTerm.Text = selectedTerm
            End If

        End If
    End Sub

    Sub BindGrades(ByVal Trm_id As String, ByVal gvGrades As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT DISTINCT GRD_ID,GRM_DISPLAY,GRD_DISPLAYORDER, " _
                              & " CASE  WHEN TRM_STR_DT='1900-01-01 00:00:00.000' THEN '' ELSE CONVERT(VARCHAR,TRM_STR_DT,106) END AS TRM_STR_DT, " _
                              & " CASE  WHEN TRM_END_DT='1900-01-01 00:00:00.000' THEN '' ELSE CONVERT(VARCHAR,TRM_END_DT,106) END AS TRM_END_DT, " _
                              & " ISNULL(CONVERT(VARCHAR,TRM_TOT_DAYS),'') TRM_TOT_DAYS  " _
                              & " FROM GRADE_BSU_M AS A " _
                              & " INNER JOIN GRADE_M AS C ON A.GRM_GRD_ID=C.GRD_ID " _
                              & " LEFT OUTER JOIN TERM_GRADEWISE D ON A.GRM_ACD_ID=D.TRM_ACD_ID AND A.GRM_GRD_ID=D.TRM_GRD_ID AND TRM_ID=" & Trm_id & " " _
                              & " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        gvGrades.DataSource = ds
        gvGrades.DataBind()

    End Sub


#End Region

    Protected Sub gvTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
       
    End Sub

    Protected Sub gvTerms_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            Try
                gvTerms.PageIndex = e.NewPageIndex
                gvTerms.DataBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvTerms_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Try
            
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvTerms_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblTermID As New Label

                lblTermID = e.Row.FindControl("lblTermId")

                Dim gvGrades As New GridView
                gvGrades = e.Row.FindControl("gvGrade")
                BindGrades(lblTermID.Text, gvGrades)

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvGrade_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlgvTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim iReturnvalue As Integer
        Dim iIndex As Integer
        Dim cmd As New SqlCommand
        Dim stTrans As SqlTransaction
        Dim objConn As SqlConnection
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            objConn = New SqlConnection(str_conn)
            objConn.Open()
            stTrans = objConn.BeginTransaction

            For Each GvRowTerm As GridViewRow In gvTerms.Rows

                '--- To process the Term wise Grades
                Dim gvGrades As New GridView
                gvGrades = GvRowTerm.FindControl("gvGrade")

                '---- To Process each Grades Based on the Term row
                For Each gvrowGrd As GridViewRow In gvGrades.Rows
                    If CheckDataExist(gvrowGrd) = True Then

                        cmd = New SqlCommand("[dbo].[SaveTERM_GRADEWISE]", objConn, stTrans)
                        cmd.CommandType = CommandType.StoredProcedure

                        cmd.Parameters.AddWithValue("@TRM_GRDID_NEW", 1)
                        cmd.Parameters.AddWithValue("@TRM_BSU_ID", Session("sBsuId"))
                        cmd.Parameters.AddWithValue("@TRM_ACD_ID", ddlAcademicYear.SelectedValue)
                        cmd.Parameters.AddWithValue("@TRM_ID", TryCast(GvRowTerm.FindControl("lblTermId"), Label).Text) '
                        cmd.Parameters.AddWithValue("@TRM_GRD_ID", gvrowGrd.Cells(1).Text)
                        cmd.Parameters.AddWithValue("@TRM_STR_DT", TryCast(gvrowGrd.FindControl("txtStartDt"), TextBox).Text)
                        cmd.Parameters.AddWithValue("@TRM_END_DT", TryCast(gvrowGrd.FindControl("txtendDt"), TextBox).Text)
                        cmd.Parameters.AddWithValue("@TRM_TOT_DAYS", TryCast(gvrowGrd.FindControl("txtHrs"), TextBox).Text)
                        cmd.Parameters.AddWithValue("@bEdit", 0)

                        cmd.Parameters.Add("@ReturnValue", SqlDbType.BigInt)
                        cmd.Parameters("@ReturnValue").Direction = ParameterDirection.ReturnValue
                        cmd.ExecuteNonQuery()
                        iReturnvalue = CInt(cmd.Parameters("@ReturnValue").Value)
                    End If
                Next

                If iReturnvalue <> 0 Then
                    stTrans.Rollback()
                    lblError.Text = "Unexpected error"
                    Exit Sub
                End If

            Next
            '-- Complete the transaction 
            stTrans.Commit()
            lblError.Text = "Successfully Saved"
            objConn.Close()
        Catch ex As Exception
            If Not stTrans Is Nothing Then
                stTrans.Rollback()
            End If
            If Not objConn Is Nothing Then
                objConn.Close()
            End If

        End Try
    End Sub
    Private Function CheckDataExist(ByVal GvRow As GridViewRow) As Boolean
        Try
            If TryCast(GvRow.FindControl("txtStartDt"), TextBox).Text = "" And _
                            TryCast(GvRow.FindControl("txtendDt"), TextBox).Text = "" And _
                            TryCast(GvRow.FindControl("txtHrs"), TextBox).Text = "" Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception

        End Try

    End Function


    Private Function DisplayGradeValues(ByVal GvRow As GridViewRow)
        Try

            TryCast(GvRow.FindControl("txtStartDt"), TextBox).Text = ""
            TryCast(GvRow.FindControl("txtendDt"), TextBox).Text = ""
            TryCast(GvRow.FindControl("txtHrs"), TextBox).Text = ""

        Catch ex As Exception

        End Try
    End Function

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GridBind()
    End Sub
End Class
