<%@ Page Language="VB" AutoEventWireup="false" CodeFile="COVID_Relief_Attachments.aspx.vb" Inherits="COVID_Relief_Attachments" Title="Untitled Page" %>

<html>
<body>
    <form id="frm1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </ajaxToolkit:ToolkitScriptManager>
                <link href='<%= ResolveUrl("~/vendor/bootstrap/css/bootstrap.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/cssfiles/sb-admin.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/vendor/datatables/dataTables.bootstrap4.css")%>' rel="stylesheet" />
        <script src='<%= ResolveUrl("~/vendor/jquery/jquery.min.js")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.pack.js?1=2")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.js?1=2")%>'></script>
        <link type="text/css" href='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.css?1=2")%>' rel="stylesheet" />
        Document Of / Uploaded By
        <asp:GridView ID="gvAttachment" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
            PageSize="20" Width="100%">
            <RowStyle CssClass="griditem" />
            <Columns>
                <asp:TemplateField HeaderText="RRF_ID" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lblRRF_ID" runat="server" Text='<%# Bind("RRF_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Document Belongs To">
                    <ItemTemplate>
                        <asp:Label ID="lblParent" runat="server" Text='<%# Bind("Parent")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employment Type">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpType" runat="server" Text='<%# Bind("RET_DESC")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Relief Reason">
                    <ItemTemplate>
                        <asp:Label ID="lblRSN" runat="server" Text='<%# Bind("RSN_DESC")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File Name ">
                    <ItemTemplate>
                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("RRF_FILENAME")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Doc Type">
                    <ItemTemplate>
                        <asp:Label ID="lblDocName" runat="server" Text='<%# Bind("RRF_FILETYPE")%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Download">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDownload" runat="server" Text="Download" OnClick="lnkDownload_Click"></asp:LinkButton>
                         <asp:HiddenField ID="hf_PhotoPath" runat="server" Value='<%# Bind("RRF_FILEPATH")%>' />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
            </Columns>
            <SelectedRowStyle BackColor="Wheat" />
            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
            <AlternatingRowStyle CssClass="griditem_alternative" />
        </asp:GridView>


    </form>
</body>
</html>
