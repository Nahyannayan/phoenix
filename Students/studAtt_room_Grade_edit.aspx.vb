Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studAtt_room_Grade_edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050112") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else


                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    '  Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                 


                    If ViewState("datamode") = "view" Then
                        vid.Visible = True
                        aid.Visible = False
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        ' ltGrade.Text = Encr_decrData.Decrypt(Request.QueryString("v1").Replace(" ", "+"))

                        Call bindData()
                        bindOther_Grade()
                        enable_control()
                        vid.Visible = False
                        aid.Visible = True

                    End If


                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If


    End Sub

    Sub bindData()

        Dim sql_string As String = String.Empty
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet
        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(1) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        PARAM(2) = New SqlParameter("@INFO_TYPE", IIf(ViewState("datamode") = "view", "PERIOD_VIEW", "PERIOD_MASTER"))
        PARAM(3) = New SqlParameter("@GRD_ID", ViewState("viewid"))


        '        If ViewState("datamode") = "view" Then
        '            sql_string = " SELECT P.RAP_PERIOD_ID AS PERIOD_ID, P.RAP_PERIOD_DESCR AS PERIOD_DESCR, G.RAPG_TIME, case when G.RAPG_ID is null then 'false' else 'true' end as flag " & _
        '" FROM   ATT.ROOM_ATTENDANCE_PERIOD AS P LEFT OUTER JOIN   ATT.ROOM_ATT_PERIOD_GRADES AS G ON P.RAP_PERIOD_ID = G.RAPG_RAP_PERIOD_ID " & _
        '" AND  G.RAPG_GRD_ID='" & ViewState("viewid") & "' AND G.RAPG_BSU_ID='" & Session("sBsuid") & "'"

        '        Else
        '            sql_string = "SELECT RAP_PERIOD_ID as PERIOD_ID, RAP_PERIOD_DESCR as PERIOD_DESCR,'' as RAPG_TIME,  'false'  as flag FROM ATT.ROOM_ATTENDANCE_PERIOD"
        '        End If
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "VA.GETMASTER_PERIOD_BIND", PARAM)
        gvDoc.DataSource = ds.Tables(0)
        gvDoc.DataBind()

    End Sub
   
    
    Sub bindOther_Grade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds1 As New DataSet
        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(1) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        PARAM(2) = New SqlParameter("@INFO_TYPE", "GRADE")
        Dim GRD_ID As String = ViewState("viewid")

        ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "VA.GETMASTER_PERIOD_BIND", PARAM)
        chkOthGrade.Items.Clear()
        Dim row As DataRow
        If ds1.Tables(0).Rows.Count > 0 Then
            For Each row In ds1.Tables(0).Rows
                Dim str1 As String = row("GRM_DISPLAY")
                Dim str2 As String = row("GRD_ID")
                'Dim FLAG As Boolean = Mid(row("GRD_ID"), InStr(1, row("GRD_ID"), "|"), 1)
                chkOthGrade.Items.Add(New ListItem(str1, str2))
            Next
        End If

        For Each item As ListItem In chkOthGrade.Items
            If item.Value.ToString.Replace("|0", "").Replace("|1", "") = GRD_ID Then
                item.Enabled = False
                item.Selected = True
            End If
            If InStr(1, item.Value, "|0") > 0 Then

                item.Enabled = False

            End If
        Next



    End Sub

   
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            disable_control()
            ViewState("viewid") = ""
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                ' Call disable_control()


            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try


    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"
            reset_state()
            Call disable_control()
        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim TEMP_GRD_ID As String
        Dim GRD_IDs As String = String.Empty
        Dim BSU_ID As String = Session("sBsuid")
        Dim str As String = String.Empty
        Dim i As Integer


        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim status As Integer
                bEdit = False
                For Each item As ListItem In chkOthGrade.Items
                    If item.Selected = True Then
                        GRD_IDs += Mid(item.Value, 1, InStr(1, item.Value, "|") - 1) + "|"
                    End If
                Next


                For i = 0 To gvDoc.Rows.Count - 1

                    Dim row As GridViewRow = gvDoc.Rows(i)

                    Dim chkPer As CheckBox = DirectCast(row.FindControl("chkPer"), CheckBox)
                    If chkPer.Checked = True Then
                        Dim V_ATT As Boolean = DirectCast(row.FindControl("chkVAtt"), CheckBox).Checked

                        str += String.Format("<PERIOD PER_ID='{0}' SCH='{1}' V_ATT='{2}' ></PERIOD>", _
                                             DirectCast(row.FindControl("lblPeriod_id"), Label).Text, _
                                             DirectCast(row.FindControl("txtRAPG_TIME"), TextBox).Text, V_ATT)
                    End If
                Next
                If str <> "" Then
                    str = "<PERIODS>" + str + "</PERIODS>"
                Else
                    str = ""
                End If

                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@GRD_IDS", GRD_IDs)
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@PERIODS", str)
                pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "VA.SAVEVERTICAL_ATT_GRADE_PERIODS", pParms)
                status = pParms(3).Value

                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If


                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                calltransaction = "0"


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
        'ElseIf ViewState("datamode") = "edit" Then
        'Dim transaction As SqlTransaction

        'Using conn As SqlConnection = ConnectionManger.GetOASISConnection
        '    transaction = conn.BeginTransaction("SampleTransaction")
        '    Try
        '        Dim status As Integer

        '        TEMP_GRD_ID = ViewState("viewid") + "|"

        '        bEdit = True

        '        For i = 0 To gvDoc.Rows.Count - 1

        '            Dim row As GridViewRow = gvDoc.Rows(i)

        '            Dim chkPer As CheckBox = DirectCast(row.FindControl("chkPer"), CheckBox)
        '            If chkPer.Checked = True Then
        '                str += String.Format("<PERIOD PER_ID='{0}' SCH='{1}' ></PERIOD>", DirectCast(row.FindControl("lblPeriod_id"), Label).Text, DirectCast(row.FindControl("txtRAPG_TIME"), TextBox).Text)
        '            End If
        '        Next
        '        If str <> "" Then
        '            str = "<PERIODS>" + str + "</PERIODS>"
        '        Else
        '            str = ""
        '        End If

        '        status = AccessStudentClass.UPDATE_ROOM_ATT_PERIOD_GRADES(TEMP_GRD_ID, BSU_ID, str, bEdit, transaction)
        '        If status <> 0 Then
        '            calltransaction = "1"
        '            errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
        '            Return "1"
        '        End If



        '        If status <> 0 Then
        '            calltransaction = "1"
        '            errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
        '            Return "1"
        '        End If

        '        ViewState("viewid") = "0"
        '        ViewState("datamode") = "none"

        '        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        '        calltransaction = "0"

        '        disable_control()
        '        ViewState("viewid") = 0
        '    Catch ex As Exception
        '        calltransaction = "1"
        '        errorMessage = "Error Occured While Saving."
        '    Finally
        '        If calltransaction <> "0" Then
        '            UtilityObj.Errorlog(errorMessage)
        '            transaction.Rollback()
        '        Else
        '            errorMessage = ""
        '            transaction.Commit()
        '        End If
        '    End Try

        'End Using
        'End If
    End Function

   
    Sub reset_state()
        vid.Visible = False
        aid.Visible = True
        chkOthGrade.Enabled = False
        gvDoc.Enabled = False
    End Sub
    Sub disable_control()
        gvDoc.Enabled = False
        chkOthGrade.Enabled = False
    End Sub
    Sub enable_control()
        gvDoc.Enabled = True
        chkOthGrade.Enabled = True
    End Sub
    

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub gvDoc_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim cs As ClientScriptManager = Page.ClientScript
        For Each gvRow As GridViewRow In gvDoc.Rows
            Dim chkPer As CheckBox = DirectCast(gvRow.FindControl("chkPer"), CheckBox)
            cs.RegisterArrayDeclaration("chkPer", String.Concat("'", chkPer.ClientID, "'"))
        Next
    End Sub
End Class
