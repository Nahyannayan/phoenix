<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="StudVATT_Registration.aspx.vb" Inherits="Students_studAtt_Registration" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function checkDate(sender, args) {

            var currenttime = document.getElementById("<%=hfDate.ClientID %>").value;

          if (sender._selectedDate > new Date(currenttime)) {
              alert("You cannot select a day greater than today!");
              sender._selectedDate = new Date(currenttime);
              // set the date back to the current date
              sender._textbox.set_Value(sender._selectedDate.format(sender._format))
          }
      }

      /*
           function getDate(mode) 
         {     
                  var sFeatures;
                  sFeatures="dialogWidth: 229px; ";
                  sFeatures+="dialogHeight: 234px; ";
                  sFeatures+="help: no; ";
                  sFeatures+="resizable: no; ";
                  sFeatures+="scroll: yes; ";
                  sFeatures+="status: no; ";
                  sFeatures+="unadorned: no; ";
                  var NameandCode;
                  var result;
                  if (mode==1)
                      result = window.showModalDialog("../accounts/calendar.aspx?dt="+document.getElementById('<=txtDate.ClientID %>').value,"", sFeatures) 
                       //    if (result=='' || result==undefined)
               //   {
      //            document.getElementById("txtDate").value=''; 
              //    return false;
                //  }
               //  if (mode==1)
                 //   document.getElementById('<=txtDate.ClientID %>').value=result; 
                        //     return true;
        //  } 
          */

      function hideButton() {
          document.forms[0].submit();
          window.setTimeout("hideButton('" + window.event.srcElement.id + "')", 0);
      }

      function hideButton(buttonID) {
          document.getElementById(buttonID).style.display = 'none';
          document.getElementById('<%=btnSave.ClientID %>').style.display = 'none';
				document.getElementById('<%=btnSave2.ClientID %>').style.display = 'none';
			}

    </script>
    <!--    
<script type="text/javascript">
  function validate_Late()
    {
           for(i=0;i<chkLate.length;i++)
            {
           if(document.getElementById(chkLate[i]).checked ==true)
                {  
                   document.getElementById(chkPresent[i]).checked=true;
             }
           }
                  }
    
 function validate_Present()
    {
           for(i=0;i<chkPresent.length;i++)
            {
           if(document.getElementById(chkPresent[i]).checked ==false)
                {  
                   document.getElementById(chkLate[i]).checked=false;
             }
           }
                  }

   </script>
-->
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Group Attendance"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

 <%--   <table id="Table1" width="100%" border="0">
        <tbody>
            <tr >
                <td class="title" align="left" width="50%">
                    <asp:Literal ID="ltHeader" runat="server" Text="GROUP ATTENDANCE"></asp:Literal></td>
            </tr>
        </tbody>
    </table>--%>

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
             
                    <div align="left">
                        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                    </div>
                    <div align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                            EnableViewState="False"  ValidationGroup="AttGroup"></asp:ValidationSummary>
                      
                    </div>
                
            </td>
        </tr>
        <tr >
            <%--<td align="center" class="matters" valign="middle">&nbsp;Fields Marked with (<span style="font-size: 8pt; color: #800000">*</span>)
                are mandatory</td>--%>
             <td align="center" class="text-danger font-small" valign="middle">
                Fields Marked with ( * ) are mandatory
            </td>
        </tr>
        <tr>
            <td >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg-lite" colspan="4">
                        
                                <asp:Literal ID="ltLabel" runat="server" Text="Group Attendance"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Academic Year</span><span class="text-danger font-small" >*</span></td>
                
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" ><span class="field-label">Group</span><span class="text-danger font-small" >*</span></td>
                  
                        <td align="left" >
                            <asp:DropDownList ID="ddlGroup" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Attendance Date</span><span class="text-danger font-small">*</span></td>
            
                        <td align="left" >
                            <asp:DropDownList ID="ddlAttDate" runat="server">
                            </asp:DropDownList><asp:TextBox ID="txtDate" runat="server" ></asp:TextBox><asp:ImageButton
                                ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Attendance Date required" 
                                    ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                        <td align="left" ><span class="field-label">Attendance Type</span></td>
                      
                        <td align="left" >
                            <asp:DropDownList ID="ddlAttType" runat="server">
                                <asp:ListItem>Session1</asp:ListItem>
                                <asp:ListItem>Session2</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td  align="center">
                <br />
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" OnClick="btnAdd_Click" ValidationGroup="AttGroup" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" OnClick="btnEdit_Click" ValidationGroup="AttGroup" /></td>
        </tr>
        <tr>
            <td ></td>
        </tr>
        <tr>
            <td  align="center">
                <asp:Button ID="btnSave2"
                    runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                <asp:Button
                        ID="btnCancel2" runat="server" CausesValidation="False" CssClass="button"
                        Text="Cancel" /></td>
        </tr>
        <tr>
            <td ></td>
        </tr>
        <tr>
            <td  align="center">
                <asp:GridView ID="gvInfo" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-row" Height="100%" Width="100%" DataKeyNames="SRNO" OnRowCreated="gvInfo_RowCreated">
                    <RowStyle CssClass="griditem"  />
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No">
                            <ItemTemplate>
                                <asp:Label ID="lblsNo" runat="server" Text='<%# Bind("SRNO") %>' ></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="StudentID">
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStuNo" runat="server" Text='<%# bind("STU_NO") %>' ></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student ID1" Visible="False">
                            <EditItemTemplate>
                                &nbsp; 
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStud_ID" runat="server" Text='<%# Bind("STU_ID") %>' ></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <EditItemTemplate>
                                &nbsp; 
                            </EditItemTemplate>
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="lblStudName" runat="server" Text='<%# bind("STUDNAME") %>' ></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" ></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlStatus" runat="server" ></asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <ItemTemplate>
                                &nbsp;<asp:TextBox ID="txtRemarks" runat="server" Text='<%# bind("REMARKS") %>' MaxLength="255" ></asp:TextBox>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" ></HeaderStyle>

                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ALG_ID" Visible="False">
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblALG_ID" runat="server" Text='<%# bind("ALG_ID") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GENDER" Visible="False">
                            <EditItemTemplate>
                                &nbsp;
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSGENDER" runat="server" Text='<%# Bind("SGENDER") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="APPLEAVE" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAPPLEAVE" runat="server" Text='<%# Bind("APPLEAVE") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MINLIST" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblMinList" runat="server" Text='<%# Bind("minList") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DAY1" HeaderText="DAY1"></asp:BoundField>
                        <asp:BoundField DataField="DAY2" HeaderText="DAY2"></asp:BoundField>
                        <asp:BoundField DataField="DAY3" HeaderText="DAY3"></asp:BoundField>
                    </Columns>
                    <SelectedRowStyle  />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td ></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnSave"
                    runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                <asp:Button
                        ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                        Text="Cancel" />
            </td>
        </tr>
        <tr>
            <td ></td>
        </tr>
        <tr>
            <td >&nbsp;
            
            
               
            
            
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfDay1" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfDay2" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfDay3" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfDay4" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfDay5" runat="server"></asp:HiddenField>
    <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgCalendar" TargetControlID="txtDate" OnClientDateSelectionChanged="checkDate">
    </ajaxToolkit:CalendarExtender>
    <asp:HiddenField ID="hfDate" runat="server"></asp:HiddenField>

                          
        </div>
        </div>
    </div>

</asp:Content>

