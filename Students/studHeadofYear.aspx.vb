Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.IO
Partial Class Students_studHeadofYear
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Dim mode As String = Request.QueryString("mode")
                If mode = "edit" Then

                    Dim editstring As String = Encr_decrData.Decrypt(Request.QueryString("editstring").Replace(" ", "+"))
                    hfSEC_ID.Value = editstring
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    If ddlAcademicYear.Items.Count <> 0 Then
                        PopulateGrade()
                    End If
                    PopulateClassTeacher()
                    DisplayHODDetails()
                    EnableDisableControls(False)
                ElseIf mode = "add" Then
                    lblError.Text = ""
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                    If ddlAcademicYear.Items.Count <> 0 Then
                        PopulateGrade()
                    End If
                    PopulateClassTeacher()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try
            If ddlGrade.Items.Count <> 0 Then
                'PopulateSection()
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ViewState("datamode") = "add"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            EnableDisableControls(True)
            lblError.Text = ""
            ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
            PopulateClassTeacher()
            PopulateGrade()
            'PopulateSection()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If IsHeadOfYearAdded() = False Then
                SaveData()
                ViewState("datamode") = "none"
            Else
                lblError.Text = "The tutor for this section is already added"
                Exit Sub
            End If
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            EnableDisableControls(False)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            ViewState("datamode") = "edit"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            UtilityObj.beforeLoopingControls(Me.Page)
            EnableDisableControls(True)
            lblError.Text = ""
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            hfSEC_ID.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                EnableDisableControls(False)
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

#Region "Private Method"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub PopulateGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder" _
                                  & " FROM grade_bsu_m,grade_m WHERE grade_bsu_m.grm_grd_id=grade_m.grd_id " _
                                  & "AND grm_acd_id=" + ddlAcademicYear.SelectedValue + " ORDER BY grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()
    End Sub


    'Sub PopulateSection()
    '    ddlSection.Items.Clear()

    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT sct_descr,sct_id FROM section_m " _
    '                & " WHERE  sct_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " AND sct_bsu_id='" + Session("Sbsuid") + "'" _
    '                & " AND sct_grm_id=" + ddlStream.SelectedValue + " and sct_id not in(select sec_sct_id from section_tutor_d where sec_todt is null)" _
    '                & " order by sct_descr"

    '    Dim ds As DataSet
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlSection.DataSource = ds
    '    ddlSection.DataTextField = "sct_descr"
    '    ddlSection.DataValueField = "sct_id"
    '    ddlSection.DataBind()
    'End Sub
    Private Sub DisplayHODDetails()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT * FROM HEADOFYEAR WHERE HODID=" & hfSEC_ID.Value & " AND BSU_ID=" & Session("sBsuid")
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count >= 1 Then
            ddlAcademicYear.SelectedValue = ds.Tables(0).Rows(0).Item("ACCYEAR")
            ddlGrade.SelectedValue = ds.Tables(0).Rows(0).Item("GRADEID")
            ddlClassTeacher.SelectedValue = ds.Tables(0).Rows(0).Item("STAFF_ID")
        End If

    End Sub
    Sub PopulateClassTeacher()
        ddlClassTeacher.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
                                 & "emp_id FROM employee_m WHERE emp_ect_id=1 and emp_bsu_id='" + Session("sBsuid") + "' order by emp_fname,emp_mname,emp_lname"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClassTeacher.DataSource = ds
        ddlClassTeacher.DataTextField = "emp_name"
        ddlClassTeacher.DataValueField = "emp_id"
        ddlClassTeacher.DataBind()
    End Sub

    Private Sub SaveData()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim transaction As SqlTransaction
        Dim prevId As Integer = Val(hfSEC_ID.Value)
        Dim flagAudit As Integer
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                If ViewState("datamode") = "add" Then
                    str_query = "exec saveHEADOFYEAR 0 ,'" + ddlGrade.SelectedValue.ToString + "',0," + ddlClassTeacher.SelectedValue.ToString _
                                & "," + Session("sBsuid").ToString + "," + ddlAcademicYear.SelectedValue.ToString.Trim + ",'Add'"
                ElseIf ViewState("datamode") = "edit" Then
                    'UtilityObj.InsertAuditdetails(transaction, "edit", "HEADOFYEAR", "HODID", "SEC_SCT_ID", "SEC_ID=" + hfSEC_ID.Value.ToString)
                    str_query = "exec saveHEADOFYEAR " + hfSEC_ID.Value + ",'" + ddlGrade.SelectedValue.ToString + "',0," + ddlClassTeacher.SelectedValue.ToString _
                                & "," + Session("sBsuid").ToString + "," + ddlAcademicYear.SelectedValue.ToString.Trim + ",'Edit'"
                End If

                hfSEC_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
                If ViewState("datamode") = "add" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "HODID(" + hfSEC_ID.Value.ToString + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                ElseIf ViewState("datamode") = "edit" Then
                    If prevId <> 0 Then
                        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "HODID(" + prevId.ToString + ")", "Edit", Page.User.Identity.Name.ToString, Me.Page)
                    End If
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "HODID(" + hfSEC_ID.Value.ToString + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using

    End Sub

    Sub EnableDisableControls(ByVal Value As Boolean)
        ddlAcademicYear.Enabled = Value
        ddlGrade.Enabled = Value
        ddlSection.Enabled = Value
        ddlClassTeacher.Enabled = Value
    End Sub

    Private Function IsHeadOfYearAdded() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If ViewState("datamode") = "edit" Then

            str_query = "SELECT count(HODID) FROM HEADOFYEAR WHERE ACCYEAR=" + ddlAcademicYear.SelectedValue.ToString + "" _
                      & " AND GRADEID='" + ddlGrade.SelectedValue.ToString + "' AND BSU_ID='" + Session("sbsuid") + "'" _
                      & " AND STAFF_ID = " + ddlClassTeacher.SelectedValue.ToString + ""
        Else
            str_query = "SELECT count(HODID) FROM HEADOFYEAR WHERE ACCYEAR=" + ddlAcademicYear.SelectedValue.ToString + "" _
                     & " AND GRADEID='" + ddlGrade.SelectedValue.ToString + "' AND BSU_ID='" + Session("sbsuid") + "'" _
                     & " AND STAFF_ID = " + ddlClassTeacher.SelectedValue.ToString + ""
        End If

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If count = 0 Then
            Return False
        End If

        Return True

    End Function
#End Region


End Class
