Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_StudEnq_Setting_Ack_edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050175") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    Call BindApplicationDecision()
                    Call loadMatter_Dropdown()
                    Call loadPattern_Dropdown()
                    Call LoadPattern_Radio()
                    btnSub.Attributes.Add("onClick", "Subj()")
                    btnMatter.Attributes.Add("onClick", "Matter()")
                    btnRemark.Attributes.Add("onClick", "Remark()")
                    btnSign.Attributes.Add("onClick", "Signature()")
                    btnParent.Attributes.Add("onClick", "Ack_parent()")
                   
                    rbMatter.SelectedIndex = 0
                    rbRemark.SelectedIndex = 0
                    rbParent.SelectedIndex = 0

                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))


                        Call Offer_letter(ViewState("viewid"))
                        disable_control()
                    ElseIf ViewState("datamode") = "add" Then
                        enable_control()
                        reset_state()

                    End If
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

       
    End Sub

    Sub Offer_letter(ByVal BAL_ID As String)
        Try
            Using readerOFFER_LETTER As SqlDataReader = AccessStudentClass.GetOffer_LetterDATA(BAL_ID)

                If readerOFFER_LETTER.HasRows = True Then
                    While readerOFFER_LETTER.Read
                        txtSub.Text = Convert.ToString(readerOFFER_LETTER("BAL_SUB"))
                        txtMatter.Text = Convert.ToString(readerOFFER_LETTER("BAL_MAT"))

                        txtRemark.Text = Convert.ToString(readerOFFER_LETTER("BAL_REM"))
                        txtSign.Text = Convert.ToString(readerOFFER_LETTER("BAL_SIGN"))
                        txtAck.Text = Convert.ToString(readerOFFER_LETTER("BAL_ACK"))

                        chkYes.Checked = Convert.ToBoolean(readerOFFER_LETTER("BAL_bDefault"))
                        rbMatter.ClearSelection()
                        rbMatter.Items.FindByValue(readerOFFER_LETTER("BAL_MAT_BLP_ID")).Selected = True

                        rbRemark.ClearSelection()
                        rbRemark.Items.FindByValue(readerOFFER_LETTER("BAL_REM_BLP_ID")).Selected = True

                        rbParent.ClearSelection()
                        rbParent.Items.FindByValue(readerOFFER_LETTER("BAL_ACK_BLP_ID")).Selected = True

                        


                        If Not ddlOfferLetter.Items.FindByValue(Convert.ToString(readerOFFER_LETTER("BAL_APL_ID"))) Is Nothing Then
                            ddlOfferLetter.ClearSelection()
                            ddlOfferLetter.Items.FindByValue(Convert.ToString(readerOFFER_LETTER("BAL_APL_ID"))).Selected = True
                            ddlOfferLetter.Enabled = False
                        End If

                    End While
                End If
               

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try



    End Sub
    Sub BindApplicationDecision()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT APL_ID,APL_DESCR FROM APPLICATIONDECISION_M WHERE APL_BSU_ID='" & Session("sBsuid") & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlOfferLetter.DataSource = ds
        ddlOfferLetter.DataTextField = "APL_DESCR"
        ddlOfferLetter.DataValueField = "APL_ID"
        ddlOfferLetter.DataBind()
    End Sub
    Sub loadMatter_Dropdown()
        Using MatterPattern_reader As SqlDataReader = AccessStudentClass.GetMatter_Pattern()

            Dim MatterPattern As ListItem
            ddlMatter.Items.Clear()

            If MatterPattern_reader.HasRows = True Then
                While MatterPattern_reader.Read
                    MatterPattern = New ListItem(MatterPattern_reader("BLP_DESC"), MatterPattern_reader("BLP_PATTERN"))
                    ddlMatter.Items.Add(MatterPattern)

                End While

            End If
        End Using
    End Sub


    Sub loadPattern_Dropdown()
        Using Pattern_reader As SqlDataReader = AccessStudentClass.GetLetter_Pattern()

            'Dim Pattern As ListItem
            ddlSub.Items.Clear()
            ddlRemark.Items.Clear()
            ddlSign.Items.Clear()
            ddlParent.Items.Clear()
            If Pattern_reader.HasRows = True Then
                While Pattern_reader.Read
                    'Pattern = New ListItem(Pattern_reader("BLP_DESC"), Pattern_reader("BLP_PATTERN"))
                    ddlSub.Items.Add(New ListItem(Pattern_reader("BLP_DESC"), Pattern_reader("BLP_PATTERN")))
                    ddlRemark.Items.Add(New ListItem(Pattern_reader("BLP_DESC"), Pattern_reader("BLP_PATTERN")))
                    ddlSign.Items.Add(New ListItem(Pattern_reader("BLP_DESC"), Pattern_reader("BLP_PATTERN")))
                    ddlParent.Items.Add(New ListItem(Pattern_reader("BLP_DESC"), Pattern_reader("BLP_PATTERN")))
                End While
                'ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using
    End Sub
    
    Sub LoadPattern_Radio()
        Using Pattern_Radio_reader As SqlDataReader = AccessStudentClass.GetBody_Tag()

            '   Dim Pattern_Radio As ListItem
            rbMatter.Items.Clear()
            rbParent.Items.Clear()
            rbRemark.Items.Clear()
            rbRemark.CssClass = "field-label"            
            rbMatter.CssClass = "field-label"
            rbParent.CssClass = "field-label"
            If Pattern_Radio_reader.HasRows = True Then
                While Pattern_Radio_reader.Read
                    ' Pattern_Radio = New ListItem(Pattern_Radio_reader("BLP_DESC"), Pattern_Radio_reader("BLP_ID"))
                    rbMatter.Items.Add(New ListItem(Pattern_Radio_reader("BLP_DESC"), Pattern_Radio_reader("BLP_ID")))
                    rbParent.Items.Add(New ListItem(Pattern_Radio_reader("BLP_DESC"), Pattern_Radio_reader("BLP_ID")))
                    rbRemark.Items.Add(New ListItem(Pattern_Radio_reader("BLP_DESC"), Pattern_Radio_reader("BLP_ID")))
                End While
                'ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using
        rbRemark.CssClass = "field-label"
        rbMatter.CssClass = "field-label"
        rbParent.CssClass = "field-label"
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            disable_control()
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call reset_state()


                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try


    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If ViewState("viewid") = 0 Then
            lblError.Text = "No records to delete"
            Exit Sub
        End If
        Dim str_err As String = String.Empty
        Dim DeleteMessage As String = String.Empty
        str_err = callDelete(DeleteMessage)
        If str_err = "0" Then
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            lblError.Text = "Record Deleted Successfully"
            reset_state()
            disable_control()
            
        Else
            lblError.Text = DeleteMessage
        End If



    End Sub

    Function callDelete(ByRef DeleteMessage As String) As Integer
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'delete needs to be modified based on the trigger

                Status = AccessStudentClass.DeleteBSU_APPL_LETTER(ViewState("viewid"), transaction)


                If Status <> 0 Then
                    callDelete = "1"
                    DeleteMessage = "Error Occured While Deleting."
                    Return "1"

                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        callDelete = "1"
                        DeleteMessage = "Could not complete your request"
                        Return "1"

                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("viewid") = 0

                End If
            Catch ex As Exception
                callDelete = "1"
                DeleteMessage = "Error occured while Deleting the record."
            Finally
                If callDelete <> "0" Then
                    UtilityObj.Errorlog(DeleteMessage)
                    transaction.Rollback()
                Else
                    DeleteMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        enable_control()
       
        ViewState("datamode") = "add"
        Call reset_state()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        enable_control()
        ddlOfferLetter.Enabled = False
        ViewState("datamode") = "edit"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"
            reset_state()
            Call disable_control()
        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim BAL_ID As String = "0"
        Dim BAL_APL_ID As String = ddlOfferLetter.SelectedItem.Value
        Dim BAL_BSU_ID As String = Session("sBsuid")
        Dim BAL_SUB As String = txtSub.Text
        Dim BAL_MAT As String = txtMatter.Text
        Dim BAL_REM As String = txtRemark.Text
        Dim BAL_SIGN As String = txtSign.Text
        Dim BAL_ACK As String = txtAck.Text
        Dim BAL_bDefault As Boolean = chkYes.Checked
        Dim BAL_MAT_BLP_ID As String = String.Empty
        Dim BAL_REM_BLP_ID As String = String.Empty
        Dim BAL_ACK_BLP_ID As String = String.Empty
        For Each item As ListItem In rbMatter.Items
            If item.Selected = True Then
                BAL_MAT_BLP_ID = item.Value
            End If
        Next
        For Each item As ListItem In rbRemark.Items
            If item.Selected = True Then
                BAL_REM_BLP_ID = item.Value
            End If
        Next
        For Each item As ListItem In rbParent.Items
            If item.Selected = True Then
                BAL_ACK_BLP_ID = item.Value
            End If
        Next



        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Dim status As Integer
                    bEdit = False



                    status = AccessStudentClass.SaveBSU_APPLIC_LETTER(BAL_ID, BAL_BSU_ID, BAL_SUB, _
                       BAL_MAT, BAL_REM, BAL_SIGN, BAL_ACK, BAL_bDefault, BAL_APL_ID, _
 BAL_MAT_BLP_ID, BAL_REM_BLP_ID, BAL_ACK_BLP_ID, bEdit, transaction)
                    If status = 777 Then
                        calltransaction = "1"
                        errorMessage = "Duplicate entry for application decision is not allowed"
                        Return "1"
                    ElseIf status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If






                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"


                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim status As Integer

                    BAL_ID = ViewState("viewid")

                    bEdit = True
                    status = AccessStudentClass.SaveBSU_APPLIC_LETTER(BAL_ID, BAL_BSU_ID, BAL_SUB, _
                       BAL_MAT, BAL_REM, BAL_SIGN, BAL_ACK, BAL_bDefault, BAL_APL_ID, _
 BAL_MAT_BLP_ID, BAL_REM_BLP_ID, BAL_ACK_BLP_ID, bEdit, transaction)
                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If


                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If

                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                    disable_control()
                    ViewState("viewid") = 0
                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        End If
    End Function
   
   
    Sub reset_state()
        txtSub.Text = ""
        txtMatter.Text = ""
        txtRemark.Text = ""
        txtSign.Text = ""
        txtAck.Text = ""
    End Sub
    Sub disable_control()

        txtSub.Enabled = False
        txtMatter.Enabled = False
        txtRemark.Enabled = False
        txtSign.Enabled = False
        txtAck.Enabled = False
        ddlOfferLetter.Enabled = False
        chkYes.Enabled = False
        rbMatter.Enabled = False
        ddlMatter.Enabled = False
        btnMatter.Enabled = False
        rbRemark.Enabled = False
        ddlRemark.Enabled = False
        btnRemark.Enabled = False
        ddlSign.Enabled = False
        btnSign.Enabled = False
        rbParent.Enabled = False
        ddlParent.Enabled = False
        btnParent.Enabled = False
      
    End Sub
    Sub enable_control()
        txtSub.Enabled = True
        txtMatter.Enabled = True
        txtRemark.Enabled = True
        txtSign.Enabled = True
        txtAck.Enabled = True
        ddlOfferLetter.Enabled = True
        chkYes.Enabled = True
        rbMatter.Enabled = True
        ddlMatter.Enabled = True
        btnMatter.Enabled = True
        rbRemark.Enabled = True
        ddlRemark.Enabled = True
        btnRemark.Enabled = True
        ddlSign.Enabled = True
        btnSign.Enabled = True
        rbParent.Enabled = True
        ddlParent.Enabled = True
        btnParent.Enabled = True
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
End Class
