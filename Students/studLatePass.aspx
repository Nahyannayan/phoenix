<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studLatePass.aspx.vb" Inherits="Students_studLatePass" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
 <%--   <link href="../cssfiles/newstyle.css" rel="stylesheet" type="text/css" />--%>

    <script src="js/common.js" type="text/javascript"></script>
    <style type="text/css">
           .btnvisible {
            display: none;
        }


 
.school-menu ul{
  float: none !important;
  }


.school-menu ul {
  float: left;
  margin: 0;
  padding: 15px 4px 17px 0;
  list-style: none;
  -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
  -moz-box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
  box-shadow: none;
  padding: 0px;
  margin-bottom: 0px;
}
.school-menu ul li {
  display: inline-block;
  margin-right: 10px;
  position: relative;
  padding: 10px 20px;
  background: #fff;
  cursor: pointer;
  -webkit-transition: all 0.2s;
  -moz-transition: all 0.2s;
  -ms-transition: all 0.2s;
  -o-transition: all 0.2s;
  transition: all 0.2s;
  border-radius: 10px 10px 0px 0px;
  border: 1px solid #808080;
  margin-bottom: -4px;
  border-bottom: none;
  background: rgb(193,194,194); /* Old browsers */
  text-decoration:none;
background: -moz-linear-gradient(left,  rgba(193,194,194,1) 0%, rgba(234,234,234,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(193,194,194,1)), color-stop(100%,rgba(234,234,234,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(left,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%); /* IE10+ */
background: linear-gradient(to right,  rgba(193,194,194,1) 0%,rgba(234,234,234,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ); /* IE6-9 */

}
.school-menu ul li:hover {
  /*background: #cb091f;*/
  background-color:#cecece !important;
 color: #fff;
 background: -moz-linear-gradient(left,  rgba(234,234,234,1) 0%, rgba(193,194,194,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(234,234,234,1)), color-stop(100%,rgba(193,194,194,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(left,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%); /* IE10+ */
background: linear-gradient(to right,  rgba(234,234,234,1) 0%,rgba(193,194,194,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c1c2c2', endColorstr='#eaeaea',GradientType=1 ); /* IE6-9 */

}
.school-menu ul li ul {
  padding: 0;
  position: absolute;
  top: 40px;
  left: 0;

  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
  display: none;
  opacity: 0;
  visibility: hidden;
  -webkit-transiton: opacity 0.2s;
  -moz-transition: opacity 0.2s;
  -ms-transition: opacity 0.2s;
  -o-transition: opacity 0.2s;
  -transition: opacity 0.2s;
  z-index: 1000;  
}
.school-menu ul li ul li { 

  display: block;
  border-radius: 0px; 
  color: #fff;
  text-shadow: 0 -1px 0 #000;
  
  border-bottom: 1px solid #333;
}
.school-menu ul li ul li:nth-child(1) {
        background:rgba(209,9,31,0.95);
    }
    .school-menu ul li ul li:nth-child(2) {
        background:rgba(207,91,31,0.95);
    }
    .school-menu ul li ul li:nth-child(3) {
        background:rgba(211,147,36,0.95);
    }
    .school-menu ul li ul li:nth-child(4) {
        background:rgba(88,115,56,0.95);
    }
    .school-menu ul li ul li:nth-child(5) {
        background:rgba(51,92,130,0.95);
    }
    .school-menu ul li ul li:nth-child(6) {
        background:rgba(22,43,83,0.95);
    }
    .school-menu ul li ul li:nth-child(7) {
        background:rgba(178,75,148,0.95);
    }
.school-menu ul li ul li:hover { //background: #666; }
.school-menu ul li:hover ul {
  display: block;
  opacity: 1;
  visibility: visible;
}

.school-menu ul li a {
    color: #333;
}
    </style>

    <script type="text/javascript">




        function GetStudent() {
            var NameandCode;
            var NameandCode;
            var result;
            var STUD_TYP = '0';
            var selType = '';
            var selACD_ID = '';
            var selDATE = '';
            var TYPE;
            if (selDATE == '')
            { if (STUD_TYP == '0') TYPE = 'stud'; else TYPE = 'TC'; }
            else
            { if (STUD_TYP == '0') TYPE = 'ENQ_DATE'; else TYPE = 'STUD_DATE'; }
            var url;

            if (STUD_TYP == "0")
                url = 'ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + selType + '&ACD_ID=' + selACD_ID + '&VALDATE=' + selDATE;
            else
                url = 'ShowStudent.aspx?TYPE=' + TYPE + '&VAL=' + selType + '&ACD_ID=' + selACD_ID + '&VALDATE=' + selDATE;

            //result = window.showModalDialog(url, "", sFeatures);
            var oWnd = radopen(url, "pop_student");

        }

        function OnClientClose(oWnd, args) {
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Student.split('||');

                document.getElementById('<%= h_Student_id.ClientID %>').value = NameandCode[0];
                document.getElementById('<%= txtStudNo.ClientID %>').value = NameandCode[2];
                __doPostBack('<%= txtStudNo.ClientID%>', 'TextChanged');
            }
        }

        function doClick(buttonName, e) {
            //the purpose of this function is to allow the enter key to 
            //point to the correct button to click.
            var key;

            if (window.event)
                key = window.event.keyCode;     //IE
            else
                key = e.which;     //firefox

            if (key == 13) {
                //Get the button the user wants to have clicked
                var btn = document.getElementById(buttonName);
                if (btn != null) { //If we find the button click it
                    btn.click();
                    event.keyCode = 0
                }
            }
        }
        function checkItem(o) {
            var i = o.value.length;

            if (i > 13) //trap enter
            {
                var str = document.getElementById('<%= txtStudNo.ClientID %>').value;
                document.getElementById('<%= txtStudNo.ClientID %>').value = str.substr(0, 14);
                // document.getElementById('<%=divLate.ClientID%>').style.display = "block";
                document.getElementById('<%= btnTest.ClientID %>').click();

                //                PageMethods.Displayitems();
                //                PageMethods.bindReasons();
                //                PageMethods.BIND_TIME();
                // document.getElementById('<%=txtStudNo.ClientID%>').TextChanged()
                // if (document.activeElement.type != 'textarea') { //if not textarea type
                event.keyCode = 9;
                return event.keyCode; //convert to Tab key
                //}
            }
        }
        function CHECK_DROPDOWN(DD) {
            var key = String.fromCharCode(event.keyCode);

            DD.value = key;
            document.getElementById('<%= btnSav.ClientID %>').click();

        }
        function CHECK_reason(DD) {
            var i = DD.value.length;

            if (i > 1) { document.getElementById('<%= btnSav.ClientID %>').click(); }


}

//        function doPostBack(o) {
//            if (this.value.length == 13) __dopostback(o.id);
//            
//        }



function autoSizeWithCalendar(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    var height = body.scrollHeight;
    var width = body.scrollWidth;

    var iframeBounds = $telerik.getBounds(iframe);
    var heightDelta = height - iframeBounds.height;
    var widthDelta = width - iframeBounds.width;

    if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
    if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
    oWindow.center();
}
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="800px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Late Pass"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div>
                    <asp:Button ID="btnTest" runat="server" OnClick="btnTest_Click" CssClass="btnvisible" />
                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                        SkinID="error"></asp:Label>
                </div>
                         <table width="100%">

                    <tr>
                        <td align="left" class="matters"  ><span class="field-label"> Student No</span>
                        </td>
                        <td align="left" class="matters"  >
                            <asp:TextBox ID="txtStudNo" runat="server"  onkeyup="checkItem(this);"
                                OnTextChanged="txtStudNo_TextChanged" AutoPostBack="true"> </asp:TextBox>&nbsp;<asp:ImageButton
                                    ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetStudent();return false;"
                                    TabIndex="7" />
                        </td>
                        <td>
                        </td>
                        <td></td>
                    </tr>
                </table>

                <div id="divLate" runat="server" visible="false">
                    <div  >
                        <div class="school-menu">
                            <ul>
                                <li><a href="#" id="hrefReason" class="tabLink" runat="server">Late reason</a> </li>
                                <li><a href="#" id="hrefHistory" class="tabLink" runat="server">History</a> </li>
                            </ul>
                        </div>
                    </div>
                    
                    <div id="divReason" runat="server"  width="100%">
                        <div class="title-bg-small">
                            <span>Late Reason</span>
                        </div>
                        <div style="display: block; width: 100%; text-align: left;">
                                <%--<asp:DropDownList ID="ddlreason1" runat="server" AutoPostBack="true" onkeyup="CHECK_DROPDOWN(this);" 
                        Width="150px" TabIndex="1">
                    </asp:DropDownList>--%>
                                <table  width="100%">
                                    <tr>
                                        <td width="20%"> <span class="field-label">Late Reason</span></td>
                                        <td width="30%">
                                            <asp:TextBox ID="txtreason" runat="server" onkeyup="CHECK_reason(this);" OnTextChanged="txtreason_TextChanged" AutoPostBack="true"></asp:TextBox> 

                                        </td>
                                        <td width="20%"> <span class="field-label">Time</span> </td>
                                        <td width="30%"><asp:TextBox ID="txtTime" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                                ControlToValidate="txtreason"
                                                ValidationExpression="\d+"
                                                Display="Static"
                                                EnableClientScript="true"
                                                ErrorMessage="Please enter numbers only"
                                                runat="server" /></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <span class="field-label"> Remarks(Optional)</span>
                                       </td>
                                        <td>
                                            <asp:TextBox ID="txtdesc" runat="server" TextMode="MultiLine" SkinID="MultiText" Width="250px" TabIndex="3"></asp:TextBox>
                                        </td>
                                        <td><asp:CheckBox ID="cbAlert" runat="server" Text="Alert Parent" CssClass="field-label" /></td>
                                        <td>  <asp:CheckBox ID="cbPrint" runat="server" Checked="true" Text="Print Receipt" CssClass="field-label" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">   <asp:Button ID="btnSav" runat="server" CssClass="button" Text="Save" TabIndex="4" /></td>
                                    </tr>
                                </table>



                                <%--<asp:DropDownList ID="ddlHr" runat="server" AutoPostBack="true"></asp:DropDownList>Hr
                                <asp:DropDownList ID="ddlMin" runat="server" AutoPostBack="true"></asp:DropDownList>Min--%>
                            </div>
                    </div>

                    <div id="divHistory" runat="server" visible="false" >
                        <div class="title-bg-small">
                            Late History 
                        </div>
                        <div style="display: block; width: 100%; text-align: center;">
                            <asp:GridView ID="gvLate" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" >
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                        <ItemStyle HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("SLS_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemStyle HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lbldate" runat="server" Text='<%# Bind("SLS_DATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason">
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblreason" runat="server" Text='<%# Bind("LR_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Time">
                                        <ItemStyle HorizontalAlign="center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblTime" runat="server" Text='<%# Bind("SLS_TIME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("SLS_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Alert" Visible="false">
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAlert" runat="server" Text='<%# Bind("SLS_bALERT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField CommandName="Alert" ImageUrl="~\images\tick.gif" ButtonType="Image"
                                        HeaderText="Alert">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lnkp" CommandName="Print" Text="Print" CommandArgument='<%# Bind("SLS_ID") %>'
                                                OnClick="lnkp_Click">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle   Wrap="False" />
                                <RowStyle  Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                            </asp:GridView>
                        </div>
                    </div>
                <table  width="100%">
                                <tr>
                                    <td width="50%" style="vertical-align:top">
                                        <div  class="panel-cover">
                                            <div class="title-bg-small" >
                                                <span>Student Details</span>
                                            </div>
                                            <div style="display: block; width: 100%; text-align: left;">
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">Student Name : <strong>
                                                        <asp:Label ID="lblStudname" runat="server" Text="" /></strong></span>
                                                </div>
                                                <div style="float: right;">
                                                    <asp:Image ImageUrl="~/images/no_image.jpg" ID="imgStud" runat="server" Width="120px"  />
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">Academic Year :
                                    <asp:Label ID="lblAcyear" runat="server" Text="" /></span>
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">Grade / Section :
                                    <asp:Label ID="lblGrade" runat="server" Text="" /></span>
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">Parent Name :
                                    <asp:Label ID="lblParentName" runat="server" Text="" /></span>
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">Parent Mobile :
                                    <asp:Label ID="lblParentMob" runat="server" Text="" /></span>
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">Parent Email :
                                    <asp:Label ID="lblParentEmail" runat="server" Text="" /></span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="50%" style="vertical-align:top">
                                        <div class="panel-cover" >
                                            <div class="title-bg-small">
                                                <span>Transport Details</span>
                                            </div>
                                            <div style="display: block; width: 100%; text-align: left;">
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">Pick Up Bus No :
                                    <asp:Label ID="lblPickupBus" runat="server" Text="" /></span>
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">DropOff Bus No :
                                    <asp:Label ID="lblDropBus" runat="server" Text="" /></span>
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">Pick Up Area :
                                    <asp:Label ID="lblPickupArea" runat="server" Text="" /></span>
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">DropOff Area :
                                    <asp:Label ID="lblDropArea" runat="server" Text="" /></span>
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">Pick Up Point :
                                    <asp:Label ID="lblPickupPoint" runat="server" Text="" /></span>
                                                </div>
                                                <div style="margin-top: 15px; padding-left: 10px;">
                                                    <span class="Latespan">DropOff Point :
                                    <asp:Label ID="lblDropPoint" runat="server" Text="" /></span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                </div>
                <div id="maindiv" runat="server" visible="false">
                    <asp:Button ID="btnPrint" runat="server" />
                </div>
                <asp:HiddenField ID="h_Student_id" runat="server" />
                <asp:HiddenField ID="h_ACD_ID" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
                <CR:CrystalReportViewer ID="CrystalReportViewer2" runat="server" />
                <div id="divPrint" runat="server"></div>


                <%--<script language='VBScript'>
Sub Print()
       OLECMDID_PRINT = 6
       OLECMDEXECOPT_DONTPROMPTUSER = 2
       OLECMDEXECOPT_PROMPTUSER = 1
      call WB.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER,1)

End Sub
document.write "<object ID='WB' WIDTH=0 HEIGHT=0 CLASSID='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>"
</script>--%>

                <script type="text/javascript">

                    function CallPrint() {
                        var prtContent = document.getElementById('<%= divPrint.clientID%>');

            var WinPrint =
 window.open('', '', 'letf=0,top=0,width=400px,height=200px,toolbar=0,scrollbars=0,sta�tus=0');

            WinPrint.document.write('<html><head></head><body >' + prtContent.innerHTML + '</body></html>');

            // WinPrint.document.write(prtContent.innerHTML); 
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            //print();
            WinPrint.close();
            prtContent.innerHTML = '';

        }



        function setfocus() {




        }
                </script>
            </div>
        </div>
    </div>

</asp:Content>
