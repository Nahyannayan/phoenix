<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studSiblingsDelink.aspx.vb" Inherits="Students_studSiblingsDelink" Title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>

    <script language="javascript" type="text/javascript">

        function getSibling(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 845px; ";
            sFeatures += "dialogHeight: 510px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Students/ShowSiblingInfo.aspx?id=' + mode;

            if (mode == 'SE') {
                //result = window.showModalDialog(url,"", sFeatures);
                var oWnd = radopen(url, "pop_usr");
            <%--if (result=='' || result==undefined)
                    {    return false; 
                    }   
                 NameandCode = result.split('___');
                
                 document.getElementById("<%=txtPar_Sib.ClientID %>").value=NameandCode[0];
                 document.getElementById("<%=h_SliblingID.ClientID %>").value=NameandCode[1];
               
               }--%>
        }

    }

        function setfocs() {
            document.getElementById("<%=txtPar_Sib.ClientID %>").focus()
           }



           function OnClientClose(oWnd, args) {
              var arg = args.get_argument();

               if (arg) {
                   NameandCode = arg.Result.split('___');
                   document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=h_SliblingID.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtPar_Sib.ClientID%>', 'TextChanged');
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Student Siblings
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" width="100%" align="center" cellpadding="0" cellspacing="0">
                   <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left"><span class="field-label">Student No</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPar_Sib" runat="server" CssClass="error" Width="197px" OnTextChanged="txtPar_Sib_TextChanged" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnSibling" runat="server" Height="16px"
                                            ImageUrl="~/Images/forum_search.gif" OnClientClick="getSibling('SE');return false;"
                                            OnClick="imgbtnSibling_Click"></asp:ImageButton></td>
                                    <td  colspan="2">
                                        <asp:Label ID="lblStudName" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                
                                <tr>
                                    <td align="center"  colspan="3">
                                        <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"  Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                     Select 
                                                    </HeaderTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Business Unit">
                                                    <ItemStyle></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBsuName" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                        <asp:Label ID="stuId" runat="server" Text='<%# bind("Stu_Id") %>' Visible="False"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Applicant Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblApplName" runat="server" Text="Student Name" CssClass="gridheader_text"></asp:Label>
                                                               
                                                    </HeaderTemplate>

                                                    <ItemStyle></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkApplName1" runat="server" CommandName="Select" Text='<%# Bind("STU_FIRSTNAME") %>' Visible="False"></asp:LinkButton>
                                                        <asp:Label ID="lnkApplName" runat="server" Text='<%# Bind("STU_FIRSTNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                      <asp:Label ID="lblH12" runat="server" Text="Grade" CssClass="gridheader_text"></asp:Label>
                                                           
                                                    </HeaderTemplate>

                                                    <ItemStyle></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRADE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Shift">
                                                    <HeaderTemplate>
                                                        Shift
                                                    </HeaderTemplate>

                                                    <ItemStyle></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("SHIFT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stream">
                                                    <HeaderTemplate>
                                                        Stream
                                                    </HeaderTemplate>

                                                    <ItemStyle></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("STREAM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop"/>
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="Button1" runat="server" CssClass="button" OnClientClick="return confirm_enroll();"
                                            TabIndex="4" Text="Save" ValidationGroup="groupM1" SkinID="ButtonNormal" OnClick="Button1_Click" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" TabIndex="4" Text="Cancel" ValidationGroup="groupM1" SkinID="ButtonNormal" OnClick="btnCancel_Click" OnClientClick="setfocs()" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_SliblingID" runat="server" />
                            <asp:HiddenField ID="h_NewSiblingID"
                                runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_StudentId" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

    <telerik:radwindowmanager id="RadWindowManager1" showcontentduringload="false" visiblestatusbar="false"
        reloadonshow="true" runat="server" enableshadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_usr" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>
       </telerik:radwindowmanager>


</asp:Content>

