<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studEnquiryNew.aspx.vb" Inherits="Students_studEnquiryNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <style>
        input {
            vertical-align: middle !important;
        }

        table th {
            pading: 0 !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudEnquiry.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }



        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click(); //fire the click event of the child element
                    }
                    if (chkstate = true) {

                    }
                }
            }
        }







    </script>
    <script language="javascript" type="text/javascript">
        function confirm_cancel() {
            if (confirm("You are about to cancel this enquiry.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Enquiry
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--<table width="100%" id="Table1" border="0">
        <tr>
            <td width="50%" align="left" class="title">
                 
            </td>
        </tr>
    </table>--%>
                <table id="tbl_ShowScreen" runat="server" align="left" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" valign="middle">
                            <table align="left" border="0" cellpadding="0" cellspacing="0"
                                style="width: 100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>

                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Prev School GEMS </span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlPrevSchool_BSU" SkinID="smallcmb" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4"></td>
                                </tr>
                                <tr>

                                    <%-- <td align="left" colspan="2">
                                        <table width="100%">--%>
                                    <%--<tr>--%>
                                    <td colspan="2">
                                        <asp:RadioButton ID="rbOpen" runat="server" AutoPostBack="True" GroupName="ENQ" OnCheckedChanged="rbOpen_CheckedChanged"
                                            Text="Open" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rbCancel" runat="server" AutoPostBack="True" GroupName="ENQ"
                                            OnCheckedChanged="rbCancel_CheckedChanged" Text="Cancelled" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" GroupName="ENQ" OnCheckedChanged="rbAll_CheckedChanged"
                                            Text="All" CssClass="field-label"></asp:RadioButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Status </span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEnqStatus" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="0">ALL</asp:ListItem>
                                            <asp:ListItem Value="2">ENQUIRY</asp:ListItem>
                                            <asp:ListItem Value="3">REGISTER</asp:ListItem>
                                            <asp:ListItem Value="4">SCREENING</asp:ListItem>
                                            <asp:ListItem Value="5">APPROVAL</asp:ListItem>
                                            <asp:ListItem Value="6">OFFER LETTER</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <span class="field-label">Contact Me </span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlContactMe" SkinID="smallcmb" runat="server" AutoPostBack="true">
                                            <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                            <asp:ListItem Value="YES">YES</asp:ListItem>
                                            <asp:ListItem Value="NO">NO</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <span class="field-label">Paid </span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPaid" SkinID="smallcmb" runat="server" AutoPostBack="true">
                                            <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                            <asp:ListItem Value="YES">YES</asp:ListItem>
                                            <asp:ListItem Value="NO">NO</asp:ListItem>
                                            <asp:ListItem Value="AtSchool">At School</asp:ListItem>
                                            <asp:ListItem Value="Online">Online</asp:ListItem>
                                            <asp:ListItem Value="NotPaidReg">Not Paid-Registered</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <span class="field-label">Follow Up Status</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlFollowupStatus" SkinID="smallcmb" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <%--</table>
                                    </td>    --%>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSIB" runat="server" CssClass="button" OnClick="btnSIB_Click" Text="Show Enquiry With Sibling" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="left"
                            valign="middle">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <table align="center" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Label ID="lblSTAT" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" valign="middle">
                                        <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found" HeaderStyle-Height="30"
                                            PageSize="20" DataKeyNames="Eqs_id" OnPageIndexChanged="gvStudEnquiry_PageIndexChanged">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this);" runat="server"
                                                            ToolTip="Click here to select/deselect all rows"></asp:CheckBox>
                                                        <%--<table>
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RowNumber" HeaderText="SI.NO" />
                                                <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("Eqs_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Enq No.">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqHeader" runat="server" Text="Enq No" CssClass="gridheader_text"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqSearch" runat="server">
                                                        </asp:TextBox>
                                                        <asp:ImageButton ID="btnEnqid_Search" OnClick="btnEnqid_Search_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                    </HeaderTemplate>

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqApplNo" runat="server" Text='<%# Bind("Eqs_ApplNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="8%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Account No.">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblAccHeader" runat="server" Text="Acc No." CssClass="gridheader_text"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtAccSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnAcc_Search" OnClick="btnAcc_Search_Click" runat="server"
                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccNo" runat="server" Text='<%# Bind("Eqs_AccNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="8%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Enq Date">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" CssClass="gridheader_text" Text="Enq.Date"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnq_Date" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnEnq_Date_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("eqm_enqdate", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="8%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Regn Date">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblRegnDate" runat="server" CssClass="gridheader_text" Text="Regn Date"></asp:Label>
                                                        <%--  <table>
                                                            <tr>
                                                                <td align="center">
                                                                   
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRegnDate" runat="server" Text='<%# Bind("EQS_REGNDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnGrade_Search_Click" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="8%" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Age" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPPL_DATE" runat="server" Text='<%# bind("Appl_Age") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle />
                                                    <ItemStyle />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Shift" Visible="False">
                                                    <HeaderTemplate>
                                                        Shift
                                                        <br />
                                                        <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <%--<table>
                                                            <tr>
                                                                <td align="center">

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                   
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stream">
                                                    <HeaderTemplate>
                                                        Stream<br />
                                                        <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged" Width="100%">
                                                        </asp:DropDownList>
                                                        <%--<table>
                                                            <tr>
                                                                <td align="center">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                   
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="5%" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Applicant Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblApplName" runat="server" Text="Applicant Name" CssClass="gridheader_text"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtApplName" runat="server">
                                                        </asp:TextBox>
                                                        <asp:ImageButton ID="btnAppl_Name" OnClick="btnAppl_Name_Click" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                            ImageAlign="Top"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkApplName" OnClick="lnkApplName_Click" runat="server" OnClientClick='<%# GetNavigateUrl(Eval("eqs_id").tostring()) %>'
                                                            Text='<%# Bind("appl_name") %>' CommandName="Select" ToolTip="Clicking on this link will show the list of documents which has to be submitted for  each stage"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="8%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Gender" HeaderStyle-Wrap="true">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH122" runat="server" CssClass="gridheader_text" Text="Gender"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtGender" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnGender_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnGender_Search_Click" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="8%" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGender" runat="server" Text='<%# Bind("Gender") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="Register" ImageUrl="~\images\tick.gif" ButtonType="Image"
                                                    HeaderText="Reg">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="Screening" ImageUrl="~\images\tick.gif" ButtonType="Image"
                                                    HeaderText="Scr">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="Approval" ImageUrl="~\images\tick.gif" ButtonType="Image"
                                                    HeaderText="App">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="Offer" ImageUrl="~\images\tick.gif" ButtonType="Image"
                                                    HeaderText="Off">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="STG1REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG1REQD" runat="server" Text='<%# Bind("STG1REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG1COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG1COMP" runat="server" Text='<%# Bind("STG1COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG2REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG2REQD" runat="server" Text='<%# Bind("STG2REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG2COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG2COMP" runat="server" Text='<%# Bind("STG2COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG3REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG3REQD" runat="server" Text='<%# Bind("STG3REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG3COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG3COMP" runat="server" Text='<%# Bind("STG3COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG4REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG4REQD" runat="server" Text='<%# Bind("STG4REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG4COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG4COMP" runat="server" Text='<%# Bind("STG4COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG5REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG5REQD" runat="server" Text='<%# Bind("STG5REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG5COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG5COMP" runat="server" Text='<%# Bind("STG5COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG6REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG6REQD" runat="server" Text='<%# Bind("STG6REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG6COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG6COMP" runat="server" Text='<%# Bind("STG6COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG7REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG7REQD" runat="server" Text='<%# Bind("STG7REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG7COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG7COMP" runat="server" Text='<%# Bind("STG7COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG8REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG8REQD" runat="server" Text='<%# Bind("STG8REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG8COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG8COMP" runat="server" Text='<%# Bind("STG8COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG9REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG9REQD" runat="server" Text='<%# Bind("STG9REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG9COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG9COMP" runat="server" Text='<%# Bind("STG9COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG10REQD" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG10REQD" runat="server" Text='<%# Bind("STG10REQD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STG10COMP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTG10COMP" runat="server" Text='<%# Bind("STG10COMP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD1" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD1" runat="server" Text='<%# Bind("ORD1") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD2" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD2" runat="server" Text='<%# Bind("ORD2") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD3" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD3" runat="server" Text='<%# Bind("ORD3") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD4" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD4" runat="server" Text='<%# Bind("ORD4") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD5" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD5" runat="server" Text='<%# Bind("ORD5") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD6" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD6" runat="server" Text='<%# Bind("ORD6") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD7" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD7" runat="server" Text='<%# Bind("ORD7") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD8" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD8" runat="server" Text='<%# Bind("ORD8") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD9" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD9" runat="server" Text='<%# Bind("ORD9") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORD10" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblORD10" runat="server" Text='<%# Bind("ORD10") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EnqApplNo" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqId" runat="server" Text='<%# Bind("eqm_enqid") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCurrStatus" runat="server" Text='<%# Bind("EQS_CURRSTATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDocStage" runat="server" Text='<%# Bind("EQS_DOC_STG_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAplDis" runat="server" Text='<%# Bind("EQS_APL_DISABLE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNextStage" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="Print" Text="Print" HeaderText="Print">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="APL_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("APL_ID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPL_ID" runat="server" Text='<%# Bind("APL_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EQS_STATUS" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("EQS_STATUS") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEQS_STATUS" runat="server" Text='<%# Bind("EQS_STATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reopen">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnReopen" OnClick="lbtnReopen_Click" runat="server" CausesValidation="false">Reopen</asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="cbeCancel" runat="server" TargetControlID="lbtnReopen"
                                                            ConfirmText="Are sure you want to reopen the Applicant enquiry ?">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ack1 Email">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnResend1" OnClick="lbtnResend1_Click" runat="server" CausesValidation="false">Resend</asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="cberesend1" runat="server" TargetControlID="lbtnResend1"
                                                            ConfirmText="Are sure you want to re send the Applicant enquiry Acknowledgment1 Email ?">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ack2 Email">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnResend" OnClick="lbtnResend_Click" runat="server" CausesValidation="false">Resend</asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="cberesend" runat="server" TargetControlID="lbtnResend"
                                                            ConfirmText="Are sure you want to re send the Applicant enquiry Acknowledgment2 Email ?">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cancel">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnCancel" runat="server" CausesValidation="false" OnClick="lbtnCancel_Click">Cancel</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EQS_bFOLLOWUP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEQS_bFOLLOWUP" runat="server" Text='<%# bind("EQS_bFOLLOWUP") %>'></asp:Label>
                                                        <asp:Label ID="lblReason" runat="server" Text='<%# bind("EFR_ICONS") %>'></asp:Label>
                                                        <asp:Label ID="lblTooltip" runat="server" Text='<%# bind("EFR_REASON") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="Followup" ImageUrl="~\images\tick.gif" ButtonType="Image"
                                                    HeaderText="Follow up">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="APL_IMAGE" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtAPL_IMAGE" runat="server" Text='<%# Bind("APL_IMAGE") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPL_IMAGE" runat="server" Text='<%# Bind("APL_IMAGE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="APL_TYPE" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtAPL_TYPE" runat="server" Text='<%# Bind("APL_TYPE") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPL_TYPE" runat="server" Text='<%# Bind("APL_TYPE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SEN">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgSEN" runat="server" HorizontalAlign="Center" ImageUrl='<%# BIND("EQS_bSEN") %>' />
                                                        <div>
                                                            <asp:LinkButton ID="lnkSEN" runat="server" Text="Note" CausesValidation="False" Visible='<%# BIND("bSEN") %>'
                                                                OnClientClick="return false;"></asp:LinkButton>
                                                            <div id="panel1" runat="server" style="background-color: white; overflow: auto;"
                                                                visible='<%# BIND("bSEN") %>'>
                                                                <asp:Literal ID="ltSEN" runat="server" Text='<%# BIND("EQS_SEN") %>'></asp:Literal>
                                                            </div>
                                                        </div>
                                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" PopupControlID="Panel1"
                                                            Position="Bottom" OffsetX="-100" TargetControlID="lnkSEN">
                                                        </ajaxToolkit:PopupControlExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Prev School" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrev_School" runat="server" Text='<%# bind("Prev_School_GEMS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle CssClass="gridheader_pop" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle">
                            <asp:Button ID="btnPrintOffer" runat="server" CssClass="button" OnClick="btnPrintOffer_Click"
                                Text="Print Offer Letter" Visible="false" />
                            <asp:Button ID="btnExport" runat="server" CssClass="button" OnClick="btnExport_Click"
                                Text="Export to Excel" />
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" OnClick="btnPrint_Click"
                                Text="Print Screeening Test" />
                            <asp:Button ID="btnReject" runat="server" Text="Delete" CssClass="button" OnClientClick="return confirm_cancel();" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"><i><span class="text-info">Note: Reg--Register   Scr--Screening  App--Approval 
                Off--Offer  Foll</span></i>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"
                            valign="top">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input
                                id="h_Selected_menu_12" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7"
                                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2" runat="server"
                                        type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server" type="hidden"
                                            value="=" />
                            <input id="h_Selected_menu_13" runat="server" type="hidden" value="=" />
                            <asp:HiddenField ID="hiddenENQIDs" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>



    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>


</asp:Content>
