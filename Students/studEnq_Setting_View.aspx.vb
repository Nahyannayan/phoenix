Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_stuEnquiry_Setting_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100063") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else



                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"

                    callYEAR_DESCRBind()
                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_Sid_img = h_Selected_menu_1.Value.Split("__")
        'getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))



    End Sub

    'Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
    '    If gvOnlineEnq.Rows.Count > 0 Then
    '        Dim s As HtmlControls.HtmlImage
    '        Try

    '            s = gvOnlineEnq.HeaderRow.FindControl("mnu_1_img")
    '            If p_imgsrc <> "" Then
    '                s.Src = p_imgsrc
    '            End If
    '            Return s.ClientID
    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End If
    '    Return ""
    'End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvOnlineEnq.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvOnlineEnq.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvOnlineEnq.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvOnlineEnq.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvOnlineEnq.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvOnlineEnq.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvOnlineEnq.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvOnlineEnq.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            ddlAca_Year.ClearSelection()
            ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_ACY_DESCR As String = String.Empty
            Dim str_filter_GRM_DESCR As String = String.Empty
            Dim str_filter_EQO_DESC As String = String.Empty
            Dim str_filter_FROMDT As String = String.Empty
            Dim str_filter_TODT As String = String.Empty
            Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
            Dim ds As New DataSet

            str_Sql = " select * from(SELECT DISTINCT  ACADEMICYEAR_M.ACY_DESCR, ENQUIRY_SETTINGS.EQS_ACD_ID AS ACD_ID, CURRICULUM_M.CLM_DESCR, ENQUIRY_OPTION.EQO_DESC, " & _
                     "  ENQUIRY_SETTINGS.EQS_FROMDT AS FROMDT, ENQUIRY_SETTINGS.EQS_TODT AS TODT, ENQUIRY_SETTINGS.EQS_BSU_ID AS BSU_ID, " & _
                     "  ACADEMICYEAR_D.ACD_CLM_ID, ENQUIRY_SETTINGS.EQS_CLM_ID, ENQUIRY_SETTINGS.EQS_ID, GRADE_BSU_M.GRM_DISPLAY AS GRM_DESCR, " & _
                     "  ENQUIRY_SETTINGS.EQS_GRD_ID, GRADE_M.GRD_DISPLAYORDER FROM  ENQUIRY_SETTINGS INNER JOIN " & _
                     "  ENQUIRY_OPTION ON ENQUIRY_SETTINGS.EQS_EQO_ID = ENQUIRY_OPTION.EQO_ID INNER JOIN " & _
                     "  ACADEMICYEAR_D ON ENQUIRY_SETTINGS.EQS_ACD_ID = ACADEMICYEAR_D.ACD_ID INNER JOIN " & _
                     "  CURRICULUM_M ON ENQUIRY_SETTINGS.EQS_CLM_ID = CURRICULUM_M.CLM_ID INNER JOIN " & _
                     "  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
                     "  GRADE_BSU_M ON ENQUIRY_SETTINGS.EQS_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
                     "  ENQUIRY_SETTINGS.EQS_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
                     "  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID)A where BSU_ID='" & Session("sBsuid") & "' and A.ACD_ID='" & ACD_ID & "'"








            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_ACY_DESCR As String = String.Empty
            Dim str_GRM_DESCR As String = String.Empty
            Dim str_EQO_DESC As String = String.Empty
            Dim str_FROMDT As String = String.Empty
            Dim str_TODT As String = String.Empty

            If gvOnlineEnq.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                'str_Sid_search = h_Selected_menu_1.Value.Split("__")
                'str_search = str_Sid_search(0)

                'txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtACY_DESCR")
                'str_ACY_DESCR = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_ACY_DESCR = " AND a.ACY_DESCR LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_ACY_DESCR = "  AND  NOT a.ACY_DESCR LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_ACY_DESCR = " AND a.ACY_DESCR  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_ACY_DESCR = " AND a.ACY_DESCR NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_ACY_DESCR = " AND a.ACY_DESCR LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_ACY_DESCR = " AND a.ACY_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                'End If



                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtGRM_DESCR")
                str_GRM_DESCR = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GRM_DESCR = " AND a.GRM_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GRM_DESCR = "  AND  NOT a.GRM_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GRM_DESCR = " AND a.GRM_DESCR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GRM_DESCR = " AND a.GRM_DESCR NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GRM_DESCR = " AND a.GRM_DESCR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GRM_DESCR = " AND a.GRM_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtEQO_DESC")
                str_EQO_DESC = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_EQO_DESC = " AND a.EQO_DESC LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_EQO_DESC = "  AND  NOT a.EQO_DESC  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_EQO_DESC = " AND a.EQO_DESC   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_EQO_DESC = " AND a.EQO_DESC  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_EQO_DESC = " AND a.EQO_DESC LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_EQO_DESC = " AND a.EQO_DESC  NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtFROMDT")
                str_FROMDT = txtSearch.Text
                Dim FROMDT As String = "  replace(CONVERT( CHAR(12), isnull(a.FROMDT,''), 106 ),' ','/') "
                If str_search = "LI" Then
                    str_filter_FROMDT = " AND " & FROMDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FROMDT = "  AND  NOT " & FROMDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FROMDT = " AND " & FROMDT & "  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FROMDT = " AND " & FROMDT & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FROMDT = " AND " & FROMDT & " LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FROMDT = " AND " & FROMDT & " NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtTODT")
                str_TODT = txtSearch.Text
                Dim TODT As String = "  replace(CONVERT( CHAR(12), isnull(a.TODT,''), 106 ),' ','/') "
                If str_search = "LI" Then
                    str_filter_TODT = " AND " & TODT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_TODT = "  AND  NOT " & TODT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_TODT = " AND " & TODT & "  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_TODT = " AND " & TODT & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_TODT = " AND " & TODT & " LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_TODT = " AND " & TODT & " NOT LIKE '%" & txtSearch.Text & "'"
                End If




            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_GRM_DESCR & str_filter_EQO_DESC & str_filter_FROMDT & str_filter_TODT & " ORDER BY a.GRD_DISPLAYORDER")

            If ds.Tables(0).Rows.Count > 0 Then

                gvOnlineEnq.DataSource = ds.Tables(0)
                gvOnlineEnq.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvOnlineEnq.DataSource = ds.Tables(0)
                Try
                    gvOnlineEnq.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvOnlineEnq.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvOnlineEnq.Rows(0).Cells.Clear()
                gvOnlineEnq.Rows(0).Cells.Add(New TableCell)
                gvOnlineEnq.Rows(0).Cells(0).ColumnSpan = columnCount
                gvOnlineEnq.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvOnlineEnq.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            'txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtACY_DESCR")
            'txtSearch.Text = str_ACY_DESCR

            txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtGRM_DESCR")
            txtSearch.Text = str_GRM_DESCR
            txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtEQO_DESC")
            txtSearch.Text = str_EQO_DESC
            txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtFROMDT")
            txtSearch.Text = str_FROMDT
            txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtTODT")
            txtSearch.Text = str_TODT

            '
            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btnSearchFromDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchToDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvOnlineEnq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOnlineEnq.PageIndexChanging
        gvOnlineEnq.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studEnq_Setting.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblEQS_ID As New Label
            Dim url As String
            Dim viewid As String
            lblEQS_ID = TryCast(sender.FindControl("lblEQS_ID"), Label)
            viewid = lblEQS_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studEnq_Setting.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    'Protected Sub btnSearchCLM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    gridbind()
    'End Sub

    'Protected Sub btnSearchACY_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    gridbind()
    'End Sub

    Protected Sub btnSearchEQO_DESC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        gridbind()

    End Sub


    Protected Sub btnSearchGRD_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
End Class
