Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studCert_Comm_new
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        BindPartBControls()
        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add" '"Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200448") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    chkCond1.Attributes.Add("name", chkCond1.ClientID)
                    chkCond1.Attributes.Add("onclick", "javascript:Cond1(event);")
                    ddlCond1.Attributes.Add("onChange", "Select_Cond()")
                    CBLbusnumber.Attributes.Add("onclick", "javascript:CondT(this);")
                    CBbusnoselectall.Attributes.Add("onclick", "javascript:CondTALL(this);")
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                    ElseIf ViewState("datamode") = "add" Then

                    End If

                    GEMS_BSU_4_Student()
                    ddlSchool.Enabled = False
                    ddlFilterBy.ClearSelection()
                    ddlFilterBy.Items.FindByValue("1").Selected = True
                    'bindAcademic_Section()
                    Call bindFilter_Cond_C()
                    'txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    'txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"

                    Session("PartC_CER") = CreateDataTable()
                    Session("PartC_CER").Rows.Clear()
                    ViewState("id") = 1
                    gridbind()
                    'imgCalendar1.Visible = False
                    'imgCalendar2.Visible = False
                    'txtCond2.Visible = False
                    'Dim str_search As String = String.Empty
                    'Dim str_Sid_search() As String

                    'str_Sid_search = h_Selected_menu_1.Value.Split("__")
                    'str_search = str_Sid_search(0)


                    ' Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                    ' "<script language=javascript>initialLoad_Cond();</script>")

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
        'set_Menu_Img()
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTC", _
        "<script language=javascript>initialLoad_Cond();</script>")
    End Sub

    'Private Sub set_Menu_Img()
    '    Dim str_Sid_img() As String
    '    str_Sid_img = h_Selected_menu_1.Value.Split("__")
    '    getid1(str_Sid_img(2))

    'End Sub

    'Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
    '    Try
    '        If p_imgsrc <> "" Then
    '            mnu_1_img.Src = p_imgsrc
    '        End If
    '        Return mnu_1_img.ClientID
    '    Catch ex As Exception
    '        Return ""
    '    End Try

    '    Return ""
    'End Function

    Sub GEMS_BSU_4_Student()
        Try
            Using BSU_4_Student_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_Student()

                ddlSchool.Items.Clear()
              
                If BSU_4_Student_reader.HasRows = True Then
                    While BSU_4_Student_reader.Read

                        ddlSchool.Items.Add(New ListItem(BSU_4_Student_reader("bsu_name"), BSU_4_Student_reader("bsu_id")))
                    End While

                End If
            End Using
            ddlSchool.ClearSelection()
            ddlSchool.Items.FindByValue(Session("sBsuid")).Selected = True
            ddlSchool_SelectedIndexChanged(ddlSchool, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Call acadamicyear_bind()
    End Sub

    Protected Sub ddlACD_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CURRICULUM_BSU_4_Student()
    End Sub

    Protected Sub ddlCLM_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bind_Shift()
        bind_Stream()
        BindTree_GradeSection()
    End Sub

    Protected Sub ddlSHF_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindTree_GradeSection()
    End Sub

    Protected Sub ddlSTM_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindTree_GradeSection()
    End Sub

    Sub acadamicyear_bind()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedItem.Value
            Dim currACY_ID As String = String.Empty
            str_Sql = "SELECT  distinct   ACADEMICYEAR_D.ACD_ACY_ID, ACADEMICYEAR_M.ACY_DESCR," & _
" (select top 1 ACD_ACY_ID from ACADEMICYEAR_D where ACD_BSU_ID = '" & BSU_ID & "' and acd_current=1) as CurrACY_ID " & _
" FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID " & _
" = ACADEMICYEAR_M.ACY_ID WHERE(ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "')" & _
" ORDER BY ACADEMICYEAR_D.ACD_ACY_ID "

            Using readerAcademic As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                If readerAcademic.HasRows = True Then
                    While readerAcademic.Read
                        ddlACD_ID.Items.Add(New ListItem(readerAcademic("ACY_DESCR"), readerAcademic("ACD_ACY_ID")))
                        currACY_ID = readerAcademic("CurrACY_ID")
                    End While
                End If
            End Using
            ddlACD_ID.ClearSelection()
            ddlACD_ID.Items.FindByValue(currACY_ID).Selected = True
            ddlACD_ID_SelectedIndexChanged(ddlACD_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub CURRICULUM_BSU_4_Student()

        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedValue
            Dim ACY_ID As String = ddlACD_ID.SelectedValue
            Dim CLM_Count As Integer
            str_Sql = "SELECT DISTINCT ACADEMICYEAR_D.ACD_CLM_ID, CURRICULUM_M.CLM_DESCR," & _
"  (select count(distinct acd_clm_id) from academicyear_d where acd_acy_id " & _
" =ACADEMICYEAR_D.ACD_ACY_ID and acd_bsu_id= '" & BSU_ID & "') as tot_count " & _
" FROM ACADEMICYEAR_D INNER JOIN  CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = " & _
" CURRICULUM_M.CLM_ID WHERE(ACADEMICYEAR_D.ACD_ACY_ID = '" & ACY_ID & "') And " & _
" (ACADEMICYEAR_D.ACD_BSU_ID = '" & BSU_ID & "')"
            ddlCLM_ID.Items.Clear()

            Using readerCLM As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                If readerCLM.HasRows = True Then
                    While readerCLM.Read
                        ddlCLM_ID.Items.Add(New ListItem(readerCLM("CLM_DESCR"), readerCLM("ACD_CLM_ID")))
                        CLM_Count = readerCLM("tot_count")
                    End While
                End If

            End Using

            If CLM_Count > 1 Then
                ddlCLM_ID.Items.Add(New ListItem("ALL", "0"))
                ddlCLM_ID.ClearSelection()
                ddlCLM_ID.Items.FindByValue("0").Selected = True
            Else

            End If
            If ddlCLM_ID.Items.Count = 1 Then
                trCurr.Visible = False
            Else
                trCurr.Visible = True
            End If
            GETEND_DATE()
            ddlCLM_ID_SelectedIndexChanged(ddlCLM_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GETEND_DATE()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_sql As String = String.Empty
        Dim BSU_ID As String = ddlSchool.SelectedValue
        Dim ACY_ID As String = ddlACD_ID.SelectedValue
        Dim CLM_ID As String = ddlCLM_ID.SelectedValue
        str_sql = " SELECT   REPLACE( CONVERT(VARCHAR(12), ACD_ENDDT,106),' ','/') AS ENDDT FROM  ACADEMICYEAR_D WHERE     (ACD_BSU_ID = '" & BSU_ID & "') AND " & _
" (ACD_ACY_ID ='" & ACY_ID & "' ) AND (ACD_CLM_ID='" & CLM_ID & "')"
        Using READERDATE As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
            If READERDATE.HasRows = True Then
                While READERDATE.Read
                    txtDate.Text = READERDATE("ENDDT")
                End While
            Else
                txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            End If
        End Using

    End Sub
    Sub bind_Shift()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedValue
            Dim ACY_ID As String = ddlACD_ID.SelectedValue
            Dim CLM_ID As String = ddlCLM_ID.SelectedValue
            ddlSHF_ID.Items.Clear()
            If CLM_ID <> "0" Then
                str_Sql = " SELECT   distinct  SHIFTS_M.SHF_ID, SHIFTS_M.SHF_DESCR " & _
" FROM  SHIFTS_M INNER JOIN  GRADE_BSU_M ON SHIFTS_M.SHF_ID = GRADE_BSU_M.GRM_SHF_ID " & _
" and GRADE_BSU_M.GRM_ACD_ID=(select acd_id from academicyear_d " & _
" where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and acd_bsu_id='" & BSU_ID & "')"

                Using readerSHF As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    If readerSHF.HasRows = True Then
                        While readerSHF.Read
                            ddlSHF_ID.Items.Add(New ListItem(readerSHF("SHF_DESCR"), readerSHF("SHF_ID")))
                        End While
                    End If
                End Using
                If ddlSHF_ID.Items.Count > 1 Then
                    ddlSHF_ID.Items.Add(New ListItem("ALL", "0"))
                    ddlSHF_ID.ClearSelection()
                    ddlSHF_ID.Items.FindByValue("0").Selected = True
                End If

            Else
                ddlSHF_ID.Items.Add(New ListItem("ALL", "0"))
                ddlSHF_ID.ClearSelection()
                ddlSHF_ID.Items.FindByValue("0").Selected = True
            End If

            If ddlSHF_ID.Items.Count = 1 Then
                trshf.Visible = False
            Else
                trshf.Visible = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bind_Stream()

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedValue
            Dim ACY_ID As String = ddlACD_ID.SelectedValue
            Dim CLM_ID As String = ddlCLM_ID.SelectedValue
            ddlSTM_ID.Items.Clear()
            If CLM_ID <> "0" Then
                str_Sql = "SELECT DISTINCT STREAM_M.STM_ID, STREAM_M.STM_DESCR " & _
               " FROM GRADE_BSU_M INNER JOIN  STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID " & _
               " and GRADE_BSU_M.GRM_ACD_ID=(select acd_id from academicyear_d " & _
               " where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and acd_bsu_id='" & BSU_ID & "')"


                Using readerSTM As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
                    If readerSTM.HasRows = True Then
                        While readerSTM.Read
                            ddlSTM_ID.Items.Add(New ListItem(readerSTM("STM_DESCR"), readerSTM("STM_ID")))
                        End While
                    End If
                End Using
                If ddlSTM_ID.Items.Count > 1 Then
                    ddlSTM_ID.Items.Add(New ListItem("ALL", "0"))
                    ddlSTM_ID.ClearSelection()
                    ddlSTM_ID.Items.FindByValue("0").Selected = True
                End If

            Else
                ddlSTM_ID.Items.Add(New ListItem("ALL", "0"))
                ddlSTM_ID.ClearSelection()
                ddlSTM_ID.Items.FindByValue("0").Selected = True

            End If
           
            If ddlSTM_ID.Items.Count = 1 Then
                trstm.Visible = False
            Else
                trstm.Visible = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindTree_GradeSection() 'for orginating tree view for the grade and section
        Dim dsData As DataSet = Nothing
        Dim dtTable As DataTable = Nothing

        Dim ACY_ID As String = ddlACD_ID.SelectedValue
        Dim BSU_ID As String = ddlSchool.SelectedValue
        Dim CLM_ID As String = ddlCLM_ID.SelectedValue
        Dim SHF_ID As String = ddlSHF_ID.SelectedValue
        Dim STM_ID As String = ddlSTM_ID.SelectedValue
        Dim SHF_STM As String = String.Empty
        SHF_STM = " AND GRADE_BSU_M.GRM_ID<>'' "
        If SHF_ID <> "0" Then
            SHF_STM += " AND GRADE_BSU_M.GRM_SHF_ID = '" & SHF_ID & "'"
        End If
        If STM_ID <> "0" Then
            SHF_STM += " AND GRADE_BSU_M.GRM_STM_ID = '" & STM_ID & "'"

        End If



        If CLM_ID <> "0" Then

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Dim sql_query As String = " with CTE_GRD (GRD_DESCR, SCT_DESCR, GRD_ID, SCT_ID,DISPLAYORDER)" & _
 "  AS ( SELECT  distinct    GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,SECTION_M.SCT_DESCR as SCT_DESCR,  " & _
 "  GRADE_BSU_M.GRM_GRD_ID as  GRD_ID,SECTION_M.SCT_ID as SCT_ID,GRADE_M.GRD_DISPLAYORDER as " & _
 " DISPLAYORDER FROM GRADE_BSU_M INNER JOIN  SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = " & _
 " SECTION_M.SCT_ACD_ID AND  GRADE_BSU_M.GRM_ID = SECTION_M.SCT_GRM_ID INNER JOIN  " & _
 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID  where  GRADE_BSU_M.GRM_ACD_ID= " & _
 " (select acd_id from academicyear_d where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and " & _
 " acd_bsu_id='" & BSU_ID & "')   " & SHF_STM & ") " & _
 " SELECT GRD_ID,GRD_DESCR, SCT_ID,SCT_DESCR FROM   CTE_GRD ORDER BY DISPLAYORDER"





                '"declare  @temptable table(GRD_DESCR varchar(20), SCT_DESCR varchar(20), GRD_ID varchar(100), SCT_ID varchar(10),DISPLAYORDER int) " & _
                '     " insert @temptable (GRD_DESCR, SCT_DESCR, GRD_ID, SCT_ID,DISPLAYORDER) " & _
                '   " SELECT  distinct    GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,SECTION_M.SCT_DESCR as SCT_DESCR, " & _
                '   "  GRADE_BSU_M.GRM_GRD_ID as  GRD_ID,SECTION_M.SCT_ID as SCT_ID,GRADE_M.GRD_DISPLAYORDER as DISPLAYORDER" & _
                '   " FROM GRADE_BSU_M INNER JOIN  SECTION_M ON GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID AND " & _
                '   " GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
                '   " where  GRADE_BSU_M.GRM_ACD_ID=(select acd_id from academicyear_d where acd_clm_id='" & CLM_ID & "' and acd_acy_id='" & ACY_ID & "' and acd_bsu_id='" & BSU_ID & "') AND SECTION_M.SCT_DESCR<>'TEMP' " & SHF_STM & " order by GRADE_M.GRD_DISPLAYORDER  ASC" & _
                '   " SELECT GRD_ID,GRD_DESCR, SCT_ID,SCT_DESCR FROM   @temptable ORDER BY DISPLAYORDER "

                dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
            End Using
            If Not dsData Is Nothing Then
                dtTable = dsData.Tables(0)
            End If

            Dim dvGRD_DESCR As New DataView(dtTable, "", "GRD_DESCR", DataViewRowState.OriginalRows)
            Dim trSelectAll As New TreeNode("All Grades", "ALL")
            Dim drGRD_DESCR As DataRow
            For i As Integer = 0 To dtTable.Rows.Count - 1
                drGRD_DESCR = dtTable.Rows(i)
                Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
                Dim contains As Boolean = False
                While (ienumSelectAll.MoveNext())
                    If ienumSelectAll.Current.Text = drGRD_DESCR("GRD_DESCR") Then
                        contains = True
                    End If
                End While
                Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("GRD_DESCR"), drGRD_DESCR("GRD_ID"))
                If contains Then
                    Continue For
                End If
                Dim strGRADE_SECT As String = "GRD_DESCR = '" & _
                drGRD_DESCR("GRD_DESCR") & "'"
                Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "SCT_DESCR", DataViewRowState.OriginalRows)
                Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
                While (ienumGRADE_SECT.MoveNext())
                    Dim drGRADE_SECT As DataRowView = ienumGRADE_SECT.Current
                    Dim trNodeMONTH_DESCR As New TreeNode(drGRADE_SECT("SCT_DESCR"), drGRADE_SECT("SCT_ID"))
                    trNodeGRD_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
                End While
                trSelectAll.ChildNodes.Add(trNodeGRD_DESCR)
            Next

            tvGrade.Nodes.Clear()
            tvGrade.Nodes.Add(trSelectAll)
            tvGrade.DataBind()

        Else
            Dim trGRDAll As New TreeNode("All Grades", "ALL")
            tvGrade.Nodes.Clear()
            tvGrade.Nodes.Add(trGRDAll)
            tvGrade.DataBind()


        End If

        ' PROCESS Filter



    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Page.IsPostBack = False Then
            If Not tvGrade Is Nothing Then
                DisableAutoPostBack(tvGrade.Nodes)
            End If
        End If


    End Sub
    Sub DisableAutoPostBack(ByVal tnc As TreeNodeCollection)
        ' Loop over every node in the passed collection 
        For Each tn As TreeNode In tnc
            ' Set the node's NavigateUrl (which equates to A HREF) to javascript:void(0);, 
            ' effectively intercepting the click event and disabling PostBack. 
            tn.NavigateUrl = "javascript:void(0);"

            ' Set the node's SelectAction to Select in order to enable client-side 
            ' expansion/collapsing of parent nodes. Caution: SelectExpand causes 
            ' expand/collapse icon to disappear! 
            tn.SelectAction = TreeNodeSelectAction.[Select]

            ' If this node has children, recurse over them as well before returning 
            If tn.ChildNodes.Count > 0 Then

                DisableAutoPostBack(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub ClearAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = False

            If tn.ChildNodes.Count > 0 Then
                ClearAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub
    Sub SelectAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = True

            If tn.ChildNodes.Count > 0 Then
                SelectAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub
   
    
    Sub checkboxchecking(ByVal cbl As CheckBoxList, ByVal cb As CheckBox)
        Dim i As Integer = 0
        Dim cblcount As Integer = 0
        For i = 0 To cbl.Items.Count - 1
            If (cbl.Items(i).Selected = True) Then
                cblcount = cblcount + 1
            End If
        Next
        If (cblcount = cbl.Items.Count) Then
            cb.Checked = True
        Else
            cb.Checked = False
        End If
    End Sub

    Public Sub BindPartBControls()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim SET_TYPE As String = "Student"
        Dim str_query = "Select * from COM_CER_M  WHERE CCM_TYPE ='Student' order by CCM_DISPLAY_ORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DataListPartB.DataSource = ds
        DataListPartB.DataBind()
        For Each row As DataListItem In DataListPartB.Items
            Dim cgi_id = DirectCast(row.FindControl("HiddenCCM_ID"), HiddenField).Value
            Dim dc As DataSet
            Dim CBLDynamic As New CheckBoxList
            CBLDynamic.ID = "|" & cgi_id & "|"
            dc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, DirectCast(row.FindControl("HiddenCondition"), HiddenField).Value & "  AND STU_BSU_ID='" & Session("sBsuid") & "' ORDER BY 2 ")
            CBLDynamic.DataSource = dc
            CBLDynamic.DataTextField = DirectCast(row.FindControl("HiddenTextField"), HiddenField).Value
            CBLDynamic.DataValueField = DirectCast(row.FindControl("HiddenValueField"), HiddenField).Value
            CBLDynamic.DataBind()
            For Each clist As ListItem In CBLDynamic.Items
                clist.Attributes.Add("onclick", "javascript:uncheckall(this);")
            Next
            Dim list As New ListItem
            list.Text = "All"
            list.Value = "0"
            list.Attributes.Add("onclick", "javascript:change_chk_state(this);")

            CBLDynamic.Items.Insert(0, list)

            Dim PanelHolder As Panel = DirectCast(row.FindControl("PanelControl"), Panel)
            PanelHolder.Controls.Add(CBLDynamic)
        Next



    End Sub

    Protected Sub ddtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call BindPartBControls()
        Call bindFilter_Cond_C()
    End Sub

    Sub bindFilter_Cond_C()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim C_TYPE As String = ddtype.SelectedItem.Value

            Dim ds As New DataSet
            str_Sql = " select CCF_FIELD_NAME,CCF_FIELD_DISPLAY from com_cer_filter where CCF_TYPE='" & C_TYPE & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlCond1.Items.Clear()

            ddlCond1.DataSource = ds.Tables(0)
            ddlCond1.DataTextField = "CCF_FIELD_DISPLAY"
            ddlCond1.DataValueField = "CCF_FIELD_NAME"
            ddlCond1.DataBind()
            ddlCond1.Items.Add(New ListItem("-- Select --", "0"))
            ddlCond1.ClearSelection()
            ddlCond1.Items.FindByValue("0").Selected = True


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindPARTC_Grid()
        Dim str_search As String = String.Empty
        Dim str_cond1 As String = String.Empty

        Dim str_cond As String = String.Empty
        Dim str_Sid_search() As String
        Dim Temp_opr As String = String.Empty
        Dim Cond As String = String.Empty
        str_Sid_search = h_Selected_menu_1.Value.Split("__")
        str_search = str_Sid_search(0)
        For Each item As ListItem In chkCond1.Items
            If (item.Selected) Then
                Temp_opr = item.Text
                If txtCond1.Text.Trim <> "" And ddlCond1.SelectedItem.Value <> "0" Then

                    If str_search = "LI" Then
                        str_cond1 = "  " & Temp_opr & "  " & ddlCond1.SelectedItem.Value & "  LIKE '%" & txtCond1.Text & "%'"
                        Cond = "Any Where"

                    ElseIf str_search = "NLI" Then
                        Cond = "Not In"
                        str_cond1 = "  " & Temp_opr & "  " & ddlCond1.SelectedItem.Value & " LIKE '%" & txtCond1.Text & "%'"
                    ElseIf str_search = "SW" Then
                        Cond = "Starts With"
                        str_cond1 = "  " & Temp_opr & "  " & ddlCond1.SelectedItem.Value & " LIKE '" & txtCond1.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        Cond = "Not Start With"
                        str_cond1 = "  " & Temp_opr & "  " & ddlCond1.SelectedItem.Value & " NOT LIKE '" & txtCond1.Text & "%'"
                    ElseIf str_search = "EW" Then
                        Cond = "Ends With"
                        str_cond1 = "  " & Temp_opr & "  " & ddlCond1.SelectedItem.Value & " LIKE  '%" & txtCond1.Text & "'"
                    ElseIf str_search = "NEW" Then
                        Cond = "Not Ends With"
                        str_cond1 = "  " & Temp_opr & "  " & ddlCond1.SelectedItem.Value & " NOT LIKE '%" & txtCond1.Text & "'"
                    ElseIf str_search = "BET" Then
                        Cond = "Between Dates"
                        str_cond1 = "  " & Temp_opr & "  " & ddlCond1.SelectedItem.Value & " BETWEEN '" & txtCond1.Text & "' AND '" & txtCond2.Text & "'"
                    End If
                    Dim checkDup As Boolean = False
                    For i As Integer = 0 To Session("PartC_CER").Rows.count - 1
                        If UCase(Session("PartC_CER").Rows(i)("STRCOND")) = UCase(str_cond1) Then
                            checkDup = True
                        End If
                    Next

                    If checkDup = False Then
                        Dim rDt As DataRow
                        rDt = Session("PartC_CER").NewRow
                        rDt("Id") = ViewState("id")
                        rDt("OPER") = Temp_opr
                        rDt("COLNAME") = ddlCond1.SelectedItem.Text
                        rDt("COND") = Cond
                        rDt("CONDVAL1") = txtCond1.Text
                        rDt("CONDVAL2") = txtCond2.Text
                        rDt("STRCOND") = str_cond1
                        '  If Session("PartC_CER").Rows.count = 0 Then

                        Session("PartC_CER").Rows.Add(rDt)
                        ViewState("id") = ViewState("id") + 1
                        gridbind()
                        lblError2.Text = ""
                    Else

                        lblError2.Text = "Duplicate Entry Not Allowed!!!"
                    End If

                    'lblError2.Text = ""

                    ' Else

                    ''Dim rDt As DataRow
                    ' ''        rDt = Session("dt").NewRow
                    'Session("PartC_CER").Rows.Add(rDt)
                    'ViewState("id") = ViewState("id") + 1
                    'gridbind()

                    '    Else
                    '    lblError2.Text = "Duplicate Entry Not Allowed!!!"
                    'End If

                    '        Next

                    ' End If
                Else

                    If ddlCond1.SelectedItem.Value = "0" Then
                        lblError2.Text = "Select the field name to be searched for!!!"
                    Else
                        lblError2.Text = "Enter the value to be searched for!!!"
                    End If

                End If

            End If
        Next

    End Sub
    Protected Sub btnPartCAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim CheckDate As Integer = InStr(1, UCase(ddlCond1.SelectedItem.Text), "DATE", CompareMethod.Text)
        For Each item As ListItem In chkCond1.Items
            If (item.Selected) Then
                i = 1
            End If
        Next
        If i = 0 Then
            lblError2.Text = "Select the AND/OR  operation before adding the filter condition"
        Else
            If CheckDate > 0 Then
                If serverDateValidate() = "0" Then
                    Call bindPARTC_Grid()

                End If
            Else
                Call bindPARTC_Grid()
            End If

        End If

    End Sub

    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtCond1.Text.Trim <> "" Then
            Dim strfDate As String = txtCond1.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtCond1.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtCond1.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        Else
            CommStr = CommStr & "<div>From Date Required</div>"
        End If

        Dim str_search As String = String.Empty
        Dim str_Sid_search() As String
        str_Sid_search = h_Selected_menu_1.Value.Split("__")
        str_search = str_Sid_search(0)
        If str_search = "BET" Then
            If txtCond2.Text.Trim <> "" Then

                Dim strfDate As String = txtCond2.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then

                    CommStr = CommStr & "<div>To Date format is Invalid</div>"
                Else
                    txtCond2.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtCond1.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtCond2.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtCond1.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime2 < dateTime1 Then

                                CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                            End If
                        Else

                            CommStr = CommStr & "<div>Invalid To Date</div>"
                        End If
                    End If

                End If
            Else
                CommStr = CommStr & "<div>To Date Required</div>"
            End If


        End If


        If CommStr <> "" Then
            lblError2.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function
    Public Function FilterQuery() As String

        Dim PartAFilter = ""


        Dim acyid As String = ddlACD_ID.SelectedValue

            Dim i As Integer
           
            If acyid <> "" Then
                If PartAFilter <> "" Then
                PartAFilter += " AND ACY_ID = '" & acyid & "'"
                Else
                PartAFilter = " ACY_ID ='" & acyid & "'"
                End If
            End If


        Dim clmid As String = ddlCLM_ID.SelectedValue

      
        If clmid <> "0" Then
            If PartAFilter <> "" Then
                PartAFilter += " AND CLM_ID = '" & clmid & "'"
            Else
                PartAFilter = " CLM_ID  = '" & clmid & "'"
            End If
        End If

        Dim checkedNodes As TreeNodeCollection = tvGrade.CheckedNodes
        Dim temp1 As String = String.Empty
        If tvGrade.CheckedNodes.Count > 0 Then
            For Each node As TreeNode In tvGrade.CheckedNodes
                If Not node Is Nothing Then
                    If node.Text <> "All Grades" Then

                        If node.Parent.Text <> "All Grades" Then
                            temp1 += "'" & node.Value & "',"
                            'node.Parent.Value, node.Value
                        End If
                    End If
                End If
            Next
        End If


        temp1 = temp1.TrimEnd(",").TrimStart(",")

        If Trim(temp1) <> "" Then

            If PartAFilter <> "" Then
                PartAFilter += " AND SCT_ID IN (" & temp1 & ")"
            Else
                PartAFilter = "  SCT_ID IN (" & temp1 & ")"
            End If

        End If


        Dim shftid As String = ddlSHF_ID.SelectedValue

        If shftid <> "0" Then
            If PartAFilter <> "" Then
                PartAFilter += " AND SHIFT_ID ='" & shftid & "'"
            Else
                PartAFilter = " SHIFT_ID ='" & shftid & "'"
            End If
        End If



        Dim STMID As String = ddlSTM_ID.SelectedValue

        If STMID <> "0" Then
            If PartAFilter <> "" Then
                PartAFilter += " AND STM_ID  ='" & STMID & "'"
            Else
                PartAFilter = " STM_ID ='" & STMID & "'"
            End If
        End If







        Dim PBfilter As String = ""

        'Part B
        For Each item As DataListItem In DataListPartB.Items
            Dim cboxlist As New CheckBoxList
            Dim ccm_id As String = DirectCast(item.FindControl("HiddenCCM_ID"), HiddenField).Value
            cboxlist = DirectCast(item.FindControl("|" & ccm_id & "|"), CheckBoxList)


            If cboxlist.Items(0).Selected = False Then

                Dim temp As String = ""


                For i = 0 To cboxlist.Items.Count - 1
                    Dim filter As String = ""
                    If cboxlist.Items(i).Selected Then

                        If i = 0 Then
                            filter += "'" & cboxlist.Items(i).Value & "'"
                        Else
                            filter += "'" & cboxlist.Items(i).Value & "',"
                        End If

                    End If
                    temp += filter
                Next
                If temp <> "" Then

                    temp = temp.Substring(0, temp.Length - 1)
                    Dim FilterField As String = DirectCast(item.FindControl("HiddenFilterField"), HiddenField).Value


                    If FilterField <> "" Then

                        FilterField = FilterField.Replace("$", temp)

                        If PBfilter <> "" Then
                            PBfilter += " AND "
                        End If
                        PBfilter += FilterField
                    End If

                End If

            End If

        Next

        Dim busnoid As String = ""
        If CBbusnoselectall.Checked <> True Then

            For i = 0 To CBLbusnumber.Items.Count - 1
                If CBLbusnumber.Items(i).Selected = True Then

                    If busnoid <> "" Then
                        busnoid += "," + "'" + CBLbusnumber.Items(i).Value + "'"
                    Else
                        busnoid = "'" + CBLbusnumber.Items(i).Value + "'"
                    End If

                End If
            Next
            If busnoid <> "" Then
                If PBfilter <> "" Then
                    PBfilter += " AND [PICKUP BUSNO] IN (" & busnoid & ")"
                Else
                    PBfilter = " [PICKUP BUSNO] IN (" & busnoid & ")"
                End If
            End If
        End If

        Dim ReturnQuery As String = ""


        If PartAFilter <> "" Then
            ReturnQuery = PartAFilter
            If PBfilter <> "" Then
                ReturnQuery += " AND " + PBfilter
            End If
        End If
        If PartAFilter = "" Then
            If PBfilter <> "" Then
                ReturnQuery += PBfilter
            End If
        End If

        'End If

        Dim j As Integer = 0

        Dim PCfilter As String = ""
        While j <= Session("PartC_CER").Rows.Count - 1
            PCfilter += Session("PartC_CER").rows(j)("STRCOND")

            j = j + 1

        End While

        Dim FilterBY As String = String.Empty
        If ddlFilterBy.SelectedValue = 0 Then
            FilterBY = " AND CURR_STATUS IN ('EN','TC','SO')"
        ElseIf ddlFilterBy.SelectedValue = 1 Then
            FilterBY = " AND CURR_STATUS IN ('EN')"
        ElseIf ddlFilterBy.SelectedValue = 2 Then
            FilterBY = " AND CURR_STATUS IN ('TC')"
        ElseIf ddlFilterBy.SelectedValue = 3 Then
            FilterBY = "  AND CURR_STATUS IN ('SO')"

        End If

        ReturnQuery += " " & PCfilter.Trim & FilterBY
       
        Return ReturnQuery
    End Function

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        If txtCerti.Text.Trim <> "" Then

            Session("Certi") = StrConv(txtCerti.Text.Trim, VbStrConv.ProperCase)
            Dim url As String = String.Empty
            Dim SET_TYPE As String = ddtype.SelectedItem.Value
            Dim SID As String = ddlSchool.SelectedItem.Value
            Session("Comm_Query") = FilterQuery()
            Session("EXCEL_ACY_ID") = ddlACD_ID.SelectedValue
            Session("EXCEL_CLM_ID") = ddlCLM_ID.SelectedValue
            Session("EXCEL_SHF_ID") = ddlSHF_ID.SelectedValue
            Session("EXCEL_STM_ID") = ddlSTM_ID.SelectedValue
            Session("EXCEL_ASDT") = txtDate.Text


            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            SET_TYPE = Encr_decrData.Encrypt(SET_TYPE)
            SID = Encr_decrData.Encrypt(SID)
            url = String.Format("~\Students\studCert_Comm_Merge_new.aspx?MainMnu_code={0}&S_type={1}&SID={2}", ViewState("MainMnu_code"), SET_TYPE, SID)

            Response.Redirect(url)
        Else
            lblError.Text = "Certificate name required"
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim OPER As New DataColumn("OPER", System.Type.GetType("System.String"))
            Dim COLNAME As New DataColumn("COLNAME", System.Type.GetType("System.String"))
            Dim COND As New DataColumn("COND", System.Type.GetType("System.String"))
            Dim CONDVAL1 As New DataColumn("CONDVAL1", System.Type.GetType("System.String"))
            Dim CONDVAL2 As New DataColumn("CONDVAL2", System.Type.GetType("System.String"))
            Dim STRCOND As New DataColumn("STRCOND", System.Type.GetType("System.String"))

            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(OPER)
            dtDt.Columns.Add(COLNAME)
            dtDt.Columns.Add(COND)
            dtDt.Columns.Add(CONDVAL1)
            dtDt.Columns.Add(CONDVAL2)
            dtDt.Columns.Add(STRCOND)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError2.Text = "Record cannot be inserted"
            Return dtDt
        End Try
    End Function

    Protected Sub gvPartC_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim COND_ID As Integer = CInt(gvPartC.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0
        While i < Session("PartC_CER").Rows.Count
            If Session("PartC_CER").rows(i)("Id") = COND_ID Then
                Session("PartC_CER").rows(i).delete()
            Else
                i = i + 1
            End If
        End While
        gridbind()
    End Sub
    Sub gridbind()
        Try
            gvPartC.DataSource = Session("PartC_CER")
            gvPartC.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

   
    Sub bindbusno()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT distinct BNO_ID,BNO_DESCR FROM transport.BUSNOS_M where bno_bsu_id='" & Session("sBsuid") & "' order by BNO_DESCR"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            CBLbusnumber.Items.Clear()
            CBLbusnumber.DataSource = ds.Tables(0)
            CBLbusnumber.DataValueField = "BNO_ID"
            CBLbusnumber.DataTextField = "BNO_DESCR"
            CBLbusnumber.DataBind()
            CBLbusnumber.ClearSelection()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub RBLbusstatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CBbusnoselectall.Checked = False


        If RBLbusstatus.SelectedValue = 1 Then

            bindbusno()

        Else

            CBLbusnumber.Items.Clear()

        End If
    End Sub

    'Protected Sub CBbusnoselectall_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Dim i As Integer = 0
    '    If (CBbusnoselectall.Checked = True) Then
    '        For i = 0 To CBLbusnumber.Items.Count - 1
    '            CBLbusnumber.Items(i).Selected = True
    '        Next
    '    Else
    '        For i = 0 To CBLbusnumber.Items.Count - 1
    '            CBLbusnumber.Items(i).Selected = False
    '        Next
    '    End If

    'End Sub


  
    'Protected Sub CBLbusnumber_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    checkboxchecking(CBLbusnumber, CBbusnoselectall)
    'End Sub

    
   
  
End Class
