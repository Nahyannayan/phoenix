﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stuLatePassReason.aspx.vb" Inherits="Students_stuLatePassReason" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Late Reasons
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">



                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="lnkadd" runat="server" Text="Add New"></asp:LinkButton>
                        </td>
                    </tr>

                    <tr>
                        <td >
                            <asp:GridView ID="gvReason" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" BorderStyle="None">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblLRID" runat="server" Text='<%# Bind("LR_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Reason">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReason" runat="server" Text='<%# Bind("LR_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />

                            </asp:GridView>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="button"/>
                        </td>
                    </tr>

                </table>
                <div id="divReason" runat="server" class="panel-cover" visible="false">
                    <div >
                        <asp:Button ID="btClose" type="button" runat="server"
                            Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                            ForeColor="White" Text="X"></asp:Button>
                        <div >
                            <div align="left">
                                <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <br/>
                            <br/>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%"> <span class="field-label">Reason</span>
                                    </td>
                                    
                                    <td  align="left">
                                        <asp:TextBox ID="txtReason" runat="server" ></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSav" Text="Save" runat="server" CssClass="button"/>
                                        <asp:Button ID="btnUClose" Text="Close" runat="server" CssClass="button" />
                                    </td>
                                </tr>

                            </table>
                        </div>

                    </div>
                </div>
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
                    HasCrystalLogo="False" Height="50px" PrintMode="ActiveX"
                    Width="350px"></CR:CrystalReportViewer>
                <asp:HiddenField ID="h_print" runat="server" />


            </div>
        </div>
    </div>
</asp:Content>

