﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.IO
Imports Telerik.Web.UI

Partial Class Students_StuIssueIDCard
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindBusinessUnit()
            HiddenShowFlag.Value = 0
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnPrint)
    End Sub
    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddlBUnit.DataSource = ds.Tables(0)
        ddlBUnit.DataValueField = "BSU_ID"
        ddlBUnit.DataTextField = "BSU_NAME"
        ddlBUnit.DataBind()
        ddlBUnit.SelectedIndex = -1

        If Not ddlBUnit.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddlBUnit.Items.FindItemByValue(Session("sBsuid")).Selected = True 'FindByValue
        End If
        bind_Academic_year()
    End Sub
    Sub bind_Academic_year()
        If ddlBUnit.SelectedValue <> "" Then
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = "SELECT ACD_ID,ACY_DESCR FROM(SELECT 0 ACD_ID,'------ALL-----' ACY_DESCR UNION ALL  SELECT ACD.ACD_ID,ACY.ACY_DESCR  from ACADEMICYEAR_M ACY INNER JOIN ACADEMICYEAR_D ACD ON ACY.ACY_ID=ACD.ACD_ACY_ID   WHERE ACD.ACD_BSU_ID='" & ddlBUnit.SelectedValue & "' and acy.ACY_bSHOW=1)A order by ACD_ID "

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            ddl_acdYear.DataTextField = "ACY_DESCR"
            ddl_acdYear.DataValueField = "ACD_ID"
            ddl_acdYear.DataSource = ds.Tables(0)
            ddl_acdYear.DataBind()
            ddl_acdYear.SelectedItem.Value=0
            BindGrade()
        End If
    End Sub
    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddl_acdYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddl_acdYear.SelectedItem.Value
            End If
            ' Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlgrade.Items.Clear()
                ' di = New ListItem("ALL", "0")
                Dim di As Telerik.Web.UI.RadComboBoxItem = New Telerik.Web.UI.RadComboBoxItem("--ALL--", "0")
                ddlgrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New Telerik.Web.UI.RadComboBoxItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlgrade.Items.Add(di)
                    End While

                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindGrade()
        If ddl_acdYear.SelectedValue <> "" Then
            callGrade_ACDBind()
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindStudentServices()
    End Sub
    Sub BindStudentServices()
        If ddlBUnit.SelectedValue <> "" Then
            Try
                Dim con As String = ConnectionManger.GetOASISConnectionString
                Dim param(10) As SqlClient.SqlParameter

                param(0) = New SqlClient.SqlParameter("@BSU_ID ", IIf(ddlBUnit.SelectedValue <> "", ddlBUnit.SelectedValue, DBNull.Value))
                param(1) = New SqlClient.SqlParameter("@GRD_ID ", IIf(ddlgrade.SelectedValue <> "0", ddlgrade.SelectedValue, DBNull.Value))
                param(2) = New SqlClient.SqlParameter("@SAS_ID ", IIf(ddlStatus.SelectedValue <> "0", ddlStatus.SelectedValue, DBNull.Value))
                param(3) = New SqlClient.SqlParameter("@STU_NO ", IIf(txtStudNo.Text <> "", txtStudNo.Text, DBNull.Value))
                param(4) = New SqlClient.SqlParameter("@STU_NAME ", IIf(txtStudName.Text <> "", txtStudName.Text, DBNull.Value))

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "GET_LOST_IDCARD_REQUESTS", param)
                gvLostIDCards.DataSource = ds.Tables(0)
                gvLostIDCards.DataBind()

            Catch ex As Exception

            End Try
        End If
    End Sub
    Protected Sub ddl_acdYear_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_acdYear.SelectedIndexChanged
        BindGrade()
    End Sub
    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBUnit.SelectedIndexChanged
        bind_Academic_year()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim constring As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(constring)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim Failure As Boolean, strError As String
        strError = ""
        Try
            For Each gr As GridViewRow In gvLostIDCards.Rows
                Dim h_SLCSTATUS As HiddenField = DirectCast(gr.FindControl("h_SLCSTATUS"), HiddenField)
                Dim h_SLCID As HiddenField = DirectCast(gr.FindControl("h_SLCID"), HiddenField)
                Dim dlAction As RadComboBox = DirectCast(gr.FindControl("dlAction"), RadComboBox)
                If dlAction.SelectedValue <> h_SLCSTATUS.Value Then
                    Dim sqlParam(3) As SqlParameter
                    sqlParam(0) = Mainclass.CreateSqlParameter("@SLC_ID", Val(h_SLCID.Value), SqlDbType.Int)
                    sqlParam(1) = Mainclass.CreateSqlParameter("@USR_ID", Session("sUsr_name"), SqlDbType.VarChar)
                    sqlParam(2) = Mainclass.CreateSqlParameter("@SLC_STATUS", dlAction.SelectedValue, SqlDbType.Int)
                    sqlParam(3) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "UpdateStudentLostCardStatus", sqlParam)
                    If sqlParam(3).Value <> "" Then
                        strError = IIf(sqlParam(3).Value = "-1", "Unexpected Error !!!", sqlParam(3).Value)
                        Failure = True
                        Exit For
                    End If
                End If
            Next
            If Failure = False Then
                sqltran.Commit()
                BindStudentServices()
                lblerror.Text = "Data Saved Successfully"
            Else
                lblerror.Text = strError
                sqltran.Rollback()
            End If
            BindStudentServices()
        Catch ex As Exception
            sqltran.Rollback()
            con.Close()
            lblerror.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvLostIDCards_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLostIDCards.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim gvServiceSubDetails As New GridView
                Dim h_SLCSTATUS As HiddenField = DirectCast(e.Row.FindControl("h_SLCSTATUS"), HiddenField)
                Dim h_SLCID As HiddenField = DirectCast(e.Row.FindControl("h_SLCID"), HiddenField)
                Dim dlAction As RadComboBox = DirectCast(e.Row.FindControl("dlAction"), RadComboBox)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub PrintCard()
        Dim hash As New Hashtable
        Session("CardhashtableAll") = Nothing
        Session("Cardhashtable") = Nothing
        Session("PromoteGrade") = Nothing
        Dim failure As Boolean
        For Each row As GridViewRow In gvLostIDCards.Rows
            Dim ch As CheckBox = DirectCast(row.FindControl("CheckBar1"), CheckBox)
            Dim h_STU_ID As HiddenField = DirectCast(row.FindControl("h_STU_ID"), HiddenField)
            Dim h_SLCSTATUS As HiddenField = DirectCast(row.FindControl("h_SLCSTATUS"), HiddenField)
            Dim h_bPaid As HiddenField = DirectCast(row.FindControl("h_bPaid"), HiddenField)
            Dim key As String
            If ch.Checked Then
                If h_SLCSTATUS.Value = 4 And h_bPaid.Value.ToUpper = "YES" Then
                    key = h_STU_ID.Value
                    If Not hash.ContainsKey(key) Then
                        hash.Add(key, h_STU_ID.Value)
                    End If
                Else
                    If h_SLCSTATUS.Value <> 4 Then
                        failure = True
                        lblerror.Text = "Please change the status to Issue and save and then print the ID Card."
                        Exit For
                    ElseIf h_bPaid.Value.ToUpper <> "YES" Then
                        failure = True
                        lblerror.Text = "Please do the payment for ID Card before printing."
                        Exit For
                    End If
                End If
            End If
        Next
        If hash.Count = 0 Or failure Then
            HiddenShowFlag.Value = 0
            Session("Cardhashtable") = Nothing
        Else
            HiddenShowFlag.Value = 1
            Session("Cardhashtable") = hash
        End If
        Session("PromoteGrade") = False
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        PrintCard()
    End Sub
End Class
