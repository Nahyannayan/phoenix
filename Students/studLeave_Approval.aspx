<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studLeave_Approval.aspx.vb" Inherits="Students_studLeave_Approval" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>

    <script language="javascript" type="text/javascript">



        function ShowRPTSETUP_S(id) {

            var sFeatures;
            sFeatures = "dialogWidth: 950px; ";
            sFeatures += "dialogHeight: 750px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Students/Tabpages/StuAttDetail.aspx?id=' + id;
            //alert(status) 

            result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }

        }




        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkG_bAVAILABLE") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvHolidayDetail.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }

    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Leave Approval"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%-- <table id="Table1" border="0" width="100%">
        <tr style="font-size: 12pt">
            <td align="left" class="title" style="width: 48%">
                LEAVE APPROVAL</td>
        </tr>
    </table>--%>
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">

                            <div align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            </div>
                            <div align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                    EnableViewState="False" ValidationGroup="AttGroup"></asp:ValidationSummary>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <%--<td align="center" class="matters"  valign="middle">
                &nbsp;Fields Marked<span style="font-size: 8pt; color: #800000"> </span>with(<span
                    style="font-size: 8pt; color: #800000">*</span>)are mandatory</td>--%>
                        <td align="center" class="text-danger font-small" valign="middle">Fields Marked with ( * ) are mandatory
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr>
                        <td class="subheader_img" colspan="6">
                            <asp:Literal id="ltLabel" runat="server" Text="Leave Approval"></asp:Literal></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Academic Year</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>

                                    <td align="left" colspan="2"></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Grade</span><span class="text-danger font-small">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Section</span><span class="text-danger font-small">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:RadioButton ID="rbPending" runat="server" GroupName="Status" Text="Pending" AutoPostBack="True" OnCheckedChanged="rbPending_CheckedChanged" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rbApproved" runat="server" GroupName="Status" Text="Approved" AutoPostBack="True" OnCheckedChanged="rbApproved_CheckedChanged" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rbNotApproved" runat="server" GroupName="Status" Text="Not Approved " AutoPostBack="True" OnCheckedChanged="rbNotApproved_CheckedChanged" CssClass="field-label"></asp:RadioButton>
                                        <asp:RadioButton ID="rbAll" runat="server" GroupName="Status" Text="All" AutoPostBack="True" OnCheckedChanged="rbAll_CheckedChanged" CssClass="field-label"></asp:RadioButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvHolidayDetail" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="No Details Added" Width="100%" AllowPaging="true" PageSize="10">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select All">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this); " runat="server" ToolTip="Click here to select/deselect all rows"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkG_bAVAILABLE" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">

                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server" OnClientClick="<%# &quot;ShowRPTSETUP_S('&quot; & Container.DataItem(&quot;STU_ID&quot;) & &quot;');return false;&quot; %>" __designer:wfdid="w17" Text='<%# Bind("STUD_ID")%>'></asp:LinkButton>
                                                        <asp:Label ID="lblSTuID" runat="server" Text='<%# Bind("STU_ID")%>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSNAME" runat="server" Text='<%# bind("SNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="SCT_DESCR" HeaderText="Section"></asp:BoundField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("FROMDT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFROMDT" runat="server" Text='<%# Bind("FROMDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("TODT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTODT" runat="server" Text='<%# Bind("TODT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Day(s)">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDay" runat="server" Text='<%# Bind("Days") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Status" ShowHeader="False">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" CausesValidation="True" CommandName="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CausesValidation="False" __designer:wfdid="w6" CommandName="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:Label ID="lblStatus" runat="server" Text='<%# bind("STATUS") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="center">

                                        <asp:Button ID="btnApproved" runat="server" CssClass="button" Text="Approve" />
                                        <asp:Button ID="btnNotApp" runat="server" CssClass="button" Text="Reject" /></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="left"></td>
                    </tr>

                    <tr>
                        <td></td>
                    </tr>
                </table>
                <asp:Panel ID="pnl_Approve" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <div style="float: right">
                                <asp:LinkButton ID="lnkClosePop" ToolTip="click here to close"
                                    runat="server" Text="X" Font-Underline="False" CausesValidation="False" OnClick="lnkClosePop_Click"></asp:LinkButton>
                            </div>
                            <table width="100%">

                                <tr>
                                    <td><span class="field-label">Remarks</span>
                                    </td>
                                   

                                    <td runat="server" id="tdDDLRemarks" >
    <asp:DropDownList ID="ddlRemarks" runat="server"></asp:DropDownList>
    <asp:RequiredFieldValidator ID="rfvRemark" runat="server" ErrorMessage="Select Remarks/Comments" ControlToValidate="ddlRemarks" ValidationGroup="save"></asp:RequiredFieldValidator>

 

</td>
<td runat="server" id="tdtxtRemarks" >
    <textarea id="txtRemarks" runat="server" cols="32" rows="3"></textarea>
    <asp:RequiredFieldValidator ID="rfvRemark1" runat="server" ErrorMessage="Enter Remarks/Comments " ControlToValidate="txtRemarks" ValidationGroup="save"></asp:RequiredFieldValidator>
</td>

                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="btnApproveLeave" runat="server" Text="APPROVE" CssClass="button" OnClick="btnApproveLeave_Click" ValidationGroup="save" />
                                        &nbsp;
                            <asp:Button ID="btnRejectLEave" runat="server" Text="REJECT" CssClass="button" OnClick="btnRejectLEave_Click" ValidationGroup="save" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </asp:Panel>


            </div>
        </div>
    </div>
</asp:Content>

