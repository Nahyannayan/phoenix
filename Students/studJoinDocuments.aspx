<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studJoinDocuments.aspx.vb" Debug="true" Inherits="Students_studJoinDocuments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    <title>Untitled Page</title>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <link href="../cssfiles/sb-admin.css" rel="stylesheet">
    <script>
    function confirmDocCollect() {         
            return confirm('You are going to change document collected status');           
        }
    
    </script> 

    <style type="text/css">
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style> 
</head>
<body>

    <form id="form1" runat="server">
        <div>
            <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
            </ajaxToolkit:ToolkitScriptManager>
            <table class="table table-bordered table-row">
                <tr>
                    <td class="title-bg">Documents Uploaded</td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            BorderStyle="None" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                            PageSize="15" Width="100%" CssClass="table table-bordered table-row">
                            <RowStyle Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <Columns>
                                <asp:TemplateField HeaderText="INCIDENTID" Visible="False">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblStudId" runat="server" Text='<%# Bind("UPLOAD_ID") %>'></asp:Label>
                                        <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("EQM_ENQID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date Uploaded">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("UPLOAD_DATE_FORMAT") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Document Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStudName" runat="server" Text='<%# Bind("DOC_DESCR") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <a href='<%# Bind("File_URL") %>' target="_blank"
                                            runat="server" id="hrefDoc">View
                        
                                        </a>
                                        <%--<asp:HyperLink ID="Open" NavigateUrl='<%# Eval("file_url") %>' Text='View' runat="server" /> --%>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Update document">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hf_doc_id" runat="server" Value='<%# Bind("UPLOAD_ID") %>' />
                                        <asp:HiddenField ID="hf_DOC_DESCR" runat="server" Value='<%# Bind("DOC_DESCR") %>' />
                                        <asp:LinkButton ID="lnkchangeDoc" runat="server" OnClick="lnkchangeDoc_Click">Upload</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                </asp:TemplateField>

                            </Columns>
                            <SelectedRowStyle Wrap="False" />
                            <HeaderStyle   Wrap="False" />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle Wrap="False" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>



            <table width="100%">
                <tr>
                    <td class="title-bg">Documents Pending</td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            BorderStyle="None" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                            PageSize="15" CssClass="table table-bordered table-row">
                            <RowStyle Wrap="False" />
                            <EmptyDataRowStyle Wrap="False" />
                            <Columns>

                                <asp:TemplateField HeaderText="Document Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("DOC_DESCR") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Collected">
                                    <ItemTemplate>

                                        <asp:ImageButton ID="imgDocCollected" runat="server"
                                            ImageUrl='<%# BIND("COLLECTED_STATUS") %>' HorizontalAlign="Center"
                                            ToolTip="Click here to change the document collect status"
                                            OnClick="imgDocCollected_Click"></asp:ImageButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender_cb_collected" ConfirmText="You are going to change document collected status"
                                            TargetControlID="imgDocCollected" runat="server">
                                        </ajaxToolkit:ConfirmButtonExtender>
                                        <asp:HiddenField ID="HF_SDU_ID" runat="server" Value='<%# Bind("EDM_ID") %>' />
                                        <asp:HiddenField ID="HF_SDU_BCOLLECTED" runat="server" Value='<%# Bind("EDM_BCOLLECTED") %>' />

                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Upload">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hf_doc_id" runat="server" Value='<%# Bind("DOC_ID") %>' />
                                        <asp:HiddenField ID="hf_DOC_DESCR" runat="server" Value='<%# Bind("DOC_DESCR") %>' />
                                        <asp:LinkButton ID="lnkUpload" runat="server" OnClick="lnkUpload_Click">Upload</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle Wrap="False" />
                            <HeaderStyle Wrap="False" />
                            <EditRowStyle Wrap="False" />
                            <AlternatingRowStyle Wrap="False" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>



        </div>
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <asp:HiddenField ID="HF_stuid" runat="server" />
        <asp:HiddenField ID="HiddenUploadid" runat="server" />
        <asp:Panel ID="pnl_UploadDoc" runat="server" class="darkPanlAlumini" >
            <div class="panel-cover inner_darkPanlAlumini">
                <div>
                    <div style="float: left; width: 88%;">
                        <b>Upload Document</b>

                    </div>
                    <div style="float: right; vertical-align: top;">
                        <asp:LinkButton ForeColor="red" ID="lbtnUploadDocClose" ToolTip="click here to close"
                            CssClass="" runat="server" Text="X" Font-Underline="false" CausesValidation="false">X</asp:LinkButton>
                    </div>
                </div>
                <table width="100%" cellpadding="0" cellspacing="0" style="color: Black">
                    <tr class="matters" style="color: Black">
                        <td><span class="field-label">Document</span></td>
                        <td>
                            <asp:Label ID="lblDoc" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr class="matters" style="color: Black">
                        <td><span class="field-label">Upload Document</span></td>
                        <td>
                            <asp:FileUpload ID="FileUpload_stud" runat="server" />
                        </td>
                    </tr>
                   
                    <%--  <tr class="matters" style="color:Black">
                <td >Verified Document</td>    
                <td style="width:1px">:&nbsp;</td>            
                <td>
                    <asp:CheckBox ID="chb_bverified" Checked="true" runat="server" /> </td>
                </tr> --%>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lbluploadError" runat="server" Text="" EnableViewState="false"
                                ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnuploadDoc" runat="server" Text="Upload" CausesValidation="true" Visible="false"
                                ValidationGroup="Upload" CssClass="button" />
                            <asp:Button ID="btnuploadChangeDoc" runat="server" Text="Upload new document" CausesValidation="true" Visible="false"
                                ValidationGroup="Upload" CssClass="button" />
                            <asp:Button ID="btnCancelupload" runat="server" Text="Close" CausesValidation="false"
                                CssClass="button" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
