﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true" CodeFile="encSetBSU.aspx.vb" Inherits="Students_encSetBSU" title="Untitled Page" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master"  %>
 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %> 
<%@ OutputCache Duration="1" VaryByParam="none" Location="None" NoStore="true" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

   <style>
       .RadTreeView_Default {
    
    font: normal 12px/16px 'Nunito', sans-serif !important;
}
   </style>
 <script language="javascript" type="text/javascript">

  function getUser()
     {     
        var sFeatures;
        sFeatures="dialogWidth: 700px; ";
        sFeatures+="dialogHeight: 500px; ";
        sFeatures+="help: no; ";
        sFeatures+="resizable: no; ";
        sFeatures+="scroll: yes; ";
        sFeatures+="status: no; ";
        sFeatures+="unadorned: no; ";
        var NameandCode;
        var result;
        var oWnd = radopen("encSelUsername.aspx?ID=USR", "pop_usr");
        <%--result = window.showModalDialog("encSelUsername.aspx?ID=USR", "", sFeatures)
        if(result != '' && result != undefined)
        {
            NameandCode = result.split('___');
            document.getElementById('<%=h_username.ClientID %>').value = NameandCode[0];
            document.getElementById('<%=txtUsers.ClientID %>').value = NameandCode[1];
            document.getElementById('<%= btnTest.ClientID %>').click();
        }
        return false;--%>
  }
     function OnClientClose(oWnd, args) {
            
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Result.split('||');
                  document.getElementById('<%=h_username.ClientID %>').value = NameandCode[0];
                  document.getElementById('<%=txtUsers.ClientID %>').value = NameandCode[1];
                  document.getElementById('<%= btnTest.ClientID %>').click();
                   //__doPostBack('<%= txtUsers.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        } 
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_usr" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>         
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Set BSU"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <asp:Button ID="btnTest" runat="server" Style="display: none;" />
                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                     <tr valign="top">
                        <td align="left" colspan="2" >
                            <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                    
                        <td align="left" width="20%" valign="top"><span class="field-label">Users</span> 
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtUsers" runat="server"></asp:TextBox>
                            <%--<asp:Button ID="btnUser" runat="server" CssClass="button" OnClientClick="getUser();return false;"
                                Text="..." Width="20px" />--%>
                            <asp:ImageButton ID="btnUser" runat="server" ImageUrl="../Images/cal.gif"
                                OnClientClick="getUser();return false;" TabIndex="18"></asp:ImageButton>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                ControlToValidate="txtUsers"
                                Display="Static"
                                ErrorMessage="*Select User name"
                                runat="server" ValidationGroup="MAINERROR" />                      
                        </td>
                      
                      
                  </tr>
                    <tr>    
                         <td><span class="field-label">Business Units</span></td>
                                       
                        <td align="left" >
                            <asp:Panel runat="server" ID="Panel2" CssClass="field-label">
                                <div>
                                    <telerik:RadTreeView ID="rtvMenuRights" runat="server" 
                                        CheckBoxes="True"
                                        CheckChildNodes="true">
                                        <DataBindings>
                                            <telerik:RadTreeNodeBinding></telerik:RadTreeNodeBinding>
                                        </DataBindings>
                                    </telerik:RadTreeView>
                                    <br />
                                </div>
                            </asp:Panel>

                        </td>
                       
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="MAINERROR" />
                        </td>
                    </tr>                   
                </table>
                <asp:HiddenField ID="h_username" runat="server" />

            </div>
        </div>
    </div> 

</asp:Content>

