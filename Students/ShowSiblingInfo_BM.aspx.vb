Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class ShowSiblingInfo_BM
    Inherits System.Web.UI.Page
    Dim SearchMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                'SearchMode = "SI" 'Request.QueryString("id")
                ViewState("TYPE") = ""
                SearchMode = Request.QueryString("id")
                If Not Request.QueryString("TYPE") Is Nothing Then
                    ViewState("TYPE") = Request.QueryString("TYPE")
                End If

                If SearchMode = "SI" Then
                    Page.Title = "Student Info"
                End If

                h_Selected_menu_0.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                gridbind()

            Catch ex As Exception

            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_selected_menu_0.Value.Split("__")
        getid0(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

    End Sub

    Public Function getid0(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_0_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

            Dim str1 As String = "SELECT COUNT(*) FROM BM.INCIDENT_ROLE_ACCESS WHERE ROLE_ID='" & Session("sroleid") & "' AND BSU_ID='" & Session("sBsuid") & "'"
            Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str1)





            Dim str_Sql As String = ""

            Dim str_filter_FeeID As String
            Dim str_filter_StudName As String
            Dim str_filter_StudNo As String
            Dim str_filter_SchoolName As String
            Dim temp_name As String = String.Empty
            Dim ds As New DataSet

            Dim lblFeeID As New Label
            Dim lblStudName As New Label
            Dim lblStudNo As New Label
            Dim lblSchoolName As New Label

            Dim txtSearch As New TextBox

            Dim str_txtFeeID, str_txtStudName, str_txtStudNo, str_txtSchoolName As String

            Dim str_search As String
            str_txtFeeID = ""
            str_txtStudName = ""
            str_txtStudNo = ""
            str_txtSchoolName = ""

            str_filter_FeeID = ""
            str_filter_StudName = ""
            str_filter_StudNo = ""
            str_filter_SchoolName = ""

            Using Userreader As SqlDataReader = AccessRoleUser.GetBUnitImage(Session("sBsuid"))
                While Userreader.Read
                    temp_name = Convert.ToString(Userreader("BSU_NAME"))
                End While
            End Using


            str_filter_SchoolName = " AND a.SchoolName LIKE '%" & temp_name & "%'"



            Dim CurBsUnit As String = Session("sBsuid").ToString & ""
            Dim CurACDID As String = Session("Current_ACD_ID").ToString & ""
            SearchMode = Request.QueryString("id")

            If SearchMode = "SI" Then 'get student detail
                If sbm = 0 Then
                    str_Sql = " Select SIBLING_ID,FeeID,StudNo,StudName, SchoolName,'' as Sibling from(SELECT  STUDENT_M.STU_NO as StudNo,   ISNULL(STUDENT_M.STU_FIRSTNAME, '') + '  ' + ISNULL(STUDENT_M.STU_MIDNAME, '') + '  ' + ISNULL(STUDENT_M.STU_LASTNAME, '') " & _
                         " AS StudName, STUDENT_M.STU_FEE_ID AS FeeID, BUSINESSUNIT_M.BSU_NAME as SchoolName, STUDENT_M.STU_SIBLING_ID as SIBLING_ID " & _
                        " FROM  STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID INNER JOIN " & _
                         " BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID and STUDENT_M.STU_ACD_ID=" & CurACDID & " AND STUDENT_M.STU_BSU_ID='" & CurBsUnit & "')a where a.SIBLING_ID<>'' "
                Else
                    str_Sql = " Select SIBLING_ID,FeeID,StudNo,StudName, SchoolName,'' as Sibling from(SELECT  STUDENT_M.STU_NO as StudNo,   ISNULL(STUDENT_M.STU_FIRSTNAME, '') + '  ' + ISNULL(STUDENT_M.STU_MIDNAME, '') + '  ' + ISNULL(STUDENT_M.STU_LASTNAME, '') " & _
                         " AS StudName, STUDENT_M.STU_FEE_ID AS FeeID, BUSINESSUNIT_M.BSU_NAME as SchoolName, STUDENT_M.STU_SIBLING_ID as SIBLING_ID " & _
                        " FROM  STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID INNER JOIN " & _
                         " BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID and STUDENT_M.STU_ACD_ID=" & CurACDID & " AND STUDENT_M.STU_BSU_ID='" & CurBsUnit & "' AND STU_GRM_ID IN(select GRM_ID FROM vw_BSU_TUTOR WHERE SEC_EMP_ID='" & Session("EmployeeId") & "' AND SEC_bsu_id='" & Session("sBsuid") & "'))a where a.SIBLING_ID<>'' "

                End If
            End If

            If SearchMode = "SE" Then 'get student detail
                If ViewState("TYPE") <> "" Then
                    str_Sql = "SELECT  SIBLING_ID,FeeID,StudNo,StudName, SchoolName, stu_sibling_id as Sibling from(SELECT  STUDENT_M.STU_NO as StudNo,STUDENT_M.stu_sibling_id,   ISNULL(STUDENT_M.STU_FIRSTNAME, '') + '  ' + ISNULL(STUDENT_M.STU_MIDNAME, '') + '  ' + ISNULL(STUDENT_M.STU_LASTNAME, '') " & _
                   " AS StudName, STUDENT_M.STU_FEE_ID AS FeeID, BUSINESSUNIT_M.BSU_NAME as SchoolName, isnull(STUDENT_M.STU_TWINS_ID,STUDENT_M.STU_ID) as SIBLING_ID " & _
                   " FROM  STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID INNER JOIN " & _
                   " BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID AND STUDENT_M.STU_BSU_ID='" & CurBsUnit & "' AND STUDENT_M.STU_ACD_ID=" & CurACDID & "  )a WHERE a.StudNo <> ''"
                Else
                    str_Sql = "SELECT SIBLING_ID,FeeID,StudNo,StudName, SchoolName, stu_sibling_id as Sibling from(SELECT  STUDENT_M.STU_NO as StudNo,STUDENT_M.stu_sibling_id,   ISNULL(STUDENT_M.STU_FIRSTNAME, '') + '  ' + ISNULL(STUDENT_M.STU_MIDNAME, '') + '  ' + ISNULL(STUDENT_M.STU_LASTNAME, '') " & _
                   " AS StudName, STUDENT_M.STU_FEE_ID AS FeeID, BUSINESSUNIT_M.BSU_NAME as SchoolName, STUDENT_M.STU_SIBLING_ID as SIBLING_ID " & _
                   " FROM  STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID INNER JOIN " & _
                   " BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID AND STUDENT_M.STU_BSU_ID='" & CurBsUnit & "' AND STUDENT_M.STU_ACD_ID=" & CurACDID & "  )a WHERE a.StudNo <> ''"
                End If


                'AND stu_sibling_id IN(" & _
                '     " SELECT stu_sibling_id from  STUDENT_M  WHERE STU_BSU_ID='" & CurBsUnit & "' group by stu_sibling_id having count(stu_sibling_id)>=1)
            End If



            If gvSiblingInfo.Rows.Count > 0 Then

                Dim str_Sid_search() As String


                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_0.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtFeeID")
                str_txtFeeID = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_FeeID = " AND a.FeeID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FeeID = " AND a.FeeID NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FeeID = " AND a.FeeID LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FeeID = " AND a.FeeID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FeeID = " AND a.FeeID LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FeeID = " AND a.FeeID NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudNo")
                str_filter_StudNo = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_StudNo = " AND a.StudNo LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_StudNo = "  AND  NOT a.StudNo LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_StudNo = " AND a.StudNo  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_StudNo = " AND a.StudNo  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_StudNo = " AND a.StudNo LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_StudNo = " AND a.StudNo NOT LIKE '%" & txtSearch.Text & "'"
                End If


                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudName")
                str_txtStudName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_StudName = " AND a.StudName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_StudName = "  AND  NOT a.StudName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_StudName = " AND a.StudName  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_StudName = " AND a.StudName  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_StudName = " AND a.StudName LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_StudName = " AND a.StudName NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtSchoolName")
                str_txtSchoolName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_SchoolName = " AND a.SchoolName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_SchoolName = "  AND  NOT a.SchoolName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_SchoolName = " AND a.SchoolName  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_SchoolName = " AND a.SchoolName  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_SchoolName = " AND a.SchoolName LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_SchoolName = " AND a.SchoolName NOT LIKE '%" & txtSearch.Text & "'"
                End If

            End If

            If SearchMode = "SE" Then
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_StudNo & str_filter_StudName)
            Else
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_FeeID & str_filter_StudNo & str_filter_StudName & str_filter_SchoolName & "order by a.StudName")
            End If
            ' ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_FeeID & str_filter_StudNo & str_filter_StudName & str_filter_SchoolName & "order by a.StudName")



            gvSiblingInfo.DataSource = ds.Tables(0)
            ' gvSiblingInfo.TemplateControl.FindControl("label1"). = ds.Tables(0).Columns("emp_ID")



            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvSiblingInfo.DataBind()
                Dim columnCount As Integer = gvSiblingInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.


                gvSiblingInfo.Rows(0).Cells.Clear()
                gvSiblingInfo.Rows(0).Cells.Add(New TableCell)
                gvSiblingInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSiblingInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSiblingInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

                'gvSiblingInfo.HeaderRow.Visible = True
            Else
                gvSiblingInfo.DataBind()


            End If

            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtFeeID")
            txtSearch.Text = str_txtFeeID
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = str_txtStudName
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudNo")
            txtSearch.Text = str_txtStudNo
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtSchoolName")
            txtSearch.Text = str_txtSchoolName

            set_Menu_Img()

            'Page.Title = "Employee Info"

        Catch ex As Exception
            UtilityObj.Errorlog("Error In collecting sibling info")
        End Try





    End Sub

    'Protected Sub lblStudName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim lblStudNo As New Label
    '    Dim lblStudID As New Label
    '    Dim lbClose As New LinkButton


    '    lbClose = sender

    '    lblStudNo = sender.Parent.FindControl("lblStudNo")
    '    lblStudID = sender.parent.findcontrol("lblStudID")

    '    ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
    '    Dim l_Str_Msg As String = lblStudNo.Text & "___" & lblStudID.Text
    '    l_Str_Msg = l_Str_Msg.Replace("'", "\'")

    '    If (Not lblStudNo Is Nothing) Then
    '        '   Response.Write(lblcode.Text)
    '        Response.Write("<script language='javascript'> function listen_window(){")
    '        Response.Write("window.returnValue = '" & l_Str_Msg & "';")


    '        Response.Write("window.close();")
    '        Response.Write("} </script>")
    '        h_SelectedId.Value = "Close"
    '    End If
    'End Sub

    Protected Sub gvSiblingInfo_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSiblingInfo.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lblStudID As Label = DirectCast(e.Row.FindControl("lblStudID"), Label)
            Dim lblStudNo As Label = DirectCast(e.Row.FindControl("lblStudNo"), Label)
            Dim lbCodeSubmit As LinkButton = DirectCast(e.Row.FindControl("lblStudName"), LinkButton)
            Dim l_Str_Msg As String = lblStudNo.Text & "___" & lblStudID.Text & "___" & lbCodeSubmit.Text
            l_Str_Msg = l_Str_Msg.Replace("'", "\'")
            If (Not lblStudNo Is Nothing) Then

                lbCodeSubmit.Attributes.Add("onClick", "return SetStudentValuetoParent('" & l_Str_Msg & "');")
            End If

        End If
    End Sub


    Protected Sub btnSearchFeeID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStudNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStudName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSchoolName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub


    Protected Sub gvSiblingInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSiblingInfo.PageIndexChanging
        gvSiblingInfo.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
End Class
