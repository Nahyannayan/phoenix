﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudEnq_Visible.aspx.vb" Inherits="Students_StudEnq_Visible" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Enquiry Validation Settings
        </div>
        <div class="card-body">
            <div class="table-responsive">




    <table id="tbl_AddGroup" runat="server" align="center" border="0" width="100%">
        <tr valign="bottom">
            <td align="left">
                <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <table align="center" width="100%" cellpadding="5" cellspacing="0">
                    <tr class="subheader_img">
                        <td align="left" colspan="4" class="title-bg">
                            
                                Apply Enquiry Visibility</td>
                    </tr>
                   
                   
                   <tr>
            <td align="center">
                <asp:GridView ID="gvVisible" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                    EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                     >
                    <RowStyle CssClass="griditem" />
                    <Columns>
                        <asp:TemplateField Visible="false">
                        <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("EQV_ID") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                        
                            <ItemTemplate>
                                <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("EQV_DESC") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Visible">
                           
                            <ItemTemplate>
                                 <asp:CheckBox id="cbVisible" runat="server" Checked='<%# Bind("EVD_bVISIBLE") %>' AutoPostBack="true" OnCheckedChanged ="CbV2_CheckedChanged"  />
                            </ItemTemplate>
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Validate">
                                      <ItemTemplate>
                                      <asp:CheckBox id="cbValid" runat="server" Checked='<%# Bind("EVD_bVALIDATE") %>' AutoPostBack="true" OnCheckedChanged ="CbV1_CheckedChanged"/>
                              </ItemTemplate>
                            <HeaderStyle Wrap="False"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                        </asp:TemplateField>
                        
                       
                    </Columns>
                    <SelectedRowStyle BackColor="Wheat" />
                    <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
                   <tr>
            <td  align="center">
               
                <asp:Button id="btnSav" runat="server" CssClass="button" Text="Update"  />
                
                </td>
        </tr>
                    </table>
                            
                                                      
            </td>
        </tr>
        
    </table>

                </div>
            </div>
        </div>

</asp:Content>

