﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Partial Class Students_studTCExitInterview
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100280") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    BindBSu()
                    BindCurriculum()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, ddlBsu.SelectedValue.ToString)
                    If Session("sbsuid") <> "999998" And ddlClm.Items.Count = 1 Then
                        trBsu.Visible = False
                    Else
                        If Session("sbsuid") <> "999998" Then
                            td1.Visible = False
                            ' td2.Visible = False
                            td3.Visible = False
                            td6.ColSpan = 11
                        End If
                        If ddlClm.Items.Count = 1 Then
                            td4.Visible = False
                            ' td5.Visible = False
                            td6.Visible = False
                            td3.ColSpan = 11
                        End If
                    End If
                    BindGrade()
                    BindSection()
                    trGrid.Visible = False
                    BindTCReason()

                    If Session("sbsuid") = "999998" Then
                        wExitPrincipal.Enabled = False
                        btnExitSave_Principal.Visible = False
                    Else
                        wExitCorp.Enabled = False
                        btnExitSave_Corp.Visible = False
                    End If
                End If
                BindStatus()
                gvStud.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Protected Sub ddlgvExit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvOnline_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvPrincipal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvCorporate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub lnkOnline_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTcmId As Label = TryCast(sender.findcontrol("lblTcmId"), Label)
        Dim lblStuId As Label = TryCast(sender.findcontrol("lblStuId"), Label)
        Dim lblName As Label = TryCast(sender.findcontrol("lblName"), Label)
        Dim lblGrade As Label = TryCast(sender.findcontrol("lblGrade"), Label)
        Dim lblSection As Label = TryCast(sender.findcontrol("lblSection"), Label)
        Dim lblOnlineFeedback As Label = TryCast(sender.findcontrol("lblOnlineFeedback"), Label)

        hfSTU_ID.Value = lblStuId.Text
        hfTCM_ID.Value = lblTcmId.Text

        lblExitOnlineMsg.Text = ""

        mdlOnline.Show()

        ShowExitForm(lblTcmId.Text, lblStuId.Text)
        txtExitFeedback.Text = lblOnlineFeedback.Text


        lblExitStudent.Text = lblName.Text
        lblExitClass.Text = lblGrade.Text + " " + lblSection.Text
    End Sub

    Protected Sub lnkPrincipal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTcmId As Label = TryCast(sender.findcontrol("lblTcmId"), Label)
        Dim lblStuId As Label = TryCast(sender.findcontrol("lblStuId"), Label)
        Dim lblName As Label = TryCast(sender.findcontrol("lblName"), Label)
        Dim lblGrade As Label = TryCast(sender.findcontrol("lblGrade"), Label)
        Dim lblSection As Label = TryCast(sender.findcontrol("lblSection"), Label)
        Dim lblSchoolFeedback As Label = TryCast(sender.findcontrol("lblSchoolFeedback"), Label)

        hfSTU_ID.Value = lblStuId.Text
        hfTCM_ID.Value = lblTcmId.Text

        lblExitGrade_School.Text = ""

        mdSchool.Show()

        txtExitPricipal.Text = lblSchoolFeedback.Text
        lblExitStudent_School.Text = lblName.Text
        lblExitGrade_School.Text = lblGrade.Text + " " + lblSection.Text
    End Sub
    Protected Sub lnkCorporate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTcmId As Label = TryCast(sender.findcontrol("lblTcmId"), Label)
        Dim lblStuId As Label = TryCast(sender.findcontrol("lblStuId"), Label)
        Dim lblName As Label = TryCast(sender.findcontrol("lblName"), Label)
        Dim lblGrade As Label = TryCast(sender.findcontrol("lblGrade"), Label)
        Dim lblSection As Label = TryCast(sender.findcontrol("lblSection"), Label)
        Dim lblCorpFeedback As Label = TryCast(sender.findcontrol("lblCorpFeedback"), Label)

        hfSTU_ID.Value = lblStuId.Text
        hfTCM_ID.Value = lblTcmId.Text

        lblExitCorpMsg.Text = ""


        mdCorp.Show()

        txtExitCorp.Text = lblCorpFeedback.Text
        lblExitStudent_Corp.Text = lblName.Text
        lblExitGrade_Corp.Text = lblGrade.Text + " " + lblSection.Text
    End Sub
    Protected Sub lnkPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTcmId As Label = TryCast(sender.findcontrol("lblTcmId"), Label)
        Dim lblStuId As Label = TryCast(sender.findcontrol("lblStuId"), Label)
        hfSTU_ID.Value = lblStuId.Text
        hfTCM_ID.Value = lblTcmId.Text
        DownloadReport(hfSTU_ID.Value, hfTCM_ID.Value)
    End Sub
    Sub BindTCReason()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "SELECT [TCR_CODE],[TCR_DESCR]  FROM [OASIS].[dbo].[TC_REASONS_M] order by [TCR_ORDER]"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        chkExitReason.DataSource = ds
        chkExitReason.DataTextField = "TCR_DESCR"
        chkExitReason.DataValueField = "TCR_CODE"
        chkExitReason.DataBind()
    End Sub


    Sub ShowExitForm(ByVal tcm_id As String, ByVal stu_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String



        Dim i As Integer

        For i = 0 To chkExitReason.Items.Count - 1
            chkExitReason.Items(i).Selected = False
        Next

        lblReason.Text = ""
        str_query = "SELECT STR_REASON_ID,ISNULL(STR_OTHER_REASON,'') FROM TCM_STU_REASON_S WHERE STR_STU_ID=" + stu_id _
                & " AND STR_TCM_ID=" + tcm_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        For i = 0 To ds.Tables(0).Rows.Count - 1

            If Not chkExitReason.Items.FindByValue(ds.Tables(0).Rows(i).Item(0)) Is Nothing Then
                chkExitReason.Items.FindByValue(ds.Tables(0).Rows(i).Item(0)).Selected = True
                If ds.Tables(0).Rows(i).Item(1) <> "" Then
                    lblReason.Text = ds.Tables(0).Rows(i).Item(1)
                End If
            End If
        Next

        str_query = "SELECT TQS_ID,TQS_SLNO,TQS_QUESTION FROM TC_EXITQUESTIONS_M ORDER BY TQS_SLNO"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlExitQ.DataSource = ds
        dlExitQ.DataBind()
    End Sub


#Region "Private Methods"
   
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindBSu()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE ISNULL(BSU_bGEMSSCHOOL,0)=1 AND ISNULL(BSU_bSCHOOL,0)=1"

        If Session("sbsuid") <> "999998" Then
            str_query += " AND BSU_ID='" + Session("sbsuid") + "'"
        End If

        str_query += " ORDER BY BSU_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub

    Sub BindCurriculum()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT DISTINCT CLM_DESCR,CLM_ID FROM CURRICULUM_M INNER JOIN ACADEMICYEAR_D ON " _
                                & " ACD_CLM_ID=CLM_ID AND ACD_CURRENT=1 AND ACD_BSU_ID='" + ddlBsu.SelectedValue.ToString + "' ORDER BY CLM_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClm.DataSource = ds
        ddlClm.DataTextField = "CLM_DESCR"
        ddlClm.DataValueField = "CLM_ID"
        ddlClm.DataBind()
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                              & " WHERE GRM_ACD_ID= " + ddlAcademicYear.SelectedValue.ToString _
                              & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
    End Sub
    Sub BindSection()
        ddlSection.Items.Clear()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        If ddlGrade.SelectedValue = "0" Then
            ddlSection.Items.Add(li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                    & " AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub

    Sub BindStatus()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SUM(CASE WHEN TCM_ENTRY_TYPE='PARENT' THEN 1 ELSE 0 END) AS ONLINETC," _
                                  & " SUM(CASE WHEN TCM_ENTRY_TYPE='SCHOOL' THEN 1 ELSE 0 END) AS SCHOOLTC," _
                                  & " SUM(CASE WHEN TCM_EXITTYPE='ONLINE' AND TCM_EXITSTATUS='COMPLETED' THEN 1 ELSE 0 END) AS ONLINECOMPLETED," _
                                  & " SUM(CASE WHEN TCM_EXITTYPE='ONLINE' AND TCM_EXITSTATUS='PENDING' THEN 1 ELSE 0 END) AS ONLINEPENDING," _
                                  & " SUM(CASE WHEN TCM_EXITTYPE='PRINCIPAL' AND TCM_EXITSTATUS='COMPLETED' THEN 1 ELSE 0 END) AS PRINCIPALCOMPLETED," _
                                  & " SUM(CASE WHEN TCM_EXITTYPE='PRINCIPAL' AND TCM_EXITSTATUS='PENDING' THEN 1 ELSE 0 END) AS PRINCIPALPENDING," _
                                  & " SUM(CASE WHEN TCM_EXITTYPE='CORPORATE' AND TCM_EXITSTATUS='COMPLETED' THEN 1 ELSE 0 END) AS CORPORATECOMPLETED," _
                                  & " SUM(CASE WHEN TCM_EXITTYPE='CORPORATE' AND TCM_EXITSTATUS='PENDING' THEN 1 ELSE 0 END) AS CORPORATEPENDING" _
                                  & " FROM TCM_M INNER JOIN STUDENT_M ON TCM_STU_ID=STU_ID WHERE TCM_TCSO='TC' AND TCM_CANCELDATE IS NULL " _
                                  & " AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

            If ddlGrade.SelectedValue <> "0" Then
                str_query += " AND STU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
            End If

            If ddlSection.SelectedValue.ToString <> "0" Then
                str_query += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
            End If

            If txtApplyStart.Text <> "" Then
                str_query += " AND TCM_APPLYDATE>='" + txtApplyStart.Text + "'"
            End If

            If txtApplyEnd.Text <> "" Then
                str_query += " AND TCM_APPLYDATE<='" + txtApplyEnd.Text + "'"
            End If


            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count > 0 Then
                With ds.Tables(0).Rows(0)
                    lblOnline.Text = .Item("ONLINETC")
                    lblSchool.Text = .Item("SCHOOLTC")
                    lblOnline_C.Text = .Item("ONLINECOMPLETED")
                    lblOnline_P.Text = .Item("ONLINEPENDING")
                    lblPrincipal_C.Text = .Item("PRINCIPALCOMPLETED")
                    lblPincipal_P.Text = .Item("PRINCIPALPENDING")
                    lblCorporate_C.Text = .Item("CORPORATECOMPLETED")
                    lblCorporate_P.Text = .Item("CORPORATEPENDING")
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub GridBind()
        'red #E60000
        'green 	#008F00
        'blue #000099
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT TCM_ID,TCM_STU_ID,TCM_APPLYDATE,TCM_EXITTYPE,ISNULL(TCM_EXITSTATUS,'PENDING')TCM_EXITSTATUS , " _
        '                        & " STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME, " _
        '                        & " GRM_DISPLAY,SCT_DESCR,ISNULL(TCM_EXITFEEDBACK_PARENT,'') TCM_EXITFEEDBACK_PARENT, " _
        '                        & " ISNULL(TCM_EXITFEEDBACK_SCHOOL,'') TCM_EXITFEEDBACK_SCHOOL,ISNULL(TCM_EXITFEEDBACK_CORP,'') TCM_EXITFEEDBACK_CORP, " _
        '                        & " CASE WHEN  TCM_EXITTYPE='ONLINE' THEN CASE WHEN ISNULL(TCM_EXITFEEDBACK_PARENT,'')<>'' THEN '#008F00' ELSE '#E60000' END  " _
        '                        & " ELSE CASE WHEN ISNULL(TCM_EXITFEEDBACK_PARENT,'')<>'' THEN '#008F00' ELSE '#000099' END END AS ONLINECOLOR," _
        '                        & " CASE WHEN  TCM_EXITTYPE='PRINCIPAL' THEN CASE WHEN ISNULL(TCM_EXITFEEDBACK_SCHOOL,'')<>'' THEN '#008F00' ELSE '#E60000' END  " _
        '                        & " ELSE CASE WHEN ISNULL(TCM_EXITFEEDBACK_SCHOOL,'')<>'' THEN '#008F00' ELSE '#000099' END END AS SCHOOLCOLOR," _
        '                        & " CASE WHEN  TCM_EXITTYPE='CORPORATE' THEN CASE WHEN ISNULL(TCM_EXITFEEDBACK_CORP,'')<>'' THEN '#008F00' ELSE '#E60000' END  " _
        '                        & " ELSE CASE WHEN ISNULL(TCM_EXITFEEDBACK_CORP,'')<>'' THEN '#008F00' ELSE '#000099' END END AS CORPORATECOLOR" _
        '                        & " FROM STUDENT_M AS A " _
        '                        & " INNER JOIN TCM_M AS B ON STU_ID=TCM_STU_ID " _
        '                        & " INNER JOIN GRADE_BSU_M ON STU_GRM_ID=GRM_ID" _
        '                        & " INNER JOIN SECTION_M ON STU_SCT_ID=SCT_ID " _
        '                        & " WHERE STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
        '                        & " AND TCM_TCSO='TC'"


        Dim str_query As String = "SELECT TCM_ID,TCM_STU_ID,TCM_APPLYDATE,TCM_EXITTYPE,ISNULL(TCM_EXITSTATUS,'PENDING')TCM_EXITSTATUS , " _
                                     & " STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME, " _
                                     & " GRM_DISPLAY,SCT_DESCR,ISNULL(TCM_EXITFEEDBACK_PARENT,'') TCM_EXITFEEDBACK_PARENT, " _
                                     & " ISNULL(TCM_EXITFEEDBACK_SCHOOL,'') TCM_EXITFEEDBACK_SCHOOL,ISNULL(TCM_EXITFEEDBACK_CORP,'') TCM_EXITFEEDBACK_CORP, " _
                                     & " CASE WHEN  TCM_EXITTYPE='ONLINE' THEN CASE WHEN ISNULL(TCM_EXITFEEDBACK_PARENT,'')<>'' THEN '#008F00' ELSE '#E60000' END  " _
                                     & " ELSE CASE WHEN (SELECT COUNT(TCE_ID) FROM TCM_STUDENT_EXITQUESTIONS WHERE TCE_TCM_ID=B.TCM_ID AND TCE_STU_ID=A.STU_ID)>0 THEN '#008F00' ELSE '#000099' END END AS ONLINECOLOR," _
                                     & " CASE WHEN  TCM_EXITTYPE='PRINCIPAL' THEN CASE WHEN ISNULL(TCM_EXITFEEDBACK_SCHOOL,'')<>'' THEN '#008F00' ELSE '#E60000' END  " _
                                     & " ELSE CASE WHEN ISNULL(TCM_EXITFEEDBACK_SCHOOL,'')<>'' THEN '#008F00' ELSE '#000099' END END AS SCHOOLCOLOR," _
                                     & " CASE WHEN  TCM_EXITTYPE='CORPORATE' THEN CASE WHEN ISNULL(TCM_EXITFEEDBACK_CORP,'')<>'' THEN '#008F00' ELSE '#E60000' END  " _
                                     & " ELSE CASE WHEN ISNULL(TCM_EXITFEEDBACK_CORP,'')<>'' THEN '#008F00' ELSE '#000099' END END AS CORPORATECOLOR" _
                                     & " FROM STUDENT_M AS A " _
                                     & " INNER JOIN TCM_M AS B ON STU_ID=TCM_STU_ID " _
                                     & " INNER JOIN GRADE_BSU_M ON STU_GRM_ID=GRM_ID" _
                                     & " INNER JOIN SECTION_M ON STU_SCT_ID=SCT_ID " _
                                     & " WHERE STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                     & " AND TCM_TCSO='TC'"

        If rdOnline.Checked = True Then
            str_query += " AND ISNULL(TCM_ENTRY_TYPE,'SCHOOL')='PARENT'"
        ElseIf rdSchool.Checked = True Then
            str_query += " AND ISNULL(TCM_ENTRY_TYPE,'SCHOOL')='SCHOOL'"
        ElseIf rdAll.Checked = True Then
            str_query += " AND TCM_CANCELDATE IS NULL"
        End If

        If rdCancel.Checked = True Then
            str_query += " AND TCM_CANCELDATE IS NOT NULL"
        Else
            str_query += " AND TCM_CANCELDATE IS NULL"
        End If
        If ddlExitStatus.SelectedValue <> "ALL" Then
            str_query += " AND TCM_EXITSTATUS='" + ddlExitStatus.SelectedValue.ToString + "'"
        End If

        If txtApplyStart.Text <> "" Then
            str_query += " AND TCM_APPLYDATE>='" + txtApplyStart.Text + "'"
        End If

        If txtApplyEnd.Text <> "" Then
            str_query += " AND TCM_APPLYDATE<='" + txtApplyEnd.Text + "'"
        End If

        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        End If

        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        If txtStudentID.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStudentID.Text + "%'"
        End If

        Dim ddlgvStatus As New DropDownList
        Dim strStatus As String = ""

        Dim ddlgvExit As New DropDownList
        Dim strExit As String = ""


        Dim ddlgvOnline As New DropDownList
        Dim strOnline As String = ""

        Dim ddlgvPrincipal As New DropDownList
        Dim strPrincipal As String = ""

        Dim ddlgvCorporate As New DropDownList
        Dim strCorporate As String = ""


        If gvStud.Rows.Count > 0 Then
            ddlgvStatus = gvStud.HeaderRow.FindControl("ddlgvStatus")
            If ddlgvStatus.SelectedItem.Text = "Completed" Then
                str_query += " AND ISNULL(TCM_EXITSTATUS,'')='COMPLETED'"
            ElseIf ddlgvStatus.SelectedItem.Text = "Not Completed" Then
                str_query += " AND ISNULL(TCM_EXITSTATUS,'PENDING')='PENDING'"
            End If
            strStatus = ddlgvStatus.SelectedItem.Text

            ddlgvExit = gvStud.HeaderRow.FindControl("ddlgvExit")
            If ddlgvExit.SelectedItem.Text <> "ALL" Then
                str_query += " AND ISNULL(TCM_EXITTYPE,'')='" + ddlgvExit.SelectedItem.Text + "'"
            End If
            strExit = ddlgvExit.SelectedItem.Text


            ddlgvOnline = gvStud.HeaderRow.FindControl("ddlgvOnline")
            If ddlgvOnline.SelectedItem.Text = "Completed" Then
                str_query += " AND ISNULL(TCM_EXITFEEDBACK_PARENT,'')<>''"
            ElseIf ddlgvOnline.SelectedItem.Text = "Not Completed" Then
                str_query += " AND ISNULL(TCM_EXITFEEDBACK_PARENT,'')=''"
            End If
            strOnline = ddlgvOnline.SelectedItem.Text

            ddlgvOnline = gvStud.HeaderRow.FindControl("ddlgvOnline")
            If ddlgvOnline.SelectedItem.Text = "Completed" Then
                str_query += " AND ISNULL(TCM_EXITFEEDBACK_PARENT,'')<>''"
            ElseIf ddlgvOnline.SelectedItem.Text = "Not Completed" Then
                str_query += " AND ISNULL(TCM_EXITFEEDBACK_PARENT,'')=''"
            End If
            strOnline = ddlgvOnline.SelectedItem.Text


            ddlgvPrincipal = gvStud.HeaderRow.FindControl("ddlgvPrincipal")
            If ddlgvPrincipal.SelectedItem.Text = "Completed" Then
                str_query += " AND ISNULL(TCM_EXITFEEDBACK_SCHOOL,'')<>''"
            ElseIf ddlgvPrincipal.SelectedItem.Text = "Not Completed" Then
                str_query += " AND ISNULL(TCM_EXITFEEDBACK_SCHOOL,'')=''"
            End If
            strPrincipal = ddlgvPrincipal.SelectedItem.Text


            ddlgvCorporate = gvStud.HeaderRow.FindControl("ddlgvCorporate")
            If ddlgvCorporate.SelectedItem.Text = "Completed" Then
                str_query += " AND ISNULL(TCM_EXITFEEDBACK_CORP,'')<>''"
            ElseIf ddlgvCorporate.SelectedItem.Text = "Not Completed" Then
                str_query += " AND ISNULL(TCM_EXITFEEDBACK_CORP,'')=''"
            End If
            strCorporate = ddlgvCorporate.SelectedItem.Text


        End If

        str_query += " ORDER BY TCM_APPLYDATE DESC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()
        If gvStud.Rows.Count > 0 Then
            If strStatus <> "" Then
                ddlgvStatus = gvStud.HeaderRow.FindControl("ddlgvStatus")
                ddlgvStatus.Items.FindByText(strStatus).Selected = True

                ddlgvExit = gvStud.HeaderRow.FindControl("ddlgvExit")
                ddlgvExit.Items.FindByText(strExit).Selected = True

                ddlgvOnline = gvStud.HeaderRow.FindControl("ddlgvOnline")
                ddlgvOnline.Items.FindByText(strOnline).Selected = True

                ddlgvPrincipal = gvStud.HeaderRow.FindControl("ddlgvPrincipal")
                ddlgvPrincipal.Items.FindByText(strPrincipal).Selected = True

                ddlgvCorporate = gvStud.HeaderRow.FindControl("ddlgvCorporate")
                ddlgvCorporate.Items.FindByText(strCorporate).Selected = True
            End If
        End If

    End Sub

    Sub SaveExitForm()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim rdSA As RadioButton
        Dim rdA As RadioButton
        Dim rdDA As RadioButton
        Dim rdSDA As RadioButton
        Dim rdNA As RadioButton
        Dim lblTqsId As Label
        Dim sAnswer As String

        For i = 0 To dlExitQ.Items.Count - 1
            With dlExitQ.Items(i)
                sAnswer = ""

                rdSA = .FindControl("rdSA")
                rdA = .FindControl("rdA")
                rdDA = .FindControl("rdDA")
                rdSDA = .FindControl("rdSDA")
                rdNA = .FindControl("rdNA")
                lblTqsId = .FindControl("lblTqsId")

                If rdSA.Checked = True Then
                    sAnswer = rdSA.Text
                End If

                If rdA.Checked = True Then
                    sAnswer = rdA.Text
                End If

                If rdDA.Checked = True Then
                    sAnswer = rdDA.Text
                End If

                If rdSDA.Checked = True Then
                    sAnswer = rdSDA.Text
                End If

                If rdNA.Checked = True Then
                    sAnswer = rdNA.Text
                End If

                If sAnswer = "" Or txtExitFeedback.Text = "" Then
                    lblExitOnlineMsg.Text = "Please complete all the fields"
                    mdlOnline.Show()
                    Exit Sub
                End If


                str_query = "saveTCM_STUDENT_EXITQUESTIONS " _
                        & " @STU_ID=" + hfstu_id.value + "," _
                        & " @TCM_ID=" + hfTCM_ID.Value + "," _
                        & " @TQS_ID=" + lblTqsId.Text + "," _
                        & " @ANSWER='" + sAnswer + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End With
        Next

        str_query = " exec updateTCFEEDBACK " _
                             & "@STU_ID =" + hfstu_id.value + "," _
                             & "@TCM_ID=" + hfTCM_ID.Value + "," _
                             & "@FEEDBACK='" + txtExitFeedback.Text.Replace("'", "''") + "'," _
                             & "@TYPE ='PARENT'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

       
    End Sub

    Sub SaveExitPrincipal()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = " exec updateTCFEEDBACK " _
                            & "@STU_ID =" + hfSTU_ID.Value + "," _
                            & "@TCM_ID=" + hfTCM_ID.Value + "," _
                            & "@FEEDBACK='" + txtExitPricipal.Text.Replace("'", "''") + "'," _
                            & "@TYPE ='SCHOOL'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        str_query = "EXEC INSERTEMAILS_TC " _
                     & " @BSU_ID ='" + Session("SBSUID") + "'," _
                     & " @STU_ID ='" + hfSTU_ID.Value + "'," _
                     & " @EMAILTYPE='TC_EXIT_COMPLETED'," _
                     & " @SENDER='SCHOOL'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


    End Sub


    Sub SaveExitCorp()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = " exec updateTCFEEDBACK " _
                            & "@STU_ID =" + hfSTU_ID.Value + "," _
                            & "@TCM_ID=" + hfTCM_ID.Value + "," _
                            & "@FEEDBACK='" + txtExitCorp.Text.Replace("'", "''") + "'," _
                            & "@TYPE ='CORPORATE'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


        str_query = "EXEC INSERTEMAILS_TC " _
                 & " @BSU_ID ='" + Session("SBSUID") + "'," _
                 & " @STU_ID ='" + hfSTU_ID.Value + "'," _
                 & " @EMAILTYPE='TC_EXIT_COMPLETED'," _
                 & " @SENDER='SCHOOL'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    End Sub

    Sub BindExitAnswers(ByVal tqs_id As String, ByVal dRow As DataListItem)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCE_ANSWER FROM TCM_STUDENT_EXITQUESTIONS WHERE TCE_STU_ID=" + hfSTU_ID.Value _
                                & " AND TCE_TCM_ID=" + hfTCM_ID.Value + " AND TCE_TQS_ID=" + tqs_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            btnExitSave.Visible = False
            Select Case ds.Tables(0).Rows(0).Item(0).ToString.ToLower
                Case "strongly agree"
                    Dim rdSA As RadioButton = dRow.FindControl("rdSA")
                    rdSA.Checked = True
                Case "agree"
                    Dim rdA As RadioButton = dRow.FindControl("rdA")
                    rdA.Checked = True
                Case "disagree"
                    Dim rdDA As RadioButton = dRow.FindControl("rdDA")
                    rdDA.Checked = True
                Case "strongly disagree"
                    Dim rdSDA As RadioButton = dRow.FindControl("rdSDA")
                    rdSDA.Checked = True
                Case "na"
                    Dim rdNA As RadioButton = dRow.FindControl("rdNA")
                    rdNA.Checked = True
            End Select
        Else
            btnExitSave.Visible = True
        End If
    End Sub

    Function CheckExitDisagree(ByVal stu_id As String, ByVal tcm_id As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(TCE_STU_ID) FROM TCM_STUDENT_EXITQUESTIONS WHERE TCE_STU_ID=" + stu_id _
                                & " AND TCE_TCM_ID=" + tcm_id _
                                & " AND TCE_ANSWER IN('disagree','strongly disagree')"

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Function getLabel(ByVal lType As String)
        If lType = "principal" Then
            If Session("sbsuid") = "999998" Then
                Return "View"
            Else
                Return "View/Edit"
            End If
        Else
            If Session("sbsuid") = "999998" Then
                Return "View/Edit"
            Else
                Return "View"
            End If
        End If
    End Function

    Function getExitImage(ByVal status As String) As String
        If status = "PENDING" Then
            Return "~/Images/buttons/cross-icon.png"
        Else
            Return "~/Images/buttons/check-icon.png"
        End If
    End Function
#End Region

    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged
        BindCurriculum()
        If ddlClm.Items.Count = 1 Then
            td4.Visible = False
            ' td5.Visible = False
            td6.Visible = False
            td3.ColSpan = 11
        Else
            td4.Visible = True
            ' td5.Visible = True
            td6.Visible = True
            td3.ColSpan = 1
        End If
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, ddlBsu.SelectedValue.ToString)
        BindGrade()
        BindSection()
        BindStatus()
        trGrid.Visible = False
    End Sub

    Protected Sub ddlClm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClm.SelectedIndexChanged
        ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, ddlBsu.SelectedValue.ToString)
        BindGrade()
        BindSection()
        BindStatus()
        trGrid.Visible = False
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindStatus()
        BindSection()
        trGrid.Visible = False
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindStatus()
        GridBind()
        trGrid.Visible = True
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindStatus()
        trGrid.Visible = False
    End Sub

    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        BindStatus()
        trGrid.Visible = False
    End Sub

    Protected Sub btnExitSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitSave.Click
        SaveExitForm()
        GridBind()
    End Sub

    Protected Sub dlExitQ_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlExitQ.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or ListItemType.AlternatingItem Then
            Dim lblTqsId As Label = e.Item.FindControl("lblTqsId")
            BindExitAnswers(lblTqsId.Text, e.Item)
        End If

    End Sub

    Protected Sub btnExitSave_Principal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitSave_Principal.Click
        If txtExitPricipal.Text = "" Then
            lblExitPrincipalMsg.Text = "Please enter the feedback comments"
            mdSchool.Show()
            Exit Sub
        End If
        SaveExitPrincipal()
        GridBind()
    End Sub

   
    Protected Sub btnExitSave_Corp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitSave_Corp.Click
        If txtExitCorp.Text = "" Then
            lblExitCorpMsg.Text = "Please enter the feedback comments"
            mdCorp.Show()
            Exit Sub
        End If
        SaveExitCorp()
        GridBind()
    End Sub

    Protected Sub btnExitClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitClose.Click
        GridBind()
    End Sub

    Protected Sub btnExitClose_Principal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitClose_Principal.Click
        GridBind()
    End Sub

    Protected Sub btnExitClose_Corp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitClose_Corp.Click
        GridBind()
    End Sub

  
    Protected Sub gvStud_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub
    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblTcmId As Label = e.Row.FindControl("lblTcmId")
            Dim lblStuId As Label = e.Row.FindControl("lblStuId")
            Dim lnkDownload As LinkButton = e.Row.FindControl("lnkDownload")
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkDownload)
            If CheckExitDisagree(lblStuId.Text, lblTcmId.Text) = True Then
                e.Row.BackColor = Drawing.Color.LightSalmon
            End If
        End If
    End Sub

    Sub DownloadReport(ByVal stu_id As String, ByVal tcm_id As String)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@STU_ID", stu_id)
        param.Add("@TCM_ID", tcm_id)
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            rptClass.reportPath = Server.MapPath("../Students/Reports/Rpt/rptTCStudentExitinterview.rpt")
        End With
        Dim rpt As New ReportDownload
        rpt.LoadReports(rptClass, rs)
        'ReportLoadSelection()
    End Sub

    Sub LoadReports(ByVal rptClass As rptClass, Optional ByVal downloadtype As String = "")
        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer


            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues


            With rptClass
                rs.ReportDocument.Load(.reportPath)


                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)


                crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If
                If .selectionFormula <> "" Then
                    rs.ReportDocument.RecordSelectionFormula = .selectionFormula
                End If

                rs.ReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
                rs.ReportDocument.Close()
                rs.Dispose()

            End With
        Catch ex As Exception
            rs.Dispose()
        End Try
    End Sub

  
    


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next
        myReportDocument.VerifyDatabase()
    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)
                End If
            Next
        Next

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    
   
   
   
    '<ajaxToolkit:PopupControlExtender Position="Bottom" ID="BalloonPopupExtender2" PopupControlID="Panel2"
    '                                       TargetControlID="lblName"  runat="server" />
    '                                   <asp:Panel ID="Panel2" runat="server" Style="background: #fff;border:Solid 2px #003D7A" Width="300px" Height="200px"
    '                                       ScrollBars="Vertical">
    '                                       <table cellpadding="10">
    '                                           <tr>
    '                                               <td class="matters">
    '                                                   Parent Feedback :
    '                                                   <asp:Label ID="lblOnlineDisplay" runat="server" Text='<%# Bind("TCM_EXITFEEDBACK_PARENT") %>'></asp:Label>
    '                                               </td>
    '                                           </tr>
    '                                           <tr>
    '                                               <td class="matters">
    '                                                   Principal Feedback :
    '                                                   <asp:Label ID="lblSchoolDisplay" runat="server" Text='<%# Bind("TCM_EXITFEEDBACK_SCHOOL") %>'></asp:Label>
    '                                               </td>
    '                                           </tr>
    '                                           <tr>
    '                                               <td class="matters">
    '                                                   Corporate Feedback :
    '                                                   <asp:Label ID="lblCorpDisplay" runat="server" Text='<%# Bind("TCM_EXITFEEDBACK_CORP") %>'></asp:Label>
    '                                               </td>
    '                                           </tr>
    '                                       </table>
    '                                   </asp:Panel>


   
End Class
