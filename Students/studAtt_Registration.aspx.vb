Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports InfoSoftGlobal
Partial Class Students_studAtt_Registration
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub lbtnStudName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lblStud_ID As New Label
            Dim lbtnStudName As New LinkButton
            lbtnStudName = sender
            lblStudName.Text = lbtnStudName.Text

            lblStud_ID = TryCast(sender.FindControl("lblStud_ID"), Label)

            ViewState("G_STUID") = lblStud_ID.Text
      
            If ViewState("ATT_OPEN") = True Then
                If IsDate(txtDate.Text) = True Then
                    ViewState("G_TILLDATE") = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
                Else
                    ViewState("G_TILLDATE") = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                End If

            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    ViewState("G_TILLDATE") = ""
                Else
                    ViewState("G_TILLDATE") = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                End If
            End If

         

            lblGradeSect.Text = ddlGrade.SelectedItem.Text & " & " & ddlSection.SelectedItem.Text
            lblSession.Text = Trim(ddlAttType.SelectedItem.Text)
            lblDate.Text = ViewState("G_TILLDATE")
            Dim CONN As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim pParms(16) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcdYear.SelectedItem.Value)
            pParms(1) = New SqlClient.SqlParameter("@GRD_ID", ddlGrade.SelectedItem.Value)
            pParms(2) = New SqlClient.SqlParameter("@SCT_ID", ddlSection.SelectedItem.Value)
            pParms(3) = New SqlClient.SqlParameter("@ASON_DATE", ViewState("G_TILLDATE"))
            pParms(4) = New SqlClient.SqlParameter("@stu_id", lblStud_ID.Text)
            pParms(5) = New SqlClient.SqlParameter("@ATT_TYPE", Trim(ddlAttType.SelectedItem.Value))
            Using ATTREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "[ATT].[GETSTUDATT_REG_DETAIL]", pParms)
                If ATTREADER.HasRows Then
                    While ATTREADER.Read
                        lblTotWorking.Text = Convert.ToString(ATTREADER("tot_wrkdays"))
                        lblTillDateWorking.Text = Convert.ToString(ATTREADER("tot_tillDate"))
                        lblDayAbsent.Text = Convert.ToString(ATTREADER("tot_abs"))
                        lblDayPres.Text = Convert.ToString(ATTREADER("tot_present"))
                        lblTotalAttMarked.Text = Convert.ToString(ATTREADER("tot_marked"))
                    End While
                End If

            End Using

          


            bindAttChart()

            imgStud.ImageUrl = TransferPath(lblStud_ID.Text)
            Me.mdlPopup.Show()
            Call backGround()
        Catch ex As Exception
            lblError.Text = "Unexpected error"
            '~/Students/StudAtt_Chart.aspx?G_ACD=<%=ViewState("G_ACD") %>&G_GRD=<%=ViewState("G_GRD") %>&G_SCT=<%=ViewState("G_SCT") %>&G_STUID=<%=ViewState("G_STUID") %>&G_ATTTYPE=<%=ViewState("G_ATTTYPE") %>&G_TILLDATE=<%=ViewState("G_TILLDATE") %>&ACD_STARTDT=<%=ViewState("ACD_STARTDT") %>&ACD_ENDDT=<%=ViewState("ACD_ENDDT") %>
        End Try
    End Sub
    Sub bindAttChart()
        Try

    
            Dim ACD As String = ddlAcdYear.SelectedItem.Value
            Dim GRD As String = ddlGrade.SelectedItem.Value
            Dim SCT As String = ddlSection.SelectedItem.Value
            Dim VSDT As String = ViewState("ACD_STARTDT")
            Dim EDT As String = ViewState("ACD_ENDDT")
            Dim W1 As String = ViewState("BSU_WEEKEND1")
            Dim W2 As String = ViewState("BSU_WEEKEND2")
            Dim TillDate As String = ViewState("G_TILLDATE")
            Dim Att_Type As String = Trim(ddlAttType.SelectedItem.Value)
            Dim STUID As String = ViewState("G_STUID")


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim strXML As String
            Dim TMonth As String = String.Empty
            Dim TOT_ATT As String = String.Empty


            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}
            Dim i As Integer = 0

            Dim SDT As Date = VSDT

            strXML = ""
            strXML = strXML & "<graph caption='Yearly Attedance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"

            Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GETSTUATT_YEAR_STU_ID_SESSION(ACD, GRD, SCT, VSDT, EDT, STUID, Att_Type, W1, W2, TillDate)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read



                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 0, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT1")) & "'  color='AFD8F8'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 1, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT2")) & "' color='F6BD0F'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 2, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT3")) & "' color='8BBA00'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 3, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT4")) & "' color='FF8E46'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 4, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT5")) & "' color='008E8E'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 5, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT6")) & "' color='D64646'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 6, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT7")) & "' color='8E468E'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 7, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT8")) & "' color='588526'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 8, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT9")) & "' color='B3AA00'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 9, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT10")) & "' color='008ED6'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 10, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT11")) & "' color='9D080D'/>"
                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 11, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT12")) & "' color='A186BE'/>"


                    End While
                    strXML = strXML & "</graph>"
                    FCLiteral.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "460", "300", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Yearly Attedance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"
                    strXML = strXML & "<set name='Jan' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Feb' value='' color='F6BD0F'/>"
                    strXML = strXML & "<set name='Mar' value='' color='8BBA00'/>"
                    strXML = strXML & "<set name='Apr' value='' color='FF8E46'/>"
                    strXML = strXML & "<set name='May' value='' color='008E8E'/>"
                    strXML = strXML & "<set name='Jun' value='' color='D64646'/>"
                    strXML = strXML & "<set name='Jul' value='' color='8E468E'/>"
                    strXML = strXML & "<set name='Aug' value='' color='588526'/>"
                    strXML = strXML & "<set name='Sep' value='' color='B3AA00'/>"
                    strXML = strXML & "<set name='Oct' value='' color='008ED6'/>"
                    strXML = strXML & "<set name='Nov' value='' color='9D080D'/>"
                    strXML = strXML & "<set name='Dec' value='' color='A186BE'/>"
                    strXML = strXML & "</graph>"
                    FCLiteral.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "460", "300", False)
                End If
            End Using


        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub
    Function TransferPath(ByVal STU_ID As String) As String


        Dim FilePath As String = GETStud_photoPath(STU_ID) 'get the image
        Dim Virtual_Path As String = Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
        If FilePath.Trim = "" Then
            TransferPath = Virtual_Path + "/NOIMG/no_image.jpg"
        Else
            TransferPath = Virtual_Path + FilePath
        End If


    End Function
    Private Function GETStud_photoPath(ByVal STU_ID As String) As String
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"
        Dim RESULT As String
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            RESULT = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return RESULT

    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059015") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    hfDate.Value = DateTime.Now
                    hfdate2.Value = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    btnSave.Attributes.Add("onclick", "hideButton()")
                    btnSave2.Attributes.Add("onclick", "hideButton()")

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                    Call bindDate()

                    btnSave2.Visible = False
                    btnCancel1.Visible = False
                    btnCancel2.Visible = False
                    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                    Dim sql_query = "select isnull(BSU_ATT_OPEN,1) as BSU_ATT_OPEN from BUSINESSUNIT_M where BSU_ID='" & Session("sBsuid") & "'"
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
                    If ds.Tables(0).Rows.Count > 0 Then
                        ViewState("ATT_OPEN") = ds.Tables(0).Rows(0).Item("BSU_ATT_OPEN")
                    End If

                    If ViewState("ATT_OPEN") = True Then
                        ddlAttDate.Visible = False
                        txtDate.Visible = True
                        txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        imgCalendar.Visible = True
                        rfvDate.Visible = True
                    Else
                        txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        ddlAttDate.Visible = True
                        txtDate.Visible = False
                        imgCalendar.Visible = False
                        rfvDate.Visible = False
                    End If
                    Call bindAcademic_Year()

                    Call bindAtt_Type()

                    Call check_transport_status_bsu()

                    If ViewState("IsvalidBsu") = 0 Then
                        gvInfo.Columns(4).Visible = False
                        trStatus.Visible = False
                    End If
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub
    Sub bindCommon_DataReader()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " Select * from(SELECT     ACADEMICYEAR_D.ACD_STARTDT,ACADEMICYEAR_D.ACD_ENDDT, " & _
" BUSINESSUNIT_M.BSU_WEEKEND1,BUSINESSUNIT_M.BSU_WEEKEND2, ISNULL(BUSINESSUNIT_M.BSU_bAPP_LEAVE_ABSENT,'False') AS BSU_bAPP_LEAVE_ABSENT, " & _
" isnull(BUSINESSUNIT_M.BSU_ATT_OPEN,'False') as BSU_ATT_OPEN_id " & _
" FROM   ACADEMICYEAR_D INNER JOIN   BUSINESSUNIT_M ON ACADEMICYEAR_D.ACD_BSU_ID =  " & _
           " BUSINESSUNIT_M.BSU_ID WHERE(ACADEMICYEAR_D.ACD_ID = '" & ddlAcdYear.SelectedValue & "'))A"


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToString(readerStudent_Detail("ACD_STARTDT")))
                        ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToString(readerStudent_Detail("ACD_ENDDT")))
                        ViewState("BSU_WEEKEND1") = Convert.ToString(readerStudent_Detail("BSU_WEEKEND1"))
                        ViewState("BSU_WEEKEND2") = Convert.ToString(readerStudent_Detail("BSU_WEEKEND2"))
                        ViewState("bAPP_LEAVE_ABS") = Convert.ToBoolean(readerStudent_Detail("BSU_bAPP_LEAVE_ABSENT"))


                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub bindDate()
        Dim preDate As Date = Now.AddDays(-7).Date
        Dim di As ListItem
        Dim i As Integer = 1
        Do While i <= 7


            'If Not (UCase(preDate.AddDays(i).DayOfWeek.ToString) = ViewState("BSU_WEEKEND1").ToUpper.Trim Or UCase(preDate.AddDays(i).DayOfWeek.ToString) = ViewState("BSU_WEEKEND2").ToUpper.Trim) Then
            Dim Display_DT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", preDate.AddDays(i).Date)
            di = New ListItem(Display_DT, Display_DT)
            ddlAttDate.Items.Add(di)
            '  End If
            i = i + 1
        Loop
        '   If Not (UCase(Now.Date.DayOfWeek.ToString) = ViewState("BSU_WEEKEND1").ToUpper.Trim Or UCase(Now.Date.DayOfWeek.ToString) = ViewState("BSU_WEEKEND2").ToUpper.Trim) Then
        Dim Select_DT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", Now.Date)
        ddlAttDate.ClearSelection()
        ddlAttDate.Items.FindByValue(Select_DT).Selected = True
        '  End If

    End Sub
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("prev_ACD_ID").ToString & "','" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcdYear_SelectedIndexChanged(ddlAcdYear, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callStream_Bind()
        Try
            ddlStream.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            PARAM(1) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        bindAcademic_Grade()

    End Sub
    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim STM_ID As String = ddlStream.SelectedItem.Value
            Dim ds As New DataSet

            str_Sql = " SELECT distinct GRADE_BSU_M.GRM_DISPLAY, GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_M.GRD_DISPLAYORDER FROM  STAFF_AUTHORIZED_ATT INNER JOIN " &
 " GRADE_BSU_M ON STAFF_AUTHORIZED_ATT.SAD_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND  STAFF_AUTHORIZED_ATT.SAD_GRD_ID = GRADE_BSU_M.GRM_GRD_ID " &
" INNER JOIN  EMPLOYEE_M ON STAFF_AUTHORIZED_ATT.SAD_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " &
" where EMPLOYEE_M.EMP_ID ='" & Session("EmployeeId") & "' and STAFF_AUTHORIZED_ATT.SAD_ACD_ID='" & ACD_ID & "' and GRADE_BSU_M.GRM_STM_ID='" & STM_ID & "' order by GRADE_M.GRD_DISPLAYORDER "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGrade.Items.Clear()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlGrade.DataSource = ds.Tables(0)
                ddlGrade.DataTextField = "GRM_DISPLAY"
                ddlGrade.DataValueField = "GRD_ID"
                ddlGrade.DataBind()
            End If
            If Session("DEF_TUTOR") = "1" Then
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(Session("EMP_TUT_GRADE")).Selected = True
            End If

            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAcademic_Section()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim STM_ID As String = ddlStream.SelectedItem.Value
            Dim GRD_ID As String
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = ""
            End If

            Dim str_Sql As String = " SELECT distinct SECTION_M.SCT_DESCR as SCT_DESCR , STAFF_AUTHORIZED_ATT.SAD_SCT_ID as SCT_ID, SECTION_M.SCT_GRD_ID FROM  STAFF_AUTHORIZED_ATT INNER JOIN " &
 " SECTION_M ON STAFF_AUTHORIZED_ATT.SAD_ACD_ID = SECTION_M.SCT_ACD_ID AND STAFF_AUTHORIZED_ATT.SAD_SCT_ID = SECTION_M.SCT_ID AND STAFF_AUTHORIZED_ATT.SAD_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN " &
 " GRADE_BSU_M ON SECTION_M.SCT_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND SECTION_M.SCT_GRD_ID = GRADE_BSU_M.GRM_GRD_ID where STAFF_AUTHORIZED_ATT.SAD_EMP_ID='" & Session("EmployeeId") & "' and STAFF_AUTHORIZED_ATT.SAD_ACD_ID='" & ACD_ID & "' and SECTION_M.SCT_GRD_ID='" & GRD_ID & "' and GRADE_BSU_M.GRM_STM_ID='" & STM_ID & "' order by SECTION_M.SCT_DESCR"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSection.Items.Clear()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
            End If

            If Session("DEF_TUTOR") = "1" Then
                ddlSection.ClearSelection()
                ddlSection.Items.FindByValue(Session("EMP_TUT_SECTION")).Selected = True
            End If

            bindAtt_Type()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindAtt_Type()
    End Sub
    Protected Sub ddlAttDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindAtt_Type()

    End Sub
    Sub bindAtt_Type()
        Try
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim CheckDate As String
            ddlAttType.Items.Clear()
            If ViewState("ATT_OPEN") = True Then
                If ValidateDate() = "0" Then
                    CheckDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
                End If
            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    CheckDate = ""
                Else
                    CheckDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                    ' ddlAttDate.ClearSelection()
                End If
            End If
            Dim ATT_Types As String = String.Empty
            Dim GRD_ID As String = ddlGrade.SelectedItem.Value

            Dim separator As String() = New String() {"|"}
            Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GetAtt_type_NEW(ACD_ID, GRD_ID, CheckDate)
                While readerSTUD_ATT_ADD.Read
                    ATT_Types = ATT_Types + Convert.ToString(readerSTUD_ATT_ADD("ATT_TYPE")) + "|"
                End While
            End Using

            Dim strSplitArr As String() = ATT_Types.Split(separator, StringSplitOptions.RemoveEmptyEntries)

            For Each arrStr As String In strSplitArr

                If Array.IndexOf(strSplitArr, "Ses1&Ses2") >= 0 Then
                    ddlAttType.Items.Add(New ListItem("Session1", "Session1"))
                    ddlAttType.Items.Add(New ListItem("Session2", "Session2"))
                    Exit For
                Else
                    ddlAttType.Items.Add(New ListItem(arrStr, arrStr))
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call GetTutor()
        Call Freeze_date_chk()
        Call callStream_Bind()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call bindAcademic_Section()
    End Sub
    'Protected Sub btnCloseedit_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseedit.click

    'End Sub
    Protected Sub btnCloseedit_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Call backGround()
    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            ViewState("flag") = "0"

            Dim inputdt As DateTime

            If ViewState("ATT_OPEN") = True Then
                inputdt = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)

            Else
                inputdt = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
            End If

            Dim freezedt As DateTime
            If hfFreezeDate.Value = "" Then
                freezedt = String.Format("{0:" & OASISConstants.DateFormat & "}", "01/jan/1900")
            Else
                freezedt = String.Format("{0:" & OASISConstants.DateFormat & "}", hfFreezeDate.Value)
            End If



            If ddlAcdYear.SelectedIndex = -1 Then
                lblError.Text = "Academic Year not selected"
                ViewState("flag") = "-1"
            ElseIf ddlGrade.SelectedIndex = -1 Then
                lblError.Text = "Grade not selected"
                ViewState("flag") = "-1"
            ElseIf ddlSection.SelectedIndex = -1 Then
                lblError.Text = "Section not selected"
                ViewState("flag") = "-1"
            ElseIf ddlAttType.SelectedIndex = -1 Then
                lblError.Text = "Attendance type not selected"
                ViewState("flag") = "-1"
            ElseIf hfGrades.Value.Contains(ddlGrade.SelectedValue) And inputdt <= freezedt Then
                lblError.Text = "Attendance is closed for this period and therefore cannot be modified upto " + hfDisplay.Value
                ViewState("flag") = "-1"

            Else
                ViewState("datamode") = "edit"
                Call GETPARAM_D()
                'If ViewState("ATT_OPEN") = True Then
                If ValidateDate() = "0" Then
                    Call bindCommon_DataReader()
                    gridbind()
                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally

            If ViewState("flag") = "0" Then
                setControl()
                gvInfo.Visible = True
                ViewState("datamode") = "edit"
                btnSave.Visible = True
                btnSave2.Visible = True
                btnCancel1.Visible = True
                btnCancel2.Visible = True
                btnAdd.Visible = False
                btnEdit.Visible = False
            Else
                ViewState("datamode") = "view"
                Reset()
                gvInfo.Visible = False
                btnSave.Visible = False
                btnSave2.Visible = False
                btnCancel1.Visible = False
                btnCancel2.Visible = False
                btnAdd.Visible = True
                btnEdit.Visible = True
            End If
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ViewState("flag") = "0"

            Dim inputdt As DateTime
            If ViewState("ATT_OPEN") = True Then
                inputdt = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)

            Else
                inputdt = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
            End If


            Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim freezedt As DateTime

            If hfFreezeDate.Value = "" Then
                freezedt = String.Format("{0:" & OASISConstants.DateFormat & "}", "01/jan/1900")
            Else
                freezedt = String.Format("{0:" & OASISConstants.DateFormat & "}", hfFreezeDate.Value)
            End If
          

            If inputdt > t2 Then
                lblError.Text = "You cannot select a day greater than today!"
                txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                If Not ddlAttDate.Items.FindByText(String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)) Is Nothing Then
                    ddlAttDate.ClearSelection()
                    ddlAttDate.Items.FindByText(String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)).Selected = True

                End If

                ViewState("flag") = "-1"
            ElseIf hfGrades.Value.Contains(ddlGrade.SelectedValue) And inputdt <= freezedt Then
                lblError.Text = "Attendance is closed for this period and therefore cannot be modified upto " + hfDisplay.Value
                ViewState("flag") = "-1"
            Else
                If ddlAcdYear.SelectedIndex = -1 Then
                    lblError.Text = "Academic Year not selected"
                    ViewState("flag") = "-1"
                ElseIf ddlGrade.SelectedIndex = -1 Then
                    lblError.Text = "Grade not selected"
                    ViewState("flag") = "-1"
                ElseIf ddlSection.SelectedIndex = -1 Then
                    lblError.Text = "Section not selected"
                    ViewState("flag") = "-1"
                ElseIf ddlAttType.SelectedIndex = -1 Then
                    lblError.Text = "Attendance type not selected"
                    ViewState("flag") = "-1"
                Else
                    ViewState("datamode") = "add"
                    Call GETPARAM_D()
                    If ValidateDate() = "0" Then
                        Call bindCommon_DataReader()
                        gridbind()
                        ' Call backGround()
                    End If
                End If



            End If

        Catch ex As Exception
            lblError.Text = "You cannot select a day greater than today and must be a valid date!"
            txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            ViewState("flag") = "-1"
        Finally
            If ViewState("flag") = "0" Then
                ViewState("datamode") = "add"
                gvInfo.Visible = True
                setControl()
                btnSave.Visible = True
                btnSave2.Visible = True
                btnCancel1.Visible = True
                btnCancel2.Visible = True
                btnAdd.Visible = False
                btnEdit.Visible = False
            Else
                ViewState("datamode") = "view"
                gvInfo.Visible = False
                Reset()
                btnSave.Visible = False
                btnSave2.Visible = False
                btnCancel1.Visible = False
                btnCancel2.Visible = False
                btnAdd.Visible = True
                btnEdit.Visible = True
            End If

        End Try
    End Sub
    Sub BindAdd_Attendance_APPROVED()
        Try
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim GRD_ID As String = ddlGrade.SelectedItem.Value
            Dim SCT_ID As String = ddlSection.SelectedItem.Value
            Dim EMP_ID As String = Session("EmployeeId")
            Dim ATT_TYPE As String = ddlAttType.SelectedItem.Value
            Dim ALS_STU_ID As String = String.Empty
            Dim ALS_APD_ID As String = String.Empty
            Dim ALS_REMARKS As String = String.Empty

            Dim lastAttDate As String
            If ViewState("ATT_OPEN") = True Then
                lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    lastAttDate = ""
                Else
                    lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                End If
            End If


            If ATT_TYPE = "Session2" Then
                Dim str_sql As String = String.Empty
                Dim conn As String = ConnectionManger.GetOASISConnectionString
                str_sql = " SELECT    ALS.ALS_STU_ID , ALS.ALS_APD_ID , ALS.ALS_REMARKS FROM  ATTENDANCE_LOG_STUDENT AS ALS " & _
    " INNER JOIN  ATTENDANCE_LOG_GRADE AS ALG ON ALS.ALS_ALG_ID = ALG.ALG_ID " & _
    " where ALG.ALG_ACD_ID='" & ACD_ID & "' AND ALG.ALG_GRD_ID='" & GRD_ID & "' AND ALG.ALG_SCT_ID='" & SCT_ID & "'  AND " & _
    " ALG.ALG_ATT_TYPE='Session1'  AND ALG.ALG_ATTDT='" & lastAttDate & "'"

                Using readerGetALG_StudentID As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_sql)
                    While readerGetALG_StudentID.Read
                        ALS_STU_ID = Convert.ToString(readerGetALG_StudentID("ALS_STU_ID"))
                        ALS_APD_ID = Convert.ToString(readerGetALG_StudentID("ALS_APD_ID"))
                        ALS_REMARKS = Convert.ToString(readerGetALG_StudentID("ALS_REMARKS"))


                        For j As Integer = 0 To gvInfo.Rows.Count - 1
                            Dim row As GridViewRow = gvInfo.Rows(j)

                            Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                            Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)

                            If Trim(ALS_STU_ID) = Trim(Temp_stud_ID) Then
                                ddlStatus.ClearSelection()
                                DirectCast(row.FindControl("txtRemarks"), TextBox).Text = ALS_REMARKS
                                ddlStatus.Items.FindByValue(ALS_APD_ID).Selected = True
                                If ddlStatus.Items(1).Selected = True Then

                                    ddlStatus.BackColor = Drawing.Color.Red
                                    ddlStatus.ForeColor = Drawing.Color.White
                                Else
                                    ddlStatus.ForeColor = Drawing.Color.Black
                                    ddlStatus.BackColor = Drawing.Color.Gray


                                End If
                            End If
                        Next
                    End While
                End Using


            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetTutor()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcdYear.SelectedItem.Value)
        pParms(1) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.GET_FORM_TUTOR", pParms)
            While reader.Read
                Session("EMP_TUT_GRADE") = Convert.ToString(reader("SCT_GRD_ID"))
                Session("EMP_TUT_SECTION") = Convert.ToString(reader("SCT_ID"))
                Session("DEF_TUTOR") = Convert.ToString(reader("DEF_TUTOR"))
            End While
        End Using

    End Sub

    Sub Freeze_date_chk()

        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim str As String = String.Empty


        str = "select top 1 replace(convert( varchar,fad_dt,106),' ','/') as fad_dt,replace(convert( varchar,fad_dt,106),' ','/') as dt,fad_grd_ids from [ATT].[FREEZE_ATT_DATE]  where fad_acd_id='" & ddlAcdYear.SelectedValue & "'  order by fad_dt desc"

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str)
            If datareader.HasRows Then
                While datareader.Read
                    hfGrades.Value = Convert.ToString(datareader("fad_grd_ids"))
                    hfFreezeDate.Value = Convert.ToString(datareader("fad_dt"))
                    hfDisplay.Value = Convert.ToString(datareader("dt"))
                    hfFlag.Value = "1"
                End While
            Else
                hfGrades.Value = "--"
                hfFreezeDate.Value = ""
                hfDisplay.Value = ""
                hfFlag.Value = "0"
            End If

        End Using

    End Sub
    Sub gridbind()
        Try
            Dim AttDate As String
            If ViewState("ATT_OPEN") = True Then
                AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    AttDate = ""
                Else
                    AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                End If
            End If
            If ViewState("datamode") = "add" Then
                gvInfo.DataSource = GetAtt_Datatable(AttDate, False)
                gvInfo.DataBind()
                Call SetSTYPE()
                Call BindAdd_Attendance_APPROVED()
            ElseIf ViewState("datamode") = "edit" Then
                gvInfo.DataSource = GetAtt_Datatable(AttDate, True)
                gvInfo.DataBind()
                Call SetSTYPE()
                Call BindEdit_AttendanceAPPROVED()
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        Finally

        End Try
    End Sub
    Sub SetSTYPE()
        If ViewState("flag") = "0" Then

            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim GRD_ID As String = ddlGrade.SelectedItem.Value
            Dim SCT_ID As String = ddlSection.SelectedItem.Value
            Dim lastAttDate As String
            If ViewState("ATT_OPEN") = True Then
                lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    lastAttDate = ""
                Else
                    lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                End If
            End If
            Using readerSTUD_GRADE_SECTION_HOLIDAY As SqlDataReader = AccessStudentClass.GetSTUD_GRADE_SECTION_HOLIDAY(ACD_ID, GRD_ID, SCT_ID, lastAttDate)
                If readerSTUD_GRADE_SECTION_HOLIDAY.HasRows Then
                    While readerSTUD_GRADE_SECTION_HOLIDAY.Read
                        ViewState("STYPE") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("STYPE"))
                        ViewState("WEEKEND1") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND1"))
                        ViewState("bWEEKEND1") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND1"))
                        ViewState("WEEKEND2") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND2"))
                        ViewState("bWEEKEND2") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND2"))
                    End While
                Else
                    ViewState("STYPE") = "Empty"
                    ViewState("WEEKEND1") = ""
                    ViewState("bWEEKEND1") = False
                    ViewState("WEEKEND2") = ""
                    ViewState("bWEEKEND2") = False
                End If
            End Using

            If StrConv(ViewState("STYPE").Trim, VbStrConv.ProperCase) = "None" Then

                lblError.Text = "Date entered is not a working day"
                ViewState("flag") = "-1"
            ElseIf StrConv(ViewState("STYPE").Trim, VbStrConv.ProperCase) = "All" Then

                ViewState("flag") = "0"
            ElseIf StrConv(ViewState("STYPE").ToString.Trim, VbStrConv.ProperCase) = "Female" Then
                For j As Integer = 0 To gvInfo.Rows.Count - 1
                    Dim row As GridViewRow = gvInfo.Rows(j)
                    Dim SGENDER As String = DirectCast(row.FindControl("lblSGENDER"), Label).Text
                    Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                    If (StrConv(SGENDER.Trim, VbStrConv.ProperCase) = "Male" Or StrConv(SGENDER.Trim, VbStrConv.Uppercase) = "M") Then
                        'All male will be checked by default

                        ddlStatus.ClearSelection()
                        ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                        ddlStatus.Enabled = False
                        If ddlStatus.Items(1).Selected = True Then

                            ddlStatus.BackColor = Drawing.Color.Red
                            ddlStatus.ForeColor = Drawing.Color.White
                        Else
                            ddlStatus.ForeColor = Drawing.Color.Black
                            ddlStatus.BackColor = Drawing.Color.Gray


                        End If
                        DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
                    End If
                Next

                ViewState("flag") = "0"
            ElseIf StrConv(ViewState("STYPE").Trim, VbStrConv.ProperCase) = "Male" Then
                For j As Integer = 0 To gvInfo.Rows.Count - 1
                    Dim row As GridViewRow = gvInfo.Rows(j)
                    Dim SGENDER As String = DirectCast(row.FindControl("lblSGENDER"), Label).Text
                    Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)


                    If (StrConv(SGENDER.Trim, VbStrConv.ProperCase) = "Female" Or StrConv(SGENDER.Trim, VbStrConv.Uppercase) = "F") Then


                        ddlStatus.ClearSelection()
                        ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                        ddlStatus.Enabled = False
                        If ddlStatus.Items(1).Selected = True Then

                            ddlStatus.BackColor = Drawing.Color.Red
                            ddlStatus.ForeColor = Drawing.Color.White
                        Else
                            ddlStatus.ForeColor = Drawing.Color.Black
                            ddlStatus.BackColor = Drawing.Color.Gray


                        End If
                        DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
                    End If
                Next

                ViewState("flag") = "0"
            Else
                ViewState("flag") = "0"
            End If
        Else
            ViewState("flag") = "-1"
        End If
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim ALG_ID As New DataColumn("ALG_ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STUDNAME As New DataColumn("STUDNAME", System.Type.GetType("System.String"))
            'Dim PRESENT As New DataColumn("PRESENT", System.Type.GetType("System.Boolean"))
            'Dim LATE As New DataColumn("LATE", System.Type.GetType("System.Boolean"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim SGENDER As New DataColumn("SGENDER", System.Type.GetType("System.String"))
            Dim APPLEAVE As New DataColumn("APPLEAVE", System.Type.GetType("System.String"))
            Dim MinList As New DataColumn("MinList", System.Type.GetType("System.String"))
            Dim SLA_APD_ID As New DataColumn("SLA_APD_ID", System.Type.GetType("System.String"))

            Dim DAY1 As New DataColumn("DAY1", System.Type.GetType("System.String"))
            Dim DAY2 As New DataColumn("DAY2", System.Type.GetType("System.String"))
            Dim DAY3 As New DataColumn("DAY3", System.Type.GetType("System.String"))
            Dim TRANSSTATUS As New DataColumn("TRANSSTATUS", System.Type.GetType("System.String"))
            Dim TRANSSTATUS_CSS As New DataColumn("TRANSSTATUS_CSS", System.Type.GetType("System.String"))
            Dim TRANSSTATUS_CODE As New DataColumn("TRANSSTATUS_CODE", System.Type.GetType("System.String"))
            'Dim DAY5 As New DataColumn("DAY5", System.Type.GetType("System.String"))


            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(ALG_ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NO)
            dtDt.Columns.Add(STUDNAME)
            'dtDt.Columns.Add(PRESENT)
            'dtDt.Columns.Add(LATE)
            dtDt.Columns.Add(STATUS)
            dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(SGENDER)
            dtDt.Columns.Add(APPLEAVE)
            dtDt.Columns.Add(MinList)
            dtDt.Columns.Add(SLA_APD_ID)
            dtDt.Columns.Add(DAY1)
            dtDt.Columns.Add(DAY2)
            dtDt.Columns.Add(DAY3)
            dtDt.Columns.Add(TRANSSTATUS)
            dtDt.Columns.Add(TRANSSTATUS_CSS)
            dtDt.Columns.Add(TRANSSTATUS_CODE)
            'dtDt.Columns.Add(DAY4)
            'dtDt.Columns.Add(DAY5)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function
    Private Function GetAtt_Datatable(ByVal AttDate As String, ByVal b_EDIT As Boolean) As DataTable

        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim GRD_ID As String = ddlGrade.SelectedItem.Value
        Dim SCT_ID As String = ddlSection.SelectedItem.Value
        Dim EMP_ID As String = Session("EmployeeId")
        Dim ATT_TYPE As String = ddlAttType.SelectedItem.Value
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim para(15) As SqlClient.SqlParameter
        para(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
        para(1) = New SqlClient.SqlParameter("@GRD_ID", GRD_ID)
        para(2) = New SqlClient.SqlParameter("@SCT_ID", SCT_ID)
        para(3) = New SqlClient.SqlParameter("@ASONDATE", AttDate)
        para(4) = New SqlClient.SqlParameter("@SESSION", ATT_TYPE)
        para(5) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_id"))
        para(6) = New SqlClient.SqlParameter("@b_EDIT", b_EDIT)
        para(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        para(7).Direction = ParameterDirection.ReturnValue
        Dim AttReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.STUDATT_REG", para)
        Dim ReturnFlag As Integer = para(7).Value
        Dim dt As DataTable = CreateDataTable()
        Dim dr As DataRow
        If ReturnFlag = 0 Then
            If AttReader.HasRows = True Then
                ViewState("idTr") = 1
                While AttReader.Read
                    dr = dt.NewRow
                    ViewState("ALG_ID") = Convert.ToString(AttReader("ALG_ID"))
                    dr("SRNO") = ViewState("idTr")
                    dr("ALG_ID") = Convert.ToString(AttReader("ALG_ID"))
                    dr("STU_ID") = Convert.ToString(AttReader("STU_ID"))
                    dr("STU_NO") = Convert.ToString(AttReader("STU_NO"))
                    dr("STUDNAME") = Convert.ToString(AttReader("STUDNAME"))
                    dr("STATUS") = ""
                    dr("REMARKS") = Convert.ToString(AttReader("REMARKS"))
                    dr("SGENDER") = Convert.ToString(AttReader("SGENDER"))
                    dr("APPLEAVE") = Convert.ToString(AttReader("APPLEAVE"))
                    dr("minList") = Convert.ToString(AttReader("MINLIST"))
                    dr("SLA_APD_ID") = Convert.ToString(AttReader("APD_ID"))
                    dr("DAY1") = Convert.ToString(AttReader(15))
                    dr("DAY2") = Convert.ToString(AttReader(16))
                    dr("DAY3") = Convert.ToString(AttReader(17))
                    dr("TRANSSTATUS_CSS") = Convert.ToString(AttReader("cssclass"))
                    dr("TRANSSTATUS") = Convert.ToString(AttReader("scan_stat"))
                    dr("TRANSSTATUS_CODE") = Convert.ToString(AttReader("SCAN_STAT_CODE"))
                    hfDay1.Value = Convert.ToString(AttReader.GetName(15))
                    hfDay2.Value = Convert.ToString(AttReader.GetName(16))
                    hfDay3.Value = Convert.ToString(AttReader.GetName(17))
                    dt.Rows.Add(dr)
                    ViewState("idTr") = ViewState("idTr") + 1
                End While
            End If
            ViewState("flag") = "0"
        ElseIf ReturnFlag = -20 Then
            ViewState("flag") = "-1"
            lblError.Text = "Please verify student date of join or staff authorized attendance date "
        ElseIf ReturnFlag = -21 Then
            ViewState("flag") = "-1"
            lblError.Text = "Record currently not updated. Please Contact System Admin !!!"
        ElseIf ReturnFlag = -22 Then
            ViewState("flag") = "-1"
            lblError.Text = "Date entered is not a working day !!!"
        ElseIf ReturnFlag = -23 Then
            ViewState("flag") = "-1"
            lblError.Text = "Attendances already marked for the given date & type !!!"
        ElseIf ReturnFlag = -24 Then
            ViewState("flag") = "-1"
            lblError.Text = "Attendance not marked for the given date & type !!!"
        End If
        Return dt


    End Function

    Sub BindEdit_AttendanceAPPROVED()
        Try

            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim GRD_ID As String = ddlGrade.SelectedItem.Value
            Dim SCT_ID As String = ddlSection.SelectedItem.Value
            Dim EMP_ID As String = Session("EmployeeId")
            Dim ATT_TYPE As String = ddlAttType.SelectedItem.Value
            Dim ALS_STU_ID As String = String.Empty
            Dim ALS_APD_ID As String = String.Empty
            Dim ALS_REMARKS As String = String.Empty


            Dim lastAttDate As String
            If ViewState("ATT_OPEN") = True Then
                lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    lastAttDate = ""
                Else
                    lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)

                End If
            End If
            Using readerGetALG_StudentID As SqlDataReader = AccessStudentClass.GetALG_StudentID(ViewState("ALG_ID"))
                While readerGetALG_StudentID.Read
                    ALS_STU_ID = Convert.ToString(readerGetALG_StudentID("ALS_STU_ID"))
                    ALS_APD_ID = Convert.ToString(readerGetALG_StudentID("ALS_APD_ID"))
                    ALS_REMARKS = Convert.ToString(readerGetALG_StudentID("ALS_REMARKS"))


                    For j As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(j)

                        Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)

                        If Trim(ALS_STU_ID) = Trim(Temp_stud_ID) Then
                            ddlStatus.ClearSelection()
                            DirectCast(row.FindControl("txtRemarks"), TextBox).Text = ALS_REMARKS
                            ddlStatus.Items.FindByValue(ALS_APD_ID).Selected = True
                            If ddlStatus.Items(1).Selected = True Then

                                ddlStatus.BackColor = Drawing.Color.LightGray
                                ddlStatus.ForeColor = Drawing.Color.White
                            Else
                                ddlStatus.ForeColor = Drawing.Color.Black
                                ddlStatus.BackColor = Drawing.Color.Gray


                            End If
                        End If
                    Next
                End While
            End Using

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSave2.Click
        System.Threading.Thread.Sleep(20)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"
            btnSave2.Visible = False
        Else
            lblError.Text = errorMessage
            ' Call backGround()
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim ALG_ID As String
        Dim ALG_CREATEDDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        Dim ALG_CREATEDTM As String = String.Format("{0:T}", DateTime.Now)
        Dim ALG_MODIFIEDDT As String = String.Empty
        Dim ALG_MODIFIEDTM As String = String.Empty
        Dim XML_DATA As New StringBuilder
        Dim XML_STRING As String = String.Empty
        Dim XML_TRANS_DATA As New StringBuilder
        Dim XML_TRANS_STRING As String = String.Empty

        Dim ALG_ATTDT As String
        If ViewState("ATT_OPEN") = True Then
            ALG_ATTDT = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
        Else
            If ddlAttDate.SelectedIndex = -1 Then
                ALG_ATTDT = ""
            Else
                ALG_ATTDT = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
            End If
        End If
        Dim ALG_EMP_ID As String = Session("EmployeeId")
        Dim ALG_BSU_ID As String = Session("sBsuid")
        Dim ALG_ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim ALG_GRD_ID As String = ddlGrade.SelectedItem.Value
        Dim ALG_SCT_ID As String = ddlSection.SelectedItem.Value
        Dim ALG_SHF_ID As String = "" 'ddlShift.SelectedItem.Value
        Dim ALG_STM_ID As String = "" 'ddlStream.SelectedItem.Value
        Dim ALG_ATT_TYPE As String = ddlAttType.Text
        Dim ALS_STU_ID As String = String.Empty
        'Dim ALS_STATUS As String = String.Empty
        Dim ALS_APD_ID As String = String.Empty
        Dim ALS_REMARKS As String = String.Empty
        Dim New_ALG_ID As String = String.Empty
        Dim TRAN_STATUS As String = String.Empty
        Dim AppLeave As String = String.Empty
        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            If (checkduplicate(ALG_ACD_ID, ALG_GRD_ID, ALG_SCT_ID, ALG_ATT_TYPE, ALG_ATTDT) = True) Then
                lblError.Text = "Record already saved successfully."

                Exit Function
            End If



            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Dim status As Integer

                    bEdit = False

                    status = AccessStudentClass.SaveAttendance_Log_Grade(ALG_ID, ALG_CREATEDDT, ALG_CREATEDTM, ALG_MODIFIEDDT, ALG_MODIFIEDTM, ALG_ATTDT, _
                                     ALG_EMP_ID, ALG_BSU_ID, ALG_ACD_ID, ALG_GRD_ID, ALG_SCT_ID, ALG_SHF_ID, ALG_STM_ID, ALG_ATT_TYPE, New_ALG_ID, bEdit, transaction)
                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    Else
                        'the returned value
                        ALG_ID = New_ALG_ID
                        Dim sqlstring As String
                        sqlstring = "DELETE ATTENDANCE_LOG_STUDENT WHERE ALS_ALG_ID='" & ALG_ID & "'"
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sqlstring)

                        For i As Integer = 0 To gvInfo.Rows.Count - 1
                            Dim row As GridViewRow = gvInfo.Rows(i)


                            'Dim chkPresent As Boolean = DirectCast(row.FindControl("chkPresent"), CheckBox).Checked
                            'Dim chkPresent_Enable As Boolean = DirectCast(row.FindControl("chkPresent"), CheckBox).Enabled
                            'Dim chkLate As Boolean = DirectCast(row.FindControl("chkLate"), CheckBox).Checked
                            'Dim chkLate_Enable As Boolean = DirectCast(row.FindControl("chkLate"), CheckBox).Enabled

                            Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                            ALS_STU_ID = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                            ALS_REMARKS = DirectCast(row.FindControl("txtRemarks"), TextBox).Text
                            AppLeave = DirectCast(row.FindControl("lblAppLeave"), Label).Text
                            TRAN_STATUS = DirectCast(row.FindControl("lblTranStatus"), Label).Text
                            'Check for present check box is unchecked and also see if it is approved leave or not 
                            'Dim STR1 As String = UCase(Trim(ddlStatus.SelectedItem.Text))
                            'NEW AUDIT CODE ADDED FOR ATTENDANCE
                            If TRAN_STATUS <> "" Then
                                XML_TRANS_DATA.Append(String.Format("<STU_ID='{0}' TR_STATUS='{1}' />", ALS_STU_ID, TRAN_STATUS))
                            End If

                            If (ddlStatus.SelectedIndex <> 0) Then
                                If (AppLeave <> "APPROVED") Then
                                    ALS_APD_ID = ddlStatus.SelectedItem.Value

                                    XML_DATA.Append(String.Format("<STU_ID='{0}' ATT_PARAM='{1}' APP_LEAVE ='{2}'" & _
                                                        " ATT_REMARKS='{3}' />", ALS_STU_ID, _
ddlStatus.SelectedItem.Value, AppLeave, ALS_REMARKS.ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")))



                                    status = AccessStudentClass.SaveAttendance_Log_Student(ALG_ID, ALS_STU_ID, ALS_APD_ID, ALS_REMARKS, transaction)
                                    If status <> 0 Then
                                        calltransaction = "1"
                                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                                        Return "1"
                                    End If

                                End If
                            End If
                        Next


                    End If
                    'NEW AUDIT CODE ADDED FOR ATTENDANCE
                    If XML_DATA.ToString <> "" Then
                        XML_STRING = "<ATT>" & XML_DATA.ToString & "</ATT>"
                    End If

                    If XML_TRANS_DATA.ToString <> "" Then
                        XML_TRANS_STRING = "<ATT>" & XML_TRANS_DATA.ToString & "</ATT>"
                    End If

                    ViewState("datamode") = "view"
                    Dim dt As DataTable = CreateDataTable()
                    Dim dr As DataRow
                    dr = dt.NewRow
                    dt.Rows.Add(dr)
                    gvInfo.DataSource = dt
                    gvInfo.DataBind()
                    gvInfo.Rows(0).Cells.Clear()

                    'NEW AUDIT CODE ADDED FOR ATTENDANCE
                    AUDIT_ATTENDANCE(Session("sUsr_id"), ALG_ID, "ADD", XML_STRING)
                    AUDIT_TRANS_ATTENDANCE(ALG_BSU_ID, ALG_ACD_ID, txtDate.Text, Session("sUsr_id"), ALG_ID, XML_TRANS_STRING)

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gvInfo.Visible = False
                    btnSave.Visible = False
                    btnSave2.Visible = False
                    btnCancel1.Visible = False
                    btnCancel2.Visible = False
                    btnAdd.Visible = True
                    btnEdit.Visible = True
                    calltransaction = "0"
                    ResetControl()


                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim status As Integer

                    bEdit = True

                    ALG_CREATEDDT = String.Empty
                    ALG_CREATEDTM = String.Empty
                    ALG_MODIFIEDDT = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    ALG_MODIFIEDTM = String.Format("{0:T}", DateTime.Now)

                    ALG_ID = ViewState("ALG_ID")

                    status = AccessStudentClass.SaveAttendance_Log_Grade(ALG_ID, ALG_CREATEDDT, ALG_CREATEDTM, ALG_MODIFIEDDT, ALG_MODIFIEDTM, ALG_ATTDT, _
                                     ALG_EMP_ID, ALG_BSU_ID, ALG_ACD_ID, ALG_GRD_ID, ALG_SCT_ID, ALG_SHF_ID, ALG_STM_ID, ALG_ATT_TYPE, New_ALG_ID, bEdit, transaction)
                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    Else
                        'the returned value
                        ALG_ID = ViewState("ALG_ID")
                        Dim sqlstring As String
                        sqlstring = "DELETE ATTENDANCE_LOG_STUDENT WHERE ALS_ALG_ID='" & ALG_ID & "'"
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sqlstring)

                        For i As Integer = 0 To gvInfo.Rows.Count - 1
                            Dim row As GridViewRow = gvInfo.Rows(i)

                            ''Check for present check box is unchecked and also see if it is approved leave or not 

                            Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                            ALS_STU_ID = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                            ALS_REMARKS = DirectCast(row.FindControl("txtRemarks"), TextBox).Text
                            AppLeave = DirectCast(row.FindControl("lblAppLeave"), Label).Text
                            'NEW AUDIT CODE ADDED FOR ATTENDANCE
                            TRAN_STATUS = DirectCast(row.FindControl("lblTranStatus"), Label).Text
                            If TRAN_STATUS <> "" Then
                                XML_TRANS_DATA.Append(String.Format("<TRANS STU_ID='{0}' TR_STATUS='{1}' />", ALS_STU_ID, TRAN_STATUS))
                            End If

                            If (ddlStatus.SelectedIndex <> 0) Then
                                If (AppLeave <> "APPROVED") Then
                                    ALS_APD_ID = ddlStatus.SelectedItem.Value

                                    XML_DATA.Append(String.Format("<STU_ID='{0}' ATT_PARAM='{1}' APP_LEAVE ='{2}'" & _
                                                      " ATT_REMARKS='{3}' />", ALS_STU_ID, _
ddlStatus.SelectedItem.Value, AppLeave, ALS_REMARKS.ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")))


                                    status = AccessStudentClass.SaveAttendance_Log_Student(ALG_ID, ALS_STU_ID, ALS_APD_ID, ALS_REMARKS, transaction)
                                    If status <> 0 Then
                                        calltransaction = "1"
                                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                                        Return "1"
                                    End If

                                End If
                            End If
                        Next

                    End If
                    Dim dt As DataTable = CreateDataTable()
                    Dim dr As DataRow
                    dr = dt.NewRow
                    dt.Rows.Add(dr)
                    gvInfo.DataSource = dt
                    gvInfo.DataBind()
                    gvInfo.Rows(0).Cells.Clear()
                    'NEW AUDIT CODE ADDED FOR ATTENDANCE
                    If XML_DATA.ToString <> "" Then
                        XML_STRING = "<ATT>" & XML_DATA.ToString & "</ATT>"
                    End If
                    If XML_TRANS_DATA.ToString <> "" Then
                        XML_TRANS_STRING = "<ATT>" & XML_TRANS_DATA.ToString & "</ATT>"
                    End If
                    AUDIT_ATTENDANCE(Session("sUsr_id"), ALG_ID, "EDIT", XML_STRING)
                    AUDIT_TRANS_ATTENDANCE(ALG_BSU_ID, ALG_ACD_ID, txtDate.Text, Session("sUsr_id"), ALG_ID, XML_TRANS_STRING)

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    gvInfo.Visible = False
                    btnSave.Visible = False
                    btnSave2.Visible = False
                    btnCancel1.Visible = False
                    btnCancel2.Visible = False
                    btnAdd.Visible = True
                    btnEdit.Visible = True
                    ResetControl()

                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        End If
    End Function

    Private Function checkduplicate(ByVal ALG_ACD_ID As String, ByVal ALG_GRD_ID As String, ByVal ALG_SCT_ID As String, _
 ByVal ALG_ATT_TYPE As String, ByVal ALG_ATTDT As String) As Boolean
        Dim status As Boolean = False

        Dim conn As String = ConnectionManger.GetOASISConnectionString

        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@ALG_ACD_ID", ALG_ACD_ID)
        param(1) = New SqlParameter("@ALG_GRD_ID", ALG_GRD_ID)
        param(2) = New SqlParameter("@ALG_SCT_ID", ALG_SCT_ID)
        param(3) = New SqlParameter("@ALG_ATT_TYPE", ALG_ATT_TYPE)
        param(4) = New SqlParameter("@ALG_ATTDT", ALG_ATTDT)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ATT.CHECKDUPLICATE_ATT", param)
            If datareader.HasRows = True Then
                While datareader.Read
                    status = Convert.ToBoolean(datareader("ISEXITS"))
                End While

            End If
        End Using
        checkduplicate = status

    End Function



    Private Sub AUDIT_ATTENDANCE(ByVal USR_ID As String, ByVal ALG_ID As String, _
ByVal ACTION As String, ByVal XML_DATA As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ClientBrs As String = String.Empty
            Dim ClientIP As String = HttpContext.Current.Request.UserHostAddress()
            With HttpContext.Current.Request.Browser
                ClientBrs &= "Browser Capabilities" & vbCrLf
                ClientBrs &= "Type = " & .Type & vbCrLf
                ClientBrs &= "Name = " & .Browser & vbCrLf
                ClientBrs &= "Version = " & .Version & vbCrLf
                ClientBrs &= "Major Version = " & .MajorVersion & vbCrLf
                ClientBrs &= "Minor Version = " & .MinorVersion & vbCrLf
                ClientBrs &= "Platform = " & .Platform & vbCrLf
                ClientBrs &= "Is Beta = " & .Beta & vbCrLf
                ClientBrs &= "Is Crawler = " & .Crawler & vbCrLf
                ClientBrs &= "Is AOL = " & .AOL & vbCrLf
                ClientBrs &= "Is Win16 = " & .Win16 & vbCrLf
                ClientBrs &= "Is Win32 = " & .Win32 & vbCrLf
                ClientBrs &= "Supports Frames = " & .Frames & vbCrLf
                ClientBrs &= "Supports Tables = " & .Tables & vbCrLf
                ClientBrs &= "Supports Cookies = " & .Cookies & vbCrLf
                ClientBrs &= "Supports VBScript = " & .VBScript & vbCrLf
                ClientBrs &= "Supports JavaScript = " & _
                    .EcmaScriptVersion.ToString() & vbCrLf
                ClientBrs &= "Supports Java Applets = " & .JavaApplets & vbCrLf
                ClientBrs &= "Supports ActiveX Controls = " & .ActiveXControls & _
                    vbCrLf
                ClientBrs &= "Supports JavaScript Version = " & _
                .JScriptVersion.ToString & vbCrLf
            End With
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SAA_USR_ID", USR_ID)
            pParms(1) = New SqlClient.SqlParameter("@SAA_ALG_ID", ALG_ID)
            pParms(2) = New SqlClient.SqlParameter("@SAA_ACTION", ACTION)
            pParms(3) = New SqlClient.SqlParameter("@SAA_XML_DATA", XML_DATA)
            pParms(4) = New SqlClient.SqlParameter("@SAA_BROWSER_INFO", ClientBrs)
            pParms(5) = New SqlClient.SqlParameter("@SAA_IP_ADDRESS", ClientIP)


            SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "[ATT].[SAVESTUDENT_ATT_AUDIT]", pParms)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "AUDIT_ATTENDANCE")
        End Try
    End Sub
    Private Sub AUDIT_TRANS_ATTENDANCE(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal DTE As String, ByVal USR_ID As String, ByVal ALG_ID As String, ByVal XML_DATA As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@bsu_id", BSU_ID)
            pParms(1) = New SqlClient.SqlParameter("@tr_date", DTE)
            pParms(2) = New SqlClient.SqlParameter("@TAA_USR_ID", USR_ID)
            pParms(3) = New SqlClient.SqlParameter("@TAA_ALG_ID", ALG_ID)
            pParms(4) = New SqlClient.SqlParameter("@TAA_XML_DATA", XML_DATA)
            pParms(5) = New SqlClient.SqlParameter("@TAA_ACD_ID", ACD_ID)

            SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "[ATT].[SAVETRANSPORT_ATT_AUDIT]", pParms)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "AUDIT_transATTENDANCE")
        End Try
    End Sub
    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtDate.Text <> "" Then
                Dim strfDate As String = txtDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"

            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
            Return "-1"
        End Try

    End Function
    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel1.Click, btnCancel2.Click
        Try

            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "view"
                Dim dt As DataTable = CreateDataTable()
                Dim dr As DataRow
                dr = dt.NewRow
                dt.Rows.Add(dr)

                gvInfo.DataSource = dt
                gvInfo.DataBind()
                gvInfo.Rows(0).Cells.Clear()
                gvInfo.Visible = False
                btnSave.Visible = False
                btnSave2.Visible = False
                btnCancel1.Visible = False
                btnCancel2.Visible = False
                btnAdd.Visible = True
                btnEdit.Visible = True

                ResetControl()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub setControl()
        ddlAcdYear.Enabled = False
        ddlGrade.Enabled = False
        ddlSection.Enabled = False
        ddlAttType.Enabled = False
        If ViewState("ATT_OPEN") = True Then
            txtDate.Enabled = False
            imgCalendar.Enabled = False
            rfvDate.Enabled = False
        Else
            ddlAttDate.Enabled = False
        End If


    End Sub
    Sub ResetControl()
        ddlAcdYear.Enabled = True
        ddlGrade.Enabled = True
        ddlSection.Enabled = True
        If ViewState("ATT_OPEN") = True Then
            txtDate.Enabled = True
            imgCalendar.Enabled = True
            rfvDate.Enabled = True
        Else
            ddlAttDate.Enabled = True
        End If

        ddlAttType.Enabled = True
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
    End Sub
    Sub GETPARAM_D()
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim BSU_ID As String = Session("sBsuid")
            Dim SESS_TYPE As String = ddlAttType.SelectedItem.Text
            Dim ds1 As New DataSet

            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@SESS_TYPE", SESS_TYPE)

            Session("Att_param") = Nothing

            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GETATT_PARAM_GRIDBIND", pParms)
            Session("Att_param") = ds1
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub DropDownList_OnSelectedIndexChanged(sender As Object, e As EventArgs)
        Dim row As GridViewRow = DirectCast(DirectCast(sender, System.Web.UI.WebControls.DropDownList).NamingContainer, GridViewRow)
        ' For i As Integer = 0 To row.Cells.Count - 1
        Dim ddl As System.Web.UI.WebControls.DropDownList = DirectCast(row.FindControl("ddlStatus"), System.Web.UI.WebControls.DropDownList)

        If ddl.Items(1).Selected = True Then

            ddl.BackColor = Drawing.Color.LightGray 'red
            ddl.ForeColor = Drawing.Color.White 'White
        Else
            ddl.ForeColor = Drawing.Color.Black
            ddl.BackColor = Drawing.Color.Gray

        End If


        ' Next
    End Sub
    Protected Sub gvInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvInfo.RowDataBound
        Try
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim BSU_ID As String = Session("sBsuid")
            Dim SESS_TYPE As String = ddlAttType.SelectedItem.Text

            Dim DS As DataSet '= AccessStudentClass.GetATTENDANCE_PARAM_D(ACD_ID, BSU_ID, SESS_TYPE)
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ddlSTATUS As DropDownList = DirectCast(e.Row.FindControl("ddlStatus"), DropDownList)
                Dim lblMinList As Label = DirectCast(e.Row.FindControl("lblMinList"), Label)
                Dim lblAppLeave As Label = DirectCast(e.Row.FindControl("lblAppLeave"), Label)
                Dim lblSLA_APD_ID As Label = DirectCast(e.Row.FindControl("lblSLA_APD_ID"), Label)

                If lblMinList.Text.ToUpper <> "REGULAR" Then
                    e.Row.BackColor = Drawing.Color.FromArgb(251, 204, 119)
                End If
                If Not Session("Att_param") Is Nothing Then
                    ddlSTATUS.Items.Clear()
                    DS = Session("Att_param")
                    If DS.Tables(0).Rows.Count > 0 Then
                        ddlSTATUS.DataSource = DS.Tables(0)
                        ddlSTATUS.DataTextField = "APD_PARAM_DESCR"
                        ddlSTATUS.DataValueField = "APD_ID"
                        ddlSTATUS.DataBind()

                        ddlSTATUS.Items(0).Attributes.Add("style", "background-color:#fdf4a8") 'GREEN
                        ddlSTATUS.Items(1).Attributes.Add("style", "background-color:#feb4a8") 'PINK



                    End If



                    If lblAppLeave.Text = "APPROVED" Then
                        ddlSTATUS.ClearSelection()

                        DirectCast(e.Row.FindControl("ddlStatus"), DropDownList).Enabled = False
                        DirectCast(e.Row.FindControl("txtRemarks"), TextBox).Enabled = False

                        If ViewState("bAPP_LEAVE_ABS") = "True" Then
                            If lblSLA_APD_ID.Text <> "0" Then
                                If Not ddlSTATUS.Items.FindByValue(lblSLA_APD_ID.Text) Is Nothing Then
                                    ddlSTATUS.Items.FindByValue(lblSLA_APD_ID.Text).Selected = True
                                Else
                                    ddlSTATUS.Items.FindByText(ddlSTATUS.Items(1).Text).Selected = True
                                End If
                            Else
                                ddlSTATUS.Items.FindByText(ddlSTATUS.Items(1).Text).Selected = True
                            End If
                        Else
                            If lblSLA_APD_ID.Text <> "0" Then
                                If Not ddlSTATUS.Items.FindByValue(lblSLA_APD_ID.Text) Is Nothing Then
                                    ddlSTATUS.Items.FindByValue(lblSLA_APD_ID.Text).Selected = True
                                Else
                                    ddlSTATUS.Items.FindByText(ddlSTATUS.Items(0).Text).Selected = True
                                End If
                            Else
                                ddlSTATUS.Items.FindByText(ddlSTATUS.Items(0).Text).Selected = True
                            End If
                        End If
                        'ElseIf AppLeave = "NOT APPROVED" Then
                        '    ddlSTATUS.Items.FindByText(ddlSTATUS.Items(1).Text).Selected = True
                    End If
                    If ddlSTATUS.Items(1).Selected = True Then
                        ' If ddlSTATUS.SelectedItem.Text = "ABSENT(O)" Then
                        '  If ddlSTATUS.Items.FindByText(ddlSTATUS.Items(1).Text).Selected = True Then


                        ddlSTATUS.BackColor = Drawing.Color.Red
                        ddlSTATUS.ForeColor = Drawing.Color.White
                    Else
                        ddlSTATUS.ForeColor = Drawing.Color.Black
                        ddlSTATUS.BackColor = Drawing.Color.Gray


                    End If

                End If
            ElseIf e.Row.RowType = DataControlRowType.Header Then
                'Dim HeaderGrid As GridView = DirectCast(sender, GridView)
                'Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                'Dim HeaderCell As New TableCell()
                'HeaderCell.Text = ""
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)
                'HeaderCell = New TableCell()
                'HeaderCell.ForeColor = Drawing.Color.Black
                'HeaderCell.Height = 20
                'HeaderCell.Text = "Last Five Day's Attendance History"
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)


                'gvInfo.Controls(0).Controls.AddAt(0, HeaderGridRow)

                e.Row.Cells(12).Text = hfDay1.Value
                e.Row.Cells(13).Text = hfDay2.Value
                e.Row.Cells(14).Text = hfDay3.Value
                'e.Row.Cells(15).Text = hfDay4.Value
                'e.Row.Cells(16).Text = hfDay5.Value
            End If

        Catch ex As Exception

        End Try


    End Sub
    Sub backGround()
        For i As Integer = 0 To gvInfo.Rows.Count - 1
            Dim row As GridViewRow = gvInfo.Rows(i)
            Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
            ddlStatus.Items(0).Attributes.Add("style", "background-color:#fdf4a8") 'GREEN
            ddlStatus.Items(1).Attributes.Add("style", "background-color:#feb4a8") 'PINK
        Next

    End Sub
    Protected Sub gvInfo_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then

            Dim HeaderGrid As GridView = DirectCast(sender, GridView)
            Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderCell As New TableCell()
            HeaderCell.Text = ""
            HeaderCell.ColumnSpan = 5
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
            HeaderCell.ForeColor = Drawing.Color.Black
            HeaderCell.Height = 20
            HeaderCell.Text = "Last Three Day's Attendance History"
            HeaderCell.ColumnSpan = 4
            HeaderGridRow.Cells.Add(HeaderCell)

            gvInfo.Controls(0).Controls.AddAt(0, HeaderGridRow)
        End If

    End Sub
    Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
        bindAtt_Type()
    End Sub
    Sub check_transport_status_bsu()

        Dim constring As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Using con As New SqlConnection(constring)

            Using cmd As New SqlCommand("att.get_trStatus_acess_bsu", con)

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))

                cmd.Parameters.Add("@valid", SqlDbType.Int)

                cmd.Parameters("@valid").Direction = ParameterDirection.Output

                con.Open()

                cmd.ExecuteNonQuery()

                con.Close()

                ViewState("IsvalidBsu") = cmd.Parameters("@valid").Value

            End Using

        End Using
    End Sub

End Class
