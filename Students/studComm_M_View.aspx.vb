Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studSubject_M_View
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S050015" And ViewState("MainMnu_code") <> "S050020" And ViewState("MainMnu_code") <> "S050025") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    If ViewState("MainMnu_code") = "S050015" Then
                        gvCommonEmp.Columns(0).Visible = False
                        ' gvCommonEmp.Columns(2).Visible = False
                        ' gvCommonEmp.Columns(3).Visible = False

                        ViewState("str_Sql") = "Select HideID,ID,Descr,bFlag from (select 0 as HideID,SBM_ID as id,SBM_DESCR as descr,SMB_LANG as bFlag  from SUBJECT_M)a where id<>'' "

                    ElseIf ViewState("MainMnu_code") = "S050020" Then
                        gvCommonEmp.Columns(0).Visible = False
                        ' gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False

                        ViewState("str_Sql") = "Select HideID,ID,Descr,bFlag from(SELECT HOUSE_M.HOUSE_ID as HideID,HOUSE_M.HOUSE_DESCRIPTION as ID,COLOR_M.COLOR_DESCR as Descr, HOUSE_M.HOUSE_BSU_ID as BSU_ID, 0 as bFlag " & _
                                                " FROM  HOUSE_M INNER JOIN COLOR_M ON HOUSE_M.HOUSE_COLOR = COLOR_M.COLOR_HEX)a where a.BSU_ID='" & CurBsUnit & "' and id<>'' "


                    ElseIf ViewState("MainMnu_code") = "S050025" Then
                        gvCommonEmp.Columns(0).Visible = False
                        ' gvCommonEmp.Columns(2).Visible = False
                        gvCommonEmp.Columns(3).Visible = False

                        ViewState("str_Sql") = "Select HideID,ID,Descr,bFlag from(SELECT DOCREQD_M.DOC_ID as HideID,DOCREQD_TYPE_M.DOCTYPE_DESCR as ID,DOCREQD_M.DOC_DESCR as Descr, 0 as bFlag " & _
                                                " FROM  DOCREQD_M INNER JOIN  DOCREQD_TYPE_M ON DOCREQD_M.DOC_DOCTYPE_ID = DOCREQD_TYPE_M.DOCTYPE_ID)a where  a.id<>'' "



                    End If


                    gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If
            Catch ex As Exception

                lblError.Text = "Request could not be processed "
            End Try

        End If

        set_Menu_Img()
    End Sub
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

            Dim ds As New DataSet

            Dim str_filter_code, str_filter_name, str_txtCode, str_txtName As String
            ''''''''
            str_filter_code = ""
            str_filter_name = ""
            str_txtCode = ""
            str_txtName = ""
            Dim str_Sid_search() As String
            str_Sid_search = h_selected_menu_1.Value.Split("__")
            Dim txtSearch As New TextBox

            '''''''''
            If gvCommonEmp.Rows.Count > 0 Then
                ''code
                ' Dim str_Sid_search() As String
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                txtSearch = gvCommonEmp.HeaderRow.FindControl("txtCode")
                str_txtCode = txtSearch.Text.Trim
                str_filter_code = set_search_filter("a.ID", str_Sid_search(0), str_txtCode)

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                txtSearch = gvCommonEmp.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text.Trim
                str_filter_name = set_search_filter("a.DESCR", str_Sid_search(0), str_txtName)
                ''column1
            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, ViewState("str_Sql") & str_filter_code & str_filter_name & " Order by a.ID")

            If ds.Tables(0).Rows.Count > 0 Then

                gvCommonEmp.DataSource = ds.Tables(0)
                gvCommonEmp.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ' ds.Tables(0).Rows(0)(0) = 1
                'ds.Tables(0).Rows(0)(1) = 1
                '  ds.Tables(0).Rows(0)(2) = True
                ds.Tables(0).Rows(0)(3) = True
                gvCommonEmp.DataSource = ds.Tables(0)
                Try
                    gvCommonEmp.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvCommonEmp.Rows(0).Cells.Count
                ' '' 'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvCommonEmp.Rows(0).Cells.Clear()
                gvCommonEmp.Rows(0).Cells.Add(New TableCell)
                gvCommonEmp.Rows(0).Cells(0).ColumnSpan = columnCount
                gvCommonEmp.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvCommonEmp.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If



            If ViewState("MainMnu_code") = "S050015" Then
                Dim lblHeader As New Label
                lblHeader = gvCommonEmp.HeaderRow.FindControl("lblPaidH")
                lblHeader.Text = "Language"
                ltHeader.Text = UCase("Subject Master")

            ElseIf ViewState("MainMnu_code") = "S050020" Then
                Dim lblHeader As New Label
                lblHeader = gvCommonEmp.HeaderRow.FindControl("lblDescrH")
                lblHeader.Text = "Color Code"

                'ltHeader.Text = UCase("House Master")
                ltHeader.Text = "House Master"

                Dim lblIDHeader As New Label
                lblIDHeader = gvCommonEmp.HeaderRow.FindControl("lblIDH")
                lblIDHeader.Text = "House"
            ElseIf ViewState("MainMnu_code") = "S050025" Then

                ltHeader.Text = UCase("Document Master")

                Dim lblHeader As New Label
                lblHeader = gvCommonEmp.HeaderRow.FindControl("lblDescrH")
                lblHeader.Text = "Description"
                Dim lblIDHeader As New Label
                lblIDHeader = gvCommonEmp.HeaderRow.FindControl("lblIDH")
                lblIDHeader.Text = "Doc Type"

            End If

            set_Menu_Img()
            txtSearch = gvCommonEmp.HeaderRow.FindControl("txtCode")
            txtSearch.Text = str_txtCode
            txtSearch = gvCommonEmp.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Function set_search_filter(ByVal p_field As String, ByVal p_criteria As String, ByVal p_searchtext As String) As String
        Dim str_filter As String = ""
        If p_criteria = "LI" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "NLI" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "%'"
        ElseIf p_criteria = "SW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "NSW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '" & p_searchtext & "%'"
        ElseIf p_criteria = "EW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " LIKE '%" & p_searchtext & "'"
        ElseIf p_criteria = "NEW" And p_searchtext <> "" Then
            str_filter = " AND " & p_field & " NOT LIKE '%" & p_searchtext & "'"
        End If
        Return str_filter
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonEmp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonEmp.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvCommonEmp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvCommonEmp.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub gvCommonEmp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCommonEmp.RowCommand
        If e.CommandName = "View" Then

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvCommonEmp.Rows(index), GridViewRow)
            Dim UserIDLabel As Label

            If ViewState("MainMnu_code") = "S050015" Then

                UserIDLabel = DirectCast(selectedRow.Cells(1).Controls(1), Label)
            ElseIf ViewState("MainMnu_code") = "S050020" Then
                UserIDLabel = DirectCast(selectedRow.Cells(0).Controls(1), Label)
            ElseIf ViewState("MainMnu_code") = "S050025" Then
                UserIDLabel = DirectCast(selectedRow.Cells(0).Controls(1), Label)
            End If

            Dim Eid As String = UserIDLabel.Text

            Dim url As String

            Eid = Encr_decrData.Encrypt(Eid)
            ViewState("datamode") = "view"
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studComm_M.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", ViewState("MainMnu_code"), ViewState("datamode"), Eid)
            Response.Redirect(url)

        End If
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click

        Try
            Dim url As String
            ViewState("datamode") = "add"
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

            Dim Queryusername As String = Session("sUsr_name")

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studComm_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))

            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Protected Sub btnCodeSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchpar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub gvCommonEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCommonEmp.PageIndexChanging
        gvCommonEmp.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


End Class
