<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studStrikeOffRequest_M.aspx.vb" Inherits="Students_studStrikeOffRequest_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Strike Off"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" />
                            &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="center" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>

                    <tr>
                        <td valign="top">
                            <table align="center" cellpadding="0" cellspacing="0"
                                style="width: 100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtName" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">SEN</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtSEN" runat="server" ReadOnly="True"></asp:TextBox></td>
                                </tr>

                                <tr>

                                    <td align="left" width="20%"><span class="field-label">Last Attendance Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtLast" runat="server" ReadOnly="True">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Recommended Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRec" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRecRemarks" runat="server" TextMode="MultiLine" ReadOnly="True">
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Approval Date</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtApr" runat="server" ReadOnly="True"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtAprRemarks" runat="server" TextMode="MultiLine"  ReadOnly="True">
                                        </asp:TextBox></td>
                                </tr>



                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4" valign="middle">&nbsp;Details</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span><span style="font-size: 8pt; color: #800000">*</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Document Date</span><span style="font-size: 8pt; color: #800000">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDocDate" runat="server" >
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgDocDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="revDoc" runat="server" ControlToValidate="txtDocDate"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Document  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator></td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Strike Off Reference No</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRefNo" runat="server" >
                                        </asp:TextBox></td>
                                    <td align="left"><span class="field-label">Strike Off Date</span><span style="font-size: 8pt; color: #800000">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStrike" runat="server" >
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgStrike" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="revStrike" runat="server" ControlToValidate="txtStrike"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Strike Off  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator></td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="bottom">&nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom" >
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtStrike"
                                Display="None" ErrorMessage="Please enter data in the field Stike Off Date" ValidationGroup="groupM1"
                                Width="23px"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks"
                                Display="None" ErrorMessage="Please enter data in the field remarks" ValidationGroup="groupM1"
                                Width="23px">
                            </asp:RequiredFieldValidator>&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDocDate"
                    Display="None" ErrorMessage="Please enter data in the field Document Date" ValidationGroup="groupM1"
                    Width="23px">
                </asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfSTK_ID" runat="server" />
                            <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtStrike" TargetControlID="txtStrike">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;&nbsp;
                <asp:HiddenField ID="HF_TCM_ID" runat="server"></asp:HiddenField>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgStrike" TargetControlID="txtStrike">
                            </ajaxToolkit:CalendarExtender>
                            <asp:HiddenField ID="hfMode" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgDocDate" TargetControlID="txtDocDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtDocDate" TargetControlID="txtDocDate">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;
                &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>

