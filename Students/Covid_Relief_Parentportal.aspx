﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Covid_Relief_Parentportal.aspx.vb" Inherits="Students_Covid_Relief_Parentportal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../cssfiles/sb-admin.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />

    <style>
        .personal-info h2 {
            background-color: #e5e5e5;
        }

        h2, .h2 {
            font-size: 1.5rem !important;
        }

        h3, .h3 {
            font-size: 1rem !important;
        }

        .column-padding {
            padding: 10px;
        }

        .bg-primary {
            background-color: #d2d2d2 !important;
        }

        .bg-light {
            background-color: #e5e5e5 !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- Posts Block -->
                    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <%--<header class="py-3 bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <h1 class="m-0 text-primary">COVID-19 Relief Application Form</h1>
                                    </div>
                                </div>
                            </div>
                        </header>--%>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        <!-- header end -->
                        <!-- form -->
                        <%-- <main role="main" class="relief-form mb-3">--%>
                        <!-- personal details -->
                        <section class="c mt-3">
                            <div class="container-fluid">
                                <div class="row">
                                    <!-- parent details -->
                                    <div class="col-lg-12 col-md-12 col-12 mb-3 mb-sm-0">
                                        <div class="card rounded-0 h-100">
                                            <div class="card-body p-0">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h2 class="m-0 bg-primary pl-2">Parent Details</h2>
                                                    </div>
                                                </div>
                                                <div class="column-padding pt-3 pb-3">
                                                    <div class="row mb-4">
                                                        <div class="col-sm-6 col-12">
                                                            <h3 class="m-0">
                                                                <span class="d-block font-weight-bold">Father's Name</span>
                                                                <asp:Label ID="lblFName" runat="server" class="label-value"></asp:Label>
                                                            </h3>
                                                        </div>
                                                        <div class="col-sm-6 col-12">
                                                            <h3 class="m-0">
                                                                <span class="d-block font-weight-bold">Mother's Name</span>
                                                                <asp:Label ID="lblMName" runat="server" class="label-value"></asp:Label>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-4">
                                                        <div class="col-sm-6 col-12">
                                                            <h3 class="m-0">
                                                                <span class="d-block font-weight-bold">Primary Contact Number</span>
                                                                <asp:Label ID="lblMobNo" runat="server" class="label-value"></asp:Label>
                                                            </h3>
                                                        </div>
                                                        <div class="col-sm-6 col-12">
                                                            <h3 class="m-0">
                                                                <span class="d-block font-weight-bold">Primary Email Address</span>
                                                                <asp:Label ID="lblEmail" runat="server" class="label-value"></asp:Label>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- parent details end -->
                                </div>
                                <div class="row">
                                    <!-- student details -->
                                    <div class="col-lg-12 col-md-12 col-12">
                                        <div class="card rounded-0">
                                            <div class="card-body p-0">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h2 class="m-0  bg-primary pl-2">Student Details</h2>
                                                    </div>
                                                </div>
                                                <div class="pb-0">
                                                    <div class="studentInfo">
                                                        <div class="accordion text-left" id="studentDetails">
                                                            <asp:Repeater ID="rptStudDet" runat="server">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdnStu_id" runat="server" Value='<%# Eval("STU_ID")%>' />
                                                                    <div class="card z-depth-0 m-0 border-bottom border-top-0 border-left-0 border-right-0">
                                                                        <div class="card-header pt-2 pb-2" id="student-1">
                                                                            <h5 class="mb-0">
                                                                                <button class="btn btn-link d-block w-100 text-left m-0 p-0 collapse" type="button" data-toggle="collapse" data-target="#student-info-1" aria-expanded="true" aria-controls="student-info-1">
                                                                                    <asp:Label ID="lblStuname" runat="server" Text='<%# Eval("STU_FULLNAME")%>' class="label-value"></asp:Label>
                                                                                    <i class="fas fa-angle-down float-right m-0"></i>
                                                                                </button>
                                                                            </h5>
                                                                        </div>
                                                                        <div id="student-info-1" class="collapse show border-top" aria-labelledby="student-1" data-parent="#studentDetails">
                                                                            <div class="card-body column-padding pt-2 pb-2">
                                                                                <div class="content">
                                                                                    <div class="row">
                                                                                        <div class="col-6 col-md-6 col-lg-6">
                                                                                            <h3 class="m-0 w-100 pt-2 pb-2">
                                                                                                <span class="d-block mb-1 font-weight-bold">School</span><asp:Label ID="lblSchool" runat="server" Text='<%# Eval("BSU_NAME")%>' class="label-value"></asp:Label>
                                                                                            </h3>
                                                                                        </div>
                                                                                        <div class="col-3 col-md-3 col-lg-3">
                                                                                            <h3 class="m-0 w-100 pt-2 pb-2">
                                                                                                <span class="d-block mb-1 font-weight-bold">Grade</span><asp:Label ID="lblGrade" runat="server" Text='<%# Eval("STU_GRADE")%>' class="label-value"></asp:Label>
                                                                                            </h3>
                                                                                        </div>
                                                                                        <div class="col-3 col-md-3 col-lg-3">
                                                                                            <h3 class="m-0 w-100 pt-2 pb-2">
                                                                                                <span class="d-block mb-1 font-weight-bold">Date of Joining</span><asp:Label ID="lblDOJ" runat="server" Text='<%# Eval("STU_DOJ")%>' class="label-value"></asp:Label>
                                                                                            </h3>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-6 col-md-6 col-lg-6">
                                                                                            <h3 class="m-0 w-100 pt-2 pb-2">
                                                                                                <span class="d-block mb-1 font-weight-bold">Fees dues until Mar 20</span><asp:Label ID="lblDueMar" runat="server" Text='<%# Eval("FeeDue_MARCH")%>' class="label-value"></asp:Label>
                                                                                            </h3>
                                                                                        </div>
                                                                                        <div class="col-6 col-md-6 col-lg-6">
                                                                                            <h3 class="m-0 w-100 pt-2 pb-2">
                                                                                                <span class="d-block mb-1 font-weight-bold">Fees due for Apr to Jun 20</span>
                                                                                                <asp:Label ID="lblDueJun" runat="server" Text='<%# Eval("FeeDue_APRJUN")%>' class="label-value"></asp:Label>
                                                                                            </h3>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:Repeater>



                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- student details end -->
                                </div>
                            </div>
                        </section>
                        <!-- personal details -->

                        <section class="salary-details mt-3">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card rounded-0 h-100">
                                            <div class="card-body p-0">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h2 class="m-0 bg-primary pl-2">Employment Details</h2>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <!-- fathers monthly income -->
                                                    <div class="col-12 col-md-6">
                                                        <div class="row">
                                                            <div class="col-12 pr-0 border-right">
                                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                                    <h3 class="m-0">Father</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 border-right">
                                                                <div class="column-padding pt-3 pb-3">
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Employment Type</span>
                                                                                <asp:Label ID="lblFempType" runat="server" Text=""></asp:Label>
                                                                                <asp:DropDownList ID="ddlFempType" runat="server" Style="display: none;" AutoPostBack="true">
                                                                                    <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                                                                    <asp:ListItem Value="1" Text="Self Employed"></asp:ListItem>
                                                                                    <asp:ListItem Value="2" Text="Salaried"></asp:ListItem>
                                                                                    <asp:ListItem Value="3" Text="Not Working"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                <%--<asp:Label ID="lblFempType" runat="server" class="text-danger small"></asp:Label>--%>
                                                                            </h3>
                                                                        </div>
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Reason for Relief</span>
                                                                                <asp:Label ID="lblFreasonrelief" runat="server" Text=""></asp:Label>
                                                                                <asp:DropDownList ID="ddlFReasonRelief" runat="server" style="display:none;"></asp:DropDownList>
                                                                                <%--<asp:Label ID="lblFReasonRelief" runat="server" class="text-danger small"></asp:Label>--%>
                                                               
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">National ID</span>
                                                                                <asp:Label ID="txtFNationID" runat="server" TextMode="Number" class="label-value"></asp:Label>
                                                                            </h3>
                                                                        </div>
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Company Name</span>
                                                                                <asp:Label ID="txtFCompany" runat="server" class="label-value"></asp:Label>
                                                                            </h3>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- fathers monthly income -->
                                                    <!-- mothers monthly income -->
                                                    <div class="col-12 col-md-6">
                                                        <div class="row">
                                                            <div class="col-12 pl-0">
                                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                                    <h3 class="m-0">Mother</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="column-padding pt-3 pb-3">
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Employment Type</span>
                                                                                <asp:Label ID="lblMemptype" runat="server" Text=""></asp:Label>
                                                                                <asp:DropDownList ID="ddlMEmpType" runat="server" style="display:none;" AutoPostBack="true">
                                                                                    <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                                                                    <asp:ListItem Value="1" Text="Self Employed"></asp:ListItem>
                                                                                    <asp:ListItem Value="2" Text="Salaried"></asp:ListItem>
                                                                                    <asp:ListItem Value="3" Text="Not Working"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </h3>
                                                                        </div>

                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Reason for Relief</span>
                                                                                <asp:Label ID="lblMreasonforrelief" runat="server" Text=""></asp:Label>
                                                                                <asp:DropDownList ID="ddlMReasonRelief" runat="server" style="display:none;"></asp:DropDownList>
                                                                            </h3>
                                                                        </div>

                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">National ID</span>
                                                                                <asp:Label ID="txtMNationID" runat="server" CssClass="label-value" TextMode="Number"></asp:Label>
                                                                            </h3>
                                                                        </div>

                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Company Name</span>
                                                                                <asp:Label ID="txtMCompany" runat="server" class="label-value"></asp:Label>
                                                                            </h3>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- mothers monthly income -->
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>


                        <!-- Salary details -->
                        <section class="salary-details mt-3">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card rounded-0 h-100">
                                            <div class="card-body p-0">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h2 class="m-0 bg-primary pl-2">Salary Details</h2>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <!-- fathers monthly income -->
                                                    <div class="col-12 col-md-6">
                                                        <div class="row">
                                                            <div class="col-12 pr-0 border-right">
                                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                                    <h3 class="m-0">Father - Monthly Salary (AED)</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 border-right">
                                                                <div class="column-padding pt-3 pb-3">
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Salary and Allowance</span>

                                                                                <asp:Label ID="txtFsalAllow" runat="server" class="label-value" Text="0" TextMode="Number"></asp:Label>

                                                                            </h3>
                                                                        </div>

                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Housing</span>
                                                                                <asp:Label ID="txtFHousing" runat="server" class="label-value" Text="0" TextMode="Number"></asp:Label>
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Tuition</span>
                                                                                <asp:Label ID="txtFTution" runat="server" class="label-value" Text="0" TextMode="Number"></asp:Label></h3>
                                                                        </div>

                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Others</span>
                                                                                <asp:Label ID="txtFOthers" runat="server" class="label-value" Text="0" TextMode="Number"></asp:Label></h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="display: none;">
                                                                        <div class="col-12">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Total salary monthly</span>
                                                                                <asp:Label ID="lblFSalary" runat="server" class="label-value" Text="0"></asp:Label>
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- fathers monthly income -->
                                                    <!-- mothers monthly income -->
                                                    <div class="col-12 col-md-6">
                                                        <div class="row">
                                                            <div class="col-12 pl-0">
                                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                                    <h3 class="m-0">Mother - Monthly Salary (AED)</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="column-padding pt-3 pb-3">
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Salary and Allowance</span>
                                                                                <asp:Label ID="txtMSalAllow" runat="server" Text="0" class="label-value" TextMode="Number"></asp:Label>
                                                                            </h3>
                                                                        </div>
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Housing</span>
                                                                                <asp:Label ID="txtMHousing" runat="server" Text="0" class="label-value" TextMode="Number"></asp:Label></h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Tuition</span>
                                                                                <asp:Label ID="txtMTution" runat="server" Text="0" class="label-value" TextMode="Number"></asp:Label></h3>
                                                                        </div>
                                                                        <div class="col-12 col-md-6">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Others</span>
                                                                                <asp:Label ID="txtMOthers" runat="server" Text="0" class="label-value" TextMode="Number"></asp:Label></h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="display: none;">
                                                                        <div class="col-12">
                                                                            <h3 class="m-0 mb-2">
                                                                                <span class="d-block font-weight-bold">Total salary monthly</span>
                                                                                <asp:Label ID="lblMSalary" runat="server" class="label-value" Text="0"></asp:Label>
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- mothers monthly income -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- salary details -->
                        <!-- document section -->
                        <section class="upload-documents mt-3">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card rounded-0 h-100">
                                            <div class="card-body p-0">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h2 class="m-0 bg-primary pl-2">Upload relevant documents in order to process your application</h2>
                                                    </div>
                                                </div>
                                                <!-- fathers documents -->
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="row d-none d-sm-flex">
                                                            <div class="col-12">
                                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                                    <h3 class="m-0">Fathers Documents </h3>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Repeater ID="rptFDocument" runat="server">

                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("RDM_ID")%>' />
                                                                <div class="row mt-3">
                                                                    <div class="col-md-9 col-12 pr-0 d-flex align-items-center">
                                                                        <div class="column-padding pt-2 pb-2">
                                                                            <h3 class="m-0 text-regular">
                                                                                <asp:Label ID="lblFDocname" runat="server" Text='<%# Eval("RDM_DESC")%>'></asp:Label>
                                                                            </h3>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-12 pl-0 d-flex  justify-content-center flex-column">
                                                                        <div class="column-padding pt-2 pb-2 text-left" style="line-height: 14px;">
                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FILEURL")%>'></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- fathers documents -->
                                                <!-- mothers documents -->
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="row d-none d-sm-flex">
                                                            <div class="col-12">
                                                                <div class="column-padding py-2 px-3 border-top border-bottom bg-light">
                                                                    <h3 class="m-0">Mothers Documents  
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Repeater ID="rptMDocument" runat="server">

                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hdnID1" runat="server" Value='<%# Eval("RDM_ID")%>' />
                                                                <div class="row mt-3">
                                                                    <div class="col-md-9 col-12 pr-0 d-flex align-items-center">
                                                                        <div class="column-padding pt-2 pb-2">
                                                                            <h3 class="m-0 text-regular">
                                                                                <asp:Label ID="lblMDocName" runat="server" Text='<%# Eval("RDM_DESC")%>'></asp:Label>
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-12 pl-0 d-flex  justify-content-center flex-column">
                                                                        <div class="column-padding pt-2 pb-2 text-left" style="line-height: 14px;">
                                                                            <asp:Label ID="lblFileName1" runat="server" Text='<%# Eval("RRF_FILENAME")%>'></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- mothers documents -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- document section -->
                        <!-- notes for GEMS section -->
                        <section class="user-message mt-3">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card rounded-0 h-100">
                                            <div class="card-body p-0">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h2 class="m-0 bg-primary pl-2">Note to GEMS</h2>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="column-padding py-3">
                                                            <div class="form-group">
                                                                <asp:Label rows="4" ID="txtRemarks" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <%--</main>--%>
                    </div>
                    <!-- /Posts Block -->
                </div>
            </div>
        </div>
    </form>
</body>
</html>
