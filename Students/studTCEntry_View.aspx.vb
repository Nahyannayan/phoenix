Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studTcEntry_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100085" And ViewState("MainMnu_code") <> "S100256" _
                And ViewState("MainMnu_code") <> "S100257" And ViewState("MainMnu_code") <> "S100480") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    ' GridBind()
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = "0"
                    ddlGrade.Items.Insert(0, li)


                    PopulateSection()

                    If ViewState("MainMnu_code") = "S100085" Then
                        lblTitle.Text = "Withdrawal Entry"
                    ElseIf ViewState("MainMnu_code") = "S100256" Then
                        lblTitle.Text = "Request For Change Of TC"
                    ElseIf ViewState("MainMnu_code") = "S100257" Then
                        lblTitle.Text = "Change TC Approval"
                    ElseIf ViewState("MainMnu_code") = "S100480" Then
                        lblTitle.Text = "Convert SO To TC/LC"
                    End If
                    tblTC.Rows(4).Visible = False
                    'tblTC.Rows(5).Visible = False
                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        Else



        End If
    End Sub
    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub lblType_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim url As String
        '      Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        '   Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
        Dim lblStuId As Label
        Dim lblStuName As Label
        Dim lblStuNo As Label
        Dim lblGrade As Label
        Dim lblSection As Label
        Dim lblDoj As Label
        Dim lblSctId As Label
        Dim lblGrdId As Label
        With sender
            lblStuId = .FindControl("lblStuId")
            lblStuName = .FindControl("lblStuName")
            lblStuNo = .FindControl("lblStuNo")
            lblGrade = .FindControl("lblGrade")
            lblSection = .FindControl("lblSection")
            lblDoj = .FindControl("lblDoj")
            lblSctId = .FindControl("lblSctId")
            lblGrdId = .FindControl("lblGrdId")
        End With



        If ViewState("MainMnu_code") = "S100256" Then
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            url = String.Format("~\Students\studTc_Student_ChangeReq_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(hfACD_ID.Value) _
                   & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
                   & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
                   & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                   & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
                   & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
                   & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        ElseIf ViewState("MainMnu_code") = "S100085" Or ViewState("MainMnu_code") = "S100480" Then
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            url = String.Format("~\Students\studTCEntry_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(hfACD_ID.Value) _
            & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
            & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
            & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
            & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
            & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
            & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        ElseIf ViewState("MainMnu_code") = "S100257" Then
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            url = String.Format("~\Students\studTc_Student_ChangeApprove_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(hfACD_ID.Value) _
            & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
            & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
            & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
            & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
            & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
            & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) _
            & "&sctid=" + Encr_decrData.Encrypt(lblSctId.Text) _
            & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
        End If
        Response.Redirect(url)
    End Sub
#Region "Private Methods"

    Private Sub PopulateSection()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"


        ddlSection.Items.Clear()
        If ddlGrade.SelectedValue = "All" Then
            ddlSection.Items.Insert(0, li)
        Else
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                     & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                     & " ORDER BY SCT_DESCR "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, li)
        End If
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub ddlgvEntry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If ViewState("MainMnu_code") = "S100085" Then
            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,''))," _
                       & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID, " _
                       & " TYPE=CASE TCM_bPREAPPROVED WHEN 'TRUE' THEN 'view' ELSE CASE WHEN TCM_ID IS NULL THEN 'add' ELSE 'edit' END  END " _
                       & " ,CASE TCM_ENTRY_TYPE WHEN 'PARENT' THEN 'ONLINE' WHEN 'SCHOOL' THEN 'SCHOOL' ELSE '' END AS ENTRYTYPE" _
                       & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                       & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                       & " LEFT OUTER JOIN TCM_M AS D ON A.STU_ID=D.TCM_STU_ID AND TCM_TCSO='TC' AND TCM_CANCELDATE IS NULL" _
                       & " WHERE STU_ACD_ID = " + hfACD_ID.Value _
                       & " AND STU_CURRSTATUS<>'CN'"

      
        ElseIf ViewState("MainMnu_code") = "S100256" Then

            'CHANGE OF TC REQUEST
            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                         & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,TYPE='View' " _
                         & " ,CASE TCM_ENTRY_TYPE WHEN 'PARENT' THEN 'ONLINE' WHEN 'SCHOOL' THEN 'SCHOOL' ELSE '' END AS ENTRYTYPE" _
                         & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                         & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                         & " INNER JOIN TCM_M AS D ON A.STU_ID=D.TCM_STU_ID " _
                         & " WHERE TCM_CANCELDATE IS NULL AND STU_ACD_ID = " + hfACD_ID.Value _
                         & " AND TCM_TCSO='TC'  AND TCM_ID NOT IN (SELECT STT_TCM_ID FROM " _
                         & " STUDENT_TCTYPES_S WHERE STT_bAPPROVED='FALSE')"
        ElseIf ViewState("MainMnu_code") = "S100257" Then
            'change tc approval
            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                                    & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID,Type='View' " _
                                    & " ,CASE TCM_ENTRY_TYPE WHEN 'PARENT' THEN 'ONLINE' WHEN 'SCHOOL' THEN 'SCHOOL' ELSE '' END AS ENTRYTYPE" _
                                    & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                                    & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                                    & " INNER JOIN TCM_M AS D ON A.STU_ID=D.TCM_STU_ID " _
                                    & " WHERE TCM_CANCELDATE IS NULL AND STU_ACD_ID = " + hfACD_ID.Value _
                                    & " AND TCM_TCSO='TC'  AND TCM_ID IN (SELECT STT_TCM_ID FROM " _
                                    & " STUDENT_TCTYPES_S WHERE STT_bAPPROVED='FALSE')"
        ElseIf ViewState("MainMnu_code") = "S100480" Then
            'CONVERT SO TO TC/LC
            str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,''))," _
                               & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR,STU_DOJ,STU_SCT_ID,STU_GRD_ID, " _
                               & " TYPE='Convert' " _
                               & " ,CASE TCM_ENTRY_TYPE WHEN 'PARENT' THEN 'ONLINE' WHEN 'SCHOOL' THEN 'SCHOOL' ELSE '' END AS ENTRYTYPE" _
                               & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                               & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                               & " INNER JOIN TCM_M AS D ON A.STU_ID=D.TCM_STU_ID AND TCM_TCSO='SO' AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL" _
                               & " WHERE STU_ACD_ID = " + hfACD_ID.Value _
                               & " AND STU_CURRSTATUS<>'CN'"
        End If


        If ViewState("MainMnu_code") <> "S100480" Then
            'EXCLUDE STRIKEOFF CASES
            str_query += " AND STU_ID NOT IN(SELECT TCM_STU_ID FROM TCM_M WHERE TCM_TCSO='SO' AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL)"
        End If
        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID= " + hfSCT_ID.Value
        End If
        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID= '" + hfGRD_ID.Value + "'"
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If


        If rdNotApplied.Checked = True Then
            str_query += " AND TCM_ID IS NULL"
        End If

        If rdApplied.Checked = True Then
            str_query += " AND TCM_ID IS NOT NULL"
        End If

        
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        Dim ddlgvEntry As DropDownList
        Dim strEntry As String = ""
        If gvStud.Rows.Count > 0 Then
            ddlgvEntry = gvStud.HeaderRow.FindControl("ddlgvEntry")
            If ddlgvEntry.SelectedItem.Text.ToUpper = "ONLINE" Then
                str_query += " AND ISNULL(TCM_ENTRY_TYPE,'')='PARENT'"
            ElseIf ddlgvEntry.SelectedItem.Text.ToUpper = "SCHOOL" Then
                str_query += " AND ISNULL(TCM_ENTRY_TYPE,'')='SCHOOL'"
            End If
            strEntry = ddlgvEntry.SelectedItem.Text
        End If
      
        If ViewState("MainMnu_code") <> "S100254" Then
            str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds


      
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            ViewState("norecord") = "1"
        Else
            gvStud.DataBind()
            ViewState("norecord") = "0"
        End If

        If gvStud.Rows.Count > 0 Then
            ddlgvEntry = gvStud.HeaderRow.FindControl("ddlgvEntry")
            If Not ddlgvEntry.Items.FindByText(strEntry) Is Nothing Then
                ddlgvEntry.Items.FindByText(strEntry).Selected = True
            End If
        End If
       


    End Sub


#End Region



    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        'GridBind()
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)


        PopulateSection()
        If ViewState("norecord") = "1" Then
            tblTC.Rows(4).Visible = False
            'tblTC.Rows(5).Visible = False
        End If
    End Sub


    Protected Sub gvStudTPT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStud.PageIndexChanging
        gvStud.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvStudTPT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStud.RowCommand
        If e.CommandName = "view" Then
            Dim url As String
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStud.Rows(index), GridViewRow)
            Dim lblStuId As Label
            Dim lblStuName As Label
            Dim lblStuNo As Label
            Dim lblGrade As Label
            Dim lblSection As Label
            Dim lblDoj As Label
            Dim lblSctId As Label
            Dim lblGrdId As Label
            With selectedRow
                lblStuId = .FindControl("lblStuId")
                lblStuName = .FindControl("lblStuName")
                lblStuNo = .FindControl("lblStuNo")
                lblGrade = .FindControl("lblGrade")
                lblSection = .FindControl("lblSection")
                lblDoj = .FindControl("lblDoj")
                lblSctId = .FindControl("lblSctId")
                lblGrdId = .FindControl("lblGrdId")
            End With



            If ViewState("MainMnu_code") = "S100256" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Students\studTc_Student_ChangeReq_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(hfACD_ID.Value) _
                       & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
                       & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
                       & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                       & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
                       & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
                       & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf ViewState("MainMnu_code") = "S100085" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Students\studTCEntry_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(hfACD_ID.Value) _
                & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
                & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
                & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
                & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
                & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            ElseIf ViewState("MainMnu_code") = "S100257" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                url = String.Format("~\Students\studTc_Student_ChangeApprove_M.aspx?MainMnu_code={0}&datamode={1}&acdid=" + Encr_decrData.Encrypt(hfACD_ID.Value) _
                & "&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
                & "&stuno=" + Encr_decrData.Encrypt(lblStuNo.Text) _
                & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                & "&section=" + Encr_decrData.Encrypt(lblSection.Text) _
                & "&doj=" + Encr_decrData.Encrypt(lblDoj.Text) _
                & "&stuname=" + Encr_decrData.Encrypt(lblStuName.Text) _
                & "&sctid=" + Encr_decrData.Encrypt(lblSctId.Text) _
                & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            End If
            Response.Redirect(url)
        End If
    End Sub



    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        PopulateSection()
        If ViewState("norecord") = "1" Then
            tblTC.Rows(4).Visible = False
            'tblTC.Rows(5).Visible = False
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        tblTC.Rows(4).Visible = True
        'tblTC.Rows(5).Visible = True
        hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        hfGRD_ID.Value = ddlGrade.SelectedValue.ToString
        hfSCT_ID.Value = ddlSection.SelectedValue.ToString
        hfSTUNO.Value = txtStuNo.Text
        hfNAME.Value = txtName.Text
        GridBind()
    End Sub
End Class
