Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Students_studSiblings
    Inherits System.Web.UI.Page

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            addsib()
        Catch ex As Exception

        End Try
    End Sub
    Sub addsib()
        Try
            If txt_par_NewSibling.Text.Trim() <> "" Then
                If h_StudentId.Value = "" Then
                    h_StudentId.Value = h_NewSiblingID.Value
                Else
                    h_StudentId.Value += "," + h_NewSiblingID.Value
                End If
                If txtPar_Sib.Text.Trim() <> "" And txt_par_NewSibling.Text.Trim() <> "" Then
                    ADDSiblings()
                    txt_par_NewSibling.Text = ""
                End If
            Else
                BindBlankRow()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If txtPar_Sib.Text = "" Then
                    lblStudName.Visible = False
                End If
                If txt_par_NewSibling.Text = "" Then
                    lblSibling.Visible = False
                End If
                h_SliblingID.Value = 0
                BindBlankRow()
            End If
            If txtPar_Sib.Text.Trim <> "" Then
                txt_par_NewSibling.Focus()
            Else
                txtPar_Sib.Focus()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindSiblings()
        Dim dsSibling As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        'lblSibling
        Dim strSQL As String = ""
        strSQL = "SELECT STUDENT_M.STU_ID ,BSU_NAME,STUDENT_M.STU_NO,isNull(STU_FIRSTNAME,'')+' '+isNull(STU_MIDNAME,'')+' '+isNull(STU_LASTNAME,'') as STU_FIRSTNAME," _
                & "STU_PRIMARYCONTACT,STU_DOB," _
                & " (select SCT_DESCR from SECTION_M where SCT_ID= STUDENT_M.stu_sct_id ) as SECTION " _
                & ",(select GRM_DISPLAY from GRADE_BSU_M where grm_id =STUDENT_M.STU_GRM_ID) as GRADE " _
                & ",(select STM_DESCR FROM STREAM_M WHERE STM_ID=STUDENT_M.STU_STM_ID)AS STREAM " _
                & ",(select SHF_DESCR from SHIFTS_M where SHF_ID=STUDENT_M.stu_SHF_ID) AS SHIFT " _
                & ",(select CTY_DESCR from country_m where CTY_ID =STUDENT_M.STU_NATIONALITY ) AS Nationality " _
                & ",ISNULL((select house_description from house_m where house_bsu_id='" & Session("sBsuid") & "' and house_id =STUDENT_M.STU_HOUSE_ID ),'') AS House " _
                & " FROM STUDENT_M " _
                & "left JOIN STUDENT_D ON STUDENT_M.STU_ID =STUDENT_D.STS_STU_ID  " _
                & "INNER JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID=STUDENT_M.STU_BSU_ID " _
                & "INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID=  SECTION_M.sct_id " _
                & "WHERE STU_CURRSTATUS NOT IN ('CN','TF') " _
                & " AND stu_sibling_id IN (select stu_sibling_id from  STUDENT_M  group by stu_sibling_id having count(stu_sibling_id)>=1) " _
                & "AND stu_sibling_id='" & h_SliblingID.Value & "' and stu_bsu_id='" & Session("sBsuid") & "' ORDER BY STUDENT_M.STU_ID "

        dsSibling = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        gvStudEnquiry.DataSource = dsSibling.Tables(0)
        'h_SliblingID
        If dsSibling.Tables(0).Rows.Count = 0 Then
            dsSibling.Tables(0).Rows.Add(dsSibling.Tables(0).NewRow())
            gvStudEnquiry.DataBind()
            Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
            gvStudEnquiry.Rows(0).Cells.Clear()
            gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
            gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudEnquiry.DataBind()
            Button1.Enabled = True
        End If
    End Sub
    Private Sub ADDSiblings()
        Dim dsSibling As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        'lblSibling
        Dim strSQL As String = ""
        strSQL = "SELECT STUDENT_M.STU_ID ,BSU_NAME,STUDENT_M.STU_NO,isNull(STU_FIRSTNAME,'')+' '+isNull(STU_MIDNAME,'')+' '+isNull(STU_LASTNAME,'') as STU_FIRSTNAME," _
                        & "STU_PRIMARYCONTACT,STU_DOB," _
                        & " (select SCT_DESCR from SECTION_M where SCT_ID= STUDENT_M.stu_sct_id ) as SECTION " _
                        & ",(select GRM_DISPLAY from GRADE_BSU_M where grm_id =STUDENT_M.STU_GRM_ID) as GRADE " _
                        & ",(select STM_DESCR FROM STREAM_M WHERE STM_ID=STUDENT_M.STU_STM_ID)AS STREAM " _
                        & ",(select SHF_DESCR from SHIFTS_M where SHF_ID=STUDENT_M.stu_SHF_ID) AS SHIFT " _
                        & ",(select CTY_DESCR from country_m where CTY_ID =STUDENT_M.STU_NATIONALITY ) AS Nationality " _
 & ",ISNULL((select house_description from house_m where house_bsu_id='" & Session("sBsuid") & "' and house_id =STUDENT_M.STU_HOUSE_ID ),'') AS House " _
 & " FROM STUDENT_M " _
                        & "left JOIN STUDENT_D ON STUDENT_M.STU_ID =STUDENT_D.STS_STU_ID  " _
                        & "INNER JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID=STUDENT_M.STU_BSU_ID " _
                        & "INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID=  SECTION_M.sct_id " _
                        & "WHERE STU_CURRSTATUS NOT IN ('CN','TF') " _
                        & " AND stu_sibling_id IN (select stu_sibling_id from  STUDENT_M  group by stu_sibling_id having count(stu_sibling_id)>=1) " _
                        & "AND stu_sibling_id='" & h_SliblingID.Value & "'and stu_bsu_id='" & Session("sBsuid") & "'"
        strSQL += " UNION "

        strSQL += "SELECT STUDENT_M.STU_ID ,BSU_NAME,STUDENT_M.STU_NO,isNull(STU_FIRSTNAME,'')+' '+isNull(STU_MIDNAME,'')+' '+isNull(STU_LASTNAME,'') as STU_FIRSTNAME," _
                      & "STU_PRIMARYCONTACT,STU_DOB," _
                      & " (select SCT_DESCR from SECTION_M where SCT_ID= STUDENT_M.stu_sct_id ) as SECTION " _
                      & ",(select GRM_DISPLAY from GRADE_BSU_M where grm_id =STUDENT_M.STU_GRM_ID) as GRADE " _
                      & ",(select STM_DESCR FROM STREAM_M WHERE STM_ID=STUDENT_M.STU_STM_ID)AS STREAM " _
                      & ",(select SHF_DESCR from SHIFTS_M where SHF_ID=STUDENT_M.stu_SHF_ID) AS SHIFT " _
                      & ",(select CTY_DESCR from country_m where CTY_ID =STUDENT_M.STU_NATIONALITY ) AS Nationality " _
                       & ",ISNULL((select house_description from house_m where house_bsu_id='" & Session("sBsuid") & "' and house_id =STUDENT_M.STU_HOUSE_ID ),'') AS House " _
                      & " FROM STUDENT_M " _
                      & "left JOIN STUDENT_D ON STUDENT_M.STU_ID =STUDENT_D.STS_STU_ID  " _
                      & "INNER JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID=STUDENT_M.STU_BSU_ID " _
                      & "INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID=  SECTION_M.sct_id " _
                      & "WHERE STU_CURRSTATUS NOT IN ('CN','TF')" _
                      & " AND stu_sibling_id IN (select stu_sibling_id from  STUDENT_M  group by stu_sibling_id having count(stu_sibling_id)>=1) " _
                      & "AND stu_sibling_id in(" & h_StudentId.Value & ")and stu_bsu_id='" & Session("sBsuid") & "' "




        'strSQL += "SELECT STUDENT_M.STU_ID ,BSU_NAME,STUDENT_M.STU_NO,isNull(STU_FIRSTNAME,'')+' '+isNull(STU_MIDNAME,'')+' '+isNull(STU_LASTNAME,'') as STU_FIRSTNAME," _
        '                & "STU_PRIMARYCONTACT,STU_DOB," _
        '                & " (select SCT_DESCR from SECTION_M where SCT_ID= STUDENT_M.stu_sct_id ) as SECTION " _
        '                & ",(select GRM_DISPLAY from GRADE_BSU_M where grm_id =STUDENT_M.STU_GRM_ID) as GRADE " _
        '                & ",(select STM_DESCR FROM STREAM_M WHERE STM_ID=STUDENT_M.STU_STM_ID)AS STREAM " _
        '                & ",(select SHF_DESCR from SHIFTS_M where SHF_ID=STUDENT_M.stu_SHF_ID) AS SHIFT " _
        '                & ",(select CTY_DESCR from country_m where CTY_ID =STUDENT_M.STU_NATIONALITY ) AS Nationality " _
        '                & " FROM STUDENT_M " _
        '                & "INNER JOIN STUDENT_D ON STUDENT_M.STU_ID =STUDENT_D.STS_STU_ID  " _
        '                & "INNER JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID=STUDENT_M.STU_BSU_ID " _
        '                & "INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID=  SECTION_M.sct_id " _
        '                & "WHERE STU_CURRSTATUS<>'CN' AND STU_ID IN (" & h_StudentId.Value & ")"

        dsSibling = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        gvStudEnquiry.DataSource = dsSibling.Tables(0)
        'h_SliblingID
        If dsSibling.Tables(0).Rows.Count = 0 Then
           
        Else
            gvStudEnquiry.DataBind()
            Button1.Enabled = True
        End If
    End Sub

    Private Sub BindBlankRow()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim dsSibling As DataSet
        Dim strSQL As String = ""
        strSQL = "SELECT STUDENT_M.STU_ID ,BSU_NAME,STUDENT_M.STU_NO,isNull(STU_FIRSTNAME,'')+' '+isNull(STU_MIDNAME,'')+' '+isNull(STU_LASTNAME,'') as STU_FIRSTNAME," _
                & "STU_PRIMARYCONTACT,STU_DOB," _
                & " (select SCT_DESCR from SECTION_M where SCT_ID= STUDENT_M.stu_sct_id ) as SECTION " _
                & ",(select GRM_DISPLAY from GRADE_BSU_M where grm_id =STUDENT_M.STU_GRM_ID) as GRADE " _
                & ",(select STM_DESCR FROM STREAM_M WHERE STM_ID=STUDENT_M.STU_STM_ID)AS STREAM " _
                & ",(select SHF_DESCR from SHIFTS_M where SHF_ID=STUDENT_M.stu_SHF_ID) AS SHIFT " _
                & ",(select CTY_DESCR from country_m where CTY_ID =STUDENT_M.STU_NATIONALITY ) AS Nationality " _
                 & ",ISNULL((select house_description from house_m where house_bsu_id='" & Session("sBsuid") & "' and house_id =STUDENT_M.STU_HOUSE_ID ),'') AS House " _
                & " FROM STUDENT_M " _
                & "INNER JOIN STUDENT_D ON STUDENT_M.STU_ID =STUDENT_D.STS_STU_ID  " _
                & "INNER JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID=STUDENT_M.STU_BSU_ID " _
                & "INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID=  SECTION_M.sct_id " _
                & "WHERE STU_CURRSTATUS  NOT IN ('CN','TF') " _
                & " AND stu_sibling_id IN (select stu_sibling_id from  STUDENT_M  group by stu_sibling_id having count(stu_sibling_id)>1) " _
                & "AND STU_NO='" & h_StudentId.Value & "' and stu_bsu_id='" & Session("sBsuid") & "' ORDER BY STUDENT_M.STU_ID "

        dsSibling = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        gvStudEnquiry.DataSource = dsSibling.Tables(0)

        dsSibling.Tables(0).Rows.Add(dsSibling.Tables(0).NewRow())
        gvStudEnquiry.DataBind()
        Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
        gvStudEnquiry.Rows(0).Cells.Clear()
        gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
        gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
        gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
        gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records."
    End Sub

    Protected Sub imgbtnSibling_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If txtPar_Sib.Text <> "" Then
                Dim arr As String()
                arr = GetStudentname(txtPar_Sib.Text)
                lblStudName.Visible = True
                lblStudName.Text = arr(0)
                'lblStudName.Text = GetStudentname(txtPar_Sib.Text) 'h_SliblingID.Value
                BindSiblings()
            Else
                BindBlankRow()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub txtPar_Sib_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtPar_Sib.Text <> "" Then
                Dim arr As String()
                arr = GetStudentname(txtPar_Sib.Text)
                If arr(0) <> "" Then
                    lblStudName.Visible = True
                End If
                lblStudName.Text = arr(0) 'h_SliblingID.Value
                h_SliblingID.Value = arr(1)

                BindSiblings()
            Else
                lblStudName.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim chkSelect As CheckBox
        Dim transaction As SqlTransaction
        Dim strQuery As String
        Dim LblStudentId As Label



        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try
                For Each GvRow As GridViewRow In gvStudEnquiry.Rows
                    If GvRow.RowType = DataControlRowType.DataRow Then
                        chkSelect = GvRow.FindControl("chkSelect")
                        If chkSelect.Checked = True Then
                            'To save the selected records
                            LblStudentId = GvRow.FindControl("stuId")
                            transaction = conn.BeginTransaction("SampleTransaction")

                            strQuery = "exec UpdateStudentSibling  " + LblStudentId.Text + "," + h_SliblingID.Value + ""
                            SqlHelper.ExecuteNonQuery(Transaction, CommandType.Text, strQuery)

                            lblError.Text = "Record Saved Successfully"
                            transaction.Commit()


                            Button1.Enabled = False
                            txt_par_NewSibling.Text = ""
                            txtPar_Sib.Text = ""
                            lblSibling.Text = ""
                            lblStudName.Text = ""
                            h_StudentId.Value = ""
                            h_NewSiblingID.Value = ""
                            BindBlankRow()
                        End If
                    End If
                Next
            Catch myex As ArgumentException
                Transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                Transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
        If gvStudEnquiry.Rows.Count <= 1 Then
            BindBlankRow()
        End If
        txtPar_Sib.Focus()
    End Sub
    Private Function GetStudentname(ByVal StudNo As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim dsStudent As DataSet
        Dim strSQL As String = ""
        strSQL = "SELECT STUDENT_M.STU_ID,isNull(STU_FIRSTNAME,'')+' '+isNull(STU_MIDNAME,'')+' '+isNull(STU_LASTNAME,'') as STU_FIRSTNAME,stu_sibling_id FROM STUDENT_M WHERE STU_NO like '%" & StudNo & "' and stu_bsu_id='" & Session("sBsuid") & "'"

        dsStudent = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        If dsStudent.Tables(0).Rows.Count >= 1 Then
            'h_SliblingID.Value = dsStudent.Tables(0).Rows(0).Item("stu_sibling_id").ToString()
            Dim stu_name_sibid(5) As String

            stu_name_sibid(0) = dsStudent.Tables(0).Rows(0).Item("STU_FIRSTNAME").ToString()
            stu_name_sibid(1) = dsStudent.Tables(0).Rows(0).Item("stu_sibling_id").ToString()
            Return stu_name_sibid
        End If
    End Function

    Protected Sub txt_par_NewSibling_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txt_par_NewSibling.Text <> "" Then
                Dim arr As String()
                arr = GetStudentname(txt_par_NewSibling.Text)
                If (arr Is Nothing) Then
                    lblSibling.Text = "NO Student Found....!!!!!"
                Else
                    lblSibling.Text = arr(0)
                    h_NewSiblingID.Value = arr(1)
                    'addsib()
                End If
                'h_NewSiblingID.Value
                'BindSiblings()
            Else
                BindBlankRow()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'Try
        '    If txt_par_NewSibling.Text <> "" Then
        '        Dim arr As String()
        '        arr = GetStudentname(txt_par_NewSibling.Text)
        '        If (arr Is Nothing) Then
        '            lblSibling.Text = "NO Student Found....!!!!!"
        '        Else
        '            lblSibling.Text = arr(0)
        '        End If
        '        'BindSiblings()
        '    Else
        '        BindBlankRow()
        '    End If
        'Catch ex As Exception

        'End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            txt_par_NewSibling.Text = ""
            txtPar_Sib.Text = ""
            lblSibling.Text = ""
            lblStudName.Text = ""
            h_StudentId.Value = ""
            h_NewSiblingID.Value = ""
            lblError.Text = ""
            BindBlankRow()
        Catch ex As Exception

        End Try
        txtPar_Sib.Focus()
    End Sub


End Class
