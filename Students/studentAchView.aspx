﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="studentAchView.aspx.vb" Inherits="Students_studentAchView" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <title>:::: GEMS :::: Student Achivements ::::</title>
    <%--  <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StudDashBoard.css" rel="stylesheet" type="text/css" />--%>

    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <%--    <link href="/cssfiles/custome.css" rel="stylesheet">--%>
    <%--    <link href="/cssfiles/sb-admin.css" rel="stylesheet" >--%>
    <link href="/cssfiles/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/cssfiles/jquery-ui.structure.min.css" rel="stylesheet">


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta content="MShtml 6.00.2900.3268" name="GENERATOR">

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="popup" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <script language="javascript" type="text/javascript">
        function showDocument(filename, contenttype) {

            result = radopen("IFrameNew.aspx?filename=" + filename + "&contenttype=" + contenttype, "popup");
            return false;
        }
        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }

        function divIMG(pId, val, ctrl1, pImg) {
            var path;

            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }

            if (pId == 1) {
                document.getElementById("<%=getid("mnu_1_img") %>").src = path;
            }
            else if (pId == 2) {
                document.getElementById("<%=getid("mnu_2_img") %>").src = path;
            }
            else if (pId == 3) {
                document.getElementById("<%=getid("mnu_3_img") %>").src = path;
            }
            else if (pId == 4) {
                document.getElementById("<%=getid("mnu_4_img") %>").src = path;
            }
            else if (pId == 5) {
                document.getElementById("<%=getid("mnu_5_img") %>").src = path;
            }
            else if (pId == 6) {
                document.getElementById("<%=getid("mnu_6_img") %>").src = path;
            }
            else if (pId == 7) {
                document.getElementById("<%=getid("mnu_7_img") %>").src = path;
                }

    document.getElementById(ctrl1).value = val + '__' + path;
}
    </script>


    <style type="text/css">
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

            .switch input {
                opacity: 0;
                width: 0;
                height: 0;
            }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

            .slider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        .slider.round {
            border-radius: 34px;
        }

            .slider.round:before {
                border-radius: 50%;
            }

        .style1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
        }
    </style>

    <style type="text/css">
        .style1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
        }

        .style2 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
            width: 3px;
        }
    </style>
</head>
<body text="#000000" bgcolor="#ffffff" leftmargin="0" topmargin="0">
    <form id="Form1" runat="server">


        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>


        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table id="tblStudProfile">
                    <tr>
                        <td align="center" class="title" valign="middle">
                            <table style="width: 100%;"
                                cellspacing="0" cellpadding="0" align="center" border="0">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr class="title-bg">
                                                    <td align="left">Student Achievements</td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <table id="Table1" align="left" border="0" cellpadding="0" cellspacing="0" width="100%" runat="server">
                                                            <tr>
                                                                <td align="left" class="style1">
                                                                    <span class="field-label">Name</span></td>

                                                                <td colspan="3" align="left">
                                                                    <asp:Literal ID="ltStudName" runat="server"></asp:Literal></td>
                                                                <td align="center" rowspan="10">
                                                                    <asp:Image ID="imgEmpImage" runat="server" Height="100px" ImageUrl="~/Images/Photos/no_image.gif" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><span class="field-label">Student ID</span> </td>

                                                                <td colspan="4" align="left">
                                                                    <asp:Literal ID="ltStudId" runat="server"></asp:Literal></td>
                                                            </tr>
                                                            <tr runat="server" id="trCurr">
                                                                <td align="left"><span class="field-label">Curriculum</span></td>

                                                                <td colspan="4" align="left">
                                                                    <asp:Literal ID="ltCLM" runat="server"></asp:Literal></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><span class="field-label">Grade</span></td>

                                                                <td colspan="4" align="left">

                                                                    <asp:Literal ID="ltGrd" runat="server"></asp:Literal></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"><span class="field-label">Section</span></td>

                                                                <td colspan="4" align="left">

                                                                    <asp:Literal ID="ltSct" runat="server"></asp:Literal></td>
                                                            </tr>


                                                            <tr>
                                                                <td align="left"><span class="field-label">Current Status</span></td>

                                                                <td colspan="4" align="left">

                                                                    <asp:Literal ID="ltStatus" runat="server"></asp:Literal></td>
                                                            </tr>



                                                        </table>



                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td align="center" valign="middle">
                                                        <asp:GridView ID="gvStudTPT" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnRowDataBound="gvStudTPT_RowDataBound"
                                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                            PageSize="20" Width="100%">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="Log Date" HeaderStyle-Width="15%">
                                                                    <HeaderTemplate>
                                                                        Log Date<br />
                                                                        <asp:TextBox ID="txtLogDate" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearch_LogDate" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                            OnClick="ImageButton1_Click" />
                                                                    </HeaderTemplate>

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEntry_Date" runat="server" Text='<%# Bind("MRT_ENTRY_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="15%">
                                                                    <HeaderTemplate>
                                                                        Date<br />
                                                                        <asp:TextBox ID="txtInciDate" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearch_InciDate" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                            OnClick="ImageButton1_Click" />
                                                                    </HeaderTemplate>

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblInci_Date" runat="server" Text='<%# Bind("MRT_INCDNT_DATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Log Date" HeaderStyle-Width="32.5%">
                                                                    <HeaderTemplate>
                                                                        Achievement<br />
                                                                        <asp:TextBox ID="txtMerit" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearch_Merit" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                            OnClick="ImageButton1_Click" />
                                                                    </HeaderTemplate>

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("BM_CATEGORYNAME") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddl_Categoryname" runat="server" Visible="false"></asp:DropDownList>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30%"></ItemStyle>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR" HeaderStyle-Width="32.5%">
                                                                    <HeaderTemplate>
                                                                        Remarks<br />
                                                                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                                                                        <asp:ImageButton ID="btnSearch_Remarks" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif"
                                                                            OnClick="ImageButton1_Click" />
                                                                    </HeaderTemplate>

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("MRT_INCDNT_REMARKS") %>'></asp:Label>
                                                                        <asp:TextBox ID="txtStu_REMARKS" runat="server" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="70%"></ItemStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Show In Parentportal">
                                                                    <ItemTemplate>
                                                                        <label class="switch">
                                                                            <asp:CheckBox ID="chk_bshow" runat="server" Checked=' <%# IIf(Eval("bShow").ToString().Equals("Y"), False, Eval("bShow"))%> ' />
                                                                            <span class="slider round"></span>
                                                                        </label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <asp:Button ID="btnEdit" runat="server" OnCommand="btnEdit_Command" Text="Edit" CommandArgument='<%# Bind("MRT_ID")%>' CssClass="button" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Delete">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkdelete" OnCommand="lnkdelete_Command" CommandArgument='<%# Bind("MRT_ID")%>' runat="server" Text="Delete"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Attachements">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hidDocName" Value='<%# Bind("MRG_FILENAME")%>' runat="server" />
                                                                        <asp:ImageButton ID="lnkAttbtn" runat="server" ImageUrl="~/Images/email-attachment.png" Visible="false" />
                                                                        <div id="divUpload" runat="server" visible="false">
                                                                            <asp:UpdatePanel ID="updpnl" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:FileUpload ID="upload" runat="server" />
                                                                                    <asp:Button ID="saveupload" runat="server" Text="Upload" OnClick="saveupload_Click" CssClass="button" />
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="saveupload" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                            <asp:LinkButton ID="lnkShowFile" runat="server" Visible="false" Text=""></asp:LinkButton>
                                                                            <asp:HiddenField ID="hidGrpId" Value='<%# Bind("MRGID")%>' runat="server" />
                                                                        </div>
                                                                        <%--<asp:LinkButton ID="lnkAttDwnldbtn" runat="server"  Visible="false" text ="Download" OnCommand="lnkAttDwnldbtn_Command" CommandArgument='<%# Bind("MRG_FILENAME")%>'></asp:LinkButton>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <HeaderStyle />
                                                            <RowStyle CssClass="griditem" />
                                                            <SelectedRowStyle />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                        </td>
                                    </tr>
                            </table>

                        </td>
                    </tr>
                </table>



                <asp:HiddenField ID="h_selected_menu_1" runat="server" />
                <asp:HiddenField ID="h_selected_menu_2" runat="server" />
                <asp:HiddenField ID="h_selected_menu_3" runat="server" />
                <asp:HiddenField ID="h_selected_menu_4" runat="server" />
                <asp:HiddenField ID="h_selected_menu_5" runat="server" />
                <asp:HiddenField ID="h_selected_menu_6" runat="server" />
                <asp:HiddenField ID="h_selected_menu_7" runat="server" />
                <asp:HiddenField ID="h_print" runat="server" />

            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>


