Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Partial Class Students_StuFollowUp
    Inherits System.Web.UI.Page
    Shared grade As New Dictionary(Of String, String)()
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False

    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            grade.Clear()
            CheckMenuandUser()
            BindAccyear()
            bind_cur_status()
            BindGrid()
        End If
    End Sub
    Public Sub CheckMenuandUser()
        Try
            Dim Encr_decrData As New Encryption64
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100012") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
        Catch ex As Exception
            Response.Redirect("~\noAccess.aspx")
        End Try


    End Sub
    Public Sub BindAccyear()
        Dim acc As New studClass
        Dim clm As String = Session("clm").ToString ''"14" ''
        Dim bsu_id As String = Session("sBsuid").ToString ''"125016" ''
        ddaccyear = acc.PopulateAcademicYear(ddaccyear, clm, bsu_id)
    End Sub
    Public Sub BindGrid()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "select * from enquiry_m m" & _
                                  " inner join PROCESSFO_APPLICANT_S s" & _
                                  " on m.EQM_ENQID =s.PRA_EQM_ENQID " & _
                                  " inner join ENQUIRY_SCHOOLPRIO_S esp" & _
                                  " on esp.EQS_ID = s.PRA_EQS_ID inner join STREAM_M on STREAM_M.STM_ID = esp.EQS_STM_ID inner join SHIFTS_M on SHIFTS_M.SHF_ID =esp.EQS_SHF_ID " _
                                   & " LEFT OUTER JOIN ENQUIRY_FOLLOW_UP_REASON AS FR ON FR.ENR_ENQ_ID=eqs_eqm_enqid  AND FR.ENR_bACTIVE=1" _
                                   & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=FR.ENR_EFR_ID " & _
                                  " inner join GRADE_BSU_M on esp.eqs_grm_id=GRADE_BSU_M.grm_id and  s.PRA_STG_ID ='1' and PRA_bCOMPLETED='true'" & _
                                  " and esp.EQS_ACD_ID = '" & ddaccyear.SelectedValue & " ' and EQS_BSU_ID='" & Session("sBsuid").ToString & "' " 'and esp.EQS_STATUS != 'REG'  and esp.EQS_STATUS != 'DEL' AND  esp.EQS_STATUS != 'ENR' AND esp.EQS_CURRSTATUS<3 "

        If ddlContactMe.SelectedValue <> "ALL" Then
            str_query += " and isnull(EQM_APPL_CONTACTME,'NO')='" & ddlContactMe.SelectedValue & "'"
        End If

        If ddlFollowupStatus.SelectedValue <> "0" Then
            str_query += " AND EFR_ID=" & ddlFollowupStatus.SelectedValue
        End If

        str_query += " order by isNULL(EQS_REGNDATE,eqm_enqdate) desc ,isNULL(EQS_ACCNO,eqs_applno) desc "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        grd_followup.DataSource = ds
        grd_followup.DataBind()
        CheckFollUp()
        BindStream()
        HeaderBindGrade(ds)
    End Sub
    Public Sub BindSearch()
        Dim txtappno As TextBox = grd_followup.HeaderRow.FindControl("txtAppno")
        Dim txtdate As TextBox = grd_followup.HeaderRow.FindControl("txtEnqDate")
        Dim txtappname As TextBox = grd_followup.HeaderRow.FindControl("txtApplName")
        Dim ddgrade As DropDownList = grd_followup.HeaderRow.FindControl("ddgrade")
        Dim ddstream As DropDownList = grd_followup.HeaderRow.FindControl("ddstream")

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strSidsearch As String()
        Dim searchfilter As String = ""

        Dim strSearch As String
        Dim str_query As String = "select * from enquiry_m m" & _
                                  " inner join PROCESSFO_APPLICANT_S s" & _
                                  " on m.EQM_ENQID =s.PRA_EQM_ENQID " & _
                                  " inner join ENQUIRY_SCHOOLPRIO_S esp" & _
                                  " on esp.EQS_ID = s.PRA_EQS_ID inner join STREAM_M on STREAM_M.STM_ID = esp.EQS_STM_ID inner join SHIFTS_M on SHIFTS_M.SHF_ID =esp.EQS_SHF_ID " _
                                   & " LEFT OUTER JOIN ENQUIRY_FOLLOW_UP_REASON AS FR ON FR.ENR_ENQ_ID=eqs_eqm_enqid  AND FR.ENR_bACTIVE=1" _
                                   & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=FR.ENR_EFR_ID " & _
                                  " inner join GRADE_BSU_M on esp.eqs_grm_id=GRADE_BSU_M.grm_id and  s.PRA_STG_ID ='1' and PRA_bCOMPLETED='true'" & _
                                  " and esp.EQS_ACD_ID = '" & ddaccyear.SelectedValue & " ' and EQS_BSU_ID='" & Session("sBsuid").ToString & "'" ' and esp.EQS_STATUS != 'DEL' AND  esp.EQS_STATUS != 'ENR' "

        If ddlContactMe.SelectedValue <> "ALL" Then
            str_query += " and isnull(EQM_APPL_CONTACTME,'NO')='" & ddlContactMe.SelectedValue & "'"
        End If
        If txtappno.Text.Trim() <> "" Then
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If searchfilter <> "" Then
                searchfilter = searchfilter & GetSearchString("convert(varchar,EQS_APPLNO)", txtappno.Text.Trim(), strSearch)
            Else
                searchfilter = searchfilter & GetSearchString1("convert(varchar,EQS_APPLNO)", txtappno.Text.Trim(), strSearch)
            End If

        End If

        If txtdate.Text.Trim() <> "" Then
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            If searchfilter <> "" Then
                searchfilter = searchfilter & GetSearchString("convert(varchar,EQM_ENQDATE,106)", txtdate.Text.Replace("/", " ").Trim(), strSearch)
            Else
                searchfilter = searchfilter & GetSearchString1("convert(varchar,EQM_ENQDATE,106)", txtdate.Text.Replace("/", " ").Trim(), strSearch)

            End If
        End If

        If txtappname.Text.Trim() <> "" Then
            strSidsearch = h_Selected_menu_3.Value.Split("__")
            strSearch = strSidsearch(0)
            If searchfilter <> "" Then
                searchfilter = searchfilter & GetSearchString("ENQ_APPLPASSPORTNAME", txtappname.Text.Trim(), strSearch)
            Else
                searchfilter = searchfilter & GetSearchString1("ENQ_APPLPASSPORTNAME", txtappname.Text.Trim(), strSearch)
            End If
        End If
        If ddgrade.SelectedIndex > 0 Then
            strSearch = ddgrade.SelectedValue
            If searchfilter <> "" Then
                searchfilter = searchfilter & " AND GRADE_BSU_M.GRM_GRD_ID LIKE COALESCE('" & strSearch & "',GRADE_BSU_M.GRM_GRD_ID) "
            Else
                searchfilter = searchfilter & "  GRADE_BSU_M.GRM_GRD_ID LIKE COALESCE('" & strSearch & "',GRADE_BSU_M.GRM_GRD_ID) "
            End If

        End If
        If ddstream.SelectedIndex > 0 Then
            strSearch = ddstream.SelectedValue
            If searchfilter <> "" Then
                searchfilter = searchfilter & " AND STREAM_M.stm_id  LIKE COALESCE(" & strSearch & ",STREAM_M.stm_id ) "
            Else
                searchfilter = searchfilter & "  STREAM_M.stm_id  LIKE COALESCE(" & strSearch & ",STREAM_M.stm_id ) "
            End If
        End If

        If searchfilter <> "" Then
            str_query = str_query & " AND " & searchfilter
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        grd_followup.DataSource = ds
        grd_followup.DataBind()
        Session("searchfollowup") = ds
        CheckFollUp()
        BindStream()
        HeaderBindGrade(ds)
    End Sub
    Public Sub CheckFollUp()
        For Each row As GridViewRow In grd_followup.Rows
            Dim lblReason As Label = DirectCast(row.FindControl("lblReason"), Label)
          
            If DirectCast(row.FindControl("HiddenStatus"), HiddenField).Value = "True" Then
                DirectCast(row.FindControl("Image1"), ImageButton).Enabled = True
                If ((lblReason.Text = "") Or (lblReason.Text = "NULL")) Then
                    DirectCast(row.FindControl("Image1"), ImageButton).ImageUrl = "~\images\tick.gif"
                Else
                    DirectCast(row.FindControl("Image1"), ImageButton).ImageUrl = "~\images\" & lblReason.Text & ""
                End If
            End If
        Next
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  COALESCE('%" & value & "'," + field + ") "
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE COALESCE('%" & value & "'," + field + ") "
            End If
        End If
        Return strFilter
    End Function
    Public Function GetSearchString1(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = "  " + field + " LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "NLI" Then
                strFilter = "   " + field + " NOT LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "SW" Then
                strFilter = "  " + field + "  LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "NSW" Then
                strFilter = "  " + field + "  NOT LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "EW" Then
                strFilter = "  " + field + " LIKE  COALESCE('%" & value & "'," + field + ") "
            ElseIf strSearch = "NEW" Then
                strFilter = "  " + field + " NOT LIKE COALESCE('%" & value & "'," + field + ") "
            End If
        End If
        Return strFilter
    End Function

    Protected Function GetNavigateUrl(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.open('stuFollowUpHistory.aspx?eqs_id={0}', ''); return false; ", pId)
    End Function

    Protected Sub grd_followup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grd_followup.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim lblContactme As Label

            lblContactme = e.Row.FindControl("lblContactme")
            

            If lblContactme IsNot Nothing Then

                If lblContactme.Text = "YES" Then

                    Dim ib As ImageButton = e.Row.Cells(6).Controls(0)

                    ib.ImageUrl = "~\images\tick.gif"

                Else
                    Dim ib As ImageButton = e.Row.Cells(6).Controls(0)
                    ib.ImageUrl = "~\images\cross.gif"
                End If
            End If
        End If
    End Sub
    Protected Sub grd_followup_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles grd_followup.SelectedIndexChanging
        Dim index = grd_followup.SelectedIndex
        Dim eqs_id = DirectCast(grd_followup.Rows(e.NewSelectedIndex).FindControl("Hiddeneqsid"), HiddenField).Value
        Dim Encr_decrData As New Encryption64()
        eqs_id = Encr_decrData.Encrypt(eqs_id)
        Dim enq_id = DirectCast(grd_followup.Rows(e.NewSelectedIndex).FindControl("Hiddenenqid"), HiddenField).Value
        enq_id = Encr_decrData.Encrypt(enq_id)
        Dim accyear = ddaccyear.SelectedItem.Text
        Dim accyearval = ddaccyear.SelectedValue
        Dim grade = DirectCast(grd_followup.Rows(e.NewSelectedIndex).FindControl("lblgrade"), Label).Text
        Dim shift = DirectCast(grd_followup.Rows(e.NewSelectedIndex).FindControl("hiddenshift"), HiddenField).Value
        Dim stream = DirectCast(grd_followup.Rows(e.NewSelectedIndex).FindControl("lblstream"), Label).Text
        Dim editString As String = Encr_decrData.Encrypt(accyear + "|" + accyearval + "|" + grade + "|" + shift + "|" + stream)
        Response.Redirect("~/Students/StuFollowUpDisplay.aspx?eqs_id=" & eqs_id & "&editstring=" & editString & "&enq_id=" & enq_id & "&MainMnu_code=T6V+LqgVMFI=&datamode=Zo4HhpVNpXc=")

    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If grd_followup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = grd_followup.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If grd_followup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = grd_followup.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If grd_followup.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = grd_followup.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Sub BindStream()
        If grd_followup.Rows.Count > 0 Then

            Dim sql_con = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ddstream As DropDownList = grd_followup.HeaderRow.FindControl("ddstream")
            Dim sql_query As String = ""
            Dim ds As DataSet
            sql_query = "select * from STREAM_M "
            ds = SqlHelper.ExecuteDataset(sql_con, CommandType.Text, sql_query)
            ddstream.DataSource = ds
            ddstream.DataValueField = "STM_ID"
            ddstream.DataTextField = "STM_DESCR"
            ddstream.DataBind()
            Dim list As New ListItem
            list.Text = "All"
            list.Value = "-1"
            ddstream.Items.Insert(0, list)
        End If
    End Sub
    Protected Sub ddaccyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddaccyear.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub grd_followup_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grd_followup.RowCommand
        If e.CommandName = "select2" Then
            Response.Write("<Script type='text/javascript'> " & e.CommandArgument & " </Script>")
        End If
    End Sub
    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSearch()
    End Sub
    Protected Sub ddSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSearch()
    End Sub
    Public Sub HeaderBindGrade(ByVal ds As DataSet)
        Try

            Dim ddgrade As DropDownList = grd_followup.HeaderRow.FindControl("ddgrade")
            If grade.Values.Count = 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim i = 0
                    For i = 0 To (ds.Tables(0).Rows.Count - 1)
                        Dim gradeid As String = ds.Tables(0).Rows(i).Item("GRM_GRD_ID").ToString()
                        Dim grade_des As String = ds.Tables(0).Rows(i).Item("GRM_DISPLAY").ToString()
                        If grade.ContainsKey(gradeid) Then
                        Else
                            grade.Add(gradeid, grade_des)
                        End If
                    Next
                    ddgrade.DataSource = grade
                    ddgrade.DataTextField = "Value"
                    ddgrade.DataValueField = "Key"
                    ddgrade.DataBind()

                    Dim list As New ListItem
                    list.Text = "All"
                    list.Value = "-1"
                    ddgrade.Items.Insert(0, list)
                End If
            Else
                ddgrade.DataSource = grade
                ddgrade.DataTextField = "Value"
                ddgrade.DataValueField = "Key"
                ddgrade.DataBind()

                Dim list As New ListItem
                list.Text = "All"
                list.Value = "-1"
                ddgrade.Items.Insert(0, list)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grd_followup_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        grd_followup.PageIndex = e.NewPageIndex

        If Session("searchfollowup") Is Nothing Then
            BindGrid()
        Else
            Dim ds As DataSet = Session("searchfollowup")
            grd_followup.DataSource = ds
            grd_followup.DataBind()
            CheckFollUp()
            BindStream()
            HeaderBindGrade(ds)
        End If
    End Sub

    Protected Sub ddlContactMe_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContactMe.SelectedIndexChanged
        Try
            BindGrid()
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub bind_cur_status()
        ddlFollowupStatus.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFR_ID,EFR_REASON   FROM ENQ_FOLLOW_UP_REASON_M  "



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlFollowupStatus.DataSource = ds
        ddlFollowupStatus.DataTextField = "EFR_REASON"
        ddlFollowupStatus.DataValueField = "EFR_ID"
        ddlFollowupStatus.DataBind()
        ddlFollowupStatus.Items.Add(New ListItem("ALL", "0"))
        ddlFollowupStatus.Items.FindByText("ALL").Selected = True
    End Sub

    Protected Sub ddlFollowupStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFollowupStatus.SelectedIndexChanged
        Try
            BindGrid()
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
End Class
