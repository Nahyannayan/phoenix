<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    MaintainScrollPositionOnPostback="true" CodeFile="studGrade_View.aspx.vb" Inherits="Students_studGrade_View"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Grade Set Up"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--   <tr class="subheader_img">
                        <td align="left" colspan="18" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Grade Set Up&nbsp;</span></font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="15%">
                                        <asp:Label ID="lblAccText" runat="server" Text="Select Academic Year"></asp:Label></td>

                                    <td align="left" width="35%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2" width="50%"></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td colspan="4" align="left">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" BorderStyle="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="grd_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("grm_grd_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Grade">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>
                                                                    Grade<br />
                                                                    <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Shift">
                                                                <HeaderTemplate>
                                                                    Shift<br />
                                                                    <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Stmid" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStmid" runat="server" Text='<%# Bind("stm_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Stream">
                                                                <HeaderTemplate>
                                                                    Stream<br />
                                                                    <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Gender" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGender" runat="server" Text='<%# Bind("gender") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Max. Capacity">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMaxCapacity" runat="server" Text='<%# Bind("grm_maxcapacity") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Open ORP">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMgmQuota" runat="server" Text='<%# Bind("GRM_bOPEN_REG_PAY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Open Online">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOpenOnline" runat="server" Text='<%# Bind("openonline") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Screening Reqd" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblScreenReqd" runat="server" Text='<%# Bind("screenreqd") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="-" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgrmdescr" runat="server" Text='<%# Bind("grm_descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="-" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgrmcert" runat="server" Text='<%# Bind("grm_certificate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="-" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgrmprio" runat="server" Text='<%# Bind("grm_priority") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Open ORP<span style='color:red;'>*</span>" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblORP" runat="server" Text='<%# Bind("GRM_bOPEN_REG_PAY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="view" HeaderText="View" Text="View">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="edit" HeaderText="Edit Stages" Text="Edit Stages">
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="add" HeaderText="Documents" Text="Documents">
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <HeaderStyle  />
                                                        <RowStyle CssClass="griditem" />
                                                        <SelectedRowStyle  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative"  />
                                                        <EmptyDataRowStyle  />
                                                        <EditRowStyle  />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                
            </div>
        </div>
    </div>
</asp:Content>
