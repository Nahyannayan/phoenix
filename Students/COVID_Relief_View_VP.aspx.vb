Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.Services

Partial Class COVID_Relief_View_VP
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Session("sBsuid") = "999998" Then
                    Session("covid_bBsuId") = "0"
                Else
                    Session("covid_bBsuId") = Session("sBsuid")
                End If

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                Session("Current_Page") = "SVP"
                'hardcode the menu code
                If USR_NAME = "" Or Not ((ViewState("MainMnu_code") = "S900052") Or (ViewState("MainMnu_code") = "S900054")) Then
                    'If 1 = 2 Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Call GetDefStatusAndRole()
                    If Session("Def_Status") <> "" And
                    ViewState("COV_USER_ROLE") <> "" Then
                        Call statusDDlbind()
                        Call schoolDDlbind()
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        If ddlStatus.SelectedValue = "10" Or ddlStatus.SelectedValue = "11" Or ddlStatus.SelectedValue = "14" Or ddlStatus.SelectedValue = "15" Then
            checkAll.Attributes.Remove("class")
            checkAll.Attributes.Add("class", "checkAll")
        Else
            checkAll.Attributes.Remove("class")
            checkAll.Attributes.Add("class", "d-none")
        End If
    End Sub


    Public Sub statusDDlbind()
        Try
            Dim dt As DataTable = GetStatusDT()
            ddlStatus.DataSource = dt

            ddlStatus.DataTextField = "CSM_DESCRIPTION"
            ddlStatus.DataValueField = "CMS_CODE"

            ddlStatus.SelectedValue = Session("Def_Status")
            ddlStatus.DataBind()


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@RRH_ID", 0)
            param(1) = New SqlParameter("@IS_CURR_STATUS_REQ", 0)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_RELIEF_STATUS_LIST_SVP")

            ddlStatusPopup.DataSource = ds.Tables(0)
            ddlStatusPopup.DataTextField = "CSM_DESCRIPTION"
            ddlStatusPopup.DataValueField = "CMS_CODE"
            ddlStatusPopup.DataBind()
            'ddlStatusPopup.SelectedValue = Session("Def_Status")

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetStatusDT() As DataTable
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@RRH_ID", 0)
        param(1) = New SqlParameter("@IS_CURR_STATUS_REQ", 0)

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_RELIEF_STATUS_LIST", param)
        Return ds.Tables(0)

    End Function
    Public Sub schoolDDlbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@USR_ID", Session("sUsr_name"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_SCHOOL_LIST", param)
            If ds.Tables(0).Rows.Count > 0 Then

                ddlSchool.DataSource = ds.Tables(0)
                ddlSchool.DataBind()

                ddlSchool.DataSource = ds
                ddlSchool.DataTextField = "BSU_NAME"
                ddlSchool.DataValueField = "BSU_ID"

                ddlSchool.SelectedValue = Session("covid_bBsuId")
                ddlSchool.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Public Sub GetDefStatusAndRole()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@USR_ID", Session("sUsr_name"))
            param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))



            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_DEF_STATUS_AND_ROLE", param)
            If ds.Tables(0).Rows.Count > 0 Then
                Session("Def_Status") = ds.Tables(0).Rows(0)("STATUS").ToString()
                ViewState("COV_USER_ROLE") = ds.Tables(0).Rows(0)("ROLE").ToString()
                ViewState("DISABLE_ACTION_STATUS") = ds.Tables(0).Rows(0)("DISABLE_ACTION_STATUS").ToString()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Public Sub statusOnSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""

        Session("Def_Status") = sender.SelectedItem.Value
        ' Call gridbind()
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "SetDataTable_New();", True)
    End Sub

    Public Sub schoolOnSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        Session("covid_bBsuId") = sender.SelectedItem.Value
        'Call gridbind()
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "SetDataTable_New();", True)
    End Sub

    Sub ShowMsg(ByVal msg As String)
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "SetDataTable_New();alert('" + msg + "');", True)
    End Sub

    Private Shared Function SaveTras(ByVal RRH_ID As String, ByVal RRH_STATUS As String, ByVal RRC_COMMENTS As String) As String
        Dim IsError As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retVal As Int64
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RRH_ID", SqlDbType.Int)
            pParms(0).Value = RRH_ID
            pParms(1) = New SqlClient.SqlParameter("@RRH_STATUS", SqlDbType.VarChar)
            pParms(1).Value = RRH_STATUS
            pParms(2) = New SqlClient.SqlParameter("@RRC_LOG_USR_ID", SqlDbType.VarChar)
            pParms(2).Value = HttpContext.Current.Session("sUsr_name")
            pParms(3) = New SqlClient.SqlParameter("@RRC_COMMENTS", SqlDbType.VarChar)
            pParms(3).Value = RRC_COMMENTS
            pParms(4) = New SqlClient.SqlParameter("@RetVal", SqlDbType.BigInt)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "COV.UPDATE_RELIEF_RER_STATUS", pParms)
            retVal = pParms(4).Value

            If (retVal = 0) Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
                IsError = True
            End If
        Catch ex As Exception
            stTrans.Rollback()
            IsError = True

        Finally
            objConn.Close()
        End Try
        Return IsError
    End Function
    'Protected Sub btnSavePopup_Click(sender As Object, e As EventArgs)
    '    Dim IsError As Boolean = False

    '    Dim rrh_id_list() As String = hdnRRH_ID.Value.Split(",")

    '    For Each rrh_id As String In rrh_id_list
    '        IsError = SaveTras(rrh_id, ddlStatusPopup.SelectedValue, textCommentPopup.Text)
    '        If IsError Then
    '            Exit For
    '        End If
    '    Next

    '    If IsError Then
    '        ShowMsg("Error! Unable to save the data")
    '    Else

    '        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Scriptasasa", "ClosedAll();", True)
    '        btnSave.CssClass = "btn btn-primary d-none"

    '        textCommentPopup.Text = ""
    '        hdnRRH_ID.Value = "0"
    '    End If

    'End Sub

    <WebMethod()>
    Public Shared Function SaveDataRRH(ByVal hdnRRH_ID As String, ByVal ddlStatusPopup As String, ByVal textCommentPopup As String) As String
        Try
            Dim errormsg As String = String.Empty
            Dim IsError As Boolean = False

            Dim rrh_id_list() As String = hdnRRH_ID.Split(",")

            For Each rrh_id As String In rrh_id_list
                IsError = SaveTras(rrh_id, ddlStatusPopup, textCommentPopup)
                If IsError Then
                    Exit For
                End If
            Next


            If IsError Then
                Return "Error"
            Else

                'ScriptManager.RegisterStartupScript(Me, Page.GetType, "Scriptasasa", "ClosedAll();", True)
                'btnSave.CssClass = "btn btn-primary d-none"
                'textCommentPopup.Text = ""
                'hdnRRH_ID.Value = "0"
            End If
            Return "success"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        Return "success"
    End Function

End Class
