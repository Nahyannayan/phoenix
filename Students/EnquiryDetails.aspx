﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EnquiryDetails.aspx.vb" Inherits="Students_NewASPX_EnquiryDetails" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
    <style>
        .title-bg-Form-Head-small.gray ,.title-bg-Form-small.gray {
            background: #dadada;
            color: #121619;
        }
        .checkbox input[type="checkbox"]
        {
            display: inline-block !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="<%= ResolveUrl("~/cssfiles/EnquiryStyle.css")%>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/cssfiles/build.css")%>" rel="stylesheet" />
     <link href="<%= ResolveUrl("~/cssfiles/accordion.css")%>" rel="stylesheet" />
    <div id="LoadImg" style="display: none" class="screenCenter">
        <br />
        <img src="../Images/loading1.gif" align="absmiddle" alt="..." /><br />
        Loading Please wait...
    </div>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <div class="row">


                <div class="col-lg-2 col-md-2 col-sm-12 pr-0">
                    <i class="fa fa-book mr-3" style="margin-right: 5px !important;"></i>
                    Enquiry Details
      
                </div>
                <div class="col-lg-10 col-md-10 col-sm-12  text-right">

                    <asp:LinkButton ID="LbtnShortlistEnquiry" Text="Shortlist Enquiry" CssClass="btn btn-success btn-xs" runat="server" OnClick="LbtnShortlistEnquiry_Click" Enabled="false" />

                    <asp:LinkButton ID="lbtnRegister" Text="Register" CssClass="btn btn-success btn-xs" runat="server" Enabled="false" OnClick="lbtnRegister_Click" />

                    <asp:LinkButton ID="lbtnRegisterSlip_Print" Text="Register Slip" CssClass="btn btn-primary btn-xs" runat="server" Visible="false" OnClick="lbtnRegisterSlip_Print_Click" />

                    <asp:LinkButton ID="lbtnScreening" Text="Screening" CssClass="btn btn-success btn-xs" runat="server" Enabled="false" OnClick="lbtnScreening_Click" />

                    <asp:LinkButton ID="lbtnApproval" Text="Approval" CssClass="btn btn-success btn-xs" runat="server" Enabled="false" OnClick="lbtnApproval_Click" />

                    <asp:LinkButton ID="lbtnOfferLetter" Text="Offer Letter" CssClass="btn btn-success btn-xs" runat="server" Enabled="false" OnClick="lbtnOfferLetter_Click" />

                    <asp:LinkButton ID="lbtnOfferLetter_Print" Text="Offer Letter Print" CssClass="btn btn-primary btn-xs" runat="server" Visible="false" OnClick="lbtnOfferLetter_Print_Click" />

                    <asp:LinkButton ID="lbtnEnrollment" Text="Enrollment" CssClass="btn btn-success btn-xs" runat="server" Enabled="false" OnClick="lbtnEnrollment_Click" />

                    <asp:LinkButton ID="lbtnFollowUp" Text="Follow Up" CssClass="btn btn-primary btn-xs" runat="server" OnClick="lbtnFollowUp_Click" />

                    <asp:LinkButton ID="lbtnDocument" Text="Document" CssClass="btn btn-primary btn-xs text-white" runat="server" OnClick="lbtnDocument_Click" />

                    <a class="btn btn-primary btn-xs" href="<%= ResolveUrl("~/Students/EnquiryDashboard.aspx?MainMnu_code=LjcF1BVVHbk%3d&datamode=Zo4HhpVNpXc%3d")%>">Enquiry Managment</a>
                </div>

            </div>
        </div>

        <div class="card-body">
            <div class="text-center">
              <span class="name_lable">  Student Name </span>
             <span class="name_text">   <asp:Literal ID="ltName" runat="server"></asp:Literal></span>
            </div>
            <div class="container">

                <ul class="step d-flex flex-nowrap p-0 mb-4 mt-4" id="progress_bar_holder">

                    <li class="step-item" id="Li_NEW">
                        <a href="#!" class="">New Enquiry</a>
                    </li>
                    <li class="step-item" id="Li_OPN">
                        <a href="#!" class="">Shortlist Enquiry</a>
                    </li>
                    <li class="step-item" id="Li_REG">
                        <a href="#!" class="">Register</a>
                    </li>
                    <li class="step-item" id="Li_SCR">
                        <a href="#!" class="">Screening</a>
                    </li>
                    <li class="step-item" id="Li_APR">
                        <a href="#!" class="">Approval</a>
                    </li>
                    <li class="step-item" id="Li_OFR">
                        <a href="#!" class="">Offer Letter</a>
                    </li>
                    <li class="step-item" id="Li_ENR">
                        <a href="#!" class="">Enrollment</a>
                    </li>
                </ul>
            </div>
            <div class=" mt-4" runat="server" id="Details_holder">
        <!------ Include the above in your HEAD tag ---------->
        <div class="accordion-container enquiryDetailsAccordion">

            <div class="set">
                <a href="javascript:void(0)" class="active">
                Application Details
              <span class="arrow"></span>
            </a>
                <div class="content" style="display: block;">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 col-12 text-left" style="display: none">
                            <span class="accor-lable">School</span>
                          <span class="text-capitalize">
                                <strong><span id="BsuName" runat="server" class="field-label"></span></strong></span>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12 text-left">
                            <span class="accor-lable">Application no.</span>
                            <span class="accor-text"><asp:Literal ID="ltAppNo" runat="server"></asp:Literal></span>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <span class="accor-lable">Enquiry Date</span>
                            <span class="accor-text"> <asp:Literal ID="ltEnqDate" runat="server"></asp:Literal></span>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <span class="accor-lable">Registration Date </span>
                            <span class="accor-text"><asp:Literal ID="ltRegDate" runat="server"></asp:Literal></span>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <span class="accor-lable">Student Id</span>
                            <span class="accor-text"> <asp:Literal ID="ltStuId" runat="server"></asp:Literal></span>
                        </div>
                        
                    </div>
                    <div class="row mt-3">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Academic Year</span>
                                <span class="accor-text"> <asp:Literal ID="ltAcdYear" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Tentative Joining Date</span>
                                <span class="accor-text"> <asp:Literal ID="ltTenJoinDate" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Grade </span>
                                <span class="accor-text">   <asp:Literal ID="ltGrade" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Shift</span>
                                <span class="accor-text">   <asp:Literal ID="ltShift" runat="server"></asp:Literal></span>
                            </div>
                            
                    </div>
                    <div class="row mt-3">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Curriculum</span>
                                <span class="accor-text"> <asp:Literal ID="ltCurr" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Stream</span>
                                <span class="accor-text"> <asp:Literal ID="ltStream" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Gender </span>
                                <span class="accor-text"> <asp:Literal ID="ltGender" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Religion</span>
                                <span class="accor-text">  <asp:Literal ID="ltReg" runat="server"></asp:Literal></span>
                            </div>        
                    </div>
                    <div class="row mt-3">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Date of Birth</span>
                                <span class="accor-text">  <asp:Literal ID="ltDob" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Place Of Birth</span>
                                <span class="accor-text">  <asp:Literal ID="ltPOB" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Country Of Birth </span>
                                <span class="accor-text"> <asp:Literal ID="ltCOB" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">nationality</span>
                                <span class="accor-text"><asp:Literal ID="ltNat" runat="server"></asp:Literal></span>
                            </div>        
                    </div>
                    <div class="row mt-3">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Emergency Contact No</span>
                                <span class="accor-text"> <asp:Literal ID="ltEmgNo" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Employer</span>
                                <span class="accor-text"> <asp:Literal ID="ltEmployer_ShortEnq" runat="server"></asp:Literal></span>
                            </div>
                                  
                    </div>
    
                </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Passport Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Passport No</span>
                                <span class="accor-text"> <asp:Literal ID="ltPassNo" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Issue Place</span>
                                <span class="accor-text"> <asp:Literal ID="ltIssPlace" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Issue Date </span>
                                <span class="accor-text"> <asp:Literal ID="ltIssDate" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Expiry Date</span>
                                <span class="accor-text"> <asp:Literal ID="ltExpDate" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Visa Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Visa No</span>
                                <span class="accor-text"> <asp:Literal ID="ltVisaNo" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Issue Place</span>
                                <span class="accor-text"> <asp:Literal ID="ltIssPlace_V" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Issue Date </span>
                                <span class="accor-text">  <asp:Literal ID="ltIssDate_V" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Expiry Date</span>
                                <span class="accor-text"> <asp:Literal ID="ltExpr_Date_V" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Issuing Authority</span>
                                        <span class="accor-text"><asp:Literal ID="ltIssAuth" runat="server"></asp:Literal></span>
                                </div>
                        </div>
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Language Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">First Language</span>
                                <span class="accor-text">  <asp:Literal ID="ltFirstLang" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Other Languages</span>
                                <span class="accor-text"> <asp:Literal ID="ltOthLang" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Proficiency in English </span>
                                <span class="accor-text">Reading  :  <asp:Literal ID="ltProEng_R" runat="server"></asp:Literal></span>
                                <span class="accor-text">Writing  :  <asp:Literal ID="ltProEng_W" runat="server"></asp:Literal></span>
                                <span class="accor-text">Speaking : <asp:Literal ID="ltProEng_S" runat="server"></asp:Literal></span>    
                            </div>
                            
                        </div>
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Sibling Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Is any of your child already studying in GEMS School, Please provide one of sibling details</span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-text"><asp:Literal ID="ltSib" runat="server"></asp:Literal></span>
                            </div>
                            
                            
                        </div>
                        <div class="row" id="trSib_1" runat="server">
                            <div class="col-lg-4 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Sibling Name</span>
                                <span class="accor-text"> <asp:Literal ID="ltSibName" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-4 col-sm-6 col-12">
                                <span class="accor-lable"> Sibling Fee ID</span>
                                <span class="accor-text">  <asp:Literal ID="ltSibFeeID" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-4 col-sm-6 col-12">
                                <span class="accor-lable">GEMS School </span>
                                <span class="accor-text"> <asp:Literal ID="ltSibGEMS" runat="server"></asp:Literal></span>
                            </div>
                        
                            
                        </div>
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Primary Contact Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Primary Contact</span>
                                <span class="accor-text">  <asp:Literal ID="ltPri_cont" runat="server"></asp:Literal></span>
                            </div>  
                        </div>
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Father's Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Name</span>
                                <span class="accor-text"><asp:Literal ID="ltFather_name" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">nationality</span>
                                    <span class="accor-text"> <asp:Label ID="ltFnationality" runat="server"></asp:Label></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">nationality 2</span>
                                    <span class="accor-text"> <asp:Literal ID="ltFNat2" runat="server"></asp:Literal></span>
                            </div> 
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Is father of student an employee with GEMS?</span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                        <span class="accor-text"> <asp:Literal  ID="ltF_GEMS" runat="server"></asp:Literal></span>
                                </div>
                               <div class="row" id="trF_GEMS" runat="server">
                            <div class="col-lg-6 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Staff ID</span>
                                <span class="accor-text"><asp:Literal ID="ltFStaffID" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Business unit</span>
                                    <span class="accor-text">  <asp:Literal ID="ltFStaff_BSU" runat="server"></asp:Literal></span>
                            </div>
                          
                        </div>
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                        <span class="accor-lable">Is father of student an Ex-student of GEMS school?</span>
                                        
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12 text-left">     
                                        <span class="accor-text"><asp:Literal ID="ltF_ExGEms" runat="server"></asp:Literal></span>
                                </div> 
                              <div class="row" id="trF_ExGEMS" runat="server">
                            <div class="col-lg-6 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Academic Year</span>
                                <span class="accor-text"><asp:Literal ID="ltFStaff_ACD" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">GEMS School</span>
                                    <span class="accor-text">   <asp:Literal ID="ltFStaff_GEMS_School" runat="server"></asp:Literal></span>
                            </div>
                          
                        </div>
                        </div>
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Permanent Address
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Address Line 1</span>
                                <span class="accor-text"><asp:Literal ID="ltFAdd1" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Address Line 2</span>
                                <span class="accor-text"> <asp:Literal ID="ltFAdd2" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Address City/State </span>
                                <span class="accor-text"><asp:Literal ID="ltFAddCity" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Address Country</span>
                                <span class="accor-text"> <asp:Literal ID="ltFAdd_Country" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Phone</span>
                                    <span class="accor-text"> <asp:Literal ID="ltFAddPhone" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">P.O. Box/Zip Code </span>
                                    <span class="accor-text">  <asp:Literal ID="ltFAdd_Zip" runat="server"></asp:Literal></span>
                                </div>    
                        </div>
                    </div>
            </div>
        
            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Current Address
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Apartment No</span>
                                <span class="accor-text">  <asp:Literal ID="ltFApart" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Building</span>
                                <span class="accor-text"> <asp:Literal ID="ltFBuild" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Street </span>
                                <span class="accor-text">   <asp:Literal ID="ltFStreet" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Area</span>
                                <span class="accor-text"> <asp:Literal ID="ltFArea" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">P.O. Box</span>
                                    <span class="accor-text"> <asp:Label ID="ltFpobox" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">City/Emirate</span>
                                    <span class="accor-text">  <asp:Label ID="ltFemirate" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Country </span>
                                        <span class="accor-text"> <asp:Literal ID="ltFCurr_Contry" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Home Tel No. </span>
                                        <span class="accor-text"><asp:Label ID="ltFhometelphone" runat="server"></asp:Label></span>
                                </div>    
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Office Tel No.</span>
                                    <span class="accor-text"><asp:Label ID="ltFofficetelephone" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">Mobile No</span>
                                    <span class="accor-text">  <asp:Label ID="ltFmobile" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Email </span>
                                        <span class="accor-text">  <asp:Label ID="ltFemail" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Fax No. </span>
                                        <span class="accor-text">  <asp:Label ID="ltFfax" runat="server"></asp:Label></span>
                                </div>    
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Preferred Contact</span>
                                    <span class="accor-text"><asp:Label ID="ltFpreferedcontact" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">Occupation</span>
                                    <span class="accor-text">  <asp:Literal ID="ltFOcc" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Company </span>
                                        <span class="accor-text"> <asp:Literal ID="ltFComp" runat="server"></asp:Literal></span>
                                </div>    
                        </div>
                    </div>
            </div>
                    <%-- -----mother--------%>
            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Mothers's Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Name</span>
                                <span class="accor-text">  <asp:Literal ID="ltMother_name" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">nationality</span>
                                    <span class="accor-text"><asp:Label ID="ltMnationality" runat="server"></asp:Label></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">nationality 2</span>
                                    <span class="accor-text"> <asp:Literal ID="ltMNat2" runat="server"></asp:Literal></span>
                            </div> 
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Is mother of student an employee with GEMS?</span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                        <span class="accor-text">  <asp:Literal ID="ltM_GEMS" runat="server"></asp:Literal></span>
                                </div>
                            <div class="row" id="trM_GEMS" runat="server">
                            <div class="col-lg-6 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Staff ID</span>
                                <span class="accor-text"> <asp:Literal ID="ltMStaffID" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Business unit</span>
                                    <span class="accor-text">  <asp:Literal ID="ltMStaff_BSU" runat="server"></asp:Literal></span>
                            </div>
                          
                        </div>
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                        <span class="accor-lable">Is mother of student an Ex-student of GEMS school?</span>
                                        
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12 text-left">     
                                        <span class="accor-text"><asp:Literal ID="ltM_ExGEms" runat="server"></asp:Literal></span>
                                </div> 
                            <div class="row" id="trM_ExGEMS" runat="server">
                            <div class="col-lg-6 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Academic Year</span>
                                <span class="accor-text">  <asp:Literal ID="ltMStaff_ACD" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">GEMS School</span>
                                    <span class="accor-text">  <asp:Literal ID="ltMStaff_GEMS_School" runat="server"></asp:Literal></span>
                            </div>
                          
                        </div>
                        </div>
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Permanent Address
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Address Line 1</span>
                                <span class="accor-text"> <asp:Literal ID="ltMAdd1" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Address Line 2</span>
                                <span class="accor-text">  <asp:Literal ID="ltMAdd2" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Address City/State </span>
                                <span class="accor-text"> <asp:Literal ID="ltMAddCity" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Address Country</span>
                                <span class="accor-text"><asp:Literal ID="ltMAdd_Country" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Phone</span>
                                    <span class="accor-text">  <asp:Literal ID="ltMAddPhone" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">P.O. Box/Zip Code </span>
                                    <span class="accor-text"> <asp:Literal ID="ltMAdd_Zip" runat="server"></asp:Literal></span>
                                </div>    
                        </div>
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Current Address
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Apartment No</span>
                                <span class="accor-text"> <asp:Literal ID="ltMApart" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Building</span>
                                <span class="accor-text">  <asp:Literal ID="ltMBuild" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Street </span>
                                <span class="accor-text"><asp:Literal ID="ltMStreet" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Area</span>
                                <span class="accor-text"><asp:Literal ID="ltMArea" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">P.O. Box</span>
                                    <span class="accor-text"><asp:Label ID="ltMpobox" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">City/Emirate</span>
                                    <span class="accor-text">  <asp:Label ID="ltMemirate" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Country </span>
                                        <span class="accor-text"><asp:Literal ID="ltMCurr_Contry" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Home Tel No. </span>
                                        <span class="accor-text"> <asp:Label ID="ltMhometelphone" runat="server"></asp:Label></span>
                                </div>    
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Office Tel No.</span>
                                    <span class="accor-text"> <asp:Label ID="ltMofficetelephone" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">Mobile No</span>
                                    <span class="accor-text">  <asp:Label ID="ltMmobile" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Email </span>
                                        <span class="accor-text"><asp:Label ID="ltMemail" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Fax No. </span>
                                        <span class="accor-text"> <asp:Label ID="ltMfax" runat="server"></asp:Label></span>
                                </div>    
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Preferred Contact</span>
                                    <span class="accor-text"> <asp:Label ID="ltMpreferedcontact" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">Occupation</span>
                                    <span class="accor-text"> <asp:Literal ID="ltMOcc" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Company </span>
                                        <span class="accor-text"> <asp:Literal ID="ltMComp" runat="server"></asp:Literal></span>
                                </div>    
                        </div>
                    </div>
            </div>

               <%-- -----Gargyan--------%>
            

            <div class="set" id="trGr1" runat="server">
                    <a href="javascript:void(0)" class="">
                   Guardian's Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Name</span>
                                <span class="accor-text">  <asp:Literal ID="ltGuardian_name" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">nationality</span>
                                    <span class="accor-text"> <asp:Label ID="ltGnationality" runat="server"></asp:Label></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">nationality 2</span>
                                    <span class="accor-text"> <asp:Literal ID="ltGNat2" runat="server"></asp:Literal></span>
                            </div> 
                        </div>
                        
                    </div>
            </div>

            <div class="set" id="trGr2" runat="server">
                    <a href="javascript:void(0)" class="">
                    Permanent Address
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Address Line 1</span>
                                <span class="accor-text"> <asp:Literal ID="ltGAdd1" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Address Line 2</span>
                                <span class="accor-text"> <asp:Literal ID="ltGAdd2" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Address City/State </span>
                                <span class="accor-text">  <asp:Literal ID="ltGAddCity" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Address Country</span>
                                <span class="accor-text"> <asp:Literal ID="ltGAdd_Country" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Phone</span>
                                    <span class="accor-text"> <asp:Literal ID="ltGAddPhone" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">P.O. Box/Zip Code </span>
                                    <span class="accor-text"> <asp:Literal ID="ltGAdd_Zip" runat="server"></asp:Literal></span>
                                </div>    
                        </div>
                    </div>
            </div>

            <div class="set" id="trGr3" runat="server">
                    <a href="javascript:void(0)" class="">
                    Current Address
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Apartment No</span>
                                <span class="accor-text"> <asp:Literal ID="ltGApart" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Building</span>
                                <span class="accor-text"> <asp:Literal ID="ltGBuild" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Street </span>
                                <span class="accor-text"> <asp:Literal ID="ltGStreet" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Area</span>
                                <span class="accor-text"><asp:Literal ID="ltGArea" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">P.O. Box</span>
                                    <span class="accor-text">  <asp:Label ID="ltGpobox" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">City/Emirate</span>
                                    <span class="accor-text">   <asp:Label ID="ltGemirate" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Country </span>
                                        <span class="accor-text">  <asp:Literal ID="ltGCurr_Contry" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Home Tel No. </span>
                                        <span class="accor-text">   <asp:Label ID="ltGhometelphone" runat="server"></asp:Label></span>
                                </div>    
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Office Tel No.</span>
                                    <span class="accor-text"> <asp:Label ID="ltGofficetelephone" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">Mobile No</span>
                                    <span class="accor-text"><asp:Label ID="ltGmobile" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Email </span>
                                        <span class="accor-text">   <asp:Label ID="ltGemail" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Fax No. </span>
                                        <span class="accor-text">  <asp:Label ID="ltGfax" runat="server"></asp:Label></span>
                                </div>    
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Preferred Contact</span>
                                    <span class="accor-text"><asp:Label ID="ltGpreferedcontact" runat="server"></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">Occupation</span>
                                    <span class="accor-text">  <asp:Literal ID="ltGOcc" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Company </span>
                                        <span class="accor-text"> <asp:Literal ID="ltGComp" runat="server"></asp:Literal></span>
                                </div>    
                        </div>
                    </div>
            </div>


         <%--   <div class="set">
                    <a >
                    School Details
               
                </a>
            </div>--%>
            <div class="col-md-12" style="padding: 0px 0px; margin-top: 5px; margin-bottom: 10px;">
                <div class=" title-bg-Form-Head-small gray">
                    <div class="col-md-12" style="padding: 0; margin-top: 5px; font-size: 16px; font-weight: bold;">
                        School Details
                   
                    </div>
                </div>
            </div>
            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Current School Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">School Name</span>
                                <span class="accor-text">   <asp:Literal ID="ltCurr_School" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Student Id</span>
                                <span class="accor-text"><asp:Literal ID="ltCurr_Stu_id" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12" id="trCurrSch_1" runat="server">
                                <span class="accor-lable">Name Of Head Teacher </span>
                                <span class="accor-text"><asp:Literal ID="ltCurr_Head" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12" >
                                <span class="accor-lable">Grade</span>
                                <span class="accor-text">  <asp:Literal ID="ltCurr_Grade" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                        <div class="row mt-3"  id="trCurrSch_2" runat="server">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Curriculum</span>
                                    <span class="accor-text"><asp:Literal ID="ltCurr_Curr" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">Language Of Instruction </span>
                                    <span class="accor-text"> <asp:Literal ID="ltCurr_Instr" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">Address </span>
                                        <span class="accor-text">  <asp:Literal ID="ltCurr_Addr" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">City </span>
                                        <span class="accor-text"> <asp:Literal ID="ltCurr_City" runat="server"></asp:Literal></span>
                                </div>    
                        </div>
                        <div class="row mt-3"  id="trCurrSch_3" runat="server">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Country</span>
                                    <span class="accor-text"> <asp:Literal ID="ltCurr_Country" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">School Phone</span>
                                    <span class="accor-text">  <asp:Literal ID="ltCurr_Sch_ph" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">School Fax </span>
                                        <span class="accor-text"><asp:Literal ID="ltCurr_Sch_fax" runat="server"></asp:Literal></span>
                                </div>
                                   
                        </div>
                        <div class="row mt-3" id="trCurrSch_4" runat="server">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">From Date</span>
                                    <span class="accor-text"> <asp:Literal ID="ltCurr_FromDT" runat="server"></asp:Literal></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">To Date</span>
                                    <span class="accor-text"> <asp:Literal ID="ltCurr_ToDT" runat="server"></asp:Literal></span>
                                </div>
                                   
                        </div>
                    </div>
            </div>

            <div class="set" id="trPrevSCH_id" runat="server">
                    <a href="javascript:void(0)" class="">
                    Previous School Details
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                      

                         
                         <asp:ListView runat="server" ID="gvSchool">
                            <ItemTemplate>
                                 <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">School Name</span>
                                <span class="accor-text">  <asp:Label ID="lblSCH_NAME" runat="server" Text='<%# Bind("SCH_NAME") %>'></asp:Label></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Head Teacher</span>
                                <span class="accor-text"> <asp:Label ID="lblSCH_HEAD" runat="server" Text='<%# Bind("SCH_HEAD") %>'></asp:Label></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Grade </span>
                                <span class="accor-text"> <asp:Label ID="lblSCH_GRADE" runat="server" Text='<%# Bind("SCH_GRADE") %>'></asp:Label></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Curriculum</span>
                                <span class="accor-text">   <asp:Label ID="Label2" runat="server" Text='<%#Bind("CLM_DESCR") %>'></asp:Label></span>
                            </div>
                            
                        </div>
                        <div class="row mt-3">
                                <div class="col-lg-3 col-sm-6 col-12 text-left">
                                    <span class="accor-lable">Language</span>
                                    <span class="accor-text">  <asp:Label ID="Label3" runat="server" Text='<%#Bind("LANG_INST") %>'></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <span class="accor-lable">School Ph. No </span>
                                    <span class="accor-text"><asp:Label ID="Label1" runat="server" Text='<%#Bind("SCH_PHONE") %>'></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">From Date </span>
                                        <span class="accor-text">  <asp:Label ID="lblSCH_FROMDT" runat="server" Text='<%#Bind("SCH_FROMDT") %>'></asp:Label></span>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                        <span class="accor-lable">To Date </span>
                                        <span class="accor-text">  <asp:Label ID="lblSchName" runat="server" Text='<%#Bind("SCH_TODT") %>'></asp:Label></span>
                                </div>    
                        </div>
                            </ItemTemplate>
                             <EmptyDataTemplate >
                                           <div class="row">
                            <div class="col-lg-12 col-sm-12 col-12 text-left">
                                <span class="accor-lable">  No record added yet</span>
                               
                                </div>
                                               </div>
                             </EmptyDataTemplate>
                        </asp:ListView>
                       
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Health Information
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Health Card No/ Medical Insurance No</span>
                                <span class="accor-text"> <asp:Literal ID="ltHth_No" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Blood Group</span>
                                <span class="accor-text">  <asp:Literal ID="ltB_grp" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Health Restrictions
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                                Does your child have any allergies? &nbsp;
                                     <asp:Literal ID="ltAlg_Detail"   runat="server"></asp:Literal>

                            <span id="divAlg" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                                Does your child have any kind of disable?  &nbsp;<asp:Literal ID="ltHthDisabled"
                            runat="server"></asp:Literal>

                            <span id="divHthDisabled" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                                Does your child have any special prescribed medication? &nbsp; <asp:Literal ID="ltSp_med" runat="server"></asp:Literal>
                            <span id="divSp_med" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                                Is there any physical Education Restriction for your child?  &nbsp; <asp:Literal ID="ltPhy_edu" runat="server"></asp:Literal>
                            <span id="divPhy_edu" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                                Any other information related to health issue of your child the school should be aware of? &nbsp;  <asp:Literal ID="ltHth_Info" runat="server"></asp:Literal>
                            <span id="divHth_Info" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                       
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Applicant's disciplinary, social, Physical or psychological Detail
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                          <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               Has child received any sort of learning support or therapy?  &nbsp;  <asp:Literal ID="ltLS_Ther" runat="server"></asp:Literal>
                            <span id="divLS_Ther" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               Does the child require any special education need?  &nbsp;   <asp:Literal ID="ltSEN" runat="server"></asp:Literal>
                            <span id="divSEN" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               Does the student required English support as Additional Language Programme (EAL)?  &nbsp;
                                      <asp:Literal ID="ltEAL" runat="server"></asp:Literal>
                            <span id="divEAL" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               Has your child's behaviour been any cause for concern in previous school?  &nbsp;
                                        <asp:Literal ID="ltPrev_sch" runat="server"></asp:Literal>
                            <span id="divPrev_sch" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                       
                    </div>
            </div>

            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Gifted and talented
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               Has your child ever been selected for specific enrichment activities? &nbsp;
                                       <asp:Literal ID="ltSEA"
                            runat="server"></asp:Literal>
                            <span id="divSEA" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               Is your child musically proficient? &nbsp;    <asp:Literal
                            ID="ltMus_Prof" runat="server"></asp:Literal>
                            <span id="divMus_Prof" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></span>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               Has your child represented School or Country in Sports? &nbsp;
                                       <asp:Literal
                            ID="ltSport" runat="server"></asp:Literal>
                            <div id="divSport" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               Any information related to communication & interaction that the school should be aware of? &nbsp;
                                       <asp:Literal
                            ID="ltHthComm" runat="server"></asp:Literal>
                            <div id="divHthComm" runat="server" style="color: Black; padding-top: 2px; padding-bottom: 2px;"></div>
                                     </span>
                            </div>
                        </div>
                      
                    </div>
            </div>
            <div class="set" id="trTranSch_1" runat="server">
                    <a href="javascript:void(0)" class="">
              Transport Information
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-12 text-left">
                                <span class="accor-lable">Main Location</span>
                                <span class="accor-text">    <asp:Literal ID="ltMain_Loc" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Sub-Location</span>
                                <span class="accor-text">    <asp:Literal ID="ltSub_Loc" runat="server"></asp:Literal></span>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-12">
                                <span class="accor-lable">Pick Up Point</span>
                                <span class="accor-text">     <asp:Literal ID="ltPick_Up" runat="server"></asp:Literal>&nbsp;
                  <asp:Literal ID="ltTransOpt" runat="server"></asp:Literal></span>
                            </div>
                            
                        </div>
                    </div>
            </div>
            <div class="set">
                    <a href="javascript:void(0)" class="">
                    Other Information
                  <span class="arrow"></span>
                </a>
                    <div class="content" style="display: block;">
                         <asp:Repeater ID="rptQus" runat="server">
                                <ItemTemplate>
                                  
                                          <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                                 <%# Eval("QUS")%>&nbsp;<%#Eval("ANS")%><hr style="color: #1b80b6;">
                                     </span>
                            </div>
                        </div>
                                </ItemTemplate>
                            </asp:Repeater>
                  
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               Are there any family circumstances of which you feel we should be aware of?(eg deceased parent/divorce/separated/adopted/others if so please give details)
                                     <br />
                            <asp:Literal ID="ltFamily_detail" runat="server"></asp:Literal>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               The Application is filed by parent?   <asp:Literal
                            ID="ltFilled_by" runat="server"></asp:Literal>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               GEMS may use the above Email Address to send newsletters or any promotional emails. <asp:Literal
                            ID="ltEmail_promo" runat="server"></asp:Literal>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               GEMS may use the above Mobile no to send messages or any alerts. <asp:Literal
                            ID="ltMob_promo" runat="server"></asp:Literal>
                                     </span>
                            </div>
                        </div>
                           <div class="row">
                            <div class="col-lg-12 col-sm-6 col-12">
                                 <span class="accor-lable">
                               GEMS has permission to include child in publication/promotion photos and Videos. <asp:Literal
                            ID="ltPhoto_promo" runat="server"></asp:Literal>
                                     </span>
                            </div>
                        </div>
                       
                    </div>
            </div>
    
            
        </div>
        <!------ Include the above in your HEAD tag ---------->
    </div>
            
            <div class="clearfix"></div>
            <div class="row" runat="server" id="Shortlist_Holder" visible="false">

                <div class="col-md-12">
                    <div class="title-bg-Form-small gray ">
                        Shortlist Enquiry
                               
                    </div>
                    <div class="col-md-12 text-center" runat="server" id="DIV_refresh" visible="false">
                        <asp:LinkButton Text="Refresh" CssClass="btn btn-info btn-sm" runat="server" ID="lbtnRefresh" OnClick="LbtnShortlistEnquiry_Click" />
                    </div>

                </div>
                <div class="col-md-12" style="padding: 0px 20px; margin-top: 5px">
                    <div class=" title-bg-Form-Head-small gray">
                        Possible Duplicate Records
                               
                    </div>
                </div>
                <div class="col-md-12 text-center">

                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">


                        <tr valign="top">
                            <td align="center" colspan="4">
                                <div id="divGrid">

                                    <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row" Width="100%" EmptyDataText="No Data Found" PageSize="15" AllowPaging="True">
                                        <PagerStyle BorderStyle="Solid" />
                                        <HeaderStyle />
                                    </asp:GridView>

                                </div>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="col-md-12 text-center">
                    <asp:LinkButton Text="Confirm" runat="server" ID="btnShortlistConfirm" CssClass="btn btn-success btn-sm" OnClick="btnShortlistConfirm_Click" />
                    <asp:LinkButton Text="Cancel" runat="server" ID="btnShortlistCancel" CssClass="btn btn-outline-success btn-sm" OnClick="btnShortlistCancel_Click" />

                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row" runat="server" id="Register_Holder" visible="false">
                <div class="col-md-12">
                    <div class="title-bg-Form-small gray ">
                        Register
                               
                    </div>
                </div>
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-12">
                            <asp:ValidationSummary ID="vSummary" runat="server" CssClass="error" ForeColor="" ValidationGroup="regdate" />
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Registration Date
                                </label>

                                <div class="Calendar_Holder">
                                    <asp:TextBox ID="txtregdate" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtntxtregdate" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                </div>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                    PopupButtonID="imgBtntxtregdate" TargetControlID="txtregdate">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rfvPass_IssDate" runat="server" ControlToValidate="txtregdate"
                                    Display="Dynamic" ErrorMessage="Registration Date  cannot be left empty"
                                    ValidationGroup="regdate">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtregdate"
                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Registration Date  in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="regdate">*</asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <asp:LinkButton Text="Save" runat="server" ID="RegisterSave" CssClass="btn btn-success btn-sm" OnClick="RegisterSave_Click" ValidationGroup="regdate" />
                            <asp:LinkButton Text="Cancel" runat="server" ID="RegisterCancel" CssClass="btn btn-outline-success btn-sm" OnClick="RegisterCancel_Click" />

                        </div>
                    </div>

                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row" runat="server" id="RegisterSlip_Print_Holder" visible="false">
                <div class="col-md-12">
                    <div class="title-bg-Form-small gray ">
                        Register Slip Print
                    </div>
                </div>
                <div class="col-md-12">
                    <asp:Label ID="lblError_RegisterSlipPrint" runat="server" CssClass="error"></asp:Label>
                </div>

                <div class="col-md-12 text-center">
                    <asp:LinkButton Text="Preview  Register Slip" runat="server" ID="btnPreviewRegisterSlip" CssClass="btn btn-success btn-sm" OnClick="btnPreviewRegisterSlip_Click" />
                    <asp:LinkButton Text="Save  Register Slip As PDF" runat="server" ID="btnRegisterSlipToPDF" CssClass="btn btn-success btn-sm" OnClick="btnRegisterSlipToPDF_Click" />
                    <%--<asp:LinkButton Text="Email Offer Letter" runat="server" ID="LinkButton3" CssClass="btn btn-success btn-sm" OnClick="btnEmailOfferLetter_Click" />--%>
                    <asp:LinkButton Text="Cancel" runat="server" ID="btnCancelPrintRegisterSlip" CssClass="btn btn-outline-success btn-sm" OnClick="btnCancelPrintRegisterSlip_Click" />
                </div>


                <div class="col-md-12 mt-2" id="RegisterSlipHolder">
                    <div class="col-md-12 OfferLetterHTML" runat="server" id="DIV_RegisterSlip" visible="false">
                    </div>
                </div>

               
            </div>
            <div class="clearfix"></div>
            <div class="row" runat="server" id="Screening_Holder" visible="false">
                <div class="col-md-12">
                    <div class="title-bg-Form-small gray ">
                        Screening
                               
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-right" id="tr2_Scr" runat="server" visible="False">
                            <asp:LinkButton ID="lnkDocs_Scr" OnClientClick="javascript:return getDocuments();" runat="server" Visible="False">Pending Documents</asp:LinkButton>
                        </div>
                        <div class="col-md-12" id="trDoc_Scr" runat="server">
                            <div class="row">
                                <div class="col-md-12">
                                    The following documents has to be collected before approval
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            To Collect</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="checkbox-list">
                                                    <asp:ListBox ID="lstNotSubmittedForScreening" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <asp:Button ID="btnRightForScreening" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                    Height="15px" OnClick="btnRightForScreening_Click" Text=">>" /><br />
                                                <asp:Button ID="btnLeftForScreening" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                    Height="15px" OnClick="btnLeftForScreening_Click" Text="<<" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Collected</label>
                                        <div class="checkbox-list">
                                            <asp:ListBox ID="lstSubmittedForScreening" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <asp:Label ID="lblmessageForScreening" runat="server" class="error"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Screening Status
                                        </label>
                                        <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlScreeningDecision" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Reason
                                        </label>
                                        <asp:DropDownList runat="server" ID="ddlScreeningReason" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4" id="tr11_Scr" runat="server">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Screening Test Date
                                        </label>
                                        <div class="Calendar_Holder">
                                            <asp:TextBox ID="txtScrDate" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                            <asp:ImageButton ID="imgBtnScrDate" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </div>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnScrDate" TargetControlID="txtScrDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-md-4" id="tr12_Scr" runat="server">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Screening Test Time
                                        </label>
                                        <telerik:RadTimePicker ID="rdtime" runat="server"></telerik:RadTimePicker>
                                        <br />
                                        <span><span style="font-size: 7pt">
                                            <span style="font-size: 8pt">(hh:mm tt)</span></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" id="tr13_Scr" runat="server">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Additional Details
                                        </label>
                                        <asp:TextBox runat="server" ID="txtDetails" TextMode="MultiLine" SkinID="MultiText_Large" CssClass="form-control inputbox_multi"> </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <asp:LinkButton Text="Save" runat="server" ID="Screening_Save" CssClass="btn btn-success btn-sm" OnClick="Screening_Save_Click" />
                            <asp:LinkButton Text="Cancel" runat="server" ID="Screening_Cancel" CssClass="btn btn-outline-success btn-sm" OnClick="Screening_Cancel_Click" />
                        </div>
                    </div>


                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row" runat="server" id="Approval_Holder" visible="false">
                <div class="col-md-12">
                    <div class="title-bg-Form-small gray ">
                        Approval
                               
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-right" id="tr2_App" runat="server" visible="False">
                            <asp:LinkButton ID="lnkDocs_App" OnClientClick="javascript:return getDocuments();" ForeColor="red" runat="server" Visible="False">Pending Documents</asp:LinkButton>
                        </div>
                        <div class="col-md-12" id="trDocApp" runat="server">
                            <div class="row">
                                <div class="col-md-12">
                                    The following documents has to be collected before approval
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            To Collect</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="checkbox-list">
                                                    <asp:ListBox ID="lstNotSubmittedForApproval" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <asp:Button ID="btnRightForApproval" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                    Height="15px" OnClick="btnRightForApproval_Click" Text=">>" /><br />
                                                <asp:Button ID="btnLeftForApproval" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                    Height="15px" OnClick="btnLeftForApproval_Click" Text="<<" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Collected</label>
                                        <div class="checkbox-list">
                                            <asp:ListBox ID="lstSubmittedForApproval" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <asp:Label ID="lblmessageForApproval" runat="server" class="error"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Application decision
                                        </label>
                                        <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlApprovalDecision" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Reason
                                        </label>
                                        <asp:DropDownList runat="server" ID="ddlApprovalReason" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" runat="server" visible="false">
                            <div class="row">
                                <div class="col-md-4" id="Div4">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Save and Print Offer letter
                                        </label>
                                        <label class="checkbox-inline checkbox checkbox-success">
                                            <asp:CheckBox ID="chkPrint" runat="server" Text="Yes"></asp:CheckBox>
                                        </label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <asp:LinkButton Text="Save" runat="server" ID="Approval_Save" CssClass="btn btn-success btn-sm" OnClick="Approval_Save_Click" />
                            <asp:LinkButton Text="Cancel" runat="server" ID="Approval_Cancel" CssClass="btn btn-outline-success btn-sm" OnClick="Approval_Cancel_Click" />
                        </div>
                    </div>


                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row" runat="server" id="OfferLetter_Holder" visible="false">
                <div class="col-md-12">
                    <div class="title-bg-Form-small gray ">
                        Offer Letter 
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-right" id="tr2_OfferLetter" runat="server" visible="False">
                            <asp:LinkButton ID="lnkDocs_OfferLetter" OnClientClick="javascript:return getDocuments();" ForeColor="red" runat="server" Visible="False">Pending Documents</asp:LinkButton>
                        </div>
                        <div class="col-md-12" id="trDoc_OfferLetter" runat="server">
                            <div class="row">
                                <div class="col-md-12">
                                    The following documents has to be collected before approval
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            To Collect</label>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="checkbox-list">
                                                    <asp:ListBox ID="lstNotSubmitted_OfferLetter" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                </div>

                                            </div>
                                            <div class="col-md-2">
                                                <asp:Button ID="btnRight_OfferLetter" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                    OnClick="btnRight_OfferLetter_Click" Text=">>" /><br />
                                                <asp:Button ID="btnLeft_OfferLetter" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                    OnClick="btnLeft_OfferLetter_Click" Text="<<" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">

                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Collected</label>
                                        <div class="checkbox-list">
                                            <asp:ListBox ID="lstSubmitted_OfferLetter" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 0px 20px; margin-top: 5px">
                            <div class=" title-bg-Form-Head-small gray">
                                Possible Duplicate Records
                           
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr valign="top">
                                    <td align="center">
                                        <asp:GridView ID="GridViewShowDetails_OfferLetter" runat="server" CssClass="table table-bordered table-row" Width="100%" EmptyDataText="No Data Found">
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12 text-center">
                            <p class="textworningOL">
                                You are about to offer admission to this candidate.
                                 <br />
                                Do you want to proceed?
                            </p>
                        </div>
                        <div class="col-md-12 text-center">
                            <asp:LinkButton Text="Confirm" runat="server" ID="btnOfferLetterConfirm" CssClass="btn btn-success btn-sm" OnClick="btnOfferLetterConfirm_Click" />
                            <asp:LinkButton Text="Cancel" runat="server" ID="btnOfferLetterCancel" CssClass="btn btn-outline-success btn-sm" OnClick="btnOfferLetterCancel_Click" />

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row" runat="server" id="Offer_Letter_Printing_Holder" visible="false">
                <div class="col-md-12">
                    <div class="title-bg-Form-small gray ">
                        Offer Letter Print
                    </div>
                </div>
                <div class="col-md-12">
                    <asp:Label ID="lblError_OfferLetterPrint" runat="server" CssClass="error"></asp:Label>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Letter Type
                                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                <asp:DropDownList ID="ddlOFFR_TYPE" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Letter Date
                                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                </label>
                                <div class="Calendar_Holder">
                                    <asp:TextBox ID="txtLDate" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnLDate" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                </div>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                    PopupButtonID="imgBtnLDate" TargetControlID="txtLDate">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ErrorMessage="Letter Date Required" ControlToValidate="txtLDate" runat="server" ValidationGroup="offer_print"  ForeColor="Red" Display="Dynamic"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Date of Join / Start Date
                                    <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                </label>
                                <div class="Calendar_Holder">
                                    <asp:TextBox ID="txtDojOfferLetterPrint" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnDoj_date" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                </div>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                                    PopupButtonID="imgBtnDoj_date" TargetControlID="txtDojOfferLetterPrint">
                                </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator ErrorMessage="Date of Join / Start Date Required" ControlToValidate="txtDojOfferLetterPrint" runat="server" ValidationGroup="offer_print"  ForeColor="Red" Display="Dynamic"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Additional Forms (if any) to be submitted
                                    <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                <asp:TextBox ID="txtNOTE" runat="server" Rows="2" TabIndex="22" TextMode="MultiLine" CssClass="form-control inputbox_multi"
                                    MaxLength="255"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <asp:LinkButton Text="Preview Offer Letter" runat="server" ID="btnPreviewOfferLetter" CssClass="btn btn-success btn-sm" OnClick="btnPreviewOfferLetter_Click" ValidationGroup="offer_print"  />
                    <asp:LinkButton Text="Save Offer Letter As PDF" runat="server" ID="btnOfferLetterToPDF" CssClass="btn btn-success btn-sm" OnClick="btnOfferLetterToPDF_Click" OnInit="btnOfferLetterToPDF_Init" ValidationGroup="offer_print"  />
                    <asp:LinkButton Text="Email Offer Letter" runat="server" ID="btnEmailOfferLetter" CssClass="btn btn-success btn-sm" OnClick="btnEmailOfferLetter_Click" ValidationGroup="offer_print"  />
                    <asp:LinkButton Text="Cancel" runat="server" ID="btnCancelPrintOfferLetter" CssClass="btn btn-outline-success btn-sm" OnClick="btnCancelPrintOfferLetter_Click"   />
                    <label class="checkbox-inline checkbox likebtn checkbox-success" >
                        <asp:CheckBox ID="chkEmailCopy" runat="server" Text="Copy to Registrar"></asp:CheckBox>
                    </label>
                </div>


                <div class="col-md-12 mt-2" id="OfferLetterHolder">
                    <div class="col-md-12 OfferLetterHTML" runat="server" id="DIV_OfferLetter" visible="false">
                    </div>
                </div>

                <div class="col-md-12 mt-2" id="TermsAndConditionsHolder">
                    <div class="col-md-12 OfferLetterHTML" runat="server" id="DIV_TermsAndConditions" visible="false">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row" runat="server" id="Enrollment_Holder" visible="false">
                <div class="col-md-12">
                    <div class="title-bg-Form-small gray ">
                        Enrollment 
                    </div>
                </div>
                <div class="col-md-12">
                    <asp:Label ID="lblError_Enrollment" runat="server" CssClass="error"></asp:Label>
                </div>
                <div class="col-md-12">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                        ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                        ValidationGroup="groupM1Enrollment" Style="display: inline" />
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Select Academic Year
                                </label>
                                <asp:DropDownList ID="ddlAcademicYear" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3" runat="server" id="Payment_Plan_DIV">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Payment Plan
                                </label>
                                <asp:DropDownList ID="ddlPayPlan" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Date of Joining
                                </label>
                                <div class="Calendar_Holder">
                                    <asp:TextBox ID="txtDoj" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy" onchange="MoveText();return false;"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnEnqDate" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                </div>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MMM/yyyy"
                                    PopupButtonID="imgBtnEnqDate" TargetControlID="txtDoj">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDoj"
                                    Display="Dynamic" ErrorMessage="Enter the Joining Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="groupM1Enrollment">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtDoj"
                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Date of join entered is not a valid date"
                                        ForeColor="" ValidationGroup="groupM1Enrollment">*</asp:CustomValidator>

                                <asp:RequiredFieldValidator ID="rfDoj" runat="server" ErrorMessage="Please enter the field Date of join" ControlToValidate="txtDoj" Display="None" ValidationGroup="groupM1Enrollment"></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Ministry Date of Joining
                                </label>
                                <div class="Calendar_Holder">
                                    <asp:TextBox ID="txtMDoj" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                </div>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MMM/yyyy"
                                    PopupButtonID="ImageButton1" TargetControlID="txtMDoj">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMDoj"
                                    Display="Dynamic" ErrorMessage="Enter the Ministry joining Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="groupM1Enrollment">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtMDoj"
                                        CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Ministry date of join entered is not a valid date"
                                        ForeColor="" ValidationGroup="groupM1Enrollment">*</asp:CustomValidator>
                                <asp:RequiredFieldValidator ID="rfMdoj" runat="server" ErrorMessage="Please enter the field Ministry  Date of join" ControlToValidate="txtMDoj" Display="None" ValidationGroup="groupM1Enrollment"></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Transfer Type
                                </label>
                                <asp:DropDownList ID="ddlTranType" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Previous School Result
                                </label>
                                <asp:DropDownList ID="ddlJoinResult" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="NEW">NEW</asp:ListItem>
                                    <asp:ListItem Value="PASS">PASS</asp:ListItem>
                                    <asp:ListItem Value="FAIL">FAIL</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <asp:LinkButton Text="Enroll" runat="server" ID="btnSaveEnroll" CssClass="btn btn-success btn-sm" OnClick="btnSaveEnroll_Click" ValidationGroup="groupM1Enrollment" />

                    <asp:LinkButton Text="Cancel" runat="server" ID="btnCancelEnroll" CssClass="btn btn-outline-success btn-sm" OnClick="btnCancelEnroll_Click" />

                </div>
            </div>
            <div class="row" runat="server" id="FollowUp_Holder" visible="false">
                <div class="col-md-12">
                    <div class="title-bg-Form-small gray mb-1">
                        Follow Up
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class=" title-bg-Form-Head-small gray">
                                Enquiry Details 
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Enquiry No
                                </label>
                                <asp:Label ID="txtEnqNo" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Academic Year
                                </label>
                                <asp:Label ID="txtAcadYear" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Grade
                                </label>
                                <asp:Label ID="txtGrade" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Shift
                                </label>
                                <asp:Label ID="txtShift" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Stream
                                </label>
                                <asp:Label ID="txtStream" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Enquiry Date
                                </label>
                                <asp:Label ID="txtenqdate" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Mobile
                                </label>
                                <asp:Label ID="txtmobile" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Applicant Name
                                </label>
                                <asp:Label ID="txtappname" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Email
                                </label>
                                <asp:Label ID="txtemail" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Primary Contact
                                </label>
                                <asp:Label ID="txtprimarycaontact" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" Visible="false"></asp:Label>
                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="col-md-12">
                            <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                                EnableViewState="False" ForeColor="" ValidationGroup="groupM1" />
                            <span style="color: #c00000"></span>
                        </div>
                        <div class="col-md-12">
                            <div class=" title-bg-Form-Head-small gray">
                                Follow Up Form
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Current Status
                                </label>
                                <asp:DropDownList ID="ddlCurStatus" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Status Reason
                                </label>
                                <asp:DropDownList ID="ddlReason" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Remarks
                                </label>
                                <asp:TextBox ID="txtReasonRemark" runat="server" MaxLength="300"
                                    TextMode="MultiLine" CssClass="form-control inputbox_multi"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr />
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Mode
                                </label>
                                <asp:DropDownList ID="ddmode" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="" Selected="true" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Telephone" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Email" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">You Want Set Alert ?</label>
                                <div class="RB_Holder">
                                    <label class="checkbox-inline checkbox checkbox-success">
                                        <asp:CheckBox ID="chbSetAlert" runat="server" Text="Yes" />

                                    </label>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix col-md-12"></div>
                            <div class="col-md-12">
                        <div class="row" id="DivAlertHolder" style="width: 100%; display: none">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="Ftitle">
                                        Alert On
                                    </label>
                                    <div class="Calendar_Holder">
                                        <asp:TextBox ID="txtAob" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                        <asp:ImageButton ID="imgDOB" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                    </div>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender7" TargetControlID="txtAOB" runat="server" PopupButtonID="imgDOB" Format="dd/MMM/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtAob"
                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Last Rejoining  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                        ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="Ftitle">
                                        Remarks
                                    </label>
                                    <asp:TextBox ID="txtremarks" runat="server" MaxLength="300"
                                        TextMode="MultiLine" CssClass="form-control inputbox_multi"></asp:TextBox>
                                 
                                </div>
                            </div>

                        </div>
                                </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class=" title-bg-Form-Head-small gray">
                                Follow Ups
                            </div>
                        </div>
                        <div class="col-md-12">
                            <asp:GridView ID="grdReason" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="Log Date">
                                        <ItemTemplate>
                                            <%#Eval("ENR_LOG_DATE", "{0:dd/MMM/yyyy}")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Current Status">
                                        <ItemTemplate>
                                            <%#Eval("EFR_REASON")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason">
                                        <ItemTemplate>
                                            <%#Eval("EFD_REASON")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <%#Eval("ENR_REMARKS")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mode">
                                        <ItemTemplate>
                                            <%#Eval("MODE_DESCR")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User">
                                        <ItemTemplate>
                                            <%#Eval("ENR_USER_ID")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                                <HeaderStyle CssClass="gridheader_pop" />
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="Green" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-12">
                            <div class=" title-bg-Form-Head-small gray">
                                Tasks
                            </div>
                        </div>
                        <div class="col-md-12">
                            <asp:GridView ID="grdFollowuphis" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="table table-bordered table-row">
                                <Columns>
                                    <asp:TemplateField HeaderText="Log Date">
                                        <ItemTemplate>
                                            <%#Eval("FOL_FOLL_UP_DATE", "{0:dd/MMM/yyyy}")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mode">
                                        <ItemTemplate>
                                            <%#Eval("MODE_DESCR")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <%#Eval("FOL_REMARKS")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Alert Date">
                                        <ItemTemplate>
                                            <%#Eval("alertdate")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="User">
                                        <ItemTemplate>
                                            <%#Eval("FOL_USER_ID")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                                <HeaderStyle CssClass="gridheader_pop" />
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="Green" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </div>

                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <asp:LinkButton Text="Save" runat="server" ID="lbtnSaveFollowUp" CssClass="btn btn-success btn-sm" OnClick="lbtnSaveFollowUp_Click" ValidationGroup="groupM1" />

                    <asp:LinkButton Text="Cancel" runat="server" ID="lbtnCancelFollowUp" CssClass="btn btn-outline-success btn-sm" OnClick="lbtnCancelFollowUp_Click" />

                </div>
            </div>
            <div class="row" runat="server" id="Document_Holder" visible="false">
                <div class="col-md-12">
                    <div class="title-bg-Form-small gray ">
                        Uploading  Document
                               
                    </div>
                </div>
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-12">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" ForeColor="" ValidationGroup="regdate" />
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">Status</label>
                                <asp:DropDownList ID="ddlEnqStatus" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEnqStatus_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Document Description
                                </label>
                                <asp:DropDownList runat="server" ID="ddlDocument" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Upload Document
                                </label>
                                <div class="upload-btn-wrapper">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <button class="btn btn-sm upload-btn">Browse Files</button>
                                        </div>
                                        <div class="col-md-4"><span class="file-info">.....</span></div>
                                    </div>

                                    <asp:FileUpload runat="server" ID="FileUpload_stud" />
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <label class="Ftitle">
                                    --
                                </label>
                                <asp:LinkButton Text="Save" runat="server" ID="DocumentSave" CssClass="btn btn-success btn-sm" OnClick="DocumentSave_Click" />
                                <asp:LinkButton Text="Cancel" runat="server" ID="DocumentCancel" CssClass="btn btn-outline-success btn-sm" OnClick="DocumentCancel_Click" />
                            </div>
                        </div>
                    </div>

                </div>


                <asp:ListView runat="server" ID="Repeater_Stage" OnItemDataBound="Repeater_Stage_ItemDataBound">
                    <ItemTemplate>
                        <div class="col-md-12">
                            <div class="title-bg-Form-small gray ">
                                <%--   <%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "FCL_DATE")).ToString("dd / MMMM / yyyy") %>--%>
                                <%# DataBinder.Eval(Container.DataItem, "PRO_DESCRIPTION") %>
                            </div>
                            <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "PRB_STG_ID") %>' runat="server" ID="lblStgId" Visible="false" />
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <asp:ListView runat="server" ID="Repeater_Stage_Document" OnItemDataBound="Repeater_Stage_Document_ItemDataBound">
                                    <ItemTemplate>
                                        <%-- <label style="display:none" >
                                        <%#  collected = Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "collected")) %>
                                        </label>--%>
                                        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "collected") %>' runat="server" ID="lblcollected" Visible="false" />

                                        <div class="col-md-4 mb-1 Doc_details_contaner" runat="server" id="upload_div">
                                            <label class="lbl_Doc">
                                                <%# If(IsDBNull(DataBinder.Eval(Container.DataItem, "FILE_NAME")), "<i class='fa fa-file-o i-style-ex'></i>", GetExtensionIcon(DataBinder.Eval(Container.DataItem, "FILE_NAME"))) %>

                                                <%# DataBinder.Eval(Container.DataItem, "DOC_DESCR") %>
                                                <i class="fa fa-check-square i-style green"></i>
                                            </label>
                                            <div class="Doc_details_Holder">
                                                <div class="row ">
                                                    <div class="col-md-4"><%# If (IsDbNull( DataBinder.Eval(Container.DataItem, "UPLOAD_DATE")), "00/00/0000", Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "UPLOAD_DATE")).ToString("dd/MMMM /yyyy")) %></div>
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-4  text-right"><a href='<%# DataBinder.Eval(Container.DataItem, "File_URL") %>' class="Doc_View_a" target="_blank">View</a></div>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-md-4 mb-1 Doc_details_contaner" runat="server" id="Pending_div">
                                            <label class="lbl_Doc">
                                                <%# If(IsDBNull(DataBinder.Eval(Container.DataItem, "FILE_NAME")), "<i class='fa fa-file-o i-style-ex'></i>", GetExtensionIcon(DataBinder.Eval(Container.DataItem, "FILE_NAME"))) %>

                                                <%# DataBinder.Eval(Container.DataItem, "DOC_DESCR") %>
                                                <i class="fa fa-times-rectangle i-style red"></i>
                                            </label>
                                            <div class="Doc_details_Holder">
                                                <div class="row ">
                                                    <div class="col-md-4">Day/Month/Year</div>
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-4  text-right"><a class="Doc_View_a disable">View</a></div>

                                                </div>
                                            </div>

                                        </div>

                                    </ItemTemplate>

                                    <EmptyDataTemplate>
                                        <label class="col-md-12 text-center">
                                            there is no Documents

                                        </label>
                                    </EmptyDataTemplate>
                                </asp:ListView>




                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <div class="row" runat="server" id="Div_EnrollmentDone" visible="false">
                <div class="col-md-12 text-center">
                    <p class="textworningOL">
                        Enrollment Successfully Completed. 
                           
                    </p>
                    <p>
                        <a runat="server" id="A_Student" visible="false">Please click here to go to Student Profile</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfENQ_ID" runat="server" />
    <%--    <asp:HiddenField ID="hfEQS_ID" runat="server" />--%>
    <asp:HiddenField ID="HiddenBsuId" runat="server" />
    <asp:HiddenField ID="HiddenAcademicyear" runat="server" />
    <asp:HiddenField runat="server" ID="HFACCNO" />
    <asp:HiddenField runat="server" ID="HFstuEqs" />
    <asp:HiddenField runat="server" ID="HFEQS_ACD" />
    <asp:HiddenField runat="server" ID="HFSTATUS" />
    <asp:HiddenField runat="server" ID="HFSTGCount" />
    <asp:HiddenField ID="hfURL" runat="server" />
    <asp:HiddenField ID="HiddenEQSID" runat="server" />
    <asp:HiddenField ID="HFEQM_ENQID" runat="server" />
    <asp:HiddenField ID="HFREGNDATE" runat="server" />
    <asp:HiddenField ID="hdnValue" runat="server" />
    <asp:HiddenField ID="HFStageID" runat="server" />
    <asp:HiddenField ID="HFstuIds" runat="server" />
    <asp:HiddenField ID="HFLinkRegFee" runat="server" />
    <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    <script>
        function progress_bar_active(id) {
            var listItems = $("#progress_bar_holder li");
            var flag = false;
            listItems.each(function (idx, li) {
                $(li).removeClass("active");
                if ($(li).attr('id') == 'Li_' + id) {
                    $(li).addClass("active");
                    flag = true;
                }
                // and the rest of your code
            });
            if (flag == false) {
                listItems.each(function (idx, li) {
                    $(li).addClass("active");
                });
            }
            if ($('#' +'<%=HFSTGCount.ClientID %>').val() === '5') {
                $("#Li_SCR").hide();
                $("#Li_APR").hide();
            }
            //jQuery("html,body").animate({ scrollTop: 0 }, 1000);
        }
    </script>
    <script>
        function MoveText() {

            document.getElementById('<%=txtMDoj.ClientID %>').value = document.getElementById('<%=txtDoj.ClientID %>').value;
            return false;
        }

        function confirm_enroll() {

            if (confirm("You are about to enroll these selected enquiries.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }

    </script>
    <script type="text/javascript">
        function Confirm_test() {

            if (confirm("Are you sure you want to Delete the enquiry ?")) {
                document.getElementById('<%= hdnValue.ClientID%>').value = "Yes";
            } else {
                document.getElementById('<%= hdnValue.ClientID%>').value = "No";
            }

        }
        function redirect_test(val) {

            window.location.href = val;

        }
    </script>
    <script>
        function OfferLetterPriview() {
            $('#OfferLetterHolder div table tbody tr td table tr:first-child').remove();
            $('#OfferLetterHolder div table').css('width', '100%');
            jQuery("html,body").animate({ scrollTop: 300 }, 1000);
        }

        function Check_Alert() {
            if (document.getElementById('ctl00_cphMasterpage_chbSetAlert').checked == true) {

                $("#DivAlertHolder").show();

            } else {
                $("#DivAlertHolder").hide();
            };
        }
        //$(document).ready(function () {
        //    Check_Alert()

        //});
        $('#ctl00_cphMasterpage_chbSetAlert').change(function () {
            Check_Alert();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#ctl00_cphMasterpage_chbSetAlert').change(function () {
                        Check_Alert();
                    });
                    try {
                        Check_Alert();
                    } catch (e) {

                    }
                }
            });
        };
    </script>
    <script>
         $(".set > a").on("click", function() {
            $(this).parent().toggleClass("hide");
            $(this).siblings(".content").slideToggle();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {

                    try {
                         $(".set > a").on("click", function() {
            $(this).parent().toggleClass("hide");
            $(this).siblings(".content").slideToggle();
        });
                    } catch (e) {

                    }
                }
            });
        };
    </script>
</asp:Content>

