﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.IO
Partial Class Students_NewASPX_EnquiryDashboard
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Session("BSU_ENQ_SELECT_MANAGE") = "0"
            Session("CONTACTME_SELECT2") = "ALL"
            Session("PAID_SELECT2") = "ALL"
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    'code modified by lijo
                    ddl_EnquiryCondition.SelectedValue = "Open"
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    If Session("ACD_SELECT") IsNot Nothing Then
                        If ddlAcademicYear.Items.FindByValue(Session("ACD_SELECT")) IsNot Nothing Then
                            ddlAcademicYear.ClearSelection()
                            '.Items.FindByValue(Session("ACD_SELECT")).Selected = True
                            ddlAcademicYear.SelectedValue = Session("ACD_SELECT")
                        End If
                    End If

                    bindPrevSchools()
                    Session("sEnqStatus") = 2
                    ddlEnqStatus.SelectedItem.Value = 2
                    'For Each item As ListItem In ddlEnqStatus.Items
                    '    If item.Value = 2 Then
                    '        item.Selected = True
                    '    End If
                    'Next





                    'Dim cb As New CheckBox
                    'For Each gvr As GridViewRow In gvStudEnquiry.Rows
                    '    cb = gvr.FindControl("chkSelect")
                    '    ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    'Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_12.Value = "LI__../Images/operations/like.gif"

                    h_Selected_menu_13.Value = "LI__../Images/operations/like.gif"
                    Dim pParms(3) As SqlClient.SqlParameter
                    Dim lstrPWD As String = String.Empty
                    Dim lstrSelGrade As String = String.Empty


                    pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBsuid"))
                    pParms(1) = New SqlClient.SqlParameter("@FORM_CODE", ViewState("MainMnu_code"))
                    Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_FORM_SORT", pParms)
                        While reader.Read
                            Session("SORTORDER") = Convert.ToString(reader("SORT_TYPE"))
                        End While
                    End Using


                    bind_cur_status()
                    BIND_BSU_DETAILS()
                    GridBind()
                    ShowAccNo()
                    get_Del_visible()
                    If ViewState("isDELPERMIT") = 0 Then
                        btnReject.Visible = False
                    End If

                End If
            Catch ex As Exception
                ' lblError.Text = "Request could not be processed"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else
            highlight_grid()
        End If
    End Sub
    Sub bindPrevSchools()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_CORP")


            ddlPrevSchool_BSU.Items.Clear()
            ddlPrevSchool_BSU.DataSource = ds.Tables(0)
            ddlPrevSchool_BSU.DataTextField = "BSU_NAME"
            ddlPrevSchool_BSU.DataValueField = "BSU_ID"
            ddlPrevSchool_BSU.DataBind()
            ddlPrevSchool_BSU.Items.Add(New ListItem("ALL", "0"))
            ddlPrevSchool_BSU.Items.Add(New ListItem("ALL GEMS", "1"))
            ddlPrevSchool_BSU.Items.Add(New ListItem("ALL NON GEMS", "2"))

            ddlPrevSchool_BSU.ClearSelection()
            If Not ddlPrevSchool_BSU.Items.FindByText("ALL") Is Nothing Then
                ddlPrevSchool_BSU.Items.FindByText("ALL").Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bind_cur_status()
        ddlFollowupStatus.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFR_ID,EFR_REASON   FROM ENQ_FOLLOW_UP_REASON_M  "



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlFollowupStatus.DataSource = ds
        ddlFollowupStatus.DataTextField = "EFR_REASON"
        ddlFollowupStatus.DataValueField = "EFR_ID"
        ddlFollowupStatus.DataBind()
        ddlFollowupStatus.Items.Add(New ListItem("ALL", "0"))
        ddlFollowupStatus.Items.FindByText("ALL").Selected = True
    End Sub
    Private Sub BIND_BSU_DETAILS()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter

        param(0) = New SqlParameter("@BSU_ID", Session("sbsuid"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ENQ.GET_EMAIL_RESEND_BSU", param)
            While datareader.Read
                ViewState("ACK1") = Convert.ToString(datareader("ERT_ACK1"))
                ViewState("ACK2") = Convert.ToString(datareader("ERT_ACK2"))


            End While
        End Using
    End Sub
    Sub ShowAccNo()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT isnull(ACD_bGENFEEID,'true') FROM ACADEMICYEAR_D WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        'Dim bShow As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'If bShow = False Then
        '    gvStudEnquiry.Columns(3).Visible = True
        'Else
        '    gvStudEnquiry.Columns(3).Visible = True
        'End If
    End Sub
    Private Sub get_Del_visible()
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        pParms(1) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "ENQ.GET_NOT_DEL_RIGHTUSERS", pParms)
        If ds.Tables(0).Rows.Count >= 1 Then
            ViewState("isDELPERMIT") = ds.Tables(0).Rows(0).Item("PERMITS")
        End If

    End Sub
    Sub highlight_grid()
        'For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1
        '    Dim row As GridViewRow = gvStudEnquiry.Rows(i)
        '    Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
        '    If isSelect Then
        '        row.BackColor = Drawing.Color.FromName("#f6deb2")
        '    Else
        '        row.BackColor = Drawing.Color.Transparent
        '    End If
        'Next
    End Sub

    Private Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = String.Empty
            Dim lstrMale = 0
            Dim lstrFemale = 0
            Dim lstrTotal = 0
            Dim lstrTop10 As String

            If gvStudEnquiry.Rows.Count > 0 Then
                If Session("ACD_SELECT_CLEAR") <> "ACD" Then
                    lstrTop10 = ""
                    Session("ACD_SELECT_CLEAR") = ""
                Else
                    lstrTop10 = "Top 10"
                    Session("ACD_SELECT_CLEAR") = ""
                End If
            Else
                lstrTop10 = "Top 10"
                Session("ACD_SELECT_CLEAR") = ""
            End If

            'If ViewState("ACK1") Then
            '    gvStudEnquiry.Columns(57).Visible = True
            'Else
            '    gvStudEnquiry.Columns(57).Visible = False
            'End If


            'If ViewState("ACK2") Then
            '    gvStudEnquiry.Columns(58).Visible = True
            'Else
            '    gvStudEnquiry.Columns(58).Visible = False
            'End If

            If ddl_EnquiryCondition.SelectedValue = "Open" Then
                'gvStudEnquiry.Columns(56).Visible = False
                str_query = "SELECT " & lstrTop10 & " row_number()OVER(order by isNULL(EQS_REGNDATE,eqm_enqdate) DESC ,isNULL(EQS_ACCNO,b.eqs_applno) DESC) AS RowNumber,eqs_id,eqs_applno,eqm_enqid,eqm_enqdate,grm_display,shf_descr,stm_descr,isnull(EFR_ICONS,'')EFR_ICONS,EFR_REASON, " _
                                                   & " appl_name=isnull(eqm_applfirstname,'') + ' '+isnull(eqm_applmidname,'') + ' ' " _
                                                   & " +isnull(eqm_appllastname,''),F.*,G.*,eqs_applno,EQS_CURRSTATUS,isnull(EQS_DOC_STG_ID,0) as  EQS_DOC_STG_ID ," _
                                                   & " ISNULL(EQS_APL_DISABLE,'FALSE') AS EQS_APL_DISABLE,B.EQS_APL_ID as APL_ID, B.EQS_STATUS as EQS_STATUS," _
                                                   & " case  when (ISNULL(B.EQS_APL_DISABLE, 'FALSE')='FALSE') AND(B.EQS_STATUS='OFR') THEN 'TRUE' ELSE 'FALSE' END AS FLAG ,isnull(Eqs_AccNo,'') as Eqs_AccNo,isnull(B.EQS_bFOLLOWUP,0) as EQS_bFOLLOWUP,EQS_REGNDATE,EQM_APPLGENDER as GENDER,APL_IMAGE,APL_TYPE,CASE when isnull(EQS_bSEN,0)=1  THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END  as EQS_bSEN ,case when rtrim(ltrim(isNULL(EQS_SEN,'')))='' then 0 else 1 end as bSEN, EQS_SEN,EQM_bSTAFFGEMS,EQM_bEXSTUDENT,EQM_bAPPLSIBLING, DBO.GETCAL_AGE( EQM_APPLDOB,ACD_AGE_CUTOFF)  	as Appl_Age,'Age on '+ replace(convert(varchar(12),ACD_AGE_CUTOFF,106),' ','/') as CUTOFF_DT ,isNULL(PREVSCHOOL.BSU_SHORTNAME,'')  as Prev_School_GEMS  FROM enquiry_m A WITH (NOLOCK)" _
                                                   & " INNER JOIN enquiry_schoolprio_s B WITH (NOLOCK) ON A.eqm_enqid=B.eqs_eqm_enqid " _
                                                   & " INNER JOIN vw_OSO_STUDAPPLSTAGES F ON B.EQS_ID=F.PRA_EQS_ID " _
                                                   & " INNER JOIN vw_OSO_STUDSTAGEORDER G ON B.EQS_ID=G.PRA_EQS_ID " _
                                                   & " INNER JOIN grade_bsu_m C WITH (NOLOCK) ON B.eqs_grm_id=C.grm_id " _
                                                   & " INNER JOIN shifts_m D WITH (NOLOCK) ON B.eqs_shf_id=D.shf_id" _
                                                   & " INNER JOIN stream_m  E WITH (NOLOCK) ON B.eqs_stm_id=E.stm_id " _
                                                   & " INNER JOIN ACADEMICYEAR_D AS ACD WITH (NOLOCK) ON B.EQS_ACD_ID = ACD.ACD_ID " _
                                                & " INNER JOIN vw_ENQ_SIBLINGS P ON B.EQS_ID=P.EQS " _
                                                 & " LEFT JOIN ApplicationDecision_M APPL ON B.EQS_APL_ID=APPL.APL_ID " _
                                                  & " LEFT JOIN BUSINESSUNIT_M  AS PREVSCHOOL ON A.EQM_PREVSCHOOL_BSU_ID = PREVSCHOOL.BSU_ID " _
                                                  & " LEFT OUTER JOIN ENQUIRY_FOLLOW_UP_REASON AS FR ON FR.ENR_EQS_ID=B.eqs_id AND FR.ENR_bACTIVE=1  " _
                                                  & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=FR.ENR_EFR_ID " _
                                                 & " WHERE  B.eqs_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                                   & " AND B.eqs_bsu_id='" + Session("sbsuid").ToString + "' AND EQS_CANCELDATE IS NULL AND EQS_STATUS<>'DEL' AND EQS_STATUS<>'ENR' "

            ElseIf ddl_EnquiryCondition.SelectedValue = "Cancelled" Then
                'gvStudEnquiry.Columns(56).Visible = True
                str_query = "SELECT " & lstrTop10 & " row_number()OVER(order by isNULL(EQS_REGNDATE,eqm_enqdate) DESC ,isNULL(EQS_ACCNO,b.eqs_applno) DESC ) AS RowNumber,eqs_id,eqs_applno,eqm_enqid,eqm_enqdate,grm_display,shf_descr,stm_descr,isnull(EFR_ICONS,'')EFR_ICONS,EFR_REASON, " _
                                                   & " appl_name=isnull(eqm_applfirstname,'') + ' '+isnull(eqm_applmidname,'') + ' ' " _
                                                   & " +isnull(eqm_appllastname,''),F.*,G.*,eqs_applno,EQS_CURRSTATUS,isnull(EQS_DOC_STG_ID,0) as  EQS_DOC_STG_ID ," _
                                                   & " ISNULL(EQS_APL_DISABLE,'FALSE') AS EQS_APL_DISABLE,B.EQS_APL_ID as APL_ID, B.EQS_STATUS as EQS_STATUS," _
                                                   & " case  when (ISNULL(B.EQS_APL_DISABLE, 'FALSE')='FALSE') AND(B.EQS_STATUS='OFR') THEN 'TRUE' ELSE 'FALSE' END AS FLAG ,isnull(Eqs_AccNo,'') as Eqs_AccNo,isnull(B.EQS_bFOLLOWUP,0) as EQS_bFOLLOWUP,EQS_REGNDATE,EQM_APPLGENDER as GENDER ,APL_IMAGE,APL_TYPE,CASE when isnull(EQS_bSEN,0)=1  THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END  as EQS_bSEN,case when rtrim(ltrim(EQS_SEN))='' then 0 else 1 end as bSEN, EQS_SEN,EQM_bSTAFFGEMS,EQM_bEXSTUDENT,EQM_bAPPLSIBLING, DBO.GETCAL_AGE( EQM_APPLDOB,ACD_AGE_CUTOFF)  	as Appl_Age ,'Age on '+ replace(convert(varchar(12),ACD_AGE_CUTOFF,106),' ','/') as CUTOFF_DT  ,isNULL(PREVSCHOOL.BSU_SHORTNAME,'')  as Prev_School_GEMS  FROM enquiry_m A WITH (NOLOCK) " _
                                                   & " INNER JOIN enquiry_schoolprio_s B WITH (NOLOCK) ON A.eqm_enqid=B.eqs_eqm_enqid " _
                                                   & " INNER JOIN vw_OSO_STUDAPPLSTAGES F ON B.EQS_ID=F.PRA_EQS_ID " _
                                                   & " INNER JOIN vw_OSO_STUDSTAGEORDER G ON B.EQS_ID=G.PRA_EQS_ID " _
                                                   & " INNER JOIN grade_bsu_m C WITH (NOLOCK) ON B.eqs_grm_id=C.grm_id " _
                                                   & " INNER JOIN shifts_m D WITH (NOLOCK) ON B.eqs_shf_id=D.shf_id" _
                                                   & " INNER JOIN stream_m  E WITH (NOLOCK) ON B.eqs_stm_id=E.stm_id " _
                                                    & " INNER JOIN vw_ENQ_SIBLINGS P ON B.EQS_ID=P.EQS " _
                                                    & " INNER JOIN ACADEMICYEAR_D AS ACD WITH (NOLOCK) ON B.EQS_ACD_ID = ACD.ACD_ID " _
                                                     & " LEFT JOIN ApplicationDecision_M APPL ON B.EQS_APL_ID=APPL.APL_ID " _
                                                       & " LEFT JOIN BUSINESSUNIT_M  AS PREVSCHOOL ON A.EQM_PREVSCHOOL_BSU_ID = PREVSCHOOL.BSU_ID " _
                                                       & " LEFT OUTER JOIN ENQUIRY_FOLLOW_UP_REASON AS FR ON FR.ENR_EQS_ID=B.eqs_id AND FR.ENR_bACTIVE=1 " _
                                                  & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=FR.ENR_EFR_ID " _
                                                   & " WHERE B.eqs_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                                   & " AND B.eqs_bsu_id='" + Session("sbsuid").ToString + "' AND  EQS_STATUS='DEL' "
            ElseIf ddl_EnquiryCondition.SelectedValue = "ALL" Then
                'gvStudEnquiry.Columns(56).Visible = True

                str_query = "SELECT " & lstrTop10 & " row_number()OVER(order by isNULL(EQS_REGNDATE,eqm_enqdate) DESC ,isNULL(EQS_ACCNO,b.eqs_applno) DESC ) AS RowNumber,eqs_id,eqs_applno,eqm_enqid,eqm_enqdate,grm_display,shf_descr,stm_descr,isnull(EFR_ICONS,'')EFR_ICONS,EFR_REASON, " _
                                                   & " appl_name=isnull(eqm_applfirstname,'') + ' '+isnull(eqm_applmidname,'') + ' ' " _
                                                   & " +isnull(eqm_appllastname,''),F.*,G.*,eqs_applno,EQS_CURRSTATUS,isnull(EQS_DOC_STG_ID,0) as  EQS_DOC_STG_ID ," _
                                                   & " ISNULL(EQS_APL_DISABLE,'FALSE') AS EQS_APL_DISABLE,B.EQS_APL_ID as APL_ID, B.EQS_STATUS as EQS_STATUS," _
                                                   & " case  when (ISNULL(B.EQS_APL_DISABLE, 'FALSE')='FALSE') AND(B.EQS_STATUS='OFR') THEN 'TRUE' ELSE 'FALSE' END AS FLAG ,isnull(Eqs_AccNo,'') as Eqs_AccNo,isnull(B.EQS_bFOLLOWUP,0) as EQS_bFOLLOWUP,EQS_REGNDATE,EQM_APPLGENDER as GENDER ,APL_IMAGE ,APL_TYPE,CASE when isnull(EQS_bSEN,0)=1  THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END  as EQS_bSEN,case when rtrim(ltrim(EQS_SEN))='' then 0 else 1 end as bSEN, EQS_SEN,EQM_bSTAFFGEMS,EQM_bEXSTUDENT,EQM_bAPPLSIBLING,DBO.GETCAL_AGE( EQM_APPLDOB,ACD_AGE_CUTOFF) 	as Appl_Age ,'Age on '+ replace(convert(varchar(12),ACD_AGE_CUTOFF,106),' ','/') as CUTOFF_DT ,isNULL(PREVSCHOOL.BSU_SHORTNAME,'')  as Prev_School_GEMS  FROM enquiry_m A " _
                                                   & " INNER JOIN enquiry_schoolprio_s B WITH (NOLOCK) ON A.eqm_enqid=B.eqs_eqm_enqid " _
                                                   & " INNER JOIN vw_OSO_STUDAPPLSTAGES F ON B.EQS_ID=F.PRA_EQS_ID " _
                                                   & " INNER JOIN vw_OSO_STUDSTAGEORDER G ON B.EQS_ID=G.PRA_EQS_ID " _
                                                   & " INNER JOIN grade_bsu_m C WITH (NOLOCK) ON B.eqs_grm_id=C.grm_id " _
                                                   & " INNER JOIN shifts_m D WITH (NOLOCK) ON B.eqs_shf_id=D.shf_id" _
                                                   & " INNER JOIN stream_m  E WITH (NOLOCK) ON B.eqs_stm_id=E.stm_id " _
                                                    & " INNER JOIN vw_ENQ_SIBLINGS P ON B.EQS_ID=P.EQS " _
                                                    & " INNER JOIN ACADEMICYEAR_D AS ACD WITH (NOLOCK) ON B.EQS_ACD_ID = ACD.ACD_ID " _
                                                     & " LEFT JOIN ApplicationDecision_M APPL ON B.EQS_APL_ID=APPL.APL_ID " _
                                                       & " LEFT JOIN BUSINESSUNIT_M  AS PREVSCHOOL ON A.EQM_PREVSCHOOL_BSU_ID = PREVSCHOOL.BSU_ID " _
                                                       & " LEFT OUTER JOIN ENQUIRY_FOLLOW_UP_REASON AS FR ON FR.ENR_EQS_ID=B.eqs_id AND FR.ENR_bACTIVE=1 " _
                                                  & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=FR.ENR_EFR_ID " _
                                                   & " WHERE B.eqs_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                                   & " AND B.eqs_bsu_id='" + Session("sbsuid").ToString + "' AND  (EQS_STATUS='DEL' or(EQS_STATUS<>'ENR' and EQS_CANCELDATE IS NULL)) "

            End If

            Session("sEnqStatus") = ddlEnqStatus.SelectedItem.Value


            If ddlEnqStatus.SelectedItem.Text <> "ALL" Then
                str_query = str_query & " AND B.EQS_CURRSTATUS='" & Session("sEnqStatus") & "'"
            Else
                '' str_query += " AND EQS_STATUS<>'NEW' "
            End If

            If Session("BSU_ENQ_SELECT_MANAGE") = "0" Then
                str_query = str_query + " "
            ElseIf Session("BSU_ENQ_SELECT_MANAGE") = "1" Then
                str_query = str_query + " and EQM_PREVSCHOOL_BSU_ID IN (SELECT BSU_ID From BusinessUnit_M WITH (NOLOCK))"
            ElseIf Session("BSU_ENQ_SELECT_MANAGE") = "2" Then
                str_query = str_query + " and EQM_PREVSCHOOL_BSU_ID NOT IN (SELECT BSU_ID From BusinessUnit_M WITH (NOLOCK))"
            Else
                str_query = str_query + " and EQM_PREVSCHOOL_BSU_ID='" + Session("BSU_ENQ_SELECT_MANAGE") + "'"
            End If



            If Session("CONTACTME_SELECT2") = "ALL" Then
                str_query = str_query + ""
            ElseIf Session("CONTACTME_SELECT2") = "YES" Then
                str_query = str_query + " and EQM_APPL_CONTACTME='Y' "
            ElseIf Session("CONTACTME_SELECT2") = "NO" Then
                str_query = str_query + " and isNULL(EQM_APPL_CONTACTME,'N')='N' "
            End If

            If Session("PAID_SELECT2") = "ALL" Then
                str_query = str_query + ""
            ElseIf Session("PAID_SELECT2") = "YES" Then
                str_query = str_query + " and EQS_REC_NO IS NOT NULL "
            ElseIf Session("PAID_SELECT2") = "NO" Then
                str_query = str_query + " and isNULL(EQS_REC_NO,'')='' "
            ElseIf Session("PAID_SELECT2") = "AtSchool" Then
                str_query = str_query + " and EQS_REC_NO IS NOT NULL and EQS_bRegisteredonline=0 "
            ElseIf Session("PAID_SELECT2") = "Online" Then
                str_query = str_query + " and EQS_REC_NO IS NOT NULL and EQS_bRegisteredonline=1 "
            ElseIf Session("PAID_SELECT2") = "NotPaidReg" Then
                str_query = str_query + " and isNULL(EQS_REC_NO,'')='' AND EQS_CURRSTATUS BETWEEN 3 AND 6 "
            End If
            If ddlFollowupStatus.SelectedValue <> "0" Then
                str_query += " AND EFR_ID=" & ddlFollowupStatus.SelectedValue
            End If
            ' B_SEN
            If ddl_sen.SelectedValue = "ALL" Then
                str_query = str_query + ""
            ElseIf ddl_sen.SelectedValue = "YES" Then
                str_query = str_query + " and EQS_bSEN = 1 "
            ElseIf ddl_sen.SelectedValue = "NO" Then
                str_query = str_query + " and EQS_bSEN = 0 "
            End If
            'GEMS_Staff
            If ddl_GEMS_Staff.SelectedValue = "ALL" Then
                str_query = str_query + ""
            ElseIf ddl_GEMS_Staff.SelectedValue = "YES" Then
                str_query = str_query + " and EQM_bSTAFFGEMS = 1 "
            ElseIf ddl_GEMS_Staff.SelectedValue = "NO" Then
                str_query = str_query + " and EQM_bSTAFFGEMS = 0 "
            End If
            'Ex Student
            If ddl_Ex_Student.SelectedValue = "ALL" Then
                str_query = str_query + ""
            ElseIf ddl_Ex_Student.SelectedValue = "YES" Then
                str_query = str_query + " and EQM_bEXSTUDENT = 1 "
            ElseIf ddl_Ex_Student.SelectedValue = "NO" Then
                str_query = str_query + " and EQM_bEXSTUDENT = 0 "
            End If
            'Siblingt
            If ddl_Siblingt.SelectedValue = "ALL" Then
                str_query = str_query + ""
            ElseIf ddl_Siblingt.SelectedValue = "YES" Then
                str_query = str_query + " and EQM_bAPPLSIBLING = 1 "
            ElseIf ddl_Siblingt.SelectedValue = "NO" Then
                str_query = str_query + " and EQM_bAPPLSIBLING = 0 "
            End If
            'ApplicantName
            If String.IsNullOrWhiteSpace(txt_ApplicantName.Text) = False Then
                str_query = str_query + "AND ((isnull(eqm_applfirstname+ ' ', '')  + isnull(eqm_applmidname+ ' ', '')  + isnull(eqm_appllastname, '')) LIKE '%" + txt_ApplicantName.Text + "%')"
            End If

            If String.IsNullOrWhiteSpace(txt_Enquiry_Number.Text) = False Then
                str_query = str_query + "AND Eqs_ApplNo LIKE  '%" + txt_Enquiry_Number.Text + "%'  "
            End If

            If String.IsNullOrWhiteSpace(txt_AcountNumber.Text) = False Then
                str_query = str_query + "AND Eqs_AccNo LIKE  '%" + txt_AcountNumber.Text + "%'  "
            End If

            'If btnSIB.Text = "Show All" Then
            '    str_query += " AND Sibling='SIB' "
            'End If
            Dim strFilter As String = ""
            Dim strSidsearch As String()
            Dim strSearch As String

            Dim enqSearch As String = ""
            Dim nameSearch As String = ""
            Dim dateSearch As String = ""
            Dim ddlgvShift As New DropDownList
            Dim ddlgvStream As New DropDownList

            Dim accSearch As String = ""

            Dim selectedGrade As String = ""
            Dim selectedGender As String = ""
            Dim selectedShift As String = ""
            Dim selectedStream As String = ""
            Dim txtSearch As New TextBox







            'If gvStudEnquiry.Rows.Count > 0 Then


            '    txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
            '    strSidsearch = h_Selected_menu_1.Value.Split("__")
            '    strSearch = strSidsearch(0)
            '    strFilter = GetSearchString("eqs_applno", txtSearch.Text, strSearch)
            '    enqSearch = txtSearch.Text

            '    txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtAccSearch")
            '    strSidsearch = h_Selected_menu_12.Value.Split("__")
            '    strSearch = strSidsearch(0)
            '    strFilter += GetSearchString("eqs_accno", txtSearch.Text, strSearch)
            '    accSearch = txtSearch.Text

            '    txtSearch = New TextBox
            '    txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
            '    strSidsearch = h_Selected_menu_2.Value.Split("__")
            '    strSearch = strSidsearch(0)
            '    strFilter += GetSearchString("convert(varchar,eqm_enqdate,106)", txtSearch.Text.Replace("/", " "), strSearch)
            '    dateSearch = txtSearch.Text


            '    txtSearch = New TextBox
            '    txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
            '    strSidsearch = h_Selected_menu_1.Value.Split("__")
            '    strSearch = strSidsearch(0)
            '    strFilter += GetSearchString("grm_display", txtSearch.Text, strSearch)
            '    selectedGrade = txtSearch.Text


            '    txtSearch = New TextBox
            '    txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGender")
            '    strSidsearch = h_Selected_menu_13.Value.Split("__")
            '    strSearch = strSidsearch(0)
            '    strFilter += GetSearchString("EQM_APPLGENDER", txtSearch.Text, strSearch)
            '    selectedGender = txtSearch.Text


            '    ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
            '    If ddlgvShift.Text <> "ALL" And ddlgvShift.Text <> "" Then
            '        strFilter = strFilter + " and shf_descr='" + ddlgvShift.Text + "'"
            '        selectedShift = ddlgvShift.Text
            '    End If


            '    ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")
            '    If ddlgvStream.Text <> "ALL" And ddlgvStream.Text <> "" Then
            '        strFilter = strFilter + " and stm_descr='" + ddlgvStream.Text + "'"
            '        selectedStream = ddlgvStream.Text
            '    End If


            '    txtSearch = New TextBox
            '    txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
            '    strSidsearch = h_Selected_menu_3.Value.Split("__")
            '    strSearch = strSidsearch(0)
            '    strFilter += GetSearchString("isnull(eqm_applfirstname,' ')+' '+isnull(eqm_applmidname,' ')+' '+isnull(eqm_appllastname,' ')", txtSearch.Text, strSearch)
            '    nameSearch = txtSearch.Text

            '    If strFilter.Trim <> "" Then
            '        str_query = str_query + strFilter
            '    End If


            'End If
            Dim ds As DataSet
            Dim dv As DataView



            'str_query += " order by isNULL(EQS_REGNDATE,eqm_enqdate) desc ,isNULL(EQS_ACCNO,b.eqs_applno) desc "
            str_query += Session("SORTORDER")

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudEnquiry.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)("FLAG") = True
                ds.Tables(0).Rows(0)("EQS_bSEN") = False
                ds.Tables(0).Rows(0)("bSEN") = False
                gvStudEnquiry.DataSource = ds.Tables(0)
                Try
                    gvStudEnquiry.DataBind()
                Catch ex As Exception
                End Try
                ViewState("norow") = "yes"
                btnPrintOffer.Visible = False
                btnExport.Visible = False

                Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
                gvStudEnquiry.Rows(0).Cells.Clear()
                gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
                gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                btnHolder.Visible = False
                lblSTAT.Text = "<span class='EM_statistic'> <span class='pull-left'> Total </span><span class='pull-right'> " & lstrTotal & "</span></span><span class='EM_statistic'> <span class='pull-left'> Male </span><span class='pull-right'>  " & lstrMale & "</span></span><span class='EM_statistic'><span class='pull-left'> Female  </span><span class='pull-right'>  " & lstrFemale & "</span></span>"

            Else
                ViewState("norow") = "no"
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim lstrGender = ds.Tables(0).Rows(i).Item("Gender")
                    ViewState("CutDate") = ds.Tables(0).Rows(i).Item("CUTOFF_DT")
                    lstrTotal = lstrTotal + 1
                    If lstrGender = "M" Then
                        lstrMale = lstrMale + 1

                    Else
                        lstrFemale = lstrFemale + 1
                    End If
                    btnHolder.Visible = True
                Next
                lblSTAT.Text = "<span class='EM_statistic'> <span class='pull-left'> Total </span><span class='pull-right'> " & lstrTotal & "</span></span><span class='EM_statistic'> <span class='pull-left'> Male </span><span class='pull-right'>  " & lstrMale & "</span></span><span class='EM_statistic'><span class='pull-left'> Female  </span><span class='pull-right'>  " & lstrFemale & "</span></span>"
                gvStudEnquiry.DataBind()
                'btnPrintOffer.Visible = True
                btnExport.Visible = True
            End If

            'txtSearch = New TextBox
            'txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
            'txtSearch.Text = enqSearch

            'txtSearch = New TextBox
            'txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
            'txtSearch.Text = dateSearch

            'txtSearch = New TextBox
            'txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
            'txtSearch.Text = nameSearch

            'txtSearch = New TextBox
            'txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
            'txtSearch.Text = selectedGrade


            'txtSearch = New TextBox
            'txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGender")
            'txtSearch.Text = selectedGender

            'txtSearch = New TextBox
            'txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtAccSearch")
            'txtSearch.Text = accSearch

            Session("eqm_enqid") = ds.Tables(0).Rows(0).Item("eqm_enqid").ToString()

            Dim dt As DataTable = ds.Tables(0)

            'If gvStudEnquiry.Rows.Count > 0 Then
            '    ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
            '    ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")

            '    Dim dr As DataRow



            '    ddlgvShift.Items.Clear()
            '    ddlgvShift.Items.Add("ALL")


            '    ddlgvStream.Items.Clear()
            '    ddlgvStream.Items.Add("ALL")




            '    For Each dr In dt.Rows
            '        If dr.Item(0) Is DBNull.Value Then
            '            Exit For
            '        End If
            '        With dr

            '            ''check for duplicate values and add to dropdownlist 
            '            If ddlgvShift.Items.FindByText(.Item("shf_descr")) Is Nothing Then
            '                ddlgvShift.Items.Add(.Item("shf_descr"))
            '            End If
            '            If ddlgvStream.Items.FindByText(.Item("stm_descr")) Is Nothing Then
            '                ddlgvStream.Items.Add(.Item("stm_descr"))
            '            End If


            '        End With
            '    Next

            '    If selectedShift <> "" Then
            '        ddlgvShift.Text = selectedShift
            '    End If

            '    If selectedStream <> "" Then
            '        ddlgvStream.Text = selectedStream
            '    End If
            'End If
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnPrintOffer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim ChkState As Boolean = False
        'If gvStudEnquiry.Rows.Count > 0 Then
        '    For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1
        '        Dim row As GridViewRow = gvStudEnquiry.Rows(i)
        '        Dim ckList As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
        '        If ckList Then
        '            ChkState = True
        '        End If
        '    Next

        '    If ChkState = True Then
        CallReport_offer()
        '    Else
        'lblError.Text = "No Enquiry selected for print"
        '    End If
        'Else
        'lblError.Text = "No Enquiry No available for print"
        'End If
    End Sub
    Private Sub CallReport_offer()

        '-------------------------------------------------------


        'For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1

        '    Dim row As GridViewRow = gvStudEnquiry.Rows(i)

        '    Dim ckList As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked

        '    If ckList Then
        '        EQS_IDs.Append((DirectCast(row.FindControl("lblEqsId"), Label).Text) & "_" & (DirectCast(row.FindControl("lblAPL_ID"), Label).Text))
        '        EQS_IDs.Append("|")
        '    End If
        'Next
        '----------------------------------------------
        Dim EQS_IDs As New StringBuilder

        Dim chk As CheckBox
        Dim Eqs_id As String = String.Empty
        Dim APL_ID As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("hashCheck_eqsIds") Is Nothing Then
            hash = Session("hashCheck_eqsIds")
        End If
        For Each rowItem As GridViewRow In gvStudEnquiry.Rows

            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

            Eqs_id = DirectCast(rowItem.FindControl("lblEqsId"), Label).Text
            APL_ID = DirectCast(rowItem.FindControl("lblAPL_ID"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(Eqs_id) = False Then
                    hash.Add(Eqs_id, Eqs_id & "_" & APL_ID)
                End If
            Else
                If hash.Contains(Eqs_id) = True Then
                    hash.Remove(Eqs_id)
                End If
            End If

        Next

        Session("hashCheck_eqsIds") = hash


        If Not Session("hashCheck_eqsIds") Is Nothing Then
            hash = Session("hashCheck_eqsIds")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                EQS_IDs.Append(hashloop.Value)
                EQS_IDs.Append("|")

            Next


        End If

        Dim param As New Hashtable

        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@EQS_IDs", EQS_IDs.ToString)
        param.Add("@BSU_ID", Session("sBsuid"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath(ResolveUrl("~\Students\Reports\RPT\rptOffer_letter_Group.rpt"))


        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('" + ResolveUrl("~/Reports/ASPX Report/rptReportViewerNew.aspx") + "');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + ResolveUrl("~/Reports/ASPX Report/rptReportViewerNew.aspx") + "','_blank');", True)
        End If
    End Sub
    Protected Function GetNavigateUrl(ByVal eqsid As String) As String
        'Dim str As String = "javascript:var popup = window.showModalDialog('studJoinDocuments.aspx?eqsid=" + eqsid + "', '','dialogHeight:500px;dialogWidth:905px;scroll:no;resizable:no;');"
        Dim str As String = "javascript:ShowWindowWithClose('studJoinDocuments.aspx?eqsid=" + eqsid + "',  'search', '55%', '85%'); return false;"
        Return str
    End Function
    Protected Sub lnkApplName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("action") = "select"
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub gvStudEnquiry_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hash As New Hashtable
        If Not Session("hashCheck_eqsIds") Is Nothing Then
            hash = Session("hashCheck_eqsIds")
        End If
        Dim chk As CheckBox
        Dim Eqs_id As String = String.Empty
        For Each rowItem As GridViewRow In gvStudEnquiry.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkSelect")), CheckBox)
            Eqs_id = gvStudEnquiry.DataKeys(rowItem.RowIndex)("Eqs_id").ToString()
            If hash.Contains(Eqs_id) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If

        Next
    End Sub
    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudEnquiry.PageIndexChanging
        Try
            gvStudEnquiry.PageIndex = e.NewPageIndex
            Dim APL_ID As String = String.Empty

            Dim hash As New Hashtable
            If Not Session("hashCheck_eqsIds") Is Nothing Then
                hash = Session("hashCheck_eqsIds")
            End If
            Dim chk As CheckBox
            Dim Eqs_id As String = String.Empty
            For Each rowItem As GridViewRow In gvStudEnquiry.Rows
                chk = DirectCast((rowItem.Cells(0).FindControl("chkSelect")), CheckBox)

                Eqs_id = gvStudEnquiry.DataKeys(rowItem.RowIndex)("Eqs_id").ToString()
                APL_ID = DirectCast(rowItem.FindControl("lblAPL_ID"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(Eqs_id) = False Then
                        hash.Add(Eqs_id, Eqs_id & "_" & APL_ID)
                    End If
                Else
                    If hash.Contains(Eqs_id) = True Then
                        hash.Remove(Eqs_id)
                    End If
                End If
            Next
            Session("hashCheck_eqsIds") = hash
            GridBind()

            'selectedCheckboxes()
            'GridBind()
            'reselectedcheckboxes()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim EQS_IDs As New StringBuilder

            Dim chk As CheckBox
            Dim Eqs_id As String = String.Empty
            Dim APL_ID As String = String.Empty
            Dim hash As New Hashtable
            If Not Session("hashCheck_eqsIds") Is Nothing Then
                hash = Session("hashCheck_eqsIds")
            End If
            For Each rowItem As GridViewRow In gvStudEnquiry.Rows

                chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

                Eqs_id = DirectCast(rowItem.FindControl("lblEqsId"), Label).Text
                APL_ID = DirectCast(rowItem.FindControl("lblAPL_ID"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(Eqs_id) = False Then
                        hash.Add(Eqs_id, Eqs_id & "_" & APL_ID)
                    End If
                Else
                    If hash.Contains(Eqs_id) = True Then
                        hash.Remove(Eqs_id)
                    End If
                End If



            Next

            Session("hashCheck_eqsIds") = hash


            If Not Session("hashCheck_eqsIds") Is Nothing Then
                hash = Session("hashCheck_eqsIds")
                Dim hashloop As DictionaryEntry

                For Each hashloop In hash
                    EQS_IDs.Append(hashloop.Value)
                    EQS_IDs.Append("|")

                Next


            End If


            If Trim(EQS_IDs.ToString) <> "" Then







                Dim str_conn = ConnectionManger.GetOASISConnectionString

                Dim str_query As String = String.Empty

                str_query = " SELECT DISTINCT  ENQUIRY_SCHOOLPRIO_S.EQS_APPLNO AS 'Enquiry Number',ISNULL(ENQUIRY_M.EQM_APPLFIRSTNAME, '') AS StudentFirstName, ISNULL(ENQUIRY_M.EQM_APPLMIDNAME, '') AS StudentMiddleName, " &
                    " ISNULL(ENQUIRY_M.EQM_APPLLASTNAME, '') AS StudentLastName,replace(CONVERT( CHAR(12), isnull(ENQUIRY_M.EQM_APPLDOB,''), 106 ),' ','/') as 'Date of Birth', " &
                 " ACADEMICYEAR_M.ACY_DESCR AS 'Year',replace(CONVERT( CHAR(12), isnull(ENQUIRY_SCHOOLPRIO_S.EQS_DOJ,''), 106 ),' ','/') as 'Date of join'," &
            " upper(TRM_M.TRM_DESCRIPTION) as 'Term of Join',GRADE_BSU_M.GRM_DESCR AS 'Grade', " &
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FFIRSTNAME, '')" &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MFIRSTNAME, '') " &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GFIRSTNAME, '') END AS PARENT_FIRSTNAME, " &
                         "  CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FMIDNAME, ' ') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MMIDNAME, ' ') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GMIDNAME, ' ') END AS PARENT_MIDDLENAME, " &
                         "  CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FLASTNAME, '') " &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MLASTNAME, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GLASTNAME, '') END AS PARENT_LASTNAME," &
         "  CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMSTREET, '') " &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMSTREET, '') " &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMSTREET, '') END AS PARENT_STREET," &
                        " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMAREA, '') " &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MCOMAREA, '') " &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GCOMAREA, '') END AS 'Area'," &
                        " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMBLDG, '') " &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MCOMBLDG, '') " &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GCOMBLDG, '') END AS 'Building'," &
                        " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMAPARTNO, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MCOMAPARTNO, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GCOMAPARTNO, '') END AS 'Apartment No', " &
                         " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FRESPHONE, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MRESPHONE, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GRESPHONE, '') END AS PARENT_RESPHONE, " &
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FOFFPHONE, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MOFFPHONE, '') " &
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GOFFPHONE, '') END AS PARENT_OFFICE, " &
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FMOBILE, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MMOBILE, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GMOBILE, '') END AS PARENT_MOBILE, " &
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FEMAIL, '')" &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MEMAIL, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GEMAIL, '') END AS PARENT_MAIL, " &
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMPOBOX, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MCOMPOBOX, '') " &
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GCOMPOBOX, '') END AS PARENT_POBOX " &
                          " FROM  ENQUIRY_M INNER JOIN  ENQUIRY_SCHOOLPRIO_S ON ENQUIRY_M.EQM_ENQID = ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID INNER JOIN " &
                          " ENQUIRY_PARENT_M ON ENQUIRY_M.EQM_ENQID = ENQUIRY_PARENT_M.EQP_EQM_ENQID INNER JOIN " &
                          " GRADE_BSU_M ON ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " &
                          " ENQUIRY_SCHOOLPRIO_S.EQS_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " &
                          " ACADEMICYEAR_M INNER JOIN   ACADEMICYEAR_D ON ACADEMICYEAR_M.ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID ON " &
                          " ENQUIRY_SCHOOLPRIO_S.EQS_ACD_ID = ACADEMICYEAR_D.ACD_ID left outer JOIN " &
                          " TRM_M ON ACADEMICYEAR_D.ACD_ID = TRM_M.TRM_ACD_ID AND ENQUIRY_M.EQM_TRM_ID = TRM_M.TRM_ID where  ENQUIRY_SCHOOLPRIO_S.EQS_ID in (SELECT str_data1 FROM fn_Split_to_table_2Col('" & EQS_IDs.ToString & "','|'))"



                Dim ds As New DataSet
                'Dim dt As New DataTable()
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                'If ds.Tables(0).Rows.Count > 0 Then


                Dim grdView As New GridView
                grdView.DataSource = ds
                grdView.DataBind()

                Dim str_err As String = String.Empty
                Dim errorMessage As String = String.Empty

                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/vnd.ms-excel"
                'Response.Clear()
                'Response.AddHeader("content-disposition", String.Format("inline;filename={0}.xls", "student"))
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", "student"))

                'Response.Charset = ""
                'Response.ContentType = "application/vnd.xls"
                'Response.ContentType = "application/ms-excel"

                Dim stringWrite As New StringWriter()
                Dim htmlWrite As New HtmlTextWriter(stringWrite)
                grdView.RenderControl(htmlWrite)
                Dim htmlString As String = stringWrite.ToString()
                Response.Write(htmlString)
                Response.Flush()

            Else

                lblError.Text = "No record selected for export!!!"
            End If

        Catch ex As Exception
            lblError.Text = "Unexpected error"
            UtilityObj.Errorlog("btnExport", ex.Message)
        Finally
            Response.End()
        End Try
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim ChkState As Boolean = False

        Dim chk As CheckBox
        Dim ENQID As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If
        For Each rowItem As GridViewRow In gvStudEnquiry.Rows

            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

            ENQID = DirectCast(rowItem.FindControl("lblEnqId"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(ENQID) = False Then
                    hash.Add(ENQID, DirectCast(rowItem.FindControl("lblEnqId"), Label).Text)
                End If
            Else
                If hash.Contains(ENQID) = True Then
                    hash.Remove(ENQID)
                End If
            End If
        Next

        Session("hashCheck") = hash

        If Not Session("hashCheck") Is Nothing Then
            CallReport()
        Else
            lblError.Text = "No Enquiry selected for print"
        End If
    End Sub
    Private Sub CallReport()

        Dim EQS_EQM_ENQIDs As New StringBuilder
        Dim hash As New Hashtable

        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                EQS_EQM_ENQIDs.Append(hashloop.Value)
                EQS_EQM_ENQIDs.Append("|")
            Next
        End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@EQS_EQM_ENQIDs", EQS_EQM_ENQIDs.ToString)
        param.Add("@STU_BSU_ID", Session("sBsuid"))
        param.Add("ACD_YEAR", ddlAcademicYear.SelectedItem.Text)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("..\Students\Reports\RPT\rptENQUIRY_NEW_LIST.rpt")
        End With
        Session.Remove("hashCheck")
        Session("rptClass") = rptClass
        'Dim jscript As New StringBuilder()

        'Dim URL As String = "../Reports/ASPX Report/rptReportViewer.aspx"
        'jscript.Append("<script>window.open('")
        'jscript.Append(URL)
        'jscript.Append("');</script>")
        'Page.RegisterStartupScript("OpenWindows", jscript.ToString())
        ReportLoadSelection()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_12.Value.Split("__")
        getid12(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_13.Value.Split("__")
        getid13(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid13(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_13_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid12(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_12_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)

        Session("ACD_SELECT") = ddlAcademicYear.SelectedValue
        Session("CONTACTME_SELECT2") = ddlContactMe.SelectedItem.Value
        Session("PAID_SELECT2") = ddlPaid.SelectedItem.Value
        Session("BSU_ENQ_SELECT_MANAGE") = ddlPrevSchool_BSU.SelectedItem.Value
        GridBind()
    End Sub
    Public Function GetSomeStringValue(Status_ID As String) As String
        Dim Status As String = ""
        Try
            If Status_ID = "2" Then
                Status = "ENQUIRY"
            ElseIf Status_ID = "1" Then
                Status = "NEW ENQUIRY"
            ElseIf Status_ID = "3" Then
                Status = "REGISTER"
            ElseIf Status_ID = "4" Then
                Status = "SCREENING"
            ElseIf Status_ID = "5" Then
                Status = "APPROVAL"
            ElseIf Status_ID = "6" Then
                Status = "OFFER LETTER"
            ElseIf Status_ID = "0" Then
                Status = "CANCELED"
            End If


        Catch ex As Exception

        End Try

        Return Status
    End Function
    Protected Sub gvStudEnquiry_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudEnquiry.RowCommand
        Try

            If e.CommandName = "Details" Or e.CommandName = "EditRow" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStudEnquiry.Rows(index), GridViewRow)
                Dim eqsid As New Label
                Dim enqid As New Label
                Dim enqno As New Label
                Dim enqApplNo As New Label


                Dim url As String
                'define the datamode to view if view is clicked
                ViewState("datamode") = "edit"
                Session("sRegister") = ""
                'Encrypt the data that needs to be send through Query String
                eqsid = selectedRow.FindControl("lblEqsid")
                enqid = selectedRow.FindControl("lblEnqId")
                enqApplNo = selectedRow.FindControl("lblEnqApplNo")
                ''added by suriya on 25-jan-2010
                Session("enqApplNo") = enqApplNo.Text
                ''over

                Dim lblGrade As Label = selectedRow.FindControl("lblGrade")
                Dim lblShift As Label = selectedRow.FindControl("lblShift")
                Dim lblStream As Label = selectedRow.FindControl("lblStream")
                Dim lblCompReg As Label = selectedRow.FindControl("lblSTG3COMP")
                Dim lblGender As Label = selectedRow.FindControl("lblGender")

                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                'modified by dhanya
                Session("eqm_enqid") = enqid

                Dim editString As String = Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text + "|" + ddlAcademicYear.SelectedValue + "|" + lblGrade.Text + "|" + lblShift.Text + "|" + lblStream.Text)

                If e.CommandName = "Details" Then
                    url = ResolveUrl(String.Format("~/Students/EnquiryDetails.aspx?MainMnu_code={0}&datamode={1}&eqsid=" + Encr_decrData.Encrypt(eqsid.Text) _
                                                         & "&enqid=" + Encr_decrData.Encrypt(enqid.Text) + "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                                                        , Encr_decrData.Encrypt(ViewState("MainMnu_code")), ViewState("datamode")))

                    'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + url + "','_blank');", True)
                    Response.Redirect(url)
                End If
                If e.CommandName = "EditRow" Then
                    url = ResolveUrl(String.Format("~/Students/stuEnquiry_Edit_new.aspx?MainMnu_code={0}&datamode={1}&enqid=" + Encr_decrData.Encrypt(enqid.Text) + "&enqno=" + Encr_decrData.Encrypt(enqApplNo.Text) + "&stgcomp=" + Encr_decrData.Encrypt(lblCompReg.Text) + "&eqsid=" + Encr_decrData.Encrypt(eqsid.Text) + "&editstring=" + editString, ViewState("MainMnu_code"), ViewState("datamode")))
                    'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + url + "','_blank');", True)
                    Response.Redirect(url)
                End If
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
End Class
