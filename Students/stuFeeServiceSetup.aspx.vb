Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class stuFeeServiceSetup
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64
    Dim CurUsr_id As String
    Dim CurRole_id As String
    Dim CurBsUnit As String
    Dim USR_NAME As String
    Dim content As ContentPlaceHolder

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            CurUsr_id = Session("sUsr_id")
            CurRole_id = Session("sroleid")
            CurBsUnit = Session("sBsuid")
            USR_NAME = Session("sUsr_name")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            If USR_NAME = "" Or CurBsUnit = "" Or MainMnu_code <> OASISConstants.MNU_STU_SET_SERVICE_DET Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                'disable the control based on the rights
                content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(content, ViewState("menu_rights"), ViewState("datamode"))
            End If

            ddlACDYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
            ddlACDYear.DataTextField = "ACY_DESCR"
            ddlACDYear.DataValueField = "ACD_ID"
            ddlACDYear.DataBind()
            ddlACDYear.SelectedValue = Session("Current_ACD_ID")

            ddlService.DataSource = StudentServiceProvider.GetSERVICES_SYS_M()
            ddlService.DataTextField = "SVC_DESCRIPTION"
            ddlService.DataValueField = "SVC_ID"
            ddlService.DataBind()

            ddlProvideBSU.DataSource = FeeCommon.GETBSUFORUSER(USR_NAME)
            ddlProvideBSU.DataTextField = "BSU_NAME"
            ddlProvideBSU.DataValueField = "BSU_ID"
            ddlProvideBSU.DataBind()
            ddlProvideBSU.SelectedValue = Session("sBSUID")

            If ViewState("datamode") = "view" Then
                DissableControls(True)
                Dim vSVB_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("SVB_ID").Replace(" ", "+"))
                FillDetails(vSVB_ID)
            End If
        End If
    End Sub

    Private Sub FillDetails(ByVal vSVB_ID As Integer)
        Dim vSERV_PROV As StudentServiceProvider
        vSERV_PROV = StudentServiceProvider.GetDetails(vSVB_ID)
        ddlProvideBSU.SelectedValue = vSERV_PROV.PROVIDER_BSU_ID
        ddlService.SelectedValue = vSERV_PROV.SERVICE_ID
        ddlACDYear.SelectedValue = vSERV_PROV.ACD_ID
        txtRate.Text = vSERV_PROV.RATE
        chkServiceAvailable.Checked = vSERV_PROV.ServiceAvailable
        Session("sSERV_PROV") = vSERV_PROV
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        ClearAllFields()
        DissableControls(False)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Session("sSERV_PROV") = Nothing
    End Sub

    Private Sub DissableControls(ByVal dissable As Boolean)
        ddlACDYear.Enabled = Not dissable
        ddlProvideBSU.Enabled = Not dissable
        ddlService.Enabled = Not dissable
        chkServiceAvailable.Enabled = Not dissable
        txtRate.Enabled = Not dissable
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        DissableControls(False)
        UtilityObj.beforeLoopingControls(Me.Page)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim vSERV_PROV As New StudentServiceProvider
        If Not Session("sSERV_PROV") Is Nothing Then
            vSERV_PROV = Session("sSERV_PROV")
        End If
        vSERV_PROV.BSU_ID = Session("sBSUID")
        vSERV_PROV.PROVIDER_BSU_ID = ddlProvideBSU.SelectedValue
        vSERV_PROV.SERVICE_ID = ddlService.SelectedValue
        vSERV_PROV.ACD_ID = ddlACDYear.SelectedValue
        vSERV_PROV.ServiceAvailable = chkServiceAvailable.Checked
        vSERV_PROV.RATE = 0 'CDbl(txtRate.Text)

        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim stTrans As SqlTransaction
        Dim newSVBID As Integer
        Dim errNo As Integer
        Try
            objConn.Close()
            objConn.Open()
            stTrans = objConn.BeginTransaction
            errNo = StudentServiceProvider.SaveDetails(vSERV_PROV, newSVBID, objConn, stTrans)
            Dim strRemarks As String = String.Empty

            Dim str_KEY As String = "INSERT"
            If ViewState("datamode") = "edit" Then
                str_KEY = "EDIT"
                If errNo = 0 Then errNo = UtilityObj.operOnAudiTable(Master.MenuName, newSVBID, str_KEY, Page.User.Identity.Name.ToString, Me.Page)
            Else
                strRemarks = "New Service Added. Sevice Name -:" & ddlService.SelectedItem.Text & "; Provider -:" & ddlProvideBSU.SelectedItem.Text & _
                "; Academic Year -:" & ddlACDYear.SelectedItem.Text & ";Is Service Available -:" & chkServiceAvailable.Checked
                If errNo = 0 Then errNo = UtilityObj.operOnAudiTable(Master.MenuName, newSVBID, str_KEY, Page.User.Identity.Name.ToString, Me.Page, strRemarks)
            End If
            If errNo = 0 Then
                stTrans.Commit()
                ClearAllFields()
                lblError.Text = "Student Service Provider Details saved sucessfully..."
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                If errNo = 200 Then
                    lblError.Text = "The sevice is already available"
                Else
                    lblError.Text = UtilityObj.getErrorMessage(errNo)
                End If
                stTrans.Rollback()
            End If
        Catch
            stTrans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(-1)
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Sub ClearAllFields()
        ddlACDYear.SelectedValue = Session("Current_ACD_ID")
        ddlProvideBSU.SelectedValue = Session("sBSUID")
        ddlService.SelectedIndex = -1
        txtRate.Text = ""
        chkServiceAvailable.Checked = False
        Session("sSERV_PROV") = Nothing
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            Call ClearAllFields()
            ViewState("datamode") = "none"
            Session("sSERV_PROV") = Nothing
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

End Class
