﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stuEnquiry_Edit_new.aspx.vb" Inherits="Students_stuEnquiry_Edit_new" %>

<%@ Register Src="../UserControls/FeeSponsor.ascx" TagName="FeeSponsor" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="<%= ResolveUrl("~/cssfiles/EnquiryStyle.css")%>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/vendor/Tabs-To-Accordion-master/css/easy-responsive-tabs.css")%>" rel="stylesheet" />
    <script src="<%= ResolveUrl("~/vendor/Tabs-to-Accordion-master/js/easyResponsiveTabs.js")%>"></script>
    <link href = "<%= ResolveUrl("~/cssfiles/build.css")%>" rel="stylesheet" />
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <div class="row">


                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <i class="fa fa-book mr-3"></i>
                    Edit Enquiry in School
      
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  text-right">
                
                    <a class="btn btn-primary btn-sm" href="<%= ResolveUrl("~/Students/EnquiryDashboard.aspx?MainMnu_code=LjcF1BVVHbk%3d&datamode=Zo4HhpVNpXc%3d")%>">Enquiry Managment</a>
                    <%--       <button type="button" class="btn btn-success btn-sm">Follow up</button>--%>
                </div>

            </div>
        </div>

        <div class="card-body">
            <div class="col-12 text-center">

                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <asp:ValidationSummary ID="vSummary" runat="server" CssClass="error" ForeColor="" ValidationGroup="groupM1" />
                <asp:ValidationSummary ID="VSummaryPass" runat="server" CssClass="error" ForeColor="" ValidationGroup="groupM2" />

            </div>
            <div class="col-12 text-center">Marked with (<font color="red">*</font>) are mandatory</div>
            <div class="clearfix"></div>
               <div class="col-12 text-center">
                    <asp:LinkButton ID="lnkDocs" OnClientClick="javascript:return getDocuments();" class="text-danger" runat="server" Visible="False">Pending Docs</asp:LinkButton>
               </div>
              <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Enquiry No
                                </label>
                                <asp:Label ID="txtEnqNo" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Academic Year
                                </label>
                                <asp:Label ID="txtAcadYear" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Grade
                                </label>
                                <asp:Label ID="txtGrade" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Shift
                                </label>
                                <asp:Label ID="txtShift" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Stream
                                </label>
                                <asp:Label ID="txtStream" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Status
                                </label>
                                <asp:Label ID="txtStatus" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div></div>
                        <div class="col-md-6" id="TR1" runat="server">
                            <div class="row">
                                <div class="col-md-4" runat="server" id="TR1_C2">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Registration Date
                                        </label>
                                        <asp:Label ID="txtregdate" runat="server" CssClass="form-control"></asp:Label>

                                    </div>
                                </div>
                                <div class="col-md-4"  runat="server" id="TR1_C3">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                            Account No.
                                        </label>
                                        <asp:Label ID="txtACCNO" runat="server" CssClass="form-control"></asp:Label>

                                    </div>
                                </div>
                                <div class="col-md-4"  runat="server" id="TR1_C4">
                                    <div class="form-group">
                                        <label class="Ftitle">
                                        ---
                                        </label>
                                         <asp:LinkButton ID="lbtnFeeSpon" Style="float: right; vertical-align: middle; display: inline; position: relative; top: -14px;  z-index: 1;" runat="server">Set Fee Sponsor</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Enquiry Date
                                </label>
                                <asp:Label ID="txtEnqDate" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">
                                    Tentative Date Of Join
                                </label>
                                <asp:Label ID="txtTenDate" runat="server" CssClass="form-control"></asp:Label>

                            </div>
                        </div>
                    </div>
                </div>



                <div class="clearfix "></div>
                <div id="parentHorizontalTab" class="mt-3">
                    <ul class="resp-tabs-list hor_1">
                        <li>Main</li>
                        <li>Passport / Visa</li>
                        <li>Previous School</li>
                        <li>Contact Details</li>
                        <li>Services</li>
                        <li>Health</li>
                        <li>Other Details</li>
                    </ul>
                    <div class="resp-tabs-container hor_1">
                        <div>
                            <asp:Panel ID="vwMain" runat="server" Width="100%">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="Ftitle">Name As In Birth Certificate</label>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtApplFirstName" runat="server" MaxLength="100" placeholder="First Name" CssClass="form-control"> </asp:TextBox>
                                                      <asp:RequiredFieldValidator ID="rfvFname_Stud" runat="server" ControlToValidate="txtApplFirstName"
                    Display="Dynamic" ErrorMessage="First name required" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtApplMidName" runat="server" MaxLength="100" placeholder="Middle Name" CssClass="form-control">  </asp:TextBox>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtApplLastName" runat="server" MaxLength="100" placeholder="Last Name" CssClass="form-control"> </asp:TextBox>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Name in English (as in National Id )
                                            </label>
                                            <asp:TextBox ID="txtSEmiratesId_ENG" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Name in Arabic (as in National Id )
                                            </label>
                                            <asp:TextBox ID="txtSEmiratesId_ARB" runat="server" CssClass="form-control"></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">Gender</label>
                                            <div class="RB_Holder">
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rdMale" runat="server" GroupName="Gender" Text="Male"></asp:RadioButton>
                                                </label>
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rdFemale" runat="server" GroupName="Gender" Text="Female"></asp:RadioButton>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Religion 
                            <asp:Label ID="ltReg" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                            <asp:DropDownList ID="ddlReligion" runat="server" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvReligion" runat="server" ControlToValidate="ddlReligion"
                                                Display="Dynamic" ErrorMessage="Please select Religion"
                                                InitialValue=" " ValidationGroup="groupM1">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Date Of Birth 
                        <asp:Label ID="ltDOB" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnDOB" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <asp:RequiredFieldValidator ID="rfvDatefrom" runat="server" ControlToValidate="txtDOB"
                                                Display="Dynamic" ErrorMessage="Date of birth cannot be left empty"
                                                ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtDOB"
                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the   Date of Birth in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                    ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnDOB" TargetControlID="txtDOB">
                                            </ajaxToolkit:CalendarExtender>
                                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtDOB"
                    CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Rejoining Date entered is not a valid date"
                    ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Place Of Birth 
                           <asp:Label ID="ltPOB" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                            <asp:TextBox ID="txtPOB" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPob" runat="server" ControlToValidate="txtPOB"
                                                Display="Dynamic" ErrorMessage="Place of birth cannot be left empty" ValidationGroup="groupM1" Visible="false">*</asp:RequiredFieldValidator>


                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Country Of Birth 
                       <asp:Label ID="ltCOB" runat="server" Text="*" ForeColor="Red"></asp:Label></label>
                                            <asp:DropDownList ID="ddlCOB" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvCOB" runat="server" ControlToValidate="ddlCOB"
                                                Display="Dynamic" ErrorMessage="Please select Country of Birth"
                                                InitialValue=" " ValidationGroup="groupM1">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Nationality 
                                            <asp:Label ID="ltNationality" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                            <asp:DropDownList ID="ddlNationality" runat="server" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvNationality" runat="server" ControlToValidate="ddlNationality"
                                                Display="Dynamic" ErrorMessage="Please select Student Nationality"
                                                InitialValue=" " ValidationGroup="groupM1">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-md-12" id="tr_documents" runat="server" visible="false">
                                        <div class="row">
                                            <div class="col-md-12">
                                                The following documents has to be collected before registration
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        To Collect 
                                                    </label>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <asp:ListBox ID="lstNotSubmitted" runat="server"
                                                                SelectionMode="Multiple" Style="overflow: auto; width: 100%;"></asp:ListBox>
                                                        </div>
                                                        <div class="col-md-2 text-center">
                                                            <asp:Button ID="btnRight" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                                OnClick="btnRight_Click" Text=">>" /><br />
                                                            <asp:Button ID="btnLeft" runat="server" CommandName="Select" CssClass="button" Font-Size="Smaller"
                                                                OnClick="btnLeft_Click" Text="<<" />
                                                        </div>
                                                    </div>



                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Collected
                                                    </label>
                                                    <asp:ListBox ID="lstSubmitted" runat="server"
                                                        SelectionMode="Multiple" Style="overflow: auto; width: 100%"></asp:ListBox>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                      <div class="col-md-12">     <hr /></div>
                <div class="col-md-4">
                    <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
                        <label class="Ftitle">
                            First Language(Main) 
                        </label>
                        <asp:DropDownList ID="ddlFLang" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                                 </div>
                    <div class="form-group">
                        <label class="Ftitle">
                            Proficiency in English 
                        </label>
                        <div class="row">

                            <div class="col-md-12 mb-2">
                                <label >
                                  <strong style="width: 80px;display: inline-block;">  Reading : </strong>
                                </label>
                                <div style="display:inline-block">

                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbRExc" runat="server" GroupName="Read" Text="Excellent" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbRGood" runat="server" GroupName="Read" Text="Good" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbRFair" runat="server" GroupName="Read" Text="Fair" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbRPoor" runat="server" GroupName="Read" Text="Poor" />
                                    </label>
                                </div>
                            </div> 
                            <div class="col-md-12 mb-2">
                                <label >
                                <strong style="width: 80px;display: inline-block;">   Writing : </strong> 
                                </label>
                                     <div style="display:inline-block">

                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbWExc" runat="server" GroupName="Write" Text="Excellent" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbWGood" runat="server" GroupName="Write" Text="Good" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbWFair" runat="server" GroupName="Write" Text="Fair" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbWPoor" runat="server" GroupName="Write" Text="Poor" />
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label >
                                    <strong style="width: 80px;display: inline-block;">
                                    Speaking : </strong>
                                </label>
                                      <div style="display:inline-block">

                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbSExc" runat="server" GroupName="Speak" Text="Excellent" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbSGood" runat="server" GroupName="Speak" Text="Good" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbSFair" runat="server" GroupName="Speak" Text="Fair" />
                                    </label>
                                    <label class="radio-inline radio radio-success">
                                        <asp:RadioButton ID="rbSPoor" runat="server" GroupName="Speak" Text="Poor" />
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="Ftitle">
                            Other Languages(Specify) 
                        </label>
                        <div id="plOLang" runat="server" class="checkbox-list modifay form-control">
                            <div class="checkbox checkbox-success ">
                            <asp:CheckBoxList ID="chkOLang" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="styled">
                            </asp:CheckBoxList>
                                </div>
                        </div>
                    </div>
                </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div>
                            <asp:Panel ID="vwAmt" runat="server" Width="100%">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Passport No 
                                     <asp:Label ID="ltPASSNO" runat="server" ForeColor="Red" Text=""></asp:Label></label>
                                            <asp:TextBox ID="txtPNo" runat="server" CssClass="form-control"> </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPassNo" runat="server" ControlToValidate="txtPNo"
                                                Display="Dynamic" ErrorMessage="Passport No. required" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Passport Issue Place</label>
                                            <asp:TextBox ID="txtPIssPlace" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPass_Issue" runat="server" ControlToValidate="txtPIssPlace"
                                                Display="Dynamic" ErrorMessage="Passport Issue Place required" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Passport Issue Date <span class="text-info"></span>
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtPIssDate" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnPIssDate" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnPIssDate" TargetControlID="txtPIssDate">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="rfvPass_IssDate" runat="server" ControlToValidate="txtPIssDate"
                                                Display="Dynamic" ErrorMessage="Passport Issue Date cannot be left empty"
                                                ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtPIssDate"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Issue  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                            <asp:CustomValidator ID="cvIss_date" runat="server" ControlToValidate="txtPIssDate"
                                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Issued Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Passport Expiry Date <span class="text-info"></span>
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtPExpDate" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnPExpDate" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnPExpDate" TargetControlID="txtPExpDate">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="rfvPass_ExpDate" runat="server" ControlToValidate="txtPExpDate"
                                                Display="Dynamic" ErrorMessage="Passport Expiry Date cannot be left empty"
                                                ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPExpDate"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="cvExp_date" runat="server" ControlToValidate="txtPExpDate"
                                                    CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Passport Expiry Date entered is not a valid date and must be greater than Issue Date"
                                                    ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Visa No</label>
                                            <asp:TextBox ID="txtVNo" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Visa Issue Place</label>
                                            <asp:TextBox ID="txtVIssPlace" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Visa Issue Date <span class="text-info"></span>
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtVIssDate" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnVIssDate" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnVIssDate" TargetControlID="txtVIssDate">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtVIssDate"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Issue  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtVIssDate"
                                                    CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Issued Date entered is not a valid date"
                                                    ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Visa Expiry Date <span class="text-info"></span>
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtVExpDate" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnVExpdate" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnVExpdate" TargetControlID="txtVExpDate">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtVExpDate"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter the Expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator4" runat="server" ControlToValidate="txtVExpDate"
                                                    CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Visa Expiry Date entered is not a valid date and must be greater than Issue Date"
                                                    ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Issuing Authority</label>
                                            <asp:TextBox ID="txtVIssAuth" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                National Id</label>
                                            <asp:TextBox ID="txtEmiratesId" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Premises Id</label>
                                            <asp:TextBox ID="txtPremisesId" runat="server" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </asp:Panel>

                        </div>
                        <div>

                            <asp:Panel ID="vwDocuments" runat="server" Width="100%">
                                <div class="row">
                                    <div class="col-md-6" id="trSch0" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        School Name</label>
                                                    <asp:DropDownList ID="ddlPreSchool_Nursery" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Select" Value="-1" />
                                                        <asp:ListItem Text="other" Value="0" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Registration No</label>
                                                    <asp:TextBox ID="txtRegNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-12" id="ddlPreSchool_Nursery_other" style="display: none">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        (If you choose other,please specify the School Name)</label>
                                                    <asp:TextBox ID="txtPreSchool_Nursery" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6" id="trSch1" runat="server">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                School Detail Type</label>
                                            <asp:Label ID="ltSchoolType" runat="server" CssClass="form-control"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="trSch2" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        School Name</label>
                                                    <asp:DropDownList ID="ddlPSchool" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Select" Value="-1" />
                                                        <asp:ListItem Text="other" Value="0" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="ddlPSchool_other" style="display: none;">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        (If you choose other,please specify the School Name)</label>
                                                    <asp:TextBox ID="txtPSchool" runat="server" MaxLength="250" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6" id="trSch3" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Name of the head teacher</label>
                                                    <asp:TextBox ID="txtSchool_head" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Student Id</label>
                                                    <asp:TextBox ID="txtFeeID_GEMS" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="trSch4" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Grade</label>
                                                    <asp:DropDownList ID="ddlPGrade" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Curriculum</label>
                                                    <asp:DropDownList ID="ddlPCurriculum" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="trSch5" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Language of Instruction</label>
                                                    <asp:TextBox ID="txtLang_Instr" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Address</label>
                                                    <asp:TextBox ID="txtSchAddr" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="trSch6" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        City</label>
                                                    <asp:TextBox ID="txtPSchCity" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Country</label>
                                                    <asp:DropDownList ID="ddlPCountry" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8" id="trSch7" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        School Phone (Country-Area-Number)
                                                    </label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <asp:TextBox ID="txtSCHPhone_Country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3 DT-p-0">
                                                            <asp:TextBox ID="txtSCHPhone_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtSCHPhone_No" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        School Fax (Country-Area-Number)
                                                    </label>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <asp:TextBox ID="txtSCHFax_Country" runat="server" MaxLength="3" CssClass="form-control" placeholder="Country"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3 DT-p-0">
                                                            <asp:TextBox ID="txtSCHFax_Area" runat="server" MaxLength="4" CssClass="form-control" placeholder="Area"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtSCHFax_No" runat="server" MaxLength="10" CssClass="form-control" placeholder="Number"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="trSch8" runat="server">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        From Date
                                                    </label>
                                                    <div class="Calendar_Holder">
                                                        <asp:TextBox ID="txtSchFrom_dt" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                        <asp:ImageButton ID="imgSchFrom_dt" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </div>
                                                    <ajaxToolkit:CalendarExtender ID="SchFrom_dt_CalendarExtender" runat="server"
                                                        Format="dd/MMM/yyyy" PopupButtonID="imgSchFrom_dt"
                                                        TargetControlID="txtSchFrom_dt">
                                                    </ajaxToolkit:CalendarExtender>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        To Date
                                                    </label>
                                                    <div class="Calendar_Holder">
                                                        <asp:TextBox ID="txtSchTo_dt" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                        <asp:ImageButton ID="imgtxtSchTo_dt" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                                    </div>
                                                    <ajaxToolkit:CalendarExtender ID="txtSchTo_dt_CalendarExtender" runat="server"
                                                        Format="dd/MMM/yyyy" PopupButtonID="imgtxtSchTo_dt"
                                                        TargetControlID="txtSchTo_dt">
                                                    </ajaxToolkit:CalendarExtender>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center" id="trSch9" runat="server">
                                        <asp:Button ID="btnGridAdd" runat="server" CssClass="button"
                                            Text="Add" />
                                        <asp:Button ID="btnGridUpdate" runat="server" CssClass="button"
                                            Text="Update" />
                                    </div>
                                    <div class="col-md-12" id="trSch10" runat="server">
                                        <asp:GridView ID="gvSchool" runat="server" AutoGenerateColumns="False"
                                            DataKeyNames="ID" EmptyDataText="No record added yet"
                                            EnableModelValidation="True" CssClass="table table-bordered table-row"
                                            Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="School Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSCH_NAME" runat="server" Text='<%# Bind("SCH_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Head Teacher">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSCH_HEAD" runat="server" Text='<%# Bind("SCH_HEAD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSCH_GRADE" runat="server" Text='<%# Bind("SCH_GRADE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSCH_FROMDT" runat="server" Text='<%#Bind("SCH_FROMDT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSchName" runat="server" Text='<%#Bind("SCH_TODT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToDt" runat="server" Text='<%#Bind("SCH_TODT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="EditBtn" runat="server"
                                                            CommandArgument='<%# Eval("id") %>' CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Khaki" />
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div>
                            <asp:Panel ID="vwConnDet" runat="server" Width="100%">
                                <div class="row mb-2">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Primary Contact 
                                            </label>
                                            <div class="RB_Holder">
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rdFather" runat="server" GroupName="ContactPrimary"
                                                        Text="Father"></asp:RadioButton>
                                                </label>
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rdMother" runat="server" GroupName="ContactPrimary"
                                                        Text="Mother"></asp:RadioButton>
                                                </label>
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rdGuardian" runat="server" GroupName="ContactPrimary" Text="Guardian"></asp:RadioButton>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Fee Sponsor/Who pays the fees</label>
                                            <asp:DropDownList ID="ddlFeeSponsor" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">Father</asp:ListItem>
                                                <asp:ListItem Value="2">Mother</asp:ListItem>
                                                <asp:ListItem Value="3">Guardian</asp:ListItem>
                                                <asp:ListItem Value="4">Father's Company</asp:ListItem>
                                                <asp:ListItem Value="5">Mother's Company</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Preferred Contact</label>
                                            <asp:DropDownList ID="ddlPrefContact" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="HOME PHONE">Home Phone</asp:ListItem>
                                                <asp:ListItem Value="OFFICE PHONE">Office Phone</asp:ListItem>
                                                <asp:ListItem Value="MOBILE">Mobile</asp:ListItem>
                                                <asp:ListItem Value="EMAIL">Email</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix "></div>
                                <div class="title-bg-Form-small ">
                                    Father's Contact Details
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="Ftitle">Full Name As Per Passport</label>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtFFirstName" runat="server" MaxLength="100" placeholder="First Name" CssClass="form-control"> </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvFFirstName" runat="server" ControlToValidate="txtFFirstName" Visible="false"
                                                        Display="Dynamic" EnableClientScript="false" ErrorMessage="Primary contact father's first name required"
                                                        ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtFFirstName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                        TargetControlID="txtFFirstName" ValidChars="-\./' ">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtFMidName" runat="server" MaxLength="100" placeholder="Middle Name" CssClass="form-control">  </asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtFMidName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                        TargetControlID="txtFMidName" ValidChars="-\./' ">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtFLastName" runat="server" MaxLength="100" placeholder="Last Name" CssClass="form-control"> </asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtFLastName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                        TargetControlID="txtFLastName" ValidChars="-\./' ">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Name in English (as in National Id )
                                            </label>
                                            <asp:TextBox ID="txtFEmiratesId_ENG" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Name in Arabic (as in National Id )
                                            </label>
                                            <asp:TextBox ID="txtFEmiratesId_ARB" runat="server" CssClass="form-control"></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                National ID
                                            </label>
                                            <asp:TextBox ID="txtFEMIRATES_ID" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                National ID Expiry Date 
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtFEMIRATES_ID_EXPDATE" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnFEMIRATES_IDExp_date" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbFEMIRATES_IDExp_date" runat="server" TargetControlID="txtFEMIRATES_ID_EXPDATE"
                                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                            <ajaxToolkit:CalendarExtender ID="ceFEMIRATES_IDExp_date" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnFEMIRATES_IDExp_date" TargetControlID="txtFEMIRATES_ID_EXPDATE">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:RegularExpressionValidator ID="revFEmir_Exp_date"
                                                runat="server" ControlToValidate="txtFEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                                ErrorMessage="Enter father's National ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Nationality 
                                            </label>
                                            <asp:DropDownList ID="ddlFNationality1" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Nationality 2 
                                            </label>
                                            <asp:DropDownList ID="ddlFNationality2" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-Head-small">
                                    Permanent Address
                                </div>
                                <div class="row  mb-2">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Line 1</label>
                                            <asp:TextBox ID="txtFContPermAddress1" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Line 2</label>
                                            <asp:TextBox ID="txtFContPermAddress2" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address City</label>
                                            <asp:TextBox ID="txtFContPermCity" runat="server" MaxLength="70" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Country</label>
                                            <asp:DropDownList ID="ddlFContPermCountry" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Res. Phone No 
                                            </label>
                                            <asp:TextBox ID="txtFContPermPhoneNo" runat="server" TabIndex="10" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                P.O Box/Zip Code</label>
                                            <asp:TextBox ID="txtFContPermPOBOX" runat="server" MaxLength="20" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-Head-small">
                                    Current Address 
                                </div>
                                <div class="row  mb-2">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Country</label>
                                            <asp:DropDownList ID="ddlFContCurrCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                City/State</label>
                                            <asp:DropDownList ID="ddlFCity" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="ddlFCity_other" style="display: none">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                If you choose other,please specify the Area Name</label>
                                            <asp:TextBox ID="txtFContCurrCity" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Area</label>
                                            <asp:DropDownList ID="ddlFArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="ddlFArea_other" style="display: none">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                If you choose other,please specify the Area Name</label>
                                            <asp:TextBox ID="txtFContCurArea" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Street</label>
                                            <asp:TextBox ID="txtFContCurStreet" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Building</label>
                                            <asp:TextBox ID="txtFContCurrBldg" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Apartment No</label>
                                            <asp:TextBox ID="txtFContCurrApartNo" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                City/Emirate</label>
                                            <asp:DropDownList ID="ddlFContCurrPOBOX_EMIR" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                P.O Box</label>
                                            <asp:TextBox ID="txtFContCurrPOBox" runat="server" MaxLength="20" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Res. Phone No
                                            </label>
                                            <asp:TextBox ID="txtFContCurrPhoneNo" runat="server" TabIndex="4" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Mobile No
                                            </label>
                                            <asp:TextBox ID="txtFContCurrMobNo" runat="server" TabIndex="5" CssClass="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>


                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-Head-small">
                                    Communication Details
                                </div>
                                <div class="row  mb-2">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Email 
                            <asp:Label ID="ltPri_Email" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                            <asp:TextBox ID="txtFContEmail" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPri_Email" runat="server" ControlToValidate="txtFContEmail"
                                                Display="Dynamic" ErrorMessage="E-Mail ID required" ValidationGroup="groupM1" EnableViewState="False">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revPri_Email" runat="server" ControlToValidate="txtFContEmail"
                                                Display="Dynamic" ErrorMessage="E-Mail entered is not valid" ValidationGroup="groupM1" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" EnableViewState="False">*</asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Occupation</label>
                                            <asp:TextBox ID="txtFOcc" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Company</label>
                                            <asp:DropDownList ID="ddlFCompany" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="Select" Value="-1" />
                                                <asp:ListItem Text="other" Value="0" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="display: none;" id="ddlCompany_other">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                (If you choose other,please specify the Company Name) 
                                            </label>
                                            <asp:TextBox ID="txtFCompany" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Office Phone</label>
                                            <asp:TextBox ID="txtFOfficePhone" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Fax No.</label>
                                            <asp:TextBox ID="txtFFax" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix "></div>
                                <div class="title-bg-Form-small ">
                                    <div class="row">
                                        <div class="col-md-6">Mother's Contact Details </div>
                                        <div class="col-md-6 text-right">
                                            <asp:CheckBox ID="chkCopyF_Details" runat="server" Style="clear: left" Text="Copy Father Details" onclick="javascript:return copyFathertoMother(this);" />
                                        </div>
                                    </div>

                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="Ftitle">Full Name As Per Passport</label>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtMFirstName" runat="server" MaxLength="100" placeholder="First Name" CssClass="form-control"> </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvMFirstName" runat="server" ControlToValidate="txtMFirstName" Visible="false"
                                                        Display="Dynamic" EnableClientScript="false" ErrorMessage="Primary contact mother's first name required"
                                                        ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtMFirstName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                        TargetControlID="txtMFirstName" ValidChars="-\./' ">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtMMidName" runat="server" MaxLength="100" placeholder="Middle Name" CssClass="form-control">  </asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtMMidName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                        TargetControlID="txtMMidName" ValidChars="-\./' ">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtMLastName" runat="server" MaxLength="100" placeholder="Last Name" CssClass="form-control"> </asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtMLastName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                        TargetControlID="txtMLastName" ValidChars="-\./' ">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Name in English (as in National Id )
                                            </label>
                                            <asp:TextBox ID="txtMEmiratesId_ENG" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Name in Arabic (as in National Id )
                                            </label>
                                            <asp:TextBox ID="txtMEmiratesId_ARB" runat="server" CssClass="form-control"></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                National ID
                                            </label>
                                            <asp:TextBox ID="txtMEMIRATES_ID" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                National ID Expiry Date 
                                            </label>
                                            <div class="Calendar_Holder">
                                                <asp:TextBox ID="txtMEMIRATES_ID_EXPDATE" runat="server" CssClass="form-control textbox pull-left flip" placeholder="dd/mmm/yyyy"></asp:TextBox>
                                                <asp:ImageButton ID="imgBtnMEMIRATES_IDExp_date" runat="server" CssClass="pull-left flip imagebtn" ImageUrl="~/Images/calendar.gif" ImageAlign="Middle"></asp:ImageButton>
                                            </div>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbMEMIRATES_IDExp_date" runat="server" TargetControlID="txtMEMIRATES_ID_EXPDATE"
                                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                            <ajaxToolkit:CalendarExtender ID="ceMEMIRATES_IDExp_date" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnMEMIRATES_IDExp_date" TargetControlID="txtMEMIRATES_ID_EXPDATE">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:RegularExpressionValidator ID="revMEmir_Exp_date"
                                                runat="server" ControlToValidate="txtMEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                                ErrorMessage="Enter mother's National ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Nationality 
                                            </label>
                                            <asp:DropDownList ID="ddlMNationality1" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Nationality 2 
                                            </label>
                                            <asp:DropDownList ID="ddlMNationality2" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-Head-small">
                                    Permanent Address
                                </div>
                                <div class="row  mb-2">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Line 1</label>
                                            <asp:TextBox ID="txtMContPermAddress1" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Line 2</label>
                                            <asp:TextBox ID="txtMContPermAddress2" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address City</label>
                                            <asp:TextBox ID="txtMContPermCity" runat="server" MaxLength="70" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Country</label>
                                            <asp:DropDownList ID="ddlMContPermCountry" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Res. Phone No 
                                            </label>
                                            <asp:TextBox ID="txtMContPermPhoneNo" runat="server" TabIndex="10" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                P.O Box/Zip Code</label>
                                            <asp:TextBox ID="txtMContPermPOBOX" runat="server" MaxLength="20" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-Head-small">
                                    Current Address 
                                </div>
                                <div class="row  mb-2">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Country</label>
                                            <asp:DropDownList ID="ddlMContCurrCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                City/State</label>
                                            <asp:DropDownList ID="ddlMCity" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="ddlFCityM_other" style="display: none">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                If you choose other,please specify the Area Name</label>
                                            <asp:TextBox ID="txtMContCurrCity" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Area</label>
                                            <asp:DropDownList ID="ddlMArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="ddlFAreaM_other" style="display: none">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                If you choose other,please specify the Area Name</label>
                                            <asp:TextBox ID="txtMContCurArea" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Street</label>
                                            <asp:TextBox ID="txtMContCurStreet" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Building</label>
                                            <asp:TextBox ID="txtMContCurrBldg" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Apartment No</label>
                                            <asp:TextBox ID="txtMContCurrApartNo" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                City/Emirate</label>
                                            <asp:DropDownList ID="ddlMContCurrPOBOX_EMIR" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                P.O Box</label>
                                            <asp:TextBox ID="txtMContCurrPOBox" runat="server" MaxLength="20" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Res. Phone No
                                            </label>
                                            <asp:TextBox ID="txtMContCurrPhoneNo" runat="server" TabIndex="4" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Mobile No
                                            </label>
                                            <asp:TextBox ID="txtMContCurrMobNo" runat="server" TabIndex="5" CssClass="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>


                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-Head-small">
                                    Communication Details
                                </div>
                                <div class="row  mb-2">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Email 
                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                            <asp:TextBox ID="txtMContEmail" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Occupation</label>
                                            <asp:TextBox ID="txtMOcc" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Company</label>
                                            <asp:DropDownList ID="ddlMCompany" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="Select" Value="-1" />
                                                <asp:ListItem Text="other" Value="0" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="display: none;" id="ddlCompanyM_other">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                (If you choose other,please specify the Company Name) 
                                            </label>
                                            <asp:TextBox ID="txtMCompany" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Office Phone</label>
                                            <asp:TextBox ID="txtMOfficePhone" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Fax No.</label>
                                            <asp:TextBox ID="txtMFax" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix "></div>
                                <div class="title-bg-Form-small ">
                                    <div class="row">
                                        <div class="col-md-6">Guardian's Contact Details </div>
                                        <div class="col-md-6 text-right">
                                            <asp:CheckBox ID="chkCopyF_Details_Guard" runat="server" Text="Copy Father Details" onclick="javascript:return copyFathertoGuardian(this);" />
                                            <asp:CheckBox ID="chkCopyM_Details_Guard" runat="server" Text="Copy Mother Details" onclick="javascript:return copyMothertoGuardian(this);" />
                                        </div>
                                    </div>

                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="Ftitle">Full Name As Per Passport</label>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtGFirstName" runat="server" MaxLength="100" placeholder="First Name" CssClass="form-control"> </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvGFirstName" runat="server" ControlToValidate="txtGFirstName" Visible="false"
                                                        Display="Dynamic" EnableClientScript="false" ErrorMessage="Primary contact Guardian's first name required"
                                                        ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtGFirstName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                        TargetControlID="txtGFirstName" ValidChars="-\./' ">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtGMidName" runat="server" MaxLength="100" placeholder="Middle Name" CssClass="form-control">  </asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtGMidName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                        TargetControlID="txtGMidName" ValidChars="-\./' ">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtGLastName" runat="server" MaxLength="100" placeholder="Last Name" CssClass="form-control"> </asp:TextBox>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FTBX_txtGLastName" runat="server" FilterType="Custom,UppercaseLetters,LowercaseLetters"
                                                        TargetControlID="txtGLastName" ValidChars="-\./' ">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </div>
                                            </div>


                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Nationality 
                                            </label>
                                            <asp:DropDownList ID="ddlGNationality1" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Nationality 2 
                                            </label>
                                            <asp:DropDownList ID="ddlGNationality2" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-Head-small">
                                    Permanent Address
                                </div>
                                <div class="row  mb-2">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Line 1</label>
                                            <asp:TextBox ID="txtGContPermAddress1" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Line 2</label>
                                            <asp:TextBox ID="txtGContPermAddress2" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address City</label>
                                            <asp:TextBox ID="txtGContPermCity" runat="server" MaxLength="70" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Address Country</label>
                                            <asp:DropDownList ID="ddlGContPermCountry" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Res. Phone No 
                                            </label>
                                            <asp:TextBox ID="txtGContPermPhoneNo" runat="server" TabIndex="10" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                P.O Box/Zip Code</label>
                                            <asp:TextBox ID="txtGContPermPOBOX" runat="server" MaxLength="20" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-Head-small">
                                    Current Address 
                                </div>
                                <div class="row  mb-2">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Country</label>
                                            <asp:DropDownList ID="ddlGContCurrCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                City/State</label>
                                            <asp:DropDownList ID="ddlGCity" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="ddlFCityG_other" style="display: none">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                If you choose other,please specify the Area Name</label>
                                            <asp:TextBox ID="txtGContCurrCity" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Area</label>
                                            <asp:DropDownList ID="ddlGArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="ddlFAreaG_other" style="display: none">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                If you choose other,please specify the Area Name</label>
                                            <asp:TextBox ID="txtGContCurArea" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Street</label>
                                            <asp:TextBox ID="txtGContCurStreet" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Building</label>
                                            <asp:TextBox ID="txtGContCurrBldg" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Apartment No</label>
                                            <asp:TextBox ID="txtGContCurrApartNo" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                City/Emirate</label>
                                            <asp:DropDownList ID="ddlGContCurrPOBOX_EMIR" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                P.O Box</label>
                                            <asp:TextBox ID="txtGContCurrPOBox" runat="server" MaxLength="20" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Res. Phone No
                                            </label>
                                            <asp:TextBox ID="txtGContCurrPhoneNo" runat="server" TabIndex="4" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Mobile No
                                            </label>
                                            <asp:TextBox ID="txtGContCurrMobNo" runat="server" TabIndex="5" CssClass="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>


                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-Head-small">
                                    Communication Details
                                </div>
                                <div class="row  mb-2">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Email 
                            <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label></label>
                                            <asp:TextBox ID="txtGContEmail" runat="server" MaxLength="100" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Occupation</label>
                                            <asp:TextBox ID="txtGOcc" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Company</label>
                                            <asp:DropDownList ID="ddlGCompany" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="Select" Value="-1" />
                                                <asp:ListItem Text="other" Value="0" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="display: none;" id="ddlCompanyG_other">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                (If you choose other,please specify the Company Name) 
                                            </label>
                                            <asp:TextBox ID="txtGCompany" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Office Phone</label>
                                            <asp:TextBox ID="txtGOfficePhone" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Fax No.</label>
                                            <asp:TextBox ID="txtGFax" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix "></div>
                                <div class="title-bg-Form-small ">
                                    Other Info
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">Receive Sms</label>
                                            <div class="RB_Holder">
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rdSmsYes" Text="Yes" GroupName="GroupSms" runat="server" />
                                                </label>
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rdSmsNo" Text="No" GroupName="GroupSms" runat="server" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">Receive Promotional Emails</label>
                                            <div class="RB_Holder">
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rdEmailYes" Text="Yes" GroupName="GroupEmail" runat="server" />
                                                </label>
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rdEmailNo" Text="No" GroupName="GroupEmail" runat="server" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">Publication/promotion photos and videos</label>
                                            <div class="RB_Holder">
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rbPubYES" runat="server" GroupName="Pub" Text="Yes" />
                                                </label>
                                                <label class="radio-inline radio radio-success">
                                                    <asp:RadioButton ID="rbPubNo" runat="server" GroupName="Pub" Text="No" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                How did you hear about
                                                <asp:Literal ID="ltAboutUs" runat="server"></asp:Literal>
                                            </label>
                                            <div id="Div1" runat="server" class="checkbox-list form-control">
                                                <div class="checkbox checkbox-success">
                                                    <asp:CheckBoxList ID="chkAboutUs" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="styled">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Are there any family circumstances of which you feel we should be aware of?
                            <br />
                                                (eg Deceased parent /divorced/separated / adopted /others if so please give detail)
                                            </label>

                                            <asp:TextBox ID="txtFamily_NOTE" runat="server" Rows="2" CssClass="form-control inputbox_multi"
                                                SkinID="MultiText" TabIndex="22" TextMode="MultiLine"
                                                MaxLength="255"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>

                            </asp:Panel>

                        </div>
                        <div>
                            <asp:Panel ID="vTransport" runat="server" Width="100%">
                                <asp:Panel runat="server" ID="Panel1">
                                    <div class="clearfix "></div>
                                    <div class="title-bg-Form-small ">
                                        Transport 
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-md-3">

                                            <div class="form-group">

                                                <label>Transport Required :  </label>
                                                <div class="RB_Holder">
                                                    <label class="radio-inline radio radio-success">
                                                        <asp:RadioButton ID="rdTYes" Text="Yes" GroupName="GroupTRS" runat="server" AutoPostBack="True" />
                                                    </label>
                                                    <label class="radio-inline radio radio-success">
                                                        <asp:RadioButton ID="rdTNo" Text="No" GroupName="GroupTRS" runat="server" AutoPostBack="True" />
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="Ftitle">
                                                    Main Location</label>
                                                <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="Ftitle">
                                                    Sub Location</label>
                                                <asp:DropDownList ID="ddlSubLocation" runat="server" AutoPostBack="True" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="Ftitle">
                                                    Pick Up Point</label>
                                                <asp:DropDownList ID="ddlPickUp" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="Ftitle">
                                                    Remarks</label>
                                                <asp:TextBox SkinID="MultiText" ID="txtTptRemarks" runat="server" TextMode="MultiLine" MaxLength="255"
                                                    CssClass="form-control inputbox_multi">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="Ftitle">
                                                    Transport Status</label>
                                                <asp:Label ID="lblTptStatus" runat="server" Text="Label" CssClass="form-control"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="Panel2">
                                    <div class="clearfix "></div>
                                    <div class="title-bg-Form-small ">
                                        Other Services
                                    </div>
                                    <div class="row mb-2">
                                        <asp:GridView ID="gvServices" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" BorderStyle="None">
                                            <Columns>

                                                <asp:TemplateField Visible="False">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSvcId" runat="server" Text='<%# Bind("SVC_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Service">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblService" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:RadioButton ID="rdYes" Text="Yes" Checked='<%# Bind("YES") %>' runat="server" GroupName='<%# Eval("SVC_ID") %>' />
                                                        <asp:RadioButton ID="rdNo" Text="No" Checked='<%# Bind("NO") %>' runat="server" GroupName='<%# Eval("SVC_ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                            <RowStyle CssClass="griditem" Wrap="False" />
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                        </asp:GridView>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </div>
                        <div>
                            <asp:Panel ID="vOther" runat="server" Width="100%">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="VALSALDET" />
                                <div class=" title-bg-Form-small">
                                    Health
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Health Card No / Medical Insurance No</label>
                                            <asp:TextBox ID="txtHealtCard" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="Ftitle">
                                                Blood Group</label>
                                            <asp:DropDownList ID="ddlBgroup" runat="server" CssClass="form-control">
                                                <asp:ListItem>--</asp:ListItem>
                                                <asp:ListItem Value="AB+">AB+</asp:ListItem>
                                                <asp:ListItem Value="AB-">AB-</asp:ListItem>
                                                <asp:ListItem Value="B+">B+</asp:ListItem>
                                                <asp:ListItem Value="A+">A+</asp:ListItem>
                                                <asp:ListItem Value="B-">B-</asp:ListItem>
                                                <asp:ListItem Value="A-">A-</asp:ListItem>
                                                <asp:ListItem Value="O+">O+</asp:ListItem>
                                                <asp:ListItem Value="O-">O-</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Health Restriction
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Allergies</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthAll_Yes" runat="server" GroupName="Allerg" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthAll_No" runat="server" GroupName="Allerg" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthAll_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Disabled ?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthDisabled_YES" runat="server" GroupName="HthDisabled" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthDisabled_No" runat="server" GroupName="HthDisabled" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthDisabled_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Special Medication</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSM_Yes" runat="server" GroupName="SPMed" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSM_No" runat="server" GroupName="SPMed" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthSM_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Physical Education Restrictions</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthPER_Yes" runat="server" GroupName="PhyEd" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthPER_No" runat="server" GroupName="PhyEd" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthPER_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Any other information related to health issue the school should be aware of?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthOther_yes" runat="server" GroupName="HthOther" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthOther_No" runat="server" GroupName="HthOther" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthOth_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Applicant's disciplinary, social,physical or psychological detail
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Has the child received any sort of learning support or theraphy?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthLS_Yes" runat="server" GroupName="LearnSp" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthLS_No" runat="server" GroupName="LearnSp" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthLS_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Does the child require any special education needs?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSE_Yes" runat="server" GroupName="SPneed" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSE_No" runat="server" GroupName="SPneed" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthSE_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Does the student require English support as Additional Language program (EAL) ?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthEAL_Yes" runat="server" GroupName="EAL" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthEAL_No" runat="server" GroupName="EAL" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthEAL_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Has your child's behaviour been any cause for concern in previous schools ?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthBehv_Yes" runat="server" GroupName="Behv" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthBehv_No" runat="server" GroupName="Behv" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        If yes, please explain and include the name of the school and principal</label>
                                                    <asp:TextBox ID="txtHthBehv_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Any information related to communication & interaction that the school should be aware of ?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthComm_Yes" runat="server" GroupName="CommInt" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthComm_No" runat="server" GroupName="CommInt" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthCommInt_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class=" title-bg-Form-small">
                                    Gifted and talented
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Has your child ever been selected for specific enrichment activities?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthEnr_Yes" runat="server" GroupName="Enr" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthEnr_No" runat="server" GroupName="Enr" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthEnr_note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Is your child musically proficient?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthMus_Yes" runat="server" GroupName="Music" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthMus_No" runat="server" GroupName="Music" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthMus_Note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="Ftitle">Has your child represented a school or country in sport?</label>
                                                    <div class="RB_Holder">
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSport_Yes" runat="server" GroupName="Sport" Text="Yes" />
                                                        </label>
                                                        <label class="radio-inline radio radio-success">
                                                            <asp:RadioButton ID="rbHthSport_No" runat="server" GroupName="Sport" Text="No" />
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label class="Ftitle">
                                                        Notes</label>
                                                    <asp:TextBox ID="txtHthSport_note" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                        CssClass="inputbox_multi form-control" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div>
                            <asp:Panel ID="vwSalary" runat="server" Width="100%">
                                <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="VALSALDET" />
                                <div class="clearfix "></div>
                                <div class="title-bg-Form-small ">
                                    Nursery Details(for KG1 Admissions)
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-3">
                                        <div class="form-group">

                                            <label class="Ftitle ">Sister Nursery   </label>
                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkNursery" runat="server" Text="Yes"></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle ">Reg No </label>
                                              <asp:TextBox ID="txtNRegNo" runat="server"  TabIndex="22" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                      <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle ">Unit </label>
                                              <asp:DropDownList ID="ddlNUnit" runat="server"  CssClass="form-control">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                 <div class="clearfix "></div>
                                <div class="title-bg-Form-small ">
                                  Sibling Details
                                </div>
                               <div class="row mb-2">
                                    <div class="col-md-3">
                                        <div class="form-group">

                                            <label class="Ftitle ">Siblings Exist   </label>
                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkSibling" runat="server" Text="Yes"></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle ">Fee Id </label>
                                              <asp:TextBox ID="txtSRegNo" runat="server"  TabIndex="22" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                      <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle ">Unit </label>
                                               <asp:DropDownList ID="ddlSUnit" runat="server" CssClass="form-control" >
                                            <asp:ListItem Value="0">GEMS ROYAL DUBAi SCHOOL</asp:ListItem>
                                        </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                    <div class="clearfix "></div>
                                <div class="title-bg-Form-small ">
                                  GEMS Staff Details
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-3">
                                        <div class="form-group">

                                            <label class="Ftitle ">Parent work with GEMS   </label>
                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkStaffGEMS" runat="server" Text="Yes"></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle ">Staff Id</label>
                                              <asp:TextBox ID="txtSPayRollId" runat="server"  TabIndex="22" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                      <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle ">Unit </label>
                                              <asp:DropDownList ID="ddlStaffUnit" runat="server"  CssClass="form-control">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                 <div class="clearfix "></div>
                                <div class="title-bg-Form-small ">
                            Ex Student
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-3">
                                        <div class="form-group">

                                            <label class="Ftitle ">Ex Student   </label>
                                            <label class="checkbox-inline checkbox checkbox-success">
                                                <asp:CheckBox ID="chkExStudent" runat="server" Text="Yes"></asp:CheckBox>
                                            </label>
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle ">Fee Id</label>
                                              <asp:TextBox ID="txtERegNo" runat="server"  TabIndex="22" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                      <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="Ftitle ">Unit </label>
                                              <asp:DropDownList ID="ddlEUnit" runat="server"  CssClass="form-control">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center mt-3">
                   <asp:Button ID="btnAdd" runat="server" CssClass="button" OnClick="btnAdd_Click" Text="Add" CausesValidation="False" />
                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-success btn-sm" Text="Save"
                                ValidationGroup="groupM1" OnClick="btnSave_Click" />
                            <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="False"
                                    CssClass="btn btn-outline-success btn-sm" Text="Cancel" OnClick="btnCancel_Click" />
            </div>
            <div class="col-12 text-center">


                
                <ajaxToolkit:ModalPopupExtender ID="mpxFeeSponsor" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="true" PopupControlID="plFeeSP" TargetControlID="lbtnFeeSpon">
                </ajaxToolkit:ModalPopupExtender>
                <asp:HiddenField ID="hfENQNo" runat="server" />
                <asp:HiddenField ID="hfENQ_ID" runat="server" />
                <asp:HiddenField ID="hfEQP_ID" runat="server" />
                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <asp:HiddenField ID="hfEQS_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfSTGREQ" runat="server" />
                <asp:HiddenField ID="hfURL" runat="server" />
                <asp:HiddenField ID="HFEMP_ID" runat="server" />
                <asp:HiddenField ID="hfACCNO" runat="server" />
                <asp:HiddenField ID="hfActual_GRD" runat="server" />
                  <asp:Panel ID="plFeeSP" runat="server" Width="541px" Style="display: none" BackColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="2px">
                    <uc1:FeeSponsor ID="FeeSponsor1" runat="server" />
                </asp:Panel>

            </div>
        </div>
    </div>

    <uc2:usrMessageBar runat="server" ID="usrMessageBar" />

    <script language="javascript" type="text/javascript">


        function getDocuments() {
            var sFeatures;
            sFeatures = "dialogWidth: 700px; ";
            sFeatures += "dialogHeight: 400px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            // url='../Students/ShowAcademicInfo.aspx?id='+mode;

            url = document.getElementById("<%=hfURL.ClientID %>").value

            window.showModalDialog(url, "", sFeatures);
            return false;

        }


        function getPageCode(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 650px; ";
            sFeatures += "dialogHeight: 370px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;


        }




        function getDocDocument() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;


        }

        function getCostUnit() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

        }

        function getCity() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;


        }

        function getEMPQUALIFICATION() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 445px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;


        }
        function getGrade() {

        }

        function AddDetails(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 550px; ";
            sFeatures += "dialogHeight: 450px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;

        }
        function UploadPhoto() {
            //        document.forms[0].Submit();

        }

        function confirm_cancel() {

            if (confirm("You are about to cancel this enquiry.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }



        function Disable_Text(ddlId) {
            var ControlName = document.getElementById(ddlId.id);



            //   var myVal = document.getElementById('myValidatorClientID');

            if (ControlName.value == '0')  //it depends on which value Selection do u want to hide or show your textbox 
            {

                document.getElementById('<%=txtFCompany.ClientID %>').value = '';
                 document.getElementById('<%=txtFCompany.ClientID %>').readOnly = "";


             }
             else {
                 document.getElementById('<%=txtFCompany.ClientID %>').value = '';
                 document.getElementById('<%=txtFCompany.ClientID %>').readOnly = "readonly";




             }
         }

         function GetSelectedItem() {
             var e = document.getElementById("<%= ddlFLang.ClientID%>");
       var fLang = e.options[e.selectedIndex].text;
       var CHK = document.getElementById("<%= chkOLang.ClientID%>");

var checkbox = CHK.getElementsByTagName("input");

var label = CHK.getElementsByTagName("label");

for (var i = 0; i < checkbox.length; i++) {

    if (checkbox[i].checked) {
        var olang = label[i].innerHTML
        if (fLang == olang) {
            checkbox[i].checked = false;
        }
    }

}

}




function chkFirst_lang() {

    GetSelectedItem();

}


function copyFathertoMother(chkThis) {
    var chk_state = chkThis.checked;
    if (chk_state == true) {
        document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality1.ClientID %>').selectedIndex;
          document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality2.ClientID %>').selectedIndex;
          document.getElementById('<%=txtMContCurStreet.ClientID %>').value = document.getElementById('<%=txtFContCurStreet.ClientID %>').value;
          document.getElementById('<%=txtMContPermAddress1.ClientID %>').value = document.getElementById('<%=txtFContPermAddress1.ClientID %>').value;
          document.getElementById('<%=txtMContCurArea.ClientID %>').value = document.getElementById('<%=txtFContCurArea.ClientID %>').value;
          document.getElementById('<%=txtMContPermAddress2.ClientID %>').value = document.getElementById('<%=txtFContPermAddress2.ClientID %>').value;
          document.getElementById('<%=txtMContCurrBldg.ClientID %>').value = document.getElementById('<%=txtFContCurrBldg.ClientID %>').value;
          document.getElementById('<%=txtMContPermCity.ClientID %>').value = document.getElementById('<%=txtFContPermCity.ClientID %>').value
          document.getElementById('<%=txtMContCurrApartNo.ClientID %>').value = document.getElementById('<%=txtFContCurrApartNo.ClientID %>').value;
          document.getElementById('<%=ddlMContPermCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContPermCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtMContCurrPOBox.ClientID %>').value = document.getElementById('<%=txtFContCurrPOBox.ClientID %>').value;
          document.getElementById('<%=txtMContPermPhoneNo.ClientID %>').value = document.getElementById('<%=txtFContPermPhoneNo.ClientID %>').value;
          document.getElementById('<%=ddlMContCurrPOBOX_EMIR.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContCurrPOBOX_EMIR.ClientID %>').selectedIndex;
          document.getElementById('<%=txtMContPermPOBOX.ClientID %>').value = document.getElementById('<%=txtFContPermPOBOX.ClientID %>').value;
          document.getElementById('<%=txtMContCurrCity.ClientID %>').value = document.getElementById('<%=txtFContCurrCity.ClientID %>').value;
          document.getElementById('<%=ddlMContCurrCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContCurrCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtMContCurrPhoneNo.ClientID %>').value = document.getElementById('<%=txtFContCurrPhoneNo.ClientID %>').value;
          document.getElementById('<%=txtMContCurrMobNo.ClientID %>').value = document.getElementById('<%=txtFContCurrMobNo.ClientID %>').value;

      }
      else {
          document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtMContCurStreet.ClientID %>').value = "";
          document.getElementById('<%=txtMContPermAddress1.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurArea.ClientID %>').value = "";
          document.getElementById('<%=txtMContPermAddress2.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurrBldg.ClientID %>').value = "";
          document.getElementById('<%=txtMContPermCity.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurrApartNo.ClientID %>').value = "";
          document.getElementById('<%=ddlMContPermCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtMContCurrPOBox.ClientID %>').value = "";
          document.getElementById('<%=txtMContPermPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=ddlMContCurrPOBOX_EMIR.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtMContPermPOBOX.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurrCity.ClientID %>').value = "";
          document.getElementById('<%=ddlMContCurrCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtMContCurrPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=txtMContCurrMobNo.ClientID %>').value = "";

      }
      return true;
  }

  function copyFathertoGuardian(chkThis) {
      var chk_state = chkThis.checked;
      if (chk_state == true) {
          document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality1.ClientID %>').selectedIndex;
          document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = document.getElementById('<%=ddlFNationality2.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurStreet.ClientID %>').value = document.getElementById('<%=txtFContCurStreet.ClientID %>').value;
          document.getElementById('<%=txtGContPermAddress1.ClientID %>').value = document.getElementById('<%=txtFContPermAddress1.ClientID %>').value;
          document.getElementById('<%=txtGContCurArea.ClientID %>').value = document.getElementById('<%=txtFContCurArea.ClientID %>').value;
          document.getElementById('<%=txtGContPermAddress2.ClientID %>').value = document.getElementById('<%=txtFContPermAddress2.ClientID %>').value;
          document.getElementById('<%=txtGContCurrBldg.ClientID %>').value = document.getElementById('<%=txtFContCurrBldg.ClientID %>').value;
          document.getElementById('<%=txtGContPermCity.ClientID %>').value = document.getElementById('<%=txtFContPermCity.ClientID %>').value
          document.getElementById('<%=txtGContCurrApartNo.ClientID %>').value = document.getElementById('<%=txtFContCurrApartNo.ClientID %>').value;
          document.getElementById('<%=ddlGContPermCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContPermCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurrPOBox.ClientID %>').value = document.getElementById('<%=txtFContCurrPOBox.ClientID %>').value;
          document.getElementById('<%=txtGContPermPhoneNo.ClientID %>').value = document.getElementById('<%=txtFContPermPhoneNo.ClientID %>').value;
          document.getElementById('<%=ddlGContCurrPOBOX_EMIR.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContCurrPOBOX_EMIR.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContPermPOBOX.ClientID %>').value = document.getElementById('<%=txtFContPermPOBOX.ClientID %>').value;
          document.getElementById('<%=txtGContCurrCity.ClientID %>').value = document.getElementById('<%=txtFContCurrCity.ClientID %>').value;
          document.getElementById('<%=ddlGContCurrCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFContCurrCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurrPhoneNo.ClientID %>').value = document.getElementById('<%=txtFContCurrPhoneNo.ClientID %>').value;
          document.getElementById('<%=txtGContCurrMobNo.ClientID %>').value = document.getElementById('<%=txtFContCurrMobNo.ClientID %>').value;
      }
      else {
          document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurStreet.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermAddress1.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurArea.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermAddress2.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrBldg.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermCity.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrApartNo.ClientID %>').value = "";
          document.getElementById('<%=ddlGContPermCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrPOBox.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=ddlGContCurrPOBOX_EMIR.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContPermPOBOX.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrCity.ClientID %>').value = "";
          document.getElementById('<%=ddlGContCurrCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrMobNo.ClientID %>').value = "";
      }
      return true;
  }

  function copyMothertoGuardian(chkThis) {
      var chk_state = chkThis.checked;
      if (chk_state == true) {
          document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex;
          document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurStreet.ClientID %>').value = document.getElementById('<%=txtMContCurStreet.ClientID %>').value;
          document.getElementById('<%=txtGContPermAddress1.ClientID %>').value = document.getElementById('<%=txtMContPermAddress1.ClientID %>').value;
          document.getElementById('<%=txtGContCurArea.ClientID %>').value = document.getElementById('<%=txtMContCurArea.ClientID %>').value;
          document.getElementById('<%=txtGContPermAddress2.ClientID %>').value = document.getElementById('<%=txtMContPermAddress2.ClientID %>').value;
          document.getElementById('<%=txtGContCurrBldg.ClientID %>').value = document.getElementById('<%=txtMContCurrBldg.ClientID %>').value;
          document.getElementById('<%=txtGContPermCity.ClientID %>').value = document.getElementById('<%=txtMContPermCity.ClientID %>').value
          document.getElementById('<%=txtGContCurrApartNo.ClientID %>').value = document.getElementById('<%=txtMContCurrApartNo.ClientID %>').value;
          document.getElementById('<%=ddlGContPermCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlMContPermCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurrPOBox.ClientID %>').value = document.getElementById('<%=txtMContCurrPOBox.ClientID %>').value;
          document.getElementById('<%=txtGContPermPhoneNo.ClientID %>').value = document.getElementById('<%=txtMContPermPhoneNo.ClientID %>').value;
          document.getElementById('<%=ddlGContCurrPOBOX_EMIR.ClientID %>').selectedIndex = document.getElementById('<%=ddlMContCurrPOBOX_EMIR.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContPermPOBOX.ClientID %>').value = document.getElementById('<%=txtMContPermPOBOX.ClientID %>').value;
          document.getElementById('<%=txtGContCurrCity.ClientID %>').value = document.getElementById('<%=txtMContCurrCity.ClientID %>').value;
          document.getElementById('<%=ddlGContCurrCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlMContCurrCountry.ClientID %>').selectedIndex;
          document.getElementById('<%=txtGContCurrPhoneNo.ClientID %>').value = document.getElementById('<%=txtMContCurrPhoneNo.ClientID %>').value;
          document.getElementById('<%=txtGContCurrMobNo.ClientID %>').selectedIndex = document.getElementById('<%=txtMContCurrMobNo.ClientID %>').selectedIndex;

      }
      else {
          document.getElementById('<%=ddlGNationality1.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=ddlGNationality2.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurStreet.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermAddress1.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurArea.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermAddress2.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrBldg.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermCity.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrApartNo.ClientID %>').value = "";
          document.getElementById('<%=ddlGContPermCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrPOBox.ClientID %>').value = "";
          document.getElementById('<%=txtGContPermPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=ddlGContCurrPOBOX_EMIR.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContPermPOBOX.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrCity.ClientID %>').value = "";
          document.getElementById('<%=ddlGContCurrCountry.ClientID %>').selectedIndex = 0;
          document.getElementById('<%=txtGContCurrPhoneNo.ClientID %>').value = "";
          document.getElementById('<%=txtGContCurrMobNo.ClientID %>').value = "";
      }
      return true;
  }





    </script>
    <script>

        function Runfunction() {
         <%--   $('#' +'<%=ddlEthnicity.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlEthnicity_Other").show();
                } else {
                    $("#ddlEthnicity_Other").hide();
                }

            });

            $('#' +'<%=chkRef.ClientID %>').change(function () {
                if (this.checked) {
                    $("#referred_to_GEMS").show();
                } else {
                    $("#referred_to_GEMS").hide();
                }

            });
          
            //ctl00_cphMasterpage_chkFam_Oth
            $('#' +'<%=chkFam_Oth.ClientID %>').change(function () {
                if (this.checked) {
                    $("#chkFam_Oth_Others").show();
                } else {
                    $("#chkFam_Oth_Others").hide();
                }

            });--%>
            $('#' + '<%=ddlFCompany.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlCompany_other").show();
                } else {
                    $("#ddlCompany_other").hide();
                }

            });
            $('#' + '<%=ddlMCompany.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlCompanyM_other").show();
                } else {
                    $("#ddlCompanyM_other").hide();
                }

            });
            $('#' + '<%=ddlGCompany.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlCompanyG_other").show();
                } else {
                    $("#ddlCompanyG_other").hide();
                }

            });
            $('#' + '<%=ddlPSchool.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlPSchool_other").show();
                } else {
                    $("#ddlPSchool_other").hide();
                }

            });
            $('#' + '<%=ddlPreSchool_Nursery.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlPreSchool_Nursery_other").show();
                } else {
                    $("#ddlPreSchool_Nursery_other").hide();
                }

            });
            $('#' + '<%=ddlFArea.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlFArea_other").show();
                } else {
                    $("#ddlFArea_other").hide();
                }

            });
            $('#' + '<%=ddlMArea.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlFAreaM_other").show();
                } else {
                    $("#ddlFAreaM_other").hide();
                }

            });
            $('#' + '<%=ddlGArea.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlFAreaG_other").show();
                } else {
                    $("#ddlFAreaG_other").hide();
                }

            });
            $('#' + '<%=ddlFCity.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlFCity_other").show();
                } else {
                    $("#ddlFCity_other").hide();
                }

            });
            $('#' + '<%=ddlMCity.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlFCityM_other").show();
                } else {
                    $("#ddlFCityM_other").hide();
                }

            });
            $('#' + '<%=ddlGCity.ClientID %>').change(function () {
                if (this.value == "0") {
                    $("#ddlFCityG_other").show();
                } else {
                    $("#ddlFCityG_other").hide();
                }

            });

        }
        Runfunction();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    Runfunction();

                }
            });
        };
    </script>

    <script type="text/javascript">
        function ResponsiveTabs() {
            $('#parentHorizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                },
            });
        }
        $(document).ready(function () {
            //Horizontal Tab
            ResponsiveTabs();

        });
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    ResponsiveTabs();
                }
            });
        };
</script>
</asp:Content>

