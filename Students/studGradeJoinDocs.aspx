<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studGradeJoinDocs.aspx.vb" Inherits="Students_studGradeJoinDocs" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Grade Join Documents"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                  
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:LinkButton ID="lnkBack" runat="server">Back</asp:LinkButton></td>
                    </tr>

                    <tr>
                        <td align="center">

                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">

                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">


                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gvStage" runat="server" CssClass="table table-bordered table-row"
                                                        AutoGenerateColumns="False"
                                                        OnRowDataBound="gvStage_RowDataBound" PageSize="20" Width="100%">
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="-" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStgId" runat="server" Text='<%# Bind("PRG_STG_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStage" runat="server" Text='<%# Bind("PRO_DESCRIPTION") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkProceed" ToolTip='<%# Eval("PRG_STG_ID") %>' AutoPostBack="true" OnCheckedChanged="chkProceed_CheckChanged" Text="Proceed to next stage without document collection" runat="server"></asp:CheckBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                </td></tr>
             <tr>
                 <td colspan="100%">
                     <table>
                         <tr>
                             <td class="matters">Not Required  
               <br />
                                 <asp:ListBox ID="lstNotRequired" Style="overflow: auto" SelectionMode="Multiple"  runat="server"></asp:ListBox>
                             </td>
                             <td>
                                 <asp:Button ID="btnRight" CommandName="Select" OnClick="btnRight_Click"  CssClass="button" runat="server" Text=">>" /><br />
                                 <asp:Button ID="btnLeft" CommandName="Select" OnClick="btnLeft_Click" CssClass="button" runat="server" Text="<<" />
                             </td>
                             <td class="matters">Required
               <br />
                                 <asp:ListBox ID="lstRequired" Font-Size="xx-Small" SelectionMode="Multiple" Style="overflow: auto"  runat="server"></asp:ListBox>
                             </td>
                         </tr>
                     </table>
                 </td>
             </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />

                                                        <RowStyle CssClass="griditem_alternative" />
                                                        <PagerStyle  HorizontalAlign="Left" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfSTM_ID" runat="server" />
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /></td>
                    </tr>

                </table>



            </div>
        </div>
    </div>

</asp:Content>

