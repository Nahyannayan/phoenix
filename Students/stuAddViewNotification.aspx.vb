﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Data
Imports System.IO
Imports System.Type
Imports System.Text
Imports UtilityObj
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Partial Class Students_stuAddNotifications
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Dim Latest_refid As Integer = 0
    Private Property VSgridNotifications() As DataTable
        Get
            Return ViewState("Notifications")
        End Get
        Set(ByVal value As DataTable)
            ViewState("Notifications") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        If Not IsPostBack Then
            Dim MainMnu_code As String
            Try
                If Session("sUsr_name") = "" Or Session("sBsuid") = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If
                    Page.Title = OASISConstants.Gemstitle
                    ShowMessage("", False)
                    ViewState("datamode") = "none"
                    If Request.QueryString("MainMnu_code") <> "" Then
                        MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                        ViewState("MainMnu_code") = MainMnu_code
                    Else
                        MainMnu_code = ""
                    End If
                    If Request.QueryString("datamode") <> "" Then
                        ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    Else
                        ViewState("datamode") = ""
                    End If
                    If Request.QueryString("viewid") <> "" Then
                        ViewState("ViewId") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    Else
                        ViewState("ViewId") = ""
                    End If
                    ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), Session("sBsuid"), MainMnu_code)
                    ShowMessage("", True) 
                    viewidUpdate.Value = 0
                    'disable the control based on the rights 
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), "add")
                    ViewState("EmployeeID") = EOS_MainClass.GetEmployeeIDFromUserName(Session("sUsr_name"))
                    ViewState("EmployeeName") = EOS_MainClass.GetEmployeeNameFromID(ViewState("EmployeeID"))

                    FillGridView() 
                End If
            Catch ex As Exception
                Message = getErrorMessage("4000")
                ShowMessage(Message, True)
                UtilityObj.Errorlog("From Load: " + ex.Message, "OASIS ACTIVITY SERVICES")
            End Try
        End If
    End Sub
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "diverrorPopUp"
            Else
                lblError.CssClass = "divvalidPopUp"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub
     
    Protected Sub FillGridView()
        Try
            Dim str_conn As String = ConnectionManger.Get_PHOENIXMISC_ConnectionString
            Dim pParams(2) As SqlParameter
            pParams(0) = Mainclass.CreateSqlParameter("@EMPID", ViewState("EmployeeID").ToString, SqlDbType.VarChar)
            pParams(1) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid").ToString, SqlDbType.VarChar)
            pParams(2) = Mainclass.CreateSqlParameter("@ACD_ID", Session("Current_ACD_ID").ToString, SqlDbType.BigInt)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "[dbo].[GET_ALLNOTIFICATIONS_TOSHOW]", pParams)
            If Not ds Is Nothing And ds.Tables.Count > 0 Then
                Dim dt As DataTable = ds.Tables(0)
                VSgridNotifications = dt
                GridBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridBind()
        Try
            If Not VSgridNotifications Is Nothing Then
                gridAllNotificationsList.DataSource = VSgridNotifications
                gridAllNotificationsList.DataBind()
            Else

            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
        End Try
    End Sub
 

    Protected Sub gridAllNotificationsList_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            Me.gridAllNotificationsList.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
        End Try
    End Sub

    Protected Sub gridAllNotificationsList_RowDataBound(sender As Object, e As GridViewRowEventArgs)

        'NLD_ISREAD_FLAG
        If e.Row.RowType = DataControlRowType.DataRow Then 

            Dim NLD_ISREAD_FLAG As Label = e.Row.FindControl("lblNLD_ISREAD_FLAG")
            'If NLD_ISREAD_FLAG.Text = "True" Then
            '    e.Row.Cells(7).Enabled = False
            '    e.Row.Cells(8).Enabled = False
            'End If
           
        End If
    End Sub
     
    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs)
        Response.Redirect("stuAddNotifications.aspx?MainMnu_code=" + Encr_decrData.Encrypt(ViewState("MainMnu_code")))
    End Sub
End Class
