<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studAtt_SummHouse.aspx.vb" Inherits="Students_studAtt_SummHouse" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd Xhtml 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">

<!DOCTYPE html PUBLIC "-//W3C//Dtd html 4.0 transitional//EN">
<!-- saved from url=(0038)http://live.gemsoasis.com/printenq.php -->
<html>
<head>
    <title>::::GEMS OASIS:::: Online Student Administration System::::</title>
    <link href="../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/example.css" rel="stylesheet" type="text/css" />
    <script language="Javascript" src="../FusionCharts/FusionCharts.js"></script>
    <script language="javascript" type="text/javascript">

        function ShowDetails(vid) {
            var result;
            result = window.open("../Students/Reports/ASPX/StudAttHouseDetail.aspx?vid=" + vid, "_blank")


        }


    </script>

</head>




<body topmargin="1px">
    <form id="Form1" runat="server">

       

                    <table cellspacing="0" cellpadding="5" width="100%" align="center" border="0">
                        <tbody>
                            <tr>
                                <td align="center" valign="middle">
                                    <table style="text-decoration: none; width: 100%;" cellspacing="0" cellpadding="0" align="center" border="0">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="left" width="15%">
                                                    <asp:Image ID="imglogo" runat="server" Height="80" />
                                                </td>
                                                <td align="left" width="35%" >
                                                    <table width="100%" border="0" align="center">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" valign="bottom"><font 
                                                                    style="text-decoration: none; font-family:Calibri; font-weight:bold; font-size:20px;"><span id="BsuName" runat="server"></span></font></td>
                                                            </tr>
                                                            <%--<tr>
                                                                <td style="background-color: #ffffff; text-align: center"><font
                                                                    style="text-decoration: none"><span id="bsuAddress" runat="server"></span></font></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="background-color: #ffffff; text-align: center"><font
                                                                    style="text-decoration: none"><span id="bsupostbox" runat="server"></span>, <span id="bsucity" runat="server"></span></font></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="background-color: #ffffff; text-align: center"><font
                                                                    style="text-decoration: none"><span id="bsutelephone" runat="server"></span>, <span id="bsufax" runat="server"></span>,<span id="bsuemail" runat="server"></span> : 
            <span id="bsuwebsite" runat="server"></span></font></td>
                                                            </tr>--%>

                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="50%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left">
                                                               <span style="font-family:Calibri;" > Report Title : </span>
                                                            </td>
                                                            <td align="left">
                                                                <span style="font-family:Calibri; font-weight:bold;" ><asp:Literal ID="ltHeader" runat="server" ></asp:Literal></span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>

                                                <td align="left" colspan="3">
                                                 <hr style="border-color:#00b300;" />
                                                    <%--<img alt=""
                                                        src="../Images/colourbar.jpg" width="100%" height="10px" />--%></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <%--<tr>
                                <td align="center" class="title" valign="middle">

                                    </td>
                            </tr>--%>
                            <tr>
                                <td align="left" class="title" valign="middle">
                                    <asp:ImageButton ID="imgbtnback" runat="server" AlternateText="Back" ImageUrl="~/Images/Misc/LEFT1.gif" />
                                    <a href="javascript:window.print()">
                                        <img src="../Images/Misc/print.gif" alt="Print this page" style="border-style: none;" /></a></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Literal ID="ltmain" runat="server"></asp:Literal>
                                    <br />
                                    <hr />
                                    <br />
                                    <asp:DataList ID="dlHouse_att" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                        ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hfHouse_id" runat="server" Value='<%# Eval("AVG_HOUSE_ID") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfHouse_Descr" runat="server" Value='<%# Eval("HOUSE_DESCRIPTION")%>'></asp:HiddenField>
                                            <asp:Literal ID="ltHouse_Lt" runat="server"></asp:Literal>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:HiddenField ID="HiddenBsuId" runat="server" />
                    <asp:HiddenField ID="HiddenAcademicyear" runat="server" />

               
    </form>
</body>

</html>
