﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="encSelUsername.aspx.vb" Inherits="Students_encSelUsername" %>

<%@ OutputCache Duration="1" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Cheque Selection</title>
   <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css"/>

     <base target="_self" />
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    
    
   
    <script language="javascript" type="text/javascript">
    function ChangeCheckBoxState(id, checkState)
        {
            var cb = document.getElementById(id);
            if (cb != null)
               cb.checked = checkState;
        }
        
        function ChangeAllCheckBoxStates(checkState)
        {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            var lstrChk = document.getElementById("chkAL").checked; 
           
            
            if (CheckBoxIDs != null)
            {
                for (var i = 0; i < CheckBoxIDs.length; i++)
                   ChangeCheckBoxState(CheckBoxIDs[i], lstrChk);
            }
        }
       
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
         </script>
</head>
<body  onload="listen_window();" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">

    <form id="form1" runat="server">
    <table id="tbl" width="100%" align="center">
                    <tr valign = "top">
                        <td  >
                            <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="False"
                              CssClass="table table-bordered table-row" PageSize="15" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="User Id" SortExpression="ID">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblID" runat="server" CssClass="gridheader_text"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCode" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnCodeSearch_Click" />
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BSU Name" SortExpression="NAME">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtBSUName" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnBSUNameSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="linklblBSUName" runat="server" Text='<%# Bind("DESCR") %>' OnClick="linklblBSUName_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                     <asp:TemplateField HeaderText="CODE"    >
                                         <HeaderTemplate>
                                             <asp:Label ID="lblCitCode" runat="server" CssClass="gridheader_text"></asp:Label>
                                             <br />
                                             <asp:TextBox ID="txtCitCode" runat="server" ></asp:TextBox>
                                             <asp:ImageButton ID="btnCitCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnNameSearch_Click" />
                                         </HeaderTemplate>
                                        <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Bind("DESCR2") %>'  ID="lblDataCode" ></asp:Label>
                                           
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        <input id="h_SelectedId" runat="server" type="hidden" value="" />
                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /></td>
                        
                    </tr> 
                </table>
            
           
    </form>
</body>
</html>
