<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studBSUJoinDoc_M.aspx.vb" Inherits="Students_studBSUJoinDoc_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Join Documents
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" 
                                ValidationGroup="groupM1"  />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                              <%--  <tr class="subheader_img">
                                    <td align="left" colspan="6" style="height: 14px" valign="middle">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                JOIN DOCUMENTS</span></font></td>
                                </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                        
                                    <td  align="left" width="30%">
                                        <asp:Label ID="lblacademicYear" runat="server" CssClass="field-value"></asp:Label></td>

                                    <td  align="left"
                                        colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Stage</span></td>
                                  
                                    <td  align="left">
                                        <asp:Label ID="lblStage" runat="server" Text="" CssClass="field-value"></asp:Label></td>
                                     <td align="left"  colspan="2" >
                                        <asp:CheckBox ID="chkProceed" runat="server" Text="Proceed to next stage without document collection" CssClass="field-label"/></td>
                                </tr>
                               


                                <tr >
                                    <td align="left" colspan="4" class="title-bg" >
                                        
                                Details</td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Document Description</span></td>
                    
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlDocs" runat="server" >
                                        </asp:DropDownList>
                                    </td>

                                     <td align="left"><span class="field-label">Document Type</span></td>
                    
                                    <td  align="left">

                                        <asp:RadioButton ID="rdOrg" runat="server" Text="Original" GroupName="g1" CssClass="field-label"/>
                                        <asp:RadioButton ID="rdDup" runat="server" Text="Duplicate" GroupName="g1" Checked="True" CssClass="field-label"/></td>
                                </tr>
                      
                                <tr>
                                    <td align="left" ><span class="field-label">No.Of Copies</span></td>
           
                                    <td align="left" >
                                        <asp:TextBox ID="txtCopies" runat="server" CausesValidation="True" ValidationGroup="groupM1"  TabIndex="2">1</asp:TextBox></td>
                                    <td align="left" ><span class="field-label">Apply to </span>
                                    </td>
                          
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlApply" runat="server">
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem Value="NAT">Locals</asp:ListItem>
                                            <asp:ListItem Value="EXP">Expats</asp:ListItem>
                                        </asp:DropDownList></td>

                                </tr>
                                <tr id="trGrade" runat="server">
                                    <td align="left" ><span class="field-label">Apply changes to grades</span></td>
                        
                                    <td align="left" colspan="3" >
                                        <asp:CheckBoxList ID="chkGrades" runat="server" RepeatDirection="Horizontal" RepeatColumns="6" CellSpacing="0" CellPadding="0" >
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr runat="server">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" TabIndex="3" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <br />
                                        <table id="Table5" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" colspan="9" class="matters">
                                                    <asp:GridView ID="gvDocs" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                       CssClass="table table-bordered table-row"
                                                        HeaderStyle-Height="30" PageSize="20" width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Document" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldocId" runat="server" Text='<%# Bind("DCB_DOC_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Document">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocument" runat="server" Text='<%# Bind("DOC_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblType" runat="server" Text='<%# Bind("DCB_TYPE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Apply To">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblApply" runat="server" Text='<%# Bind("DCB_APPLY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Copies">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCopies" runat="server" Text='<%# Bind("DCB_COPIES") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="Edit" HeaderText="Edit" Text="Edit">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="Delete" HeaderText="Delete" Text="Delete">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <HeaderStyle />
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"><asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>
                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <asp:HiddenField ID="hfSTG_ID" runat="server" />



            </div>
        </div>
    </div>
</asp:Content>

