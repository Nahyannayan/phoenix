Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Partial Class Students_StuFollowUpDisplay
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            CheckMenuandUser()
            AddAlertDays()
            BindEnqDetails()
            BindOtherDetails()
            BindGrid()
            bind_cur_status()
            bind_reason()
            bind_gridReason()

        End If

    End Sub
    Public Sub BindGrid()
        Dim Encr_decrData As New Encryption64
        Dim id = Encr_decrData.Decrypt(Request.QueryString("eqs_id").Replace(" ", "+"))
        Dim bsu_id = Session("sBsuid").ToString
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select *,(case fol_mode when '1' then 'Telephone' else 'Email' end) as mode_descr ,replace(CONVERT(varchar,ALERT_DATE,106),' ','/') as alertdate from ENQUIRY_FOLLOW_UP " & _
                        " where fol_eqs_id='" & id & "' and fol_bsu_id='" & bsu_id & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        grdFollowuphis.DataSource = ds
        grdFollowuphis.DataBind()
    End Sub
    Public Sub CheckMenuandUser()
        Try
            Dim Encr_decrData As New Encryption64
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100012") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
        Catch ex As Exception
            Response.Redirect("~\noAccess.aspx")
        End Try

    End Sub
    Public Sub AddAlertDays()
        Dim i = 1
        For i = 1 To 10
            ddalertdays.Items.Add(i)
        Next
    End Sub
    Public Sub BindEnqDetails()
        Dim Encr_decrData As New Encryption64()
        Dim editstring As String() = Encr_decrData.Decrypt(Request.QueryString("editstring").Replace(" ", "+")).Split("|")
        txtAcadYear.Text = editstring(0)
        txtGrade.Text = editstring(2)
        txtShift.Text = editstring(3)
        txtStream.Text = editstring(4)
        hdnEnq_id.Value = Encr_decrData.Decrypt(Request.QueryString("enq_id").Replace(" ", "+"))

        ' txtEnqNo.Text = Encr_decrData.Decrypt(Request.QueryString("enq_id").Replace(" ", "+"))

    End Sub
    Public Sub BindOtherDetails()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query As String = "select * from enquiry_m m " & _
                                  " inner join PROCESSFO_APPLICANT_S s" & _
                                  " on m.EQM_ENQID =s.PRA_EQM_ENQID " & _
                                  " inner join ENQUIRY_SCHOOLPRIO_S esp" & _
                                  " on esp.EQS_ID = s.PRA_EQS_ID" & _
                                  " inner join ENQUIRY_PARENT_M pm" & _
                                  " on pm.EQP_EQM_ENQID =m.EQM_ENQID" & _
                                  " and  s.PRA_STG_ID ='1' and PRA_bCOMPLETED='true'" & _
                                  " and m.EQM_ENQID = '" & hdnEnq_id.Value & "' and EQS_BSU_ID='" & Session("sBsuid") & "'"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            txtEnqNo.Text = ds.Tables(0).Rows(0).Item("EQS_APPLNO").ToString()
            txtenqdate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("EQM_ENQDATE").ToString()).ToString("dd/MMM/yyyy")
            txtappname.Text = ds.Tables(0).Rows(0).Item("EQM_APPLFIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("EQM_APPLLASTNAME").ToString()
            Dim primarycontact = ds.Tables(0).Rows(0).Item("EQM_PRIMARYCONTACT").ToString()

            If primarycontact = "F" Then
                BindParentDetails(ds, "EQP_FEMAIL", "EQP_FMOBILE", "EQP_FFIRSTNAME", "EQP_FLASTNAME")
            ElseIf primarycontact = "M" Then
                BindParentDetails(ds, "EQP_MEMAIL", "EQP_MMOBILE", "EQP_MFIRSTNAME", "EQP_MLASTNAME")
            ElseIf primarycontact = "G" Then
                BindParentDetails(ds, "EQP_GEMAIL", "EQP_GMOBILE", "EQP_GFIRSTNAME", "EQP_GLASTNAME")
            End If

        End If

    End Sub
    Public Sub BindParentDetails(ByVal ds As DataSet, ByVal email As String, ByVal mobile As String, ByVal firstname As String, ByVal lastname As String)
        txtemail.Text = ds.Tables(0).Rows(0).Item(email).ToString()
        txtmobile.Text = ds.Tables(0).Rows(0).Item(mobile).ToString()
        txtprimarycaontact.Text = ds.Tables(0).Rows(0).Item(firstname).ToString() & " " & ds.Tables(0).Rows(0).Item(lastname).ToString()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try

            Dim str_query As String


            If ddmode.SelectedValue <> "0" Then
                rfv_txtremarks.Visible = True
                rfv_txtremarks.Validate()
            Else
                rfv_txtremarks.Visible = False
            End If

            If txtAob.Text.Trim <> "" Then
                Dim strfDate As String = txtAob.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    lblmessage.Text = "Please check the alert date"
                    Exit Sub
                End If
            End If

            Dim Encr_decrData As New Encryption64()
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim bsu_id = Session("sBsuid") ''"125016" ''
            Dim eqs_id = Encr_decrData.Decrypt(Request.QueryString("eqs_id").Replace(" ", "+"))
            Dim enq_id = Encr_decrData.Decrypt(Request.QueryString("enq_id").Replace(" ", "+"))
            Dim userid = Session("sUsr_name") ''"admin" ''

            SAVE_REASON(eqs_id, enq_id)
            If ddmode.SelectedValue <> "0" Then
                Dim pParms(17) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@bsu_id", bsu_id)
                pParms(1) = New SqlClient.SqlParameter("@eqs_id", eqs_id)
                pParms(2) = New SqlClient.SqlParameter("@enq_id", enq_id)
                pParms(3) = New SqlClient.SqlParameter("@user_id", userid)
                pParms(4) = New SqlClient.SqlParameter("@mode", Convert.ToInt16(ddmode.SelectedItem.Value))
                pParms(5) = New SqlClient.SqlParameter("@remarks", txtremarks.Text)
                pParms(6) = New SqlClient.SqlParameter("@alertdays", "1")
                pParms(11) = New SqlClient.SqlParameter("@ALERT_DATE", txtAob.Text)
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "stu_enq_insert_followup", pParms)



                If txtAob.Text.Trim <> "" Then
                    Dim lstrEMPNAME
                    Dim lstrEMPEMAIL
                    Dim pParmss(3) As SqlClient.SqlParameter
                    pParmss(0) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
                    pParmss(1) = New SqlClient.SqlParameter("@EQS_ID", eqs_id)

                    Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetEMP_Name_Email", pParmss)
                        While reader.Read
                            lstrEMPNAME = Convert.ToString(reader("EMP_FNAME"))
                            lstrEMPEMAIL = Convert.ToString(reader("EMD_EMAIL"))
                        End While
                    End Using

                    Dim MSG As String, SUBJECT As String
                    SUBJECT = "OASIS ALERT - Enquiry Followup : " + txtEnqNo.Text
                    MSG = "<table width=100% border=0 style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>              <tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Enquiry Followup Alert : " + txtEnqNo.Text + "</td></tr> <tr><td>Please accept this email as confirmation for your enquiry follow up with details given below : <br></td></tr>  <tr><td> Enquiry Number: " + txtEnqNo.Text + " <br></td></tr> <tr><td> Academic Year: " + txtAcadYear.Text + " <br></td></tr> <tr><td> Grade: " + txtGrade.Text + " <br></td></tr>     <tr><td> Parent Name: " + txtprimarycaontact.Text + " <br></td></tr>   <tr><td> Mobile: " + txtmobile.Text + " <br></td></tr>    <tr><td> Email: " + txtemail.Text + " <br></td></tr>   <tr><td> Remarks:<br> " + txtremarks.Text + " </td></tr>          </table>                      <table width=100% border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>    <tr><td><br><br>This is an Automated mail from GEMS OASIS. </td></tr>              <tr></tr><tr></tr>            </table>    "
                    Dim SendAppt As New AppointmentBLL(txtAob.Text + " 08:00AM", txtAob.Text + " 08:00AM", SUBJECT, MSG, "School", lstrEMPNAME, lstrEMPEMAIL, SUBJECT, "communication@gemsedu.com")
                    SendAppt.EmailAppointment()
                End If
            End If

            If ((check_isDel(ddlCurStatus.SelectedValue) <> 0) And (txtReasonRemark.Text <> "")) Then

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Confirm_test", "Confirm_test();", True)



                If hdnValue.Value = "Yes" Then
                    str_query = "exec studSaveStudEnqAcceptReject " + eqs_id + ",'DEL',0 "
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If



                '        'Response.Redirect("~/Students/StuFollowUp.aspx?MainMnu_code=T6V+LqgVMFI=&datamode=Zo4HhpVNpXc=")
            End If

            lblmessage.Text = "Saved"

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirect_test", "redirect_test('" & ViewState("ReferrerUrl") & "');", True)
            '  Response.Redirect("~/Students/StuFollowUp.aspx?MainMnu_code=T6V+LqgVMFI=&datamode=Zo4HhpVNpXc=")
            '  Response.Redirect(ViewState("ReferrerUrl"))
        Catch ex As Exception
            lblmessage.Text = "Error"
        End Try
    End Sub

    Private Sub SAVE_REASON(ByVal EQSID As Integer, ByVal ENQID As Integer)

        Dim Status As Integer
        Dim param(9) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                param(0) = New SqlParameter("@ENR_BSU_ID", Session("sBsuid"))
                param(1) = New SqlParameter("@ENR_EQS_ID", EQSID)
                param(2) = New SqlClient.SqlParameter("@ENR_ENQ_ID", ENQID)
                param(3) = New SqlClient.SqlParameter("@ENR_EFR_ID", ddlCurStatus.SelectedValue)
                param(4) = New SqlClient.SqlParameter("@ENR_EFD_ID", ddlReason.SelectedValue)
                param(5) = New SqlClient.SqlParameter("@ENR_USER_ID", Session("sUsr_name"))
                param(6) = New SqlClient.SqlParameter("@ENR_REMARKS", txtReasonRemark.Text)
                param(8) = New SqlClient.SqlParameter("@ENR_mode_descr", 0)
                param(7) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(7).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVE_ENQ_FOLLOW_UP_REASON", param)
                Status = param(7).Value





            Catch ex As Exception

                lblError.Text = "Record could not be updated"
            Finally
                If Status <> 0 Then
                    UtilityObj.Errorlog(lblError.Text)
                    transaction.Rollback()
                Else
                    lblError.Text = ""
                    transaction.Commit()





                    bind_gridReason()
                End If
            End Try

        End Using
    End Sub

    Sub bind_cur_status()
        ddlCurStatus.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFR_ID,EFR_REASON   FROM ENQ_FOLLOW_UP_REASON_M  "



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCurStatus.DataSource = ds
        ddlCurStatus.DataTextField = "EFR_REASON"
        ddlCurStatus.DataValueField = "EFR_ID"
        ddlCurStatus.DataBind()
        ddlCurStatus.Items.Add(New ListItem("--", "0"))
        ddlCurStatus.Items.FindByText("--").Selected = True
    End Sub
    Function check_isDel(ByVal id As Integer) As Integer
        Dim i As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT isnull(EFR_bDEL,0) EFR_bDEL  FROM ENQ_FOLLOW_UP_REASON_M  where EFR_ID=" & id

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            i = ds.Tables(0).Rows(0).Item("EFR_bDEL")
        End If
        Return i
    End Function
    Sub bind_reason()
        ddlReason.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFD_ID,EFD_REASON  FROM ENQ_FOLLOW_UP_REASON_D where EFD_EFR_ID=" & ddlCurStatus.SelectedValue



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReason.DataSource = ds
        ddlReason.DataTextField = "EFD_REASON"
        ddlReason.DataValueField = "EFD_ID"
        ddlReason.DataBind()

        If (Not ddlReason.Items Is Nothing) AndAlso (ddlReason.Items.Count = 0) Then
            ddlReason.Items.Add(New ListItem("--", "0"))
            ddlReason.Items.FindByText("--").Selected = True
        End If

    End Sub
    Sub bind_gridReason()
        Dim Encr_decrData As New Encryption64
        Dim id = Encr_decrData.Decrypt(Request.QueryString("eqs_id").Replace(" ", "+"))
        Dim bsu_id = Session("sBsuid").ToString
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " SELECT ENR_LOG_DATE,EFR_REASON,EFD_REASON,ENR_REMARKS,ENR_USER_ID  FROM ENQUIRY_FOLLOW_UP_REASON " _
                        & " INNER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=ENR_EFR_ID " _
                        & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_D ON EFd_ID=ENR_EFD_ID " _
                        & " WHERE ENR_BSU_ID='" & bsu_id & "' AND ENR_EQS_ID =" & id & " order by enr_id desc"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        grdReason.DataSource = ds
        grdReason.DataBind()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub


    Protected Sub ddlCurStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurStatus.SelectedIndexChanged
        bind_reason()
    End Sub


End Class
