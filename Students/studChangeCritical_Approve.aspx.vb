﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studChangeCritical_Approve
    Inherits System.Web.UI.Page

    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Try


            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                'Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200226") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()
                    rbOpen.Checked = True
                    GridBind()
                End If

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rbAll.Checked Then
            rbAll_CheckedChanged(sender, e)
        ElseIf rbApproved.Checked Then
            rbApproved_CheckedChanged(sender, e)
        ElseIf rbCancel.Checked Then
            rbCancel_CheckedChanged(sender, e)
        ElseIf rbOpen.Checked Then
            rbOpen_CheckedChanged(sender, e)
        End If
    End Sub
    Protected Sub rbOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
            tr_Remarks.Visible = True
            tr_button.Visible = True
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub rbApproved_CheckedChanged(sender As Object, e As EventArgs) Handles rbApproved.CheckedChanged
        Try
            GridBind()
            tr_Remarks.Visible = False
            tr_button.Visible = False
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub rbCancel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
            tr_Remarks.Visible = False
            tr_button.Visible = False
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
            tr_Remarks.Visible = False
            tr_button.Visible = False
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If GrdView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = GrdView.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If GrdView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = GrdView.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub bindbsu(ByVal ddlbsu As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT   BSU_ID,BSU_NAME  FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlbsu.DataSource = ds.Tables(0)
        ddlbsu.DataValueField = "BSU_ID"
        ddlbsu.DataTextField = "BSU_NAME"
        ddlbsu.DataBind()
        ddlbsu.SelectedValue = Session("sBsuid")
        'Dim lst As New ListItem
        'lst.Text = "--------Select--------"
        'lst.Value = 0
        'ddlFschool.Items.Insert(0, lst)
    End Sub
    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim TOP_FILTER As String = ""
        If ddlTopFilter.SelectedItem.Value <> "All" Then
            TOP_FILTER = " TOP " & ddlTopFilter.SelectedItem.Value
        End If

        If rbOpen.Checked = True Then
            str_query = "SELECT " & TOP_FILTER & " row_number() over (ORDER BY A.F_GRD_DISPLAYORDER,A.F_SCT_DESCR,A.STU_NAME)as SINO,SEC_ID as STS_ID,STU_ID,STU_NO,STU_NAME,F_GRD_DISPLAYORDER,F_GRM_DISPLAY,F_GRM_ID,F_STS_SCT_ID,F_SCT_DESCR,F_STS_ACD_ID,F_BSU_NAME,F_ACY_DESCR,F_BSU_ID,T_GRD_DISPLAYORDER,T_GRM_DISPLAY,T_GRM_ID,T_STS_SCT_ID,T_SCT_DESCR,T_STS_ACD_ID,T_BSU_NAME,T_ACY_DESCR,T_BSU_ID ,F_STS_STM_ID,F_STM_DESCR,T_STS_STM_ID,T_STM_DESCR , STU_DOJ ,SEC_ENTRY_REMARKS FROM(SELECT TRANS.SEC_ID,STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_LASTNAME,' ') AS STU_NAME,GRD_FROM.GRD_DISPLAYORDER as F_GRD_DISPLAYORDER, GRADE_BSU_FROM.GRM_DISPLAY AS F_GRM_DISPLAY,GRADE_BSU_FROM.GRM_ID AS F_GRM_ID, TRANS.SEC_SCT_ID AS F_STS_SCT_ID,SECTION_M_FROM.SCT_DESCR AS F_SCT_DESCR,TRANS.SEC_ACD_ID AS F_STS_ACD_ID,BSU_FROM.BSU_NAME AS F_BSU_NAME,ACY_FROM.ACY_DESCR AS F_ACY_DESCR,TRANS.SEC_BSU_ID AS  F_BSU_ID,GRD_TO.GRD_DISPLAYORDER as T_GRD_DISPLAYORDER, GRADE_BSU_TO.GRM_DISPLAY AS T_GRM_DISPLAY,GRADE_BSU_TO.GRM_ID AS T_GRM_ID, TRANS.SEC_TO_SCT_ID AS T_STS_SCT_ID,SECTION_M_TO.SCT_DESCR AS T_SCT_DESCR,TRANS.SEC_TO_ACD_ID AS T_STS_ACD_ID,BSU_TO.BSU_NAME AS T_BSU_NAME,ACY_TO.ACY_DESCR AS T_ACY_DESCR,TRANS.SEC_BSU_ID AS  T_BSU_ID,TRANS.SEC_STM_ID AS F_STS_STM_ID,STREAM_M_FROM.STM_DESCR AS F_STM_DESCR ,TRANS.SEC_TO_STM_ID AS T_STS_STM_ID,STREAM_M_TO.STM_DESCR AS T_STM_DESCR , replace(CONVERT(varchar,SEC_DOJ,106),' ','/') as STU_DOJ , SEC_ENTRY_REMARKS FROM STU.STUDENT_CRITCALEDIT_REQ TRANS INNER JOIN STUDENT_M STU ON STU.STU_ID= TRANS.SEC_STU_ID INNER JOIN GRADE_BSU_M  GRADE_BSU_FROM ON TRANS.SEC_GRM_ID = GRADE_BSU_FROM.GRM_ID INNER JOIN GRADE_M  GRD_FROM ON GRADE_BSU_FROM.GRM_GRD_ID=GRD_FROM.GRD_ID INNER JOIN SECTION_M SECTION_M_FROM ON TRANS.SEC_SCT_ID = SECTION_M_FROM.SCT_ID INNER JOIN STREAM_M STREAM_M_FROM ON TRANS.SEC_STM_ID = STREAM_M_FROM.STM_ID  INNER JOIN BUSINESSUNIT_M BSU_FROM ON TRANS.SEC_BSU_ID=BSU_FROM.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_FROM ON TRANS.SEC_ACD_ID=ACD_FROM.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_FROM ON ACY_FROM.ACY_ID=ACD_FROM.ACD_ACY_ID  INNER JOIN GRADE_BSU_M  GRADE_BSU_TO ON TRANS.SEC_TO_GRM_ID = GRADE_BSU_TO.GRM_ID INNER JOIN GRADE_M  GRD_TO ON GRADE_BSU_TO.GRM_GRD_ID=GRD_TO.GRD_ID INNER JOIN SECTION_M SECTION_M_TO ON TRANS.SEC_TO_SCT_ID = SECTION_M_TO.SCT_ID INNER JOIN STREAM_M STREAM_M_TO ON TRANS.SEC_TO_STM_ID = STREAM_M_TO.STM_ID  INNER JOIN BUSINESSUNIT_M BSU_TO ON TRANS.SEC_BSU_ID=BSU_TO.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_TO ON TRANS.SEC_TO_ACD_ID=ACD_TO.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_TO ON ACY_TO.ACY_ID=ACD_TO.ACD_ACY_ID AND ISNULL(STU.STU_LEAVEDATE,'')='' AND STU.STU_CurrStatus NOT IN ('CN','TF') AND TRANS.SEC_bCANCEL_TRANS='FALSE' AND TRANS.SEC_bApproved='FALSE')A WHERE A.F_BSU_ID='" & Session("sBsuid") & "' "
        ElseIf rbApproved.Checked = True Then
            str_query = "SELECT " & TOP_FILTER & " row_number() over (ORDER BY A.F_GRD_DISPLAYORDER,A.F_SCT_DESCR,A.STU_NAME)as SINO,SEC_ID as STS_ID,STU_ID,STU_NO,STU_NAME,F_GRD_DISPLAYORDER,F_GRM_DISPLAY,F_GRM_ID,F_STS_SCT_ID,F_SCT_DESCR,F_STS_ACD_ID,F_BSU_NAME,F_ACY_DESCR,F_BSU_ID,T_GRD_DISPLAYORDER,T_GRM_DISPLAY,T_GRM_ID,T_STS_SCT_ID,T_SCT_DESCR,T_STS_ACD_ID,T_BSU_NAME,T_ACY_DESCR,T_BSU_ID ,F_STS_STM_ID,F_STM_DESCR,T_STS_STM_ID,T_STM_DESCR , STU_DOJ ,SEC_ENTRY_REMARKS FROM(SELECT TRANS.SEC_ID,STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_LASTNAME,' ') AS STU_NAME,GRD_FROM.GRD_DISPLAYORDER as F_GRD_DISPLAYORDER, GRADE_BSU_FROM.GRM_DISPLAY AS F_GRM_DISPLAY,GRADE_BSU_FROM.GRM_ID AS F_GRM_ID, TRANS.SEC_SCT_ID AS F_STS_SCT_ID,SECTION_M_FROM.SCT_DESCR AS F_SCT_DESCR,TRANS.SEC_ACD_ID AS F_STS_ACD_ID,BSU_FROM.BSU_NAME AS F_BSU_NAME,ACY_FROM.ACY_DESCR AS F_ACY_DESCR,TRANS.SEC_BSU_ID AS  F_BSU_ID,GRD_TO.GRD_DISPLAYORDER as T_GRD_DISPLAYORDER, GRADE_BSU_TO.GRM_DISPLAY AS T_GRM_DISPLAY,GRADE_BSU_TO.GRM_ID AS T_GRM_ID, TRANS.SEC_TO_SCT_ID AS T_STS_SCT_ID,SECTION_M_TO.SCT_DESCR AS T_SCT_DESCR,TRANS.SEC_TO_ACD_ID AS T_STS_ACD_ID,BSU_TO.BSU_NAME AS T_BSU_NAME,ACY_TO.ACY_DESCR AS T_ACY_DESCR,TRANS.SEC_BSU_ID AS  T_BSU_ID,TRANS.SEC_STM_ID AS F_STS_STM_ID,STREAM_M_FROM.STM_DESCR AS F_STM_DESCR ,TRANS.SEC_TO_STM_ID AS T_STS_STM_ID,STREAM_M_TO.STM_DESCR AS T_STM_DESCR , replace(CONVERT(varchar,SEC_DOJ,106),' ','/') as STU_DOJ , SEC_ENTRY_REMARKS FROM STU.STUDENT_CRITCALEDIT_REQ TRANS INNER JOIN STUDENT_M STU ON STU.STU_ID= TRANS.SEC_STU_ID INNER JOIN GRADE_BSU_M  GRADE_BSU_FROM ON TRANS.SEC_GRM_ID = GRADE_BSU_FROM.GRM_ID INNER JOIN GRADE_M  GRD_FROM ON GRADE_BSU_FROM.GRM_GRD_ID=GRD_FROM.GRD_ID INNER JOIN SECTION_M SECTION_M_FROM ON TRANS.SEC_SCT_ID = SECTION_M_FROM.SCT_ID INNER JOIN STREAM_M STREAM_M_FROM ON TRANS.SEC_STM_ID = STREAM_M_FROM.STM_ID  INNER JOIN BUSINESSUNIT_M BSU_FROM ON TRANS.SEC_BSU_ID=BSU_FROM.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_FROM ON TRANS.SEC_ACD_ID=ACD_FROM.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_FROM ON ACY_FROM.ACY_ID=ACD_FROM.ACD_ACY_ID INNER JOIN GRADE_BSU_M  GRADE_BSU_TO ON TRANS.SEC_TO_GRM_ID = GRADE_BSU_TO.GRM_ID INNER JOIN GRADE_M  GRD_TO ON GRADE_BSU_TO.GRM_GRD_ID=GRD_TO.GRD_ID INNER JOIN SECTION_M SECTION_M_TO ON TRANS.SEC_TO_SCT_ID = SECTION_M_TO.SCT_ID INNER JOIN STREAM_M STREAM_M_TO ON TRANS.SEC_TO_STM_ID = STREAM_M_TO.STM_ID  INNER JOIN BUSINESSUNIT_M BSU_TO ON TRANS.SEC_BSU_ID=BSU_TO.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_TO ON TRANS.SEC_TO_ACD_ID=ACD_TO.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_TO ON ACY_TO.ACY_ID=ACD_TO.ACD_ACY_ID AND ISNULL(STU.STU_LEAVEDATE,'')='' AND STU.STU_CurrStatus NOT IN ('CN','TF') AND TRANS.SEC_bApproved='TRUE')A WHERE A.F_BSU_ID='" & Session("sBsuid") & "' "
        ElseIf rbCancel.Checked = True Then
            str_query = "SELECT " & TOP_FILTER & " row_number() over (ORDER BY A.F_GRD_DISPLAYORDER,A.F_SCT_DESCR,A.STU_NAME)as SINO,SEC_ID as STS_ID,STU_ID,STU_NO,STU_NAME,F_GRD_DISPLAYORDER,F_GRM_DISPLAY,F_GRM_ID,F_STS_SCT_ID,F_SCT_DESCR,F_STS_ACD_ID,F_BSU_NAME,F_ACY_DESCR,F_BSU_ID,T_GRD_DISPLAYORDER,T_GRM_DISPLAY,T_GRM_ID,T_STS_SCT_ID,T_SCT_DESCR,T_STS_ACD_ID,T_BSU_NAME,T_ACY_DESCR,T_BSU_ID ,F_STS_STM_ID,F_STM_DESCR,T_STS_STM_ID,T_STM_DESCR , STU_DOJ ,SEC_ENTRY_REMARKS FROM(SELECT TRANS.SEC_ID,STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_LASTNAME,' ') AS STU_NAME,GRD_FROM.GRD_DISPLAYORDER as F_GRD_DISPLAYORDER, GRADE_BSU_FROM.GRM_DISPLAY AS F_GRM_DISPLAY,GRADE_BSU_FROM.GRM_ID AS F_GRM_ID, TRANS.SEC_SCT_ID AS F_STS_SCT_ID,SECTION_M_FROM.SCT_DESCR AS F_SCT_DESCR,TRANS.SEC_ACD_ID AS F_STS_ACD_ID,BSU_FROM.BSU_NAME AS F_BSU_NAME,ACY_FROM.ACY_DESCR AS F_ACY_DESCR,TRANS.SEC_BSU_ID AS  F_BSU_ID,GRD_TO.GRD_DISPLAYORDER as T_GRD_DISPLAYORDER, GRADE_BSU_TO.GRM_DISPLAY AS T_GRM_DISPLAY,GRADE_BSU_TO.GRM_ID AS T_GRM_ID, TRANS.SEC_TO_SCT_ID AS T_STS_SCT_ID,SECTION_M_TO.SCT_DESCR AS T_SCT_DESCR,TRANS.SEC_TO_ACD_ID AS T_STS_ACD_ID,BSU_TO.BSU_NAME AS T_BSU_NAME,ACY_TO.ACY_DESCR AS T_ACY_DESCR,TRANS.SEC_BSU_ID AS  T_BSU_ID,TRANS.SEC_STM_ID AS F_STS_STM_ID,STREAM_M_FROM.STM_DESCR AS F_STM_DESCR ,TRANS.SEC_TO_STM_ID AS T_STS_STM_ID,STREAM_M_TO.STM_DESCR AS T_STM_DESCR , replace(CONVERT(varchar,SEC_DOJ,106),' ','/') as STU_DOJ , SEC_ENTRY_REMARKS FROM STU.STUDENT_CRITCALEDIT_REQ TRANS INNER JOIN STUDENT_M STU ON STU.STU_ID= TRANS.SEC_STU_ID INNER JOIN GRADE_BSU_M  GRADE_BSU_FROM ON TRANS.SEC_GRM_ID = GRADE_BSU_FROM.GRM_ID INNER JOIN GRADE_M  GRD_FROM ON GRADE_BSU_FROM.GRM_GRD_ID=GRD_FROM.GRD_ID INNER JOIN SECTION_M SECTION_M_FROM ON TRANS.SEC_SCT_ID = SECTION_M_FROM.SCT_ID INNER JOIN STREAM_M STREAM_M_FROM ON TRANS.SEC_STM_ID = STREAM_M_FROM.STM_ID  INNER JOIN BUSINESSUNIT_M BSU_FROM ON TRANS.SEC_BSU_ID=BSU_FROM.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_FROM ON TRANS.SEC_ACD_ID=ACD_FROM.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_FROM ON ACY_FROM.ACY_ID=ACD_FROM.ACD_ACY_ID INNER JOIN GRADE_BSU_M  GRADE_BSU_TO ON TRANS.SEC_TO_GRM_ID = GRADE_BSU_TO.GRM_ID INNER JOIN GRADE_M  GRD_TO ON GRADE_BSU_TO.GRM_GRD_ID=GRD_TO.GRD_ID INNER JOIN SECTION_M SECTION_M_TO ON TRANS.SEC_TO_SCT_ID = SECTION_M_TO.SCT_ID INNER JOIN STREAM_M STREAM_M_TO ON TRANS.SEC_TO_STM_ID = STREAM_M_TO.STM_ID  INNER JOIN BUSINESSUNIT_M BSU_TO ON TRANS.SEC_BSU_ID=BSU_TO.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_TO ON TRANS.SEC_TO_ACD_ID=ACD_TO.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_TO ON ACY_TO.ACY_ID=ACD_TO.ACD_ACY_ID AND ISNULL(STU.STU_LEAVEDATE,'')='' AND STU.STU_CurrStatus NOT IN ('CN','TF') AND TRANS.SEC_bCANCEL_TRANS='TRUE' AND TRANS.SEC_bApproved='FALSE')A WHERE A.F_BSU_ID='" & Session("sBsuid") & "' "
        Else
            str_query = "SELECT " & TOP_FILTER & " row_number() over (ORDER BY A.F_GRD_DISPLAYORDER,A.F_SCT_DESCR,A.STU_NAME)as SINO,SEC_ID as STS_ID,STU_ID,STU_NO,STU_NAME,F_GRD_DISPLAYORDER,F_GRM_DISPLAY,F_GRM_ID,F_STS_SCT_ID,F_SCT_DESCR,F_STS_ACD_ID,F_BSU_NAME,F_ACY_DESCR,F_BSU_ID,T_GRD_DISPLAYORDER,T_GRM_DISPLAY,T_GRM_ID,T_STS_SCT_ID,T_SCT_DESCR,T_STS_ACD_ID,T_BSU_NAME,T_ACY_DESCR,T_BSU_ID ,F_STS_STM_ID,F_STM_DESCR,T_STS_STM_ID,T_STM_DESCR , STU_DOJ ,SEC_ENTRY_REMARKS FROM(SELECT TRANS.SEC_ID,STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_LASTNAME,' ') AS STU_NAME,GRD_FROM.GRD_DISPLAYORDER as F_GRD_DISPLAYORDER, GRADE_BSU_FROM.GRM_DISPLAY AS F_GRM_DISPLAY,GRADE_BSU_FROM.GRM_ID AS F_GRM_ID, TRANS.SEC_SCT_ID AS F_STS_SCT_ID,SECTION_M_FROM.SCT_DESCR AS F_SCT_DESCR,TRANS.SEC_ACD_ID AS F_STS_ACD_ID,BSU_FROM.BSU_NAME AS F_BSU_NAME,ACY_FROM.ACY_DESCR AS F_ACY_DESCR,TRANS.SEC_BSU_ID AS  F_BSU_ID,GRD_TO.GRD_DISPLAYORDER as T_GRD_DISPLAYORDER, GRADE_BSU_TO.GRM_DISPLAY AS T_GRM_DISPLAY,GRADE_BSU_TO.GRM_ID AS T_GRM_ID, TRANS.SEC_TO_SCT_ID AS T_STS_SCT_ID,SECTION_M_TO.SCT_DESCR AS T_SCT_DESCR,TRANS.SEC_TO_ACD_ID AS T_STS_ACD_ID,BSU_TO.BSU_NAME AS T_BSU_NAME,ACY_TO.ACY_DESCR AS T_ACY_DESCR,TRANS.SEC_BSU_ID AS  T_BSU_ID,TRANS.SEC_STM_ID AS F_STS_STM_ID,STREAM_M_FROM.STM_DESCR AS F_STM_DESCR ,TRANS.SEC_TO_STM_ID AS T_STS_STM_ID,STREAM_M_TO.STM_DESCR AS T_STM_DESCR , replace(CONVERT(varchar,SEC_DOJ,106),' ','/') as STU_DOJ , SEC_ENTRY_REMARKS FROM STU.STUDENT_CRITCALEDIT_REQ TRANS INNER JOIN STUDENT_M STU ON STU.STU_ID= TRANS.SEC_STU_ID INNER JOIN GRADE_BSU_M  GRADE_BSU_FROM ON TRANS.SEC_GRM_ID = GRADE_BSU_FROM.GRM_ID INNER JOIN GRADE_M  GRD_FROM ON GRADE_BSU_FROM.GRM_GRD_ID=GRD_FROM.GRD_ID INNER JOIN SECTION_M SECTION_M_FROM ON TRANS.SEC_SCT_ID = SECTION_M_FROM.SCT_ID INNER JOIN STREAM_M STREAM_M_FROM ON TRANS.SEC_STM_ID = STREAM_M_FROM.STM_ID  INNER JOIN BUSINESSUNIT_M BSU_FROM ON TRANS.SEC_BSU_ID=BSU_FROM.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_FROM ON TRANS.SEC_ACD_ID=ACD_FROM.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_FROM ON ACY_FROM.ACY_ID=ACD_FROM.ACD_ACY_ID  INNER JOIN GRADE_BSU_M  GRADE_BSU_TO ON TRANS.SEC_TO_GRM_ID = GRADE_BSU_TO.GRM_ID INNER JOIN GRADE_M  GRD_TO ON GRADE_BSU_TO.GRM_GRD_ID=GRD_TO.GRD_ID INNER JOIN SECTION_M SECTION_M_TO ON TRANS.SEC_TO_SCT_ID = SECTION_M_TO.SCT_ID INNER JOIN STREAM_M STREAM_M_TO ON TRANS.SEC_TO_STM_ID = STREAM_M_TO.STM_ID  INNER JOIN BUSINESSUNIT_M BSU_TO ON TRANS.SEC_BSU_ID=BSU_TO.BSU_ID INNER JOIN ACADEMICYEAR_D ACD_TO ON TRANS.SEC_TO_ACD_ID=ACD_TO.ACD_ID INNER JOIN ACADEMICYEAR_M ACY_TO ON ACY_TO.ACY_ID=ACD_TO.ACD_ACY_ID AND ISNULL(STU.STU_LEAVEDATE,'')='' AND STU.STU_CurrStatus NOT IN ('CN','TF') )A WHERE A.F_BSU_ID='" & Session("sBsuid") & "' "
        End If

        'Dim txtStuNo As TextBox
        'Dim txtName As TextBox
        'If txtStuNo.Text <> "" Then
        '    str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        'End If

        'If txtName.Text <> "" Then
        '    str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        'End If


        ' AND C.SCT_GRM_ID=B.GRM_ID" 
        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedStatus As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim txtSearch As New TextBox

        If GrdView.Rows.Count > 0 Then


            txtSearch = GrdView.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter += " AND "
                strFilter += GetSearchString("STU_NAME", txtSearch.Text, strSearch)
            End If
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = GrdView.HeaderRow.FindControl("txtFeeSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then

                strFilter += " AND "

                strFilter += GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strFee = txtSearch.Text

        End If

        str_query += strFilter + " ORDER BY A.F_GRD_DISPLAYORDER,A.F_SCT_DESCR,A.STU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'Dim dv As DataView = ds.Tables(0).DefaultView
        'If strFilter <> "" Then
        '    dv.RowFilter = strFilter
        'End If

        Dim dt As DataTable
        If ds.Tables(0).Rows.Count = 0 Then
            GrdView.DataSource = ds
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            GrdView.DataBind()
            Dim columnCount As Integer = GrdView.Rows(0).Cells.Count
            GrdView.Rows(0).Cells.Clear()
            GrdView.Rows(0).Cells.Add(New TableCell)
            GrdView.Rows(0).Cells(0).ColumnSpan = columnCount
            GrdView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            GrdView.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

        Else
            GrdView.DataSource = ds
            GrdView.DataBind()

        End If

        set_Menu_Img()
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim CurBsUnit As String = Session("sBsuid")
        Dim CurACDID As String = "" ' ddlAca_Year.SelectedValue
        Dim stu_name As String = ""
        'ANDACD_ACY_ID=20 ANDACD_ACY_ID=20
        Dim con As New SqlConnection(str_conn)
        Dim transaction As SqlTransaction
        con.Open()

        Try
            For Each gr As GridViewRow In GrdView.Rows
                Dim btransfer As CheckBox = TryCast(gr.FindControl("chkSelect"), CheckBox)
                Dim HF_sts_id As HiddenField = TryCast(gr.FindControl("HF_sts_id"), HiddenField)

                If btransfer.Checked = True Then
                    transaction = con.BeginTransaction("trans")
                    Dim stu_id As String = TryCast(gr.FindControl("HF_stu_id"), HiddenField).Value
                    stu_name = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT STU_NAME FROM dbo.VW_OSO_STUDENT_ENQUIRY WITH(NOLOCK) WHERE STU_ID=" & stu_id)
                    Dim param(10) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@STS_ID", HF_sts_id.Value)
                    param(1) = New SqlClient.SqlParameter("@DES", "A")

                    param(2) = New SqlClient.SqlParameter("@Return_msg", SqlDbType.VarChar, 400)
                    param(2).Direction = ParameterDirection.Output
                    param(3) = New SqlClient.SqlParameter("@APR_USER", Session("sUsr_name"))
                    param(4) = New SqlClient.SqlParameter("@APR_EMP_ID", Session("EmployeeId"))
                    param(5) = New SqlClient.SqlParameter("@APR_REMARKS", Replace(txtRemarks.Text, "'", "''"))
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.SAVE_STUDENT_CRITICAL_APPROVE", param)
                    transaction.Commit() '.Rollback() 
                End If
            Next

            lblError.Text = "Records Saved sucessfully...!!!!"
            GridBind()
        Catch ex As Exception
            Dim Msg As String = ex.Message
            If Msg.Contains("cancel concession") Then
                lblError.Text = "Please cancel the active concession(s) for " & stu_name
            End If
            transaction.Rollback()

        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim CurBsUnit As String = Session("sBsuid")
        Dim CurACDID As String = "" ' ddlAca_Year.SelectedValue
        'ANDACD_ACY_ID=20 ANDACD_ACY_ID=20
        Dim con As New SqlConnection(str_conn)
        Dim transaction As SqlTransaction
        con.Open()
        transaction = con.BeginTransaction("trans")
        Try
            For Each gr As GridViewRow In GrdView.Rows
                Dim btransfer As CheckBox = TryCast(gr.FindControl("chkSelect"), CheckBox)
                Dim HF_sts_id As HiddenField = TryCast(gr.FindControl("HF_sts_id"), HiddenField)
                If btransfer.Checked = True Then
                    Dim stu_id As String = TryCast(gr.FindControl("HF_stu_id"), HiddenField).Value
                    Dim param(10) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@STS_ID", HF_sts_id.Value)
                    param(1) = New SqlClient.SqlParameter("@DES", "C")
                    param(2) = New SqlClient.SqlParameter("@Return_msg", SqlDbType.VarChar, 400)
                    param(2).Direction = ParameterDirection.Output
                    param(3) = New SqlClient.SqlParameter("@APR_USER", Session("sUsr_name"))
                    param(4) = New SqlClient.SqlParameter("@APR_EMP_ID", Session("EmployeeId"))
                    param(5) = New SqlClient.SqlParameter("@APR_REMARKS", Replace(txtRemarks.Text, "'", "''"))
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.SAVE_STUDENT_CRITICAL_APPROVE", param)
                End If
            Next
            transaction.Commit()
            lblError.Text = "Records Saved sucessfully...!!!!"
            GridBind()
        Catch ex As Exception
            transaction.Rollback()
            lblError.Text = "Insertion Failed...!!!!"
        End Try
    End Sub

    
End Class
