<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studContinueCert_Print.aspx.vb" Inherits="studContinueCert_Print" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Students Info"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table align="center" cellpadding="0" cellspacing="0"
                    width="100%%">
                    <tr>
                        <td></td>
                        <td colspan="2" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                <tr>
                                    <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td align="left" colspan="4" valign="top">
                                                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="4" valign="top">&nbsp;
                                        &nbsp;&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4" valign="top">
                                                    <table id="tbl_test" runat="server" align="center" width="100%"
                                                        cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" valign="middle" width="20%">
                                                                <asp:Label ID="lblAccText" runat="server" Text="Select Academic Year " CssClass="field-label"></asp:Label></td>
                                                            <td width="30%">
                                                                <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" colspan="4" valign="top">
                                                                <asp:GridView ID="gvTCSO_View" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                    CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                                    PageSize="20" Width="100%">
                                                                    <RowStyle CssClass="griditem" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="HideID" Visible="False">
                                                                            <EditItemTemplate>
                                                                                &nbsp; 
                                                                            </EditItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblHideID" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Student No">
                                                                            <HeaderTemplate>

                                                                                <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label><br />
                                                                                <asp:TextBox ID="txtStu_No" runat="server"></asp:TextBox>
                                                                                <asp:ImageButton ID="btnSearchStu_No" OnClick="btnSearchStu_No_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                                            <EditItemTemplate>
                                                                                &nbsp; 
                                                                            </EditItemTemplate>
                                                                            <HeaderTemplate>

                                                                                <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label><br />
                                                                                <asp:TextBox ID="txtStu_Name" runat="server"></asp:TextBox>
                                                                                <asp:ImageButton ID="btnSearchStu_Name" OnClick="btnSearchStu_Name_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Grade">
                                                                            <EditItemTemplate>
                                                                                &nbsp; 
                                                                            </EditItemTemplate>
                                                                            <HeaderTemplate>

                                                                                <asp:Label ID="lblGradeH" runat="server">Grade</asp:Label><br />
                                                                                <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                                                <asp:ImageButton ID="btnSearchGrade" OnClick="btnSearchGrade_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("PRESENTGRADE") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Section">
                                                                            <EditItemTemplate>
                                                                            </EditItemTemplate>
                                                                            <HeaderTemplate>

                                                                                <asp:Label ID="lblSectionH" runat="server">Section</asp:Label><br />
                                                                                <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                                                                <asp:ImageButton ID="btnSearchSection" OnClick="btnSearchSection_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                        </asp:TemplateField>

                                                                        <asp:ButtonField CommandName="View" Text="Print" HeaderText="Print">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                        </asp:ButtonField>
                                                                    </Columns>
                                                                    <SelectedRowStyle BackColor="Aqua" />
                                                                    <HeaderStyle />
                                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                                        <input id="h_selected_menu_2" runat="server" type="hidden" value="=" />
                                        <input id="h_selected_menu_3" runat="server" type="hidden" value="=" />
                                        <input id="h_selected_menu_4" runat="server" type="hidden" value="=" />
                                        <input id="h_selected_menu_5" runat="server" type="hidden" value="=" />

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



</asp:Content>

