﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="COVID_Relief_StatusUpdate.aspx.vb" Inherits="Students_COVID_Relief_StatusUpdate" %>

<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc1" TagName="usrMessageBar" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link href="../cssfiles/title.css" rel="stylesheet" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <style type="text/css">
        .stud-profile {
            width: 80px;
            height: 80px;
            border-width: 0px;
            border-radius: 6px;
            vertical-align: top;
            border: 1px solid rgba(0,0,0,0.3) !important;
        }

        .RadComboBoxDropDown .rcbItem > label, .RadComboBoxDropDown .rcbHovered > label, .RadComboBoxDropDown .rcbDisabled > label, .RadComboBoxDropDown .rcbLoading > label, .RadComboBoxDropDown .rcbCheckAllItems > label, .RadComboBoxDropDown .rcbCheckAllItemsHovered > label {
            display: inline;
            float: left;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
        }

        .RadComboBox_Default .rcbInner, .RadComboBox_Default .rcbReadOnly .rcbInputCellLeft {
            padding: 10px !important;
            border-color: #dee2da !important;
            border-radius: 6px !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
            width: 80%;
            background-image: none !important;
            background-color: #fff !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Nunito', sans-serif !important;
        }

        .RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
            border: 0 !important;
            box-shadow: none;
        }

        .RadComboBox_Default .rcbActionButton, .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }


        .RadComboBox .rcbInputCell {
            width: 100%;
            height: 31px !important;
            background-color: transparent !important;
            border-radius: 6px !important;
            background-image: none !important;
            box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
        }

        .RadComboBox .rcbArrowCellRight a {
            background-position: -18px -176px;
            border: solid black;
            border-width: 0 1px 1px 0;
            transform: rotate(360deg);
            -webkit-transform: rotate(45deg) !important;
            color: black;
            width: 7px !important;
            height: 7px !important;
            overflow: hidden;
            margin-top: -1px;
            margin-left: -15px;
        }

        .RadComboBox .rcbArrowCell a {
            width: 18px;
            height: 31px;
            position: relative;
            outline: 0;
            font-size: 0;
            line-height: 1px;
            text-decoration: none;
            text-indent: 9999px;
            display: block;
            overflow: hidden;
            cursor: default;
            padding: 0px !important;
        }

        .RadComboBox_Default {
            /* color: #333; */
            font: inherit;
            width: 80% !important;
            font-style: normal !important;
        }

            .RadComboBox_Default .rcbEmptyMessage {
                font-style: normal !important;
            }

            .RadComboBox_Default .rcbFocused .rcbInput {
                color: black;
                /*padding: 10px;*/
            }

        .RadComboBox .rcbActionButton {
            border: 0px;
            background-image: none !important;
            height: 100% !important;
            color: transparent !important;
            background-color: transparent !important;
        }

        .RadComboBox_Default .rcbInputCell, .RadComboBox_Default .rcbArrowCell {
            background-image: none !important;
        }

        .RadComboBox table td.rcbInputCell {
            padding: 0px !important;
            border-width: 0;
        }

        .RadComboBox .rcbReadOnly .rcbInput {
            cursor: default;
            /*border: 1px solid rgba(0,0,0,0.12) !important;
    padding: 20px 10px !important;*/
        }

        /*Rad calender style overwride starts here*/
        .RadPicker_Default .rcCalPopup, .RadPicker_Default .rcTimePopup {
            background-image: url(../Images/calendar.gif) !important;
            width: 30px;
            height: 30px;
        }

        .RadPicker_Default a.rcCalPopup:hover, .RadPicker_Default a.rcCalPopup:focus, .RadPicker_Default a.rcCalPopup:active {
            background-image: url(../Images/calendar.gif) !important;
            width: 30px;
            height: 30px;
            background-position: 0 0 !important;
        }

        .RadPicker {
            width: 80% !important;
        }

            .RadPicker .rcCalPopup, .RadPicker .rcTimePopup {
                width: 30px !important;
                height: 30px !important;
            }

        table.RadCalendar_Default, .RadCalendar .rcMainTable {
            background: #ffffff !important;
        }

        .RadComboBox_Default .rcbActionButton {
            border-color: transparent !important;
        }

        .Mygrid {
            width: auto !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sman" runat="server"></asp:ScriptManager>
        <div>
            <table style="width: 100%;">

                <tr>
                         <asp:Label ID="errLbl" runat="server" ></asp:Label>
                </tr>
                <tr>
                    <td class="title-bg" colspan="3">Update Status
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <uc1:usrMessageBar runat="server" ID="usrMessageBar" />
                    </td>
                </tr>

                <tr>
                    <td align="left" style="width: 20%;"><span class="field-label">Status</span>
                    </td>

                    <td align="left" width="40%">

                        <asp:DropDownList ID="ddlStatus" runat="server" class="form-control" AutoPostBack="true"/>
                                                                
                                                               
                   
                </tr>
              
                <tr>
                    <td align="left" style="width: 20%;"><span class="field-label">Comments</span>
                    </td>

                    <td align="left" width="80%">
                        <asp:TextBox ID="txtComments" runat="server" MaxLength="750" TextMode="MultiLine"></asp:TextBox>
                    </td>
                   
                </tr>
              
                <tr>
                    <td colspan="3" align="center">
                        <asp:Button ID="btnProceed" CssClass="button" runat="server" Text="Save" />
                    </td>
                </tr>
               
            </table>
        </div>
    </form>
 
</body>
</html>

