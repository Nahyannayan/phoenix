﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports GemBox.Spreadsheet
Imports System.Data.OleDb
Partial Class Students_studHealthStatus
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050029") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    BindGrade()
                    BindSection()

                    tr1.visible = False
                    tr2.visible = False
                    tr3.visible = False
                    tr4.visible = False

                    gvStud.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnUpload)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownload)
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                   & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                                   & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                                   & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                                   & " where grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                   & " order by grd_displayorder"
        Else
            str_query = "SELECT DISTINCT CASE GRM_STM_ID WHEN 1 THEN grm_display ELSE GRM_DISPLAY+'-'+STM_DESCR END AS GRM_DISPLAY " _
                                  & " ,grm_grd_id+'|'+CONVERT(VARCHAR(100),STM_ID) AS GRM_GRD_ID,grd_displayorder,STM_ID FROM " _
                                  & " grade_bsu_m AS A inner join grade_m as b on a.grm_grd_id=b.grd_id " _
                                  & " inner join stream_m as c on a.grm_stm_id=c.stm_id " _
                                  & " inner join section_m on grm_id=sct_grm_id" _
                                  & " where grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                  & " and sct_emp_id='" + Session("EMPLOYEEID") + "'" _
                                  & " order by grd_displayorder"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()


    End Sub

    Sub BindSection()
        Dim grade As String() = ddlGrade.SelectedValue.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                        & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                        & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                        & " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'" _
                        & " ORDER BY SCT_DESCR"
        Else
            str_query = "SELECT DISTINCT SCT_DESCR,SCT_ID FROM SECTION_M AS A " _
                       & " INNER JOIN GRADE_BSU_M AS B ON A.SCT_GRM_ID=B.GRM_ID" _
                       & " WHERE SCT_DESCR<>'TEMP' AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND GRM_GRD_ID='" + grade(0) + "' AND GRM_STM_ID='" + grade(1) + "'" _
                       & " AND SCT_EMP_ID='" + Session("EMPLOYEEID") + "'" _
                       & " ORDER BY SCT_DESCR"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String


        If ddlAcademicYear.SelectedValue.ToString = Session("Current_ACD_ID") Then

            str_query = "SELECT STU_ID,STU_NO,ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')) AS STU_NAME," _
                             & " ISNULL(STH_HEIGHT,'') STH_HEIGHT,ISNULL(STH_WEIGHT,'') STH_WEIGHT," _
                             & " ISNULL(STH_BLOODGROUP,ISNULL(STU_BLOODGROUP,'--')) STH_BLOODGROUP," _
                             & " ISNULL(STH_VISION_L,'') STH_VISION_L,ISNULL(STH_VISION_R,'') STH_VISION_R,ISNULL(STH_DENTAL,'') STH_DENTAL," _
                             & " CASE STU_CURRSTATUS WHEN 'EN' THEN '' ELSE STU_CURRSTATUS END AS STU_STATUS," _
                             & " ISNULL(STH_ROLLNO,'') STH_ROLLNO," _
                             & " ISNULL(STH_FATHERNAME,ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'')) STH_FATHERNAME," _
                             & " ISNULL(STH_MOTHERNAME,ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'')) STH_MOTHERNAME," _
                             & " ISNULL(STH_REGNO,'') STH_REGNO" _
                             & " FROM STUDENT_M AS A  " _
                             & " INNER JOIN STUDENT_D AS C ON A.STU_SIBLING_ID=C.STS_STU_ID" _
                             & " LEFT OUTER JOIN STUDENT_HEALTHSTATUS AS B ON " _
                             & " STU_ID=STH_STU_ID AND STU_ACD_ID=STH_ACD_ID" _
                             & " WHERE STU_ACD_ID=" + hACD_ID.Value _
                             & " AND STU_SCT_ID=" + hSCT_ID.Value _
                             & " AND STU_CURRSTATUS<>'CN'" _
                             & " ORDER BY STU_PASPRTNAME"

        Else
            str_query = "SELECT STU_ID,STU_NO,ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')) AS STU_NAME," _
                             & " ISNULL(STH_HEIGHT,'') STH_HEIGHT,ISNULL(STH_WEIGHT,'') STH_WEIGHT," _
                             & " ISNULL(STH_BLOODGROUP,ISNULL(STU_BLOODGROUP,'--')) STH_BLOODGROUP," _
                             & " ISNULL(STH_VISION_L,'') STH_VISION_L,ISNULL(STH_VISION_R,'') STH_VISION_R,ISNULL(STH_DENTAL,'') STH_DENTAL," _
                             & " CASE STU_CURRSTATUS WHEN 'EN' THEN '' ELSE STU_CURRSTATUS END AS STU_STATUS," _
                             & " ISNULL(STH_ROLLNO,'') STH_ROLLNO," _
                             & " ISNULL(STH_FATHERNAME,ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'')) STH_FATHERNAME," _
                             & " ISNULL(STH_MOTHERNAME,ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'')) STH_MOTHERNAME," _
                             & " ISNULL(STH_REGNO,'') STH_REGNO" _
                             & " FROM STUDENT_M AS A  INNER JOIN STUDENT_PROMO_S AS B ON STU_ID=STP_STU_ID" _
                             & " INNER JOIN STUDENT_D AS D ON A.STU_SIBLING_ID=D.STS_STU_ID" _
                             & " LEFT OUTER JOIN STUDENT_HEALTHSTATUS AS C ON " _
                             & " STP_STU_ID=STH_STU_ID AND STP_ACD_ID=STH_ACD_ID" _
                             & " WHERE STP_ACD_ID=" + hACD_ID.Value _
                             & " AND STP_SCT_ID=" + hSCT_ID.Value _
                             & " ORDER BY STU_PASPRTNAME"

        End If
      
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvStud.DataSource = ds
        gvStud.DataBind()

        Dim i As Integer

        Dim lblBloodGroup As Label
        Dim ddlbloodgroup As DropDownList



        For i = 0 To gvstud.rows.count - 1
            With gvstud.rows(i)
                lblBloodGroup = .FindControl("lblBloodGroup")
                ddlbloodgroup = .FindControl("ddlbloodgroup")

                If Not ddlbloodgroup.Items.FindByValue(lblBloodGroup.Text) Is Nothing Then
                    ddlbloodgroup.Items.FindByValue(lblBloodGroup.Text).Selected = True
                End If
            End With
        Next


    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        Dim i As Integer

        Dim lblStuID As Label
        Dim txtHeight As TextBox
        Dim txtWeight As TextBox
        Dim ddlbloodgroup As DropDownList
        Dim txtVisionL As TextBox
        Dim txtVisionR As TextBox
        Dim txtDental As TextBox
        Dim txtRollNo As TextBox
        Dim txtFather As TextBox
        Dim txtMother As TextBox
        Dim txtRegNo As TextBox

        For i = 0 To gvStud.Rows.Count - 1

            With gvStud.Rows(i)
                lblStuID = .FindControl("lblStuID")
                txtHeight = .FindControl("txtHeight")
                txtWeight = .FindControl("txtWeight")
                ddlbloodgroup = .FindControl("ddlbloodgroup")
                txtVisionL = .FindControl("txtVisionL")
                txtVisionR = .FindControl("txtVisionR")
                txtDental = .FindControl("txtDental")
                txtRollNo = .FindControl("txtRollNo")
                txtFather = .FindControl("txtFather")
                txtMother = .FindControl("txtMother")
                txtRegNo = .FindControl("txtRegNo")
            End With

            str_query = "exec saveSTUDENTHEALTHSTATUS " _
                      & "@STH_STU_ID=" + lblStuID.Text + "," _
                      & "@STH_ACD_ID=" + hACD_ID.Value + "," _
                      & "@STH_GRD_ID='" + hGRD_ID.Value + "'," _
                      & "@STH_FATHERNAME='" + txtFather.Text + "'," _
                      & "@STH_MOTHERNAME='" + txtMother.Text + "'," _
                      & "@STH_ROLLNO='" + txtRollNo.Text + "'," _
                      & "@STH_REGNO='" + txtRegNo.Text + "'," _
                      & "@STH_HEIGHT='" + txtHeight.Text + "'," _
                      & "@STH_WEIGHT='" + txtWeight.Text + "'," _
                      & "@STH_BLOODGROUP='" + ddlbloodgroup.SelectedValue + "'," _
                      & "@STH_VISION_L='" + txtVisionL.Text + "'," _
                      & "@STH_VISION_R='" + txtVisionR.Text + "'," _
                      & "@STH_DENTAL='" + txtDental.Text + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next


        lblError.Text = "Record Saved Successfully"
    End Sub
    Private Sub UpLoadExcelFiletoXml()
        Dim strFileNameOnly As String
        Dim grade As String()
        grade = ddlGrade.SelectedValue.Split("|")
        hSCT_ID.Value = ddlSection.SelectedValue.ToString
        hACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        hGRD_ID.Value = ddlSection.SelectedValue.ToString
        lblError.Text = ""
        If uploadFile.HasFile Then
            Try
                ' alter path for your project
                Dim PhotoVirtualpath = Server.MapPath("~/Curriculum/ReportDownloads/")
                strFileNameOnly = Format(Date.Now, "ddMMyyHmmss").Replace("/", "-") & Session("sBsuid") & "_" & grade(0) & ".xls"
                uploadFile.SaveAs(Server.MapPath("~/Curriculum/ReportDownloads/") & "/" & strFileNameOnly)
                Dim myDataset As New DataSet()

                Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                "Data Source=" & Server.MapPath("~/Curriculum/ReportDownloads/") & "/" & strFileNameOnly & ";" & _
                "Extended Properties=Excel 8.0;"

                ''You must use the $ after the object you reference in the spreadsheet
                Dim myData As New OleDbDataAdapter("SELECT * FROM [" & ExcelFunctions.GetExcelSheetNames(Server.MapPath("~/Curriculum/ReportDownloads/") & "/" & strFileNameOnly) & "]", strConn)
                'myData.TableMappings.Add("Table", "ExcelTest")
                myData.Fill(myDataset)
                myData.Dispose()
                Dim dt As DataTable

                dt = myDataset.Tables(0)
                Dim s As New MemoryStream()

                dt.WriteXml(s, True)



                'Retrieve the text from the stream

                s.Seek(0, SeekOrigin.Begin)

                Dim sr As New StreamReader(s)

                Dim xmlString As String

                xmlString = sr.ReadToEnd()



                'close 

                sr.Close()

                sr.Dispose()
                Dim cmd As New SqlCommand
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                cmd = New SqlCommand("saveSTUDENTHEALTHSTATUSXml", objConn)

                cmd.CommandType = CommandType.StoredProcedure



                cmd.Parameters.AddWithValue("@STH_ACD_ID", hACD_ID.Value.ToString)
                cmd.Parameters.AddWithValue("@STH_GRD_ID", hGRD_ID.Value)
              

                Dim p As SqlParameter

                p = cmd.Parameters.AddWithValue("@data", xmlString)

                p.SqlDbType = SqlDbType.Xml

                cmd.ExecuteNonQuery()

                cmd.Dispose()

                lblError.Text = "Records Saved Sucessfully"
                GridBind()

            Catch ex As Exception
                lblError.Text = "Error: " & ex.Message.ToString
            End Try
        Else
            lblError.Text = "Please select a file to upload."
        End If


    End Sub
#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        tr1.visible = True
        tr2.visible = True
        tr3.visible = True
        tr4.visible = True


        hSCT_ID.Value = ddlSection.SelectedValue.ToString
        hACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        hGRD_ID.Value = ddlSection.SelectedValue.ToString
        GridBind()

        lblError.Text = ""
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrade()
        BindSection()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveData()
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        UpLoadExcelFiletoXml()
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        downloaddata()
    End Sub
    Sub downloaddata()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        hSCT_ID.Value = ddlSection.SelectedValue.ToString
        hACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        hGRD_ID.Value = ddlSection.SelectedValue.ToString


        If ddlAcademicYear.SelectedValue.ToString = Session("Current_ACD_ID") Then

            str_query = "SELECT STU_NO as STUNO,ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')) AS STUNAME," _
                             & " ISNULL(STH_FATHERNAME,ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'')) FATHERNAME," _
                             & " ISNULL(STH_MOTHERNAME,ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'')) MOTHERNAME," _
                             & " ISNULL(STH_ROLLNO,'') ROLLNO," _
                             & " ISNULL(STH_REGNO,'') REGNO," _
                             & " ISNULL(STH_HEIGHT,'') HEIGHT,ISNULL(STH_WEIGHT,'') WEIGHT," _
                             & " ISNULL(STH_BLOODGROUP,ISNULL(STU_BLOODGROUP,'--')) BLOODGROUP," _
                             & " ISNULL(STH_VISION_L,'') VISIONL,ISNULL(STH_VISION_R,'') VISIONR,ISNULL(STH_DENTAL,'') DENTAL" _
                             & " FROM STUDENT_M AS A  " _
                             & " INNER JOIN STUDENT_D AS C ON A.STU_SIBLING_ID=C.STS_STU_ID" _
                             & " LEFT OUTER JOIN STUDENT_HEALTHSTATUS AS B ON " _
                             & " STU_ID=STH_STU_ID AND STU_ACD_ID=STH_ACD_ID" _
                             & " WHERE STU_ACD_ID=" + hACD_ID.Value _
                             & " AND STU_SCT_ID=" + hSCT_ID.Value _
                             & " AND STU_CURRSTATUS<>'CN'" _
                             & " ORDER BY STU_PASPRTNAME"

        Else
            str_query = "SELECT STU_NO as STUNO,ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')) AS STUNAME," _
                             & " ISNULL(STH_FATHERNAME,ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'')) FATHERNAME," _
                             & " ISNULL(STH_MOTHERNAME,ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'')) MOTHERNAME," _
                             & " ISNULL(STH_ROLLNO,'') ROLLNO," _
                             & " ISNULL(STH_REGNO,'') REGNO," _
                             & " ISNULL(STH_HEIGHT,'') HEIGHT,ISNULL(STH_WEIGHT,'') WEIGHT," _
                             & " ISNULL(STH_BLOODGROUP,ISNULL(STU_BLOODGROUP,'--')) BLOODGROUP," _
                             & " ISNULL(STH_VISION_L,'') VISIONL,ISNULL(STH_VISION_R,'') VISIONR,ISNULL(STH_DENTAL,'') DENTAL" _
                             & " FROM STUDENT_M AS A  INNER JOIN STUDENT_PROMO_S AS B ON STU_ID=STP_STU_ID" _
                             & " INNER JOIN STUDENT_D AS D ON A.STU_SIBLING_ID=D.STS_STU_ID" _
                             & " LEFT OUTER JOIN STUDENT_HEALTHSTATUS AS C ON " _
                             & " STP_STU_ID=STH_STU_ID AND STP_ACD_ID=STH_ACD_ID" _
                             & " WHERE STP_ACD_ID=" + hACD_ID.Value _
                             & " AND STP_SCT_ID=" + hSCT_ID.Value _
                             & " ORDER BY STU_PASPRTNAME"

        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim dt As DataTable
        dt = ds.Tables(0)

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        'ws.HeadersFooters.AlignWithMargins = True
        ef.Save(tempFileName)

        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))

        'HttpContext.Current.Response.WriteFile(tempFileName)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()

        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        System.IO.File.Delete(tempFileName)
        lblError.Text = "Download Completed"
    End Sub
End Class
