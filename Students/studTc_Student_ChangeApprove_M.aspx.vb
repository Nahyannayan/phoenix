Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studTc_Student_ChangeApprove_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100257") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))




                hfTCM_ID.Value = 0
                hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                hfSCT_ID.Value = Encr_decrData.Decrypt(Request.QueryString("sctid").Replace(" ", "+"))
                hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                txtGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                txtSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                txtName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                txtStudID_Fee.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                txtDoJ.Text = Format(Date.Parse(Encr_decrData.Decrypt(Request.QueryString("doj").Replace(" ", "+"))), "dd/MMM/yyyy")
                BindData()
                txtAprDate.Text = Format(Now.Date, "dd/MMM/yyyy")
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCM_REFNO,TCM_APPLYDATE,TCM_LEAVEDATE," _
                                 & " TCM_LASTATTDATE,TCM_ID,D.TCT_CODE,B.TCT_DESCR,STT_REQDATE,STT_REQREASON,D.TCT_DESCR,STT_ID,STT_REQTYPE FROM " _
                                 & " TCM_M AS A INNER JOIN TC_TFRTYPE_M AS B " _
                                 & " ON A.TCM_TCTYPE=B.TCT_CODE" _
                                 & " INNER JOIN STUDENT_TCTYPES_S AS C ON A.TCM_ID=C.STT_TCM_ID " _
                                 & " INNER JOIN TC_TFRTYPE_M AS D " _
                                 & " ON C.STT_TCTYPE=D.TCT_CODE" _
                                 & " WHERE TCM_STU_ID=" + hfSTU_ID.Value _
                                 & " AND TCM_CANCELDATE IS NULL AND TCM_TCSO='TC' AND STT_bAPPROVED='FALSE'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            With reader
                txtRefNo.Text = .GetString(0)
                txtApplyDate.Text = Format(.GetDateTime(1), "dd/MMM/yyyy")
                txtLeave_Date.Text = Format(.GetDateTime(2), "dd/MMM/yyyy")
                txtLast_Attend.Text = Format(.GetDateTime(3), "dd/MMM/yyyy")
                hfTCM_ID.Value = .GetValue(4)
                hfTCTAPR_CODE.Value = reader.GetValue(5)
                txtTCtype.Text = reader.GetString(6)
                txtReqDate.Text = Format(.GetDateTime(7), "dd/MMM/yyyy")
                txtReason.Text = .GetString(8)
                txtTCTypeReq.Text = reader.GetString(9)
                hfSTT_ID.Value = reader.GetValue(10)
                If Convert.ToString(reader("STT_REQTYPE")) = "PR" Then
                    rbParentRequest.Checked = True
                    rbDataEntry.Checked = False
                Else
                    rbDataEntry.Checked = True
                    rbParentRequest.Checked = False
                End If

            End With
        End While
        reader.Close()
    End Sub

    Function SaveData() As Boolean
        Dim REQTYPE As String = String.Empty
        If rbDataEntry.Checked = True Then
            REQTYPE = "DEM"
        ElseIf rbParentRequest.Checked = True Then
            REQTYPE = "PR"
        End If

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " saveSTUDENTTCCHANGEAPPROVE_Newver " _
                                  & hfSTT_ID.Value + "," _
                                  & hfTCM_ID.Value + "," _
                                  & hfSTU_ID.Value + "," _
                                  & hfACD_ID.Value + "," _
                                  & "'" + Session("SBSUID") + "'," _
                                  & "'" + hfGRD_ID.Value + "'," _
                                  & hfSCT_ID.Value + "," _
                                  & "'" + Format(Date.Parse(txtAprDate.Text), "yyyy-MM-dd") + "'," _
                                  & "'" + txtRemarks.Text + "'," _
                                  & hfTCTAPR_CODE.Value + "," _
                                   & "'" + REQTYPE + "'"
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                UtilityObj.InsertAuditdetails(transaction, "edit", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + hfTCM_ID.Value.ToString, "TCM_M_CHANGE_TCTYPE_APPROVE")

                SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using


    End Function

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
End Class
