<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studHeadofYear_View.aspx.vb" Inherits="Students_studHeadofYear_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
     Head Of Year Setup </div> 
    <div class="card-body">
            <div class="table-responsive m-auto">

    <table id="tbl_ShowScreen" runat="server" align="center" width="100%"
        cellpadding="5" cellspacing="0" >
        <tr >
            <td align="left">
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr >
            <td align="center" >
                <table id="Table1" runat="server" align="center"  width="100%" cellpadding="5" cellspacing="0" >
                  
                    <tr>
                        <td align="left" width="20%" >
                            <asp:Label id="lblAccText" runat="server" CssClass="field-label" Text="Select Academic Year " >
                            </asp:Label></td>
                      
                        <td align="left"  width="20%">
                            <asp:DropDownList id="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>

                        <td></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="3">
                            <table id="Table2" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:LinkButton id="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvStudTutor" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            BorderStyle="None" CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30">
                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>
                                                <asp:TemplateField Visible="False" HeaderText="ACCID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblHODID" runat="server" Text='<%# Bind("HODID") %>' __designer:wfdid="w2"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRD_DESCR") %>' __designer:wfdid="w1"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Head Of Year">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblClassTeacher" runat="server" Text='<%# Bind("EMP_NAME") %>' __designer:wfdid="w5"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="False" HeaderText="TeacherId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTeacherId" runat="server" Text='<%# Bind("STAFF_ID") %>' __designer:wfdid="w6" Visible="False"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField HeaderText="View" Text="View" CommandName="View">
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                </asp:ButtonField>
                                            </Columns>
                                            <RowStyle CssClass="griditem"  Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle CssClass="gridheader_pop"  Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                &nbsp;&nbsp;
            </td>
        </tr>
    </table>

                </div> 
        </div> 
    </div> 

</asp:Content>

