<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studEnqForm.aspx.vb" Inherits="studEnqForm"
    ClientTarget="uplevel" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="aspAjax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="MScap" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.5)">
    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <title>Student Registration Form</title>

    <script language="javascript" type="text/javascript">
        function ValidatorsRef() {

            var state = document.getElementById('rbRefYes');


            ValidatorEnable(document.getElementById('rfvRef_code'), state.checked);

            ValidatorEnable(document.getElementById('rfvRef_Email'), state.checked);
            ValidatorEnable(document.getElementById('vsRef_content'), state.checked);
            if (state.checked == true) {
                document.getElementById('vsRef_content').style.visibility = "visible"
                document.getElementById('vsRef_content').style.display = "block"

            } else {
                document.getElementById('vsRef_content').style.visibility = "hidden"
                document.getElementById('vsRef_content').style.display = "none"
                document.getElementById('txtREFEmail').value = "";
                document.getElementById('txtRefCode').value = "";
            }

        }





        function Hide_me() {
            var chkF = document.getElementById('chkFHide');
            var chkM = document.getElementById('chkMHide');
            var chkG = document.getElementById('chkGHide');

            var i = 0;

            for (i = 111; i <= 126; i++) {
                if (chkF.checked == 1) {
                    document.getElementById(i).style.display = 'none';
                }
                else {
                    document.getElementById(i).style.display = 'table-row';

                }
            }


            for (i = 135; i <= 150; i++) {
                if (chkM.checked == 1)
                { document.getElementById(i).style.display = 'none'; }
                else {
                    document.getElementById(i).style.display = 'table-row';

                }


            }


            for (i = 160; i <= 175; i++) {
                if (chkG.checked == 1)
                { document.getElementById(i).style.display = 'none'; }
                else {
                    document.getElementById(i).style.display = 'table-row';

                }
            }

            document.getElementById(127).style.display = 'table-row';
            document.getElementById(128).style.display = 'none';
            document.getElementById(129).style.display = 'none';
            document.getElementById(130).style.display = 'table-row';
            document.getElementById(131).style.display = 'none';
            document.getElementById(132).style.display = 'none';
            document.getElementById(151).style.display = 'table-row';
            document.getElementById(152).style.display = 'none';
            document.getElementById(153).style.display = 'none';
            document.getElementById(154).style.display = 'table-row';
            document.getElementById(155).style.display = 'none';
            document.getElementById(156).style.display = 'none';



        }


        function copyFathertoMother(chkThis) {
            var chk_state = chkThis.checked;
            if (chk_state == true) {
                document.getElementById('ddlMPri_National1').selectedIndex = document.getElementById('ddlFPri_National1').selectedIndex;
                document.getElementById('ddlMPri_National2').selectedIndex = document.getElementById('ddlFPri_National2').selectedIndex;
                document.getElementById('txtMAdd1_overSea').value = document.getElementById('txtFAdd1_overSea').value;
                document.getElementById('txtMAdd2_overSea').value = document.getElementById('txtFAdd2_overSea').value;
                document.getElementById('txtMOverSeas_Add_City').value = document.getElementById('txtFOverSeas_Add_City').value;
                document.getElementById('ddlMOverSeas_Add_Country').selectedIndex = document.getElementById('ddlFOverSeas_Add_Country').selectedIndex;
                document.getElementById('txtMPhone_Oversea_Country').value = document.getElementById('txtFPhone_Oversea_Country').value;
                document.getElementById('txtMPhone_Oversea_Area').value = document.getElementById('txtFPhone_Oversea_Area').value
                document.getElementById('txtMPhone_Oversea_No').value = document.getElementById('txtFPhone_Oversea_No').value;
                document.getElementById('txtMPoBox_Pri').value = document.getElementById('txtFPoBox_Pri').value;
                document.getElementById('txtMApartNo').value = document.getElementById('txtFApartNo').value;
                document.getElementById('txtMBldg').value = document.getElementById('txtFBldg').value;
                document.getElementById('txtMStreet').value = document.getElementById('txtFStreet').value;
                document.getElementById('txtMArea').value = document.getElementById('txtFArea').value;
                document.getElementById('txtMCity_pri').value = document.getElementById('txtFCity_pri').value;
                document.getElementById('ddlMCountry_Pri').selectedIndex = document.getElementById('ddlFCountry_Pri').selectedIndex;
                document.getElementById('txtMPoboxLocal').value = document.getElementById('txtFPoboxLocal').value;
                document.getElementById('ddlMEmirate').selectedIndex = document.getElementById('ddlFEmirate').selectedIndex;
                document.getElementById('txtMHPhone_Country').value = document.getElementById('txtFHPhone_Country').value;
                document.getElementById('txtMHPhone_Area').value = document.getElementById('txtFHPhone_Area').value;
                document.getElementById('txtMHPhone').value = document.getElementById('txtFHPhone').value;
                document.getElementById('txtMM_Country').value = document.getElementById('txtFM_Country').value;
                document.getElementById('txtMM_Area').value = document.getElementById('txtFM_Area').value;
                document.getElementById('txtMMobile_Pri').value = document.getElementById('txtFMobile_Pri').value;
                document.getElementById('txtMOPhone_Country').value = document.getElementById('txtFOPhone_Country').value;
                document.getElementById('txtMOPhone_Area').value = document.getElementById('txtFOPhone_Area').value;
                document.getElementById('txtMOPhone').value = document.getElementById('txtFOPhone').value;
                document.getElementById('txtMFaxNo_country').value = document.getElementById('txtFFaxNo_country').value;
                document.getElementById('txtMFaxNo_Area').value = document.getElementById('txtFFaxNo_Area').value;
                document.getElementById('txtMFaxNo').value = document.getElementById('txtFFaxNo').value;
            }
            else {
                document.getElementById('ddlMPri_National1').selectedIndex = 0;
                document.getElementById('ddlMPri_National2').selectedIndex = 0;
                document.getElementById('txtMAdd1_overSea').value = "";
                document.getElementById('txtMAdd2_overSea').value = "";
                document.getElementById('txtMOverSeas_Add_City').value = "";
                document.getElementById('ddlMOverSeas_Add_Country').selectedIndex = 0;
                document.getElementById('txtMPhone_Oversea_Country').value = "";
                document.getElementById('txtMPhone_Oversea_Area').value = "";
                document.getElementById('txtMPhone_Oversea_No').value = "";
                document.getElementById('txtMPoBox_Pri').value = "";
                document.getElementById('txtMApartNo').value = "";
                document.getElementById('txtMBldg').value = "";
                document.getElementById('txtMStreet').value = "";
                document.getElementById('txtMArea').value = "";
                document.getElementById('txtMCity_pri').value = "";
                document.getElementById('ddlMCountry_Pri').selectedIndex = 0;
                document.getElementById('txtMPoboxLocal').value = "";
                document.getElementById('ddlMEmirate').selectedIndex = 0;
                document.getElementById('txtMHPhone_Country').value = "";
                document.getElementById('txtMHPhone_Area').value = "";
                document.getElementById('txtMHPhone').value = "";
                document.getElementById('txtMM_Country').value = "";
                document.getElementById('txtMM_Area').value = "";
                document.getElementById('txtMMobile_Pri').value = "";
                document.getElementById('txtMOPhone_Country').value = "";
                document.getElementById('txtMOPhone_Area').value = "";
                document.getElementById('txtMOPhone').value = "";
                document.getElementById('txtMFaxNo_country').value = "";
                document.getElementById('txtMFaxNo_Area').value = "";
                document.getElementById('txtMFaxNo').value = "";
            }
            return true;
        }

        function copyFathertoGuardian(chkThis) {
            var chk_state = chkThis.checked;
            if (chk_state == true) {
                document.getElementById('ddlGPri_National1').selectedIndex = document.getElementById('ddlFPri_National1').selectedIndex;
                document.getElementById('ddlGPri_National2').selectedIndex = document.getElementById('ddlFPri_National2').selectedIndex;
                document.getElementById('txtGAdd1_overSea').value = document.getElementById('txtFAdd1_overSea').value;
                document.getElementById('txtGAdd2_overSea').value = document.getElementById('txtFAdd2_overSea').value;
                document.getElementById('txtGOverSeas_Add_City').value = document.getElementById('txtFOverSeas_Add_City').value;
                document.getElementById('ddlGOverSeas_Add_Country').selectedIndex = document.getElementById('ddlFOverSeas_Add_Country').selectedIndex;
                document.getElementById('txtGPhone_Oversea_Country').value = document.getElementById('txtFPhone_Oversea_Country').value;
                document.getElementById('txtGPhone_Oversea_Area').value = document.getElementById('txtFPhone_Oversea_Area').value
                document.getElementById('txtGPhone_Oversea_No').value = document.getElementById('txtFPhone_Oversea_No').value;
                document.getElementById('txtGPoBox_Pri').value = document.getElementById('txtFPoBox_Pri').value;
                document.getElementById('txtGApartNo').value = document.getElementById('txtFApartNo').value;
                document.getElementById('txtGBldg').value = document.getElementById('txtFBldg').value;
                document.getElementById('txtGStreet').value = document.getElementById('txtFStreet').value;
                document.getElementById('txtGArea').value = document.getElementById('txtFArea').value;
                document.getElementById('txtGCity_pri').value = document.getElementById('txtFCity_pri').value;
                document.getElementById('ddlGCountry_Pri').selectedIndex = document.getElementById('ddlFCountry_Pri').selectedIndex;
                document.getElementById('txtGPoboxLocal').value = document.getElementById('txtFPoboxLocal').value;
                document.getElementById('ddlGEmirate').selectedIndex = document.getElementById('ddlFEmirate').selectedIndex;
                document.getElementById('txtGHPhone_Country').value = document.getElementById('txtFHPhone_Country').value;
                document.getElementById('txtGHPhone_Area').value = document.getElementById('txtFHPhone_Area').value;
                document.getElementById('txtGHPhone').value = document.getElementById('txtFHPhone').value;
                document.getElementById('txtGM_Country').value = document.getElementById('txtFM_Country').value;
                document.getElementById('txtGM_Area').value = document.getElementById('txtFM_Area').value;
                document.getElementById('txtGMobile_Pri').value = document.getElementById('txtFMobile_Pri').value;
                document.getElementById('txtGOPhone_Country').value = document.getElementById('txtFOPhone_Country').value;
                document.getElementById('txtGOPhone_Area').value = document.getElementById('txtFOPhone_Area').value;
                document.getElementById('txtGOPhone').value = document.getElementById('txtFOPhone').value;
                document.getElementById('txtGFaxNo_country').value = document.getElementById('txtFFaxNo_country').value;
                document.getElementById('txtGFaxNo_Area').value = document.getElementById('txtFFaxNo_Area').value;
                document.getElementById('txtGFaxNo').value = document.getElementById('txtFFaxNo').value;
            }
            else {
                document.getElementById('ddlGPri_National1').selectedIndex = 0;
                document.getElementById('ddlGPri_National2').selectedIndex = 0;
                document.getElementById('txtGAdd1_overSea').value = "";
                document.getElementById('txtGAdd2_overSea').value = "";
                document.getElementById('txtGOverSeas_Add_City').value = "";
                document.getElementById('ddlGOverSeas_Add_Country').selectedIndex = 0;
                document.getElementById('txtGPhone_Oversea_Country').value = "";
                document.getElementById('txtGPhone_Oversea_Area').value = "";
                document.getElementById('txtGPhone_Oversea_No').value = "";
                document.getElementById('txtGPoBox_Pri').value = "";
                document.getElementById('txtGApartNo').value = "";
                document.getElementById('txtGBldg').value = "";
                document.getElementById('txtGStreet').value = "";
                document.getElementById('txtGArea').value = "";
                document.getElementById('txtGCity_pri').value = "";
                document.getElementById('ddlGCountry_Pri').selectedIndex = 0;
                document.getElementById('txtGPoboxLocal').value = "";
                document.getElementById('ddlGEmirate').selectedIndex = 0;
                document.getElementById('txtGHPhone_Country').value = "";
                document.getElementById('txtGHPhone_Area').value = "";
                document.getElementById('txtGHPhone').value = "";
                document.getElementById('txtGM_Country').value = "";
                document.getElementById('txtGM_Area').value = "";
                document.getElementById('txtGMobile_Pri').value = "";
                document.getElementById('txtGOPhone_Country').value = "";
                document.getElementById('txtGOPhone_Area').value = "";
                document.getElementById('txtGOPhone').value = "";
                document.getElementById('txtGFaxNo_country').value = "";
                document.getElementById('txtGFaxNo_Area').value = "";
                document.getElementById('txtGFaxNo').value = "";
            }
            return true;
        }

        function copyMothertoGuardian(chkThis) {
            var chk_state = chkThis.checked;
            if (chk_state == true) {
                document.getElementById('ddlGPri_National1').selectedIndex = document.getElementById('ddlMPri_National1').selectedIndex;
                document.getElementById('ddlGPri_National2').selectedIndex = document.getElementById('ddlMPri_National2').selectedIndex;
                document.getElementById('txtGAdd1_overSea').value = document.getElementById('txtMAdd1_overSea').value;
                document.getElementById('txtGAdd2_overSea').value = document.getElementById('txtMAdd2_overSea').value;
                document.getElementById('txtGOverSeas_Add_City').value = document.getElementById('txtMOverSeas_Add_City').value;
                document.getElementById('ddlGOverSeas_Add_Country').selectedIndex = document.getElementById('ddlMOverSeas_Add_Country').selectedIndex;
                document.getElementById('txtGPhone_Oversea_Country').value = document.getElementById('txtMPhone_Oversea_Country').value;
                document.getElementById('txtGPhone_Oversea_Area').value = document.getElementById('txtMPhone_Oversea_Area').value
                document.getElementById('txtGPhone_Oversea_No').value = document.getElementById('txtMPhone_Oversea_No').value;
                document.getElementById('txtGPoBox_Pri').value = document.getElementById('txtMPoBox_Pri').value;
                document.getElementById('txtGApartNo').value = document.getElementById('txtMApartNo').value;
                document.getElementById('txtGBldg').value = document.getElementById('txtMBldg').value;
                document.getElementById('txtGStreet').value = document.getElementById('txtMStreet').value;
                document.getElementById('txtGArea').value = document.getElementById('txtMArea').value;
                document.getElementById('txtGCity_pri').value = document.getElementById('txtMCity_pri').value;
                document.getElementById('ddlGCountry_Pri').selectedIndex = document.getElementById('ddlMCountry_Pri').selectedIndex;
                document.getElementById('txtGPoboxLocal').value = document.getElementById('txtMPoboxLocal').value;
                document.getElementById('ddlGEmirate').selectedIndex = document.getElementById('ddlMEmirate').selectedIndex;
                document.getElementById('txtGHPhone_Country').value = document.getElementById('txtMHPhone_Country').value;
                document.getElementById('txtGHPhone_Area').value = document.getElementById('txtMHPhone_Area').value;
                document.getElementById('txtGHPhone').value = document.getElementById('txtMHPhone').value;
                document.getElementById('txtGM_Country').value = document.getElementById('txtMM_Country').value;
                document.getElementById('txtGM_Area').value = document.getElementById('txtMM_Area').value;
                document.getElementById('txtGMobile_Pri').value = document.getElementById('txtMMobile_Pri').value;
                document.getElementById('txtGOPhone_Country').value = document.getElementById('txtMOPhone_Country').value;
                document.getElementById('txtGOPhone_Area').value = document.getElementById('txtMOPhone_Area').value;
                document.getElementById('txtGOPhone').value = document.getElementById('txtMOPhone').value;
                document.getElementById('txtGFaxNo_country').value = document.getElementById('txtMFaxNo_country').value;
                document.getElementById('txtGFaxNo_Area').value = document.getElementById('txtMFaxNo_Area').value;
                document.getElementById('txtGFaxNo').value = document.getElementById('txtMFaxNo').value;
            }
            else {
                document.getElementById('ddlGPri_National1').selectedIndex = 0;
                document.getElementById('ddlGPri_National2').selectedIndex = 0;
                document.getElementById('txtGAdd1_overSea').value = "";
                document.getElementById('txtGAdd2_overSea').value = "";
                document.getElementById('txtGOverSeas_Add_City').value = "";
                document.getElementById('ddlGOverSeas_Add_Country').selectedIndex = 0;
                document.getElementById('txtGPhone_Oversea_Country').value = "";
                document.getElementById('txtGPhone_Oversea_Area').value = "";
                document.getElementById('txtGPhone_Oversea_No').value = "";
                document.getElementById('txtGPoBox_Pri').value = "";
                document.getElementById('txtGApartNo').value = "";
                document.getElementById('txtGBldg').value = "";
                document.getElementById('txtGStreet').value = "";
                document.getElementById('txtGArea').value = "";
                document.getElementById('txtGCity_pri').value = "";
                document.getElementById('ddlGCountry_Pri').selectedIndex = 0;
                document.getElementById('txtGPoboxLocal').value = "";
                document.getElementById('ddlGEmirate').selectedIndex = 0;
                document.getElementById('txtGHPhone_Country').value = "";
                document.getElementById('txtGHPhone_Area').value = "";
                document.getElementById('txtGHPhone').value = "";
                document.getElementById('txtGM_Country').value = "";
                document.getElementById('txtGM_Area').value = "";
                document.getElementById('txtGMobile_Pri').value = "";
                document.getElementById('txtGOPhone_Country').value = "";
                document.getElementById('txtGOPhone_Area').value = "";
                document.getElementById('txtGOPhone').value = "";
                document.getElementById('txtGFaxNo_country').value = "";
                document.getElementById('txtGFaxNo_Area').value = "";
                document.getElementById('txtGFaxNo').value = "";
            }
            return true;
        }


        function CHK_CONTACT_DETAIL() {
            var str_con = '';
            var fname = '';
            var email = '';
            var Fchk = document.getElementById('rdPri_Father');
            var Mchk = document.getElementById('rbPri_Mother');
            var Gchk = document.getElementById('rdPri_Guard');
            if (Fchk.checked) {
                fName = document.getElementById('txtFPri_Fname').value;
                email = document.getElementById('txtFEmail_Pri').value;
                if (fName.replace(/^\s*|\s*$/g, '') == '') {
                    styleObj = document.getElementById(100).style;
                    styleObj.display = '';
                    str_con = str_con + 'Father\'s first name is mandatory \n'
                }
                else {
                    styleObj = document.getElementById(100).style;
                    styleObj.display = 'none';
                }

                if (email.replace(/^\s*|\s*$/g, '') == '') {
                    str_con = str_con + 'Father\'s email address is required \n'
                    styleObj1 = document.getElementById(101).style;
                    styleObj1.display = '';
                }
                else {
                    styleObj1 = document.getElementById(101).style;
                    styleObj1.display = 'none';
                }
            }

            else if (Mchk.checked) {
                fName = document.getElementById('txtMPri_Fname').value;
                email = document.getElementById('txtMEmail_Pri').value;
                if (fName.replace(/^\s*|\s*$/g, '') == '') {
                    styleObj2 = document.getElementById(102).style;
                    styleObj2.display = '';
                    str_con = str_con + 'Mother\'s first name is mandatory \n'
                }
                else {
                    styleObj2 = document.getElementById(102).style;
                    styleObj2.display = 'none';
                }

                if (email.replace(/^\s*|\s*$/g, '') == '') {
                    styleObj3 = document.getElementById(103).style;
                    styleObj3.display = '';
                    str_con = str_con + 'Mother\'s email address is required \n'
                }
                else {
                    styleObj3 = document.getElementById(103).style;
                    styleObj3.display = 'none';
                }
            }
            else if (Gchk.checked) {
                fName = document.getElementById('txtGPri_Fname').value;
                email = document.getElementById('txtGEmail_Pri').value;
                if (fName.replace(/^\s*|\s*$/g, '') == '') {
                    styleObj4 = document.getElementById(104).style;
                    styleObj4.display = '';
                    str_con = str_con + 'Guardian\'s first name is mandatory \n'
                }
                else {
                    styleObj4 = document.getElementById(104).style;
                    styleObj4.display = 'none';
                }
                if (email.replace(/^\s*|\s*$/g, '') == '') {
                    styleObj5 = document.getElementById(105).style;
                    styleObj5.display = '';
                    str_con = str_con + 'Guardian\'s email address is required \n'
                }
                else {
                    styleObj5 = document.getElementById(105).style;
                    styleObj5.display = 'none';
                }
            }
            if (str_con != '') {
                document.getElementById('lblError').innerText = str_con;
                return false;
            }
            else {
                return true;
            }
        }


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }



        function DisableButton(varno) {

            var browserName = navigator.appName;
            if (browserName != "Microsoft Internet Explorer") {
                if (varno == 1) {
                    if (document.getElementById('btnPrevSave1').value != null) {
                        document.getElementByid('btnPrevSave1').click();
                    }
                }
                else {
                    if (document.getElementById('btnPrevSave2').value != null) {
                        document.getElementByid('btnPrevSave2').click();
                    }
                }
            }


            document.forms[0].submit();
            window.setTimeout("disableButton('" + window.event.srcElement.id + "')", 0);
        }

        function disableButton(buttonID) {
            document.getElementById(buttonID).style.display = 'none';
            document.getElementById('btnPrevedit').style.display = 'none';
            document.getElementById('btnPrevSave1').style.display = 'none';
            document.getElementById('btnPrevClose1').style.display = 'none';
        }






        function fill_Staff(obj) {

            var instals = eval(obj.checked);



            if (instals == true) {
                styleObj1 = document.getElementById(128).style;
                styleObj2 = document.getElementById(129).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {

                styleObj1 = document.getElementById(128).style;
                styleObj2 = document.getElementById(129).style;

                styleObj1.display = 'none';
                styleObj2.display = 'none';


            }

        }


        function fill_Staffs() {


            if (document.getElementById('chkFStaff_GEMS').checked == true) {
                styleObj1 = document.getElementById(128).style;
                styleObj2 = document.getElementById(129).style;
                styleObj1.display = '';
                styleObj2.display = '';
            }
            else {

                styleObj1 = document.getElementById(128).style;
                styleObj2 = document.getElementById(129).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';

            }

        }

        function fill_Stud(obj) {
            var instals = eval(obj.checked);
            if (instals == true) {
                styleObj1 = document.getElementById(131).style;
                styleObj2 = document.getElementById(132).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {

                styleObj1 = document.getElementById(131).style;
                styleObj2 = document.getElementById(132).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';


            }

        }

        function fill_Studs() {
            if (document.getElementById('chkFExStud_Gems').checked == true) {
                styleObj1 = document.getElementById(131).style;
                styleObj2 = document.getElementById(132).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {
                styleObj1 = document.getElementById(131).style;
                styleObj2 = document.getElementById(132).style;

                styleObj1.display = 'none';
                styleObj2.display = 'none';


            }

        }

        //--------------------------------------------



        function fill_MStaff(obj) {

            var instals = eval(obj.checked);



            if (instals == true) {
                styleObj1 = document.getElementById(152).style;
                styleObj2 = document.getElementById(153).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {

                styleObj1 = document.getElementById(152).style;
                styleObj2 = document.getElementById(153).style;

                styleObj1.display = 'none';
                styleObj2.display = 'none';


            }

        }


        function fill_MStaffs() {


            if (document.getElementById('chkMStaff_GEMS').checked == true) {
                styleObj1 = document.getElementById(152).style;
                styleObj2 = document.getElementById(153).style;
                styleObj1.display = '';
                styleObj2.display = '';
            }
            else {

                styleObj1 = document.getElementById(153).style;
                styleObj2 = document.getElementById(153).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';

            }

        }

        function fill_MStud(obj) {
            var instals = eval(obj.checked);
            if (instals == true) {
                styleObj1 = document.getElementById(155).style;
                styleObj2 = document.getElementById(156).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {

                styleObj1 = document.getElementById(155).style;
                styleObj2 = document.getElementById(156).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';


            }

        }

        function fill_MStuds() {
            if (document.getElementById('chkMExStud_Gems').checked == true) {
                styleObj1 = document.getElementById(155).style;
                styleObj2 = document.getElementById(156).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {
                styleObj1 = document.getElementById(155).style;
                styleObj2 = document.getElementById(156).style;

                styleObj1.display = 'none';
                styleObj2.display = 'none';


            }

        }

        //------------------------------------------





        function fill_Sibl(obj) {

            var instals = eval(obj.checked);

            if (instals == true) {
                styleObj1 = document.getElementById(18).style;
                styleObj2 = document.getElementById(19).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {

                styleObj1 = document.getElementById(18).style;
                styleObj2 = document.getElementById(19).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';

            }

        }


        function fill_Sibls() {

            var instals = document.getElementById('chkSibling').checked;


            if (instals == true) {
                styleObj1 = document.getElementById(18).style;
                styleObj2 = document.getElementById(19).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {
                styleObj1 = document.getElementById(18).style;
                styleObj2 = document.getElementById(19).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';


            }

        }




        function fill_Tran(obj) {
            var instals = eval(obj.checked);


            if (instals == true) {
                styleObj1 = document.getElementById(16).style;
                styleObj2 = document.getElementById(17).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {

                styleObj1 = document.getElementById(16).style;
                styleObj2 = document.getElementById(17).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';

            }

        }

        function fill_Tran() {

            var instals = document.getElementById('chkTran_Req').checked;


            if (instals == true) {
                styleObj1 = document.getElementById(16).style;
                styleObj2 = document.getElementById(17).style;
                styleObj1.display = '';
                styleObj2.display = '';

            }
            else {

                styleObj1 = document.getElementById(16).style;
                styleObj2 = document.getElementById(17).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';


            }

        }



        function fillPassport() {
            var passIssu = document.getElementById('txtPassIss_date')

            if (passIssu.value != "") {
                if (document.getElementById('txtPassExp_Date').value == "") {
                    document.getElementById('txtPassExp_Date').value = AddYears(passIssu.value, 5)
                }
                else {
                    document.getElementById('txtPassExp_Date').value = '';
                    document.getElementById('txtPassExp_Date').value = AddYears(passIssu.value, 5)
                }
            }



        }

        function fillVisa() {
            var VisaIssu = document.getElementById('txtVisaIss_date')


            if (VisaIssu.value != "") {
                if (document.getElementById('txtVisaExp_date').value == "") {
                    document.getElementById('txtVisaExp_date').value = AddYears(VisaIssu.value, 3)
                }
                else {
                    document.getElementById('txtVisaExp_date').value = '';
                    document.getElementById('txtVisaExp_date').value = AddYears(VisaIssu.value, 3)
                }
            }

        }

        function AddYears(dtStr, num) {
            var dtCh = "/";
            var pos1 = dtStr.indexOf(dtCh)
            var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
            var strMonth = dtStr.substring(pos1 + 1, pos2)
            var tempMonth = dtStr.substring(pos1 + 1, pos2)
            var tempValid = false;
            var strDay = dtStr.substring(0, pos1)
            var strYear = dtStr.substring(pos2 + 1)
            var monthname = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

            if (isNaN(strMonth)) {
                for (var i = 0; i < monthname.length; i++) {
                    if (monthname[i].toLowerCase() == strMonth.toLowerCase()) {
                        strMonth = i + 1;
                        strMonth = strMonth + "";
                        tempValid = true;
                    }
                }
            }
            else if ((parseInt(strMonth) >= 1) && (parseInt(strMonth) <= 12)) {
                tempValid = true;
            }

            if (tempValid == false) {
                return '';
            }


            if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
            if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)


            strMonth = strMonth - 1;

            var myDate = new Date()
            myDate.setFullYear(strYear, strMonth, strDay)
            myDate.setDate(myDate.getDate() - 1)
            myDate.setFullYear(myDate.getFullYear() + num)


            var lstrDay = myDate.getDate()
            var lstrMonth = myDate.getMonth()
            lstrMonth = parseInt(lstrMonth) + 1


            if (parseInt(lstrDay) < 10) {
                lstrDay = "0" + lstrDay
            }


            if (isNaN(tempMonth)) {
                lstrMonth = monthname[lstrMonth - 1]
            }
            else {
                if (parseInt(lstrMonth) < 10) {
                    lstrMonth = "0" + lstrMonth
                }
            }

            return (lstrDay + "/" + lstrMonth + "/" + myDate.getFullYear())
        }

          
          
        
    </script>

    <script language="javascript" type="text/javascript">

        function changePopupPanel(rowState) {
            //if($find("mpe"))
            //{
            //$find("mpe").dispose();}

            // $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OnOkScript":"onPrvOk();","PopupControlID":"plPrev","dynamicServicePath":"/studEnqForm.aspx","id":"mpe"}, null, null, $get("btnTarget"));       



            if (rowState == 1) {

                styleObj1 = document.getElementById(81).style;
                styleObj2 = document.getElementById(82).style;
                styleObj3 = document.getElementById(83).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';
                styleObj3.display = 'none';
            }
            else {

                styleObj1 = document.getElementById(81).style;
                styleObj2 = document.getElementById(82).style;
                styleObj3 = document.getElementById(83).style;
                styleObj1.display = '';
                styleObj2.display = '';
                styleObj3.display = '';

            }

            //$find("mpe").show();       
        }




        function GetSelectedItem() {
            var e = document.getElementById('ddlFLang');
            var fLang = e.options[e.selectedIndex].text;
            var CHK = document.getElementById('chkOLang');

            var checkbox = CHK.getElementsByTagName("input");

            var label = CHK.getElementsByTagName("label");

            for (var i = 0; i < checkbox.length; i++) {

                if (checkbox[i].checked) {
                    var olang = label[i].innerHTML
                    if (fLang == olang) {
                        checkbox[i].checked = false;
                    }
                }

            }

        }




        function chkFirst_lang() {

            GetSelectedItem();

        }
     
     
   
     
    </script>

    <style>
        .tbBorder
        {
            border-collapse: collapse;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #1B80B6;
        }
        .popupMenu
        {
            border: solid 1px #CCCCCC;
            background-color: #FFFFFF;
            padding: 5px; width:250px;z-index:2;position:absolute;visibility:hidden;opacity:.9;filter:alpha(opacity=90);font-size:12px;color:#1b80b6;}
        .close
        {
            display: block;
            background: url(../Images/closePanel.png) no-repeat 0px 0px;
            left: -14px;
            width: 26px;
            text-indent: -1000em;
            position: absolute;
            top: -17px;
            height: 28px;
        }
       
    </style>
</head>
<body style="margin-top: -1px">
    <form id="frmRegStudent" runat="server">
    <aspAjax:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="3600">
    </aspAjax:ScriptManager>
    <%-- <aspAjax:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
      
        
                             --%>
    <div>
        <div align="center">
            <table style="margin-top: -2px; border-collapse: collapse">
                <tr>
                    <td style="height: 91px;">
                        <img height="90" src="../images/loginImage/head.jpg" width="840" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 23px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 823px; background-repeat: repeat; height: 21%">
                        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%; margin-top: -1px;
                            border-collapse: collapse; height: 157px">
                            <tr>
                                <td style="height: 19px" width="22%">
                                    <img alt="" height="46" src="../images/loginImage/cellone_top.gif" width="192" />
                                </td>
                                <td style="background-image: url(Images/titlebackground.jpg); background-repeat: no-repeat;
                                    height: 19px; width: 740px;">
                                    <font color="#ffffff" face="Arial" size="4" style="display: inline; float: left;
                                        visibility: visible; text-align: left">&nbsp;&nbsp;&nbsp;Online Administration and
                                        Student Information System</font>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2" style="height: 350px" valign="top">
                                    <table style="margin-top: 0px; margin-left: -1px; width: 773px; border-collapse: collapse;
                                        border: 0; padding-left: 0px; height: 195px">
                                        <tr>
                                            <td align="left" rowspan="3" style="width: 180px; background-color: #1b80b6; height: 345px;"
                                                valign="top">
                                                <br />
                                                <asp:Menu ID="mnuRegMaster" runat="server" Font-Names="Verdana" Font-Size="8pt">
                                                    <StaticMenuItemStyle Font-Names="Verdana" Font-Size="10pt" Font-Strikeout="False" />
                                                    <StaticSelectedStyle Font-Names="Verdana" Font-Size="10pt" />
                                                    <DynamicSelectedStyle Font-Names="Verdana" Font-Size="10pt" />
                                                    <Items>
                                                        <asp:MenuItem ImageUrl="~/Images/FormRegImg/Schools_1.jpg" Selected="True" Text=" "
                                                            Value="0"></asp:MenuItem>
                                                        <asp:MenuItem ImageUrl="~/Images/FormRegImg/APPLICANT_1.jpg" Text=" " Value="1">
                                                        </asp:MenuItem>
                                                        <asp:MenuItem ImageUrl="~/Images/FormRegImg/PRIMARY_1.jpg" Text=" " Value="2"></asp:MenuItem>
                                                        <asp:MenuItem ImageUrl="~/Images/FormRegImg/PREVIOUS_1.jpg" Text=" " Value="3"></asp:MenuItem>
                                                        <asp:MenuItem ImageUrl="~/Images/FormRegImg/OtherDetails_1.jpg" Text=" " Value="4">
                                                        </asp:MenuItem>
                                                        <asp:MenuItem ImageUrl="~/Images/FormRegImg/Declaration_1.jpg" Text=" " Value="5">
                                                        </asp:MenuItem>
                                                    </Items>
                                                </asp:Menu>
                                            </td>
                                            <td align="left" valign="top" style="padding: 0px;">
                                                <asp:Image Style="display: inline; float: right; height: 30px; margin-left: -2px;
                                                    margin-top: -2px" ID="Image1" runat="server" ImageUrl="../Images/menu/top_curve.gif"
                                                    ImageAlign="Left"></asp:Image>
                                            </td>
                                            <td rowspan="3" style="width: 100%;" valign="top">
                                                <table width="98%">
                                                    <tr>
                                                        <td align="left" valign="baseline">
                                                            <asp:Literal ID="ltMessage" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="baseline" >
                                                            
                                                            <asp:ValidationSummary ID="vsPrim_Contact" runat="server" ValidationGroup="Prim_cont"
                                                                EnableViewState="False" CssClass="error" ForeColor="" />
                                                            <asp:ValidationSummary ID="vsPrev_Info" runat="server" ValidationGroup="Prev_Info"
                                                                CssClass="error" EnableViewState="False" ForeColor="" />
                                                            <asp:ValidationSummary ID="vsAppl_info" runat="server" EnableViewState="False" ValidationGroup="App_Info"
                                                                CssClass="error" ForeColor="" />
                                                            <asp:ValidationSummary ID="vsRef_content" runat="server" ValidationGroup="ref_cont"
                                                                EnableViewState="False" CssClass="error" ForeColor=""  />
                                                                <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 620px">
                                                            <asp:MultiView ID="mvRegMain" runat="server" ActiveViewIndex="0">
                                                                <asp:View ID="vwAcademic" runat="server">
                                                                    <div runat="server" id="hometab">
                                                                        <div style="border: 1px solid #1B80B6; width: 600px; height: 170px; text-align: left;
                                                                            font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #800000;
                                                                            padding: 5pt;">
                                                                            <center>
                                                                                <font size="3px"><u>Note </u></font>
                                                                            </center>
                                                                            <br />
                                                                             <asp:Literal ID="ltMain" runat="server"></asp:Literal>
                                                                            <p style="margin-top: 3; margin-bottom: 3; margin-left: 8px;" align="left">
                                                                                <img border="0" src="..\Images\acrobat_icon.jpg" width="30" height="35">
                                                                                <font color="#fcfcfc" face="Arial" size="2"><a href="..\HelpFiles\Enquiry\Help_OnlineEnquiry.pdf">
                                                                                    <b>Refer before completing the On-Line Enquiry form.</b> </a></font>
                                                                            </p>
                                                                        </div>
                                                                        <div style="text-align: right; padding-top: 1px; width: 99%;">
                                                                            <br />
                                                                            <asp:Button ID="btnHome" runat="server" Text="Continue" CausesValidation="False"
                                                                                CssClass="button" Height="25px" Width="80px" /></div>
                                                                    </div>
                                                                    <div runat="server" id="maintab">
                                                                        <table style="border-collapse: collapse" width="100%">
                                                                            <tr>
                                                                                <td align="left" style="width: 620px; height: 87px;">
                                                                                    <table align="left" border="1" style="border-collapse: collapse" bordercolor="#0066cc"
                                                                                        cellpadding="5" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td gray colspan="7" style="background-color: #1b80b6; height: 28px;"
                                                                                                valign="middle">
                                                                                                <span style="font-size: 10pt; color: #ffffff">Select School with following details</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td gray style="width: 16%; height: 26px">
                                                                                                School<span style="color: #ff0000">*</span>
                                                                                            </td>
                                                                                            <td gray style="width: 5px; height: 26px">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray colspan="4" style="height: 26px">
                                                                                                <asp:DropDownList ID="ddlGEMSSchool" runat="server" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td gray style="width: 16%; height: 26px">
                                                                                                Curriculum<span style="color: #ff0000">*</span>
                                                                                            </td>
                                                                                            <td gray style="width: 5px;">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray colspan="4" style=" height: 26px">
                                                                                                <asp:DropDownList ID="ddlCurri" runat="server" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                           
                                                                                        </tr>
                                                                                        
                                                                                        <tr style="font-size: 7pt; color: #ff0000" >
                                                                                           <td gray style="width: 20%; height: 26px">
                                                                                                Academic Year<span style="color: #ff0000">*</span>
                                                                                            </td>
                                                                                            <td gray style="width: 2px; color: #1b80b6; height: 26px">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray colspan="4" style=" height: 26px">
                                                                                                <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        
                                                                                        
                                                                                        <tr>
                                                                                            <td gray style="width: 16%; height: 26px;">
                                                                                                <asp:Literal ID="ltGrade_lb" runat="server"></asp:Literal><span style="color: #ff0000">*</span>
                                                                                            </td>
                                                                                            <td gray style="height: 26px; width: 5px;">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray colspan="4" style="height: 26px;">
                                                                                                <span style="color: #ff0000">
                                                                                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                                                                                    </asp:DropDownList>
                                                                                                </span>
                                                                                            </td>                                                                                      
                                                                                           
                                                                                            
                                                                                          
                                                                                          
                                                                                           
                                                                                        </tr>
                                                                                         <tr style="font-size: 7pt; color: #ff0000" id="trStream" runat="server">
                                                                                           <td gray style="width: 20%; height: 26px">
                                                                                                 <asp:Literal ID="ltStream_lb" runat="server"></asp:Literal><span style="color: #ff0000">*</span>
                                                                                            </td>
                                                                                            <td gray style="width: 2px; color: #1b80b6; height: 26px">
                                                                                                :
                                                                                            </td>
                                                                                           <td gray colspan="4" style="height: 20px;" >
                                                                                                <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        
                                                                                        <!-- Genmder -->
                                                                                        <tr style="font-size: 7pt; color: #ff0000" id="trShift" runat="server">
                                                                                           <td gray style="width: 20%; height: 26px;">
                                                                                                Meal Plan<span style="color: #1b80b6"><span style="color: #ff0000">*</span></span>
                                                                                            </td>
                                                                                            <td gray style="width: 2px; color: #1b80b6; height: 26px;">
                                                                                                <span>:</span>
                                                                                            </td>
                                                                                            <td gray style="width: 166px; height: 26px;" colspan="4">
                                                                                                <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                                <asp:RadioButton ID="rbMealYes" runat="server" GroupName="meal" Text="Yes" />
                                                                                                <asp:RadioButton ID="rbMealNo" runat="server" GroupName="Meal" Text="No" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr style="font-size: 7pt; color: #ff0000">
                                                                                            <td gray style="width: 16%; height: 20px">
                                                                                                Term/Semester<span style="color: #ff0000">*</span>
                                                                                            </td>
                                                                                            <td gray style="width: 5px; height: 20px">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray colspan="4" style="height: 20px">
                                                                                                <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr style="font-size: 7pt; color: #ff0000">
                                                                                            <td gray style="width: 16%; height: 20px">
                                                                                                Tentative Joining Date
                                                                                            </td>
                                                                                            <td gray style="width: 5px; height: 20px">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray colspan="4" style="height: 20px">
                                                                                                <asp:TextBox ID="txtDOJ" runat="server" Width="110px"></asp:TextBox>
                                                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                                                <br />
                                                                                                <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <!-- Grade & Section -->
                                                                                        <!-- Grade & Section -->
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3" align="center">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" style="width: 600px; height: 33px">
                                                                                    <asp:Label ID="lblErrorInfo" runat="server" EnableViewState="False"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" style="width: 600px; height: 21px">
                                                                                    &nbsp;<asp:Button ID="btnVwAcad_Next" runat="server" CssClass="button" Text="Next"
                                                                                        Height="25px" Width="80px" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <br />
                                                                    <div id="divRef" runat="server">
                                                                        <table style="border-collapse: collapse" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <table align="left" cellpadding="5" border="1px" cellspacing="0" width="600px">
                                                                                        <tr>
                                                                                            <td gray style="background-color: #1b80b6; " valign="middle"
                                                                                                colspan="2">
                                                                                                <span style="font-size: 10pt; color: #ffffff">Referred to GEMS ?</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr style="border-bottom: none 0px">
                                                                                            <td style="border-bottom: none 0px; " valign="middle" gray
                                                                                                colspan="2">
                                                                                                Were you referred to GEMS by a friend?
                                                                                                <asp:RadioButton ID="rbRefYes" runat="server" Text="Yes" GroupName="refer" Checked="true" />
                                                                                                <asp:RadioButton ID="rbRefNo" runat="server" Text="No" GroupName="refer" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr style="border-top: none 0px; border-bottom: none 0px">
                                                                                            <td gray style="border-top: none 0px; border-bottom: none 0px; height: 20px;"
                                                                                                colspan="2">
                                                                                                If �YES� enter the reference code & your reference email address.
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr style="border-top: none 0px; border-bottom: none 0px">
                                                                                            <td gray style="border-top: none 0px; border-bottom: none 0px; border-right: none 0px;
                                                                                                height: 20px; width: 200px;">
                                                                                                Reference Code(Case sensitive)
                                                                                            </td>
                                                                                            <td style="border-top: none 0px; border-bottom: none 0px; border-left: none 0px;">
                                                                                                <asp:TextBox ID="txtRefCode" runat="server" Width="75px"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="rfvRef_code" runat="server" ControlToValidate="txtRefCode"
                                                                                                    Display="Dynamic" ErrorMessage="Reference code required" ValidationGroup="ref_cont">*</asp:RequiredFieldValidator>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr style="border-top: none 0px; ">
                                                                                            <td gray style="border-top: none 0px; border-right: none 0px;">
                                                                                                Email Address
                                                                                            </td>
                                                                                            <td style="border-top: none 0px; border-bottom: none 0px; border-left: none 0px;">
                                                                                                <asp:TextBox ID="txtREFEmail" runat="server" Width="176px"></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="rfvRef_Email" runat="server" ControlToValidate="txtREFEmail"
                                                                                                    Display="Dynamic" ErrorMessage="Your reference email address required" ValidationGroup="ref_cont">*</asp:RequiredFieldValidator>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 600px; height: 33px;" align="right">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" style="height: 21px">
                                                                                    &nbsp;<asp:Button ID="btnRefPrev" runat="server" CausesValidation="False" CssClass="button"
                                                                                        Height="25px" Text="Previous" Width="80px" />
                                                                                    <asp:Button ID="btnRefNext" runat="server" CssClass="button" Height="25px" Text="Next"
                                                                                        ValidationGroup="ref_cont" Width="80px" OnClientClick="ValidatorsRef()" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </asp:View>
                                                                <asp:View ID="vwApp" runat="server">
                                                                    <table style="border-collapse: collapse" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table align="left" border="1" bordercolor="#0066cc" cellpadding="5" cellspacing="0"
                                                                                    style="border-collapse: collapse">
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 18px; background-color: #1b80b6" valign="middle">
                                                                                            <span style="font-size: 10pt; color: #ffffff">Applicant Info</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 24%">
                                                                                            Name As In Passport
                                                                                        </td>
                                                                                        <td gray style="width: 83px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:TextBox ID="txtFname" runat="server" MaxLength="100" Width="130px"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfvFname_Stud" runat="server" ControlToValidate="txtFname"
                                                                                                Display="Dynamic" ErrorMessage="First name required" ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                            <asp:TextBox ID="txtMname" runat="server" MaxLength="100" Width="130px" ValidationGroup="App_Info"></asp:TextBox>&nbsp;
                                                                                            <asp:TextBox ID="txtLname" runat="server" MaxLength="100" Width="130px"></asp:TextBox>
                                                                                            <br />
                                                                                            First Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Middle Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; Last Name
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 24%; height: 26px;">
                                                                                            Gender
                                                                                        </td>
                                                                                        <td gray style="height: 26px; width: 83px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%; height: 26px;">
                                                                                            <asp:RadioButton ID="rdMale" runat="server" GroupName="Gender" Text="Male" /><span
                                                                                                style="color: #ff0000"> </span>
                                                                                            <asp:RadioButton ID="rdFemale" runat="server" GroupName="Gender" Text="Female" />
                                                                                        </td>
                                                                                        <td gray style="width: 20%; height: 26px;">
                                                                                            Religion
                                                                                        </td>
                                                                                        <td gray style="width: 15px; color: #1b80b6; height: 26px;">
                                                                                            <span>:</span>
                                                                                        </td>
                                                                                        <td gray style="width: 166px; height: 26px;">
                                                                                            <asp:DropDownList ID="ddlReligion" runat="server">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfvReligion" runat="server" ControlToValidate="ddlReligion"
                                                                                                Display="Dynamic" ErrorMessage="Please select Religion" InitialValue=" " ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <!-- Genmder -->
                                                                                    <tr style="font-size: 7pt; color: #ff0000">
                                                                                        <td gray style="width: 24%; height: 31px;">
                                                                                            Date Of Birth
                                                                                        </td>
                                                                                        <td gray style="height: 31px; width: 83px;">
                                                                                            <span>:</span>
                                                                                        </td>
                                                                                        <td gray style="width: 34%; height: 31px;">
                                                                                            <asp:TextBox ID="txtDob" runat="server" Width="110px"></asp:TextBox>
                                                                                            <asp:ImageButton ID="imgBtnDob_date" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator
                                                                                                ID="rfvDatefrom" runat="server" ControlToValidate="txtDob" Display="Dynamic"
                                                                                                ErrorMessage="Date of birth cannot be left empty" ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                            <br />
                                                                                            <font size="1"><span style="font-size: 7pt">(dd/mmm/yyyy)</span></font>
                                                                                        </td>
                                                                                        <td gray style="width: 20%; height: 31px;">
                                                                                            Place Of Birth
                                                                                        </td>
                                                                                        <td gray style="width: 15px; color: #1b80b6; height: 31px;">
                                                                                            <span>:</span>
                                                                                        </td>
                                                                                        <td gray style="width: 166px; height: 31px;">
                                                                                            <asp:TextBox ID="txtPob" runat="server" Width="130px"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfvPob" runat="server" ControlToValidate="txtPob"
                                                                                                Display="Dynamic" ErrorMessage="Place of birth cannot be left empty" ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <!-- Grade & Section -->
                                                                                    <tr>
                                                                                        <td gray style="width: 24%; height: 24px;">
                                                                                            Country Of Birth
                                                                                        </td>
                                                                                        <td gray style="height: 24px; width: 83px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="height: 24px;">
                                                                                            <asp:DropDownList ID="ddlCountry" runat="server">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfvCOB" runat="server" ControlToValidate="ddlCountry"
                                                                                                Display="Dynamic" ErrorMessage="Please select Country of Birth" InitialValue=" "
                                                                                                ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td gray style="width: 20%; height: 24px;">
                                                                                            Nationality
                                                                                        </td>
                                                                                        <td gray style="width: 15px; height: 24px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="height: 24px;">
                                                                                            <asp:DropDownList ID="ddlNational" runat="server">
                                                                                            </asp:DropDownList>
                                                                                            <asp:RequiredFieldValidator ID="rfvNationality" runat="server" ControlToValidate="ddlNational"
                                                                                                Display="Dynamic" ErrorMessage="Please select Student Nationality" InitialValue=" "
                                                                                                ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <!-- Grade & Section -->
                                                                                    <tr>
                                                                                        <td gray style="width: 24%; height: 24px;">
                                                                                            Emergency Contact Number
                                                                                        </td>
                                                                                        <td gray style="height: 24px; width: 83px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="height: 24px">
                                                                                             <asp:TextBox ID="txtStud_Contact_Country" runat="server" Width="35px" MaxLength="3"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtStud_Contact_Area" runat="server" Width="35px" MaxLength="4"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtStud_Contact_No" runat="server" Width="85px" MaxLength="10"></asp:TextBox><br />
                                                                                            <span style="font-size: 7pt">(Country-Area-Number)</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 24%; height: 24px"> Ethnicity
                                                                                        </td>
                                                                                        <td gray style="width: 83px; height: 24px">:
                                                                                        </td>
                                                                                        <td gray colspan="4" style="height: 24px">
                                                                                         <asp:DropDownList ID="ddlEthnicity" runat="server">
                                                                                            </asp:DropDownList>
                                                                                            <br />
                                                                                            (If you choose other,please specify the ethnicity)<br />
                                                                                            <asp:TextBox ID="txtEthnicity" runat="server" MaxLength="150"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 10px; background-color: #75c6f2">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Passport Details
                                                                                                <img id="ImgPassHelp" src="../Images/help.png" align="absMiddle" height="20" runat="server" />
                                                                                                &nbsp;&nbsp; </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td  style="width: 24%; height: 32px">
                                                                                            Passport No
                                                                                        </td>
                                                                                        <td gray style="height: 32px; width: 83px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%; height: 32px">
                                                                                            <asp:TextBox ID="txtPassport" runat="server" Width="120px"></asp:TextBox><asp:RequiredFieldValidator
                                                                                                ID="rfvPassNo" runat="server" ControlToValidate="txtPassport" Display="Dynamic"
                                                                                                ErrorMessage="Passport No. required" ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                        <td gray style="width: 20%; height: 32px">
                                                                                            Passport Issue Place
                                                                                        </td>
                                                                                        <td gray style="width: 15px; height: 32px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px; height: 32px">
                                                                                            <asp:TextBox ID="txtPassportIssue" runat="server" Width="130px"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="rfvPass_Issue" runat="server" ControlToValidate="txtPassportIssue"
                                                                                                Display="Dynamic" ErrorMessage="Passport Issue Place required" ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 24%; height: 28px">
                                                                                            Passport Issue Date
                                                                                        </td>
                                                                                        <td gray style="height: 28px; width: 83px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%; height: 28px">
                                                                                            <asp:TextBox ID="txtPassIss_date" runat="server" Width="110px"></asp:TextBox>&nbsp;<asp:ImageButton
                                                                                                ID="imgBtnPassIss_date" runat="server" ImageUrl="~/Images/calendar.gif" Height="16px" /><asp:RequiredFieldValidator
                                                                                                    ID="rfvPass_IssDate" runat="server" ControlToValidate="txtPassIss_date" Display="Dynamic"
                                                                                                    ErrorMessage="Passport Issue Date cannot be left empty" ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                            <br />
                                                                                            <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 20%; height: 28px">
                                                                                            Passport Expiry Date
                                                                                        </td>
                                                                                        <td gray style="width: 15px; height: 28px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px; height: 28px">
                                                                                            <asp:TextBox ID="txtPassExp_Date" runat="server" Width="110px"></asp:TextBox>&nbsp;<asp:ImageButton
                                                                                                ID="imgBtnPassExp_date" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator
                                                                                                    ID="rfvPass_ExpDate" runat="server" ControlToValidate="txtPassExp_Date" Display="Dynamic"
                                                                                                    ErrorMessage="Passport Expiry Date cannot be left empty" ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                            <div style="color: #1b80b6; font-size: 7pt">
                                                                                                (dd/mmm/yyyy)</div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 14px; background-color: #75c6f2">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Visa / Emirates ID Details </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 24%;">
                                                                                            Visa No.
                                                                                        </td>
                                                                                        <td gray style="width: 83px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:TextBox ID="txtVisaNo" runat="server" Width="110px"></asp:TextBox>
                                                                                        </td>
                                                                                        
                                                                                         <td gray style="width: 24%;">
                                                                                            Emirates Id No.
                                                                                        </td>
                                                                                        <td gray style="width: 83px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:TextBox ID="txtEMIRATES_ID" runat="server" Width="150px"></asp:TextBox>
                                                                                        </td>
                                                                                       
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 24%; height: 22px">
                                                                                            Visa Issue Date
                                                                                        </td>
                                                                                        <td gray style="height: 22px; width: 83px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%; height: 22px">
                                                                                            <asp:TextBox ID="txtVisaIss_date" runat="server" Width="110px"></asp:TextBox>&nbsp;<asp:ImageButton
                                                                                                ID="imgBtnVisaIss_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                                            <br />
                                                                                            <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 20%; height: 22px">
                                                                                            Visa Expiry Date
                                                                                        </td>
                                                                                        <td gray style="width: 15px; height: 22px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px; height: 22px">
                                                                                            <asp:TextBox ID="txtVisaExp_date" runat="server" Width="110px"></asp:TextBox>&nbsp;<asp:ImageButton
                                                                                                ID="imgBtnVisaExp_date" runat="server" ImageUrl="~/Images/calendar.gif" /><div style="font-size: 7pt">
                                                                                                    (dd/mmm/yyyy)</div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                    
                                                                                        <td gray style="width: 20%;">
                                                                                            Issuing Place
                                                                                        </td>
                                                                                        <td gray style="width: 15px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px;">
                                                                                            <asp:TextBox ID="txtIss_Place" runat="server" Width="110px"></asp:TextBox>
                                                                                        </td>
                                                                                        
                                                                                        <td gray style="width: 24%;">
                                                                                            Issuing Authority
                                                                                        </td>
                                                                                        <td gray style="width: 83px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px; height: 22px">
                                                                                            <asp:TextBox ID="txtIss_Auth" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        
                                                                                        
                                                                                    </tr>
                                                                                    <tr style="display: none">
                                                                                        <td gray colspan="6" style="height: 8px">
                                                                                            <span style="color: #ff0000">Transport Required </span>
                                                                                            <asp:CheckBox ID="chkTran_Req" runat="server" Text="Yes" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="16" style="display: none">
                                                                                        <td gray style="width: 24%; height: 15px">
                                                                                            Main Location<span style="color: #ff0000">*</span>
                                                                                        </td>
                                                                                        <td gray style="height: 15px; width: 83px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%; height: 15px">
                                                                                            <asp:DropDownList ID="ddlMainLocation" runat="server" AutoPostBack="True">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td gray style="width: 20%; height: 15px">
                                                                                            Sub-Location<span style="color: #ff0000">*</span>
                                                                                        </td>
                                                                                        <td gray style="width: 15px; height: 15px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px; height: 15px">
                                                                                            <asp:DropDownList ID="ddlSubLocation" runat="server" AutoPostBack="True">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="17" style="display: none">
                                                                                        <td gray style="width: 24%;">
                                                                                            Pick Up Point<span style="color: #ff0000">*</span>
                                                                                        </td>
                                                                                        <td gray style="width: 83px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:DropDownList ID="ddlPickup" runat="server">
                                                                                            </asp:DropDownList>
                                                                                            <br />
                                                                                            <br />
                                                                                            (If your pick up point is unavailable in the list please select the nearest pick
                                                                                            up point and provide details in the box below)<br />
                                                                                            <asp:TextBox ID="txtOthers" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="6">
                                                                                            <span style="color: #ff0000">Does the Student have one or more Siblings studying in
                                                                                                a GEMS school? </span>
                                                                                            <asp:CheckBox ID="chkSibling" runat="server" Text="Yes" />&nbsp;<br />
                                                                                            <span style="color: #ff0000">Please give details of any one</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="18" style="display: none">
                                                                                        <td gray style="width: 24%;">
                                                                                            Sibling Name
                                                                                        </td>
                                                                                        <td gray style="width: 83px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtSib_Name" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 22%;">
                                                                                            Sibling Fee ID<span style="color: #ff0000">*</span>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px">
                                                                                            <asp:TextBox ID="txtSib_ID" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="19" style="display: none">
                                                                                        <td gray style="width: 24%">
                                                                                            GEMS School
                                                                                        </td>
                                                                                        <td gray style="font-weight: bold; width: 83px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="font-weight: bold">
                                                                                            <asp:DropDownList ID="ddlSib_BSU" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 24%">
                                                                                            First Language(Main)
                                                                                        </td>
                                                                                        <td gray style="font-weight: bold; width: 83px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="font-weight: bold">
                                                                                            <asp:DropDownList ID="ddlFLang" runat="server">
                                                                                            </asp:DropDownList><asp:RequiredFieldValidator ID="reqFLang" runat="server" ControlToValidate="ddlFLang"
                                                                                                Display="Dynamic" ErrorMessage="Please choose the first language." InitialValue=" "
                                                                                                ValidationGroup="App_Info">*</asp:RequiredFieldValidator>
                                                                                           
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 24%">
                                                                                            Other Languages(Specify)
                                                                                        </td>
                                                                                        <td gray style="font-weight: bold; width: 83px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="font-weight: bold">
                                                                                            <asp:Panel ID="plOLang" runat="server" Height="100px" ScrollBars="Vertical" Width="220px">
                                                                                                <asp:CheckBoxList ID="chkOLang" runat="server">
                                                                                                </asp:CheckBoxList>
                                                                                            </asp:Panel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray rowspan="3" style="width: 24%">
                                                                                            Proficiency in English
                                                                                        </td>
                                                                                        <td gray style="font-weight: bold; width: 83px" rowspan="3">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="font-weight: bold">
                                                                                            Reading&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                                                            <asp:RadioButton ID="rbRExc" runat="server" GroupName="Read" Text="Excellent" />
                                                                                            <asp:RadioButton ID="rbRGood" runat="server" GroupName="Read" Text="Good" />
                                                                                            <asp:RadioButton ID="rbRFair" runat="server" GroupName="Read" Text="Fair" />
                                                                                            <asp:RadioButton ID="rbRPoor" runat="server" GroupName="Read" Text="Poor" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="4" style="font-weight: bold">
                                                                                            Writing&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                                                            <asp:RadioButton ID="rbWExc" runat="server" GroupName="Write" Text="Excellent" />
                                                                                            <asp:RadioButton ID="rbWGood" runat="server" GroupName="Write" Text="Good" />
                                                                                            <asp:RadioButton ID="rbWFair" runat="server" GroupName="Write" Text="Fair" />
                                                                                            <asp:RadioButton ID="rbWPoor" runat="server" GroupName="Write" Text="Poor" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="4" style="font-weight: bold">
                                                                                            Speaking&nbsp; &nbsp;:
                                                                                            <asp:RadioButton ID="rbSExc" runat="server" GroupName="Speak" Text="Excellent" />
                                                                                            <asp:RadioButton ID="rbSGood" runat="server" GroupName="Speak" Text="Good" />
                                                                                            <asp:RadioButton ID="rbSFair" runat="server" GroupName="Speak" Text="Fair" />
                                                                                            <asp:RadioButton ID="rbSPoor" runat="server" GroupName="Speak" Text="Poor" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 485px;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" style="width: 539px; height: 29px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" style="height: 21px">
                                                                                &nbsp;<asp:Button ID="btnVwApp_Pre" runat="server" CssClass="button" Text="Previous"
                                                                                    CausesValidation="False" Height="25px" Width="80px" />
                                                                                <asp:Button ID="btnVwApp_Next" runat="server" CssClass="button" Text="Next" ValidationGroup="App_Info"
                                                                                    Height="25px" Width="80px" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="vwPrimary" runat="server">
                                                                    <table style="border-collapse: collapse" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table align="left" border="1" bordercolor="#0066cc" cellpadding="5" cellspacing="0"
                                                                                    style="border-collapse: collapse" width="100%">
                                                                                    <tr>
                                                                                        <td colspan="6" style="height: 18px" valign="middle">
                                                                                            <font color="maroon">Primary Contact :
                                                                                                <asp:RadioButton ID="rdPri_Father" runat="server" GroupName="Pri_contact" Text="Father"
                                                                                                    ValidationGroup="Pri_contact" />
                                                                                                <asp:RadioButton ID="rbPri_Mother" runat="server" GroupName="Pri_contact" Text="Mother"
                                                                                                    ValidationGroup="Pri_contact" />
                                                                                                <asp:RadioButton ID="rdPri_Guard" runat="server" GroupName="Pri_contact" Text="Guardian"
                                                                                                    ValidationGroup="Pri_contact" />
                                                                                            </font>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="6" style="height: 18px" valign="middle">
                                                                                            <font color="maroon">Preferred Contact :</font>
                                                                                            <asp:DropDownList ID="ddlFPref_contact" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                                                                ForeColor="Maroon">
                                                                                                <asp:ListItem>Email</asp:ListItem>
                                                                                                <asp:ListItem>Home Phone</asp:ListItem>
                                                                                                <asp:ListItem>Office Phone</asp:ListItem>
                                                                                                <asp:ListItem>Mobile</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="6" style="height: 18px" valign="middle">
                                                                                            <font color="maroon">Fee Sponsor/Who pays the fees :</font>
                                                                                            <asp:DropDownList ID="ddlFee_sponsor" runat="server"
                                                                                              Font-Bold="True" Font-Names="Verdana"
                                                                                                ForeColor="Maroon">
                           <asp:ListItem Value="1">Father</asp:ListItem>
                                <asp:ListItem Value="2">Mother</asp:ListItem>
                                <asp:ListItem Value="3">Guardian</asp:ListItem>
                                <asp:ListItem Value="4">Father's Company</asp:ListItem>
                                <asp:ListItem Value="5">Mother's Company</asp:ListItem>
                            </asp:DropDownList>
                            
                              <asp:RequiredFieldValidator ID="rfvPri_FeeSp_F" runat="server" ControlToValidate="ddlFee_sponsor"
                                Display="Dynamic" ErrorMessage="Fee sponsor detail required" InitialValue=" "
                                ValidationGroup="Prim_cont"  EnableClientScript="false">*</asp:RequiredFieldValidator>
                                                                                                
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 18px; background-color: #1b80b6;"
                                                                                            valign="middle">
                                                                                            <div style="font-size: 10pt; color: #ffffff; font-family: Arial; text-align: left">
                                                                                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><strong>Father&#39;s
                                                                                                    Contact Details </strong>
                                                                                                    <asp:CheckBox ID="chkFHide" runat="server" EnableViewState="true" Text="Hide" />
                                                                                                </font>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="111" style="display: table-row;">
                                                                                        <td gray style="width: 24%; height: 36px">
                                                                                            Name As In Passport
                                                                                        </td>
                                                                                        <td gray style="width: 17px; color: #1b80b6; height: 36px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="width: 560px; height: 36px">
                                                                                            <asp:DropDownList ID="ddlFCont_Status" runat="server">
                                                                                                <asp:ListItem>Mr</asp:ListItem>
                                                                                                <asp:ListItem>Dr</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                            <asp:TextBox ID="txtFPri_Fname" runat="server" MaxLength="70" Width="130px"></asp:TextBox>
                                                                                            <span id="100" style="color: red; display: none;">* </span>
                                                                                            <asp:TextBox ID="txtFPri_Mname" runat="server" MaxLength="70" Width="130px"></asp:TextBox>
                                                                                            &nbsp;
                                                                                            <asp:TextBox ID="txtFPri_Lname" runat="server" MaxLength="70" Width="130px"></asp:TextBox>
                                                                                            <br />
                                                                                            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; First Name &nbsp; &nbsp;
                                                                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                                                            Middle Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                            &nbsp; &nbsp; &nbsp;&nbsp; Last&nbsp; Name
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="112" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            <span>Nationality</span>
                                                                                        </td>
                                                                                        <td gray style="width: 17px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:DropDownList ID="ddlFPri_National1" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Nationality2
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            <span>:</span>
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlFPri_National2" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="113" style="display: table-row;">
                                                                                        <td gray colspan="6" style="height: 14px; background-color: #75c6f2">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Permanent Address </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="114" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            Address Line 1
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtFAdd1_overSea" runat="server" MaxLength="100"></asp:TextBox><span id="Fadd1" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Address Line 2
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtFAdd2_overSea" runat="server" MaxLength="100"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="115" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            Address City/State
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtFOverSeas_Add_City" runat="server" MaxLength="70"></asp:TextBox><span id="FaddCity" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Address Country
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlFOverSeas_Add_Country" runat="server">
                                                                                            </asp:DropDownList><span id="FaddCty" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="116" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            Phone
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtFPhone_Oversea_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtFPhone_Oversea_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtFPhone_Oversea_No" runat="server" MaxLength="10" Width="40%"></asp:TextBox><span id="FPhNo" runat="server" style="color:Red;" visible="false" >*</span><br />
                                                                                            <span style="font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            P.O Box/Zip Code
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtFPoBox_Pri" runat="server" MaxLength="20" Width="110px"></asp:TextBox><span id="Fpobox" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="117" style="display: table-row;">
                                                                                        <td gray colspan="6" style="height: 14px; background-color: #75c6f2; vertical-align: middle;
                                                                                            text-align: left;">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Current Address </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="118" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            Apartment No
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            <span>:</span>
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:TextBox ID="txtFApartNo" runat="server" MaxLength="50"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Building
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtFBldg" runat="server" MaxLength="50"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="119" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            Street
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:TextBox ID="txtFStreet" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Area
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtFArea" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="120" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            City/State
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtFCity_pri" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Country
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlFCountry_Pri" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="121" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            P.O Box
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtFPoboxLocal" runat="server" MaxLength="20" Width="110px"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            City/Emirate
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlFEmirate" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="122" style="display: table-row;">
                                                                                        <td gray>
                                                                                            Home Phone
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtFHPhone_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtFHPhone_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtFHPhone" runat="server" MaxLength="10" Width="40%"></asp:TextBox><span style="display: block;
                                                                                                    font-size: 7pt">(Country-Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            Mobile
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtFM_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtFM_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtFMobile_Pri" runat="server" MaxLength="10" Width="40%"></asp:TextBox>
                                                                                            <span style="display: block; font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="123" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            Office Phone
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtFOPhone_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtFOPhone_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtFOPhone" runat="server" MaxLength="10" Width="38%"></asp:TextBox><br />
                                                                                            <span style="font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            &nbsp;Fax No
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtFFaxNo_country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtFFaxNo_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtFFaxNo" runat="server" MaxLength="10" Width="40%"></asp:TextBox><br />
                                                                                            <span style="font-size: 7pt">(Country-Area-Number)</span><font color="red"><span
                                                                                                style="color: #1b80b6"></span></font>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="124" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            Email
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:TextBox ID="txtFEmail_Pri" runat="server" MaxLength="100"></asp:TextBox>
                                                                                            <span id="101" style="color: red; display: none;">* </span>
                                                                                            <asp:RegularExpressionValidator ID="revPri_Email" runat="server" ControlToValidate="txtFEmail_Pri"
                                                                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Father's e-mail id entered is not valid"
                                                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                                                                            <br />
                                                                                            (Online enquiry acknowledgement will be mailed to primary contact e-mail address)
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="125" style="display: table-row;">
                                                                                        <td gray style="width: 24%">
                                                                                            Occupation
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:TextBox ID="txtFOccup" runat="server" MaxLength="50"></asp:TextBox><span id="Focc" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="126" style="display: table-row;">
                                                                                        <td gray style="width: 24%; height: 70px;">
                                                                                            Company
                                                                                        </td>
                                                                                        <td gray style="width: 17px; height: 70px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="height: 70px">
                                                                                            <asp:DropDownList ID="ddlFCompany" runat="server">
                                                                                            </asp:DropDownList><span id="Fcomp" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                            <br />
                                                                                            (If you choose other,please specify the Company Name)<br />
                                                                                            <asp:TextBox ID="txtFComp" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="127" style="display: table-row;">
                                                                                        <td gray colspan="6" style="height: 15px;">
                                                                                            <span style="color: #ff0000">Is father of Student an employee with GEMS ?</span><asp:CheckBox
                                                                                                ID="chkFStaff_GEMS" runat="server" Text="Yes" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="128" style="display: none">
                                                                                        <td gray style="width: 22%;">
                                                                                            Staff Name<span style="color: #ff0000">*</span>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtFStaff_Name" runat="server"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td gray style="width: 117px;">
                                                                                            Staff ID<span style="color: #ff0000">*</span>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px">
                                                                                            <asp:TextBox ID="txtFStaffID" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="129" style="display: none">
                                                                                        <td gray style="width: 22%;">
                                                                                            Business unit
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:DropDownList ID="ddlFStaff_BSU" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="130" style="display: table-row;">
                                                                                        <td gray colspan="6">
                                                                                            <span style="color: #ff0000">Is father of Student an Ex-Student of a&nbsp; GEMS School?</span>
                                                                                            <asp:CheckBox ID="chkFExStud_Gems" runat="server" Text="Yes" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="131" style="display: none">
                                                                                        <td gray style="width: 22%;">
                                                                                            Name
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtFExStudName_Gems" runat="server"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td gray style="width: 117px;">
                                                                                            Academic Year
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px">
                                                                                            <asp:DropDownList ID="ddlFExYear" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="132" style="display: none">
                                                                                        <td gray style="width: 22%;">
                                                                                            GEMS School
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:DropDownList ID="ddlFExStud_Gems" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 18px; background-color: #1b80b6;"
                                                                                            valign="middle">
                                                                                            <span style="font-size: 10pt; color: #ffffff; font-family: Arial; text-align: left">
                                                                                                <span style="float: left; display: inline; left: 0px; position: relative; top: 3px;
                                                                                                    height: 3px; z-index: 100;">Mother&#39;s Contact Details
                                                                                                    <asp:CheckBox ID="chkMHide" runat="server" EnableViewState="true" Text="Hide" />
                                                                                                </span><span style="float: right; top: 3px; z-index: 100;">
                                                                                                    <asp:CheckBox ID="chkCopyF_Details" runat="server" onclick="javascript:return copyFathertoMother(this);"
                                                                                                        Style="clear: left" Text="Copy Father Details" />
                                                                                                </span></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="135" style="display: table-row">
                                                                                        <td gray style="height: 36px; width: 19%;">
                                                                                            Name As In Passport
                                                                                        </td>
                                                                                        <td gray style="width: 17px; color: #1b80b6; height: 36px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="width: 550px; height: 36px">
                                                                                            <asp:DropDownList ID="ddlMCont_Status" runat="server">
                                                                                                <asp:ListItem Selected="True">Mrs</asp:ListItem>
                                                                                                <asp:ListItem>Ms</asp:ListItem>
                                                                                                <asp:ListItem>Dr</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                            <asp:TextBox ID="txtMPri_Fname" runat="server" MaxLength="70" Width="130px"></asp:TextBox>
                                                                                            <span id="102" style="color: red; display: none;">* </span>
                                                                                            <asp:TextBox ID="txtMPri_Mname" runat="server" MaxLength="70" Width="130px"></asp:TextBox>
                                                                                            <asp:TextBox ID="txtMPri_Lname" runat="server" MaxLength="70" Width="130px"></asp:TextBox>
                                                                                            <br />
                                                                                            &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; First Name &nbsp;
                                                                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                                                            Middle Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                            &nbsp; &nbsp; &nbsp;&nbsp; Last&nbsp;Name
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="136" style="display: table-row;">
                                                                                        <td gray style="width: 19%;">
                                                                                            <span>Nationality</span>
                                                                                        </td>
                                                                                        <td gray style="width: 17px; color: #1b80b6;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:DropDownList ID="ddlMPri_National1" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Nationality2
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            <span>:</span>
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlMPri_National2" runat="server" AppendDataBoundItems="True">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="137" style="display: table-row;">
                                                                                        <td gray colspan="6" style="height: 14px; background-color: #75c6f2;">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Permanent Address </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="138" style="display: none">
                                                                                        <td gray style="width: 19%;">
                                                                                            Address Line 1
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtMAdd1_overSea" runat="server" MaxLength="100"></asp:TextBox><span id="Madd1" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Address Line 2
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtMAdd2_overSea" runat="server" MaxLength="100"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="139" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            &nbsp;Address City/State
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtMOverSeas_Add_City" runat="server" MaxLength="70"></asp:TextBox><span id="MaddCity" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Address Country
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlMOverSeas_Add_Country" runat="server">
                                                                                            </asp:DropDownList><span id="MaddCty" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="140" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Phone
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtMPhone_Oversea_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtMPhone_Oversea_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtMPhone_Oversea_No" runat="server" MaxLength="10" Width="40%"></asp:TextBox><span id="MPhNo" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                            <br />
                                                                                            <span style="font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            P.O Box/Zip Code
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtMPoBox_Pri" runat="server" MaxLength="20" Width="110px"></asp:TextBox><span id="Mpobox" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="141" style="display: table-row;">
                                                                                        <td gray colspan="6" style="height: 14px; background-color: #75c6f2; vertical-align: middle;
                                                                                            text-align: left;">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Current Address </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="142" style="display: table-row;">
                                                                                        <td gray style="width: 19%;">
                                                                                            Apartment No
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            <span>:</span>
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:TextBox ID="txtMApartNo" runat="server" MaxLength="50"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Building
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtMBldg" runat="server" MaxLength="50"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="143" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Street
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:TextBox ID="txtMStreet" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Area
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtMArea" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="144" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            City/State
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:TextBox ID="txtMCity_pri" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Country
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlMCountry_Pri" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="145" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            P.O Box
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtMPoboxLocal" runat="server" MaxLength="20" Width="110px"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            City/Emirate
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlMEmirate" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="146" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Home Phone
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtMHPhone_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtMHPhone_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtMHPhone" runat="server" MaxLength="10" Width="40%"></asp:TextBox><span style="display: block;
                                                                                                    font-size: 7pt">(Country-Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Mobile
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtMM_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtMM_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtMMobile_Pri" runat="server" MaxLength="10" Width="40%"></asp:TextBox>
                                                                                            <span style="display: block; font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="147" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Office Phone
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtMOPhone_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtMOPhone_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtMOPhone" runat="server" MaxLength="10" Width="38%"></asp:TextBox><br />
                                                                                            <span style="font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            &nbsp;Fax No
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtMFaxNo_country" runat="server" MaxLength="3" onkeypress="return isNumberKey(event)"
                                                                                                Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtMFaxNo_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtMFaxNo" runat="server" MaxLength="10" Width="40%"></asp:TextBox><br />
                                                                                            <span style="font-size: 7pt">(Country-Area-Number)</span><font color="red"><span
                                                                                                style="color: #1b80b6"></span></font>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="148" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Email
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:TextBox ID="txtMEmail_Pri" runat="server" MaxLength="100"></asp:TextBox>
                                                                                            <span id="103" style="color: red; display: none;">* </span>
                                                                                            <asp:RegularExpressionValidator ID="revMPri_Email" runat="server" ControlToValidate="txtMEmail_Pri"
                                                                                                Display="Dynamic" EnableViewState="False" ErrorMessage="E-Mail entered is not valid"
                                                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                                                                            <br />
                                                                                            (Online enquiry acknowledgement will be mailed to primary contact e-mail address)
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="149" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Occupation
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:TextBox ID="txtMOccup" runat="server" MaxLength="50"></asp:TextBox><span id="Mocc" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="150" style="display: table-row;">
                                                                                        <td gray style="width: 19%; height: 70px;">
                                                                                            Company
                                                                                        </td>
                                                                                        <td gray style="width: 17px; height: 70px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="height: 70px">
                                                                                            <asp:DropDownList ID="ddlMCompany" runat="server">
                                                                                            </asp:DropDownList><span id="Mcomp" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                            <br />
                                                                                            (If you choose other,please specify the Company Name)<br />
                                                                                            <asp:TextBox ID="txtMComp" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="151" style="display: table-row;">
                                                                                        <td gray colspan="6" style="height: 15px;">
                                                                                            <span style="color: #ff0000">Is mother of Student an employee with GEMS ?</span><asp:CheckBox
                                                                                                ID="chkMStaff_GEMS" runat="server" Text="Yes" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="152" style="display: none">
                                                                                        <td gray style="width: 22%;">
                                                                                            Staff Name<span style="color: #ff0000">*</span>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtMStaff_Name" runat="server"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td gray style="width: 117px;">
                                                                                            Staff ID<span style="color: #ff0000">*</span>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px">
                                                                                            <asp:TextBox ID="txtMStaffID" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="153" style="display: none">
                                                                                        <td gray style="width: 22%;">
                                                                                            Business unit
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:DropDownList ID="ddlMStaff_BSU" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="154" style="display: table-row;">
                                                                                        <td gray colspan="6">
                                                                                            <span style="color: #ff0000">Is mother of Student an Ex-Student of a&nbsp; GEMS School?</span>
                                                                                            <asp:CheckBox ID="chkMExStud_Gems" runat="server" Text="Yes" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="155" style="display: none">
                                                                                        <td gray style="width: 22%;">
                                                                                            Name
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtMExStudName_Gems" runat="server"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td gray style="width: 117px;">
                                                                                            Academic Year
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 166px">
                                                                                            <asp:DropDownList ID="ddlMExYear" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="156" style="display: none">
                                                                                        <td gray style="width: 22%;">
                                                                                            GEMS School
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:DropDownList ID="ddlMExStud_Gems" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 18px; background-color: #1b80b6;"
                                                                                            valign="middle">
                                                                                            <span style="font-size: 10pt; color: #ffffff; font-family: Arial; text-align: left">
                                                                                                <span style="float: left; display: inline; left: 0px; position: relative; top: 3px;
                                                                                                    height: 3px; z-index: 101;">Guardian&#39;s Contact Details
                                                                                                    <asp:CheckBox ID="chkGHide" runat="server" EnableViewState="true" Text="Hide" />
                                                                                                </span><span style="float: right; top: 3px; z-index: 100;">
                                                                                                    <asp:CheckBox ID="chkCopyF_Details_Guard" runat="server" onclick="javascript:return copyFathertoGuardian(this);"
                                                                                                        Text="Copy Father Details" />
                                                                                                    <asp:CheckBox ID="chkCopyM_Details_Guard" runat="server" onclick="javascript:return copyMothertoGuardian(this);"
                                                                                                        Text="Copy Mother Details" />
                                                                                                </span></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="160" style="display: table-row;">
                                                                                        <td gray style="height: 36px; width: 19%;">
                                                                                            Name As In Passport
                                                                                        </td>
                                                                                        <td gray style="width: 17px; color: #1b80b6; height: 36px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="width: 550px; height: 36px">
                                                                                            <asp:DropDownList ID="ddlGCont_Status" runat="server">
                                                                                                <asp:ListItem>Mr</asp:ListItem>
                                                                                                <asp:ListItem>Mrs</asp:ListItem>
                                                                                                <asp:ListItem>Ms</asp:ListItem>
                                                                                                <asp:ListItem>Dr</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                            <asp:TextBox ID="txtGPri_Fname" runat="server" MaxLength="70" Width="130px"></asp:TextBox>
                                                                                            <span id="105" style="color: red; display: none;">* </span>
                                                                                            <asp:TextBox ID="txtGPri_Mname" runat="server" MaxLength="70" Width="130px"></asp:TextBox>
                                                                                            <asp:TextBox ID="txtGPri_Lname" runat="server" MaxLength="70" Width="130px"></asp:TextBox>
                                                                                            <br />
                                                                                            &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; First Name &nbsp;
                                                                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                                                            Middle Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                                            &nbsp; &nbsp; &nbsp;&nbsp; Last&nbsp;Name
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="161" style="display: table-row;">
                                                                                        <td gray style="width: 19%;">
                                                                                            <span>Nationality</span>
                                                                                        </td>
                                                                                        <td gray style="width: 17px; color: #1b80b6;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:DropDownList ID="ddlGPri_National1" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Nationality2
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            <span>:</span>
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlGPri_National2" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="162" style="display: table-row;">
                                                                                        <td gray colspan="6" style="height: 14px; background-color: #75c6f2;">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Permanent Address </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="163" style="display: table-row;">
                                                                                        <td gray style="width: 19%;">
                                                                                            Address Line 1
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtGAdd1_overSea" runat="server" MaxLength="100"></asp:TextBox><span id="Gadd1" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Address Line 2
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtGAdd2_overSea" runat="server" MaxLength="100"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="164" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            &nbsp;Address City/State
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtGOverSeas_Add_City" runat="server" MaxLength="70"></asp:TextBox><span id="GaddCity" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Address Country
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlGOverSeas_Add_Country" runat="server">
                                                                                            </asp:DropDownList><span id="GaddCty" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="165" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Phone
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtGPhone_Oversea_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtGPhone_Oversea_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtGPhone_Oversea_No" runat="server" MaxLength="10" Width="40%"></asp:TextBox><span id="GPhNo" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                            <br />
                                                                                            <span style="font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            P.O Box/Zip Code
                                                                                        </td>
                                                                                        <td gray style="width: 14px; color: #1b80b6">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtGPoBox_Pri" runat="server" MaxLength="20" Width="110px"></asp:TextBox><span id="Gpobox" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="166" style="display: table-row;">
                                                                                        <td gray colspan="6" style="height: 14px; background-color: #75c6f2; vertical-align: middle;
                                                                                            text-align: left;">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Current Address </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="167" style="display: table-row;">
                                                                                        <td gray style="width: 19%;">
                                                                                            Apartment No
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            <span>:</span>
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:TextBox ID="txtGApartNo" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Building
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtGBldg" runat="server" MaxLength="50"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="168" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Street
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <asp:TextBox ID="txtGStreet" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Area
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtGArea" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="169" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            City/State
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%;">
                                                                                            <span style="color: #ff0000">
                                                                                                <asp:TextBox ID="txtGCity_pri" runat="server" MaxLength="50"></asp:TextBox>
                                                                                                <span></span></span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Country
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlGCountry_Pri" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="170" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            P.O Box
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtGPoboxLocal" runat="server" MaxLength="20" Width="110px"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            City/Emirate
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:DropDownList ID="ddlGEmirate" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="171" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Home Phone
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtGHPhone_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtGHPhone_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtGHPhone" runat="server" MaxLength="10" Width="40%"></asp:TextBox><span style="display: block;
                                                                                                    font-size: 7pt">(Country-Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            Mobile
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtGM_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtGM_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                                                            -
                                                                                            <asp:TextBox ID="txtGMobile_Pri" runat="server" MaxLength="10" Width="40%"></asp:TextBox>
                                                                                            <span style="display: block; font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="172" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Office Phone
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 34%">
                                                                                            <asp:TextBox ID="txtGOPhone_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtGOPhone_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtGOPhone" runat="server" MaxLength="10" Width="38%"></asp:TextBox><br />
                                                                                            <span style="font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray style="width: 117px">
                                                                                            &nbsp;Fax No
                                                                                        </td>
                                                                                        <td gray style="width: 14px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 30%">
                                                                                            <asp:TextBox ID="txtGFaxNo_country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                            -<asp:TextBox ID="txtGFaxNo_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtGFaxNo" runat="server" MaxLength="10" Width="40%"></asp:TextBox><br />
                                                                                            <span style="font-size: 7pt">(Country-Area-Number)</span><font color="red"><span
                                                                                                style="color: #1b80b6"></span></font>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="173" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Email
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:TextBox ID="txtGEmail_Pri" runat="server" MaxLength="100"></asp:TextBox>
                                                                                            <span id="106" style="color: red; display: none;">* </span>
                                                                                            <asp:RegularExpressionValidator ID="revGPri_Email" runat="server" ControlToValidate="txtGEmail_Pri"
                                                                                                Display="Dynamic" EnableViewState="False" ErrorMessage="E-Mail entered is not valid"
                                                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                                                                            <br />
                                                                                            (Online enquiry acknowledgement will be mailed to primary contact e-mail address)
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="174" style="display: table-row;">
                                                                                        <td gray style="width: 19%">
                                                                                            Occupation
                                                                                        </td>
                                                                                        <td gray style="width: 17px">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:TextBox ID="txtGOccup" runat="server" MaxLength="50"></asp:TextBox><span id="Gocc" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="175" style="display: table-row;">
                                                                                        <td gray style="width: 19%; height: 70px;">
                                                                                            Company
                                                                                        </td>
                                                                                        <td gray style="width: 17px; height: 70px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4" style="height: 70px">
                                                                                            <asp:DropDownList ID="ddlGCompany" runat="server">
                                                                                            </asp:DropDownList><span id="Gcomp" runat="server" style="color:Red;" visible="false" >*</span>
                                                                                            <br />
                                                                                            (If you choose other,please specify the Company Name)<br />
                                                                                            <asp:TextBox ID="txtGComp" runat="server" MaxLength="50"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 18px; background-color: #1b80b6;"
                                                                                            valign="middle">
                                                                                            <span style="font-size: 10pt; color: #ffffff; font-family: Arial; text-align: left">
                                                                                                Other Details </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    
                                                                                    <tr>
                                                                                        <td gray colspan="6">
                                                                                            How did you hear about
                                                                                            <asp:Literal ID="ltAboutUs" runat="server"></asp:Literal>
                                                                                            <br />
                                                                                            <asp:CheckBoxList ID="chkAboutUs" runat="server" CellPadding="2" CellSpacing="1"
                                                                                                RepeatColumns="3" RepeatDirection="Horizontal">
                                                                                            </asp:CheckBoxList> <br />
                                                                                            Please give details
                                                                                             <asp:TextBox ID="txtMode_NOTE" runat="server" Height="58px" Rows="2" 
                                                                                                         SkinID="MultiText" TabIndex="22" TextMode="MultiLine" Width="590px" 
                                                                                                     MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    
                                                                                 
                                                                                    
                                                                                    
                                                                                    <tr>
                                                                                        <td gray colspan="6">
                                                                                            <div style="padding-top:4px;padding-bottom:4px;">
                                                                                                Are there any family circumstances of which you feel we should be aware of ? Check as appropriate</div>
                                                                                               <div style="padding-top:4px;">
                                                                                                <asp:CheckBox ID="chkFam_Par_sep" runat="server" Text="Parents separated" />
                                                                                                <asp:CheckBox ID="chkFam_Par_div" runat="server" Text="Parents divorced" />
                                                                                                <asp:CheckBox ID="chkFam_Fath_Des" runat="server" Text="Father deceased" />
                                                                                                 <asp:CheckBox ID="chkFam_Moth_Des" runat="server" Text="Mother deceased" />
                                                                                                 <asp:CheckBox ID="chkFam_Oth" runat="server" Text="Others" />
                                                                                                 </div>
                                                                                            <div style="padding-top:4px;"> &nbsp;if choosen others please give detail :<div style="padding-top:4px;">
                                                                                              <asp:TextBox ID="txtFamily_NOTE" runat="server" Height="58px" Rows="2" 
                                                                                                         SkinID="MultiText" TabIndex="22" TextMode="MultiLine" Width="590px" 
                                                                                                     MaxLength="255"></asp:TextBox></div></div>
                                                                                            <br />
                                                                                             <div style="padding-top:4px;">If separated or divorced, who has legal custody ? (proof of legal 
                                                                                                 custody/guardianship must be submitted to the school <u>prior</u> 
                                                                                                 to the first day of attendance)
                                                                                             <div style="padding-top:4px;"><asp:TextBox ID="txtStu_Leg" runat="server" MaxLength="200" Width="200px"></asp:TextBox></div></div>
                                                                                           
                                                                                                 <div style="padding-top:7px;">Who will the student be living with ?<div style="padding-top:4px;padding-bottom:4px;">
                                                                                                <asp:TextBox ID="txtStu_Living" runat="server" MaxLength="150" Width="200px"></asp:TextBox> </div></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 600px;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 600px; height: 33px;" align="right">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" style="height: 21px">
                                                                                &nbsp;<asp:Button ID="btnVwPri_Pre" runat="server" CausesValidation="False" CssClass="button"
                                                                                    Height="25px" Text="Previous" Width="80px" />
                                                                                <asp:Button ID="btnVwPri_Next" runat="server" CssClass="button" Height="25px" OnClientClick="return CHK_CONTACT_DETAIL()"
                                                                                    Text="Next" ValidationGroup="Prim_cont" Width="80px" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="vwPrevious" runat="server">
                                                                    <table style="border-collapse: collapse" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table align="left" border="1" bordercolor="#0066cc" cellpadding="5" cellspacing="0"
                                                                                    style="border-collapse: collapse" width="600px">
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 18px; background-color: #1b80b6" valign="middle">
                                                                                            <span style="font-size: 10pt; color: #ffffff" id="currPre_SchTitle" runat="server">
                                                                                            </span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch1" runat="server">
                                                                                        <td gray>
                                                                                            School Name
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:DropDownList ID="ddlPreSchool_Nursery" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            Registration No.
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtRegNo" runat="server" Width="90px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch11" runat="server">
                                                                                        <td gray>
                                                                                            School Detail Type
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:Literal ID="ltSchoolType" runat="server"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch2" runat="server">
                                                                                        <td gray>
                                                                                            School Name
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray colspan="4">
                                                                                            <asp:DropDownList ID="ddlGemsSchools" runat="server">
                                                                                            </asp:DropDownList>
                                                                                            <br />
                                                                                            (If you choose other,please specify the School Name)<br />
                                                                                            <asp:TextBox ID="txtSch_other" runat="server" MaxLength="250" Width="300"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch3" runat="server">
                                                                                        <td gray>
                                                                                            Name of the head teacher
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtSchool_head" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            Student Id
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtFeeID_GEMS" runat="server" Width="90px" MaxLength="20"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch4" runat="server">
                                                                                        <td gray>
                                                                                            Grade
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:DropDownList ID="ddlPre_Grade" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            Curriculum
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:DropDownList ID="ddlPre_Curriculum" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch5" runat="server">
                                                                                        <td gray>
                                                                                            Language of Instruction
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtLang_Instr" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            Address
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtSchAddr" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch6" runat="server">
                                                                                        <td gray>
                                                                                            City
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtSchCity" runat="server"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            Country
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:DropDownList ID="ddlPre_Country" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch7" runat="server">
                                                                                        <td gray>
                                                                                            School Phone
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtSCHPhone_Country" runat="server" Width="14%" MaxLength="3"></asp:TextBox>
                                                                                            -&nbsp;<asp:TextBox ID="txtSCHPhone_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtSCHPhone_No" runat="server" MaxLength="10" Width="40%"></asp:TextBox><br />
                                                                                            <span style="font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            School Fax
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtSCHFax_Country" runat="server" Width="14%" MaxLength="3"></asp:TextBox>
                                                                                            -&nbsp;<asp:TextBox ID="txtSCHFax_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-<asp:TextBox
                                                                                                ID="txtSCHFax_No" runat="server" MaxLength="10" Width="35%"></asp:TextBox><br />
                                                                                            <span style="font-size: 7pt">(Country -Area-Number)</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch8" runat="server">
                                                                                        <td gray>
                                                                                            From Date
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtSchFrom_dt" runat="server" Width="110px"></asp:TextBox>
                                                                                            <ajaxToolkit:CalendarExtender ID="SchFrom_dt_CalendarExtender" runat="server" Format="dd/MMM/yyyy"
                                                                                                PopupButtonID="imgSchFrom_dt" TargetControlID="txtSchFrom_dt">
                                                                                            </ajaxToolkit:CalendarExtender>
                                                                                            &nbsp;<asp:ImageButton ID="imgSchFrom_dt" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                                            <br />
                                                                                            <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                                                                                        </td>
                                                                                        <td gray>
                                                                                            To Date
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td gray>
                                                                                            <asp:TextBox ID="txtSchTo_dt" runat="server" Width="110px"></asp:TextBox>
                                                                                            <ajaxToolkit:CalendarExtender ID="txtSchTo_dt_CalendarExtender" runat="server" Format="dd/MMM/yyyy"
                                                                                                PopupButtonID="imgtxtSchTo_dt" TargetControlID="txtSchTo_dt">
                                                                                            </ajaxToolkit:CalendarExtender>
                                                                                            &nbsp;<asp:ImageButton ID="imgtxtSchTo_dt" runat="server" ImageUrl="~/Images/calendar.gif" />
                                                                                            <br />
                                                                                            <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch9" runat="server">
                                                                                        <td align="center" gray colspan="6">
                                                                                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Height="25px" Text="Add"
                                                                                                Width="80px" />
                                                                                            <asp:Button ID="btnUpdate" runat="server" CssClass="button" Height="25px" Text="Update"
                                                                                                Width="80px" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="trSch10" runat="server">
                                                                                        <td align="center" gray colspan="6">
                                                                                            <asp:GridView ID="gvSchool" runat="server" AutoGenerateColumns="False" BorderColor="#1B80B6"
                                                                                                DataKeyNames="ID" EmptyDataText="No record added yet" EnableModelValidation="True"
                                                                                                Font-Names="Verdana" Font-Size="8pt" ForeColor="#1B80B6" Width="100%">
                                                                                                <RowStyle CssClass="griditem" Height="23px" />
                                                                                                <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                                                                                <Columns>
                                                                                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="School Name">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblSCH_NAME" runat="server" Text='<%# Bind("SCH_NAME") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="185px" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Head Teacher">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblSCH_HEAD" runat="server" Text='<%# Bind("SCH_HEAD") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Grade">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblSCH_GRADE" runat="server" Text='<%# BIND("SCH_GRADE") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="From Date">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblSCH_FROMDT" runat="server" Text='<%# bind("SCH_FROMDT") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="To Date">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblSchName" runat="server" Text='<%# bind("SCH_TODT") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblToDt" runat="server" Text='<%# bind("SCH_TODT") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Edit">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:LinkButton ID="EditBtn" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="Edit">Edit</asp:LinkButton>
                                                                                                        </ItemTemplate>
                                                                                                        <HeaderStyle Width="40px" />
                                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <SelectedRowStyle BackColor="Khaki" />
                                                                                                <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                                                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                                            </asp:GridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" colspan="6" style="height: 29px;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" style="height: 21px" colspan="6">
                                                                                <asp:Button ID="btnVwPrev_Pre" runat="server" CssClass="button" Text="Previous" CausesValidation="False"
                                                                                    Height="25px" Width="80px" />
                                                                                <asp:Button ID="btnVwPrev_Next" runat="server" CssClass="button" Text="Next" ValidationGroup="Prev_Info"
                                                                                    Height="25px" Width="80px" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="vwHealth" runat="server">
                                                                    <table style="border-collapse: collapse" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table align="left" width="100%" style="border-collapse: collapse" border="1" bordercolor="#0066cc"
                                                                                    cellpadding="5" cellspacing="0">
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 18px; background-color: #1b80b6" valign="middle">
                                                                                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-weight: bold;
                                                                                                font-family: Verdana">Other Details</span></font>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 19%">
                                                                                            Health Card No / Medical Insurance No
                                                                                        </td>
                                                                                        <td gray style="width: 17px" align="center">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 550px;">
                                                                                            <asp:TextBox ID="txtHthNo" runat="server" MaxLength="50" Width="110px"></asp:TextBox>
                                                                                        </td>
                                                                                        <td gray style="width: 19%">
                                                                                            Blood Group
                                                                                        </td>
                                                                                        <td gray style="width: 17px" align="center">
                                                                                            :
                                                                                        </td>
                                                                                        <td gray style="width: 550px;">
                                                                                            <asp:DropDownList ID="ddlBgroup" runat="server">
                                                                                                <asp:ListItem>--</asp:ListItem>
                                                                                                <asp:ListItem Value="AB+">AB+</asp:ListItem>
                                                                                                <asp:ListItem Value="AB-">AB-</asp:ListItem>
                                                                                                <asp:ListItem Value="B+">B+</asp:ListItem>
                                                                                                <asp:ListItem Value="A+">A+</asp:ListItem>
                                                                                                <asp:ListItem Value="B-">B-</asp:ListItem>
                                                                                                <asp:ListItem Value="A-">A-</asp:ListItem>
                                                                                                <asp:ListItem Value="O+">O+</asp:ListItem>
                                                                                                <asp:ListItem Value="O-">O-</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 14px; background-color: #75c6f2; vertical-align: middle;
                                                                                            text-align: left;">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Health Restriction</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Allergies
                                                                                        </td>
                                                                                        <td gray align="center">
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthAll_Yes" runat="server" GroupName="Allerg" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthAll_No" runat="server" GroupName="Allerg" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthAll_Note" runat="server" Height="58px" TabIndex="22" TextMode="MultiLine"
                                                                                                Width="229px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Disabled ?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthDisabled_YES" runat="server" GroupName="HthDisabled" 
                                                                                                Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthDisabled_No" runat="server" GroupName="HthDisabled" 
                                                                                                Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthDisabled_Note" runat="server" CssClass="inputbox_multi" 
                                                                                                Height="58px" MaxLength="255" Rows="4" SkinID="MultiText" TabIndex="22" 
                                                                                                TextMode="MultiLine" Width="229px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Special Medication
                                                                                        </td>
                                                                                        <td gray align="center">
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthSM_Yes" runat="server" GroupName="SPMed" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthSM_No" runat="server" GroupName="SPMed" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthSM_Note" runat="server" Height="58px" TabIndex="22" TextMode="MultiLine"
                                                                                                Width="229px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" gray>
                                                                                            Physical Education Restrictions
                                                                                        </td>
                                                                                        <td gray align="center">
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray>
                                                                                            <asp:RadioButton ID="rbHthPER_Yes" runat="server" GroupName="PhyEd" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthPER_No" runat="server" GroupName="PhyEd" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray>
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray>
                                                                                            <asp:TextBox ID="txtHthPER_Note" runat="server" Height="58px" TabIndex="22" TextMode="MultiLine"
                                                                                                Width="229px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" gray>
                                                                                            Any other information related to health issue the school should be aware of?
                                                                                        </td>
                                                                                        <td gray align="center">
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray>
                                                                                            <asp:RadioButton ID="rbHthOther_yes" runat="server" GroupName="HthOther" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthOther_No" runat="server" GroupName="HthOther" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray>
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray>
                                                                                            <asp:TextBox ID="txtHthOth_Note" runat="server" CssClass="inputbox_multi" Height="58px"
                                                                                                Rows="2" SkinID="MultiText" TabIndex="22" TextMode="MultiLine" Width="229px"
                                                                                                MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray colspan="6" style="height: 14px; background-color: #75c6f2; vertical-align: middle;
                                                                                            text-align: left;">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Applicant's disciplinary, social, physical
                                                                                                or psychological detail</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 253px;" align="left">
                                                                                            Has the child received any sort of learning support or therapy?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthLS_Yes" runat="server" GroupName="LearnSp" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthLS_No" runat="server" GroupName="LearnSp" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthLS_Note" runat="server" CssClass="inputbox_multi" Height="58px"
                                                                                                Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine" Width="229px"
                                                                                                MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" style="width: 253px;" gray>
                                                                                            Does the child require any special education needs?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthSE_Yes" runat="server" GroupName="SPneed" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthSE_No" runat="server" GroupName="SPneed" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthSE_Note" runat="server" CssClass="inputbox_multi" Height="58px"
                                                                                                Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine" Width="229px"
                                                                                                MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Does the student require English support as Additional Language program (EAL) ?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthEAL_Yes" runat="server" GroupName="EAL" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthEAL_No" runat="server" GroupName="EAL" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthEAL_Note" runat="server" Height="58px" TabIndex="22" TextMode="MultiLine"
                                                                                                Width="229px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Has your child ever repeated/failed a grade ?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbRepGRD_Yes" runat="server" GroupName="RepGRD" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbRepGRD_No" runat="server" GroupName="RepGRD" Text="No"  Checked="true"/>
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            If Yes, which grade ?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtRep_GRD" runat="server" TabIndex="22" TextMode="SingleLine"
                                                                                                Width="229px"    MaxLength="100"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    
                                                                                    
                                                                                    
                                                                                    <tr>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                           Has your child's behaviour been any cause for concern in previous schools ?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthBehv_Yes" runat="server" GroupName="Behv" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthBehv_No" runat="server" GroupName="Behv" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            If yes, please explain and include the name of the school and principal
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthBehv_Note" runat="server" Height="58px" TabIndex="22" TextMode="MultiLine"
                                                                                                Width="229px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText" MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                          
                                  
                                                                                    <tr> 
                                    <td align="left" gray style="width: 253px; ">
                                        Any information related to communication & interaction that the school should be aware of ?</td>
                                    <td gray>
                                        :</td>
                                    <td align="left" gray style="width: 288px; ">
                                        <asp:RadioButton ID="rbHthComm_Yes" runat="server" GroupName="CommInt" 
                                            Text="Yes" />
                                        <asp:RadioButton ID="rbHthComm_No" runat="server" GroupName="CommInt" 
                                            Text="No" /></td>
                                    <td align="left" gray style="width: 253px;">
                                        Notes</td>
                                    <td gray>
                                        :</td>
                                    <td align="left" gray style="width: 288px;">
                                        <asp:TextBox ID="txtHthCommInt_Note" runat="server" Height="58px" TabIndex="22" TextMode="MultiLine"
                                            Width="229px" Rows="4" CssClass="inputbox_multi" SkinID="MultiText" 
                                            MaxLength="255"></asp:TextBox></td>
                                </tr>
                                                                                    <tr>
                                                                                        <td gray 
                                                                                            style="height: 14px; background-color: #75c6f2; vertical-align: middle; text-align: left;" 
                                                                                            colspan="6">
                                                                                            <span style="font-size: 9pt; color: #ffffff">Gifted and talented</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 253px;" align="left">
                                                                                            Has your child ever been selected for specific enrichment activities?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthEnr_Yes" runat="server" GroupName="Enr" Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthEnr_No" runat="server" GroupName="Enr" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthEnr_note" runat="server" CssClass="inputbox_multi" Height="58px"
                                                                                                Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine" Width="229px"
                                                                                                MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td gray style="width: 253px;" align="left">
                                                                                            Is your child musically proficient?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthMus_Yes" runat="server" GroupName="Music" 
                                                                                                Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthMus_No" runat="server" GroupName="Music" Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthMus_Note" runat="server" CssClass="inputbox_multi" Height="58px"
                                                                                                Rows="4" SkinID="MultiText" TabIndex="22" TextMode="MultiLine" Width="229px"
                                                                                                MaxLength="255"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Has your child represented a school or country in sport?
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:RadioButton ID="rbHthSport_Yes" runat="server" GroupName="Sport" 
                                                                                                Text="Yes" />
                                                                                            <asp:RadioButton ID="rbHthSport_No" runat="server" GroupName="Sport" 
                                                                                                Text="No" />
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 253px;">
                                                                                            Notes
                                                                                        </td>
                                                                                        <td gray>
                                                                                            :
                                                                                        </td>
                                                                                        <td align="left" gray style="width: 288px;">
                                                                                            <asp:TextBox ID="txtHthSport_note" runat="server" CssClass="inputbox_multi" 
                                                                                                Height="58px" MaxLength="255" Rows="4" SkinID="MultiText" TabIndex="22" 
                                                                                                TextMode="MultiLine" Width="229px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" style="height: 29px">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <asp:Button ID="btnVwHealth_Pre" runat="server" CausesValidation="False" CssClass="button"
                                                                                    Height="25px" Text="Previous" Width="80px" />
                                                                                <asp:Button ID="btnVwHealth_Next" runat="server" CssClass="button" Height="25px"
                                                                                    Text="Next" ValidationGroup="Health_Info" Width="80px" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="vwDeclaration" runat="server">
                                                                    <div id="divDecl" runat="server">
                                                                        <table style="border-collapse: collapse" width="100%">
                                                                            <tr>
                                                                                <td style="width: 608px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 608px">
                                                                                    <table align="center" border="1" style="border-collapse: collapse" bordercolor="#1b80b6"
                                                                                        cellpadding="5" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td gray colspan="2" style="height: 18px; background-color: #1b80b6" valign="middle">
                                                                                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-weight: bold;
                                                                                                    font-family: Verdana">Declaration</span></font>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td gray colspan="2">
                                                                                                <table cellpadding="5" cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td colspan="6">
                                                                                                            The application is filled by :<asp:RadioButton ID="rbParentF" runat="server" GroupName="Filled"
                                                                                                                Text="Parent" AutoPostBack="True" />
                                                                                                            <asp:RadioButton ID="rbRelocAgent" runat="server" GroupName="Filled" Text="Relocation Agent"
                                                                                                                AutoPostBack="True" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <asp:Table ID="tbReloc" runat="server" BorderColor="#1B80B6" BorderStyle="Solid"
                                                                                                    BorderWidth="1px" CellPadding="4" CellSpacing="0" Cssgray Width="100%">
                                                                                                    <asp:TableRow ID="r1Reloc" runat="server" BorderColor="#1B80B6" BorderStyle="Solid"
                                                                                                        BorderWidth="1px">
                                                                                                        <asp:TableCell ID="TableCell1" runat="server" BorderColor="#1B80B6" BorderStyle="Solid"
                                                                                                            BorderWidth="1px" Width="16%">Agent Name</asp:TableCell>
                                                                                                        <asp:TableCell ID="TableCell2" runat="server" BorderColor="#1B80B6" BorderStyle="Solid"
                                                                                                            BorderWidth="1px" HorizontalAlign="Center" Width="4%">:</asp:TableCell>
                                                                                                        <asp:TableCell ID="TableCell3" runat="server" BorderColor="#1B80B6" BorderStyle="Solid"
                                                                                                            BorderWidth="1px" Width="25%">
                                                                                                            &nbsp;<asp:TextBox ID="txtAgentName" runat="server" Width="140px"></asp:TextBox>
                                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAgentName"
                                                                                                                Display="Dynamic" ErrorMessage="Agent Name required" ValidationGroup="Prim_cont">*</asp:RequiredFieldValidator>
                                                                                                        </asp:TableCell>
                                                                                                        <asp:TableCell ID="TableCell4" runat="server" BorderColor="#1B80B6" BorderStyle="Solid"
                                                                                                            BorderWidth="1px" Width="16%">Mobile</asp:TableCell>
                                                                                                        <asp:TableCell ID="TableCell5" runat="server" BorderColor="#1B80B6" BorderStyle="Solid"
                                                                                                            BorderWidth="1px" HorizontalAlign="Center" Width="3%">:</asp:TableCell>
                                                                                                        <asp:TableCell ID="TableCell6" runat="server" BorderColor="#1B80B6" BorderStyle="Solid"
                                                                                                            BorderWidth="1px">
                                                                                                            &nbsp;
                                                                                                            <asp:TextBox ID="txtMAgent_country" runat="server" Width="14%" MaxLength="3"></asp:TextBox>
                                                                                                            -
                                                                                                            <asp:TextBox ID="txtMAgent_Area" runat="server" Width="14%" MaxLength="4"></asp:TextBox>-
                                                                                                            <asp:TextBox ID="txtMAgent_No" runat="server" Width="40%" MaxLength="10"></asp:TextBox>
                                                                                                            <span style="display: block; font-size: 7pt">(Country -Area-Number)</span>
                                                                                                        </asp:TableCell>
                                                                                                    </asp:TableRow>
                                                                                                </asp:Table>
                                                                                                I declare that the information given above is to the best of my knowledge, true
                                                                                                and correct. It may be stored and used in accordance with GEMS registration and
                                                                                                selection procedures.
                                                                                                <br />
                                                                                                I understand that giving false information will disqualify my application.
                                                                                                <br />
                                                                                                <asp:CheckBox ID="chkEAdd" runat="server"  />
                                                                                                GEMS may use the above Email Address to send Newsletters and promotional mails.
                                                                                                <br />
                                                                                                <asp:CheckBox ID="chksms" runat="server" Checked="true" Enabled="false"/>
                                                                                                GEMS may use the above Mobile Number to send text messages and alerts.
                                                                                                <br />
                                                                                                <asp:CheckBox ID="chkPubl" runat="server" Checked="true" />
                                                                                                GEMS has permission to include child in promotional photos and videos for use in print/online media for the school.
                                                                                                <br />
                                                                                                <asp:CheckBox ID="chkagree" runat="server" />
                                                                                                Ticking the declaration box (as a substitute for your signature) is to confirm that
                                                                                                you agree to the above declaration.<br />
                                                                                                <br />
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td gray>
                                                                                                            &#8226;&nbsp;Word Verification :
                                                                                                        </td>
                                                                                                        <td gray>
                                                                                                            <asp:TextBox ID="txtCapt" runat="server" Width="80px"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            Type the characters you see in the picture below <span style="color: #ff0033">without
                                                                                                                space</span>.
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                        </td>
                                                                                                        <td colspan="2">
                                                                                                            <MScap:CaptchaControl ID="Captcha1" runat="server" CaptchaBackgroundNoise="medium"
                                                                                                                CaptchaLength="5" CaptchaHeight="40" CaptchaWidth="170" CaptchaLineNoise="None"
                                                                                                                CaptchaMinTimeout="10" CaptchaMaxTimeout="240" FontColor="#1B80B6" Width="170"
                                                                                                                Height="35" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" style="height: 21px; width: 608px;">
                                                                                    <asp:Button ID="btnVwDecl_Pre" runat="server" CssClass="button" Text="Previous" CausesValidation="False"
                                                                                        Height="25px" Width="80px" />
                                                                                    <asp:Button ID="btnVwDecl_Finish" runat="server" CssClass="button" Text="Next" Height="25px"
                                                                                        Width="80px" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" style="height: 21px; width: 608px;">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <div id="divConf" runat="server">
                                                                        <table style="border-collapse: collapse" width="100%">
                                                                            <tr>
                                                                                <td style="width: 608px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 608px">
                                                                                    <table align="left" border="1" style="border-collapse: collapse" bordercolor="#0066cc"
                                                                                        cellpadding="5" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td gray colspan="7" style="background-color: #1b80b6; height: 28px;"
                                                                                                valign="middle">
                                                                                                <span style="font-size: 10pt; color: #ffffff">Confirm Details</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td gray style="width: 16%; height: 26px;">
                                                                                                Applicant name
                                                                                            </td>
                                                                                            <td gray style="height: 26px; width: 5px;">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray style="width: 34%; height: 26px;" colspan="4">
                                                                                                <asp:Literal ID="ltAppName" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td gray style="width: 16%; height: 26px">
                                                                                                School
                                                                                            </td>
                                                                                            <td gray style="width: 5px; height: 26px">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray colspan="4" style="height: 26px">
                                                                                                <asp:Literal ID="ltSchool" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td gray style="width: 16%; height: 26px">
                                                                                                Curriculum
                                                                                            </td>
                                                                                            <td gray style="width: 5px;">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray style="width: 34%; height: 26px">
                                                                                                <asp:Literal ID="ltCurr" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td gray style="width: 20%; height: 26px">
                                                                                                Academic Year
                                                                                            </td>
                                                                                            <td gray style="width: 2px; color: #1b80b6; height: 26px">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray style="width: 166px; height: 26px">
                                                                                                <asp:Literal ID="ltAcd" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td gray style="width: 16%; height: 26px;">
                                                                                                Grade
                                                                                            </td>
                                                                                            <td gray style="height: 26px; width: 5px;">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray style="width: 34%; height: 26px;" colspan="4">
                                                                                                <asp:Literal ID="ltGrade" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td gray style="width: 16%; height: 26px">
                                                                                                Date of birth
                                                                                            </td>
                                                                                            <td gray style="width: 5px;">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray style="width: 34%; height: 26px">
                                                                                                <asp:Literal ID="ltDob" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td gray style="width: 20%; height: 26px">
                                                                                                Nationality
                                                                                            </td>
                                                                                            <td gray style="width: 2px; color: #1b80b6; height: 26px">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray style="width: 166px; height: 26px">
                                                                                                <asp:Literal ID="ltNat" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td gray style="width: 16%; height: 26px">
                                                                                                Primary Contact
                                                                                            </td>
                                                                                            <td gray style="width: 5px;">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray style="width: 34%; height: 26px">
                                                                                                <asp:Literal ID="ltPrim_cont" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                            <td gray style="width: 20%; height: 26px">
                                                                                                Preferred Contact
                                                                                            </td>
                                                                                            <td gray style="width: 2px; color: #1b80b6; height: 26px">
                                                                                                :
                                                                                            </td>
                                                                                            <td gray style="width: 166px; height: 26px">
                                                                                                <asp:Literal ID="LtPref_Cont" runat="server"></asp:Literal>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right" style="height: 21px; width: 608px;">
                                                                                    <asp:Button ID="btnConf_Prev" runat="server" CssClass="button" Text="Previous" CausesValidation="False"
                                                                                        Height="25px" Width="80px" />
                                                                                    <asp:Button ID="btnConfirm" runat="server" CssClass="button" Text="Confirm" Height="25px"
                                                                                        Width="80px" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div id="divNote" runat="server" style="border: 1px solid #1B80B6; width: 600px;
                                                                text-align: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;
                                                                color: #800000; padding: 5pt;">
                                                                <b>Note :-</b>
                                                                <ul style="margin-top: 1px; margin-bottom: 1px;">
                                                                    <li>This site is best viewed with IE 7.0 or above at a minimum screen resolution of
                                                                        1024x768.</li>
                                                                    <li>View the error message which gets displayed below Online Administration and Student
                                                                        Information System title.</li>
                                                                    <li>Progress to each step is indicated on the left hand side menu.</li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2" style="background-repeat: repeat; height: 22px" valign="top">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="center">
                    <td>
                        <img height="6px" src="../Images/colourbar.jpg" width="840" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 840px">
                        <font face="Arial" style="font-size: 7pt; letter-spacing: 1px">PO Box 8607, Dubai, United
                            Arab Emirates | Tel +971 4 347 7770 | Fax +971 4 340 3390 | &nbsp;                        Arab Emirates | Tel +971 4 347 7770 | Fax +971 4 340 3390 | &nbsp; <a href="http://www.gemseducation.com">
                                www.gemseducation.com</a> </font>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <ajaxToolkit:CalendarExtender ID="caldoj" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
        TargetControlID="txtDoj">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnDob_date" TargetControlID="txtDob">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnPassIss_date" TargetControlID="txtPassIss_date">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnPassExp_date" TargetControlID="txtPassExp_Date">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnVisaIss_date" TargetControlID="txtVisaIss_date">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnVisaExp_date" TargetControlID="txtVisaExp_date">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:HoverMenuExtender ID="HoverMenuExtender1" runat="server" TargetControlID="ImgPassHelp"
        PopupControlID="divPassport" PopupPosition="right" OffsetX="6" PopDelay="25">
    </ajaxToolkit:HoverMenuExtender>
    <asp:HiddenField ID="hfcutoff" runat="server" Value=" " />
    <asp:HiddenField ID="hfF" runat="server" Value="1" />
    <asp:HiddenField ID="hfM" runat="server" Value="0" />
    <asp:HiddenField ID="hfG" runat="server" Value="0" />
    <div id="divPassport" class="popupMenu">
        If Student applicant does not hold a passport,please contact Registrar.&nbsp;</div>
    <asp:HiddenField ID="hfCurrSchool" runat="server" />
    <asp:HiddenField ID="hf_status" runat="server" Value="0" />
    <%--  </ContentTemplate>
                            </aspAjax:UpdatePanel> --%>
    <asp:HiddenField ID="hfValid_ID" runat="server" />
    </form>
</body>
</html>

<script language="javascript" type="text/javascript">
    if (document.getElementById('chkFHide') != null) {

        for (i = 111; i <= 126; i++) {
            if (document.getElementById('chkFHide').checked == 1) {
                document.getElementById(i).style.display = 'none';
            }
            else {
                document.getElementById(i).style.display = 'table-row';

            }
        }
        document.getElementById(127).style.display = 'table-row';
        document.getElementById(128).style.display = 'none';
        document.getElementById(129).style.display = 'none';
        document.getElementById(130).style.display = 'table-row';
        document.getElementById(131).style.display = 'none';
        document.getElementById(132).style.display = 'none';

    }

    if (document.getElementById('chkMHide') != null) {

        for (i = 135; i <= 150; i++) {
            if (document.getElementById('chkMHide').checked == 1) {
                document.getElementById(i).style.display = 'none';
            }
            else {
                document.getElementById(i).style.display = 'table-row';

            }
        }

        document.getElementById(151).style.display = 'table-row';
        document.getElementById(152).style.display = 'none';
        document.getElementById(153).style.display = 'none';
        document.getElementById(154).style.display = 'table-row';
        document.getElementById(155).style.display = 'none';
        document.getElementById(156).style.display = 'none';


    }
    if (document.getElementById('chkGHide') != null) {

        for (i = 160; i <= 175; i++) {
            if (document.getElementById('chkGHide').checked == 1) {
                document.getElementById(i).style.display = 'none';
            }
            else {
                document.getElementById(i).style.display = 'table-row';

            }
        }

    }
                               
	                          

</script>

