﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class Students_UpdateStudentContractViewer
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Public Property bTRANSPORT() As Boolean
        Get
            Return ViewState("bTRANSPORT")
        End Get
        Set(value As Boolean)
            ViewState("bTRANSPORT") = value
        End Set
    End Property
    Public Property bMatchNames() As Boolean
        Get
            Return ViewState("bMatchNames")
        End Get
        Set(value As Boolean)
            ViewState("bMatchNames") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_4.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_5.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
            h_selected_menu_8.Value = "LI__../Images/operations/like.gif"
            gvJournal.Attributes.Add("bordercolor", "#1b80b6")
            Try
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S000230") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Dim str_transaction As String = ""
                    Select Case ViewState("MainMnu_code").ToString
                        Case "S000230"
                            lblHead.Text = "Update Student Contract "
                            bTRANSPORT = False
                            'rblFilter.Items(0).Text = ""
                            'rblFilter.Items(0).Attributes.CssStyle.Add("visibility", "hidden")

                    End Select



                    bMatchNames = True
                    bindAYear()
                    Bindgrade()
                    gridbind()

                    'If rblFilter.SelectedValue = "PND" Then
                    '    Me.btnApprove.Visible = True
                    '    Me.btnReject.Visible = True
                    'Else
                    '    Me.btnApprove.Visible = False
                    '    Me.btnReject.Visible = False
                    'End If
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                usrMessageBar.ShowNotification("Request could not be processed ", UserControls_usrMessageBar.WarningType.Danger)
            End Try
        Else
            'If rblFilter.SelectedValue = "PND" Then
            '    Me.btnApprove.Visible = True
            '    Me.btnReject.Visible = True
            'Else
            '    Me.btnApprove.Visible = False
            '    Me.btnReject.Visible = False
            'End If

        End If
    End Sub
    Private Sub SetNameMismatchAlert()
        If gvJournal.Rows.Count <= 0 Then
            Me.lblAlert.Text = ""
        ElseIf bMatchNames = False Then
            Me.lblAlert.Text = "Highlighted are Concession(s) having mismatch in Employee name and Parent name of Child, Please verify before approval"
        Else
            Me.lblAlert.Text = ""
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvJournal.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvJournal.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Protected Sub rblFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        gridbind()
    End Sub

    Sub bindAYear()

        Dim str_query As String = "select distinct ACY_ID,ACY_DESCR from ACADEMICYEAR_M inner join ACADEMICYEAR_D on ACD_ACY_ID=ACY_ID where ACD_BSU_ID='" & Session("sBsuid") & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_query)
        ddlAcaYear.DataSource = ds
        ddlAcaYear.DataTextField = "ACY_DESCR"
        ddlAcaYear.DataValueField = "ACY_ID"
        ddlAcaYear.DataBind()

        ddlAcaYear.SelectedValue = "30"
        ddlAcaYear.SelectedItem.Text = "2019-2020"





    End Sub
    Sub Bindgrade()
        Dim str_query1 As String = "select GRD_ID,GRD_DISPLAY  from GRADE_M "

        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_query1)
        ddlGrade.DataSource = ds1
        ddlGrade.DataTextField = "GRD_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        BindSection()

    End Sub

    Sub BindSection()
        'Dim str_query2 As String = ""
        'If ddlAcaYear.SelectedValue = "" Then
        '    str_query2 = "select SCT_ID,SCT_DESCR  from SECTION_M where SCT_BSU_ID='" & Session("sBsuid") & "'"
        'Else
        '    str_query2 = "select SCT_ID,SCT_DESCR  from SECTION_M where SCT_ACD_ID='" & ddlAcaYear.SelectedValue & "' and SCT_BSU_ID='" & Session("sBsuid") & "' and SCT_GRD_ID='" & ddlGrade.SelectedValue & "'"
        'End If
        'Dim ds2 As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_query2)
        'ddlSection.DataSource = ds2
        'ddlSection.DataTextField = "SCT_DESCR"
        'ddlSection.DataValueField = "SCT_ID"
        'ddlSection.DataBind()

    End Sub
    Sub gridbind()
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7, lstrCondn8 As String

            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            lstrCondn8 = ""
            str_Filter = ""
            
            If gvJournal.Rows.Count > 0 Then
                ' --- Initialize The Variables
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                '   --- FILTER CONDITIONS ---
                '   -- 1   Student NO
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STUNO", lstrCondn1)

                '   -- 2  txtStudentName
                larrSearchOpr = h_selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtStuName")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "NAME", lstrCondn2)

                '   -- 3  txtGrade
                larrSearchOpr = h_selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRADE", lstrCondn3)

                '   -- 4   txtSection
                larrSearchOpr = h_selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvJournal.HeaderRow.FindControl("txtSection")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "Section", lstrCondn4)

            End If


            Dim STR_RECEIPT As String = "SP_GET_STUDENT_NOTUPDATEDCONTRACT_LIST"
            Dim SPParam(5) As SqlParameter

            SPParam(0) = Mainclass.CreateSqlParameter("@BSU_ID", Session("sBsuid"), SqlDbType.VarChar)
            SPParam(1) = Mainclass.CreateSqlParameter("@ACY_ID", ddlAcaYear.SelectedValue, SqlDbType.Int)
            SPParam(2) = Mainclass.CreateSqlParameter("@GRADE_ID", ddlGrade.SelectedItem.Value, SqlDbType.VarChar)
            'SPParam(3) = Mainclass.CreateSqlParameter("@SECTION_ID", ddlSection.SelectedItem.Value, SqlDbType.VarChar)
            SPParam(3) = Mainclass.CreateSqlParameter("@SECTION_ID", "", SqlDbType.VarChar)
            SPParam(4) = Mainclass.CreateSqlParameter("@str_Filter", str_Filter, SqlDbType.VarChar)
            SPParam(5) = Mainclass.CreateSqlParameter("@STATUS", rblFilter.SelectedValue, SqlDbType.VarChar)



            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, STR_RECEIPT, SPParam)
            gvJournal.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvJournal.DataBind()
                Dim columnCount As Integer = gvJournal.Rows(0).Cells.Count
                gvJournal.Rows(0).Cells.Clear()
                gvJournal.Rows(0).Cells.Add(New TableCell)
                gvJournal.Rows(0).Cells(0).ColumnSpan = columnCount
                gvJournal.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvJournal.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvJournal.DataBind()
            End If
            
            bMatchNames = True

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvJournal.HeaderRow.FindControl("txtStuName")
            txtSearch.Text = lstrCondn2

            txtSearch = gvJournal.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvJournal.HeaderRow.FindControl("txtSection")
            txtSearch.Text = lstrCondn4

          

            If rblFilter.SelectedValue = "YES" Then
                Me.gvJournal.Columns(0).Visible = False

                btnApprove.Visible = False
            Else
                Me.gvJournal.Columns(0).Visible = True

                btnApprove.Visible = True



            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvJournal.PageIndexChanging
        gvJournal.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblReceipt As Label = sender.Parent.parent.findcontrol("lblReceipt")


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
        gridbind()

    End Sub

    'Protected Sub ddlCount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged
    '    gridbind()
    'End Sub
    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcaYear.SelectedIndexChanged


        Bindgrade()
        gridbind()

    End Sub


    Protected Sub gvJournal_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJournal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hf_EMPID As HiddenField = DirectCast(e.Row.FindControl("hf_STP"), HiddenField)
            
        End If
    End Sub

    Private Function CompareEmpParent(ByVal EMPID As Int64, ByVal STUID As Int64) As Boolean
        CompareEmpParent = True

        Try
            Dim bMatch As Boolean = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT dbo.fnCompEmployeeAndParent(" & EMPID & "," & STUID & ")")
            CompareEmpParent = bMatch
        Catch ex As Exception
            CompareEmpParent = False
        End Try


    End Function

    Private Function GetSelectedRows() As String
        GetSelectedRows = ""
        For Each gvr As GridViewRow In gvJournal.Rows
            Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
            Dim hf_SCRID As HiddenField = DirectCast(gvr.FindControl("hf_SCRID"), HiddenField)
            Dim hf_SCDID As HiddenField = DirectCast(gvr.FindControl("hf_SCDID"), HiddenField)
            If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then
                GetSelectedRows &= "|" & hf_SCDID.Value
            End If
        Next
    End Function
    Sub Approve_Reject_Tuition_Fee_request(ByVal action As String)
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
        objConn.Open()
        Dim bRolledBack As Boolean = False
        Dim bCommitted As Boolean = False
        Dim Trans As SqlTransaction
        Try
            Dim objcls As New clsEMPLOYEE_CHILD_FEE_CONCESSION
            For Each gvr As GridViewRow In gvJournal.Rows

                Dim chkSelect As CheckBox = DirectCast(gvr.FindControl("chkSelect"), CheckBox)
                Dim cUpdatdateText As TextBox = DirectCast(gvr.FindControl("txtCUpdateDate"), TextBox)
                Dim STPID As HiddenField = DirectCast(gvr.FindControl("hf_STP"), HiddenField)
                Dim STUID As HiddenField = DirectCast(gvr.FindControl("hf_STU"), HiddenField)
                Dim ACDID As HiddenField = DirectCast(gvr.FindControl("hf_ACD_ID"), HiddenField)


                Dim lblErrorMessage As Label = DirectCast(gvr.FindControl("lblErrorMessage"), Label)
                If Not chkSelect Is Nothing AndAlso chkSelect.Checked Then
                    Dim retval As String = "1"
                    If 1 = 1 Then
                        Trans = objConn.BeginTransaction("APVE")
                        If 1 = 0 Then
                            retval = "1"
                        Else

                            retval = UpdateStudentContract(objConn, Trans, Session("sBsuid"), STUID.Value, ACDID.Value, cUpdatdateText.Text)

                        End If
                        If retval <> "0" Then
                            bRolledBack = True
                            Trans.Rollback()
                        Else
                            Trans.Commit()
                            bCommitted = True
                        End If
                    Else
                        bRolledBack = True
                        gvJournal.Columns(20).Visible = True
                    End If
                End If
            Next
        Catch ex As Exception
            'Me.lblError.Text = "Unable to approve Request(s)"
            Trans.Rollback()
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            If bRolledBack = True And bCommitted = True Then
                If action = "A" Then
                    usrMessageBar.ShowNotification("Not all request(s) have been approved, check the 'Error' column for details", UserControls_usrMessageBar.WarningType.Danger)
                Else
                    usrMessageBar.ShowNotification("Not all request(s) have been rejected, check the 'Error' column for details", UserControls_usrMessageBar.WarningType.Danger)
                End If

            ElseIf bCommitted Then

                If action = "A" Then
                    usrMessageBar.ShowNotification("Selected Request(s) have been approved", UserControls_usrMessageBar.WarningType.Success)
                Else
                    usrMessageBar.ShowNotification("Selected Request(s) have been rejected", UserControls_usrMessageBar.WarningType.Success)
                End If

            ElseIf bRolledBack Then
                If action = "A" Then
                    usrMessageBar.ShowNotification("Unable to approve Request(s)", UserControls_usrMessageBar.WarningType.Danger)
                Else
                    usrMessageBar.ShowNotification("Unable to reject Request(s)", UserControls_usrMessageBar.WarningType.Danger)

                End If

            End If
            gridbind()
        End Try
        'Else
        'Me.lblError.Text = "Please select one or more concession(s) to be approved"
        'End If

    End Sub
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Approve_Reject_Tuition_Fee_request("A")
    End Sub
    Public Function UpdateStudentContract(ByVal objCon As SqlConnection, ByVal TRANS As SqlTransaction, ByVal BSUID As String, ByVal STU_ID As Integer, ByVal ACD_ID As Integer, ByVal CUDate As DateTime) As String
        UpdateStudentContract = "0"
        Try
            Dim cmd As SqlCommand

            cmd = New SqlCommand("UPDATE_STUDENT_CONTRACT", objCon, TRANS)
            cmd.CommandType = CommandType.StoredProcedure




            Dim pParms(4) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
            pParms(0).Value = BSUID
            cmd.Parameters.Add(pParms(0))

            pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.Int)
            pParms(1).Value = STU_ID
            cmd.Parameters.Add(pParms(1))

            pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
            pParms(2).Value = ACD_ID
            cmd.Parameters.Add(pParms(2))


            pParms(3) = New SqlClient.SqlParameter("@STP_CUDATE", SqlDbType.DateTime)
            pParms(3).Value = CUDate
            cmd.Parameters.Add(pParms(3))


            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(pParms(4))


            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            UpdateStudentContract = pParms(4).Value

        Catch ex As Exception
            UpdateStudentContract = "1"
            usrMessageBar.ShowNotification(ex.Message, UserControls_usrMessageBar.WarningType.Danger)
        End Try

    End Function
    Protected Sub gvJournal_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvJournal.DataBound

        For Each gvr As GridViewRow In gvJournal.Rows
            If rblFilter.SelectedValue = "YES" Then
                gvr.Cells(0).Enabled = False
                gvr.Cells(1).Enabled = False
            Else
                gvr.Cells(0).Enabled = True
                gvr.Cells(1).Enabled = True

            End If
        Next
    End Sub


    Protected Sub rblFilter_SelectedIndexChanged1(sender As Object, e As EventArgs) Handles rblFilter.SelectedIndexChanged
        If rblFilter.SelectedValue = "YES" Then
            UpdatePanel.Visible = False
        Else
            UpdatePanel.Visible = True
        End If
    End Sub
End Class
