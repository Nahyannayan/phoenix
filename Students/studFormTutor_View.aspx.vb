Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections

Partial Class Students_studFormTutor_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        lblError.Text = ""

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100025") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                    GridBind()

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            '     ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            url = String.Format("~\Students\studFormtutor_M.aspx?MainMnu_code={0}&datamode={1}&mode=add", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnClassTeacher_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    'Protected Sub gvStudTutor_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudTutor.RowCommand

    'End Sub

#Region "Private Methods"

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTutor.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTutor.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTutor.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTutor.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTutor.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTutor.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GridBind()
        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
        Dim ddlgvShift As New DropDownList
        Dim ddlgvStream As New DropDownList

        Dim dv As New DataView
        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedShift As String = ""
        Dim selectedStream As String = ""
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        ViewState("str_secid") = ""
        Dim arrGrade As New ArrayList
        Dim arrSection As New ArrayList
        'Dim str_query As String = "SELECT sec_id,sct_descr,sec_grm_id,emp_fname+' '+emp_mname+' '+emp_lname as emp_name,sec_emp_id,sec_sct_id FROM " _
        '                        & "section_tutor_d,section_m,employee_m WHERE " _
        '                        & "section_tutor_d.sec_sct_id = section_m.sct_id " _
        '                        & "And section_tutor_d.sec_emp_id = employee_m.emp_id " _
        '                        & "And sec_grm_id = sct_grm_id And sec_acd_id = sct_acd_id " _
        '                        & "AND sec_acd_id=" + ViewState("SelectedYear").ToString + " AND sec_bsu_id='" + Session("sbsuid").ToString + "' and sec_todt is null"



        Dim str_query As String = "SELECT SEC_ID,SEC_SCT_ID,SEC_GRM_ID,GRM_DISPLAY,SHF_DESCR,STM_DESCR," _
                                 & "SCT_DESCR,EMP_NAME=ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,''), SEC_EMP_ID FROM SECTION_TUTOR_D " _
                                 & "AS A INNER JOIN GRADE_BSU_M AS B ON A.SEC_GRM_ID = B.GRM_ID INNER JOIN " _
                                 & "EMPLOYEE_M AS F ON A.SEC_EMP_ID = F.EMP_ID INNER JOIN  SECTION_M AS E ON " _
                                 & "A.SEC_SCT_ID = E.SCT_ID  AND E.SCT_GRM_ID=B.GRM_ID INNER JOIN SHIFTS_M AS C ON B.GRM_SHF_ID = C.SHF_ID INNER JOIN " _
                                 & "STREAM_M AS D ON B.GRM_STM_ID = D.STM_ID inner join GRADE_M on GRD_ID=GRM_GRD_ID  WHERE SEC_ACD_ID=" + ddlAcademicYear.SelectedValue + " AND SEC_BSU_ID='" + Session("sbsuid") + "' AND SEC_TODT IS NULL"



        Dim str_filter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim txtSearch As TextBox

        If gvStudTutor.Rows.Count > 0 Then


            txtSearch = New TextBox
            txtSearch = gvStudTutor.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            str_filter += GetSearchString("grm_display", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            ddlgvShift = gvStudTutor.HeaderRow.FindControl("ddlgvShift")
            If ddlgvShift.Text <> "ALL" Then
                If str_filter = "" Then
                    str_filter = " and shf_descr='" + ddlgvShift.Text + "'"
                Else
                    str_filter = str_filter + " and shf_descr='" + ddlgvShift.Text + "'"
                End If

                selectedShift = ddlgvShift.Text
            End If

            ddlgvStream = gvStudTutor.HeaderRow.FindControl("ddlgvStream")
            If ddlgvStream.Text <> "ALL" Then
                If str_filter = "" Then
                    str_filter = " and stm_descr='" + ddlgvStream.Text + "'"
                Else
                    str_filter = str_filter + " and stm_descr='" + ddlgvStream.Text + "'"
                End If

                selectedStream = ddlgvStream.Text
            End If


            txtSearch = New TextBox
            txtSearch = gvStudTutor.HeaderRow.FindControl("txtSection")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            str_filter += GetSearchString("sct_descr", txtSearch.Text, strSearch)
            selectedSection = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudTutor.HeaderRow.FindControl("txtClassTeacher")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            str_filter += GetSearchString("isnull(emp_fname,' ')+' '+isnull(emp_mname,' ')+' '+isnull(emp_lname,' ')", txtSearch.Text, strSearch)

            If str_filter.Trim <> "" Then
                str_query = str_query + str_filter
            End If

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query & " order by GRD_DISPLAYORDER,SCT_DESCR")
        gvStudTutor.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStudTutor.DataBind()
            Dim columnCount As Integer = gvStudTutor.Rows(0).Cells.Count
            gvStudTutor.Rows(0).Cells.Clear()
            gvStudTutor.Rows(0).Cells.Add(New TableCell)
            gvStudTutor.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudTutor.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudTutor.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudTutor.DataBind()
        End If

        Dim dt As DataTable = ds.Tables(0)

        Dim dr_sec As DataRow
        Dim str_secid As String = ""

        For Each dr_sec In dt.Rows
            If str_secid.Trim() = "" Then
                str_secid = dr_sec(0).ToString
            Else
                str_secid += "," + dr_sec(0).ToString
            End If
        Next
        ViewState("str_secid") = str_secid

        If gvStudTutor.Rows.Count > 0 Then


            ddlgvStream = gvStudTutor.HeaderRow.FindControl("ddlgvStream")
            ddlgvShift = gvStudTutor.HeaderRow.FindControl("ddlgvShift")

            Dim dr As DataRow


            ddlgvShift.Items.Clear()
            ddlgvShift.Items.Add("ALL")


            ddlgvStream.Items.Clear()
            ddlgvStream.Items.Add("ALL")

            For Each dr In dt.Rows
                With dr
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    If ddlgvShift.Items.FindByText(.Item(4)) Is Nothing Then
                        ddlgvShift.Items.Add(.Item(4))
                    End If
                    If ddlgvStream.Items.FindByText(.Item(5)) Is Nothing Then
                        ddlgvStream.Items.Add(.Item(5))
                    End If

                End With
            Next


            If selectedShift <> "" Then
                ddlgvShift.Text = selectedShift
            End If

            If selectedStream <> "" Then
                ddlgvStream.Text = selectedStream
            End If

            txtSearch = New TextBox
            txtSearch = gvStudTutor.HeaderRow.FindControl("txtSection")
            txtSearch.Text = selectedSection

            txtSearch = New TextBox
            txtSearch = gvStudTutor.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = selectedGrade
        End If
    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region

  

    Protected Sub gvStudTutor_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs)
        Try
            Dim index As Integer = Convert.ToInt32(e.NewSelectedIndex)
            Dim selectedRow As GridViewRow = DirectCast(gvStudTutor.Rows(index), GridViewRow)
            Dim url As String
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            Dim lblsecid As New Label
            Dim lblgrade As New Label
            Dim lblsctid As New Label
            Dim lblsection As New Label
            Dim lblTeacher As New Label
            Dim lblTeacherId As New Label
            Dim lblGrmid As New Label
            Dim lblShift As New Label
            Dim lblStream As New Label
            With selectedRow
                lblsecid = .FindControl("lblSecid")
                lblsctid = .FindControl("lblsctid")
                lblgrade = .FindControl("lblGrade")
                lblsection = .FindControl("lblSection")
                lblTeacher = .FindControl("lblClassTeacher")
                lblTeacherId = .FindControl("lblTeacherId")
                lblGrmid = .FindControl("lblGrmId")
                lblShift = .FindControl("lblShift")
                lblStream = .FindControl("lblStream")
            End With
            'Encrypt the data that needs to be send through Query String
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim editString As String = lblsecid.Text + "|" + ddlAcademicYear.SelectedItem.Text + "|" + ddlAcademicYear.SelectedValue.ToString _
                                       & "|" + lblgrade.Text + "|" + lblsection.Text + "|" + lblsctid.Text + "|" + lblTeacher.Text + "|" + lblTeacherId.Text _
                                       & "|" + lblShift.Text + "|" + lblStream.Text + "|" + lblGrmid.Text

            editString = Encr_decrData.Encrypt(editString)
            url = String.Format("~\Students\studFormTutor_M.aspx?MainMnu_code={0}&datamode={1}&mode=edit&editstring=" + editString, ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub gvStudTutor_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvStudTutor.PageIndex = e.NewPageIndex
        GridBind()

    End Sub

    Protected Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("str_secid").ToString.Trim = "" Then
            lblError.Text = "No Records To Print....!!!"
        Else
            h_print.Value = "print"
            CallReport()
        End If
    End Sub
    Sub CallReport()
        Try
            Dim param As New Hashtable
            param.Add("@IMG_BSU_ID", Session("SBSUID"))
            param.Add("@IMG_TYPE", "LOGO")

            param.Add("@SEC_ID", ViewState("str_secid"))
            param.Add("@BSU_ID", Session("SBSUID"))
            param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)

            param.Add("UserName", Session("sUsr_name"))
            param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = param
                .reportPath = Server.MapPath("~/Students/Reports/RPT/rptstudFormTutor_print.rpt")
            End With
            Session("rptClass") = rptClass
            '' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

            'Dim url As String, strRedirect As String
            ' strRedirect = "~/Reports/ASPX Report/rptReportViewer.aspx"
            'strRedirect = "~/Reports/ASPX Report/RptViewerModal.aspx"
            'url = String.Format("{0}", strRedirect)
            'ResponseHelper.Redirect(url, "_blank", "")
            ' Session("ReportSource") = rptClass
        Catch ex As Exception
            lblError.Text = "Request cannot be completed"

        End Try
    End Sub
    'Protected Sub PrintReceipt()

    '    Dim ds As New DataSet
    '    If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
    '        cmd.Connection = New SqlConnection(str_conn)
    '        Dim repSource As New MyReportClass
    '        Dim params As New Hashtable

    '        params("RPT_CAPTION") = "PRO FORMA INVOICE"

    '        params("UserName") = Session("sUsr_name")
    '        repSource.Parameter = params
    '        repSource.Command = cmd
    '        repSource.IncludeBSUImage = True
    '        If bCompanyProforma Then
    '            Dim repSourceSubRep(2) As MyReportClass
    '            repSourceSubRep(0) = New MyReportClass
    '            Dim cmdSubEarn As New SqlCommand

    '            cmdSubEarn.CommandText = str_Sql
    '            cmdSubEarn.Connection = New SqlConnection(str_conn)
    '            cmdSubEarn.CommandType = CommandType.Text
    '            repSourceSubRep(0).Command = cmdSubEarn
    '            repSource.SubReport = repSourceSubRep

    '            repSourceSubRep(1) = New MyReportClass
    '            Dim cmdNetPayable As New SqlCommand
    '            cmdNetPayable.CommandText = str_Sql_net
    '            cmdNetPayable.Connection = New SqlConnection(str_conn)
    '            cmdNetPayable.CommandType = CommandType.Text
    '            repSourceSubRep(0).Command = cmdNetPayable
    '            repSource.SubReport = repSourceSubRep
    '            repSource.ResourceName = "../../fees/Reports/RPT/rptFeeProformaInvoiceCompany.rpt"
    '        Else
    '            Dim repSourceSubRep(1) As MyReportClass
    '            repSourceSubRep(0) = New MyReportClass
    '            Dim cmdNetPayable As New SqlCommand
    '            cmdNetPayable.CommandText = str_Sql_net
    '            cmdNetPayable.Connection = New SqlConnection(str_conn)
    '            cmdNetPayable.CommandType = CommandType.Text
    '            repSourceSubRep(0).Command = cmdNetPayable
    '            repSource.SubReport = repSourceSubRep
    '            repSource.ResourceName = "../../fees/Reports/RPT/rptFeeProformaInvoice.rpt"
    '        End If
    '        Session("ReportSource") = repSource
    '        'Response.Redirect("../Reports/ASPX Report/rptviewer.aspx", True)
    '    End If
    'End Sub
End Class
