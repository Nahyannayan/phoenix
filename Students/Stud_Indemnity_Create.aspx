<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Stud_Indemnity_Create.aspx.vb" Inherits="Students_Stud_Indemnity_Create" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript">

        function Matter() {
            var ctrl = document.getElementById('<%= txtMatter.clientID %>');

       var saveText = ctrl.value;
       ctrl.focus();
       var range = document.selection.createRange();
       var specialchar = String.fromCharCode(1);

       range.text = specialchar;
       var pos = ctrl.value.indexOf(specialchar);
       ctrl.value = saveText;
       range = ctrl.createTextRange();
       range.move('character', pos);
       range.select();
       var e = document.getElementById('<%= ddlMatter.clientID %>');
         range.text = e.options[e.selectedIndex].value;

         document.getElementById('<%= txtMatter.clientID %>').focus();
          window.event.returnValue = false;
      }
      function Remark() {
          var ctrl = document.getElementById('<%= txtRemark.clientID %>');

     var saveText = ctrl.value;
     ctrl.focus();
     var range = document.selection.createRange();
     var specialchar = String.fromCharCode(1);

     range.text = specialchar;
     var pos = ctrl.value.indexOf(specialchar);
     ctrl.value = saveText;
     range = ctrl.createTextRange();
     range.move('character', pos);
     range.select();
     var e = document.getElementById('<%= ddlRemark.clientID %>');
         range.text = e.options[e.selectedIndex].value;

         document.getElementById('<%= txtRemark.clientID %>').focus();
          window.event.returnValue = false;
      }
      function Signature() {
          var ctrl = document.getElementById('<%= txtSign.clientID %>');

        var saveText = ctrl.value;
        ctrl.focus();
        var range = document.selection.createRange();
        var specialchar = String.fromCharCode(1);

        range.text = specialchar;
        var pos = ctrl.value.indexOf(specialchar);
        ctrl.value = saveText;
        range = ctrl.createTextRange();
        range.move('character', pos);
        range.select();
        var e = document.getElementById('<%= ddlSign.clientID %>');
         range.text = e.options[e.selectedIndex].value;

         document.getElementById('<%= txtSign.clientID %>').focus();
          window.event.returnValue = false;
      }
      function Ack_parent() {
          var ctrl = document.getElementById('<%= txtAck.clientID %>');

        var saveText = ctrl.value;
        ctrl.focus();
        var range = document.selection.createRange();
        var specialchar = String.fromCharCode(1);

        range.text = specialchar;
        var pos = ctrl.value.indexOf(specialchar);
        ctrl.value = saveText;
        range = ctrl.createTextRange();
        range.move('character', pos);
        range.select();
        var e = document.getElementById('<%= ddlParent.clientID %>');
         range.text = e.options[e.selectedIndex].value;

         document.getElementById('<%= txtAck.clientID %>').focus();
          window.event.returnValue = false;
      }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Indemnity Form Setting"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive ">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                        Font-Size="10px"></asp:Label><span style="font-size: 8pt; color: #800000">&nbsp;</span>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="Offer"></asp:ValidationSummary>
                                    <span style="font-size: 7pt; color: #800000">&nbsp;</span>
                                </div>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" valign="bottom">
                            <table align="left" style="border-collapse: collapse; width: 100%;" cellpadding="5" cellspacing="0">

                                <tr>
                                    <td align="left">
                                        <span class="field-label">Matter</span><span style="font-size: 7pt; color: #800000">*</span></td>

                                    <td align="left" valign="middle">
                                        <table >
                                            <tr>
                                                <td rowspan="2"  valign="top">
                                                    <asp:TextBox ID="txtMatter" runat="server" Height="141px" TextMode="MultiLine" Width="436px" SkinID="MultiText_Large"></asp:TextBox></td>
                                                <td align="left" valign="top">
                                                    <div>
                                                        <span class="field-label">Set Line Spacing</span><asp:RadioButtonList ID="rbMatter" GroupName="Mat" runat="server">
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><span class="field-label">Insert Applicant Info or the selected format</span>
                            <asp:DropDownList ID="ddlMatter" runat="server">
                            </asp:DropDownList>&nbsp;
                                        <asp:Button ID="btnMatter" runat="server" CssClass="button" Text="Insert" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMatter"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Matter cannot be left empty" ForeColor=""
                                            ValidationGroup="Offer">*</asp:RequiredFieldValidator></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Remarks</span></td>
                                   
                                    <td align="left" valign="middle">
                                        <table >
                                            <tr>
                                                <td rowspan="2"  valign="top">

                                                    <asp:TextBox ID="txtRemark" runat="server" Width="436px" Height="105px" SkinID="MultiText_Large" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="top">
                                                    <div>
                                                       <span class="field-label"> Set Line Spacing</span><asp:RadioButtonList ID="rbRemark" runat="server" GroupName="Remark">
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><span class="field-label">Insert  the selected format</span>
                         
                         <asp:DropDownList ID="ddlRemark" runat="server">
                         </asp:DropDownList>&nbsp;
                                        <asp:Button ID="btnRemark" runat="server" CssClass="button"
                                            Text="Insert" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="display: none;">
                                    <td align="left" ><span class="field-label">Signature  </span>                     
                                    </td>
                                   
                                    <td align="left" valign="middle">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtSign" runat="server" Width="319px" Height="46px" TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <div>Insert  the selected format</div>
                                                    <asp:DropDownList ID="ddlSign" runat="server">
                                                    </asp:DropDownList>&nbsp;
                            <asp:Button ID="btnSign" runat="server" CssClass="button"
                                Text="Insert" />
                                                </td>

                                            </tr>

                                        </table>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">&nbsp;Acknowledgement</span></td>
                                    
                                    <td align="left" valign="middle">
                                        <table >
                                            <tr>
                                                <td rowspan="2"  valign="top">

                                                    <asp:TextBox ID="txtAck" runat="server" Height="104px" TextMode="MultiLine" Width="436px" SkinID="MultiText_Large"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="top">
                                                    <div><span class="field-label">Set Line Spacing</span></div>
                                                    <asp:RadioButtonList ID="rbParent" GroupName="Ack" runat="server">
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <div><span class="field-label">Insert  the selected format</span></div>
                                                    <asp:DropDownList ID="ddlParent" runat="server">
                                                    </asp:DropDownList>&nbsp;
                            <asp:Button ID="btnParent" runat="server" CssClass="button"
                                Text="Insert" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="bottom"></td>
                    </tr>
                    <tr>
                        <td  valign="bottom" align="center">
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False"
                                CssClass="button" OnClick="btnEdit_Click" Text="Edit" /><asp:Button ID="btnSave"
                                    runat="server" CssClass="button" Text="Save" ValidationGroup="Offer" /><asp:Button
                                        ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" OnClick="btnCancel_Click"
                                        Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

