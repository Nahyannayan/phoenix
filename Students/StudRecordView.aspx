<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudRecordView.aspx.vb" Inherits="StudRecordView" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

      <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i> Edit Student Records
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">&nbsp; &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table id="tbl_test" runat="server" align="center" width="100%" cellpadding="5" cellspacing="0">
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Academic year</span></td>
                                    <td  width="25%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server" CssClass="listbox" Width="132px" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td  width="25%"></td>
                                    <td  width="25%"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" valign="top">
                                        <asp:GridView ID="gvStudentRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("eqs_id") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Join Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("STU_DOJ", "{0:dd/MMM/yyyy}") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                       <asp:Label ID="lblJOINDTH" runat="server" Text="Join Date" CssClass="gridheader_text" ></asp:Label>
                                                        <br />
                                                       <asp:TextBox ID="txtJOINDT" runat="server" Width="75%" ></asp:TextBox>
                                                       <asp:ImageButton ID="btnSearchJOINDT" OnClick="btnSearchJOINDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="middle" ></asp:ImageButton>
                                                       
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblJOINTDT" runat="server" Text='<%# Bind("STU_DOJ", "{0:dd/MMM/yyyy}") %>' ></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("STU_DOJ", "{0:dd/MMM/yyyy}") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblSTUD_IDH" runat="server" Text="Student ID" CssClass="gridheader_text" ></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtSTUD_ID" runat="server" Width="75%" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStud_ID" OnClick="btnSearchStud_ID_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="middle" ></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTUD_ID" runat="server" Text='<%# Bind("STU_No") %>' ></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("STU_No") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStud_NameH" runat="server" Text="Student Name" CssClass="gridheader_text" ></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtStud_Name" runat="server" Width="75%" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStud_Name" OnClick="btnSearchStud_Name_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="middle" ></asp:ImageButton>
                                                                                
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblStud_Name" OnClick="lblStud_Name_Click" runat="server" Text='<%# Bind("STU_PASPRTNAME") %>' OnClientClick='<%# GetNavigateUrl(Eval("STU_BSU_ID").tostring(),Eval("Stu_ID").tostring()) %>'  CommandName="Select"></asp:LinkButton>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("SName") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblGradeH" runat="server" Text="Grade Section" CssClass="gridheader_text" ></asp:Label></td>
                                                        <br />
                                                        <asp:TextBox ID="txtStud_Grade" runat="server" Width="75%" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchGrade" OnClick="btnSearchGrade_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="middle" ></asp:ImageButton>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                                        <asp:Label ID="lblGradeH" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' ></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pending Documents">
                                                    <HeaderTemplate>
                                                      Documents
                                                      <br />
                                                        <asp:DropDownList ID="ddlgvDoc" runat="server" CssClass="listbox" AutoPostBack="True" OnSelectedIndexChanged="ddlgvDoc_SelectedIndexChanged">
                                                                            <asp:ListItem>ALL</asp:ListItem>
                                                                            <asp:ListItem>YES</asp:ListItem>
                                                                            <asp:ListItem>NO</asp:ListItem>
                                                                        </asp:DropDownList>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgDoc" runat="server" ImageUrl='<%# BIND("pendingDoc") %>' HorizontalAlign="Center"></asp:Image>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Age" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPPL_DATE" runat="server" Text='<%# bind("Appl_Age") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEditH" runat="server" Text="Edit" ></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lblEdit" OnClick="lblEdit_Click" runat="server" >Edit</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Services" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblService" OnClick="lblService_Click" runat="server" >Services</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Print">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lblPrint" runat="server" OnClick="lblPrint_Click">Print</asp:LinkButton>

                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("stu_id") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudentID" runat="server" Text='<%# Bind("stu_id") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>

