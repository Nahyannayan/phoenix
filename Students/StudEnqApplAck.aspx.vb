Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Imports EmailService
Imports System.Collections.Generic
Imports System.Collections
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.Color
Imports iTextSharp.text.html.simpleparser

Partial Class Students_StudEnqApplAck
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                'Dim siteUrl As String = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "")
                Dim filepath As String = String.Empty
                Dim EnqCode As String = String.Empty
                Dim html As String = ScreenScrapeHtml(Server.MapPath("~\Students\StudEnqApplAck.html"))
                Dim msg As New MailMessage
                Dim temp_ID As String = String.Empty
                Dim temp_Grade As String = String.Empty
                Dim temp_Acd As String = String.Empty
                Dim temp_STREET As String = String.Empty
                Dim temp_AREA As String = String.Empty
                Dim temp_BLDG As String = String.Empty
                Dim temp_APART As String = String.Empty
                Dim classlabel As String = String.Empty
                Dim temp_Shift As String = String.Empty
                Dim temp_Stream As String = String.Empty
                Dim Comm1 As String = String.Empty
                Dim Comm2 As String = String.Empty
                Dim Curri As String = String.Empty
                Dim BSU_Name As String = String.Empty
                Dim City As String = String.Empty
                Dim Country As String = String.Empty
                Dim Phone As String = String.Empty
                Dim Email As String = String.Empty
                Dim Grade As String = String.Empty
                Dim POBox As String = String.Empty
                Dim WardName As String = String.Empty
                Dim ParentName As String = String.Empty
                Dim bParentLogin As Boolean
                Dim DOB As String = String.Empty
                Dim BSU_ID As String = String.Empty
                Dim StrPath As String = "https://school.gemsoasis.com/"
                Dim bFileWriten As Boolean = True
                spTime.InnerHtml = "<div>" & Replace(String.Format("{0:" & OASISConstants.DateFormat & "}", Date.Today), "/", "-") & " </div><div> " & DateTime.Now.ToShortTimeString & "</div>" 'ToString("hh:mm tt")
                html = html.Replace("@ENQDATE", " <strong>" & String.Format("{0:" & OASISConstants.DateFormat & "}", Date.Today) & "</strong>")

                html = html.Replace("@ENQHOUR", "<b>" & DateTime.Now.ToShortTimeString & " </b>")
                'JC enq
                'Session("TEMP_ApplNo") = 7398
                'Session("enq_id") = 2716062
                'GWA enq--with payment details
                'Session("TEMP_ApplNo") = 2407
                'Session("enq_id") = 2514458

                'GWA enq--with out payment
                ' Session("TEMP_ApplNo") = 2657
                'Session("enq_id") = 2521380
                'most details enq--with out payment
                'Session("TEMP_ApplNo") = 1693
                'Session("enq_id") = 2553317

                ' eqm_enqid()
                'Session("chkemailSend") = True

                'virtual school enq--with out payment
                'Session("TEMP_ApplNo") = 10933
                'Session("enq_id") = 2704196



                If Session("TEMP_ApplNo") Is Nothing Then
                    Session("TEMP_ApplNo") = "0"
                End If

                If Session("Enq_ID") Is Nothing Then
                    Session("Enq_ID") = "0"
                End If

                If Not Request.QueryString("Eid") Is Nothing Then
                    Session("Enq_ID") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))
                    bParentLogin = True
                End If
                If Not Request.QueryString("Aid") Is Nothing Then
                    Session("TEMP_ApplNo") = Encr_decrData.Decrypt(Request.QueryString("Aid").Replace(" ", "+"))
                    bParentLogin = True
                End If
                If Not Request.QueryString("reg") Is Nothing Then
                    hfReg.Value = Encr_decrData.Decrypt(Request.QueryString("reg").Replace(" ", "+"))

                End If

                If Not Request.QueryString("vid") Is Nothing Then
                    hfVid.Value = Encr_decrData.Decrypt(Request.QueryString("vid").Replace(" ", "+"))
                    If hfVid.Value = "S100070" Or hfVid.Value = "S100067" Then
                        lbtnBack.Visible = False
                        spAppl.Visible = False
                    Else
                        lbtnBack.Visible = True
                        spAppl.Visible = True
                    End If
                End If



                Dim panelState As Integer
                Dim conn As String = ConnectionManger.GetOASISConnectionString
                Dim Footer2string As String = String.Empty
                Dim param(2) As SqlParameter
                param(0) = New SqlParameter("@ENQID", Session("Enq_ID"))
                param(1) = New SqlParameter("@APPLNO", Session("TEMP_ApplNo"))

                Using readerEnq_Ack_MAIN As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "STU.GETENQ_ACK_MAIN", param)
                    If readerEnq_Ack_MAIN.HasRows = True Then
                        While readerEnq_Ack_MAIN.Read
                            ltMain.Text = " <table style='top=0px;margin-top=0px;'><tr><td><b>" & Convert.ToString(readerEnq_Ack_MAIN("EAS_TEXT_MAIN")) & "</b></td></tr></table>"
                            ltFooter.Text = " <table style='top=0px;margin-top=0px;'><tr><td><b>" & Convert.ToString(readerEnq_Ack_MAIN("EAS_FOOTER_MAIN")) & "</b></td></tr></table>"
                            lt_Footer3.Text = Convert.ToString(readerEnq_Ack_MAIN("EAS_FOOTER_MAIN3"))
                            ltAcknow.Text = Convert.ToString(readerEnq_Ack_MAIN("EAS_FOOTER_ACKNOW"))
                            html = html.Replace("@Footer1", " <table style='top=0px;margin-top=0px;' border='0'><tr><td><b>" & Convert.ToString(readerEnq_Ack_MAIN("EAS_FOOTER_MAIN")) & "</b></td></tr></table>")

                            If Convert.ToString(readerEnq_Ack_MAIN("EAS_FOOTER_MAIN2")) = "" Then
                                ltFooter2.Visible = False
                                html = html.Replace("@Footer2", "")
                            Else
                                ltFooter2.Visible = True
                                ltFooter2.Text = "<div style='display:inline;float:left;margin-top:6px;padding-top:6px;'><b>" & Convert.ToString(readerEnq_Ack_MAIN("EAS_FOOTER_MAIN2")) & "</b></div>"
                                html = html.Replace("@Footer2", "<div style='display:inline;float:left;margin-top:6px;padding-top:6px;'><b>" & Convert.ToString(readerEnq_Ack_MAIN("EAS_FOOTER_MAIN2")) & "</b></div>")
                            End If

                            If Convert.ToString(readerEnq_Ack_MAIN("EAS_FOOTER_ACKNOW")) = "" Then

                                html = html.Replace("@ACKNOW", "")
                            Else
                                html = html.Replace("@ACKNOW", Convert.ToString(readerEnq_Ack_MAIN("EAS_FOOTER_ACKNOW")))
                            End If



                            html = html.Replace("@MAINMSG", "<table style='top=0px;margin-top=0px;'><tr><td><b>" & Convert.ToString(readerEnq_Ack_MAIN("EAS_TEXT_MAIN")) & "</b></td></tr></table>")

                        End While
                    Else
                        panelState = 1
                        html = html.Replace("@MAINMSG", "")
                        ltMain.Visible = False
                    End If

                End Using


                Dim ds As DataSet = AccessStudentClass.GetEnq_Ack_SUB(Session("TEMP_ApplNo"), Session("Enq_ID"))
                Dim Str_doc As String = String.Empty
                Dim i As Integer = 0
                If ds.Tables(0).Rows.Count > 0 Then
                    While i <= ds.Tables(0).Rows.Count - 1
                        Str_doc = Str_doc & "<div style='display:block'>" & ds.Tables(0).Rows(i).Item("EAD_TEXT").ToString & "</div>"
                        If ds.Tables(0).Rows(i).Item("EAS_bBULLET_POINTS") = True Then
                            ds.Tables(0).Rows(i)(0) = "<li style='list-style-type:disc;list-style-position:inside;'>" & ds.Tables(0).Rows(i)(0) & "</li>"

                        End If

                        i = i + 1
                    End While



                    dlSub.DataSource = ds.Tables(0)
                    dlSub.DataBind()
                    html = html.Replace("@SUBMSG", Str_doc)
                    Str_doc = String.Empty
                Else
                    dlSub.Visible = False
                    panelState = panelState + 1
                    html = html.Replace("@SUBMSG", "")
                End If


                If panelState = 2 Then
                    Panel1.Visible = False
                End If


                If (Session("Sibling_FeeID")) Is Nothing Then
                    lblSibName.Visible = False
                    html = html.Replace("@SIB_ID", "")
                Else
                    lblSibName.Text = Session("Sibling_FeeID")
                    html = html.Replace("@SIB_ID", Session("Sibling_FeeID"))
                End If
                Dim logPath As String = String.Empty






                Using readerEnquiry_ApplAck As SqlDataReader = GetEnq_ACK(Session("TEMP_ApplNo"), Session("Enq_ID"))
                    If readerEnquiry_ApplAck.HasRows = True Then
                        While readerEnquiry_ApplAck.Read
                            temp_ID = Session("TEMP_ApplNo") 'Convert.ToString(readerEnquiry_ApplAck("ENQID"))
                            WardName = Convert.ToString(readerEnquiry_ApplAck("APPLPASSPORTNAME"))
                            ParentName = Convert.ToString(readerEnquiry_ApplAck("FNAME"))
                            City = Convert.ToString(readerEnquiry_ApplAck("City"))

                            Country = Convert.ToString(readerEnquiry_ApplAck("Country"))
                            If StrConv(Country, VbStrConv.Lowercase) = "uae" Then
                                Country = Country
                            Else
                                Curri = StrConv(Country, VbStrConv.ProperCase)
                            End If
                            Phone = Convert.ToString(readerEnquiry_ApplAck("FMOBILE"))
                            Email = Convert.ToString(readerEnquiry_ApplAck("FEMAIL"))
                            'BSU_Name = Convert.ToString(readerEnquiry_ApplAck("BSU_NAME"))
                            temp_Grade = Convert.ToString(readerEnquiry_ApplAck("GRM_DISPLAY"))
                            'temp_Acd = Convert.ToString(readerEnquiry_ApplAck("ACY_DESCR"))
                            Grade = Convert.ToString(readerEnquiry_ApplAck("GRM_DISPLAY"))
                            POBox = Convert.ToString(readerEnquiry_ApplAck("COMPOBOX"))
                            DOB = Convert.ToString(readerEnquiry_ApplAck("EQS_DOJ"))
                            temp_STREET = Convert.ToString(readerEnquiry_ApplAck("EQP_FCOMSTREET"))
                            temp_AREA = Convert.ToString(readerEnquiry_ApplAck("EQP_FCOMAREA"))
                            temp_BLDG = Convert.ToString(readerEnquiry_ApplAck("EQP_FCOMBLDG"))
                            temp_APART = Convert.ToString(readerEnquiry_ApplAck("EQP_FCOMAPARTNO"))

                            temp_Shift = Convert.ToString(readerEnquiry_ApplAck("SHF_DESCR"))
                            temp_Stream = Convert.ToString(readerEnquiry_ApplAck("STM_DESCR"))
                            Curri = Convert.ToString(readerEnquiry_ApplAck("CLM_DESCR"))
                            If Curri.Length > 5 Then
                                Curri = StrConv(Curri, VbStrConv.ProperCase)
                            Else
                                Curri = StrConv(Curri, VbStrConv.Uppercase)
                            End If
                            If Curri = "Cbse/Cbse I" Then
                                Curri = "CBSE/CBSE i"
                            End If
                            Session("TEMP_BSU_ID") = Convert.ToString(readerEnquiry_ApplAck("BSU_ID"))
                            CUSTOM_LABEL(Convert.ToString(readerEnquiry_ApplAck("BSU_ID")), classlabel)

                            If Session("TEMP_BSU_ID") = "133006" Then

                                html = html.Replace("@FontRED", " ")
                                bold1.Visible = False
                            Else
                                html = html.Replace("@FontRED", lt_Footer3.Text)
                                bold1.Visible = True

                            End If

                            If Convert.ToString(readerEnquiry_ApplAck("ACY_DESCR")) = "2011-2012" And Session("TEMP_BSU_ID") = "114003" Then
                                ltHeadmain.Text = " You have enquired for admission in <b > " & "" & "GEMS American Academy - Khalifa City, Abu Dhabi " & Convert.ToString(readerEnquiry_ApplAck("Country")) & ", </b>" & _
                           " following the <b >" & Curri & " </b> curriculum for the <b > academic year " & Convert.ToString(readerEnquiry_ApplAck("ACY_DESCR")) & "</b>"
                                html = html.Replace("@HEADER", ltHeadmain.Text)
                            ElseIf Convert.ToString(readerEnquiry_ApplAck("ACY_DESCR")) = "2012-2013" And Session("TEMP_BSU_ID") = "123004" And temp_Grade = "Pre KG" Then
                                ltHeadmain.Text = " You have enquired for admission to<b> Pre-kindergarten at the Modern�s Nursery  in <b > " & Convert.ToString(readerEnquiry_ApplAck("BSU_NAME")) & " , " & Convert.ToString(readerEnquiry_ApplAck("Country")) & ", </b>" & _
                          " for the <b> academic year " & Convert.ToString(readerEnquiry_ApplAck("ACY_DESCR")) & "</b>"
                                html = html.Replace("@HEADER", ltHeadmain.Text)


                            Else
                                ltHeadmain.Text = " You have enquired for admission in <b > " & Convert.ToString(readerEnquiry_ApplAck("BSU_NAME")) & " , " & Convert.ToString(readerEnquiry_ApplAck("Country")) & ", </b>" & _
                            " following the <b >" & Curri & " </b> curriculum for the <b > academic year " & Convert.ToString(readerEnquiry_ApplAck("ACY_DESCR")) & "</b>"
                                html = html.Replace("@HEADER", ltHeadmain.Text)
                            End If


                            spReg.InnerText = IIf(Session("TEMP_BSU_ID") = "115005", Convert.ToString(readerEnquiry_ApplAck("ENAME")), StrConv(Convert.ToString(readerEnquiry_ApplAck("ENAME")), VbStrConv.ProperCase))
                            html = html.Replace("@REGNAME", IIf(Session("TEMP_BSU_ID") = "115005", Convert.ToString(readerEnquiry_ApplAck("ENAME")), StrConv(Convert.ToString(readerEnquiry_ApplAck("ENAME")), VbStrConv.ProperCase)))
                            liRegMail.Text = "<a href='mailto:" & Convert.ToString(readerEnquiry_ApplAck("Email")) & "?subject=Online Enquiry Acknowledgement' style='color:black;'>" & Convert.ToString(readerEnquiry_ApplAck("Email")) & "</a><div>" & Convert.ToString(readerEnquiry_ApplAck("REG_INFO")) & "</div>"

                            html = html.Replace("@REGMAIL", "<a href='mailto:" & Convert.ToString(readerEnquiry_ApplAck("Email")) & "?subject=Online Enquiry Acknowledgement' style='color:black;'>" & Convert.ToString(readerEnquiry_ApplAck("Email")) & "</a><div>" & Convert.ToString(readerEnquiry_ApplAck("REG_INFO")) & "</div>")

                            liAddr.Text = "<font size='1pt'  face='Arial'>PO Box " & Convert.ToString(readerEnquiry_ApplAck("BSU_POBOX")) & " , " & Convert.ToString(readerEnquiry_ApplAck("BSU_ADDRESS")) & " , " & _
                             Convert.ToString(readerEnquiry_ApplAck("BSU_CITY")) & " , " & Convert.ToString(readerEnquiry_ApplAck("CTY_DESCR")) & " | " & "Tel +" & Convert.ToString(readerEnquiry_ApplAck("BSU_TEL")) & " |  Fax  +" & _
                            Convert.ToString(readerEnquiry_ApplAck("BSU_FAX")) & " | " & "<a href='http://" & Convert.ToString(readerEnquiry_ApplAck("BSU_URL")) & "' style='color:black;'>" & Convert.ToString(readerEnquiry_ApplAck("BSU_URL")).TrimEnd("/").TrimEnd("\") & " </a></font>"
                            Session("temp_SADDR") = "PO Box " & Convert.ToString(readerEnquiry_ApplAck("BSU_POBOX")) & " , " & Convert.ToString(readerEnquiry_ApplAck("BSU_ADDRESS")) & " , " & _
                              Convert.ToString(readerEnquiry_ApplAck("BSU_CITY")) & " , " & Convert.ToString(readerEnquiry_ApplAck("CTY_DESCR")) & " | " & "Tel +" & Convert.ToString(readerEnquiry_ApplAck("BSU_TEL")) & " |  Fax  +" & _
                             Convert.ToString(readerEnquiry_ApplAck("BSU_FAX")) & " | " & Convert.ToString(readerEnquiry_ApplAck("BSU_URL")).TrimEnd("/").TrimEnd("\") & ""
                            imglogo.ImageUrl = StrPath + Replace(Replace(Replace(Convert.ToString(readerEnquiry_ApplAck("BSU_MOE_LOGO")), "..\", ""), "../", ""), "~\", "")
                            Session("temp_logoPath") = "~\" + Replace(Replace(Replace(Replace(Convert.ToString(readerEnquiry_ApplAck("BSU_MOE_LOGO")), "..\", ""), "../", ""), "~\", ""), "~/", "").TrimStart("\").TrimStart("/")

                            logPath = Convert.ToString(readerEnquiry_ApplAck("BSU_MOE_LOGO"))


                        End While
                    End If

                End Using




                imgBarCode.ImageUrl = "https://school.gemsoasis.com/students/barcodeEnq.aspx?enqNo=" & Session("TEMP_ApplNo")




                lblSibName.Text = UCase(Session("Sibling_FeeID"))
                If Trim(WardName) <> "" Then
                    html = html.Replace("@WNAME", UCase(WardName))
                    lblWardName.Text = UCase(WardName)
                Else
                    html = html.Replace("@WNAME", "")
                    lblWardName.Visible = False
                End If

                If Trim(ParentName) <> "" Then
                    html = html.Replace("@PNAME", UCase(ParentName))
                    lblParent.Text = UCase(ParentName)
                Else
                    html = html.Replace("@PNAME", "")
                    lblParent.Visible = False
                End If


                If Trim(City) <> "" Then
                    html = html.Replace("@CITY", UCase(City))
                    lblCity.Text = UCase(City)
                Else
                    html = html.Replace("@CITY", "")
                    lblCity.Visible = False
                End If



                If Trim(DOB) <> "" Then
                    lblDOB.Text = UCase(DOB)
                    html = html.Replace("@DOB", UCase(DOB))
                End If

                If Trim(Country) <> "" Then
                    html = html.Replace("@COUNTRY", UCase(Country))
                    lblCountry.Text = UCase(Country)
                Else
                    html = html.Replace("@COUNTRY", "")
                    lblCountry.Visible = False
                End If


                If Trim(Phone) <> "" Then
                    html = html.Replace("@PHONE", UCase(Phone))
                    lblPhone.Text = UCase(Phone)
                Else
                    html = html.Replace("@PHONE", "")
                    lblPhone.Visible = False
                End If


                If Trim(Email) <> "" Then
                    html = html.Replace("@EMAIL", Email)
                    lblEmail.Text = Email
                Else
                    html = html.Replace("@EMAIL", "")
                    lblEmail.Visible = False
                End If



                html = html.Replace("@CLASS", UCase(temp_Grade))
                html = html.Replace("##CLASSLB##", classlabel)

                lblClassYearGrade.Text = UCase(temp_Grade)

                Session("Temp_Display_ENQ") = UCase(temp_Grade) & "/" & temp_ID
                lblEnqNo.Text = UCase(temp_Grade) & "/" & temp_ID
                EnqCode = UCase(temp_Grade).TrimEnd & "-" & temp_ID
                If Session("chkemailSend") Is Nothing Then
                    Session("chkemailSend") = True
                End If

                HTMLToPdf(html.ToString, EnqCode, filepath, bFileWriten)
                If bFileWriten = True Then
                    'hfFilePath.Value = filepath
                    hrefAck.HRef = filepath
                Else
                    hfFilePath.Value = ""
                End If

                hfFileName.Value = "OnlineEnquiryAck-" & EnqCode.Replace(" ", "") & ".pdf"

                If Session("chkemailSend") = True Then


                    Dim BSC_HOST As String = String.Empty
                    Dim BSC_USERNAME As String = String.Empty
                    Dim BSC_PASSWORD As String = String.Empty
                    Dim BSC_PORT As String = String.Empty
                    Dim BSC_TYPE As String = String.Empty
                    Dim MailContent As New StringBuilder
                    Dim EMAILSTATUS As String = String.Empty
                    Using EmailHost_reader As SqlDataReader = AccessStudentClass.GetEMAIL_HOST(Session("TEMP_BSU_ID"))
                        If EmailHost_reader.HasRows Then

                            While EmailHost_reader.Read

                                BSC_HOST = Convert.ToString(EmailHost_reader("BSC_HOST"))
                                BSC_USERNAME = Convert.ToString(EmailHost_reader("BSC_USERNAME"))
                                BSC_PASSWORD = Convert.ToString(EmailHost_reader("BSC_PASSWORD"))
                                BSC_PORT = Convert.ToString(EmailHost_reader("BSC_PORT"))
                                BSC_TYPE = Convert.ToString(EmailHost_reader("BSC_TYPE"))


                            End While
                        End If


                    End Using


                    '  SendPlainTextEmails(BSC_USERNAME, Email, "Online Enquiry Acknowledgement", html.ToString, BSC_USERNAME, BSC_PASSWORD, BSC_HOST, BSC_PORT, "0", False)
                    MailContent.Append("<table border='0' style=' font-family: Verdana;font-size: 12pt;color: #003a63;'>")
                    MailContent.Append("<tr><td  >Dear Parent , </td></tr>")
                    MailContent.Append("<tr><td ><br />Please find the attached Online Enquiry Acknowledgement Letter (Enquiry No :" & Session("Temp_Display_ENQ") & ") of your child.<br /><br /><br /></td></tr>")
                    MailContent.Append("<tr><td >Yours sincerely, </td></tr>")
                    MailContent.Append("<tr><td><br /><br /></td></tr>")
                    MailContent.Append("<tr><td >" & spReg.InnerText & " </td></tr>")
                    MailContent.Append("<tr><td><br /><br /></td></tr>")
                    'MailContent.Append("<tr><td >On Behalf of Principal/CEO </td></tr>")

                    MailContent.Append("</table>")

                    ' SendPDFEmails( FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String

                    EMAILSTATUS = SendPDFEmails(BSC_USERNAME, Email, " Online Enquiry Acknowledgement -" & EnqCode, MailContent.ToString, BSC_USERNAME, BSC_PASSWORD, BSC_HOST, BSC_PORT, hfFilePath.Value, True)
                    AuditEnquiryEmail(Session("TEMP_BSU_ID"), Session("TEMP_ApplNo"), Session("Enq_ID"), EMAILSTATUS)

                    Session("chkemailSend") = False
                End If

                SchoolVisit(Email)

            Catch ex As Exception
                'lblError.Text = "Error while processing your request"
            End Try
      
        End If
    End Sub
    Public Sub AuditEnquiryEmail(ByVal BSU_ID As String, ByVal ApplNo As String, ByVal EqmEnqId As String, _
                                          ByVal EmailStatus As String)
        Try

            Dim conn As String = ConnectionManger.GetOASISAuditConnectionString
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@BSU_ID", BSU_ID)
            param(1) = New SqlParameter("@ApplNo", ApplNo)
            param(2) = New SqlParameter("@EqmEnqId", EqmEnqId)
            param(3) = New SqlParameter("@EmailStatus", EmailStatus)

            SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "ENQ.SAVEOnlineEnquiryEmailLog", param)

        Catch ex As Exception

        End Try

    End Sub
    Public Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml
    Private Sub CUSTOM_LABEL(ByVal bsu_id As String, ByRef classlabel As String)


        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", bsu_id)
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "GETENQUIRY_CUSTOM_LABEL", PARAM)
            If DATAREADER.HasRows Then
                While DATAREADER.Read
                    ltClass_lb.Text = "Class/Year/" & Convert.ToString(DATAREADER("ECL_GRADE"))
                    classlabel = "Class/Year/" & Convert.ToString(DATAREADER("ECL_GRADE"))
                End While

            Else

                ltClass_lb.Text = "Class/Year/Grade"
                classlabel = "Class/Year/Grade"

            End If

        End Using


    End Sub
    Private Function GetEnq_ACK(ByVal EQS_APPLNO As String, ByVal EQS_EQM_ENQID As String) As SqlDataReader

        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim pParms(9) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_APPLNO", EQS_APPLNO)
        pParms(1) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", EQS_EQM_ENQID)

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ENQ.GETENQUIRY_ACK", pParms)

        SqlConnection.ClearPool(conn)
        Return reader
    End Function

    Private Sub SchoolVisit(ByVal Email As String)
        Try
            Session("SchoolVisitFlag") = "Processed"
            Dim html As String = ScreenScrapeHtml(Server.MapPath("~\Students\MailContent.htm"))
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim PAR_EMAIL_ID As String = String.Empty
            Dim ENCR_PAR_PASS As String = String.Empty
            Dim ENQ_bONLINE As Boolean
            Dim ACK_POPUP_TEXT As String = String.Empty
            Dim ACK_MAIL_TEXT As String = String.Empty
            Dim BSC_HOST As String = String.Empty
            Dim BSC_FROMEMAIL As String = String.Empty
            Dim BSC_PASSWORD As String = String.Empty
            Dim BSC_PORT As String = String.Empty
            Dim BSC_TYPE As String = String.Empty
            Dim SITE_ADDR As String = String.Empty
            Dim Email_status As String = String.Empty
            Dim TRACK_ID As String = String.Empty

            Dim oldPasswordEnrc As String = getOldPassword(Email)
            Dim oldPassword As String = String.Empty
            If oldPasswordEnrc.Trim <> "" Then
                oldPassword = Encr_decrData.Decrypt(oldPasswordEnrc)
            End If
            Dim New_pass_enrc As String = Encr_decrData.Encrypt("enq" & Convert.ToString(CInt(Session("TEMP_ApplNo"))))
            Dim param(8) As SqlParameter
            param(0) = New SqlClient.SqlParameter("@EQM_ENQID", Session("enq_id"))
            param(1) = New SqlClient.SqlParameter("@EQS_APPLNO", Session("TEMP_ApplNo"))
            param(2) = New SqlClient.SqlParameter("@BSU_ID", Session("TEMP_BSU_ID"))
            param(3) = New SqlClient.SqlParameter("@ENCR_ID", Encr_decrData.Encrypt(Email))
            param(4) = New SqlClient.SqlParameter("@old_pass", IIf(oldPassword.Trim = "", System.DBNull.Value, oldPassword))
            param(5) = New SqlClient.SqlParameter("@old_pass_enrc", IIf(oldPasswordEnrc.Trim = "", System.DBNull.Value, oldPasswordEnrc))
            param(6) = New SqlClient.SqlParameter("@New_pass_enrc", IIf(New_pass_enrc.Trim = "", System.DBNull.Value, New_pass_enrc))
            Using SchoolVisitReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV.GET_SAVE_ENQ_PARENT_LOGIN_NEW", param)
                If SchoolVisitReader.HasRows Then
                    While SchoolVisitReader.Read
                        PAR_EMAIL_ID = Convert.ToString(SchoolVisitReader("EPLM_EMAIL_ID"))
                        ENCR_PAR_PASS = Convert.ToString(SchoolVisitReader("EPLM_PASSWORD"))
                        ENQ_bONLINE = Convert.ToBoolean(SchoolVisitReader("bENQ_ONLINE"))
                        ACK_POPUP_TEXT = Convert.ToString(SchoolVisitReader("ACK_POPUP_TEXT"))
                        SITE_ADDR = Convert.ToString(SchoolVisitReader("SITE_ADDR"))

                    End While
                End If
            End Using

        Catch ex As Exception

        End Try

    End Sub
    Private Function getOldPassword(ByVal Email As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim password As String = String.Empty
        Dim param(3) As SqlParameter
        param(0) = New SqlClient.SqlParameter("@Email", Email)
        Using OldPasswordReader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "SV_ENQ.GetCurrent_Password", param)
            If OldPasswordReader.HasRows Then
                While OldPasswordReader.Read
                    password = Convert.ToString(OldPasswordReader("EPLM_PASSWORD"))
                End While
            End If
        End Using
        getOldPassword = password
    End Function

   


    Private Function SendPDFEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, _
                                   ByVal MailBody As String, ByVal Username As String, ByVal password As String, _
                                   ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, _
                                   ByVal HasAttachments As Boolean) As String

        Dim ReturnValue As String = ""
        Dim client As New System.Net.Mail.SmtpClient(Host, Port)
        Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)

        Try

            msg.Subject = Subject
            msg.Body = MailBody
            msg.Priority = Net.Mail.MailPriority.High
            msg.IsBodyHtml = True
            If Templateid = "" Then
                HasAttachments = False
            End If
            '' If Attachments.
            If HasAttachments Then
                Dim attach As New System.Net.Mail.Attachment(Templateid)
                msg.Attachments.Add(attach)
            End If
            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            client.Send(msg)
            client = Nothing
            msg.Dispose()
            ReturnValue = "Successfully sent to " & ToEmailId
        Catch ex As Exception
            ReturnValue = "Error in Sending Mail :" & ToEmailId & " " & ex.Message
            client = Nothing
            msg.Dispose()
        End Try
        Return ReturnValue
    End Function

    Protected Sub lbClickHere_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbClickHere.Click
        Try
            Dim url As String
            'ViewState("datamode") = "view"
            'ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            'ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            'Dim Eid As String = hfACD_ID.Value
            'Eid = Encr_decrData.Encrypt(Eid)

            url = String.Format("studEnqForm.aspx")
            Response.Redirect(url)
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub lbtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnBack.Click

        Try
            Session("Applic") = "0"

            If hfVid.Value = "S100070" Or hfVid.Value = "S100067" Then
                Response.Redirect("~\Students\studEnqForm_Reg.aspx")
            Else
                Response.Redirect("~\Students\studEnqForm.aspx")
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
        End Try
    End Sub
    Protected Sub lbApplic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbApplic.Click
        Try

            Dim url As String = String.Empty

            Dim MainMnu_code As String = "S100065"
            Dim strRedirect As String = "~\Students\studEnqForm.aspx"
            MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
            If Not Session("Registrar") Is Nothing Then
                If Session("Registrar") = "S100065" Then
                    url = String.Format("{0}?MainMnu_code={1}", strRedirect, MainMnu_code)
                    Session("Applic") = "1"
                Else
                    url = String.Format("~\Students\studEnqForm.aspx")
                    Session("Applic") = "1"
                End If
            Else
                url = String.Format("~\Students\studEnqForm.aspx")
                Session("Applic") = "1"
            End If
            Response.Redirect(url)
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
        End Try
    End Sub


    Public Sub HTMLToPdf(ByVal HTMLstr As String, ByVal EnqCode As String, ByRef DownLoadPath As String, ByRef bFileWriten As Boolean)

        Try

            Using myMemoryStream As New MemoryStream()
                Dim page As New pdfPage
                Dim document As New Document(PageSize.A4, 10, 10, 100, 65)
                Dim myPDFWriter As PdfWriter = PdfWriter.GetInstance(document, myMemoryStream)
                Dim PhyPath As String = Web.Configuration.WebConfigurationManager.AppSettings("EnqAckFilepath").ToString()
                Dim VirPath As String = Web.Configuration.WebConfigurationManager.AppSettings("EnqAckFilepathvirtual").ToString()

                Dim pathSave As String = "OnlineEnquiryAck-" & EnqCode.Replace(" ", "") & ".pdf"
                If Not Directory.Exists(PhyPath & "\" & Session("TEMP_BSU_ID") & "\") Then
                    ' Create the directory.
                    Directory.CreateDirectory(PhyPath & "\" & Session("TEMP_BSU_ID") & "\")
                End If




                Dim path = PhyPath & "\" & Session("TEMP_BSU_ID") & "\" & pathSave
                DownLoadPath = VirPath & "\" & Session("TEMP_BSU_ID") & "\" & pathSave
                myPDFWriter.PageEvent = page
                'document.Add(New Header(iTextSharp.text.html.Markup.HTML_ATTR_STYLESHEET, "Style.css"))
                document.Open()
                hfFilePath.Value = path
                ' Add to content to your PDF here...

                Dim htmlarraylist = HTMLWorker.ParseToList(New StringReader(HTMLstr), Nothing)
                For k As Integer = 0 To htmlarraylist.Count - 1
                    document.Add(DirectCast(htmlarraylist(k), IElement))
                Next

                ' We're done adding stuff to our PDF.
                document.Close()


                Try
                    Dim d As New DirectoryInfo(PhyPath & "\" & Session("TEMP_BSU_ID") & "\")

                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then
                        For Each f As System.IO.FileInfo In fi
                            If (Now - f.CreationTime).Hours > 5 Then
                                f.Delete()
                            End If
                        Next
                    End If
                Catch ex As Exception
                End Try

                Dim content As Byte() = myMemoryStream.ToArray()

                ' Write out PDF from memory stream.
                Using fs As FileStream = File.Create(path)
                    fs.Write(content, 0, CInt(content.Length))
                End Using



            End Using


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            bFileWriten = False
        End Try


    End Sub
    
   
End Class
Public Class pdfPage
    Inherits iTextSharp.text.pdf.PdfPageEventHelper
    ' This is the contentbyte object of the writer
    Private cb As PdfContentByte
    Private template As PdfTemplate
    ' Override the on OpenDocument method
    Private bf As BaseFont = Nothing
    Public Overrides Sub OnOpenDocument(ByVal writer As PdfWriter, ByVal document As Document)
        Try
            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
            cb = writer.DirectContent
            template = cb.CreateTemplate(50, 50)
        Catch de As DocumentException
        Catch ioe As System.IO.IOException
        End Try
    End Sub
    'override the OnStartPage event handler to add our header
    Public Overrides Sub OnStartPage(ByVal writer As PdfWriter, ByVal doc As Document)
        'PdfPtable with 3 column to position  header 
        Dim headerTbl As New PdfPTable(3)
        Dim cellwidth() As Integer = {30, 70, 30}
        'set the width of the table to be the same as the document
        headerTbl.SetWidths(cellwidth)
        headerTbl.TotalWidth = doc.PageSize.Width - 140


        'get school logo
        Dim logo As Image = Image.GetInstance(HttpContext.Current.Server.MapPath(HttpContext.Current.Session("temp_logoPath")))

        'reduce image size to 50 % of original size.
        logo.ScalePercent(50.0F)


        'set Font style for header text
        Dim titleColor = FontFactory.GetFont("verdana Arial Helvetica sans-serif", 13, Font.BOLD, New Color(3, 87, 141))
        Dim datetimefont = FontFactory.GetFont("verdana Arial Helvetica sans-serif", 7, Font.NORMAL, New Color(0, 0, 0))

        'Create a paragraph that contains the header text
        Dim headertitle As New Paragraph
        headertitle.Add(New Phrase("Online Enquiry Acknowledgement", titleColor))
        Dim headerdatetime As New Paragraph
        headerdatetime.Add(New Phrase(DateTime.Today.ToString("dd-MMM-yyyy"), datetimefont))
        headerdatetime.Add(Environment.NewLine)
        headerdatetime.Add(New Phrase(DateTime.Now.ToShortTimeString, datetimefont))


        'create instance of a table cell to contain date and time
        Dim cell As New PdfPCell(headerdatetime)
        cell.HorizontalAlignment = Element.ALIGN_LEFT
        cell.VerticalAlignment = Element.ALIGN_MIDDLE
        'add a bit of padding to bring it away from the right edge
        cell.PaddingLeft = 2
        cell.UseVariableBorders = True
        cell.BorderColorBottom = New iTextSharp.text.Color(0, 58, 99)
        cell.BorderWidthLeft = 0.0F
        cell.BorderWidthRight = 0.0F
        cell.BorderWidthTop = 0.0F
        cell.BorderWidthBottom = 1.0F
        'Add the cell to the table
        headerTbl.AddCell(cell)

        'create instance of a table cell to contain title
        cell = New PdfPCell(headertitle)
        cell.HorizontalAlignment = Element.ALIGN_CENTER
        cell.VerticalAlignment = Element.ALIGN_MIDDLE
        cell.UseVariableBorders = True
        cell.BorderColorBottom = New iTextSharp.text.Color(0, 58, 99)
        cell.BorderWidthLeft = 0.0F
        cell.BorderWidthRight = 0.0F
        cell.BorderWidthTop = 0.0F
        cell.BorderWidthBottom = 1.0F
        'Add the cell to the table
        headerTbl.AddCell(cell)

        'align the logo to the right of the cell
        cell = New PdfPCell(logo)
        cell.HorizontalAlignment = Element.ALIGN_RIGHT
        cell.PaddingRight = 2
        cell.UseVariableBorders = True
        cell.BorderColorBottom = New iTextSharp.text.Color(0, 58, 99)
        cell.BorderWidthLeft = 0.0F
        cell.BorderWidthRight = 0.0F
        cell.BorderWidthTop = 0.0F
        cell.BorderWidthBottom = 1.0F
        cell.PaddingBottom = 8
        headerTbl.AddCell(cell)


        'continue with the table  with next row
        Dim enq_str As String = "Enquiry Number :" & HttpContext.Current.Session("Temp_Display_ENQ")
        Dim EnqColor = FontFactory.GetFont("verdana Arial Helvetica sans-serif", 10, Font.BOLD, New Color(0, 0, 0))

        'create instance of a table cell to contain enquiry no
        cell = New PdfPCell(New Phrase(enq_str, EnqColor))
        'apply colspan for the enquiry number to be displayed
        cell.Colspan = 2
        cell.PaddingTop = 4
        cell.PaddingBottom = 4
        cell.PaddingLeft = 2
        cell.Border = 0
        cell.VerticalAlignment = Element.ALIGN_MIDDLE
        headerTbl.AddCell(cell)

        Dim code128 As New Barcode128()
        code128.CodeType = Barcode.CODE128
        code128.ChecksumText = True
        code128.GenerateChecksum = True
        code128.BarHeight = 30
        code128.X = 2
        code128.Code = HttpContext.Current.Session("TEMP_ApplNo")
        code128.AltText = ""


        'create instance of a table cell to contain bar code
        Dim EnqBarCode As Image = code128.CreateImageWithBarcode(cb, Nothing, Nothing)
        'Image.GetInstance(New Uri("https://school.gemsoasis.com/students/barcodeEnq.aspx?enqNo=" & HttpContext.Current.Session("TEMP_ApplNo") & "'"))
        '
        EnqBarCode.ScalePercent(50.0F)
        cell = New PdfPCell(EnqBarCode)
        cell.Border = 0
        cell.HorizontalAlignment = Element.ALIGN_RIGHT
        cell.PaddingTop = 4
        cell.PaddingRight = 0
        headerTbl.AddCell(cell)

        'write the rows out to the PDF output stream. 
        ' WriteSelectedRows (colStart As Integer,colEnd As Integer,rowStart As Integer, rowEnd As Integer, xPos As Single, yPos As Single, canvases As PdfContentByte()) As Single
        headerTbl.WriteSelectedRows(0, -1, doc.PageSize.GetLeft(68), doc.PageSize.GetTop(8), writer.DirectContent)

        ' write the image logo contain to contentbyte object of the writer at the bottom  right corner
        Dim pageSize As Rectangle = doc.PageSize
        Dim foot As iTextSharp.text.Image = Image.GetInstance(HttpContext.Current.Server.MapPath("~/Images/swoosh_small.png"))
        foot.ScalePercent(50.0F)
        foot.Alignment = Image.UNDERLYING
        'postion of absoluteX and absoluteY 
        foot.SetAbsolutePosition(pageSize.GetRight(183), pageSize.GetBottom(0))
        cb.AddImage(foot, True)


    End Sub
   
    Public Overrides Sub OnEndPage(ByVal writer As PdfWriter, ByVal document As Document)
        Try

            MyBase.OnEndPage(writer, document)

            Dim pageN As Integer = writer.PageNumber
            Dim text As String = HttpContext.Current.Session("temp_SADDR")
            'Dim pageSize As Rectangle = document.PageSize


            'Dim foot As iTextSharp.text.Image = Image.GetInstance(New Uri("https://school.gemsoasis.com/Images/swoosh_small.png"))
            'foot.ScalePercent(50.0F)
            ''foot.ScaleToFit(200.0F, 200.0F)
            'foot.Alignment = Image.UNDERLYING

            'foot.SetAbsolutePosition(pageSize.GetRight(183), pageSize.GetBottom(0))
            'cb.AddImage(foot, True)

            'Position the footer horizontal line
            'postion of absoluteX and absoluteY 
            cb.MoveTo(document.PageSize.GetLeft(60), document.PageSize.GetBottom(60))

            'draws a line from X point to the position specified Y
            cb.LineTo(530.0F, document.PageSize.GetBottom(60))
            cb.SetRGBColorStroke(0, 58, 99)
            'line is drawn
            cb.Stroke()
            'creating column on a canvas
            Dim ct As New ColumnText(cb)
            'two ways to add text to the column either through Chunk or Phrase of text and add it to the column:
            ' parameters of SetSimpleColumn :
            '1:the(Phrase)
            '2:leftmargin(coordinate)
            '3:bottommargin(coordinate)
            '4:box(width)
            '5:box(height)
            '6:line(height)
            '7:alignment
            ct.SetSimpleColumn(New Phrase(New Chunk(text, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL, New Color(0, 58, 99)))), _
                               60, 75, 530, 36, 25, Element.ALIGN_CENTER)
            ct.Go()



            ' cb.AddTemplate(template, pageSize.GetLeft(40) + 100, pageSize.GetBottom(30))

            'cb.BeginText()
            'cb.SetFontAndSize(bf, 8)
            'cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Printed On  10:10 AM", pageSize.GetRight(40), pageSize.GetBottom(0), 0)
            'cb.EndText()

        Catch ex As Exception

        End Try
    End Sub

    Public Overrides Sub OnCloseDocument(ByVal writer As PdfWriter, ByVal document As Document)
        MyBase.OnCloseDocument(writer, document)

        'template.BeginText()
        'template.SetFontAndSize(bf, 8)
        'template.SetTextMatrix(0, 0)
        'template.ShowText("" & Convert.ToString((writer.PageNumber - 1)))
        'template.EndText()
    End Sub

End Class
'Public Class CustomItextImageProvider
'    Implements iTextSharp.text.html.simpleparser.IImageProvider


'    Public Function GetImage(ByVal src As String, _
'                             ByVal imageProperties As Dictionary(Of String, String), ByVal cprops As ChainedProperties, _
'                             ByVal doc As IDocListener) As iTextSharp.text.Image
'        Dim imageLocation As String = imageProperties("src").ToString()
'        Dim siteUrl As String = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "")
'        If siteUrl.EndsWith("/") Then
'            siteUrl = siteUrl.Substring(0, siteUrl.LastIndexOf("/"))
'        End If
'        Dim image As iTextSharp.text.Image = Nothing
'        If Not imageLocation.StartsWith("http:") AndAlso Not imageLocation.StartsWith("file:") AndAlso Not imageLocation.StartsWith("https:") AndAlso Not imageLocation.StartsWith("ftp:") Then
'            imageLocation = siteUrl & (If(imageLocation.StartsWith("/"), "", "/")) & imageLocation
'        End If
'        image = iTextSharp.text.Image.GetInstance(imageLocation)
'        Return image
'    End Function


'End Class

