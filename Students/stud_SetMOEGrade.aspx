<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stud_SetMOEGrade.aspx.vb" Inherits="Students_stud_SetMOEGrade" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvMOEGrade.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state1(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }






        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
            document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
        }

        function test2(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid2()%>").src = path;
            document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
        }





    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Ministry Grade/Section
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <%-- <table width="100%" id="Table1" border="0">
                    <tr >
                        <td width="50%" align="left" class="title" style="height: 27px">&nbsp;Ministry Grade/Section</td>
                    </tr>
                </table>--%>
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False" ValidationGroup="groupM1"
                                 HeaderText="You must enter a value in the following fields:" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" colspan="2" width="50%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Select Grade</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" width="20%"><span class="field-label">Select Section</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlSection" runat="server">
                                                    </asp:DropDownList>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                                </td>

                                            </tr>
                                        </table>
                                    </td>
                                </tr>



                                <tr>
                                    <td>
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Ministry Grade</span></td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlMOE_Grade" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" width="20%"><span class="field-label">Minsitry Section</span></td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtMOE_Section" runat="server" MaxLength="10"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"></td>
                                                <td align="left"></td>
                                                <td align="left"></td>
                                                <td align="left"></td>

                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="gvMOEGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="40" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />

                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        select <br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state1(this);"
                                                            ToolTip="Click here to select/deselect all rows" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />

                                                    </ItemTemplate>

                                                    <HeaderStyle></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblStHeader" runat="server" Text="Student No"></asp:Label><br />
                                                        <asp:TextBox ID="txtStuNoSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStuNo_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <ItemStyle Width="50px"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblName" runat="server" Text="Student Name"></asp:Label><br />
                                                        <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MOE Grade/Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("MOE_Grade") %>'></asp:Label>

                                                    </ItemTemplate>

                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" TabIndex="4" ValidationGroup="groupM1" />

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>




                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfSCT_ID" runat="server" />
                <asp:HiddenField ID="hfACY_DESCR" runat="server" />

            </div>
        </div>
    </div>

</asp:Content>

