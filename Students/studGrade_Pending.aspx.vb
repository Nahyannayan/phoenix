Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studGrade_Att
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try



                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059030") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else


                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    Call bindAcademic_Year()


                    Session("Pending_Att") = CreateDataTable()
                    rbPending.Checked = True
                    callPending_Att()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))


    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAttendance.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAttendance.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAttendance.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAttendance.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            ddlAcademicYear.DataSource = ds.Tables(0)
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACD_ID"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
          

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim ds As New DataSet
            str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGrade.Items.Clear()

            ddlGrade.DataSource = ds.Tables(0)
            ddlGrade.DataTextField = "GRM_DISPLAY"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade.Items.Add(New ListItem("ALL", "ALL"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByValue("ALL").Selected = True

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAtt_Type()
        Try
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim GRD_ID As String = ddlGrade.SelectedItem.Value
            Dim CheckDate As String = txtDate.Text
            Dim ATT_Type As String
            'Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GetAtt_type(ACD_ID, GRD_ID, CheckDate)
            Dim ATT_Types As String = String.Empty
            Dim separator As String() = New String() {"|"}


            Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GetAtt_type(ACD_ID, CheckDate)
                While readerSTUD_ATT_ADD.Read
                    ATT_Types = ATT_Types + Convert.ToString(readerSTUD_ATT_ADD("ATT_TYPE")) + "|"
                End While
            End Using

            Dim strSplitArr As String() = ATT_Types.Split(separator, StringSplitOptions.RemoveEmptyEntries)

            For Each arrStr As String In strSplitArr

                If Array.IndexOf(strSplitArr, "Ses1&Ses2") >= 0 Then
                    ddlType.Items.Add(New ListItem("Session1", "Session1"))
                    ddlType.Items.Add(New ListItem("Session2", "Session2"))
                    Exit For
                Else
                    ddlType.Items.Add(New ListItem(arrStr, arrStr))
                End If

            Next


            'Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GetAtt_type(ACD_ID, CheckDate)
            '    If readerSTUD_ATT_ADD.HasRows Then
            '        While readerSTUD_ATT_ADD.Read
            '            ATT_Type = Convert.ToString(readerSTUD_ATT_ADD("ATT_TYPE"))
            '        End While
            '        If ATT_Type = "Ses1&Ses2" Then
            '            ddlType.Items.Add(New ListItem("Ses1&Ses2", "Ses1&Ses2"))
            '            ddlType.Items.Add(New ListItem("Session1", "Session1"))
            '            ddlType.Items.Add(New ListItem("Session2", "Session2"))
            '        Else
            '            ddlType.Items.Add(New ListItem(ATT_Type, ATT_Type))
            '        End If
            '    End If

            'End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub callPending_Att()

        If DateValidate() = 0 Then
            Try
                Dim ACD_ID As String = String.Empty
                If ddlAcademicYear.SelectedIndex = -1 Then
                    ACD_ID = ""
                Else
                    ACD_ID = ddlAcademicYear.SelectedItem.Value
                End If


                Dim GRD_ID As String = String.Empty

                Dim Att_Type As String = String.Empty


                Dim Att_date As String = "'" & txtDate.Text & "'"



                Dim ds As DataSet

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Session("Pending_Att").Rows.Clear()
                If rbPending.Checked = True Then
                    '                Dim str_Sql As String = " declare  @temptable table(GRM_DISPLAY varchar(20), SCT_DESCR varchar(20), EMP_NAME varchar(100), ATT_TYPE varchar(10), DISP_ORD int)" & _
                    '" insert @temptable (GRM_DISPLAY, SCT_DESCR, EMP_NAME, ATT_TYPE, DISP_ORD)" & _
                    '" select distinct GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY, SECTION_M.SCT_DESCR as SCT_DESCR," & _
                    '" ISNULL(EMPLOYEE_M.EMP_FNAME, '') + ' ' + ISNULL(EMPLOYEE_M.EMP_MNAME, '') + ' ' + ISNULL(EMPLOYEE_M.EMP_LNAME, '') AS Ename,  " & _
                    '" STAFF_AUTHORIZED_ATT.SAD_ATT_TYPE as ATT_TYPE, " & _
                    '" GRADE_M.GRD_DISPLAYORDER as DISPLAYORDER  FROM STAFF_AUTHORIZED_ATT INNER JOIN EMPLOYEE_M ON " & _
                    '" STAFF_AUTHORIZED_ATT.SAD_EMP_ID = EMPLOYEE_M.EMP_ID INNER JOIN  SHIFTS_M ON " & _
                    '" STAFF_AUTHORIZED_ATT.SAD_SHF_ID = SHIFTS_M.SHF_ID INNER JOIN  SECTION_M ON " & _
                    '" STAFF_AUTHORIZED_ATT.SAD_SCT_ID = SECTION_M.SCT_ID And STAFF_AUTHORIZED_ATT.SAD_GRD_ID = SECTION_M.SCT_GRD_ID " & _
                    '" AND  STAFF_AUTHORIZED_ATT.SAD_ACD_ID = SECTION_M.SCT_ACD_ID INNER JOIN  GRADE_BSU_M ON " & _
                    '" STAFF_AUTHORIZED_ATT.SAD_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
                    '" STAFF_AUTHORIZED_ATT.SAD_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN   GRADE_M ON " & _
                    '" GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
                    '"  where STAFF_AUTHORIZED_ATT.SAD_ACD_ID='" & ACD_ID & "' and '" & Att_date & "' between STAFF_AUTHORIZED_ATT.SAD_FROMDT and " & _
                    '"  isnull(STAFF_AUTHORIZED_ATT.SAD_TODT,'" & Att_date & "')and STAFF_AUTHORIZED_ATT.SAD_SCT_ID " & _
                    '"  not in (SELECT ALG_SCT_ID FROM  ATTENDANCE_LOG_GRADE   where ALG_ACD_ID='" & ACD_ID & "'  and  ALG_ATTDT ='" & Att_date & "' " & _
                    '"  and ALG_ATT_TYPE in(" & Att_Type & ")  and ALG_GRD_ID " & GRD_ID & ")  and STAFF_AUTHORIZED_ATT.SAD_ATT_TYPE in (" & Att_Type & ") " & _
                    '" and STAFF_AUTHORIZED_ATT.SAD_GRD_ID  " & GRD_ID & "  order by GRADE_M.GRD_DISPLAYORDER, SECTION_M.SCT_DESCR " & _
                    '" SELECT distinct GRM_DISPLAY,SCT_DESCR,ATT_TYPE, " & _
                    '" STUFF(EMP_LIST, 1, 1, '') AS EMP_NAME FROM @temptable AS D " & _
                    '" CROSS APPLY (SELECT  ',' +  EMP_NAME FROM @temptable AS DP    WHERE DP.SCT_DESCR = D.SCT_DESCR and D.ATT_TYPE =DP.ATT_TYPE " & _
                    '"  FOR XML PATH('')) AS P (EMP_LIST) "

                    If ddlGrade.SelectedItem.Text = "ALL" Then
                        GRD_ID = "<>''''"
                    Else
                        GRD_ID = "='" & ddlGrade.SelectedItem.Value & "'"
                    End If

                    If ddlType.SelectedIndex <> -1 Then
                        If ddlType.SelectedItem.Text = "Ses1&Ses2" Then
                            Att_Type = "'Session1'',''Session2'"
                        Else
                            Att_Type = "'" & ddlType.SelectedItem.Text & "'"
                        End If
                    End If
                    Att_date = "'" & txtDate.Text & "'"

                    ds = AccessStudentClass.GetPENDING_ATTENDANCE(ACD_ID, GRD_ID, Att_date, Att_Type)


                    If ds.Tables(0).Rows.Count > 0 Then


                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim rDt As DataRow
                            rDt = Session("Pending_Att").NewRow
                            'If temp_Grade = ds.Tables(0).Rows(i)("GRM_DISPLAY") Then
                            '    rDt("GRADE") = ""
                            'Else
                            '    rDt("GRADE") = ds.Tables(0).Rows(i)("GRM_DISPLAY")
                            '    temp_Grade = ds.Tables(0).Rows(i)("GRM_DISPLAY")
                            'End If
                            rDt("GRADE") = ds.Tables(0).Rows(i)("GRM_DISPLAY")
                            rDt("SECTION") = ds.Tables(0).Rows(i)("SCT_DESCR")
                            rDt("ATT_TYPE") = ds.Tables(0).Rows(i)("ATT_TYPE")
                            rDt("EMP_NAME") = ds.Tables(0).Rows(i)("EMP_NAME")
                            Session("Pending_Att").Rows.Add(rDt)
                        Next
                    End If
                Else
                    If ddlGrade.SelectedItem.Text = "ALL" Then
                        GRD_ID = "<>''"
                    Else
                        GRD_ID = "='" & ddlGrade.SelectedItem.Value & "'"
                    End If

                    If ddlType.SelectedIndex <> -1 Then
                        If ddlType.SelectedItem.Text = "Both" Then
                            Att_Type = "'Session1','Session2'"
                        Else
                            Att_Type = "'" & ddlType.SelectedItem.Text & "'"
                        End If
                    End If
                    Att_date = txtDate.Text


                    Dim str_Sql As String = String.Empty

                    If rbMarked.Checked = True Then
                        str_Sql = " SELECT distinct ISNULL(EMPLOYEE_M.EMP_FNAME, '') + ' ' + ISNULL(EMPLOYEE_M.EMP_MNAME, '') + ' ' + ISNULL(EMPLOYEE_M.EMP_LNAME, '') AS EMP_NAME, " & _
                              " GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,SECTION_M.SCT_DESCR as SCT_DESCR, GRADE_M.GRD_DISPLAYORDER, ATTENDANCE_LOG_GRADE.ALG_CREATEDDT as CREATEDDT, " & _
                        " ATTENDANCE_LOG_GRADE.ALG_CREATEDTM as CREATEDTM,ATTENDANCE_LOG_GRADE.ALG_ATT_TYPE as ATT_TYPE FROM  ATTENDANCE_LOG_GRADE INNER JOIN OASIS..EMPLOYEE_M ON ATTENDANCE_LOG_GRADE.ALG_EMP_ID_ADD = EMPLOYEE_M.EMP_ID INNER JOIN " & _
                              " OASIS..SECTION_M ON ATTENDANCE_LOG_GRADE.ALG_SCT_ID = SECTION_M.SCT_ID AND ATTENDANCE_LOG_GRADE.ALG_ACD_ID = SECTION_M.SCT_ACD_ID AND " & _
                             " ATTENDANCE_LOG_GRADE.ALG_GRD_ID = SECTION_M.SCT_GRD_ID INNER JOIN  OASIS..GRADE_BSU_M ON ATTENDANCE_LOG_GRADE.ALG_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND " & _
                             " ATTENDANCE_LOG_GRADE.ALG_GRD_ID = GRADE_BSU_M.GRM_GRD_ID INNER JOIN   OASIS..GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
                        " where ATTENDANCE_LOG_GRADE.ALG_ATTDT='" & Att_date & "' and ATTENDANCE_LOG_GRADE.ALG_ACD_ID='" & ACD_ID & "' and ATTENDANCE_LOG_GRADE.ALG_GRD_ID " & GRD_ID & "  and " & _
                        "  ATTENDANCE_LOG_GRADE.ALG_ATT_TYPE in(" & Att_Type & ") order by GRADE_M.GRD_DISPLAYORDER,SECTION_M.SCT_DESCR"


                    ElseIf rbFormTutor.Checked = True Then


                        str_Sql = "  SELECT DISTINCT ISNULL(E.EMP_FNAME, '') + ' ' + ISNULL(E.EMP_MNAME, '') + ' ' + ISNULL(E.EMP_LNAME, '') AS EMP_NAME, G.GRM_DISPLAY, S.SCT_DESCR, " & _
"  Gm.GRD_DISPLAYORDER, ALog.ALG_CREATEDDT AS CREATEDDT, ALog.ALG_CREATEDTM AS CREATEDTM, ALog.ALG_ATT_TYPE AS ATT_TYPE " & _
"  FROM ATTENDANCE_LOG_GRADE AS ALog INNER JOIN  EMPLOYEE_M AS E ON ALog.ALG_EMP_ID_ADD = E.EMP_ID INNER JOIN " & _
 "  SECTION_M AS S ON ALog.ALG_SCT_ID = S.SCT_ID AND ALog.ALG_ACD_ID = S.SCT_ACD_ID AND ALog.ALG_GRD_ID = S.SCT_GRD_ID INNER JOIN " & _
"  GRADE_BSU_M AS G ON ALog.ALG_ACD_ID = G.GRM_ACD_ID AND ALog.ALG_GRD_ID = G.GRM_GRD_ID INNER JOIN   GRADE_M AS Gm ON " & _
"  G.GRM_GRD_ID = Gm.GRD_ID and S.SCT_EMP_ID=E.EMP_ID WHERE (ALog.ALG_ATTDT ='" & Att_date & "') AND (ALog.ALG_ACD_ID = '" & ACD_ID & "') " & _
"   AND (ALog.ALG_GRD_ID  " & GRD_ID & ") AND (ALog.ALG_ATT_TYPE IN (" & Att_Type & ")) order by Gm.GRD_DISPLAYORDER,S.SCT_DESCR,EMP_NAME"



                    End If

                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                    If ds.Tables(0).Rows.Count > 0 Then

                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim rDt As DataRow
                            rDt = Session("Pending_Att").NewRow
                            'If temp_Grade = ds.Tables(0).Rows(i)("GRM_DISPLAY") Then
                            '    rDt("GRADE") = ""
                            'Else
                            '    rDt("GRADE") = ds.Tables(0).Rows(i)("GRM_DISPLAY")
                            '    temp_Grade = ds.Tables(0).Rows(i)("GRM_DISPLAY")
                            'End If
                            rDt("GRADE") = ds.Tables(0).Rows(i)("GRM_DISPLAY")
                            rDt("SECTION") = ds.Tables(0).Rows(i)("SCT_DESCR")
                            rDt("ATT_TYPE") = ds.Tables(0).Rows(i)("ATT_TYPE")
                            rDt("EMP_NAME") = ds.Tables(0).Rows(i)("EMP_NAME")
                            rDt("DT_TIME") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(i)("CREATEDDT")) & " & " & String.Format("{0:HH:mm tt}", ds.Tables(0).Rows(i)("CREATEDTM"))

                            Session("Pending_Att").Rows.Add(rDt)
                        Next
                    End If

                End If
                gridbind()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        Else
            lblError.Text = "Invalid Date Entry."
        End If



    End Sub
    Sub gridbind()

        If rbPending.Checked = True Then
            gvAttendance.Columns(4).Visible = False

        ElseIf rbMarked.Checked = True Then
            gvAttendance.Columns(4).Visible = True
        ElseIf rbFormTutor.Checked = True Then
            gvAttendance.Columns(4).Visible = True
        End If
        gvAttendance.DataSource = Session("Pending_Att")
        gvAttendance.DataBind()
       
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim GRADE As New DataColumn("GRADE", System.Type.GetType("System.String"))
            Dim SECTION As New DataColumn("SECTION", System.Type.GetType("System.String"))
            Dim ATT_TYPE As New DataColumn("ATT_TYPE", System.Type.GetType("System.String"))
            Dim EMP_NAME As New DataColumn("EMP_NAME", System.Type.GetType("System.String"))
            Dim DT_TIME As New DataColumn("DT_TIME", System.Type.GetType("System.String"))
            dtDt.Columns.Add(GRADE)
            dtDt.Columns.Add(SECTION)
            dtDt.Columns.Add(ATT_TYPE)
            dtDt.Columns.Add(EMP_NAME)
            dtDt.Columns.Add(DT_TIME)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function
   
    Protected Sub rbMarked_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callPending_Att()
    End Sub

    Protected Sub rbPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callPending_Att()
    End Sub
    Protected Sub rbFormTutor_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbFormTutor.CheckedChanged
        callPending_Att()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call bindAtt_Type()
        callPending_Att()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callPending_Att()
    End Sub

    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        Dim strfDate As String = txtDate.Text.Trim
        Dim str_err As String = checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtDate.Text = strfDate
            Call bindAtt_Type()
        End If
        callPending_Att()

    End Sub
    Function checkdate(ByRef p_date As String) As String
        Dim dFrom As Date

        If DateFunctions.ConvertoDate(p_date, dFrom) = False Then
            Return "Invalid Date Entry"
        End If
        p_date = String.Format("{0:dd/MMM/yyyy}", dFrom)
        Return ""

    End Function
    Function DateValidate() As Integer
        Dim sflag As Boolean = False
        Dim datetime1 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            datetime1 = Date.ParseExact(txtDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(datetime1) Then
                sflag = True
            End If

            If sflag Then
                DateValidate = 0

            Else
                DateValidate = 1
            End If
        Catch
            'catch when format string through an error
            DateValidate = 1
        End Try
    End Function

   
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        bindAcademic_Grade()
        Call bindAtt_Type()
        callPending_Att()
    End Sub

    Protected Sub gvAttendance_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAttendance.PageIndexChanging
        gvAttendance.PageIndex = e.NewPageIndex
        callPending_Att()

    End Sub
End Class
