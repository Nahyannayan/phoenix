Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections

Partial Class Students_studGrade_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S050035") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'GetActive_ACD_4_Grade


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    GridBind()
                End If
            Catch ex As Exception

                lblError.Text = "Request could not be processed "
            End Try

        End If


    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudGrade_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudGrade.PageIndexChanging
        Try
            gvStudGrade.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudGrade.RowCommand
        Try
            Dim url As String
            Dim lblGrdid As New Label
            Dim lblGrade As New Label
            Dim lbldescr As New Label
            Dim lblStmid As Label
            Dim lblStream As Label
            Dim lblcert As Label
            Dim lblprio As Label

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStudGrade.Rows(index), GridViewRow)
            lblGrdid = selectedRow.FindControl("lblGrdId")
            lblGrade = selectedRow.FindControl("lblGrade")
            lbldescr = selectedRow.FindControl("lblgrmdescr")
            lblcert = selectedRow.FindControl("lblgrmcert")
            lblprio = selectedRow.FindControl("lblgrmprio")
            If e.CommandName = "view" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

                ViewState("datamode") = "view"
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

                url = String.Format("~\Students\studGrade_M.aspx?MainMnu_code={0}&datamode={1}&grdid=" + Encr_decrData.Encrypt(lblGrdid.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&academicyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) + "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue) + "&descr=" + Encr_decrData.Encrypt(lbldescr.Text) + "&cert=" + Encr_decrData.Encrypt(lblcert.Text) + "&prio=" + Encr_decrData.Encrypt(lblprio.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            ElseIf e.CommandName = "edit" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = "add"
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

                lblStmid = selectedRow.FindControl("lblStmid")
                lblStream = selectedRow.FindControl("lblStream")
                url = String.Format("~\Students\studGradeStage_Edit.aspx?MainMnu_code={0}&datamode={1}&grdid=" + Encr_decrData.Encrypt(lblGrdid.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&stmid=" + Encr_decrData.Encrypt(lblStmid.Text) + "&stream=" + Encr_decrData.Encrypt(lblStream.Text) + "&academicyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) + "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            ElseIf e.CommandName = "add" Then
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = "add"
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

                lblStmid = selectedRow.FindControl("lblStmid")
                url = String.Format("~\Students\studGradeJoinDocs.aspx?MainMnu_code={0}&datamode={1}&grdid=" + Encr_decrData.Encrypt(lblGrdid.Text) + "&stmid=" + Encr_decrData.Encrypt(lblStmid.Text) + "&acdid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))


            url = String.Format("~\Students\studGrade_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudGrade_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudGrade.RowCreated

    End Sub

#Region "PrivateMethods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GridBind()
        Dim ddlgvGrade As New DropDownList
        Dim ddlgvShift As New DropDownList
        Dim ddlgvStream As New DropDownList

        Dim dv As New DataView
        Dim selectedGrade As String = ""
        Dim str_filter As String = ""
        Dim selectedShift As String = ""
        Dim selectedStream As String = ""
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT grm_grd_id,grm_display,shf_descr,stm_id,stm_descr," _
                                 & "CASE grm_gender WHEN 'A' THEN 'All' WHEN 'M' THEN 'Male' ELSE 'NO' END AS gender," _
                                 & "grm_maxcapacity,grm_mgmcapacity,case ISNULL(GRM_bOPEN_REG_PAY,0) when 1 then 'Yes' else 'No' end as GRM_bOPEN_REG_PAY," _
                                 & " CASE grm_openonline WHEN 1 THEN 'Yes' ELSE 'No' END AS openonline," _
                                 & "CASE grm_bscreeningreqd WHEN 1 THEN 'Yes' ELSE 'No' END AS screenreqd,grd_displayorder,grm_descr," _
                                 & " grm_certificate,isnull(grm_priority,0) as grm_priority,ISNULL(GRM_bOPEN_REG_PAY,0)GRM_bOPEN_REG_PAY" _
                                 & " FROM grade_bsu_m,grade_m,shifts_m,stream_m WHERE grade_bsu_m.grm_grd_id=grade_m.grd_id" _
                                 & " AND grade_bsu_m.grm_shf_id = shifts_m.shf_id And grade_bsu_m.grm_stm_id = stream_m.stm_id" _
                                 & " AND grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " ORDER BY grd_displayorder"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dv = ds.Tables(0).DefaultView
        If gvStudGrade.Rows.Count > 0 Then
            ddlgvGrade = gvStudGrade.HeaderRow.FindControl("ddlgvGrade")
            If ddlgvGrade.Text <> "ALL" Then
                str_filter = "grm_display='" + ddlgvGrade.Text + "'"
                selectedGrade = ddlgvGrade.Text
            End If

            ddlgvShift = gvStudGrade.HeaderRow.FindControl("ddlgvShift")

            If ddlgvShift.Text <> "ALL" Then
                If str_filter = "" Then
                    str_filter = "shf_descr='" + ddlgvShift.Text + "'"
                Else
                    str_filter = str_filter + " AND shf_descr='" + ddlgvShift.Text + "'"
                End If

                selectedShift = ddlgvShift.Text
            End If

            ddlgvStream = gvStudGrade.HeaderRow.FindControl("ddlgvStream")

            If ddlgvStream.Text <> "ALL" Then
                If str_filter = "" Then
                    str_filter = "stm_descr='" + ddlgvStream.Text + "'"
                Else
                    str_filter = str_filter + " AND stm_descr='" + ddlgvStream.Text + "'"
                End If

                selectedStream = ddlgvStream.Text
            End If


            If ddlgvGrade.Text = "ALL" And ddlgvShift.Text = "ALL" And ddlgvStream.Text = "ALL" Then
                dv = New DataView
                dv = ds.Tables(0).DefaultView
            Else
                dv.RowFilter = str_filter
            End If
        End If
        gvStudGrade.DataSource = dv
        gvStudGrade.DataBind()

        If gvStudGrade.Rows.Count = 0 Then
            ds.Tables(0).Rows.Clear()
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStudGrade.DataSource = ds
            gvStudGrade.DataBind()
            Dim columnCount As Integer = gvStudGrade.Rows(0).Cells.Count
            gvStudGrade.Rows(0).Cells.Clear()
            gvStudGrade.Rows(0).Cells.Add(New TableCell)
            gvStudGrade.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudGrade.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        End If

        If gvStudGrade.Rows.Count > 0 Then

            str_query = "SELECT distinct(grm_display),grd_displayorder FROM grade_bsu_m,grade_m WHERE grade_bsu_m.grm_grd_id=grade_m.grd_id" _
                       & " AND grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " ORDER BY grd_displayorder"

            ddlgvGrade = gvStudGrade.HeaderRow.FindControl("ddlgvGrade")
            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")
            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                ddlgvGrade.Items.Add(reader.GetString(0))
            End While
            reader.Close()
            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If

            str_query = "SELECT distinct(shf_descr) FROM grade_bsu_m,shifts_m WHERE grade_bsu_m.grm_shf_id=shifts_m.shf_id" _
                       & " AND grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + ""

            ddlgvShift = gvStudGrade.HeaderRow.FindControl("ddlgvShift")
            ddlgvShift.Items.Clear()
            ddlgvShift.Items.Add("ALL")
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                ddlgvShift.Items.Add(reader.GetString(0))
            End While
            reader.Close()
            If selectedShift <> "" Then
                ddlgvShift.Text = selectedShift
            End If

            str_query = "SELECT distinct(stm_descr) FROM grade_bsu_m,stream_m WHERE grade_bsu_m.grm_stm_id=stream_m.stm_id" _
                                 & " AND grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + ""

            ddlgvStream = gvStudGrade.HeaderRow.FindControl("ddlgvStream")
            ddlgvStream.Items.Clear()
            ddlgvStream.Items.Add("ALL")
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                ddlgvStream.Items.Add(reader.GetString(0))
            End While
            reader.Close()
            If selectedStream <> "" Then
                ddlgvStream.Text = selectedStream
            End If
        End If
    End Sub

#End Region
End Class
