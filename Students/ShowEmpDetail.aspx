<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowEmpDetail.aspx.vb" Inherits="ShowEmpDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ OutputCache Duration="1" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <base target="_self" />
    <%--   <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <script language="javascript" type="text/javascript">
        // function settitle(){
        //alert('fgfg');
        //document.title ='sfsdsd';
        // alert('fgfg');
        //      }


        function test(val) {
            //alert(val);
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid()%>").src = path;
            document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
        }
        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
                 document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
        }

        function SetValuetoParent(stuid) {
            //alert(stuid);
            //alert(window.parent.document.p);
            parent.setEmpValue(stuid);
            return false;
        }

        function SetValuetoParent2(stuid) {
            //alert(stuid);
            //alert(window.parent.document.p);
            parent.setEmpValue2(stuid);
            return false;
        }
    </script>

</head>
<body onload="listen_window();">
    <form id="form1" runat="server">

        <div>
            <div>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" width="100%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gvEmpInfo" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" DataKeyNames="ID" CssClass="table table-bordered table-row">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Designation">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblDesignation" runat="server" Text="Designation"></asp:Label><br />
                                                                    <asp:DropDownList ID="ddldesignation" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddldesignation_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldesignation" runat="server" Text='<%# Bind("DES_DESCR") %>'></asp:Label>
                                                                    <br />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Employee ID" SortExpression="EMP_ID">
                                                                <EditItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server"  Text='<%# Bind("EMPNO")%>'></asp:LinkButton><%--OnClick="LinkButton2_Click"--%>
                                                                </ItemTemplate>
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblID" runat="server" EnableViewState="False" Text="Business ID"></asp:Label><br />
                                                                    <asp:TextBox ID="txtcode" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchEmpId_Click" />
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblName" runat="server" Text="Business Unit Name"></asp:Label><br />
                                                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                        OnClick="btnSearchEmpName_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Selected"
                                                                        Text='<%# Eval("E_Name") %>' ></asp:LinkButton><%--OnClick="LinkButton1_Click"--%>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                        <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <RowStyle CssClass="griditem" />
                                                    </asp:GridView>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" ></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <input type="hidden" id="h_SelectedId" runat="server" value="0" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            </table>
        </div>
       


    </form>

</body>
</html>
