Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_studEnqChange_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100150") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue.ToString)
                    ddlStream = PopulateGradeStream(ddlStream, ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)

                    Dim cb As New CheckBox
                    For Each gvr As GridViewRow In gvStudEnquiry.Rows
                        cb = gvr.FindControl("chkSelect")
                        ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()

                    btnChange.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try



        Else

            highlight_grid()

        End If

    End Sub
    Protected Sub btnEnqid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnEnq_Date_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnAppl_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function PopulateGradeStream(ByVal ddl As DropDownList, ByVal grdid As String, ByVal acdid As String, ByVal shfid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT stm_descr,grm_stm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                             & " grm_acd_id=" + acdid + " and grm_grd_id='" + grdid + "' and grm_shf_id=" + shfid
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "stm_descr"
        ddl.DataValueField = "grm_stm_id"
        ddl.DataBind()
        Return ddl
    End Function


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub highlight_grid()
        For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1
            Dim row As GridViewRow = gvStudEnquiry.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

    Private Sub GridBind()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT  EQS_ID,eqs_applno,EQM_ENQID,EQM_ENQDATE, GRM_DISPLAY, SHF_DESCR,STM_DESCR, " _
                                      & " APPL_NAME=isnull(EQM_APPLFIRSTNAME,'') + ' ' + ISNULL(EQM_APPLMIDNAME, '') + ' ' + ISNULL(EQM_APPLLASTNAME, '') " _
                                     & " FROM   ENQUIRY_M AS A INNER JOIN " _
                                     & "ENQUIRY_SCHOOLPRIO_S AS B ON A.EQM_ENQID = B.EQS_EQM_ENQID INNER JOIN" _
                                     & " GRADE_BSU_M AS C ON B.EQS_GRM_ID = C.GRM_ID INNER JOIN " _
                                     & " SHIFTS_M AS D ON B.EQS_SHF_ID = D.SHF_ID INNER JOIN " _
                                     & " STREAM_M AS E ON B.EQS_STM_ID = E.STM_ID " _
                                     & " WHERE EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                     & " AND EQS_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'" _
                                     & " AND EQS_SHF_ID=" + ddlShift.SelectedValue.ToString _
                                     & " AND EQS_STM_ID=" + ddlStream.SelectedValue.ToString _
                                     & " AND EQS_BSU_ID='" + Session("sbsuid") + "'  " _
                                     & " AND eqs_status<>'DEL' AND EQS_STATUS<>'ENR'  AND EQS_CANCELDATE IS NULL " _
                                     & " AND EQS_STATUS<>'CR' AND EQS_STATUS<>'CO'"
            Dim strFilter As String = ""
            Dim strSidsearch As String()
            Dim strSearch As String

            Dim enqSearch As String = ""
            Dim nameSearch As String = ""
            Dim dateSearch As String = ""
            Dim txtSearch As New TextBox

            If gvStudEnquiry.Rows.Count > 0 Then
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = GetSearchString("eqs_applno", txtSearch.Text, strSearch)
                enqSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("convert(varchar,eqm_enqdate,106)", txtSearch.Text.Replace("/", " "), strSearch)
                dateSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
                strSidsearch = h_Selected_menu_3.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("isnull(eqm_applfirstname,' ')+' '+isnull(eqm_applmidname,' ')+' '+isnull(eqm_appllastname,' ')", txtSearch.Text, strSearch)
                nameSearch = txtSearch.Text

                If strFilter.Trim <> "" Then
                    str_query = str_query + strFilter
                End If
            End If

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudEnquiry.DataSource = ds

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudEnquiry.DataBind()
                Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
                gvStudEnquiry.Rows(0).Cells.Clear()
                gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
                gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudEnquiry.DataBind()
            End If

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
            txtSearch.Text = enqSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
            txtSearch.Text = dateSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
            txtSearch.Text = nameSearch

            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub BindChangeGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        If chkAcademicYear.Checked = True Then
            str_query = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                        & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                       & "  grm_acd_id=" + ddlCAcademicYear.SelectedValue.ToString + " order by grd_displayorder"
        Else
            str_query = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                         & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                        & "  grm_acd_id=" + hfacd_id.value + " order by grd_displayorder"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlCGrade.DataSource = ds
        ddlCGrade.DataTextField = "grm_display"
        ddlCGrade.DataValueField = "grm_grd_id"
        ddlCGrade.DataBind()
    End Sub

    Sub BindChangeShift()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strfilter As String

        Dim str_query As String = "SELECT distinct shf_descr,shf_id FROM grade_bsu_m,shifts_m WHERE" _
                                 & " grade_bsu_m.grm_shf_id=shifts_m.shf_id and "
        If chkAcademicYear.Checked = True Then
            strfilter = " grm_acd_id=" + ddlCAcademicYear.SelectedValue.ToString
        Else
            strfilter = " grm_acd_id=" + hfACD_ID.Value.ToString
        End If

        If chkGrade.Checked = True Then
            strfilter += " and grm_grd_id='" + ddlCGrade.SelectedValue.ToString + "'"
        Else
            strfilter += " and grm_grd_id='" + hfGRD_ID.Value.ToString + "'"
        End If

        str_query += strfilter

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCShift.DataSource = ds
        ddlCShift.DataTextField = "shf_descr"
        ddlCShift.DataValueField = "shf_id"
        ddlCShift.DataBind()
    End Sub


    Sub BindChangeStream()
        Dim strfilter As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT stm_descr,grm_stm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and "

        If chkAcademicYear.Checked = True Then
            strfilter = " grm_acd_id=" + ddlCAcademicYear.SelectedValue.ToString
        Else
            strfilter = " grm_acd_id=" + hfACD_ID.Value.ToString
        End If

        If chkGrade.Checked = True Then
            strfilter += " and grm_grd_id='" + ddlCGrade.SelectedValue.ToString + "'"
        Else
            strfilter += " and grm_grd_id='" + hfGRD_ID.Value.ToString + "'"
        End If

        If chkShift.Checked = True Then
            strfilter += " and  grm_shf_id=" + ddlCShift.SelectedValue.ToString
        Else
            strfilter += " and  grm_shf_id=" + hfSHF_ID.Value.ToString
        End If


        str_query += strfilter

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlCStream.DataSource = ds
        ddlCStream.DataTextField = "stm_descr"
        ddlCStream.DataValueField = "grm_stm_id"
        ddlCStream.DataBind()

    End Sub

    Sub SaveData()
        Dim i As Integer
        Dim transaction As SqlTransaction
        Dim lblEqsId As Label
        Dim lblEnqId As Label
        Dim strQuery As String
        Dim acdChange As String
        Dim grdChange As String
        Dim shfChange As String
        Dim stmChange As String
        Dim chkSelect As CheckBox

        If chkAcademicYear.Checked = True Then
            acdChange = ddlCAcademicYear.SelectedValue.ToString
        Else
            acdChange = hfACD_ID.Value
        End If

        If chkGrade.Checked = True Then
            grdChange = ddlCGrade.SelectedValue.ToString
        Else
            grdChange = hfGRD_ID.Value.ToString
        End If

        If chkShift.Checked = True Then
            shfChange = ddlCShift.SelectedValue.ToString
        Else
            shfChange = hfSHF_ID.Value.ToString
        End If

        If chkStream.Checked = True Then
            stmChange = ddlCStream.SelectedValue.ToString
        Else
            stmChange = hfSTM_ID.Value.ToString
        End If
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try
                For i = 0 To gvStudEnquiry.Rows.Count - 1

                    chkSelect = gvStudEnquiry.Rows(i).FindControl("chkSelect")
                    If chkSelect.Checked = True Then
                        lblEqsId = gvStudEnquiry.Rows(i).FindControl("lblEqsId")
                        lblEnqId = gvStudEnquiry.Rows(i).FindControl("lblEnqId")
                        transaction = conn.BeginTransaction("SampleTransaction")

                        UtilityObj.InsertAuditdetails(transaction, "edit", "ENQUIRY_SCHOOLPRIO_S", "EQS_ID", "EQS_EQM_ENQID", "EQS_ID=" + lblEqsId.Text)
                        UtilityObj.InsertAuditdetails(transaction, "delete", "PROCESSFO_APPLICANT_S", "PRA_ID", "PRA_EQS_ID", "PRA_EQS_ID=" + lblEqsId.Text)

                        strQuery = "exec STUDENQUIRYCHANGE " + lblEqsId.Text + "," _
                                                             & lblEnqId.Text + "," _
                                                             & "'" + Session("sbsuid") + "'," _
                                                             & hfACD_ID.Value.ToString + "," _
                                                             & "'" + hfGRD_ID.Value.ToString + "'," _
                                                             & hfSHF_ID.Value.ToString + "," _
                                                             & hfSTM_ID.Value.ToString + "," _
                                                             & acdChange + "," _
                                                             & "'" + grdChange + "'," _
                                                             & shfChange + "," _
                                                             & stmChange + "," _
                                                             & "'" + Session("sUsr_name") + "'"

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, strQuery)

                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "EQS_ID(" + lblEqsId.Text + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        lblError.Text = "Record Saved Successfully"
                        transaction.Commit()
                    End If
                Next
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

#End Region

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue.ToString)
        ddlStream = PopulateGradeStream(ddlStream, ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
         ddlShift = studClass.PopulateGradeShift(ddlShift, ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue.ToString)
        ddlStream = PopulateGradeStream(ddlStream, ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        ddlStream = PopulateGradeStream(ddlStream, ddlGrade.SelectedValue, ddlAcademicYear.SelectedValue.ToString, ddlShift.SelectedValue.ToString)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblError.Text = ""
        GridBind()
        hfACD_ID.Value = ddlAcademicYear.SelectedValue
        hfGRD_ID.Value = ddlGrade.SelectedValue
        hfSHF_ID.Value = ddlShift.SelectedValue
        hfSTM_ID.Value = ddlStream.SelectedValue
        chkAcademicYear.Enabled = True
        chkGrade.Enabled = True
        chkShift.Enabled = True
        chkStream.Enabled = True
        btnChange.Visible = True
    End Sub

    Protected Sub chkAcademicYear_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAcademicYear.CheckedChanged
        If chkAcademicYear.Checked = True Then
            ddlCAcademicYear = studClass.PopulateAcademicYear(ddlCAcademicYear, Session("clm"), Session("sbsuid"))
            ddlCAcademicYear.Enabled = True
        Else
            ddlCAcademicYear.Enabled = False
        End If
        If chkGrade.Checked = True Then
            BindChangeGrade()
        End If
        If chkShift.Checked = True Then
            BindChangeShift()
        End If
        If chkStream.Checked = True Then
            BindChangeStream()
        End If
    End Sub

    Protected Sub chkGrade_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGrade.CheckedChanged
        If chkGrade.Checked = True Then
            BindChangeGrade()
            ddlCGrade.Enabled = True
        Else
            ddlCGrade.Enabled = False
        End If
        If chkShift.Checked = True Then
            BindChangeShift()
        End If
        If chkStream.Checked = True Then
            BindChangeStream()
        End If
    End Sub

    Protected Sub ddlCAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCAcademicYear.SelectedIndexChanged
        If chkGrade.Checked = True Then
            BindChangeGrade()
        End If
        If chkShift.Checked = True Then
            BindChangeShift()
        End If
        If chkStream.Checked = True Then
            BindChangeStream()
        End If
    End Sub

    Protected Sub ddlCGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCGrade.SelectedIndexChanged
        If chkShift.Checked = True Then
            BindChangeShift()
        End If
        If chkStream.Checked = True Then
            BindChangeStream()
        End If
    End Sub

    Protected Sub chkShift_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShift.CheckedChanged
        If chkShift.Checked = True Then
            BindChangeShift()
            ddlCShift.Enabled = True
        Else
            ddlCShift.Enabled = False
        End If
        If chkStream.Checked = True Then
            BindChangeStream()
        End If
    End Sub

    Protected Sub chkStream_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkStream.CheckedChanged
        If chkStream.Checked = True Then
            BindChangeStream()
            ddlCStream.Enabled = True
        Else
            ddlCStream.Enabled = False
        End If
    End Sub

    Protected Sub btnChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChange.Click
        If chkAcademicYear.Checked = True Or chkGrade.Checked = True Or chkShift.Checked = True Or chkStream.Checked = True Then
            SaveData()
            GridBind()
            hfACD_ID.Value = ddlAcademicYear.SelectedValue
            hfGRD_ID.Value = ddlGrade.SelectedValue
            hfSHF_ID.Value = ddlShift.SelectedValue
            hfSTM_ID.Value = ddlStream.SelectedValue
        End If
    End Sub


    Protected Sub ddlCShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCShift.SelectedIndexChanged
        If chkStream.Checked = True Then
            BindChangeStream()
        End If
    End Sub

    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudEnquiry.PageIndexChanging
        gvStudEnquiry.PageIndex = e.NewPageIndex
        GridBind()
    End Sub
End Class
