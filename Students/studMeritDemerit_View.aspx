<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studMeritDemerit_View.aspx.vb" Inherits="studMeritDemerit_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

     <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />



    <script language="javascript" type="text/javascript">
        

            function ShowMerits(STU_ID, ACD_ID) {

                var sFeatures;
                sFeatures = "dialogWidth: 950px; ";
                sFeatures += "dialogHeight: 750px; ";
                sFeatures += "help: no; ";
                sFeatures += "resizable: yes; ";
                sFeatures += "scroll: yes; "; 
                sFeatures += "status: no; ";
                sFeatures += "unadorned: no; ";
                var NameandCode;
                var result;
                var url;
                url = '../Students/StudMerits_View.aspx?STU_ID=' + STU_ID + '&ACD_ID=' + ACD_ID + '';
                //alert(status) 
                return ShowWindowWithClose(url, 'search', '65%', '90%')
                return false;
                //result = window.showModalDialog(url, "", sFeatures);

                //if (result == '' || result == undefined) {
                //    return false;
                //}

            }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Merits-Demerits"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--  <tr style="font-size: 12pt;">
        <td width="50%" align="left" class="title" style="height: 50px">
            MERITS-DEMERITS</td>
        </tr>--%>

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Academic Year</span></td>

                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left"  width="20%"></td>
                                     <td align="left"  width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:LinkButton ID="lnkadd" runat="server">Add New</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">

                                        <asp:GridView ID="gvStudTPT" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="10" Width="100%">
                                            <Columns>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student No">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label><br />
                                                        <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuNo_Click" />

                                                    </HeaderTemplate>
                                                    <ItemStyle />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label><br />
                                                        <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click" />

                                                    </HeaderTemplate>
                                                    <ItemStyle />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        Grade<br />
                                                        <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Section">
                                                    <HeaderTemplate>
                                                        Section<br />
                                                        <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True"
                                                            OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Merits">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblMerits" runat="server">Merits</asp:Label><br />
                                                        <asp:TextBox ID="txtMerits" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnMerits" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchMerits_Click" />

                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMerits" runat="server" Text='<%# Bind("Merits") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="DeMerits">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblDeMerits" runat="server">DeMerits</asp:Label><br />
                                                        <asp:TextBox ID="txtDeMerits" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnDeMerits" runat="server" ImageAlign="Middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchDeMerits_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle  HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDeMerits"  runat="server" Text='<%# Bind("DeMerits") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server" OnClientClick="<%# &quot;ShowMerits('&quot; & Container.DataItem(&quot;STU_ID&quot;) & &quot;','&quot; & Container.DataItem(&quot;STU_ACD_ID&quot;) & &quot;');return false;&quot; %>">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>                                                
                                            </Columns>
                                            <HeaderStyle  />
                                            <RowStyle CssClass="griditem"  />
                                            <SelectedRowStyle  />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>

                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server"
                                type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server"
                                type="hidden" value="=" /></td>
                    </tr>


                </table>



            </div>
        </div>
    </div>
     <script type="text/javascript" lang="javascript">
         function ShowWindowWithClose(gotourl, pageTitle, w, h) {
             $.fancybox({
                 type: 'iframe',
                 //maxWidth: 300,
                 href: gotourl,
                 //maxHeight: 600,
                 fitToView: true,
                 padding: 6,
                 width: w,
                 height: h,
                 autoSize: false,
                 openEffect: 'none',
                 showLoading: true,
                 closeClick: true,
                 closeEffect: 'fade',
                 'closeBtn': true,
                 afterLoad: function () {
                     this.title = '';//ShowTitle(pageTitle);
                 },
                 helpers: {
                     overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                     title: { type: 'inside' }
                 },
                 onComplete: function () {
                     $("#fancybox-wrap").css({ 'top': '90px' });
                 },
                 onCleanup: function () {
                     var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                     if (hfPostBack == "Y")
                         window.location.reload(true);
                 },
                 afterClose: function () {
                     window.location.reload();

                } 
             });

             return false;
         }
    </script>
</asp:Content>

