Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studTRM_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub setbutton()
        btnAcademic.Visible = False

    End Sub
    Sub resetbutton()
        btnAcademic.Visible = True

    End Sub
    Sub clearRecords()
        txtAcadYear.Text = ""
        txtTermDesc.Text = ""
        txtTo_date.Text = ""
        txtActualStartDate.Text = ""
        txtFrom_date.Text = ""
        ViewState("viewid") = ""
        txtAcadYear.Attributes.Add("readonly", "readonly")
        Session("TermDuration") = Nothing
        Session("termWeeks") = Nothing
        gvWeek.DataSource = Session("termWeeks")
        gvWeek.DataBind()
        gvDuration.DataSource = Session("TermDuration")
        gvDuration.DataBind()
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ViewState("datamode") = "add"
            Call clearRecords()
            resetbutton()
            Call callActive_year()
            txtTermDesc.Attributes.Remove("readonly")
            txtFrom_date.Attributes.Remove("readonly")
            txtTo_date.Attributes.Remove("readonly")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            lnkAddTerm.Visible = True
        Catch ex As Exception
            UtilityObj.Errorlog("Term Master", ex.Message)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim retVal As Integer = AccessStudentClass.CheckEditingAllowed(ViewState("viewid"))
        If retVal = 1000 Then
            'lblError.Text = "Editing not Allowed"
            usrMessageBar.ShowNotification("Editing not Allowed", UserControls_usrMessageBar.WarningType.Danger)
            Return
        ElseIf retVal = 1 Then
            'lblError.Text = "Only weeks & Actual Start date editing is allowed"
            usrMessageBar.ShowNotification("Only weeks & Actual Start date editing is allowed", UserControls_usrMessageBar.WarningType.Danger)
            DissableControlsForWeekedit(True)
            ViewState("EditOnlyWeeks") = True
        Else
            ViewState("EditOnlyWeeks") = False
            txtTermDesc.Attributes.Remove("readonly")
            txtFrom_date.Attributes.Remove("readonly")
            txtTo_date.Attributes.Remove("readonly")
        End If
        ViewState("datamode") = "edit"
        lnkAddTerm.Visible = True
         UtilityObj.beforeLoopingControls(Me.Page)
        resetbutton()
        MakeWeeksEditable()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub DissableControlsForWeekedit(ByVal bDissable As Boolean)
        txtTermDesc.Attributes.Add("ReadOnly", "ReadOnly")
        txtFrom_date.Attributes.Add("ReadOnly", "ReadOnly")
        txtTo_date.Attributes.Add("ReadOnly", "ReadOnly")
        gvDuration.Enabled = Not bDissable
        btnAcademic.Visible = Not bDissable
        txtAcadYear.ReadOnly = bDissable
        CETFromDate.Enabled = Not bDissable
        CETToDate.Enabled = Not bDissable
        CBTEFromDate.Enabled = Not bDissable
        CBETToDate.Enabled = Not bDissable

    End Sub

    Private Sub MakeWeeksEditable()
        Dim dtWeek As DataTable = Session("termWeeks")
        For Each dtRow As DataRow In dtWeek.Rows
            dtRow("bEdit") = True
        Next
        Session("termWeeks") = dtWeek
    End Sub

    ''' <summary>
    ''' Fnction Updates the Session Datatable before it gets Saved
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateDataTable()
        Dim txtFromDate As TextBox
        Dim txtToDate As TextBox
        Dim lblUniqueID As Label
        Dim dtTerm As DataTable = Session("TermDuration")
        For Each grdRow As GridViewRow In gvDuration.Rows
            If Not grdRow.FindControl("txtFromDate") Is Nothing Then
                txtFromDate = grdRow.FindControl("txtFromDate")
                If Not grdRow.FindControl("txtToDate") Is Nothing Then
                    txtToDate = grdRow.FindControl("txtToDate")
                    If Not grdRow.FindControl("lblUniqueID") Is Nothing Then
                        lblUniqueID = grdRow.FindControl("lblUniqueID")
                        UpdateTable(dtTerm, lblUniqueID.Text, txtFromDate.Text, txtToDate.Text)
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub UpdateTable(ByRef dtTerm As DataTable, ByVal UniqueID As String, ByVal frmDT As DateTime, ByVal toDT As DateTime)
        For Each dr As DataRow In dtTerm.Rows
            If dr("UniqueID") = UniqueID Then
                dr("FromDate") = frmDT
                dr("ToDate") = toDT
            End If
        Next
    End Sub

    ''' <summary>
    ''' Fnction Updates the Session Datatable of the Week before it gets Saved
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateWeekDataTable()
        Dim txtFromDate As TextBox
        Dim txtToDate As TextBox
        Dim lblUniqueID As Label
        Dim dtTerm As DataTable = Session("termWeeks")
        For Each grdRow As GridViewRow In gvWeek.Rows
            If Not grdRow.FindControl("txtFromDate") Is Nothing Then
                txtFromDate = grdRow.FindControl("txtFromDate")
                If Not grdRow.FindControl("txtToDate") Is Nothing Then
                    txtToDate = grdRow.FindControl("txtToDate")
                    If Not grdRow.FindControl("lblUniqueID") Is Nothing Then
                        lblUniqueID = grdRow.FindControl("lblUniqueID")
                        UpdateTable(dtTerm, lblUniqueID.Text, txtFromDate.Text, txtToDate.Text)
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub UpdateWeekTable(ByRef dtTerm As DataTable, ByVal UniqueID As String, ByVal frmDT As DateTime, ByVal toDT As DateTime)
        For Each dr As DataRow In dtTerm.Rows
            If dr("UniqueID") = UniqueID Then
                dr("FromDate") = frmDT
                dr("ToDate") = toDT
            End If
        Next
    End Sub

    Private Function ValidWeeks() As Boolean
        Dim dtWeeks As DataTable = Session("termWeeks")
        Dim frmDT, toDT As Date
        Dim dtTemp As Date()
        For Each dr As DataRow In dtWeeks.Rows
            If dr("bDelete") Then Continue For
            frmDT = dr("FromDate")
            toDT = dr("ToDate")
            If frmDT > toDT Then
                Return False
            End If
            dtTemp = GetDatesforPeriod(frmDT, toDT)
            For Each chkDT As Date In dtTemp
                For Each drInner As DataRow In dtWeeks.Rows
                    If drInner("bDelete") Then Continue For
                    If dr("UniqueID") <> drInner("UniqueID") Then
                        If chkDT >= drInner("FromDate") AndAlso chkDT <= drInner("ToDate") Then
                            Return False
                        End If
                    End If
                Next
            Next
        Next
        Return True
    End Function

    Private Function GetDatesforPeriod(ByVal fromDT As Date, ByVal TODT As Date) As Date()
        Dim count As Integer = DateDiff(DateInterval.Day, fromDT, TODT)
        Dim dtDates(count) As Date
        Dim i As Integer = 0
        While (fromDT <= TODT)
            dtDates(i) = fromDT
            fromDT = fromDT.AddDays(1)
            i += 1
        End While
        Return dtDates
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then
            UpdateDataTable()
            UpdateWeekDataTable()
            If Not ValidWeeks() Then
                'lblError.Text = "The Week Period is not valid..Term weeks may be repeating"
                usrMessageBar.ShowNotification("The Week Period is not valid..Term weeks may be repeating", UserControls_usrMessageBar.WarningType.Danger)
                Return
            End If
            Dim TRM_ID As Integer = 0
            Dim TRM_ACD_ID As Integer = hfACD_ID.Value
            Dim TRM_BSU_ID As String = Session("sBsuid")
            Dim TRM_DESCRIPTION As String = txtTermDesc.Text
            Dim TRM_STARTDATE As Date = txtFrom_date.Text
            Dim TRM_ENDDATE As Date = txtTo_date.Text
            Dim TRM_ACTUALSTARTDATE As Date = txtActualStartDate.Text
            Dim vWeeksCount As Integer = IIf(gvWeek.Rows Is Nothing, 0, gvWeek.Rows.Count)

            Dim bEdit As Boolean

            Dim status As Integer
            Dim transaction As SqlTransaction

            If ViewState("datamode") = "add" Then
                bEdit = False

                'get the max Id to be inserted in audittrial report
                Dim sqlTRM_ID As String = "Select isnull(max(TRM_ID),0)+1  from TRM_M"
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlTRM_ID, connection)
                Dim temp As String
                command.CommandType = CommandType.Text
                temp = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()

                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        'call the class to insert the record into TRM_M
                        Dim NEW_TRM_ID As Integer

                        status = AccessStudentClass.SaveTRM_M(TRM_ID, TRM_ACD_ID, TRM_BSU_ID, TRM_DESCRIPTION, TRM_STARTDATE, TRM_ENDDATE, vWeeksCount, bEdit, NEW_TRM_ID, transaction, TRM_ACTUALSTARTDATE)
                        Dim bDeleteAll As Boolean = IIf(ViewState("bDeleteAllTerms_Months") Is Nothing, False, ViewState("bDeleteAllTerms_Months"))
                        If bDeleteAll AndAlso status = 0 Then status = AccessStudentClass.DeleteTermMonths(NEW_TRM_ID, TRM_ACD_ID, transaction, ViewState("MainMnu_code"), Session("sUsr_name"))
                        If status = 0 Then status = AccessStudentClass.SaveTermMonths(NEW_TRM_ID, TRM_ACD_ID, Session("TermDuration"), transaction, ViewState("MainMnu_code"), Session("sUsr_name"), False)
                        If bDeleteAll AndAlso status = 0 Then status = AccessStudentClass.DeleteTermWeeks(NEW_TRM_ID, TRM_ACD_ID, transaction, ViewState("MainMnu_code"), Session("sUsr_name"))
                        If status = 0 Then status = AccessStudentClass.SaveTermWeeks(NEW_TRM_ID, TRM_ACD_ID, Session("TermWeeks"), transaction, ViewState("MainMnu_code"), Session("sUsr_name"), False)
                        'throw error if insert to the TRM_M is not Successfully
                        If status = 708 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        ElseIf status = 709 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        ElseIf status = 782 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        ElseIf status <> 0 Then
                            Throw New ArgumentException("Record could not be Inserted")
                        End If


                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), temp, "Insert", Page.User.Identity.Name.ToString)

                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                        transaction.Commit()

                        'lblError.Text = "Record Inserted Successfully"
                        usrMessageBar.ShowNotification("Record Inserted Successfully", UserControls_usrMessageBar.WarningType.Success)

                        ViewState("datamode") = "none"

                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                        Call clearRecords()
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        'lblError.Text = myex.Message
                        usrMessageBar.ShowNotification(myex.Message, UserControls_usrMessageBar.WarningType.Danger)
                    Catch ex As Exception
                        transaction.Rollback()
                        'lblError.Text = "Record could not be Inserted"
                        usrMessageBar.ShowNotification("Record could not be Inserted", UserControls_usrMessageBar.WarningType.Danger)

                    End Try
                End Using

            ElseIf ViewState("datamode") = "edit" Then
                bEdit = True

                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        'call the class to insert the record into TRM_M
                        TRM_ID = ViewState("viewid")
                        Dim NEW_TRM_ID As Integer = 0
                        'throw error if insert to the TRM_M is not Successfully
                        Dim bEditOnlyWeeks As Boolean = ViewState("EditOnlyWeeks")
                        If Not bEditOnlyWeeks Then
                            status = AccessStudentClass.SaveTRM_M(TRM_ID, TRM_ACD_ID, TRM_BSU_ID, TRM_DESCRIPTION, TRM_STARTDATE, TRM_ENDDATE, vWeeksCount, bEdit, NEW_TRM_ID, transaction, TRM_ACTUALSTARTDATE)
                            Dim bDeleteAll As Boolean = IIf(ViewState("bDeleteAllTerms_Months") Is Nothing, False, ViewState("bDeleteAllTerms_Months"))
                            If bDeleteAll AndAlso status = 0 Then status = AccessStudentClass.DeleteTermMonths(NEW_TRM_ID, TRM_ACD_ID, transaction, ViewState("MainMnu_code"), Session("sUsr_name"))
                            If status = 0 Then status = AccessStudentClass.SaveTermMonths(NEW_TRM_ID, TRM_ACD_ID, Session("TermDuration"), transaction, ViewState("MainMnu_code"), Session("sUsr_name"), Not bDeleteAll)
                            If bDeleteAll AndAlso status = 0 Then status = AccessStudentClass.DeleteTermWeeks(NEW_TRM_ID, TRM_ACD_ID, transaction, ViewState("MainMnu_code"), Session("sUsr_name"))
                            If status = 0 Then status = AccessStudentClass.SaveTermWeeks(NEW_TRM_ID, TRM_ACD_ID, Session("TermWeeks"), transaction, ViewState("MainMnu_code"), Session("sUsr_name"), Not bDeleteAll)
                        Else
                            Dim sql As String
                            sql = "update TRM_M SET TRM_ACTUALSTARTDATE ='" & TRM_ACTUALSTARTDATE & "' WHERE TRM_ID=" & TRM_ID.ToString
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sql)
                            If status = 0 Then status = AccessStudentClass.SaveTermWeeks(TRM_ID, TRM_ACD_ID, Session("TermWeeks"), transaction, ViewState("MainMnu_code"), Session("sUsr_name"), True)
                        End If
                        If status <> 0 Then
                            Select Case status
                                Case 708, 709, 782
                                    Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                                Case Else
                                    Throw New ArgumentException("Record could not be Inserted")
                            End Select
                        End If

                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), TRM_ID, "edit", Page.User.Identity.Name.ToString, Me.Page)

                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                        transaction.Commit()

                        'lblError.Text = "Record Updated Successfully"
                        usrMessageBar.ShowNotification("Record Updated Successfully", UserControls_usrMessageBar.WarningType.Success)

                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        Call clearRecords()
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        'lblError.Text = myex.Message
                        usrMessageBar.ShowNotification(myex.Message, UserControls_usrMessageBar.WarningType.Danger)
                    Catch ex As Exception
                        transaction.Rollback()
                        'lblError.Text = "Record could not be Updated"
                        usrMessageBar.ShowNotification("Record could not be Updated", UserControls_usrMessageBar.WarningType.Danger)

                    End Try
                End Using

            End If

        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearRecords()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"
            setbutton()
            txtFrom_date.Attributes.Add("readonly", "readonly")
            txtTo_date.Attributes.Add("readonly", "readonly")
            txtTermDesc.Attributes.Add("readonly", "readonly")
            txtActualStartDate.Attributes.Add("readonly", "readonly")

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                'delete needs to be modified based on the trigger
                ' Status = AccessStudentClass.DeleteACADEMICYEAR_D(ViewState("viewid"), transaction)


                If Status = -1 Then

                    Throw New ArgumentException("Record does not exist for deleting")
                ElseIf Status <> 0 Then


                    Throw New ArgumentException("Record could not be Deleted")

                Else

                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)


                    If Status <> 0 Then


                        Throw New ArgumentException("Could not complete your request")

                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    transaction.Commit()
                    Call clearRecords()
                    'lblError.Text = "Record Deleted Successfully"
                    usrMessageBar.ShowNotification("Record Deleted Successfully", UserControls_usrMessageBar.WarningType.Success)

                End If

            Catch myex As ArgumentException
                'lblError.Text = myex.Message
                usrMessageBar.ShowNotification(myex.Message, UserControls_usrMessageBar.WarningType.Danger)
                UtilityObj.Errorlog(myex.Message)
                transaction.Rollback()

            Catch ex As Exception
                'lblError.Text = "Record could not be Deleted"
                usrMessageBar.ShowNotification("Record could not be Deleted", UserControls_usrMessageBar.WarningType.Danger)
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
            End Try
        End Using
    End Sub
    Protected Sub txtFrom_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrom_date.TextChanged
        Dim strfDate As String = txtFrom_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            'lblError.Text = str_err
            usrMessageBar.ShowNotification(str_err, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            txtFrom_date.Text = strfDate
        End If
    End Sub

    Protected Sub txtTo_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo_date.TextChanged
        Dim strfDate As String = txtTo_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            'lblError.Text = str_err
            usrMessageBar.ShowNotification(str_err, UserControls_usrMessageBar.WarningType.Danger)
            Exit Sub
        Else
            txtTo_date.Text = strfDate
        End If
    End Sub
    Protected Sub cvTodate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTodate.ServerValidate
        Dim sflag As Boolean = False

        Dim DateTime2 As Date
        Dim dateTime1 As Date
        Try
            DateTime2 = Date.ParseExact(txtTo_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            dateTime1 = Date.ParseExact(txtFrom_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date

            If IsDate(DateTime2) Then

                If DateTime2 < dateTime1 Then
                    sflag = False
                Else
                    sflag = True
                End If
            End If
            If sflag Then
                args.IsValid = True
            Else
                'CustomValidator1.Text = ""
                args.IsValid = False
            End If
        Catch
            args.IsValid = False
        End Try
    End Sub

    Protected Sub cvDateFrom_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvDateFrom.ServerValidate
        Dim sflag As Boolean = False
        Dim dateTime1 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            dateTime1 = Date.ParseExact(txtFrom_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(dateTime1) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ViewState("WEEK_COUNT") = 1
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    If ViewState("datamode") = "view" Then
                        lnkAddTerm.Visible = False
                        txtAcadYear.Attributes.Add("readonly", "readonly")
                        txtFrom_date.Attributes.Add("readonly", "readonly")
                        txtActualStartDate.Attributes.Add("readonly", "readonly")
                        txtTo_date.Attributes.Add("readonly", "readonly")
                        txtTermDesc.Attributes.Add("readonly", "readonly")
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        Using readerTerm_M As SqlDataReader = AccessStudentClass.GetDetailsTERM_M(ViewState("viewid"))
                            While readerTerm_M.Read
                                'handle the null value returned from the reader incase  convert.tostring
                                hfTRM_ID.Value = Convert.ToString(readerTerm_M("TRM_ID"))
                                hfACD_ID.Value = Convert.ToString(readerTerm_M("ACD_ID"))
                                txtTermDesc.Text = Convert.ToString(readerTerm_M("TRM_DESCR"))
                                txtAcadYear.Text = Convert.ToString(readerTerm_M("Y_DESCR"))
                                txtFrom_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerTerm_M("STARTDT"))))
                                txtTo_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerTerm_M("ENDDT"))))
                                txtActualStartDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerTerm_M("TRM_ACTUALSTARTDATE"))))
                            End While
                            setbutton()
                            gvDuration.DataSource = GetDataSetforGrid(AccessStudentClass.Fill_TRM_DETAILS(ViewState("viewid")))
                            gvDuration.DataBind()
                            gvWeek.DataSource = GetDataSetWeekforGrid(AccessStudentClass.Fill_TRM_WEEKS(ViewState("viewid")))
                            gvWeek.DataBind()
                        End Using
                    ElseIf ViewState("datamode") = "add" Then
                        callActive_year()
                        txtFrom_date.Attributes.Remove("readonly")
                        txtTo_date.Attributes.Remove("readonly")
                        txtActualStartDate.Attributes.Remove("readonly")
                        txtTermDesc.Attributes.Remove("readonly")
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Sub callActive_year()
        Using readerActive_year As SqlDataReader = AccessStudentClass.GetActive_Year(Session("sBsuid"), Session("CLM"))
            While readerActive_year.Read

                txtAcadYear.Text = Convert.ToString(readerActive_year("Y_DESCR"))
                hfACD_ID.Value = Convert.ToString(readerActive_year("ACD_ID"))
            End While

        End Using
    End Sub

    Protected Function GetDataSetWeekforGrid(ByVal ds As DataSet) As DataView
        Session("termWeeks") = CreateTermWeekTable()
        Dim dtTerm As DataTable = Session("termWeeks")
        If ds Is Nothing OrElse ds.Tables(0).Rows.Count <= 0 Then
            Return Nothing
        End If
        Dim ldrTempNew As DataRow
        Dim i As Integer = 1
        For Each dr As DataRow In ds.Tables(0).Rows
            ldrTempNew = dtTerm.NewRow
            ldrTempNew("UniqueID") = dr("AWS_ID")
            ldrTempNew("WEEK_ID") = i 'dr("AWS_ID")
            ldrTempNew("WEEK_DESCR") = i
            ldrTempNew("FromDate") = dr("AWS_DTFROM")
            ldrTempNew("ToDate") = dr("AWS_DTTO")
            ldrTempNew("bDelete") = False
            dtTerm.Rows.Add(ldrTempNew)
            i += 1
        Next

        Dim dv As DataView = New DataView(dtTerm)
        dv.Sort = "FromDate"
        Return dv
    End Function

    Protected Function GetDataSetforGrid(ByVal ds As DataSet) As DataView
        Session("TermDuration") = CreateTermDurationTable()
        Dim dtTerm As DataTable = Session("TermDuration")
        If ds Is Nothing OrElse ds.Tables(0).Rows.Count <= 0 Then
            Return Nothing
        End If
        Dim ldrTempNew As DataRow
        For Each dr As DataRow In ds.Tables(0).Rows
            ldrTempNew = dtTerm.NewRow
            ldrTempNew("UniqueID") = dr("AMS_ID")
            ldrTempNew("ORD_ID") = dr("AMS_ORDER")
            ldrTempNew("Month_ID") = dr("AMS_MONTH")
            ldrTempNew("MONTH_DESCR") = GetMonth(dr("AMS_MONTH"))
            ldrTempNew("FromDate") = dr("AMS_DTFROM")
            ldrTempNew("ToDate") = dr("AMS_DTTO")
            dtTerm.Rows.Add(ldrTempNew)
        Next

        Dim dv As DataView = New DataView(dtTerm)
        dv.Sort = "FromDate"
        Return dv
    End Function

    Protected Sub lnkAddTerm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTerm.Click
        FillTermMonths()
        FillTermWeeks()
        ViewState("bDeleteAllTerms_Months") = True
    End Sub

    Private Sub FillTermWeeks()
        Session("TermWeeks") = CreateTermWeekTable()
        Dim dtTerm As DataTable = Session("TermWeeks")
        Dim frmdt As DateTime = txtFrom_date.Text
        Dim todt As DateTime = txtTo_date.Text
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim cmd As New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            cmd.Parameters.AddWithValue("@FROMDT", frmdt)
            cmd.Parameters.AddWithValue("@TODT", todt)
            cmd.Parameters.AddWithValue("@BSU_ID", Session("sBSUID"))
            cmd.Parameters.AddWithValue("@ACD_ID", Session("Current_ACD_ID"))
            cmd.CommandText = "[FEES].[F_GetWEEKSForPeriod]"
            Dim drRead As SqlDataReader = cmd.ExecuteReader()
            Dim ldrTempNew As DataRow
            While (drRead.Read())
                ldrTempNew = dtTerm.NewRow
                ldrTempNew("UniqueID") = dtTerm.Rows.Count + 1
                ldrTempNew("WEEK_ID") = drRead("Id")
                ldrTempNew("WEEK_DESCR") = drRead("Id")
                ldrTempNew("FromDate") = Format(drRead("FRMDT"), OASISConstants.DateFormat)
                ldrTempNew("ToDate") = Format(drRead("TODT"), OASISConstants.DateFormat)
                ldrTempNew("bDelete") = False
                ldrTempNew("bEdit") = False
                dtTerm.Rows.Add(ldrTempNew)
            End While
        End Using
        Dim dv As DataView = New DataView(dtTerm)
        dv.RowFilter = " bDelete = False "
        dv.Sort = "WEEK_ID"
        gvWeek.DataSource = dv
        gvWeek.DataBind()
    End Sub

    Private Sub FillTermMonths()
        Session("TermDuration") = CreateTermDurationTable()
        Dim dtTerm As DataTable = Session("TermDuration")
        Dim frmdt As DateTime = txtFrom_date.Text
        Dim todt As DateTime = txtTo_date.Text
        Dim tempdt As DateTime = txtTo_date.Text
        Dim ldrTempNew As DataRow
        While (frmdt < todt)
            ldrTempNew = dtTerm.NewRow
            ldrTempNew("UniqueID") = dtTerm.Rows.Count + 1
            ldrTempNew("ORD_ID") = dtTerm.Rows.Count + 1
            ldrTempNew("Month_ID") = frmdt.Month
            ldrTempNew("MONTH_DESCR") = GetMonth(frmdt.Month)
            ldrTempNew("FromDate") = frmdt
            ldrTempNew("ToDate") = frmdt.AddDays(DateTime.DaysInMonth(frmdt.Year, frmdt.Month) - frmdt.Day)
            ldrTempNew("bDelete") = False
            ldrTempNew("bEdit") = False
            If frmdt.AddMonths(1) > todt Then
                ldrTempNew("ToDate") = todt
            End If
            frmdt = ldrTempNew("ToDate").AddDays(1)
            dtTerm.Rows.Add(ldrTempNew)
        End While

        Dim dv As DataView = New DataView(dtTerm)
        dv.Sort = "FromDate"
        gvDuration.DataSource = dv
        gvDuration.DataBind()
    End Sub

    Private Function GetMonth(ByVal month_id As Integer) As String
        Select Case month_id
            Case 1
                Return "January"
            Case 2
                Return "February"
            Case 3
                Return "March"
            Case 4
                Return "April"
            Case 5
                Return "May"
            Case 6
                Return "June"
            Case 7
                Return "July"
            Case 8
                Return "August"
            Case 9
                Return "September"
            Case 10
                Return "October"
            Case 11
                Return "November"
            Case 12
                Return "December"
        End Select
        Return ""
    End Function

    Private Function CheckForDuplicatePeriod(ByVal FromDate As DateTime, ByVal ToDate As DateTime, ByVal dttab As DataTable) As Boolean
        Dim dtFromDate As DateTime = FromDate
        Dim dtToDate As DateTime = ToDate
        Dim dtFromDateChk As DateTime
        Dim dtToDateChk As DateTime

        For Each dr As DataRow In dttab.Rows
            dtFromDateChk = dr("FromDate")
            dtToDateChk = dr("ToDate")
            If dtFromDate > dtFromDateChk AndAlso dtFromDate <= dtToDateChk Then
                Return False
            ElseIf dtFromDate < dtFromDateChk AndAlso dtFromDate > dtToDateChk Then
                Return False
                'ElseIf dtToDate <= dtToDateChk Then
                '    Return False
            End If
        Next
        Return True
    End Function

    Private Function CreateTermDurationTable() As DataTable
        Dim dt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cOrderID As New DataColumn("ORD_ID", System.Type.GetType("System.Int32"))
            Dim cMonthID As New DataColumn("MONTH_ID", System.Type.GetType("System.Int32"))
            Dim cMonthDesc As New DataColumn("MONTH_DESCR", System.Type.GetType("System.String"))
            Dim cFromDate As New DataColumn("FromDate", System.Type.GetType("System.DateTime"))
            Dim cToDate As New DataColumn("ToDate", System.Type.GetType("System.DateTime"))
            Dim cbEdit As New DataColumn("bEdit", System.Type.GetType("System.Boolean"))
            Dim cbDelete As New DataColumn("bDelete", System.Type.GetType("System.Boolean"))
            cUniqueID.Unique = True

            dt.Columns.Add(cUniqueID)
            dt.Columns.Add(cOrderID)
            dt.Columns.Add(cMonthID)
            dt.Columns.Add(cMonthDesc)
            dt.Columns.Add(cFromDate)
            dt.Columns.Add(cToDate)
            dt.Columns.Add(cbEdit)
            dt.Columns.Add(cbDelete)

            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
        Return Nothing
    End Function

    Private Function CreateTermWeekTable() As DataTable
        Dim dt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cWEEKID As New DataColumn("WEEK_ID", System.Type.GetType("System.Int32"))
            Dim cWEEKDesc As New DataColumn("WEEK_DESCR", System.Type.GetType("System.String"))
            Dim cFromDate As New DataColumn("FromDate", System.Type.GetType("System.DateTime"))
            Dim cToDate As New DataColumn("ToDate", System.Type.GetType("System.DateTime"))
            Dim cbEdit As New DataColumn("bEdit", System.Type.GetType("System.Boolean"))
            Dim cbDelete As New DataColumn("bDelete", System.Type.GetType("System.Boolean"))
            cUniqueID.Unique = True
            dt.Columns.Add(cUniqueID)
            dt.Columns.Add(cWEEKID)
            dt.Columns.Add(cWEEKDesc)
            dt.Columns.Add(cFromDate)
            dt.Columns.Add(cToDate)
            dt.Columns.Add(cbEdit)
            dt.Columns.Add(cbDelete)

            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
        Return Nothing
    End Function

    Protected Sub lnkbtnDurationDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblUniqueID As New Label
        lblUniqueID = TryCast(sender.FindControl("lblUniqueID"), Label)
        If Not lblUniqueID Is Nothing Then
            If Not Session("TermDuration") Is Nothing Then
                Dim dtTermDuration As DataTable = Session("TermDuration")
                Dim i As Integer
                For i = 0 To dtTermDuration.Rows.Count - 1
                    If dtTermDuration.Rows(i)(0) = lblUniqueID.Text Then
                        dtTermDuration.Rows.RemoveAt(i)
                        Exit For
                    End If
                Next
                Session("TermDuration") = dtTermDuration
                Dim dv As DataView = New DataView(dtTermDuration)
                dv.Sort = "FromDate"
                dv.Sort = " bDelete = False "
                gvDuration.DataSource = dv
                gvDuration.DataBind()
            End If
        End If
    End Sub

    Protected Sub gvDuration_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        'If TypeOf e.Row.FindControl("txtFromDate") Is TextBox Then
        '    Dim txtFromDate As TextBox = e.Row.FindControl("txtFromDate")
        '    txtFromDate.Attributes.Add("onBlur", "checkdate(this)")
        '    If TypeOf e.Row.FindControl("imgBtnFrom_date") Is ImageButton Then
        '        Dim imgBtnFrom_date As ImageButton = e.Row.FindControl("imgBtnFrom_date")
        '        imgBtnFrom_date.OnClientClick = "getDateGrid('" & txtFromDate.ClientID & "','1')"
        '    End If
        'End If
        'If TypeOf e.Row.FindControl("txtToDate") Is TextBox Then
        '    Dim txtToDate As TextBox = e.Row.FindControl("txtToDate")
        '    txtToDate.Attributes.Add("onBlur", "checkdate(this)")
        '    If TypeOf e.Row.FindControl("imgBtnTodate") Is ImageButton Then
        '        Dim imgBtnTodate As ImageButton = e.Row.FindControl("imgBtnTodate")
        '        imgBtnTodate.OnClientClick = "getDateGrid('" & txtToDate.ClientID & "','1')"
        '    End If
        'End If
    End Sub

    Protected Sub lnkAddWeek_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AddDeleteWeeks(sender, True)
    End Sub

    Private Sub AddDeleteWeeks(ByVal sender As Object, ByVal bAdd As Boolean)
        UpdateWeekDataTable()
        Dim dttab As DataTable = Session("TermWeeks")
        Dim grRow As GridViewRow = sender.parent.parent
        Dim lblUniqueID As Label
        Dim rowStartID As Integer
        If Not grRow Is Nothing Then
            If Not grRow.FindControl("lbluniqueID") Is Nothing Then
                lblUniqueID = grRow.FindControl("lbluniqueID")
                rowStartID = 0
                Dim dr As DataRow
                For Each drow As DataRow In dttab.Rows
                    If drow("UniqueID") = lblUniqueID.Text Then
                        If IIf(drow("bDelete") Is Nothing, False, drow("bDelete")) = False Then
                            dr = drow
                            Exit For
                        End If
                    End If
                    rowStartID += 1
                Next
                If dr Is Nothing Then Return
                ' Dim incrementer As Integer = 1
                If bAdd Then
                    Dim newRow As DataRow = dttab.NewRow()
                    newRow("UniqueID") = GetNextNo(dttab)
                    newRow("WEEK_ID") = dr("WEEK_ID") + 1
                    newRow("WEEK_DESCR") = dr("WEEK_ID") + 1
                    newRow("FromDate") = Format(dr("FromDate"), OASISConstants.DateFormat)
                    newRow("ToDate") = Format(dr("ToDate"), OASISConstants.DateFormat)
                    newRow("bDelete") = False
                    dttab.Rows.InsertAt(newRow, rowStartID)
                    'incrementer = 1
                Else
                    dr("bDelete") = True
                    'dttab.Rows.RemoveAt(rowStartID - 1)
                    'incrementer = -1
                    'rowStartID -= 2
                End If
                'For i As Integer = rowStartID + 1 To dttab.Rows.Count - 1
                '    dr = dttab.Rows(i)
                '    If IIf(dr("bDelete") Is Nothing, False, dr("bDelete")) = True Then
                '        Continue For
                '    End If
                '    dr("WEEK_DESCR") = dr("WEEK_ID") + incrementer
                '    dr("WEEK_ID") = dr("WEEK_ID") + incrementer
                'Next
            End If
            Session("TermWeeks") = dttab
            Dim dv As DataView = New DataView(dttab)
            dv.Sort = "FromDate"
            dv.RowFilter = " bDelete = False "
            gvWeek.DataSource = dv
            gvWeek.DataBind()
        End If
    End Sub

    Private Function GetNextNo(ByVal dttab As DataTable) As Integer
        Dim dv As DataView = New DataView(dttab)
        dv.Sort = "UniqueID"
        Dim maxID As Integer = 0
        For Each dr As DataRow In dttab.Rows
            If dr("UniqueID") > maxID Then
                maxID = dr("UniqueID")
            End If
        Next
        Return maxID + 1
    End Function

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AddDeleteWeeks(sender, False)
    End Sub

    Protected Sub gvWeek_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvWeek.RowDataBound
        Dim lblWeekDescr As Label = e.Row.FindControl("lblWeekDescr")
        If Not lblWeekDescr Is Nothing Then
            lblWeekDescr.Text = ViewState("WEEK_COUNT")
            ViewState("WEEK_COUNT") += 1
        End If
    End Sub
End Class



