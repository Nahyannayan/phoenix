﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="encEmailText_M.aspx.vb" Inherits="Students_encEmailText_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Email Text
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:LinkButton ID="lnkadd" runat="server" Text="Add New"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr align="left">
                        <td width="20%"><span class="field-label">Business Unit</span>
                        </td>

                        <td>
                            <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td width="20%"><span class="field-label">Academic Year</span>
                        </td>

                        <td width="15%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr align="left" id="tr_Status" runat="server">
                        <td width="20%"><span class="field-label">Grade</span>
                        </td>

                        <td>
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true"></asp:DropDownList>

                        </td>
                        <td width="20%"><span class="field-label">Stream</span>
                        </td>

                        <td>
                            <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="Tr1" align="left" runat="server">
                        <td width="20%"><span class="field-label">Subject</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <%--<table id="Table2" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td>--%>
                            <asp:GridView ID="gvEmail" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" BorderStyle="None">
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblEMTID" runat="server" Text='<%# Bind("EMT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("EMT_GRD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Stream">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStream" runat="server" Text='<%# Bind("STM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Subject">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("ESM_SUBJECT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:ButtonField CommandName="view" HeaderText="View" Text="View">
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>
                                    <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="delete"
                                                Text="Delete"></asp:LinkButton>
                                            <ajaxToolkit:ConfirmButtonExtender ID="c1" TargetControlID="LinkButton1" ConfirmText="Selected item will be deleted permanently.Are you sure you want to continue?"
                                                runat="server">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />
                                <RowStyle CssClass="griditem" Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <EditRowStyle Wrap="False" />
                            </asp:GridView>
                            <%-- </td>
                                </tr>
                            </table>--%>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

