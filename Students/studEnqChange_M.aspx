<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnqChange_M.aspx.vb" Inherits="Students_studEnqChange_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudEnquiry.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}

    </script>
    <style>
        input {
            vertical-align :middle !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Change Enquiry
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left"   valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td >
                                        <table align="left" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                              
                                                <td align="left"  >

                                                    <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left"  width="20%"><span class="field-label">Select Grade</span> </td>
                                                
                                                <td align="left">

                                                    <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Select Shift</span> </td>
                                               
                                                <td align="left">

                                                    <asp:DropDownList ID="ddlShift" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left"  width="20%"><span class="field-label">Select Stream</span> </td>
                                                
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlStream" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                                    </asp:DropDownList>

                                                </td>

                                            </tr>
                                            <tr>
                                                <td  colspan="4" align="center">
                                                    <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" ValidationGroup="groupM1" TabIndex="4" Height="27px" Width="58px" />
                                                </td>

                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <table align="center" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left"  >
                                                    <asp:CheckBox runat="server" ID="chkAcademicYear" Text="Change AcademicYear"  AutoPostBack="True" Enabled="False" CssClass="field-label"></asp:CheckBox>
                                                </td>
                                                <td align="left"  >
                                                    <asp:CheckBox runat="server" ID="chkGrade" Text="Change Grade"  AutoPostBack="True" Enabled="False" CssClass="field-label"></asp:CheckBox></td>
                                                <td align="left"  >
                                                    <asp:CheckBox runat="server" ID="chkShift" Text="Change Shift"  AutoPostBack="True" Enabled="False" CssClass="field-label"></asp:CheckBox></td>
                                                <td  align="left" >
                                                    <asp:CheckBox runat="server" ID="chkStream" Text="Change Stream"  Style="text-align: left" AutoPostBack="True" Enabled="False" CssClass="field-label"></asp:CheckBox></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  >&nbsp; &nbsp; &nbsp;
                                        <asp:DropDownList ID="ddlCAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True" Enabled="False"  >
                                        </asp:DropDownList>
                                                </td>
                                                <td align="left"  >&nbsp; &nbsp; &nbsp;
                                        <asp:DropDownList ID="ddlCGrade" SkinID="smallcmb" runat="server" AutoPostBack="True" Enabled="False"  >
                                        </asp:DropDownList></td>
                                                <td align="left"  >&nbsp; &nbsp; &nbsp;<asp:DropDownList ID="ddlCShift" SkinID="smallcmb" runat="server" AutoPostBack="True" Enabled="False"  >
                                                </asp:DropDownList></td>
                                                <td align="left"  >&nbsp; &nbsp; &nbsp;
                                        <asp:DropDownList ID="ddlCStream" SkinID="smallcmb" runat="server" Enabled="False" >
                                        </asp:DropDownList></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  valign="top">
                                        <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            HeaderStyle-Height="30" PageSize="20" >
                                            <RowStyle CssClass="griditem"  />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <%--<table>
                                                            <tr>
                                                                <td align="center">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                   </td>
                                                            </tr>
                                                        </table>--%>
                                                        Select
                                                                    <br />
                                                                     <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("Eqs_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqId" runat="server" Text='<%# Bind("eqm_enqid") %>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Enq No">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqHeader" runat="server" CssClass="gridheader_text" Text="Enq No"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqSearch" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnqid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnqid_Search_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqNo" runat="server" Text='<%# Bind("eqs_applno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Enq Date">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" CssClass="gridheader_text" Text="Enq.Date"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqDate" runat="server" Width="60px"></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnq_Date" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnq_Date_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("eqm_enqdate", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>

                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Shift">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stream">

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>

                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Applicant Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblApplName" runat="server" CssClass="gridheader_text" Text="Applicant Name"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtApplName" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnAppl_Name" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnAppl_Name_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblApplName" runat="server" Text='<%# Bind("appl_name") %>'></asp:Label>

                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>


                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle  CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnChange" runat="server" Text="Update" CssClass="button" ValidationGroup="groupM1" TabIndex="4"  />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>

                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server"
                    type="hidden" value="=" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfSHF_ID" runat="server" />
                <asp:HiddenField ID="hfSTM_ID" runat="server" />


            </div>
        </div>
    </div> 


</asp:Content>

