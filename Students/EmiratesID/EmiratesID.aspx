﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EmiratesID.aspx.vb" Inherits="Students_EmiratesID_EmiratesID" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 <script src="../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
 <script type="text/javascript">
     $(document).ready(function () {

         var iframe = document.getElementById("ifEmiratesID");
         var bsuId = '<%= Session("sBsuid") %>';
         var clmId = '<%= Session("CLM") %>';
         var currentAcd = '<%= Session("Current_ACD_ID") %>';
         var roleID = '<%= Session("sroleid") %>';
         var usrId = '<%= Session("sUsr_name") %>';
         var params = "bsuId=" + bsuId + "&clmId=" + clmId + "&currentAcd=" + currentAcd + "&roleID=" + roleID + "&usrId=" + usrId;
         iframe.src = iframe.src + '?' + params;
     });

 </script>
    <%--https://oasis2.gemsoasis.com/EID/EmiratesID/StudEmiratesIDMain.aspx--%>
    <%--http://localhost:61347/EmiratesID/--%>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Emirates ID Sync
        </div>
        <div class="card-body">
            <div class="table-responsive ">


<%--<iframe id="ifEmiratesID" src="http://localhost:1856/Students/EmiratesID/StudEmiratesIDMain.aspx" width="950px" height="950px" frameborder="0"></iframe>--%>
 <iframe id="ifEmiratesID" src="https://school.gemsoasis.com/Students/EmiratesID/StudEmiratesIDMain.aspx" width="950px" height="950px" frameborder="0"></iframe>
                
                   </div>
        </div>
    </div>
</asp:Content>