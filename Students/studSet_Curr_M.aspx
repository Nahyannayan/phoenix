<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studSet_Curr_M.aspx.vb" Inherits="Students_studSet_Curr_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Set Curriculum
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left" >
                &nbsp;</td>
        </tr>
        <tr valign="bottom">
            <td align="left" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td >
               <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                  <%--  <tr >
                        <td align="left" >
                           <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana"> Set Curriculum</span></font></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%">
                          <span class="field-label">  Select Curriculum</span></td>
                      
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlCurr" runat="server">
                            </asp:DropDownList></td>

                          <td align="left" width="30%">
                              </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td >
            </td>
        </tr>
        <tr>
            <td >
                &nbsp;
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />&nbsp;
            </td>
        </tr>
        <tr>
            <td >
                &nbsp;
            </td>
        </tr>
    </table>

                
            </div>
        </div>
    </div>

</asp:Content>

