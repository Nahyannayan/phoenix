<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studtcClearance_Grade_View.aspx.vb" Inherits="Students_studtcClearance_Grade_View" Title="Untitled Page" %>

<script runat="server">

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">


        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
                 }

                 function test1(val) {
                     var path;
                     if (val == 'LI') {
                         path = '../Images/operations/like.gif';
                     } else if (val == 'NLI') {
                         path = '../Images/operations/notlike.gif';
                     } else if (val == 'SW') {
                         path = '../Images/operations/startswith.gif';
                     } else if (val == 'NSW') {
                         path = '../Images/operations/notstartwith.gif';
                     } else if (val == 'EW') {
                         path = '../Images/operations/endswith.gif';
                     } else if (val == 'NEW') {
                         path = '../Images/operations/notendswith.gif';
                     }
                     document.getElementById("<%=getid7()%>").src = path;
                document.getElementById("<%=h_selected_menu_7.ClientID %>").value = val + '__' + path;
                  }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>TC Clearance - Grade
        </div>
        <div class="card-body">
            <div class="table-responsive ">
    <table id="tbl_ShowScreen" runat="server" align="center" border="0" 
        cellpadding="5" cellspacing="0" style="width: 100%">

        

        <tr >
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>




        <tr>
            <td align="center" colspan="8">

                <table id="Table1" runat="server" align="center" border="0" 
                    cellpadding="5" cellspacing="0" style="width: 100%">



                    <tr>
                        <td colspan="3" align="center" >

                            <table id="Table2" runat="server" align="center" border="0" 
                                cellpadding="5" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                   
                                    <td align="left" width="30%"  >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" >
                                        </asp:DropDownList></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="left"  >
                                        <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="3" align="center" style="width: 100%">

                                        <asp:GridView ID="gvClearance" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20" Width="100%" BorderStyle="None">
                                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />


                                            <Columns>

                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTcgId" runat="server" Text='<%# Bind("TCG_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTccId" runat="server" Text='<%# Bind("TCC_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="-" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStmId" runat="server" Text='<%# Bind("STM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="sbm_id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("GRM_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Clearance Type">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblopt" runat="server" CssClass="gridheader_text" Text="Clearance Type"></asp:Label><br />
                                                                
                                                                                <asp:TextBox ID="txtClearance" runat="server" Width="60px"></asp:TextBox>
                                                                                    <asp:ImageButton ID="btnClearance_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnClearance_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblClearance" runat="server" Text='<%# Bind("TCC_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label><br />
                                                                                <asp:TextBox ID="txtGrade" runat="server" Width="60px"></asp:TextBox>
                                                                                    <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stream">

                                                    <HeaderTemplate>
                                                        Stream<br />
                                                        <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                        OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged" Width="70px">
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                     <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                </asp:TemplateField>


                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px"></ItemStyle>
                                                </asp:ButtonField>
                                            </Columns>

                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>

                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>


     </div>
            </div>
        </div>


    <script type="text/javascript">

        cssdropdown.startchrome("Div2")

    </script>


</asp:Content>

