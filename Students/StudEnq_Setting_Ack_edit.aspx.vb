Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_StudEnq_Setting_Ack_edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100064") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else


                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("DOC_LIST") = CreateDataTable()
                    Session("DOC_LIST").Rows.Clear()
                    ViewState("id") = 1
                    BSU_YEAR()
                    bindAcademic_Grade()
                    bindOther_Grade()

                    If ViewState("datamode") = "view" Then
                        ViewState("ChangeEvent") = 2
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        Call bindData()
                        disable_control()
                        gvDoc.Enabled = False


                    ElseIf ViewState("datamode") = "add" Then
                        gridbind()
                        enable_control()
                        reset_state()

                        ViewState("ChangeEvent") = 1
                        gvDoc.Enabled = True
                    End If
                   
                   
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
        If ViewState("ChangeEvent") = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>ShowOthGrade(1);</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                               "<script language=javascript>ShowOthGrade(2);</script>")
        End If
       
    End Sub
    Sub bindData()

        Using readerAck_Data As SqlDataReader = AccessStudentClass.GetACK_MAIN_VIEW(ViewState("viewid"))



            While readerAck_Data.Read
                If Not ddlAcdYear.Items.FindByValue(Convert.ToString(readerAck_Data("EAS_ACD_ID"))) Is Nothing Then
                    ddlAcdYear.ClearSelection()
                    ddlAcdYear.Items.FindByValue(Convert.ToString(readerAck_Data("EAS_ACD_ID"))).Selected = True
                    ddlAcdYear_SelectedIndexChanged(ddlAcdYear, Nothing)
                End If
                If Not ddlGrade.Items.FindByValue(Convert.ToString(readerAck_Data("EAS_GRD_ID"))) Is Nothing Then
                    ddlGrade.ClearSelection()
                    ddlGrade.Items.FindByValue(Convert.ToString(readerAck_Data("EAS_GRD_ID"))).Selected = True
                    ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
                End If


                chkMessage.Checked = Convert.ToBoolean(readerAck_Data("EAS_MAIN_bSHOW"))
                txtMessage.Text = Convert.ToString(readerAck_Data("EAS_TEXT_MAIN"))
                chkDoc.Checked = Convert.ToBoolean(readerAck_Data("EAS_SUB_bSHOW"))
                chkBulletPoints.Checked = Convert.ToBoolean(readerAck_Data("EAS_bBULLET_POINTS"))
            End While
        End Using

        Dim gvDataset As DataSet = AccessStudentClass.GetACK_SUB_VIEW(ViewState("viewid"))

        If gvDataset.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To gvDataset.Tables(0).Rows.Count - 1
                Dim rDt As DataRow
                rDt = Session("DOC_LIST").NewRow()
                rDt("ID") = ViewState("id")
                rDt("EAD_ID") = Convert.ToString(gvDataset.Tables(0).Rows(i)("EAD_ID"))
                rDt("DIS_ORDER") = Convert.ToString(gvDataset.Tables(0).Rows(i)("DIS_ORDER"))
                rDt("DOCLIST") = Convert.ToString(gvDataset.Tables(0).Rows(i)("DOCLIST"))
                rDt("DOC_STATUS") = "OLD"
                ViewState("id") = CInt(ViewState("id")) + 1
                Session("DOC_LIST").Rows.Add(rDt)
            Next

        End If
        gvDoc.DataSource = Session("DOC_LIST")
        gvDoc.DataBind()

    End Sub
    Sub BSU_YEAR()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim CLM_ID As String = Session("CLM")
        ddlAcdYear.ClearSelection()

        str_Sql = "SELECT  ACD_ID,ACY_DESCR FROM ACADEMICYEAR_D WITH(NOLOCK) INNER JOIN " & _
" ACADEMICYEAR_M WITH(NOLOCK)  ON ACD_ACY_ID= ACY_ID where ACD_BSU_ID='" & Session("sBsuid") & "'  AND ACD_CLM_ID = '" & Session("CLM") & "' order by ACY_ID"

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If DATAREADER.HasRows = True Then

                While DATAREADER.Read
                    ddlAcdYear.Items.Add(New ListItem(Convert.ToString(DATAREADER("ACY_DESCR")), Convert.ToString(DATAREADER("ACD_ID"))))

                End While
            End If

            ' ddlAcdYear.DataBind()
        End Using

        If Not ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True

        End If

    End Sub
    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim ds As New DataSet
            str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGrade.Items.Clear()
            ddlGrade.DataSource = ds.Tables(0)
            ddlGrade.DataTextField = "GRM_DISPLAY"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindOther_Grade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds1 As New DataSet
        Dim str_Sql As String
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim GRD_ID As String = ddlGrade.SelectedItem.Value
        str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  as GRM_DESCR ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where  GRADE_BSU_M.GRM_GRD_ID<>'" & GRD_ID & "' AND GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

        ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
      
        chkOthGrade.Items.Clear()
        Dim row As DataRow


        If ds1.Tables(0).Rows.Count > 0 Then
           
            ViewState("ChangeEvent") = 1
           
            For Each row In ds1.Tables(0).Rows

                Dim str1 As String = row("GRM_DESCR")
                Dim str2 As String = row("GRD_ID")
                chkOthGrade.Items.Add(New ListItem(str1, str2))

            Next

        Else
            ViewState("ChangeEvent") = 2
          
        End If

       

    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindOther_Grade()
       
        If ViewState("ChangeEvent") = 1 Then

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                        "<script language=javascript>ShowOthGrade(1);</script>")
        Else
            '2 is to hide
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                    "<script language=javascript>ShowOthGrade(2);</script>")
        End If

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            disable_control()
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call reset_state()
                gvDoc.Enabled = False

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try


    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gvDoc.Enabled = True
        If ViewState("viewid") = 0 Then
            lblError.Text = "No records to delete"
            Exit Sub
        End If
        Dim str_err As String = String.Empty
        Dim DeleteMessage As String = String.Empty
        str_err = callDelete(DeleteMessage)
        If str_err = "0" Then
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            lblError.Text = "Record Deleted Successfully"
            reset_state()
            disable_control()
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                    "<script language=javascript>ShowOthGrade(2);</script>")
        Else
            lblError.Text = DeleteMessage
        End If



    End Sub

    Function callDelete(ByRef DeleteMessage As String) As Integer
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'delete needs to be modified based on the trigger

                Status = AccessStudentClass.DeleteENQ_ACK(ViewState("viewid"), transaction)


                If Status <> 0 Then
                    callDelete = "1"
                    DeleteMessage = "Error Occured While Deleting."
                    Return "1"

                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        callDelete = "1"
                        DeleteMessage = "Could not complete your request"
                        Return "1"

                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("viewid") = 0

                End If
            Catch ex As Exception
                callDelete = "1"
                DeleteMessage = "Error occured while Deleting the record."
            Finally
                If callDelete <> "0" Then
                    UtilityObj.Errorlog(DeleteMessage)
                    transaction.Rollback()
                Else
                    DeleteMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gvDoc.Enabled = True
        enable_control()
        If ViewState("ChangeEvent") = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>ShowOthGrade(1);</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                               "<script language=javascript>ShowOthGrade(2);</script>")
        End If
        ViewState("datamode") = "add"
        Call reset_state()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        gvDoc.Enabled = True
        enable_control()
        ViewState("ChangeEvent") = 2
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                   "<script language=javascript>ShowOthGrade(2);</script>")
        ViewState("datamode") = "edit"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"
            reset_state()
            Call disable_control()
            gvDoc.Enabled = False

            Call bindData()

        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim EAS_ID As String = "0"
        Dim NEW_EAS_ID As String = String.Empty
        Dim EAS_ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim EAS_GRD_ID As String = ddlGrade.SelectedItem.Value
        Dim GRD_IDs As String = String.Empty
        Dim EAS_BSU_ID As String = Session("sBsuid")
        Dim EAS_CLM_ID As String = Session("CLM")
        Dim EAS_TEXT_MAIN As String = txtMessage.Text
        EAS_TEXT_MAIN = HttpUtility.HtmlEncode(EAS_TEXT_MAIN)

        Dim EAS_MAIN_bSHOW As Boolean = chkMessage.Checked
        Dim EAS_SUB_bSHOW As Boolean = chkDoc.Checked
        Dim EAS_bBULLET_POINTS As Boolean = chkBulletPoints.Checked
        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Dim status As Integer
                    bEdit = False

                    GRD_IDs = EAS_GRD_ID + "|"
                    For Each item As ListItem In chkOthGrade.Items
                        If item.Selected = True Then

                            GRD_IDs += item.Value + "|"
                        End If
                    Next

                    Dim separator As String() = New String() {"|"}

                    Dim strSplitArr As String() = GRD_IDs.Split(separator, StringSplitOptions.RemoveEmptyEntries)

                    For Each arrStr As String In strSplitArr
                        EAS_GRD_ID = arrStr


                        status = AccessStudentClass.SaveSaveENQUIRY_ACK_SETTING(EAS_ID, EAS_ACD_ID, EAS_CLM_ID, EAS_GRD_ID, EAS_BSU_ID, EAS_TEXT_MAIN, EAS_MAIN_bSHOW, _
                              EAS_SUB_bSHOW, NEW_EAS_ID, EAS_bBULLET_POINTS, bEdit, transaction)
                        If status <> 0 Then
                            calltransaction = "1"
                            errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                            Return "1"
                        End If

                        If Session("DOC_LIST").Rows.Count <> 0 Then

                            For i As Integer = 0 To Session("DOC_LIST").Rows.Count - 1
                                If Session("DOC_LIST").Rows(i)("DOC_STATUS") <> "DELETED" Then
                                    status = AccessStudentClass.SaveENQUIRY_ACK_DOC(NEW_EAS_ID, Session("DOC_LIST").Rows(i)("DOCLIST"), _
                                               Session("DOC_LIST").Rows(i)("DIS_ORDER"), transaction)
                                    If status <> 0 Then
                                        calltransaction = "1"
                                        errorMessage = "Error while inserting Document List"
                                        Return "1"
                                    End If
                                End If
                            Next
                        End If

                    Next


                    ViewState("viewid") = NEW_EAS_ID
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"


                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim status As Integer

                    EAS_ID = ViewState("viewid")

                    bEdit = True
                    status = AccessStudentClass.SaveSaveENQUIRY_ACK_SETTING(EAS_ID, EAS_ACD_ID, EAS_CLM_ID, EAS_GRD_ID, EAS_BSU_ID, EAS_TEXT_MAIN, EAS_MAIN_bSHOW, _
                                EAS_SUB_bSHOW, NEW_EAS_ID, EAS_bBULLET_POINTS, bEdit, transaction)
                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If
                    status = AccessStudentClass.DeleteENQ_ACK_LIST(EAS_ID, transaction)
                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = "Error while deleting Document List  "
                        Return "1"
                    End If


                    If Session("DOC_LIST").Rows.Count <> 0 Then

                        For i As Integer = 0 To Session("DOC_LIST").Rows.Count - 1
                            If Session("DOC_LIST").Rows(i)("DOC_STATUS") <> "DELETED" Then
                                status = AccessStudentClass.SaveENQUIRY_ACK_DOC(EAS_ID, Session("DOC_LIST").Rows(i)("DOCLIST"), _
                                                                          Session("DOC_LIST").Rows(i)("DIS_ORDER"), transaction)


                                If status <> 0 Then
                                    calltransaction = "1"
                                    errorMessage = "Error while inserting Document List"
                                    Return "1"
                                End If
                            End If
                        Next
                    End If

                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If

                    ViewState("viewid") = EAS_ID
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                    disable_control()
                    ViewState("viewid") = 0
                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        End If
    End Function
   
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindAcademic_Grade()
        If ViewState("ChangeEvent") = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>ShowOthGrade(1);</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                               "<script language=javascript>ShowOthGrade(2);</script>")
        End If
    End Sub
    Protected Sub gvDoc_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        If ViewState("ChangeEvent") = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>ShowOthGrade(1);</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                               "<script language=javascript>ShowOthGrade(2);</script>")
        End If
        Dim COND_ID As Integer = CInt(gvDoc.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0
        While i < Session("DOC_LIST").Rows.Count - 1
            If Session("DOC_LIST").rows(i)("Id") = COND_ID Then
                Session("DOC_LIST").rows(i)("DOC_STATUS") = "DELETED"
                Exit While
            End If
            i = i + 1
        End While
        gridbind()
    End Sub

    Protected Sub btnAddDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Call gridbind_Doc()
        If ViewState("ChangeEvent") = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>ShowOthGrade(1);</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                               "<script language=javascript>ShowOthGrade(2);</script>")
        End If
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim EAD_ID As New DataColumn("EAD_ID", System.Type.GetType("System.String"))
            Dim DIS_ORDER As New DataColumn("DIS_ORDER", System.Type.GetType("System.String"))
            Dim DOCLIST As New DataColumn("DOCLIST", System.Type.GetType("System.String"))
            Dim DOC_STATUS As New DataColumn("DOC_STATUS", System.Type.GetType("System.String"))
            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(EAD_ID)
            dtDt.Columns.Add(DIS_ORDER)
            dtDt.Columns.Add(DOCLIST)
            dtDt.Columns.Add(DOC_STATUS)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

            Return dtDt
        End Try
    End Function
    Sub gridbind()
        Try
            Dim dtDt As New DataTable
           
            If Not Session("DOC_LIST") Is Nothing Then
                dtDt = Session("DOC_LIST")
                ''dtDt.Select(strexpr, strsort, DataViewRowState.ModifiedOriginal)
                'dtDt.DefaultView.Sort = "ID DESC"
                dtDt.DefaultView.RowFilter = "DOC_STATUS <> 'DELETED'"
            End If

            gvDoc.DataSource = dtDt
            gvDoc.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub gridbind_Doc()
        Try


            If Trim(txtDoc.Text) = "" Then
                lblError.Text = "Enter the matter for the Document List"
                Exit Sub
            ElseIf Trim(txtNo.Text) = "" Or IsNumeric(txtNo.Text) = False Then
                lblError.Text = "Enter the valid order number in which the document to be listed"
                Exit Sub
            End If

            For i As Integer = 0 To Session("DOC_LIST").Rows.Count - 1

                If Session("DOC_LIST").Rows(i)("DIS_ORDER") = Trim(txtNo.Text) Then
                    If Session("DOC_LIST").Rows(i)("DOC_STATUS") <> "DELETED" Then
                        lblError.Text = "Duplicate display order entry not allowed"
                        Exit Sub
                    End If
                ElseIf CInt(Trim(txtNo.Text)) < 0 Then

                    lblError.Text = "Enter number greater than zero"
                    Exit Sub
                End If

            Next

            Dim rDt As DataRow


            rDt = Session("DOC_LIST").NewRow
            rDt("Id") = ViewState("id")
            rDt("DIS_ORDER") = txtNo.Text
            rDt("DOCLIST") = txtDoc.Text
            rDt("EAD_ID") = "0"
            rDt("DOC_STATUS") = "ADD"

            ViewState("id") = ViewState("id") + 1
            Session("DOC_LIST").Rows.Add(rDt)
            gridbind()


        Catch ex As Exception
            lblError.Text = "Check Student Leave date"
        End Try
    End Sub

    Protected Sub gvDoc_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        If ViewState("ChangeEvent") = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>ShowOthGrade(1);</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                               "<script language=javascript>ShowOthGrade(2);</script>")
        End If

        btnSave.Enabled = False
        gvDoc.SelectedIndex = e.NewEditIndex


        Dim row As GridViewRow = gvDoc.Rows(e.NewEditIndex)
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)

        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("DOC_LIST").Rows.Count - 1
            If iIndex = Session("DOC_LIST").Rows(iEdit)("ID") Then

                txtNo.Text = Session("DOC_LIST").Rows(iEdit)("DIS_ORDER")
                txtDoc.Text = Session("DOC_LIST").Rows(iEdit)("DOCLIST")

                Exit For
            End If
        Next
        btnAddDoc.Visible = False
        btnUpdate.Visible = True
        btnCancelDoc.Visible = True
        btnSave.Enabled = False
        gvDoc.Columns(4).Visible = False
        gvDoc.Columns(3).Visible = False
        gridbind()
    End Sub
    Sub reset_state()
        BSU_YEAR()
        bindAcademic_Grade()
        Session("DOC_LIST").Rows.Clear()
        txtMessage.Text = ""
        txtDoc.Text = ""
        txtNo.Text = ""
        chkMessage.Checked = False
        chkDoc.Checked = False
        gridbind()
    End Sub
    Sub disable_control()

        ddlAcdYear.Enabled = False
        ddlGrade.Enabled = False
        chkOthGrade.Enabled = False
        chkMessage.Enabled = False
        txtMessage.Enabled = False
        chkDoc.Enabled = False
        txtDoc.Enabled = False
        txtNo.Enabled = False
        btnAddDoc.Enabled = False
        btnUpdate.Visible = False
        btnCancelDoc.Visible = False
      
    End Sub
    Sub enable_control()
        ddlAcdYear.Enabled = True
        ddlGrade.Enabled = True
        chkOthGrade.Enabled = True
        chkMessage.Enabled = True
        txtMessage.Enabled = True
        chkDoc.Enabled = True
        txtDoc.Enabled = True
        txtNo.Enabled = True
        btnAddDoc.Enabled = True
        btnUpdate.Visible = False
        btnCancelDoc.Visible = False
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("ChangeEvent") = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>ShowOthGrade(1);</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                               "<script language=javascript>ShowOthGrade(2);</script>")
        End If

        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        Dim row As GridViewRow = gvDoc.Rows(gvDoc.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        iIndex = CInt(idRow.Text)

        If Trim(txtDoc.Text) = "" Then
            lblError.Text = "Enter the matter for the Document List"
            Exit Sub
        ElseIf Trim(txtNo.Text) = "" Or IsNumeric(txtNo.Text) = False Then
            lblError.Text = "Enter the valid order number in which the document to be listed"
            Exit Sub
        Else
           
            For iEdit = 0 To Session("DOC_LIST").Rows.Count - 1

                If iIndex <> Session("DOC_LIST").Rows(iEdit)("ID") Then
                    If Session("DOC_LIST").Rows(iEdit)("DIS_ORDER") = Trim(txtNo.Text) Then
                        If Session("DOC_LIST").Rows(iEdit)("DOC_STATUS") <> "DELETED" Then
                            lblError.Text = "Duplicate display order entry not allowed"
                            Exit Sub
                        End If
                    ElseIf CInt(Trim(txtNo.Text)) < 0 Then

                        lblError.Text = "Enter number greater than zero"
                        Exit Sub
                    End If

                End If

            Next
            For iEdit = 0 To Session("DOC_LIST").Rows.Count - 1
                If iIndex = Session("DOC_LIST").Rows(iEdit)("ID") Then

                    Session("DOC_LIST").Rows(iEdit)("DIS_ORDER") = txtNo.Text
                    Session("DOC_LIST").Rows(iEdit)("DOCLIST") = txtDoc.Text
                    If Session("DOC_LIST").Rows(iEdit)("EAD_ID") = "0" Then
                        Session("DOC_LIST").Rows(iEdit)("DOC_STATUS") = "ADD"
                    Else
                        Session("DOC_LIST").Rows(iEdit)("DOC_STATUS") = "UPDATE"
                    End If

                    Exit For
                End If
            Next

            btnAddDoc.Visible = True
            btnUpdate.Visible = False
            btnCancelDoc.Visible = False
            btnSave.Enabled = True
            gvDoc.SelectedIndex = -1
            gvDoc.Columns(4).Visible = True
            gvDoc.Columns(3).Visible = True
            gridbind()
            txtNo.Text = ""
            txtDoc.Text = ""
        End If
    End Sub
    Protected Sub btnCancelDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("ChangeEvent") = 1 Then
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                       "<script language=javascript>ShowOthGrade(1);</script>")
        Else
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                               "<script language=javascript>ShowOthGrade(2);</script>")
        End If
        btnAddDoc.Visible = True
        btnUpdate.Visible = False
        btnCancelDoc.Visible = False
        txtNo.Text = ""
        txtDoc.Text = ""
        gvDoc.Columns(4).Visible = True
        gvDoc.Columns(3).Visible = True
        btnSave.Enabled = True
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
End Class
