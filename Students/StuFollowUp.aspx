<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"
    CodeFile="StuFollowUp.aspx.vb" Inherits="Students_StuFollowUp" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Student Follow Up
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">

                <table width="100%">
                    <tr>
                        <td>
                            <span class="field-label">Academic Year</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddaccyear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <span class="field-label">Contact Me</span>

                        </td>
                        <td>
                            <asp:DropDownList ID="ddlContactMe" SkinID="smallcmb" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                <asp:ListItem Value="YES">YES</asp:ListItem>
                                <asp:ListItem Value="NO">NO</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td><span class="field-label">Follow Up Status</span> </td>
                        <td>
                            <asp:DropDownList ID="ddlFollowupStatus" SkinID="smallcmb" runat="server" AutoPostBack="true">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>

                        <td colspan="6" align="center">
                            <asp:GridView ID="grd_followup" EmptyDataText="No Records Found" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                                runat="server" AllowPaging="True" OnPageIndexChanging="grd_followup_PageIndexChanging"
                                PageSize="20">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Enq No">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblAppHeader" runat="server" Text="App No"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtAppno" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAppNo_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btn_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("EQS_APPLNO")%>
                                            <asp:HiddenField ID="Hiddeneqsid" Value='<%#Eval("PRA_EQS_ID")%>' runat="server" />
                                            <asp:HiddenField ID="Hiddenenqid" Value='<%#Eval("EQM_ENQID")%>' runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Enq Date">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEnqDate" runat="server" Text="Enq.Date"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtEnqDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnEnq_Date" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btn_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("EQM_ENQDATE", "{0:dd/MMM/yyyy}")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Applicant Name">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblApplName" runat="server" Text="Applicant Name"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtApplName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAppl_Name" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btn_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("ENQ_APPLPASSPORTNAME")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            Grade
                        <br />
                                            <asp:DropDownList ID="ddgrade" runat="server" OnSelectedIndexChanged="ddSearch_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblgrade" Text=' <%#Eval("GRM_DISPLAY")%>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stream">
                                        <HeaderTemplate>
                                            Stream<br />
                                            <asp:DropDownList ID="ddstream" runat="server" OnSelectedIndexChanged="ddSearch_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblstream" Text='<%#Eval("STM_DESCR")%>' runat="server"></asp:Label>
                                            <asp:HiddenField ID="hiddenshift" Value='<%#Eval("SHF_DESCR")%>' runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblContactme" runat="server" Text='<%#Eval("EQM_APPL_CONTACTME")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:ButtonField CommandName="ContactMe" ImageUrl="..\images\tick.gif" ButtonType="Image"
                                        HeaderText="Contact Me">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenStatus" Value='<%#Eval("EQS_bFOLLOWUP")%>' runat="server" />
                                            <asp:Label ID="lblReason" runat="server" Text='<%# bind("EFR_ICONS") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblTooltip" runat="server" Text='<%# bind("EFR_REASON") %>' Visible="false"></asp:Label>
                                            <%--<center><asp:ImageButton ID="Image1" ImageUrl="~\images\cross.gif" CommandName="select2" CommandArgument='<%# GetNavigateUrl(Eval("PRA_EQS_ID").ToString()) %>'   Enabled="false" runat="server" /></center>--%>
                                            <center>
                            <asp:ImageButton ID="Image1" ImageUrl="~\images\cross.gif" CommandName="select" CommandArgument='<%#Eval("PRA_EQS_ID")%>'
                                Enabled="false" runat="server"></asp:ImageButton></center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Follow Up">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkfollowup" Text="Follow Up" CommandName="select" CommandArgument='<%#Eval("PRA_EQS_ID")%>'
                                                runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            <input id="h_Selected_menu_1" type="hidden" value="LI_" runat="server" /><input id="h_Selected_menu_2"
                                type="hidden" value="LI_" runat="server" /><input id="h_Selected_menu_3" type="hidden"
                                    value="LI_" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
