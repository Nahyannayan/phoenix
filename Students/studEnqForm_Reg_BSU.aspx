<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnqForm_Reg_BSU.aspx.vb" Inherits="StudRecordEdit_bsu" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

      <script language="javascript" type="text/javascript">
    function getSibling(mode) 
        {     
            
           
            var NameandCode;
            var result;
            var url;
            var Bsu_id=document.getElementById("<%=ddlbsu.ClientID %>").value
            url='../Students/ShowStudentAll.aspx?id='+mode+'&bsu_id='+ Bsu_id +''  ;
                    
                if (mode=='SE')
                {
                    var oWnd = radopen(url, "pop_all");
               <%-- result = window.showModalDialog(url,"", sFeatures);
                                     
                    if (result=='' || result==undefined)
                    {    return false; 
                    }   
                 NameandCode = result.split('___');
                
                 document.getElementById("<%=txtstudno.ClientID %>").value=NameandCode[0];--%>
                
               
               }
    }


 function OnClientClose(oWnd, args) {
            
            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Result.split('___');
                  document.getElementById("<%=txtstudno.ClientID %>").value=NameandCode[0];
                   __doPostBack('<%= txtstudno.ClientID%>', 'TextChanged');
            }
        }


        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }   
    
     function Sel_Grade_KG1()
	
{
  
var val;
val=document.getElementById("<%=h_Grade_KG1.ClientID %>").value;
if (val=='1')
  {
					
				 styleObj1=document.getElementById(20).style;
				 styleObj2=document.getElementById(21).style; 
				 styleObj3=document.getElementById(22).style; 
				 styleObj4=document.getElementById(23).style;
				 styleObj5=document.getElementById(24).style; 
				 styleObj6=document.getElementById(25).style; 
				  
				styleObj1.display='none';
				styleObj2.display='';
				styleObj3.display='none';
				styleObj4.display='none';
				styleObj5.display='none';
				styleObj6.display='none';
				
	}
	else
	{
	
	             styleObj1=document.getElementById(20).style;
				 styleObj2=document.getElementById(21).style; 
				 styleObj3=document.getElementById(22).style; 
				 styleObj4=document.getElementById(23).style;
				 styleObj5=document.getElementById(24).style; 
				 styleObj6=document.getElementById(25).style; 
				  
				styleObj1.display='';
				styleObj2.display='none';
				styleObj3.display='';
		        styleObj4.display='';
				styleObj5.display='';
				styleObj6.display='';
	
	
	}
	
	}
	
	
	
	
	
	
	
	
	
	
	
	function fill_Sib(){
	

	if (document.getElementById('<%=chkSibling.ClientID %>').checked==true)
	{
		styleObj1=document.getElementById(28).style; 
		styleObj2=document.getElementById(29).style; 
		styleObj1.display='';
		styleObj2.display='';
		}
	else
	{
				
				 styleObj1=document.getElementById(28).style;
				 styleObj2=document.getElementById(29).style; 
				styleObj1.display='none';
			styleObj2.display='none';
	
	}
	
	}
	
	
	
	
	
	
	function fill_Staff(){
	

	if (document.getElementById('<%=chkStaff_GEMS.ClientID %>').checked==true)
	{
		styleObj1=document.getElementById(30).style; 
		styleObj2=document.getElementById(31).style; 
		styleObj1.display='';
		styleObj2.display='';
		}
	else
	{
				
				 styleObj1=document.getElementById(30).style;
				 styleObj2=document.getElementById(31).style; 
				styleObj1.display='none';
			styleObj2.display='none';
	
	}
	
	}
	
	
	
	function fill_Stud()
	{
	if (document.getElementById('<%=chkExStud_Gems.ClientID %>').checked==true)
	{
		styleObj1=document.getElementById(32).style; 
		styleObj2=document.getElementById(33).style; 
		styleObj1.display='';
		styleObj2.display='';
		}
	else
	{
				
				 styleObj1=document.getElementById(32).style;
				 styleObj2=document.getElementById(33).style; 
				styleObj1.display='none';
			styleObj2.display='none';
	
	}
	
	}
	
	
	
	 function getDate(val) 
          {     
            var sFeatures;
            sFeatures="dialogWidth: 227px; ";
            sFeatures+="dialogHeight: 252px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: no; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            //alert("hello")
            result = window.showModalDialog("../Payroll/Calendar.aspx","", sFeatures)
            if (result=='' || result==undefined)
            {

            return false;
            }
            
            if (val==1)
            { 
           document.getElementById('<%=txtDob.ClientID %>').value='';
           document.getElementById('<%=txtDob.ClientID %>').value=result; 
         }  
         
          else if (val==2)
                {
                document.getElementById('<%=txtPassIss_date.ClientID %>').value='';
                document.getElementById('<%=txtPassIss_date.ClientID %>').value=result; 
                document.getElementById('<%=txtPassIss_date.ClientID %>').focus();
                }
                else if (val==3)
                {
                 document.getElementById('<%=txtPassExp_Date.ClientID %>').value='';
                document.getElementById('<%=txtPassExp_Date.ClientID %>').value=result; 
                }
         else if (val==4)
                {
                document.getElementById('<%=txtVisaIss_date.ClientID %>').value='';
                document.getElementById('<%=txtVisaIss_date.ClientID %>').value=result; 
                  document.getElementById('<%=txtVisaIss_date.ClientID %>').focus();
                }
                else if (val==5)
                {
                document.getElementById('<%=txtVisaExp_date.ClientID %>').value='';
                document.getElementById('<%=txtVisaExp_date.ClientID %>').value=result; 
                
                }
         
        
         
          else if (val==8)
                {
                document.getElementById('<%=txtLast_Att.ClientID %>').value='';
                document.getElementById('<%=txtLast_Att.ClientID %>').value=result; 
                }
                  else if (val==9)
                {
                document.getElementById('<%=txtDOJ.ClientID %>').value='';
                document.getElementById('<%=txtDOJ.ClientID %>').value=result; 
                }
         } 
          
             
          function fillPassport()
{
var passIssu=document.getElementById('<%=txtPassIss_date.ClientID %>')

            if (passIssu.value!="")
            {
                        if (document.getElementById('<%=txtPassExp_Date.ClientID %>').value=="")
                        {
                        document.getElementById('<%=txtPassExp_Date.ClientID %>').value=AddYears(passIssu.value,5)
                        }
                        else
                        {
                        document.getElementById('<%=txtPassExp_Date.ClientID %>').value='';
                        document.getElementById('<%=txtPassExp_Date.ClientID %>').value=AddYears(passIssu.value,5)
                        }
            }
        
            

}

function fillVisa()
{
var VisaIssu=document.getElementById('<%=txtVisaIss_date.ClientID %>')


            if (VisaIssu.value!="")
            {
                       if (document.getElementById('<%=txtVisaExp_date.ClientID %>').value=="")  
                         {
                        document.getElementById('<%=txtVisaExp_date.ClientID %>').value=AddYears(VisaIssu.value,3)
                        }
                        else
                        {
                         document.getElementById('<%=txtVisaExp_date.ClientID %>').value='';
                         document.getElementById('<%=txtVisaExp_date.ClientID %>').value=AddYears(passIssu.value,3)
                        }
            }

}

function AddYears(dtStr,num)
{
var dtCh= "/";
var pos1=dtStr.indexOf(dtCh)
var pos2=dtStr.indexOf(dtCh,pos1+1)
var strMonth=dtStr.substring(pos1+1,pos2)
var tempMonth=dtStr.substring(pos1+1,pos2)
var tempValid=false;
var strDay=dtStr.substring(0,pos1)
var strYear=dtStr.substring(pos2+1)
var monthname=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")

if(isNaN(strMonth))
{
for(var i=0;i<monthname.length;i++)
{
if(monthname[i].toLowerCase()==strMonth.toLowerCase())
{
strMonth=i+1;
strMonth=strMonth+"";
tempValid=true;
}
 }
 } 
 else if ((parseInt(strMonth)>=1) && (parseInt(strMonth)<=12))
 {
 tempValid=true;
 }

if (tempValid==false)
{
return '';
}


if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)

      
strMonth=strMonth-1;

var myDate=new Date()
myDate.setFullYear(strYear,strMonth,strDay) 
myDate.setDate(myDate.getDate()-1)
myDate.setFullYear(myDate.getFullYear()+num)


var lstrDay=myDate.getDate()
var lstrMonth=myDate.getMonth()
lstrMonth=parseInt(lstrMonth)+1


if (parseInt(lstrDay) < 10)
{           
            lstrDay="0" + lstrDay
}


if(isNaN(tempMonth))
{
lstrMonth=monthname[lstrMonth-1]
 } 
else
{
if (parseInt(lstrMonth) < 10)
{           
            lstrMonth="0" + lstrMonth
}
}

return (lstrDay + "/" + lstrMonth + "/" + myDate.getFullYear())
}

   function GetSelectedItem() 

{ 
var e =document.getElementById("<%= ddlFLang.ClientID%>"); 
var fLang=e.options[e.selectedIndex].text;
   var CHK = document.getElementById("<%= chkOLang.ClientID%>"); 

   var checkbox = CHK.getElementsByTagName("input"); 

   var label = CHK.getElementsByTagName("label"); 

   for (var i=0;i<checkbox.length;i++) 

   { 

       if (checkbox[i].checked) 

       { 
var olang= label[i].innerHTML
if(fLang==olang)
{
       checkbox[i].checked=false; 
}
       } 

   } 

} 


       
        
               function chkFirst_lang()
     {
      
GetSelectedItem(); 
     
     }         
          
        
      </script>

    <style>
        table td input[type=text] {
            min-width :1% !important;
        }
        table td select {
            min-width :1% !important;
        }
        table.menu_a tr td  table tr td a { 
    display: block;
  width: 100%;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  color: #495057;
  /*background-color: #b4b2b2 !important;*/
  background-clip: padding-box;
  border: 1px solid #8dc24c;
  border-radius: 0.25rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;

      background: rgb(165,224,103) !important;
    background: -moz-linear-gradient(top, rgba(165,224,103,1) 0%, rgba(144,193,79,1) 44%, rgba(131,193,50,1) 100%) !important;
    background: -webkit-linear-gradient(top, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    background: linear-gradient(to bottom, rgba(165,224,103,1) 0%,rgba(144,193,79,1) 44%,rgba(131,193,50,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a5e067', endColorstr='#83c132',GradientType=0 ) !important;
}

        table.menu_a tr td  table tr td a:hover {
                /*background-color: #ffffff !important;*/
    background: rgb(131,193,50) !important;
    background: -moz-linear-gradient(top, rgba(131,193,50,1) 0%, rgba(144,193,79,1) 56%, rgba(165,224,103,1) 100%) !important;
    background: -webkit-linear-gradient(top, rgba(131,193,50,1) 0%,rgba(144,193,79,1) 56%,rgba(165,224,103,1) 100%) !important;
    background: linear-gradient(to bottom, rgba(131,193,50,1) 0%,rgba(144,193,79,1) 56%,rgba(165,224,103,1) 100%) !important;
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#83c132', endColorstr='#a5e067',GradientType=0 ) !important;
            }

      table.menu_a tr td  table tr td a:visited {
            background-color: #b4b2b2 !important;
        }

         table.menu_a tr td  table tr td a:active {
            background-color: #b4b2b2 !important;
        }
    </style>

     <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_all" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>          
    </telerik:RadWindowManager>

 <ajaxToolkit:ModalPopupExtender ID="mpe"   runat="server"
             Enabled="True" TargetControlID="btnTarget" PopupControlID="plPrev" BackgroundCssClass="modalBackground" x="450" Y="160"/> 


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Enquiry Form
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Literal ID="ltMessage" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td align="left" text-align: left">
                            <asp:ValidationSummary ID="vsPrim_Contact" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" ValidationGroup="groupM1"></asp:ValidationSummary>
                            <asp:ValidationSummary ID="vsPrev_Info" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" ValidationGroup="Prev_Info"></asp:ValidationSummary>
                            <asp:ValidationSummary ID="vsAppl_info" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" ValidationGroup="App_Info"></asp:ValidationSummary>
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"  text-align: center">Fields Marked with (<font color="red">*</font>) are mandatory</td>
                    </tr>
                    <tr>
                        
                        <td align="center">
                            <table align="left" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table align="left" cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%" valign="middle" ><span class="field-label">Bussiness Unit</span></td>

                                                <td align="left" width="30%" valign="middle">
                                                    <asp:DropDownList ID="ddlbsu" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlbsu_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                        ControlToValidate="ddlbsu" CssClass="error" ErrorMessage="*" Font-Bold="True"
                                                        InitialValue="0" ValidationGroup="gr"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left"  width="20%" valign="middle" ><span class="field-label">Student No</span></td>

                                                <td align="left"  width="30%" valign="middle">
                                                    <asp:TextBox ID="txtstudno" runat="server" AutoPostBack="True" OnTextChanged="txtstudno_TextChanged"></asp:TextBox>
                                                    <asp:ImageButton ID="btnStudent_Search" runat="server" ImageUrl="~/Images/forum_search.gif"
                                                        OnClientClick="getSibling('SE'); return false;"></asp:ImageButton></td>
                                            </tr>                                            
                                        </table>
                                    </td>
                                </tr>
                                <%--<tr class="subheader_img">
                        <td align="left" colspan="6" style="width: 827px" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                       </font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left"  rowspan="1" >
                                        <asp:Panel ID="pnlHeader" runat="server" Width="100%">
                                            <table align="left" cellpadding="5" cellspacing="0" width="100%">
                                                <tr >
                                                    <td align="left" width="20%" ><span class="field-label">Applicant Name As
                                        In Passport</span>
                                        <asp:Label ID="ltAPPFNAME" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                                    <td align="left" width="30%"  >
                                                        <asp:TextBox ID="txtFname" runat="server" MaxLength="100" width="100%" >
                                                        </asp:TextBox> 
                                                        <asp:RequiredFieldValidator ID="rfvFname_Stud" runat="server" ControlToValidate="txtFname"
                                                            Display="Dynamic" ErrorMessage="First name required" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                       </td>
                                                    <td width="20%" align="center">
                                                         <asp:TextBox ID="txtMname" runat="server" MaxLength="100" ValidationGroup="App_Info" Width="100%"
                                                           > 
                                                        </asp:TextBox>
                                                        </td>
                                                    <td width="30%">
                                        <asp:TextBox ID="txtLname" runat="server" MaxLength="100" >
                                        </asp:TextBox>
                                                    </td>
                                                    
                                                </tr>
                                                <tr >
                                                    <td align="left"><span class="field-label">Gender</span></td>

                                                    <td align="left" >
                                                        <asp:RadioButton ID="rdMale" runat="server" GroupName="Gender" Text="Male" CssClass="field-label"></asp:RadioButton>
                                                        <asp:RadioButton ID="rdFemale" runat="server" GroupName="Gender" Text="Female" CssClass="field-label"></asp:RadioButton>
                                                    </td>
                                                    <td align="left" ><span class="field-label">Religion</span>
                                        <asp:Label ID="ltReg" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlReligion" runat="server">
                                                        </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rfvReligion" runat="server" ControlToValidate="ddlReligion"
                                                                Display="Dynamic" ErrorMessage="Please select Religion"
                                                                InitialValue=" " ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr >
                                                    <td align="left" ><span class="field-label">Date Of Birth</span>
                                    <asp:Label ID="ltDOB" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                                    <td align="left">
                                                        <asp:TextBox ID="txtDob" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="imgBtnDob_date" runat="server" ImageUrl="~/Images/calendar.gif"
                                                            >
                                                        </asp:ImageButton>
                                                        <asp:RequiredFieldValidator ID="rfvDatefrom" runat="server" ControlToValidate="txtDob"
                                                                Display="Dynamic" ErrorMessage="Date of birth cannot be left empty"
                                                                ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                                    <br />
                                                                    (dd/mmm/yyyy)
                                                    </td>
                                                    <td align="left" ><span class="field-label">Place Of Birth</span><asp:Label ID="ltPOB" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                                    <td align="left" >
                                                        <asp:TextBox ID="txtPob" runat="server" ></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvPob" runat="server" ControlToValidate="txtPob"
                                                            Display="Dynamic" ErrorMessage="Place of birth cannot be left empty" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr >
                                                    <td align="left" ><span class="field-label">Country Of Birth</span><asp:Label ID="ltCOB" runat="server" Text="*" ForeColor="Red"></asp:Label></td>

                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlCountry" runat="server">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvCOB" runat="server" ControlToValidate="ddlCountry"
                                                            Display="Dynamic" ErrorMessage="Please select Country of Birth"
                                                            InitialValue=" " ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                                    <td align="left" ><span class="field-label">Nationality</span><asp:Label runat="server" Text="*" ForeColor="Red" ID="ltNationality"></asp:Label></td>

                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlNational" runat="server">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvNationality" runat="server" ControlToValidate="ddlNational"
                                                            Display="Dynamic" ErrorMessage="Please select Student Nationality"
                                                            InitialValue=" " ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                                </tr>
                                                <tr >
                                                    <td align="left" ><span class="field-label">Passport No</span>
                                    <asp:Label ID="ltPASSNO" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                                    <td align="left" >
                                                        <asp:TextBox ID="txtPassport" runat="server">
                                                        </asp:TextBox></td>
                                                </tr>
                                                <tr >
                                                    <td align="left" ><span class="field-label">Emergency
                                        <br />
                                                        Contact No</span></td>

                                                    <td align="left" >
                                                        <asp:TextBox ID="txtStud_Contact_Country" runat="server" MaxLength="3" Width="14%" ></asp:TextBox>
                                                        -
                                        <asp:TextBox ID="txtStud_Contact_Area" runat="server" MaxLength="4" Width="14%" ></asp:TextBox>
                                                        -
                                        <asp:TextBox ID="txtStud_Contact_No" runat="server" MaxLength="10" Width="41%" ></asp:TextBox><br /><asp:CompareValidator ID="cvContact_Country" runat="server" ControlToValidate="txtStud_Contact_Country"
                                            Display="Dynamic" ErrorMessage="Enter valid country code" Operator="DataTypeCheck"
                                            Type="Integer" ValidationGroup="groupM1"></asp:CompareValidator>
                                                        <asp:CompareValidator ID="cvContact_Area" runat="server" ControlToValidate="txtStud_Contact_Area"
                                                            Display="Dynamic" ErrorMessage="Enter valid area code" Operator="DataTypeCheck"
                                                            Type="Integer" ValidationGroup="groupM1"></asp:CompareValidator>
                                                        <asp:CompareValidator ID="cvContact_No" runat="server" ControlToValidate="txtStud_Contact_No"
                                                            Display="Dynamic" ErrorMessage="Enter valid contact no" Operator="DataTypeCheck"
                                                            Type="Integer" ValidationGroup="groupM1"></asp:CompareValidator>
                                                        <asp:RegularExpressionValidator ID="revContactNo" runat="server" ControlToValidate="txtStud_Contact_No"
                                                            Display="Dynamic" ErrorMessage="Enter valid contact no." SetFocusOnError="True"
                                                            ValidationExpression="^[0-9]{1,10}$" ValidationGroup="groupM1"></asp:RegularExpressionValidator>
                                                        (Country-Area-Number)</td>
                                                    <td align="left" >
                                                      <span class="field-label"> Mobile No</span></td>

                                                    <td align="left" >
                                                        <asp:TextBox ID="txtM_Country" runat="server" MaxLength="3" Width="14%">
                                                        </asp:TextBox>
                                                        -
                                    <asp:TextBox ID="txtM_Area" runat="server" MaxLength="4" Width="14%">
                                    </asp:TextBox> -
                                    <asp:TextBox ID="txtMobile_Pri" runat="server" MaxLength="10" Width="41%">
                                    </asp:TextBox>
                                                        <asp:CompareValidator ID="cvM_Country" runat="server" ControlToValidate="txtM_Country"
                                                            Display="Dynamic" ErrorMessage="Enter valid mobile country code" Operator="DataTypeCheck"
                                                            Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator>
                                                        <asp:CompareValidator ID="cvM_Area" runat="server" ControlToValidate="txtM_Area"
                                                            Display="Dynamic" ErrorMessage="Enter valid mobile area code" Operator="DataTypeCheck"
                                                            Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator>
                                                        <asp:CompareValidator ID="cvM_No" runat="server" ControlToValidate="txtMobile_Pri"
                                                            Display="Dynamic" ErrorMessage="Enter valid mobile no." Operator="DataTypeCheck"
                                                            Type="Integer" ValidationGroup="groupM1">*</asp:CompareValidator>
                                                        <asp:RegularExpressionValidator ID="revMobile" runat="server" ControlToValidate="txtMobile_Pri"
                                                            Display="Dynamic" ErrorMessage="Enter valid mobile no." SetFocusOnError="True"
                                                            ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator><br />
                                                        (Country
                                        -Area-Number)</td>
                                                </tr>
                                                <tr >
                                                    <td align="left" ><span class="field-label">Email</span><asp:Label ID="ltPri_Email" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                                    <td align="left">
                                                        <asp:TextBox ID="txtEmail_Pri" runat="server" MaxLength="100">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvPri_Email" runat="server" ErrorMessage="E-Mail ID required"
                                                            ValidationGroup="groupM1" ControlToValidate="txtEmail_Pri">*</asp:RequiredFieldValidator></td>
                                                    <td class="2"></td>
                                                </tr>
                                                <tr >
                                                    <td align="left" ><span class="field-label">Primary Contact</span></td>

                                                    <td align="left"  >
                                                        <asp:RadioButton ID="rdPri_Father" runat="server" AutoPostBack="True" GroupName="Pri_contact" CssClass="field-label"
                                                            Text="Father" ValidationGroup="Pri_contact"></asp:RadioButton>
                                                        <asp:RadioButton ID="rbPri_Mother" runat="server" AutoPostBack="True" GroupName="Pri_contact" CssClass="field-label"
                                                            Text="Mother" ValidationGroup="Pri_contact"></asp:RadioButton>
                                                        <asp:RadioButton ID="rdPri_Guard" runat="server" GroupName="Pri_contact" Text="Guardian" CssClass="field-label"
                                                            ValidationGroup="Pri_contact"></asp:RadioButton>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td align="left" style=""><span class="field-label">Name As In Passport</span><asp:Label ID="ltPri_Fname" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlCont_Status" runat="server" Width="20%">
                                                            <asp:ListItem>Mr</asp:ListItem>
                                                            <asp:ListItem>Mrs</asp:ListItem>
                                                            <asp:ListItem>Ms</asp:ListItem>
                                                            <asp:ListItem>Dr</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="txtPri_Fname" runat="server" MaxLength="70" width="79%">
                                                        </asp:TextBox><asp:RequiredFieldValidator ID="rfvPri_Fname" runat="server" ControlToValidate="txtPri_Fname"
                                                            Display="Dynamic" ErrorMessage="Primary Conact First name required"
                                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                                        </td><td>
                                                        <asp:TextBox ID="txtPri_Mname" runat="server" MaxLength="70" Width="100%" >
                                                        </asp:TextBox>
                                                            </td><td>
                                    <asp:TextBox ID="txtPri_Lname" runat="server" MaxLength="70" >
                                    </asp:TextBox>
                                                        
                                                    </td>
                                                </tr>
                                                <tr >

                                                    <td align="left" style=""><span class="field-label">Nationality</span><asp:Label ID="ltNational1" runat="server" ForeColor="Red" Text="*"></asp:Label></td>

                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlPri_National1" runat="server">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvNational1" runat="server" ControlToValidate="ddlPri_National1"
                                                            Display="Dynamic" ErrorMessage="Please select Primary Contact Nationality"
                                                            InitialValue=" " ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                                    <td align="left" style=""><span class="field-label">Nationality2</span></td>

                                                    <td align="left" style="">
                                                        <asp:DropDownList ID="ddlPri_National2" runat="server">
                                                        </asp:DropDownList>

                                                    </td>
                                                </tr>


                                                <tr id="77" style="display: none">
                                                    <td align="left" ><span class="field-label">Set as sibling</span></td>

                                                    <td align="left" colspan="4">
                                                        <asp:RadioButton ID="rdSib" runat="server" GroupName="SetSib"
                                                            Text="Yes" Checked="true" CssClass="field-label"></asp:RadioButton>
                                                        
                                    <asp:RadioButton ID="rdSib2" runat="server" GroupName="SetSib" CssClass="field-label"
                                        Text="No"></asp:RadioButton></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" ><span class="field-label">First Language(Main)</span></td>

                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlFLang" runat="server">
                                                        </asp:DropDownList></td>


                                                </tr>
                                                <tr>
                                                    <td align="left" ><span class="field-label">Other Languages(Specify)</span></td>

                                                    <td align="left" colspan="4">
                                                            <div ID="plOLang" runat="server" class="checkbox-list">
                                                            <asp:CheckBoxList ID="chkOLang" runat="server" >
                                                            </asp:CheckBoxList>
                                                                </div>
                                                    </td>
                                                </tr>

                                                <tr >
                                                    <td align="left" colspan="4" >
                                                        <asp:CheckBox ID="chkRef" runat="server" CssClass="field-label"
                                                            Text="Were you referred to GEMS by a friend? If �YES� enter the reference code &amp;amp; your reference email address." />
                                                        <table width="100%">
                                                            <tr>
                                                                                                                               <td width="20%">
                                                                    <span class="field-label"> Reference Code(Case sensitive)</span></td><td width="30%">
                                                        <asp:TextBox ID="txtRefCode" runat="server" ></asp:TextBox>
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td width="20%">
                                                                    <span class="field-label">   Email </span> </td><td width="30%">
                                                        <asp:TextBox ID="txtREFEmail" runat="server" ></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>                                                                                                                                                                            
                                                    </td>
                                                </tr>

                                                <tr >
                                                    <td align="left" ><span class="field-label">Print Application Form</span></td>

                                                    <td align="left">
                                                        <asp:CheckBox ID="chkPrint" runat="server" Text="Yes"></asp:CheckBox></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                       
                    </tr>

                    <tr>

                        <td>
                            <asp:Menu ID="mnuMaster" runat="server" OnMenuItemClick="mnuMaster_MenuItemClick"  CssClass="menu_a"
                                Orientation="Horizontal">
                                <Items>
                                    <%--<asp:MenuItem ImageUrl="../Images/Tab_studEdit/btnMain.jpg" Selected="True" Text="" Value="0"></asp:MenuItem>
                                    <asp:MenuItem ImageUrl="../Images/Tab_studEdit/btnPassport.jpg" Text="" Value="1"></asp:MenuItem>
                                    <asp:MenuItem ImageUrl="../Images/Tab_studEdit/btnContact.jpg" Text="" Value="2"></asp:MenuItem>
                                    <asp:MenuItem ImageUrl="../Images/Tab_studEdit/btnCurr1.jpg" Text="" Value="3"></asp:MenuItem>
                                    <asp:MenuItem ImageUrl="../Images/Tab_studEdit/btnTransport.jpg" Text="" Value="4"></asp:MenuItem>
                                    <asp:MenuItem ImageUrl="../Images/Tab_studEdit/btnOther.jpg" Text="" Value="5"></asp:MenuItem>--%>
                                    <asp:MenuItem  Selected="True" Text="Main" Value="0"></asp:MenuItem>
                                    <asp:MenuItem  Text="Passport / Visa" Value="1"></asp:MenuItem>
                                    <asp:MenuItem  Text="Contact Details" Value="2"></asp:MenuItem>
                                    <asp:MenuItem  Text="Current School" Value="3"></asp:MenuItem>
                                    <asp:MenuItem  Text="Services" Value="4"></asp:MenuItem>
                                    <asp:MenuItem  Text="Other Details" Value="5"></asp:MenuItem>
                                </Items>
                                <StaticMenuItemStyle Font-Strikeout="False" />
                                <StaticSelectedStyle />
                                <DynamicSelectedStyle />
                            </asp:Menu>
                            <asp:MultiView ID="mvMaster" runat="server" ActiveViewIndex="0">

                                <asp:View ID="vwMain" runat="server">
                                    <asp:Panel ID="pnlMain" runat="server" Width="100%">
                                        <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                         <%--   <tr>
                                                <td align="left" >Main</td>
                                            </tr>--%>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">School</span><span style="color: red">*</span></td>

                                                <td align="left" width="60%" colspan="2" >

                                                    <asp:DropDownList ID="ddlGEMSSchool" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Curriculum</span><span style="color: red">*</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:DropDownList ID="ddlCurri" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                                <td align="left"  width="20%" ><span class="field-label">Academic Year</span><span style="color: red">*</span></td>

                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Grade</span><span style="color: red">*</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                                <td align="left"  width="20%" ><span class="field-label">Shift</span><span style="color: red">*</span></td>

                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Stream</span><span style="color: red">*</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Term</span><span style="color: red">*</span></td>

                                                <td align="left"  width="30%" >
                                                    <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Tentative Joining Date</span><span style="color: red">*</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtDOJ" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        ></asp:ImageButton>
                                                     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="ImageButton1" TargetControlID="txtDOJ">
                </ajaxToolkit:CalendarExtender>
                                                    <br />
                                                    (dd/mmm/yyyy)</td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwPassport" runat="server">
                                    <asp:Panel ID="pnlPassPort" runat="server" Width="100%">
                                        <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                           <%-- <tr>
                                                <td align="left" ><span class="field-label">Passport/Visa</span></td>
                                            </tr>--%>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Passport Issue Place</span></td>

                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtPassportIssue" runat="server" >
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Passport Issue Date</span></td>

                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtPassIss_date" runat="server" >
                                                    </asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnPassIss_date" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        ></asp:ImageButton>(dd/mmm/yyyy)\
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnPassIss_date" TargetControlID="txtPassIss_date">
                </ajaxToolkit:CalendarExtender>
                                                </td>
                                                <td align="left" width="30%"><span class="field-label">Passport Expiry Date</span></td>
                                                <td align="left" width="20%">
                                                    <asp:TextBox ID="txtPassExp_Date" runat="server" >
                                                    </asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnPassExp_date" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        ></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnPassExp_date" TargetControlID="txtPassExp_Date">
                </ajaxToolkit:CalendarExtender>
                                           (dd/mmm/yyyy)
                                           
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Visa No</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtVisaNo" runat="server" >
                                                    </asp:TextBox></td>
                                                <td align="left"   width="20%"><span class="field-label">Visa Issue Place</span></td>

                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtIss_Place" runat="server" >
                                                    </asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  width="20%"><span class="field-label">Visa Issue Date</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtVisaIss_date" runat="server" >
                                                    </asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnVisaIss_date" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        ></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnVisaIss_date" TargetControlID="txtVisaIss_date">
                </ajaxToolkit:CalendarExtender>
                                                    <br />
                                                   (dd/mmm/yyyy)
                                                </td>
                                                <td align="left"  width="20%" ><span class="field-label">Visa Expiry Date</span></td>

                                                <td align="left"  width="30%">
                                                    <asp:TextBox ID="txtVisaExp_date" runat="server" >
                                                    </asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnVisaExp_date" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        ></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnVisaExp_date" TargetControlID="txtVisaExp_date">
                </ajaxToolkit:CalendarExtender>
                                            (dd/mmm/yyyy)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"><span class="field-label">Issuing Authority</span></td>

                                                <td align="left"  width="30%">
                                                    <asp:TextBox ID="txtIss_Auth" runat="server">
                                                    </asp:TextBox></td>
                                                <td colspan="2"></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwParent" runat="server">
                                    <asp:Panel ID="pnlParent" runat="server" Width="100%">
                                        <table align="left" cellpadding="5" cellspacing="0" width="100%">

                                           <%-- <tr>
                                                <td align="left" colspan="4" >Contact Detail</td>
                                            </tr>--%>                                            
                                            <tr>
                                                <td colspan="4" align="left">
                                                     <span class="field-label">Does the Student have one or more Siblings studying in
                                        a G E M S school? </span>
                                                    <span class="field-label">Please give details of any one</span>
                                                    <asp:CheckBox ID="chkSibling" runat="server" Text="Yes" CssClass="field-label"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr id="28" style="display: none">
                                                <td  align="left"><span class="field-label">Sibling Name</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtSib_Name" runat="server"></asp:TextBox></td>
                                                <td  align="left"><span class="field-label">Sibling Fee ID</span><span style="color: red">*</span></td>

                                                <td  align="left">
                                                    <asp:TextBox ID="txtSib_ID" runat="server" AutoPostBack="True" OnTextChanged="txtSib_ID_TextChanged"></asp:TextBox></td>
                                            </tr>
                                            <tr id="29" style="display: none">
                                                <td  align="left"><span class="field-label">G E M S School</span></td>

                                                <td  align="left">
                                                    <asp:DropDownList ID="ddlSib_BSU" runat="server">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr >
                                                <td align="left" class="title-bg" colspan="4">
                                                    Primary Contact Details 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr >
                                                <td align="left" class="title-bg-small" colspan="4" >
                                                    Permanent Address 
                                                </td>
                                            </tr>
                                            <tr >
                                                <td align="left" ><span class="field-label">Address Line 1</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtAdd1_overSea" runat="server" MaxLength="100">
                                                    </asp:TextBox></td>
                                                <td align="left" ><span class="field-label">Address Line 2</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtAdd2_overSea" runat="server" MaxLength="100">
                                                    </asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Address City/State</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtOverSeas_Add_City" runat="server" MaxLength="70">
                                                    </asp:TextBox></td>
                                                <td align="left" ><span class="field-label">Address Country</span></td>

                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlOverSeas_Add_Country" runat="server">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left"><span class="field-label">Phone</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtPhone_Oversea_Country" runat="server" MaxLength="3" Width="14%">
                                                    </asp:TextBox>
                                                    -
                                    <asp:TextBox ID="txtPhone_Oversea_Area" runat="server" MaxLength="4" Width="14%">
                                    </asp:TextBox>
                                                    -
                                    <asp:TextBox ID="txtPhone_Oversea_No" runat="server" MaxLength="10" Width="42%">
                                    </asp:TextBox>
                                                    <asp:CompareValidator ID="cvPhone_OS_Country" runat="server" ControlToValidate="txtPhone_Oversea_Country"
                                                        Display="Dynamic" ErrorMessage="Enter valid country code" Operator="DataTypeCheck"
                                                        Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:CompareValidator ID="cvPhone_OS_Area" runat="server" ControlToValidate="txtPhone_Oversea_Area"
                                                        Display="Dynamic" ErrorMessage="Enter valid  area code" Operator="DataTypeCheck"
                                                        Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator><span style="font-size: 7pt">
                                                        </span>
                                                    <asp:CompareValidator ID="cvPhone_OS_No" runat="server" ControlToValidate="txtPhone_Oversea_No"
                                                        Display="Dynamic" ErrorMessage="Enter valid  no." Operator="DataTypeCheck" Type="Double"
                                                        ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone_Oversea_No"
                                                        Display="Dynamic" ErrorMessage="Enter valid Phone no." SetFocusOnError="True"
                                                        ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator><br />
                                                    (Country -Area-Number)</td>
                                                <td align="left"><span class="field-label">P.O Box/Zip Code</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtPoBox_Pri" runat="server" MaxLength="20" >
                                                    </asp:TextBox>
                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtPoBox_Pri"
                                                        Display="Dynamic" ErrorMessage="Enter valid Zip code" Operator="DataTypeCheck"
                                                        Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator></td>
                                            </tr>
                                            <tr >
                                                <td align="left" class="title-bg-small" colspan="4">
                                                    Current Address 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Apartment No</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtApartNo" runat="server" MaxLength="50">
                                                    </asp:TextBox>
                                                </td>
                                                <td align="left" ><span class="field-label">Building</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtBldg" runat="server" MaxLength="50">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Street</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtStreet" runat="server" MaxLength="50">
                                                    </asp:TextBox></td>
                                                <td align="left" ><span class="field-label">Area</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtArea" runat="server" MaxLength="50">
                                                    </asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">City/State</span></td>

                                                <td align="left" >
                                                    
                                                        <asp:TextBox ID="txtCity_pri" runat="server" MaxLength="50">
                                                        </asp:TextBox></td>
                                                <td align="left" ><span class="field-label">Country</span></td>

                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlCountry_Pri" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">P.O Box</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtPoboxLocal" runat="server" MaxLength="20" >
                                                    </asp:TextBox></td>
                                                <td align="left" ><span class="field-label">City/Emirate</span></td>

                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlEmirate" runat="server">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Home Phone</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtHPhone_Country" runat="server" MaxLength="3" Width="14%">
                                                    </asp:TextBox> - 
                                                    <asp:TextBox ID="txtHPhone_Area"
                                                        runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                    -
                                                    <asp:TextBox ID="txtHPhone" runat="server" MaxLength="10" Width="42%"></asp:TextBox>
                                                    <asp:CompareValidator ID="cvUH_Country" runat="server" ControlToValidate="txtHPhone_Country"
                                                        Display="Dynamic" ErrorMessage="Enter valid home phone country code" Operator="DataTypeCheck"
                                                        Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator><span style="color: #ff0000">
                                                        </span>
                                                    <asp:CompareValidator ID="cvUH_Area" runat="server" ControlToValidate="txtHPhone_Area"
                                                        Display="Dynamic" ErrorMessage="Enter valid home phone area code" Operator="DataTypeCheck"
                                                        Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:CompareValidator ID="cvHPhone" runat="server" ControlToValidate="txtHPhone"
                                                        Display="Dynamic" ErrorMessage="Enter valid home phone no." Operator="DataTypeCheck"
                                                        Type="Double" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtHPhone"
                                                        Display="Dynamic" ErrorMessage="Enter valid Home Phone no." SetFocusOnError="True"
                                                        ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                                    <br />(Country-Area-Number)</td>
                                                <td align="left" ><span class="field-label">Office Phone</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtOPhone_Country" runat="server" MaxLength="3" Width="14%" >
                                                    </asp:TextBox>
                                                    -<asp:TextBox ID="txtOPhone_Area"
                                                        runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                    -<asp:TextBox ID="txtOPhone" runat="server" MaxLength="10" Width="43%"></asp:TextBox>
                                                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtOPhone_Country"
                                                        Display="Dynamic" ErrorMessage="Enter valid office phone country code" Operator="DataTypeCheck"
                                                        Type="Double" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtOPhone_Area"
                                                        Display="Dynamic" ErrorMessage="Enter valid office phone area code" Operator="DataTypeCheck"
                                                        Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtOPhone"
                                                        Display="Dynamic" ErrorMessage="Enter valid office phone no." Operator="DataTypeCheck"
                                                        Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtOPhone"
                                                        Display="Dynamic" ErrorMessage="Enter valid office no." SetFocusOnError="True"
                                                        ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator><br />
                                                    (Country -Area-Number)</td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 19%"><span class="field-label">Fax No</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtFaxNo_country" runat="server" MaxLength="3" Width="14%" ></asp:TextBox>
                                                    -
                                                    <asp:TextBox ID="txtFaxNo_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>
                                                    -
                                                    <asp:TextBox ID="txtFaxNo" runat="server" MaxLength="10" Width="42%" ></asp:TextBox>
                                                    <asp:CompareValidator ID="cvUFax_country" runat="server" ControlToValidate="txtFaxNo_country"
                                                        Display="Dynamic" ErrorMessage="Enter valid Fax country code" Operator="DataTypeCheck"
                                                        Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:CompareValidator ID="cvUFax_Area" runat="server" ControlToValidate="txtFaxNo_Area"
                                                        Display="Dynamic" ErrorMessage="Enter valid Fax area code" Operator="DataTypeCheck"
                                                        Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:CompareValidator ID="cvUFax_No" runat="server" ControlToValidate="txtFaxNo_country"
                                                        Display="Dynamic" ErrorMessage="Enter valid Fax no." Operator="DataTypeCheck"
                                                        Type="Double" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtFaxNo"
                                                        Display="Dynamic" ErrorMessage="Enter valid Fax no." SetFocusOnError="True" ValidationExpression="^[0-9]{1,10}$"
                                                        ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator><br />
                                                    (Country-Area-Number)<font color="red"></font></td>
                                                <td colspan="2"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Prefered Contact</span></td>

                                                <td align="left" ><asp:DropDownList ID="ddlPref_contact" runat="server" >
                                                    <asp:ListItem
                                                        Value="HOME PHONE">Home Phone</asp:ListItem>
                                                    <asp:ListItem Value="OFFICE PHONE">Office Phone</asp:ListItem>
                                                    <asp:ListItem Value="MOBILE">Mobile</asp:ListItem>
                                                    <asp:ListItem Value="EMAIL">Email</asp:ListItem>
                                                </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Occupation</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtOccup" runat="server" MaxLength="50">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label"> Company</span></td>

                                                <td align="left"  >
                                                    <asp:DropDownList ID="ddlCompany" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    (If you choose other,please specify the Company Name)<br />
                                                    <asp:TextBox ID="txtComp" runat="server" MaxLength="50">
                                                    </asp:TextBox>
                                                </td>
                                                <td colspan="2"></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  colspan="4">
                                                  <span class="field-label">  Is any one parent of Student an employee with G E M S ?</span><asp:CheckBox
                                                        ID="chkStaff_GEMS" runat="server" Text="Yes" CssClass="field-label"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr id="30" style="display: none">
                                                <td align="left" ><span class="field-label">Staff Name</span><span style="color: red">*</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtStaff_Name" runat="server">
                                                    </asp:TextBox></td>
                                                <td align="left" ><span class="field-label">Staff ID</span><span style="color: red">*</span></td>

                                                <td align="left" >
                                                    <asp:TextBox ID="txtStaffID" runat="server">
                                                    </asp:TextBox></td>
                                            </tr>
                                            <tr id="31" style="display: none">
                                                <td align="left" ><span class="field-label">Business unit</span></td>

                                                <td align="left" >
                                                    <asp:DropDownList ID="ddlStaff_BSU" runat="server">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4" >
                                                   <span class="field-label"> Is parent of Student an Ex-Student of a G E M S School?</span>
                                                    <asp:CheckBox ID="chkExStud_Gems" runat="server" Text="Yes" CssClass="field-label"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr id="32" style="display: none">
                                                <td align="left" ><span class="field-label">Name</span></td>

                                                <td align="left">
                                                    <asp:TextBox ID="txtExStudName_Gems" runat="server">
                                                    </asp:TextBox></td>
                                                <td align="left" ><span class="field-label">Academic Year</span></td>

                                                <td  align="left">
                                                    <asp:DropDownList ID="ddlExYear" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="33" style="display: none">
                                                <td align="left" ><span class="field-label">G E M S School</span></td>

                                                <td align="left" colspan="4">
                                                    <asp:DropDownList ID="ddlExStud_Gems" runat="server">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4" ><span class="field-label">How did you hear about
                                    <asp:Literal ID="ltAboutUs" runat="server"></asp:Literal></span><br />
                                                    <asp:CheckBoxList ID="chkAboutUs" runat="server" CellPadding="2" CellSpacing="1"
                                                        RepeatColumns="6" RepeatDirection="Horizontal">
                                                    </asp:CheckBoxList></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwPrevious" runat="server" EnableTheming="True">
                                    <asp:Panel ID="pnlCurrent" runat="server" Width="100%">
                                        <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                           <%-- <tr>
                                                <td align="left" colspan="4" ><span class="field-label"></span>Current School</td>
                                            </tr>--%>
                                            <tr id="20" style="display: none">
                                                <td align="left" ><span class="field-label">G E M S School</span></td>

                                                <td align="left" colspan="4">
                                                    <asp:RadioButton ID="rdGemsGr" runat="server" AutoPostBack="True" GroupName="PrevSchool"
                                                        Text="Yes" CssClass="field-label"></asp:RadioButton>
                                                    
                                    <asp:RadioButton ID="rdOther" runat="server" AutoPostBack="True" GroupName="PrevSchool"
                                        Text="No" CssClass="field-label"></asp:RadioButton></td>
                                            </tr>
                                            <tr id="21" style="display: none">
                                                <td align="left" width="20%" ><span class="field-label">School Name</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:DropDownList ID="ddlPreSchool_Nursery" runat="server">
                                                    </asp:DropDownList></td>
                                                <td align="left" width="20%" ><span class="field-label">Registration No</span></td>

                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtRegNo" runat="server" >
                                                    </asp:TextBox></td>
                                            </tr>
                                            <tr id="22" style="display: none">
                                                <td align="left" width="20%" ><span class="field-label">School Name</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtPre_School" runat="server" >
                                                    </asp:TextBox>
                                    <asp:DropDownList ID="ddlGemsGr" runat="server">
                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr id="23" style="display: none">
                                                <td align="left" width="20%" ><span class="field-label">City</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtPre_City" runat="server">
                                                    </asp:TextBox>
                                                </td>
                                                <td align="left" width="20%" ><span class="field-label">Country</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:DropDownList ID="ddlPre_Country" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="24" style="display: none">
                                                <td align="left" width="20%" ><span class="field-label">Grade</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:DropDownList ID="ddlPre_Grade" runat="server">
                                                    </asp:DropDownList></td>
                                                <td align="left" width="20%" ><span class="field-label">Curriculum</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:DropDownList ID="ddlPre_Curriculum" runat="server">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr id="25" style="display: none">
                                                <td align="left" width="20%" ><span class="field-label">Medium of Instruction</span></td>

                                                <td align="left" width="30%" >
                                                    <asp:TextBox ID="txtMedInst" runat="server" Width="140px">
                                                    </asp:TextBox></td>
                                                <td align="left" width="20%" ><span class="field-label">Last Attendance Date</span></td>

                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtLast_Att" runat="server" >
                                                    </asp:TextBox>
                                                    <asp:ImageButton ID="imgBtnLastAtt_date" runat="server" ImageUrl="~/Images/calendar.gif"
                                                        ></asp:ImageButton>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnLastAtt_date" TargetControlID="txtLast_Att">
                </ajaxToolkit:CalendarExtender>
                                                    <br />
                                                    (dd/mmm/yyyy)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="3" ><span class="field-label">Does your child have any medical problems/physical disabilities/specific learning
                                    difficulties the school should be aware of?</span></td>

                                                <td align="left" >
                                                    <asp:RadioButton ID="rdMisc_Yes" runat="server" GroupName="Misc" Text="Yes" CssClass="field-label"></asp:RadioButton>
                                                    
                                    <asp:RadioButton ID="rdMisc_No" runat="server" GroupName="Misc" Text="No" CssClass="field-label"></asp:RadioButton></td>
                                            </tr>
                                            <tr>
                                                <td align="left" ><span class="field-label">Give Details</span></td>

                                                <td align="left" ><asp:TextBox ID="txtNote" runat="server"  Rows="3" SkinID="MultiText"
                                                    TextMode="MultiLine" ></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vTransport" runat="server">
                                    <asp:Panel ID="pnlTrans" runat="server" Width="100%">
                                        <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                            <tbody>
                                               
                                                <tr>
                                                    <td align="left" width="20%" ><span class="field-label">Transport Required</span>
                                                    </td>
                                                    <td align="left" width="30%">
                                                        <asp:CheckBox ID="chkTran_Req" runat="server" Text="Yes" CssClass="field-label"></asp:CheckBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Main
                                            Location</span></td>

                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="ddlMainLocation" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList></td>

                                                    <td align="left" width="20%"><span class="field-label">Sub Location</span>
                                                    </td>

                                                    <td align="left" width="30%" ><asp:DropDownList ID="ddlSubLocation" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="20%" ><span class="field-label">Pick Up Point</span></td>

                                                    <td align="left" width="30%" >
                                                        <asp:DropDownList ID="ddlPickup" runat="server">
                                                        </asp:DropDownList><br />
                                                        <br />
                                                       <span class="field-label"> (If your pick up point is unavailable in the list please select the nearest pick
                                            up point and provide details in the box below)</span><br />
                                                        <asp:TextBox ID="txtOthers" runat="server">
                                                        </asp:TextBox></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="vwHealth" runat="server">
                                    <asp:Panel ID="pnlOther" runat="server" Width="100%">
                                        <table align="center" cellpadding="5" cellspacing="0" width="100%">

                                            <tbody>                                                
                                                <tr>
                                                    <td align="left" >
                                                        <table align="center" border="0" cellpadding="5" cellspacing="0"
                                                            width="100%">
                                                            <tr>
                                                                <td >
                                                                    <table cellpadding="5" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td colspan="6"><span class="field-label">The application info provided by </span><asp:RadioButton ID="rbParentF" runat="server" AutoPostBack="True"
                                                                                GroupName="Filled" Text="Parent" CssClass="field-label"></asp:RadioButton>
                                                                                <asp:RadioButton ID="rbRelocAgent" runat="server" AutoPostBack="True" GroupName="Filled"
                                                                                    Text="Relocation Agent" CssClass="field-label"></asp:RadioButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:Table ID="tbReloc" runat="server" CellPadding="4" CellSpacing="0" Css Width="100%">
                                                                        <asp:TableRow ID="r1Reloc" runat="server" 
                                                                            >
                                                                            <asp:TableCell ID="TableCell1" runat="server" 
                                                                                 Width="16%"><span class="field-label">Agent Name</span> </asp:TableCell>
                                                                           
                                                                            <asp:TableCell ID="TableCell3" runat="server" 
                                                                                 Width="25%">
                                                                                <asp:TextBox ID="txtAgentName" runat="server" ></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAgentName"
                                                                                    Display="Dynamic" ErrorMessage="Agent Name required" ValidationGroup="Prim_cont">*</asp:RequiredFieldValidator>
                                                                            </asp:TableCell>
                                                                            <asp:TableCell ID="TableCell4" runat="server" 
                                                                                 Width="16%"><span class="field-label">Mobile</span></asp:TableCell>
                                                                           
                                                                            <asp:TableCell ID="TableCell6" runat="server" 
                                                                                >
                                                                                  
                                                                                <asp:TextBox ID="txtMAgent_country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                                                                -
                                                                                                <asp:TextBox ID="txtMAgent_Area" runat="server" MaxLength="4" Width="14%"></asp:TextBox>-
                                                                                                <asp:TextBox ID="txtMAgent_No" runat="server" MaxLength="10" Width="43%"></asp:TextBox>

                                                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtMAgent_country"
                                                                                    Display="Dynamic" ErrorMessage="Enter valid mobile country code" Operator="DataTypeCheck"
                                                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtMAgent_Area"
                                                                                    Display="Dynamic" ErrorMessage="Enter valid mobile area code" Operator="DataTypeCheck"
                                                                                    Type="Integer" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                                                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtMAgent_No"
                                                                                    Display="Dynamic" ErrorMessage="Enter valid mobile no." Operator="DataTypeCheck"
                                                                                    Type="Double" ValidationGroup="Prim_cont">*</asp:CompareValidator>
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator55" runat="server" ControlToValidate="txtMAgent_No"
                                                                                    Display="Dynamic" ErrorMessage="Enter valid mobile no." SetFocusOnError="True"
                                                                                    ValidationExpression="^[0-9]{1,10}$" ValidationGroup="Prim_cont">*</asp:RegularExpressionValidator>
                                                                               (Country -Area-Number)
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                    <br />
                                                                    <span class="field-label">Subscribe to Newsletters and promotional mails.</span><asp:CheckBox ID="chkEAdd" runat="server"></asp:CheckBox><br />
                                                                  <span class="field-label">  G E M S may use the above Mobile Number to send text messages and alerts.</span><asp:CheckBox ID="chksms" runat="server"></asp:CheckBox><br />

                                                                 <span class="field-label">   G E M S has permission to include child in publication/promotion photos and videos</span>
                                                                                                <asp:CheckBox ID="chkPubl"
                                                                                                    runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </asp:Panel>
                                </asp:View>



                            </asp:MultiView>

                        </td>

                    </tr>

                    <tr>

                        <td align="center" valign="middle">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" OnClick="btnAdd_Click" Text="Add" CausesValidation="False" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                ValidationGroup="groupM1" OnClick="btnSave_Click" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                    CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /></td>

                    </tr>
                    <tr>

                        <td align="center">
                            <input id="h_Grade_KG1" runat="server" type="hidden" value="1" />
                            <input id="hd_More_App" type="hidden" runat="server" /></td>

                    </tr>
                </table>
                <asp:Button Style="display: none" ID="btnTarget" runat="server" Text="Button"></asp:Button>

                <div id="plPrev" runat="server">
                    <div class="msg_header" >
                        <div class="msg" align="center">
                           <span class="field-label"> Confirm</span>
                        </div>
                    </div>

                    <asp:Panel ID="plStudAtt" runat="server"  BackColor="White">

                        <table align="center" width="100%">
                            <tr style="padding: 1px;">
                                <td align="left">
                                    <img src="../Images/alert.png" style="left: 6px; position: relative; top: 10px;" />
                                    <asp:Label ID="lblText" runat="server"  
                                         Style="position: relative; top: 10px; text-align: left; padding: 4px; left: 2px;"
                                        > <span class="field-label">Would like to continue with the same Primary Contact info for the next applicant? </span></asp:Label></td>

                            </tr>



                        </table>
                        <table style="width: 100%;" align="center">
                          
                            <tr>
                                <td>
                                    <asp:Button ID="btnYes2" runat="server" CssClass="button" OnClick="btnYes2_Click"
                                        Text="Yes"  />
                                    <asp:Button ID="btnNo2" runat="server" CssClass="button" OnClick="btnNo2_Click" Text="No"
                                         />
                                    <asp:Button ID="btnYes" runat="server" OnClick="btnYes_Click" Text="Yes" CssClass="button"  /><asp:Button
                                        ID="btnNo" runat="server" CssClass="button" Text="No"  OnClick="btnNo_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblErr" runat="server"  EnableViewState="False"></asp:Label>
                                </td>
                            </tr>

                        </table>



                    </asp:Panel>
                </div>

            </div>
        </div>
    </div>
    
</asp:Content>

