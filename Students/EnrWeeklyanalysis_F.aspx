﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EnrWeeklyanalysis_F.aspx.vb" Inherits="Students_EnrWeeklyanalysis_F" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Enrollment Weekly analysis Detail</title>
      <link href="../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
       <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
         <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
          <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
       <script type="text/javascript">
           function fancyClose() {

               parent.$.fancybox.close();
           }
       </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        style="width: 80%;">
        <tr class="subheader_img">
            <td align="center"  colspan="8" style="height: 1px" valign="middle">
                <div align="left">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                        <asp:Label ID="lblCaption" runat="server" Text="Enrollment Weekly Analysis"></asp:Label></font>&nbsp;</div>
            </td>
        </tr>
    <tr>

              <td colspan="8">
                  <table width="100%">
                    <tr class="subheader_img">
                        <td>
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                            </span></font>
                        </td>
                    </tr>
                      <tr>
                        <td>
                            <asp:GridView ID="gvEnroll" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="gridstyle" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" Width="100%" OnPageIndexChanging="gvEnroll_PageIndexChanging" DataKeyNames ="BSU">
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    
                                 <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                              
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit">
                                    
                                        <ItemTemplate>
                                          <a id="frameSubActivity" class="frameActivity" href="EnrWeeklyanalysis_F.aspx?BSU=<%#Eval("BSU")%>&AsonDate=<%# ViewState("AsonDAte")%>">    <asp:Label ID="lblBsu" runat="server" Text='<%# Bind("[BSU NAME]")%>' __designer:wfdid="w40"></asp:Label></a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Month">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCName" runat="server" Text='<%# Bind("Month")%>' __designer:wfdid="w40"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:TemplateField>
                                  
                                  <asp:BoundField  DataField="Class" HeaderText="Class"></asp:BoundField>

                                       <asp:BoundField  DataField="Student Capacity/Class" HeaderText="Student Capacity/Class"></asp:BoundField>
                                       <asp:BoundField  DataField="Student Budget Sep 2016" HeaderText="Student Budget Sep 2016"></asp:BoundField>
                                       <asp:BoundField  DataField="Current Enrolments" HeaderText="Current Enrolments"></asp:BoundField>
                                       <asp:BoundField  DataField="OPEN Seats" HeaderText="OPEN Seats"></asp:BoundField>
                                       <asp:BoundField  DataField="Future Dated Admission" HeaderText="Future Dated Admission"></asp:BoundField>
                                       <asp:BoundField  DataField="Offers Pending" HeaderText="Offers Pending"></asp:BoundField>
                                       <asp:BoundField  DataField="Waitlisted" HeaderText="Waitlisted"></asp:BoundField>

                                     <asp:BoundField  DataField="Assessments" HeaderText="Assessments"></asp:BoundField>
                                    <asp:BoundField  DataField="Registrations" HeaderText="Registrations"></asp:BoundField>
                                    <asp:BoundField  DataField="Enquiries" HeaderText="Enquiries"></asp:BoundField>

                                </Columns>
                                <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </td>
                    </tr>
                      </table>

              </td>
          </tr>
              </table>
    </div>
    </form>
</body>
</html>
