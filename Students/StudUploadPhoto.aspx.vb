Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports system.io

Partial Class Students_StudUploadPhoto
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Dim SearchMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Try
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200115") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    'Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                FUUploadStuPhoto.Attributes.Add("onblur", "javascript:UploadPhoto();")
                ViewState("BSU_ID") = Session("sBsuid")
                Page.Title = "Upload Student Photo"
                h_Selected_menu_0.Value = "LI__../Images/operations/like.gif"
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                callYEAR_DESCRBind()
                ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))

                Using DefReader As SqlDataReader = GetDefSchool(Session("sBsuid"))
                    While DefReader.Read
                        Session("School_Type") = Convert.ToString(DefReader("BSU_bGEMSSchool"))
                        
                        
                    End While
                End Using


                gridbind()
            End If
            set_Menu_Img()
            If ViewState("popup") = 1 Then
                ModalpopupUpload.Show()
            End If

            'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(Page.FindControl("FUUploadStuPhoto"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Function GetDefSchool(ByVal BSU_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSchoolType", pParms)
        Return reader
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
  
    
    Public Sub callYEAR_DESCRBind()
        Try
            Dim Active_year As String
            Using Activereader As SqlDataReader = AccessStudentClass.GetActive_ACD_4_Grade(Session("sBsuid"), Session("CLM"))
                While (Activereader.Read())
                    Active_year = Convert.ToString(Activereader("Y_DESCR"))
                End While
            End Using
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAca_Year.Items.Clear()
                di = New ListItem("Not Selected", "0")
                ddlAca_Year.Items.Add(di)
                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAca_Year.Items.Add(di)
                    End While
                End If
            End Using
            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlAca_Year.Items.Count - 1
                If ddlAca_Year.Items(ItemTypeCounter).Text = Active_year Then
                    ddlAca_Year.SelectedIndex = ItemTypeCounter
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_0.Value.Split("__")
        getid0(str_Sid_img(2))
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

    End Sub

    Public Function getid0(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_0_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvSiblingInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvSiblingInfo.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

            If Session("School_Type") = "1" Then
                str_conn = ConnectionManger.GetOASISConnection.ConnectionString
            Else
                str_conn = ConnectionManger.GetOASISTransportConnection.ConnectionString
            End If
            Dim str_Sql As String = ""

            Dim str_filter_FeeID As String
            Dim str_filter_StudName As String
            Dim str_filter_StudNo As String
            Dim str_filter_SectionName As String
            Dim str_filter_Grade As String
            Dim temp_name As String = String.Empty
            Dim ds As New DataSet

            Dim lblFeeID As New Label
            Dim lblStudName As New Label
            Dim lblStudNo As New Label
            Dim lblSchoolName As New Label

            Dim txtSearch As New TextBox

            Dim str_txtFeeID, str_txtStudName, str_txtStudNo, str_txtSectionName, str_txtGrade As String

            Dim str_search As String
            str_txtFeeID = ""
            str_txtStudName = ""
            str_txtStudNo = ""
            str_txtSectionName = ""
            str_txtGrade = ""

            str_filter_FeeID = ""
            str_filter_StudName = ""
            str_filter_StudNo = ""
            str_filter_SectionName = ""
            str_filter_Grade = ""
            Using Userreader As SqlDataReader = AccessRoleUser.GetBUnitImage(Session("sBsuid"))
                While Userreader.Read
                    temp_name = Convert.ToString(Userreader("BSU_NAME"))
                End While
            End Using


            'str_filter_SectionName = " AND a.SchoolName LIKE '%" & temp_name & "%'"



            Dim CurBsUnit As String = ViewState("BSU_ID").ToString
            Dim CurACDID As String = ddlAca_Year.SelectedValue

            ' If ViewState("GRD_ACCESS") > 0 Then 'get student detail
            str_Sql = "SELECT STU_ID,FeeID,StudNo,StudName, SchoolName, GRADE,GRD_DISPLAYORDER,SCT_DESCR,PHOTOAvailable,STU_PHOTOPATH from(SELECT  DISTINCT  STUDENT_M.STU_NO as StudNo,STU_ID,STUDENT_M.STU_FEE_ID AS FeeID, ISNULL(STUDENT_M.STU_FIRSTNAME, '') + '  ' + ISNULL(STUDENT_M.STU_MIDNAME, '') + '  ' + ISNULL(STUDENT_M.STU_LASTNAME, '') AS StudName,BUSINESSUNIT_M.BSU_NAME as SchoolName,STU_GRD_ID AS GRADE,GRD_DISPLAYORDER,SCT_DESCR,STU_ACD_ID,CASE ISNULL(STU_PHOTOPATH,'') WHEN '' THEN '~/Images/cross.gif'  ELSE '~/Images/tick.gif' END AS  PHOTOAvailable,STU_PHOTOPATH FROM STUDENT_M INNER JOIN  STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID INNER JOIN OASIS.dbo.ACADEMICYEAR_D ON STU_ACD_ID=ACD_ID INNER JOIN OASIS.dbo.BUSINESSUNIT_M ON STUDENT_M.STU_BSU_ID = BUSINESSUNIT_M.BSU_ID INNER JOIN OASIS.dbo.GRADE_M ON GRD_ID=STU_GRD_ID INNER JOIN OASIS.dbo.SECTION_M ON STU_SCT_ID=SCT_ID AND STUDENT_M.STU_BSU_ID='" & CurBsUnit & "' AND ISNULL(STU_LEAVEDATE,'')='' AND STU_ACD_ID='" & CurACDID & "')a WHERE a.StudNo <> ''"
            'Else

            'End If

            If gvSiblingInfo.Rows.Count > 0 Then

                Dim str_Sid_search() As String
                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_Selected_menu_0.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtFeeID")
                str_txtFeeID = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_FeeID = " AND a.FeeID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FeeID = " AND a.FeeID NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FeeID = " AND a.FeeID LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FeeID = " AND a.FeeID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FeeID = " AND a.FeeID LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FeeID = " AND a.FeeID NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudNo")
                str_filter_StudNo = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_StudNo = " AND a.StudNo LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_StudNo = "  AND  NOT a.StudNo LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_StudNo = " AND a.StudNo  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_StudNo = " AND a.StudNo  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_StudNo = " AND a.StudNo LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_StudNo = " AND a.StudNo NOT LIKE '%" & txtSearch.Text & "'"
                End If


                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudName")
                str_txtStudName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_StudName = " AND a.StudName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_StudName = "  AND  NOT a.StudName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_StudName = " AND a.StudName  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_StudName = " AND a.StudName  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_StudName = " AND a.StudName LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_StudName = " AND a.StudName NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtSectionName")
                str_txtSectionName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_SectionName = " AND a.SCT_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_SectionName = "  AND  NOT a.SCT_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_SectionName = " AND a.SCT_DESCR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_SectionName = " AND a.SCT_DESCR  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_SectionName = " AND a.SCT_DESCR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_SectionName = " AND a.SCT_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If
                ''name
                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtGrd_search")
                str_txtGrade = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Grade = " AND a.GRADE LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Grade = "  AND  NOT a.GRADE LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Grade = " AND a.GRADE  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Grade = " AND a.GRADE  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Grade = " AND a.GRADE LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Grade = " AND a.GRADE NOT LIKE '%" & txtSearch.Text & "'"
                End If

            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_FeeID & str_filter_StudNo & str_filter_StudName & str_filter_SectionName & str_filter_Grade & "order by a.GRD_DISPLAYORDER,a.SCT_DESCR,a.StudName")

            gvSiblingInfo.DataSource = ds.Tables(0)
            ' gvSiblingInfo.TemplateControl.FindControl("label1"). = ds.Tables(0).Columns("emp_ID")
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvSiblingInfo.DataBind()
                Dim columnCount As Integer = gvSiblingInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvSiblingInfo.Rows(0).Cells.Clear()
                gvSiblingInfo.Rows(0).Cells.Add(New TableCell)
                gvSiblingInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSiblingInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSiblingInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

                'gvSiblingInfo.HeaderRow.Visible = True
            Else
                gvSiblingInfo.DataBind()


            End If

            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtFeeID")
            txtSearch.Text = str_txtFeeID
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudName")
            txtSearch.Text = str_txtStudName
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtStudNo")
            txtSearch.Text = str_txtStudNo
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtSectionName")
            txtSearch.Text = str_txtSectionName
            txtSearch = gvSiblingInfo.HeaderRow.FindControl("txtGrd_search")
            txtSearch.Text = str_txtGrade

            set_Menu_Img()

            'Page.Title = "Employee Info"

        Catch ex As Exception
            UtilityObj.Errorlog("Error In collecting sibling info")
        End Try





    End Sub

    Protected Sub lblStudName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblStudNo As New Label
        Dim lblStudID As New HiddenField
        Dim lbClose As New LinkButton


        lbClose = sender

        lblStudNo = sender.Parent.FindControl("lblStudNo")
        lblStudID = sender.parent.findcontrol("HF_STU_ID")

        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lblStudNo.Text & "___" & lblStudID.Value
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")

        If (Not lblStudNo Is Nothing) Then
            '   Response.Write(lblcode.Text)
            Response.Write("<script language='javascript'> function listen_window(){")
            Response.Write("window.returnValue = '" & l_Str_Msg & "';")


            Response.Write("window.close();")
            Response.Write("} </script>")
            h_SelectedId.Value = "Close"
        End If
    End Sub

    Protected Sub btnSearchFeeID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStudNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStudName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSection_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvSiblingInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSiblingInfo.PageIndexChanging
        gvSiblingInfo.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
        gridbind()
    End Sub
    Protected Sub lnkbtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim HF_STU_ID As HiddenField = TryCast(sender.Parent.FindControl("HF_STU_ID"), HiddenField)
        Dim str_img As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
        Dim photopath As String = TryCast(sender.Parent.FindControl("HF_Photopath"), HiddenField).Value
        If photopath <> "" Then
            imgstuImage.ImageUrl = str_img + photopath
        Else
            imgstuImage.ImageUrl = str_img + "\NOIMG\no_image.jpg"
        End If

        ViewState("STUID") = HF_STU_ID.Value
        ViewState("popup") = 1
        ModalpopupUpload.Show()
    End Sub
    'Private Sub UpLoadPhoto()
    '    If FUUploadStuPhoto.FileName <> "" Then
    '        lblError.Text = ""
    '        If Not FUUploadStuPhoto.PostedFile.ContentType.Contains("image") Then
    '            ''Throw New Exception
    '            lblError.Text = "Select Image Only"
    '            Exit Sub
    '        End If
    '        If FUUploadStuPhoto.HasFile Then

    '            Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
    '            Dim ds As New DataSet
    '            Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
    '            ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")

    '                If FUUploadStuPhoto.PostedFile.ContentLength > iImgSize Then
    '                    lblError.Text = "Select Image Size Maximum 20KB"
    '                    Exit Sub
    '                End If
    '            End If
    '        End If
    '        Dim str_img As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
    '        Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
    '        Dim fs As New FileInfo(FUUploadStuPhoto.PostedFile.FileName)

    '        If Not Directory.Exists(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\") Then
    '            Directory.CreateDirectory(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\")
    '        Else
    '            Dim d As New DirectoryInfo(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\")

    '            Dim fi() As System.IO.FileInfo
    '            fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
    '            If fi.Length > 0 Then '' If Having Attachments
    '                For Each f As System.IO.FileInfo In fi
    '                    f.Delete()
    '                Next
    '            End If
    '        End If

    '        Dim str_tempfilename As String = FUUploadStuPhoto.FileName
    '        Dim strFilepath As String = str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
    '        'ImgHeightnWidth(strFilepath)
    '        FUUploadStuPhoto.PostedFile.SaveAs(strFilepath)
    '        Try
    '            If Not FUUploadStuPhoto.PostedFile.ContentType.Contains("image") Then
    '                Throw New Exception
    '            End If
    '            imgstuImage.ImageUrl = str_imgvirtual & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
    '            ViewState("EMPPHOTOFILEPATHoldPath") = str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
    '            ViewState("EMPPHOTOFILEPATH") = "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
    '        Catch ex As Exception
    '            File.Delete(strFilepath)
    '            UtilityObj.Errorlog("No Image found")
    '            imgstuImage.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
    '            imgstuImage.AlternateText = "No Image found"
    '        End Try
    '    End If
    'End Sub


    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        ViewState("popup") = 0
        ModalpopupUpload.Hide()
    End Sub
    Function callTrans_Student_M(ByVal trans As SqlTransaction) As Integer
        Try
            Dim status As Integer
            Dim STU_ID As String = ViewState("STUID")
            Dim STU_BSU_ID As String = Session("sBsuid")

            Dim PHOTO_PATH As String

            Dim str_img As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
            imgstuImage.ImageUrl = str_imgvirtual & ViewState("EMPPHOTOFILEPATH")
            imgstuImage.AlternateText = "No Image found"
            PHOTO_PATH = ViewState("EMPPHOTOFILEPATH")
            'Dim d As New DirectoryInfo(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID"))
            'Dim fi() As System.IO.FileInfo
            'fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            'If fi.Length > 0 Then '' If Having Attachments
            '    For Each f As System.IO.FileInfo In fi
            '        f.Delete()
            '    Next
            'End If
            'Dim strFilepath As String = str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
            'FUUploadStuPhoto.PostedFile.SaveAs(strFilepath)
            'File.Copy(ViewState("EMPPHOTOFILEPATHoldPath"), WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString & ViewState("EMPPHOTOFILEPATH"), True)
            Dim str_Sql As String = "UPDATE STUDENT_M SET STU_PHOTOPATH='" & PHOTO_PATH & "'  WHERE STU_ID= " & ViewState("STUID") & ""
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))
            param(1) = New SqlClient.SqlParameter("@PHOTOPATH", PHOTO_PATH)
            param(2) = New SqlClient.SqlParameter("@SchoolType", Session("School_Type"))
            param(3) = New SqlClient.SqlParameter("@User", Session("sUsr_name"))
            status = SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "UPDATE_STUDENTM_PHOTOPATH", param)
            Return status
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Private Sub SavePhotoToCRMINTEGRATION(ByVal photobase64 As String)
        Try
            Dim str_Sql As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_CRM_INTEGRATIONConnectionString").ConnectionString
            Dim photoPathSTU As String = "https://school.gemsoasis.com/OASISPHOTOS/OASIS_HR/ApplicantPhoto/" & Session("sBsuid") & "/" & ViewState("STUID") & "/STUPHOTO.JPG"
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SF_STUDENT_ID", ViewState("STUID"))
            param(1) = New SqlClient.SqlParameter("@PHOTO_BASE64", "")
            param(2) = New SqlClient.SqlParameter("@PHOTO_PATH", photoPathSTU)
            param(3) = New SqlClient.SqlParameter("@PHOTO_TYPE", "S")
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "dbo.SAVE_PHOTO_CRM_SF", param)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UpLoadPhoto()

        If FUUploadStuPhoto.FileName <> "" Then
            lblError.Text = ""
            'If Not FUUploadStuPhoto.PostedFile.ContentType.Contains("image") Then
            '    ''Throw New Exception
            '    lblError.Text = "Select Image Only"
            '    Exit Sub
            'End If

            If FUUploadStuPhoto.HasFile Then

                Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
                Dim ds As New DataSet
                Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")

                    If FUUploadStuPhoto.PostedFile.ContentLength > iImgSize Then
                        lblError.Text = "Select Image Size Maximum 20KB"
                        Exit Sub
                    End If
                End If
            End If
            Dim str_img As String = WebConfigurationManager.ConnectionStrings("empfilepath").ConnectionString
            Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
            Dim fs As New FileInfo(FUUploadStuPhoto.PostedFile.FileName)

            If Not Directory.Exists(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID")) Then
                Directory.CreateDirectory(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID"))
            Else
                Dim d As New DirectoryInfo(str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID"))

                Dim fi() As System.IO.FileInfo
                fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fi.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fi
                        f.Delete()
                    Next
                End If
            End If

            Dim str_tempfilename As String = FUUploadStuPhoto.FileName
            Dim strFilepath As String = str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
            '' nahyan to insert image as bas64 to crm integration 13SEP2017
            Dim fs1 As System.IO.Stream = FUUploadStuPhoto.PostedFile.InputStream
            Dim br As New System.IO.BinaryReader(fs1)
            Dim bytes As Byte() = br.ReadBytes(CType(fs1.Length, Integer))
            Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
            SavePhotoToCRMINTEGRATION(Convert.ToBase64String(bytes, 0, bytes.Length))
            ''nahyan ends here 
            ''ImgHeightnWidth(strFilepath)
            FUUploadStuPhoto.PostedFile.SaveAs(strFilepath)
            Try
                If Not FUUploadStuPhoto.PostedFile.ContentType.Contains("image") Then
                    Throw New Exception
                End If
                imgstuImage.ImageUrl = str_imgvirtual & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
                ViewState("EMPPHOTOFILEPATHoldPath") = str_img & "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
                ViewState("EMPPHOTOFILEPATH") = "\" & Session("sbsuid") & "\" & ViewState("STUID") & "\" & "STUPHOTO" & fs.Extension
            Catch ex As Exception
                'File.Delete(strFilepath)
                ' hfParent.Value = ""
                UtilityObj.Errorlog("No Image found")
                imgstuImage.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
                imgstuImage.AlternateText = "No Image found"
            End Try
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim status As Integer
        Dim transaction As SqlTransaction
        lblError.Text = ""
        Try
            UpLoadPhoto()
            If FUUploadStuPhoto.HasFile Then

                If Not FUUploadStuPhoto.PostedFile.ContentType.Contains("image") Then
                    ''Throw New Exception
                    lblError.Text = "Select Image Only"
                    Exit Sub
                End If
                ''''
                Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
                Dim ds As New DataSet
                Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")

                    If FUUploadStuPhoto.PostedFile.ContentLength > iImgSize Then
                        lblError.Text = "Select Image Size Maximum 20KB"
                        Exit Sub
                    End If
                    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                        transaction = conn.BeginTransaction("SampleTransaction")
                        status = callTrans_Student_M(transaction)

                        If status < 1 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        End If
                        lblError.Text = "Photo Saved Successfully"
                        gridbind()
                    End Using
                End If
            Else
                lblError.Text = "Select Image"
                Exit Sub
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Photo Upload Failed"
        End Try
    End Sub
End Class
