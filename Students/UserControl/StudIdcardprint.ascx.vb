Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_UserControls_StudIdcardprint
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblmessage.Text = ""
            Dim studClass As New studClass
            Hiddenbsuid.Value = Session("sbsuid")
            ddaccyear = studClass.PopulateAcademicYear(ddaccyear, Session("clm").ToString, Session("sbsuid").ToString)
            BindStudentView()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))

    End Sub
    Public Function GetData() As DataSet

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim strQuery = "SELECT STU_ID,STU_NO,STU_NAME,GRM_DISPLAY,SCT_DESCR,PHOTOAVALIABLE,STU_BUS_NO,STU_SBL_ID_PICKUP,STU_ACD_ID,STU_LEAVEDATE,STU_CURRSTATUS from (SELECT A.STU_ID,A.STU_NO,STU_NAME=(ISNULL(A.STU_FIRSTNAME,'')+' ' + ISNULL(A.STU_MIDNAME,'')+' '+ISNULL(A.STU_LASTNAME,''))," & _
                              " ISNULL(B.GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(C.SCT_DESCR,'') AS SCT_DESCR,CASE ISNULL(A.STU_PHOTOPATH,'') WHEN '' THEN 'False' ELSE 'True' END AS PHOTOAVALIABLE,CASE WHEN isNULL(OnwardBus,'') ='' Then '--'  ELSE    (CASE  WHEN OnwardBus=RETURNBUS THEN  OnwardBus ELSE + OnwardBus +'/' + RETURNBUS END) END AS STU_BUS_NO,ISNULL(TRANSP.STU_SBL_ID_PICKUP,'') as STU_SBL_ID_PICKUP,A.STU_ACD_ID,A.STU_LEAVEDATE,A.STU_CURRSTATUS" & _
                              " FROM oasis.dbo.STUDENT_M AS A INNER JOIN oasis.dbo.GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" & _
                              " INNER JOIN oasis.dbo.SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                              " LEFT JOIN OASIS_Transport.TRANSPORT.VV_STUDTRANSPORTINFO_TEMP TRANSP ON A.STU_ID=TRANSP.STU_ID AND A.STU_ACD_ID=TRANSP.STU_ACD_ID)TEMP " & _
                              " WHERE STU_ACD_ID = " + ddaccyear.SelectedValue.ToString + " AND STU_LEAVEDATE IS NULL and STU_CURRSTATUS NOT IN ('CN','TF')"

        If CheckOwnTransport.Checked Then
            strQuery &= " AND STU_SBL_ID_PICKUP='' "
        End If

        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String
        Dim txtBusno As String

        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()
            txtBusno = DirectCast(GrdStudents.HeaderRow.FindControl("txtBusno"), TextBox).Text.Trim()
            If txtnumber.Trim() <> "" Then
                strQuery &= " and STU_NO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                strQuery &= " and STU_NAME like '%" & txtname & "%' "
            End If

            If txtGrade.Trim() <> "" Then
                strQuery &= " and GRM_DISPLAY like '%" & txtGrade.Replace(" ", "") & "%' "
            End If

            If txtSection.Trim() <> "" Then
                strQuery &= " and SCT_DESCR like '%" & txtSection.Replace(" ", "") & "%' "
            End If
            If txtBusno.Trim() <> "" Then
                strQuery &= " and STU_BUS_NO like '%" & txtBusno.Replace(" ", "") & "%' "
            End If
        End If

        strQuery &= " order by GRM_DISPLAY,SCT_DESCR,STU_NAME,STU_NO "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        Dim total = 0
        Dim puploaded = 0
        Dim pnotuploaded = 0

        Dim i = 0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim PHOTOAVALIABLE = ds.Tables(0).Rows(i).Item("PHOTOAVALIABLE")

            total = total + 1
            If PHOTOAVALIABLE = "True" Then
                puploaded = puploaded + 1

            Else
                pnotuploaded = pnotuploaded + 1
            End If

        Next

        lblmessage.Text = "Total : " & total & " , Uploaded : " & puploaded & " , Not Uploaded : " & pnotuploaded

        Return ds




    End Function

    Public Sub BindStudentView()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String

        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

        End If

        Dim ds As DataSet

        ds = GetData()

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("stu_id")
            dt.Columns.Add("stu_no")
            dt.Columns.Add("MEMBERSHIP_DES")
            dt.Columns.Add("STU_NAME")
            dt.Columns.Add("GRM_DISPLAY")
            dt.Columns.Add("SCT_DESCR")
            dt.Columns.Add("PHOTOAVALIABLE")
            dt.Columns.Add("STU_BUS_NO")
            Dim dr As DataRow = dt.NewRow()
            dr("stu_id") = ""
            dr("stu_no") = ""
            dr("MEMBERSHIP_DES") = ""
            dr("STU_NAME") = ""
            dr("GRM_DISPLAY") = ""
            dr("SCT_DESCR") = ""
            dr("PHOTOAVALIABLE") = "false"
            dr("STU_BUS_NO") = ""
            dt.Rows.Add(dr)
            GrdStudents.DataSource = dt
            GrdStudents.DataBind()
            'DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button).Visible = False
            DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = False
        Else
            GrdStudents.DataSource = ds
            GrdStudents.DataBind()
            DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))

        End If

        If GrdStudents.Rows.Count > 0 Then

            DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text = txtGrade
            DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text = txtSection

        End If



    End Sub

    Protected Sub GridMemberships_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStudents.RowCommand

        'If e.CommandName = "Deleting" Then
        '    Dim Record_id = e.CommandArgument
        '    Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        '    Dim transaction As SqlTransaction
        '    connection.Open()
        '    transaction = connection.BeginTransaction()
        '    Try
        '        '' Check if item taken for this membership
        '        Dim sqlquery = " SELECT * FROM dbo.LIBRARY_MEMBERSHIP_USERS A " & _
        '                       " INNER JOIN dbo.LIBRARY_MEMBERSHIPS B ON A.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
        '                       " INNER JOIN dbo.LIBRARY_TRANSACTIONS C ON C.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
        '                       " WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND A.RECORD_ID='" & Record_id & "' " & _
        '                       " AND C.USER_ID=(SELECT USER_ID FROM dbo.LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' AND USER_TYPE='STUDENT') "

        '        Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sqlquery)

        '        If ds.Tables(0).Rows.Count > 0 Then
        '            lblMessage.Text = "Library Memebership is in use. <br> " & ds.Tables(0).Rows.Count & " Item(s) has been taken by this user using this membership. Record cannot be deleted.Return the item(s) and proceed with deletion."
        '        Else
        '            Dim query = "DELETE LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' "
        '            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
        '            transaction.Commit()
        '            lblMessage.Text = "User Membership Deleted Successfully."
        '        End If


        '    Catch ex As Exception
        '        lblMessage.Text = "Error while transaction. Error :" & ex.Message
        '        transaction.Rollback()

        '    Finally
        '        connection.Close()

        '    End Try

        '    MO1.Show()

        '    BindStudentView()

        'End If

        If e.CommandName = "barcode" Then

            HiddenShowFlag.Value = 1
            Dim hash As New Hashtable

            Session("CardhashtableAll") = Nothing
            Session("Cardhashtable") = Nothing
            Session("PromoteGrade") = Nothing
            'If Session("Cardhashtable") Is Nothing Then
            'Else
            '    hash = Session("Cardhashtable")
            'End If


            For Each row As GridViewRow In GrdStudents.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("CheckBar1"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("HiddensStuid"), HiddenField)
                Dim key = Hid.Value
                If ch.Checked Then
                    If hash.ContainsKey(key) Then
                    Else
                        hash.Add(key, Hid.Value)
                    End If

                Else

                    If hash.ContainsKey(key) Then
                        hash.Remove(key)
                    Else
                    End If
                End If
            Next

            If hash.Count = 0 Then
                HiddenShowFlag.Value = 0
            End If

            'If DirectCast(GrdStudents.FooterRow.FindControl("CheckIssueAll"), CheckBox).Checked Then

            '    hash.Clear()

            '    Session("CardhashtableAll") = "All"
            '    HiddenShowFlag.Value = 1
            'End If

            If check_next_accid.Checked Then
                Session("PromoteGrade") = True
            Else
                Session("PromoteGrade") = False
            End If

            Session("Cardhashtable") = hash

        End If


    End Sub



    'Protected Sub GrdStudents_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStudents.RowCommand

    '    If e.CommandName = "save" Then
    '        MO3.Show()
    '    End If

    '    If e.CommandName = "search" Then
    '        BindStudentView()
    '    End If

    'End Sub

    Protected Sub GrdStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdStudents.PageIndexChanging
        GrdStudents.PageIndex = e.NewPageIndex
        BindStudentView()
    End Sub

    Protected Sub ddaccyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddaccyear.SelectedIndexChanged
        BindStudentView()
    End Sub

    Protected Sub ImageSearchStuno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchStuname_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchgrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchsection_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub CheckOwnTransport_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckOwnTransport.CheckedChanged
        BindStudentView()
    End Sub
    Protected Sub ImageSearchBusNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub


End Class
