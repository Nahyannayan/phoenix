Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_UserControls_StudIdcardprint_Schools
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblmessage.Text = ""
            Dim studClass As New studClass
            BindBSUnit()
            'ddlSchool.DataBind()
            Hiddenbsuid.Value = ddlSchool.SelectedValue
            ''commmented by nahyan 8-10-2018
            ''   ddaccyear = PopulateAcademicYear(ddaccyear, Session("clm").ToString, ddlSchool.SelectedValue.ToString)
            FillACD(ddlSchool.SelectedValue.ToString)
            Session("printID_Transport_paper") = chkPrintOnPaper.Checked
            trFilterDetails.Visible = chkPrintOnPaper.Checked

            lblmessage.Visible = Not chkPrintOnPaper.Checked
            BindStudentView()
            BindBusNo()
            Crdtype()
            Session("Suppress_Photo") = "False"
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
        trSuppress.Cells(1).Visible = Not chkPrintOnPaper.Checked
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))

    End Sub
    'Public Sub Crdtype()
    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringGPS").ConnectionString
    '    Dim query = " select * from GPS_CARD_TYPES where MODULE='GPS' ORDER BY CARD_TYPE_DESC"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
    '    ddcardtype.DataSource = ds
    '    ddcardtype.DataTextField = "CARD_TYPE_DESC"
    '    ddcardtype.DataValueField = "ID"
    '    ddcardtype.DataBind()
    '    Dim list As New ListItem
    '    list.Text = "Select Card Type"
    '    list.Value = "-1"
    '    ddcardtype.Items.Insert(0, list)


    'End Sub
    Public Sub Crdtype()
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sUsr_name")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.StoredProcedure, "Get_PrintIDCARD_FORMAT_CUST", pParms)



        'Dim strQuery = "SELECT BSU_ID, BSU_NAME FROM BUSINESSUNIT_M A INNER JOIN UserACCESS_MENU B ON BSU_ID=USM_WBSU_ID AND USM_MNU_CODE='H000099' WHERE ISNULL(BSU_bSHOW, 0) = 1 "
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, strQuery)
        ddcardtype.DataSource = ds
        ddcardtype.DataTextField = "CARD_TYPE_DESC"
        ddcardtype.DataValueField = "ID"
        ddcardtype.DataBind()
    End Sub

    Public Sub BindBSUnit()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(2) As SqlClient.SqlParameter
        ''pParms(0) = New SqlClient.SqlParameter("@bGetAll", chkPrintOnPaper.Checked)
        pParms(0) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
        ''commented by nahyan on 8oct2015 to display bsu based on user access
        ''  Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[TRANSPORT].[GETSTSIDCARDSCHOOLS_CUST]", pParms)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[GetUserBusinessUnitsSchools]", pParms)

        ddlSchool.DataSource = ds
        ddlSchool.DataTextField = "BSU_NAME"
        ddlSchool.DataValueField = "BSU_ID"
        ddlSchool.DataBind()
    End Sub

    Sub FillACD(ByVal bsuid As String)
        Dim dtACD As DataTable = FeeCommon.GetBSUAcademicYear(bsuid)
        ddaccyear.DataSource = dtACD
        ddaccyear.DataTextField = "ACY_DESCR"
        ddaccyear.DataValueField = "ACD_ID"
        ddaccyear.DataBind()

        Dim li As New ListItem
        'li.Text = ds.Tables(0).Rows(0).Item(0)

        'ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        For Each rowACD As DataRow In dtACD.Rows
            li.Value = rowACD("ACD_ID")
            If rowACD("ACD_CURRENT") Then
                ddaccyear.Items.FindByValue(rowACD("ACD_ID")).Selected = True

                Exit For
            End If
        Next
    End Sub

    Public Function PopulateAcademicYear(ByVal ddlAcademicYear As DropDownList, ByVal clm As String, ByVal bsuid As String)
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + bsuid + "'" _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + bsuid + "'"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        'Dim rdr As SqlDataReader
        'Using rdr
        '    rdr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        '    ' Using rdr As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, str_query)
        '    Dim li As New ListItem
        '    While rdr.Read
        '        li.Text = rdr.GetString(0)
        '        li.Value = rdr.GetValue(1)
        '        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        '    End While
        '    ' End Using
        '    rdr.Close()
        'End Using
        Return ddlAcademicYear
    End Function

    Sub BindBusNo()
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT BNO_ID,BNO_DESCR FROM TRANSPORT.BUSNOS_M WHERE BNO_BSU_ID='" + ddlSchool.SelectedValue.ToString + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBusNo.DataSource = ds
        ddlBusNo.DataTextField = "BNO_DESCR"
        ddlBusNo.DataValueField = "BNO_ID"
        ddlBusNo.DataBind()


        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "ALL"
        ddlBusNo.Items.Insert(0, li)

    End Sub

    Public Function GetAllStudentsWithFilter() As Hashtable

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        Dim strQuery
        strQuery = "SELECT STU_ID FROM(SELECT A.STU_ID,A.STU_NO,STU_NAME=(ISNULL(A.STU_FIRSTNAME,'')+' ' + ISNULL(A.STU_MIDNAME,'')+' '+ISNULL(A.STU_LASTNAME,''))," & _
                   " ISNULL(B.GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(C.SCT_DESCR,'') AS SCT_DESCR " & _
                   ", A.STU_ACD_ID, A.STU_LEAVEDATE, A.STU_CURRSTATUS, " & _
                   " CASE ISNULL(A.STU_PHOTOPATH,'') WHEN '' THEN 'False' ELSE 'True' END AS PHOTOAVALIABLE, " & _
                   " CASE WHEN isNULL(OnwardBus,'') ='' Then '--'  ELSE    (CASE  WHEN OnwardBus=RETURNBUS THEN  OnwardBus ELSE + OnwardBus +'/' + RETURNBUS END) END AS STU_BUS_NO,ISNULL(TRANSP.STU_SBL_ID_PICKUP,'') as STU_SBL_ID_PICKUP, OnwardBus , RETURNBUS " & _
                   " FROM STUDENT_M AS A INNER JOIN VW_OSO_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" & _
                   " INNER JOIN VV_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                   " LEFT JOIN TRANSPORT.VV_STUDTRANSPORTINFO_TEMP TRANSP ON A.STU_ID=TRANSP.STU_ID AND A.STU_ACD_ID=TRANSP.STU_ACD_ID)TEMP " & _
                   " WHERE STU_ACD_ID = " + ddaccyear.SelectedValue.ToString + " AND STU_LEAVEDATE IS NULL and STU_CURRSTATUS ='EN'"
        If Not chkPrintOnPaper.Checked Then
            If ddlPhotoStat.SelectedItem.Value = "1" Then
                strQuery = strQuery & " AND PHOTOAVALIABLE='True'"
            ElseIf ddlPhotoStat.SelectedItem.Value = "2" Then
                strQuery = strQuery & " AND PHOTOAVALIABLE='false'"
            End If
        End If

        If chkPrintOnPaper.Checked Then
            strQuery = strQuery & ViewState("busFilter")
        End If

        'If CheckOwnTransport.Checked Then
        '    strQuery &= " AND STU_SBL_ID_PICKUP='' "
        'End If

        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String
        Dim txtBusno As String

        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()
            txtBusno = DirectCast(GrdStudents.HeaderRow.FindControl("txtBusno"), TextBox).Text.Trim()
            If txtnumber.Trim() <> "" Then
                strQuery &= " and STU_NO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                strQuery &= " and STU_NAME like '%" & txtname & "%' "
            End If

            If txtGrade.Trim() <> "" Then
                strQuery &= " and GRM_DISPLAY like '%" & txtGrade.Replace(" ", "") & "%' "
            End If

            If txtSection.Trim() <> "" Then
                strQuery &= " and SCT_DESCR like '%" & txtSection.Replace(" ", "") & "%' "
            End If
            If txtBusno.Trim() <> "" Then
                strQuery &= " and STU_BUS_NO like '%" & txtBusno.Replace(" ", "") & "%' "
            End If
        End If

        strQuery &= " order by GRM_DISPLAY,SCT_DESCR,STU_NAME,STU_NO "

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, strQuery)
        Dim htTemp As New Hashtable
        While (dr.Read())
            htTemp(dr("STU_ID")) = dr("STU_ID")
        End While

        Return htTemp
    End Function

    Public Function GetData(ByVal bCheckFilter As Boolean) As DataSet

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString

        'Dim strQuery = "SELECT STU_ID,STU_NO,STU_NAME,GRM_DISPLAY,SCT_DESCR,PHOTOAVALIABLE,STU_BUS_NO,STU_SBL_ID_PICKUP,STU_ACD_ID,STU_LEAVEDATE,STU_CURRSTATUS from (SELECT A.STU_ID,A.STU_NO,STU_NAME=(ISNULL(A.STU_FIRSTNAME,'')+' ' + ISNULL(A.STU_MIDNAME,'')+' '+ISNULL(A.STU_LASTNAME,''))," & _
        '                      " ISNULL(B.GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(C.SCT_DESCR,'') AS SCT_DESCR,CASE ISNULL(A.STU_PHOTOPATH,'') WHEN '' THEN 'False' ELSE 'True' END AS PHOTOAVALIABLE,CASE WHEN isNULL(OnwardBus,'') ='' Then '--'  ELSE    (CASE  WHEN OnwardBus=RETURNBUS THEN  OnwardBus ELSE + OnwardBus +'/' + RETURNBUS END) END AS STU_BUS_NO,ISNULL(TRANSP.STU_SBL_ID_PICKUP,'') as STU_SBL_ID_PICKUP,A.STU_ACD_ID,A.STU_LEAVEDATE,A.STU_CURRSTATUS" & _
        '                      " FROM STUDENT_M AS A INNER JOIN VW_OSO_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" & _
        '                      " INNER JOIN VV_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
        '                      " LEFT JOIN TRANSPORT.VV_STUDTRANSPORTINFO_TEMP TRANSP ON A.STU_ID=TRANSP.STU_ID)TEMP " & _
        '                      " WHERE STU_ACD_ID = " + ddaccyear.SelectedValue.ToString + " AND STU_LEAVEDATE IS NULL and STU_CURRSTATUS <>'CN'"
        Dim strQuery
        If chkPrintOnPaper.Checked Then
            strQuery = "SELECT STU_ID,STU_NO,STU_NAME,GRM_DISPLAY,SCT_DESCR,PHOTOAVALIABLE,STU_BUS_NO,STU_SBL_ID_PICKUP,STU_ACD_ID,STU_LEAVEDATE,STU_CURRSTATUS, OnwardBus, RETURNBUS from (SELECT A.STU_ID,A.STU_NO,STU_NAME=(ISNULL(A.STU_FIRSTNAME,'')+' ' + ISNULL(A.STU_MIDNAME,'')+' '+ISNULL(A.STU_LASTNAME,''))," & _
                       " ISNULL(B.GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(C.SCT_DESCR,'') AS SCT_DESCR,CASE ISNULL(A.STU_PHOTOPATH,'') WHEN '' THEN 'True' ELSE 'True' END AS PHOTOAVALIABLE,CASE WHEN isNULL(OnwardBus,'') ='' Then '--'  ELSE    (CASE  WHEN OnwardBus=RETURNBUS THEN  OnwardBus ELSE + OnwardBus +'/' + RETURNBUS END) END AS STU_BUS_NO,ISNULL(TRANSP.STU_SBL_ID_PICKUP,'') as STU_SBL_ID_PICKUP,A.STU_ACD_ID,A.STU_LEAVEDATE,A.STU_CURRSTATUS, OnwardBus , RETURNBUS " & _
                       " FROM STUDENT_M AS A INNER JOIN VW_OSO_GRADE_BSU_M AS B with (nolock) ON A.STU_GRM_ID=B.GRM_ID" & _
                       " INNER JOIN VV_SECTION_M AS C with (nolock) ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                       " LEFT JOIN TRANSPORT.VV_STUDTRANSPORTINFO_TEMP TRANSP with (nolock) ON A.STU_ID=TRANSP.STU_ID AND A.STU_ACD_ID=TRANSP.STU_ACD_ID)TEMP " & _
                       " WHERE STU_ACD_ID = " + ddaccyear.SelectedValue.ToString + " AND STU_LEAVEDATE IS NULL and STU_CURRSTATUS ='EN'"

        Else

            strQuery = "SELECT STU_ID,STU_NO,STU_NAME,GRM_DISPLAY,SCT_DESCR,PHOTOAVALIABLE,STU_BUS_NO,STU_SBL_ID_PICKUP,STU_ACD_ID,STU_LEAVEDATE,STU_CURRSTATUS, OnwardBus, RETURNBUS from (SELECT A.STU_ID,A.STU_NO,STU_NAME=(ISNULL(A.STU_FIRSTNAME,'')+' ' + ISNULL(A.STU_MIDNAME,'')+' '+ISNULL(A.STU_LASTNAME,''))," & _
                       " ISNULL(B.GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(C.SCT_DESCR,'') AS SCT_DESCR,CASE ISNULL(A.STU_PHOTOPATH,'') WHEN '' THEN 'false' ELSE 'True' END AS PHOTOAVALIABLE,CASE WHEN isNULL(OnwardBus,'') ='' Then '--'  ELSE    (CASE  WHEN OnwardBus=RETURNBUS THEN  OnwardBus ELSE + OnwardBus +'/' + RETURNBUS END) END AS STU_BUS_NO,ISNULL(TRANSP.STU_SBL_ID_PICKUP,'') as STU_SBL_ID_PICKUP,A.STU_ACD_ID,A.STU_LEAVEDATE,A.STU_CURRSTATUS, OnwardBus , RETURNBUS " & _
                       " FROM STUDENT_M AS A WITH (NOloCK) INNER JOIN VW_OSO_GRADE_BSU_M AS B with (nolock) ON A.STU_GRM_ID=B.GRM_ID" & _
                       " INNER JOIN VV_SECTION_M AS C with (nolock) ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                       " LEFT JOIN TRANSPORT.VV_STUDTRANSPORTINFO_TEMP TRANSP with (nolock) ON A.STU_ID=TRANSP.STU_ID AND A.STU_ACD_ID=TRANSP.STU_ACD_ID)TEMP " & _
                       " WHERE STU_ACD_ID = " + ddaccyear.SelectedValue.ToString + " AND STU_LEAVEDATE IS NULL and STU_CURRSTATUS ='EN'"
        End If

        If chkPrintOnPaper.Checked = False Then
            If ddlPhotoStat.SelectedItem.Value = "1" Then
                strQuery = strQuery & " AND PHOTOAVALIABLE='True'"
            ElseIf ddlPhotoStat.SelectedItem.Value = "2" Then
                strQuery = strQuery & " AND PHOTOAVALIABLE='false'"
            End If
        End If

        If bCheckFilter Then
            strQuery = strQuery & ViewState("busFilter")
        End If

        'If CheckOwnTransport.Checked Then
        '    strQuery &= " AND STU_SBL_ID_PICKUP='' "
        'End If

        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String
        Dim txtBusno As String

        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()
            txtBusno = DirectCast(GrdStudents.HeaderRow.FindControl("txtBusno"), TextBox).Text.Trim()
            If txtnumber.Trim() <> "" Then
                strQuery &= " and STU_NO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                strQuery &= " and STU_NAME like '%" & txtname & "%' "
            End If

            If txtGrade.Trim() <> "" Then
                strQuery &= " and GRM_DISPLAY like '%" & txtGrade.Replace(" ", "") & "%' "
            End If

            If txtSection.Trim() <> "" Then
                strQuery &= " and SCT_DESCR like '%" & txtSection.Replace(" ", "") & "%' "
            End If
            If txtBusno.Trim() <> "" Then
                strQuery &= " and STU_BUS_NO like '%" & txtBusno.Replace(" ", "") & "%' "
            End If
        End If

        strQuery &= " order by GRM_DISPLAY,SCT_DESCR,STU_NAME,STU_NO "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        Dim total = 0
        Dim puploaded = 0
        Dim pnotuploaded = 0

        Dim i = 0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim PHOTOAVALIABLE = ds.Tables(0).Rows(i).Item("PHOTOAVALIABLE")

            total = total + 1
            If PHOTOAVALIABLE = "True" Then
                puploaded = puploaded + 1

            Else
                pnotuploaded = pnotuploaded + 1
            End If

        Next

        lblmessage.Text = "Total : " & total & " , Uploaded : " & puploaded & " , Not Uploaded : " & pnotuploaded

        Return ds




    End Function

    Public Sub BindStudentView(Optional ByVal bCheckFilter As Boolean = False)
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String

        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

        End If

        Dim ds As DataSet

        ds = GetData(bCheckFilter)

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("stu_id")
            dt.Columns.Add("stu_no")
            dt.Columns.Add("MEMBERSHIP_DES")
            dt.Columns.Add("STU_NAME")
            dt.Columns.Add("GRM_DISPLAY")
            dt.Columns.Add("SCT_DESCR")
            dt.Columns.Add("PHOTOAVALIABLE")
            dt.Columns.Add("STU_BUS_NO")
            Dim dr As DataRow = dt.NewRow()
            dr("stu_id") = ""
            dr("stu_no") = ""
            dr("MEMBERSHIP_DES") = ""
            dr("STU_NAME") = ""
            dr("GRM_DISPLAY") = ""
            dr("SCT_DESCR") = ""
            dr("PHOTOAVALIABLE") = "false"
            dr("STU_BUS_NO") = ""
            dt.Rows.Add(dr)
            GrdStudents.DataSource = dt
            GrdStudents.DataBind()
            'DirectCast(GrdStudents.FooterRow.FindControl("btnSave"), Button).Visible = False
            DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = False
        Else
            GrdStudents.DataSource = ds
            GrdStudents.DataBind()
            DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))

        End If

        If GrdStudents.Rows.Count > 0 Then

            DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text = txtGrade
            DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text = txtSection

        End If



    End Sub

    Protected Sub GridMemberships_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStudents.RowCommand

        'If e.CommandName = "Deleting" Then
        '    Dim Record_id = e.CommandArgument
        '    Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString)
        '    Dim transaction As SqlTransaction
        '    connection.Open()
        '    transaction = connection.BeginTransaction()
        '    Try
        '        '' Check if item taken for this membership
        '        Dim sqlquery = " SELECT * FROM dbo.LIBRARY_MEMBERSHIP_USERS A " & _
        '                       " INNER JOIN dbo.LIBRARY_MEMBERSHIPS B ON A.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
        '                       " INNER JOIN dbo.LIBRARY_TRANSACTIONS C ON C.MEMBERSHIP_ID=B.MEMBERSHIP_ID " & _
        '                       " WHERE ISNULL(ITEM_ACTUAL_RETURN_DATE,'')='' AND A.RECORD_ID='" & Record_id & "' " & _
        '                       " AND C.USER_ID=(SELECT USER_ID FROM dbo.LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' AND USER_TYPE='STUDENT') "

        '        Dim ds As DataSet = SqlHelper.ExecuteDataset(transaction, CommandType.Text, sqlquery)

        '        If ds.Tables(0).Rows.Count > 0 Then
        '            lblMessage.Text = "Library Memebership is in use. <br> " & ds.Tables(0).Rows.Count & " Item(s) has been taken by this user using this membership. Record cannot be deleted.Return the item(s) and proceed with deletion."
        '        Else
        '            Dim query = "DELETE LIBRARY_MEMBERSHIP_USERS WHERE RECORD_ID='" & Record_id & "' "
        '            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, query)
        '            transaction.Commit()
        '            lblMessage.Text = "User Membership Deleted Successfully."
        '        End If


        '    Catch ex As Exception
        '        lblMessage.Text = "Error while transaction. Error :" & ex.Message
        '        transaction.Rollback()

        '    Finally
        '        connection.Close()

        '    End Try

        '    MO1.Show()

        '    BindStudentView()

        'End If

        If e.CommandName = "barcode" Then

            HiddenShowFlag.Value = 1
            Dim hash As New Hashtable

            Session("CardhashtableAll") = Nothing
            Session("Cardhashtable") = Nothing
            Session("PromoteGrade") = Nothing
            'If Session("Cardhashtable") Is Nothing Then
            'Else
            '    hash = Session("Cardhashtable")
            'End If
            Dim chkAll As CheckBox = DirectCast(GrdStudents.FooterRow.FindControl("chkPrintAll"), CheckBox)

            If chkAll.Checked Then
                hash = GetAllStudentsWithFilter()
            Else
                For Each row As GridViewRow In GrdStudents.Rows
                    Dim ch As CheckBox = DirectCast(row.FindControl("CheckBar1"), CheckBox)
                    Dim Hid As HiddenField = DirectCast(row.FindControl("HiddensStuid"), HiddenField)
                    Dim key = Hid.Value
                    If ch.Checked Then
                        If hash.ContainsKey(key) Then
                        Else
                            hash.Add(key, Hid.Value)
                        End If
                    Else
                        If hash.ContainsKey(key) Then
                            hash.Remove(key)
                        Else
                        End If
                    End If
                Next
                If hash.Count = 0 Then
                    HiddenShowFlag.Value = 0
                End If
            End If

            'If DirectCast(GrdStudents.FooterRow.FindControl("CheckIssueAll"), CheckBox).Checked Then

            '    hash.Clear()

            '    Session("CardhashtableAll") = "All"
            '    HiddenShowFlag.Value = 1
            'End If

            'If check_next_accid.Checked Then
            '    Session("PromoteGrade") = True
            'Else
            '    Session("PromoteGrade") = False
            'End If

            Session("Cardhashtable") = hash

        End If


    End Sub



    'Protected Sub GrdStudents_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStudents.RowCommand

    '    If e.CommandName = "save" Then
    '        MO3.Show()
    '    End If

    '    If e.CommandName = "search" Then
    '        BindStudentView()
    '    End If

    'End Sub

    Protected Sub GrdStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdStudents.PageIndexChanging
        GrdStudents.PageIndex = e.NewPageIndex
        BindStudentView(chkPrintOnPaper.Checked)
    End Sub

    Protected Sub ddaccyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddaccyear.SelectedIndexChanged
        BindStudentView()
    End Sub

    Protected Sub ImageSearchStuno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchStuname_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchgrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchsection_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchBusNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub


    Protected Sub ddlSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSchool.SelectedIndexChanged
        Hiddenbsuid.Value = ddlSchool.SelectedValue
        ''commmented by nahyan 8-10-2018
        ''   ddaccyear = PopulateAcademicYear(ddaccyear, Session("clm").ToString, ddlSchool.SelectedValue.ToString)
        FillACD(ddlSchool.SelectedValue.ToString)
        BindBusNo()
        BindStudentView()

    End Sub

    Protected Sub chkPrintOnPaper_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPrintOnPaper.CheckedChanged
        Session("printID_Transport_paper") = chkPrintOnPaper.Checked
        trFilterDetails.Visible = chkPrintOnPaper.Checked
        lblmessage.Visible = Not chkPrintOnPaper.Checked
        BindBSUnit()
        ''commmented by nahyan 8-10-2018
        ''   ddaccyear = PopulateAcademicYear(ddaccyear, Session("clm").ToString, ddlSchool.SelectedValue.ToString)
        FillACD(ddlSchool.SelectedValue.ToString)
        BindStudentView(True)
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        SetFilter()
        BindStudentView(True)
    End Sub

    Private Sub SetFilter()
        'ViewState("busFilter")
        If ddlBusNo.SelectedValue <> "ALL" Then
            If Not radAll.Checked Then
                If radOnwards.Checked Then
                    ViewState("busFilter") = " AND OnwardBus = '" & ddlBusNo.SelectedItem.Text & "' "
                    Session("printID_Tran_paper_TripType") = 1
                ElseIf radReturn.Checked Then
                    ViewState("busFilter") = " AND RETURNBUS = '" & ddlBusNo.SelectedItem.Text & "' "
                    Session("printID_Tran_paper_TripType") = 2
                End If
            Else
                Session("printID_Tran_paper_TripType") = 3
                ViewState("busFilter") = " AND ((RETURNBUS = '" & ddlBusNo.SelectedItem.Text & "') OR (OnwardBus = '" & ddlBusNo.SelectedItem.Text & "'))"
            End If
        Else
            Session("printID_Tran_paper_TripType") = Nothing
            ViewState("busFilter") = ""
        End If
    End Sub


    Protected Sub ddlPhotoStat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhotoStat.SelectedIndexChanged
        BindStudentView()

    End Sub

    Protected Sub chkPhotoSuprs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPhotoSuprs.CheckedChanged
        Session("Suppress_Photo") = chkPhotoSuprs.Checked
    End Sub


End Class
