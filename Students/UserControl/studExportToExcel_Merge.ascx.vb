Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports GemBox.Spreadsheet
Partial Class Students_UserControl_studExportToExcel_Merge
    Inherits System.Web.UI.UserControl
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        BindPartCControls()

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnMerge)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnBack)
        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 

                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200448") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ' btnMerge.Attributes.Add("onclick", "hideButton()")
                    bindSortField()
                    Session("PartD_CER") = CreateDataTable()
                    Session("PartD_CER").Rows.Clear()
                    ViewState("id") = 1
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub
    Public Sub bindSortField()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim SET_TYPE As String = Encr_decrData.Decrypt(Request.QueryString("S_type").Replace(" ", "+"))
        Dim str_query = "Select CCT_FIELD_NAME,CCT_FIELD_DESC from COM_CER_TYPE  where cct_type= '" & SET_TYPE & "' order by CCT_DISPLAY_ORDER,cct_field_desc"

        Using readerfield As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            If readerfield.HasRows = True Then
                While readerfield.Read
                    ddlFields.Items.Add(New ListItem(readerfield("CCT_FIELD_DESC"), readerfield("CCT_FIELD_NAME")))

                End While
            End If
        End Using



    End Sub
    Public Sub BindPartCControls()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim SET_TYPE As String = Encr_decrData.Decrypt(Request.QueryString("S_type").Replace(" ", "+"))
        Dim str_query = "Select distinct CCT_DISPLAY_ORDER,CCT_DESC from COM_CER_TYPE  WHERE CCT_TYPE ='" & SET_TYPE & "' order by CCT_DISPLAY_ORDER"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        DataListPartC.DataSource = ds
        DataListPartC.DataBind()
        For Each row As DataListItem In DataListPartC.Items
            Dim CCT_DISPLAY = DirectCast(row.FindControl("HiddenCCT_DISPLAY"), HiddenField).Value
            Dim dc As DataSet
            Dim CBLDynamic As New CheckBoxList
            CBLDynamic.ID = "|" & CCT_DISPLAY & "|"

            Dim inner_str_query As String = "Select CCT_FIELD_NAME,CCT_FIELD_DESC from COM_CER_TYPE WHERE CCT_TYPE ='" & SET_TYPE & "' And CCT_DISPLAY_ORDER='" & CCT_DISPLAY & "' order by CCT_ID"
            dc = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, inner_str_query)
            CBLDynamic.DataSource = dc
            CBLDynamic.DataTextField = "CCT_FIELD_DESC"
            CBLDynamic.DataValueField = "CCT_FIELD_NAME"
            CBLDynamic.DataBind()
            For Each clist As ListItem In CBLDynamic.Items
                clist.Attributes.Add("onclick", "javascript:uncheckall(this);")
            Next
            Dim list As New ListItem
            'list.Text = "<span style='background: wheat; border-width: medium 0; border-style: solid; font-weight:bold'>All    </span>"
            list.Text = "All"
            list.Value = "0"
            list.Attributes.Add("onclick", "javascript:change_chk_state(this);")

            CBLDynamic.Items.Insert(0, list)

            Dim PanelHolder As Panel = DirectCast(row.FindControl("PanelControl"), Panel)
            PanelHolder.Controls.Add(CBLDynamic)
        Next



    End Sub
    Public Function Merge_FilterQuery() As String



        Dim PCfilter As String = ""

        'Part B
        For Each item As DataListItem In DataListPartC.Items
            Dim cboxlist As New CheckBoxList
            Dim CCT_DISPLAY As String = DirectCast(item.FindControl("HiddenCCT_DISPLAY"), HiddenField).Value
            cboxlist = DirectCast(item.FindControl("|" & CCT_DISPLAY & "|"), CheckBoxList)

            Dim temp As String = ""
            Dim i = 0

            For i = 0 To cboxlist.Items.Count - 1
                Dim filter As String = ""
                If cboxlist.Items(i).Selected Then

                    If i = 0 Then
                        If Trim(cboxlist.Items(i).Value) <> "0" Then
                            filter += cboxlist.Items(i).Value
                        End If
                    Else
                        If Trim(cboxlist.Items(i).Value) <> "0" Then
                            filter += cboxlist.Items(i).Value & ","
                        End If
                    End If

                End If
                temp += filter
            Next

            If temp <> "" Then


                PCfilter += temp
            End If

        Next

        Return PCfilter.TrimEnd(",")
    End Function
    Function calltransaction(ByRef errorMessage As String, ByVal C_QUERY As String) As Integer

        Dim CCQ_NAME As String = Session("Certi")
        Dim CCQ_QUERY As String = C_QUERY
        Dim CCQ_BSU_ID As String = Session("sBsuid")
        Dim CCQ_CREATE_DT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        Dim CCQ_TYPE As String = Encr_decrData.Decrypt(Request.QueryString("S_type").Replace(" ", "+"))



        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer


                status = AccessStudentClass.SaveCOM_CER_QUERY(CCQ_NAME, CCQ_BSU_ID, CCQ_QUERY, _
                CCQ_CREATE_DT, CCQ_TYPE, transaction)


                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If


                Session("Certi") = ""
                calltransaction = "0"
                'btnMerge.Visible = False
                'btnBack.Text = "Back"

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                   
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function

    Protected Sub btnMerge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMerge.Click
        Try


            If Session("Certi") <> "" Then

                Dim Select_field As String = Merge_FilterQuery()
                If Select_field <> "" Then

                    Dim j As Integer = 0
                    Dim dt As DataTable = Nothing
                    dt = Session("PartD_CER")

                    ' Presuming the DataTable has a column named Date.
                    Dim expression As String = "id <>-1"

                    ' Sort descending by column named CompanyName.
                    Dim sortOrder As String = "SortOrder ASC"
                    Dim foundRows() As DataRow

                    ' Use the Select method to find all rows matching the filter.
                    foundRows = dt.Select(expression, sortOrder)

                    Dim i As Integer
                    Dim PDORDER As String = ""
                    ' Print column 0 of each returned row.
                    For i = 0 To foundRows.GetUpperBound(0)
                        PDORDER += foundRows(i)("STRCOND") + " , "

                    Next i

                    PDORDER = Trim(PDORDER).TrimEnd(",")

                    If PDORDER.Length > 1 Then
                        PDORDER = " Order by " & PDORDER
                    End If
                    Dim str_conn = ConnectionManger.GetOASISConnectionString
                    Dim SID As String = Encr_decrData.Decrypt(Request.QueryString("SID").Replace(" ", "+"))
                    Dim CDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                    Dim ACY_ID As Integer = Session("EXCEL_ACY_ID")
                    Dim CLM_ID As Integer = Session("EXCEL_CLM_ID")
                    Dim SHF_ID As Integer = Session("EXCEL_SHF_ID")
                    Dim STM_ID As Integer = Session("EXCEL_STM_ID")


                    Dim ASONDT As String = IIf(Session("EXCEL_ASDT").ToString.Trim = "", "NULL", "'" & Session("EXCEL_ASDT") & "'")


                    Dim str_query As String = String.Empty




                    If Session("Comm_Query") = "" Then
                        str_query = "Select " & Select_field & " from FU_STU_CER_DETAILS_NEW('" & SID & "','" & ACY_ID & "','" & CLM_ID & "','" & SHF_ID & "','" & STM_ID & "'," & ASONDT & ") " & PDORDER
                    Else
                        str_query = "Select " & Select_field & " from FU_STU_CER_DETAILS_NEW('" & SID & "','" & ACY_ID & "','" & CLM_ID & "','" & SHF_ID & "','" & STM_ID & "'," & ASONDT & ") WHERE " & Session("Comm_Query") & PDORDER
                    End If
                    Session.Remove("EXCEL_ACY_ID")
                    Session.Remove("EXCEL_CLM_ID")
                    Session.Remove("EXCEL_SHF_ID")
                    Session.Remove("EXCEL_STM_ID")

                    Session.Remove("PartD_CER")

                    Dim dtEXCEL As New DataTable()


                    Dim str_err As String = String.Empty
                    Dim errorMessage As String = String.Empty

                    str_err = calltransaction(errorMessage, str_query)
                    If str_err = "0" Then


                        Dim dsExcel As DataSet
                        dsExcel = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

                        dtEXCEL = dsExcel.Tables(0)



                        'SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
                        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
                        Dim ef As ExcelFile = New ExcelFile
                        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
                        Dim dts As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
                        Dim stuFilename As String = "student_" & Left(dts, Len(dts) - 2) & ".xls"
                        ws.InsertDataTable(dtEXCEL, "A1", True)
                        Response.ContentType = "application/vnd.ms-excel"
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()


                        Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename
                        If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
                            ' Create the directory.
                            Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
                        End If


                        ef.SaveXls(cvVirtualPath & pathSave)
                        Dim path = cvVirtualPath & pathSave

                        Dim bytes() As Byte = File.ReadAllBytes(path)
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Response.Clear()
                        Response.ClearHeaders()
                        Response.ContentType = "application/octect-stream"
                        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                        Response.BinaryWrite(bytes)
                        Response.Flush()
                        Response.End()
                        'HttpContext.Current.Response.ContentType = "application/octect-stream"
                        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                        'HttpContext.Current.Response.Clear()
                        'HttpContext.Current.Response.WriteFile(path)
                        'HttpContext.Current.Response.End()



                        lblError.Text = "Record Saved Successfully"


                    Else
                        lblError.Text = errorMessage
                    End If
                Else
                    lblError.Text = "Please select the field to be merged"
                End If

            Else
                lblError.Text = "Enter a document name"
            End If

        Catch ex As Exception
            lblError.Text = "Unexpected error"
            UtilityObj.Errorlog("studCert_Comm_Merge_new", ex.Message)


        End Try
    End Sub


    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim Id As New DataColumn("Id", System.Type.GetType("System.String"))
            Dim COLNAME As New DataColumn("COLNAME", System.Type.GetType("System.String"))
            Dim DISPLAYIN As New DataColumn("DISPLAYIN", System.Type.GetType("System.String"))
            Dim SortOrder As New DataColumn("SortOrder", System.Type.GetType("System.String"))
            Dim STRCOND As New DataColumn("STRCOND", System.Type.GetType("System.String"))
            dtDt.Columns.Add(Id)
            dtDt.Columns.Add(COLNAME)
            dtDt.Columns.Add(DISPLAYIN)
            dtDt.Columns.Add(SortOrder)
            dtDt.Columns.Add(STRCOND)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

            Return dtDt
        End Try
    End Function

    Protected Sub gvPartC_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim COND_ID As Integer = CInt(gvPartC.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0
        While i < Session("PartD_CER").Rows.Count
            If Session("PartD_CER").rows(i)("Id") = COND_ID Then
                Session("PartD_CER").rows(i).delete()
            Else
                i = i + 1
            End If
        End While
        gridbind()
    End Sub
    Sub gridbind()
        Try
            gvPartC.DataSource = Session("PartD_CER")
            gvPartC.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim checkDup As Boolean = False
        Dim msg As String = String.Empty
        For i As Integer = 0 To Session("PartD_CER").Rows.count - 1
            If UCase(Session("PartD_CER").Rows(i)("COLNAME")) = UCase(ddlFields.SelectedItem.Text) Then
                checkDup = True
                msg = "COL"
            ElseIf UCase(Session("PartD_CER").Rows(i)("SortOrder")) = UCase(ddlSortPos.SelectedValue) Then
                checkDup = True
                msg = "Sort"
            End If
        Next
        If checkDup = False Then
            Dim rDt As DataRow
            rDt = Session("PartD_CER").NewRow
            rDt("Id") = ViewState("id")
            rDt("COLNAME") = ddlFields.SelectedItem.Text
            rDt("DISPLAYIN") = ddlSort.SelectedItem.Text
            rDt("SortOrder") = ddlSortPos.SelectedValue
            rDt("STRCOND") = ddlFields.SelectedValue & "  " & ddlSort.SelectedValue

            '  If Session("PartC_CER").Rows.count = 0 Then

            Session("PartD_CER").Rows.Add(rDt)
            ViewState("id") = ViewState("id") + 1
            gridbind()
            lblError2.Text = ""
        Else

            If msg = "Sort" Then
                lblError2.Text = "Duplicate Sort Order Not Allowed!!!"
            ElseIf msg = "COL" Then
                lblError2.Text = "Duplicate Field Name Not Allowed!!!"
            End If

        End If
    End Sub



    'Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim url As String = String.Empty
    '    Dim MainMnu_code As String = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
    '    Dim datamode As String = Encr_decrData.Encrypt("none")
    '    'If btnBack.Text <> "Back" Then
    '    url = String.Format("{0}?MainMnu_code={1}&datamode={2}", "~Students\studCert_Comm_new.aspx", MainMnu_code, datamode)
    '    Response.Redirect(url)
    '    'Else

    '    '    url = String.Format("{0}?MainMnu_code={1}&datamode={2}", "~Students\studCert_Comm_View_new.aspx", MainMnu_code, datamode)
    '    '    Response.Redirect(url)
    '    'End If

    'End Sub

    
  
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim url As String = String.Empty
        Dim MainMnu_code As String = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        Dim datamode As String = Encr_decrData.Encrypt("none")
        'If btnBack.Text <> "Back" Then
        url = String.Format("{0}?MainMnu_code={1}&datamode={2}", "..\students\studCert_Comm_new.aspx", MainMnu_code, datamode)
        Response.Redirect(url)
        'Else

        '    url = String.Format("{0}?MainMnu_code={1}&datamode={2}", "~Students\studCert_Comm_View_new.aspx", MainMnu_code, datamode)
        '    Response.Redirect(url)
        'End If
    End Sub
End Class
