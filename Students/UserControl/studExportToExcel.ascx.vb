Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class Students_UserControl_studExportToExcel
    Inherits System.Web.UI.UserControl
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200448") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
                    Dim pathSave As String = Session("sUsr_id")

                    Dim path As String = (cvVirtualPath + pathSave)

                    If System.IO.Directory.Exists(path) Then
                        Try
                            System.IO.Directory.Delete(path, True)
                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message)
                        End Try
                    End If
                    gridbind()

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If


           

        ' set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))



    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_CCQ_NAME As String = String.Empty
            Dim str_filter_CCQ_DT As String = String.Empty
            Dim str_filter_CCQ_TYPE As String = String.Empty
            Dim BSU_ID As String = Session("sBsuid")
            Dim ds As New DataSet

            str_Sql = " select * from(SELECT [CCQ_BSU_ID],[CCQ_bDeleted],[CCQ_ID] ,[CCQ_CREATE_DT] as CCQ_DT ,UPPER([CCQ_NAME]) AS CCQ_NAME , case  when rtrim(upper([CCQ_TYPE]))='STU' then 'STUDENT' " & _
" when rtrim(upper([CCQ_TYPE]))='ENQ' then 'ENQUIRY' when rtrim(upper([CCQ_TYPE]))='Student' then 'STUDENT'end as CCQ_TYPE " & _
 " FROM [COM_CER_QUERY]) A  where A.[CCQ_BSU_ID]='" & BSU_ID & "' and A.[CCQ_bDeleted]=0 "





            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_CCQ_NAME As String = String.Empty
            Dim str_CCQ_TYPE As String = String.Empty
            Dim str_CCQ_DT As String = String.Empty

            If gvAuthorizedRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String


                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtCCQ_DT")
                str_CCQ_DT = txtSearch.Text
                Dim CCQ_DT As String = "  replace(CONVERT( CHAR(12), isnull(a.CCQ_DT,''), 106 ),' ','/') "
                If str_search = "LI" Then
                    str_filter_CCQ_DT = " AND " & CCQ_DT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_CCQ_DT = "  AND  NOT " & CCQ_DT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_CCQ_DT = " AND " & CCQ_DT & "  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_CCQ_DT = " AND " & CCQ_DT & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_CCQ_DT = " AND " & CCQ_DT & " LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_CCQ_DT = " AND " & CCQ_DT & " NOT LIKE '%" & txtSearch.Text & "'"
                End If




                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtCCQ_NAME")
                str_CCQ_NAME = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_CCQ_NAME = " AND a.CCQ_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_CCQ_NAME = "  AND  NOT a.CCQ_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_CCQ_NAME = " AND a.CCQ_NAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_CCQ_NAME = " AND a.CCQ_NAME NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_CCQ_NAME = " AND a.CCQ_NAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_CCQ_NAME = " AND a.CCQ_NAME NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtCCQ_TYPE")
                str_CCQ_TYPE = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_CCQ_TYPE = " AND a.CCQ_TYPE LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_CCQ_TYPE = "  AND  NOT a.CCQ_TYPE  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_CCQ_TYPE = " AND a.CCQ_TYPE   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_CCQ_TYPE = " AND a.CCQ_TYPE  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_CCQ_TYPE = " AND a.CCQ_TYPE LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_CCQ_TYPE = " AND a.CCQ_TYPE  NOT LIKE '%" & txtSearch.Text & "'"
                End If










            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_CCQ_NAME & str_filter_CCQ_TYPE & str_filter_CCQ_DT & " ORDER BY a.CCQ_DT DESC")
            '& ViewState("str_filter_Year") & str_filter_C_DESCR & str_filter_STARTDT & str_filter_ENDDT & CurrentDatedType & str_filter_OPENONLINE & "  order by  a.Y_DESCR")

            If ds.Tables(0).Rows.Count > 0 Then

                gvAuthorizedRecord.DataSource = ds.Tables(0)
                gvAuthorizedRecord.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from for gridcolumn is visible
                ds.Tables(0).Rows(0)(5) = True

                gvAuthorizedRecord.DataSource = ds.Tables(0)
                Try
                    gvAuthorizedRecord.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAuthorizedRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAuthorizedRecord.Rows(0).Cells.Clear()
                gvAuthorizedRecord.Rows(0).Cells.Add(New TableCell)
                gvAuthorizedRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAuthorizedRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAuthorizedRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtCCQ_NAME")
            txtSearch.Text = str_CCQ_NAME

            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtCCQ_TYPE")
            txtSearch.Text = str_CCQ_TYPE
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtCCQ_DT")
            txtSearch.Text = str_CCQ_DT

            'Call callYEAR_DESCRBind()

            'Call ddlOpenOnLine_state(ddlOPENONLINEH.SelectedItem.Text)

            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub gvStudentRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAuthorizedRecord.PageIndexChanging
        gvAuthorizedRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studCert_Comm_new.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Sub btnSearchCCQ_TYPE_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchCCQ_NAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchCCQ_DT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub lbExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lblCCQ_ID As New Label


        lblCCQ_ID = TryCast(sender.FindControl("lblCCQ_ID"), Label)

        Dim str_conn = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "Select CCQ_QUERY FROM COM_CER_QUERY WHERE CCQ_ID='" & Trim(lblCCQ_ID.Text) & "'"
        Dim Exec_Query As String = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query))
        ' Dim ds As New DataSet
        Dim dtEXCEL As New DataTable
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Exec_Query)
        Dim dsExcel As DataSet
        dsExcel = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Exec_Query)

        dtEXCEL = dsExcel.Tables(0)

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile
        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
        Dim stuFilename As String = "student_" & Left(dt, Len(dt) - 2) & ".xlsx"
        ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        '  ws.HeadersFooters.AlignWithMargins = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentExportToExcel").ToString()
        Dim pathSave As String = Session("sUsr_id") & "\" & stuFilename

        If Not Directory.Exists(cvVirtualPath + Session("sUsr_id") & "\") Then
            ' Create the directory.
            Directory.CreateDirectory(cvVirtualPath + Session("sUsr_id") & "\")
        End If

        ef.Save(cvVirtualPath & pathSave)
        Dim path = cvVirtualPath & pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(path)
        'HttpContext.Current.Response.End()


    End Sub

    Protected Sub lblDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lblCCQ_ID As New Label
            lblCCQ_ID = TryCast(sender.FindControl("lblCCQ_ID"), Label)

            Dim str_conn = ConnectionManger.GetOASISConnectionString

            Dim str_query As String = "update COM_CER_QUERY set CCQ_bDeleted=1 where CCQ_ID='" & Trim(lblCCQ_ID.Text) & "'"
            Dim Exec_Query As String = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            If Exec_Query = "1" Then
                'Response.Write("<script language='javascript'>")
                'Response.Write("window.location.reload(true);")
                'Response.Write(" </script>")
                lblError.Text = "Record Deleted Successfully"
                gridbind()
            Else
                lblError.Text = "Request could not be processed "

            End If


        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Protected Sub gvAuthorizedRecord_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        '  AddConfirmDelete(DirectCast(sender, GridView), e)

        Try

            Dim lblDelete As New LinkButton
            lblDelete = e.Row.FindControl("lblDelete")


            If lblDelete IsNot Nothing Then
                lblDelete.Attributes.Add("onclick", "return confirm(""Are you sure you want to delete the record?"")")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

End Class
