<%@ Control Language="VB" AutoEventWireup="false" CodeFile="studExportToExcel_Merge.ascx.vb" Inherits="Students_UserControl_studExportToExcel_Merge" %>
<script type="text/javascript">

    function change_chk_state(chkThis) {
        var ids = chkThis.id
        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                document.forms[0].elements[i].checked = state;
            }
        }
        return false;
    }


    function uncheckall(chkThis) {
        var ids = chkThis.id
        var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent
        var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child
        var state = false
        if (chkThis.checked) {
            state = true
        }
        else {
            state = false
        }
        var uncheckflag = 0

        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                if (currentid.indexOf(value) == -1) {
                    if (document.forms[0].elements[i].checked == false) {
                        uncheckflag = 1
                    }
                }
            }
        }

        if (uncheckflag == 1) {
            // uncheck parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = false;
                }
            }
        }
        else {
            // Check parent
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = true;
                }
            }
        }


        return false;
    }
    function change_chk_stateg(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }



</script>

<%--<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr class="title">
            <td align="left" style="height: 25px">
            </td>
        </tr>
    </table>--%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="left" colspan="2">
            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
    </tr>
    <tr>
        <td align="left" colspan="2">
            <div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center">
                            <div>
                                <table cellpadding="0" cellspacing="0" align="left" width="100%">
                                    <tr>
                                        <td class="title-bg">Part D(Select fields to be merged)</td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="100%">
                                            <asp:DataList ID="DataListPartC" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                                ShowHeader="False" Width="100%">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="HiddenCCT_DISPLAY" runat="server" Value='<%# Eval("CCT_DISPLAY_ORDER") %>'></asp:HiddenField>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="title-bg">
                                                                <%#Eval("CCT_DESC")%></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="checkbox-list">
                                                                    <asp:Panel ID="PanelControl" runat="server">
                                                                    </asp:Panel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                            </asp:DataList></td>
                                    </tr>

                                </table>

                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="5" cellspacing="0" align="left" width="100%">
                                <tr>
                                    <td class="title-bg" colspan="4">Part E(Select fields to be sorted)</td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Field Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlFields" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:DropDownList ID="ddlSort" runat="server">
                                            <asp:ListItem Value="asc">Ascending</asp:ListItem>
                                            <asp:ListItem Value="desc">Descending</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Sort Order</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSortPos" runat="server">
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <span>
                                            <asp:Label ID="lblError2" runat="server" CssClass="error"></asp:Label></span>
                                       </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="gvPartC" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            DataKeyNames="id" EmptyDataText="No search condition added yet for Part C."
                                            OnRowDeleting="gvPartC_RowDeleting" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="COLNAME" HeaderText="Field Name"></asp:BoundField>
                                                <asp:BoundField DataField="DISPLAYIN" HeaderText="Display In"></asp:BoundField>
                                                <asp:BoundField DataField="SortOrder" HeaderText="Sort  Order"></asp:BoundField>
                                                <asp:BoundField DataField="STRCOND" HeaderText="STRCOND" Visible="False"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="DeleteBtn"
                                                            CommandArgument='<%# Eval("id") %>'
                                                            CommandName="Delete" runat="server">
         Delete</asp:LinkButton>


                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Aqua" />
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>
                        </td>
    </tr>

    </table>
            </div>
        </td>
    </tr>
    <%--  <tr>
        <td align="center">&nbsp;
        </td>
    </tr>--%>
    <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnMerge" runat="server" CssClass="button" Text="Export" />
            <asp:Button ID="btnBack" runat="server" CssClass="button" Text="Back" />
        </td>
        
    </tr>

</table>
