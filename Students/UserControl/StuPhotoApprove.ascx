<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StuPhotoApprove.ascx.vb"
    Inherits="Students_UserControls_StudIdcardprint" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<script type="text/javascript">
    function change_chk_stateg1(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;

            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckBar1") != -1) {
                //                           alert(document.forms[0].elements[i].disabled)                    
                if (document.forms[0].elements[i].disabled) {


                } else {
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                    //alert()
                }

            }
        }
    }
    function change_chk_stateNextacd(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chbxNextacd") != -1) {
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }
</script>
<div class="matters">

    <table width="100%">
        <tr>
            <td>
                <asp:Label ID="lblmessage" runat="server" ForeColor="Blue" CssClass="field-label"></asp:Label>
            </td>
        </tr>
        <tr align="left">
            <td>
                <asp:Label ID="lblmessageTotal" runat="server" CssClass="field-label"></asp:Label>
            </td>
        </tr>
    </table>



    <table width="100%">
        <tr>
            <td width="20%"><span class="field-label">Academic Year</span>
            </td>
            <td width="30%">
                <asp:DropDownList ID="ddaccyear" runat="server"
                    AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td width="20%"></td>
            <td width="30%"></td>
        </tr>
        <tr>
            <td><span class="field-label">Image Uploaded</span>
            </td>
            <td>
                <asp:DropDownList ID="DropDownImageExist" runat="server" AutoPostBack="True">
                    <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                    <asp:ListItem Text="No" Value="False"></asp:ListItem>
                </asp:DropDownList>
            </td>

            <td><span class="field-label">Approved</span>
            </td>
            <td>
                <asp:DropDownList ID="DropDownApproved" runat="server" AutoPostBack="True">
                    <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                    <asp:ListItem Text="No" Value="False"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GrdStudents" AutoGenerateColumns="False" Width="100%" EmptyDataText="No record found for selected search"
                    runat="server" ShowFooter="True" AllowPaging="True" PageSize="35" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Barcode">
                            <HeaderTemplate>
                                Approve
                                            <br />
                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg1(this);"
                                    ToolTip="Click here to select/deselect all rows" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:CheckBox ID="CheckBar1" runat="server"  />
                                </center>
                            </ItemTemplate>
                            <ItemStyle   />
                            <FooterTemplate>
                                <center>
                                   
                                    <asp:Button ID="btnbarcode" runat="server" CssClass="button" CommandName="approve"
                                        Text="Update" /></center>
                            </FooterTemplate>
                            <ItemStyle  HorizontalAlign="Center"  />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Number">
                            <HeaderTemplate>
                                Student Number
                                            <br />
                                <asp:TextBox ID="txtnumber"   runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearchStuno" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="ImageSearchStuno_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddensStuid" Value='<%#Eval("stu_id")%>' runat="server" />
                                <%#Eval("stu_no")%>
                            </ItemTemplate>
                            <ItemStyle  HorizontalAlign="Center"  />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <HeaderTemplate>
                                Student Name
                                            <br />
                                <asp:TextBox ID="txtname"   runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearchStuname" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="ImageSearchStuname_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STU_NAME")%>
                            </ItemTemplate>
                            <ItemStyle   />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade">
                            <HeaderTemplate>
                                Grade
                                            <br />
                                <asp:TextBox ID="txtGrade"   runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearchgrade" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="ImageSearchgrade_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("GRM_DISPLAY")%>
                                </center>
                            </ItemTemplate>
                            <ItemStyle  HorizontalAlign="Center"  />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section">
                            <HeaderTemplate>
                                Section
                                            <br />
                                <asp:TextBox ID="txtSection"   runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearchsection" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif"
                                    OnClick="ImageSearchsection_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("SCT_DESCR")%>
                                </center>
                            </ItemTemplate>
                           <ItemStyle  HorizontalAlign="Center"  />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Approved">
                            <HeaderTemplate>
                                Approved
                               
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                  <asp:Image ID="Imagecross" ImageUrl='<%#Eval("CrossEnd")%>' runat="server" />   
                                </center>
                            </ItemTemplate>
                            <ItemStyle  HorizontalAlign="Center"  />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Image">
                            <HeaderTemplate>
                                Image
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <asp:Image ID="Imagepath" Width="80px" Height="100px" ImageUrl='<%#Eval("imagepath")%>' runat="server" />  
                                </center>
                            </ItemTemplate>
                           <ItemStyle  HorizontalAlign="Center"  />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
                &nbsp;</td>
        </tr>
    </table>
    &nbsp;<asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="HiddenShowFlag" runat="server" />
</div>

