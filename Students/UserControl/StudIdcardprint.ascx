<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudIdcardprint.ascx.vb"
    Inherits="Students_UserControls_StudIdcardprint" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<script type="text/javascript">
    function change_chk_stateg1(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;

            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckBar1") != -1) {
                //                           alert(document.forms[0].elements[i].disabled)                    
                if (document.forms[0].elements[i].disabled) {


                } else {
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                    //alert()
                }

            }
        }
    }
    function change_chk_stateNextacd(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chbxNextacd") != -1) {
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }
</script>
<div class="matters">
    <table width="100%">
        <tr>
            <td>
                <asp:Label ID="lblmessage" runat="server" CssClass="field-label"></asp:Label><br />
            </td>
        </tr>
    </table>

    <table width="100%">
        <tr>
            <td width="25%">
                <span class="field-label">Academic Year</span></td>
            <td width="25%">
                <asp:DropDownList
                    ID="ddaccyear" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td width="25%"></td>
            <td width="25%"></td>
            <tr>
                <td>

                    <asp:CheckBox ID="check_next_accid" runat="server" CssClass="field-label"
                        Text="Print info of next academic year" />
                </td>
                <td>

                    <asp:CheckBox ID="CheckOwnTransport" runat="server" AutoPostBack="True" CssClass="field-label"
                        Text="Own Transport" />
                </td>


                <td>
                    <asp:CheckBox ID="Checkoption" runat="server" CssClass="field-label"
                        Text="Print Pickup/Dropoff Points" Checked="True" Enabled="False" /></td>
                <td>
                    <asp:CheckBox ID="Checkoption0" runat="server" CssClass="field-label"
                        Text="Print academic year" Checked="True" Enabled="False" />
                </td>
            </tr>
        <tr>
            <td colspan="4" style="text-align:center">
                <asp:GridView ID="GrdStudents" AutoGenerateColumns="False" Width="100%" EmptyDataText="No record found for selected search" CssClass="table table-bordered table-row"
                    runat="server" ShowFooter="True" AllowPaging="True" PageSize="15">
                    <Columns>
                        <asp:TemplateField HeaderText="Barcode">
                            <HeaderTemplate>
                                 Barcode
                                            <br />
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg1(this);" ToolTip="Click here to select/deselect all rows" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                  <asp:CheckBox ID="CheckBar1" runat="server" Enabled='<%# Bind("PHOTOAVALIABLE") %>' />
                                </center>
                            </ItemTemplate>
                            <ItemStyle   />
                            <FooterTemplate>
                                <center>
                               <%-- <asp:CheckBox ID="CheckIssueAll" Text="<br>Issue All" Width="50px" runat="server" />--%>
                                <br />
                                    <asp:Button ID="btnbarcode" runat="server" CssClass="button" CommandName="barcode" Text="Issue" /></center>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Student Number">
                            <HeaderTemplate>
                                Student Number
                                            <br />
                                            <asp:TextBox ID="txtnumber"   runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchStuno" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchStuno_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddensStuid" Value='<%#Eval("stu_id")%>' runat="server" />
                                <%#Eval("stu_no")%>
                            </ItemTemplate>
                            <ItemStyle   />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <HeaderTemplate>
                                Student Name
                                            <br />
                                            <asp:TextBox ID="txtname"  runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchStuname" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchStuname_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STU_NAME")%>
                            </ItemTemplate>
                            <ItemStyle   />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade">
                            <HeaderTemplate>
                                Grade
                                            <br />
                                            <asp:TextBox ID="txtGrade"   runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchgrade" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchgrade_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("GRM_DISPLAY")%>
                                </center>
                            </ItemTemplate>
                            <ItemStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section">
                            <HeaderTemplate>
                                Section
                                            <br />
                                            <asp:TextBox ID="txtSection"   runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchsection" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchsection_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("SCT_DESCR")%>
                                </center>
                            </ItemTemplate>
                            <ItemStyle   />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bus No">
                            <HeaderTemplate>
                                Bus No
                                            <br />
                                            <asp:TextBox ID="txtBusNo"  runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchBusNo" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchBusNo_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("STU_BUS_NO")%>
                                </center>
                            </ItemTemplate>
                            <ItemStyle   />
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"  Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                </asp:GridView>
             <span class="field-label"> * In the absence of photograph, ID Card print option is inactive</span>   </td>
        </tr>
    </table>
    &nbsp;<asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="HiddenShowFlag" runat="server" />
</div>
<script type="text/javascript">

    if (document.getElementById("<%=HiddenShowFlag.ClientID %>").value == '1') {
        document.getElementById("<%=HiddenShowFlag.ClientID %>").value = 0


    var sFeatures;
    sFeatures = "dialogWidth: 800px; ";
    sFeatures += "dialogHeight: 600px; ";

    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";


    var qstng = "Type=STUDENT&Checked=" + document.getElementById("<%=Checkoption.ClientID %>").checked;
            qstng += "&ayshow=" + document.getElementById("<%=Checkoption0.ClientID %>").checked
    var result;
    //result = window.showModalDialog('Barcode/BarcodeStudentNumber.aspx?Type=STUDENT' ,"", sFeatures);
    result = window.open('../CardPrinter/NewApprovedCard/Barcode.aspx?' + qstng)


}


</script>
