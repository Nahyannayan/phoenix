<%@ Control Language="VB" AutoEventWireup="false" CodeFile="studExportToExcel.ascx.vb" Inherits="Students_UserControl_studExportToExcel" %>
<style>
    input {
        vertical-align:middle !important;
    }
</style>
<%--<script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>--%>
<%--
    <script language="javascript" type="text/javascript">
           
                       
          
                
                function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                }
               
                
               
                function test2(val)
                {
                //alert(val);
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                }
                
                
                 function test3(val)
                {
                //alert(val);
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid3()%>").src = path;
                document.getElementById("<%=h_selected_menu_3.ClientID %>").value=val+'__'+path;
                }
                
               
           
    </script>--%>
<div class="card mb-3">
    <div class="card-header letter-space">
        <i class="fa fa-users mr-3"></i>

        Customized Export To Excel
    </div>
    <div class="card-body">
        <div class="table-responsive m-auto">

            <table id="Table1" border="0" width="100%">
                <tr style="font-size: 12pt">
                    <td align="left" class="title" ></td>
                </tr>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" colspan="4" style="height: 14px" valign="top">
                        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left" colspan="4" valign="top" style="height: 12px">
                        <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                </tr>
                <tr>
                    <td align="left" colspan="4" valign="top">
                        <table id="tbl_test" runat="server" align="center" width="100%"
                            cellpadding="5" cellspacing="0">
                            <tr >
                                <td align="left"   valign="top">
                                    <asp:GridView ID="gvAuthorizedRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                        PageSize="20" Width="100%" OnRowDataBound="gvAuthorizedRecord_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Date">
                                                <EditItemTemplate>
                                                    &nbsp; 
                                                </EditItemTemplate>
                                                <HeaderTemplate>
                                                     <asp:Label ID="lblDateH" runat="server" Text="Date" CssClass="gridheader_text"></asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtCCQ_DT" runat="server" ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchCCQ_DT" OnClick="btnSearchCCQ_DT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                            
                                                </HeaderTemplate>

                                                <ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>

                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                <ItemTemplate>
                                                    &nbsp;
                                        <asp:Label ID="lblCCQ_DT" runat="server" Text='<%# Bind("CCQ_DT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <EditItemTemplate>
                                                    &nbsp; 
                                                </EditItemTemplate>
                                                <HeaderTemplate>
                                                     <asp:Label ID="lblDESCRH" runat="server" Text="Description" CssClass="gridheader_text"></asp:Label>
                                                                                    <br />
                                                                                    <asp:TextBox ID="txtCCQ_NAME" runat="server" ></asp:TextBox>
                                                                                     <asp:ImageButton ID="btnSearchCCQ_NAME" OnClick="btnSearchCCQ_NAME_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                                             
                                                </HeaderTemplate>

                                                <ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>

                                                <HeaderStyle Wrap="False"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCCQ_NAME" runat="server" Text='<%# Bind("CCQ_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <EditItemTemplate>
                                                    &nbsp; 
                                                </EditItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblTypeH" runat="server" Text="Type" CssClass="gridheader_text"></asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtCCQ_TYPE" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchCCQ_TYPE" OnClick="btnSearchCCQ_TYPE_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                            
                                                </HeaderTemplate>

                                                <ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>

                                                <HeaderStyle Wrap="False"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCCQ_TYPEH" runat="server" Text='<%# Bind("CCQ_TYPE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                </EditItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblExportH" runat="server" Text="Export"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    &nbsp;<asp:LinkButton ID="lbExport" runat="server" OnClick="lbExport_Click">Export</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    &nbsp; 
                                                </EditItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblDeleteH" runat="server" Text="Delete"></asp:Label>
                                                </HeaderTemplate>

                                                <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                <ItemTemplate>
                                                    &nbsp;<asp:LinkButton ID="lblDelete" OnClick="lblDelete_Click" runat="server">Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="False" HeaderText="CCQ_ID">
                                                <EditItemTemplate>
                                                    &nbsp; 
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCCQ_ID" runat="server" Text='<%# Bind("CCQ_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="griditem" />
                                        <SelectedRowStyle BackColor="Wheat" />
                                        <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                    &nbsp;<br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
                </tr>
                </table>
        </div>
    </div>
</div>


