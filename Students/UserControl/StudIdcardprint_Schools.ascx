<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StudIdcardprint_Schools.ascx.vb"
    Inherits="Students_UserControls_StudIdcardprint_Schools" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<style type="text/css">
    .style1 {
        width: 100%;
    }
</style>
<script type="text/javascript">
    function change_chk_stateg1(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;

            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckBar1") != -1) {
                //                           alert(document.forms[0].elements[i].disabled)                    
                if (document.forms[0].elements[i].disabled) {


                } else {
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                    //alert()
                }

            }
        }
    }
    function change_chk_stateNextacd(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chbxNextacd") != -1) {
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();
            }
        }
    }
</script>
<div class="matters">
    <table width="100%">
        <tr>
            <td>
                <asp:Label ID="lblmessage" runat="server" CssClass="field-label"></asp:Label><br />
            </td>
        </tr>
        <tr align="left">
            <td>
                <asp:CheckBox ID="chkPrintOnPaper" runat="server" AutoPostBack="True" CssClass="field-label" Text="Print On Paper" />
            </td>
        </tr>
    </table>

    <table width="100%">


        <tr>
            <td width="20%">
                <span class="field-label" >School</span></td>
            <td width="30%">
                <asp:DropDownList ID="ddlSchool" runat="server" AutoPostBack="True"
                    DataTextField="BSU_NAME" DataValueField="BSU_ID">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsSchool" runat="server"
                    ConnectionString="<%$ ConnectionStrings:OASIS_TRANSPORTConnectionString %>"
                    SelectCommand="[TRANSPORT].[GETSTSIDCARDSCHOOLS]" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            </td>
            <td width="20%"></td>
            <td width="30%"></td>
        </tr>


        <tr id="trSuppress" runat="server" align="left">
            <td>  <span class="field-label" >Academic Year</span> </td><td><asp:DropDownList
                ID="ddaccyear" runat="server" AutoPostBack="True">
            </asp:DropDownList>
            </td>
            <td style="vertical-align:text-top">
                <span class="field-label" >  Filter</span></td>
            <td><asp:DropDownList ID="ddlPhotoStat" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="1">Photo Uploaded</asp:ListItem>
                                <asp:ListItem Value="2">Photo Not Uploaded  </asp:ListItem>
                                <asp:ListItem Value="0">All</asp:ListItem>

                            </asp:DropDownList>
                <br />
                <asp:CheckBox ID="chkPhotoSuprs" runat="server" AutoPostBack="True" Text="Suppress Photo While Printing" CssClass="field-label" />
                        </td>
                    </tr>
        <tr>


            <td  align="left">  <span class="field-label" >Card Type</span> </td>
            <td>
                            <asp:DropDownList ID="ddcardtype" runat="server" ></asp:DropDownList>



            </td>
        </tr>


        <tr id="trFilterDetails" runat="server" align="left">
            <td >Bus No </td>
            <td>
                <asp:DropDownList ID="ddlBusNo" runat="server">
                </asp:DropDownList> <asp:RadioButton ID="radOnwards" Checked="True" runat="server" GroupName="BUS_TYPE"
                    Text="Onwards" />
                <asp:RadioButton ID="radReturn" runat="server" GroupName="BUS_TYPE" Text="Return" CssClass="field-label" />
                <asp:RadioButton ID="radAll" runat="server" GroupName="BUS_TYPE" Text="All" CssClass="field-label" /> 
                <asp:Button ID="btnFilter" runat="server" Text="Filter" CssClass="button" /></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center">
                <asp:GridView ID="GrdStudents" AutoGenerateColumns="False" Width="100%" EmptyDataText="No record found for selected search"
                    runat="server" ShowFooter="True" AllowPaging="True" PageSize="15" CssClass="table table-bordered table-row">
                    <Columns>
                        <asp:TemplateField HeaderText="Barcode">
                            <HeaderTemplate>
                                 Barcode
                                            <br />
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg1(this);" ToolTip="Click here to select/deselect all rows" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                 <%-- <asp:CheckBox ID="CheckBar1" runat="server" Enabled='<%# Bind("PHOTOAVALIABLE") %>' />--%>
                                  <asp:CheckBox ID="CheckBar1" runat="server"  />
                                </center>
                            </ItemTemplate>
                              <ItemStyle HorizontalAlign="Center"   />
                            <FooterTemplate>
                                <%-- <asp:CheckBox ID="CheckIssueAll" Text="<br>Issue All" Width="50px" runat="server" />--%>
                                <asp:CheckBox ID="chkPrintAll" runat="server" Text="Print All" /><br />
                                <asp:Button ID="btnbarcode" runat="server" CssClass="button" CommandName="barcode" Text="Issue" />
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Student Number">
                            <HeaderTemplate>
                                Student Number
                                            <br />
                                            <asp:TextBox ID="txtnumber"   runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchStuno" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchStuno_Click" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddensStuid" Value='<%#Eval("stu_id")%>' runat="server" />
                                <%#Eval("stu_no")%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"   />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Student Name">
                            <HeaderTemplate>
                                Student Name
                                            <br />
                                            <asp:TextBox ID="txtname"   runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchStuname" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchStuname_Click" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("STU_NAME")%>
                            </ItemTemplate>
                             <ItemStyle HorizontalAlign="Center"   />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade">
                            <HeaderTemplate>
                                Grade
                                            <br />
                                            <asp:TextBox ID="txtGrade"  runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchgrade" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchgrade_Click" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("GRM_DISPLAY")%>
                                </center>
                            </ItemTemplate>
                              <ItemStyle HorizontalAlign="Center"   />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Section">
                            <HeaderTemplate>
                                Section
                                            <br />
                                            <asp:TextBox ID="txtSection"  runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchsection" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchsection_Click" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                    <%#Eval("SCT_DESCR")%>
                                </center>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"   />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Bus No">
                            <HeaderTemplate>
                                Bus No
                                        <br />
                                            <asp:TextBox ID="txtBusNo"   runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageSearchBusNo" runat="server" CommandName="search" ImageUrl="~/Images/forum_search.gif" OnClick="ImageSearchBusNo_Click" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <center>
                                <%#Eval("STU_BUS_NO")%>
                            </center>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"/>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="griditem"   Wrap="False" />
                    <EmptyDataRowStyle Wrap="False" />
                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                    <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                    <EditRowStyle Wrap="False" />
                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                    <FooterStyle HorizontalAlign="center" />
                </asp:GridView>
                <br />
               <span class="field-label"> *In the absence of photograph, ID Card print option is inactive </span></td>
        </tr>
    </table>
    &nbsp;<asp:HiddenField ID="Hiddenbsuid" runat="server" />
    <asp:HiddenField ID="HiddenShowFlag" runat="server" />
</div>
<script type="text/javascript">
    if (document.getElementById("<%=HiddenShowFlag.ClientID %>").value == '1') {

        document.getElementById("<%=HiddenShowFlag.ClientID %>").value = 0
        var sFeatures;
        sFeatures = "dialogWidth: 800px; ";
        sFeatures += "dialogHeight: 600px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var result;
        var cid = document.getElementById("<%=ddcardtype.ClientID %>").value;
        //result = window.showModalDialog('Barcode/BarcodeStudentNumber.aspx?Type=STUDENT' ,"", sFeatures);

        if (cid == '7') {

            result = window.open('../CardPrinter/NewApprovedCard/Barcode.aspx?Type=STUDENT&ctype=' + cid)
        }
        else if (cid == '8') {

            result = window.open('../CardPrinter/NewApprovedCard/Barcode.aspx?Type=STUDENT&ctype=' + cid)
        }
        else if (cid == '9') {

            result = window.open('../CardPrinter/NewApprovedCard/Barcode.aspx?Type=STUDENT&ctype=' + cid)
        }
        else if (cid == '10') {

            result = window.open('../CardPrinter/NewApprovedCard/Barcode.aspx?Type=STUDENT&ctype=' + cid)
        }
        else if (cid == '20') {

            result = window.open('../CardPrinter/NewApprovedCard/Barcode.aspx?Type=STUDENT&ctype=' + cid)
        }
        else {

            result = window.open('../CardPrinter/NewApprovedCard/Barcode.aspx?Type=TRANSPORT_STUDENT&ctype=' + cid)
        }

    }

</script>

