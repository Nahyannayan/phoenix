Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_UserControls_StudIdcardprint
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblmessage.Text = ""
            Dim studClass As New studClass
            Hiddenbsuid.Value = Session("sbsuid")
            ddaccyear = studClass.PopulateAcademicYear(ddaccyear, Session("clm").ToString, Session("sbsuid").ToString)
            Using DefReader As SqlDataReader = GetDefSchool(Session("sBsuid"))
                While DefReader.Read
                    Session("School_Type") = Convert.ToString(DefReader("BSU_bGEMSSchool"))


                End While
            End Using


            BindStudentView()
        End If
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))

    End Sub

    Function GetDefSchool(ByVal BSU_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSchoolType", pParms)
        Return reader
    End Function

    Public Function GetData() As DataSet

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        If Session("School_Type") = "1" Then
            str_conn = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()
        Else
            str_conn = System.Configuration.ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ToString()
        End If


        Dim noimage = ""
        Dim vpath = "https://school.gemsoasis.com/OASISPHOTOS/OASIS_HR/ApplicantPhoto"


        Dim strQuery = " SELECT STU_ID,STU_NO,STU_NAME,GRM_DISPLAY,SCT_DESCR,PHOTOAVALIABLE,STU_PHOTOPATH_TEMP, PhotoUploaded," & _
                        " Case PHOTOAVALIABLE when 'False' then '~/images/Cross.png' else '~/images/tick.gif' end as CrossEnd, " & _
                        " Case isnull(STU_PHOTOPATH_TEMP,'') when '' then '" & noimage & "' else '" & vpath & "' + STU_PHOTOPATH_TEMP end as imagepath, " & _
                        " STU_BUS_NO,STU_SBL_ID_PICKUP,STU_ACD_ID,STU_LEAVEDATE,STU_CURRSTATUS from ( " & _
                        " SELECT A.STU_ID,A.STU_NO,STU_NAME=(ISNULL(A.STU_FIRSTNAME,'')+' ' + ISNULL(A.STU_MIDNAME,'')+' '+ISNULL(A.STU_LASTNAME,'')), " & _
                        " ISNULL(B.GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(C.SCT_DESCR,'') AS SCT_DESCR,CASE ISNULL(A.STU_PHOTOPATH,'') WHEN '' THEN 'False' ELSE 'True' END AS PHOTOAVALIABLE,CASE ISNULL(A.STU_PHOTOPATH_TEMP,'') WHEN '' THEN 'False' ELSE 'True' END AS  PhotoUploaded,STU_PHOTOPATH_TEMP,CASE WHEN isNULL(OnwardBus,'') ='' Then '--'  ELSE    (CASE  WHEN OnwardBus=RETURNBUS THEN  OnwardBus ELSE + OnwardBus +'/' + RETURNBUS END) END AS STU_BUS_NO,ISNULL(TRANSP.STU_SBL_ID_PICKUP,'') as STU_SBL_ID_PICKUP,A.STU_ACD_ID,A.STU_LEAVEDATE,A.STU_CURRSTATUS " & _
                        " FROM STUDENT_M AS A INNER JOIN oasis.dbo.GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID " & _
                        " INNER JOIN oasis.dbo.SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                        " LEFT JOIN OASIS_Transport.TRANSPORT.VV_STUDTRANSPORTINFO_TEMP TRANSP ON A.STU_ID=TRANSP.STU_ID)TEMP " & _
                        " WHERE STU_ACD_ID = " + ddaccyear.SelectedValue.ToString + " AND STU_LEAVEDATE IS NULL and STU_CURRSTATUS <>'CN'"


        If DropDownApproved.SelectedIndex > 0 Then
            strQuery &= " AND PHOTOAVALIABLE = '" & DropDownApproved.SelectedValue & "'"
        End If

        If DropDownImageExist.SelectedIndex > 0 Then
            strQuery &= " AND PhotoUploaded = '" & DropDownImageExist.SelectedValue & "'"
        End If

        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String


        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

            If txtnumber.Trim() <> "" Then
                strQuery &= " and STU_NO like '%" & txtnumber.Replace(" ", "") & "%' "
            End If

            If txtname.Trim() <> "" Then
                strQuery &= " and STU_NAME like '%" & txtname & "%' "
            End If

            If txtGrade.Trim() <> "" Then
                strQuery &= " and GRM_DISPLAY like '%" & txtGrade.Replace(" ", "") & "%' "
            End If

            If txtSection.Trim() <> "" Then
                strQuery &= " and SCT_DESCR like '%" & txtSection.Replace(" ", "") & "%' "
            End If

        End If

        strQuery &= " order by GRM_DISPLAY,SCT_DESCR,STU_NAME,STU_NO "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)

        Dim total = 0
        Dim Approved = 0
        Dim pnotApproved = 0

        Dim i = 0

        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim PHOTOAVALIABLE = ds.Tables(0).Rows(i).Item("PHOTOAVALIABLE")

            total = total + 1
            If PHOTOAVALIABLE = "True" Then
                Approved = Approved + 1

            Else
                pnotApproved = pnotApproved + 1
            End If

        Next

        lblmessageTotal.Text = "Total : " & total & " , Approved : " & Approved & " , Not Approved : " & pnotApproved

        Return ds




    End Function

    Public Sub BindStudentView()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString

        Dim txtnumber As String
        Dim txtname As String
        Dim txtGrade As String
        Dim txtSection As String

        If GrdStudents.Rows.Count > 0 Then

            txtnumber = DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text.Trim()
            txtname = DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text.Trim()
            txtGrade = DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text.Trim()
            txtSection = DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text.Trim()

        End If

        Dim ds As DataSet

        ds = GetData()

        If ds.Tables(0).Rows.Count = 0 Then
            Dim dt As New DataTable
            dt.Columns.Add("stu_id")
            dt.Columns.Add("stu_no")
            dt.Columns.Add("STU_NAME")
            dt.Columns.Add("GRM_DISPLAY")
            dt.Columns.Add("SCT_DESCR")
            dt.Columns.Add("CrossEnd")
            dt.Columns.Add("imagepath")

            Dim dr As DataRow = dt.NewRow()
            dr("stu_id") = ""
            dr("stu_no") = ""
            dr("STU_NAME") = ""
            dr("GRM_DISPLAY") = ""
            dr("SCT_DESCR") = ""
            dr("CrossEnd") = ""
            dr("imagepath") = ""

            dt.Rows.Add(dr)
            GrdStudents.DataSource = dt
            GrdStudents.DataBind()

            DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = False
        Else
            GrdStudents.DataSource = ds
            GrdStudents.DataBind()
            DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button).Visible = True
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(DirectCast(GrdStudents.FooterRow.FindControl("btnbarcode"), Button))

        End If

        If GrdStudents.Rows.Count > 0 Then

            DirectCast(GrdStudents.HeaderRow.FindControl("txtnumber"), TextBox).Text = txtnumber
            DirectCast(GrdStudents.HeaderRow.FindControl("txtname"), TextBox).Text = txtname
            DirectCast(GrdStudents.HeaderRow.FindControl("txtGrade"), TextBox).Text = txtGrade
            DirectCast(GrdStudents.HeaderRow.FindControl("txtSection"), TextBox).Text = txtSection

        End If



    End Sub

    Protected Sub GridMemberships_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdStudents.RowCommand

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        If Session("School_Type") = "1" Then
            str_conn = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()
        Else
            str_conn = System.Configuration.ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ToString()
        End If


        Dim query = ""
        Dim f = 0
        If e.CommandName = "approve" Then

            For Each row As GridViewRow In GrdStudents.Rows
                Dim ch As CheckBox = DirectCast(row.FindControl("CheckBar1"), CheckBox)
                Dim Hid As HiddenField = DirectCast(row.FindControl("HiddensStuid"), HiddenField)
                Dim key = Hid.Value

                If ch.Checked Then
                    f = 1
                    query = " UPDATE STUDENT_M SET STU_PHOTOPATH=STU_PHOTOPATH_TEMP WHERE STU_ID='" & key & "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, query)
                End If

            Next

            If f = 0 Then
                lblmessage.Text = "Please select records for approval."
            Else
                lblmessage.Text = "Selected record(s) approved successfully."
            End If

            BindStudentView()
        End If


    End Sub


    Protected Sub GrdStudents_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdStudents.PageIndexChanging
        GrdStudents.PageIndex = e.NewPageIndex
        BindStudentView()
    End Sub

    Protected Sub ddaccyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddaccyear.SelectedIndexChanged
        BindStudentView()
    End Sub

    Protected Sub ImageSearchStuno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchStuname_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchgrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub ImageSearchsection_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindStudentView()
    End Sub

    Protected Sub DropDownApproved_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownApproved.SelectedIndexChanged
        BindStudentView()
    End Sub
    Protected Sub DropDownImageExist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownImageExist.SelectedIndexChanged
        BindStudentView()
    End Sub

End Class
