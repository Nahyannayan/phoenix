<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudLEAVE_APPROVAL_EDIT_VIEW.aspx.vb" Inherits="Students_StudStaffAuthorized_Grade_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <script language="javascript" type="text/javascript">

        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
                }

                function test2(val) {
                    var path;
                    if (val == 'LI') {
                        path = '../Images/operations/like.gif';
                    } else if (val == 'NLI') {
                        path = '../Images/operations/notlike.gif';
                    } else if (val == 'SW') {
                        path = '../Images/operations/startswith.gif';
                    } else if (val == 'NSW') {
                        path = '../Images/operations/notstartwith.gif';
                    } else if (val == 'EW') {
                        path = '../Images/operations/endswith.gif';
                    } else if (val == 'NEW') {
                        path = '../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
                }



                function test3(val) {
                    //alert(val);
                    var path;
                    if (val == 'LI') {
                        path = '../Images/operations/like.gif';
                    } else if (val == 'NLI') {
                        path = '../Images/operations/notlike.gif';
                    } else if (val == 'SW') {
                        path = '../Images/operations/startswith.gif';
                    } else if (val == 'NSW') {
                        path = '../Images/operations/notstartwith.gif';
                    } else if (val == 'EW') {
                        path = '../Images/operations/endswith.gif';
                    } else if (val == 'NEW') {
                        path = '../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid3()%>").src = path;
                document.getElementById("<%=h_selected_menu_3.ClientID %>").value = val + '__' + path;
                }




                function test5(val) {

                    var path;
                    if (val == 'LI') {
                        path = '../Images/operations/like.gif';
                    } else if (val == 'NLI') {
                        path = '../Images/operations/notlike.gif';
                    } else if (val == 'SW') {
                        path = '../Images/operations/startswith.gif';
                    } else if (val == 'NSW') {
                        path = '../Images/operations/notstartwith.gif';
                    } else if (val == 'EW') {
                        path = '../Images/operations/endswith.gif';
                    } else if (val == 'NEW') {
                        path = '../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid5()%>").src = path;
                document.getElementById("<%=h_selected_menu_5.ClientID %>").value = val + '__' + path;
                  }

                  function test6(val) {
                      //alert(val);
                      var path;
                      if (val == 'LI') {
                          path = '../Images/operations/like.gif';
                      } else if (val == 'NLI') {
                          path = '../Images/operations/notlike.gif';
                      } else if (val == 'SW') {
                          path = '../Images/operations/startswith.gif';
                      } else if (val == 'NSW') {
                          path = '../Images/operations/notstartwith.gif';
                      } else if (val == 'EW') {
                          path = '../Images/operations/endswith.gif';
                      } else if (val == 'NEW') {
                          path = '../Images/operations/notendswith.gif';
                      }
                      document.getElementById("<%=getid6()%>").src = path;
                document.getElementById("<%=h_selected_menu_6.ClientID %>").value = val + '__' + path;
                 }

                 function test7(val) {
                     //alert(val);
                     var path;
                     if (val == 'LI') {
                         path = '../Images/operations/like.gif';
                     } else if (val == 'NLI') {
                         path = '../Images/operations/notlike.gif';
                     } else if (val == 'SW') {
                         path = '../Images/operations/startswith.gif';
                     } else if (val == 'NSW') {
                         path = '../Images/operations/notstartwith.gif';
                     } else if (val == 'EW') {
                         path = '../Images/operations/endswith.gif';
                     } else if (val == 'NEW') {
                         path = '../Images/operations/notendswith.gif';
                     }
                     document.getElementById("<%=getid7()%>").src = path;
                document.getElementById("<%=h_selected_menu_7.ClientID %>").value = val + '__' + path;
                 }

                 function test8(val) {
                     //alert(val);
                     var path;
                     if (val == 'LI') {
                         path = '../Images/operations/like.gif';
                     } else if (val == 'NLI') {
                         path = '../Images/operations/notlike.gif';
                     } else if (val == 'SW') {
                         path = '../Images/operations/startswith.gif';
                     } else if (val == 'NSW') {
                         path = '../Images/operations/notstartwith.gif';
                     } else if (val == 'EW') {
                         path = '../Images/operations/endswith.gif';
                     } else if (val == 'NEW') {
                         path = '../Images/operations/notendswith.gif';
                     }
                     document.getElementById("<%=getid8()%>").src = path;
                document.getElementById("<%=h_selected_menu_8.ClientID %>").value = val + '__' + path;
                }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Leave Approval Edit"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--    <table id="Table1" border="0" width="100%">
        <tr >
            <td align="left" >
                <asp:Literal id="ltHeader" runat="server" Text="LEAVE APPROVAL EDIT"></asp:Literal></td>
        </tr>
    </table>--%>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="tbl_test" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%">  <span class="field-label"> Select Academic year</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                     <td align="left" width="20%"> </td>
                                     <td align="left" width="30%"> </td>
                        </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="gvAuthorizedRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Stream">
                                        <HeaderTemplate>
                                      
                                                            <asp:Label ID="lblSTUD_IDLabel" runat="server" Text="Student Id" ></asp:Label>
                                                                            <asp:TextBox ID="txtSTUD_ID" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchSTUD_ID" OnClick="btnSearchSTUD_ID_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTUD_ID" runat="server" Text='<%# Bind("STUD_ID") %>' __designer:wfdid="w10"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Authorized Staff">
                                        <EditItemTemplate>
                                           
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            
                                                            <asp:Label ID="lblStud_NameH" runat="server" Text="Student Name" ></asp:Label>
                                                                            <asp:TextBox ID="txtSNAME" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchSNAME" OnClick="btnSearchSNAME_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNAME" runat="server" Text='<%# Bind("SNAME") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle ></HeaderStyle>

                                        <ItemStyle ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <EditItemTemplate>
                                          
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            
                                                            <asp:Label ID="lblGRD_DESCRH" runat="server" Text="Grade" ></asp:Label>
                                                                            <asp:TextBox ID="txtGRM_DIS" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchGRM_DIS" OnClick="btnSearchGRM_DIS_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" ></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGRM_DIS" runat="server" Text='<%# Bind("GRM_DIS") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle ></HeaderStyle>

                                        <ItemStyle ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Date">
                                        <EditItemTemplate>
                                  
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                           
                                                            <asp:Label ID="lblFROMDTH" runat="server" Text="From Date" ></asp:Label>
                                                                            <asp:TextBox ID="txtFromDT" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchFromDT" OnClick="btnSearchFromDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                     
                                            <asp:Label ID="lblFromDTH" runat="server" Text='<%# Bind("FromDT", "{0:dd/MMM/yyyy}") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle ></HeaderStyle>

                                        <ItemStyle ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <EditItemTemplate>
                                           
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            
                                                            <asp:Label ID="lblToDateH" runat="server" Text="To Date" ></asp:Label>
                                                                            <asp:TextBox ID="txtToDT" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchToDT" OnClick="btnSearchToDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" ></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblToDTH" runat="server" Text='<%# Bind("ToDT", "{0:dd/MMM/yyyy}") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Shift">
                                        <HeaderTemplate>
                                            
                                                            <asp:Label ID="lblREMARKSH" runat="server" Text="Remarks" ></asp:Label>
                                                                            <asp:TextBox ID="txtREMARKS" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchREMARKS" OnClick="btnSearchREMARKS_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" ></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblREMARKS" runat="server" Text='<%# Bind("REMARKS") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <HeaderTemplate>
                                           
                                                            <asp:Label ID="lblStatusH" runat="server" Text="Status" ></asp:Label>
                                                                            <asp:TextBox ID="txtStatus" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchStatus" OnClick="btnSearchStatus_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle" ></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("STATUS") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblViewH" runat="server" Text="View" ></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server" >View</asp:LinkButton>
                                        </ItemTemplate>

                                        <ItemStyle ></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student ID" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSID" runat="server" Text='<%# Bind("SID") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle  />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                            &nbsp;<br />
                            <br />
                        </td>
                    </tr>
                        </table>
            </td>
                    </tr>
                    <tr>
                        <td >
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_8" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</asp:Content>

