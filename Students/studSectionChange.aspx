<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studSectionChange.aspx.vb" Inherits="Students_studSectionChange" Title="Untitled Page" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i> Change Section
        </div>
        <div class="card-body">
            <div class="table-responsive">



                <table id="tblStud1" runat="server" width="100%" align="center" border="0" cellpadding="0"
                    cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>

                            <table id="tblStud" runat="server" align="center" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>
                                     <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Shift</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSHF" runat="server" AutoPostBack="True" SkinID="smallcmb"
                                            Width="100px" OnSelectedIndexChanged="ddlSHF_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Stream</span></td>
                                   
                                    <td align="left">
                                        <asp:DropDownList ID="ddlStream"  runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStream_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Grade</span></td>
                                    
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade"  runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" ><span class="field-label">Select Section</span></td>
                                   
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection"  runat="server" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student ID</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx1"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNo" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtStuNo">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>
                                    <td align="left"><span class="field-label">Name</span></td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server" Width="188px"></asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSAME" runat="server" BehaviorID="AutoCompleteEx2"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNAME" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtNAME">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="List Students" CssClass="button" TabIndex="4" CausesValidation="False" /></td>
                                </tr>
                                <tr>
                                   
                                    <td align="left" width="20%">
                                        <asp:CheckBox runat="server" ID="chkSection" CssClass="field-label" Text="Change Section To" AutoPostBack="True" OnCheckedChanged="chkSection_CheckedChanged"></asp:CheckBox>
                                         </td>
                                         <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSectionTo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSectionTo_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr runat="server" id="TrSaveTop">
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSaveTop" runat="server" Text="Save" CssClass="button" ValidationGroup="groupM1" TabIndex="4" OnClick="btnSaveTop_Click" /></td>
                                </tr>


                                <tr runat="server" id="trStudGrid">
                                    <td align="center"  colspan="4">
                                        <asp:GridView ID="gvStudChange" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="20" Width="100%" AllowSorting="True">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />

                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Select
                                                        <br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state1(this);"
                                                                        ToolTip="Click here to select/deselect all rows" />
                                                            

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server" __designer:wfdid="w1"></asp:CheckBox>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" __designer:wfdid="w5" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldoj" runat="server" __designer:wfdid="w6" Text='<%# Bind("STU_DOJ") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STP_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStpId" runat="server" __designer:wfdid="w7" Text='<%# Bind("STP_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SL.No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlNo" runat="server" __designer:wfdid="w2" Text="<%# getSerialNo() %>"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stud. No">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStHeader" runat="server" Text="Stud. No" CssClass="gridheader_text" __designer:wfdid="w4"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtStuNoSearch" runat="server" Width="75%" __designer:wfdid="w5"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStuNo_Search" OnClick="btnStuNo_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="middle" __designer:wfdid="w6"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" __designer:wfdid="w3" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text="Student Name" CssClass="gridheader_text" __designer:wfdid="w8"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtStudName" runat="server" Width="75%" __designer:wfdid="w9"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStudName_Search" OnClick="btnStudName_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="middle" __designer:wfdid="w10"></asp:ImageButton>
                                                                                
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" __designer:wfdid="w7" Text='<%# Bind("SNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" __designer:wfdid="w17" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsctId" runat="server" __designer:wfdid="w18" Text='<%# Bind("STP_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" __designer:wfdid="w19" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GRD_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRDID" runat="server" Text='<%# BIND("STP_GRD_ID") %>' __designer:wfdid="w20"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr runat="server" id="TrSave">
                                    <td align="center" colspan="6">
                                        <asp:Button ID="btnChange" runat="server" Text="Save" CssClass="button" ValidationGroup="groupM1" TabIndex="4" Height="27px" Width="61px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>

                <input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server"
                    type="hidden" value="=" />

            </div>
        </div>
    </div>

</asp:Content>

