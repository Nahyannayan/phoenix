Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Students_StudATT_Freeze
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim str_sql As String = ""

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050118") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                callYEAR_DESCRBind()

                If ViewState("datamode") = "view" Then
                    ViewState("FAD_ID") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    DISABLECONTROL()
                    binddata()
                    txtDate.Enabled = False
                    ddlAcademicYear.Enabled = False

                ElseIf ViewState("datamode") = "add" Then
                    ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
                    ENABLECONTROL()
                    txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                End If

                lblError.Text = ""


            End If
        End If
    End Sub
    Sub binddata()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str As String = "select FAD_ACD_ID,REPLACE(CONVERT(VARCHAR(12),FAD_DT,106),' ','/') AS FAD_DT,FAD_GRD_IDS from ATT.FREEZE_ATT_DATE where  FAD_ID='" & ViewState("FAD_ID") & "'"
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str)
            If reader.HasRows Then
                While reader.Read
                    If Not ddlAcademicYear.Items.FindByValue(Convert.ToString(reader("FAD_ACD_ID"))) Is Nothing Then
                        ddlAcademicYear.ClearSelection()
                        ddlAcademicYear.Items.FindByValue(Convert.ToString(reader("FAD_ACD_ID"))).Selected = True
                        ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
                    End If
                    txtDate.Text = Convert.ToString(reader("FAD_DT"))

                    For Each tn As TreeNode In tvGrade.Nodes
                        If tn.ChildNodes.Count > 0 Then
                            For Each cTn As TreeNode In tn.ChildNodes
                                If Convert.ToString(reader("FAD_GRD_IDS")).Contains(cTn.Value.ToString) Then
                                    cTn.Checked = True
                                End If
                            Next
                        End If



                    Next

                End While
            End If
        End Using

    End Sub
    Public Sub callYEAR_DESCRBind()
        Try

            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read

                        ddlAcademicYear.Items.Add(New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID")))

                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        PopulateTree()
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtDate.Text.Trim <> "" Then
                Dim strfDate As String = txtDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "As on Date format is Invalid"
                Else
                    txtDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "As on Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "As on Date required"

            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN AS ON DATE SELECT", "School Strength Detail")
            Return "-1"
        End Try

    End Function

    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        tvGrade.Nodes.Clear()
        str_Sql = "SELECT 0 AS GRD_ID, 'All' AS GRM_DISPLAY,COUNT(DISTINCT GRM_GRD_ID)as childnodecount FROM GRADE_BSU_M WHERE GRM_ACD_ID = '" & ddlAcademicYear.SelectedItem.Value & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvGrade.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("GRM_DISPLAY").ToString()
            tn.Value = dr("GRD_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        tvGrade.DataBind()
        tvGrade.ExpandAll()
    End Sub

    Protected Sub tvGrade_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvGrade.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
        ByVal parentNode As TreeNode)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String


        str_Sql = "SELECT  distinct   GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,0  AS childnodecount,GRADE_M.GRD_DISPLAYORDER as GRD_DISPLAYORDER " & _
                    " FROM GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID left OUTER JOIN " & _
                  " ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND " & _
                 " GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & Session("CLM") & "' And GRADE_BSU_M.GRM_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "'  order by GRADE_M.GRD_DISPLAYORDER"



        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)



    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim FAD_ID As String = "0"

        Dim str_GRD_IDs As New StringBuilder
        Dim FREEZE_DT As String = txtDate.Text
        Dim status As Integer

        For Each node As TreeNode In tvGrade.CheckedNodes
            If node.Value <> "0" Then
                str_GRD_IDs.Append(node.Value)
                str_GRD_IDs.Append("|")
            End If
        Next
        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    bEdit = False
                    Dim param(12) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@FAD_ID", FAD_ID)
                    param(1) = New SqlClient.SqlParameter("@FAD_ACD_ID", ddlAcademicYear.SelectedItem.Value)
                    param(2) = New SqlClient.SqlParameter("@FAD_GRD_IDS", str_GRD_IDs.ToString)
                    param(3) = New SqlClient.SqlParameter("@FAD_DT", FREEZE_DT)
                    param(4) = New SqlClient.SqlParameter("@FAD_CREATED_DT", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
                    param(5) = New SqlClient.SqlParameter("@FAD_CREATED_BY", Session("sUsr_id"))
                    param(6) = New SqlClient.SqlParameter("@FAD_EDIT_BY", Session("sUsr_id"))
                    param(7) = New SqlClient.SqlParameter("@FAD_EDIT_DT", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
                    param(8) = New SqlClient.SqlParameter("@FAD_bEDIT", bEdit)
                    param(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    param(9).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ATT.SAVEFREEZE_ATT_DATE", param)
                    status = param(9).Value
                    If status = -11 Then
                        calltransaction = "1"
                        errorMessage = "Record already exist.Please select next month date."
                        Return "1"
                    ElseIf status <> 0 Then
                        calltransaction = "1"
                        errorMessage = "Error Occured While Saving"
                        Return "1"
                    End If

                    ViewState("FAD_ID") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                    DISABLECONTROL()
                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage, "calltransaction")
                        transaction.Rollback()
                    Else
                        clear()
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    bEdit = True
                    FAD_ID = ViewState("FAD_ID")

                    Dim param(12) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@FAD_ID", FAD_ID)
                    param(1) = New SqlClient.SqlParameter("@FAD_ACD_ID", ddlAcademicYear.SelectedItem.Value)
                    param(2) = New SqlClient.SqlParameter("@FAD_GRD_IDS", str_GRD_IDs.ToString)
                    param(3) = New SqlClient.SqlParameter("@FAD_DT", FREEZE_DT)
                    param(4) = New SqlClient.SqlParameter("@FAD_CREATED_DT", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
                    param(5) = New SqlClient.SqlParameter("@FAD_CREATED_BY", Session("sUsr_id"))
                    param(6) = New SqlClient.SqlParameter("@FAD_EDIT_BY", Session("sUsr_id"))
                    param(7) = New SqlClient.SqlParameter("@FAD_EDIT_DT", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
                    param(8) = New SqlClient.SqlParameter("@FAD_bEDIT", bEdit)
                    param(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    param(9).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ATT.SAVEFREEZE_ATT_DATE", param)
                    status = param(9).Value

                    If status = -11 Then
                        calltransaction = "1"
                        errorMessage = "Record already exist.Please select next month."
                        Return "1"
                    ElseIf status <> 0 Then
                        calltransaction = "1"
                        errorMessage = "Error Occured While Saving"
                        Return "1"
                    End If

                    ViewState("FAD_ID") = "0"
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"
                    DISABLECONTROL()


                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage, "calltransaction")
                        transaction.Rollback()
                    Else
                        clear()
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        End If
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_err As String = String.Empty
        Dim nodeStatus As Boolean
        Dim errorMessage As String = String.Empty
        If Page.IsValid Then
            If ValidateDate() = "0" Then
                For Each node As TreeNode In tvGrade.CheckedNodes
                    If node.Value.Length > 1 Then
                        nodeStatus = True
                        Exit For
                    End If
                Next
                If nodeStatus Then
                    str_err = calltransaction(errorMessage)
                    If str_err = "0" Then
                        lblError.Text = "Record Saved Successfully"
                    Else
                        lblError.Text = errorMessage
                    End If
                Else
                    lblError.Text = "Please select Grade"
                End If

            End If
        End If

    End Sub

    Function DUPLICATE() As Integer


        Dim STR_CONN As String = ConnectionManger.GetOASISConnectionString
        Dim STR As String = "SELECT COUNT(FAD_ID) FROM ATT.FREEZE_ATT_DATE WHERE FAD_ACD_ID='" & ddlAcademicYear.SelectedValue & "' AND CAST(REPLACE(CONVERT(VARCHAR(7), FAD_DT, 120),'-','') AS BIGINT)<=  CAST(REPLACE(CONVERT(VARCHAR(7),cast('" & txtDate.Text & "' as datetime), 120),'-','') AS BIGINT) "
        Dim returnVal As Integer = CInt(SqlHelper.ExecuteScalar(STR_CONN, CommandType.Text, STR))
        Return returnVal



    End Function
    Sub clear()
        txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

        ViewState("FAD_ID") = "0"

        If Not ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then
            ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
        End If

    End Sub
    Sub ENABLECONTROL()
        ddlAcademicYear.Enabled = True
        txtDate.Enabled = True
        imgFromDate.Visible = True
        tvGrade.Enabled = True
    End Sub
    Sub DISABLECONTROL()
        ddlAcademicYear.Enabled = False
        txtDate.Enabled = False
        imgFromDate.Visible = False
        tvGrade.Enabled = False
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        ENABLECONTROL()
        clear()
        imgFromDate.Visible = True
        txtDate.Enabled = True
        ddlAcademicYear.Enabled = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        ENABLECONTROL()
        imgFromDate.Visible = False
        txtDate.Enabled = False
        ddlAcademicYear.Enabled = False
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            ViewState("FAD_ID") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                DISABLECONTROL()
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
End Class
