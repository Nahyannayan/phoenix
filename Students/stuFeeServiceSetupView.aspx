<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stuFeeServiceSetupView.aspx.vb" Inherits="stuFeeServiceSetupView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Student Sevice Provider...."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td align="left" colspan="2">&nbsp;<asp:HyperLink ID="hlAddNew" runat="server">Add New</asp:HyperLink>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label><input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <a id='top'></a>
                <table id="tbl_test" runat="server" align="center" cellpadding="5" cellspacing="0" width="100%">

                    <tr>
                        <td align="center" valign="top" >
                            <asp:GridView ID="gvJournal" CssClass="table table-bordered table-row" runat="server" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30">
                                <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="SVB_ID">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSVB_ID" runat="server" Text='<%# Bind("SVB_ID") %>' __designer:wfdid="w15"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit" >
                                        <HeaderTemplate>
                                            Provider Business Unit
                                            <br />
                                            <asp:TextBox ID="txtBSUNAME" runat="server" Width="75%" __designer:wfdid="w6" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnAmtSearcha" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="middle" __designer:wfdid="w7"></asp:ImageButton>
                                                                        
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="left" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("BSU_NAME") %>' __designer:wfdid="w25"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AcademicYear">
                                        <HeaderTemplate>
                                            Academic Year
                                            <br />
                                            <asp:TextBox ID="txtCounterDescr" runat="server" Width="75%" __designer:wfdid="w9" SkinID="Gridtxt"></asp:TextBox>
                                            <asp:ImageButton ID="btnBankACSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="middle" __designer:wfdid="w10"></asp:ImageButton>
                                                                
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ACY_DESCR") %>' __designer:wfdid="w19"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Service">
                                        <HeaderTemplate>
                                            Service
                                            <br />                                
                                            <asp:TextBox ID="txtUser" runat="server" Width="75%" __designer:wfdid="w12" SkinID="Gridtxt"></asp:TextBox>   
                                            <asp:ImageButton ID="btnNarration" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="middle" __designer:wfdid="w13"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>' __designer:wfdid="w22"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            &nbsp;<asp:HyperLink ID="hlEdit" runat="server">View</asp:HyperLink>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>
