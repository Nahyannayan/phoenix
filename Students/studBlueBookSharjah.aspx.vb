Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studBlueBookSharjah
    Inherits System.Web.UI.Page
    Dim lstrMenuCode As String
    Dim lstrGrade As String
    Dim lstrSection As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100191" And ViewState("MainMnu_code") <> "S100190" And ViewState("MainMnu_code") <> "S100450") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                GetBlueBook(Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+")), Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")))
                '    Else
                '    REPRINT_GetBlueBookForSection(Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+")), Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")))
                'End If


            End If
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'End Try
        End If
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetBlueBook(ByVal grd_id As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder

        Dim str_query_Red As String = "SELECT ISNULL(BB_BLANKSHEET,'FALSE') FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim reader_Red As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query_Red)
        While reader_Red.Read
            'strHeader = reader_Red.GetString(0)
            'strFooter = reader_Red.GetString(1)
            'strTblHeader = reader_Red.GetString(2)
            ViewState("BlankSheet") = reader_Red.GetBoolean(0)
        End While
        reader_Red.Close()



        If sct_id = "ALL" Then
            str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
                        & "' AND SCT_ACD_ID=" + Session("Current_ACD_ID")
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer = 0
            With ds.Tables(0)
                For i = 0 To .Rows.Count - 1
                    sb.AppendLine(GetBlueBookForSection(grd_id, .Rows(i).Item(0).ToString))
                    If ViewState("BlankSheet") = True Then
                        sb.AppendLine(GetBlank(grd_id, .Rows(i).Item(0).ToString))
                    End If
                Next
            End With
        Else
            If Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")) <> "0" Then
                sb.AppendLine(GetBlueBookForSection(grd_id, sct_id))
                If ViewState("BlankSheet") = True Then

                    sb.AppendLine(GetBlank(grd_id, sct_id))
                End If
            Else
                sb.AppendLine(REPRINT_GetBlueBookForSection(grd_id, sct_id))
            End If

        End If
        ltBlueBook.Text = sb.ToString

    End Sub
    Function GetBlank_OLD(ByVal grd_id As String, ByVal sct_id As String) As String
        Dim sb As New StringBuilder
        Dim strHeaders As String() = GetHeadersAndFooter(sct_id)
        Dim srNo As Integer = 0
        Dim strHeader As String
        Dim strFooter As String = strHeaders(1)
        Dim strTblHeader As String = strHeaders(2)
        Dim recordCount As Integer = 0
        Dim bRedLine As Boolean = False
        Dim pages As Integer
        Dim pageSize As Integer = 16


        Dim ageYear As Integer = 0
        Dim ageMonth As Integer = 0
        Dim cutOffDate As Date
        Dim dob As Date
        Dim intHold As Integer
        Dim tdStyle As String


        Dim intJoin As Integer
        Dim doj As Date
        Dim cutOffDate_TFRTYPE As Date
        Dim lstrPrevYrResult As String


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query_Red As String = "SELECT BB_HDR,BB_FTR,BB_TBL_HDR,BB_REDLINE FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim reader_Red As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query_Red)
        While reader_Red.Read
            'strHeader = reader_Red.GetString(0)
            'strFooter = reader_Red.GetString(1)
            'strTblHeader = reader_Red.GetString(2)
            bRedLine = reader_Red.GetBoolean(3)
        End While
        reader_Red.Close()




        Dim str_query As String = "SELECT Top 1 ISNULL(STU_BLUEID,'&nbsp'),ISNULL((SELECT GRM_DISPLAY+' '+SCT_DESCR AS LastGrade " _
                           & " FROM  STUDENT_PROMO_S INNER JOIN  GRADE_BSU_M ON STUDENT_PROMO_S.STP_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN" _
                           & " SECTION_M ON STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID " _
                           & " WHERE STUDENT_PROMO_S.STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STUDENT_PROMO_S.STP_STU_ID =A.STU_ID),'NEW') AS LastGrade," _
                           & " Case When [dbo].[ufn_CountChar] (Replace(Replace(Ltrim(Rtrim(STU_PASPRTNAME)),'  ',' '),'  ',' '),' ')<=1 Then " _
                           & " STU_PASPRTNAME+'/'+Case Left(STU_PRIMARYCONTACT,1) " _
                        & " When 'F' Then isNULL(STS_FFIRSTNAME,'')+' '+isNULL(STS_FMIDNAME,'')+' '+isNULL(STS_FLASTNAME,'') " _
                        & " When 'M' Then isNULL(STS_MFIRSTNAME,'')+' '+isNULL(STS_MMIDNAME,'')+' '+isNULL(STS_MLASTNAME,'') " _
                        & " When 'G' Then isNULL(STS_GFIRSTNAME,'')+' '+isNULL(STS_GMIDNAME,'')+' '+isNULL(STS_GLASTNAME,'') " _
                           & " End Else STU_PASPRTNAME END as STU_PASPRTNAME " _
                           & " ,STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')," _
                           & " ISNULL(STU_GENDER,''),STU_GENDERDETAIL=CASE STU_GENDER WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END," _
                           & " ISNULL(C.CTY_SHORT,''),ISNULL(C.CTY_NATIONALITY,''),ISNULL(RLG_ID,0),ISNULL(RLG_DESCR,''),POB=CASE STU_COB WHEN 172 THEN ISNULL(STU_POB,'')+'/UAE' ELSE ISNULL(J.CTY_DESCR,'') END ,ISNULL(STU_DOB,'')," _
                           & " STU_EMGCONTACT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FMOBILE,'') " _
                   & " WHEN 'M' THEN ISNULL(STS_MMOBILE,'') " _
                   & " ELSE ISNULL(STS_GMOBILE,'') END, " _
                           & " STU_POBOX=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMPOBOX,'') WHEN 'M' THEN ISNULL(STS_MCOMPOBOX,'') ELSE ISNULL(STS_GCOMPOBOX,'') END," _
                           & " ISNULL(STU_TFRTYPE,''), ISNULL((SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
                           & " AND STP_STU_ID =A.STU_ID),'NEW') ,ISNULL(STU_PREVSCHI,''),ISNULL(STU_PREVSCHI_COUNTRY,''),ISNULL(CLM_DESCR,''), " _
                           & " ISNULL(STU_PREVSCHI_LASTATTDATE,'01/01/1900'),ISNULL(STU_MINDOJ,''),ISNULL(STU_GRD_ID_JOIN,''),ISNULL(I.TCM_STU_ID,0),ISNULL(TCM_TCSO,''),ISNULL(I.TCM_LASTATTDATE,'2100-01-01'),ISNULL((select CASE WHEN ((tct_code=1) OR (tct_code=3)) THEN 'WITHIN UAE TC' WHEN ((tct_code=2) OR (tct_code=4)) THEN 'OUTSIDE UAE TC' ELSE '' END  from dbo.TC_TFRTYPE_M where tct_code=ISNULL(I.TCM_TCTYPE,0)),''),isNULL(STU_COB,5) as ST_COB,isNULL(STU_POB,'') as STU_POB,isNULL(STS_FCOMCITY,'') as  STS_FCOMCITY,ISNULL(STU_GENDER,'') as STU_GENDER " _
                           & " , STU_ID ,ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
                           & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
                           & " (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'Regular') as STP_MINLIST," _
                           & " isnull((select isnull(STP_BBSLNO,'') from STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID),'') as STP_BBSLNO,TCM_LASTATTDATE,isNULL(STU_BB_REMARKS,'') as BBRemarks FROM STUDENT_M AS A INNER JOIN " _
                           & " STUDENT_D AS B ON A.STU_SIBLING_ID = B.STS_STU_ID" _
                           & " INNER JOIN COUNTRY_M AS C  ON C.CTY_ID = A.STU_NATIONALITY" _
                           & " INNER JOIN RELIGION_M AS F ON A.STU_RLG_ID = F.RLG_ID" _
                           & " LEFT OUTER JOIN CURRICULUM_M AS G ON A.STU_PREVSCHI_CLM=G.CLM_ID" _
                           & " LEFT OUTER JOIN GRADE_BSU_M AS H ON A.STU_GRM_ID_JOIN=H.GRM_ID" _
                           & " LEFT OUTER JOIN TCM_M AS I ON A.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL" _
                           & " LEFT OUTER JOIN TC_REASONS_M AS K ON I.TCM_REASON=K.TCR_CODE" _
                           & " LEFT OUTER JOIN COUNTRY_M AS J  ON J.CTY_ID = A.STU_COB" _
                           & " WHERE (STU_SCT_ID = " + sct_id + ") AND STU_MINLIST='Regular'" _
                           & "   AND STU_CURRSTATUS NOT IN ('CN','TF') and isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"

        str_query += " ORDER BY STU_PASPRTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)


        str_query = " SELECT COUNT(STU_ID) FROM STUDENT_M LEFT OUTER JOIN TCM_M AS I ON STUDENT_M.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL WHERE STU_SCT_ID=" + sct_id + " AND STU_MINLIST='Regular' AND STU_CURRSTATUS NOT IN ('CN','TF') and isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"
        Dim records As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: Blue Book ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<BODY>")

        Dim pageNo As Integer = 1
        Dim lstrPOB As String


        While reader.Read


            If recordCount = 0 Then
                strHeader = strHeaders(0)
                strHeader = strHeader.Replace("%pgno%", pageNo)
                sb.AppendLine(strHeader)
                pageNo += 1
            End If

            If recordCount = 0 Then
                sb.AppendLine(strTblHeader)
            End If

            srNo += 1
            Dim sty As String = ""
            Dim tdStyle_red As String = String.Empty
            If bRedLine = True Then
                If srNo = records Then
                    sty = ";border-bottom-color:red; border-bottom-style: solid;border-bottom-width :thick"
                    'style="width: 100px; border-bottom-color: red; border-bottom-style: solid;"
                    ' tdStyleArabic = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 9pt verdana,timesroman,garamond""

                End If
            End If
            If reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=green>")
            ElseIf reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=red>")
            Else
                sb.AppendLine("<TR style=""height:30px"">")
            End If







            'serial no
            sb.AppendLine("<TD ALIGN=CENTER style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine("</TD>")
            'last grade
            sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")


            If StrConv(Trim(reader("STP_MINLIST").ToString), VbStrConv.ProperCase) <> "Regular" Then
                sb.AppendLine("</TD>")
            Else

                If Trim(reader("STP_BBSLNO").ToString) <> "" Then
                    sb.AppendLine("</TD>")
                Else
                    sb.AppendLine("</TD>")
                End If

            End If



            ' bluebookID
            sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine("&nbsp;</TD>")

            'name in english
            Dim stu_id As String = String.Empty
            Dim stuLink As String = String.Empty
            stu_id = Encr_decrData.Encrypt(Convert.ToString(reader("STU_ID")))

            stuLink = "../Students/StudRecordEdit_new.aspx?MainMnu_code=SbWsUfLyXSM=&datamode=4xCdg/cr4Xw=&viewid=" + stu_id + "&SetMnu=" + lstrMenuCode + "&SetGrade=" + lstrGrade + "&SetSect=" + lstrSection




            'stuName = reader.GetString(2).Trim

            'stuName = stuName.Replace("  ", " ")
            'stuNames = stuName.Split(" ")
            'parentName = reader.GetString(28)

            'If (bShowParent1 = True And stuNames.Length = 1) Or (bShowParent2 = True And stuNames.Length = 2) Then
            '    stuName = stuName + " / " + parentName
            'End If

            'sb.AppendLine(stuName.ToUpper + "</a></TD></TR></TABLE></TD>")

            If Session("sBSUID") <> "131002" Then
                If Len(reader.GetString(2)) < 25 Then

                    sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond " + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond "" align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                    '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
                    sb.AppendLine("</a></TD></TR></TABLE></TD>")
                Else
                    sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond"" align=left><a STYLE=""text-decoration:none;color:black""href=" + stuLink + ">")
                    ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                    ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                    sb.AppendLine("</a></TD></TR></TABLE></TD>")
                End If
            Else
                If Session("sroleid") <> "33" And Session("sroleid") <> "38" Then
                    If Len(reader.GetString(2)) < 25 Then
                        sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond " + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond "" align=left>")
                        '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
                        sb.AppendLine("</a></TD></TR></TABLE></TD>")
                    Else
                        sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond"" align=left>")
                        ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                        ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                        sb.AppendLine("</a></TD></TR></TABLE></TD>")
                    End If
                Else
                    If Len(reader.GetString(2)) < 25 Then
                        sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond " + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond "" align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                        '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
                        sb.AppendLine("</a></TD></TR></TABLE></TD>")
                    Else
                        sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond"" align=left><a STYLE=""text-decoration:none;color:black""href=" + stuLink + ">")
                        ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                        ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                        sb.AppendLine("</a></TD></TR></TABLE></TD>")
                    End If
                End If
            End If


            'name in arabic
            sb.AppendLine("<TD style=""FONT: bold 12pt verdana,timesroman,garamond;text-align:Right" + sty + """>")
            sb.AppendLine("&nbsp;</TD>")


            If ((Session("sbsuid") = "131001") Or (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010")) Then
                'Gender
                sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond;text-align:Center" + sty + """>")
                sb.AppendLine("&nbsp;</TD>")
            End If

            'nationality
            sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine("&nbsp;</TD>")

            'religion
            sb.AppendLine("<TD style=""FONT: bold 11pt verdana,timesroman,garamond;text-align:Center" + sty + """>")
            If reader.GetString(8) = "ISL" Then
                sb.AppendLine("&nbsp;</TD>")
            Else
                sb.AppendLine("&nbsp;</TD>")
            End If

            'place of birth

            If (reader.GetString(26) = 172) Then
                If InStr(reader.GetString(27), "BU", CompareMethod.Text) Then
                    lstrPOB = "AUH,UAE"

                ElseIf InStr(reader.GetString(27), "MAN", CompareMethod.Text) Then
                    lstrPOB = "AJ,UAE"
                ElseIf InStr(reader.GetString(27), "BAI", CompareMethod.Text) Then
                    lstrPOB = "DXB,UAE"
                ElseIf InStr(reader.GetString(27), "UJ", CompareMethod.Text) Then
                    lstrPOB = "FUJ,UAE"
                ElseIf InStr(reader.GetString(27), "AS", CompareMethod.Text) Then
                    lstrPOB = "RAK,UAE"
                ElseIf InStr(reader.GetString(27), "HAR", CompareMethod.Text) Then
                    lstrPOB = "SHJ,UAE"
                ElseIf InStr(reader.GetString(27), "Q", CompareMethod.Text) Then
                    lstrPOB = "UMQ,UAE"
                ElseIf InStr(reader.GetString(27), "AIN", CompareMethod.Text) Then
                    lstrPOB = "AIN,UAE"
                ElseIf InStr(reader.GetString(27), "OR", CompareMethod.Text) Then
                    lstrPOB = "KORF,UAE"
                End If

            Else
                lstrPOB = reader.GetString(10)
            End If
            sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine("&nbsp;</TD>")

            'date of birth
            sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine("&nbsp;</TD>")


            dob = reader.GetDateTime(11)
            cutOffDate = Session("Cutoff_Age")

            'intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))
            'intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate))
            'ageYear = Int(intHold / 12)
            'ageMonth = intHold Mod 12

            intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))

            intHold = Int(DateDiff(DateInterval.Day, dob, cutOffDate))
            ageYear = Int(intHold / 365.2425)
            ageMonth = Math.Floor((intHold Mod 365.2425) / 30.436875)




            'age years
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 11pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine("&nbsp;</TD>")

            ''new pass readmit
            'sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond"">")
            'If grd_id <> "KG1" And reader.GetString(14) = "I" Then
            '    sb.AppendLine(reader.GetString(15) + "</TD>")
            'Else
            '    sb.AppendLine("NEW</TD>")
            'End If


            'new pass readmit

            ''If (reader.GetString(14) = "I") And reader.GetString(27) = "Y" Then -- Commented as told by Charan - 08 Sep
            'If (reader.GetString(14) = "I") Then
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=""PASS"" /></TD>")
            'ElseIf grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            'Else
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            '    'sb.AppendLine("NEW</TD>")
            'End If

            If ((Session("sbsuid") = "131001") Or (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010")) Then
                sb.AppendLine("<TD align=""middle"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
                cutOffDate_TFRTYPE = "2008-04-01"
                doj = reader.GetDateTime(20)
                intJoin = Int(DateDiff(DateInterval.Day, doj, cutOffDate_TFRTYPE))
                lstrPrevYrResult = reader.GetString(15)
                If grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf ((reader.GetString(14) = "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) = "R")) Then
                    lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) <> "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "New"
                End If

                If grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "I") Then

                    lstrPrevYrResult = "Pass"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "N") Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" Then
                    lstrPrevYrResult = "New"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper = "FAIL" Then
                    lstrPrevYrResult = "Fail"
                End If

                sb.AppendLine("&nbsp;</TD>")
            Else
                sb.AppendLine("<TD align=""middle"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
                lstrPrevYrResult = reader.GetString(15)
                If grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf ((reader.GetString(14) = "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) <> "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "New"
                End If

                If grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "I") Then

                    lstrPrevYrResult = "Pass"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" Then
                    lstrPrevYrResult = "New"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper = "FAIL" Then
                    lstrPrevYrResult = "Fail"
                End If

                sb.AppendLine("&nbsp;</TD>")
            End If


            'telephone
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 8pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine("&nbsp;</TD>")
            'sb.AppendLine(Trim(reader.GetString(12)).Replace("-", "").Replace("00971", "").Replace("0971", "").Replace("971", "") + "</TD>")



            'pobox
            'Dim lstrPB As String

            'If InStr(reader.GetString(28), "BU", CompareMethod.Text) Then
            '    lstrPB = "AUH"
            'ElseIf InStr(reader.GetString(28), "AIN", CompareMethod.Text) Then
            '    lstrPB = "AIN"
            'ElseIf InStr(reader.GetString(28), "MAN", CompareMethod.Text) Then
            '    lstrPB = "AJ"
            'ElseIf (InStr(reader.GetString(28), "BAI", CompareMethod.Text) Or InStr(reader.GetString(28), "DXB", CompareMethod.Text)) Then
            '    lstrPB = "DXB"
            'ElseIf InStr(reader.GetString(28), "UJ", CompareMethod.Text) Then
            '    lstrPB = "FUJ"
            'ElseIf InStr(, "AS", CompareMethod.Text) Then
            '    lstrPB = "RAK"
            'ElseIf InStr(reader.GetString(28), "HAR", CompareMethod.Text) Then
            '    lstrPB = "SHJ"
            'ElseIf InStr(reader.GetString(28), "Q", CompareMethod.Text) Then
            '    lstrPB = "UMQ"
            'ElseIf InStr(reader.GetString(28), "OR", CompareMethod.Text) Then
            '    lstrPB = "KORF"
            'Else
            '    lstrPB = "SHJ"
            'End If

            Dim lstrPB As String

            If InStr(Convert.ToString(reader.GetString(28)), "AUH", CompareMethod.Text) Then
                lstrPB = "AUH"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AIN", CompareMethod.Text) Then
                lstrPB = "AIN"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AJ", CompareMethod.Text) Then
                lstrPB = "AJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "DXB", CompareMethod.Text) Then
                lstrPB = "DXB"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "FUJ", CompareMethod.Text) Then
                lstrPB = "FUJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "RAK", CompareMethod.Text) Then
                lstrPB = "RAK"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "SHJ", CompareMethod.Text) Then
                lstrPB = "SHJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "UMQ", CompareMethod.Text) Then
                lstrPB = "UMQ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "KORF", CompareMethod.Text) Then
                lstrPB = "KORF"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "BU", CompareMethod.Text) Then
                lstrPB = "AUH"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AIN", CompareMethod.Text) Then
                lstrPB = "AIN"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "MAN", CompareMethod.Text) Then
                lstrPB = "AJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "BAI", CompareMethod.Text) Then
                lstrPB = "DXB"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "UJ", CompareMethod.Text) Then
                lstrPB = "FUJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AS", CompareMethod.Text) Then
                lstrPB = "RAK"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "HAR", CompareMethod.Text) Then
                lstrPB = "SHJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "Q", CompareMethod.Text) Then
                lstrPB = "UMQ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "OR", CompareMethod.Text) Then
                lstrPB = "KORF"

            Else
                lstrPB = "SHJ"

            End If











            sb.AppendLine("<TD align=""center"" style=""FONT: bold 8pt verdana,timesroman,garamond" + sty + """>")
            If reader.GetString(13) = "" Then
                sb.AppendLine("&nbsp</TD>")
            Else
                sb.AppendLine("&nbsp;</TD>")
            End If


            'date of join
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine("&nbsp;</TD>")

            'join grade
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine("&nbsp;</TD>")






            'new TI readmit

            If ((Session("sbsuid") = "131001") Or (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010")) Then
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
                If ((reader.GetString(14) = "I") And (intJoin <= 0)) Then
                    sb.AppendLine("&nbsp;</TD>")
                ElseIf reader.GetString(14) = "R" Then
                    sb.AppendLine("&nbsp;</TD>")
                ElseIf ((reader.GetString(14) <> "I") And (intJoin <= 0)) Then
                    sb.AppendLine("&nbsp;</TD>")
                    'ElseIf ((reader.GetString(14) = "I")) Then
                    '    sb.AppendLine("T.I</TD>")
                Else
                    sb.AppendLine("&nbsp;</TD>")

                End If
            Else
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
                If reader.GetString(14) = "I" Then
                    sb.AppendLine("&nbsp;</TD>")
                ElseIf reader.GetString(14) = "R" Then
                    sb.AppendLine("&nbsp;</TD>")
                Else
                    sb.AppendLine("&nbsp;</TD>")
                End If
            End If





            If reader.GetDateTime(24).Date < Now.Date Then

                'leaving date
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")
                sb.AppendLine("&nbsp;</TD>")

                'leaving reason
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")

                If reader.GetString(25).ToUpper <> "" Then
                    sb.AppendLine("&nbsp;</TD>")
                Else
                    sb.AppendLine("&nbsp;</TD>")
                End If


            Else
                'leaving date
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond;color:white" + sty + """><TABLE Width=""60""><TR><TD>")
                sb.AppendLine("&nbsp;</TD></TR></TABLE></TD>")
                'leaving reason
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")
                sb.AppendLine("&nbsp</TD>")
            End If


            'result annual
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>&nbsp</TD>")


            'reexam
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>&nbsp</TD>")

            'remarks
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>&nbsp")
            sb.AppendLine("&nbsp;</TD>")
            sb.AppendLine("</TR>")



            recordCount += 1



            If recordCount = pageSize Then

                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")

                sb.AppendLine(strFooter)

                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")


                recordCount = 0

            End If

            If recordCount = pageSize Then
                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                recordCount = 0
            End If


        End While
        reader.Close()


        If recordCount <> 0 Then
            Dim remainingLines As Integer = pageSize - recordCount
            Dim i As Integer
            For i = 0 To remainingLines - 1
                sb.AppendLine(EmptyLines)
            Next

            sb.AppendLine("</TABLE>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            sb.AppendLine(strFooter)
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")

        End If
        reader.Close()
        sb.AppendLine("</BODY></HTML>")
        Return sb.ToString
    End Function
    Function GetBlueBookForSection(ByVal grd_id As String, ByVal sct_id As String) As String
        Dim sb As New StringBuilder
        Dim strHeaders As String() = GetHeadersAndFooter(sct_id)
        Dim srNo As Integer = 0
        Dim strHeader As String
        Dim strFooter As String = strHeaders(1)
        Dim strTblHeader As String = strHeaders(2)
        Dim recordCount As Integer = 0
        Dim bRedLine As Boolean = False
        Dim pages As Integer
        Dim pageSize As Integer = 16


        Dim ageYear As Integer = 0
        Dim ageMonth As Integer = 0
        Dim cutOffDate As Date
        Dim dob As Date
        Dim intHold As Integer
        Dim tdStyle As String


        Dim intJoin As Integer
        Dim doj As Date
        Dim cutOffDate_TFRTYPE As Date
        Dim lstrPrevYrResult As String
        Dim lstrBB_EDIT_ROLES As String
        Dim lstrBB_CAN_EDIT As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query_Red As String = "SELECT BB_HDR,BB_FTR,BB_TBL_HDR,BB_REDLINE ,BB_EDIT_ROLES FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim reader_Red As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query_Red)
        While reader_Red.Read
            'strHeader = reader_Red.GetString(0)
            'strFooter = reader_Red.GetString(1)
            'strTblHeader = reader_Red.GetString(2)
            bRedLine = reader_Red.GetBoolean(3)
            lstrBB_EDIT_ROLES = reader_Red.GetString(4)
        End While
        reader_Red.Close()

        If lstrBB_EDIT_ROLES = "ALL" Then
            lstrBB_CAN_EDIT = "1"
        Else
            If lstrBB_EDIT_ROLES.Contains(Session("SRoleId")) Then
                lstrBB_CAN_EDIT = "1"
            Else
                lstrBB_CAN_EDIT = "0"
            End If
        End If

       


        'Dim str_query As String = "SELECT ISNULL(STU_BLUEID,'&nbsp'),isNULL(STU_BB_PREVGRADE,ISNULL((SELECT GRM_DISPLAY+' '+SCT_DESCR AS LastGrade " _
        '                   & " FROM  STUDENT_PROMO_S INNER JOIN  GRADE_BSU_M ON STUDENT_PROMO_S.STP_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN" _
        '                   & " SECTION_M ON STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID " _
        '                   & " WHERE STUDENT_PROMO_S.STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STUDENT_PROMO_S.STP_STU_ID =A.STU_ID),'NEW')) AS LastGrade," _
        '                   & " Case When [dbo].[ufn_CountChar] (Replace(Replace(Ltrim(Rtrim(STU_PASPRTNAME)),'  ',' '),'  ',' '),' ')<=1 Then " _
        '                   & " STU_PASPRTNAME+'/'+Case Left(STU_PRIMARYCONTACT,1) " _
        '                & " When 'F' Then isNULL(STU_BB_PARNAME,isNULL(STS_FFIRSTNAME,'')+' '+isNULL(STS_FMIDNAME,'')+' '+isNULL(STS_FLASTNAME,'')) " _
        '                & " When 'M' Then isNULL(STU_BB_PARNAME,isNULL(STS_MFIRSTNAME,'')+' '+isNULL(STS_MMIDNAME,'')+' '+isNULL(STS_MLASTNAME,'')) " _
        '                & " When 'G' Then isNULL(STU_BB_PARNAME,isNULL(STS_GFIRSTNAME,'')+' '+isNULL(STS_GMIDNAME,'')+' '+isNULL(STS_GLASTNAME,'')) " _
        '                   & " End Else STU_PASPRTNAME END as STU_PASPRTNAME " _
        '                   & " ,STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')," _
        '                   & " ISNULL(STU_GENDER,''),STU_GENDERDETAIL=CASE STU_GENDER WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END," _
        '                   & " ISNULL(C.CTY_SHORT,''),ISNULL(C.CTY_NATIONALITY,''),ISNULL(RLG_ID,0),ISNULL(RLG_DESCR,''),POB=CASE STU_COB WHEN 172 THEN ISNULL(STU_POB,'')+'/UAE' ELSE ISNULL(J.CTY_DESCR,'') END ,ISNULL(STU_DOB,'')," _
        '                   & " STU_EMGCONTACT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FMOBILE,'') " _
        '           & " WHEN 'M' THEN ISNULL(STS_MMOBILE,'') " _
        '           & " ELSE ISNULL(STS_GMOBILE,'') END, " _
        '                   & " STU_POBOX=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMPOBOX,'') WHEN 'M' THEN ISNULL(STS_MCOMPOBOX,'') ELSE ISNULL(STS_GCOMPOBOX,'') END," _
        '                   & " ISNULL(STU_TFRTYPE,''), " _
        '                   & " ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
        '           & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
        '           & " (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'NEW')" _
        '                   & " ,ISNULL(STU_PREVSCHI,''),ISNULL(STU_PREVSCHI_COUNTRY,''),ISNULL(CLM_DESCR,''), " _
        '                   & " ISNULL(STU_PREVSCHI_LASTATTDATE,'01/01/1900'),ISNULL(STU_MINDOJ,''),ISNULL(STU_GRD_ID_JOIN,''),ISNULL(I.TCM_STU_ID,0),ISNULL(TCM_TCSO,''),ISNULL(I.TCM_LASTATTDATE,'2100-01-01'),ISNULL((select CASE WHEN ((tct_code=1) OR (tct_code=3)) THEN 'WITHIN UAE TC' WHEN ((tct_code=2) OR (tct_code=4)) THEN 'OUTSIDE UAE TC' ELSE '' END  from dbo.TC_TFRTYPE_M where tct_code=ISNULL(I.TCM_TCTYPE,0)),''),isNULL(STU_COB,5) as ST_COB,isNULL(STU_POB,'') as STU_POB,isNULL(STS_FCOMCITY,'') as  STS_FCOMCITY,ISNULL(STU_GENDER,'') as STU_GENDER " _
        '                   & " , STU_ID ,ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
        '                   & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
        '                   & " (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'Regular') as STP_MINLIST," _
        '                   & " isnull((select isnull(STP_BBSLNO,'') from STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID),'') as STP_BBSLNO,TCM_LASTATTDATE,isNULL(STU_BB_REMARKS,'') as BBRemarks,isNULL(STU_BB_TFRTYPE,'') as STU_BB_TFRTYPE ,STU_PASPRTNAME as Act_Stu_PasprtName,isNULL(MNT_FLAG,0) as SHOW_FLAG FROM STUDENT_M AS A INNER JOIN " _
        '                   & " STUDENT_D AS B ON A.STU_SIBLING_ID = B.STS_STU_ID" _
        '                   & " INNER JOIN COUNTRY_M AS C  ON C.CTY_ID = A.STU_NATIONALITY" _
        '                   & " INNER JOIN RELIGION_M AS F ON A.STU_RLG_ID = F.RLG_ID" _
        '                   & " LEFT OUTER JOIN CURRICULUM_M AS G ON A.STU_PREVSCHI_CLM=G.CLM_ID" _
        '                   & " LEFT OUTER JOIN GRADE_BSU_M AS H ON A.STU_GRM_ID_JOIN=H.GRM_ID" _
        '                   & " LEFT OUTER JOIN TCM_M AS I ON A.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL" _
        '                   & " LEFT OUTER JOIN TC_REASONS_M AS K ON I.TCM_REASON=K.TCR_CODE" _
        '                   & " LEFT OUTER JOIN COUNTRY_M AS J  ON J.CTY_ID = A.STU_COB" _
        '                   & " LEFT JOIN BSU_MOE_NATS on A.STU_NATIONALITY =MNT_CTY_ID AND A.STU_BSU_ID=MNT_BSU_ID " _
        '                   & " WHERE (STU_SCT_ID = " + sct_id + ") AND STU_MINLIST='Regular'" _
        '                   & "   AND STU_CURRSTATUS NOT IN ('CN','TF') and isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"

        'str_query += " ORDER BY STU_PASPRTNAME,STU_MIDNAME,STU_LASTNAME"

        Dim pParms(11) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
        pParms(1) = New SqlClient.SqlParameter("@SCT_ID", sct_id)
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID_PREV", Session("Prev_ACD_ID"))
        pParms(3) = New SqlClient.SqlParameter("@ACD_ID_CURRENT", Session("Current_ACD_ID"))


        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BLUEBOOK_SHJ", pParms)


        Dim str_query As String = " SELECT COUNT(STU_ID) FROM STUDENT_M LEFT OUTER JOIN TCM_M AS I ON STUDENT_M.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL WHERE STU_SCT_ID=" + sct_id + " AND STU_MINLIST='Regular' AND STU_CURRSTATUS NOT IN ('CN','TF') and isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"
        Dim records As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: Blue Book ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<BODY>")

        Dim pageNo As Integer = 1
        Dim lstrPOB As String


        Dim boldStyle As String

        ' If Session("sbsuid") = "135010" Then
        ' boldStyle = ""
        'Else
        boldStyle = "bold"
        'End If

        While reader.Read


            If recordCount = 0 Then
                strHeader = strHeaders(0)
                strHeader = strHeader.Replace("%pgno%", pageNo)
                sb.AppendLine(strHeader)
                pageNo += 1
            End If

            If recordCount = 0 Then
                sb.AppendLine(strTblHeader)
            End If

            srNo += 1
            Dim sty As String = ""
            Dim tdStyle_red As String = String.Empty
            If bRedLine = True Then
                If srNo = records Then
                    sty = ";border-bottom-color:red; border-bottom-style: solid;border-bottom-width :thick"
                    'style="width: 100px; border-bottom-color: red; border-bottom-style: solid;"
                    ' tdStyleArabic = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 9pt verdana,timesroman,garamond""

                End If
            End If
            If reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=green>")
            ElseIf reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=red>")
            Else
                sb.AppendLine("<TR style=""height:30px"">")
            End If







            'serial no
            sb.AppendLine("<TD ALIGN=CENTER style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(srNo.ToString + "</TD>")
            'last grade
            sb.AppendLine("<TD style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """>")


            If StrConv(Trim(reader("STP_MINLIST").ToString), VbStrConv.ProperCase) <> "Regular" Then
                sb.AppendLine("NEW" + "</TD>")
            Else

                If Trim(reader("STP_BBSLNO").ToString) <> "" Then
                    sb.AppendLine(reader.GetString(1).ToUpper + "/" + Trim(reader("STP_BBSLNO").ToString) + "</TD>")
                Else
                    sb.AppendLine(reader.GetString(1).ToUpper + "</TD>")
                End If

            End If



            ' bluebookID
            sb.AppendLine("<TD style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(reader.GetString(0) + "&nbsp;</TD>")



            If Session("sbsuid") = "131001" Or (Session("sbsuid") = "131002") Then

                sb.AppendLine("<TD style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """>")
                sb.AppendLine(reader.GetString(38) + "&nbsp;</TD>")
            End If

            'name in english

            Dim stu_ShowName As String
            If reader("SHOW_FLAG").ToString = "0" Then
                stu_ShowName = reader.GetString(2).ToUpper
            Else
                stu_ShowName = reader("Act_Stu_PasprtName").ToString
            End If
            Dim stu_id As String = String.Empty
            Dim stuLink As String = String.Empty
            stu_id = Encr_decrData.Encrypt(Convert.ToString(reader("STU_ID")))

            stuLink = "../Students/StudRecordEdit_new.aspx?MainMnu_code=SbWsUfLyXSM=&datamode=4xCdg/cr4Xw=&viewid=" + stu_id + "&SetMnu=" + lstrMenuCode + "&SetGrade=" + lstrGrade + "&SetSect=" + lstrSection




            'stuName = reader.GetString(2).Trim

            'stuName = stuName.Replace("  ", " ")
            'stuNames = stuName.Split(" ")
            'parentName = reader.GetString(28)

            'If (bShowParent1 = True And stuNames.Length = 1) Or (bShowParent2 = True And stuNames.Length = 2) Then
            '    stuName = stuName + " / " + parentName
            'End If

            'sb.AppendLine(stuName.ToUpper + "</a></TD></TR></TABLE></TD>")


            'If Session("sBSUID") <> "131002" Then
            '    If Len(reader.GetString(2)) < 25 Then

            '        sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond " + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond "" align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
            '        '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
            '        sb.AppendLine(reader.GetString(2).ToUpper + "</a></TD></TR></TABLE></TD>")
            '    Else
            '        sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond"" align=left><a STYLE=""text-decoration:none;color:black""href=" + stuLink + ">")
            '        ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
            '        ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
            '        sb.AppendLine(reader.GetString(2).ToUpper + "</a></TD></TR></TABLE></TD>")
            '    End If
            'Else
            If Session("sbsuid") = "135010" Then
                If lstrBB_CAN_EDIT <> "1" Then
                    sb.AppendLine("<TD style=""FONT: bold 8pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 8pt verdana,timesroman,garamond"" align=left>")
                Else
                    sb.AppendLine("<TD style=""FONT: bold 8pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 8pt verdana verdana,timesroman,garamond"" align=left><a STYLE=""text-decoration:none;color:black""href=" + stuLink + ">")
                End If
                ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
            ElseIf lstrBB_CAN_EDIT <> "1" Then
                'If Session("sroleid") <> "33" And Session("sroleid") <> "38" Then
                If Len(stu_ShowName) < 25 Then
                    sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond " + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond "" align=left>")
                    '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
                    sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
                Else
                    sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond"" align=left>")
                    ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                    ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                    sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
                End If
            Else
                If Len(reader.GetString(2)) < 25 Then
                    sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond " + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond "" align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                    '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
                    sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
                Else
                    sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond"" align=left><a STYLE=""text-decoration:none;color:black""href=" + stuLink + ">")
                    ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                    ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                    sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
                End If
            End If
            'End If


            'name in arabic
            If Session("sbsuid") = "135010" Then
                sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond;text-align:Right" + sty + """><TABLE Width=""300""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond;text-align:Right" + "" + """>")

            Else
                sb.AppendLine("<TD style=""FONT: bold 12pt verdana,timesroman,garamond;text-align:Right" + sty + """><TABLE Width=""300""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond;text-align:Right" + "" + """>")

            End If
            sb.AppendLine(reader.GetString(3) + "&nbsp;&nbsp;</TD></TR></TABLE></TD>")

            Dim fontsize As String

            If Session("sbsuid") = "135010" Then
                fontsize = "bold 8pt"
            Else
                fontsize = "bold 10pt"
            End If

            'For 131002, this is not required since it is a boys school
            If ((Session("sbsuid") = "131001") Or (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010")) Then
                'Gender
                sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond;text-align:Center" + sty + """>")
                sb.AppendLine(Convert.ToString(reader("STU_GENDER")).ToUpper + "</TD>")
            End If

            'nationality
            sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(reader.GetString(6).ToUpper + "</TD>")

            'religion
            sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond;text-align:Center" + sty + """>")
            If reader.GetString(8) = "ISL" Then
                sb.AppendLine("M</TD>")
            Else
                sb.AppendLine("NM</TD>")
            End If

            'place of birth

            If (reader.GetString(26) = 172) Then
                If InStr(reader.GetString(27), "BU", CompareMethod.Text) Then
                    lstrPOB = "AUH,UAE"

                ElseIf InStr(reader.GetString(27), "MAN", CompareMethod.Text) Then
                    lstrPOB = "AJ,UAE"
                ElseIf InStr(reader.GetString(27), "BAI", CompareMethod.Text) Then
                    lstrPOB = "DXB,UAE"
                ElseIf InStr(reader.GetString(27), "UJ", CompareMethod.Text) Then
                    lstrPOB = "FUJ,UAE"
                ElseIf InStr(reader.GetString(27), "AS", CompareMethod.Text) Then
                    lstrPOB = "RAK,UAE"
                ElseIf InStr(reader.GetString(27), "HAR", CompareMethod.Text) Then
                    lstrPOB = "SHJ,UAE"
                ElseIf InStr(reader.GetString(27), "Q", CompareMethod.Text) Then
                    lstrPOB = "UMQ,UAE"
                ElseIf InStr(reader.GetString(27), "AIN", CompareMethod.Text) Then
                    lstrPOB = "AIN,UAE"
                ElseIf InStr(reader.GetString(27), "OR", CompareMethod.Text) Then
                    lstrPOB = "KORF,UAE"
                Else
                    lstrPOB = "UAE"
                End If

            Else
                lstrPOB = reader.GetString(10)
            End If
            sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """><TABLE Width=""98%""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            sb.AppendLine(lstrPOB.ToUpper + "</TD></tr></table></TD>")

            'date of birth
            sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """><TABLE Width=""98%""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            sb.AppendLine(Format(reader.GetDateTime(11), "dd/MM/yyyy") + "</TD></tr></table></TD>")


            dob = reader.GetDateTime(11)
            cutOffDate = Session("Cutoff_Age")

            'intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))
            'intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate))
            'ageYear = Int(intHold / 12)
            'ageMonth = intHold Mod 12

            intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))

            intHold = Int(DateDiff(DateInterval.Day, dob, cutOffDate))
            ageYear = Int(intHold / 365.2425)
            ageMonth = Math.Floor((intHold Mod 365.2425) / 30.436875)




            'age years
            sb.AppendLine("<TD align=""center"" style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(ageYear.ToString + "</TD>")

            ''new pass readmit
            'sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond"">")
            'If grd_id <> "KG1" And reader.GetString(14) = "I" Then
            '    sb.AppendLine(reader.GetString(15) + "</TD>")
            'Else
            '    sb.AppendLine("NEW</TD>")
            'End If


            'new pass readmit

            ''If (reader.GetString(14) = "I") And reader.GetString(27) = "Y" Then -- Commented as told by Charan - 08 Sep
            'If (reader.GetString(14) = "I") Then
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=""PASS"" /></TD>")
            'ElseIf grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            'Else
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            '    'sb.AppendLine("NEW</TD>")
            'End If

            If ((Session("sbsuid") <> "131002")) Then
                sb.AppendLine("<TD align=""middle"" style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """>")
                cutOffDate_TFRTYPE = "2008-04-01"
                doj = reader.GetDateTime(20)
                intJoin = Int(DateDiff(DateInterval.Day, doj, cutOffDate_TFRTYPE))
                lstrPrevYrResult = reader.GetString(15)
                If grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf ((reader.GetString(14) = "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = reader.GetString(15)
                    If lstrPrevYrResult <> "Fail" Then lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) = "R")) Then
                    lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) <> "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "New"
                End If

                If grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "I") Then

                    lstrPrevYrResult = "Pass"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "N") Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" Then
                    lstrPrevYrResult = "New"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper = "FAIL" Then
                    lstrPrevYrResult = "Fail"
                End If
                If (Session("sbsuid") = "135010") Then
                    sb.AppendLine(" <input style=""FONT: bold 8pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + lstrPrevYrResult + " /></TD>")
                Else
                    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + lstrPrevYrResult + " /></TD>")
                End If
            Else
                sb.AppendLine("<TD align=""middle"" style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """>")
                lstrPrevYrResult = reader.GetString(15)
                If grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf ((reader.GetString(14) = "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) <> "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = reader.GetString(15)
                End If

                If grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "I") Then

                    lstrPrevYrResult = "Pass"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" Then
                    lstrPrevYrResult = "New"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper = "FAIL" Then
                    lstrPrevYrResult = "Fail"
                End If

                sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + lstrPrevYrResult + " /></TD>")

            End If


            'telephone
            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 8pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(Right(Replace(reader.GetString(12), "-", ""), 9) + "</TD>")
            'sb.AppendLine(Trim(reader.GetString(12)).Replace("-", "").Replace("00971", "").Replace("0971", "").Replace("971", "") + "</TD>")



            'pobox
            'Dim lstrPB As String

            'If InStr(reader.GetString(28), "BU", CompareMethod.Text) Then
            '    lstrPB = "AUH"
            'ElseIf InStr(reader.GetString(28), "AIN", CompareMethod.Text) Then
            '    lstrPB = "AIN"
            'ElseIf InStr(reader.GetString(28), "MAN", CompareMethod.Text) Then
            '    lstrPB = "AJ"
            'ElseIf (InStr(reader.GetString(28), "BAI", CompareMethod.Text) Or InStr(reader.GetString(28), "DXB", CompareMethod.Text)) Then
            '    lstrPB = "DXB"
            'ElseIf InStr(reader.GetString(28), "UJ", CompareMethod.Text) Then
            '    lstrPB = "FUJ"
            'ElseIf InStr(, "AS", CompareMethod.Text) Then
            '    lstrPB = "RAK"
            'ElseIf InStr(reader.GetString(28), "HAR", CompareMethod.Text) Then
            '    lstrPB = "SHJ"
            'ElseIf InStr(reader.GetString(28), "Q", CompareMethod.Text) Then
            '    lstrPB = "UMQ"
            'ElseIf InStr(reader.GetString(28), "OR", CompareMethod.Text) Then
            '    lstrPB = "KORF"
            'Else
            '    lstrPB = "SHJ"
            'End If

            Dim lstrPB As String

            If InStr(Convert.ToString(reader.GetString(28)), "AUH", CompareMethod.Text) Then
                lstrPB = "AUH"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AIN", CompareMethod.Text) Then
                lstrPB = "AIN"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AJ", CompareMethod.Text) Then
                lstrPB = "AJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "DXB", CompareMethod.Text) Then
                lstrPB = "DXB"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "FUJ", CompareMethod.Text) Then
                lstrPB = "FUJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "RAK", CompareMethod.Text) Then
                lstrPB = "RAK"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "SHJ", CompareMethod.Text) Then
                lstrPB = "SHJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "UMQ", CompareMethod.Text) Then
                lstrPB = "UMQ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "KORF", CompareMethod.Text) Then
                lstrPB = "KORF"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "BU", CompareMethod.Text) Then
                lstrPB = "AUH"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AIN", CompareMethod.Text) Then
                lstrPB = "AIN"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "MAN", CompareMethod.Text) Then
                lstrPB = "AJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "BAI", CompareMethod.Text) Then
                lstrPB = "DXB"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "UJ", CompareMethod.Text) Then
                lstrPB = "FUJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AS", CompareMethod.Text) Then
                lstrPB = "RAK"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "HAR", CompareMethod.Text) Then
                lstrPB = "SHJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "Q", CompareMethod.Text) Then
                lstrPB = "UMQ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "OR", CompareMethod.Text) Then
                lstrPB = "KORF"

            Else
                lstrPB = "SHJ"

            End If











            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 8pt verdana,timesroman,garamond" + sty + """>")
            If reader.GetString(13) = "" Then
                sb.AppendLine("&nbsp</TD>")
            Else
                sb.AppendLine(reader.GetString(13).ToUpper + " " + lstrPB + "</TD>")
            End If


            'date of join
            sb.AppendLine("<TD align=""center"" style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(Format(reader.GetDateTime(20), "dd/MM/yyyy") + "</TD>")

            'join grade
            sb.AppendLine("<TD align=""center"" style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(reader.GetString(21) + "</TD>")






            'new TI readmit

            'If ((Session("sbsuid") <> "131002")) Then
            '    sb.AppendLine("<TD align=""center"" style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """>")
            '    If Trim(reader("STU_BB_TFRTYPE").ToString) <> "" Then
            '        sb.AppendLine(Trim(reader("STU_BB_TFRTYPE").ToString) + "</TD>")

            '    ElseIf ((reader.GetString(14) = "I") And (intJoin <= 0)) Then
            '        If (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010") Then
            '            sb.AppendLine("I.T</TD>")
            '        Else
            '            sb.AppendLine("T.I</TD>")
            '        End If
            '    ElseIf reader.GetString(14) = "R" Then
            '        sb.AppendLine("RE-ADMIT</TD>")
            '    ElseIf ((reader.GetString(14) <> "I") And (intJoin <= 0)) Then
            '        sb.AppendLine("NEW</TD>")
            '        'ElseIf ((reader.GetString(14) = "I")) Then
            '        '    sb.AppendLine("T.I</TD>")
            '    Else
            '        sb.AppendLine("&nbsp;</TD>")

            '    End If
            'Else
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """><TABLE Width=""98%""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            If reader.GetString(14) = "I" Then
                sb.AppendLine("I.T</TD></TR></TABLE></TD>")
            ElseIf reader.GetString(14) = "R" Then
                sb.AppendLine("RE-ADMIT</TD></TR></TABLE></TD>")
            Else
                sb.AppendLine("NEW</TD></TR></TABLE></TD>")
            End If
            'End If





            If reader.GetDateTime(24).Date < Now.Date Then

                'leaving date
                sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """><TABLE Width=""60""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
                sb.AppendLine(Format(reader.GetDateTime(33).Date, "dd/MMM/yyyy") + "</TD></TR></TABLE></TD>")

                'leaving reason
                sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """><TABLE Width=""60""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")

                If reader.GetString(25).ToUpper <> "" Then
                    sb.AppendLine(reader.GetString(25).ToUpper + "</TD></TR></TABLE></TD>")
                Else
                    sb.AppendLine("SO </TD></TR></TABLE></TD>")
                End If


            Else
                'leaving date
                sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;color:white" + sty + """><TABLE Width=""60""><TR><TD>")
                sb.AppendLine("&nbsp;</TD></TR></TABLE></TD>")
                'leaving reason
                sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """>")
                sb.AppendLine("&nbsp</TD>")
            End If


            'result annual
            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """>&nbsp</TD>")


            'reexam
            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """>&nbsp</TD>")

            'remarks
            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """>&nbsp")
            sb.AppendLine(reader.GetString(34).ToUpper + "</TD>")
            sb.AppendLine("</TR>")



            recordCount += 1



            If recordCount = pageSize Then

                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")

                sb.AppendLine(strFooter)

                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")


                recordCount = 0

            End If

            If recordCount = pageSize Then
                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                recordCount = 0
            End If


        End While
        reader.Close()


        If recordCount <> 0 Then
            Dim remainingLines As Integer = pageSize - recordCount
            Dim i As Integer
            For i = 0 To remainingLines - 1
                sb.AppendLine(EmptyLines)
            Next

            sb.AppendLine("</TABLE>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            sb.AppendLine(strFooter)
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")

        End If
        reader.Close()
        sb.AppendLine("</BODY></HTML>")
        Return sb.ToString
    End Function
    Function GetBlank(ByVal grd_id As String, ByVal sct_id As String) As String
        Dim sb As New StringBuilder
        Dim strHeaders As String() = GetHeadersAndFooter(sct_id)
        Dim srNo As Integer = 0
        Dim strHeader As String
        Dim strFooter As String = strHeaders(1)
        Dim strTblHeader As String = strHeaders(2)
        Dim recordCount As Integer = 0
        Dim bRedLine As Boolean = False
        Dim pages As Integer
        Dim pageSize As Integer = 16


        Dim ageYear As Integer = 0
        Dim ageMonth As Integer = 0
        Dim cutOffDate As Date
        Dim dob As Date
        Dim intHold As Integer
        Dim tdStyle As String


        Dim intJoin As Integer
        Dim doj As Date
        Dim cutOffDate_TFRTYPE As Date
        Dim lstrPrevYrResult As String
        Dim lstrBB_EDIT_ROLES As String
        Dim lstrBB_CAN_EDIT As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query_Red As String = "SELECT BB_HDR,BB_FTR,BB_TBL_HDR,BB_REDLINE ,BB_EDIT_ROLES FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim reader_Red As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query_Red)
        While reader_Red.Read
            'strHeader = reader_Red.GetString(0)
            'strFooter = reader_Red.GetString(1)
            'strTblHeader = reader_Red.GetString(2)
            bRedLine = reader_Red.GetBoolean(3)
            lstrBB_EDIT_ROLES = reader_Red.GetString(4)
        End While
        reader_Red.Close()

        If lstrBB_EDIT_ROLES = "ALL" Then
            lstrBB_CAN_EDIT = "1"
        Else
            If lstrBB_EDIT_ROLES.Contains(Session("SRoleId")) Then
                lstrBB_CAN_EDIT = "1"
            Else
                lstrBB_CAN_EDIT = "0"
            End If
        End If




        'Dim str_query As String = "SELECT ISNULL(STU_BLUEID,'&nbsp'),isNULL(STU_BB_PREVGRADE,ISNULL((SELECT GRM_DISPLAY+' '+SCT_DESCR AS LastGrade " _
        '                   & " FROM  STUDENT_PROMO_S INNER JOIN  GRADE_BSU_M ON STUDENT_PROMO_S.STP_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN" _
        '                   & " SECTION_M ON STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID " _
        '                   & " WHERE STUDENT_PROMO_S.STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STUDENT_PROMO_S.STP_STU_ID =A.STU_ID),'NEW')) AS LastGrade," _
        '                   & " Case When [dbo].[ufn_CountChar] (Replace(Replace(Ltrim(Rtrim(STU_PASPRTNAME)),'  ',' '),'  ',' '),' ')<=1 Then " _
        '                   & " STU_PASPRTNAME+'/'+Case Left(STU_PRIMARYCONTACT,1) " _
        '                & " When 'F' Then isNULL(STU_BB_PARNAME,isNULL(STS_FFIRSTNAME,'')+' '+isNULL(STS_FMIDNAME,'')+' '+isNULL(STS_FLASTNAME,'')) " _
        '                & " When 'M' Then isNULL(STU_BB_PARNAME,isNULL(STS_MFIRSTNAME,'')+' '+isNULL(STS_MMIDNAME,'')+' '+isNULL(STS_MLASTNAME,'')) " _
        '                & " When 'G' Then isNULL(STU_BB_PARNAME,isNULL(STS_GFIRSTNAME,'')+' '+isNULL(STS_GMIDNAME,'')+' '+isNULL(STS_GLASTNAME,'')) " _
        '                   & " End Else STU_PASPRTNAME END as STU_PASPRTNAME " _
        '                   & " ,STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')," _
        '                   & " ISNULL(STU_GENDER,''),STU_GENDERDETAIL=CASE STU_GENDER WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END," _
        '                   & " ISNULL(C.CTY_SHORT,''),ISNULL(C.CTY_NATIONALITY,''),ISNULL(RLG_ID,0),ISNULL(RLG_DESCR,''),POB=CASE STU_COB WHEN 172 THEN ISNULL(STU_POB,'')+'/UAE' ELSE ISNULL(J.CTY_DESCR,'') END ,ISNULL(STU_DOB,'')," _
        '                   & " STU_EMGCONTACT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FMOBILE,'') " _
        '           & " WHEN 'M' THEN ISNULL(STS_MMOBILE,'') " _
        '           & " ELSE ISNULL(STS_GMOBILE,'') END, " _
        '                   & " STU_POBOX=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMPOBOX,'') WHEN 'M' THEN ISNULL(STS_MCOMPOBOX,'') ELSE ISNULL(STS_GCOMPOBOX,'') END," _
        '                   & " ISNULL(STU_TFRTYPE,''), " _
        '                   & " ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
        '           & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
        '           & " (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'NEW')" _
        '                   & " ,ISNULL(STU_PREVSCHI,''),ISNULL(STU_PREVSCHI_COUNTRY,''),ISNULL(CLM_DESCR,''), " _
        '                   & " ISNULL(STU_PREVSCHI_LASTATTDATE,'01/01/1900'),ISNULL(STU_MINDOJ,''),ISNULL(STU_GRD_ID_JOIN,''),ISNULL(I.TCM_STU_ID,0),ISNULL(TCM_TCSO,''),ISNULL(I.TCM_LASTATTDATE,'2100-01-01'),ISNULL((select CASE WHEN ((tct_code=1) OR (tct_code=3)) THEN 'WITHIN UAE TC' WHEN ((tct_code=2) OR (tct_code=4)) THEN 'OUTSIDE UAE TC' ELSE '' END  from dbo.TC_TFRTYPE_M where tct_code=ISNULL(I.TCM_TCTYPE,0)),''),isNULL(STU_COB,5) as ST_COB,isNULL(STU_POB,'') as STU_POB,isNULL(STS_FCOMCITY,'') as  STS_FCOMCITY,ISNULL(STU_GENDER,'') as STU_GENDER " _
        '                   & " , STU_ID ,ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
        '                   & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
        '                   & " (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'Regular') as STP_MINLIST," _
        '                   & " isnull((select isnull(STP_BBSLNO,'') from STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID),'') as STP_BBSLNO,TCM_LASTATTDATE,isNULL(STU_BB_REMARKS,'') as BBRemarks,isNULL(STU_BB_TFRTYPE,'') as STU_BB_TFRTYPE ,STU_PASPRTNAME as Act_Stu_PasprtName,isNULL(MNT_FLAG,0) as SHOW_FLAG FROM STUDENT_M AS A INNER JOIN " _
        '                   & " STUDENT_D AS B ON A.STU_SIBLING_ID = B.STS_STU_ID" _
        '                   & " INNER JOIN COUNTRY_M AS C  ON C.CTY_ID = A.STU_NATIONALITY" _
        '                   & " INNER JOIN RELIGION_M AS F ON A.STU_RLG_ID = F.RLG_ID" _
        '                   & " LEFT OUTER JOIN CURRICULUM_M AS G ON A.STU_PREVSCHI_CLM=G.CLM_ID" _
        '                   & " LEFT OUTER JOIN GRADE_BSU_M AS H ON A.STU_GRM_ID_JOIN=H.GRM_ID" _
        '                   & " LEFT OUTER JOIN TCM_M AS I ON A.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL" _
        '                   & " LEFT OUTER JOIN TC_REASONS_M AS K ON I.TCM_REASON=K.TCR_CODE" _
        '                   & " LEFT OUTER JOIN COUNTRY_M AS J  ON J.CTY_ID = A.STU_COB" _
        '                   & " LEFT JOIN BSU_MOE_NATS on A.STU_NATIONALITY =MNT_CTY_ID AND A.STU_BSU_ID=MNT_BSU_ID " _
        '                   & " WHERE (STU_SCT_ID = " + sct_id + ") AND STU_MINLIST='Regular'" _
        '                   & "   AND STU_CURRSTATUS NOT IN ('CN','TF') and isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"

        'str_query += " ORDER BY STU_PASPRTNAME,STU_MIDNAME,STU_LASTNAME"

        Dim pParms(11) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
        pParms(1) = New SqlClient.SqlParameter("@SCT_ID", sct_id)
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID_PREV", Session("Prev_ACD_ID"))
        pParms(3) = New SqlClient.SqlParameter("@ACD_ID_CURRENT", Session("Current_ACD_ID"))


        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BLUEBOOK_SHJ_Blank", pParms)


        Dim str_query As String = " SELECT COUNT(STU_ID) FROM STUDENT_M LEFT OUTER JOIN TCM_M AS I ON STUDENT_M.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL WHERE STU_SCT_ID=" + sct_id + " AND STU_MINLIST='Regular' AND STU_CURRSTATUS NOT IN ('CN','TF') and isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"
        Dim records As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: Blue Book ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<BODY >")

        Dim pageNo As Integer = 1
        Dim lstrPOB As String


        Dim boldStyle As String

        ' If Session("sbsuid") = "135010" Then
        ' boldStyle = ""
        'Else
        boldStyle = "bold"
        'End If

        While reader.Read


            If recordCount = 0 Then
                strHeader = strHeaders(0)
                strHeader = strHeader.Replace("%pgno%", pageNo)
                sb.AppendLine(strHeader)
                pageNo += 1
            End If

            If recordCount = 0 Then
                sb.AppendLine(strTblHeader)
            End If

            srNo += 1
            Dim sty As String = ""
            Dim tdStyle_red As String = String.Empty
            If bRedLine = True Then
                If srNo = records Then
                    sty = ";border-bottom-color:red; border-bottom-style: solid;border-bottom-width :thick"
                    'style="width: 100px; border-bottom-color: red; border-bottom-style: solid;"
                    ' tdStyleArabic = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 9pt verdana,timesroman,garamond""

                End If
            End If
            If reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=green>")
            ElseIf reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=red>")
            Else
                sb.AppendLine("<TR style=""height:30px"">")
            End If







            'serial no
            sb.AppendLine("<TD ALIGN=CENTER style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>")
            sb.AppendLine(srNo.ToString + "</TD>")
            'last grade
            sb.AppendLine("<TD style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>")


            If StrConv(Trim(reader("STP_MINLIST").ToString), VbStrConv.ProperCase) <> "Regular" Then
                sb.AppendLine("NEW" + "</TD>")
            Else

                If Trim(reader("STP_BBSLNO").ToString) <> "" Then
                    sb.AppendLine(reader.GetString(1).ToUpper + "/" + Trim(reader("STP_BBSLNO").ToString) + "</TD>")
                Else
                    sb.AppendLine(reader.GetString(1).ToUpper + "</TD>")
                End If

            End If



            ' bluebookID
            sb.AppendLine("<TD style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>")
            sb.AppendLine("xxxxx</TD>")


            If Session("sbsuid") = "131001" Then

                sb.AppendLine("<TD style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>")
                sb.AppendLine("xxxxxxxxxx</TD>")
            End If



            'name in english

            Dim stu_ShowName As String
            If reader("SHOW_FLAG").ToString = "0" Then
                stu_ShowName = reader.GetString(2).ToUpper
            Else
                stu_ShowName = reader("Act_Stu_PasprtName").ToString
            End If
            Dim stu_id As String = String.Empty
            Dim stuLink As String = String.Empty
            stu_id = Encr_decrData.Encrypt(Convert.ToString(reader("STU_ID")))

            stuLink = "../Students/StudRecordEdit_new.aspx?MainMnu_code=SbWsUfLyXSM=&datamode=4xCdg/cr4Xw=&viewid=" + stu_id + "&SetMnu=" + lstrMenuCode + "&SetGrade=" + lstrGrade + "&SetSect=" + lstrSection




            'stuName = reader.GetString(2).Trim

            'stuName = stuName.Replace("  ", " ")
            'stuNames = stuName.Split(" ")
            'parentName = reader.GetString(28)

            'If (bShowParent1 = True And stuNames.Length = 1) Or (bShowParent2 = True And stuNames.Length = 2) Then
            '    stuName = stuName + " / " + parentName
            'End If

            'sb.AppendLine(stuName.ToUpper + "</a></TD></TR></TABLE></TD>")


            'If Session("sBSUID") <> "131002" Then
            '    If Len(reader.GetString(2)) < 25 Then

            '        sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond " + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond "" align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
            '        '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
            '        sb.AppendLine(reader.GetString(2).ToUpper + "</a></TD></TR></TABLE></TD>")
            '    Else
            '        sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond"" align=left><a STYLE=""text-decoration:none;color:black""href=" + stuLink + ">")
            '        ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
            '        ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
            '        sb.AppendLine(reader.GetString(2).ToUpper + "</a></TD></TR></TABLE></TD>")
            '    End If
            'Else
            If Session("sbsuid") = "135010" Then
                If lstrBB_CAN_EDIT <> "1" Then
                    sb.AppendLine("<TD style=""FONT: bold 8pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 8pt verdana,timesroman,garamond';COLOR:#FFFFFF"" align=left>")
                Else
                    sb.AppendLine("<TD style=""FONT: bold 8pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 8pt verdana verdana,timesroman,garamond;COLOR:#FFFFFF"" align=left><a STYLE=""text-decoration:none;color:black""href=" + stuLink + ">")
                End If
                ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
            ElseIf lstrBB_CAN_EDIT <> "1" Then
                'If Session("sroleid") <> "33" And Session("sroleid") <> "38" Then
                If Len(stu_ShowName) < 25 Then
                    sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond;COLOR:#FFFFFF " + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond;COLOR:#FFFFFF "" align=left>")
                    '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
                    sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
                Else
                    sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond;COLOR:#FFFFFF"" align=left>")
                    ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                    ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                    sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
                End If
            Else
                If Len(reader.GetString(2)) < 25 Then
                    sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond;COLOR:#FFFFFF " + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond;COLOR:#FFFFFF "" align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                    '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
                    sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
                Else
                    sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """ align=left><TABLE Width=""200""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond;COLOR:#FFFFFF"" align=left><a STYLE=""text-decoration:none;color:black""href=" + stuLink + ">")
                    ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                    ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                    sb.AppendLine(stu_ShowName.ToUpper + "</a></TD></TR></TABLE></TD>")
                End If
            End If
            'End If


            'name in arabic
            If Session("sbsuid") = "135010" Then
                sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond;COLOR:#FFFFFF;text-align:Right" + sty + """><TABLE Width=""300""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond;text-align:Right" + "" + """>")

            Else
                sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond;text-align:Right" + sty + """><TABLE Width=""300""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond;text-align:Right" + "" + """>")

            End If
            sb.AppendLine(reader.GetString(3) + "&nbsp;&nbsp;</TD></TR></TABLE></TD>")

            Dim fontsize As String

            If Session("sbsuid") = "135010" Then
                fontsize = "bold 8pt"
            Else
                fontsize = "bold 10pt"
            End If

            'For 131002, this is not required since it is a boys school
            If ((Session("sbsuid") = "131001") Or (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010")) Then
                'Gender
                sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF;text-align:Center;COLOR:#FFFFFF" + sty + """>")
                sb.AppendLine("&nbsp;</TD>")
            End If

            'nationality
            sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""40""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond;text-align:Right" + "" + """>")
            sb.AppendLine("&nbsp;&nbsp;</TD></TR></TABLE></TD>")

            'religion
            sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF;text-align:Center" + sty + """>")
            If reader.GetString(8) = "ISL" Then
                sb.AppendLine("&nbsp;&nbsp;</TD>")
            Else
                sb.AppendLine("&nbsp;&nbsp;</TD>")
            End If

            'place of birth

            If (reader.GetString(26) = 172) Then
                If InStr(reader.GetString(27), "BU", CompareMethod.Text) Then
                    lstrPOB = "AUH,UAE"

                ElseIf InStr(reader.GetString(27), "MAN", CompareMethod.Text) Then
                    lstrPOB = "AJ,UAE"
                ElseIf InStr(reader.GetString(27), "BAI", CompareMethod.Text) Then
                    lstrPOB = "DXB,UAE"
                ElseIf InStr(reader.GetString(27), "UJ", CompareMethod.Text) Then
                    lstrPOB = "FUJ,UAE"
                ElseIf InStr(reader.GetString(27), "AS", CompareMethod.Text) Then
                    lstrPOB = "RAK,UAE"
                ElseIf InStr(reader.GetString(27), "HAR", CompareMethod.Text) Then
                    lstrPOB = "SHJ,UAE"
                ElseIf InStr(reader.GetString(27), "Q", CompareMethod.Text) Then
                    lstrPOB = "UMQ,UAE"
                ElseIf InStr(reader.GetString(27), "AIN", CompareMethod.Text) Then
                    lstrPOB = "AIN,UAE"
                ElseIf InStr(reader.GetString(27), "OR", CompareMethod.Text) Then
                    lstrPOB = "KORF,UAE"
                Else
                    lstrPOB = "UAE"
                End If

            Else
                lstrPOB = reader.GetString(10)
            End If
            sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""80""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            sb.AppendLine("</TD></tr></table></TD>")

            'date of birth
            sb.AppendLine("<TD style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""80""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            sb.AppendLine("</TD></tr></table></TD>")


            dob = reader.GetDateTime(11)
            cutOffDate = Session("Cutoff_Age")

            'intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))
            'intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate))
            'ageYear = Int(intHold / 12)
            'ageMonth = intHold Mod 12

            intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))

            intHold = Int(DateDiff(DateInterval.Day, dob, cutOffDate))
            ageYear = Int(intHold / 365.2425)
            ageMonth = Math.Floor((intHold Mod 365.2425) / 30.436875)




            'age years
            sb.AppendLine("<TD align=""center"" style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>")
            sb.AppendLine(ageYear.ToString + "</TD>")

            ''new pass readmit
            'sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond"">")
            'If grd_id <> "KG1" And reader.GetString(14) = "I" Then
            '    sb.AppendLine(reader.GetString(15) + "</TD>")
            'Else
            '    sb.AppendLine("NEW</TD>")
            'End If


            'new pass readmit

            ''If (reader.GetString(14) = "I") And reader.GetString(27) = "Y" Then -- Commented as told by Charan - 08 Sep
            'If (reader.GetString(14) = "I") Then
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=""PASS"" /></TD>")
            'ElseIf grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            'Else
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            '    'sb.AppendLine("NEW</TD>")
            'End If

            If ((Session("sbsuid") <> "131002")) Then
                sb.AppendLine("<TD align=""middle"" style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>")
                cutOffDate_TFRTYPE = "2008-04-01"
                doj = reader.GetDateTime(20)
                intJoin = Int(DateDiff(DateInterval.Day, doj, cutOffDate_TFRTYPE))
                lstrPrevYrResult = reader.GetString(15)
                If grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf ((reader.GetString(14) = "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = reader.GetString(15)
                    If lstrPrevYrResult <> "Fail" Then lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) = "R")) Then
                    lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) <> "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "New"
                End If

                If grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "I") Then

                    lstrPrevYrResult = "Pass"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "N") Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" Then
                    lstrPrevYrResult = "New"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper = "FAIL" Then
                    lstrPrevYrResult = "Fail"
                End If
                If (Session("sbsuid") = "135010") Then
                    sb.AppendLine(" <input style=""FONT: bold 8pt verdana,timesroman,garamond;COLOR:#FFFFFF ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + lstrPrevYrResult + " /></TD>")
                Else
                    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ;COLOR:#FFFFFF; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + lstrPrevYrResult + " /></TD>")
                End If
            Else
                sb.AppendLine("<TD align=""middle"" style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>")
                lstrPrevYrResult = reader.GetString(15)
                If grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf ((reader.GetString(14) = "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) <> "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = reader.GetString(15)
                End If

                If grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "I") Then

                    lstrPrevYrResult = "Pass"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" Then
                    lstrPrevYrResult = "New"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper = "FAIL" Then
                    lstrPrevYrResult = "Fail"
                End If

                sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond;COLOR:#FFFFFF ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + lstrPrevYrResult + " /></TD>")

            End If


            'telephone
            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 8pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""80""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            sb.AppendLine("</TD></TR></TABLE></TD>")
            'sb.AppendLine(Trim(reader.GetString(12)).Replace("-", "").Replace("00971", "").Replace("0971", "").Replace("971", "") + "</TD>")



            'pobox
            'Dim lstrPB As String

            'If InStr(reader.GetString(28), "BU", CompareMethod.Text) Then
            '    lstrPB = "AUH"
            'ElseIf InStr(reader.GetString(28), "AIN", CompareMethod.Text) Then
            '    lstrPB = "AIN"
            'ElseIf InStr(reader.GetString(28), "MAN", CompareMethod.Text) Then
            '    lstrPB = "AJ"
            'ElseIf (InStr(reader.GetString(28), "BAI", CompareMethod.Text) Or InStr(reader.GetString(28), "DXB", CompareMethod.Text)) Then
            '    lstrPB = "DXB"
            'ElseIf InStr(reader.GetString(28), "UJ", CompareMethod.Text) Then
            '    lstrPB = "FUJ"
            'ElseIf InStr(, "AS", CompareMethod.Text) Then
            '    lstrPB = "RAK"
            'ElseIf InStr(reader.GetString(28), "HAR", CompareMethod.Text) Then
            '    lstrPB = "SHJ"
            'ElseIf InStr(reader.GetString(28), "Q", CompareMethod.Text) Then
            '    lstrPB = "UMQ"
            'ElseIf InStr(reader.GetString(28), "OR", CompareMethod.Text) Then
            '    lstrPB = "KORF"
            'Else
            '    lstrPB = "SHJ"
            'End If

            Dim lstrPB As String

            If InStr(Convert.ToString(reader.GetString(28)), "AUH", CompareMethod.Text) Then
                lstrPB = "AUH"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AIN", CompareMethod.Text) Then
                lstrPB = "AIN"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AJ", CompareMethod.Text) Then
                lstrPB = "AJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "DXB", CompareMethod.Text) Then
                lstrPB = "DXB"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "FUJ", CompareMethod.Text) Then
                lstrPB = "FUJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "RAK", CompareMethod.Text) Then
                lstrPB = "RAK"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "SHJ", CompareMethod.Text) Then
                lstrPB = "SHJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "UMQ", CompareMethod.Text) Then
                lstrPB = "UMQ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "KORF", CompareMethod.Text) Then
                lstrPB = "KORF"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "BU", CompareMethod.Text) Then
                lstrPB = "AUH"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AIN", CompareMethod.Text) Then
                lstrPB = "AIN"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "MAN", CompareMethod.Text) Then
                lstrPB = "AJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "BAI", CompareMethod.Text) Then
                lstrPB = "DXB"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "UJ", CompareMethod.Text) Then
                lstrPB = "FUJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AS", CompareMethod.Text) Then
                lstrPB = "RAK"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "HAR", CompareMethod.Text) Then
                lstrPB = "SHJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "Q", CompareMethod.Text) Then
                lstrPB = "UMQ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "OR", CompareMethod.Text) Then
                lstrPB = "KORF"

            Else
                lstrPB = "SHJ"

            End If











            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 8pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""50""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            If reader.GetString(13) = "" Then
                sb.AppendLine("&nbsp</TD>")
            Else
                sb.AppendLine("</TD></TR></TABLE></TD>")
            End If


            'date of join
            sb.AppendLine("<TD align=""center"" style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""80""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            sb.AppendLine("</TD></TR></TABLE></TD>")

            'join grade
            sb.AppendLine("<TD align=""center"" style=""FONT: " + fontsize + " verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""40""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            sb.AppendLine("</TD></TR></TABLE></TD>")






            'new TI readmit

            'If ((Session("sbsuid") <> "131002")) Then
            '    sb.AppendLine("<TD align=""center"" style=""FONT: " + fontsize + " verdana,timesroman,garamond" + sty + """>")
            '    If Trim(reader("STU_BB_TFRTYPE").ToString) <> "" Then
            '        sb.AppendLine(Trim(reader("STU_BB_TFRTYPE").ToString) + "</TD>")

            '    ElseIf ((reader.GetString(14) = "I") And (intJoin <= 0)) Then
            '        If (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010") Then
            '            sb.AppendLine("I.T</TD>")
            '        Else
            '            sb.AppendLine("T.I</TD>")
            '        End If
            '    ElseIf reader.GetString(14) = "R" Then
            '        sb.AppendLine("RE-ADMIT</TD>")
            '    ElseIf ((reader.GetString(14) <> "I") And (intJoin <= 0)) Then
            '        sb.AppendLine("NEW</TD>")
            '        'ElseIf ((reader.GetString(14) = "I")) Then
            '        '    sb.AppendLine("T.I</TD>")
            '    Else
            '        sb.AppendLine("&nbsp;</TD>")

            '    End If
            'Else
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""98%""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
            If reader.GetString(14) = "I" Then
                sb.AppendLine("I.T</TD></TR></TABLE></TD>")
            ElseIf reader.GetString(14) = "R" Then
                sb.AppendLine("RE-ADMIT</TD></TR></TABLE></TD>")
            Else
                sb.AppendLine("NEW</TD></TR></TABLE></TD>")
            End If
            'End If





            If reader.GetDateTime(24).Date < Now.Date Then

                'leaving date
                sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""60""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")
                sb.AppendLine(Format(reader.GetDateTime(33).Date, "dd/MMM/yyyy") + "</TD></TR></TABLE></TD>")

                'leaving reason
                sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """><TABLE Width=""60""><TR><TD style=""FONT: " + fontsize + " verdana,timesroman,garamond" + "" + """>")

                If reader.GetString(25).ToUpper <> "" Then
                    sb.AppendLine(reader.GetString(25).ToUpper + "</TD></TR></TABLE></TD>")
                Else
                    sb.AppendLine("SO </TD></TR></TABLE></TD>")
                End If


            Else
                'leaving date
                sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF;color:white" + sty + """><TABLE Width=""60""><TR><TD>")
                sb.AppendLine("&nbsp;</TD></TR></TABLE></TD>")
                'leaving reason
                sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>")
                sb.AppendLine("&nbsp</TD>")
            End If


            'result annual
            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>&nbsp</TD>")


            'reexam
            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond;COLOR:#FFFFFF" + sty + """>&nbsp</TD>")

            'remarks
            sb.AppendLine("<TD align=""center"" style=""FONT: " + boldStyle + " 7pt verdana,timesroman,garamond" + sty + """>&nbsp")
            sb.AppendLine(reader.GetString(34).ToUpper + "</TD>")
            sb.AppendLine("</TR>")



            recordCount += 1



            If recordCount = pageSize Then

                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")

                sb.AppendLine(strFooter)

                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")


                recordCount = 0

            End If

            If recordCount = pageSize Then
                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                recordCount = 0
            End If


        End While
        reader.Close()


        If recordCount <> 0 Then
            Dim remainingLines As Integer = pageSize - recordCount
            Dim i As Integer
            For i = 0 To remainingLines - 1
                sb.AppendLine(EmptyLines)
            Next

            sb.AppendLine("</TABLE>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            sb.AppendLine(strFooter)
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")

        End If
        reader.Close()
        sb.AppendLine("</BODY></HTML>")
        Return sb.ToString
    End Function
    Function EmptyLines() As String
        Dim sb1 As New StringBuilder
        Dim i As Integer
        Dim j As Integer
        sb1.AppendLine("<TR style=""height:30px"">")

        If ((Session("sbsuid") = "131001") Or (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010")) Then
            j = 21
        Else
            j = 20
        End If

        For i = 0 To j

            sb1.AppendLine("<TD>&nbsp</TD>")

        Next
        sb1.AppendLine("</TR>")

        Return sb1.ToString
    End Function


    Function GetHeadersAndFooter(ByVal sct_id As String) As String()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BB_HDR,BB_FTR,BB_TBL_HDR,BB_REDLINE FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim strHeader As String
        Dim strFooter As String
        Dim strTblHeader As String
        Dim totstuds As Integer = 0
        Dim boys As Integer = 0
        Dim girls As Integer = 0
        Dim bRedLine As Boolean

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            strHeader = reader.GetString(0)
            strFooter = reader.GetString(1)
            strTblHeader = reader.GetString(2)
            bRedLine = reader.GetBoolean(3)
        End While
        reader.Close()
        strHeader = strHeader.Replace("''", "'")
        strFooter = strFooter.Replace("''", "'")
        strTblHeader = strTblHeader.Replace("''", "'")
        str_query = "exec studGETBLUEBOOKHEADERANDFOOTER_SHARJAH " + sct_id

        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            With reader
                strHeader = strHeader.Replace("%moelogo%", "..\images\moe.gif")
                strHeader = strHeader.Replace("%logo%", .GetString(1))
                If Session("sbsuid") = "135010" Then
                    If .GetString(2) = "FS1" Then
                        strHeader = strHeader.Replace("%grade%", "KG1/FS1")
                    ElseIf .GetString(2) = "FS2" Then
                        strHeader = strHeader.Replace("%grade%", "KG2/FS2")
                    Else
                        strHeader = strHeader.Replace("%grade%", .GetString(2))
                    End If
                Else
                    strHeader = strHeader.Replace("%grade%", .GetString(2))
                End If
                strHeader = strHeader.Replace("%section%", .GetString(3))
                strHeader = strHeader.Replace("%accyear%", .GetString(4))
                strHeader = strHeader.Replace("%cteacher%", .GetString(5))
                strFooter = strFooter.Replace("%registrar%", .GetString(6))
                strFooter = strFooter.Replace("%checkedby%", .GetString(10))
                strFooter = strFooter.Replace("%printdate%", Session("PrintDate"))
                totstuds = .GetValue(7)
                boys = .GetValue(8)
                girls = .GetValue(9)
                If totstuds = boys Then
                    If ((Session("sbsuid") = "131001")) Then
                        strHeader = strHeader.Replace("%wing%", "B")
                    Else
                        strHeader = strHeader.Replace("%wing%", "Boys Wing")
                    End If
                ElseIf totstuds = girls Then
                    If ((Session("sbsuid") = "131001")) Then
                        strHeader = strHeader.Replace("%wing%", "G")
                    Else
                        strHeader = strHeader.Replace("%wing%", "Girls Wing")
                    End If
                Else
                    If ((Session("sbsuid") = "131001")) Then
                        strHeader = strHeader.Replace("%wing%", "G and B")
                    Else
                        strHeader = strHeader.Replace("%wing%", "Girls and Boys")
                    End If
                End If


            End With
        End While
        reader.Close()
        Dim str(2) As String

        str(0) = strHeader
        str(1) = strFooter
        str(2) = strTblHeader
        Return str
    End Function


    Function REPRINT_GetBlueBookForSection(ByVal grd_id As String, ByVal sct_id As String) As String
        Dim sb As New StringBuilder
        Dim strHeaders As String
        Dim srNo As Integer = 0
        Dim strHeader As String
        Dim strFooter As String
        Dim strTblHeader As String
        Dim recordCount As Integer = 0
        Dim bRedLine As Boolean = False
        Dim pages As Integer
        Dim pageSize As Integer = 16


        Dim ageYear As Integer = 0
        Dim ageMonth As Integer = 0
        Dim cutOffDate As Date
        Dim dob As Date
        Dim intHold As Integer
        Dim tdStyle As String


        Dim intJoin As Integer
        Dim doj As Date
        Dim cutOffDate_TFRTYPE As Date
        Dim lstrPrevYrResult As String



        Dim bHdrAllPages As Boolean = False
        Dim bFtrAllPages As Boolean = False
        Dim bNationality As Boolean = False
        Dim bReligion As Boolean = False
        Dim bRelDISAll As Boolean = False
        Dim bGender As Boolean = False
      
        Dim bBlank As Boolean = False
        Dim bIncludeTC As Boolean = False
        Dim bIncludeSO As Boolean = False
        Dim bShowParent1 As Boolean = False
        Dim bShowParent2 As Boolean = False
        Dim bAutoPage_No As Boolean = False
        Dim bBlankSheet As Boolean = False
        Dim BB_bPOB_ABBRVN As Boolean = False
        Dim prevSchInt As String = ""
        Dim prevSchOvr As String = ""
        Dim prevSchNew As String = ""


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT BB_TBL_HDR FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
       
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            strTblHeader = reader.GetString(0)
        End While
        reader.Close()
        strTblHeader = strTblHeader.Replace("''", "'")

        




        str_query = "SELECT BB_HDR_ALLPAGES,BB_FTR_ALLPAGES,BB_NATIONALITY," _
                                & " BB_RELIGION,BB_REL_DISPLAYALL,BB_GENDER,BB_PAGESIZE," _
                                & " BB_REDLINE,BB_BLANK_REPLACE,ISNULL(BB_INCLUDETC,'FALSE'),ISNULL(BB_INCLUDESO,'FALSE')," _
                                & " ISNULL(BB_INTPREVSCHOOL,'DATA'), " _
                                & " ISNULL(BB_OVRPREVSCHOOL,'BLANK'),ISNULL(BB_NEWPREVSCHOOL,'BLANK'),ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE'),  " _
                                & " ISNULL(BB_AUTOPAGE_NO,'FALSE'),ISNULL(BB_BLANKSHEET,'FALSE') ,ISNULL(BB_bPOB_ABBRVN,'FALSE') FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"


        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            With reader
                bHdrAllPages = .GetBoolean(0)
                bFtrAllPages = .GetBoolean(1)
                bNationality = .GetBoolean(2)
                bReligion = .GetBoolean(3)
                bRelDISAll = .GetBoolean(4)
                bGender = .GetBoolean(5)
                pageSize = .GetValue(6)
                bRedLine = .GetBoolean(7)
                bBlank = .GetBoolean(8)
                bIncludeTC = .GetBoolean(9)
                bIncludeSO = .GetBoolean(10)
                prevSchInt = .GetString(11)
                prevSchOvr = .GetString(12)
                prevSchNew = .GetString(13)
                bShowParent1 = .GetBoolean(14)
                bShowParent2 = .GetBoolean(15)
                ViewState("AutoPage_No") = .GetBoolean(16)
                ViewState("BlankSheet") = .GetBoolean(17)
                BB_bPOB_ABBRVN = .GetBoolean(18)

            End With

        End While
        reader.Close()

        str_query = "SELECT ISNULL(STU_BLUEID,'&nbsp'),ISNULL((SELECT GRM_DISPLAY+' '+SCT_DESCR AS LastGrade " _
                           & " FROM  STUDENT_PROMO_S INNER JOIN  GRADE_BSU_M ON STUDENT_PROMO_S.STP_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN" _
                           & " SECTION_M ON STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID " _
                           & " WHERE STUDENT_PROMO_S.STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STUDENT_PROMO_S.STP_STU_ID =A.STU_ID),'NEW') AS LastGrade," _
                           & " Case When [dbo].[ufn_CountChar] (Replace(Replace(Ltrim(Rtrim(STU_PASPRTNAME)),'  ',' '),'  ',' '),' ')<=1 Then " _
                           & " STU_PASPRTNAME+'/'+Case Left(STU_PRIMARYCONTACT,1) " _
                        & " When 'F' Then isNULL(STS_FFIRSTNAME,'')+' '+isNULL(STS_FMIDNAME,'')+' '+isNULL(STS_FLASTNAME,'') " _
                        & " When 'M' Then isNULL(STS_MFIRSTNAME,'')+' '+isNULL(STS_MMIDNAME,'')+' '+isNULL(STS_MLASTNAME,'') " _
                        & " When 'G' Then isNULL(STS_GFIRSTNAME,'')+' '+isNULL(STS_GMIDNAME,'')+' '+isNULL(STS_GLASTNAME,'') " _
                           & " End Else STU_PASPRTNAME END as STU_PASPRTNAME " _
                           & " ,STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')," _
                           & " ISNULL(STU_GENDER,''),STU_GENDERDETAIL=CASE STU_GENDER WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END," _
                           & " ISNULL(C.CTY_SHORT,''),ISNULL(C.CTY_NATIONALITY,''),ISNULL(RLG_ID,0),ISNULL(RLG_DESCR,''),POB=CASE STU_COB WHEN 172 THEN ISNULL(STU_POB,'')+'/UAE' ELSE ISNULL(J.CTY_DESCR,'') END ,ISNULL(STU_DOB,'')," _
                           & " STU_EMGCONTACT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FMOBILE,'') " _
                   & " WHEN 'M' THEN ISNULL(STS_MMOBILE,'') " _
                   & " ELSE ISNULL(STS_GMOBILE,'') END, " _
                           & " STU_POBOX=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMPOBOX,'') WHEN 'M' THEN ISNULL(STS_MCOMPOBOX,'') ELSE ISNULL(STS_GCOMPOBOX,'') END," _
                           & " ISNULL(STU_TFRTYPE,''), ISNULL((SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
                           & " AND STP_STU_ID =A.STU_ID),'NEW') ,ISNULL(STU_PREVSCHI,''),ISNULL(STU_PREVSCHI_COUNTRY,''),ISNULL(CLM_DESCR,''), " _
                           & " ISNULL(STU_PREVSCHI_LASTATTDATE,'01/01/1900'),ISNULL(STU_MINDOJ,''),ISNULL(STU_GRD_ID_JOIN,''),ISNULL(I.TCM_STU_ID,0),ISNULL(TCM_TCSO,''),ISNULL(I.TCM_LASTATTDATE,'2100-01-01'),ISNULL((select CASE WHEN ((tct_code=1) OR (tct_code=3)) THEN 'WITHIN UAE TC' WHEN ((tct_code=2) OR (tct_code=4)) THEN 'OUTSIDE UAE TC' ELSE '' END  from dbo.TC_TFRTYPE_M where tct_code=ISNULL(I.TCM_TCTYPE,0)),''),isNULL(STU_COB,5) as ST_COB,isNULL(STU_POB,'') as STU_POB,isNULL(STS_FCOMCITY,'') as  STS_FCOMCITY,ISNULL(STU_GENDER,'') as STU_GENDER " _
                           & " , STU_ID ,ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
                           & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
                           & " (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'Regular') as STP_MINLIST," _
                           & " isnull((select isnull(STP_BBSLNO,'') from STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID),'') as STP_BBSLNO FROM STUDENT_M AS A INNER JOIN " _
                           & " STUDENT_D AS B ON A.STU_SIBLING_ID = B.STS_STU_ID" _
                           & " INNER JOIN COUNTRY_M AS C  ON C.CTY_ID = A.STU_NATIONALITY" _
                           & " INNER JOIN RELIGION_M AS F ON A.STU_RLG_ID = F.RLG_ID" _
                           & " LEFT OUTER JOIN CURRICULUM_M AS G ON A.STU_PREVSCHI_CLM=G.CLM_ID" _
                           & " LEFT OUTER JOIN GRADE_BSU_M AS H ON A.STU_GRM_ID_JOIN=H.GRM_ID" _
                           & " LEFT OUTER JOIN TCM_M AS I ON A.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL" _
                           & " LEFT OUTER JOIN TC_REASONS_M AS K ON I.TCM_REASON=K.TCR_CODE" _
                           & " LEFT OUTER JOIN COUNTRY_M AS J  ON J.CTY_ID = A.STU_COB" _
                           & " WHERE STU_MINLIST='Regular'" _
                           & "   AND STU_CURRSTATUS<>'CN' and isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"

        If bIncludeTC = False And bIncludeSO = False Then
            str_query += " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) "
        End If

        If ViewState("MainMnu_code") = "S100450" Then
            str_query += "AND STU_ID IN(" + Session("BlueBookStuIds") + ")"
        End If


        str_query += " ORDER BY STU_PASPRTNAME"
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)


        str_query = " SELECT COUNT(STU_ID) FROM STUDENT_M WHERE STU_SCT_ID=" + sct_id + " AND STU_MINLIST='Regular'"
        Dim records As Integer
        If ViewState("MainMnu_code") = "S100450" Then
            records = CType(Session("BlueBookRecords"), Integer)
        Else
            str_query = " SELECT COUNT(STU_ID) FROM STUDENT_M WHERE STU_SCT_ID=" + sct_id + " AND STU_MINLIST='Regular'"
            records = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        End If

        Dim blank As String = "&nbsp"
        If bBlank = True Then
            blank = "-"
        End If

        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: Blue Book ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<BODY>")

        Dim pageNo As Integer = 1
        Dim lstrPOB As String

        While reader.Read


            If recordCount = 0 And bHdrAllPages = True Then
                If ViewState("FirstPage") <> "1" Then
                    sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
                End If
                ViewState("FirstPage") = "0"
                'strHeader = strHeaders(0)
                '  strHeader = strHeader.Replace("%pgno%", pageNo)
                '  sb.AppendLine(strHeader)
                ' pageNo += 1
            End If
            strTblHeader = strTblHeader.Replace("Page No:", "")
            If recordCount = 0 Then
                sb.AppendLine(strTblHeader)
            End If

            srNo += 1
            Dim sty As String = ""
            Dim tdStyle_red As String = String.Empty
            If bRedLine = True Then
                If srNo = records Then
                    sty = ";border-bottom-color:red; border-bottom-style: solid;border-bottom-width :thick"
                    'style="width: 100px; border-bottom-color: red; border-bottom-style: solid;"
                    ' tdStyleArabic = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 9pt verdana,timesroman,garamond""

                End If
            End If
            If reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=green>")
            ElseIf reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=red>")
            Else
                sb.AppendLine("<TR style=""height:30px"">")
            End If







            'serial no
            sb.AppendLine("<TD ALIGN=CENTER style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(srNo.ToString + "</TD>")
            'last grade
            sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")


            If StrConv(Trim(reader("STP_MINLIST").ToString), VbStrConv.ProperCase) <> "Regular" Then
                sb.AppendLine("NEW" + "</TD>")
            Else

                If Trim(reader("STP_BBSLNO").ToString) <> "" Then
                    sb.AppendLine(reader.GetString(1).ToUpper + "/" + Trim(reader("STP_BBSLNO").ToString) + "</TD>")
                Else
                    sb.AppendLine(reader.GetString(1).ToUpper + "</TD>")
                End If

            End If



            ' bluebookID
            sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(reader.GetString(0) + "&nbsp;</TD>")

            'name in english
            Dim stu_id As String = String.Empty
            Dim stuLink As String = String.Empty
            stu_id = Encr_decrData.Encrypt(Convert.ToString(reader("STU_ID")))

            stuLink = "../Students/StudRecordEdit_new.aspx?MainMnu_code=SbWsUfLyXSM=&datamode=4xCdg/cr4Xw=&viewid=" + stu_id + "&SetMnu=" + lstrMenuCode + "&SetGrade=" + lstrGrade + "&SetSect=" + lstrSection




            'stuName = reader.GetString(2).Trim

            'stuName = stuName.Replace("  ", " ")
            'stuNames = stuName.Split(" ")
            'parentName = reader.GetString(28)

            'If (bShowParent1 = True And stuNames.Length = 1) Or (bShowParent2 = True And stuNames.Length = 2) Then
            '    stuName = stuName + " / " + parentName
            'End If

            'sb.AppendLine(stuName.ToUpper + "</a></TD></TR></TABLE></TD>")



            If Len(reader.GetString(2)) < 25 Then

                sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond " + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 10pt verdana,timesroman,garamond "" align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                '' sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond"">")
                sb.AppendLine(reader.GetString(2).ToUpper + "</a></TD></TR></TABLE></TD>")
            Else
                sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """ align=left><TABLE Width=""98%""><TR><TD style=""FONT: bold 7pt verdana,timesroman,garamond"" align=left><a STYLE=""text-decoration:none;color:black""href=" + stuLink + ">")
                ' sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
                ' sb.AppendLine("<TD style=""FONT: bold 7pt verdana,timesroman,garamond"">")
                sb.AppendLine(reader.GetString(2).ToUpper + "</a></TD></TR></TABLE></TD>")
            End If


            'name in arabic
            sb.AppendLine("<TD style=""FONT: bold 12pt verdana,timesroman,garamond;text-align:Right" + sty + """>")
            sb.AppendLine(reader.GetString(3) + "</TD>")


            If ((Session("sbsuid") = "131001") Or (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010")) Then
                'Gender
                sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond;text-align:Center" + sty + """>")
                sb.AppendLine(Convert.ToString(reader("STU_GENDER")).ToUpper + "</TD>")
            End If

            'nationality
            sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(reader.GetString(6).ToUpper + "</TD>")

            'religion
            sb.AppendLine("<TD style=""FONT: bold 11pt verdana,timesroman,garamond;text-align:Center" + sty + """>")
            If reader.GetString(8) = "ISL" Then
                sb.AppendLine("M</TD>")
            Else
                sb.AppendLine("NM</TD>")
            End If

            'place of birth

            If (reader.GetString(26) = 172) Then
                If InStr(reader.GetString(27), "BU", CompareMethod.Text) Then
                    lstrPOB = "AUH,UAE"

                ElseIf InStr(reader.GetString(27), "MAN", CompareMethod.Text) Then
                    lstrPOB = "AJ,UAE"
                ElseIf InStr(reader.GetString(27), "BAI", CompareMethod.Text) Then
                    lstrPOB = "DXB,UAE"
                ElseIf InStr(reader.GetString(27), "UJ", CompareMethod.Text) Then
                    lstrPOB = "FUJ,UAE"
                ElseIf InStr(reader.GetString(27), "AS", CompareMethod.Text) Then
                    lstrPOB = "RAK,UAE"
                ElseIf InStr(reader.GetString(27), "HAR", CompareMethod.Text) Then
                    lstrPOB = "SHJ,UAE"
                ElseIf InStr(reader.GetString(27), "Q", CompareMethod.Text) Then
                    lstrPOB = "UMQ,UAE"
                ElseIf InStr(reader.GetString(27), "AIN", CompareMethod.Text) Then
                    lstrPOB = "AIN,UAE"
                ElseIf InStr(reader.GetString(27), "OR", CompareMethod.Text) Then
                    lstrPOB = "KORF,UAE"
                End If

            Else
                lstrPOB = reader.GetString(10)
            End If
            sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(lstrPOB.ToUpper + "</TD>")

            'date of birth
            sb.AppendLine("<TD style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(Format(reader.GetDateTime(11), "dd/MM/yyyy") + "</TD>")


            dob = reader.GetDateTime(11)
            cutOffDate = Session("Cutoff_Age")

            intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))
            intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate))
            ageYear = Int(intHold / 12)
            ageMonth = intHold Mod 12




            'age years
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 11pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(ageYear.ToString + "</TD>")

            ''new pass readmit
            'sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond"">")
            'If grd_id <> "KG1" And reader.GetString(14) = "I" Then
            '    sb.AppendLine(reader.GetString(15) + "</TD>")
            'Else
            '    sb.AppendLine("NEW</TD>")
            'End If


            'new pass readmit

            ''If (reader.GetString(14) = "I") And reader.GetString(27) = "Y" Then -- Commented as told by Charan - 08 Sep
            'If (reader.GetString(14) = "I") Then
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=""PASS"" /></TD>")
            'ElseIf grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            'Else
            '    sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            '    'sb.AppendLine("NEW</TD>")
            'End If

            If ((Session("sbsuid") = "131001") Or (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010")) Then
                sb.AppendLine("<TD align=""middle"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
                cutOffDate_TFRTYPE = "2008-04-01"
                doj = reader.GetDateTime(20)
                intJoin = Int(DateDiff(DateInterval.Day, doj, cutOffDate_TFRTYPE))
                lstrPrevYrResult = reader.GetString(15)
                If grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf ((reader.GetString(14) = "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) <> "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "New"
                End If

                If grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "I") Then

                    lstrPrevYrResult = "Pass"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" Then
                    lstrPrevYrResult = "New"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper = "FAIL" Then
                    lstrPrevYrResult = "Fail"
                End If

                sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + lstrPrevYrResult + " /></TD>")
            Else
                sb.AppendLine("<TD align=""middle"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
                lstrPrevYrResult = reader.GetString(15)
                If grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                    lstrPrevYrResult = reader.GetString(15)
                ElseIf ((reader.GetString(14) = "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "Pass"
                ElseIf ((reader.GetString(14) <> "I") And reader.GetString(1) = "NEW") Then
                    lstrPrevYrResult = "New"
                End If

                If grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" And (reader.GetString(14) = "I") Then

                    lstrPrevYrResult = "Pass"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper <> "FAIL" Then
                    lstrPrevYrResult = "New"
                ElseIf grd_id = "KG1" And reader.GetString(15).ToUpper = "FAIL" Then
                    lstrPrevYrResult = "Fail"
                End If

                sb.AppendLine(" <input style=""FONT: bold 9pt verdana,timesroman,garamond ; border:0 ;width:40px""  id=""Text" + srNo.ToString + " type=""text"" value=" + lstrPrevYrResult + " /></TD>")

            End If


            'telephone
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 8pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(Right(Replace(reader.GetString(12), "-", ""), 9) + "</TD>")
            'sb.AppendLine(Trim(reader.GetString(12)).Replace("-", "").Replace("00971", "").Replace("0971", "").Replace("971", "") + "</TD>")



            'pobox
            'Dim lstrPB As String

            'If InStr(reader.GetString(28), "BU", CompareMethod.Text) Then
            '    lstrPB = "AUH"
            'ElseIf InStr(reader.GetString(28), "AIN", CompareMethod.Text) Then
            '    lstrPB = "AIN"
            'ElseIf InStr(reader.GetString(28), "MAN", CompareMethod.Text) Then
            '    lstrPB = "AJ"
            'ElseIf (InStr(reader.GetString(28), "BAI", CompareMethod.Text) Or InStr(reader.GetString(28), "DXB", CompareMethod.Text)) Then
            '    lstrPB = "DXB"
            'ElseIf InStr(reader.GetString(28), "UJ", CompareMethod.Text) Then
            '    lstrPB = "FUJ"
            'ElseIf InStr(, "AS", CompareMethod.Text) Then
            '    lstrPB = "RAK"
            'ElseIf InStr(reader.GetString(28), "HAR", CompareMethod.Text) Then
            '    lstrPB = "SHJ"
            'ElseIf InStr(reader.GetString(28), "Q", CompareMethod.Text) Then
            '    lstrPB = "UMQ"
            'ElseIf InStr(reader.GetString(28), "OR", CompareMethod.Text) Then
            '    lstrPB = "KORF"
            'Else
            '    lstrPB = "SHJ"
            'End If

            Dim lstrPB As String

            If InStr(Convert.ToString(reader.GetString(28)), "AUH", CompareMethod.Text) Then
                lstrPB = "AUH"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AIN", CompareMethod.Text) Then
                lstrPB = "AIN"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AJ", CompareMethod.Text) Then
                lstrPB = "AJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "DXB", CompareMethod.Text) Then
                lstrPB = "DXB"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "FUJ", CompareMethod.Text) Then
                lstrPB = "FUJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "RAK", CompareMethod.Text) Then
                lstrPB = "RAK"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "SHJ", CompareMethod.Text) Then
                lstrPB = "SHJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "UMQ", CompareMethod.Text) Then
                lstrPB = "UMQ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "KORF", CompareMethod.Text) Then
                lstrPB = "KORF"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "BU", CompareMethod.Text) Then
                lstrPB = "AUH"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AIN", CompareMethod.Text) Then
                lstrPB = "AIN"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "MAN", CompareMethod.Text) Then
                lstrPB = "AJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "BAI", CompareMethod.Text) Then
                lstrPB = "DXB"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "UJ", CompareMethod.Text) Then
                lstrPB = "FUJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "AS", CompareMethod.Text) Then
                lstrPB = "RAK"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "HAR", CompareMethod.Text) Then
                lstrPB = "SHJ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "Q", CompareMethod.Text) Then
                lstrPB = "UMQ"
            ElseIf InStr(Convert.ToString(reader.GetString(28)), "OR", CompareMethod.Text) Then
                lstrPB = "KORF"

            Else
                lstrPB = "SHJ"

            End If











            sb.AppendLine("<TD align=""center"" style=""FONT: bold 8pt verdana,timesroman,garamond" + sty + """>")
            If reader.GetString(13) = "" Then
                sb.AppendLine("&nbsp</TD>")
            Else
                sb.AppendLine(reader.GetString(13).ToUpper + " " + lstrPB + "</TD>")
            End If


            'date of join
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(Format(reader.GetDateTime(20), "dd/MM/yyyy") + "</TD>")

            'join grade
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
            sb.AppendLine(reader.GetString(21) + "</TD>")






            'new TI readmit

            If ((Session("sbsuid") = "131001") Or (Session("sbsuid") = "133006") Or (Session("sbsuid") = "135010")) Then
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
                If ((reader.GetString(14) = "I") And (intJoin <= 0)) Then
                    sb.AppendLine("T.I</TD>")
                ElseIf reader.GetString(14) = "R" Then
                    sb.AppendLine("RE-ADMIT</TD>")
                ElseIf ((reader.GetString(14) <> "I") And (intJoin <= 0)) Then
                    sb.AppendLine("NEW</TD>")
                    'ElseIf ((reader.GetString(14) = "I")) Then
                    '    sb.AppendLine("T.I</TD>")
                Else
                    sb.AppendLine("&nbsp;</TD>")

                End If
            Else
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 10pt verdana,timesroman,garamond" + sty + """>")
                If reader.GetString(14) = "I" Then
                    sb.AppendLine("I.T</TD>")
                ElseIf reader.GetString(14) = "R" Then
                    sb.AppendLine("RE-ADMIT</TD>")
                Else
                    sb.AppendLine("NEW</TD>")
                End If
            End If





            If reader.GetDateTime(24).Date < Now.Date Then

                'leaving date
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")
                sb.AppendLine(Format(reader.GetDateTime(24).Date, "dd/MMM/yyyy") + "</TD>")

                'leaving reason
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")

                If reader.GetString(25).ToUpper <> "" Then
                    sb.AppendLine(reader.GetString(25).ToUpper + "</TD>")
                Else
                    sb.AppendLine("SO </TD>")
                End If


            Else
                'leaving date
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond;color:white" + sty + """><TABLE Width=""60""><TR><TD>")
                sb.AppendLine("&nbsp;</TD></TR></TABLE></TD>")
                'leaving reason
                sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>")
                sb.AppendLine("&nbsp</TD>")
            End If


            'result annual
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>&nbsp</TD>")

            'reexam
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>&nbsp</TD>")

            'remarks
            sb.AppendLine("<TD align=""center"" style=""FONT: bold 7pt verdana,timesroman,garamond" + sty + """>&nbsp</TD>")

            sb.AppendLine("</TR>")



            recordCount += 1



            If recordCount = pageSize Then

                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")

                sb.AppendLine(strFooter)

                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")


                recordCount = 0

            End If

            If recordCount = pageSize Then
                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                recordCount = 0
            End If


        End While
        reader.Close()


        If recordCount <> 0 Then
            Dim remainingLines As Integer = pageSize - recordCount
            Dim i As Integer
            For i = 0 To remainingLines - 1
                sb.AppendLine(EmptyLines)
            Next

            sb.AppendLine("</TABLE>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            sb.AppendLine(strFooter)
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")

        End If
        reader.Close()
        sb.AppendLine("</BODY></HTML>")
        Return sb.ToString
    End Function
#End Region
End Class
