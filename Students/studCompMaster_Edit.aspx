<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studCompMaster_Edit.aspx.vb" Inherits="studCompEdit" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <style>
        table td input[type=text] {
            min-width: 1% !important;
            width: 80%;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>Company
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0"
                    cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" class="BlueTable" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left"><span class="field-label">Company Name</span> <span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtComp" runat="server"></asp:TextBox></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Address1</span><span style="color: red">*</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtADDR1" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Address2</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtADDR2" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">City</span><span style="color: red">*</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Country</span><span style="color: red">*</span> </td>

                                    <td align="left" colspan="4">
                                        <asp:DropDownList AutoPostBack="TRUE" ID="ddlCountry" runat="server" Width="80%">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">P.O BOX</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtPOBOX" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">City/Emirate</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlEmirate" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Phone No</span><span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtOffPhone_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtOffPhone_Country" runat="server" FilterType="Numbers"
                                            TargetControlID="txtOffPhone_Country" />
                                        -
                            <asp:TextBox ID="txtOffPhone_Area" runat="server" MaxLength="5" Width="14%"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtOffPhone_Area" runat="server" FilterType="Numbers"
                                            TargetControlID="txtOffPhone_Area" />
                                        -
                            <asp:TextBox ID="txtOffPhone_No" runat="server" MaxLength="10" Width="47.5%"></asp:TextBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftetxtOffPhone_No" runat="server" FilterType="Numbers"
                                            TargetControlID="txtOffPhone_No" />
                                        <br />
                                        (Country-Area-Number) </td>
                                    <td align="left"><span class="field-label">Fax No</span></td>

                                    <td align="left" colspan="4">
                                        <asp:TextBox ID="txtFax_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                        -
                            <asp:TextBox ID="txtFax_Area" runat="server" MaxLength="5" Width="14%"></asp:TextBox>
                                        -
                            <asp:TextBox ID="txtFax_No" runat="server" MaxLength="10" Width="47.5%"></asp:TextBox><br />
                                        (Country-Area-Number)</td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">E-Mail</span> <span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Company URL</span> </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtURL" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Contact Person</span><span style="color: red">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtContactPer" runat="server"></asp:TextBox></td>
                                    <td align="left"><span class="field-label">Contact Mobile No</span></td>

                                    <td align="left" colspan="4">
                                        <asp:TextBox ID="txtMobile_Country" runat="server" MaxLength="3" Width="14%"></asp:TextBox>
                                        -
                            <asp:TextBox ID="txtMobile_Area" runat="server" MaxLength="5" Width="14%"></asp:TextBox>
                                        -
                            <asp:TextBox ID="txtMobile_No" runat="server" MaxLength="10" Width="47.5%"></asp:TextBox><br />
                                        (Country-Area-Number) </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" class="title-bg"><span class="field-label">Fee Components </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Fee Type</span> </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlFeeType" runat="server">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Discount Percent</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtDiscPer" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left"><span class="field-label">Print Proforma ?</span></td>

                                    <td align="left">
                                        <asp:RadioButton ID="rdYes" runat="server" GroupName="r1" Text="Yes"></asp:RadioButton><asp:RadioButton ID="rdNo" runat="server" GroupName="r1" Text="No"></asp:RadioButton><asp:Button
                                            ID="btnFill" runat="server" CssClass="button" Text="Add" ValidationGroup="groupM1" /><asp:Button
                                                ID="btnUpdate" runat="server" CssClass="button" Text="Update" ValidationGroup="groupM1" /><asp:Button
                                                    ID="btnChildCancel" runat="server" CssClass="button" Text="Cancel" ValidationGroup="groupM1" /></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" class="title-bg">Fee Details</td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvFeeDetail" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added" CssClass="table table-bordered table-row"
                                            Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle />
                                            <Columns>
                                                <asp:BoundField DataField="CIS_FEE_Descr" HeaderText="Fee Type">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CIS_DiscPerct" DataFormatString="{0:0.00}" HeaderText="Amount">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CIS_bPreformaInvoice" HeaderText="Print Proforma?">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:CommandField ShowDeleteButton="True">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:CommandField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                                            Text="Update"></asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                                            Text="Cancel"></asp:LinkButton>

                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CausesValidation="False" CommandName="Edit" __designer:wfdid="w1"></asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("id") %>' __designer:wfdid="w2"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="griditem_hilight" />
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" /></td>
                    </tr>

                </table>

            </div>
        </div>
    </div>
</asp:Content>

