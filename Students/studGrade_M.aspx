<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="studGrade_M.aspx.vb" MaintainScrollPositionOnPostback="true" Inherits="Students_studGrade_M"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text=" Grade Set Up"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM2" />
                            <asp:CompareValidator ID="cvCapacit" runat="server" ErrorMessage="Total Capacity should be greater than Mgmt Quota"
                                ValidationGroup="groupM1" ControlToCompare="txtMgmQuota" ControlToValidate="txtCapacity"
                                ForeColor="Transparent" Operator="GreaterThan" Type="Integer"
                                CssClass="error" Display="None">
                            </asp:CompareValidator>
                            <asp:RequiredFieldValidator ID="rfCap" runat="server" ErrorMessage="Please enter the field total capacity"
                                ControlToValidate="txtCapacity" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfMgm" runat="server" ErrorMessage="Please enter the field management quota"
                                ControlToValidate="txtMgmQuota" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regCap" runat="server" ErrorMessage="Please enter a valid data in the field total capacity"
                                ControlToValidate="txtCapacity" Display="None" ValidationExpression="^\d+$" ValidationGroup="groupM1"></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="regMgm" runat="server" ErrorMessage="Please enter a valid data in the field Mgmt Quota"
                                ControlToValidate="txtMgmQuota" Display="None" ValidationExpression="^\d+$" ValidationGroup="groupM1"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rfDisplay" runat="server" ControlToValidate="txtGradeDisplay"
                                Display="None" ErrorMessage="Please enter the field grade display" ValidationGroup="groupM2"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfDescr" runat="server" ControlToValidate="txtGradeDescr"
                                Display="None" ErrorMessage="Please enter the field grade description" ValidationGroup="groupM2"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfPrio" runat="server" ControlToValidate="txtPriority"
                                Display="None" ErrorMessage="Please enter the field student priority count" ValidationGroup="groupM2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regPrio" runat="server" ControlToValidate="txtPriority"
                                Display="None" ErrorMessage="Please enter a valid data in the field student priority count"
                                ValidationExpression="^\d+$" ValidationGroup="groupM1"></asp:RegularExpressionValidator>
                            <asp:Label ID="Label5" runat="server" Text="Student Priority Count"
                                Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="text-danger font-small">Fields Marked with ( * ) are mandatory
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <%-- &nbsp;Fields Marked<span style="font-size: 8pt; color: #800000"> </span>with(<span
                    style="font-size: 8pt; color: #800000">*</span>)are mandatory--%>

                            <asp:TextBox ID="txtPriority"
                                runat="server" Visible="False">0</asp:TextBox>
                            <asp:DropDownList ID="ddlGender" runat="server" Visible="False">
                                <asp:ListItem Value="A">All</asp:ListItem>
                                <asp:ListItem Value="M">Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- <tr class="subheader_img">
                        <td align="left" colspan="8" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Grade Set Up&nbsp;</span></font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Academicyear</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Grade</span><span class="text-danger font-small">*</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>


                                    <td align="left">
                                        <span class="field-label">Grade Display</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtGradeDisplay" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Grade Description</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtGradeDescr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>


                                    <td align="left" width="20%">
                                        <span class="field-label">Grade Certificate</span></td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtGradeCertificate" runat="server"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                </tr>
                                <%--  <tr class="subheader_img">
                        <td align="left" colspan="8" style="height: 17px" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Details&nbsp;</span></font>
                        </td>
                    </tr>--%>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Copy details from Grade</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlCopyGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" >
                                        <asp:Button ID="btnCopy" runat="server" Text="Copy" CssClass="button" ValidationGroup="groupM2" /></td>
                                         <td align="left" >
                                        <asp:CheckBox ID="chkOORPayment" runat="server" Text="Open Online Registration Payment" CssClass="field-label" />
                                    </td>
                                    <%-- <td align="left" class="matters" colspan="3">
                           
                        </td>--%>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Shift</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlShift" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left">
                                        <span class="field-label">Stream</span><span class="text-danger font-small">*</span></td>

                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlStream" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" > <span class="field-label">Total Capacity</span><span class="text-danger font-small">*</span></td>
                                  
                                    <td align="left" >
                                        <asp:TextBox ID="txtCapacity" runat="server" ></asp:TextBox>
                                    </td>
                                    <td align="left" ><span class="field-label">Mgmt Quota Capacity</span><span class="text-danger font-small">*</span></td>
                                
                                    <td align="left" >
                                        <asp:TextBox ID="txtMgmQuota" runat="server" ></asp:TextBox>
                                    </td>
                                     </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:CheckBox ID="chkOpen" Text="Open Online" runat="server" CssClass="field-label" />
                                        <asp:CheckBox ID="chkScreenReqd" Text="Screening Reqd" runat="server" CssClass="field-label" Visible="False" />
                                    </td>
                                    <td align="left" width="20%">
                                        <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" /></td>
                                    <td align="left" width="60%" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" >
                                        <table id="Table4" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" >
                                                    <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="No records to edit." 
                                                        PageSize="20" Width="100%" BorderStyle="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="acd_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAcdId" runat="server" Text='<%# Bind("acd_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AcademicYear">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAcademicYear" runat="server" Text='<%# Bind("acy_descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="grd_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("grm_grd_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="grd_id" Visible="False">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrmId" runat="server" Text='<%# Bind("grm_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Grade">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Shift">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Shift" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShiftId" runat="server" Text='<%# Bind("grm_shf_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Stream">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Stream" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStreamId" runat="server" Text='<%# Bind("grm_stm_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Gender" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGender" runat="server" Text='<%# Bind("gender") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Max. Capacity">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMaxCapacity" runat="server" Text='<%# Bind("grm_maxcapacity") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Mgmt Quota">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMgmQuota" runat="server" Text='<%# Bind("grm_mgmcapacity") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Open Online">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOpenOnline" runat="server" Text='<%# Bind("openonline") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Screening Reqd" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblScreenReqd" runat="server" Text='<%# Bind("screenreqd") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="gradedescr" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgradedescr" runat="server" Text='<%# Bind("grm_descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="gradecert" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblgradecert" runat="server" Visible="false" Text='<%# Bind("grm_certificate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="priority" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpriority" runat="server" Visible="false" Text='<%# Bind("grm_priority") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Open ORP">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("GRM_bOPEN_REG_PAY") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblORP" runat="server" Text='<%# Bind("GRM_bOPEN_REG_PAY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="edit" HeaderText="Edit" Text="Edit">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="delete" HeaderText="Delete" Text="Delete">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <HeaderStyle />
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative"  />
                                                        <EmptyDataRowStyle  />
                                                        <EditRowStyle />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" valign="bottom" colspan="8">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" TabIndex="5" />
                                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Edit" TabIndex="6" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM2"
                                            TabIndex="7" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                            <asp:HiddenField ID="hfDisplay" runat="server" />
                            <asp:HiddenField ID="hfDescription" runat="server" />
                            <asp:HiddenField ID="hfCertificate" runat="server" />
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</asp:Content>
