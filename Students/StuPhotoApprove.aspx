﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StuPhotoApprove.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Students_StuPhotoApprove" %>

<%@ Register Src="usercontrol/StuPhotoApprove.ascx" TagName="StuPhotoApprove" TagPrefix="uc1" %>

<asp:Content ID="c1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Student Photo Approval"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td>
                            <uc1:StuPhotoApprove ID="StuPhotoApprove1" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

