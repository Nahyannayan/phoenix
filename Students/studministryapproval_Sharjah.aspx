<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studministryapproval_Sharjah.aspx.vb" Inherits="Students_studministryapproval_Sharjah" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script type="text/javascript">

                function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                
                }
                function test2(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                }
                

</script>
    <script type="text/javascript">
    
    
    
    function opensha(a)
    {
    //alert(a)
    window.open('studMOEPrint_Sharjah.aspx?stuid='+ a)
    }
    
     function openrak(a)
    {
    //alert(a)
    window.open('studMOEPrint_RAK.aspx?stuid='+ a)
    }
    </script>
    
       
  <div>
      <table class="title" style="width: 100%">
          <tr>
              <td style="width: 204px" >
                  Ministry Approval Sharjah School</td>
              <td >
              </td>
          </tr>
      </table>
      <table class="matters" style="width: 382px">
          <tr>
              <td align="right" >
                  Academic Year</td>
              <td >
      <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
      </asp:DropDownList></td>
          </tr>
      </table>
      <div class="matters">
        <asp:GridView ID="gridMiniApp"  CssClass="gridstyle" EmptyDataText="No Records Found" AutoGenerateColumns="false" runat="server" AllowPaging="True" OnPageIndexChanging="gridMiniApp_PageIndexChanging" PageSize="20">
                    <RowStyle CssClass="griditem" Height="25px" />
        <Columns>
<asp:TemplateField HeaderText="No"><HeaderTemplate>
       <table>
                                  <tr>
                                   <td align="center">
                                  <asp:Label ID="lblno" runat="server" CssClass="gridheader_text" Text="Student No"></asp:Label></td>
                                </tr>
                                <tr>
                             <td >
                             <table border="0" cellpadding="0" cellspacing="0" >
                            <tr>
                               <td >
                               <div id="Div1" class="chromestyle">
                                <ul>
                               <li><a href="#" rel="dropmenu1"></a>
                               <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                  </li>
                                </ul>
                               </div>
                                </td>
                                  <td >
                            <asp:TextBox ID="txtno" runat="server" Width="40px"></asp:TextBox></td>
                                                          <td >
                               <td  valign="middle">
                               <asp:ImageButton ID="btnno_search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnNo_Search_Click"  /></td>
                               </tr>
                              </table>
                              </td>
                              </tr>
                             </table>
         
</HeaderTemplate>
<ItemTemplate>
<asp:Label id="L1" runat="server" Text='<%# Eval("STU_NO") %>'></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Name"><HeaderTemplate>
                                 <table >
                                  <tr>
                                   <td align="center">
                                  <asp:Label ID="lblname" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label></td>
                                </tr>
                                <tr>
                             <td >
                             <table border="0" cellpadding="0" cellspacing="0" >
                            <tr>
                               <td >
                               <div id="Div2" class="chromestyle">
                                <ul>
                               <li><a href="#" rel="dropmenu2"></a>
                               <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                  </li>
                                </ul>
                               </div>
                                </td>
                                  <td >
                            <asp:TextBox ID="txtName" runat="server" Width="60px"></asp:TextBox></td>
                                                          <td>
                               <td  valign="middle">
                               <asp:ImageButton ID="btnName_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"  OnClick="btnName_Search_Click" /></td>
                               </tr>
                              </table>
                              </td>
                              </tr>
                             </table>
         
</HeaderTemplate>
<ItemTemplate>
 <asp:HiddenField ID="HiddenStudid" value='<%# Eval("STU_ID") %>' runat="server" />
        <asp:Label ID="L2" Text='<%# Eval("STU_PASPRTNAME") %>' runat="server"></asp:Label>
       
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Grade"><HeaderTemplate>
        <table >
            <tr> 
               <td align="center">Grade</td>
            </tr>
            <tr>
            <td align="center">
             <asp:DropDownList ID="ddgrade" AutoPostBack="True" CssClass="listbox" Width="70px" OnSelectedIndexChanged="ddgrade_SelectedIndexChanged" runat="server"></asp:DropDownList>
             </td>
            </tr>
         </table>
         
</HeaderTemplate>
<ItemTemplate>
        <asp:Label ID="L3" Text='<%# Eval("GRM_DISPLAY") %> ' runat="server"></asp:Label>
        
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Section"><HeaderTemplate>
        <table >
            <tr> 
               <td align="center">Section</td>
            </tr>
            <tr>
            <td align="center">
                  <asp:DropDownList ID="ddsection" AutoPostBack="True" CssClass="listbox" Width="70px" OnSelectedIndexChanged="ddsection_SelectedIndexChanged" runat="server"></asp:DropDownList>
            </td>
            </tr>
         </table>
         
</HeaderTemplate>
<ItemTemplate>
         <asp:Label ID="L4" Text='<%#Eval("SCT_DESCR")%> ' runat="server"></asp:Label>
        
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Stream"><HeaderTemplate>
        <table >
            <tr> 
               <td align="center">Stream</td>
            </tr>
            <tr>
            <td align="center">
             <asp:DropDownList ID="ddstream" AutoPostBack="True" CssClass="listbox" Width="70px" OnSelectedIndexChanged="ddstream_SelectedIndexChanged" runat="server"></asp:DropDownList>
            </td>
            </tr>
         </table>
         
</HeaderTemplate>
<ItemTemplate>
         <asp:Label ID="L5" Text=' <%#Eval("STM_DESCR")%>' runat="server"></asp:Label>
       
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Updated"><ItemTemplate>
<center>
       <asp:Image id="Image1" runat="server" ImageUrl='<%#Eval("moeupdateimagepath")%>'></asp:Image>
       <br />
       <asp:Label id="Labelupdatedate" Text='<%# Eval("STU_MOE_UPDATE_DATE", "{0:dd/MMM/yyyy}") %>'  runat="server"></asp:Label></center>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Return Date"><ItemTemplate>
<center>
<%# Eval("STU_MOERETURNDATE", "{0:dd/MMM/yyyy}") %>
</center>
     
     
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="View"><ItemTemplate>
<asp:LinkButton id="lblView" runat="server" Text="View" CommandName="View" CommandArgument='<%# Eval("STU_ID") %>' OnClick="lblView_Click"></asp:LinkButton> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Print"><ItemTemplate>
<asp:LinkButton id="lblPrint" runat="server" Text="Print" ></asp:LinkButton> 
</ItemTemplate>
</asp:TemplateField>
</Columns>
                    <SelectedRowStyle CssClass="Green" />
        <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
        </asp:GridView>
   </div>
    </div>
      <div id="dropmenu1" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test1('LI');">
            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test1('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test1('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test1('NSW');">
                        <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test1('EW');">
                            <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test1('NEW');">
                                <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>
    <input id="h_Selected_menu_1" runat="server" type="hidden" value="LI_" /><input id="h_Selected_menu_2"
        runat="server" type="hidden" value="LI_" />
        
        <div id="dropmenu2" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test2('LI');">
            <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test2('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test2('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test2('NSW');">
                        <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test2('EW');">
                            <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test2('NEW');">
                                <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>
   <script type="text/javascript">

cssdropdown.startchrome("Div1")
cssdropdown.startchrome("Div2")

    </script>
</asp:Content>

