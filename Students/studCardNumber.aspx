<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studCardNumber.aspx.vb" Inherits="Students_studCardNumber" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Update ID Card Number</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table width="100%">
                                <tr>
                                    <td> <span class="field-label" >Select Academic Year</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="matters"> <span class="field-label" >Grade</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td> <span class="field-label" >Select Section</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server"  
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2" align="center">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr id="tr1" runat="server">
                                    <td colspan="4">&nbsp;
                                    </td>
                                </tr>
                                <tr id="tr2" runat="server"  >
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr id="tr3" runat="server">
                                    <td colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Card Number">
                                                   <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtRollNo" runat="server" Text='<%# Bind("STU_TCPOS_CARDNUM") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle   CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="tr4" runat="server">
                                    <td colspan="4" align="left" class="matters">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4" />

                                        <asp:HiddenField ID="hSCT_ID" runat="server" />
                                        <asp:HiddenField ID="hACD_ID" runat="server" />

                                        <asp:HiddenField ID="hGRD_ID" runat="server" />

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

