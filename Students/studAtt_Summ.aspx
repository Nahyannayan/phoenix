<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAtt_Summ.aspx.vb" Inherits="Students_stuEnquiryPrint" Title="::::PHOENIX:::: School Management System::::" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <!DOCTYPE html PUBLIC "-//W3C//Dtd Xhtml 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">

    <!DOCTYPE html PUBLIC "-//W3C//Dtd html 4.0 transitional//EN">
    <!-- saved from url=(0038)http://live.gemsoasis.com/printenq.php -->

    <title>GEMS OASIS - Online Student Administration System</title>
    <link href="/cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="/cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="/cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="/cssfiles/example.css" rel="stylesheet" type="text/css" />
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="Javascript" src="/FusionCharts/FusionCharts.js"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        function AddDetails(vid) {
            //    if($find("MyMPE"))
            //    {
            //  $find("MyMPE").dispose();
            //  }
            //        
            //  $create(AjaxControlToolkit.ModalPopupBehavior, {"BackgroundCssClass":"modalBackground","OnOkScript":"onPrvOk();","PopupControlID":"plPrev","dynamicServicePath":"/studAtt_Summ.aspx","id":"MyMPE"}, null, null, $get("dummyLink"));       
            //    $find("MyMPE").show(); 


            var sFeatures;
            sFeatures = "dialogWidth: 650px; ";
            sFeatures += "dialogHeight: 360px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //alert("hello")
            //result = window.showModalDialog("../Students/AttChart_drilldown.aspx?vid=" + vid, "", sFeatures)
            Popup("/Students/AttChart_drilldown.aspx?vid=" + vid)


        }

        function AddMainDetails(vid) {
            var sFeatures;
            sFeatures = "dialogWidth: 650px; ";
            sFeatures += "dialogHeight: 360px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            //alert(1);
            //result = window.showModalDialog("../Students/AttChart_Maindrilldown.aspx?vid=" + vid, "", sFeatures)
            Popup("/Students/AttChart_Maindrilldown.aspx?vid=" + vid)

        }
    </script>

    <table width="100%" align="center">
        <tbody>
            <tr>
                <td align="center" class="title" valign="middle">
                    <table width="100%" align="center">
                        <tbody>
                            <tr>
                                <td valign="top" align="left" width="15%">
                                    <asp:Image ID="imglogo" runat="server" Height="80" />
                                </td>
                                <td align="left" width="35%">
                                    <table width="100%" border="0" align="center">
                                        <tbody>

                                            <tr>
                                                <td align="left" valign="bottom"><span id="BsuName" runat="server"></span></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </td>
                                <td width="50%">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <span class="field-label">Report Title : </span>
                                            </td>
                                            <td align="left">
                                                <asp:Literal ID="ltHeader" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td align="left" colspan="3">
                                    <hr style="border-color: #00b300;" />
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </td>
            </tr>
        </tbody>
    </table>

    <table width="100%">
        <tbody>
            <tr>
                <td align="left" class="title" valign="middle">
                    <asp:ImageButton ID="imgbtnback" runat="server" AlternateText="Back" ImageUrl="~/Images/Misc/LEFT1.gif" />
                    <a href="javascript:window.print()">
                        <img src="../Images/Misc/print.gif" alt="Print this page" style="border-style: none;" /></a></td>
            </tr>
            <tr>
                <td align="left">

                    <table width="100%">

                        <tr>
                            <td align="left">
                                <asp:Literal ID="ltmain" runat="server"></asp:Literal><!-- <iframe id="Ifmain" height="400" src="../Students/AttChart_main.aspx" scrolling="no"  marginwidth="0px"  frameborder="0"  width="470"></iframe>--></td>

                            <td align="left">

                                <div align="center" style="margin-top: 0px;">
                                    <asp:Literal ID="ltsub" runat="server"></asp:Literal><!-- <iframe id="ifsub" height="400" src="../Students/AttChart_sub.aspx"  scrolling="no"  marginwidth="0px"  frameborder="0"  width="470"></iframe>-->
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <font style="font-weight: bold;">
                                        <asp:Literal ID="ltSchoolPer" runat="server"></asp:Literal>
                                    </font></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" valign="top">
                                <asp:GridView ID="gvAttQuick" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                    OnRowDataBound="gvAttQuick_RowDataBound" AllowPaging="True" CssClass="table table-bordered table-row" PageSize="10" UseAccessibleHeader="False">
                                    <RowStyle CssClass="griditem" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Grade">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("GRD_DES") %>'></asp:Label>
                                                <asp:Label ID="lblGroup" runat="server" Text='<%# bind("GROUP_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GRD_ID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGRD_ID" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Section">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("SCT_DES") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Strength">
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("TOT_STR") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Present">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("TOT_PRS") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Absent">
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("TOT_ABS") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Per(%)">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("TOT_PERC", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="Left" Font-Bold="true" />
                                </asp:GridView>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnPrint0" runat="server" Text="Print" CssClass="button" OnClick="btnPrint0_Click" />
                </td>
            </tr>
        </tbody>
    </table>
    <asp:HiddenField ID="hfENQ_ID" runat="server" />
    <asp:HiddenField ID="hfEQS_ID" runat="server" />
    <asp:HiddenField ID="HiddenBsuId" runat="server" />
    <asp:HiddenField ID="HiddenAcademicyear" runat="server" />


</asp:Content>
