Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studAttendance_M_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050100") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    bindAcademic_Year()
                    bindAcademic_Grade()
                    Call getStart_EndDate()
                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                        Call bindControl_ID()
                        Call disablecontrol()
                    ElseIf ViewState("datamode") = "add" Then
                        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        txtTo.Text = ViewState("ACD_ENDDT")
                        Call enablecontrol()
                    End If





                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub

    Sub bindControl_ID()
        Using readerAttendance_M As SqlDataReader = AccessStudentClass.GetAttendance_M(ViewState("viewid"))
            If readerAttendance_M.HasRows = True Then
                While readerAttendance_M.Read
                    ViewState("ATT_ACD_ID") = Convert.ToString(readerAttendance_M("ATT_ACD_ID"))
                    ViewState("ATT_TYPE") = Convert.ToString(readerAttendance_M("ATT_TYPE"))
                    ViewState("ATT_GRD_ID") = Convert.ToString(readerAttendance_M("ATT_GRD_ID"))
                    If IsDate(readerAttendance_M("ATT_FROMDT")) = True Then
                        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerAttendance_M("ATT_FROMDT")))).Replace("01/Jan/1900", "")
                    End If

                    If IsDate(readerAttendance_M("ATT_TODT")) = True Then
                        txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerAttendance_M("ATT_TODT")))).Replace("01/Jan/1900", "")
                    End If


                End While
            End If

            If ViewState("datamode") = "view" Then
                ddlAttType.ClearSelection()
                ddlAttType.Items.FindByValue(ViewState("ATT_TYPE")).Selected = True
            End If
            If ViewState("datamode") = "view" Then
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(ViewState("ATT_GRD_ID")).Selected = True
            End If


        End Using




    End Sub



    Sub getStart_EndDate()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query = "select ACD_StartDt,ACD_ENDDT  from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        If ds.Tables(0).Rows.Count > 0 Then
            ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_StartDt"))
            ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_ENDDT"))
        Else
            ViewState("ACD_STARTDT") = ""
            ViewState("ACD_ENDDT") = ""

        End If
    End Sub


    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If

        If txtTo.Text.Trim <> "" Then

            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>To Date format is Invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid To date</div>"
                    End If
                End If
            End If
        End If
        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function

    Function validateDateRange() As String
        Dim CommStr As String = String.Empty
        Dim Acd_SDate As Date = ViewState("ACD_STARTDT")
        Dim Acd_EDate As Date = ViewState("ACD_ENDDT")
        Dim From_date As Date = txtFrom.Text


        If (From_date >= Acd_SDate And From_date <= Acd_EDate) Then
            If txtTo.Text.Trim <> "" Then
                Dim To_Date As Date = txtTo.Text
                If Not ((To_Date >= Acd_SDate And To_Date <= Acd_EDate)) Then
                    CommStr = CommStr & "<div>From Date to To Date must be with in the academic year</div>"
                End If
            End If
        Else
            CommStr = CommStr & "<div>From Date  must be with in the academic year</div>"
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If
    End Function


    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True

            If ViewState("datamode") = "view" Then
                ddlAcdYear.ClearSelection()
                ddlAcdYear.Items.FindByValue(ViewState("ATT_ACD_ID")).Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim ds As New DataSet
            str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGrade.Items.Clear()
            ddlGrade.DataSource = ds.Tables(0)
            ddlGrade.DataTextField = "GRM_DISPLAY"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If serverDateValidate() = "0" Then
            If validateDateRange() = "0" Then
                str_err = calltransaction(errorMessage)
                If str_err = "0" Then
                    lblError.Text = "Record Saved Successfully"
                    Call disablecontrol()
                Else
                    lblError.Text = errorMessage
                End If
            End If
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim ATT_ID As String = "0"
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim GRD_ID As String = String.Empty
        Dim BSU_ID As String = Session("sBsuid")
        Dim FromDT As String = txtFrom.Text
        Dim ToDT As String = txtTo.Text
        Dim AttType As String = ddlAttType.Text

        If ddlGrade.SelectedIndex = -1 Then
            errorMessage = "Grade is not available!!!Record can not be saved."
            Return "1"
        Else

            GRD_ID = ddlGrade.SelectedItem.Value
        End If

        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Dim status As Integer



                    bEdit = False


                    status = AccessStudentClass.SaveAttendance_M(ATT_ID, ACD_ID, GRD_ID, BSU_ID, FromDT, ToDT, AttType, bEdit, transaction)


                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If

                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                    reset_state()
                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim status As Integer

                    ATT_ID = ViewState("viewid")

                    bEdit = True
                    status = AccessStudentClass.SaveAttendance_M(ATT_ID, ACD_ID, GRD_ID, BSU_ID, FromDT, ToDT, AttType, bEdit, transaction)

                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If

                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"

                    reset_state()
                    ViewState("viewid") = 0
                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        End If
    End Function

    Sub disablecontrol()
        ddlAcdYear.Enabled = False
        ddlGrade.Enabled = False
        ddlAttType.Enabled = False
        txtFrom.Enabled = False
        txtTo.Enabled = False
        imgCalendar.Visible = False
        ImageButton1.Visible = False
    End Sub
    Sub enablecontrol()
        ddlAcdYear.Enabled = True
        ddlGrade.Enabled = True
        ddlAttType.Enabled = True
        txtFrom.Enabled = True
        txtTo.Enabled = True
        imgCalendar.Visible = True
        ImageButton1.Visible = True
    End Sub
    Sub reset_state()

        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTo.Text = ViewState("ACD_ENDDT")
        bindAcademic_Year()

    End Sub



    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"

        reset_state()
        txtTo.Text = ViewState("ACD_ENDDT")
        Call enablecontrol()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Call enablecontrol()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call reset_state()
                disablecontrol()
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    'Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If ViewState("viewid") = 0 Then
    '        lblError.Text = "No records to delete"
    '        Exit Sub
    '    End If
    '    Dim str_err As String = String.Empty
    '    Dim DeleteMessage As String = String.Empty
    '    str_err = callDelete(DeleteMessage)
    '    If str_err = "0" Then
    '        ViewState("datamode") = "none"
    '        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '        lblError.Text = "Record Deleted Successfully"
    '        disablecontrol()
    '    Else
    '        lblError.Text = DeleteMessage
    '    End If
    'End Sub
    'Function callDelete(ByRef DeleteMessage As String) As Integer
    '    Dim transaction As SqlTransaction
    '    Dim Status As Integer
    '    'Delete  the  user

    '    Using conn As SqlConnection = ConnectionManger.GetOASISConnection
    '        transaction = conn.BeginTransaction("SampleTransaction")
    '        Try
    '            'delete needs to be modified based on the trigger

    '            Status = AccessStudentClass.DeleteAttendance_M(ViewState("viewid"), transaction)


    '            If Status = -1 Then
    '                callDelete = "1"
    '                DeleteMessage = "Record Does Not Exist "
    '                Return "1"

    '            ElseIf Status <> 0 Then
    '                callDelete = "1"
    '                DeleteMessage = "Error Occured While Deleting."
    '                Return "1"
    '            Else
    '                Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
    '                If Status <> 0 Then
    '                    callDelete = "1"
    '                    DeleteMessage = "Could not complete your request"
    '                    Return "1"

    '                End If
    '                ViewState("datamode") = "none"
    '                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    '                ViewState("viewid") = 0
    '                reset_state()
    '            End If
    '        Catch ex As Exception
    '            callDelete = "1"
    '            DeleteMessage = "Error Occured While Saving."
    '        Finally
    '            If callDelete <> "0" Then
    '                UtilityObj.Errorlog(DeleteMessage)
    '                transaction.Rollback()
    '            Else
    '                DeleteMessage = ""
    '                transaction.Commit()
    '            End If
    '        End Try

    '    End Using
    'End Function

    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call getStart_EndDate()
        Call bindAcademic_Grade()
    End Sub
End Class

