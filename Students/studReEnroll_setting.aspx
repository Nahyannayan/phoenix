﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studReEnroll_setting.aspx.vb" Inherits="Students_studReEnroll_setting" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
   <style>
      .darkPanelM
{
	width: 100%;
	height: 100%;
	position: fixed;
	left: 0%;
	top: 0%;
	background: url(../Images/dark.png) 0 0 !important;
	display: block;
	z-index: 10000;
}


.darkPanelMTop
{
	
	background-color: #f0f0f0;
	border: 1px solid #0092d7;
	margin: 100px auto 0px auto;
	overflow-x: hidden;
	overflow-y: hidden;
	padding: 0px 0px 0px 0px;
	width:50%;
}

.holderInner
{
	overflow: hidden;
}

.holderInner
{
	
	border: solid 1px #ccc;
	border-collapse: collapse;
	margin: 6px 6px 6px 6px;
	background-color: #FFFFFF;
	padding-top: 12px;
	text-align: left;
}

   </style>


     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Re-enrollment Settings
        </div>
        <div class="card-body">
            <div class="table-responsive ">
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    <table id="Table1" runat="server" align="center" border="0" 
        cellpadding="5"
        cellspacing="0" style="width: 100%">
        <tr>
            <td align="center" >

                <table id="Table2" runat="server" align="center" 
                    cellpadding="5" cellspacing="0" style="width: 100%; border-collapse: collapse">
                    

                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvOption" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="50" Width="100%">
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />

                                <Columns>


                                    <asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>

                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="School Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblschname" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" Width="300px"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Curriculum">

                                        <ItemTemplate>
                                            <asp:Label ID="lblcurri" runat="server" Text='<%# Bind("CLM_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Online" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOnline" runat="server" Text='<%# Bind("PLMS_bSHOW_MENU") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField CommandName="Online" ImageUrl="~\images\tick.gif" ButtonType="Image"
                                        HeaderText="Online">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:ButtonField>

                                    <asp:TemplateField HeaderText="Start Date">

                                        <ItemTemplate>
                                            <asp:Label ID="lblstdte" runat="server" Text='<%# Bind("RED_STARTDT","{0:dd/MMMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="End Date">

                                        <ItemTemplate>
                                            <asp:Label ID="lblenddte" runat="server" Text='<%# Bind("RED_ENDDT","{0:dd/MMMM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>


                                    <asp:ButtonField CommandName="Add" Text="Edit" HeaderText="Edit">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px"></ItemStyle>
                                    </asp:ButtonField>

                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbsuid" runat="server" Text='<%# Bind("BSU_ID") %>'></asp:Label>
                                            <asp:Label ID="lblacdid" runat="server" Text='<%# Bind("ACD_ID") %>'></asp:Label>
                                            <asp:Label ID="lblclmid" runat="server" Text='<%# Bind("ACD_CLM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>




                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle CssClass="gridheader_pop" Wrap="False" />

                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                </div>
            </div>
         </div>
    <asp:HiddenField ID="hd_bsu_id" runat="server" />
    <asp:HiddenField ID="hd_acd_id" runat="server" />
    <asp:HiddenField ID="hd_clm_id" runat="server" />
    <div id="Panel_MSG" runat="server" class="darkPanelM" visible="false">
        <div class="darkPanelMTop">
            <asp:Button ID="btn" type="button" runat="server"
                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                ForeColor="White" Text="X"></asp:Button>
            <div class="holderInner">
                <div align="CENTER">
                    <asp:Label ID="lblError1" runat="server" CssClass="error" EnableViewState="False"
                        Font-Size="10px"></asp:Label>
                </div>
                <br>
                <table align="center"  cellpadding="2" cellspacing="0"
                    style="width: 70%; border-collapse: collapse;">
                    <tr>
                        <td  align="left"><span class="field-label">School Name</span>
                        </td>
                        
                        <td  align="left">
                            <asp:DropDownList ID="ddlbsu" runat="server" AutoPostBack="true" Width="250px"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td  align="left"><span class="field-label">Curriculum</span>
                        </td>
                       
                        <td  align="left">
                            <asp:DropDownList ID="ddlCLM" runat="server" AutoPostBack="true" Width="250px"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td  align="center"><span class="field-label">Online</span>
                        </td>
                        
                        <td >
                            <asp:RadioButtonList ID="rdOnline" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                <asp:ListItem>Yes</asp:ListItem>
                                <asp:ListItem>No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td  align="center"><span class="field-label">Payment Source</span>
                        </td>
                        
                        <td >
                            <asp:CheckBox ID="chkOnlinePayment" runat="server" AutoPostBack="False"
                                Text="Online" Visible="True" />
                            <asp:CheckBox ID="chkSchoolPayment" runat="server" AutoPostBack="False"
                                Text="School" Visible="True" />
                        </td>
                    </tr>
                    <tr>
                        <td  align="center"><span class="field-label">Start Date</span>
                        </td>
                        
                        <td >
                            <asp:TextBox ID="txtFrom" runat="server" Width="100px"></asp:TextBox><asp:ImageButton
                                ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td  align="center"><span class="field-label">End Date</span>
                        </td>
                        
                        <td >
                            <asp:TextBox ID="txtTo" runat="server" Width="100px"></asp:TextBox><asp:ImageButton
                                ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td  align="center" colspan="3">
                            <asp:Button ID="btnOnline" Text="Save" runat="server" CssClass="button" />
                            <asp:Button ID="btnClose" Text="Close" runat="server" CssClass="button" />
                        </td>
                    </tr>

                </table>
            </div>

        </div>
    </div>
</asp:Content>

