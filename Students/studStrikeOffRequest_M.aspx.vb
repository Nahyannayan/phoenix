Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studStrikeOffRequest_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100252") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    txtName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    txtSEN.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                    GetData()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub



#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STK_ID,STK_LASTATTDATE,STK_RECREMARKS,STK_RECDATE," _
                                 & " isnull(STK_APRREMARKS,''),STK_APRDATE FROM " _
                                 & " STRIKEOFF_RECOMMEND_M WHERE STK_STU_ID=" + hfSTU_ID.Value _
                                 & " AND STK_bAPPROVED='TRUE' AND STK_CANCELDATE IS NULL"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            hfSTK_ID.Value = reader.GetValue(0)
            txtLast.Text = Format(reader.GetDateTime(1), "dd/MMM/yyyy")
            txtRecRemarks.Text = reader.GetString(2).Replace("''", "'")
            txtRec.Text = Format(reader.GetDateTime(3), "dd/MMM/yyyy")
            txtAprRemarks.Text = reader.GetString(4).Replace("''", "'")
            txtApr.Text = Format(reader.GetDateTime(5), "dd/MMM/yyyy")
        End While
        reader.Close()
    End Sub

    Function CheckStrikeOffDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value + " AND '" + txtStrike.Text + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The strikeoff date should be within the academic year"
            Return False
        Else

            If DateDiff("d", Date.Parse(txtStrike.Text), Date.Parse(txtLast.Text)) > 0 Then
                lblError.Text = "Strike of date has to be a date higher than the last attendance date"
                Return False
            End If
            Return True
        End If


        If Date.Parse(txtStrike.Text) < Now.Date Then
            lblError.Text = "Strike off date should not be less than current date"
            Exit Function
        End If

    End Function
    Sub GetDataFROMTCM(ByVal transaction As SqlTransaction)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCM_ID FROM TCM_M WHERE TCM_STU_ID=" & hfSTU_ID.Value & " AND TCM_TCSO='SO'AND TCM_bAPPROVED=0  AND TCM_CANCELDATE IS NULL"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(transaction, CommandType.Text, str_query)
        If reader.HasRows Then
            While reader.Read
                HF_TCM_ID.Value = reader.GetValue(0)
                'txtLast.Text = Format(reader.GetDateTime(1), "dd/MMM/yyyy")
                'txtRemarks.Text = reader.GetString(2).Replace("''", "'")
                'txtRec.Text = Format(reader.GetDateTime(3), "dd/MMM/yyyy")
                'txtRecRemarks.Text = reader.GetString(4).Replace("''", "'")
            End While
        Else
            HF_TCM_ID.Value = 0

        End If

        reader.Close()
    End Sub

    Function SaveData() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " EXEC saveSTRIKEOFFREQUEST " _
                                 & hfSTU_ID.Value + "," _
                                 & "'" + Format(Date.Parse(txtDocDate.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + txtRefNo.Text + "'," _
                                 & "'" + Format(Date.Parse(txtLast.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + Format(Date.Parse(txtStrike.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + txtRemarks.Text.Replace("'", "''") + "'"
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                GetDataFROMTCM(transaction)
                ViewState("TCMdatamode") = "add"

                UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hfSTU_ID.Value.ToString)
                UtilityObj.InsertAuditdetails(transaction, "edit", "STRIKEOFF_RECOMMEND_M", "STK_ID", "STK_STU_ID", "STK_ID=" + hfSTK_ID.Value.ToString, "SO")

                If HF_TCM_ID.Value > 0 Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + HF_TCM_ID.Value.ToString, "SO")
                    ViewState("TCMdatamode") = "edit"
                End If
                SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                If ViewState("TCMdatamode").ToString = "add" Then
                    GetDataFROMTCM(transaction)
                    UtilityObj.InsertAuditdetails(transaction, "add", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + HF_TCM_ID.Value.ToString)
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using


    End Function

    Function getPendingFee() As Double
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "exec OASIS_FEES.FEES.F_GetFeeForCollection " _
                                  & "'" + Format(Date.Parse(txtStrike.Text), "yyyy-MM-dd") + "'," _
                                  & hfSTU_ID.Value
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim fee As Double
        While reader.Read
            fee += reader.GetValue(4)
        End While
        Return fee
    End Function

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TCM_STU_ID", hfSTU_ID.Value)
        pParms(1) = New SqlClient.SqlParameter("@TCM_LASTATTDATE", Format(Date.Parse(txtLast.Text), "yyyy-MM-dd"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "TCSO_LastAttDate_Validate", pParms)
            While reader.Read
                Dim lstrRet = Convert.ToString(reader("Ret"))
                If lstrRet = "0" Then
                    lblError.Text = "Last date of attendance cannot be less  than the actual attended date"
                    Exit Sub
                End If
            End While
        End Using




        If CheckStrikeOffDate() = False Then
            Exit Sub
        End If
        If CheckDiscontinueRequestExists(hfSTU_ID.Value) = True Then
            lblError.Text = "Can't Save.Discontinue Request not Approved"
            Exit Sub
        End If
        If SaveData() = True Then
            Dim fee As Double = getPendingFee()
            lblError.Text = "The student " + txtName.Text + "'s name will be struck off from the records on " + txtStrike.Text + "."
            If fee <> 0 Then
                lblError.Text += vbCrLf + "Note:He/She has a pending fee payment amount of AED " + fee.ToString
            End If
            btnSave.Enabled = False
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
    Private Function CheckDiscontinueRequestExists(ByVal IntSTU_ID As Integer) As Boolean

        Dim bSTATUS As Boolean = False
        Dim cmd As SqlCommand
        Dim strSql As String
        Dim ds As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        strSql = "SELECT isnull(SSD_ID,0) SSD_ID FROM STUDENT_SERVICE_DISCONTINUE_REQ WHERE SSD_BSU_ID='" & Session("SBsuid") & "'" _
                  & " AND SSD_STU_ID='" & IntSTU_ID & "'" _
                  & " AND SSD_ACD_ID IN (SELECT STU_ACD_ID FROM STUDENT_M " _
                  & " WHERE STU_ID='" & IntSTU_ID & "'" _
                  & " AND STU_BSU_ID='" & Session("SBsuid") & "')" _
                  & " AND SSD_SVC_ID IS NOT NULL " _
                  & " AND SSD_bAPPROVE_STATUS NOT IN('A','R')" _
                  & " AND SSD_APPROV_DT IS NULL" _
                  & " AND SSD_SVC_ID<>'1'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)("SSD_ID") > 0 Then
                bSTATUS = True
            End If
        End If


        Return bSTATUS
    End Function
End Class
