<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="COVID_Relief_View.aspx.vb" Inherits="COVID_Relief_View" Title="Untitled Page" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style> 
        .page-item.active .page-link {
            background-color: #8dc24c !important;
            border-color: gray !important;
        }
    </style>
    <style type="text/css">
        .gridfont {
            font-size: 7pt !important;
           
        }
        table th, table td
        {
            padding: 0.1em !important;
        }

        tr.gridheader_pop {
            font-size: 125%;
            background-color: rgba(0, 0, 0, 0.075);
        }
        .griditem, .griditem_alternative {
            font-size:120%;
        }

        .modalWait {
            display: none;
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 )  50% 50% no-repeat;
        }
        .clsTDBorder {
        }
        
    </style>
    <script lang="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript"></script>
  
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Covid Relief Application Approval"></asp:Literal>
        </div>
        <div class="card-body">
            <div>
                 <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </div>
            <div class="mb-3">
                <table style="width: 100%;">
                    <tr>
                        <td align="left" style="width: 10%;"><span class="field-label">Status</span>
                        </td> 
                        <td align="left" width="20%"> 
                            <asp:DropDownList ID="ddlStatus" runat="server" class="form-control" OnSelectedIndexChanged="statusOnSelectedIndexChanged" AutoPostBack="true" />
                        </td> 
                        <td align="right" style="width: 10%;"><span class="field-label">School</span>
                        </td> 
                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddlSchool" runat="server" class="form-control" OnSelectedIndexChanged="schoolOnSelectedIndexChanged" AutoPostBack="true" />
                        </td> 
                    </tr>
                </table> 
            </div>
            <div>
                <table id="dataTableNew" class="table table-hover table-bordered  table-responsive" style="width: 100%">
                    <thead>
                       <tr style="background-color:lightgray;">
                            <th align="center" style="vertical-align:middle;">Sl No</th>
                            <th align="center" style="vertical-align:middle;">Action</th>
                            <th align="center" style="vertical-align:middle;">School</th>
                            <th align="center" style="vertical-align:middle;">Father Name</th>
                            <th align="center" style="vertical-align:middle;">Employment Type</th>

                            <th align="center" style="vertical-align:middle;">Relief Reason</th>
                            <th align="center" style="vertical-align:middle;">Company</th>
                            <th align="center" style="vertical-align:middle;">Salary</th>
                            <th align="center" style="vertical-align:middle;">Housing + Other</th>
                            <th align="center" style="vertical-align:middle;">Tuition Fee</th>

                            <th align="center" style="vertical-align:middle;">Mother Name</th>
                             <th align="center" style="vertical-align:middle;">Employment Type</th>
                             <th align="center" style="vertical-align:middle;">Relief Reason</th>
                             <th align="center" style="vertical-align:middle;">Company</th>
                             <th align="center" style="vertical-align:middle;">Salary</th>

                             <th align="center" style="vertical-align:middle;">Housing + Other</th>
                             <th align="center" style="vertical-align:middle;">Tuition Fee </th>
                             <th align="center" style="vertical-align:middle;">Due Previous Term</th>
                             <th align="center" style="vertical-align:middle;">Due AprJun</th>
                             <th align="center" style="vertical-align:middle;">Status</th>
                             <th align="center" style="vertical-align:middle;">Submitted Date / Approved By</th>  
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                 
            </div>
             
        </div>
    </div>
     
    <script>
        $(document).ready(function () {  
             //setFacnyBox();
            SetDataTable_New();
           
        })


         
         function SetDataTable_New() {
             //setFacnyBox();
            $("#dataTableNew").DataTable(
                {
                    "createdRow": function (row, data, dataIndex) {
                        $(row).css("background-color", data["BG_COLOR"]);
                        $(row).attr("title", data["COLOR_DESC"]);
                    },
                    "responsive": true,
                    "destroy": true,
                    "stateSave": true,                    
                    //"lengthMenu": [[30, 50, 100, -1], [30, 50, 100, "All"]],
                    "columnDefs": [ 
                        { 
                            "targets": 1,
                            "orderable": false
                        },
                        { 
                            "targets": 20,
                            "orderable": false
                        }
                    ],
                     "order": [[0, "asc" ]],
                    "language":
                    {
                        "processing": "Please wait...."
                    },
                    "processing": true,
                    "serverSide": true,
                    "ajax":
                    {
                        "url":'<%= Page.ResolveUrl("~/Students/COVID_Relief_View.aspx/GetData")%>',
                        "contentType": "application/json",
                        "type": "GET",
                        "dataType": "JSON",
                        "data": function (d) { 
                            return d;
                        },
                        "dataSrc": function (json) {
                            json.draw = json.d.draw;
                            json.recordsTotal = json.d.recordsTotal;
                            json.recordsFiltered = json.d.recordsFiltered;
                            json.data = json.d.data; 
                            var return_data = json;
                            return return_data.data;
                        },
                        "error": function (xhr, error, code) {
                            console.log(xhr);
                            console.log(code);
                        }
                    },
                    "columns": [ 
                        { "data": "SlNo" },
                        {
                            "render": function (data, type, full, meta) { 
                                var htm1 = "<div>";
                                //htm1 = htm1 + "<a class='fncyboxClass' onclick='OpenCovidPOPUP(1,"+full.RRH_ID_ENCR+");' href='#'>Details</a>";
                               // htm1 = htm1 + "<button  datahRef = 'COVID_Relief_StudentDetail.aspx?MainMnu_code=S050002&RRH_ID=" + full.RRH_ID_ENCR + "' onclick = 'OpenPopup(this)'>Click me</button>";

                                  
                                htm1 = htm1 + "<a href='#' datahRef='COVID_Relief_StudentDetail.aspx?MainMnu_code=S050002&RRH_ID="+full.RRH_ID_ENCR+"' onclick='return OpenPopup(this)'>Details</a>";
                                htm1 = htm1 + "<br/><a  href='#' datahRef='COVID_Relief_Attachments.aspx?MainMnu_code=S050002&RRH_ID="+full.RRH_ID_ENCR+"' onclick='return OpenPopup(this)'>Attachment</a>";
                                <%If ViewState("DISABLE_ACTION_STATUS") <> Session("Def_Status") Then %>
                                    <%If ViewState("COV_USER_ROLE") = "PRE" Then %>
                                       htm1 = htm1 + "<br/><a  href='#' datahRef='COVID_Relief_StatusUpdate.aspx?MainMnu_code=S050002&RRH_ID="+full.RRH_ID_ENCR+"' onclick='return OpenPopup(this)'>Action</a>";
                                    <% Else %>
                                    htm1 = htm1 + "<br/><a  href='#' datahRef='COVID_Relief_StatusUpdateFinance.aspx?MainMnu_code=S050002&RRH_ID="+full.RRH_ID_ENCR+"' onclick='return OpenPopup(this)'>Action</a>";
                                    <% End If %>
                                 <% End If %>
                                htm1 = htm1 + "<br/><a  href='#' datahRef='COVID_Relief_Call_Logs.aspx?MainMnu_code=S050002&RRH_ID="+full.RRH_ID_ENCR+"' onclick='return OpenPopup(this)'>Call a Log </a>"; 
                                htm1 = htm1 + "</div>";
                                return htm1;
                            }
                        },
                        { "data": "BSU" },
                        { "data": "FatherName" },
                        { "data": "FatherEmploymentType" },
                        { "data": "FatherReliefReason" },
                        { "data": "FatherCompanyName" },
                        { "data": "Father_Salary" },
                        { "data": "FatherSalary_Accomodation" },
                        { "data": "FatherSalary_TuitionFee" },
                        
                        //<th>Sl No</th>
                        //    <th>Action</th>
                        //    <th>School</th>
                        //    <th>Father Name</th>
                        //    <th>Employment Type</th>

                        //    <th>Relief Reason</th>
                        //    <th>Company</th>
                        //    <th>Salary</th>
                        //    <th>Housing + Other</th>
                        //    <th>Tuition Fee</th>

                        { "data": "MotherName" },
                        { "data": "MotherEmploymentType" },
                        { "data": "MotherReliefReason" },
                        { "data": "MotherCompanyName" },
                        { "data": "Mother_Salary" },
                        { "data": "MotherSalary_Accomodation" },
                        { "data": "MotherSalary_TuitionFee" },
                        { "data": "Due_PreviousTerm" },
                        { "data": "Due_AprJun" },
                        { "data": "ApprovalStatus" }, 
                        {
                            "render": function (data, type, full, meta) {
                                var htm1 = "<div>"+full.Submitted_Date+"/"+full.Submitted_BY+"</div>";
                                 return htm1;
                            }
                        }
                        //<th>Mother Name</th>
                        //     <th>Employment Type</th>
                        //     <th>Relief Reason</th>
                        //     <th>Company</th>
                        //     <th>Salary</th>

                        //     <th>Housing + Other</th>
                        //     <th>Tuition Fee </th>
                        //     <th>Due Previous Term</th>
                        //     <th>Due AprJun</th>
                        //     <th>Status</th>

                        //     <th>Submitted Date / Approved By</th>
                        
                        //{ "data": "Areas1" }, 
                        
                    ]
                });
        }


         

        function OpenPopup(obj) {  
            var url1 = $(obj).attr("datahRef"); 
            $.fancybox({
                type: 'iframe',
                href: url1,
                fitToView: true,
                autoSize: true,
                width: "70%",
                height:"70%",
                //closeClick: false,
                openEffect: 'none',
                closeEffect: 'none' ,
                'afterClose': function () {
                    //alert("sdkasldj");
                    SetDataTable_New();
                } 
            });

            return false;
        }
    </script>
    
</asp:Content>

