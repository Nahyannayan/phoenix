Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studEnquiryTransfer_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                hfEQS_ID.Value = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
              

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100045") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    BindBusinessunit()


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try



        Else


        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindBusinessunit()
        ddlBsu.Items.Clear()
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        str_query = "SELECT BSU_NAME,BSU_ID FROM BUSINESSUNIT_M WHERE  BSU_ENQ_GROUP=1 ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlBsu.DataSource = ds
        ddlBsu.DataTextField = "BSU_NAME"
        ddlBsu.DataValueField = "BSU_ID"
        ddlBsu.DataBind()
    End Sub

    Sub SaveData()
        Dim str_query As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                UtilityObj.InsertAuditdetails(transaction, "edit", "ENQUIRY_SCHOOLPRIO_S", "EQS_ID", "EQS_EQM_ENQID", "EQS_ID=" + hfEQS_ID.Value.ToString)
                str_query = "exec studEnquiryTransferRequest " + hfEQS_ID.Value.ToString + ",'" + ddlBsu.SelectedValue.ToString + "','" + Format(Date.Parse(txtTransferDate.Text), "yyyy-MM-dd") + "','" + txtRemarks.Text + "'"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "EQS_ID(" + hfEQS_ID.Value.ToString + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                lblError.Text = "Record Saved Successfully"
                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    '    Private Sub isTransferedGradeOpen()
    '        Dim str_query As String = "SELECT GRM_OPENONLINE FROM GRADE_BSU_M AS A"
    'INNER JOIN ENQUIRY_SCHOOLPRIO_S AS B ON
    'A.GRM_GRD_ID =B.EQS_GRD_ID AND 
    'A.GRM_SHF_ID=B.EQS_SHF_ID AND 
    'A.GRM_STM_ID=B.EQS_STM_ID AND
    '        A.GRM_ACY_ID = B.EQS_ACY_ID
    'WHERE GRM_BSU_ID='121013' AND EQS_ID=111
    '    End Sub
#End Region

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        SaveData()
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
End Class
