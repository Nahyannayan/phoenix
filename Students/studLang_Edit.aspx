<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studLang_Edit.aspx.vb" Inherits="Students_studShifts_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }
</script>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Language Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                     HeaderText="You must enter a value in the following fields:" 
                    ValidationGroup="groupM1" />
                <asp:RequiredFieldValidator ID="rfdescr" runat="server" ErrorMessage="Please enter the Language description" ControlToValidate="txtLangDescr" Display="None" ValidationGroup="groupM1" CssClass="error" ForeColor=""></asp:RequiredFieldValidator>   
          
            </td>
        </tr>
        <tr >
            <td align="left" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error"></asp:Label></td>
        </tr>
        <tr>
            <td >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                  <%--  <tr class="subheader_img">
                        <td align="left" colspan="9" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Language Master</span></font></td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%">
                          <span class="field-label">  Language Descr</span></td>
                   
                        <td align="left" colspan="2" width="50%">
                           <asp:TextBox ID="txtLangDescr" runat="server" TabIndex="3" ReadOnly="True"></asp:TextBox></td>

                        <td align="left" >
                            </td>

                    </tr>
                   
                     </table>
               </td>
        </tr>

     <tr>
            <td > &nbsp;</td>
        </tr>
       
        <tr>
            <td >
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
        </tr>
        <tr>
            <td class="matters" valign="bottom">
           <asp:HiddenField ID="hfLNG_ID" runat="server" />
                       </td>
        </tr>
    </table>

                
            </div>
        </div>
    </div>
</asp:Content>

