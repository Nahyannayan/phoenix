<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnquiryTransfer_View.aspx.vb" Inherits="Students_studEnquiryTransfer_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
                                          
    </script>
    <style>
        input {
            vertical-align: middle !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Transfer Enquiry
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <tr>

                        <td align="left" class="title"></td>
                    </tr>

                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                                    <td align="left" width="20%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" valign="top">
                                        <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30" PageSize="20">
                                            <Columns>


                                                <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("Eqs_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Enq No.">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqHeader" runat="server" CssClass="gridheader_text" Text="Enq No."></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnqid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnqid_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqId" runat="server" Text='<%# Bind("eqs_applno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Enq Date">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" CssClass="gridheader_text" Text="Enq.Date"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnq_Date" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnq_Date_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("eqm_enqdate", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Shift">
                                                    <HeaderTemplate>
                                                        Shift<br />
                                                        <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged" Width="70px">
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stream">
                                                    <HeaderTemplate>
                                                        Stream<br />
                                                        <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged" Width="70px">
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Applicant Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblApplName" runat="server" CssClass="gridheader_text" Text="Applicant Name"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtApplName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnAppl_Name" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnAppl_Name_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblApplName" runat="server" Text='<%# Bind("appl_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:ButtonField CommandName="view" HeaderText="Transfer" Text="Transfer">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle CssClass="Green" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                    type="hidden" value="=" /></td>
                    </tr>


                </table>
            </div>
        </div>
    </div>

</asp:Content>

