Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studEnq_Setting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim temp_ID As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If


            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100063") Then

                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling page right class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Call ENQ_OPT()
                ' Call CLM_LIST()
                Call BSU_YEAR()
                'Call bindAcademic_Grade()
                Call Enquiry_date()
                Call dllOption_Change()


                Call Enquiry_Option()
                If ViewState("datamode") = "view" Then
                    ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                    Call ENQ_SETTING_DATA()

                    disable_control()
                ElseIf ViewState("datamode") = "add" Then
                    txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    txtTo.Text = ViewState("ACD_ENDDT")
                    Call ReloadGrade()
                    enable_control()
                End If

             


            End If
        End If
    End Sub

    Sub ENQ_SETTING_DATA()

        Try
            Dim BSU_IDs As String = String.Empty
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT [EQS_ACD_ID],[EQS_CLM_ID],[EQS_GRD_ID],[EQS_FROMDT] ,[EQS_TODT]  ,[EQS_EQO_ID]," & _
" [EQS_BSU_IDS],[EQS_MSG1],[EQS_MSG2]  FROM [ENQUIRY_SETTINGS] where [EQS_ID] ='" & ViewState("viewid") & "'"

            Using readerENQ_SETTING_DATA As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, str_query)
                If readerENQ_SETTING_DATA.HasRows = True Then
                    While readerENQ_SETTING_DATA.Read

                        ViewState("EQS_ACD_ID") = Convert.ToString(readerENQ_SETTING_DATA("EQS_ACD_ID"))
                        ViewState("EQS_CLM_ID") = Convert.ToString(readerENQ_SETTING_DATA("EQS_CLM_ID"))
                        ViewState("EQS_GRD_ID") = Convert.ToString(readerENQ_SETTING_DATA("EQS_GRD_ID"))
                        ViewState("EQS_EQO_ID") = Convert.ToString(readerENQ_SETTING_DATA("EQS_EQO_ID"))
                        BSU_IDs = Convert.ToString(readerENQ_SETTING_DATA("EQS_BSU_IDS"))
                        'Setting date
                        If IsDate(readerENQ_SETTING_DATA("EQS_FROMDT")) = True Then
                            txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerENQ_SETTING_DATA("EQS_FROMDT"))))
                        End If
                        If IsDate(readerENQ_SETTING_DATA("EQS_TODT")) = True Then
                            txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerENQ_SETTING_DATA("EQS_TODT"))))
                        End If

                        txtMsg1.Text = Convert.ToString(readerENQ_SETTING_DATA("EQS_MSG1"))
                        txtMsg2.Text = Convert.ToString(readerENQ_SETTING_DATA("EQS_MSG2"))

                    End While
                End If

                'ddlCLM.ClearSelection()
                'ddlCLM.Items.FindByValue(ViewState("EQS_CLM_ID")).Selected = True
                ddlAcdYear.ClearSelection()
                ddlAcdYear.Items.FindByValue(ViewState("EQS_ACD_ID")).Selected = True
                If Not ddlOptions.Items.FindByValue(ViewState("EQS_EQO_ID")) Is Nothing Then
                    ddlOptions.ClearSelection()
                    ddlOptions.Items.FindByValue(ViewState("EQS_EQO_ID")).Selected = True
                End If

                Call ReloadGrade()
                If Not ddlGrade.Items.FindByValue(ViewState("EQS_GRD_ID")) Is Nothing Then
                    ddlGrade.ClearSelection()
                    ddlGrade.Items.FindByValue(ViewState("EQS_GRD_ID")).Selected = True
                End If


                Dim separator As String() = New String() {"|"}
                'Dim EI As Integer

                Dim strSplitArr As String() = BSU_IDs.Split(separator, StringSplitOptions.RemoveEmptyEntries)
                ' EI = (UBound(Filter(strSplitArr, "125016", True, CompareMethod.Text)) > -1)
                'If Array.IndexOf(strSplitArr, "126008") >= 0 Then
                '    MsgBox("Found")
                'Else
                '    MsgBox("Not found")
                'End If
                For Each arrStr As String In strSplitArr
                    chkSchool.Items.FindByValue(arrStr).Selected = True
                Next

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub ENQ_OPT()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim dsOpt As New DataSet
        Dim Enq_opt As String = "select EQO_ID,EQO_DESC from ENQUIRY_OPTION  order by EQO_DESC"
        dsOpt = SqlHelper.ExecuteDataset(conn, CommandType.Text, Enq_opt)
        ddlOptions.Items.Clear()
        ddlOptions.DataSource = dsOpt.Tables(0)
        ddlOptions.DataTextField = "EQO_DESC"
        ddlOptions.DataValueField = "EQO_ID"
        ddlOptions.DataBind()
    End Sub


    'Sub CLM_LIST()
    '    Dim conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim dsCLM As New DataSet
    '    dsCLM = AccessStudentClass.getBSU_CLM(Session("sBsuid"))
    '    ddlCLM.Items.Clear()
    '    ddlCLM.DataSource = dsCLM.Tables(0)
    '    ddlCLM.DataTextField = "CLM_DESCR"
    '    ddlCLM.DataValueField = "CLM_ID"
    '    ddlCLM.DataBind()

    'End Sub
    Sub BSU_YEAR()

        Dim CLM_ID As String = Session("CLM")
       
       
        Dim dsYear As New DataSet
        dsYear = AccessStudentClass.GetACD_Year_Enquiry(Session("sBsuid"), CLM_ID)
        ddlAcdYear.Items.Clear()
        ddlAcdYear.DataSource = dsYear.Tables(0)
        ddlAcdYear.DataTextField = "ACY_DESCR"
        ddlAcdYear.DataValueField = "ACD_ID"
        ddlAcdYear.DataBind()
    End Sub
    '   Sub bindAcademic_Grade()
    '       Try
    '           Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '           Dim str_Sql As String
    '           Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
    '           Dim ds As New DataSet
    '           str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
    '" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

    '           ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '           ddlGrade.Items.Clear()
    '           ddlGrade.DataSource = ds.Tables(0)
    '           ddlGrade.DataTextField = "GRM_DISPLAY"
    '           ddlGrade.DataValueField = "GRD_ID"
    '           ddlGrade.DataBind()

    '       Catch ex As Exception
    '           UtilityObj.Errorlog(ex.Message)
    '       End Try
    '   End Sub
    Sub Enquiry_date()
        Try
            Dim ACD_ID As String
            Dim CLM_ID As String = Session("CLM")
           
            If ddlAcdYear.SelectedIndex <> -1 Then
                ACD_ID = ddlAcdYear.SelectedItem.Value
            Else
                ACD_ID = "0"
            End If

            Using Date_reader As SqlDataReader = AccessStudentClass.GetACD_Year_Date(ACD_ID, CLM_ID)


                If Date_reader.HasRows = True Then
                    While Date_reader.Read


                        If IsDate(Date_reader("ACD_STARTDT")) = True Then
                            ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Date_reader("ACD_STARTDT")))).Replace("01/Jan/1900", "")
                        End If



                        If IsDate(Date_reader("ACD_ENDDT")) = True Then

                            txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Date_reader("ACD_ENDDT")))).Replace("01/Jan/1900", "")
                            ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Date_reader("ACD_ENDDT")))).Replace("01/Jan/1900", "")
                        End If
                    End While
                End If

            End Using
            txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, " Enquiry_date")
        End Try
    End Sub
    Sub Enquiry_Option()
        Dim ds1 As New DataSet
        'ds1.Tables(0).Rows.Clear()
        ds1 = AccessStudentClass.GetBSU_Option(ddlOptions.SelectedValue)
        chkSchool.Items.Clear()
        Dim row As DataRow

        For Each row In ds1.Tables(0).Rows

            chkSchool.Items.Add(New ListItem(row("bsu_name"), row("bsu_id")))

        Next
    End Sub
    Protected Sub ddlOptions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call Enquiry_Option()
        Call dllOption_Change()
        Call ReloadGrade()

    End Sub
    Sub dllOption_Change()
        If ddlOptions.SelectedItem.Value = 1 Then
            ltOption.Text = "Business Unit"

        ElseIf ddlOptions.SelectedItem.Value = 2 Then
            ltOption.Text = "GEMS Schools"

        ElseIf ddlOptions.SelectedItem.Value = 3 Then
            ltOption.Text = "Sister Concern Schools"

        ElseIf ddlOptions.SelectedItem.Value = 4 Then
            ltOption.Text = "GEMS Student"

        ElseIf ddlOptions.SelectedItem.Value = 5 Then
            ltOption.Text = "Female Applicant"
        ElseIf ddlOptions.SelectedItem.Value = 6 Then
            ltOption.Text = "Male Applicant"
        End If
    End Sub
    Sub ReloadGrade()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim ds As New DataSet
            If ddlOptions.SelectedItem.Value <> "4" Then
                str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
     " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "
            Else
                str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
                 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_GRD_ID not in('KG1','PK') and GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "
            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlGrade.Items.Clear()

                ddlGrade.DataSource = ds.Tables(0)
                ddlGrade.DataTextField = "GRM_DISPLAY"
                ddlGrade.DataValueField = "GRD_ID"
                ddlGrade.DataBind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlCLM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call BSU_YEAR()
        Call Enquiry_date()
    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call ReloadGrade()
        Call Enquiry_date()
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        Call chkSchool_State()
        enable_control()
        txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtTo.Text = ViewState("ACD_ENDDT")
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
   
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim chkSchool_check As Boolean = False
        Dim chkDropdown As Boolean = True

        If ddlAcdYear.SelectedIndex = -1 Then
            lblError.Text = "Academic year needs to be selected"
            chkDropdown = False
        ElseIf ddlGrade.SelectedIndex = -1 Then
            lblError.Text = "Record cannot be saved since Grade is not available for the selected Academic year"
            chkDropdown = False
        End If



        If chkDropdown = True Then
            If serverDateValidate() = "0" Then
                ' If validateDateRange() = "0" Then
                For Each item As ListItem In chkSchool.Items
                    If item.Selected = True Then
                        chkSchool_check = True
                    End If
                Next
                If ltOption.Text.Trim = "Female Applicant" Or ltOption.Text.Trim = "Male Applicant" Then
                    chkSchool_check = True
                End If

                If chkSchool_check = True Then
                    str_err = calltransaction(errorMessage)
                    If str_err = "0" Then
                        Call chkSchool_state()
                        disable_control()
                        lblError.Text = "Record Saved Successfully"



                    Else
                        lblError.Text = errorMessage
                    End If
                Else
                    lblError.Text = ltOption.Text & " not selected"
                End If


                'End If


            End If
        End If





    End Sub
    Sub enable_control()

        ddlAcdYear.Enabled = True
        ddlOptions.Enabled = True
        ddlGrade.Enabled = True
        txtFrom.Enabled = True
        txtTo.Enabled = True
        imgCalendar.Enabled = True
        ImageButton1.Enabled = True
        plChkBUnit.Enabled = True
        Panel1.Enabled = True
    End Sub
    Sub disable_control()

        ddlAcdYear.Enabled = False
        ddlGrade.Enabled = False
        ddlOptions.Enabled = False
        txtFrom.Enabled = False
        txtTo.Enabled = False
        imgCalendar.Enabled = False
        ImageButton1.Enabled = False
        plChkBUnit.Enabled = False
        Panel1.Enabled = False
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer
        Dim bEdit As Boolean
        Dim EQS_CLM_ID As String = String.Empty
        Dim EQS_ID As String = String.Empty
        If ViewState("datamode") = "add" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Dim status As Integer



                    bEdit = False

                    EQS_CLM_ID = Session("CLM")
                    status = SaveENQUIRY_SETTINGS(EQS_ID, EQS_CLM_ID, bEdit, transaction)


                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If

                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"


                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        ElseIf ViewState("datamode") = "edit" Then
            Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim status As Integer

                    EQS_ID = ViewState("viewid")
                    EQS_CLM_ID = ViewState("EQS_CLM_ID")
                    bEdit = True
                    status = SaveENQUIRY_SETTINGS(EQS_ID, EQS_CLM_ID, bEdit, transaction)


                    If status <> 0 Then
                        calltransaction = "1"
                        errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                        Return "1"
                    End If

                    ViewState("viewid") = "0"
                    ViewState("datamode") = "none"

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    calltransaction = "0"


                    ViewState("viewid") = 0
                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
        End If
    End Function



    Private Function SaveENQUIRY_SETTINGS(ByVal EQS_ID As String, ByVal EQS_CLM_ID As String, ByVal bEdit As Boolean, _
 ByVal transaction As SqlTransaction) As Integer



        Dim EQS_ACD_ID As String = ddlAcdYear.SelectedItem.Value

        Dim EQS_GRD_ID As String = ddlGrade.SelectedItem.Value
        Dim EQS_EQO_ID As String = ddlOptions.SelectedItem.Value
        Dim EQS_BSU_ID As String = Session("sBsuid")
        Dim EQS_FROMDT As String = txtFrom.Text
        Dim EQS_TODT As String = txtTo.Text
        Dim EQS_BSU_IDS As String = String.Empty
        Dim EQS_MSG1 As String = txtMsg1.Text.Trim
        Dim EQS_MSG2 As String = txtMsg2.Text.Trim
        '  Dim str_bsu_ids As New StringBuilder

        For Each item As ListItem In chkSchool.Items
            If item.Selected = True Then

                EQS_BSU_IDS += item.Value + "|"
                'str_bsu_ids.Append(item.Value)
                'str_bsu_ids.Append("|")
            End If
        Next

        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(14) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQS_ID", EQS_ID)
                pParms(1) = New SqlClient.SqlParameter("@EQS_ACD_ID", EQS_ACD_ID)
                pParms(2) = New SqlClient.SqlParameter("@EQS_CLM_ID", EQS_CLM_ID)
                pParms(3) = New SqlClient.SqlParameter("@EQS_GRD_ID", EQS_GRD_ID)
                pParms(4) = New SqlClient.SqlParameter("@EQS_BSU_ID", EQS_BSU_ID)
                pParms(5) = New SqlClient.SqlParameter("@EQS_FROMDT", EQS_FROMDT)
                pParms(6) = New SqlClient.SqlParameter("@EQS_TODT", EQS_TODT)
                pParms(7) = New SqlClient.SqlParameter("@EQS_EQO_ID", EQS_EQO_ID)
                pParms(8) = New SqlClient.SqlParameter("@EQS_BSU_IDS", EQS_BSU_IDS)
                pParms(9) = New SqlClient.SqlParameter("@bEdit", bEdit)
                pParms(10) = New SqlClient.SqlParameter("@EQS_MSG1", EQS_MSG1)
                pParms(11) = New SqlClient.SqlParameter("@EQS_MSG2", EQS_MSG2)
                pParms(12) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(12).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveENQUIRY_SETTINGS", pParms)
                Dim ReturnFlag As Integer = pParms(12).Value
                Return ReturnFlag

            End Using

        Catch ex As Exception
            Return 1
        End Try


    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        enable_control()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            disable_control()
            ViewState("viewid") = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                ' Call reset_state()

                Call chkSchool_state()
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        If ViewState("viewid") = 0 Then
            lblError.Text = "No records to delete"
            Exit Sub
        End If
        Dim str_err As String = String.Empty
        Dim DeleteMessage As String = String.Empty
        str_err = callDelete(DeleteMessage)
        If str_err = "0" Then
            ViewState("datamode") = "none"
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            lblError.Text = "Record Deleted Successfully"
            disable_control()


            Call chkSchool_state()
        Else
            lblError.Text = DeleteMessage
        End If

    End Sub
    Sub chkSchool_state()
        ' ddlCLM.SelectedIndex = 0

        Call BSU_YEAR()
        ddlOptions.SelectedIndex = 0
        For Each item As ListItem In chkSchool.Items
            If item.Selected = True Then
                item.Selected = False
            End If
        Next
    End Sub

    Function callDelete(ByRef DeleteMessage As String) As Integer
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                'delete needs to be modified based on the trigger

                Status = AccessStudentClass.DeleteENQ_SETTINGs(ViewState("viewid"), transaction)


                If Status <> 0 Then
                    callDelete = "1"
                    DeleteMessage = "Error Occured While Deleting."
                    Return "1"

                Else
                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("viewid"), "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        callDelete = "1"
                        DeleteMessage = "Could not complete your request"
                        Return "1"

                    End If
                    ViewState("datamode") = "none"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("viewid") = 0

                End If
            Catch ex As Exception
                callDelete = "1"
                DeleteMessage = "Error Occured While Saving."
            Finally
                If callDelete <> "0" Then
                    UtilityObj.Errorlog(DeleteMessage)
                    transaction.Rollback()
                Else
                    DeleteMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty

        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)

                If Not IsDate(dateTime1) Then

                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If

        If txtTo.Text.Trim <> "" Then

            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then

                CommStr = CommStr & "<div>To Date format is Invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid To date</div>"
                    End If
                End If
            End If
        End If
        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If


    End Function

    Function validateDateRange() As String
        Dim CommStr As String = String.Empty
        Dim Acd_SDate As Date = ViewState("ACD_STARTDT")
        Dim Acd_EDate As Date = ViewState("ACD_ENDDT")
        Dim From_date As Date = txtFrom.Text


        If (From_date >= Acd_SDate And From_date <= Acd_EDate) Then
            If txtTo.Text.Trim <> "" Then
                Dim To_Date As Date = txtTo.Text
                If Not ((To_Date >= Acd_SDate And To_Date <= Acd_EDate)) Then
                    CommStr = CommStr & "<div>From Date to To Date must be with in the academic year</div>"
                End If
            End If
        Else
            CommStr = CommStr & "<div>From Date  must be with in the academic year</div>"
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If
    End Function



End Class
