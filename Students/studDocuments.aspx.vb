Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studDocuments
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ''HF_stuid.Value = Request.QueryString("eqsid")
                ''Session("DB_Stu_ID") = HF_stuid.Value
                ''ViewState("stu_id") = Session("DB_Stu_ID").ToString


                HF_stuid.Value = Encr_decrData.Decrypt(Request.QueryString("StuId").Replace(" ", "+"))
                ViewState("STU_BSU_ID") = Encr_decrData.Decrypt(Request.QueryString("BSU").Replace(" ", "+"))
                BindGridStuPendingDoc() 
                BindGridStuDocuments()
                pnl_UploadDoc.Visible = False
            End If
        Catch ex As Exception

        End Try


    End Sub
    Protected Function GetNavigateUrl1(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('http://school.gemsoasis.com/oasisfiles/hr_files/enquiry/1.gif', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ")
    End Function
    Private Sub BindGridStuPendingDoc()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", HF_stuid.Value)
            PARAM(1) = New SqlParameter("@INFO_TYPE", "PEND_DOC")
            PARAM(2) = New SqlParameter("@STG_ID", "10")

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_STU_DOCUMENTS", PARAM)
            gv_StuPendingDoc.DataSource = dsDetails
            gv_StuPendingDoc.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindGridStuDocuments()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", HF_stuid.Value)
            PARAM(1) = New SqlParameter("@INFO_TYPE", "STUD_DOC")

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_STU_DOCUMENTS", PARAM)
            gv_StuDocuments.DataSource = dsDetails
            gv_StuDocuments.DataBind()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub lnkUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hf_doc_id As HiddenField = sender.parent.FindControl("hf_doc_id")
        Dim hf_DOC_DESCR As HiddenField = sender.parent.FindControl("hf_DOC_DESCR")
        lblDoc.Text = hf_DOC_DESCR.Value
        ViewState("doc_id") = hf_doc_id.Value
        pnl_UploadDoc.Visible = True
        btnuploadDoc.Visible = True
        btnuploadChangeDoc.Visible = False
    End Sub

    Protected Sub lbtnUploadDocClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnUploadDocClose.Click
        pnl_UploadDoc.Visible = False
    End Sub

    Protected Sub btnCancelResetPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelupload.Click
        pnl_UploadDoc.Visible = False
    End Sub

    Protected Sub btnuploadDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnuploadDoc.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        lblerror.Text = ""
        Dim con As SqlConnection = New SqlConnection(str_conn)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim Stu_Id As String = HF_stuid.Value
        Try
            If FileUpload_stud.HasFile Then
                Dim StudentDocumentPath As String = WebConfigurationManager.AppSettings("StudentDocumentPath").ToString() + ViewState("STU_BSU_ID")
                Dim StudentDocumentPathVirtual As String = WebConfigurationManager.AppSettings("StudentDocumentPathVirtual").ToString() + ViewState("STU_BSU_ID")
                Dim filen As String()
                Dim FileName As String = FileUpload_stud.PostedFile.FileName
                filen = FileName.Split("\")
                FileName = filen(filen.Length - 1)

                Dim extention = Path.GetExtension(FileUpload_stud.PostedFile.FileName)
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTION", "ADD")
                pParms(1) = New SqlClient.SqlParameter("@STU_ID", Stu_Id)
                pParms(2) = New SqlClient.SqlParameter("@STG_ID", "10")
                pParms(3) = New SqlClient.SqlParameter("@DOC_ID", ViewState("doc_id"))
                pParms(4) = New SqlClient.SqlParameter("@FILE_EXTENTION", extention)
                pParms(5) = New SqlClient.SqlParameter("@BVERIFIED_BYSCHOOL", IIf(chb_bverified.Checked = True, True, False))
                pParms(6) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
                pParms(7) = New SqlClient.SqlParameter("@StudentDocumentPathVirtual", StudentDocumentPathVirtual)
                pParms(8) = New SqlClient.SqlParameter("@Audit_Filename", SqlDbType.VarChar, 1000)
                pParms(8).Direction = ParameterDirection.Output

                Dim DOC_FILENAME As String = SqlHelper.ExecuteScalar(sqltran, CommandType.StoredProcedure, "dbo.SAVE_DELETE_STUDENT_DOCUPLOAD", pParms)
                StudentDocumentPath = StudentDocumentPath + "\" + Stu_Id
                Dim isExists As Boolean = System.IO.Directory.Exists(StudentDocumentPath)

                If Not isExists Then
                    System.IO.Directory.CreateDirectory(StudentDocumentPath)
                End If

                FileUpload_stud.SaveAs(StudentDocumentPath + "\" + DOC_FILENAME)
                sqltran.Commit()
                lblerror.Text = "Uploaded sucessfully"
            End If
        Catch ex As Exception
            sqltran.Rollback()
            lbluploadError.Text = ex.Message
        End Try
        BindGridStuDocuments()
        BindGridStuPendingDoc()
        pnl_UploadDoc.Visible = False
    End Sub

    Protected Sub lnkDoc_Verify_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim HF_SDU_ID As HiddenField = sender.parent.FindControl("HF_SDU_ID")
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", "Verified")
            pParms(1) = New SqlClient.SqlParameter("@SDU_ID", HF_SDU_ID.Value)
            pParms(2) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            pParms(3) = New SqlClient.SqlParameter("@Audit_Filename", SqlDbType.VarChar, 1000)
            pParms(3).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "dbo.SAVE_DELETE_STUDENT_DOCUPLOAD", pParms)
            BindGridStuDocuments()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cb_collected_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim HF_SDU_ID As HiddenField = sender.parent.FindControl("HF_SDU_ID")
            'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            'Dim pParms(3) As SqlClient.SqlParameter
            'pParms(0) = New SqlClient.SqlParameter("@OPTION", "Collected")
            'pParms(1) = New SqlClient.SqlParameter("@SDU_ID", HF_SDU_ID.Value)
            'pParms(2) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "dbo.SAVE_DELETE_STUDENT_DOCUPLOAD", pParms)
            'BindGridStuDocuments()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub imgDocCollected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim HF_SDU_ID As HiddenField = sender.parent.FindControl("HF_SDU_ID")
            Dim hf_doc_id As HiddenField = sender.parent.FindControl("hf_doc_id")
            Dim HF_SDU_BCOLLECTED As HiddenField = sender.parent.FindControl("HF_SDU_BCOLLECTED")

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTION", "Collected")
            pParms(1) = New SqlClient.SqlParameter("@STU_ID", HF_stuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@SDU_ID", HF_SDU_ID.Value)
            pParms(3) = New SqlClient.SqlParameter("@DOC_ID", hf_doc_id.Value)
            pParms(4) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
            pParms(5) = New SqlClient.SqlParameter("@SDU_BCOLLECTED", IIf(HF_SDU_BCOLLECTED.Value = "False", "True", "False"))
            pParms(6) = New SqlClient.SqlParameter("@Audit_Filename", SqlDbType.VarChar, 1000)
            pParms(6).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "dbo.SAVE_DELETE_STUDENT_DOCUPLOAD", pParms)
            BindGridStuPendingDoc()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lnkchangeDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hf_doc_id As HiddenField = sender.parent.FindControl("hf_doc_id")
        Dim hf_DOC_DESCR As HiddenField = sender.parent.FindControl("hf_DOC_DESCR")
        lblDoc.Text = hf_DOC_DESCR.Value
        ViewState("doc_id") = hf_doc_id.Value
        pnl_UploadDoc.Visible = True
        btnuploadDoc.Visible = False
        btnuploadChangeDoc.Visible = True
    End Sub

    Protected Sub btnuploadChangeDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnuploadChangeDoc.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        lblerror.Text = ""
        Dim con As SqlConnection = New SqlConnection(str_conn)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim Stu_Id As String = HF_stuid.Value
        Try
            If FileUpload_stud.HasFile Then
                Dim StudentDocumentPath As String = WebConfigurationManager.AppSettings("StudentDocumentPath").ToString() + ViewState("STU_BSU_ID")
                Dim StudentDocumentPathVirtual As String = WebConfigurationManager.AppSettings("StudentDocumentPathVirtual").ToString() + ViewState("STU_BSU_ID")
                Dim filen As String()
                Dim FileName As String = FileUpload_stud.PostedFile.FileName
                filen = FileName.Split("\")
                FileName = filen(filen.Length - 1)

                Dim extention = Path.GetExtension(FileUpload_stud.PostedFile.FileName)
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTION", "ADD")
                pParms(1) = New SqlClient.SqlParameter("@STU_ID", Stu_Id)
                pParms(2) = New SqlClient.SqlParameter("@STG_ID", "10")
                pParms(3) = New SqlClient.SqlParameter("@DOC_ID", ViewState("doc_id"))
                pParms(4) = New SqlClient.SqlParameter("@FILE_EXTENTION", extention)
                pParms(5) = New SqlClient.SqlParameter("@BVERIFIED_BYSCHOOL", IIf(chb_bverified.Checked = True, True, False))
                pParms(6) = New SqlClient.SqlParameter("@USERNAME", Session("sUsr_name"))
                pParms(7) = New SqlClient.SqlParameter("@StudentDocumentPathVirtual", StudentDocumentPathVirtual)
                pParms(8) = New SqlClient.SqlParameter("@Audit_Filename", SqlDbType.VarChar, 1000)
                pParms(8).Direction = ParameterDirection.Output
                Dim DOC_FILENAME As String = SqlHelper.ExecuteScalar(sqltran, CommandType.StoredProcedure, "dbo.SAVE_DELETE_STUDENT_DOCUPLOAD", pParms)
                StudentDocumentPath = StudentDocumentPath + "\" + Stu_Id
                Dim Audit_Filename As String = pParms(8).Value

                Dim isExists As Boolean = System.IO.Directory.Exists(StudentDocumentPath)
                If Not isExists Then
                    System.IO.Directory.CreateDirectory(StudentDocumentPath)
                End If
                'myString.Substring(2, 1)
                Dim audit_file = Audit_Filename.Substring(Audit_Filename.IndexOf("_") + 1)
                Dim DocumentExists As Boolean = System.IO.File.Exists(StudentDocumentPath + "\" + audit_file)
                If DocumentExists Then
                    If (Not System.IO.Directory.Exists(StudentDocumentPath + "\Audit")) Then
                        System.IO.Directory.CreateDirectory(StudentDocumentPath + "\Audit")
                    End If
                    System.IO.File.Move(StudentDocumentPath + "\" + audit_file, StudentDocumentPath + "\Audit\" + Audit_Filename)
                End If


                FileUpload_stud.SaveAs(StudentDocumentPath + "\" + DOC_FILENAME)
                sqltran.Commit()
                lblerror.Text = "Uploaded sucessfully"
            End If
        Catch ex As Exception
            sqltran.Rollback()
            lbluploadError.Text = ex.Message
        End Try
        BindGridStuDocuments()
        BindGridStuPendingDoc()
        pnl_UploadDoc.Visible = False
    End Sub
End Class
