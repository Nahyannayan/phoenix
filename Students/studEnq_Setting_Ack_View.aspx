<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnq_Setting_Ack_View.aspx.vb" Inherits="Students_studEnq_Setting_Ack_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>




    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Enquiry Acknowledgement Setting"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">



                <table align="center" width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Select Academic year</span> </td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlAca_Year" runat="server"
                                AutoPostBack="True"   OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged"
                                >
                            </asp:DropDownList></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <table id="tbl_test" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0">
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:GridView ID="gvOnlineEnq_Ack" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                 <asp:Label ID="lblGRADE_H" runat="server" Text="GRADE" CssClass="gridheader_text" __designer:wfdid="w13"></asp:Label>
                                                                     <br />
                                                                       <asp:TextBox ID="txtGRM_DIS" runat="server" Width="75%" __designer:wfdid="w14"></asp:TextBox>
                                                                       <asp:ImageButton ID="btnSearchGRD" OnClick="btnSearchGRD_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w15"></asp:ImageButton>
                                                                       
                                                      
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRM_DIS" runat="server" Text='<%# Bind("GRM_DIS") %>'  ></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Message">
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" Text='<%# Bind("EAS_TEXT_MAIN") %>' ID="TextBox2"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%# Bind("EAS_TEXT_MAIN") %>' ID="Label2"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Show Message">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgMain" runat="server" ImageUrl='<%# returnpath(Container.DataItem("Main_bShow")) %>' />
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No. of Doc">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDOC_CountH" runat="server" Text='<%# Bind("DOC_Count") %>' __designer:wfdid="w4"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Doc List Show">
                                                    <EditItemTemplate>
                                                        <asp:TextBox runat="server" Text='<%# Bind("Sub_bShow") %>' ID="TextBox3"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgSub" runat="server" ImageUrl='<%# returnpath(Container.DataItem("Sub_bShow")) %>' />
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" __designer:wfdid="w6"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View" __designer:wfdid="w7"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server" __designer:wfdid="w5">View</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EAS_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEAS_ID" runat="server" Text='<%# Bind("EAS_ID") %>' __designer:wfdid="w10"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

