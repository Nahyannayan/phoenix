﻿
'Partial Class Students_COVID_Relief_StatusUpdate
'    Inherits System.Web.UI.Page

'End Class

Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_COVID_Relief_StatusUpdate
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                ViewState("RRH_ID") = Encr_decrData.Decrypt(Request.QueryString("RRH_ID").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                gridbind(ViewState("RRH_ID"))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
    End Sub



    Public Sub gridbind(ByRef RRH_ID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@RRH_ID", RRH_ID)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_RELIEF_STATUS_LIST", param)
            If ds.Tables(0).Rows.Count > 0 Then

                ddlStatus.DataSource = ds.Tables(0)
                ddlStatus.DataBind()

                ddlStatus.DataSource = ds
                ddlStatus.DataTextField = "CSM_DESCRIPTION"
                ddlStatus.DataValueField = "CMS_CODE"
                ddlStatus.SelectedIndex = 0
                ddlStatus.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Public Sub btnProceed_Click(sender As Object, e As EventArgs) Handles btnProceed.Click
        If (txtComments.Text <> "") Then
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try


                Dim retVal As Int64


                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@RRH_ID", SqlDbType.Int)
                pParms(0).Value = ViewState("RRH_ID")
                pParms(1) = New SqlClient.SqlParameter("@RRH_STATUS", SqlDbType.VarChar)
                pParms(1).Value = ddlStatus.SelectedValue
                pParms(2) = New SqlClient.SqlParameter("@RRC_LOG_USR_ID", SqlDbType.VarChar)
                pParms(2).Value = Session("sUsr_name")
                pParms(3) = New SqlClient.SqlParameter("@RRC_COMMENTS", SqlDbType.VarChar)
                pParms(3).Value = txtComments.Text
                pParms(4) = New SqlClient.SqlParameter("@RetVal", SqlDbType.BigInt)
                pParms(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "COV.UPDATE_RELIEF_RER_STATUS", pParms)
                retVal = pParms(4).Value

                If (retVal = 0) Then
                    stTrans.Commit()

                    errLbl.Text = "Data Saved Sucessfully"
                Else
                    stTrans.Rollback()

                    errLbl.Text = "Unable to save the data"
                End If






            Catch ex As Exception
                stTrans.Rollback()

            Finally

            End Try
        Else
            errLbl.Text = "Please enter the comments"

        End If

    End Sub
End Class

