﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports Telerik.Web.UI

Partial Class Students_studLibLabClearance_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050375") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    '   Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

                BindGrade()
                BindLibrarian()
                BindLabAssit()
                GridBind()
                If gvStudGrade.Rows.Count = 0 Then
                    trGrid.Visible = False
                End If
                gvStudGrade.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If


    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID WHERE GRM_ACD_ID=" + Session("CURRENT_ACD_ID") _
                                 & " ORDER BY GRD_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_ID"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ROW_NUMBER()OVER(ORDER BY SCL_GRD_ID)-1 AS UNIQUEID,SCL_ID,SCL_BSU_ID,SCL_GRD_ID,SCL_LIB_EMP_ID,SCL_LIB_EMAIL,SCL_LAB_EMP_ID,SCL_LAB_EMAIL," _
                                & " ISNULL(A.EMP_FNAME,'')+' '+ISNULL(A.EMP_MNAME,'')+' '+ISNULL(A.EMP_LNAME,'') AS EMP_NAME_LIBRARIAN," _
                                & " ISNULL(B.EMP_FNAME,'')+' '+ISNULL(B.EMP_MNAME,'')+' '+ISNULL(B.EMP_LNAME,'') AS EMP_NAME_LAB" _
                                & " FROM TC_LIB_LAB_CLEARANCE_M LEFT OUTER JOIN EMPLOYEE_M AS A ON SCL_LIB_EMP_ID=A.EMP_ID" _
                                & " LEFT OUTER JOIN EMPLOYEE_M AS B ON SCL_LAB_EMP_ID=B.EMP_ID WHERE " _
                                & " SCL_BSU_ID='" + Session("SBSUID") + "' ORDER BY SCL_GRD_ID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStudGrade.DataSource = ds
        gvStudGrade.DataBind()
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "EXEC saveTC_LIB_LAB_CLEARANCE_M" _
                                & " @SCL_BSU_ID='" + Session("sbsuid") + "'," _
                                & " @SCL_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                                & " @SCL_LIB_EMP_ID=" + Val(hfEMP_ID_LIB.Value).ToString + "," _
                                & " @SCL_LIB_EMAIL='" + txtLibEmail.Text + "'," _
                                & " @SCL_LAB_EMP_ID=" + Val(hfEMP_ID_LAB.Value).ToString + "," _
                                & " @SCL_LAB_EMAIL='" + txtLabEmail.Text + "'," _
                                & " @MODE='" + ViewState("datamode") + "'," _
                                & " @SCL_LIB_EMP_ID_ORG='" + hfEMP_ID_LIB_ORG.Value + "', " _
                                & " @SCL_LAB_EMP_ID_ORG='" + hfEMP_ID_LAB_ORG.Value + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
        'txtLabAsst.Text = ""
        txtLabEmail.Text = ""
        txtLibEmail.Text = ""
        'txtLibrarian.Text = ""
        trGrid.Visible = True
    End Sub
    Protected Sub ddlLibrarian_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        hfEMP_ID_LIB.Value = ddlLibrarian.SelectedValue.ToString
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT EMP_ID,ISNULL(EMPNO,'') EMPNO, ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')  AS EMP_NAME, " _
                     & " ISNULL(EMD_EMAIL,'') EMD_EMAIL FROM EMPLOYEE_M A INNER JOIN EMPLOYEE_D ON EMP_ID=EMD_EMP_ID WHERE EMP_BACTIVE=1 AND EMP_BSU_ID='" & Session("SBSUID") & "' AND EMP_ID=" & ddlLibrarian.SelectedValue & " order by EMP_FNAME  "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        txtLibEmail.Text = ds.Tables(0).Rows(0)("EMD_EMAIL").ToString()


    End Sub

    Sub BindLibrarian()
        'lstGrade.Items.Clear()
        ddlLibrarian.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT EMP_ID,ISNULL(EMPNO,'') EMPNO, ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')  AS EMP_NAME, " _
                     & " ISNULL(EMD_EMAIL,'') EMD_EMAIL FROM EMPLOYEE_M A INNER JOIN EMPLOYEE_D ON EMP_ID=EMD_EMP_ID WHERE EMP_BACTIVE=1 AND EMP_BSU_ID='" & Session("SBSUID") & "' order by EMP_FNAME  "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'lstGrade.DataSource = ds
        'lstGrade.DataTextField = "GRM_DISPLAY"
        'lstGrade.DataValueField = "GRM_GRD_ID"
        'lstGrade.DataBind()
        ddlLibrarian.DataSource = ds
        ddlLibrarian.DataTextField = "EMP_NAME"
        ddlLibrarian.DataValueField = "EMP_ID"
        ddlLibrarian.DataBind()

    End Sub

    Protected Sub ddlLabAsst_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        hfEMP_ID_LAB.Value = ddlLabAsst.SelectedValue
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT EMP_ID,ISNULL(EMPNO,'') EMPNO, ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')  AS EMP_NAME, " _
                     & " ISNULL(EMD_EMAIL,'') EMD_EMAIL FROM EMPLOYEE_M A INNER JOIN EMPLOYEE_D ON EMP_ID=EMD_EMP_ID WHERE EMP_BACTIVE=1 AND EMP_BSU_ID='" & Session("SBSUID") & "' AND EMP_ID=" & ddlLabAsst.SelectedValue & " order by EMP_FNAME  "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        txtLabEmail.Text = ds.Tables(0).Rows(0)("EMD_EMAIL").ToString()
    End Sub

    Sub BindLabAssit()
        'lstGrade.Items.Clear()
        ddlLabAsst.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT EMP_ID,ISNULL(EMPNO,'') EMPNO, ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'')  AS EMP_NAME, " _
                     & " ISNULL(EMD_EMAIL,'') EMD_EMAIL FROM EMPLOYEE_M A INNER JOIN EMPLOYEE_D ON EMP_ID=EMD_EMP_ID WHERE EMP_BACTIVE=1 AND EMP_BSU_ID='" & Session("SBSUID") & "' order by EMP_FNAME  "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'lstGrade.DataSource = ds
        'lstGrade.DataTextField = "GRM_DISPLAY"
        'lstGrade.DataValueField = "GRM_GRD_ID"
        'lstGrade.DataBind()
        ddlLabAsst.DataSource = ds
        ddlLabAsst.DataTextField = "EMP_NAME"
        ddlLabAsst.DataValueField = "EMP_ID"
        ddlLabAsst.DataBind()

    End Sub

  
    Protected Sub gvStudGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudGrade.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim selectedRow As GridViewRow = DirectCast(gvStudGrade.Rows(index), GridViewRow)
        With selectedRow

            Dim lblGrdId As Label = .FindControl("lblGrdId")
            Dim lblLibrarian As Label = .FindControl("lblLibrarian")
            Dim lblLibEmail As Label = .FindControl("lblLibEmail")
            Dim lblLabAsst As Label = .FindControl("lblLabAsst")
            Dim lblLabEmail As Label = .FindControl("lblLabEmail")
            Dim lblLibEmpId As Label = .FindControl("lblLibEmpId")
            Dim lblLabEmpId As Label = .FindControl("lblLabEmpId")



            If e.CommandName = "edit" Then
                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(lblGrdId.Text).Selected = True
                hfEMP_ID_LAB.Value = lblLabEmpId.Text
                hfEMP_ID_LIB.Value = lblLibEmpId.Text
                hfEMP_ID_LAB_ORG.Value = lblLabEmpId.Text
                hfEMP_ID_LIB_ORG.Value = lblLibEmpId.Text
                'txtLabAsst.Text = lblLabAsst.Text
                'txtLibrarian.Text = lblLibrarian.Text
                txtLabEmail.Text = lblLabEmail.Text
                txtLibEmail.Text = lblLibEmail.Text
                ViewState("datamode") = "edit"
            ElseIf e.CommandName = "delete" Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_query As String = "EXEC saveTC_LIB_LAB_CLEARANCE_M" _
                                        & " @SCL_BSU_ID='" + Session("sbsuid") + "'," _
                                        & " @SCL_GRD_ID='" + lblGrdId.Text + "'," _
                                        & " @SCL_LIB_EMP_ID=" + lblLibEmpId.Text + "," _
                                        & " @SCL_LIB_EMAIL='" + lblLibEmail.Text + "'," _
                                        & " @SCL_LAB_EMP_ID=" + lblLabEmpId.Text + "," _
                                        & " @SCL_LAB_EMAIL='" + lblLabEmail.Text + "'," _
                                        & " @MODE='delete'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                selectedRow.Visible = False
            End If
        End With
    End Sub
    Protected Sub gvStudGrade_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvStudGrade.RowDeleting

    End Sub
    Protected Sub gvStudGrade_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStudGrade.RowEditing
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        'txtLabAsst.Text = ""
        'txtLibrarian.Text = ""
        txtLabEmail.Text = ""
        txtLibEmail.Text = ""
        ViewState("datamode") = "add"
    End Sub
End Class
