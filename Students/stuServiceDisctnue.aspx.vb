Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports CURRICULUM

Partial Class stuServiceDisctnue
    Inherits System.Web.UI.Page

    Dim MainMnu_code As String
    Dim Encr_decrData As New Encryption64
    Dim CurUsr_id As String
    Dim CurRole_id As String
    Dim CurBsUnit As String
    Dim USR_NAME As String
    Dim content As ContentPlaceHolder

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GridBindStudents(ByVal vSTU_IDs As String)
        Try
            h_STU_IDs.Value = vSTU_IDs.Split("___")(0)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not IsPostBack Then
            h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
            h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            Page.Title = OASISConstants.Gemstitle
            lblError.Text = ""
            ViewState("datamode") = "none"
            CurUsr_id = Session("sUsr_id")
            CurRole_id = Session("sroleid")
            CurBsUnit = Session("sBsuid")
            USR_NAME = Session("sUsr_name")
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("MainMnu_code") <> "" Then
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
                MainMnu_code = ""
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            Else
                ViewState("datamode") = ""
            End If

            H_SSD_ID.Value = 0

            If USR_NAME = "" Or CurBsUnit = "" Or MainMnu_code <> "S100017" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)
                'disable the control based on the rights
                content = Page.Master.FindControl("cphMasterpage")
                Call AccessRight.setpage(content, ViewState("menu_rights"), ViewState("datamode"))
            End If

            ddlACDYear.DataSource = FeeCommon.GetBSUAcademicYear(Session("sBSUID"))
            ddlACDYear.DataTextField = "ACY_DESCR"
            ddlACDYear.DataValueField = "ACD_ID"
            ddlACDYear.DataBind()
            ddlACDYear.SelectedValue = Session("Current_ACD_ID")

            Dim LstAll As New ListItem("ALL", 0)

            ddlGrade.DataSource = GetGrades()
            ddlGrade.DataTextField = "GRD_DISPLAY"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade.Items.Insert(0, LstAll)

            ddlSection.DataSource = GetSections(ddlGrade.SelectedValue)
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
            ddlSection.Items.Insert(0, LstAll)

            ddlServices.DataSource = GetSERVICES_Details(1)
            ddlServices.DataTextField = "SVC_DESCRIPTION"
            ddlServices.DataValueField = "SVC_ID"
            ddlServices.DataBind()

            txtReqDate.Text = Format(Date.Today(), OASISConstants.DateFormat)

            If ViewState("datamode") = "edit" Then
                DisplayRequest(Request.QueryString("SSDID"))
                H_SSD_ID.Value = Request.QueryString("SSDID")
                Dim str_ As String = Lock(h_STU_IDs.Value)
                If str_ <> "0" Then
                    '    stTrans.Rollback()
                    lblError.Text = getErrorMessage(str_)
                    btnSave.Enabled = False
                    Exit Sub
                End If
            Else
                DissableControls(False)
            End If

            If ViewState("datamode") = "view" Then
                DissableControls(True)
                Dim vSVB_ID As Integer = Encr_decrData.Decrypt(Request.QueryString("SVB_ID").Replace(" ", "+"))
                FillDetails(vSVB_ID)
            End If
        End If
    End Sub

    Private Function GetGrades() As DataTable

        Dim sql_query As String = "SELECT GRD_ID,GRD_DISPLAY,GRD_DISPLAYORDER FROM GRADE_M ORDER BY GRD_DISPLAYORDER "

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Private Function GetSections(ByVal GRDID As String) As DataTable

        Dim sql_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M " & _
                                " WHERE SCT_BSU_ID=" & Session("sBsuId") & " AND SCT_ACD_ID=" & ddlACDYear.SelectedValue & " AND SCT_GRD_ID='" & ddlGrade.SelectedValue & "' ORDER BY 2"

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Private Function DisplayRequest(ByVal SSDID As Integer)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""


            str_Sql = "SELECT SSD_ID,SSD_BSU_ID,SSD_ACD_ID,SSD_SVC_ID,SSD_STU_ID,STU_NO,STU_NAME, " & _
                        "SSD_SERVICE_DIS_DT,SSD_REQUESTED_DT " & _
                        "FROM  STUDENT_SERVICE_DISCONTINUE_REQ " & _
                        "INNER JOIN VW_OSO_STUDENT_M ON STU_ID=SSD_STU_ID " & _
                        " WHERE SSD_ID=" & SSDID

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count >= 1 Then

                txtReqDate.Text = Format(CDate(ds.Tables(0).Rows(0).Item("SSD_REQUESTED_DT")), OASISConstants.DateFormat)
                ViewState("DisDate") = Format(CDate(ds.Tables(0).Rows(0).Item("SSD_SERVICE_DIS_DT")), OASISConstants.DateFormat)
                ddlACDYear.SelectedValue = ds.Tables(0).Rows(0).Item("SSD_ACD_ID")

                'txtStudIDs.Text = ds.Tables(0).Rows(0).Item("STU_NAME")
                h_STU_IDs.Value = ds.Tables(0).Rows(0).Item("SSD_STU_ID")
                ViewState("SVCID") = CInt(ds.Tables(0).Rows(0).Item("SSD_SVC_ID"))

                GridBind(SSDID)

                DissableControls(True)
            End If
        Catch ex As Exception
            DissableControls(True)
        End Try
    End Function


    Private Function GetSERVICES_Details(Optional ByVal SVCNOt As Integer = 0) As DataTable
        Dim sql_query As String = "SELECT SVB_ID,SVC_ID, SVC_DESCRIPTION FROM SERVICES_BSU_M " & _
                                  " INNER JOIN SERVICES_SYS_M ON SVB_SVC_ID=SVC_ID WHERE " & _
                                  " SVB_ACD_ID=" & ddlACDYear.SelectedValue & " AND SVB_BSU_ID='" & Session("sBsuId") & "'" & _
                                  " AND SVB_bAvailable='True'"

        If SVCNOt >= 1 Then
            sql_query += " AND SVC_ID<>" & SVCNOt.ToString
        End If
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

#Region "Grid Bind Functions"

    Private Function GetSelectedStudentsServices()

        Dim str_sql As String = ""
        str_sql = "SELECT SSV_ID,SSV_STU_ID,SVC_ID AS SSV_SVC_ID,SVC_DESCRIPTION,CONVERT(VARCHAR,SSV_FROMDATE,103)AS SSV_FROMDATE," & _
                 "SSV_TODATE AS SSD_SERVICE_DIS_DT, SSV_BACTIVE  FROM STUDENT_SERVICES_D " & _
                 " INNER JOIN SERVICES_SYS_M ON SVC_ID=SSV_SVC_ID " & _
                 " INNER JOIN VW_OSO_STUDENT_M ON STU_ID=SSV_STU_ID " & _
                 " WHERE SSV_TODATE IS NULL AND SSV_BSU_ID='" & Session("sBsuId") & "' " & _
                 " AND SSV_STU_ID=" & h_STU_IDs.Value & " AND SVC_ID<>1 "


        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function

    Private Sub GridBind(Optional ByVal SSDID As String = "0")
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim strFilterMain As String = ""

        str_query = " SELECT STU_ID,SVC_ID,SSV_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," & _
                         " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR, " & _
                         " SVC_DESCRIPTION,SSV_FROMDATE, " & _
                         " SSD_SERVICE_DIS_DT,ISNULL(SSD_ID,0) AS SSD_ID, SSV_BACTIVE,SSV_ACD_ID " & _
                         " FROM STUDENT_M AS A INNER JOIN OASIS_TRANSPORT.dbo.VV_GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" & _
                         " INNER JOIN STUDENT_SERVICES_D ON  STU_ID=SSV_STU_ID " & _
                         " INNER JOIN SERVICES_SYS_M ON SVC_ID=SSV_SVC_ID" & _
                         " LEFT OUTER JOIN STUDENT_SERVICE_DISCONTINUE_REQ ON SSD_STU_ID=STU_ID AND SSD_APPROV_DT IS NULL " & _
                         " LEFT OUTER JOIN OASIS_TRANSPORT.dbo.VV_SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " & _
                         " WHERE SSV_TODATE IS NULL AND SSV_BSU_ID='" & Session("sBsuId") & "' " & _
                         " AND SSV_ACD_ID = " + ddlACDYear.SelectedValue.ToString + "  " & _
                         " AND SVC_ID=" & ddlServices.SelectedValue & " "

        'AND STU_CURRSTATUS<>'CN'

        If ddlGrade.SelectedValue <> "0" Then
            strFilterMain = strFilterMain + " AND A.STU_GRD_ID='" & ddlGrade.SelectedValue & "'"
        End If
        If ddlSection.SelectedValue <> "0" Then
            strFilterMain = strFilterMain + " AND A.STU_SCT_ID='" & ddlSection.SelectedValue & "'"
        End If

        If SSDID <> "0" Then
            strFilterMain = strFilterMain + " AND SSD_ID=" & SSDID & ""
        End If

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""

        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
        Dim ddlgvPick As New DropDownList
        Dim ddlgvDrop As New DropDownList

        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        If gvStudTPT.Rows.Count > 0 Then


            txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            txtSearch = gvStudTPT.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtSection")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SCT_DESCR", txtSearch.Text, strSearch)
            selectedSection = txtSearch.Text

            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If
            'Else
            'str_query += " and stu_pickup_trp_id is not null and stu_dropoff_trp_id is not null"

        End If
        str_query += strFilterMain
        str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStudTPT.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            Try
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudTPT.DataSource = ds.Tables(0)
                Try
                    gvStudTPT.DataBind()
                Catch ex As Exception
                End Try
                ViewState("norow") = "yes"
                Dim columnCount As Integer = gvStudTPT.Rows(0).Cells.Count
                gvStudTPT.Rows(0).Cells.Clear()
                gvStudTPT.Rows(0).Cells.Add(New TableCell)
                gvStudTPT.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudTPT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudTPT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Catch ex As Exception
            End Try
        Else
            gvStudTPT.DataBind()
        End If


        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        If gvStudTPT.Rows.Count > 0 Then

            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = selectedGrade

            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtSection")
            txtSearch.Text = selectedSection

        End If
        set_Menu_Img()

    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(1))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(1))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(1))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(1))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
#End Region

    Private Sub FillDetails(ByVal vSVB_ID As Integer)
        Dim vSERV_PROV As StudentServiceProvider
        vSERV_PROV = StudentServiceProvider.GetDetails(vSVB_ID)
        ddlGrade.SelectedValue = vSERV_PROV.SERVICE_ID
        ddlACDYear.SelectedValue = vSERV_PROV.ACD_ID
        Session("sSERV_PROV") = vSERV_PROV
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "add"
        ClearAllFields()
        DissableControls(False)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Session("sSERV_PROV") = Nothing
    End Sub

    Private Sub DissableControls(ByVal dissable As Boolean)
        ddlACDYear.Enabled = Not dissable
        ddlSection.Enabled = Not dissable
        ddlGrade.Enabled = Not dissable
        ddlServices.Enabled = Not dissable
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        DissableControls(False)
        UtilityObj.beforeLoopingControls(Me.Page)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim stTrans As SqlTransaction
        Dim newSVBID As Integer
        Dim errNo As Integer
        Dim flagAudit As Integer
        Dim SSDID As Double
        Try

            'Date Validation
            For Each gvRow As GridViewRow In gvStudTPT.Rows
                If TryCast(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then

                    If TryCast(gvRow.FindControl("txtDisDate"), TextBox).Text.Trim = "" Then
                        lblError.Text = " Discontinue date Can't be Blank!. " & TryCast(gvRow.FindControl("lblStu_Name"), Label).Text
                        Exit Sub
                    End If
                    If CDate(TryCast(gvRow.FindControl("txtDisDate"), TextBox).Text) < Format(CDate(gvRow.Cells(6).Text), OASISConstants.DateFormat) Then
                        lblError.Text = " Discontinue date must be greater than Service date!. " & TryCast(gvRow.FindControl("lblStu_Name"), Label).Text
                        Exit Sub
                    End If

                End If
            Next
            '--------

            objConn.Close()
            objConn.Open()
            stTrans = objConn.BeginTransaction

            For Each gvRow As GridViewRow In gvStudTPT.Rows
                Dim HF_SSV_ACD_ID As String = TryCast(gvRow.FindControl("HF_SSV_ACD_ID"), HiddenField).Value
                Dim HF_SSV_ID As Integer = TryCast(gvRow.FindControl("HF_SSV_ID"), HiddenField).Value

                If TryCast(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then

                    H_SSD_ID.Value = TryCast(gvRow.FindControl("LBLssd"), Label).Text
                   
                    If gvRow.Style.Value = "background-color:#FFC0C0" Then ' On Edit

                        SSDID = SaveStudentsDiscRequest(TryCast(gvRow.FindControl("lblStuid"), Label).Text, ddlServices.SelectedValue, CDate(TryCast(gvRow.FindControl("txtDisDate"), TextBox).Text), objConn, stTrans, "edit", HF_SSV_ACD_ID, HF_SSV_ID)

                        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SSD_ID(" + SSDID.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                    Else

                        SSDID = SaveStudentsDiscRequest(TryCast(gvRow.FindControl("lblStuid"), Label).Text, ddlServices.SelectedValue, CDate(TryCast(gvRow.FindControl("txtDisDate"), TextBox).Text), objConn, stTrans, "add", HF_SSV_ACD_ID, HF_SSV_ID)
                        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SSD_ID(" + SSDID.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                    End If

                End If
            Next
            'Else
            '    errNo = SaveStudentsDiscRequest(h_STU_IDs.Value, ViewState("SVCID"), CDate(TryCast(gvStudTPT.Rows(0).FindControl("txtDisDate"), TextBox).Text), objConn, stTrans)
            'End If

            If SSDID >= 1 Then
                stTrans.Commit()
                ClearAllFields()
                lblError.Text = "Service Discontinue Details saved sucessfully..."
                ViewState("datamode") = "add"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))


                Dim url As String

                MainMnu_code = Request.QueryString("MainMnu_code")
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

                url = String.Format("stuServiceDisctnueView.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))
                Response.Redirect(url)

            Else
                lblError.Text = UtilityObj.getErrorMessage(errNo)
                stTrans.Rollback()
            End If

        Catch ex As Exception
            'stTrans.Rollback()
            lblError.Text = ex.Message
        Finally
            objConn.Close()
        End Try
    End Sub

    Private Function SaveStudentsDiscRequest(ByVal IntSTU_ID As Integer, ByVal SVCID As Integer, ByVal Discdate As DateTime, ByVal objConn As SqlConnection, ByVal stTrans As SqlTransaction, ByVal DataMode As String, ByVal SSV_ACD_ID As String, ByVal SSV_ID As Integer) As Integer

        Dim iReturnvalue As Integer
        Dim cmd As SqlCommand

        cmd = New SqlCommand("SaveSTUDDISCONTINUE_REQUEST", objConn, stTrans)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpSSR_ID As New SqlParameter("@SSD_ID", SqlDbType.Int)
        sqlpSSR_ID.Value = H_SSD_ID.Value
        cmd.Parameters.Add(sqlpSSR_ID)

        Dim sqlpSSR_BSU_ID As New SqlParameter("@SSD_BSU_ID", SqlDbType.VarChar, 20)
        sqlpSSR_BSU_ID.Value = Session("sBsuId")
        cmd.Parameters.Add(sqlpSSR_BSU_ID)

        Dim sqlpSSR_ACD_ID As New SqlParameter("@SSD_ACD_ID", SqlDbType.Int)
        sqlpSSR_ACD_ID.Value = SSV_ACD_ID
        cmd.Parameters.Add(sqlpSSR_ACD_ID)

        Dim sqlpSSR_SVC_ID As New SqlParameter("@SSD_SVC_ID", SqlDbType.Int)
        sqlpSSR_SVC_ID.Value = SVCID
        cmd.Parameters.Add(sqlpSSR_SVC_ID)

        Dim sqlpSSR_STU_ID As New SqlParameter("@SSD_STU_ID", SqlDbType.BigInt)
        sqlpSSR_STU_ID.Value = IntSTU_ID
        cmd.Parameters.Add(sqlpSSR_STU_ID)

        Dim sqlpSRV_STARTDT As New SqlParameter("@SSD_SERVICE_DIS_DT", SqlDbType.DateTime)
        sqlpSRV_STARTDT.Value = Discdate
        cmd.Parameters.Add(sqlpSRV_STARTDT)

        Dim sqlpREQ_DATE As New SqlParameter("@SSD_REQUESTED_DT", SqlDbType.DateTime)
        sqlpREQ_DATE.Value = txtReqDate.Text
        cmd.Parameters.Add(sqlpREQ_DATE)

        Dim sqlpApprove As New SqlParameter("@bApprove", SqlDbType.VarChar, 10)
        sqlpApprove.Value = "N"
        cmd.Parameters.Add(sqlpApprove)

        Dim sqlpSessionID As New SqlParameter("@SSD_SESSIONID", SqlDbType.VarChar, 200)
        sqlpSessionID.Value = Session.SessionID
        cmd.Parameters.Add(sqlpSessionID)

        Dim sqlpLock As New SqlParameter("@SSD_LOCK", SqlDbType.VarChar, 50)
        sqlpLock.Value = Session("sUsr_name")
        cmd.Parameters.Add(sqlpLock)

        Dim sqlpDataMode As New SqlParameter("@datamode", SqlDbType.VarChar, 10)
        sqlpDataMode.Value = DataMode
        cmd.Parameters.Add(sqlpDataMode)

        Dim sqlp_SSV_ID As New SqlParameter("@SSD_SSV_ID", SqlDbType.Int)
        sqlp_SSV_ID.Value = SSV_ID
        cmd.Parameters.Add(sqlp_SSV_ID)

        Dim sqlReturnValue As New SqlParameter("@RETURNVALUE", SqlDbType.BigInt)
        sqlReturnValue.Direction = ParameterDirection.Output
        cmd.Parameters.Add(sqlReturnValue)


        iReturnvalue = cmd.ExecuteNonQuery()

        Return sqlReturnValue.Value
    End Function

    Private Sub ClearAllFields()
        ddlACDYear.SelectedValue = Session("Current_ACD_ID")
        ddlGrade.SelectedIndex = -1
        Session("sSERV_PROV") = Nothing
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            If ViewState("datamode") = "edit" Then
                UnLock()
            End If
            Call ClearAllFields()
            ViewState("datamode") = "none"
            Session("sSERV_PROV") = Nothing
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Dim url As String

            MainMnu_code = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("stuServiceDisctnueView.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        End If
    End Sub

    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If h_STU_IDs.Value <> "" Then
            GridBindStudents(h_STU_IDs.Value)
        End If
    End Sub

    Protected Sub grdStudent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudTPT.PageIndexChanging
        gvStudTPT.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub grdStudent_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If ViewState("datamode") = "edit" Then

                If e.Row.RowType = DataControlRowType.DataRow Then
                    TryCast(e.Row.FindControl("txtDisDate"), TextBox).Text = ViewState("DisDate")
                    TryCast(e.Row.FindControl("chkSelect"), CheckBox).Checked = True
                End If

            End If
            e.Row.Cells(1).Style.Value = "Display :None"
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            GridBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        Try
            If chkAll.Checked = True And txtReqDate.Text <> "" Then

                For Each GvRow As GridViewRow In gvStudTPT.Rows
                    If TryCast(GvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                        If TryCast(GvRow.FindControl("txtDisDate"), TextBox).Text = "" Then
                            TryCast(GvRow.FindControl("txtDisDate"), TextBox).Text = txtReqDate.Text
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvStudTPT_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                If TryCast(e.Row.FindControl("txtDisDate"), TextBox).Text <> "" Then
                    'TryCast(e.Row.FindControl("chkSelect"), CheckBox).Checked = False
                    'TryCast(e.Row.FindControl("chkSelect"), CheckBox).Enabled = False
                    'TryCast(e.Row.FindControl("txtDisDate"), TextBox).Enabled = False
                    e.Row.Style.Value = "background-color:#FFC0C0"
                    Dim str_ As String = Lock(CInt(TryCast(e.Row.FindControl("lblStuId"), Label).Text))
                    If str_ <> "0" Then
                        '    stTrans.Rollback()
                        lblError.Text = getErrorMessage(str_)
                        btnSave.Enabled = False
                        Exit Sub
                    End If
                End If
            End If

           


            'txtDisDate
        Catch ex As Exception

        End Try
    End Sub
    Private Function Lock(ByVal IntSTU_ID As Integer) As String

        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim Trans As SqlTransaction

        objConn.Close()
        objConn.Open()
        Trans = objConn.BeginTransaction
        Try
            Dim cmd As New SqlCommand("LockSTUDENTSERVICEDISCONTINUE", objConn, Trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpSSR_STU_ID As New SqlParameter("@SSD_STU_ID", SqlDbType.VarChar, 10)
            sqlpSSR_STU_ID.Value = IntSTU_ID
            cmd.Parameters.Add(sqlpSSR_STU_ID)

            Dim sqlpsqlpSSR_BSU_ID As New SqlParameter("@SSD_BSU_ID", SqlDbType.VarChar, 10)
            sqlpsqlpSSR_BSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpsqlpSSR_BSU_ID)

            Dim sqlpsqlpSSR_ACD_ID As New SqlParameter("@SSD_ACD_ID", SqlDbType.Int)
            sqlpsqlpSSR_ACD_ID.Value = ddlACDYear.SelectedValue
            cmd.Parameters.Add(sqlpsqlpSSR_ACD_ID)

            Dim sqlpSSR_DOCTYPE As New SqlParameter("@SSD_DOCTYPE", SqlDbType.VarChar, 20)
            sqlpSSR_DOCTYPE.Value = "R"
            cmd.Parameters.Add(sqlpSSR_DOCTYPE)

            Dim sqlpSSR_SVC_ID As New SqlParameter("@SSD_SVC_ID", SqlDbType.Int)
            sqlpSSR_SVC_ID.Value = ddlServices.SelectedValue
            cmd.Parameters.Add(sqlpSSR_SVC_ID)

            Dim sqlpSSR_USER As New SqlParameter("@SSD_USER", SqlDbType.VarChar, 50)
            sqlpSSR_USER.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpSSR_USER)

            Dim sqlpSESSION As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
            sqlpSESSION.Value = Session.SessionID
            cmd.Parameters.Add(sqlpSESSION)

            Dim iReturnvalue As Integer
            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            cmd.ExecuteNonQuery()

            iReturnvalue = retValParam.Value

            If iReturnvalue <> 0 Then
                Trans.Rollback()
                'lblError.Text = "Error while locking table"
            Else
                Trans.Commit()
            End If
            Return iReturnvalue
        Catch ex As Exception
            Trans.Rollback()
            lblError.Text = "Error while locking table"
        Finally
            objConn.Close()
        End Try
        Return True
    End Function
    Private Function UnLock() As String

        Dim objConn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim Trans As SqlTransaction

        objConn.Close()
        objConn.Open()
        Trans = objConn.BeginTransaction
        Try
            Dim cmd As New SqlCommand("UnLockSTUDENTSERVICEDISCONTINUE", objConn, Trans)
            cmd.CommandType = CommandType.StoredProcedure

            Dim sqlpSSD_ID As New SqlParameter("@SSD_ID", SqlDbType.VarChar, 10)
            sqlpSSD_ID.Value = H_SSD_ID.Value
            cmd.Parameters.Add(sqlpSSD_ID)

            Dim sqlpsqlpSSR_BSU_ID As New SqlParameter("@SSD_BSU_ID", SqlDbType.VarChar, 10)
            sqlpsqlpSSR_BSU_ID.Value = Session("sBsuid")
            cmd.Parameters.Add(sqlpsqlpSSR_BSU_ID)

            Dim sqlpsqlpSSR_ACD_ID As New SqlParameter("@SSD_ACD_ID", SqlDbType.Int)
            sqlpsqlpSSR_ACD_ID.Value = ddlACDYear.SelectedValue
            cmd.Parameters.Add(sqlpsqlpSSR_ACD_ID)

            Dim sqlpSSR_DOCTYPE As New SqlParameter("@SSD_DOCTYPE", SqlDbType.VarChar, 20)
            sqlpSSR_DOCTYPE.Value = "R"
            cmd.Parameters.Add(sqlpSSR_DOCTYPE)

            Dim sqlpSSR_SVC_ID As New SqlParameter("@SSD_SVC_ID", SqlDbType.Int)
            sqlpSSR_SVC_ID.Value = ddlServices.SelectedValue
            cmd.Parameters.Add(sqlpSSR_SVC_ID)

            Dim sqlpSSR_USER As New SqlParameter("@SSD_USER", SqlDbType.VarChar, 50)
            sqlpSSR_USER.Value = Session("sUsr_name")
            cmd.Parameters.Add(sqlpSSR_USER)

            Dim sqlpSESSION As New SqlParameter("@SESSION", SqlDbType.VarChar, 50)
            sqlpSESSION.Value = Session.SessionID
            cmd.Parameters.Add(sqlpSESSION)

            Dim iReturnvalue As Integer
            Dim retValParam As New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            retValParam.Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add(retValParam)
            cmd.ExecuteNonQuery()

            iReturnvalue = retValParam.Value

            If iReturnvalue <> 0 Then
                Trans.Rollback()
                'lblError.Text = "Error while locking table"
            Else
                Trans.Commit()
            End If
            Return iReturnvalue
        Catch ex As Exception
            Trans.Rollback()
            lblError.Text = "Error while Unlocking table"
        Finally
            objConn.Close()
        End Try
        Return True
    End Function

    Protected Sub chkAll_CheckedChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If TryCast(gvStudTPT.HeaderRow.FindControl("ChkAll"), CheckBox).Checked = True Then
                For Each GvRow As GridViewRow In gvStudTPT.Rows
                    TryCast(GvRow.FindControl("chkSelect"), CheckBox).Checked = True
                Next
            Else
                For Each GvRow As GridViewRow In gvStudTPT.Rows
                    TryCast(GvRow.FindControl("chkSelect"), CheckBox).Checked = False
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Dim LstAll As New ListItem("ALL", 0)
        GetSections(ddlGrade.SelectedValue)
        ddlSection.DataSource = GetSections(ddlGrade.SelectedValue)
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        'ddlSection.Items.Insert(0, LstAll)
    End Sub

    Protected Sub ddlServices_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlServices.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception

        End Try
    End Sub


End Class
