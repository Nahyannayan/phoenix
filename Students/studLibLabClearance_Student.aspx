﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studLibLabClearance_Student.aspx.vb" Inherits="Students_studLibLabClearance_Student" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStud.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click();//fire the click event of the child element
                    }
                    if (chkstate = true) {

                    }
                }
            }
        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_ShowScreen" runat="server" width="100%" align="center" border="0" cellpadding="0"
                    cellspacing="0">

                    <tr>

                        <td align="left">

                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1"></asp:ValidationSummary>
                        </td>
                    </tr>

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="25%">
                                        <span class="field-label">Select Academic Year</span></td>

                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="108px">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>



                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTcmId" runat="server" Text='<%# Bind("TCM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("STU_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("STU_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Select
                                                            <br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Student ID">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStu_NoH" runat="server">Student ID</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtStuNo" runat="server" Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuNo_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtStuName" runat="server" Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtGrade" runat="server" Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Section">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblH123" runat="server" CssClass="gridheader_text" Text="Section"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtSection" runat="server" Width="75%"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSection_Search" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSection_Search_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Clearances" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:GridView ID="gvClearance" runat="server" EmptyDataText="No Records" AutoGenerateColumns="false" PageSize="2">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTcsId" runat="server" Text='<%# Bind("TCS_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("TCC_FEE_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Clearance Type">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblClearance" Text='<%# Bind("TCC_DESCR") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Fee Pending">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtFee" runat="server"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                            </Columns>

                                                            <HeaderStyle CssClass="gridheader_pop"></HeaderStyle>
                                                            <RowStyle CssClass="griditem"></RowStyle>
                                                            <SelectedRowStyle CssClass="Green"></SelectedRowStyle>
                                                            <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                                        </asp:GridView>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="button" TabIndex="4" ValidationGroup="groupM1" />

                                    </td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>


            </div>
        </div>
    </div>


</asp:Content>

