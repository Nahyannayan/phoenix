<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudStaffAuthorized_Grade_Edit.aspx.vb" Inherits="Students_StudStaffAuthorized_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">





        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFrom.ClientID %>').value, "", sFeatures)
            else if (mode == 2)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtTo.ClientID %>').value, "", sFeatures)

        if (result == '' || result == undefined) {
            //            document.getElementById("txtDate").value=''; 
            return false;
        }
        if (mode == 1)
            document.getElementById('<%=txtFrom.ClientID %>').value = result;
        else if (mode == 2)
            document.getElementById('<%=txtTo.ClientID %>').value = result;
       return true;
        }
        //coomented by rajesh 28april2019
  // function ShowValue(val) {
    //   alert(val);
   //}


    </script>



    <script language="javascript" type="text/javascript">
        var gridViewCtlId = '<%=gvInfo.ClientID%>';
        var gridViewCtl = null;
        var curSelRow = null;
        function getGridViewControl() {
            if (null == gridViewCtl) {
                gridViewCtl = document.getElementById(gridViewCtlId);
            }
        }

        function onGridViewRowSelected(rowIdx) {
            var selRow = getSelectedRow(rowIdx);
            if (curSelRow != null) {
                curSelRow.style.backgroundColor = '#f5dc84';
            }

            if (null != selRow) {
                curSelRow = selRow;
                curSelRow.style.backgroundColor = '#f5dc84';
            }
        }

        function getSelectedRow(rowIdx) {
            getGridViewControl();
            if (null != gridViewCtl) {
                return gridViewCtl.rows[rowIdx];
            }
            return null;
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" >
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                         ></asp:Label>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"  ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                    &nbsp;
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="center" class="matters"   valign="middle">&nbsp;Fields Marked with (<font color="#c00000">*</font>) are mandatory</td>
                    </tr>

                    <tr>
                        <td class="matters"   valign="bottom">
                            <table     width="100%">
                                <tr>
                                    <td align="left"><span class="field-label">Academic Year <span style=" color: #800000">*</span></span></td>
                                    <td align="left" colspan="1">
                                        <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                          <td align="left" ><span class="field-label">Stream <span style=" color: #800000">*</span></span></td>
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStream_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Grade <span style=" color: #800000">*</span></span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                   
                                </tr>
                                <tr>
                               <td align="left" width="20%"><span class="field-label">Section<span style=" color: #800000">*</span></span></td>
                                    <td align="left"  width="30%">
                                        <asp:DropDownList ID="ddlAuthSection" runat="server" OnSelectedIndexChanged="ddlAuthSection_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></td>
                                    <td align="left" ><span class="field-label">Shift <span style=" color: #800000">*</span></span></td>
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlShift_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">From  <span style=" color: #800000">*</span></span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtFrom" runat="server"  >
                                        </asp:TextBox>&nbsp;<asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                             ></asp:ImageButton><asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                                CssClass="error" Display="Dynamic" ErrorMessage="From Date required" ForeColor=""
                                                ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                                    <td align="left" ><span class="field-label">To <span style=" color: #800000">*</span></span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtTo" runat="server"  >
                                        </asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                            ></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTo"
                                            CssClass="error" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                            ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left"  ><span class="field-label">Authorized Staff <span style=" color: #800000">*</span></span></td>
                                    <td align="left"  colspan="1">
                                        <asp:DropDownList ID="ddlAuthStaff" runat="server">
                                        </asp:DropDownList></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr class="title-bg">
                                    <td colspan="6"   align="left" class="matters">Authorized Staff</td>
                                </tr>
                                <tr>
                                    <td colspan="6" valign="top">
                                        <asp:GridView ID="gvInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="100%"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Details Added Yet"   Height="100%" OnPageIndexChanging="gvInfo_PageIndexChanging">
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>
                                                <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSAD_ID" runat="server" Text='<%# Bind("SAD_ID") %>' ></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Authorized Staff">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:Label ID="lblEMP_Name" runat="server" Text='<%# Bind("EMP_Name") %>' ></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Left" Wrap="True"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:Label ID="lblsection" runat="server" Text='<%# Bind("section") %>' ></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Left" Wrap="True"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFROMDT" runat="server" Text='<%# Bind("FromDT", "{0:dd/MMM/yyyy}") %>' ></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Left" Wrap="True"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTODT" runat="server" Text='<%# Bind("TODT", "{0:dd/MMM/yyyy}") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle  />
                                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"   valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" OnClick="btnAdd_Click" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" OnClick="btnEdit_Click" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                ValidationGroup="AttGroup" OnClick="btnSave_Click" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                    CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /><asp:Button ID="btnDelete" runat="server" CausesValidation="False"
                                        CssClass="button" Text="Delete" OnClick="btnDelete_Click" />

                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfSAD_ID" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
         <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar"
        TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
         <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
        TargetControlID="txtTo">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

