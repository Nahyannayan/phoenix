Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studAtt_room_Grade_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                Dim menu_rights As String
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050112") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub


    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try




            Dim str_filter_GRM_DIS As String = String.Empty
            Dim ds As New DataSet

            '            str_Sql = " SELECT RAPG_GRD_ID,GRM_DIS,substring(rtrim(period),1,len(rtrim(period))-1) AS period  FROM(SELECT distinct A.RAPG_GRD_ID,A.GRM_DIS, STUFF((SELECT distinct P.RAP_PERIOD_DESCR + ', ' FROM ATT.ROOM_ATT_PERIOD_GRADES AS G " & _
            ' " INNER JOIN ATT.ROOM_ATTENDANCE_PERIOD AS P ON P.RAP_PERIOD_ID = G.RAPG_RAP_PERIOD_ID " & _
            '" AND G.RAPG_GRD_ID =A.RAPG_GRD_ID  WHERE (G.RAPG_BSU_ID = '" & Session("sBsuid") & "') " & _
            ' " FOR XML PATH('')), 1, 0,'') AS period  FROM(SELECT DISTINCT RAPG_GRD_ID, (SELECT DISTINCT TOP 1 " & _
            ' " GRM_DISPLAY from GRADE_BSU_M where GRM_GRD_ID =RAPG_GRD_ID) AS GRM_DIS " & _
            '" FROM ATT.ROOM_ATT_PERIOD_GRADES  WHERE RAPG_BSU_ID = '" & Session("sBsuid") & "')A)B "
            '            '" SELECT  A.RAPG_GRD_ID,A.GRM_DIS, substring(rtrim(A.period),1,len(rtrim(A.period))-1) as period  FROM(SELECT DISTINCT RAPG_GRD_ID, (SELECT DISTINCT TOP 1 GRM_DISPLAY from GRADE_BSU_M where GRM_GRD_ID =RAPG_GRD_ID) AS GRM_DIS,  STUFF((SELECT distinct P.RAP_PERIOD_DESCR + ', ' FROM ATT.ROOM_ATT_PERIOD_GRADES AS G " & _
            '            '           " INNER JOIN ATT.ROOM_ATTENDANCE_PERIOD AS P ON P.RAP_PERIOD_ID = G.RAPG_RAP_PERIOD_ID AND G.RAPG_GRD_ID =A.RAPG_GRD_ID " & _
            '            '           " WHERE (G.RAPG_BSU_ID = '" & Session("sBsuid") & "')  " & _
            '            '           "  FOR XML PATH('')), 1, 0,'') AS period FROM ATT.ROOM_ATT_PERIOD_GRADES  WHERE RAPG_BSU_ID = '" & Session("sBsuid") & "')A"

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds1 As New DataSet
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            PARAM(2) = New SqlParameter("@INFO_TYPE", "GRIDBIND")

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "VA.GETMASTER_PERIOD_BIND", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then

                gvOnlineEnq_Ack.DataSource = ds.Tables(0)
                gvOnlineEnq_Ack.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ' ds.Tables(0).Rows(0)(4) = True

                gvOnlineEnq_Ack.DataSource = ds.Tables(0)
                Try
                    gvOnlineEnq_Ack.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvOnlineEnq_Ack.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvOnlineEnq_Ack.Rows(0).Cells.Clear()
                gvOnlineEnq_Ack.Rows(0).Cells.Add(New TableCell)
                gvOnlineEnq_Ack.Rows(0).Cells(0).ColumnSpan = columnCount
                gvOnlineEnq_Ack.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvOnlineEnq_Ack.Rows(0).Cells(0).Text = "No records available !"
            End If




        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    'Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim url As String
    '        'define the datamode to Add if Add New is clicked
    '        ViewState("datamode") = "add"

    '        'Encrypt the data that needs to be send through Query String
    '        ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
    '        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

    '        url = String.Format("~\Students\studAtt_room_Grade_edit.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
    '        Response.Redirect(url)
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed "
    '    End Try
    'End Sub
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblGRD_ID As New Label
            Dim lblGRM_DIS As New Label
            Dim url As String
            Dim viewid As String
            lblGRD_ID = TryCast(sender.FindControl("lblRAPG_GRD_ID"), Label)
            lblGRM_DIS = TryCast(sender.FindControl("lblGRM_DIS"), Label)
            viewid = lblGRD_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studAtt_room_Grade_edit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}&v1={3}", ViewState("MainMnu_code"), ViewState("datamode"), viewid, Encr_decrData.Encrypt(lblGRM_DIS.Text))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub






End Class
