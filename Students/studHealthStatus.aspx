<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studHealthStatus.aspx.vb" Inherits="Students_studHealthStatus" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Health Status
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--  <tr style="font-size: 12pt;">
            <td width="50%" align="left" class="title" style="height: 50px">
                <asp:Label ID="lblTitle" runat="server">HEALTH STATUS</asp:Label>
            </td>
        </tr>--%>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Select Academic Year</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"
                                            AutoPostBack="True">
                                        </asp:DropDownList>

                                    </td>
                                    <td align="left" colspan="2"></td>


                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="20%">
                                        <span class="field-label">Select Section</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr id="tr1" runat="server">
                                    <td colspan="4">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Select File</span>
                                    </td>

                                    <td align="left">
                                        <asp:FileUpload ID="uploadFile" runat="server" BorderStyle="None" EnableTheming="True" />
                                        <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Upload"
                                            OnClick="btnUpload_Click" CausesValidation="False" />
                                    </td>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />

                                        <asp:Button ID="btnDownload" runat="server" CssClass="button" Text="Download in Excel"
                                            OnClick="btnDownload_Click" CausesValidation="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>



                                <tr id="tr2" runat="server">
                                    <td colspan="4" align="left">
                                        <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr id="tr3" runat="server">
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBloodGroup" runat="server" Text='<%# Bind("STH_BLOODGROUP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Student No">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Stu_Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Roll.No">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtRollNo" runat="server" Text='<%# Bind("STH_ROLLNO") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Father Name">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtFather" runat="server" Text='<%# Bind("STH_FATHERNAME") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mother Name">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtMother" runat="server" Text='<%# Bind("STH_MOTHERNAME") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reg.No">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtRegNo" runat="server" Text='<%# Bind("STH_REGNO") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Height (cm)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtHeight" runat="server" Text='<%# Bind("STH_HEIGHT") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Weight (kg)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWeight" runat="server" Text='<%# Bind("STH_WEIGHT") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Blood Group">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlbloodgroup" runat="server">
                                                            <asp:ListItem Text="--" Value="--"></asp:ListItem>
                                                            <asp:ListItem Text="A+" Value="A+"></asp:ListItem>
                                                            <asp:ListItem Text="A-" Value="A-"></asp:ListItem>
                                                            <asp:ListItem Text="B+" Value="B+"></asp:ListItem>
                                                            <asp:ListItem Text="B-" Value="B-"></asp:ListItem>
                                                            <asp:ListItem Text="O+" Value="O+"></asp:ListItem>
                                                            <asp:ListItem Text="O-" Value="O-"></asp:ListItem>
                                                            <asp:ListItem Text="AB+" Value="AB+"></asp:ListItem>
                                                            <asp:ListItem Text="AB-" Value="AB-"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vision(L)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVisionL" runat="server" Text='<%# Bind("STH_VISION_L") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vision(R)">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtVisionR" runat="server" Text='<%# Bind("STH_VISION_R") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dental Hygiene">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtDental" TextMode="MultiLine" SkinID="MultiText" runat="server" Text='<%# Bind("STH_DENTAL") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="tr4" runat="server">
                                    <td colspan="4" align="left">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" TabIndex="4" />

                                        <asp:HiddenField ID="hSCT_ID" runat="server" />
                                        <asp:HiddenField ID="hACD_ID" runat="server" />

                                        <asp:HiddenField ID="hGRD_ID" runat="server" />

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</asp:Content>

