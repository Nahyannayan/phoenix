Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studtcClearance_Grade_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050150") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    GridBind()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnClearance_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
#Region "Private methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_query As String = "SELECT DISTINCT TCC_ID,TCG_ID,TCC_DESCR,GRM_DISPLAY,GRM_GRD_ID,STM_DESCR,STM_ID FROM TCCLEARANCE_GRADE_M AS A " _
                                 & " INNER JOIN TCCLEARANCE_M AS B ON A.TCG_TCC_ID=B.TCC_ID " _
                                 & " INNER JOIN GRADE_BSU_M AS C ON A.TCG_GRD_ID=C.GRM_GRD_ID AND A.TCG_ACD_ID=C.GRM_ACD_ID" _
                                 & " INNER JOIN STREAM_M AS D ON A.TCG_STM_ID=D.STM_ID " _
                                 & " WHERE TCG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        Dim txtSearch As New TextBox
        Dim tccSearch As String = ""
        Dim grmSearch As String = ""

        Dim ddlgvStream As New DropDownList
        Dim selectedStream As String = ""

        If gvClearance.Rows.Count > 0 Then

            txtSearch = gvClearance.HeaderRow.FindControl("txtClearance")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("TCC_DESCR", txtSearch.Text, strSearch)
            tccSearch = txtSearch.Text

            txtSearch = gvClearance.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            tccSearch = txtSearch.Text


            If strFilter.Trim <> "" Then
                str_query += " " + strFilter
            End If

            ddlgvStream = gvClearance.HeaderRow.FindControl("ddlgvStream")
            If ddlgvStream.Text <> "ALL" And ddlgvStream.Text <> "" Then

                strFilter = strFilter + " and stm_descr='" + ddlgvStream.Text + "'"
                selectedStream = ddlgvStream.Text
            End If

        End If

        str_query += " order by tcc_descr"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvClearance.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvClearance.DataBind()
            Dim columnCount As Integer = gvClearance.Rows(0).Cells.Count
            gvClearance.Rows(0).Cells.Clear()
            gvClearance.Rows(0).Cells.Add(New TableCell)
            gvClearance.Rows(0).Cells(0).ColumnSpan = columnCount
            gvClearance.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvClearance.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvClearance.DataBind()
        End If

        txtSearch = New TextBox
        txtSearch = gvClearance.HeaderRow.FindControl("txtClearance")
        txtSearch.Text = tccSearch

        txtSearch = New TextBox
        txtSearch = gvClearance.HeaderRow.FindControl("txtGrade")
        txtSearch.Text = grmSearch
        Dim dt As DataTable = ds.Tables(0)
        If gvClearance.Rows.Count > 0 Then
            ddlgvStream = gvClearance.HeaderRow.FindControl("ddlgvStream")
            ddlgvStream.Items.Clear()
            ddlgvStream.Items.Add("ALL")
            Dim dr As DataRow
            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    If ddlgvStream.Items.FindByText(.Item(4)) Is Nothing Then
                        ddlgvStream.Items.Add(.Item(4))
                    End If
                End With
            Next


            If selectedStream <> "" Then
                ddlgvStream.Text = selectedStream
            End If


        End If

    End Sub


    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvClearance.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvClearance.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvClearance.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvClearance.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region
    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
            url = String.Format("~\Students\studtcClearance_Grade_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvClearance_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvClearance.PageIndexChanging
        Try
            gvClearance.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvClearance_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvClearance.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvClearance.Rows(index), GridViewRow)
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                Dim lblGrdId As Label
                Dim lblTcgId As Label
                Dim lblTccId As Label
                Dim lblClearance As Label
                Dim lblStmId As Label
                Dim lblGrade As Label
                Dim lblStream As Label
                With selectedRow
                    lblTcgId = .FindControl("lblTcgId")
                    lblTccId = .FindControl("lblTccId")
                    lblClearance = .FindControl("lblClearance")
                    lblGrdId = .FindControl("lblGrdId")
                    lblGrade = .FindControl("lblGrade")
                    lblStream = .FindControl("lblStream")
                    lblStmId = .FindControl("lblStmId")
                End With
                Dim url As String
                url = String.Format("~\Students\studtcClearance_Grade_M.aspx?MainMnu_code={0}&datamode={1}" _
                     & "&tccid=" + Encr_decrData.Encrypt(lblTccId.Text) _
                     & "&clearance=" + Encr_decrData.Encrypt(lblClearance.Text) _
                     & "&tcgid=" + Encr_decrData.Encrypt(lblTcgId.Text) _
                     & "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) _
                     & "&stream=" + Encr_decrData.Encrypt(lblStream.Text) _
                     & "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) _
                     & "&stmid=" + Encr_decrData.Encrypt(lblStmId.Text) _
                     & "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                     & "&accid=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) _
                      , ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
End Class
