<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studTCEntry_Approval.aspx.vb" Inherits="Students_studTCEntry_Approval"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
           <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
        <tr>
            <td align="left"  valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table id="tblTC" runat="server" align="center" 
                     cellpadding="5" cellspacing="0" width="100%">
                    
                    <tr>
                        <td align="left" >
                            <span class="field-label">Select Academic Year</span>
                        </td>
                        
                        <td align="left" >
                            <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                Width="108px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" >
                            <span class="field-label">Select Entry Type</span>
                        </td>
                        
                        <td align="left"  >
                            <asp:DropDownList ID="ddlEntry" SkinID="smallcmb" runat="server" Width="108px">
                                <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem>School</asp:ListItem>
                                <asp:ListItem>Parent</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        </tr>
                    <tr>
                        <td  colspan="6">
                        <asp:RadioButton ID="rdComplete" Checked="true" runat="server" Text="Exit Interview Completed" GroupName="g1" />
                        <asp:RadioButton ID="rdPending" runat="server" Text="Exit Interview Not Completed" GroupName="g1" />
                        <asp:RadioButton ID="rdAll" runat="server" Text="All" GroupName="g1" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <span class="field-label">Select Grade</span>
                        </td>
                       
                        <td align="left" >
                            <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True"
                                Width="68px">
                            </asp:DropDownList>
                        </td>
                        <td align="left" >
                            <span class="field-label">Select Section</span>
                        </td>
                       
                        <td align="left"  >
                            <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server" Width="71px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                           <span class="field-label"> Student ID</span>
                        </td>
                       
                        <td align="left" >
                            <asp:TextBox ID="txtStuNo" runat="server">
                            </asp:TextBox>
                        </td>
                        <td align="left"  >
                            <span class="field-label">Student Name</span>
                        </td>
                        
                        <td align="left" >
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                        </td>
                        <td  colspan="2">
                            <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"
                                Width="51px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">Approve Date</span>
                        </td>
                        
                        <td align="left"  >
                            <asp:TextBox ID="txtApprovedDate" runat="server" Width="105px" Enabled="False"></asp:TextBox>
                        </td>
                       
                    </tr>
                    <tr>
                        <td colspan="8">
                        </td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="8" valign="top">
                            <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" OnRowDataBound="gvStud_RowDataBound">
                                <RowStyle CssClass="griditem" Height="25px" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Select">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTCMID" runat="server" Text='<%# Bind("TCM_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSctId" runat="server" Text='<%# Bind("Stu_SCT_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("Stu_GRD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDoj" runat="server" Text='<%# Bind("Stu_Doj") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>' Width="80px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="100px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250px"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Attendance Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLDA" runat="server" Text='<%# Bind("TCM_LASTATTDATE", "{0:dd/MMM/yyyy}") %>' ForeColor='<%# getLastDateColor(Eval("TCM_LASTATTDATE", "{0:dd/MMM/yyyy}")) %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Exit Form">
                                        <ItemTemplate>
                                            <asp:Label ID="lblExitForm" Width="100px" runat="server" Text='<%# Bind("EXITFORM") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fees">
                                        <ItemTemplate>
                                            <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" >
                                                <Columns>
                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTCSID" runat="server" Text='<%# Bind("TCS_ID") %>' ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFEEID" runat="server" Text='<%# Bind("TCC_FEE_ID") %>' ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="FeeType">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFeeType" runat="server" Text='<%# Bind("TCC_DESCR") %>' ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Fee Pending">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtFee" runat="server" Width="86px" ></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader_pop"></HeaderStyle>
                                                <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                            </asp:GridView>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblType" runat="server" OnClick="lblType_Click" Text='<%# Bind("type") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="Green" />
                                <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr runat="server" id="trApprove">
                        <td align="center"  colspan="8" valign="top">
                            <asp:Button ID="btnApprove" runat="server" CssClass="button" OnClick="btnApprove_Click"
                                Text="Approve" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                Text="Cancel" />
                        </td>
                    </tr>
                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                        type="hidden" value="=" />
                <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" /><br />
                <ajaxToolkit:CalendarExtender ID="CalApprovedDate" runat="server" Format="dd/MMM/yyyy"
                    TargetControlID="txtApprovedDate">
                </ajaxToolkit:CalendarExtender>
            </td>
        </tr>
    </table>
 </div>
        </div>
    </div>
</asp:Content>
