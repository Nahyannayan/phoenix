<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studEnquiryList_View.aspx.vb" Inherits="Students_studEnquiryList_View"
    Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <div id="LoadImg" style="display: none" class="screenCenter">
        <br />
        <img src="../Images/loading1.gif" align="absmiddle" alt="..." /><br />
        Loading Please wait...
    </div>
    <style>
        input {
            vertical-align: middle !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function Showdata(STU_ID, obj) {

            document.getElementById('LoadImg').style.display = 'inline';
            highlight(obj);
            if (obj.checked == true) {
                var sFeatures, url;
                sFeatures = "dialogWidth:750px; ";
                sFeatures += "dialogHeight: 400px; ";
                sFeatures += "help: no; ";
                sFeatures += "resizable: no; ";
                sFeatures += "scroll: yes; ";
                sFeatures += "status: no; ";
                sFeatures += "unadorned: no; ";
                var NameandCode;
                url = "../common/PopupShowData.aspx?id=STUDENTSHORTLIST&stuid=" + STU_ID;
                var oWnd = radopen(url, "pop_stu");
                //result = window.showModalDialog(url, "", sFeatures);
            }
            document.getElementById('LoadImg').style.display = 'none';
            return false;
        }

        <%-- function OnClientClose(oWnd, args) {

            var arg = args.get_argument();

            if (arg) {
                NameandCode = arg.Result.split('___');
                document.getElementById("<%=txtAcadYear.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfACY_ID.ClientID %>").value = NameandCode[1];
                __doPostBack('<%= txtAcadYear.ClientID%>', 'TextChanged');
            }
        }--%>

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


        Sys.Application.add_load(
                function CheckForPrint() {
                    if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                        document.getElementById('<%= h_print.ClientID %>').value = '';
                        showModelessDialog('../Reports/ASPX Report/rptreportViewerModel.aspx?paging=1', '', "dialogWidth:800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                    }
                }
                    );
                var color = '';
                function highlight(obj) {
                    var rowObject = getParentrow(obj);
                    var parenttable = document.getElementById("<%=gvStudEnquiry.ClientID %>");
            if (color == '') {
                color = getrowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getrowColor() {
                if (rowObject.style.backgroundColor == '')
                    return parenttable.style.backgroundColor;
                else
                    return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentrow(obj) {
            do {
                obj = obj.parentElement;
            } while (obj.tagName.toUpperCase() != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click(); //fire the click event of the child element
                    }
                    if (chkstate = true) {

                    }
                }
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        function confirm_cancel() {
            if (confirm("You are about to cancel this enquiry.Do you want to proceed?") == true)
                return true;
            else
                return false;
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_stu" runat="server" Behaviors="Close,Move"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <%-- <table width="100%" id="table1" border="0">
        <tr style="font-size: 12pt;">
            <td width="50%" align="left" class="title">
                &nbsp; 
            </td>
        </tr>
    </table>--%>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            New Enquiry
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="Label1" runat="server" CssClass="error"></asp:Label>
                            <table align="center" border="0" cellpadding="5" cellspacing="0"
                                style="width: 100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year </span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Contact Me</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlContactMe" SkinID="smallcmb" runat="server" AutoPostBack="true">
                                            <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                            <asp:ListItem Value="YES">YES</asp:ListItem>
                                            <asp:ListItem Value="NO">NO</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Follow Up Status</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlFollowupStatus" SkinID="smallcmb" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Prev School GEMS</span> </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlPrevSchool_BSU" SkinID="smallcmb" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <table border="0" align="center" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td colspan="4" align="left">
                                                    <asp:CheckBox ID="chkDues" runat="server" Text="Check for Dues" AutoPostBack="true"
                                                        Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="10%"><span class="field-label">GEMS Staff</span>
                                                </td>

                                                <td align="left" width="10%">
                                                    <asp:Label ID="lblStaff" CssClass="field-value" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td align="left" width="10%"><span class="field-label">Ex Students</span>
                                                </td>

                                                <td align="left" width="10%">
                                                    <asp:Label ID="lblEx" runat="server" Text="" CssClass="field-value"></asp:Label>
                                                </td>

                                                <td align="left" width="10%"><span class="field-label">Siblings</span>
                                                </td>

                                                <td align="left" width="10%">
                                                    <asp:Label ID="lblSib" CssClass="field-value" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td width="10%" align="left"><span class="field-label">Sister Concern</span>
                                                </td>

                                                <td align="left" width="10%">
                                                    <asp:Label ID="lblSis" CssClass="field-value" runat="server" Text=""></asp:Label>
                                                </td>

                                                <td align="left" width="10%"><span class="field-label">General Public</span>
                                                </td>

                                                <td align="left" width="10%">
                                                    <asp:Label ID="lblGP" CssClass="field-value" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td colspan="2"></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>

                                        </table>
                                        <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found" HeaderStyle-Height="30"
                                            PageSize="20">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this);" runat="server"
                                                            ToolTip="Click here to select/deselect all rows"></asp:CheckBox>
                                                        <%-- <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" Enabled='<%# BIND("CHK") %>'></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RowNumber" HeaderText="SI.NO"></asp:BoundField>
                                                <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("Eqs_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqId" runat="server" Text='<%# Bind("EQM_ENQID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Enq No">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqHeader" runat="server" CssClass="gridheader_text" Text="Enq No">
                                                        </asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnqid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnEnqid_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqNo" runat="server" Text='<%# Bind("eqs_applno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Enq Date">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" CssClass="gridheader_text" Text="Enq.Date">
                                                        </asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnq_Date" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnEnq_Date_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("eqm_enqdate", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnGrade_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ContactMe">
                                                    <HeaderTemplate>
                                                        ContactMe
                                                        <br />
                                                        <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="true" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged" Visible="false">
                                                        </asp:DropDownList>
                                                        <%-- <table>
                                                            <tr>
                                                                <td align="center">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                   
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stream">
                                                    <HeaderTemplate>
                                                        Stream<br />
                                                        <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="true" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <%-- <table>
                                                            <tr>
                                                                <td align="center">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Applicant Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblApplName" runat="server" CssClass="gridheader_text" Text="Applicant Name"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtApplName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnAppl_Name" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnAppl_Name_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkApplName" CommandName="Select" OnClientClick='<%# GetNavigateUrl(Eval("eqs_id").tostring()) %>'
                                                            OnClick="lnkApplName_Click" runat="server" Text='<%# Bind("appl_name") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ex Student">
                                                    <HeaderTemplate>
                                                        Gender<br />
                                                        <asp:DropDownList ID="ddlgvGender" runat="server" CssClass="listbox"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlgvGender_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>M</asp:ListItem>
                                                            <asp:ListItem>F</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <%-- <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGender" runat="server" Text='<%# Bind("EQM_APPLGENDER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Gems Staff">
                                                    <HeaderTemplate>
                                                        GEMS Staff
                                                        <br />
                                                        <asp:DropDownList ID="ddlgvStaff" runat="server" CssClass="listbox"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlgvStaff_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>YES</asp:ListItem>
                                                            <asp:ListItem>NO</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <%-- <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgStaff" runat="server" HorizontalAlign="Center" ImageUrl='<%# BIND("IMGGEMS") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ex Student">
                                                    <HeaderTemplate>
                                                        Ex Student
                                                        <br />

                                                        <asp:DropDownList ID="ddlgvEx" runat="server" CssClass="listbox" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlgvEx_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>YES</asp:ListItem>
                                                            <asp:ListItem>NO</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <%-- <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgEx" runat="server" HorizontalAlign="Center" ImageUrl='<%#  BIND("IMGEX") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sibling">
                                                    <HeaderTemplate>
                                                        Sibling<br />
                                                        <asp:DropDownList ID="ddlgvSib" runat="server" CssClass="listbox" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlgvSib_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>YES</asp:ListItem>
                                                            <asp:ListItem>NO</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <%--<table>
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgSib" runat="server" HorizontalAlign="Center" ImageUrl='<%# BIND("IMGSIB") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sibling">
                                                    <HeaderTemplate>
                                                        Sister Concern
                                                        <br />
                                                        <asp:DropDownList ID="ddlgvSis" runat="server" CssClass="listbox" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlgvSis_SelectedIndexChanged">
                                                            <asp:ListItem>ALL</asp:ListItem>
                                                            <asp:ListItem>YES</asp:ListItem>
                                                            <asp:ListItem>NO</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <%--<table>
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                       
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgSis" runat="server" HorizontalAlign="Center" ImageUrl='<%# BIND("IMGSIS") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SEN">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgSEN" runat="server" HorizontalAlign="Center" ImageUrl='<%# BIND("EQS_bSEN") %>' />
                                                        <div>
                                                            <asp:LinkButton ID="lnkSEN" runat="server" Text="Note" CausesValidation="False" Visible='<%# BIND("bSEN") %>'
                                                                OnClientClick="return false;"></asp:LinkButton>
                                                            <div id="panel1" runat="server" style="background-color: white; border: solid 1px #1b80b6; color: #1b80b6; width: 135px; height: 100px; overflow: auto;"
                                                                visible='<%# BIND("bSEN") %>'>
                                                                <asp:Literal ID="ltSEN" runat="server" Text='<%# BIND("EQS_SEN") %>'></asp:Literal>
                                                            </div>
                                                        </div>
                                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" OffsetX="-50"
                                                            PopupControlID="Panel1" Position="Bottom" TargetControlID="lnkSEN">
                                                        </ajaxToolkit:PopupControlExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Age" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPPL_DATE" runat="server" Text='<%# bind("Appl_Age") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ack1 Email">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnResend1" OnClick="lbtnResend1_Click" runat="server" CausesValidation="false">Resend</asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="cberesend1" runat="server" TargetControlID="lbtnResend1"
                                                            ConfirmText="Are sure you want to re send the Applicant enquiry Acknowledgment1 Email ?">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EQS_bFOLLOWUP" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEQS_bFOLLOWUP" runat="server" Text='<%# bind("EQS_bFOLLOWUP") %>'></asp:Label>
                                                        <asp:Label ID="lblReason" runat="server" Text='<%# bind("EFR_ICONS") %>'></asp:Label>
                                                        <asp:Label ID="lblTooltip" runat="server" Text='<%# bind("EFR_REASON") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="Followup" ImageUrl="~\images\tick.gif" ButtonType="Image"
                                                    HeaderText="Follow up">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="Prev School" Visible="True">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrev_School" runat="server" Text='<%# bind("Prev_School_GEMS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="Edit" Text="Edit Stages" HeaderText="Edit" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="Print" Text="Print" HeaderText="Print">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="EQM_ENQTYPE" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EQM_ENQTYPE") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEQM_ENQTYPE" runat="server" Text='<%# Bind("EQM_ENQTYPE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="lblEQS_RFS_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEQS_RFS_ID" runat="server" Text='<%# bind("EQS_RFS_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="RePrint" HeaderText="Print Ack" Text="Print" Visible="false">
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:ButtonField>
                                            </Columns>
                                            <SelectedRowStyle HorizontalAlign="Left" />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnApprove" runat="server" Text="Short List" CssClass="button" ValidationGroup="groupM1"
                                            TabIndex="4" />
                                        <asp:Button ID="btnReject" runat="server" Text="Delete" CssClass="button" ValidationGroup="groupM1"
                                            TabIndex="5" OnClientClick="return confirm_cancel();" />
                                        <asp:Button ID="btnPrint" runat="server" CssClass="button" OnClick="btnPrint_Click"
                                            Text="Print Screeening Test" />
                                        <asp:Button ID="btnprintlabel" runat="server" CssClass="button" OnClick="btnprintlabel_Click"
                                            Text="Print Label" Visible="False" />
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <asp:HiddenField ID="h_print" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="lstValues" runat="server" name="lstValues"
                                type="hidden" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>
