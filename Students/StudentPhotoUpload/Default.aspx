﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Student Image Uploading"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <div align="center" class="matters">
                    <table  width="100%">
                        <tr>
                            <td width="10%">
                                <span class="field-label"> Business Unit</span>
                            </td>
                            <td width="40%">
                                <asp:DropDownList ID="ddbsu" runat="server">
            </asp:DropDownList>
                            </td>
                            <td width="20%">
                                <span class="field-label"> Upload Zip File <span style="font-size: smaller">(less than 2 mb)</span> </span>
                            </td>
                            <td  width="30%">
            <asp:FileUpload ID="FileUploadPhoto" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                  <asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="button" OnClick="btnupload_Click" Width="134px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblmessage" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                  <asp:Label ID="lblStatus" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                               <asp:CheckBox ID="CheckError" runat="server" AutoPostBack="True" CssClass="field-label"
                        Text="Show Error List" Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                  <asp:GridView ID="GridUploaded" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" AllowPaging="True" Width="100%"
                        PageSize="15">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    File Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("File_Name") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Student_Name") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Image
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" ImageUrl='<%# Eval("Image_View") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Referenced
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Student_Reference") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Student ID
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Student_Id") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Status
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("Status") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="LightGray" ForeColor="Black" />
                    </asp:GridView>
                            </td>
                        </tr>
                    </table>
                  
                </div>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Select a Business Unit"
        ControlToValidate="ddbsu" Display="None" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Upload a Zip File (Student Image)"
                    ControlToValidate="FileUploadPhoto" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="FileUploadPhoto"
        Display="None" ErrorMessage="Please upload (*.zip) Files" SetFocusOnError="True"
        Style="position: static" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.zip)$"></asp:RegularExpressionValidator>--%>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                    ShowSummary="False" />
                <asp:HiddenField ID="HiddenPostBack" Value="0" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
