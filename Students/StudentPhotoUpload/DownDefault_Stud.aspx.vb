﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports ICSharpCode.SharpZipLib

Partial Class DownDefault_Stud
    Inherits System.Web.UI.Page

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Function GetDefSchool(ByVal BSU_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetSchoolType", pParms)
        Return reader
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Session.Timeout = 5
            HiddenPostBack.Value = 1
            Session("Data") = Nothing
            BindBsu()

            Using DefReader As SqlDataReader = GetDefSchool(Session("sBsuid"))
                While DefReader.Read
                    Session("School_Type") = Convert.ToString(DefReader("BSU_bGEMSSchool"))


                End While
            End Using


        End If

        'If HiddenPostBack.Value = 0 Then
        '    Session.Timeout = 5
        '    HiddenPostBack.Value = 1
        '    Session("Data") = Nothing
        '    BindBsu()
        'End If

    End Sub

    Public Sub BindBsu()
        Try
            Dim sql_Connection As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()


            Dim Sql_Query As String = "SELECT BSU_SHORTNAME,BSU_ID,BSU_NAME FROM BUSINESSUNIT_M ORDER BY BSU_NAME"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(sql_Connection, CommandType.Text, Sql_Query)
            ddbsu.DataSource = ds.Tables(0)
            ddbsu.DataTextField = "BSU_NAME"
            ddbsu.DataValueField = "BSU_ID"
            ddbsu.DataBind()
            Dim list As New ListItem
            list.Text = "Select a Business Unit"
            list.Value = "-1"
            ddbsu.items.insert(0, list)

            ddbsu.SelectedValue = Session("sBsuid")

            ddbsu.Enabled = False

        Catch ex As Exception



        End Try


    End Sub


    Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupload.Click ' Handles btnupload.Click

        Dim Filter As String = WebConfigurationManager.AppSettings("ExtensionFilters").ToString()
        Dim dir(), d, fl(), f As String
        Dim di As DirectoryInfo
        Dim fi As FileInfo
        Dim photoPath = Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString()
        Dim formatarray As String() = Filter.Split(";")
        Dim ExtractPath As String = photoPath & "\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString



        dir = Directory.GetDirectories(ExtractPath)
        For Each d In dir
            di = New DirectoryInfo(d)
            If di.Name <> "EMPPHOTO" Then
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", di.Name)

                Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_STUDNO_FILTER", pParms)
                    While reader.Read
                        Dim lstrEMPNO = Convert.ToString(reader("STU_NO"))
                        Dim ExtractPath2 As String = photoPath & "\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\" & di.Name
                        Dim UploadPath As String = photoPath & "\OASIS_HR\Download_Misc\" & ddbsu.SelectedValue & "\" & lstrEMPNO & ".jpg"
                        fl = Directory.GetFiles(ExtractPath2)
                        For Each f In fl
                            fi = New FileInfo(f)
                            Dim ExtractPath3 As String = photoPath & "\OASIS_HR\ApplicantPhoto\" & ddbsu.SelectedValue & "\" & di.Name & "\" & fi.Name
                            File.Copy(ExtractPath3, UploadPath, True)

                        Next
                    End While
                End Using
            End If
        Next


    End Sub


End Class
