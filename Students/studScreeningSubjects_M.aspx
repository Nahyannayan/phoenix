<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studScreeningSubjects_M.aspx.vb" Inherits="Students_studScreeningSubjects_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
function confirm_delete()
{

  if (confirm("You are about to delete this record.Do you want to proceed?")==true)
    return true;
  else
    return false;
   
 }
</script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book"></i>  Screening Subject Master
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" style="text-align: left" Width="342px" />
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
         <td align="center"  valign="bottom">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
        <tr>
            <td align="left">
                <table align="center" width="100%" cellpadding="5" cellspacing="0" >
                   
                         <tr>
                        <td align="left" width="20%">
                        <span class="field-label">Subject</span> </td>
                        <td align="left" width="40%">
                               <asp:TextBox ID="txtSubject" runat="server" TabIndex="1" MaxLength="100"></asp:TextBox>
                      </td>  
                       <td></td>  
                       <td></td>       
                    </tr>
                                       
                     </table>
               </td>
        </tr>
       
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" TabIndex="5" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" TabIndex="6" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" TabIndex="7" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" UseSubmitBehavior="False" TabIndex="8" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" OnClientClick="return confirm_delete();" TabIndex="9" /></td>
        </tr>
        <tr>
            <td>
          <asp:HiddenField ID="hfSCB_ID" runat="server" />
          <asp:RequiredFieldValidator ID="rfsUBJECT" runat="server" ErrorMessage="Please enter the field Subject" ControlToValidate="txtSubject" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>&nbsp;
          </td>
          
        </tr>
    </table>

            </div>
        </div>
    </div>

</asp:Content>

