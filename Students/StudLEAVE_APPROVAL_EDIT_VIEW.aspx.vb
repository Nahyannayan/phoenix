Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_StudStaffAuthorized_Grade_View
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try



                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059027") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                   

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_6.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    callYEAR_DESCRBind()


                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))

        'str_Sid_img = h_Selected_menu_4.Value.Split("__")
        'getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))

        str_Sid_img = h_selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))

        str_Sid_img = h_selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))

        str_Sid_img = h_selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))


    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    'Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
    '    If gvAuthorizedRecord.Rows.Count > 0 Then
    '        Dim s As HtmlControls.HtmlImage
    '        Try

    '            s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_4_img")
    '            If p_imgsrc <> "" Then
    '                s.Src = p_imgsrc
    '            End If
    '            Return s.ClientID

    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End If
    '    Return ""
    'End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            Dim CurrentDatedType As String = String.Empty

            Dim ddlOPENONLINEH As New DropDownList

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_GRM_DIS As String = String.Empty
            Dim str_filter_SCT_DESCR As String = String.Empty
            Dim str_filter_STUD_ID As String = String.Empty
            Dim str_filter_SNAME As String = String.Empty
            Dim str_filter_REMARKS As String = String.Empty
            Dim str_filter_STATUS As String = String.Empty
            Dim str_filter_FROMDT As String = String.Empty
            Dim str_filter_TODT As String = String.Empty
            Dim ACD_ID As String = ddlAca_Year.SelectedItem.Value
            Dim ds As New DataSet

            str_Sql = " SELECT SID,STUD_ID,SNAME,GRM_DIS,FROMDT,TODT,REMARKS,STATUS,GRD_ORDER FROM( " & _
  " SELECT     STUDENT_LEAVE_APPROVAL.SLA_ID AS SID, STUDENT_M.STU_NO AS STUD_ID, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME), '') " & _
 "  + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME), '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME), '') AS SNAME, " & _
" STUDENT_LEAVE_APPROVAL.SLA_FROMDT AS FROMDT, STUDENT_LEAVE_APPROVAL.SLA_TODT AS TODT,STUDENT_LEAVE_APPROVAL.SLA_REMARKS AS REMARKS, " & _
" STUDENT_LEAVE_APPROVAL.SLA_APPRLEAVE AS STATUS,GRADE_BSU_M.GRM_DISPLAY + ' & ' + SECTION_M.SCT_DESCR AS GRM_DIS," & _
" GRADE_M.GRD_DISPLAYORDER AS GRD_ORDER FROM STUDENT_LEAVE_APPROVAL INNER JOIN STUDENT_M ON " & _
" STUDENT_LEAVE_APPROVAL.SLA_STU_ID = STUDENT_M.STU_ID INNER JOIN GRADE_BSU_M ON STUDENT_M.STU_ACD_ID = GRADE_BSU_M.GRM_ACD_ID " & _
" AND STUDENT_M.STU_GRD_ID = GRADE_BSU_M.GRM_GRD_ID INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID = SECTION_M.SCT_ID " & _
" AND STUDENT_M.STU_GRD_ID = SECTION_M.SCT_GRD_ID AND STUDENT_M.STU_ACD_ID = SECTION_M.SCT_ACD_ID INNER JOIN " & _
" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID WHERE(STUDENT_LEAVE_APPROVAL.SLA_ACD_ID = '" & ACD_ID & "') AND " & _
" (UPPER(RTRIM(STUDENT_LEAVE_APPROVAL.SLA_APPRLEAVE)) IN ('APPROVED', 'NOT APPROVED')))A WHERE STUD_ID<>''  "




            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_GRM_DIS As String = String.Empty
            Dim str_SCT_DESCR As String = String.Empty
            Dim str_STUD_ID As String = String.Empty
            Dim str_SNAME As String = String.Empty
            Dim str_REMARKS As String = String.Empty
            Dim str_STATUS As String = String.Empty
            Dim str_FROMDT As String = String.Empty
            Dim str_TODT As String = String.Empty

            If gvAuthorizedRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTUD_ID")
                str_STUD_ID = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STUD_ID = " AND a.STUD_ID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STUD_ID = "  AND  NOT a.STUD_ID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STUD_ID = " AND a.STUD_ID  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STUD_ID = " AND a.STUD_ID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STUD_ID = " AND a.STUD_ID LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STUD_ID = " AND a.STUD_ID NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSNAME")
                str_SNAME = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_SNAME = " AND a.SNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_SNAME = "  AND  NOT a.SNAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_SNAME = " AND a.SNAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_SNAME = " AND a.SNAME NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_SNAME = " AND a.SNAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_SNAME = " AND a.SNAME NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtGRM_DIS")
                str_GRM_DIS = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GRM_DIS = "  AND  NOT a.GRM_DIS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GRM_DIS = " AND a.GRM_DIS NOT LIKE '%" & txtSearch.Text & "'"
                End If


                'str_Sid_search = h_Selected_menu_4.Value.Split("__")
                'str_search = str_Sid_search(0)

                'txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSCT_DESCR")
                'str_SCT_DESCR = txtSearch.Text

                'If str_search = "LI" Then
                '    str_filter_SCT_DESCR = " AND a.SCT_DESCR LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "NLI" Then
                '    str_filter_SCT_DESCR = "  AND  NOT a.SCT_DESCR LIKE '%" & txtSearch.Text & "%'"
                'ElseIf str_search = "SW" Then
                '    str_filter_SCT_DESCR = " AND a.SCT_DESCR  LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "NSW" Then
                '    str_filter_SCT_DESCR = " AND a.SCT_DESCR NOT LIKE '" & txtSearch.Text & "%'"
                'ElseIf str_search = "EW" Then
                '    str_filter_SCT_DESCR = " AND a.SCT_DESCR LIKE  '%" & txtSearch.Text & "'"
                'ElseIf str_search = "NEW" Then
                '    str_filter_SCT_DESCR = " AND a.SCT_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                'End If





                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtFROMDT")
                str_FROMDT = txtSearch.Text
                Dim FROMDT As String = "  replace(CONVERT( CHAR(12), isnull(a.FROMDT,''), 106 ),' ','/') "
                If str_search = "LI" Then
                    str_filter_FROMDT = " AND " & FROMDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FROMDT = "  AND  NOT " & FROMDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FROMDT = " AND " & FROMDT & "  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FROMDT = " AND " & FROMDT & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FROMDT = " AND " & FROMDT & " LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FROMDT = " AND " & FROMDT & " NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_selected_menu_6.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtTODT")
                str_TODT = txtSearch.Text
                Dim TODT As String = "  replace(CONVERT( CHAR(12), isnull(a.TODT,''), 106 ),' ','/') "
                If str_search = "LI" Then
                    str_filter_TODT = " AND " & TODT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_TODT = "  AND  NOT " & TODT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_TODT = " AND " & TODT & "  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_TODT = " AND " & TODT & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_TODT = " AND " & TODT & " LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_TODT = " AND " & TODT & " NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_selected_menu_7.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtREMARKS")
                str_REMARKS = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_REMARKS = " AND a.REMARKS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_REMARKS = "  AND  NOT a.REMARKS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_REMARKS = " AND a.REMARKS  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_REMARKS = " AND a.REMARKS NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_REMARKS = " AND a.REMARKS LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_REMARKS = " AND a.REMARKS NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_selected_menu_8.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTATUS")
                str_STATUS = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STATUS = " AND a.STATUS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STATUS = "  AND  NOT a.STATUS  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STATUS = " AND a.STATUS   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STATUS = " AND a.STATUS  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STATUS = " AND a.STATUS LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STATUS = " AND a.STATUS  NOT LIKE '%" & txtSearch.Text & "'"
                End If





            End If





            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_GRM_DIS & str_filter_SCT_DESCR & str_filter_STUD_ID & str_filter_SNAME & str_filter_REMARKS & str_filter_FROMDT & str_filter_TODT & str_filter_STATUS & " order by GRD_ORDER,STATUS")
           
            If ds.Tables(0).Rows.Count > 0 Then

                gvAuthorizedRecord.DataSource = ds.Tables(0)
                gvAuthorizedRecord.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                'ds.Tables(0).Rows(0)(6) = True

                gvAuthorizedRecord.DataSource = ds.Tables(0)
                Try
                    gvAuthorizedRecord.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAuthorizedRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAuthorizedRecord.Rows(0).Cells.Clear()
                gvAuthorizedRecord.Rows(0).Cells.Add(New TableCell)
                gvAuthorizedRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAuthorizedRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAuthorizedRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtGRM_DIS")
            txtSearch.Text = str_GRM_DIS
            'txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSCT_DESCR")
            'txtSearch.Text = str_SCT_DESCR
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTUD_ID")
            txtSearch.Text = str_STUD_ID
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSNAME")
            txtSearch.Text = str_SNAME
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtREMARKS")
            txtSearch.Text = str_REMARKS
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTATUS")
            txtSearch.Text = str_STATUS
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtFROMDT")
            txtSearch.Text = str_FROMDT
            txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtTODT")
            txtSearch.Text = str_TODT

          
            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btnSearchFromDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchToDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    'Protected Sub btnSearchSCT_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    gridbind()
    'End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSTUD_ID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchGRM_DIS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchREMARKS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStatus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    
    Protected Sub gvStudentRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAuthorizedRecord.PageIndexChanging
        gvAuthorizedRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Sub callYEAR_DESCRBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.Items.Clear()
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "ACY_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            ddlAca_Year.ClearSelection()
            ddlAca_Year.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblSID As New Label
            Dim url As String
            Dim viewid As String
            lblSID = TryCast(sender.FindControl("lblSID"), Label)
            viewid = lblSID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\StudLEAVE_APPROVAL_EDIT.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub gvAuthorizedRecord_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAuthorizedRecord.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStatus As Label = DirectCast(e.Row.FindControl("lblStatus"), Label)
            If lblStatus.Text = "APPROVED" Then
                lblStatus.ForeColor = Drawing.Color.Green
            Else
                lblStatus.ForeColor = Drawing.Color.Red
            End If
        End If
    End Sub
End Class
