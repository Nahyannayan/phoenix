<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Covid_Stud_Concession_view.aspx.vb" Inherits="Students_Covid_Stud_Concession_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script language="javascript">

        function ShowRPTSETUP_S(id, status) {

            var sFeatures;
            sFeatures = "dialogWidth: 65%; ";
            sFeatures += "dialogHeight: 90%; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: yes; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '/phoenixbeta/Students/Covid_Stud_Concession_det.aspx?id=' + id + '&status=' + status + '';
            //alert(status) 
            return ShowWindowWithClose(url, 'search', '35%', '60%')
            //var win = window.open(url, '_blank');
            //win.focus();
            //return false;
            //result = window.showModalDialog(url, "", sFeatures);

            //if (result == '' || result == undefined) {
            //    return false;
            //}

        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Student Details
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <%--<table width="100%" id="Table1" border="0">
                
               
               <tr style="font-size: 12pt;">
              
                <td align="left" class="title" >&nbsp;STUDENT DETAILS</td>
              </tr>
              </table>--%>
                <table id="tblStud1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" Style="vertical-align: middle" EnableViewState="False"></asp:Label>

                            <table id="tblStud" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" class=" title-bg" colspan="4">Search Option</td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%">
                                        <span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcdID" runat="server"
                                            OnSelectedIndexChanged="ddlAcdID_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2" width="50%"></td>
                                </tr>
                                <tr runat="server" id="trCLM">
                                    <td align="left">
                                        <span class="field-label">Curriculum</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlCurri" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurri_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2" width="50%"></td>
                                </tr>
                                <tr >
                                     <td class="text-left flip"><span class="field-label" >Stream</span></td>
                                     <td class="text-left flip">  <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2" width="50%"></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Select Grade</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left">
                                        <span class="field-label">Select Section</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Student ID</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx1"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNo" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtStuNo">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>
                                    <td align="left">
                                        <span class="field-label">Name</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server">
                                        </asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSAME" runat="server" BehaviorID="AutoCompleteEx2"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNAME" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtNAME">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Passport No</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtPassNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">E-mail address</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server">
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="field-label">Mobile No</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtMobNo" runat="server">
                                        </asp:TextBox></td>
                                    <td align="left">
                                        <span class="field-label">Emergency Contact Number</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtEmgNo" runat="server">
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" TabIndex="4" CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvStudChange" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                PageSize="20" Width="100%" AllowSorting="True">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStu_id" runat="server"  Text='<%# Bind("Stu_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stud. No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("STU_NO") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNAME" runat="server" Text='<%# Bind("SNAME") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade &amp; Section">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrd_Sct" runat="server" Text='<%# Bind("grm_display") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Current Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# bind("Stu_currstatus") %>' ></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server" OnClientClick="<%# &quot;ShowRPTSETUP_S('&quot; & Container.DataItem(&quot;STU_ID&quot;) & &quot;','&quot; & Container.DataItem(&quot;Stu_currstatus&quot;) & &quot;');return false;&quot; %>" >Action</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle  />
                                <HeaderStyle  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>

                </table>


                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server"
                    type="hidden" value="=" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfSCT_ID" runat="server" />
                &nbsp;
    
    <asp:HiddenField ID="hfSTM_ID" runat="server" />


            </div>
        </div>
    </div>
    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>

