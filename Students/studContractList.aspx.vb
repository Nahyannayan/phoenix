﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studContractList
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try



                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or MainMnu_code <> "S100515" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"

                    callYEAR_DESCRBind()

                    Call gridbind()
                    If ViewState("datamode") = "edit" Then
                        Dim msg As String = String.Empty
                        msg = Encr_decrData.Decrypt(Request.QueryString("msg").Replace(" ", "+"))

                        lblError.Text = msg
                        ViewState("datamode") = "none"
                    End If
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String



        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))


    End Sub
   
    Protected Sub lblStud_Name_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("action") = "select"
    End Sub
    Protected Sub ddlgvDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub


    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudentRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudentRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudentRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudentRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudentRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudentRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvStudentRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudentRecord.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub btnSearchStud_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStud_ID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

  
    Protected Sub btnSearchGrade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnimg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim lblCONId As New Label
        Dim lblSTUDID As Label
        Dim lblGradeH As Label
        Dim lblContract As Label

        lblCONId = TryCast(sender.FindControl("lblCONId"), Label)
        lblSTUDID = TryCast(sender.FindControl("lblSTUDID"), Label)
        lblGradeH = TryCast(sender.FindControl("lblGradeH"), Label)
        lblContract = TryCast(sender.FindControl("lblContract"), Label)

        If lblContract.Text = "False" Then
            lblContract.Text = 1
        Else
            lblContract.Text = 0
        End If



        Dim ReturnFlag As Integer
        Dim Transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim PARAM(14) As SqlClient.SqlParameter
                PARAM(0) = New SqlClient.SqlParameter("@CON_STU_ID", lblSTUDID.Text)
                PARAM(1) = New SqlClient.SqlParameter("@CON_ACD_ID", ddlAca_Year.SelectedValue)
                PARAM(2) = New SqlClient.SqlParameter("@CON_GRD_ID", lblGradeH.Text)
                PARAM(3) = New SqlClient.SqlParameter("@CON_bCONTRACT", lblContract.Text)
                PARAM(4) = New SqlClient.SqlParameter("@CON_ID", lblCONId.Text)
           
                PARAM(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                PARAM(5).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(Transaction, CommandType.StoredProcedure, "STU.SAVE_STUD_CONTRACT_DETS", PARAM)
                ReturnFlag = PARAM(5).Value

                If ReturnFlag <> 0 Then
                    ReturnFlag = 1
                End If

            Catch ex As Exception
                ReturnFlag = 1
                lblError.Text = "Error Occured While Saving."
            Finally
                If ReturnFlag = 1 Then

                    lblError.Text = "Error Occured While Saving."
                    Transaction.Rollback()
                Else
                    lblError.Text = ""
                    Transaction.Commit()
                End If
            End Try

        End Using






        gridbind()
    End Sub

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            Dim CurrentDatedType As String = String.Empty

            Dim ddlOPENONLINEH As New DropDownList

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_JOINDT As String = String.Empty
            Dim str_filter_STUDID As String = String.Empty
            Dim str_filter_STUDNAME As String = String.Empty
            Dim str_filter_GRADE As String = String.Empty


            Dim ds As New DataSet


            str_Sql = "select DISTINCT(STU_No),STU_DOJ,Stu_ID,STU_PASPRTNAME,GRM_DISPLAY,STU_BSU_ID," _
                       & " STU_ACD_ID, STU_GRD_ID, GRD_DISPLAYORDER,ISNULL(CON_BCONTRACT, 0)BCONTRACT, CASE WHEN ISNULL(CON_BCONTRACT, 0)=1 THEN '~/images/tick.gif' ELSE '~/images/cross.gif' END CON_BCONTRACT,ISNULL(CON_ID,0) CON_ID " _
                      & " FROM STUDENT_M " _
                      & " INNER JOIN GRADE_BSU_M ON  STU_GRD_ID = GRM_GRD_ID AND STU_ACD_ID=GRM_ACD_ID " _
                      & " INNER JOIN  GRADE_M ON GRM_GRD_ID = GRD_ID " _
                      & " INNER JOIN  SECTION_M ON STU_SCT_ID = SCT_ID " _
                      & " LEFT OUTER JOIN STU.STUD_CONTRACT_DETS ON STU_ID = CON_STU_ID  "

            Dim ACD_ID_year As String

            ACD_ID_year = "WHERE STU_ACD_ID='" & ddlAca_Year.SelectedItem.Value & "'"
            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_JOINDT As String = String.Empty
            Dim str_STUDID As String = String.Empty
            Dim str_STUDNAME As String = String.Empty
            Dim str_GRADE As String = String.Empty




            Dim docSearch As String = ""

            If gvStudentRecord.Rows.Count > 0 Then

                Dim str_Sid_search() As String

            



                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvStudentRecord.HeaderRow.FindControl("txtstud_ID")
                str_STUDID = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STUDID = " AND isnull(STU_NO,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STUDID = "  AND  NOT isnull(STU_NO,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STUDID = " AND isnull(STU_NO,'')  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STUDID = " AND isnull(STU_NO,'') NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STUDID = " AND isnull(STU_NO,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STUDID = " AND isnull(STU_NO,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvStudentRecord.HeaderRow.FindControl("txtstud_Name")

                str_STUDNAME = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STUDNAME = " AND isnull(STU_PASPRTNAME,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STUDNAME = "  AND  NOT isnull(STU_PASPRTNAME,'')  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STUDNAME = " AND isnull(STU_PASPRTNAME,'')   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STUDNAME = " AND isnull(STU_PASPRTNAME,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STUDNAME = " AND isnull(STU_PASPRTNAME,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STUDNAME = " AND isnull(STU_PASPRTNAME,'')  NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvStudentRecord.HeaderRow.FindControl("txtStud_Grade")

                str_GRADE = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GRADE = " AND isnull(GRM_DISPLAY,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GRADE = "  AND  NOT isnull(GRM_DISPLAY,'') LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GRADE = " AND isnull(GRM_DISPLAY,'') LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GRADE = " AND isnull(GRM_DISPLAY,'')  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GRADE = " AND isnull(GRM_DISPLAY,'') LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GRADE = " AND isnull(GRM_DISPLAY,'') NOT LIKE '%" & txtSearch.Text & "'"
                End If

               


            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & ACD_ID_year & str_filter_STUDID & str_filter_STUDNAME & str_filter_GRADE & "ORDER BY GRD_DISPLAYORDER,GRM_DISPLAY")
            '& ViewState("str_filter_Year") & str_filter_C_DESCR & str_filter_STARTDT & str_filter_ENDDT & CurrentDatedType & str_filter_OPENONLINE & "  order by  a.Y_DESCR")

            If ds.Tables(0).Rows.Count > 0 Then

                gvStudentRecord.DataSource = ds.Tables(0)
                gvStudentRecord.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvStudentRecord.DataSource = ds.Tables(0)
                Try
                    gvStudentRecord.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvStudentRecord.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvStudentRecord.Rows(0).Cells.Clear()
                gvStudentRecord.Rows(0).Cells.Add(New TableCell)
                gvStudentRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudentRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudentRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

         
            txtSearch = gvStudentRecord.HeaderRow.FindControl("txtSTUD_ID")
            txtSearch.Text = str_STUDID
            txtSearch = gvStudentRecord.HeaderRow.FindControl("txtSTUD_Name")
            txtSearch.Text = str_STUDNAME
            txtSearch = gvStudentRecord.HeaderRow.FindControl("txtSTUD_Grade")
            txtSearch.Text = str_GRADE
           
            'Call callYEAR_DESCRBind()

            'Call ddlOpenOnLine_state(ddlOPENONLINEH.SelectedItem.Text)

            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    'Sub ddlOpenOnLine_state(ByVal selText As String)
    '    Try


    '        Dim ItemTypeCounter As Integer
    '        Dim ddlOpenonLineH As New DropDownList
    '        ddlOpenonLineH = gvStudentRecord.HeaderRow.FindControl("ddlOPENONLINEH")
    '        For ItemTypeCounter = 0 To ddlOpenonLineH.Items.Count - 1
    '            'keep loop until you get the counter for default OPENONLINE into  the SelectedIndex

    '            If ddlOpenonLineH.Items(ItemTypeCounter).Text = selText Then
    '                ddlOpenonLineH.SelectedIndex = ItemTypeCounter
    '            End If
    '        Next
    '        '  ddlOpenonLineH.DataBind()
    '    Catch ex As Exception

    '    End Try
    'End Sub
    'Protected Sub ddlOPENONLINEH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    'drop down list for Reg OPENONLINE
    '    ' Call ddlOpenOnLine_state(sender.selectedItem.Text)
    '    gridbind()

    'End Sub
    'Protected Sub ddlACY_DESCRH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    ViewState("Y_DESCR") = sender.SelectedItem.Text
    '    callYEAR_DESCRBind(ViewState("Y_DESCR"))
    '    gridbind()
    'End Sub


    'Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try

    '        Dim lblACD_ID As New Label
    '        Dim url As String
    '        Dim viewid As String

    '        lblACD_ID = TryCast(sender.FindControl("lblACD_ID"), Label)
    '        viewid = lblACD_ID.Text


    '        'define the datamode to view if view is clicked
    '        ViewState("datamode") = "view"
    '        'Encrypt the data that needs to be send through Query String

    '        MainMnu_code = Request.QueryString("MainMnu_code")

    '        viewid = Encr_decrData.Encrypt(viewid)
    '        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

    '        url = String.Format("~\Students\studAcademicyear_D.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
    '        Response.Redirect(url)
    '    Catch ex As Exception
    '        lblError.Text = "Request could not be processed "
    '    End Try

    'End Sub



    Protected Sub gvStudentRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudentRecord.PageIndexChanging
        gvStudentRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub

    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
       
        gridbind()
    End Sub

    Public Sub callYEAR_DESCRBind()

        Try
            Dim Active_year As String
            Using Activereader As SqlDataReader = AccessStudentClass.GetActive_ACD_4_Grade(Session("sBsuid"), Session("CLM"))
                While (Activereader.Read())

                    Active_year = Convert.ToString(Activereader("Y_DESCR"))
                End While

            End Using

            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAca_Year.Items.Clear()
                di = New ListItem("Not Selected", "0")

                ddlAca_Year.Items.Add(di)
                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAca_Year.Items.Add(di)


                    End While
                End If
            End Using

            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlAca_Year.Items.Count - 1
                If ddlAca_Year.Items(ItemTypeCounter).Text = Active_year Then
                    ddlAca_Year.SelectedIndex = ItemTypeCounter
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
End Class
