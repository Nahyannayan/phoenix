<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studApplicationDecision_M.aspx.vb" Inherits="Students_studApplicationDecision_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Application Decision
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left" >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1"  />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" cellpadding="5"
                                cellspacing="0" width="100%" class="BlueTableView">
                                
                                <tr>
                                    <td align="left" width="20%"><span class="field-label" >Description</span></td>
                                   
                                    <td   align="left">
                                        <asp:TextBox ID="txtDescr" runat="server" CausesValidation="True" TabIndex="2" ValidationGroup="groupM1"
                                           ></asp:TextBox></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%" ><span class="field-label" >Type</span> </td>
                                   
                                    <td   align="left" >
                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True">
                                            <asp:ListItem Text="SCR" Value="SCR" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="APR" Value="APR"></asp:ListItem>

                                        </asp:DropDownList>

                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td align="left"  >
                                        <asp:CheckBox ID="chkProceed" runat="server"  class="field-label" Text="Disable proceeding to next stage" /></td>
                                    <td></td>
                                    <td>

                                    </td>

                                </tr>


                                <tr >
                                    <td align="left" class="title-bg" colspan="3" >
                                         <span class="field-label" >Details</span> </td>
                                    
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label" >Reason</span> </td>
                                 
                                    <td align="left" >
                                        <asp:TextBox ID="txtReason" runat="server" CausesValidation="True" ValidationGroup="groupM1"  TabIndex="2"></asp:TextBox></td>
                                    <td align="left"  >
                                        <asp:Button ID="btnAddNew" runat="server" Text="Add" CssClass="button" ValidationGroup="groupM1" TabIndex="3" /></td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="3">
                                        <br />
                                      <%--  <table id="Table5" runat="server" align="center" cellpadding="0"
                                            cellspacing="0" >
                                            <tr>
                                                <td align="center"  >--%>
                                                    <asp:GridView ID="gvReason" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row"
                                                        HeaderStyle-Height="30" PageSize="20" AllowSorting="True">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="-" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAprId" runat="server" Text='<%# Bind("APR_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Reason">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblReason" runat="server" Text='<%# Bind("APR_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:ButtonField CommandName="Edit" HeaderText="Edit" Text="Edit">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="Delete" HeaderText="Delete" Text="Delete">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <HeaderStyle  CssClass="gridheader_pop" />
                                                        <RowStyle CssClass="griditem" />
                                                        <SelectedRowStyle CssClass="Green" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>

                                               <%-- </td>
                                            </tr>
                                        </table>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td   valign="bottom" colspan="3" align="center">
                                        <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Add" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Edit" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" ValidationGroup="groupM1" CausesValidation="False" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                </table>

                <asp:HiddenField ID="hfAPL_ID" runat="server" />
                <asp:HiddenField ID="hfSTG_ID" runat="server" />
            </div>
        </div>
    </div> 
</asp:Content>

