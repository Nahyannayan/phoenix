<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stuServiceDisctnue.aspx.vb" Inherits="stuServiceDisctnue" Title="Service Discontinue" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            result = window.showModalDialog("../Curriculum/clmPopupForm.aspx?multiselect=false&ID=STUDENT_GRADESRV&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs, "", sFeatures)
            if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-graduation-cap mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Service  Discontinue Requests"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="REQUEST"></asp:ValidationSummary>
                <br />
                <table align="center" width="100%">

                    <tr>
                        <td align="left" width="10%"><span class="field-label">Academic Year</span></td>
                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlACDYear" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="10%"><span class="field-label">Grade</span></td>
                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                            </asp:DropDownList></td>
                        <td align="left" width="10%"><span class="field-label">Section</span></td>
                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddlSection" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="10%"><span class="field-label">Services</span></td>

                        <td align="left" width="25%">
                            <asp:DropDownList ID="ddlServices" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" colspan="2">
                            <asp:Button ID="btnList" runat="server" OnClick="btnList_Click" Text="List" CssClass="button" /></td>
                        <td align="left" colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" width="10%"><span class="field-label">Discontinue Request Date</span></td>
                        <td align="left" width="25%">
                            <asp:TextBox ID="txtReqDate" runat="server" ValidationGroup="REQ"></asp:TextBox></td>
                        <td align="left" width="10%">
                            <asp:CheckBox ID="chkAll" runat="server" Text="Apply All" CssClass="field-label" AutoPostBack="True" OnCheckedChanged="chkAll_CheckedChanged"></asp:CheckBox></td>
                        <td align="left" colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:GridView ID="gvStudTPT" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" Width="100%" OnRowDataBound="gvStudTPT_RowDataBound">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                            <asp:Label ID="LBLSSD" runat="server" Text='<%# BIND("SSD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" OnCheckedChanged="chkAll_CheckedChanged1" AutoPostBack="True"></asp:CheckBox><br />
                                            Select All
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label><br />
                                            <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchStuNo" OnClick="btnSearchStuNo_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                            <asp:HiddenField ID="HF_SSV_ID" runat="server" Value='<%# Bind("SSV_ID") %>' />
                                            <asp:HiddenField ID="HF_SSV_ACD_ID" runat="server" Value='<%# Bind("SSV_ACD_ID") %>' />
                                        </ItemTemplate>

                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label><br />
                                            <asp:TextBox ID="txtStuName" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchStuName" OnClick="btnSearchStuName_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblH12" runat="server" Text="Grade"></asp:Label><br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnGrade_Search" OnClick="btnGrade_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section">
                                        <HeaderTemplate>

                                            <asp:Label ID="lblH123" runat="server" Text="Section"></asp:Label><br />
                                            <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnSection_Search" OnClick="btnSection_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SSV_FROMDATE" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Service Date"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Disc: Date">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDisDate" runat="server" Text='<%# BIND("SSD_SERVICE_DIS_DT","{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="calStartDate" runat="server" Format="dd/MMM/yyyy" TargetControlID="txtDisDate"></ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnAdd_Click" Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnEdit_Click" Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
                                Text="Save" ValidationGroup="REQUEST" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnCancel_Click" Text="Cancel" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calRequestDate" runat="server" TargetControlID="txtReqDate" Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>

                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                        type="hidden" value="=" /><input id="h_Selected_menu_4" runat="server" type="hidden"
                            value="=" /><asp:HiddenField ID="H_SSD_ID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="h_STU_IDs" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>

