Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studCancelAdmission_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                txtName.Text = Encr_decrData.Decrypt(Request.QueryString("name").Replace(" ", "+"))
                txtStuNo.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))

                ViewState("mode") = Request.QueryString("mode")
                'if query string returns Eid  if datamode is view state
              

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "S100300")) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

        Else


        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub SaveData()

        Dim str_query As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hfSTU_ID.Value.ToString)
                str_query = "exec saveSTUDCANCELADMISSION " + hfSTU_ID.Value.ToString + ",'" + Format(Date.Parse(txtCancelDate.Text), "yyyy-MM-dd") + "','" + txtRemarks.Text.Replace("'", "''") + "'"
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "STU_ID(" + hfSTU_ID.Value.ToString + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                lblError.Text = "Record Saved Successfully"
                transaction.Commit()
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub
#End Region

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", hfSTU_ID.Value.ToString)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_CancelAdm_Validate", pParms)
            While reader.Read
                Dim lstrACD = Convert.ToString(reader("ACD"))
                If lstrACD = "0" Then

                Else
                    'If Date.Parse(txtCancelDate.Text).Date > Now.Date Then
                    '    lblError.Text = "Cancel date cannot be a future date"
                    '    Exit Sub
                    'End If
                End If
            End While
        End Using
        
        SaveData()
        'Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
End Class

