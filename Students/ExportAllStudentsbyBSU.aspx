﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="ExportAllStudentsbyBSU.aspx.vb" Inherits="Students_ExportAllStudentsbyBSU" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Export Student List
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tblData" runat="server">
                    <tr id="trLabelError">
                        <td align="left" colspan="2">
                            <div id="lblNoAccess" runat="server">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table id="tblCategory" cellspacing="0" cellpadding="0" width="100%" runat="server">
                                <%--  <tr class="subheader_img">
                                    <td align="left" colspan="4" valign="middle">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Export Student List</span></font>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="left"><span class="field-label">Academic Year</span><span class="text-danger">*</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcdYear" runat="server"></asp:DropDownList>

                                    </td>
                                    <td align="left"><span class="field-label">As on date</span><span class="text-danger">*</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="calendarButtonExtender0" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgCalendar" TargetControlID="txtDate">
                                        </ajaxToolkit:CalendarExtender>


                                    </td>
                                </tr>

                                <tr align="center">
                                    <td colspan="4">
                                        <asp:Button ID="btnExportExcel" runat="server" Text="Download Excel" CssClass="button" />

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

