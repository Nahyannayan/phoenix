<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studentidcardcategoryAssign.aspx.vb" Inherits="Students_studHouse_M"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudHouse.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state1(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }



    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="ID Card Category"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error" Style="vertical-align: middle"></asp:Label>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left" class="matters" width="20%"><span class="field-label">Select Academic Year</span>
                                                </td>
                                                <td align="left" class="matters" width="30%">
                                                    <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" class="matters" width="20%"> <span class="field-label">Select Grade</span>
                                                </td>
                                                <td align="left" class="matters" width="30%">
                                                    <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="matters"><span class="field-label">Select Section</span>
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:DropDownList ID="ddlSection" SkinID="smallcmb" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" class="matters"><span class="field-label">ID Category</span>
                                                </td>
                                                <td align="left" class="matters">
                                                    <asp:DropDownList ID="ddlHouse" runat="server" Enabled="False">
                                                    </asp:DropDownList>
                                                </td>


                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" ValidationGroup="groupM1"
                                                        TabIndex="4" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4" valign="top">
                                        <asp:GridView ID="gvStudHouse" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            PageSize="50" Width="100%" AllowSorting="True">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Select<br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state1(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STP_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStpId" runat="server" Text='<%# Bind("STP_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SL.No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSlNo" runat="server" Text='<%# getSerialNo() %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stud. No">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStHeader" runat="server" CssClass="gridheader_text" Text="Stud. No"></asp:Label><br />
                                                        <asp:TextBox ID="txtStuNoSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStuNo_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnStuNo_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label><br />
                                                        <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnStudName_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsctId" runat="server" Text='<%# Bind("STU_SCT_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="House">
                                                    <HeaderTemplate>
                                                        Category
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHouse" runat="server" Text='<%# Bind("CAT_DES") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" ValidationGroup="groupM1"
                                            TabIndex="4" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfSCT_ID" runat="server" />
                <asp:HiddenField ID="hfACY_DESCR" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
