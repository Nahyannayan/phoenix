Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studScreen_M
    Inherits System.Web.UI.Page
    Dim studClass As New studClass
    Dim Encr_decrData As New Encryption64

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "S050060" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                      ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    GridBind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
    End Sub

    Protected Sub gvStudScreen_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudScreen.RowCommand
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStudScreen.Rows(index), GridViewRow)
            Dim url As String
            Dim lblGrdId As New Label
            Dim lblGrade As New Label
            Dim lblStmId As New Label
            Dim lblStream As New Label
            ViewState("datamode") = "view"
            With selectedRow
                lblGrdId = .Cells(0).FindControl("lblGrdId")
                lblGrade = .Cells(1).FindControl("lblGrade")
                lblStmId = .Cells(2).FindControl("lblStmId")
                lblStream = .Cells(3).FindControl("lblStream")
            End With
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studScreen_D.aspx?academicyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) + "&acd_id=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedValue.ToString) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&grdid=" + Encr_decrData.Encrypt(lblGrdId.Text) + "&stream=" + Encr_decrData.Encrypt(lblStream.Text) + "&stmid=" + Encr_decrData.Encrypt(lblStmId.Text) + "&MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            'url = String.Format("~\Students\studScreen_D.aspx?academicyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) + "&grade=" + Encr_decrData.Encrypt(lblGrade.Text) + "&acd_id= " + ViewState("SelectedYear").ToString + "&MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.TextChanged
        Try
            ViewState("SelectedYear") = ddlAcademicYear.SelectedValue
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function returnpath(ByVal grdid As String, ByVal stmid As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT count(screening_d.guid) from screening_d,screening_m where screening_d.scd_scm_id=screening_m.scm_id " _
                                     & " and scm_acd_id =" + ddlAcademicYear.SelectedValue + " and screening_m.scm_grd_id='" + grdid + "'" _
                                     & "and scm_bsu_id='" + Session("sbsuid") + "' and scm_smt_id=" + stmid + ""
            Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)


            'Dim Flag_status As Boolean '= Convert.ToBoolean(Status)
            If count > 0 Then
                Return "~/Images/tick.gif"
            Else
                Return "~/Images/cross.gif"
            End If
        Catch ex As Exception
            Return "~/Images/cross.gif"
        End Try

    End Function

    Private Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT distinct grm_grd_id,grm_display,grm_stm_id,stm_descr,grd_displayorder FROM " _
                                      & "grade_bsu_m,stream_m,grade_m WHERE grade_bsu_m.grm_stm_id=stream_m.stm_id AND " _
                                      & " grade_bsu_m.grm_grd_id = grade_m.grd_id And grm_acd_id =" + ddlAcademicYear.SelectedValue _
                                      & "  ORDER BY grd_displayorder"

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudScreen.DataSource = ds
            gvStudScreen.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#End Region
End Class
