<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studCancelAdmissionRev_M.aspx.vb" Inherits="Students_studCancelAdmissionRev_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function getDate(val) {
            var sFeatures;
            sFeatures = "dialogWidth: 227px; ";
            sFeatures += "dialogHeight: 252px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var nofuture = "../Accounts/calendar.aspx?nofuture=yes";
            if (val == 0) {
                result = window.showModalDialog(nofuture, "", sFeatures)

            }
            else {
                result = window.showModalDialog("../Accounts/calendar.aspx", "", sFeatures)
            }
            if (result == '' || result == undefined) {
                return false;
            }


            if (val == 1) {
                document.getElementById('<%=txtCancelDate.ClientID %>').value = result;
            }


            return false;
        }

        function confirm_cancel() {

            if (confirm("You are about to reverse this cancel admission.Do you wish to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Cancel Admission - Reverse
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCancelDate"
                                Display="None" ErrorMessage="Please enter the date" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table align="center" width="100%" cellpadding="5" cellspacing="0"
                                class="BlueTableView">
                                <%--  <tr class="subheader_img">
                        <td align="left" colspan="9" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                               </span></font></td>
                    </tr>--%>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Student ID</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtStuNo" runat="server" Enabled="False">
                                        </asp:TextBox></td>
                                    <td align="left" width="20%"><span class="field-label">Name</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtName" runat="server" Enabled="False">
                                        </asp:TextBox></td>
                                </tr>


                                <tr id="tr_DATE" runat="Server">

                                    <td align="left" width="20%"><span class="field-label">Date</span></td>


                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCancelDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnEnqDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnEnqDate" TargetControlID="txtCancelDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCancelDate"
                                            Display="Dynamic" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtCancelDate"
                                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                                    <td colspan="2"></td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Remarks</span></td>

                                    <td align="left" >
                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" SkinID="MultiText"></asp:TextBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td valign="bottom" align="center">
                <asp:Button ID="btnUpdate" OnClientClick="javascript:return confirm_cancel();" runat="server" CssClass="button" Text="Update" ValidationGroup="groupM1" CausesValidation="False" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks"
                                Display="None" ErrorMessage="Please enter data in the field remarks" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                <asp:HiddenField ID="hfSTU_ID" runat="server" />
                             
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

