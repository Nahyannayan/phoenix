﻿<%@ Page Language="VB"  AutoEventWireup="true" CodeFile="StudPro_details.aspx.vb" Inherits="StudRecordEdit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <title>:::: GEMS :::: Student Profile ::::</title>
<%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../cssfiles/StudDashBoard.css" rel="stylesheet" type="text/css" />--%>
    
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta content="MShtml 6.00.2900.3268" name="GENERATOR">
    <script language="javascript" type="text/javascript">
//          window.setTimeout('setpath()',700);
//      function setpath()
//        {
//          var objFrame=document.getElementById("ifMainDetail"); 
//                   
//          objFrame.scr="../Tabpages/stuContactdetails.aspx" 
// alert( objFrame.scr)
//          var objFrame=document.getElementById("ifContDetail"); 
//          var objFrame=document.getElementById("ifSibDetail"); 
//          var objFrame=document.getElementById("ifFeeDetail"); 
//          var objFrame=document.getElementById("ifAttDetail"); 
//          var objFrame=document.getElementById("ifCurrDetail"); 
//          var objFrame=document.getElementById("ifBehDetail"); 
//          var objFrame=document.getElementById("ifTranDetail"); 
//          var objFrame=document.getElementById("ifLibDetail"); 
//          var objFrame=document.getElementById("ifCommHDetail"); 
//          var objFrame=document.getElementById("ifOthDetail"); 
//          
//          
// 
//        }
//    

    function  printTable()
    { 
        var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
        disp_setting+="scrollbars=yes,width=1280, height=1000, left=2, top=2"; 
        var content_vlue = document.getElementById('tblStudProfile').innerHTML;

        var docprint=window.open("","",disp_setting); 
        docprint.document.open();
            docprint.document.write('<html><head>'); 
        docprint.document.write('<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />'); 
        docprint.document.write('</head><body onLoad="self.print();" style="margin-top:0px;margin-left:0px;margin-right:0px;" ><center>');          
        docprint.document.write(content_vlue); 
               docprint.document.write('</center></body></html>'); 
        docprint.document.close(); 
        docprint.focus(); 
    }
    function  printAll()
    {

var sFeatures;
            sFeatures="dialogWidth: 950px; ";
            sFeatures+="dialogHeight: 750px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: yes; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var url;
           url='../Students/Tabpages/StudPro_details_printAll.aspx?id=0';
                    //alert(status) 
                           
            result = window.showModalDialog(url,"", sFeatures);
                 if (result=='' || result==undefined)
               {
        return false;
             } 

    }
    
    </script>

        <style type="text/css">

            table td span.field-value
            {
                width:100% !important;
            }

    table.menu_a tr td  table tr td a { 
    display: block;
  width: 100%;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  color: #495057;
  background-color: #b4b2b2 !important;
  background-clip: padding-box;
  border: 1px solid #8dc24c;
  border-radius: 0.25rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}

        table.menu_a tr td  table tr td a:hover {
                background-color: #ffffff !important;
            }

      table.menu_a tr td  table tr td a:visited {
            background-color: #b4b2b2 !important;
        }

         table.menu_a tr td  table tr td a:active {
            background-color: #b4b2b2 !important;
        }

    </style>

    </head>
<body text="#000000" bgcolor="#ffffff" leftmargin="0" topmargin="0">
<form id="Form1" runat="server">
 <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <div style="padding:1px;margin-bottom:1px;margin-top:1px;">&nbsp;&nbsp;&nbsp; <asp:LinkButton ID="lbPrintTab" runat="server" Text="Print Selected Tab" OnClientClick="javascript: window.print();return false;" ></asp:LinkButton>
          <asp:LinkButton ID="lbPrintAll" runat="server" Text="Print All" OnClientClick="javascript: printAll();return false;" >
         </asp:LinkButton></div> <table id="tblStudProfile"> <tr>
          <td align="center" class="title" colspan="6" valign="middle">
<table width="100%" cellSpacing="0" cellPadding="0" align="center" border="0">
  <tbody>
  <tr>
    <td align="left" width="15%">
        <asp:Image ID="imglogo" runat="server" height="80"  />
  </td>
    <td align="left" width="100%">
      <table width="70%" border="0" align="center">
        <tbody>
        <tr>
          <td align="center"><span id="BsuName" class="field-label" runat="server"></span> </td></tr>
        <tr>
          <td align="center"><span id="bsuAddress"  runat="server"></span></td></tr>
        <tr>
          <td align="center"><span id="bsupostbox"  runat="server"></span> , <span id="bsucity"  runat="server"></span> </td></tr>
        <tr>
          <td align="center"><span id="bsutelephone"  runat="server"></span>, <span id="bsufax" runat="server"></span>,<span id="bsuemail" runat="server"></span> : 
            <span id="bsuwebsite"  runat="server"></span></td></tr>
        <tr>
          <td align="center"></td></tr></tbody></table></td>
    </tr>
  <tr>
     
    <td  align="center" colSpan="2" > <hr></hr>
     <%--   <IMG 
      src="../Images/colourbar.jpg" width="100%" height="3px">--%>

    </td></tr></tbody></table>
          </td>
      </tr><tr>
          
      <td>
         
         
         
         
         
        <table align="center" >
       <tr>
       <td align="left" >
        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                    EnableViewState="False"  ValidationGroup="groupM1" />

                
               
       </td>
       
       </tr>
       <tr>
       <td>
         <table align="left"  cellpadding="0" cellspacing="0" width="100%">
                    <tr class="title-bg">
                        <td align="left"  >
                          Student Profile</td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <table id="Table1" align="left" cellpadding="0" cellspacing="0" width="100%" runat="server">
                                <tr>
                                    <td align="left" >
                                        <span  class="field-label"> Name</span></td>
                           
                                    <td align="left" colspan="3">
                                       <asp:Label ID="ltStudName" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="center" colspan="2" rowspan="11" >
                                       <asp:Image ID="imgEmpImage" runat="server" Height="178px" ImageUrl="~/Images/Photos/no_image.gif"
                                        Width="175px" /></td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span  class="field-label"> Student ID </span></td>
                                 
                                    <td align="left" colspan="3">
                                       <asp:Label ID="ltStudId" runat="server"  CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr runat="server" id="trCurr">
                                    <td align="left" >
                                        <span  class="field-label"> Curriculum</span></td>
                                  
                                    <td  align="left" colspan="3">
                                        <asp:Label ID="ltCLM" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                      <span  class="field-label">   Grade</span></td>
                                   
                                    <td  align="left">
                                        <asp:Label ID="ltGrd" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" >
                                        <span  class="field-label"> Section</span></td>
                                  
                                    <td align="left">
                                        <asp:Label ID="ltSct" runat="server" Text="-" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                    <span  class="field-label"> House</span></td>
                                 
                                    <td align="left" colspan="3"> <asp:Label ID="ltHouse" runat="server" Text="-"  CssClass="field-value"></asp:Label>
                                    </td>
                                  
                                </tr>
                                <tr runat="server" id="trshf">
                                    <td align="left" >
                                       <span  class="field-label">  Shift</span></td>
                                  
                                    <td colspan="3" align="left" >
                                       <asp:Label ID="ltShf" runat="server" CssClass="field-value"></asp:Label><br />
                                    </td>
                                </tr>
                                <tr runat="server" id="trstm">
                                    <td align="left" >
                                        <span  class="field-label"> Stream</span></td>
                                  
                                    <td  colspan="3" align="left">
                                        <asp:Label ID="ltStm" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                       <span  class="field-label">  Current Status</span></td>
                                
                                    <td  colspan="3" align="left">
                                        <asp:Label ID="ltStatus" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <span  class="field-label"> Father's Name</span></td>
                                 
                                    <td  colspan="3" align="left">
                                       <asp:Label ID="ltFather" runat="server" CssClass="field-value"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                       <span  class="field-label">  Mother's Name</span></td>
                                
                                    <td  colspan="3" align="left">
                                       <asp:Label ID="ltMother" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                 <tr>
                                    <td align="left" >
                                        <span  class="field-label"> Parent User Name</span></td>
                              
                                    <td align="left" >
                                        <asp:Label ID="ltParUserName" runat="server" CssClass="field-value"></asp:Label></td>
                                        
                                      

                                      <td align="left">
                                        <span  class="field-label"> Ministry List</span></td>
                                 
                                    <td colspan="0" align="left">
                                        <asp:Label ID="ltminlist" runat="server" CssClass="field-value"></asp:Label></td>
                                        
                                </tr>
                                <tr>
                                      <td align="left" >
                                        <span  class="field-label"> Student User Name</span></td>
                                
                                    <td align="left">
                                        <asp:Label ID="ltStuUserName" runat="server"  CssClass="field-value"></asp:Label></td>
                                    <td align="left" >
                                        <span  class="field-label"> Ministry Type</span></td>
                                
                                    <td align="left">
                                        <asp:Label ID="ltmintype" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                
                                
                                <tr>
                                    <td align="left" >
                                        <span  class="field-label"> TC Last Att Date</span></td>
                               
                                    <td  align="left" >
                                         <asp:Label ID="ltLDA" runat="server" CssClass="field-value"></asp:Label></td>
                                    <td align="left" >
                                        <span  class="field-label"> TC Leave Date</span></td>
                                 
                                    <td align="left">
                                        <asp:Label ID="ltLD" runat="server" CssClass="field-value"></asp:Label></td>
                                </tr>
                                
                                
                            </table>
                        </td>
                    </tr>
                </table>
       </td>
       </tr>
       <tr>
       <td style="height: 11px;" align="right">
           
           <!--<a href="javascript:window.print()" ><img   src="../Images/Misc/print.gif" style="width: 30px; border-top-style: none; border-right-style: none; border-left-style: none;  height: 30px; border-bottom-style: none; font-weight: bold; " alt="Print this page"></a> &nbsp;  &nbsp; -->    
       </td>
       </tr>
       <tr>
       <td align="center">
  <div id="topDiv">
                     <asp:Menu ID="mnuMaster" runat="server" Font-Names="Verdana" Font-Size="8pt" OnMenuItemClick="mnuMaster_MenuItemClick"
                    Orientation="Horizontal"  CssClass="menu_a">
                    <Items>
                         <asp:MenuItem  Selected="True" Text="Main" Value="0"></asp:MenuItem>
                        <asp:MenuItem  Text="Contact Info" Value="1"></asp:MenuItem>
                        <asp:MenuItem  Text="Sibling Info" Value="2"></asp:MenuItem>
                        <asp:MenuItem  Text="Fee History" Value="3"></asp:MenuItem>
                        <asp:MenuItem Text="Attendance" Value="4"></asp:MenuItem>
                        <asp:MenuItem  Text="Curriculum" Value="5"></asp:MenuItem>
                        <asp:MenuItem  Text="Time Table" Value="6"></asp:MenuItem>     
                    </Items>
                    <StaticMenuItemStyle Font-Names="Verdana" Font-Size="10pt" Font-Strikeout="False" />
                    <StaticSelectedStyle Font-Names="Verdana" Font-Size="10pt" />
                    <DynamicSelectedStyle Font-Names="Verdana" Font-Size="10pt" />
                    </asp:Menu>
        </div>
            <div id="bottomDiv">
           
                   <asp:Menu ID="mnuChild" runat="server" Font-Names="Verdana" Font-Size="8pt" 
                    Orientation="Horizontal" OnMenuItemClick="mnuChild_MenuItemClick"  CssClass="menu_a">
                    <Items>
                         <asp:MenuItem   Text="Behavior/Pastoral" Value="7"></asp:MenuItem>
                        <asp:MenuItem  Text="SP needs/Medical" Value="8"></asp:MenuItem>
                        <asp:MenuItem  Text="Services" Value="9"></asp:MenuItem>
                        <asp:MenuItem  Text="Library" Value="10"></asp:MenuItem>
                        <asp:MenuItem  Text="Comm History" Value="11"></asp:MenuItem>
                        <asp:MenuItem  Text="Other Details" Value="12"></asp:MenuItem>
                                            
                    </Items>
                    <StaticMenuItemStyle Font-Names="Verdana" Font-Size="10pt" Font-Strikeout="False" />
                    <StaticSelectedStyle Font-Names="Verdana" Font-Size="10pt" />
                    <DynamicSelectedStyle Font-Names="Verdana" Font-Size="10pt" />
                </asp:Menu>
           </div>
                           <asp:MultiView ID="mvMaster" runat="server" ActiveViewIndex="0">
                
                    <asp:View ID="vwMain" runat="server">
                       <div align="center" style="margin-top:0px;">
                       <iframe id="ifMainDetail"  height="700" src="../Students/Tabpages/STU_Maindetails.aspx"    scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                       </div>
                    </asp:View>
                    <asp:View ID="vwCont" runat="server">
                    <div align="center" style="margin-top:0px;">
                     <iframe id="ifContDetail" height="700" src="../Students/Tabpages/stuContactdetails.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                    </div>
                    </asp:View>
                    <asp:View ID="vwSib" runat="server">
                    <div align="center" style="margin-top:0px;">
                     <iframe id="ifSibDetail" height="700" src="../Students/Tabpages/StuSiblingDetails.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                   </div>
                    </asp:View>
                    <asp:View ID="vwFee" runat="server" EnableTheming="True">
                    <div align="center" style="margin-top:0px;">
                     <iframe id="ifFeeDetail" height="700" src="../Students/Tabpages/STU_TAB_FEE.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                    </div>
                    </asp:View>
                     <asp:View ID="vwAtt" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                       <iframe id="ifAttDetail" height="700" src="../Students/Tabpages/StuAttDetail.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                   
                    </asp:View>
                     <asp:View ID="vwCurr" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifCurrDetail" height="700" src="../Students/Tabpages/STU_TAB_curr.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                    <asp:View ID="vwTime" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifTimeDetail" height="700" src="../Students/Tabpages/STU_TAB_Time.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                    </div>
                    </asp:View>
                     <asp:View ID="vwBeh" runat="server" EnableTheming="True">
                    <div align="center" style="margin-top:0px;">
                     <iframe id="ifBehDetail" height="700" src="../Students/Tabpages/STU_TAB_PASTORAL.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                   </div>
                    </asp:View>
                     <asp:View ID="vwMed" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifMedDetail" height="700" src="../Students/Tabpages/STU_TAB_MED.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                     <asp:View ID="vwTran" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifTranDetail" height="700" src="../Students/Tabpages/STU_TAB_TPT.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                     <asp:View ID="vwLib" runat="server" EnableTheming="True">
                    <div align="center" style="margin-top:0px;">
                     <iframe id="ifLibDetail" height="700" src="../Students/Tabpages/STU_TAB_LIB.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                     <asp:View ID="vwCommH" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifCommHDetail" height="700" src="../Students/Tabpages/STU_TAB_COM.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                     <asp:View ID="vwOth" runat="server" EnableTheming="True">
                     <div align="center" style="margin-top:0px;">
                     <iframe id="ifOthDetail" height="700" src="../Students/Tabpages/STU_TAB_OTH.aspx"  scrolling="auto"  marginwidth="0px"  frameborder="0"  width="910"></iframe>
                     </div>
                    </asp:View>
                                    </asp:MultiView>
       </td></tr>
       </table> 
       
       
       
       
       </td>
       </tr>
       </table>
          </ContentTemplate>
        </asp:UpdatePanel>
       
 </form>
</body>
</html>


