Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studCancelPromotion
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Session("liUserList") = Nothing
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100460") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    ' GridBind()
                    'ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
                    callStream_Bind()
                    Dim li As New ListItem
                    li.Text = "All"
                    li.Value = "0"

                    PopulateSection()
                    tblTC.Rows(4).Visible = False
                    tblTC.Rows(5).Visible = False
                    tblTC.Rows(6).Visible = False
                    tblTC.Rows(7).Visible = False

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try

        End If
    End Sub
    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnSection_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

#Region "Private Methods"
    Private Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim GRM_ID_Select = "SELECT grm_id FROM grade_bsu_m,stream_m WHERE" _
                                     & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                                 & " grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString + " and grm_grd_id='" + ddlGrade.SelectedValue + "' and STM_ID= '" + ddlStream.SelectedValue + "'"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "') AND SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                 & " and  [SCT_GRM_ID] in (" + GRM_ID_Select + ")  ORDER BY SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = 0
        ddlSection.Items.Insert(0, li)
    End Sub



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_8.Value.Split("__")
        getid8(str_Sid_img(2))
    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid8(Optional ByVal p_imgsrc As String = "") As String
        If gvStud.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStud.HeaderRow.FindControl("mnu_8_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String


        str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                    & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,STU_DOJ,STU_GRD_ID,Type='View',SCT_DESCR " _
                    & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                    & " INNER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID " _
                    & " WHERE  STU_CURRSTATUS<>'CN' AND STU_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                    & " AND ISNULL(STU_bPROMOTED,'FALSE')='TRUE'"
        If ddlStream.SelectedValue <> "0" Then
            str_query += " AND B.GRM_STM_ID='" + ddlStream.SelectedValue.ToString + "'"
        End If

        If ddlGrade.SelectedValue <> "0" Then
            str_query += " AND STU_GRD_ID= '" + hfGRD_ID.Value + "'"
        End If

        If ddlSection.SelectedValue <> "0" Then
            str_query += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

        If txtStuNo.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + hfSTUNO.Value + "%'"
        End If
        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        If rdTc.Checked = True Then
            str_query += " AND STU_ID IN(SELECT TCM_STU_ID FROM TCM_M WHERE TCM_CANCELDATE IS NULL AND TCM_TCSO='TC')"
        End If

        If rdSo.Checked = True Then
            str_query += " AND STU_ID IN(SELECT TCM_STU_ID FROM TCM_M WHERE TCM_CANCELDATE IS NULL AND TCM_TCSO='SO')"
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""


        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        If gvStud.Rows.Count > 0 Then


            txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("GRM_DISPLAY", txtSearch.Text, strSearch)
            selectedGrade = txtSearch.Text


            txtSearch = gvStud.HeaderRow.FindControl("txtSection")
            strSidsearch = h_Selected_menu_8.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("SCT_DESCR", txtSearch.Text, strSearch)
            selectedSection = txtSearch.Text

            If strFilter <> "" Then
                str_query += strFilter
            End If

        End If
        If ViewState("MainMnu_code") <> "S100254" Then
            str_query += " ORDER BY STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME"
        End If
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStud.DataBind()
            Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
            gvStud.Rows(0).Cells.Clear()
            gvStud.Rows(0).Cells.Add(New TableCell)
            gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStud.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            ViewState("norecord") = "1"
        Else
            gvStud.DataBind()
            ViewState("norecord") = "0"
        End If

        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtGrade")
        txtSearch.Text = selectedGrade

        txtSearch = New TextBox
        txtSearch = gvStud.HeaderRow.FindControl("txtSection")
        txtSearch.Text = selectedSection




        set_Menu_Img()


    End Sub


    Sub SaveData()
        Dim str_query As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For i = 0 To gvStud.Rows.Count - 1
                    chkSelect = gvStud.Rows(i).FindControl("chkSelect")
                    If chkSelect.Checked = True Then
                        lblStuId = gvStud.Rows(i).FindControl("lblStuId")
                        str_query = "exec studCANCELPROMOTION " _
                                  & lblStuId.Text + "," _
                                  & hfACD_ID.Value
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    End If
                Next
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
          
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region



    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        'GridBind()
        'ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue)
        callStream_Bind()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)


        If ViewState("norecord") = "1" Then
            tblTC.Rows(4).Visible = False
            tblTC.Rows(5).Visible = False
        End If
    End Sub


    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        If ViewState("norecord") = "1" Then
            tblTC.Rows(4).Visible = False
            tblTC.Rows(5).Visible = False
        End If
        BindGrade()
        PopulateSection()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        If ViewState("norecord") = "1" Then
            tblTC.Rows(4).Visible = False
            tblTC.Rows(5).Visible = False
        End If
        PopulateSection()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Session("liUserList") = New List(Of String)
        tblTC.Rows(4).Visible = True
        tblTC.Rows(5).Visible = True
        tblTC.Rows(6).Visible = True
        tblTC.Rows(7).Visible = True
        hfACD_ID.Value = ddlAcademicYear.SelectedValue
        hfGRD_ID.Value = ddlGrade.SelectedValue
        hfSTUNO.Value = txtStuNo.Text
        hfNAME.Value = txtName.Text
        GridBind()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        SaveData()
        GridBind()
    End Sub

    Protected Sub btnUpdate1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate1.Click
        SaveData()
        GridBind()
    End Sub
    Public Sub callStream_Bind()
        Try
            ddlSection.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            ddlStream.SelectedValue = "1"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub
    Sub BindGrade()
        Try
            ddlSection.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlGrade.Items.Add(di)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub
End Class
