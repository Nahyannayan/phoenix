<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studComm_M_View.aspx.vb" Inherits="Students_studSubject_M_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <style>
        input{
            vertical-align :middle !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--<table align="center"  cellpadding="0" cellspacing="0" width="100%">
                    <tr>                        
                        <td align="center" colspan="2" valign="top">--%>
                           <%-- <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td >--%>
                                        <table align="center" width="100%" cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td align="left"  valign="bottom">
                                                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="left"   valign="top">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton></td>
                                            </tr>
                                            <tr>
                                                <td align="center"  valign="top">
                                                    <asp:GridView ID="gvCommonEmp" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                                                        CssClass="table table-bordered table-row" DataKeyNames="ID" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        HeaderStyle-Height="30" PageSize="20" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="HideID" Visible="False">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("HideID") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHideID" runat="server" Text='<%# Bind("HideID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ID" SortExpression="GRP_ID">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblIDH" runat="server">ID</asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtCode" runat="server" ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnCodeSearch" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="btnCodeSearch_Click" />
                                                                </HeaderTemplate>
                                                                <ItemStyle  />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle  />
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Description" SortExpression="DESCR">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtGroupname" runat="server" CssClass="inputbox" MaxLength="100"
                                                                        Text='<%# Bind("DESCR") %>' Width="1%"></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblDescrH" runat="server">Description</asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtName" runat="server" ></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchpar" runat="server" ImageAlign="Top" ImageUrl="../Images/forum_search.gif"
                                                                        OnClick="btnSearchpar_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle  />
                                                                <HeaderStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Paid">
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox3" runat="server" Checked='<%# Bind("bFlag") %>' />
                                                                </EditItemTemplate>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblPaidH" runat="server" Text="Paid"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("bFlag") %>' Enabled="False" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem"  />
                                                        <SelectedRowStyle BackColor="Aqua" />
                                                        <HeaderStyle CssClass="gridheader_pop"  />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                   <%-- </td>
                                </tr>
                            </table>--%>
                            <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                                    type="hidden" value="=" />
               <%-- </td>
                    </tr>                   
                </table>--%>

            </div>
        </div>
    </div>
</asp:Content>

