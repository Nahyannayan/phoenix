﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_StudTCNotification_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S900020") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    '   Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If
                BindGrade()
                GridBind()
                If gvStudGrade.Rows.Count = 0 Then
                    trGrid.Visible = False
                End If
                gvStudGrade.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If


    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID WHERE GRM_ACD_ID=" + Session("CURRENT_ACD_ID") _
                                 & " ORDER BY GRD_DISPLAYORDER"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRD_ID"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ROW_NUMBER()OVER(ORDER BY TNS_GRD_ID)-1 AS UNIQUEID,TNS_ID,TNS_BSU_ID,TNS_GRD_ID,TNS_EMP_ID,TNS_EMP_EMAIL,ISNULL(C.EMP_FNAME,'')+' '+ISNULL(C.EMP_MNAME,'')+' '+ISNULL(C.EMP_LNAME,'') AS EMP_NAME_SUP FROM TC_NOTIFICATIONSETUP_M " _
                                & " LEFT OUTER JOIN EMPLOYEE_M AS C ON TNS_EMP_ID=C.EMP_ID " _
                                & " WHERE  TNS_BSU_ID='" + Session("SBSUID") + "' and TNS_GRD_ID='" & ddlGrade.SelectedValue & "' ORDER BY TNS_GRD_ID "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStudGrade.DataSource = ds
        gvStudGrade.DataBind()
    End Sub
    Sub SaveData()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "EXEC saveTC_NOTIFICATIONSETUP_M" _
                                    & " @TNS_BSU_ID='" + Session("sbsuid") + "'," _
                                    & " @TNS_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                                    & " @TNS_EMP_ID=" + Val(hfEMP_ID_SUP.Value).ToString + "," _
                                    & " @TNS_EMP_EMAIL='" + txtSupervisorEmail.Text + "'," _
                                    & " @MODE='" + ViewState("datamode") + "'," _
                                    & " @TNS_EMP_ID_ORG='" + hfEMP_ID_SUP_ORG.Value + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Catch ex As Exception

        End Try

    End Sub
#End Region
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        GridBind()
        txtSupervisor.Text = ""
        txtSupervisorEmail.Text = ""
        trGrid.Visible = True
    End Sub
    Protected Sub gvStudGrade_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudGrade.PageIndexChanging
        gvStudGrade.PageIndex = e.NewPageIndex
        GridBind()
    End Sub
    Protected Sub gvStudGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudGrade.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        If index > 1 Then
            Dim selectedRow As GridViewRow = DirectCast(gvStudGrade.Rows(index), GridViewRow)
            With selectedRow
                Dim lblGrdId As Label = .FindControl("lblGrdId")
                Dim lblSupervisor As Label = .FindControl("lblSupervisor")
                Dim lblSUPEmpId As Label = .FindControl("lblSupEmpId")
                Dim lblSupEmail As Label = .FindControl("lblSupervisorEmail")
                If e.CommandName = "edit" Then
                    ddlGrade.ClearSelection()
                    ddlGrade.Items.FindByValue(lblGrdId.Text).Selected = True
                    hfEMP_ID_SUP.Value = lblSUPEmpId.Text
                    hfEMP_ID_SUP_ORG.Value = lblSUPEmpId.Text
                    txtSupervisor.Text = lblSupervisor.Text
                    txtSupervisorEmail.Text = lblSupEmail.Text
                    ViewState("datamode") = "edit"
                ElseIf e.CommandName = "delete" Then
                    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                    Dim str_query As String = "EXEC saveTC_NOTIFICATIONSETUP_M" _
                                            & " @TNS_BSU_ID='" + Session("sbsuid") + "'," _
                                            & " @TNS_GRD_ID='" + lblGrdId.Text + "'," _
                                            & " @TNS_EMP_ID=" + lblSUPEmpId.Text + "," _
                                            & " @TNS_EMP_EMAIL='" + lblSupEmail.Text + "'," _
                                            & " @MODE='delete'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    selectedRow.Visible = False
                    'GridBind()
                End If
            End With
        End If
    End Sub
    Protected Sub gvStudGrade_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvStudGrade.RowDeleting

    End Sub
    Protected Sub gvStudGrade_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStudGrade.RowEditing

    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        txtSupervisor.Text = ""
        txtSupervisorEmail.Text = ""
        ViewState("datamode") = "add"
        GridBind()


    End Sub
End Class
