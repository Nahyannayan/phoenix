<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BmIncidenttrackDetails.aspx.vb" Inherits="Students_BehaviorManagement_BmIncidenttrackDetails" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Incidents Track Records
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--  <tr class="subheader_img">
            <td align="left" colspan="18" valign="middle">
                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                    Incidents Track Records </span></font>
            </td>
        </tr>--%>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblAccText" runat="server" Text="Student Name" CssClass="field-label"></asp:Label></td>

                        <td align="left">
                            <asp:Label ID="lblStudentName" runat="server" CssClass="field-label"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:Label ID="txtGrade" runat="server" CssClass="field-label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            BorderStyle="None" CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="15" Width="100%">
                                            <EmptyDataRowStyle />
                                            <Columns>
                                                <asp:TemplateField Visible="False" HeaderText="INCIDENTID">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudId" runat="server" Text='<%# Bind("BM_ID") %>' ></asp:Label>
                                                        <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("STU_ID") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("BM_ENTRY_DATE") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Staff Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ActionType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("BM_INCIDENT_TYPE") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Points">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStmid" runat="server" Text='<%# Bind("BM_SCORE") %>' __designer:wfdid="w17"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Category">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("BM_CATEGORYNAME") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="LinkButton1" runat="server"  OnClientClick='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>' CommandArgument='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'>View</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <EditRowStyle />
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        <asp:HiddenField ID="HidStudId" runat="server"></asp:HiddenField>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                
            </div>
        </div>
    </div>
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>
<%--<%# GetNavigateUrl1(Eval("BM_ID").ToString()) %>--%>