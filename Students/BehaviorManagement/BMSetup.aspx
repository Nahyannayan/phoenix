<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BMSetup.aspx.vb" Inherits="Students_BehaviorManagement_BMSetup" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0">
        <tr>
            <td align="center" class="title" style="width: 228px">
                SetUp&nbsp;</td>
        </tr>
        <tr>
            <td align="left" valign="bottom" style="width: 228px">
                <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="matters" valign="top" style="width: 228px">
                <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    class="matters" style="height: 127px; width: 81%;">
                    <tr>
                        <td class="subheader_img" colspan="6" style="height: 13px">
                            <span style="font-size: 10pt; color: #ffffff">
                                <asp:Literal id="ltLabel" runat="server" Text="SetUp"></asp:Literal></span></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 237px; height: 14px">
                            AlertPoints</td>
                        <td align="center" style="font-size: 7pt; width: 4px; color: #1b80b6; height: 14px">
                            :</td>
                        <td align="left" colspan="4" style="height: 14px">
                            <asp:TextBox id="txtAlertPoints" runat="server" Width="152px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 237px; height: 14px">
                            ActionPoints</td>
                        <td align="center" style="width: 4px; height: 14px">
                            :</td>
                        <td align="left" colspan="4" style="height: 14px">
                            <asp:TextBox id="txtActionPoints" runat="server" Width="152px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 237px; height: 14px">
                            Alerts</td>
                        <td align="center" style="width: 4px; height: 14px">
                        </td>
                        <td align="left" colspan="4" style="height: 14px">
                            <asp:CheckBox id="chkEmail" runat="server" Text="By Email">
                            </asp:CheckBox>
                            <asp:CheckBox id="chkMessage" runat="server" Text="ByMessage">
                            </asp:CheckBox></td>
                    </tr>
                </table>
                &nbsp;
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" Width="48px" OnClick="btnAdd_Click" />&nbsp;
                <asp:Button id="btnSave" runat="server" CssClass="button" Text="Save" Width="46px" OnClick="btnSave_Click" />
                <asp:Button id="btnDelete" runat="server" CssClass="button" Text="Delete" Width="48px" OnClick="btnDelete_Click" />&nbsp;
            </td>
        </tr>
        <tr>
            <td class="matters" valign="top" style="width: 228px">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;</td>
        </tr>
        <tr>
            <td class="matters" style="height: 99px; width: 228px;" valign="bottom">
                <asp:HiddenField id="hfId1" runat="server">
                </asp:HiddenField>
                <asp:HiddenField id="hfTemp" runat="server">
                </asp:HiddenField>
            </td>
        </tr>
    </table>

</asp:Content>

