﻿<%@ Page Language="VB" AutoEventWireup="false"  MasterPageFile="~/mainMasterPage.master"  CodeFile="bm_CreateNewAchivement.aspx.vb" Inherits="Students_BehaviorManagement_bm_CreateNewAchivement_aspx" %>
<%@ Register Src="UserControl/bm_CreateNewAchivement_Simple.ascx" TagName="bm_CreateNewAchivement_Simple"
    TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
   
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Achievement"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <uc1:bm_CreateNewAchivement_Simple ID="bm_CreateNewAchivement_Simple" runat="server" />
            </div>
        </div>
    </div>

</asp:Content> 