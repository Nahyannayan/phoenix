<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BMActionRoutingSetup.aspx.vb" Inherits="Students_BehaviorManagement_BMActionRoutingSetup" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Set Action Hierarchy
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--    <tr>
            <td align="center" class="title">
                Set Action Hierarchy&nbsp;</td>
        </tr>--%>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="4" class="title-bg">

                                        <asp:Literal ID="ltLabel" runat="server" Text="SetUp"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td align="left" width="30%">
                                        <asp:Label ID="lblassigneddesig" runat="server" Text="Assigned Designations" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlistboxassigneddesig" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td width="10%"></td>
                                    <td align="center" width="30%">
                                      </td>

                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                  <asp:Button ID="btnassigneddesig" runat="server" Text="Show" CssClass="button" />
                                    </td>
                                </tr>
                                   <tr>
                                    <td colspan="4" align="center">
                             
                                    </td>
                                </tr>

                                <tr>
                                    <th align="left" width="30%"><span class="field-label">Designation From</span></th>
                                    <th align="left" width="30%"><span class="field-label">DesignationList</span></th>
                                    <th width="10%"></th>
                                    <th align="left" width="30%"><span class="field-label">Assigned To</span></th>
                                </tr>
                                <tr>
                                    <td align="left" width="30%">
                                        <asp:ListBox ID="lstFromDesig" runat="server"
                                            AutoPostBack="True" OnSelectedIndexChanged="lstFromDesig_SelectedIndexChanged"></asp:ListBox></td>
                                    <td align="left" width="30%">

                                        <script type="text/javascript">
                                            function checkAll(obj1) {
                                                var checkboxCollection = document.getElementById('<%=chkDesigList.ClientID %>').getElementsByTagName('input');
                                                for (var i = 0; i < checkboxCollection.length; i++) {
                                                    if (checkboxCollection[i].type.toString().toLowerCase() == "checkbox") {
                                                        checkboxCollection[i].checked = obj1.checked;
                                                    }
                                                }
                                            }
                                        </script>
                                        <asp:CheckBox ID="chkboxall" runat="server" Text="SELECT / UNSELECT ALL"
                                            AutoPostBack="True"
                                            Width="100%" />

                                        <asp:Panel ID="Panel1" runat="server">
                                            <div class="checkbox-list">
                                                <asp:CheckBoxList ID="chkDesigList" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    <td align="center" width="10%">
                                        <%-- <br />
                            <br />
                            <br />--%>
                                        <br />
                                        <asp:Button ID="btnAdd" runat="server" CssClass="button" Text=">>" OnClick="btnAdd_Click" />
                                        <br />
                                        <br />
                                        <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="<<" OnClick="btnRemove_Click" />
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:ListBox ID="chkAssigned" runat="server"
                                            SelectionMode="Multiple"></asp:ListBox></td>
                                </tr>
                            </table>
                            <br />


                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hfId1" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfTemp" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

