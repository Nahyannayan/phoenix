Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_BmPrintAction
    Inherits System.Web.UI.Page

    Private Sub BindStudentGrid(ByVal Criteria As String, ByVal strField As String)
        Try
            Dim dsStudetIncidents As New DataSet
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim strF As String() = strField.Split("As")

            'Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
            '               " BM_INCIDENT_TYPE,C.BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID,SCT_DESCR,GRM_DISPLAY,STM_DESCR,SHF_DESCR," & _
            '               " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName " & strField & " FROM BM.BM_MASTER A " & _
            '               " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
            '               " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
            '               " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
            '               " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =S.STU_SCT_ID " & _
            '               " INNER JOIN STREAM_M ON STREAM_M.STM_ID =S.STU_STM_ID " & _
            '               " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=S.STU_SHF_ID " & _
            '               " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= S.STU_GRM_ID " & _
            '               " INNER JOIN BM.BM_ACTION_MASTER C ON C.BM_STU_ID=S.STU_ID AND C.BM_ID=A.BM_ID " & _
            '               " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=C.BM_CATEGORYID " & _
            '               " " & Criteria & " ORDER BY 15 "

            Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
                           " BM_INCIDENT_TYPE,C.BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID,SCT_DESCR,GRM_DISPLAY,STM_DESCR,SHF_DESCR," & _
                           " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName " & strField & " FROM BM.BM_MASTER A " & _
                           " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
                           " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
                           " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
                           " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =S.STU_SCT_ID " & _
                           " INNER JOIN STREAM_M ON STREAM_M.STM_ID =S.STU_STM_ID " & _
                           " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=S.STU_SHF_ID " & _
                           " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= S.STU_GRM_ID " & _
                           " INNER JOIN BM.BM_ACTION_MASTER C ON C.BM_STU_ID=S.STU_ID AND C.BM_ID=A.BM_ID " & _
                           " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=C.BM_CATEGORYID " & _
                           " " & Criteria & " ORDER BY 15 "

            dsStudetIncidents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            GrdView.DataSource = dsStudetIncidents

            If dsStudetIncidents.Tables(0).Rows.Count = 0 Then
                dsStudetIncidents.Tables(0).Rows.Add(dsStudetIncidents.Tables(0).NewRow())
                GrdView.DataBind()
                Dim columnCount As Integer = GrdView.Rows(0).Cells.Count
                GrdView.Rows(0).Cells.Clear()
                GrdView.Rows(0).Cells.Add(New TableCell)
                GrdView.Rows(0).Cells(0).ColumnSpan = columnCount
                GrdView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GrdView.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                GrdView.DataBind()
                Dim intRow As Integer = 1
                For Each GrdvRow As GridViewRow In GrdView.Rows
                    If GrdvRow.RowType = DataControlRowType.DataRow Then
                        DirectCast(GrdvRow.FindControl("lblSlno"), Label).Text = intRow.ToString
                    End If
                    intRow = intRow + 1
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim DsInfo As DataSet
            Dim info As String = Session("FilterInfo")
            Dim val As String() = info.Split(";")
            lblTitle.Text = val(1) ''"51823"
            lblHead.Text = val(0)
            BindStudentGrid(val(2), val(3))
            DsInfo = GetBusinessunitInfo()

            If Not DsInfo Is Nothing Then
                If DsInfo.Tables(0).Rows.Count >= 1 Then
                    'Image2.ImageUrl = DsInfo.Tables(0).Rows(0).Item("IMG_IMAGE")
                    lblBsuName.Text = DsInfo.Tables(0).Rows(0).Item("BSU_NAME")
                    lblAddress1.Text = DsInfo.Tables(0).Rows(0).Item("BSU_ADDRESS")
                    lblAddress2.Text = DsInfo.Tables(0).Rows(0).Item("ADDRESS")
                    LblEmail.Text = DsInfo.Tables(0).Rows(0).Item("BSU_URL")
                    lblUser.Text = "User Name : " & Session("sUsr_name").ToString
                End If
            End If

            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If
    End Sub

    Private Function GetBusinessunitInfo() As DataSet
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim strSQL As String = ""

        strSQL = " SELECT (ISNULL(BSU_TEL,'')+ '  ' + ISNULL(BSU_FAX,'')+ '  '+ ISNULL(BSU_EMAIL,'')) As ADDRESS,BSU_ADDRESS,BSU_NAME,BSU_NAME_ARABIC,('P.O.BOX '+ISNULL(BSU_POBOX,'')+ '  ' + ISNULL(BSU_CITY,'')) AS CITY,BSU_URL,IMG_IMAGE " & _
                 " FROM  BUSINESSUNIT_M LEFT OUTER JOIN BSU_IMAGES ON BSU_IMAGES.IMG_BSU_ID = BUSINESSUNIT_M.BSU_ID AND IMG_TYPE='LOGO' " & _
                 " WHERE BSU_ID='" & Session("sBsuId") & "'"
        'strSQL = " SELECT (ISNULL(BSU_TEL,'')+ '  ' + ISNULL(BSU_FAX,'')+ '  '+ ISNULL(BSU_EMAIL,'')) As ADDRESS,BSU_ADDRESS,BSU_NAME,BSU_NAME_ARABIC,('P.O.BOX '+ISNULL(BSU_POBOX,'')+ '  ' + ISNULL(BSU_CITY,'')) AS CITY,BSU_URL,IMG_IMAGE " & _
        '       " FROM  BUSINESSUNIT_M LEFT OUTER JOIN BSU_IMAGES ON BSU_IMAGES.IMG_BSU_ID = BUSINESSUNIT_M.BSU_ID AND IMG_TYPE='LOGO' " & _
        '       " WHERE BSU_ID='125016'"
        Dim dsBSUInfo As DataSet
        dsBSUInfo = SqlHelper.ExecuteDataset(connection, CommandType.Text, strSQL)
        Return dsBSUInfo
    End Function

End Class
