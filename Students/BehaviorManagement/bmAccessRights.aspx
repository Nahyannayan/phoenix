<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="bmAccessRights.aspx.vb" Inherits="Students_BehaviorManagement_bmAccessRights" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        class="matters" style="height: 127px" width="100%">
        <tr>
            <td class="subheader_img" colspan="6" style="height: 13px">
                <span style="font-size: 10pt; color: #ffffff">
                    <asp:Literal id="ltLabel" runat="server" Text="SetUp"></asp:Literal></span></td>
        </tr>
        <tr style="font-size: 7pt">
            <td align="left" style="width: 229px; height: 8px">
                <span style="color: #1b80b6"><span>Grade</span></span></td>
            <td align="center" style="font-size: 7pt; width: 6px; color: #1b80b6; height: 8px">
                DesignationList</td>
            <td align="left" colspan="4" style="height: 8px">
                Assigned To</td>
        </tr>
        <tr>
            <td align="left" style="width: 200px; height: 14px" valign="top">
                <asp:ListBox id="lstFromDesig" runat="server" AutoPostBack="True" Height="181px"
                    OnSelectedIndexChanged="lstFromDesig_SelectedIndexChanged" Width="200px"></asp:ListBox></td>
            <td align="left" style="font-size: 7pt; width: 200px; color: #1b80b6; height: 14px"
                valign="top">
                <asp:Panel id="Panel1" runat="server" Height="179px" HorizontalAlign="Left" ScrollBars="Vertical"
                    Width="200px">
                    <asp:CheckBoxList id="chkDesigList" runat="server" Width="200px">
                    </asp:CheckBoxList>
                </asp:Panel></td>
            <td align="left" colspan="4" style="height: 14px" valign="top">
                <asp:ListBox id="chkAssigned" runat="server" Height="180px" SelectionMode="Multiple"
                    Width="200px"></asp:ListBox></td>
        </tr>
        <tr>
            <td align="left" style="width: 229px; height: 14px">
            </td>
            <td align="center">
               <%-- <asp:Button id="btnAdd" runat="server" onclick="btnAdd_Click" Text=">>" Width="39px" />
                <asp:Button id="btnRemove" runat="server" onclick="btnRemove_Click" Text="<<" Width="38px" />--%>
                &nbsp;
            </td>
            <td align="left" colspan="4" style="height: 14px">
            </td>
        </tr>
    </table>
    <asp:Button id="btnSave" runat="server" CssClass="button" onclick="btnSave_Click"
        Text="Save" Width="50px" />
</asp:Content>

