Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_BmIncidentTracks
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                'BindAccaDamicYear()
                PopulateGradeMaster()
                BindStudentGrid()
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
                Response.Cache.SetAllowResponseInBrowserHistory(False)
            End If
        Catch ex As Exception
        End Try
    End Sub

    'Private Sub BindAccaDamicYear()
    '    'To Get the Accadamic Year
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim str_Sql As String
    '        Dim ds As New DataSet
    '        str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
    '        " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
    '        " WHERE acd_id IN('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"

    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        ddlAcademicYear.Items.Clear()
    '        ddlAcademicYear.DataSource = ds.Tables(0)
    '        ddlAcademicYear.DataTextField = "ACY_DESCR"
    '        ddlAcademicYear.DataValueField = "ACD_ID"
    '        ddlAcademicYear.DataBind()
    '        ddlAcademicYear.ClearSelection()
    '        ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub

    Public Sub PopulateGradeMaster()
        ddlClass.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str1 As String = "SELECT COUNT(*) FROM BM.INCIDENT_ROLE_ACCESS WHERE ROLE_ID='" & Session("sroleid") & "' AND BSU_ID='" & Session("sBsuid") & "'"
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str1)
        Dim str_query As String


        'Dim str_query As String = "SELECT grd_display,grd_id,grd_displayorder from grade_m " _
        '                          & " where grd_id  in(select grm_grd_id from grade_bsu_m where grm_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
        '                          & " and grm_bsu_id='" + Session("sbsuid").ToString + "') order by grd_displayorder"
        If sbm = 0 Then
            str_query = "SELECT grd_display,grd_id,grd_displayorder from grade_m " _
                              & " where grd_id  in(select grm_grd_id from grade_bsu_m where " _
                              & "grm_bsu_id='" + Session("sbsuid").ToString + "') order by grd_displayorder"
        Else
            str_query = "SELECT grd_display,grd_id,grd_displayorder from grade_m " _
                              & " where grd_id  in(select grm_grd_id from grade_bsu_m where " _
                              & "grm_bsu_id='" + Session("sbsuid").ToString + "' AND GRM_ID IN (select GRM_ID FROM vw_BSU_TUTOR WHERE SEC_EMP_ID='" & Session("EmployeeId") & "' AND SEC_bsu_id='" & Session("sBsuid") & "')) order by grd_displayorder"
        End If



        Dim ds As DataSet 'not
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClass.DataSource = ds
        ddlClass.DataTextField = "grd_display"
        ddlClass.DataValueField = "grd_id"
        ddlClass.DataBind()
        Dim LstItem As New ListItem
        LstItem.Text = "ALL"
        LstItem.Value = "0"
        ddlClass.Items.Insert(0, LstItem)

    End Sub

    Protected Function GetNavigateUrl(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('bm_ActionView.aspx?inc_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
    End Function
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'ddlClass.Items.Clear()
            'PopulateGradeMaster()
            'BindStudentGrid()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindStudentGrid()
        Try
            Dim dsStudetIncidents As New DataSet
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim strCriteria As String
            If ddlClass.SelectedValue <> "0" Then
                'strCriteria = " AND GRADE_BSU_M.GRM_GRD_ID='" & ddlClass.SelectedValue & "'"
                strCriteria = " AND GRADE_BSU_M.GRM_GRD_ID= '" & ddlClass.SelectedItem.Value & "'"
            Else
                strCriteria = ""
            End If

            Dim str1 As String = "SELECT COUNT(*) FROM BM.INCIDENT_ROLE_ACCESS WHERE ROLE_ID=" & Session("sroleid") & " AND BSU_ID=" & Session("sBsuid")
            Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str1)
            Dim StrSQL As String
            If sbm = 0 Then
                StrSQL = "SELECT DISTINCT (Student_M.Stu_ID) as STUDID, ISNULL(STUDENT_M.STU_NO,'') as STUDNO ," & _
                                   "ISNULL(STUDENT_M.STU_PASPRTNAME,'') as SNAME,ISNULL(GRADE_BSU_M.GRM_DISPLAY,'') as GRADE, STUDENT_M.STU_bActive, " & _
                                   "STUDENT_M.STU_BSU_ID as BSU_ID, STUDENT_M.STU_ACD_ID,SHIFTS_M.SHF_DESCR As SHIFT, STREAM_M.STM_DESCR As STREAM, " & _
                                   "(SELECT COUNT(*) FROM BM.BM_STUDENTSINVOLVED WHERE BM_STU_ID=Stu_ID) AS INCCOUNT " & _
                                   "FROM  STUDENT_M " & _
                                   "INNER JOIN  ACADEMICYEAR_D ON STUDENT_M.STU_ACD_ID = ACADEMICYEAR_D.ACD_ID " & _
                                   "INNER JOIN GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID " & _
                                   "AND STUDENT_M.STU_GRD_ID = GRADE_BSU_M.GRM_GRD_ID  " & _
                                   "AND STUDENT_M.STU_ACD_ID = GRADE_BSU_M.GRM_ACD_ID " & _
                                   "AND STUDENT_M.STU_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & _
                                   "AND ACADEMICYEAR_D.ACD_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & _
                                   "INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
                                   "INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                                   "INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                                   "INNER JOIN BM.BM_STUDENTSINVOLVED ON BM_STUDENTSINVOLVED.BM_STU_ID=STUDENT_M.STU_ID " & _
                                   "INNER JOIN BM.BM_MASTER ON BM_MASTER.BM_ID=BM_STUDENTSINVOLVED.BM_ID " & _
                                   "AND STUDENT_M.STU_BSU_ID=" & Session("sbsuid") & strCriteria & " "
            Else
                StrSQL = "SELECT DISTINCT (Student_M.Stu_ID) as STUDID, ISNULL(STUDENT_M.STU_NO,'') as STUDNO ," & _
                                   "ISNULL(STUDENT_M.STU_PASPRTNAME,'') as SNAME,ISNULL(GRADE_BSU_M.GRM_DISPLAY,'') as GRADE, STUDENT_M.STU_bActive, " & _
                                   "STUDENT_M.STU_BSU_ID as BSU_ID, STUDENT_M.STU_ACD_ID,SHIFTS_M.SHF_DESCR As SHIFT, STREAM_M.STM_DESCR As STREAM, " & _
                                   "(SELECT COUNT(*) FROM BM.BM_STUDENTSINVOLVED WHERE BM_STU_ID=Stu_ID) AS INCCOUNT " & _
                                   "FROM  STUDENT_M " & _
                                   "INNER JOIN  ACADEMICYEAR_D ON STUDENT_M.STU_ACD_ID = ACADEMICYEAR_D.ACD_ID " & _
                                   "INNER JOIN GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID " & _
                                   "AND STUDENT_M.STU_GRD_ID = GRADE_BSU_M.GRM_GRD_ID  " & _
                                   "AND STUDENT_M.STU_ACD_ID = GRADE_BSU_M.GRM_ACD_ID " & _
                                   "AND STUDENT_M.STU_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & _
                                   "AND ACADEMICYEAR_D.ACD_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & _
                                   "INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
                                   "INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                                   "INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                                   "INNER JOIN BM.BM_STUDENTSINVOLVED ON BM_STUDENTSINVOLVED.BM_STU_ID=STUDENT_M.STU_ID " & _
                                   "INNER JOIN BM.BM_MASTER ON BM_MASTER.BM_ID=BM_STUDENTSINVOLVED.BM_ID " & _
                                   "AND STUDENT_M.STU_BSU_ID=" & Session("sbsuid") & strCriteria & " AND STUDENT_M.STU_GRM_ID " & _
             "IN(select GRM_ID FROM vw_BSU_TUTOR WHERE SEC_EMP_ID='" & Session("EmployeeId") & "' AND SEC_bsu_id='" & Session("sBsuid") & "')"

            End If

            'OLD QUERY:
            ''Dim StrSQL As String = "SELECT DISTINCT (Student_M.Stu_ID) as STUDID, ISNULL(STUDENT_M.STU_NO,'') as STUDNO ," & _
            ''                       "ISNULL(STUDENT_M.STU_PASPRTNAME,'') as SNAME,ISNULL(GRADE_BSU_M.GRM_DISPLAY,'') as GRADE, STUDENT_M.STU_bActive, " & _
            ''                       "STUDENT_M.STU_BSU_ID as BSU_ID, STUDENT_M.STU_ACD_ID,SHIFTS_M.SHF_DESCR As SHIFT, STREAM_M.STM_DESCR As STREAM, " & _
            ''                       "(SELECT COUNT(*) FROM BM.BM_STUDENTSINVOLVED WHERE BM_STU_ID=Stu_ID) AS INCCOUNT " & _
            ''                       "FROM  STUDENT_M " & _
            ''                       "INNER JOIN  ACADEMICYEAR_D ON STUDENT_M.STU_ACD_ID = ACADEMICYEAR_D.ACD_ID " & _
            ''                       "INNER JOIN GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID " & _
            ''                       "AND STUDENT_M.STU_GRD_ID = GRADE_BSU_M.GRM_GRD_ID  " & _
            ''                       "AND STUDENT_M.STU_ACD_ID = GRADE_BSU_M.GRM_ACD_ID " & _
            ''                       "AND STUDENT_M.STU_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & _
            ''                       "AND ACADEMICYEAR_D.ACD_BSU_ID = GRADE_BSU_M.GRM_BSU_ID " & _
            ''                       "INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
            ''                       "INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
            ''                       "INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
            ''                       "INNER JOIN BM.BM_STUDENTSINVOLVED ON BM_STUDENTSINVOLVED.BM_STU_ID=STUDENT_M.STU_ID " & _
            ''                       "INNER JOIN BM.BM_MASTER ON BM_MASTER.BM_ID=BM_STUDENTSINVOLVED.BM_ID " & _
            ''                       "AND STUDENT_M.STU_BSU_ID=" & Session("sbsuid") & strCriteria & " "
            ' ''"AND STU_ACD_ID=" & ddlAcademicYear.SelectedValue & strCriteria & " "
            StrSQL = StrSQL & strCriteria
            dsStudetIncidents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)
            gvStudGrade.DataSource = dsStudetIncidents

            If dsStudetIncidents.Tables(0).Rows.Count = 0 Then
                dsStudetIncidents.Tables(0).Rows.Add(dsStudetIncidents.Tables(0).NewRow())
                gvStudGrade.DataBind()
                Dim columnCount As Integer = gvStudGrade.Rows(0).Cells.Count
                gvStudGrade.Rows(0).Cells.Clear()
                gvStudGrade.Rows(0).Cells.Add(New TableCell)
                gvStudGrade.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudGrade.DataBind()
                Dim StrIncidents As String
                For Each GrdvRow As GridViewRow In gvStudGrade.Rows
                    If GrdvRow.RowType = DataControlRowType.DataRow Then
                        StrIncidents = DirectCast(GrdvRow.FindControl("Lblgrmprio"), Label).Text
                        If CInt(StrIncidents) >= 1 Then
                            DirectCast(GrdvRow.FindControl("LinkButton1"), LinkButton).Text = ""
                            DirectCast(GrdvRow.FindControl("LinkButton1"), LinkButton).Enabled = False
                            DirectCast(GrdvRow.FindControl("LnkEdit"), LinkButton).Enabled = True
                            DirectCast(GrdvRow.FindControl("LnkEdit"), LinkButton).Text = "View"
                        End If
                    End If
                Next
            End If
            'DirectCast(GrdvRow.FindControl("lblActionDate"), Label).Text = 

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClass.SelectedIndexChanged
        Try
            BindStudentGrid()
        Catch ex As Exception

        End Try
    End Sub

   
    Protected Sub gvStudGrade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudGrade.RowCommand
        Try
            Dim strArg As String
            If e.CommandName = "View" Then
                strArg = e.CommandArgument
                Response.Redirect("BmIncidenttrackDetails.aspx?StudId=" & strArg)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvStudGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudGrade.SelectedIndexChanged

    End Sub
End Class
