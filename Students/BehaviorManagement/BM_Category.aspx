<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BM_Category.aspx.vb" Inherits="Students_BehaviorManagement_BM_Category" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 40%;
            position: fixed;
            width: 70%;
        }
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Behaviour/Achievement Category
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr>
            <td align="center" class="title" style="width: 450px">
                Behaviour/Achievement Category&nbsp;</td>
        </tr>--%>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                EnableViewState="False" HeaderText="You must enter a value in the following fields:"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="4" class="title-bg">

                                        <asp:Literal ID="ltLabel" runat="server" Text="Add New Sub Category"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Parent Category</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="cmbCategory" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="cmbCategory_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Category Name</span><span class="text-danger font-small">*</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCategory" runat="server"></asp:TextBox>

                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Category Score</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtScore" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnchkscore" runat="server" CssClass="button" Text="Check Score" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Applicable Grades</span><span class="text-danger font-small">*</span></td>

                                    <td align="left" colspan="3">
                                        <asp:CheckBox ID="chkboxselectall" runat="server" Text="Check All" CssClass="field-label"
                                            AutoPostBack="True" />

                                        <asp:Button ID="btngradeavailable"
                                            CssClass="button" runat="server"
                                            Text="Check Grades Availability" />
                                        <br />
                                        <asp:CheckBoxList ID="chkboxlistgrade" runat="server" RepeatColumns="9" CssClass="field-label"
                                            AutoPostBack="True">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <br />
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button" Text="Edit" OnClick="btnEdit_Click" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="vg1" />
                            <%--OnClick="btnSave_Click"--%>
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" OnClick="btnDelete_Click" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <span class="text-danger font-small">*<b> Mandatory Fields</b></span></td>
                    </tr>
                    <tr>
                        <td><%--<asp:RequiredFieldValidator id="ReqGrade" runat="server" ControlToValidate="cmbGrade"
                    Display="Dynamic" ErrorMessage="Grade" ForeColor="White" InitialValue="0"></asp:RequiredFieldValidator>--%>
                            <asp:HiddenField ID="hfId1" runat="server"></asp:HiddenField>
                            <asp:RequiredFieldValidator ID="Req1" runat="server" ControlToValidate="txtCategory"
                                Display="Dynamic" ErrorMessage="Catagory Name" ForeColor="White" SetFocusOnError="True" ValidationGroup="vg1"></asp:RequiredFieldValidator>


                            <asp:HiddenField ID="hfTemp" runat="server"></asp:HiddenField>
                            <br />
                        </td>
                    </tr>
                </table>



                <asp:Panel ID="divAge" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div>
                            <asp:Button ID="btClose" type="button" runat="server"
                                Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                                ForeColor="White" Text="X"></asp:Button>
                            <div>
                                <div align="CENTER">
                                    <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>

                                <table align="center" width="100%" cellpadding="2" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnUpdate" Text="OK" CssClass="button" runat="server" />
                                            <asp:Button ID="btnUClose" Text="Cancel" CssClass="button" runat="server" />
                                        </td>
                                    </tr>

                                </table>
                            </div>

                        </div>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>

