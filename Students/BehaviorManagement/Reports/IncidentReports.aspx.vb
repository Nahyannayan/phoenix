Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_Reports_IncidentReports
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
    End Sub
    Public Sub ReportSearch(ByVal ds As DataSet)
        ds.Tables(0).TableName = "Incident"

        '--(Optional) I have used it to disable the properties 
        CrystalReportViewer1.DisplayGroupTree = False
        CrystalReportViewer1.HasCrystalLogo = False

        '--Initializing CrystalReport 
        Dim myReportDocument As ReportDocument
        myReportDocument = New ReportDocument()
        myReportDocument.Load(Server.MapPath("bm_IncidentReports.rpt"))
        myReportDocument.SetDataSource(ds)
        '--Binding report with CrystalReportViewer 
        CrystalReportViewer1.ReportSource = myReportDocument
        CrystalReportViewer1.DataBind()
    End Sub
End Class
