<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IncidentReports.aspx.vb" Inherits="Students_BehaviorManagement_Reports_IncidentReports" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Src="../UserControl/bm_IncidentSearch.ascx" TagName="bm_IncidentSearch"
    TagPrefix="uc1" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="/aspnet_client/System_Web/2_0_50727/CrystalReportWebFormViewer3/css/default.css"
        rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <uc1:bm_IncidentSearch ID="Bm_IncidentSearch1" OnbtnHandler="ReportSearch" runat="server" />
        <cr:crystalreportviewer id="CrystalReportViewer1" runat="server" autodatabind="true"
            displaytoolbar="False"></cr:crystalreportviewer>
    
    </div>
    </form>
</body>
</html>
