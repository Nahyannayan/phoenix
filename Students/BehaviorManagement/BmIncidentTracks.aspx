<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BmIncidentTracks.aspx.vb" Inherits="Students_BehaviorManagement_BmIncidentTracks" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Incidents Track Records
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr class="subheader_img">
            <td align="left" valign="middle">
                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                    Incidents Track Records</span></font></td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"> <span class="field-label">Select the</span>
                            <asp:Label ID="Label1" runat="server" Text="Grade" CssClass="field-label" ></asp:Label>
                               </td>
                            <td align="left" width="30%">
                           <asp:DropDownList ID="ddlClass" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlClass_SelectedIndexChanged"></asp:DropDownList>

                       </td> 
                         <td align="left" colspan="2" width="50%">
                              </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            BorderStyle="None" CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="15" Width="100%">
                                            <EmptyDataRowStyle  />
                                            <Columns>
                                                <asp:TemplateField Visible="False" HeaderText="StudentID">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudId" runat="server" Text='<%# Bind("STUDID") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="StudentNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("STUDNO") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStudName" runat="server" Text='<%# Bind("SNAME") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Shift">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server"  Text='<%# Bind("SHIFT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stream">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server"  Text='<%# Bind("STREAM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Incidents">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Lblgrmprio" runat="server"  Text='<%# Bind("INCCOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                      <asp:LinkButton ID="LinkButton1" runat="server"  OnClientClick='<%# GetNavigateUrl(Eval("STUDID").ToString()) %>'  CommandArgument='<%# Bind("INCCOUNT") %>'>View</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False"  CommandArgument='<%# Bind("STUDID") %>' CommandName="View">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerTemplate>
                                            
                                            </PagerTemplate>
                                            <RowStyle CssClass="griditem"  />
                                            <EditRowStyle  />
                                            <SelectedRowStyle  />
                                            <HeaderStyle  />
                                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</asp:Content>

