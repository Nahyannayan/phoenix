Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_BmIncidenttrackDetails
    Inherits System.Web.UI.Page



    'Protected Function GetNavigateUrl(ByVal pId As String) As String
    '    Return String.Format("javascript:var popup = window.showModalDialog('bm_ActionView.aspx?inc_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
    'End Function
    Protected Function GetNavigateUrl1(ByVal pId As String) As String
        'Return String.Format("ShowWindowWithClose('bm_ActionViewDetails.aspx?Info_id={0}', 'search', '55%', '85%'); return false; ", pId)
        Dim str As String = "ShowWindowWithClose('bm_ActionViewDetails.aspx?Info_id=" + pId + "', 'search', '55%', '85%'); "
        Return str

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Response.Cache.SetCacheability(HttpCacheability.Public)
                Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
                Response.Cache.SetAllowResponseInBrowserHistory(False)
                HidStudId.Value = Request.QueryString("StudId")
                BindGrid()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
                            " BM_INCIDENT_TYPE,C.BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
                            " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName FROM BM.BM_MASTER A " & _
                            " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
                            " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
                            " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
                            " RIGHT OUTER JOIN BM.BM_ACTION_MASTER C ON C.BM_STU_ID=S.STU_ID " & _
                            " LEFT OUTER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=C.BM_CATEGORYID " & _
                            " WHERE S.STU_ID =" & HidStudId.Value & "  ORDER BY BM_ENTRY_DATE DESC"


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudGrade.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade.DataBind()
                Dim columnCount As Integer = gvStudGrade.Rows(0).Cells.Count
                gvStudGrade.Rows(0).Cells.Clear()
                gvStudGrade.Rows(0).Cells.Add(New TableCell)
                gvStudGrade.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                GetStudentInfo(HidStudId.Value)
            Else
              GetStudentInfo(HidStudId.Value)

                gvStudGrade.DataBind()
                For Each GrdvRow As GridViewRow In gvStudGrade.Rows
                    If GrdvRow.RowType = DataControlRowType.DataRow Then
                        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
                        If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                            GrdvRow.ForeColor = Drawing.Color.Blue
                        Else
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                            GrdvRow.ForeColor = Drawing.Color.Red
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Function GetGradeDisplay(ByVal GrdId As String) As String
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "SELECT GRD_DESCR FROM GRADE_M WHERE GRD_ID='" & GrdId & "'"

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsDetails.Tables(0).Rows.Count >= 1 Then
                Return "Grade : " + dsDetails.Tables(0).Rows(0).Item("GRD_DESCR").ToString
            Else
                Return "Grade : "
            End If

        Catch ex As Exception

        End Try
    End Function


    Private Function GetStudentInfo(ByVal Stu_ID As String) As String
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "SELECT STU_NO, (ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName,STU_GRD_ID  FROM STUDENT_M WHERE STU_ID='" & Stu_ID & "'"

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsDetails.Tables(0).Rows.Count >= 1 Then
                txtGrade.Text = "Grade  " + dsDetails.Tables(0).Rows(0).Item("STU_GRD_ID").ToString
                lblStudentName.Text = dsDetails.Tables(0).Rows(0).Item("StudName")
            Else
                txtGrade.Text = "Grade  NIL"
                lblStudentName.Text = ""
            End If
           
        Catch ex As Exception

        End Try
    End Function

End Class
