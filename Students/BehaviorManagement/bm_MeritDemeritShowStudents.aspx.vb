﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports UtilityObj
Imports Microsoft.VisualBasic
Imports System.Web.Services
Imports System.Collections.Generic
Partial Class Students_BehaviorManagement_bm_MeritDemeritShowStudents
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try

               

                Session("liUserList") = New ArrayList

                GET_STUDENT_LIST()
            Catch ex As Exception
                Errorlog(ex.Message)
            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){" & vbCrLf)
            'Response.Write(" alert('uuu');")
            Response.Write("} </script>" & vbCrLf)
        End If

    End Sub
    Protected Sub btnSearchFeeID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        GET_STUDENT_LIST()
    End Sub
    Protected Sub btnSearchStudName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GET_STUDENT_LIST()
    End Sub

    Protected Sub btnSearchGrade_Click(sender As Object, e As ImageClickEventArgs)
        GET_STUDENT_LIST()
    End Sub
    Protected Sub btnSearchHouse_Click(sender As Object, e As ImageClickEventArgs)
        GET_STUDENT_LIST()
    End Sub

    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    'h_SelectedId.Value = h_SelectedId.Value & "||" & chk.Value.ToString
                    'Response.Write(chk.Value.ToString & "->")
                    If list_add(chk.Value) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String) As Boolean
        If Session("liUserList").Contains(p_userid) Then
            Return False
        Else
            Session("liUserList").Add(p_userid)
            'DropDownList1.DataSource =  Session("liUserList")
            'DropDownList1.DataBind()
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String)
        If Session("liUserList").Contains(p_userid) Then
            Session("liUserList").Remove(p_userid)
        End If
    End Sub

    Protected Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        SetChk(Me.Page)
        h_SelectedId.Value = ""
        For i As Integer = 0 To Session("liUserList").Count - 1
            If h_SelectedId.Value <> "" Then
                h_SelectedId.Value += "||"
            End If
            h_SelectedId.Value += Session("liUserList")(i).ToString
        Next
        '  Response.Write("<script language='javascript'> function listen_window(){")
        '   Response.Write("window.returnValue = document.getElementById('h_SelectedId').value;")
        '  Response.Write("window.close();")
        '  Response.Write("} </script>")

        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.NameandCode ='" & h_SelectedId.Value & "';")
        Response.Write("var oWnd = GetRadWindow('" & h_SelectedId.Value & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
    End Sub
    Protected Sub btnSearchsection_Click(sender As Object, e As ImageClickEventArgs)
        GET_STUDENT_LIST()
    End Sub
    Public Function GET_STUDENT_LIST() As DataTable
        '' EXCLUDE_DUPLICATE_STUDENT()
        Dim txtstudentsearchname As New TextBox
        Dim txtstudentsearchnumber As New TextBox
        Dim txtgrade As New TextBox
        Dim txtsection As New TextBox
        Dim txthouse As New TextBox
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PartAFilter As String = ""
        Dim pParms(6) As SqlClient.SqlParameter


        pParms(0) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("sBsuId"))

        If (GvStudents.Rows.Count > 0) Then
            txtstudentsearchnumber = GvStudents.HeaderRow.FindControl("txtFeeId")
            txtstudentsearchname = GvStudents.HeaderRow.FindControl("txtStudName")

            txtgrade = GvStudents.HeaderRow.FindControl("txtGrade")
            txtsection = GvStudents.HeaderRow.FindControl("txtSection")

            txthouse = GvStudents.HeaderRow.FindControl("txtHouse")

            If txtstudentsearchnumber.Text <> "" Then
                pParms(1) = New SqlClient.SqlParameter("@STU_NO", txtstudentsearchnumber.Text.Trim)

            Else
                pParms(1) = New SqlClient.SqlParameter("@STU_NO", System.DBNull.Value)
            End If
            If txtstudentsearchname.Text <> "" Then
                pParms(2) = New SqlClient.SqlParameter("@STU_NAME", txtstudentsearchname.Text.Trim)

            Else
                pParms(2) = New SqlClient.SqlParameter("@STU_NAME", System.DBNull.Value)
            End If
            If txtgrade.Text <> "" Then
                pParms(3) = New SqlClient.SqlParameter("@STU_GRADE", txtgrade.Text.Trim)

            Else
                pParms(3) = New SqlClient.SqlParameter("@STU_GRADE", System.DBNull.Value)
            End If
            If txtsection.Text <> "" Then
                pParms(4) = New SqlClient.SqlParameter("@STU_SECTION", txtsection.Text.Trim)

            Else
                pParms(4) = New SqlClient.SqlParameter("@STU_SECTION", System.DBNull.Value)
            End If

            If txthouse.Text <> "" Then
                pParms(5) = New SqlClient.SqlParameter("@HOUSE", txthouse.Text.Trim)
            Else
                pParms(5) = New SqlClient.SqlParameter("@HOUSE", System.DBNull.Value)
            End If
        End If
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_StudentInfo_MeritDemerit", pParms)
        If Not ds Is Nothing Then

            Dim dt As DataTable = ds.Tables(0)
            If dt.Rows.Count >= 0 Then
                GvStudents.DataSource = dt
                GvStudents.DataBind()
                'Added by vikranth on 6th Feb 2020, for fixing page index issue after filter as told by Nahyan
                If (GvStudents.Rows.Count > 0) Then
                    Dim txtstudentsearchnumber1 As TextBox = GvStudents.HeaderRow.FindControl("txtFeeId")
                    Dim txtstudentsearchname1 As TextBox = GvStudents.HeaderRow.FindControl("txtStudName")

                    Dim txtgrade1 As TextBox = GvStudents.HeaderRow.FindControl("txtGrade")
                    Dim txtsection1 As TextBox = GvStudents.HeaderRow.FindControl("txtSection")

                    Dim txthouse1 As TextBox = GvStudents.HeaderRow.FindControl("txtHouse")
                    txtstudentsearchnumber1.Text = txtstudentsearchnumber.Text
                    txtstudentsearchname1.Text = txtstudentsearchname.Text
                    txtgrade1.Text = txtgrade.Text
                    txtsection1.Text = txtsection.Text
                    txthouse1.Text = txthouse.Text
                End If
                'End By vikranth
            End If
            Return dt
        Else
            Return ds.Tables(0)
        End If

    End Function

    Protected Sub GvStudents_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        GvStudents.PageIndex = e.NewPageIndex
        GET_STUDENT_LIST()
    End Sub
End Class
