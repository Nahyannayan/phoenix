<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BmActionRecords.aspx.vb" Inherits="Students_BehaviorManagement_BmActionRecords" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">

        function showprint() {
            //window.showModalDialog('BmPrintAction.aspx?Info_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
            window.open('BmPrintAction.aspx', '', 'dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;','_blank')
        }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="View Action Details"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="title-bg" colspan="4">Action Taken Lists</td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">Action Types</span></td>

                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddOtherType" runat="server"  AutoPostBack="True" OnSelectedIndexChanged="ddOtherType_SelectedIndexChanged">
                                <asp:ListItem
                                    Text="ALL" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Notes In Student's Planner" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Break Detention Given" Value="1"></asp:ListItem>
                                <asp:ListItem Text="After School Detention Given" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Suspension" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Referred To Students Counsellor" Value="4"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"><span class="field-label">From Date</span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtFromDt" runat="server" >
                            </asp:TextBox>&nbsp;&nbsp;<asp:ImageButton ID="imgFromDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="javascript:return false;"></asp:ImageButton></td>
                        <td align="left" width="20%"><span class="field-label">ToDate</span></td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtToDt" runat="server" >
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgToDt" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="javascript:return false;"></asp:ImageButton>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="View" CssClass="button"/>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="GrdView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                Width="100%">
                                <EmptyDataRowStyle Wrap="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Incident No:">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenIncidentid" runat="server" Value='<%#Eval("BM_ID")%>'></asp:HiddenField>
                                            <%#Eval("BM_ID")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudName" runat="server" Text='<%# Eval("STUDNAME") %>' __designer:wfdid="w11"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("GRM_DISPLAY") %>' __designer:wfdid="w32"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stream">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("STM_DESCR") %>' __designer:wfdid="w33"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Shift">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("SHF_DESCR") %>' __designer:wfdid="w34"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Report Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDetenction" runat="server" __designer:wfdid="w8"></asp:Label><%# Eval("BM_ENTRY_DATE","{0:dd/MMM/yyyy}") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action By">
                                        <ItemTemplate>
                                            <%#Eval("EMPNAME")%>&nbsp;
                                            <asp:Label ID="lblDetenctionDate" runat="server" __designer:wfdid="w7"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Stud Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudId" runat="server" Text='<%# Eval("STU_ID") %>' __designer:wfdid="w14"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Detn Date">
                                        <ItemTemplate>
                                            &nbsp;<asp:Label ID="lblDetnDate" runat="server" Text='<%# Eval("DETNDATE","{0:dd/MMM/yyyy}") %>' __designer:wfdid="w17"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                <EditRowStyle Wrap="False" />
                                <SelectedRowStyle CssClass="Green" Wrap="False" />
                                <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                                <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                            </asp:GridView>
                            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                            <asp:Button ID="btnPrint" runat="server" OnClick="btnPrint_Click" OnClientClick="showprint()"
                                Text="Print" CssClass="button" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="HIDCRITERIA" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDt"
                    TargetControlID="txtFromDt">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImgToDt"
                    TargetControlID="txtToDt">
                </ajaxToolkit:CalendarExtender>


            </div>
        </div>
    </div>
</asp:Content>

