Imports System
Imports System.Data
Imports System.IO
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI
Partial Class Students_BehaviorManagement_bm_CreateNewIncident_Simple
    Inherits Page
    Dim BsuID As String
    Dim AcyID As String
    Dim AcdID As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BsuID = Session("sbsuid")
        AcyID = Session("Current_ACY_ID")
        AcdID = Session("Current_ACD_ID")
        If Not IsPostBack Then
            'If Not Request.UrlReferrer Is Nothing Then
            '    Response.Redirect(Request.UrlReferrer.ToString())
            'Else

            '    Response.Redirect("~\noAccess.aspx")
            'End If
            RadDatePicker1.SelectedDate = DateTime.Now
            BindSubCtagory()
            LoadMeritDemerit()
        End If
        'Master.ShowMessage("<strong> Error! </strong> Record could not be Inserted", WarningType.Danger)
    End Sub



    Protected Sub cmbStudent_DataBound(sender As Object, e As EventArgs)
        'set the initial footer label
        CType(cmbStudent.Footer.FindControl("RadComboItemsCount"), Literal).Text = Convert.ToString(cmbStudent.Items.Count)
    End Sub

    Protected Sub cmbStudent_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sqlquery As String = ""

        sqlquery = "select top 1000 stu_no,stu_id, stu_firstname +' '+ STU_LASTNAME as StudentName,sct_descr,grm_display" & _
                      " ,student_m.STU_PRIMARYCONTACT  " & _
                      " from STUDENT_M " & _
                      " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID  " & _
                      " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                      " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                      " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                      " INNER JOIN STUDENT_D ON  STUDENT_D.STS_STU_ID= STUDENT_M.STU_ID  " & _
                      " where student_m.stu_bsu_id in('" & BsuID & "') and " & _
                      " student_m.stu_acd_id in('" & AcdID & "') and " & _
                      " (stu_firstname +' '+ STU_LASTNAME like '%" & e.Text & "%' or " & _
                      " stu_no like '%" & e.Text & "%' ) "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlquery)
        cmbStudent.DataSource = ds
        cmbStudent.DataBind()
    End Sub
    Protected Sub cmbStudent_ItemDataBound(ByVal sender As Object, ByVal e As RadComboBoxItemEventArgs)
        'set the Text and Value property of every item
        'here you can set any other properties like Enabled, ToolTip, Visible, etc.
        e.Item.Text = (DirectCast(e.Item.DataItem, DataRowView))("StudentName").ToString()
        e.Item.Value = (DirectCast(e.Item.DataItem, DataRowView))("stu_id").ToString()
    End Sub

    Sub BindStudent()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sqlquery As String = ""

        sqlquery = "select  top 1000 stu_no,stu_id, stu_firstname +' '+ STU_LASTNAME as StudentName,sct_descr,grm_display" & _
                      " ,student_m.STU_PRIMARYCONTACT  " & _
                      " from STUDENT_M " & _
                      " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID  " & _
                      " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                      " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                      " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                      " INNER JOIN STUDENT_D ON  STUDENT_D.STS_STU_ID= STUDENT_M.STU_ID   " & _
                      " where student_m.stu_bsu_id in('" & BsuID & "')  and " & _
                        "student_m.stu_acd_id in('" & AcdID & "')"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlquery)
        'Session("Student") = ds
        cmbStudent.DataSource = ds
        cmbStudent.DataBind()
    End Sub

    Sub BindSubCtagory()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sqlquery As String = ""
        sqlquery = "select  *  " & _
                      " from BM.BM_Category " & _
                      " where BM_BSU_ID in('" & BsuID & "') "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlquery)
        'Session("Student") = ds
        ddlSubCategory.DataSource = ds
        ddlSubCategory.DataTextField = "BM_CATEGORYNAME"
        ddlSubCategory.DataValueField = "BM_CATEGORYID"
        ddlSubCategory.DataBind()
        ddlSubCategory.Items.Insert(0, New ListItem("Select a subcategory", "NA"))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Status = callTrans_Save_Hdr(transaction)
                If Status <> 0 Then
                    Throw New ArgumentException("Record could not be Inserted")
                End If
                Status = callTrans_Save_Dtl(transaction)
                If Status <> 0 Then
                    Throw New ArgumentException("Record could not be Inserted")
                End If

                transaction.Commit()
                lblerror.Text = "Record Saved Succesfully"
                ' ShowMessage("<strong> Success! </strong> Record Saved Succesfully!!!", WarningType.Success)
                LoadMeritDemerit()
                '' txtincidentdate.Text = Today.Date.ToString("dd/MMM/yyyy")
                txtDescription.Text = ""

            Catch myex As ArgumentException
                transaction.Rollback()
                lblerror.Text = myex.Message
                'ShowMessage("<strong> Error! </strong> " + myex.Message, WarningType.Danger)
            Catch ex As Exception
                transaction.Rollback()
                lblerror.Text = "Record could not be Updated"
                '  ShowMessage("<strong> Error! </strong> Record could not be Updated", WarningType.Danger)
            End Try
        End Using
    End Sub
    Function callTrans_Save_Hdr(ByVal trans As SqlTransaction) As Integer
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
            pParms(1) = New SqlClient.SqlParameter("@INCIDENT_DATE", RadDatePicker1.SelectedDate.Value.ToShortDateString)
            pParms(2) = New SqlClient.SqlParameter("@INCIDENT_TYPE", ddlCategory.SelectedItem.Text)
            pParms(3) = New SqlClient.SqlParameter("@INCIDENT_DESC", txtDescription.Text)
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            pParms(5) = New SqlClient.SqlParameter("@NewMRT_ID", SqlDbType.BigInt)
            pParms(5).Direction = ParameterDirection.Output
            pParms(6) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "BM.SaveMerit_M", pParms)
            Dim ReturnFlag As Integer = pParms(4).Value
            If ReturnFlag = 0 Then
                Session("MRT_ID") = IIf(TypeOf (pParms(5).Value) Is DBNull, String.Empty, pParms(5).Value)
            End If
            Return ReturnFlag
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Function callTrans_Save_Dtl(ByVal trans As SqlTransaction) As Integer
        Dim ReturnFlag As Integer

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(1) = New SqlClient.SqlParameter("@INCIDENT_ID", Session("MRT_ID"))
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", cmbStudent.SelectedValue)
                pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM.SaveMerit_D", pParms)
                ReturnFlag = pParms(4).Value
            Return ReturnFlag
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Sub LoadMeritDemerit()
        If BsuID Is Nothing Or AcdID Is Nothing Then
            ' ShowMessage("<strong> Error! </strong> Try Logging in again ", WarningType.Danger)

        Else
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlquery As String = ""
            sqlquery = "EXEC BM.LoadMerits '" + BsuID + "','" + AcdID + "','100' "
            Dim divs As String
            divs = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sqlquery)
            Comments.InnerHtml = divs
        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
End Class