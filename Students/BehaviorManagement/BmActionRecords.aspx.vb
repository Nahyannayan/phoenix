Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_BmActionRecords
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                BindStudentGrid()
                'RetriveAccadamicYear()
                ' cmbAccadamicYear.SelectedValue = GetAccadamicID(Session("F_Descr"))
                ' PopulateGradeMaster()
                btnPrint.Enabled = False
            End If

        Catch ex As Exception

        End Try
    End Sub
    Protected Function GetNavigateUrl(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('BmPrintAction.aspx?Info_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
    End Function

    'Private Sub RetriveAccadamicYear()
    '    Dim str_conn = ConnectionManger.GetOASISConnectionString
    '    Dim str_query = ""
    '    str_query = "SELECT ACY_ID,ACY_DESCR FROM dbo.ACADEMICYEAR_M "

    '    Dim dsACADEMICYEAR As DataSet
    '    dsACADEMICYEAR = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    cmbAccadamicYear.DataSource = dsACADEMICYEAR.Tables(0)
    '    cmbAccadamicYear.DataTextField = "ACY_DESCR"
    '    cmbAccadamicYear.DataValueField = "ACY_ID"
    '    cmbAccadamicYear.DataBind()
    'End Sub

    Private Function GetAccadamicID(ByVal Year As String) As Integer
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT ACY_ID  " & _
                        "FROM ACADEMICYEAR_M WHERE ACY_DESCR='" & Year & "'"

        Dim DSAccadamic As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Return DSAccadamic.Tables(0).Rows(0).Item(0)
    End Function

    'Public Sub PopulateGradeMaster()
    '    cmbGrade.Items.Clear()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT grd_display,grd_id,grd_displayorder from grade_m order by grd_displayorder" ' _
    '    '& " where grd_id  in(select grm_grd_id from grade_bsu_m where grm_acd_id=" + cmbAccadamicYear.SelectedValue.ToString _
    '    '& " and grm_bsu_id='" + Session("sbsuid").ToString + "') order by grd_displayorder"
    '    Dim ds As DataSet 'not
    '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    cmbGrade.DataSource = ds
    '    cmbGrade.DataTextField = "grd_display"
    '    cmbGrade.DataValueField = "grd_id"
    '    cmbGrade.DataBind()
    'End Sub
    Private Sub BindStudentGrid()
        Try

            'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim dsStudetIncidents As New DataSet
            Dim strCriteria As String
            Dim strTitle As String
            Dim strDateCriteria As String
            Dim strField As String
            Dim strFieldName As String
            'If cmbGrade.SelectedIndex >= 0 Then
            '    'strCriteria = " AND STREAM_M.STM_ID=" & cmbGrade.SelectedValue
            'End If
            If ddOtherType.SelectedValue <> -1 Then
                strTitle = " " & ddOtherType.SelectedItem.Text
            Else
                strTitle = " All Types "
            End If
            Dim allstr As String = ""
            Select Case ddOtherType.SelectedValue
                Case 0
                    strCriteria = strCriteria + " WHERE BM_NOTES_STU_PLANNER='Yes' "
                    strField = " ,BM_NOTES_STU_PLANNER_DATE AS DETNDATE "
                    strFieldName = "BM_NOTES_STU_PLANNER_DATE"
                Case 1
                    strCriteria = strCriteria + " WHERE BM_BREAK_DETENTION='Yes' "
                    strField = " ,BM_BREAK_DETENTION_DATE AS DETNDATE "
                    strFieldName = "BM_BREAK_DETENTION_DATE"
                Case 2
                    strCriteria = strCriteria + " WHERE BM_AFTER_SCHOOL_DETENTION='Yes' "
                    strField = " ,BM_AFTER_SCHOOL_DETENTION_DATE AS DETNDATE "
                    strFieldName = "BM_AFTER_SCHOOL_DETENTION_DATE"
                Case 3
                    strCriteria = strCriteria + " WHERE BM_SUSPENSION='Yes' "
                    strField = " ,BM_SUSPENSION_DATE AS DETNDATE "
                    strFieldName = "BM_SUSPENSION_DATE"
                Case 4
                    strCriteria = strCriteria + " WHERE BM_REF_STU_COUNSELLOR='Yes' "
                    strField = " ,BM_REF_STU_COUNSELLOR_DATE AS DETNDATE "
                    strFieldName = "BM_REF_STU_COUNSELLOR_DATE"
                Case Else
                    strFieldName = " 1"
                    strField = " ,'' AS DETNDATE "
            End Select

            'If strCriteria <> "" Then
            '    If txtFromDt.Text <> "" Then
            '        strDateCriteria = " AND  " & strFieldName & " >= '" & txtFromDt.Text & "'"
            '        strTitle = strTitle + " " + " From : " + txtFromDt.Text
            '    End If

            '    If txtToDt.Text <> "" Then
            '        strDateCriteria = strDateCriteria + " AND  " & strFieldName & " <= '" & txtToDt.Text & "'"
            '        strTitle = strTitle + " " + " To : " + txtToDt.Text
            '    End If
            'Else
            '    If txtFromDt.Text <> "" Then
            '        strDateCriteria = " WHERE  " & strFieldName & " >='" & txtFromDt.Text & "'"
            '    End If
            '    If strDateCriteria <> "" Then
            '        If txtToDt.Text <> "" Then
            '            strDateCriteria = strDateCriteria + " AND  " & strFieldName & " >='" & txtToDt.Text & "'"
            '        End If
            '    Else
            '        If txtToDt.Text <> "" Then
            '            strDateCriteria = strDateCriteria + " WHERE  " & strFieldName & " <='" & txtToDt.Text & "'"
            '        End If
            '    End If

            'End If

            If strCriteria <> "" Then
                If txtFromDt.Text <> "" Then
                    strDateCriteria = " AND  A.BM_ENTRY_DATE BETWEEN '" & txtFromDt.Text & "'"
                    strTitle = strTitle + " " + " From : " + txtFromDt.Text
                End If

                If txtToDt.Text <> "" Then
                    strDateCriteria = strDateCriteria + " AND '" & txtToDt.Text & "'"
                    strTitle = strTitle + " " + " To : " + txtToDt.Text
                End If
            Else
                If txtFromDt.Text <> "" Then
                    strDateCriteria = " WHERE A.BM_ENTRY_DATE BETWEEN '" & txtFromDt.Text & "'"
                End If
                If txtToDt.Text <> "" Then
                    strDateCriteria = strDateCriteria + " AND  '" & txtToDt.Text & "'"
                End If
            End If

            strCriteria = strCriteria + strDateCriteria

            'Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
            '               " BM_INCIDENT_TYPE,C.BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID,S.STU_ID,SCT_DESCR,GRM_DISPLAY,STM_DESCR,SHF_DESCR," & _
            '               " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName " & strField & " FROM BM.BM_MASTER A " & _
            '               " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
            '               " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
            '               " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
            '               " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =S.STU_SCT_ID " & _
            '               " INNER JOIN STREAM_M ON STREAM_M.STM_ID =S.STU_STM_ID " & _
            '               " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=S.STU_SHF_ID " & _
            '               " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= S.STU_GRM_ID " & _
            '               " INNER JOIN BM.BM_ACTION_MASTER C ON C.BM_STU_ID=S.STU_ID AND C.BM_ID=A.BM_ID " & _
            '               " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=C.BM_CATEGORYID " & _
            '               " " & strCriteria & " AND STU_BSU_ID='" & Session("sBsuid").ToString & "' AND STU_ACD_ID='" & Session("Current_ACD_ID") & "' ORDER BY  " & strFieldName

            Dim str1 As String = "SELECT COUNT(*) FROM BM.INCIDENT_ROLE_ACCESS WHERE ROLE_ID=" & Session("sroleid") & " AND BSU_ID=" & Session("sBsuid")
            Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str1)
            Dim str_query As String
            If sbm = 0 Then
                str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
                          " BM_INCIDENT_TYPE,C.BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID,S.STU_ID,SCT_DESCR,GRM_DISPLAY,STM_DESCR,SHF_DESCR," & _
                          " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName " & strField & " FROM BM.BM_MASTER A " & _
                          " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
                          " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
                          " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
                          " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =S.STU_SCT_ID " & _
                          " INNER JOIN STREAM_M ON STREAM_M.STM_ID =S.STU_STM_ID " & _
                          " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=S.STU_SHF_ID " & _
                          " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= S.STU_GRM_ID " & _
                          " INNER JOIN BM.BM_ACTION_MASTER C ON C.BM_STU_ID=S.STU_ID AND C.BM_ID=A.BM_ID " & _
                          " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=C.BM_CATEGORYID " & _
                          " " & strCriteria & " AND STU_BSU_ID='" & Session("sBsuid").ToString & "' AND STU_ACD_ID='" & Session("Current_ACD_ID") & "' "
            Else
                str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
                         " BM_INCIDENT_TYPE,C.BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID,S.STU_ID,SCT_DESCR,GRM_DISPLAY,STM_DESCR,SHF_DESCR," & _
                         " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName " & strField & " FROM BM.BM_MASTER A " & _
                         " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
                         " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
                         " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
                         " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =S.STU_SCT_ID " & _
                         " INNER JOIN STREAM_M ON STREAM_M.STM_ID =S.STU_STM_ID " & _
                         " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=S.STU_SHF_ID " & _
                         " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= S.STU_GRM_ID " & _
                         " INNER JOIN BM.BM_ACTION_MASTER C ON C.BM_STU_ID=S.STU_ID AND C.BM_ID=A.BM_ID " & _
                         " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=C.BM_CATEGORYID " & _
                         " " & strCriteria & " AND STU_BSU_ID='" & Session("sBsuid").ToString & "' AND STU_ACD_ID='" & Session("Current_ACD_ID") & "' AND S.STU_GRM_ID " & _
            "IN(select GRM_ID FROM vw_BSU_TUTOR WHERE SEC_EMP_ID='" & Session("EmployeeId") & "' AND SEC_bsu_id='" & Session("sBsuid") & "')"

            End If


            ''Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
            ''              " BM_INCIDENT_TYPE,C.BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID,S.STU_ID,SCT_DESCR,GRM_DISPLAY,STM_DESCR,SHF_DESCR," & _
            ''              " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName " & strField & " FROM BM.BM_MASTER A " & _
            ''              " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
            ''              " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
            ''              " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
            ''              " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =S.STU_SCT_ID " & _
            ''              " INNER JOIN STREAM_M ON STREAM_M.STM_ID =S.STU_STM_ID " & _
            ''              " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=S.STU_SHF_ID " & _
            ''              " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= S.STU_GRM_ID " & _
            ''              " INNER JOIN BM.BM_ACTION_MASTER C ON C.BM_STU_ID=S.STU_ID AND C.BM_ID=A.BM_ID " & _
            ''              " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=C.BM_CATEGORYID " & _
            ''              " " & strCriteria & " AND STU_BSU_ID='" & Session("sBsuid").ToString & "' AND STU_ACD_ID='" & Session("Current_ACD_ID") & "' "

            HIDCRITERIA.Value = strCriteria
            Session("FilterInfo") = ddOtherType.SelectedItem.Text & " Report ;" & strTitle & ";" & HIDCRITERIA.Value & ";" & strField

            ' " WHERE STU_GRD_ID='" & cmbGrade.SelectedValue & "' " & strCriteria
            '" WHERE S.STU_ID =" & HidStudId.Value & "  ORDER BY BM_ENTRY_DATE DESC"
            'RIGHT OUTER JOIN
            'LEFT OUTER 

            dsStudetIncidents = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            GrdView.DataSource = dsStudetIncidents

            If dsStudetIncidents.Tables(0).Rows.Count = 0 Then
                dsStudetIncidents.Tables(0).Rows.Add(dsStudetIncidents.Tables(0).NewRow())
                GrdView.DataBind()
                Dim columnCount As Integer = GrdView.Rows(0).Cells.Count
                GrdView.Rows(0).Cells.Clear()
                GrdView.Rows(0).Cells.Add(New TableCell)
                GrdView.Rows(0).Cells(0).ColumnSpan = columnCount
                GrdView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GrdView.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                btnPrint.Enabled = False
            Else
                GrdView.DataBind()
                btnPrint.Enabled = True

            End If
            'DirectCast(GrdvRow.FindControl("lblActionDate"), Label).Text = 
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cmbAccadamicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' PopulateGradeMaster()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub ddOtherType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddOtherType.SelectedIndexChanged
        Try
            BindStudentGrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            BindStudentGrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Response.Redirect("BmPrintAction.aspx")
    End Sub
End Class
