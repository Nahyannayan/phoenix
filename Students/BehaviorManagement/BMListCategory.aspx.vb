Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System.Xml
Partial Class Students_BehaviorManagement_BMListCategory
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            If Not IsPostBack Then

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                CheckMenuRights()
                Session("gm") = Nothing
                
                BindParentCategory()

                'cmbCategory.Items.Insert(0, New ListItem(String.Empty, String.Empty))
                'cmbCategory.SelectedIndex = 0
                'cmbCategory.SelectedIndex=-1
                BindGridViewALL()
                'BindGridViewZERO()
                'BindGridView(0)
            End If
            
        Catch ex As Exception

        End Try
    End Sub

#Region "Private Functions"

    'Public Sub BindGridView(ByVal CatId As Integer)
    '    'To bind the Gridview with Data values..
    '    Dim str_Conn = ConnectionManger.GetOASISConnectionString

    '    Dim str_Query As String
    '    If CatId <> 0 Then 'Or CatId = Nothing
    '        'str_Query = "SELECT BM_CATEGORYID,BM_CATEGORYHRID,BM_CATEGORYNAME,BM_CATEGORY_SCORE FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID <> 0 ORDER BY BM_CATEGORYID"
    '        str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM1.BM_CATEGORYHRID = " & CatId & " AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYID"
    '    Else
    '        'str_Query = "SELECT BM_CATEGORYID,BM_CATEGORYHRID,BM_CATEGORYNAME,BM_CATEGORY_SCORE " & _
    '        '                       "FROM BM.BM_CATEGORY WHERE BM_CATEGORYID =" & CatId & _
    '        '                       "ORDER BY BM_CATEGORYHRID,BM_CATEGORYID "
    '        str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM1.BM_CATEGORYHRID = 0 AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYID"
    '    End If

    '    Dim dsSurveyList As DataSet
    '    dsSurveyList = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, str_Query)
    '    gvCategory.DataSource = dsSurveyList

    '    If dsSurveyList.Tables(0).Rows.Count = 0 Then
    '        dsSurveyList.Tables(0).Rows.Add(dsSurveyList.Tables(0).NewRow())
    '        gvCategory.DataBind()
    '        Dim columnCount As Integer = gvCategory.Rows(0).Cells.Count
    '        gvCategory.Rows(0).Cells.Clear()
    '        gvCategory.Rows(0).Cells.Add(New TableCell)
    '        gvCategory.Rows(0).Cells(0).ColumnSpan = columnCount
    '        gvCategory.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
    '        gvCategory.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
    '    Else
    '        gvCategory.DataBind()
    '    End If

    '    For Each row As GridViewRow In gvCategory.Rows
    '        If DirectCast(row.FindControl("lblMainCategory"), Label).Text = "0" Then
    '            DirectCast(row.FindControl("lblMainCategory"), Label).Text = DirectCast(row.FindControl("lblSubCategory"), Label).Text
    '            DirectCast(row.FindControl("lblSubCategory"), Label).Text = "-"
    '        End If

    '        If IsNumeric(DirectCast(row.FindControl("lblMainCategory"), Label).Text) = True Then
    '            DirectCast(row.FindControl("lblMainCategory"), Label).Text = GetCategoryName(DirectCast(row.FindControl("lblMainCategory"), Label).Text)
    '        End If

    '    Next
    'End Sub

    'Public Sub BindGridViewZERO()
    '    Dim str_Conn = ConnectionManger.GetOASISConnectionString

    '    Dim str_Query As String

    '    str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM1.BM_CATEGORYHRID = 0 AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYID"

    '    Dim dsSurveyList As DataSet
    '    dsSurveyList = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, str_Query)
    '    gvCategory.DataSource = dsSurveyList

    '    If dsSurveyList.Tables(0).Rows.Count = 0 Then
    '        dsSurveyList.Tables(0).Rows.Add(dsSurveyList.Tables(0).NewRow())
    '        gvCategory.DataBind()
    '        Dim columnCount As Integer = gvCategory.Rows(0).Cells.Count
    '        gvCategory.Rows(0).Cells.Clear()
    '        gvCategory.Rows(0).Cells.Add(New TableCell)
    '        gvCategory.Rows(0).Cells(0).ColumnSpan = columnCount
    '        gvCategory.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
    '        gvCategory.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
    '    Else
    '        gvCategory.DataBind()
    '    End If

    '    For Each row As GridViewRow In gvCategory.Rows
    '        If DirectCast(row.FindControl("lblMainCategory"), Label).Text = "0" Then
    '            DirectCast(row.FindControl("lblMainCategory"), Label).Text = DirectCast(row.FindControl("lblSubCategory"), Label).Text
    '            DirectCast(row.FindControl("lblSubCategory"), Label).Text = "-"
    '        End If

    '        If IsNumeric(DirectCast(row.FindControl("lblMainCategory"), Label).Text) = True Then
    '            DirectCast(row.FindControl("lblMainCategory"), Label).Text = GetCategoryName(DirectCast(row.FindControl("lblMainCategory"), Label).Text)
    '        End If

    '    Next
    'End Sub
    Public Sub BindGridViewALL()
        Dim CatId As Integer = cmbCategory.SelectedValue

        Dim str_Conn = ConnectionManger.GetOASISConnectionString
        Dim str_Query As String

        If CatId > 0 Then 'Or CatId = Nothing
            Dim scat As String = DirectCast(gvCategory.HeaderRow.FindControl("txtsubcatsearch"), TextBox).Text
            Session("sc") = scat
            Dim gr As String = DirectCast(gvCategory.HeaderRow.FindControl("txtgradesearch"), TextBox).Text
            Session("gr") = gr

            If scat <> "" And gr <> "" Then
                str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM1.BM_CATEGORYHRID = " & CatId & " AND BM1.BM_CATEGORYNAME = '" & scat & "' AND BM2.BCD_GRD_ID = '" & gr & "') AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID AND BM_BSU_ID='" & Session("sBsuid") & "' ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
            ElseIf scat = "" And gr <> "" Then
                str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM1.BM_CATEGORYHRID = " & CatId & " AND BM2.BCD_GRD_ID = '" & gr & "' ) AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID AND BM_BSU_ID='" & Session("sBsuid") & "' ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
            ElseIf scat <> "" And gr = "" Then
                str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM1.BM_CATEGORYHRID = " & CatId & " AND BM1.BM_CATEGORYNAME = '" & scat & "')  AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID AND BM_BSU_ID='" & Session("sBsuid") & "' ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
            Else
                'str_Query = "SELECT BM_CATEGORYID,BM_CATEGORYHRID,BM_CATEGORYNAME,BM_CATEGORY_SCORE FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID <> 0 ORDER BY BM_CATEGORYID"
                str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM1.BM_CATEGORYHRID = " & CatId & " AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID AND BM_BSU_ID='" & Session("sBsuid") & "' ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
            End If
        ElseIf CatId = 0 Then
            Dim scat As String = DirectCast(gvCategory.HeaderRow.FindControl("txtsubcatsearch"), TextBox).Text
            Session("sc") = scat
            Dim gr As String = DirectCast(gvCategory.HeaderRow.FindControl("txtgradesearch"), TextBox).Text
            Session("gr") = gr
            If scat <> "" And gr <> "" Then
                str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM1.BM_CATEGORYNAME = '" & scat & "' AND BM2.BCD_GRD_ID = '" & gr & "') AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID AND BM_BSU_ID='" & Session("sBsuid") & "' ORDER BY BM1.BM_CATEGORYHRID,BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID"
            ElseIf scat = "" And gr <> "" Then
                str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM2.BCD_GRD_ID = '" & gr & "' ) AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID AND BM_BSU_ID='" & Session("sBsuid") & "' ORDER BY BM1.BM_CATEGORYHRID,BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID"
            ElseIf scat <> "" And gr = "" Then
                str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM1.BM_CATEGORYNAME = '" & scat & "')  AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID AND BM_BSU_ID='" & Session("sBsuid") & "' ORDER BY BM1.BM_CATEGORYHRID,BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID"
            Else
                'str_Query = "SELECT BM_CATEGORYID,BM_CATEGORYHRID,BM_CATEGORYNAME,BM_CATEGORY_SCORE FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID <> 0 ORDER BY BM_CATEGORYID"
                str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID AND BM_BSU_ID='" & Session("sBsuid") & "' ORDER BY BM1.BM_CATEGORYHRID,BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID"
            End If
        Else
            str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM1.BM_CATEGORYHRID = 0 AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID AND BM_BSU_ID='" & Session("sBsuid") & "' ORDER BY BM1.BM_CATEGORYID"
        End If

        Dim dsSurveyList As DataSet
        dsSurveyList = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, str_Query)
        gvCategory.DataSource = dsSurveyList

        If dsSurveyList.Tables(0).Rows.Count = 0 Then
            dsSurveyList.Tables(0).Rows.Add(dsSurveyList.Tables(0).NewRow())
            gvCategory.DataBind()
            Dim columnCount As Integer = gvCategory.Rows(0).Cells.Count
            gvCategory.Rows(0).Cells.Clear()
            gvCategory.Rows(0).Cells.Add(New TableCell)
            gvCategory.Rows(0).Cells(0).ColumnSpan = columnCount
            gvCategory.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvCategory.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvCategory.DataBind()
            DirectCast(gvCategory.HeaderRow.FindControl("txtsubcatsearch"), TextBox).Text = Session("sc")
            DirectCast(gvCategory.HeaderRow.FindControl("txtgradesearch"), TextBox).Text = Session("gr")
        End If

        For Each row As GridViewRow In gvCategory.Rows
            If DirectCast(row.FindControl("lblMainCategory"), Label).Text = "0" Then
                DirectCast(row.FindControl("lblMainCategory"), Label).Text = DirectCast(row.FindControl("lblSubCategory"), Label).Text
                DirectCast(row.FindControl("lblSubCategory"), Label).Text = "-"
            End If

            If IsNumeric(DirectCast(row.FindControl("lblMainCategory"), Label).Text) = True Then
                DirectCast(row.FindControl("lblMainCategory"), Label).Text = GetCategoryName(DirectCast(row.FindControl("lblMainCategory"), Label).Text)
            End If

        Next

    End Sub

    'Public Sub BindGridViewALL2()
    '    Dim CatId As Integer = cmbCategory.SelectedValue
    '    Dim scat As String = DirectCast(gvCategory.HeaderRow.FindControl("txtsubcatsearch"), TextBox).Text
    '    Dim gr As String = DirectCast(gvCategory.HeaderRow.FindControl("txtgradesearch"), TextBox).Text

    '    Dim str_Conn = ConnectionManger.GetOASISConnectionString
    '    Dim str_Query As String

    '    If CatId <> 0 Then 'Or CatId = Nothing
    '        If scat <> "" And gr <> "" Then
    '            str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM1.BM_CATEGORYHRID = " & CatId & " AND BM1.BM_CATEGORYNAME = '" & scat & "' AND BM2.BCD_GRD_ID = '" & gr & "') AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
    '        ElseIf scat = "" And gr <> "" Then
    '            str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM1.BM_CATEGORYHRID = " & CatId & " AND BM2.BCD_GRD_ID = '" & gr & "' ) AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
    '        ElseIf scat <> "" And gr = "" Then
    '            str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM1.BM_CATEGORYHRID = " & CatId & " AND BM1.BM_CATEGORYNAME = '" & scat & "')  AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
    '        Else
    '            'str_Query = "SELECT BM_CATEGORYID,BM_CATEGORYHRID,BM_CATEGORYNAME,BM_CATEGORY_SCORE FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID <> 0 ORDER BY BM_CATEGORYID"
    '            str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM1.BM_CATEGORYHRID = " & CatId & " AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
    '        End If

    '    Else
    '        If scat <> "" And gr <> "" Then
    '            str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE (BM1.BM_CATEGORYNAME = '" & scat & "' AND BM2.BCD_GRD_ID = '" & gr & "') AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
    '        ElseIf scat = "" And gr <> "" Then
    '            str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM2.BCD_GRD_ID = '" & gr & "' AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
    '        ElseIf scat <> "" And gr = "" Then
    '            str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM1.BM_CATEGORYNAME = '" & scat & "'  AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
    '        Else
    '            str_Query = "SELECT BM1.BM_CATEGORYID,BM1.BM_CATEGORYHRID, BM1.BM_CATEGORYNAME,BM2.BCD_GRD_ID,BM1.BM_CATEGORY_SCORE FROM BM.BM_CATEGORY BM1, BM.BM_CATEGORY_GRADES BM2 WHERE BM1.BM_CATEGORYHRID = 0 AND BM1.BM_CATEGORYID=BM2.BCD_BM_CATEGORYID ORDER BY BM1.BM_CATEGORYNAME, BM2.BCD_GRD_ID"
    '        End If
    '        'str_Query = "SELECT BM_CATEGORYID,BM_CATEGORYHRID,BM_CATEGORYNAME,BM_CATEGORY_SCORE " & _
    '        '                       "FROM BM.BM_CATEGORY WHERE BM_CATEGORYID =" & CatId & _
    '        '                       "ORDER BY BM_CATEGORYHRID,BM_CATEGORYID "
    '    End If

    '    Dim dsSurveyList As DataSet
    '    dsSurveyList = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, str_Query)
    '    gvCategory.DataSource = dsSurveyList

    '    If dsSurveyList.Tables(0).Rows.Count = 0 Then
    '        dsSurveyList.Tables(0).Rows.Add(dsSurveyList.Tables(0).NewRow())
    '        gvCategory.DataBind()
    '        Dim columnCount As Integer = gvCategory.Rows(0).Cells.Count
    '        gvCategory.Rows(0).Cells.Clear()
    '        gvCategory.Rows(0).Cells.Add(New TableCell)
    '        gvCategory.Rows(0).Cells(0).ColumnSpan = columnCount
    '        gvCategory.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
    '        gvCategory.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
    '    Else
    '        gvCategory.DataBind()
    '    End If

    '    For Each row As GridViewRow In gvCategory.Rows
    '        If DirectCast(row.FindControl("lblMainCategory"), Label).Text = "0" Then
    '            DirectCast(row.FindControl("lblMainCategory"), Label).Text = DirectCast(row.FindControl("lblSubCategory"), Label).Text
    '            DirectCast(row.FindControl("lblSubCategory"), Label).Text = "-"
    '        End If

    '        If IsNumeric(DirectCast(row.FindControl("lblMainCategory"), Label).Text) = True Then
    '            DirectCast(row.FindControl("lblMainCategory"), Label).Text = GetCategoryName(DirectCast(row.FindControl("lblMainCategory"), Label).Text)
    '        End If

    '    Next
    '    DirectCast(gvCategory.HeaderRow.FindControl("txtsubcatsearch"), TextBox).Text = scat
    '    DirectCast(gvCategory.HeaderRow.FindControl("txtgradesearch"), TextBox).Text = gr
    'End Sub

    Private Sub BindParentCategory()
        cmbCategory.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID=0 ORDER BY BM_CATEGORYID"
        Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        cmbCategory.DataSource = dsInitiator.Tables(0)
        cmbCategory.DataTextField = "BM_CATEGORYNAME"
        cmbCategory.DataValueField = "BM_CATEGORYID"
        cmbCategory.DataBind()

        'Dim LstItem As New ListItem
        'LstItem.Value = 0
        'LstItem.Text = ""
        'cmbCategory.Items.Add(LstItem)
        'cmbCategory.SelectedValue = 0

        Dim list As New ListItem
        list.Text = "Select Category"
        list.Value = "-1"
        cmbCategory.Items.Insert(0, list)

        Dim list1 As New ListItem
        list1.Text = "ALL"
        list1.Value = "0"
        Dim n As Integer = dsInitiator.Tables(0).Rows.Count()
        cmbCategory.Items.Insert(n + 1, list1)

    End Sub

    Private Function GetCategoryName(ByVal CatId As Integer) As String
        Try
            Dim str_Conn = ConnectionManger.GetOASISConnectionString
            Dim str_Query = "SELECT BM_CATEGORYID,BM_CATEGORYHRID,BM_CATEGORYNAME " & _
                            "FROM BM.BM_CATEGORY WHERE BM_CATEGORYID=" & CatId

            Dim dsCatagory As DataSet
            dsCatagory = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, str_Query)
            If dsCatagory.Tables(0).Rows.Count >= 1 Then
                Return dsCatagory.Tables(0).Rows(0).Item(2).ToString
            End If

        Catch ex As Exception

        End Try
    End Function

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString
        End If
        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
    End Sub

#End Region

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            ViewState("datamode") = "add"
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & ViewState("datamode").ToString()
            Response.Redirect("~/Students/BehaviorManagement/BM_Category.aspx" & mInfo)

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.SelectedIndexChanged
        Try
            'If cmbCategory.SelectedIndex >= 0 And cmbCategory.Text <> "" Then
            BindGridViewALL()
            'BindGridView(cmbCategory.SelectedValue)
            'gvCategory.PageIndex = 0
            'Else
            'BindGridViewZERO()
            ''BindGridView(0)
            'End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvCategory_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Try

            If e.CommandName = "view" Then

                Dim EncDec As New Encryption64

                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvCategory.Rows(index), GridViewRow)

                Dim lblCategoryId As Label

                With selectedRow
                    lblCategoryId = .FindControl("lblCategoryId")
                End With

                Dim CategoryId As String = lblCategoryId.Text
                CategoryId = EncDec.Encrypt(CategoryId)
                ViewState("datamode") = "none"
                Dim mInfo As String '= "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & ViewState("datamode").ToString()

                ViewState("datamode") = "view"

                mInfo = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & EncDec.Encrypt(ViewState("datamode").ToString())
                Response.Redirect("~\Students\BehaviorManagement\BM_Category.aspx?CategoryId=" & CategoryId & mInfo)
            End If

            '    If e.CommandName = "search" Then
            '        Dim subcat = DirectCast(gvCategory.HeaderRow.FindControl("txtsubcatsearch"), TextBox).Text
            '        If subcat <> "" Then
            '            BindGridView1(subcat)
            '        Else
            '            BindGridView(0)
            '        End If
            '    End If

            '    If e.CommandName = "gradesearch" Then
            '        Dim grd = DirectCast(gvCategory.HeaderRow.FindControl("txtgradesearch"), TextBox).Text
            '        If grd <> "" Then
            '            BindGridView2(grd)
            '        Else
            '            BindGridView(0)
            '        End If
            '    End If
            'Catch ex As Exception

            If e.CommandName = "search" Then
                'Dim subcat = DirectCast(gvCategory.HeaderRow.FindControl("txtsubcatsearch"), TextBox).Text
                'Dim grd = DirectCast(gvCategory.HeaderRow.FindControl("txtgradesearch"), TextBox).Text
                BindGridViewALL()
            End If
        Catch ex As Exception

        End Try
    End Sub
    '    For Each row As GridViewRow In gvCategory.Rows
    '        If DirectCast(row.FindControl("lblMainCategory"), Label).Text = "0" Then
    '            DirectCast(row.FindControl("lblMainCategory"), Label).Text = DirectCast(row.FindControl("lblSubCategory"), Label).Text
    '            DirectCast(row.FindControl("lblSubCategory"), Label).Text = "-"
    '        End If

    '        If IsNumeric(DirectCast(row.FindControl("lblMainCategory"), Label).Text) = True Then
    '            DirectCast(row.FindControl("lblMainCategory"), Label).Text = GetCategoryName(DirectCast(row.FindControl("lblMainCategory"), Label).Text)
    '        End If

    '    Next
    'End Sub

    Protected Sub imgbtnsubcatsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    End Sub

    Protected Sub gvCategory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCategory.PageIndexChanging
        'Dim subcat = DirectCast(gvCategory.HeaderRow.FindControl("txtsubcatsearch"), TextBox).Text
        'Dim grd = DirectCast(gvCategory.HeaderRow.FindControl("txtgradesearch"), TextBox).Text
        'If subcat = "" Or grd = "" Then
        '    BindGridView(cmbCategory.SelectedValue)
        'Else
        '    BindGridView1(subcat, grd)
        'End If
        gvCategory.PageIndex = e.NewPageIndex
        BindGridViewALL()
    End Sub

    Protected Sub gvCategory_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvCategory.RowCancelingEdit
        gvCategory.EditIndex = -1
        BindGridViewALL()
    End Sub

    Protected Sub gvCategory_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvCategory.RowEditing
        gvCategory.EditIndex = e.NewEditIndex
        BindGridViewALL()
    End Sub

End Class
