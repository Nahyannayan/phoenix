<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_CreateNewIncident_Simple.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_CreateNewIncident_Simple" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Students/BehaviorManagement/UserControl/bm_MeritDemeritStudents.ascx" TagName="bm_CreatemeritDemeritStudent" TagPrefix="uc3" %>
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
<script type="text/javascript">

    function change_chk(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch23") != -1) {
                //if (document.forms[0].elements[i].type=='checkbox' )
                //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();//fire the click event of the child element
            }
        }
    }

    function getstaffID(mode) {
        var sFeatures;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 310px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        url = '../ShowEmpDetail.aspx?id=' + mode;
        if (mode == 'EN') {
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined)
            { return false; }
            NameandCode = result.split('___');
            document.getElementById("<%=txtstaff.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hfstaff_id.ClientID %>").value = NameandCode[1];

        }
    }
    function getEmpID(mode) {
        var sFeatures;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 310px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        url = '../ShowEmpDetail.aspx?id=' + mode;
        if (mode == 'EN') {
            result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined)
            { return false; }
            NameandCode = result.split('___');
            document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=hfEmp_ID.ClientID %>").value = NameandCode[1];
        }
    }
    function WitnessType() {
        var radStudent = document.getElementById('<%=radStudent.ClientID%>')

        if (radStudent.checked == true) {
            getSibling('SI'); return true;
        }
        else {
            getEmpID('EN'); return false;
        }
    }
    function getSibling(mode) {
        var sFeatures;
        sFeatures = "dialogWidth: 565px; ";
        sFeatures += "dialogHeight: 310px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        url = '../Showstudentlist.aspx?id=' + mode;

        if (mode == 'SI') {
            result = window.showModalDialog(url, "", sFeatures);

            if (result == '' || result == undefined) {
                return false;
            }
            NameandCode = result.split('___');

            document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[0];
            document.getElementById("<%=h_SliblingID.ClientID %>").value = NameandCode[1];

        }
    }

    function showDocument(filename, contenttype) {

        result = radopen("../IFrameNew.aspx?filename=" + filename + "&contenttype=" + contenttype, "popup");
        return false;
    }

    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;
        var height = body.scrollHeight;
        var width = body.scrollWidth;
        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;
        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

</script>
  <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="popup" runat="server" Behaviors="Close,Move"  
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="1000px" Height="720px" >
            </telerik:RadWindow>
       </Windows>
       <Windows>
        <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
        </telerik:RadWindow>
      </Windows>
  </telerik:RadWindowManager> 


<style>
    
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>


          

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="title-bg">Merit-Demerit Details</td>
                </tr>
                <tr>
                    <td align="left">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr runat="server" id="tr_Action_Info">
                                <td align="left" width="20%">
                                    <asp:RadioButtonList ID="RadioReportType" runat="server" RepeatDirection="Horizontal" Width="360px">
                                        <asp:ListItem Selected="True" Value="FA"><span class="field-label">For Action</span></asp:ListItem>
                                        <asp:ListItem Value="FI"><span class="field-label">For Information</span></asp:ListItem>
                                    </asp:RadioButtonList></td>
                                <td align="left" width="30%"></td>

                                <td align="left" width="20%">
                                    <span class="field-label">Date</span></td>

                                <td align="left" width="30%">
                                    <asp:Label ID="lbltodaydate" runat="server" CssClass="field-value"></asp:Label></td>
                            </tr>
                            <tr runat="server" id="tr_Staff_Reporting">
                                <td>
                                    <span class="field-label">Staff Reporting the Incident</span></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr runat="server" id="tr_Staff_Name">
                                <td>
                                    <span class="field-label">Staff Name</span></td>

                                <td>
                                    <asp:TextBox ID="txtstaff" runat="server" CssClass="error" Enabled="False"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgbtnstaff" runat="server" ImageUrl="~/Images/forum_search.gif"
                    OnClientClick="getstaffID('EN');return true;" />&nbsp;
                                </td>
                                <td></td>
                                <td>
                                    <asp:Label ID="lblDesignation" runat="server" CssClass="field-value"></asp:Label></td>
                            </tr>
                            <tr>

                                <td align="left">
                                    <asp:Label ID="lblValidIncidentDate" runat="server"  CssClass="error"></asp:Label></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Date</span></td>

                                <td>
                                    <asp:TextBox ID="txtincidentdate" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                        OnClientClick="javascript:return false;" /></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Merit/De- Merit</span></td>

                                <td>
                                    <asp:DropDownList ID="cmbCategory" runat="server" CssClass="field-label" OnSelectedIndexChanged="cmbCategory_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Value="Merit">Merit</asp:ListItem>
                                        <asp:ListItem Value="De-Merit">De-Merit</asp:ListItem>
                            
                                    </asp:DropDownList></td>
                                <td></td>
                                <td>
                                    <%--<asp:Label ID="lblPoint" runat="server" Text="Total Points :" Width="113px"></asp:Label>--%>      
                                </td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Sub Category</span></td>
                                <td>
                                    <asp:DropDownList ID="cmbSubCategory" runat="server" CssClass="field-label">
                                    </asp:DropDownList></td>
                                <td></td>
                                <td>
                                    <%--<asp:Label ID="lblPoint" runat="server" Text="Total Points :" Width="113px"></asp:Label>--%>      
                                </td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Comments</span></td>
                                <td>
                                    <asp:TextBox ID="txtReportonincident" runat="server" Height="62px"
                                        TextMode="MultiLine" Width="500px"></asp:TextBox></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                 <td  align="left"><span class="field-label">Show in parent portal</span></td>
                                            <td  align="left">
                                                <label class="switch">
                                                    <asp:CheckBox ID="chk_bshow" runat="server" Checked="false" />
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td  align="left"><span class="field-label">upload document </span></td>
                                            <td align="left">
                                                <asp:UpdatePanel ID="updpnl" runat="server">
                                                    <ContentTemplate>
                                                        <asp:FileUpload ID="upload" runat="server" />
                                                        <asp:Button ID="saveupload" runat="server" Text="Upload" OnClick="saveupload_Click" CssClass="button" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="saveupload" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                 <asp:LinkButton ID="lnkShowFile" runat="server" Visible="false" Text=""></asp:LinkButton>
                                            </td>
                            </tr>
                           

                        </table>
                        <table width="100%">
                            <tr>
                                <td align="left" colspan="1">
                                  
                                    <uc3:bm_CreatemeritDemeritStudent id="bm_CreatemeritDemeritStudent" runat="server" OnbtnHandler="SearchStudents"></uc3:bm_CreatemeritDemeritStudent>
                                    <br />
                                    <asp:GridView ID="GrdView" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-row" AllowPaging="true" PageSize="5"
                                        >
                                        <Columns>
                                            <%--  <asp:TemplateField HeaderText="Check">
                <HeaderTemplate>
                    All<br  />
                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk(this);" ToolTip="Click here to select/deselect all rows"  />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="ch23" runat="server" Text=""  />
                </ItemTemplate>
            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Student Id">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%#Eval("STU_ID")%>' />
                                                    <asp:Label ID="lblno" runat="server" Text='<%#Eval("STU_NO")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstufirst" runat="server" Text='<%#Eval("STU_FIRSTNAME")%>'></asp:Label>
                                                    <%--<asp:Label ID="lblstulast" runat="server" Text='<%#Eval("STU_LASTNAME")%>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Grade">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstugrade" runat="server" Text='<%#Eval("grm_display")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Section">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstusection" runat="server" Text='<%#Eval("sct_descr")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Parent Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstuparentname" runat="server" Text='<%#Eval("parent_name")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstuparentemail" runat="server" Text='<%#Eval("ParentEmail")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstuparentmobile" runat="server" Text='<%#Eval("ParentMobile")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkdelete" Text="Delete" CommandName="delete" CausesValidation="false" runat="server"></asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="CBE1" ConfirmText="Are you sure?" runat="server" TargetControlID="lnkdelete"></ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle />
                                        <RowStyle CssClass="griditem" />
                                        <SelectedRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                        <EmptyDataRowStyle />
                                        <EditRowStyle />
                                    </asp:GridView>
                                </td>
                              
                              
                            </tr>
                        </table>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtstaff"
                            Display="None" ErrorMessage="Please Select Staff" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtincidentdate"
                            Display="None" ErrorMessage="Please Enter Incident Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtReportonincident"
                            Display="None" ErrorMessage="Please Enter (Report on the incident)" SetFocusOnError="True" ValidationGroup="vg1"></asp:RequiredFieldValidator>&nbsp;&nbsp;--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                    ControlToValidate="cmbCategory" Display="None" ErrorMessage="Please Select Incident Category"
                                    InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                            ShowSummary="False" Height="53px" />
                        <asp:HiddenField ID="h_SliblingID" runat="server" />


                    </td>
                </tr>
                <tr runat="server" id="tr_Witness">
                    <td align="left" class="title-bg-lite">Witnesses of the Incident</td>
                </tr>
                <tr runat="server" id="tr_Staff_Student">
                    <td align="left">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Staff / Student Name</span></td>
                                <td align="left" width="30%">
                                    <asp:RadioButton ID="radStaff" runat="server" GroupName="WitnessType" Text="Staff" CssClass="field-label"
                                        AutoPostBack="True" />
                                    <asp:RadioButton ID="radStudent" runat="server" GroupName="WitnessType" Text="Student" CssClass="field-label"
                                        AutoPostBack="True" />
                                </td>
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtPar_Sib" runat="server" Enabled="False"></asp:TextBox><asp:ImageButton ID="imgbtnSibling" runat="server" ImageUrl="~/Images/forum_search.gif"
                                        OnClientClick="WitnessType();return true;" />
                                    <asp:Button ID="Add" runat="server" Text="Add" CssClass="button" CausesValidation="False" /></td>
                                <td align="left" width="20%"></td>
                            </tr>
                            <tr>
                                <td>What the witnesses said</td>
                                <td colspan="3">
                                    <asp:Label ID="lblCaption" runat="server" CssClass="field-value"></asp:Label></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtwithnesssaid" runat="server" Height="62px" TextMode="MultiLine"
                                        Width="684px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:GridView ID="gvWitness" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Student Id">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%# Eval("STUD_ID") %>' />
                                                    <asp:Label ID="lblno" runat="server" Text='<%# Eval("STUD_ID") %>' Width="81px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblType" runat="server" Text='<%# Eval("STU_TYPE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstufirst" runat="server" Text='<%# Eval("STUD_NAME") %>'></asp:Label>&nbsp;
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Spoken">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSpoken" runat="server" Text='<%# Eval("SPOKEN") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="delete"
                                                        Text="Delete"></asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="CBE1" ConfirmText="Are you sure?" runat="server" TargetControlID="lnkdelete">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle />
                                        <RowStyle CssClass="griditem" />
                                        <SelectedRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                        <EmptyDataRowStyle />
                                        <EditRowStyle />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
               
                <tr>
                    <td align="center">
                        <asp:Button ID="cmdSave" runat="server" Text="Save" CssClass="button" ValidationGroup="vg1" />
                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" Text="Cancel" />
                    </td>
                </tr>
            </table>

            <br />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr runat="server" id="tr_Action">
                    <td class="title-bg-lite">Action Taken</td>
                </tr>
                <tr runat="server" id="tr_ActionsTaken">
                    <td align="left">
                        <table>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Student Name</span></td>

                                <td align="left" width="30%">
                                    <asp:DropDownList ID="cmbStudent" runat="server" AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td align="left" width="20%"></td>
                                <td align="left" width="30%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"><span class="field-label">Parents Called</span></td>

                                <td align="left" width="30%">
                                    <asp:RadioButtonList ID="R1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                        <asp:ListItem Value="Yes"><span class="field-label">Yes</span></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="No"><span class="field-label">No</span></asp:ListItem>
                                    </asp:RadioButtonList></td>
                                <td align="left" width="20%"><span class="field-label">Date </span></td>
                                <td align="left" width="30%">
                                    <asp:TextBox ID="txtCalledDate" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgCalledDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                        OnClientClick="javascript:return false;" /></td>
                            </tr>
                            <tr>
                                <td colspan="4">What was said
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbStudent"
                Display="None" ErrorMessage="Please Select Student Name" InitialValue="-1" SetFocusOnError="True"
                ValidationGroup="action"></asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="ReqPatentsCalled" runat="server" ControlToValidate="txtparentscalledsaid"
                                        Display="None" ErrorMessage="Please Enter , What parent was Said!." InitialValue="-1"
                                        SetFocusOnError="True" ValidationGroup="action"></asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td align="left" width="20%"></td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtparentscalledsaid" runat="server" Height="62px" TextMode="MultiLine"
                                        Width="682px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Parents Interviewed</span></td>

                                <td>
                                    <asp:RadioButtonList ID="R2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                        <asp:ListItem Value="Yes"><span class="field-label">Yes</span></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="No"><span class="field-label">No</span></asp:ListItem>
                                    </asp:RadioButtonList></td>
                                <td><span class="field-label">Date</span> </td>
                                <td>
                                    <asp:TextBox ID="txtIntvDate" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgIntvDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                        OnClientClick="javascript:return false;" /></td>
                            </tr>
                            <tr>
                                <td><span class="field-label">What was said</span>
                                </td>
                                <td>
                                    <asp:Label ID="lblValidDAte" runat="server" ForeColor="Red"></asp:Label></td>
                                <td align="left"></td>
                                <td align="left"></td>
                            </tr>
                            <tr>
                                <td align="left"></td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtparentsinterviewssaid" runat="server" Height="62px" TextMode="MultiLine"
                                        Width="683px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Notes in student's planner</span></td>

                                <td style="width: 110px">
                                    <asp:RadioButtonList ID="R3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                                    </asp:RadioButtonList></td>
                                <td><span class="field-label">Date</span></td>

                                <td>
                                    <asp:TextBox ID="T1" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                                        OnClientClick="javascript:return false;" />
                                </td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Break detention given</span></td>

                                <td>
                                    <asp:RadioButtonList ID="R4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                        <asp:ListItem Value="Yes"><span class="field-label">Yes</span></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="No"><span class="field-label">No</span></asp:ListItem>
                                    </asp:RadioButtonList></td>
                                <td><span class="field-label">Date</span></td>

                                <td>
                                    <asp:TextBox ID="T2" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif"
                                        OnClientClick="javascript:return false;" />
                                </td>
                            </tr>
                            <tr>
                                <td><span class="field-label">After school detention given</span></td>

                                <td>
                                    <asp:RadioButtonList ID="R5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                        <asp:ListItem Value="Yes"><span class="field-label">Yes</span></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="No"><span class="field-label">No</span></asp:ListItem>
                                    </asp:RadioButtonList></td>
                                <td><span class="field-label">Date</span></td>

                                <td>
                                    <asp:TextBox ID="T3" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/calendar.gif"
                                        OnClientClick="javascript:return false;" />
                                </td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Suspension</span></td>

                                <td>
                                    <asp:RadioButtonList ID="R6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                        <asp:ListItem Value="Yes"><span class="field-label">Yes</span></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="No"><span class="field-label">No</span></asp:ListItem>
                                    </asp:RadioButtonList></td>
                                <td><span class="field-label">Date</span></td>

                                <td>
                                    <asp:TextBox ID="T4" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/calendar.gif"
                                        OnClientClick="javascript:return false;" />
                                </td>
                            </tr>
                            <tr>
                                <td><span class="field-label">Referred to students counsellor</span>
                                </td>

                                <td>
                                    <asp:RadioButtonList ID="R7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                        <asp:ListItem Value="Yes"><span class="field-label">Yes</span></asp:ListItem>
                                        <asp:ListItem Selected="True" Value="No"><span class="field-label">No</span></asp:ListItem>
                                    </asp:RadioButtonList></td>
                                <td><span class="field-label">Date</span></td>

                                <td>
                                    <asp:TextBox ID="T5" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/calendar.gif"
                                        OnClientClick="javascript:return false;" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><span class="field-label">Action Forward ToHigher Authority </span>
                                    <asp:CheckBox ID="chkMail" runat="server" Text="Mail Alert" /></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkFwd" runat="server" AutoPostBack="True" /></td>
                                <td>
                                    <asp:DropDownList ID="cmbFwdTo" runat="server" AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td><span class="field-label">To</span></td>
                                <td>
                                    <asp:DropDownList ID="ddlStaff" runat="server" Width="274px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RFV_staff" runat="server" ControlToValidate="ddlStaff" InitialValue="-1" ValidationGroup="action" ErrorMessage="select forward authority"></asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtComments" runat="server" Height="62px" TextMode="MultiLine"
                                        Width="683px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="action" /></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Label ID="lblEmpName" runat="server" CssClass="field-label"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Label ID="lblmessage" runat="server" ForeColor="Red" CssClass="field-label"></asp:Label></td>
                            </tr>
                        </table>
                        <asp:LinkButton ID="lnkinsertnew" runat="server" CausesValidation="False" Enabled="False">Issue New Incident Slip</asp:LinkButton>
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                            ShowSummary="False" ValidationGroup="action" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="Hiddenbsuid" runat="server" />
            <asp:HiddenField ID="HiddenFieldEntryId" runat="server" />
            <asp:HiddenField ID="HidBMID" runat="server" />
            <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
                TargetControlID="txtincidentdate">
            </ajaxToolkit:CalendarExtender>
            <asp:HiddenField ID="hfEmp_ID" runat="server" />
            <asp:HiddenField ID="HidCategoryPoints" runat="server" />
            <asp:HiddenField ID="hidActionDet" runat="server" />
            <asp:HiddenField ID="HidActionId" runat="server" />
            <asp:HiddenField ID="hfstaff_id" runat="server" />
            <asp:HiddenField ID="hf_incidentdate" runat="server" />
            <br />
            <ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2"
                TargetControlID="T1">
            </ajaxToolkit:CalendarExtender>
            <asp:HiddenField ID="HiDCategory" runat="server" />
            <br />
            <ajaxToolkit:CalendarExtender ID="CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton3"
                TargetControlID="T2">
            </ajaxToolkit:CalendarExtender>
            <br />
            <ajaxToolkit:CalendarExtender ID="CE4" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton4"
                TargetControlID="T3">
            </ajaxToolkit:CalendarExtender>
            <br />
            <ajaxToolkit:CalendarExtender ID="CE5" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton5"
                TargetControlID="T4">
            </ajaxToolkit:CalendarExtender>
            <br />
            <ajaxToolkit:CalendarExtender ID="CE6" Format="dd/MMM/yyyy" PopupButtonID="ImageButton6" runat="server" TargetControlID="T5">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:CalendarExtender ID="cALLEDdaTE" Format="dd/MMM/yyyy" PopupButtonID="imgCalledDate" runat="server" TargetControlID="txtCalledDate">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:CalendarExtender ID="InterViewDate" Format="dd/MMM/yyyy" PopupButtonID="imgIntvDate" runat="server" TargetControlID="txtIntvDate">
            </ajaxToolkit:CalendarExtender>
            <ajaxToolkit:CalendarExtender ID="EntryDate" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1" runat="server" TargetControlID="txtincidentdate">
            </ajaxToolkit:CalendarExtender>






