Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_UserControl_bm_IncidentView
    Inherits System.Web.UI.UserControl
    Public Sub bind1()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'str_query = "SELECT BM_ID,BM_ENTRY_DATE,EMP_FNAME,EMP_LNAME " & _
        '                "FROM BM.BM_MASTER A INNER JOIN " & _
        '                "EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID ORDER BY BM_ENTRY_DATE DESC"
        'Previous code:
        'Dim str_query = "SELECT DISTINCT A.BM_ID, A.BM_INCIDENT_DATE,A.BM_INCIDENT_TYPE, A.BM_ENTRY_DATE,D.EMP_FNAME,D.EMP_LNAME FROM BM.BM_MASTER A " & _
        '         "LEFT JOIN BM.BM_ACTION_MASTER B ON A.BM_ID= B.BM_ID " & _
        '         "LEFT JOIN STUDENT_M C ON C.STU_ID= B.BM_STU_ID " & _
        '         "LEFT JOIN EMPLOYEE_M D ON D.EMP_ID= A.BM_REPORTING_STAFF_ID and C.stu_bsu_id='" & Session("sBsuid") & "' ORDER BY A.BM_INCIDENT_DATE DESC"

        'Dim str_query = "SELECT DISTINCT A.BM_ID, A.BM_INCIDENT_DATE,A.BM_INCIDENT_TYPE, A.BM_ENTRY_DATE,D.EMP_FNAME,D.EMP_LNAME FROM BM.BM_MASTER A " & _
        '                "LEFT JOIN dbo.EMPLOYEE_M D ON D.EMP_ID= A.BM_REPORTING_STAFF_ID, BM.BM_ACTION_MASTER B, dbo.STUDENT_M C WHERE C.stu_bsu_id='" & Session("sBsuid") & "' ORDER BY A.BM_ENTRY_DATE DESC"

        Dim str1 As String = "SELECT COUNT(*) FROM BM.INCIDENT_ROLE_ACCESS WHERE ROLE_ID='" & Session("sroleid") & "' AND BSU_ID='" & Session("sBsuid") & "'"
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str1)
        Dim str_query As String
        'Commented by mahesh on 27-02-2017 
        'If sbm = 0 Then
        'str_query = "SELECT DISTINCT A.BM_ID, A.BM_INCIDENT_DATE,A.BM_INCIDENT_TYPE, A.BM_ENTRY_DATE,D.EMP_FNAME,D.EMP_LNAME FROM BM.BM_MASTER A " & _
        '" INNER JOIN BM.BM_STUDENTSINVOLVED C ON A.BM_ID= C.BM_ID " & _
        '"INNER JOIN EMPLOYEE_M D ON D.EMP_ID= A.BM_REPORTING_STAFF_ID WHERE bsu_id='" & Session("sBsuid") & "' ORDER BY A.BM_INCIDENT_DATE DESC"
        'Else
        'str_query = "SELECT DISTINCT A.BM_ID, A.BM_INCIDENT_DATE,A.BM_INCIDENT_TYPE, A.BM_ENTRY_DATE,D.EMP_FNAME,D.EMP_LNAME FROM BM.BM_MASTER A " & _
        '"LEFT JOIN BM.BM_STUDENTSINVOLVED B ON A.BM_ID= B.BM_ID " & _
        '"LEFT JOIN STUDENT_M C ON C.STU_ID= B.BM_STU_ID " & _
        '"LEFT JOIN EMPLOYEE_M D ON D.EMP_ID= A.BM_REPORTING_STAFF_ID and C.stu_bsu_id='" & Session("sBsuid") & "' WHERE C.STU_GRM_ID " & _
        '"IN(select GRM_ID FROM vw_BSU_TUTOR WHERE SEC_EMP_ID='" & Session("EmployeeId") & "' AND SEC_bsu_id='" & Session("sBsuid") & "')" & "ORDER BY A.BM_INCIDENT_DATE DESC"
        ' End If
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim parm(2) As SqlClient.SqlParameter
        parm(0) = New System.Data.SqlClient.SqlParameter("@OPTION", sbm)
        parm(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        parm(2) = New SqlClient.SqlParameter("@EMP_ID", Session("EmployeeId"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[GET_MYINCIDENTLIST]", parm)


        GrdView.DataSource = ds
        GrdView.DataBind()
        For Each GrdvRow As GridViewRow In GrdView.Rows
            If GrdvRow.RowType = DataControlRowType.DataRow Then
                If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                    GrdvRow.ForeColor = Drawing.Color.Blue
                Else
                    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                    GrdvRow.ForeColor = Drawing.Color.Red
                End If
            End If
        Next
        'For Each row As GridViewRow In GrdView.Rows
        '    Dim inc_id = DirectCast(row.FindControl("HiddenIncidentid"), HiddenField).Value
        '    str_query = "SELECT (BM_STU_ID)as VIEWID ,B.STU_FIRSTNAME ,B.STU_LASTNAME FROM BM.BM_ACTION_MASTER A " & _
        '                "INNER JOIN STUDENT_M B on B.STU_ID = A.BM_STU_ID " & _
        '                "WHERE BM_ID=" & inc_id '& "

        '    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        '    If ds.Tables(0).Rows.Count >= 1 Then
        '        Dim GrdStudents As GridView = DirectCast(row.FindControl("GrdStudents"), GridView)
        '        GrdStudents.DataSource = ds
        '        GrdStudents.DataBind()
        '    End If

        'Next
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bind1()
        End If
    End Sub
    Protected Function GetNavigateUrl(ByVal pId As String) As String
        Return String.Format("javascript: window.open('bm_ActionView.aspx?inc_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;');  ", pId)
    End Function
    Protected Function GetNavigateUrlPrint(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.open('bm_ActionViewDetails.aspx?Info_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); ", pId)
    End Function
    Protected Sub lnkinsertnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkinsertnew.Click
        Response.Redirect("bm_CreateNewIncident.aspx")
    End Sub

    Public Sub Searchdate(ByVal ds As System.Data.DataSet)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        GrdView.DataSource = ds
        GrdView.DataBind()
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            GrdView.DataBind()
            Dim columnCount As Integer = GrdView.Rows(0).Cells.Count
            GrdView.Rows(0).Cells.Clear()
            GrdView.Rows(0).Cells.Add(New TableCell)
            GrdView.Rows(0).Cells(0).ColumnSpan = columnCount
            GrdView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            GrdView.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            GrdView.DataBind()
            For Each GrdvRow As GridViewRow In GrdView.Rows
                If GrdvRow.RowType = DataControlRowType.DataRow Then
                    If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                        DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                        DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                        GrdvRow.ForeColor = Drawing.Color.Blue
                    Else
                        DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                        DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                        GrdvRow.ForeColor = Drawing.Color.Red
                    End If
                End If
            Next
        End If

        'For Each row As GridViewRow In GrdView.Rows
        '    'Dim inc_id = DirectCast(row.FindControl("HiddenIncidentid"), HiddenField).Value
        '    'Dim str_query = "SELECT (BM_STU_ID)as VIEWID ,B.STU_FIRSTNAME ,B.STU_LASTNAME FROM BM.BM_ACTION_MASTER A " & _
        '    '                "INNER JOIN STUDENT_M B on B.STU_ID = A.BM_STU_ID " & _
        '    '                "WHERE BM_ID=" & inc_id '& "

        '    'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        '    'Dim GrdStudents As GridView = DirectCast(row.FindControl("GrdStudents"), GridView)
        '    'GrdStudents.DataSource = ds.Tables(0)
        '    'GrdStudents.DataBind()
        'Next
    End Sub

    Protected Sub GrdView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdView.RowCommand
        If e.CommandName = "Editing" Then
            Response.Redirect("bm_EditIncident.aspx?inc_id=" & e.CommandArgument)
        End If
    End Sub

End Class
