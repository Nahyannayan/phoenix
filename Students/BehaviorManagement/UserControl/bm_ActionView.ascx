<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_ActionView.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_ActionView" %>

        <!-- Bootstrap core CSS-->
    <link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">


<div align="center">
<table border="0"  cellpadding="5" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg">
            Incident Details-(
            <asp:Label ID="lbltype" runat="server"></asp:Label>
            )<asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/Images/tick.gif"
                                    OnClientClick="javascript:window.print(); return false;" ToolTip="Print Page" /></td>
    </tr>
    <tr>
        <td align="left" >
            <table width="100%">
                <tr>
                    <td>
                      <span class="field-label">  Date</span></td>
                    
                    <td colspan="4">
                        <asp:Label ID="lbltodaydate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                      <span class="field-label">  Staff Reporting the Incident</span></td>
                    
                    <td colspan="4">
                        <asp:Label ID="lblreportingstaff" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                      <span class="field-label">  Incident Date</span></td>
                   
                    <td colspan="4">
                        <asp:Label ID="lblincidentdate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="6" class="subheader_img">
                       <span class="field-label"> Report on the incident</span></td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:Label ID="txtReportonincident" runat="server" CssClass="field-value"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="5" class="title-bg">
                        Details
                        &nbsp; of witnesses spoken to</td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:Table ID="TabWitness" runat="server" Width="100%" Border="0"  CssClass="table table-bordered table-row" >
                        </asp:Table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
 <table border="0" cellpadding="0" cellspacing="0" Width="100%" Class="table table-bordered table-row">
                        <tr>
                            <td class="title-bg">
                                Students Involved</td>
                        </tr>
                        <tr>
                            <td >
<asp:GridView ID="GrdView" runat="server"  AutoGenerateColumns="False" CssClass="table table-bordered table-row"
    Width="100%">
    <Columns>
        <asp:TemplateField HeaderText="Student Id">
            <%--                <HeaderTemplate>

                                Student Id<br />

                                            <asp:TextBox ID="txtSearch1" runat="server" ></asp:TextBox>

                                            <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />

                </HeaderTemplate>--%>
            <ItemTemplate>
                <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%#Eval("STU_ID")%>' />
                <asp:Label ID="lblno" runat="server" Text='<%#Eval("STU_NO")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Name">
            <%-- <HeaderTemplate>
                   
                                Student Name<br />

                                            <asp:TextBox ID="txtSearch2" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                </HeaderTemplate>--%>
            <ItemTemplate>
                <asp:Label ID="lblstufirstname" runat="server" Text='<%#Eval("STU_FIRSTNAME")%>'></asp:Label>
                 <asp:Label ID="lblstulastname" runat="server" Text='<%#Eval("STU_LASTNAME")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Grade">
            <ItemTemplate>
                <asp:Label ID="lblstugrade" runat="server" Text='<%#Eval("grm_display")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Section">
            <ItemTemplate>
                <asp:Label ID="lblstusection" runat="server" Text='<%#Eval("sct_descr")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Parent Name">
            <%--<HeaderTemplate>
                    
                                Parent Name<br />
                      
                                            <asp:TextBox ID="txtSearch3" runat="server" ></asp:TextBox>
                                 
                                            <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                </HeaderTemplate>--%>
            <ItemTemplate>
                <asp:Label ID="lblstuparentname" runat="server" Text='<%#Eval("parent_name")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Email">
            <%--<HeaderTemplate>

                                Email<br />

                                            <asp:TextBox ID="txtSearch4" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                </HeaderTemplate>--%>
            <ItemTemplate>
                <asp:Label ID="lblstuparentemail" runat="server" Text='<%#Eval("ParentEmail")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Mobile">
            <%--<HeaderTemplate>
                   
                                Mobile<br />
                      <asp:TextBox ID="txtSearch5" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" />
                </HeaderTemplate>--%>
            <ItemTemplate>
                <asp:Label ID="lblstuparentmobile" runat="server" Text='<%#Eval("ParentMobile")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <%--<asp:TemplateField HeaderText="Company Name">
                <HeaderTemplate>
                    
                                Company Name<br />
                                <asp:DropDownList ID="DropSearch1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged">
                                </asp:DropDownList>
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("comp_name")%>
                </ItemTemplate>
            </asp:TemplateField>--%>
            
                <asp:TemplateField HeaderText="Mobile" Visible="false">
                <ItemTemplate>
                <asp:Label ID="lblSEC_EMP" runat="server" Text='<%#Eval("SEC_EMP")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
            
        <asp:TemplateField HeaderText="View Details">
            <ItemTemplate>
              <center><asp:LinkButton ID="lnkView" runat="server" OnClientClick='<%# GetNavigateUrl(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>' Text="View"></asp:LinkButton></center> 
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <HeaderStyle  />
    <RowStyle CssClass="griditem"  />
    <SelectedRowStyle  />
    <AlternatingRowStyle CssClass="griditem_alternative"  />
    <EmptyDataRowStyle  />
    <EditRowStyle  />
</asp:GridView>

 </td>
                    </tr>
               </table>
<asp:HiddenField ID="Hiddenincidentid" runat="server" />
</div>