<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_StudentSearch.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_StudentSearch" %>
<div class="matters">
      <script type="text/javascript">

 function change_chk_stateg(chkThis)
             {
            var chk_state= ! chkThis.checked ;
             for(i=0; i<document.forms[0].elements.length; i++)
                   {
                   var currentid =document.forms[0].elements[i].id; 
                   if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("ch2")!=-1)
                 {
                   //if (document.forms[0].elements[i].type=='checkbox' )
                      //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                        document.forms[0].elements[i].checked=chk_state;
                         document.forms[0].elements[i].click();//fire the click event of the child element
                     }
                  }
              }

</script>

    <asp:LinkButton ID="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;">Add Students</asp:LinkButton>

    <asp:Panel ID="Panel1" runat="server" >
 
<table>
            <%--<tr>
                <td >
                    Student Number</td>
                <td >
                    :</td>
                <td colspan="8">
                    <asp:TextBox ID="txtstudentsearchnumber" runat="server"></asp:TextBox></td>
            </tr>
    <tr>
        <td >
            Student Name</td>
        <td >
        </td>
        <td colspan="8">
            <asp:TextBox ID="txtstudentsearchname" runat="server"></asp:TextBox></td>
    </tr>--%>
            <tr>
                <td >
                    Curriculum</td>
                <td >
                    :</td>
                <td >
                    <asp:DropDownList ID="ddclm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddclm_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td >
                    Grade</td>
                <td >
                    :</td>
                <td colspan="5">
                    <asp:DropDownList ID="ddgrade" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td >
                    Shift</td>
                <td >
                    :</td>
                <td >
                    <asp:DropDownList ID="ddshift" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td >
                    Stream</td>
                <td >
                    :</td>
                <td >
                    <asp:DropDownList ID="ddstream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddstream_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td >
                    Section&nbsp;</td>
                <td>
                    :</td>
                <td >
                    <asp:DropDownList ID="ddsection" runat="server">
                    </asp:DropDownList></td>
            </tr>
    <tr>
        <td colspan="9" align="center">
            <asp:Button ID="btnsearch" CssClass="button" runat="server" Text="Search" CausesValidation="False" /></td>
    </tr>
</table>

    <asp:GridView ID="GrdView" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="680px">
        <Columns>
            <asp:TemplateField HeaderText="Check">
                <HeaderTemplate>
                    All<br />
                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);" ToolTip="Click here to select/deselect all rows" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="ch2" runat="server" Text="" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Student Id">
<%--                <HeaderTemplate>
                    <table>
                        <tr>
                            <td align="center">
                                Student Id</td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <div id="Div1" class="chromestyle">
                                                <ul>
                                                    <li><a href="#" rel="dropmenu1"></a>
                                                        <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSearch1" runat="server" Width="60px"></asp:TextBox></td>
                                        <td>
                                        </td>
                                        <td valign="middle">
                                            <asp:ImageButton ID="btnSearch1" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>--%>
                <ItemTemplate>
                    <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%#Eval("STU_ID")%>' />
                    <asp:Label ID="lblno" runat="server" Text='<%#Eval("STU_NO")%>'></asp:Label>
                  
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
               <%-- <HeaderTemplate>
                    <table>
                        <tr>
                            <td align="center">
                                Student Name</td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <div id="Div2" class="chromestyle">
                                                <ul>
                                                    <li><a href="#" rel="dropmenu2"></a>
                                                        <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSearch2" runat="server" Width="60px"></asp:TextBox></td>
                                        <td>
                                        </td>
                                        <td valign="middle">
                                            <asp:ImageButton ID="btnSearch2" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>--%>
                <ItemTemplate>
                 <asp:Label ID="lblstufirst" runat="server" Text='<%#Eval("STU_FIRSTNAME")%>'></asp:Label>
                 <asp:Label ID="lblstulast" runat="server" Text='<%#Eval("STU_LASTNAME")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Grade">
            <ItemTemplate>
                     <asp:Label ID="lblstugrade" runat="server" Text='<%#Eval("grm_display")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Section">
            <ItemTemplate>
            <asp:Label ID="lblstusection" runat="server" Text='<%#Eval("sct_descr")%>'></asp:Label>
                   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Parent Name">
                <%--<HeaderTemplate>
                    <table>
                        <tr>
                            <td align="center">
                                Parent Name</td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <div id="Div3" class="chromestyle">
                                                <ul>
                                                    <li><a href="#" rel="dropmenu3"></a>
                                                        <img id="mnu_3_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSearch3" runat="server" Width="60px"></asp:TextBox></td>
                                        <td>
                                        </td>
                                        <td valign="middle">
                                            <asp:ImageButton ID="btnSearch3" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>--%>
                <ItemTemplate>
                <asp:Label ID="lblstuparentname" runat="server" Text='<%#Eval("parent_name")%>'></asp:Label>
                   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <%--<HeaderTemplate>
                    <table>
                        <tr>
                            <td align="center">
                                Email</td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <div id="Div4" class="chromestyle">
                                                <ul>
                                                    <li><a href="#" rel="dropmenu4"></a>
                                                        <img id="mnu_4_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSearch4" runat="server" Width="60px"></asp:TextBox></td>
                                        <td>
                                        </td>
                                        <td valign="middle">
                                            <asp:ImageButton ID="btnSearch4" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>--%>
                <ItemTemplate>
                 <asp:Label ID="lblstuparentemail" runat="server" Text='<%#Eval("ParentEmail")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile">
                <%--<HeaderTemplate>
                    <table>
                        <tr>
                            <td align="center">
                                Mobile</td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <div id="Div5" class="chromestyle">
                                                <ul>
                                                    <li><a href="#" rel="dropmenu5"></a>
                                                        <img id="mnu_5_img" runat="server" align="middle" alt="Menu" border="0" src="../Images/operations/like.gif" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSearch5" runat="server" Width="60px"></asp:TextBox></td>
                                        <td>
                                        </td>
                                        <td valign="middle">
                                            <asp:ImageButton ID="btnSearch5" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearch_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>--%>
                <ItemTemplate>
                <asp:Label ID="lblstuparentmobile" runat="server" Text='<%#Eval("ParentMobile")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="Company Name">
                <HeaderTemplate>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                Company Name</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropSearch1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <%#Eval("comp_name")%>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
        <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
        <SelectedRowStyle CssClass="Green" Wrap="False" />
        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
        <EmptyDataRowStyle Wrap="False" />
        <EditRowStyle Wrap="False" />
    </asp:GridView>
            <asp:Button ID="btnaddstu" runat="server" Width="80px" Text="Add" CssClass="button" CausesValidation="False" /></asp:Panel>
<asp:HiddenField ID="Hiddenacyid" runat="server" />
<asp:HiddenField ID="Hiddenacdid" runat="server" />
<asp:HiddenField ID="Hiddenbsu" runat="server" />
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdvanceSearch"
        Collapsed="true"  CollapsedSize="0" CollapsedText="Add Students" ExpandControlID="LinkAdvanceSearch"
         ExpandedText="Hide Search" ScrollContents="false" TargetControlID="Panel1"
        TextLabelID="LinkAdvanceSearch">
    </ajaxToolkit:CollapsiblePanelExtender>


<input id="h_Selected_menu_1" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />
<input id="h_Selected_menu_2" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />
<input id="h_Selected_menu_3" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />
<input id="h_Selected_menu_4" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />
<input id="h_Selected_menu_5" runat="server" type="hidden"  value="LI__../Images/operations/like.gif" />

</div>