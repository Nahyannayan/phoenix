﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports UtilityObj
Imports EmailService
Partial Class Students_BehaviorManagement_UserControl_bm_CreateNewAchivement_Simple
    Inherits System.Web.UI.UserControl

    Dim Message As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session("ds1") = Nothing
            Session("gvStudents") = Nothing
            ach_id.Visible = False
            Hiddenbsuid.Value = Session("sbsuid") '"125016" '
            HidBMID.Value = "0"
            CreateWitNessWiewTable() 'Craete Witness table Stracture
            txtincidentdate.Text = Today.Date.ToString("dd/MMM/yyyy")

            'binddesig()
            'BidStaff()
            'BindCategory()
            BIndInvolvedStudents(HidBMID.Value)
            HiddenFieldEntryId.Value = Nothing
            cmdSave.Enabled = True
            ClearActionSection()
            DisableActionSection()
            radStudent.Checked = True
            Call HideBSU_CustomFlds(Session("sBSUID"))
            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            BindSubCategory(cmbSubCategory, 1) 'default merit selected
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"

            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect(ViewState("ReferrerUrl"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub HideBSU_CustomFlds(ByVal BSU_ID As String)

        tr_Action_Info.Visible = False
        tr_Staff_Reporting.Visible = False
        tr_Staff_Name.Visible = False
        tr_Witness.Visible = False
        tr_Action.Visible = False
        tr_ActionsTaken.Visible = False
        tr_Staff_Student.Visible = False
    End Sub
    Public Function CheckPerviousId(ByVal id As String) As Boolean
        Dim returnvalue = False
        For Each row As GridViewRow In GrdView.Rows
            Dim preval As String = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            If preval = id Then
                returnvalue = True
            End If
        Next
        Return returnvalue
    End Function

    Shared dt As DataTable


    Public Sub SearchStudents(ByVal ds As DataSet)
        dt = New DataTable
        dt.Columns.Add("STU_ID") ''
        dt.Columns.Add("STU_NO") ''
        dt.Columns.Add("STU_FIRSTNAME") ''
        '' dt.Columns.Add("STU_LASTNAME") ''
        dt.Columns.Add("grm_display") ''
        dt.Columns.Add("sct_descr") ''
        dt.Columns.Add("parent_name") ''
        dt.Columns.Add("ParentEmail") ''
        dt.Columns.Add("ParentMobile") ''

        For Each row As GridViewRow In GrdView.Rows
            Dim dr As DataRow = dt.NewRow()

            dr.Item("STU_ID") = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            dr.Item("STU_NO") = DirectCast(row.FindControl("lblno"), Label).Text.Trim()
            dr.Item("STU_FIRSTNAME") = DirectCast(row.FindControl("lblstufirst"), Label).Text.Trim()
            ''dr.Item("STU_LASTNAME") = DirectCast(row.FindControl("lblstulast"), Label).Text.Trim()
            dr.Item("grm_display") = DirectCast(row.FindControl("lblstugrade"), Label).Text.Trim()
            dr.Item("sct_descr") = DirectCast(row.FindControl("lblstusection"), Label).Text.Trim()
            dr.Item("parent_name") = DirectCast(row.FindControl("lblstuparentname"), Label).Text.Trim()
            dr.Item("ParentEmail") = DirectCast(row.FindControl("lblstuparentemail"), Label).Text.Trim()
            dr.Item("ParentMobile") = DirectCast(row.FindControl("lblstuparentmobile"), Label).Text.Trim()
            dt.Rows.Add(dr)

        Next

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim id As String = ds.Tables(0).Rows(i).Item("STU_ID").ToString()
                If CheckPerviousId(id) Then
                Else
                    Dim dr As DataRow = dt.NewRow()
                    dr.Item("STU_ID") = ds.Tables(0).Rows(i).Item("STU_ID").ToString()
                    dr.Item("STU_NO") = ds.Tables(0).Rows(i).Item("STU_NO").ToString()
                    dr.Item("STU_FIRSTNAME") = ds.Tables(0).Rows(i).Item("STU_FIRSTNAME").ToString()
                    ''dr.Item("STU_LASTNAME") = ds.Tables(0).Rows(i).Item("STU_LASTNAME").ToString()
                    dr.Item("grm_display") = ds.Tables(0).Rows(i).Item("grm_display").ToString()
                    dr.Item("sct_descr") = ds.Tables(0).Rows(i).Item("sct_descr").ToString()
                    dr.Item("parent_name") = ds.Tables(0).Rows(i).Item("parent_name").ToString()
                    dr.Item("ParentEmail") = ds.Tables(0).Rows(i).Item("ParentEmail").ToString()
                    dr.Item("ParentMobile") = ds.Tables(0).Rows(i).Item("ParentMobile").ToString()
                    dt.Rows.Add(dr)
                End If
            Next
        End If

        GrdView.DataSource = dt
        GrdView.DataBind()
        ''Session("ds1") = dt
    End Sub

    Protected Sub GrdView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdView.PageIndexChanging
        GrdView.PageIndex = e.NewPageIndex
        If Session("ds1") Is Nothing Then
        Else
            GrdView.DataSource = Session("ds1")
            GrdView.DataBind()
        End If
    End Sub

    Protected Sub GrdView_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GrdView.RowDeleting
        dt.Rows.Item(e.RowIndex).Delete()
        Session("ds1") = dt
        Session("gvStudents") = dt
        GrdView.DataSource = Session("ds1")
        GrdView.DataBind()
    End Sub
    Public Function CheckSelected() As Boolean
        Dim returnvalue As Boolean = False
        For Each row As GridViewRow In GrdView.Rows
            Dim check As CheckBox = DirectCast(row.FindControl("ch23"), CheckBox)
            If check.Checked Then
                returnvalue = True
            End If
        Next
        Return returnvalue
    End Function
    Public Function GetCategoryPoints() As Integer
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME,BM_CATEGORY_SCORE FROM BM.BM_CATEGORY WHERE BM_CATEGORYID=" & cmbCategory.SelectedValue
            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("BM_CATEGORY_SCORE")), 0, dsCategory.Tables(0).Rows(0).Item("BM_CATEGORY_SCORE"))
            End If
        Catch ex As Exception

        End Try
    End Function

    Sub BindSubCategory(ByVal cmbSubCategory As DropDownList, ByVal bolmerit As Integer)
        Try
            cmbSubCategory.Items.Clear()
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT  [BM_CATEGORYID], [BM_CATEGORYNAME],[BM_CATEGORY_SCORE] ,[BM_BSU_ID] ,[BM_MERIT_DEMERIT]  FROM [OASIS].[BM].[BM_CATEGORY] WHERE [BM_BSU_ID] = '" & Session("sbsuid") & "' AND [BM_CATEGORYHRID]= " & bolmerit
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            cmbSubCategory.DataSource = ds
            cmbSubCategory.DataTextField = "BM_CATEGORYNAME"
            cmbSubCategory.DataValueField = "BM_CATEGORYID"
            cmbSubCategory.DataBind()
        Catch ex As Exception

        End Try
    End Sub


    Public Function GetDesignation(ByVal StaffID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DES_ID,DES_DESCR,DES_FLAG FROM EMPDESIGNATION_M WHERE DES_ID=" & _
             "( SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=" & StaffID & ")"
            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("DES_DESCR")), 0, dsCategory.Tables(0).Rows(0).Item("DES_DESCR"))
            End If
        Catch ex As Exception

        End Try
    End Function
    Public Function GetDesignationID(ByVal StaffID As String) As Integer
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DES_ID,DES_DESCR,DES_FLAG FROM EMPDESIGNATION_M WHERE DES_ID=" & _
             "( SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=" & StaffID & ")"
            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("DES_ID")), 0, dsCategory.Tables(0).Rows(0).Item("DES_ID"))
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function BIndInvolvedStudents(ByVal BMID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT STU_ID,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) AS STU_FIRSTNAME FROM STUDENT_M WHERE STU_ID IN " & _
             "( SELECT BM_STU_ID FROM BM.BM_STUDENTSINVOLVED WHERE BM_ID=" & BMID & " AND BM_STU_ID NOT IN (SELECT BM_STU_ID FROM BM.BM_ACTION_MASTER WHERE BM_ID=" & BMID & "))"

            Dim dsStudents As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            cmbStudent.DataSource = dsStudents.Tables(0)
            cmbStudent.DataTextField = "STU_FIRSTNAME"
            cmbStudent.DataValueField = "STU_ID"
            cmbStudent.DataBind() ' 
        Catch ex As Exception

        End Try
    End Function

    Private Function BIndFWDDesignations(ByVal FromDesigNo As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DES_ID,DES_DESCR FROM EMPDESIGNATION_M WHERE DES_FLAG='SD' AND DES_DESCR<>'--' " & _
                                      " AND DES_ID IN (SELECT BM_TO_DESIGID FROM BM.BM_DESIG_ROUTING WHERE BM_FROM_DESIGID=" & FromDesigNo & " AND BSU_ID=" & Session("sbsuid") & ")"
            Dim dsStudents As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            cmbFwdTo.DataSource = dsStudents.Tables(0)
            cmbFwdTo.DataTextField = "DES_DESCR"
            cmbFwdTo.DataValueField = "DES_ID"
            cmbFwdTo.DataBind()
        Catch ex As Exception

        End Try
    End Function

    Private Sub BindCategory()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY ORDER BY BM_CATEGORYID"
            Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            cmbCategory.DataSource = dsInitiator.Tables(0)
            cmbCategory.DataTextField = "BM_CATEGORYNAME"
            cmbCategory.DataValueField = "BM_CATEGORYID"
            cmbCategory.DataBind()
            Dim list As New ListItem
            list.Text = "Select Category"
            list.Value = "-1"
            cmbCategory.Items.Insert(0, list)

        Catch ex As Exception

        End Try
    End Sub

    Public Sub SaveActionMaster()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(23) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACTION_ID", HiddenFieldEntryId.Value)
            pParms(1) = New SqlClient.SqlParameter("@BM_ID", HidBMID.Value)
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", cmbStudent.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@CATEGORYID", cmbCategory.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@PARENT_CALLED", R1.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@PARENT_SAID", txtparentscalledsaid.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@PARENT_CALLDATE", txtCalledDate.Text.Trim)
            pParms(7) = New SqlClient.SqlParameter("@PARENT_INTERVIED", R2.SelectedValue)
            pParms(8) = New SqlClient.SqlParameter("@PARENT_INTVSAID", txtparentsinterviewssaid.Text)
            pParms(9) = New SqlClient.SqlParameter("@PARENT_INTERVDATE", txtIntvDate.Text.Trim())
            pParms(10) = New SqlClient.SqlParameter("@NOTES_STUPLANNER", R3.SelectedValue)
            pParms(11) = New SqlClient.SqlParameter("@NOTES_STUPLANDATE", T1.Text.Trim())
            pParms(12) = New SqlClient.SqlParameter("@BREAK_DETENTION", R4.SelectedValue)
            pParms(13) = New SqlClient.SqlParameter("@BREAK_DETENTION_DATE", T2.Text.Trim())
            pParms(14) = New SqlClient.SqlParameter("@AFTER_SCH_DETN", R5.SelectedValue)
            pParms(15) = New SqlClient.SqlParameter("@AFTER_SCH_DETNDATE", T3.Text.Trim())
            pParms(16) = New SqlClient.SqlParameter("@SUSPENSION", R6.SelectedValue)
            pParms(17) = New SqlClient.SqlParameter("@SUSPENSION_DATE", T4.Text.Trim())
            pParms(18) = New SqlClient.SqlParameter("@STU_COUNSELLOR", R6.SelectedValue)
            pParms(19) = New SqlClient.SqlParameter("@STU_COUNSELLOR_DATE", T5.Text.Trim())
            pParms(20) = New SqlClient.SqlParameter("@ENTRY_DATE", lbltodaydate.Text.Trim)
            pParms(21) = New SqlClient.SqlParameter("@SCORE", HidCategoryPoints.Value)
            pParms(22) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
            HidActionId.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "BM.BM_SaveACTIONMASTER", pParms)
            lblmessage.Text = "Record(s) Inserted"
        Catch ex As Exception
            lblmessage.Text = "Record Is not Inserted"
        End Try
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        'To Save the Action Taken Infor mation aganist Each student.

        ' To Check the Validation in the Action Detail Section. If the Parents Said is Blanks Then 

        If R1.SelectedValue = "Yes" Then
            If txtCalledDate.Text.Trim = "" Then
                lblValidDAte.Text = "Please Select the Parents Called Date."
                Exit Sub
            End If
            If txtparentscalledsaid.Text.Trim = "" Then
                lblValidDAte.Text = "Please Enter what Parents was Said on Call."
                Exit Sub
            End If
        End If

        If R2.SelectedValue = "Yes" Then
            If txtIntvDate.Text.Trim = "" Then
                lblValidDAte.Text = "Please Select the Parents Interviewed Date."
                Exit Sub
            End If
            If txtparentsinterviewssaid.Text.Trim = "" Then
                lblValidDAte.Text = "Please Enter what Parents was Said on Interview."
                Exit Sub
            End If
        End If

        If CheckincidentDateIsValid() = False Then
            lblValidDAte.Text = "The Date can't be lesser than incident date"
            Exit Sub
        End If

        ' The Validations Finished on Action Details  -----------------------------------------------

        If cmbStudent.SelectedIndex >= 0 Then
            If CheckDateIsValid() = True Then
                SaveActionMaster()
                'To Save the Action Fwd Details
                If chkFwd.Checked = True Then
                    If cmbFwdTo.Text <> "" Then
                        hidActionDet.Value = 0
                        SaveActionFwd(HidActionId.Value)
                        SaveAlert("New Student Incidents Added : " & cmbStudent.SelectedItem.Text)
                    End If
                End If
                lnkinsertnew.Enabled = True
                btnsave.Enabled = False
                ClearActionSection()
                lblValidDAte.Text = ""
                txtComments.Enabled = False
                BIndInvolvedStudents(HidBMID.Value)
                SendEmail(Session("sbsuid"), cmbCategory.SelectedValue, cmbStudent.SelectedValue, txtComments.Text)
                'If cmbStudent.Items.Count >= 1 Then
                '    cmbStudent.Items.Remove(cmbStudent.SelectedItem.Text)
                'Else
                '     cmbStudent.Enabled = False
                '    'Response.Redirect("bm_CreateNewIncident.aspx")
                'End If

            Else
                lblValidDAte.Text = " The above dates Can't be grater than Today's date."
            End If
        Else
            lblmessage.Text = "Please select student(s)"
        End If

    End Sub

    Protected Sub SendEmail(ByVal BSU_ID As String, ByVal TYPE As String, ByVal STU_ID As String, ByVal REMARKS As String)
        Dim Mailstatus As String = ""
        Dim STUDENT_NAME As String = ""
        Try

            Dim dt6 As DataTable = NotificationEmail.GetParentDetails(1, BSU_ID)

            For Each drow As DataRow In dt6.Rows

                Dim ToEmailId As String = ""
                Dim ToEMPName As String = ""
                ToEmailId = drow.Item("PARENT_EMAIL")
                ToEmailId = "prem.sunder@gemseducation.com"
                If ToEmailId = "" Then
                    ToEmailId = "prem.sunder@gemseducation.com"
                End If
                Dim dt4 As DataTable = NotificationEmail.GetStudentDetails(BSU_ID, STU_ID)
                For Each row As DataRow In dt4.Rows
                    STUDENT_NAME = row.Item("NAME")
                Next

                Dim Subject As String = "GEMS STUDENT NOTIFICATION - " & TYPE


                Dim Email_Text As String = NotificationEmail.getEmailText(1)
                'Email_Text = Email_Text.Replace(" $$$$$$$$$$ ", drow.Item("EMP_NAME"))
                Email_Text = Email_Text.Replace(" $$$$_PARENT_$$$$ ", "Parent")
                Email_Text = Email_Text.Replace(" $$$$_MERIT_DEMERIT_$$$$ ", TYPE)


                Email_Text = Email_Text.Replace(" $$$$_DESCRIPTION_$$$$ ", REMARKS)
                Email_Text = Email_Text.Replace(" $$$$_STUDENT_$$$$ ", STUDENT_NAME)
                ''Email_Text = Email_Text.Replace(" CCCC_LEVEL_CCCC ", Encr_decrData.Encrypt("2"))

                ''Dim dt As DataTable = ViewAmbassadorProgramDetails(1, ViewState("APH_ID"))
                ''For Each drow22 As DataRow In dt.Rows
                ''    Email_Text = Email_Text.Replace(" DDDD_REFERRED_DDDD ", "Name: " & drow22.Item("STU_NAME"))
                ''    Email_Text = Email_Text.Replace(" EEEE_REFERRED_EEEE ", "Grade: " & drow22.Item("GRD_DISPLAY"))
                ''Next

                'Dim ds2 As New DataSet
                'ds2 = GetCommunicationSettings(BSU_ID)

                'Dim username = ""
                'Dim password = ""
                'Dim port = ""
                'Dim host = ""
                'Dim fromemailid = ""
                'If ds2.Tables(0).Rows.Count > 0 Then
                '    fromemailid = ds2.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
                '    username = ds2.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                '    password = ds2.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                '    port = ds2.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                '    host = ds2.Tables(0).Rows(0).Item("BSC_HOST").ToString()
                'End If
                Try
                    'Mailstatus = EmailService.email.SendPlainTextEmails(fromemailid, ToEmailId, Subject, Email_Text, username, password, host, port, 0, False)
                    Dim ReturnValue As Integer = NotificationEmail.InsertIntoEmailSendSchedule(BSU_ID, "STUDENT NOTIFICATION", "SYSTEM", ToEmailId, Subject, Email_Text)
                Catch ex As Exception
                    'lblError.Text = ex.Message.ToString
                Finally

                End Try

            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub SaveActionFwd(ByVal ActioId As Integer)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim pParms(11) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACTIONDET_ID", hidActionDet.Value)
        pParms(1) = New SqlClient.SqlParameter("@BM_ID", HidBMID.Value)
        pParms(2) = New SqlClient.SqlParameter("@ACTION_ID", ActioId)
        pParms(3) = New SqlClient.SqlParameter("@ACTIONDATE", Date.Now())
        ''pParms(4) = New SqlClient.SqlParameter("@STAFF_ID", ddstaff.SelectedValue) 'Session("sUsr_name")
        pParms(4) = New SqlClient.SqlParameter("@STAFF_ID", hfstaff_id.Value)
        pParms(5) = New SqlClient.SqlParameter("@ACTION_DESCR", txtComments.Text)
        pParms(6) = New SqlClient.SqlParameter("@COMMENTS", txtComments.Text)
        pParms(7) = New SqlClient.SqlParameter("@FWDTO", cmbFwdTo.SelectedValue)
        pParms(8) = New SqlClient.SqlParameter("@FWDDATE", Date.Now())
        ''  pParms(9) = New SqlClient.SqlParameter("@BM_DESIGID", GetDesignationID(ddstaff.SelectedValue))
        pParms(9) = New SqlClient.SqlParameter("@BM_DESIGID", GetDesignationID(hfstaff_id.Value))
        pParms(10) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
        hidActionDet.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "BM.BM_saveACTIONDETAILS", pParms)

    End Sub



    Private Sub SaveWitnessDetails()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim DtWitNess As DataTable
            DtWitNess = Session("WitnessTable")
            For Each DrRow As DataRow In DtWitNess.Rows
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@WITNESS_ID", HidBMID.Value)
                pParms(1) = New SqlClient.SqlParameter("@BM_ID", HidBMID.Value)
                pParms(2) = New SqlClient.SqlParameter("@WITNESSID", DrRow.Item("STUD_ID"))
                pParms(3) = New SqlClient.SqlParameter("@WITNESS_TYPE", DrRow.Item("STU_TYPE"))
                pParms(4) = New SqlClient.SqlParameter("@WITNESS_SAID", DrRow.Item("SPOKEN"))
                pParms(5) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM.BM_SaveINCIDENTWITNESS", pParms)
            Next
        Catch ex As Exception
        End Try
    End Sub



    Private Sub SaveAlert(ByVal AlertDesc As String)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TRA_ID", 4)
            pParms(1) = New SqlClient.SqlParameter("@ALR_DESCR", AlertDesc)
            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            pParms(3) = New SqlClient.SqlParameter("@ALR_ID", 0)
            pParms(4) = New SqlClient.SqlParameter("@USR_ID", ddlStaff.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@ALR_ACTIVE", 1)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "saveALERTS_M", pParms)

            'Send A mail to To the emploee whose task assigned
            '--------------------------
            If chkMail.Enabled = True And chkMail.Checked = True Then
                lblmessage.Text = SendPlainText("1", "")
            End If
            '--------------------------
            ' Sending an Alert to The Head of Year of the Grade.

            'FindHeadOfYear
            Dim pParms1(5) As SqlClient.SqlParameter
            pParms1(0) = New SqlClient.SqlParameter("@TRA_ID", 4)
            pParms1(1) = New SqlClient.SqlParameter("@ALR_DESCR", AlertDesc)
            pParms1(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            pParms1(3) = New SqlClient.SqlParameter("@ALR_ID", 0)
            pParms1(4) = New SqlClient.SqlParameter("@USR_ID", FindHeadOfYear())
            pParms1(5) = New SqlClient.SqlParameter("@ALR_ACTIVE", 1)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "saveALERTS_M", pParms1)



        Catch ex As Exception

        End Try
    End Sub

    Public Function SendPlainText(ByVal templateid As String, ByVal email_id As String)
        Dim Retutnvalue As String = ""
        Dim str_query As String
        Dim MailBody As String

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            str_query = "Select * from  COM_MANAGE_EMAIL where EML_ID='" & templateid & "'"
            Dim ds1 As DataSet
            ds1 = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)

            Dim Subject = ds1.Tables(0).Rows(0).Item("EML_SUBJECT").ToString().Trim()
            Dim FromEmailid As String = ds1.Tables(0).Rows(0).Item("EML_FROM").ToString().Trim()
            Dim DisplayName = ds1.Tables(0).Rows(0).Item("EML_DISPLAY").ToString().Trim()
            Dim UserName = ds1.Tables(0).Rows(0).Item("EML_USERNAME").ToString().Trim()
            Dim Password = ds1.Tables(0).Rows(0).Item("EML_PASSWORD").ToString().Trim()
            Dim Host = ds1.Tables(0).Rows(0).Item("EML_HOST").ToString().Trim().Trim()
            Dim Port = Convert.ToInt16(ds1.Tables(0).Rows(0).Item("EML_PORT").ToString().Trim())


            MailBody = "This Is the Test Message"
            MailBody = MailBody.Replace("$", "")

            If isEmail(email_id) Then

                Retutnvalue = email.SendPlainTextEmails(FromEmailid, email_id, Subject, MailBody, UserName, Password, Host, Port, templateid, False)

            Else
                Retutnvalue = "Error: Email Id Not Valid"
            End If


        Catch ex As Exception
            Retutnvalue = "Error : <br> " & ex.Message
        End Try
        Return Retutnvalue
    End Function

    Private Function FindHeadOfYear() As Integer
        Try
            Dim pParms(1) As SqlClient.SqlParameter
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim STRSQL As String = "SELECT HODID,STAFF_ID FROM HEADOFYEAR WHERE GRADEID=(SELECT STU_GRD_ID FROM " & _
                                    " STUDENT_M WHERE STU_ID=" & cmbStudent.SelectedValue & ") AND BSU_ID=" & Session("sbsuid") 'AND ACCYEAR=" & Session("Accyear") & " 

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, STRSQL)
            If ds.Tables(0).Rows.Count >= 1 Then
                Return ds.Tables(0).Rows(0).Item("STAFF_ID")
            Else
                Return 0
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Sub CreateWitNessWiewTable()

        Dim DtWitNess As DataTable
        DtWitNess = New DataTable
        DtWitNess.Columns.Add("STUD_ID")
        DtWitNess.Columns.Add("STU_TYPE")
        DtWitNess.Columns.Add("STUD_NAME")
        DtWitNess.Columns.Add("SPOKEN")
        Session("WitnessTable") = DtWitNess

    End Sub

    Sub PopulateClassTeacher()
        ddlStaff.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query = "SELECT ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(emp_mname,'') + ' ' + ISNULL(EMP_LNAME,'') AS emp_name , EMP_ID FROM EMPLOYEE_M WHERE EMP_BSU_ID='" & Session("sBsuid") & "' " ' & _
        'Dim str_query As String = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
        '                         & "emp_id FROM employee_m WHERE emp_ect_id=1 and emp_bsu_id='" + Session("sBsuid") + "' order by emp_fname,emp_mname,emp_lname"

        If cmbFwdTo.Items.Count > 0 Then
            str_query += " and emp_des_id=" & cmbFwdTo.SelectedValue & ""

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query & "order by EMP_FNAME")
        ddlStaff.DataSource = ds
        ddlStaff.DataTextField = "emp_name"
        ddlStaff.DataValueField = "emp_id"
        ddlStaff.DataBind()
        Dim list As New ListItem
        list.Text = "---select---"
        list.Value = "-1"
        ddlStaff.Items.Insert(0, list)
    End Sub

    Public Sub ActionTakendetails(ByVal stu_id As String)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT * FROM BM.BM_ACTION_MASTER WHERE BM_STU_ID='" & stu_id & "' AND BM_ID='" & HidBMID.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            hidActionId.Value = ds.Tables(0).Rows(0).Item("BM_ACTION_ID").ToString()
            R1.SelectedValue = ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED").ToString()
            R2.SelectedValue = ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED").ToString()
            R3.SelectedValue = ds.Tables(0).Rows(0).Item("BM_NOTES_STU_PLANNER").ToString()
            R4.SelectedValue = ds.Tables(0).Rows(0).Item("BM_BREAK_DETENTION").ToString()
            R5.SelectedValue = ds.Tables(0).Rows(0).Item("BM_AFTER_SCHOOL_DETENTION").ToString()
            R6.SelectedValue = ds.Tables(0).Rows(0).Item("BM_SUSPENSION").ToString()
            R7.SelectedValue = ds.Tables(0).Rows(0).Item("BM_REF_STU_COUNSELLOR").ToString()
            T1.Text = Format(ds.Tables(0).Rows(0).Item("BM_NOTES_STU_PLANNER_DATE"), "dd-MMM-yyyy")
            T2.Text = Format(ds.Tables(0).Rows(0).Item("BM_BREAK_DETENTION_DATE"), "dd-MMM-yyyy")
            T3.Text = Format(ds.Tables(0).Rows(0).Item("BM_AFTER_SCHOOL_DETENTION_DATE"), "dd-MMM-yyyy")
            T4.Text = Format(ds.Tables(0).Rows(0).Item("BM_SUSPENSION_DATE"), "dd-MMM-yyyy")
            T5.Text = Format(ds.Tables(0).Rows(0).Item("BM_REF_STU_COUNSELLOR_DATE"), "dd-MMM-yyyy")
            txtCalledDate.Text = Format(ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED_DATE"), "dd-MMM-yyyy")
            txtIntvDate.Text = Format(ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED_DATE"), "dd-MMM-yyyy")
            txtparentscalledsaid.Text = ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED_SAID").ToString()
            txtparentsinterviewssaid.Text = ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED_SAID").ToString()
            cmbCategory.SelectedValue = ds.Tables(0).Rows(0).Item("BM_CATEGORYID") '
            'If cmbCategory.SelectedIndex >= 0 Then
            '    lblPoint.Text = "Total Points : " + GetCategoryPoints().ToString()
            'End If
            'btnSaveAction.Enabled = True
            If R1.SelectedValue = "No" Then
                txtCalledDate.Text = ""
                txtCalledDate.Enabled = False
                imgCalledDate.Enabled = False
            End If
            If R2.SelectedValue = "No" Then
                txtIntvDate.Text = ""
                txtIntvDate.Enabled = False
                imgIntvDate.Enabled = False
            End If
            If R3.SelectedValue = "No" Then
                T1.Text = ""
                T1.Enabled = False
                ImageButton2.Enabled = False
            End If
            If R4.SelectedValue = "No" Then
                T2.Text = ""
                T2.Enabled = False
                ImageButton3.Enabled = False
            End If
            If R5.SelectedValue = "No" Then
                T3.Text = ""
                T3.Enabled = False
                ImageButton4.Enabled = False
            End If
            If R6.SelectedValue = "No" Then
                T4.Text = ""
                T4.Enabled = False
                ImageButton5.Enabled = False
            End If
            If R7.SelectedValue = "No" Then
                T5.Text = ""
                T5.Enabled = False
                ImageButton6.Enabled = False
            End If
        Else
            'btnSaveAction.Enabled = False
            R1.SelectedValue = "No"
            R2.SelectedValue = "No"
            R3.SelectedValue = "No"
            R4.SelectedValue = "No"
            R5.SelectedValue = "No"
            R6.SelectedValue = "No"
            R7.SelectedValue = "No"
            T1.Text = ""
            T1.Enabled = False
            T2.Text = ""
            T2.Enabled = False
            T3.Text = ""
            T3.Enabled = False
            T4.Text = ""
            T4.Enabled = False
            T5.Text = ""
            T5.Enabled = False
            txtIntvDate.Text = ""
            txtIntvDate.Enabled = False
            txtCalledDate.Text = ""
            txtCalledDate.Enabled = False
            ImageButton6.Enabled = False
            ImageButton5.Enabled = False
            ImageButton4.Enabled = False
            ImageButton3.Enabled = False
            ImageButton2.Enabled = False
            imgIntvDate.Enabled = False
            imgCalledDate.Enabled = False
            txtparentscalledsaid.Text = ""
            txtparentsinterviewssaid.Text = ""
            ''txtnotes.Text = ""
        End If

    End Sub
    Private Function CheckActionExist(ByVal StudId As Integer) As Boolean
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "SELECT * FROM BM.BM_ACTION_MASTER WHERE BM_STU_ID='" & StudId & "' AND BM_ID='" & HidBMID.Value & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try
    End Function
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer

        If Final_validation() = True Then
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Status = callTrans_Save_Hdr(transaction)
                    If Status <> 0 Then
                        Throw New ArgumentException("Record could not be Inserted")
                    End If
                    'Status = callTrans_Save_Dtl(transaction)
                    'If Status <> 0 Then
                    '    Throw New ArgumentException("Record could not be Inserted")
                    'End If

                    transaction.Commit()
                    lblValidIncidentDate.Text = "Record Saved Succesfully!!!"
                    txtincidentdate.Text = Today.Date.ToString("dd/MMM/yyyy")
                    txtReportonincident.Text = ""
                    dt.Reset()
                    dt.Clear()
                    GrdView.DataSource = dt
                    GrdView.DataBind()
                    Session("gvStudents") = Nothing
                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblValidIncidentDate.Text = myex.Message
                Catch ex As Exception
                    transaction.Rollback()
                    lblValidIncidentDate.Text = "Record could not be Updated"
                End Try
            End Using
        End If
    End Sub

    Function callDocSave(ByRef trans As SqlTransaction, ByVal DocPath As String, ByVal type As Char, ByVal mrg_id As Integer) As Integer
        Dim ReturnFlag As Int16
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim params(8) As SqlClient.SqlParameter
        params(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
        params(1) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        params(2) = New SqlClient.SqlParameter("@INC_TYPE", cmbCategory.SelectedItem.Text)
        params(3) = New SqlClient.SqlParameter("@INC_DATE", txtincidentdate.Text)
        params(4) = New SqlClient.SqlParameter("@TYPE", type)
        params(5) = New SqlClient.SqlParameter("@DOCName", DocPath)
        params(6) = New SqlClient.SqlParameter("@MRG_ID", mrg_id)
        params(7) = New SqlClient.SqlParameter("@NewMRg_ID", SqlDbType.BigInt)
        params(7).Direction = ParameterDirection.Output
        params(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        params(8).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[BM].[SaveDocMerit_M]", params)
        If type = "I" Then
            Session("MRG_ID") = params(7).Value
        End If
        ReturnFlag = params(8).Value.ToString
        Return ReturnFlag
    End Function
    Function callTrans_Save_Hdr(ByRef trans As SqlTransaction) As Integer
        Try
            Session("MRT_ID") = ""
            ' Do save document before save
            Dim ReturnFlag As Integer
            ReturnFlag = callDocSave(trans, "", "I", 0)
            ' end of save document
            If ReturnFlag = 0 Then
                If Not Session("MRG_ID") Is Nothing Then
                    If Session("MRG_ID") > 0 Then
                        ReturnFlag = callSavefile(Session("sbsuid"), trans)
                        For Each DrRow As DataRow In dt.Rows
                            Dim pParms(11) As SqlClient.SqlParameter
                            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
                            pParms(1) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
                            pParms(2) = New SqlClient.SqlParameter("@INCIDENT_DATE", Format(CDate(txtincidentdate.Text), "dd-MMM-yyyy"))
                            pParms(3) = New SqlClient.SqlParameter("@INCIDENT_TYPE", cmbCategory.SelectedItem.Value)

                            pParms(4) = New SqlClient.SqlParameter("@INCIDENT_DESC", txtReportonincident.Text)
                            If (cmbCategory.SelectedValue = "Achieves") Then
                                pParms(5) = New SqlClient.SqlParameter("@CAT_ID", 1)
                            ElseIf (cmbCategory.SelectedValue = "Merit") Then
                                pParms(5) = New SqlClient.SqlParameter("@CAT_ID", 2)
                            ElseIf (cmbCategory.SelectedValue = "De-Merit") Then
                                pParms(5) = New SqlClient.SqlParameter("@CAT_ID", 3)
                            End If
                            pParms(6) = New SqlClient.SqlParameter("@SUB_CAT_ID", cmbSubCategory.SelectedItem.Value)
                            pParms(7) = New SqlClient.SqlParameter("@STU_ID", DrRow.Item("STU_ID"))
                            If chk_bshow.Checked Then
                                pParms(8) = New SqlClient.SqlParameter("@bShow", 1)
                            Else
                                pParms(8) = New SqlClient.SqlParameter("@bShow", 0)
                            End If
                            pParms(9) = New SqlClient.SqlParameter("@MRG_ID", Session("MRG_ID"))
                            pParms(10) = New SqlClient.SqlParameter("@NewMRT_ID", SqlDbType.BigInt)
                            pParms(10).Direction = ParameterDirection.Output
                            pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                            pParms(11).Direction = ParameterDirection.ReturnValue

                            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "BM.SaveMerit_M", pParms)
                            ReturnFlag = pParms(11).Value
                            If ReturnFlag = 0 Then
                                If Session("MRT_ID") Is Nothing Or Session("MRT_ID").ToString = "" Then
                                    Session("MRT_ID") = (IIf(TypeOf (pParms(10).Value) Is DBNull, String.Empty, pParms(10).Value)).ToString
                                Else
                                    Session("MRT_ID") = Session("MRT_ID") & "|" & (IIf(TypeOf (pParms(10).Value) Is DBNull, String.Empty, pParms(10).Value)).ToString
                                End If
                                ReturnFlag = callTrans_Save_Dtl(trans, DrRow.Item("STU_ID"), pParms(10).Value)
                            End If
                            '  Return ReturnFlag
                            If (ReturnFlag <> 0) Then
                                Return ReturnFlag
                            End If
                        Next
                    End If
                End If
            End If
            Return ReturnFlag
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "merit1hdr1111")
            Return 1000
        End Try
    End Function
    Function callTrans_Save_Dtl(ByRef trans As SqlTransaction, ByVal stuid As Int64, ByVal MRT_ID As Int64) As Integer
        Dim ReturnFlag As Integer

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'For Each DrRow As DataRow In dt.Rows
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(1) = New SqlClient.SqlParameter("@INCIDENT_ID", MRT_ID)
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", stuid)
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM.SaveMerit_D", pParms)
            ReturnFlag = pParms(4).Value
            ' Next
            Return ReturnFlag
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "merit1dtl")
            Return -1
        End Try
    End Function

    '///REFER OLD CODE PORTION
    'Function callTrans_Save_Hdr(ByVal trans As SqlTransaction) As Integer
    '    Try
    '        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

    '        Dim pParms(9) As SqlClient.SqlParameter
    '        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
    '        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
    '        pParms(2) = New SqlClient.SqlParameter("@INCIDENT_DATE", Format(CDate(txtincidentdate.Text), "dd-MMM-yyyy"))
    '        pParms(3) = New SqlClient.SqlParameter("@INCIDENT_TYPE", cmbCategory.SelectedItem.Value)

    '        pParms(4) = New SqlClient.SqlParameter("@INCIDENT_DESC", txtReportonincident.Text)
    '        If (cmbCategory.SelectedValue = "Achieves") Then
    '            pParms(5) = New SqlClient.SqlParameter("@CAT_ID", 1)
    '        ElseIf (cmbCategory.SelectedValue = "Merit") Then
    '            pParms(5) = New SqlClient.SqlParameter("@CAT_ID", 2)
    '        ElseIf (cmbCategory.SelectedValue = "De-Merit") Then
    '            pParms(5) = New SqlClient.SqlParameter("@CAT_ID", 3)
    '        End If

    '        pParms(6) = New SqlClient.SqlParameter("@SUB_CAT_ID", cmbSubCategory.SelectedItem.Value)

    '        pParms(7) = New SqlClient.SqlParameter("@NewMRT_ID", SqlDbType.BigInt)
    '        pParms(7).Direction = ParameterDirection.Output
    '        pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '        pParms(8).Direction = ParameterDirection.ReturnValue

    '        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "BM.SaveMerit_M", pParms)
    '        Dim ReturnFlag As Integer = pParms(8).Value
    '        If ReturnFlag = 0 Then
    '            Session("MRT_ID") = IIf(TypeOf (pParms(7).Value) Is DBNull, String.Empty, pParms(7).Value)
    '        End If
    '        Return ReturnFlag
    '    Catch ex As Exception
    '        Return 1000
    '    End Try
    'End Function

    'Function callTrans_Save_Dtl(ByVal trans As SqlTransaction) As Integer
    '    Dim ReturnFlag As Integer

    '    Try
    '        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '        For Each DrRow As DataRow In dt.Rows
    '            Dim pParms(10) As SqlClient.SqlParameter
    '            pParms(1) = New SqlClient.SqlParameter("@INCIDENT_ID", Session("MRT_ID"))
    '            pParms(2) = New SqlClient.SqlParameter("@STU_ID", DrRow.Item("STU_ID"))
    '            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '            pParms(4).Direction = ParameterDirection.ReturnValue
    '            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM.SaveMerit_D", pParms)
    '            ReturnFlag = pParms(4).Value
    '        Next
    '        Return ReturnFlag
    '    Catch ex As Exception
    '        Return -1
    '    End Try
    'End Function
    '///REFER OLD CODE PORTION

    Function Final_validation() As Boolean
        Dim IS_ERROR As Boolean

        IS_ERROR = False
        If GrdView.Rows.Count <= 0 Then
            lblValidIncidentDate.Text = "Please Select the Students"
            IS_ERROR = True
        ElseIf CahekDateWithTodate(CDate(txtincidentdate.Text)) = False Then
            lblValidIncidentDate.Text = " Incident date Can't be grater than Today's date."
            IS_ERROR = True
        ElseIf txtReportonincident.Text = "" Then
            lblValidIncidentDate.Text = " Please fill out comments."
            IS_ERROR = True
        End If

        If IS_ERROR = True Then
            Final_validation = False
        Else
            Final_validation = True
        End If

    End Function


    Sub bindincidentdate(ByVal bmid As Integer)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query = "select BM_INCIDENT_DATE FROM BM.BM_MASTER WHERE BM_ID=" & bmid & ""
        Dim incident_date As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        hf_incidentdate.Value = incident_date.ToString()
    End Sub

    Protected Sub lnkinsertnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkinsertnew.Click
        Response.Redirect("bm_CreateNewIncident.aspx")
    End Sub

    Protected Sub cmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.SelectedIndexChanged
        Try
            If cmbCategory.SelectedIndex >= 0 Then
                HidCategoryPoints.Value = GetCategoryPoints().ToString
                'lblPoint.Text = "Total Points : " + HidCategoryPoints.Value
                HiDCategory.Value = GetCategoryPoints()

                If (cmbCategory.SelectedValue = "Achieves") Then
                    BindSubCategory(cmbSubCategory, 1)
                ElseIf (cmbCategory.SelectedValue = "Merit") Then
                    BindSubCategory(cmbSubCategory, 2)
                ElseIf (cmbCategory.SelectedValue = "De-Merit") Then
                    BindSubCategory(cmbSubCategory, 3)

                End If

            End If

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub radStaff_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStaff.CheckedChanged
        If radStaff.Checked = True Then
            'btnEmp_Name.Enabled = True
            'imgbtnSibling.Enabled = False
            txtPar_Sib.Text = ""
        Else
            'btnEmp_Name.Enabled = False
            'imgbtnSibling.Enabled = True
        End If
    End Sub

    Protected Sub radStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStudent.CheckedChanged
        If radStudent.Checked = True Then
            'imgbtnSibling.Enabled = True
            'btnEmp_Name.Enabled = False
            txtPar_Sib.Text = ""
        Else
            'imgbtnSibling.Enabled = False
            'btnEmp_Name.Enabled = True
        End If
    End Sub


    'Add Witness Details To The Grid.
    Protected Sub Add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Add.Click
        Dim strWitNessType As String
        Dim strStuID As String

        If txtPar_Sib.Text.Trim <> "" Then
            Dim DtWitness As DataTable
            DtWitness = Session("WitnessTable")
            Dim DrWitness As DataRow = DtWitness.NewRow()


            If radStaff.Checked = True Then
                strWitNessType = "STF"
                strStuID = hfEmp_ID.Value
                DrWitness.Item("STUD_NAME") = GetSaffName(strStuID) ' txtPar_Sib.Text
            Else
                strWitNessType = "STU"
                strStuID = h_SliblingID.Value
                DrWitness.Item("STUD_NAME") = GetStudentName(strStuID) ' txtPar_Sib.Text
            End If

            DrWitness.Item("STUD_ID") = strStuID
            DrWitness.Item("STU_TYPE") = strWitNessType
            'DrWitness.Item("STUD_NAME") = GetStudentName(strStuID) ' txtPar_Sib.Text
            DrWitness.Item("SPOKEN") = txtwithnesssaid.Text
            DtWitness.Rows.Add(DrWitness)
            Session("WitnessTable") = DtWitness

            gvWitness.DataSource = DtWitness
            gvWitness.DataBind()

            txtwithnesssaid.Text = ""
            txtPar_Sib.Text = ""
            lblCaption.Text = ""
        End If
    End Sub

    Private Function GetSaffName(ByVal StaffID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT EMPNO,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EmpName FROM EMPLOYEE_M WHERE EMP_ID=" & StaffID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("EmpName")
            Else
                Return ""
            End If

        Catch ex As Exception
        End Try
    End Function

    Private Function GetStudentName(ByVal StudID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT STU_NO,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) As StudentName FROM STUDENT_M WHERE STU_ID=" & StudID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("StudentName")
            Else
                Return ""
            End If
        Catch ex As Exception
        End Try
    End Function

    Private Function GetStudentNameByNumber(ByVal StudID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT STU_NO,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) As StudentName FROM STUDENT_M WHERE STU_NO=" & StudID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("StudentName")
            Else
                Return ""
            End If
        Catch ex As Exception
        End Try
    End Function

    Protected Sub gvWitness_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvWitness.RowDeleting
        Try
            Dim DtWitness As DataTable
            DtWitness = Session("WitnessTable")

            DtWitness.Rows.Item(e.RowIndex).Delete()
            Session("WitnessTable") = DtWitness
            gvWitness.DataSource = DtWitness
            gvWitness.DataBind()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub EnableIncidentMaster()
        RadioReportType.Enabled = True
        '' ddstaff.Enabled = True
        imgbtnstaff.Enabled = True
        txtincidentdate.Enabled = True
        ImageButton1.Enabled = True
        cmbCategory.Enabled = True
        txtReportonincident.Enabled = True
        radStaff.Enabled = True
        radStudent.Enabled = True
        txtPar_Sib.Enabled = True

        imgbtnSibling.Enabled = True
        Add.Enabled = True
        txtwithnesssaid.Enabled = True
        gvWitness.Enabled = True
        GrdView.Enabled = True
    End Sub

    Private Sub DisableIncidentMaster()
        RadioReportType.Enabled = False
        ''ddstaff.Enabled = False
        imgbtnstaff.Enabled = False
        cmbCategory.Enabled = False
        txtincidentdate.Enabled = False
        ImageButton1.Enabled = False
        txtReportonincident.Enabled = False
        radStaff.Enabled = False
        radStudent.Enabled = False
        txtPar_Sib.Enabled = False

        imgbtnSibling.Enabled = False
        Add.Enabled = False
        txtincidentdate.Enabled = False
        txtwithnesssaid.Enabled = False
        gvWitness.Enabled = False
        GrdView.Enabled = False


    End Sub
    '------------------ The Below Part is Exclusively for Behavieour Action Session -------------------------------------
    Private Sub EnableActionSection()
        cmbStudent.Enabled = True
        R1.Enabled = True
        txtparentscalledsaid.Enabled = True
        ReqPatentsCalled.Enabled = True
        R2.Enabled = True
        txtparentsinterviewssaid.Enabled = True
        R3.Enabled = True
        R4.Enabled = True
        R5.Enabled = True
        R6.Enabled = True
        R7.Enabled = True
        T1.Enabled = True
        T2.Enabled = True
        T3.Enabled = True
        T4.Enabled = True
        T5.Enabled = True
        'ImageButton1.Enabled = True
        ImageButton2.Enabled = True
        ImageButton3.Enabled = True
        ImageButton4.Enabled = True
        ImageButton5.Enabled = True
        ImageButton6.Enabled = True
        chkFwd.Enabled = True
        chkMail.Enabled = True
        cmbFwdTo.Enabled = False
        btnsave.Enabled = True
        txtCalledDate.Enabled = True
        imgCalledDate.Enabled = True
        imgIntvDate.Enabled = True
    End Sub

    Private Sub DisableActionSection()
        cmbStudent.Enabled = False
        R1.Enabled = False
        'cmbCategory.Enabled = False
        txtparentscalledsaid.Enabled = False
        'ReqPatentsCalled.Enabled = False
        R2.Enabled = False
        txtparentsinterviewssaid.Enabled = False
        R3.Enabled = False
        R4.Enabled = False
        R5.Enabled = False
        R6.Enabled = False
        R7.Enabled = False
        T1.Enabled = False
        T2.Enabled = False
        T3.Enabled = False
        T4.Enabled = False
        T5.Enabled = False
        'ImageButton1.Enabled = False
        ImageButton2.Enabled = False
        ImageButton3.Enabled = False
        ImageButton4.Enabled = False
        ImageButton5.Enabled = False
        ImageButton6.Enabled = False
        chkFwd.Enabled = False
        chkMail.Enabled = False
        cmbFwdTo.Enabled = False
        btnsave.Enabled = False
        txtCalledDate.Enabled = False
        txtComments.Enabled = False
        txtIntvDate.Enabled = False
        imgCalledDate.Enabled = False
        imgIntvDate.Enabled = False
    End Sub
    Private Sub ClearActionSection()
        cmbStudent.SelectedIndex = -1
        cmbCategory.SelectedIndex = -1
        txtparentscalledsaid.Text = ""
        txtparentsinterviewssaid.Text = ""
        txtCalledDate.Text = ""
        txtIntvDate.Text = ""
        txtComments.Text = ""
        cmbFwdTo.Items.Clear()
        ddlStaff.Items.Clear()
        cmbFwdTo.Enabled = False
        ddlStaff.Enabled = False
        txtComments.Enabled = False


        T1.Text = ""
        T2.Text = ""
        T3.Text = ""
        T4.Text = ""
        T5.Text = ""
        txtComments.Text = ""
        chkFwd.Checked = False
        chkMail.Enabled = False
        R1.Enabled = True
        R2.Enabled = True
        R3.Enabled = True
        R4.Enabled = True
        R5.Enabled = True
        R6.Enabled = True
        R7.Enabled = True

        R1.SelectedValue = "No"
        R2.SelectedValue = "No"
        R3.SelectedValue = "No"
        R4.SelectedValue = "No"
        R5.SelectedValue = "No"
        R6.SelectedValue = "No"
        R7.SelectedValue = "No"

        T1.Enabled = False
        T2.Enabled = False
        T3.Enabled = False
        T4.Enabled = False
        T5.Enabled = False
        ImageButton2.Enabled = False
        ImageButton3.Enabled = False
        ImageButton4.Enabled = False
        ImageButton5.Enabled = False
        ImageButton6.Enabled = False
        btnsave.Enabled = True
        txtCalledDate.Enabled = False
        txtComments.Enabled = True
        imgCalledDate.Enabled = False
        imgIntvDate.Enabled = False
    End Sub

    Private Sub DisbaleActionNo()
        T1.Enabled = False
        T2.Enabled = False
        T3.Enabled = False
        T4.Enabled = False
        T5.Enabled = False
        txtCalledDate.Enabled = False
        txtIntvDate.Enabled = False
        txtparentscalledsaid.Enabled = False
        ' ReqPatentsCalled.Enabled = False
        txtparentsinterviewssaid.Enabled = False

        ImageButton2.Enabled = False
        ImageButton3.Enabled = False
        ImageButton4.Enabled = False
        ImageButton5.Enabled = False
        ImageButton6.Enabled = False
        imgCalledDate.Enabled = False
        imgIntvDate.Enabled = False
    End Sub

    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Trim = "" Then
            Return (False)
        Else
            Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            Dim re As New Regex(strRegex)
            If re.IsMatch(inputEmail) Then
                Return (True)
            Else
                Return (False)
            End If
        End If
    End Function
    Private Function CheckincidentDateIsValid() As Boolean
        Try
            If R1.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(txtCalledDate.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R2.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(txtIntvDate.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R3.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T1.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R4.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T2.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R5.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T3.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R6.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T4.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R7.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T5.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If
            Return True
        Catch ex As Exception

        End Try
    End Function
    Private Function CheckDateIsValid() As Boolean
        Try
            If R1.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(txtCalledDate.Text), Date.Now()) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R2.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(txtIntvDate.Text), Date.Now()) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            Return True
        Catch ex As Exception

        End Try
    End Function
    Private Function CahekDateWithTodate(ByVal CheckDate As Date) As Boolean
        Try
            If DateDiff(DateInterval.Day, CheckDate, Date.Now()) < 0 Then
                Return False
                Exit Function
            End If
            Return True
        Catch ex As Exception
        End Try
    End Function

    Public Sub Send(ByVal ds As DataSet)
        Dim MailBody As String = ""
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString()
            Dim str_query = "Select * FROM COM_MANAGE_EMAIL"

            Dim ds1 As DataSet
            ds1 = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)

            If ds1.Tables(0).Rows.Count >= 1 Then
                Dim Subject = ds1.Tables(0).Rows(0).Item("EML_SUBJECT").ToString().Trim()
                Dim FromEmailid As String = ds1.Tables(0).Rows(0).Item("EML_FROM").ToString().Trim()
                Dim DisplayName = ds1.Tables(0).Rows(0).Item("EML_DISPLAY").ToString().Trim()
                Dim UserName = ds1.Tables(0).Rows(0).Item("EML_USERNAME").ToString().Trim()
                Dim Password = ds1.Tables(0).Rows(0).Item("EML_PASSWORD").ToString().Trim()
                Dim Host = ds1.Tables(0).Rows(0).Item("EML_HOST").ToString().Trim().Trim()
                Dim Port = Convert.ToInt16(ds1.Tables(0).Rows(0).Item("EML_PORT").ToString().Trim())
                Dim Hasattachments As Boolean = ds1.Tables(0).Rows(0).Item("EML_ATTACHMENT")

                If ds.Tables(0).Rows.Count > 0 Then '' If contain data 
                    Dim i
                    Dim rcount = 0
                    For i = Session("count") To ds.Tables(0).Rows.Count - 1
                        Dim email_id As New StringBuilder
                        email_id.Append(ds.Tables(0).Rows(i).Item("Parentemail").ToString())
                        Dim emailcheck As String = email_id.ToString()
                        Session("count") = i + 1
                        rcount = rcount + 1

                        If isEmail(emailcheck) Then
                            lblmessage.Text = email.SendPlainTextEmails(FromEmailid, emailcheck, Subject, MailBody, UserName, Password, Host, Port, 0, Hasattachments)
                            If lblmessage.Text.IndexOf("Error") > -1 Then
                                'Send Failed
                            Else
                                'Send Succedded
                            End If
                        Else
                            'Invalid Email Id
                        End If
                    Next

                End If
            End If
        Catch ex As Exception
            lblmessage.Text = "Error : <br>" & ex.Message ''& "<br> or <br> Unable to connect to the remote server.Please check network connectivity."
        End Try
    End Sub

    Protected Sub chkFwd_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFwd.CheckedChanged
        If chkFwd.Checked = False Then
            cmbFwdTo.Enabled = False
            ddlStaff.Items.Clear()
            ddlStaff.Enabled = False
            txtComments.Enabled = False
        Else
            PopulateClassTeacher()
            ' BIndFWDDesignations(GetDesignationID(ddstaff.SelectedValue))
            BIndFWDDesignations(GetDesignationID(hfstaff_id.Value))
            cmbFwdTo.Enabled = True
            ddlStaff.Enabled = True
            txtComments.Enabled = True
        End If
    End Sub


    Protected Sub R3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R3.SelectedIndexChanged
        If R3.SelectedValue = "No" Then
            T1.Enabled = False
            ImageButton2.Enabled = False
        Else
            T1.Enabled = True
            ImageButton2.Enabled = True
        End If

    End Sub

    Protected Sub R4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R4.SelectedIndexChanged
        If R4.SelectedValue = "No" Then
            T2.Enabled = False
            ImageButton3.Enabled = False
        Else
            T2.Enabled = True
            ImageButton3.Enabled = True
        End If
    End Sub

    Protected Sub R5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R5.SelectedIndexChanged
        If R5.SelectedValue = "No" Then
            T3.Enabled = False
            ImageButton4.Enabled = False
        Else
            T3.Enabled = True
            ImageButton4.Enabled = True
        End If
    End Sub

    Protected Sub R6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R6.SelectedIndexChanged
        If R6.SelectedValue = "No" Then
            T4.Enabled = False
            ImageButton5.Enabled = False
        Else
            T4.Enabled = True
            ImageButton5.Enabled = True
        End If
    End Sub

    Protected Sub R7_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R7.SelectedIndexChanged
        If R7.SelectedValue = "No" Then
            T5.Enabled = False
            ImageButton6.Enabled = False
        Else
            T5.Enabled = True
            ImageButton6.Enabled = True
        End If
    End Sub

    Protected Sub R1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R1.SelectedIndexChanged
        If R1.SelectedValue = "No" Then
            txtCalledDate.Enabled = False
            imgCalledDate.Enabled = False
            txtparentscalledsaid.Enabled = False
            'ReqPatentsCalled.Enabled = False
        Else
            txtCalledDate.Enabled = True
            imgCalledDate.Enabled = True
            txtparentscalledsaid.Enabled = True
            'ReqPatentsCalled.Enabled = True
        End If
    End Sub

    Protected Sub R2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R2.SelectedIndexChanged
        If R2.SelectedValue = "No" Then
            txtIntvDate.Enabled = False
            imgIntvDate.Enabled = False
            txtparentsinterviewssaid.Enabled = False
        Else
            txtparentsinterviewssaid.Enabled = True
            txtIntvDate.Enabled = True
            imgIntvDate.Enabled = True
        End If
    End Sub

    Protected Sub cmbStudent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbStudent.SelectedIndexChanged
        Try
            If cmbStudent.SelectedValue <> "" Then
                If CheckActionExist(cmbStudent.SelectedValue) = True Then
                    ActionTakendetails(cmbStudent.SelectedValue)
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub cmbFwdTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFwdTo.SelectedIndexChanged

    End Sub

    Protected Sub imgbtnSibling_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnSibling.Click
        Try
            If (radStudent.Checked = True) Then
                lblCaption.Text = GetStudentNameByNumber(txtPar_Sib.Text)
            Else
                lblCaption.Text = ""
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgbtnstaff_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnstaff.Click
        Try
            If hfstaff_id.Value <> "" Then
                lblDesignation.Text = GetDesignation(hfstaff_id.Value)
                BIndFWDDesignations(GetDesignationID(hfstaff_id.Value))
            End If
        Catch ex As Exception

        End Try
    End Sub

#Region "FileUpload"
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblValidIncidentDate.CssClass = "error"
            Else
                lblValidIncidentDate.CssClass = "error"
            End If
        Else
            lblValidIncidentDate.CssClass = ""
        End If
        lblValidIncidentDate.Text = Message
    End Sub

    Function ContainsSpecialChars(s As String) As Boolean
        Return s.IndexOfAny("[~`!@#$%^&*()-+=|{}':;,<>/?]".ToCharArray) <> -1
    End Function
    Private Function GetExtension(ByVal FileName As String) As String
        Dim Extension As String
        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)
        Return Extension
    End Function
    Public Function CurrentDate() As String
        'Dim lastDate As String = Day(DateTime.Now).ToString() & "/" & MonthName(Month(DateTime.Now), True) & "/" & Year(DateTime.Now).ToString()
        'lastDate = Convert.ToDateTime(lastDate).ToString("ddMMMyyyy")
        'Return lastDate
        Dim lastdate As String = DateTime.Now.Ticks.ToString
        Return lastdate
    End Function

    Function callSavefile(ByVal bsuid As String, ByRef strans As SqlTransaction) As Integer
        callSavefile = 1000
        Try
            Dim status As Integer = 0
            Dim STU_BSU_ID As String = bsuid
            Dim PHOTO_PATH As String = String.Empty
            Dim ConFigPath As String = WebConfigurationManager.AppSettings("FileUploadPathFinal").ToString
            Dim path As String
            If ViewState("ACTIVITYFILEPATH") <> "" Then

                If Not Directory.Exists(ConFigPath & bsuid & "\" & Session("MRG_ID") & "\") Then
                    Directory.CreateDirectory(ConFigPath & bsuid & "\" & Session("MRG_ID") & "\")
                    path = ConFigPath & bsuid & "\" & Session("MRG_ID") & "\"
                Else
                    Dim dold As New DirectoryInfo(ConFigPath & bsuid & "\" & Session("MRG_ID") & "\")
                    path = ConFigPath & bsuid & "\" & Session("MRG_ID") & "\"
                    Dim fiold() As System.IO.FileInfo
                    fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fiold.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fiold
                            f.Delete()
                        Next
                    End If
                End If

                Dim d As New DirectoryInfo(ViewState("ACTIVITYFILEPATH"))

                Dim fi() As System.IO.FileInfo
                fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fi.Length = 1 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fi
                        f.MoveTo(path & f.Name)
                        Dim Str_fullpath As String = bsuid & "\" & Session("MRG_ID") & "\" & "" & f.Name
                        If path <> "" Then
                            status = callDocSave(strans, Str_fullpath, "U", Session("MRG_ID"))
                        Else
                            callSavefile = 1000
                            strans.Rollback()
                            Exit Function
                        End If
                    Next
                End If
                d.Delete()
                ViewState("ACTIVITYFILEPATH") = ""
            End If
            callSavefile = status
        Catch ex As Exception
            UtilityObj.Errorlog("From callSavefile: " + ex.Message, "OASIS ACTIVITY SERVICES")
            strans.Rollback()
            callSavefile = 1000
        End Try
    End Function
    Protected Sub saveupload_Click(sender As Object, e As EventArgs)
        Try
            Dim str_error As String = ""
            Dim obj As Object = sender.parent
            Dim BSUID = Session("sbsuid")
            Dim errCount As Integer = 0
            Dim fileCount As Integer = 0
            Dim returnvalue As Boolean = False
            Dim ConFigPath As String = WebConfigurationManager.AppSettings("FileUploadPathTemp").ToString
            Dim Tempath As String = ConFigPath & BSUID & "\Achievement\"

            'to remove exising files in temp path
            Dim dold As New DirectoryInfo(Tempath)
            Dim fiold() As System.IO.FileInfo
            If dold.Exists Then
                fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fiold.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fiold
                        f.Delete()
                    Next
                End If
            End If
            ' end of deletion of exising files
            ' For Each rptr As RepeaterItem In repdoc.Items
            ' Dim lblfile As Label = CType(rptr.FindControl("lblfile"), Label)
            ' Dim Fileupload As FileUpload = CType(rptr.FindControl("Fileupload"), FileUpload)
            ' Dim hffile As HiddenField = CType(rptr.FindControl("hffile"), HiddenField)
            Dim UploadfileName As String = ""
            If upload.HasFile Then
                Try
                    fileCount = fileCount + 1
                    Dim strPostedFileName As String = upload.FileName 'get the file name
                    Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower
                    If strPostedFileName <> "" And ConFigPath <> "" Then
                        Dim intDocFileLength As Integer = upload.FileContent.Length ' get the file size
                        Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower
                        Dim ContainsSplChar As Boolean = ContainsSpecialChars(strPostedFileName)
                        Dim FileNameLength As Integer = strPostedFileName.Length
                        UploadfileName = "Achievement" & "_" & CurrentDate() & strExtn
                        Dim s As String = strPostedFileName
                        Dim result() As String
                        result = s.Split(".")
                        Dim fileext = GetExtension(strExtn)
                        'Checking file extension
                        If (strPostedFileName <> String.Empty) And fileext <> "" Then
                            If Not (fileext.ToUpper = "PDF") And Not (fileext.ToUpper = "PNG") And Not (fileext.ToUpper = "JPG") Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Upload PDF Files Only!!"
                                Else
                                    str_error = str_error & "Upload PDF Files Only!!"
                                End If
                                returnvalue = False
                                '  Exit Sub
                                'Checking file extension length
                            ElseIf fileext.Length = 0 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "file with out extension not allowed...!!"
                                Else
                                    str_error = str_error & "file with out extension not allowed...!!"
                                End If
                                returnvalue = False
                                '   Exit Sub
                                'Checking Special Characters in file name

                            ElseIf ContainsSplChar = True Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                Else
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub
                                'Checking FileName length

                            ElseIf FileNameLength = 0 Or FileNameLength > 1500 Then '255
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Error with file Name Length..!!"
                                Else
                                    str_error = str_error & "Error with file Name Length..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub

                            ElseIf result.Length > 2 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Invalid file Name..!!"
                                Else
                                    str_error = str_error & "Invalid file Name..!!"
                                End If

                                returnvalue = False
                                'Exit Sub

                            ElseIf upload.FileContent.Length > 5000000 Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                Else
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                End If

                                returnvalue = False
                                '' Exit Sub

                            ElseIf Not (strExtn = ".pdf") And Not strExtn = ".jpg" And Not strExtn = ".png" Then 'exten type
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File type is different than allowed!"
                                Else
                                    str_error = str_error & "File type is different than allowed!"
                                End If

                                returnvalue = False
                                'Exit Sub
                            Else
                                str_error = ""

                                If Not Directory.Exists(Tempath) Then
                                    Directory.CreateDirectory(Tempath)
                                End If
                                If errCount = 0 Then
                                    upload.SaveAs(Tempath & UploadfileName)
                                End If
                                ViewState("ACTIVITYFILEPATH") = ConFigPath & BSUID & "\Achievement"
                            End If
                        End If
                    End If

                Catch ex As Exception
                    ' returnvalue = False
                    '  Return returnvalue
                    ShowMessage(ex.Message, True)
                    Exit Sub
                End Try
            End If
            'Next

            If str_error <> "" Then
                ShowMessage(str_error, True)
                ' returnvalue = False
                ' Return returnvalue
                Exit Sub
            ElseIf fileCount <> 1 Then
                If str_error <> "" Then
                    str_error = str_error & "<br /><br />"
                    str_error = str_error & "Please select file!!!"
                Else
                    str_error = str_error & "Please select file!!!"
                End If
                '  returnvalue = False
                ' Return returnvalue
                ShowMessage(str_error, True)
                Exit Sub
            Else
                If Not (errCount = 0 And fileCount = 1) Then
                    ' Dim status As Integer = callSave(BSUID, ActivityID, strans)
                    ' If status = 1000 Then
                    If str_error <> "" Then
                        str_error = str_error & "<br /><br />"
                        str_error = str_error & "Please upload file!"
                    Else
                        str_error = str_error & "Please upload file!"
                    End If
                    ShowMessage(str_error, True)
                    'returnvalue = False
                Else
                    'If str_error <> "" Then
                    '    str_error = str_error & "<br /><br />"
                    '    str_error = str_error & "File saved successfully!"
                    'Else
                    '    str_error = str_error & "File saved successfully!"
                    '    ShowMessage(str_error, True)
                    'End If
                    '   returnvalue = True
                    updpnl.Visible = False
                    lnkShowFile.Visible = True
                    lnkShowFile.Text = UploadfileName
                    Dim uploadpath As String = ("Temp\" & BSUID & "\" & cmbCategory.SelectedItem.Text & "\" & UploadfileName)
                    uploadpath = uploadpath.Replace("\", "\\")
                    Dim strExtn As String = System.IO.Path.GetExtension(UploadfileName).ToLower
                    lnkShowFile.Attributes.Add("OnClick", "javascript: return showDocument('" & uploadpath & "','" & strExtn & "')")
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From saveupload_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
#End Region

End Class
