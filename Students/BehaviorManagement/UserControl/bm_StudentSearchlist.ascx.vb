Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_UserControl_bm_StudentSearchlist
    Inherits System.Web.UI.UserControl

    Dim studClass As New studClass
    ' Delegate declaration 
    Public Delegate Sub OnButtonClick(ByVal ds As DataSet)


    ' Event declaration 
    Public Event btnHandler As OnButtonClick
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                btnaddstu.Visible = False
                Session("ds") = Nothing
                Hiddenbsu.Value = Session("sbsuid") ''
                Hiddenacyid.Value = Session("Current_ACY_ID") '"3" '
                Hiddenacdid.Value = Session("Current_ACD_ID") '"79" '


                h_Selected_menu_1.Value = "LI__../../../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../../../Images/operations/like.gif"

                Dim list As New ListItem
                list.Text = "All"
                list.Value = "0"
                ddshift.Items.Insert(0, list)
                ddclm.Items.Insert(0, list)
                ddstream.Items.Insert(0, list)
                ddsection.Items.Insert(0, list)
                BindPartAControls()
                BindClm()
                BindShift()
                BindStream()
                BindGrade()
                BindSection()

            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        ' str_Sid_img = h_Selected_menu_1.Value.Split("__")
        ' getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        'str_Sid_img = h_Selected_menu_3.Value.Split("__")
        'getid3(str_Sid_img(2))

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If GrdView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = GrdView.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If GrdView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = GrdView.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Sub BindPartAControls()

        Dim acc As New studClass
        Dim accid = Hiddenacyid.Value
        ddgrade = acc.PopulateGrade(ddgrade, Hiddenacdid.Value)
        Dim list3 As New ListItem
        list3.Text = "All"
        list3.Value = "0"
        ddgrade.Items.Insert(0, list3)


    End Sub
    Public Sub BindClm()
        ddclm.Items.Clear()
        ddclm.Visible = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = ""
        str_query = "select distinct ACADEMICYEAR_D.acd_id,CURRICULUM_M.CLM_DESCR,CLM_ID from ACADEMICYEAR_D " & _
                    " inner join CURRICULUM_M on ACADEMICYEAR_D.ACD_CLM_ID=CURRICULUM_M.CLM_ID and ACADEMICYEAR_D.ACD_BSU_ID= '" & Hiddenbsu.Value & "'" & _
                    " and ACD_ACY_ID='" & Hiddenacyid.Value & "' AND ACD_CURRENT='TRUE'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then

            ddclm.DataSource = ds
            ddclm.DataTextField = "CLM_DESCR"
            ddclm.DataValueField = "ACD_ID"
            ddclm.DataBind()
            ddclm.SelectedValue = ds.Tables(0).Rows(0)("ACD_ID")
        End If
        Dim list As New ListItem
        list.Text = "All"
        list.Value = "0"
        ddclm.Items.Insert(0, list)


    End Sub


    Protected Sub ddgrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddgrade.SelectedIndexChanged
     
        BindSection()

    End Sub
    Public Sub BindSection()
        ddsection.Items.Clear()
        If ddgrade.SelectedIndex > 0 Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sql_query = "select distinct sct_descr,sct_id from section_m where sct_bsu_id='" & Hiddenbsu.Value & "' and sct_acd_id='" & Hiddenacdid.Value & "' "

            If (ddgrade.SelectedIndex > 0) Then
                sql_query += "and sct_grd_id='" & ddgrade.SelectedValue & "'"
            End If

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query & "order by sct_descr")
            If ds.Tables(0).Rows.Count > 0 Then
                ddsection.DataSource = ds
                ddsection.DataTextField = "SCT_DESCR"
                ddsection.DataValueField = "SCT_ID"
                ddsection.DataBind()
            End If
        End If
        Dim list As New ListItem
        list.Text = "All"
        list.Value = "0"
        ddsection.Items.Insert(0, list)
    End Sub

    Protected Sub ddshift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddshift.SelectedIndexChanged
        BindStream()
        BindGrade()
        BindSection()
    End Sub
    Public Sub BindStream()
        ddstream.Visible = True
        Dim stu As New studClass
        ddstream = PopulateGradeStream(ddstream, Hiddenacdid.Value, ddshift.SelectedValue)
        Dim list3 As New ListItem
        list3.Text = "All"
        list3.Value = "0"
        ddstream.Items.Insert(0, list3)
    End Sub
    Public Function PopulateGradeStream(ByVal ddl As DropDownList, ByVal acdid As String, ByVal shfid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT stm_descr,stm_id FROM stream_m order by stm_id"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            ddl.DataSource = ds
            ddl.DataTextField = "stm_descr"
            ddl.DataValueField = "stm_id"
            ddl.DataBind()

            ddl.SelectedValue = ds.Tables(0).Rows(0)("stm_id")
        End If

        Return ddl
    End Function
    Public Sub BindShift()
        ddshift.Visible = True
        Dim stu As New studClass
        ddshift = PopulateGradeShift(ddshift, ddgrade.SelectedValue, Hiddenacdid.Value)
        Dim list3 As New ListItem
        list3.Text = "All"
        list3.Value = "0"
        ddshift.Items.Insert(0, list3)
    End Sub
    Public Function PopulateGradeShift(ByVal ddl As DropDownList, ByVal grdid As String, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct shf_descr,shf_id FROM shifts_m WHERE SHF_BSU_ID = '" & Hiddenbsu.Value & "' ORDER BY shf_id"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            ddl.DataSource = ds
            ddl.DataTextField = "shf_descr"
            ddl.DataValueField = "shf_id"
            ddl.DataBind()
            ddl.SelectedValue = ds.Tables(0).Rows(0)("shf_id")
        End If
        Return ddl
    End Function
    Public Sub BindGrade()
        Dim studClass As New studClass
        ddgrade = PopulateGrade(ddgrade, ddclm.SelectedValue.ToString())
        Dim list3 As New ListItem
        list3.Text = "All"
        list3.Value = "0"
        ddgrade.Items.Insert(0, list3)
    End Sub
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim gradefilter As String = ""

        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE grade_bsu_m.grm_grd_id=grade_m.grd_id and grm_acd_id=" + acdid + " "

        If ddclm.SelectedIndex > 0 Then
            'If gradefilter <> "" Then
            '    gradefilter += " AND " & "grade_bsu_m.grm_acd_id in('" & ddclm.SelectedValue & "')"
            'Else
            gradefilter += " AND grade_bsu_m.grm_acd_id in('" & ddclm.SelectedValue & "')"
            '' End If
        End If

        If ddstream.SelectedIndex > 0 Then
            'If gradefilter <> "" Then
            '    gradefilter += " AND " & "grade_bsu_m.grm__STM_ID in('" & ddstream.SelectedValue & "')"
            'Else
            gradefilter += "AND grade_bsu_m.grm_STM_ID in('" & ddstream.SelectedValue & "')"
            ''End If

        End If
        If ddshift.SelectedIndex > 0 Then
            'If gradefilter <> "" Then
            '    gradefilter += " AND " & "grade_bsu_m.grm__shf_id in('" & ddshift.SelectedValue & "')"
            'Else
            gradefilter += "AND grade_bsu_m.grm_shf_id in('" & ddshift.SelectedValue & "')"
            '' End If
        End If


        ' and  order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query & gradefilter & "order by grd_displayorder")

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function
    Protected Sub ddclm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddclm.SelectedIndexChanged

        BindShift()
        BindStream()
        BindGrade()
        BindSection()
    End Sub

    Protected Sub ddstream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrade()
        BindSection()
    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Session("ds") = Nothing
        Search()
    End Sub
    Public Sub Search()
        Dim txtstudentsearchname As New TextBox
        Dim txtstudentsearchnumber As New TextBox
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PartAFilter As String = ""

        PartAFilter = "select stu_no,stu_id,(ISNULL(STUDENT_M.STU_FIRSTNAME, '') + ' ' + ISNULL(STUDENT_M.STU_MIDNAME, '') + ' '+ ISNULL( STUDENT_M.STU_LASTNAME, ''))AS StudName,sct_descr,grm_display,stm_descr,shf_descr ,student_m.STU_PRIMARYCONTACT , " & _
                      " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FEMAIL  when 'M' then STUDENT_D.STS_MEMAIL  when 'G' then STUDENT_D.STS_GEMAIL  end) as ParentEmail  , " & _
                      " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FMOBILE  when 'M' then STUDENT_D.STS_MMOBILE  when 'G' then STUDENT_D.STS_GMOBILE  end) as ParentMobile , " & _
                      " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FFIRSTNAME + ' ' + STUDENT_D.STS_FLASTNAME when 'M' then STUDENT_D.STS_MFIRSTNAME + ' ' + STUDENT_D.STS_MLASTNAME when 'G' then STUDENT_D.STS_GFIRSTNAME + ' ' + STUDENT_D.STS_GLASTNAME end) as parent_name " & _
                      " from STUDENT_M " & _
                      " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID  " & _
                      " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                      " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                      " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                      " INNER JOIN STUDENT_D ON  STUDENT_D.STS_STU_ID= STUDENT_M.STU_SIBLING_ID inner join grade_m on grade_bsu_m.grm_grd_id=grd_id "

        PartAFilter += " where student_m.stu_bsu_id in('" & Hiddenbsu.Value & "')and(isnull(STUDENT_M.stu_leavedate,getdate())>=getdate())and STU_CURRSTATUS<>'cn'"

        If ddclm.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_acd_id in('" & ddclm.SelectedValue & "')"
            Else
                PartAFilter = "student_m.stu_acd_id in('" & ddclm.SelectedValue & "')"
            End If

        End If

        If ddgrade.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_grd_id in('" & ddgrade.SelectedValue & "')"
            Else
                PartAFilter = "student_m.stu_grd_id in('" & ddgrade.SelectedValue & "')"
            End If

        End If

        If ddsection.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_sct_id in('" & ddsection.SelectedValue & "')"
            Else
                PartAFilter = "student_m.stu_sct_id in('" & ddsection.SelectedValue & "')"
            End If
        End If

        If ddstream.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.STU_STM_ID in('" & ddstream.SelectedValue & "')"
            Else
                PartAFilter = "student_m.STU_STM_ID in('" & ddstream.SelectedValue & "')"
            End If

        End If
        If ddshift.SelectedIndex > 0 Then
            If PartAFilter <> "" Then
                PartAFilter += " AND " & "student_m.stu_shf_id in('" & ddshift.SelectedValue & "')"
            Else
                PartAFilter = "student_m.stu_shf_id in('" & ddshift.SelectedValue & "')"
            End If
        End If
        If (GrdView.Rows.Count > 0) Then

            txtstudentsearchnumber = GrdView.HeaderRow.FindControl("txtFeeId")
            If txtstudentsearchnumber.Text.Trim() <> "" Then
                If PartAFilter <> "" Then
                    PartAFilter += " AND " & "student_m.STU_NO LIKE '%" & txtstudentsearchnumber.Text.Trim() & "%'"
                Else
                    PartAFilter = "student_m.STU_NO LIKE '%" & txtstudentsearchnumber.Text.Trim() & "%'"
                End If
            End If

            txtstudentsearchname = GrdView.HeaderRow.FindControl("txtStudName")
            If txtstudentsearchname.Text.Trim() <> "" Then
                If PartAFilter <> "" Then
                    PartAFilter += " AND (student_m.STU_FIRSTNAME LIKE '%" & txtstudentsearchname.Text.Trim() & "%' OR student_m.STU_LASTNAME LIKE '%" & txtstudentsearchname.Text.Trim() & "%' OR STUDENT_M.STU_MIDNAME LIKE '%" & txtstudentsearchname.Text.Trim() & "%')"

                Else
                    PartAFilter = "(student_m.STU_FIRSTNAME LIKE '%" & txtstudentsearchname.Text.Trim() & "%' OR student_m.STU_LASTNAME LIKE '%" & txtstudentsearchname.Text.Trim() & "%' OR STUDENT_M.STU_MIDNAME LIKE '%" & txtstudentsearchname.Text.Trim() & "%')"
                End If
            End If
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, PartAFilter & "order by GRD_DISPLAYORDER,SCT_DESCR,StudName")
        If ds.Tables(0).Rows.Count > 0 Then
            btnaddstu.Visible = True
            GrdView.DataSource = ds
            GrdView.DataBind()
        Else
            GrdView.DataSource = ds
            GrdView.DataBind()
            btnaddstu.Visible = False
        End If
        Session("ds") = ds
    End Sub
   
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  COALESCE('%" & value & "'," + field + ") "
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE COALESCE('%" & value & "'," + field + ") "
            End If
        End If
        Return strFilter
    End Function
    Public Function GetSearchString1(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = "  " + field + " LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "NLI" Then
                strFilter = "   " + field + " NOT LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "SW" Then
                strFilter = "  " + field + "  LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "NSW" Then
                strFilter = "  " + field + "  NOT LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "EW" Then
                strFilter = "  " + field + " LIKE  COALESCE('%" & value & "'," + field + ") "
            ElseIf strSearch = "NEW" Then
                strFilter = "  " + field + " NOT LIKE COALESCE('%" & value & "'," + field + ") "
            End If
        End If
        Return strFilter
    End Function
    Public Sub SearchQ()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim sql_query = "select stu_no,stu_id, stu_firstname,STU_LASTNAME,sct_descr,grm_display,stm_descr,shf_descr " & _
                      " ,student_m.STU_PRIMARYCONTACT , " & _
                      " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FEMAIL  when 'M' then STUDENT_D.STS_MEMAIL  when 'G' then STUDENT_D.STS_GEMAIL  end) as ParentEmail  , " & _
                      " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FMOBILE  when 'M' then STUDENT_D.STS_MMOBILE  when 'G' then STUDENT_D.STS_GMOBILE  end) as ParentMobile , " & _
                      " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FFIRSTNAME + ' ' + STUDENT_D.STS_FLASTNAME when 'M' then STUDENT_D.STS_MFIRSTNAME + ' ' + STUDENT_D.STS_MLASTNAME when 'G' then STUDENT_D.STS_GFIRSTNAME + ' ' + STUDENT_D.STS_GLASTNAME end) as parent_name " & _
                      " from STUDENT_M " & _
                      " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID  " & _
                      " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                      " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                      " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                      " INNER JOIN STUDENT_D ON  STUDENT_D.STS_STU_ID= STUDENT_M.STU_ID "

        sql_query += " where student_m.stu_bsu_id in('" & Hiddenbsu.Value & "')"

        Dim strSidsearch As String()
        Dim SearchFilter As String = ""

        Dim Studentid As String = DirectCast(GrdView.HeaderRow.FindControl("txtSearch1"), TextBox).Text.Trim()
        Dim studentname As String = DirectCast(GrdView.HeaderRow.FindControl("txtSearch2"), TextBox).Text.Trim()
        Dim Parentname As String = DirectCast(GrdView.HeaderRow.FindControl("txtSearch3"), TextBox).Text.Trim()
        Dim email As String = DirectCast(GrdView.HeaderRow.FindControl("txtSearch4"), TextBox).Text.Trim()
        Dim mobile As String = DirectCast(GrdView.HeaderRow.FindControl("txtSearch5"), TextBox).Text.Trim()
        ''Dim company As DropDownList = DirectCast(GrdView.HeaderRow.FindControl("DropSearch1"), DropDownList)

        If Studentid <> "" Then
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & GetSearchString1("student_m.stu_no", Studentid.Trim(), strSidsearch(0))
            Else
                SearchFilter = SearchFilter & GetSearchString("student_m.stu_no", Studentid.Trim(), strSidsearch(0))
            End If

        End If

        If studentname <> "" Then
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & GetSearchString1("student_m.stu_firstname + student_m.stu_lastname", studentname.Replace(" ", "").Trim(), strSidsearch(0))
            Else
                SearchFilter = SearchFilter & GetSearchString("student_m.stu_firstname +  student_m.stu_lastname", studentname.Replace(" ", "").Trim(), strSidsearch(0))
            End If

        End If

        If Parentname <> "" Then
            strSidsearch = h_Selected_menu_3.Value.Split("__")
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & GetSearchString1("STUDENT_D.sts_ffirstname +  STUDENT_D.sts_flastname+STUDENT_D.sts_mfirstname + STUDENT_D.sts_mlastname + STUDENT_D.sts_gfirstname +  STUDENT_D.sts_glastname", Parentname.Replace(" ", "").Trim(), strSidsearch(0))
            Else
                SearchFilter = SearchFilter & GetSearchString("STUDENT_D.sts_ffirstname +  STUDENT_D.sts_flastname+STUDENT_D.sts_mfirstname +  STUDENT_D.sts_mlastname + STUDENT_D.sts_gfirstname +  STUDENT_D.sts_glastname", Parentname.Replace(" ", "").Trim(), strSidsearch(0))
            End If

        End If

        If email <> "" Then
            strSidsearch = h_Selected_menu_4.Value.Split("__")
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & GetSearchString1("STUDENT_D.sts_femail +STUDENT_D.sts_memail + STUDENT_D.sts_gemail", email.Replace(" ", "").Trim(), strSidsearch(0))
            Else
                SearchFilter = SearchFilter & GetSearchString("STUDENT_D.sts_femail +STUDENT_D.sts_memail + STUDENT_D.sts_gemail", email.Replace(" ", "").Trim(), strSidsearch(0))
            End If

        End If
        If mobile <> "" Then
            strSidsearch = h_Selected_menu_5.Value.Split("__")
            If SearchFilter = "" Then
                SearchFilter = SearchFilter & GetSearchString1("STUDENT_D.sts_fmobile +STUDENT_D.sts_mmobile+STUDENT_D.sts_gmobile", mobile.Replace(" ", "").Trim(), strSidsearch(0))
            Else
                SearchFilter = SearchFilter & GetSearchString("STUDENT_D.sts_fmobile +STUDENT_D.sts_mmobile+STUDENT_D.sts_gmobile", mobile.Replace(" ", "").Trim(), strSidsearch(0))
            End If

        End If

        'If company.SelectedIndex > 0 Then
        '    If SearchFilter = "" Then
        '        SearchFilter = SearchFilter & " (STUDENT_D.sts_f_comp_id like coalesce('" & company.SelectedValue & "',STUDENT_D.sts_f_comp_id) or STUDENT_D.sts_m_comp_id like coalesce('" & company.SelectedValue & "',STUDENT_D.sts_m_comp_id) or STUDENT_D.sts_g_comp_id like coalesce('" & company.SelectedValue & "',STUDENT_D.sts_g_comp_id))"
        '    Else
        '        SearchFilter = SearchFilter & " and (STUDENT_D.sts_f_comp_id like coalesce('" & company.SelectedValue & "',STUDENT_D.sts_f_comp_id) or STUDENT_D.sts_m_comp_id like coalesce('" & company.SelectedValue & "',STUDENT_D.sts_m_comp_id) or STUDENT_D.sts_g_comp_id like coalesce('" & company.SelectedValue & "',STUDENT_D.sts_g_comp_id))"
        '    End If

        'End If

        If SearchFilter <> "" Then
            sql_query = sql_query & " and " & SearchFilter
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        GrdView.DataSource = ds
        GrdView.DataBind()
        Session("ds") = ds

    End Sub
    Public Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        SearchQ()
    End Sub
    Protected Sub ddlsearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SearchQ()
    End Sub

    Protected Sub GrdView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdView.PageIndexChanging
        GrdView.PageIndex = e.NewPageIndex
        If Session("ds") Is Nothing Then
            Session("ds") = Nothing
            Search()
        Else
            GrdView.DataSource = Session("ds")
            GrdView.DataBind()
        End If
    End Sub

    Protected Sub btnaddstu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnaddstu.Click
        Dim dt As New DataTable
        dt.Columns.Add("STU_ID") ''
        dt.Columns.Add("STU_NO") ''
        dt.Columns.Add("STU_FIRSTNAME") ''
        dt.Columns.Add("STU_LASTNAME") ''
        dt.Columns.Add("grm_display") ''
        dt.Columns.Add("sct_descr") ''
        dt.Columns.Add("parent_name") ''
        dt.Columns.Add("ParentEmail") ''
        dt.Columns.Add("ParentMobile") ''

        For Each row As GridViewRow In GrdView.Rows
            Dim dr As DataRow = dt.NewRow()
            Dim ch1 As CheckBox = DirectCast(row.FindControl("ch2"), CheckBox)
            If ch1.Checked Then
                dr.Item("STU_ID") = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
                dr.Item("STU_NO") = DirectCast(row.FindControl("lblno"), Label).Text.Trim()
                dr.Item("STU_FIRSTNAME") = DirectCast(row.FindControl("lblstufirst"), Label).Text.Trim()
                'dr.Item("STU_LASTNAME") = DirectCast(row.FindControl("lblstulast"), Label).Text.Trim()
                dr.Item("grm_display") = DirectCast(row.FindControl("lblstugrade"), Label).Text.Trim()
                dr.Item("sct_descr") = DirectCast(row.FindControl("lblstusection"), Label).Text.Trim()
                dr.Item("parent_name") = DirectCast(row.FindControl("lblstuparentname"), Label).Text.Trim()
                dr.Item("ParentEmail") = DirectCast(row.FindControl("lblstuparentemail"), Label).Text.Trim()
                dr.Item("ParentMobile") = DirectCast(row.FindControl("lblstuparentmobile"), Label).Text.Trim()
                dt.Rows.Add(dr)
            End If
        Next
        Dim ds As New DataSet
        ds.Tables.Add(dt)
        Session("ds1") = ds
        ''CollapsiblePanelExtender1.AutoCollapse = True
        CollapsiblePanelExtender1.Collapsed = True
        RaiseEvent btnHandler(ds)


    End Sub

    Protected Sub LinkAdvanceSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkAdvanceSearch.Click
        CollapsiblePanelExtender1.AutoCollapse = False

        'javascript:return false;
    End Sub

    Protected Sub LinkAdvanceSearch_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles LinkAdvanceSearch.Command
        CollapsiblePanelExtender1.AutoCollapse = False
    End Sub

    Protected Sub btnaddstu_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnaddstu.Command
        ' CollapsiblePanelExtender1.AutoCollapse = False
    End Sub

    Protected Sub btnSearchStudName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Search()
    End Sub

    Protected Sub btnSearchFeeID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Search()
    End Sub

    Protected Sub ddsection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddsection.SelectedIndexChanged

    End Sub
End Class
