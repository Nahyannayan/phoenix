Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_UserControl_bm_ActionView
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Hiddenincidentid.Value = Request.QueryString("inc_id").Trim()
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "SELECT *, (EMP_FNAME+' ' +EMP_LNAME) AS STAFF FROM BM.BM_MASTER A  " & _
                            " INNER JOIN EMPLOYEE_M  B ON  A.BM_REPORTING_STAFF_ID = B.EMP_ID " & _
                            " WHERE A.BM_ID='" & Hiddenincidentid.Value & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count >= 1 Then
                lbltype.Text = ds.Tables(0).Rows(0).Item("BM_INCIDENT_TYPE").ToString()
                If lbltype.Text = "FA" Then
                    lbltype.Text = "For Action"
                Else
                    lbltype.Text = "For Information"
                End If
                lbltodaydate.Text = Format(ds.Tables(0).Rows(0).Item("BM_ENTRY_DATE"), "dd-MMM-yyyy")
                lblreportingstaff.Text = ds.Tables(0).Rows(0).Item("STAFF").ToString()
                lblincidentdate.Text = Format(ds.Tables(0).Rows(0).Item("BM_INCIDENT_DATE"), "dd-MMM-yyyy")
                txtReportonincident.Text = ds.Tables(0).Rows(0).Item("BM_REPORT_ON_INCIDENT").ToString()
            End If
            BindGrid()
            GetWitNessDeatils()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If
    End Sub
    Protected Function GetNavigateUrl(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('bm_ActionViewDetails.aspx?Info_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
    End Function
    Public Sub BindGrid()
        'AS VIEWID
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str1 As String = "SELECT COUNT(*) FROM BM.INCIDENT_ROLE_ACCESS WHERE ROLE_ID='" & Session("sroleid") & "' AND BSU_ID='" & Session("sBsuid") & "'"
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str1)
        Dim str_query As String
        If sbm = 0 Then
            str_query = "SELECT STU_ID, STU_NO ,STUDENT_M.STU_FIRSTNAME,BM.BM_STUDENTSINVOLVED.BM_ID," & _
                            "STUDENT_M.STU_LASTNAME,SCT_DESCR,GRM_DISPLAY,STM_DESCR,SHF_DESCR ,STUDENT_M.STU_PRIMARYCONTACT, " & _
                            "(Case STUDENT_M.STU_PRIMARYCONTACT WHEN 'F' Then STUDENT_D.STS_FEMAIL  WHEN 'M' then STUDENT_D.STS_MEMAIL  WHEN 'G' then STUDENT_D.STS_GEMAIL  end) as ParentEmail, " & _
                            "(Case STUDENT_M.STU_PRIMARYCONTACT WHEN 'F' Then STUDENT_D.STS_FMOBILE  WHEN 'M' then STUDENT_D.STS_MMOBILE  WHEN 'G' then STUDENT_D.STS_GMOBILE  end) as ParentMobile ," & _
                            "(Case STUDENT_M.STU_PRIMARYCONTACT WHEN 'F' Then STUDENT_D.STS_FFIRSTNAME + ' ' + STUDENT_D.STS_FLASTNAME when 'M' then STUDENT_D.STS_MFIRSTNAME + ' ' + STUDENT_D.STS_MLASTNAME when 'G' then STUDENT_D.STS_GFIRSTNAME + ' ' + STUDENT_D.STS_GLASTNAME end) as parent_name " & _
                            " ,'' as SEC_EMP FROM STUDENT_M " & _
                            " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID  " & _
                            " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                            " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                            " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                            " INNER JOIN STUDENT_D ON  STUDENT_D.STS_STU_ID= STUDENT_M.STU_ID " & _
                            " INNER JOIN BM.BM_STUDENTSINVOLVED ON BM_STUDENTSINVOLVED.BM_STU_ID=STUDENT_M.STU_ID " & _
                            " WHERE BM.BM_STUDENTSINVOLVED.BM_ID=" & Hiddenincidentid.Value & " AND STU_BSU_ID='" & Session("sBSUID") & "'"
        Else
            str_query = "SELECT STU_ID, STU_NO ,STUDENT_M.STU_FIRSTNAME,BM.BM_STUDENTSINVOLVED.BM_ID," & _
                            "STUDENT_M.STU_LASTNAME,SECTION_M.SCT_DESCR,GRM_DISPLAY,STM_DESCR,SHF_DESCR ,STUDENT_M.STU_PRIMARYCONTACT, " & _
                            "(Case STUDENT_M.STU_PRIMARYCONTACT WHEN 'F' Then STUDENT_D.STS_FEMAIL  WHEN 'M' then STUDENT_D.STS_MEMAIL  WHEN 'G' then STUDENT_D.STS_GEMAIL  end) as ParentEmail, " & _
                            "(Case STUDENT_M.STU_PRIMARYCONTACT WHEN 'F' Then STUDENT_D.STS_FMOBILE  WHEN 'M' then STUDENT_D.STS_MMOBILE  WHEN 'G' then STUDENT_D.STS_GMOBILE  end) as ParentMobile ," & _
                            "(Case STUDENT_M.STU_PRIMARYCONTACT WHEN 'F' Then STUDENT_D.STS_FFIRSTNAME + ' ' + STUDENT_D.STS_FLASTNAME when 'M' then STUDENT_D.STS_MFIRSTNAME + ' ' + STUDENT_D.STS_MLASTNAME when 'G' then STUDENT_D.STS_GFIRSTNAME + ' ' + STUDENT_D.STS_GLASTNAME end) as parent_name " & _
                            " ,isNULL(SEC_EMP_ID,'') as SEC_EMP FROM STUDENT_M  " & _
                            " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID  " & _
                            " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                            " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                            " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                            " INNER JOIN STUDENT_D ON  STUDENT_D.STS_STU_ID= STUDENT_M.STU_ID " & _
                            " INNER JOIN BM.BM_STUDENTSINVOLVED ON BM_STUDENTSINVOLVED.BM_STU_ID=STUDENT_M.STU_ID " & _
                            " LEFT JOIN vw_BSU_TUTOR BSU_TUT ON STU_GRM_ID=BSU_TUT.GRM_ID AND SEC_EMP_ID='" & Session("EmployeeId") & "' AND SEC_BSU_ID='" & Session("sBsuid") & "' " & _
                            " WHERE BM.BM_STUDENTSINVOLVED.BM_ID=" & Hiddenincidentid.Value & " AND STU_BSU_ID='" & Session("sBSUID") & "'"
        End If


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        GrdView.DataSource = ds
        GrdView.DataBind()


        For Each row As GridViewRow In GrdView.Rows
            If sbm = 1 Then
                If DirectCast(row.FindControl("lblSEC_EMP"), Label).Text.Trim() = "0" Then
                    DirectCast(row.FindControl("lnkView"), LinkButton).Enabled = False
                End If
            End If
        Next



    End Sub

    Private Sub GetWitNessDeatils()
        Try
            'AS VIEWID
            Dim blnHeader As Boolean = False

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "SELECT BM_WITNESS_ID,BM_ID,BM_WITNESSID,BM_WITNESS_TYPE,BM_WITNESS_SAID FROM BM.BM_INCIDENTWITNESS WHERE BM_ID=" & Hiddenincidentid.Value & ""

            Dim dsWitnesses As DataSet
            dsWitnesses = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If blnHeader = False Then ' Header Information Included
                Dim Headerrow_1 As New TableRow
                Headerrow_1.Style("font-weight") = "bold"
                Headerrow_1.Style("font-size") = "8pt"
                Headerrow_1.Style("text-align") = "center"
                Headerrow_1.Style("Height") = "5px"
                Headerrow_1.Style("font-family") = "verdana,timesroman,garamond"

                For intCols As Integer = 1 To 4
                    Dim HeaderCell_1 As New TableCell
                    HeaderCell_1.ColumnSpan = "0"
                    HeaderCell_1.Style("Height") = "5px"
                    HeaderCell_1.Wrap = True
                    HeaderCell_1.BorderStyle = BorderStyle.Solid
                    HeaderCell_1.BorderWidth = "1"
                    HeaderCell_1.Style("FONT-SIZE") = "12px"
                    HeaderCell_1.Style("FONT-FAMILY") = "Verdana"
                    HeaderCell_1.Style("Text-Align") = "center"
                    HeaderCell_1.Style("FONT-WEIGHT") = "bold"
                    If intCols = 1 Then
                        HeaderCell_1.Style("Width") = "50px"
                        HeaderCell_1.Text = "SlNo"
                    ElseIf intCols = 2 Then
                        HeaderCell_1.Style("Width") = "170px"
                        HeaderCell_1.Text = "Staff/Student"
                    ElseIf intCols = 3 Then
                        HeaderCell_1.Style("Width") = "170px"
                        HeaderCell_1.Text = "No"
                    ElseIf intCols = 4 Then
                        HeaderCell_1.Text = "Name"
                    End If
                    Headerrow_1.Cells.Add(HeaderCell_1)
                Next
                TabWitness.Rows.Add(Headerrow_1)
            End If 'Ending Header If

            For IntCnt As Integer = 0 To dsWitnesses.Tables(0).Rows.Count - 1
                With dsWitnesses.Tables(0)
                    If .Rows(IntCnt).Item("BM_WITNESS_TYPE") = "STU" Then
                        Dim dssStudent As DataSet
                        dssStudent = GetStudentDetails(.Rows(IntCnt).Item("BM_WITNESSID"))

                        Dim Childrow_1 As New TableRow
                        Childrow_1.Style("font-weight") = "bold"
                        Childrow_1.Style("font-size") = "8pt"
                        Childrow_1.Style("text-align") = "center"
                        Childrow_1.Style("Height") = "5px"
                        Childrow_1.Style("font-family") = "verdana,timesroman,garamond"

                        For intCols As Integer = 1 To 4
                            Dim ChildCell_1 As New TableCell
                            ChildCell_1.ColumnSpan = "0"
                            ChildCell_1.Style("Height") = "5px"
                            ChildCell_1.Wrap = True
                            ChildCell_1.BorderStyle = BorderStyle.Solid
                            ChildCell_1.BorderWidth = "1"
                            ChildCell_1.Style("FONT-SIZE") = "12px"
                            ChildCell_1.Style("FONT-FAMILY") = "Verdana"
                            ChildCell_1.Style("text-align") = "center"
                            'HeaderCell_1.Style("FONT-WEIGHT") = "bold"
                            If intCols = 1 Then
                                ChildCell_1.Style("Width") = "50px"
                                ChildCell_1.Text = IntCnt + 1
                            ElseIf intCols = 2 Then
                                ChildCell_1.Style("Width") = "170px"
                                ChildCell_1.Text = "Student"
                            ElseIf intCols = 3 Then
                                ChildCell_1.Style("Width") = "170px"
                                ChildCell_1.Text = dssStudent.Tables(0).Rows(0).Item("STU_NO")
                            ElseIf intCols = 4 Then
                                ChildCell_1.Text = dssStudent.Tables(0).Rows(0).Item("STUDENTNAME")
                            End If
                            Childrow_1.Cells.Add(ChildCell_1)
                        Next
                        TabWitness.Rows.Add(Childrow_1)
                        'Witnesss Comments Including
                        Dim CommentsRow As New TableRow
                        CommentsRow.Style("font-weight") = "bold"
                        CommentsRow.Style("font-size") = "8pt"
                        CommentsRow.Style("text-align") = "center"
                        CommentsRow.Style("Height") = "5px"
                        CommentsRow.Style("font-family") = "verdana,timesroman,garamond"
                        'Comment Cell
                        Dim CommentCell As New TableCell
                        CommentCell.ColumnSpan = "0"
                        CommentCell.Style("Height") = "5px"
                        CommentCell.ColumnSpan = 4
                        CommentCell.Wrap = True
                        CommentCell.BorderStyle = BorderStyle.Solid
                        CommentCell.BorderWidth = "1"
                        CommentCell.Style("FONT-SIZE") = "12px"
                        CommentCell.Style("FONT-FAMILY") = "Verdana"
                        CommentCell.Style("text-align") = "left"
                        CommentCell.Text = .Rows(IntCnt).Item("BM_WITNESS_SAID")
                        CommentsRow.Cells.Add(CommentCell)
                        TabWitness.Rows.Add(CommentsRow)

                    Else
                        Dim dsStaffDet As DataSet
                        dsStaffDet = GetStaffDetails(.Rows(IntCnt).Item("BM_WITNESSID"))

                        Dim Childrow_1 As New TableRow
                        Childrow_1.Style("font-weight") = "bold"
                        Childrow_1.Style("font-size") = "8pt"
                        Childrow_1.Style("text-align") = "center"
                        Childrow_1.Style("Height") = "5px"
                        Childrow_1.Style("font-family") = "verdana,timesroman,garamond"
                        For intCols As Integer = 1 To 4
                            Dim ChildCell_1 As New TableCell
                            ChildCell_1.ColumnSpan = "0"
                            ChildCell_1.Style("Height") = "5px"
                            ChildCell_1.Wrap = True
                            ChildCell_1.BorderStyle = BorderStyle.Solid
                            ChildCell_1.BorderWidth = "1"
                            ChildCell_1.Style("FONT-SIZE") = "12px"
                            ChildCell_1.Style("FONT-FAMILY") = "Verdana"
                            ChildCell_1.Style("text-align") = "center"
                            'HeaderCell_1.Style("FONT-WEIGHT") = "bold"
                            If intCols = 1 Then
                                ChildCell_1.Style("Width") = "50px"
                                ChildCell_1.Text = IntCnt + 1
                            ElseIf intCols = 2 Then
                                ChildCell_1.Style("Width") = "170px"
                                ChildCell_1.Text = "Staff"
                            ElseIf intCols = 3 Then
                                ChildCell_1.Style("Width") = "170px"
                                ChildCell_1.Text = IIf(IsDBNull(dsStaffDet.Tables(0).Rows(0).Item("EMPNO")), "", dsStaffDet.Tables(0).Rows(0).Item("EMPNO"))
                            ElseIf intCols = 4 Then
                                ChildCell_1.Text = IIf(IsDBNull(dsStaffDet.Tables(0).Rows(0).Item("EMPNAME")), "", dsStaffDet.Tables(0).Rows(0).Item("EMPNAME"))
                            End If
                            Childrow_1.Cells.Add(ChildCell_1)
                        Next
                        TabWitness.Rows.Add(Childrow_1)
                        'Witnesss Comments Including
                        Dim CommentsRow As New TableRow
                        CommentsRow.Style("font-weight") = "bold"
                        CommentsRow.Style("font-size") = "8pt"
                        CommentsRow.Style("text-align") = "center"
                        CommentsRow.Style("Height") = "5px"
                        CommentsRow.Style("font-family") = "verdana,timesroman,garamond"
                        'Comment Cell
                        Dim CommentCell As New TableCell
                        CommentCell.ColumnSpan = "0"
                        CommentCell.Style("Height") = "5px"
                        CommentCell.ColumnSpan = 4
                        CommentCell.Wrap = True
                        CommentCell.BorderStyle = BorderStyle.Solid
                        CommentCell.BorderWidth = "1"
                        CommentCell.Style("FONT-SIZE") = "12px"
                        CommentCell.Style("FONT-FAMILY") = "Verdana"
                        CommentCell.Style("text-align") = "left"
                        CommentCell.Text = .Rows(IntCnt).Item("BM_WITNESS_SAID")
                        CommentsRow.Cells.Add(CommentCell)
                        TabWitness.Rows.Add(CommentsRow)

                    End If

                End With
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Function GetStaffDetails(ByVal StaffId As String) As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strSQL As String

        strSQL = "SELECT EMP_ID, EMPNO ,(EMP_FNAME + ' ' + EMP_LNAME) As EMPNAME " & _
                    " FROM EMPLOYEE_M " & _
                    " WHERE EMP_ID=" & StaffId

        Dim dsStaffDet As DataSet
        dsStaffDet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        Return dsStaffDet
    End Function

    Private Function GetStudentDetails(ByVal StudentId As String) As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strSQL As String
        strSQL = "SELECT STU_ID, STU_NO ,(STUDENT_M.STU_FIRSTNAME + ' ' +STUDENT_M.STU_LASTNAME)As STUDENTNAME," & _
                    " SCT_DESCR, GRM_DISPLAY, STM_DESCR, SHF_DESCR " & _
                    " FROM STUDENT_M " & _
                    " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID " & _
                    " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                    " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                    " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                    " WHERE STU_ID=" & StudentId & ""
        Dim dsStudentDet As DataSet
        dsStudentDet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        Return dsStudentDet
    End Function

    Protected Sub GrdView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrdView.SelectedIndexChanged

    End Sub
End Class
