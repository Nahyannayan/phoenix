<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_IncidentSearch.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_IncidentSearch" %>


<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">



        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left">
                    <br />
      
                    <asp:LinkButton id="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;">Advance Search</asp:LinkButton></td>
            </tr>
        </table>
        <br />
<asp:Panel ID="Panel2" runat="server" >
 <table border="0"  cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="title-bg-lite">
                               Search</td>
                        </tr>
                        <tr>
                            <td align="left">
<table width="100%">
    <tr>
        <td align="left" width="20%">
            <span class="field-label">  Reported From</span></td>
      
        <td align="left" width="30%">
            <asp:TextBox ID="txtReportFrom" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton1"
                runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="javascript:return false;" /></td>
        <td align="left" width="20%">
            <span class="field-label">  Reported To</span></td>
      
        <td align="left" width="30%">
            <asp:TextBox ID="txtReportto" runat="server"></asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton2"
                runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="javascript:return false;" /></td>
    </tr>
    <tr>
        <td >
            <span class="field-label">  Type</span></td>
     
        <td >
         <asp:DropDownList ID="ddType" runat="server">
           <asp:ListItem Value="0">ALL</asp:ListItem>
             <asp:ListItem Value="FA">For Action</asp:ListItem>
             <asp:ListItem Value="FI">For Information</asp:ListItem>
            </asp:DropDownList>
          </td>
        <td >
           <span class="field-label">   Incident Date</span></td>
     
        <td >
            <asp:TextBox ID="txtIncidentDate" runat="server"></asp:TextBox>
            <asp:ImageButton ID="ImageButton3"
                runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="javascript:return false;" /></td>
    </tr>
    <tr>
        <td >
            <span class="field-label">  Student Id</span></td>
      
        <td >
            <asp:TextBox ID="txtStudentId" runat="server"></asp:TextBox></td>
        <td >
            <span class="field-label">  Student No</span></td>
   
        <td >
            <asp:TextBox ID="txtStudentno" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td >
            <span class="field-label">  Other Type</span></td>
      
        <td >
            <asp:DropDownList ID="ddOtherType" runat="server">
            <asp:ListItem Value="-1" Text="ALL"></asp:ListItem>
            <asp:ListItem Value="0" Text="Notes In Student's Planner"></asp:ListItem>
            <asp:ListItem Value="1" Text="Break Detention Given"></asp:ListItem>
            <asp:ListItem Value="2" Text="After School Detention Given"></asp:ListItem>
            <asp:ListItem Value="3" Text="Suspension"></asp:ListItem>
            <asp:ListItem Value="4" Text="Referred To Students Counsellor"></asp:ListItem>
            </asp:DropDownList></td>
        <td >
        </td>
        <td >
        </td>
      
    </tr>
    <tr>
        <td>
           <span class="field-label">   Main Category</span></td>
  
        <td>
            <asp:DropDownList ID="cmbMainCategory" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td>
            <span class="field-label">  Sub Category</span></td>
 
        <td>
            <asp:DropDownList ID="cmbSubCategory" runat="server" >
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="4" align="center">
            <asp:Button ID="btnSearch" CausesValidation="false" runat="server" Text="Search" CssClass="button" />
            <asp:Button ID="btnreset" CausesValidation="false" runat="server" Text="Reset"  CssClass="button" /></td>
    </tr>
</table>

 </td>
                    </tr>
               </table>
</asp:Panel>
  <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1"  TargetControlID="Panel2" CollapsedSize="0" ExpandedSize="400"
    Collapsed="False" ExpandControlID="LinkAdvanceSearch" CollapseControlID="LinkAdvanceSearch"
    AutoCollapse="False" AutoExpand="False" ScrollContents="false"  TextLabelID="LinkAdvanceSearch" CollapsedText="Advance Search" ExpandedText="Hide Search" runat="server">
        </ajaxToolkit:CollapsiblePanelExtender>
        
<ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
    TargetControlID="txtReportFrom">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2"
    TargetControlID="txtReportto">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton3"
    TargetControlID="txtIncidentDate">
</ajaxToolkit:CalendarExtender>
