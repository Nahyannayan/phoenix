<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_CreateNewIncident.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_CreateNewIncident" %>
<%@ Register Src="bm_StudentSearchlist.ascx" TagName="bm_StudentSearchlist" TagPrefix="uc2" %>

<%--<link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
<!-- Bootstrap core CSS-->
<link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

  <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />
<%--<%@ Register Src="bm_StudentSearch.ascx" TagName="bm_StudentSearch" TagPrefix="uc1" %>
--%>
<script type="text/javascript">

    function change_chk(chkThis) {
        var chk_state = !chkThis.checked;
        for (i = 0; i < document.forms[0].elements.length; i++) {
            var currentid = document.forms[0].elements[i].id;
            if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch23") != -1) {
                //if (document.forms[0].elements[i].type=='checkbox' )
                //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                document.forms[0].elements[i].checked = chk_state;
                document.forms[0].elements[i].click();//fire the click event of the child element
            }
        }
    }

    function getstaffID(mode) {
        var sFeatures;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 310px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        url = '../ShowEmpDetail.aspx?id=' + mode;
        if (mode == 'EN') {
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;

           <%-- result = window.showModalDialog(url, "", sFeatures);
            if (result == '' || result == undefined)
            { return false; }
            NameandCode = result.split('___');
            document.getElementById("<%=txtstaff.ClientID %>").value = NameandCode[0];
       document.getElementById("<%=hfstaff_id.ClientID %>").value = NameandCode[1];--%>

        }
    }
    function getEmpID(mode) {
        var sFeatures;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 310px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        url = '../ShowEmpDetail.aspx?id=' + mode + '&type=WitnessType';
        if (mode == 'EN') {
            return ShowWindowWithClose(url, 'search', '55%', '85%')
            return false;

     <%--   result = window.showModalDialog(url, "", sFeatures);
        if (result == '' || result == undefined)
        { return false; }
        NameandCode = result.split('___');
        document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[0];
       document.getElementById("<%=hfEmp_ID.ClientID %>").value = NameandCode[1];--%>
    }
}
    function WitnessType() {
        var radStudent = document.getElementById('<%=radStudent.ClientID%>')

    if (radStudent.checked == true) {
        getSibling('SI'); return true;
    }
    else {
        getEmpID('EN'); return false;
    }
}
function getSibling(mode) {
    var sFeatures;
    sFeatures = "dialogWidth: 565px; ";
    sFeatures += "dialogHeight: 310px; ";
    sFeatures += "help: no; ";
    sFeatures += "resizable: no; ";
    sFeatures += "scroll: yes; ";
    sFeatures += "status: no; ";
    sFeatures += "unadorned: no; ";
    var NameandCode;
    var result;
    var url;
    url = '../Showstudentlist.aspx?id=' + mode;

    if (mode == 'SI') {
        return ShowWindowWithClose(url, 'search', '55%', '85%')
        return false;

               <%--result = window.showModalDialog(url, "", sFeatures);

               if (result == '' || result == undefined) {
                   return false;
               }
               NameandCode = result.split('___');

               document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[0];
     document.getElementById("<%=h_SliblingID.ClientID %>").value = NameandCode[1];--%>

           }

       }

    function setEmpValue(result) {
        //alert(result);
        NameandCode = result.split('___');
        document.getElementById("<%=txtstaff.ClientID %>").value = NameandCode[0];
         document.getElementById("<%=hfstaff_id.ClientID %>").value = NameandCode[1];
             CloseFrame();
             return false;
    }

    
    function setEmpValue2(result) {
        //alert(result);
        NameandCode = result.split('___');
        document.getElementById("<%=txtPar_Sib.ClientID%>").value = NameandCode[0];
         document.getElementById("<%=hfEmp_ID.ClientID %>").value = NameandCode[1];
             CloseFrame();
             return false;
    }

    function setStudValue(result) {
        NameandCode = result.split('___');

        document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[0];
       document.getElementById("<%=h_SliblingID.ClientID %>").value = NameandCode[1];
        CloseFrame();
        return false;
    }



         function CloseFrame() {
             jQuery.fancybox.close();
         }
</script>

<%--<style type="text/css">
    .style1 {
        width: 268435440px;
    }

    .style3 {
        height: 26px;
    }

    .style4 {
        width: 136px;
    }

    .style5 {
        height: 26px;
        width: 136px;
    }

    .style6 {
        font-size: small;
        font-weight: bold;
        height: 25px;
        border-left-color: #A0A0A0;
        border-right-color: #C0C0C0;
        border-top-color: #A0A0A0;
        border-bottom-color: #C0C0C0;
        padding: 1px;
    }

    .style8 {
        font-size: medium;
        height: 25px;
        border-left-color: #A0A0A0;
        border-right-color: #C0C0C0;
        border-top-color: #A0A0A0;
        border-bottom-color: #C0C0C0;
        padding: 1px;
    }

    .style9 {
        height: 25px;
        font-weight: bold;
        border-left-color: #A0A0A0;
        border-right-color: #C0C0C0;
        border-top-color: #A0A0A0;
        border-bottom-color: #C0C0C0;
    }
</style>--%>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg-lite">Event Details</td>
    </tr>
    <tr>
        <td align="left">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="2" width="50%">
                        <asp:RadioButtonList ID="RadioReportType" runat="server"
                            RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Value="FA"><span class="field-label">For Action</span></asp:ListItem>
                            <asp:ListItem Value="FI"><span class="field-label">For Information</span></asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td width="20%"><span class="field-label">Date</span></td>

                    <td width="30%">
                        <asp:Label ID="lbltodaydate" runat="server" CssClass="field-value"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="4"><span class="field-label">Staff Reporting the Event</span></td>
                </tr>
                <tr>
                    <td align="left" width="20%"><span class="field-label">Staff Name</span></td>

                    <td align="left" width="30%">
                        <asp:TextBox ID="txtstaff" runat="server"
                            Enabled="False" ></asp:TextBox>&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="imgbtnstaff" runat="server" ImageUrl="~/Images/forum_search.gif" OnClick="imgbtnstaff_Click"
                    OnClientClick="getstaffID('EN');return false;" />
                        <%--ImageUrl="~/Images/sibling.jpg"--%>
                    </td>
                     <td align="left" ></td>
                    <td align="left" >
                        <asp:Label ID="lblDesignation" runat="server" CssClass="field-value" Text="-"></asp:Label></td>
                </tr>
                <tr>
                    <td align="left"><span class="field-label">Event Main Category</span></td>

                    <td align="left">
                        <asp:DropDownList ID="cmbMainCategory" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left"><span class="field-label">Event Sub Catagory</span></td>

                    <td align="left">
                        <asp:DropDownList ID="cmbSubCategory" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                       <td align="left"></td>
                    <td align="left">
                        <asp:Label ID="lblPoint" runat="server" Text="Total Points :" CssClass="field-value"></asp:Label></td>
                </tr>
                <tr>
                    <td><span class="field-label">Event Date</span></td>

                    <td>
                        <asp:TextBox ID="txtincidentdate" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                            OnClientClick="javascript:return false;" /></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td><span class="field-label">Event Time</span></td>

                    <td >
                        <asp:DropDownList ID="ddlhourpick" runat="server" style="min-width:10% !important; width:25% !important; ">
                        </asp:DropDownList>
                        
                            <asp:DropDownList ID="ddlminutepick" runat="server" style="min-width:10% !important; width:25% !important; ">
                            </asp:DropDownList>
                        
                    </td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" class="title-bg-lite" colspan="4">Students Involved</td>
                </tr>
                <tr>
                    <td align="left" colspan="4">&nbsp;&nbsp;
                 <uc2:bm_StudentSearchlist ID="Bm_StudentSearchlist1" OnbtnHandler="SearchStudents" runat="server" />
                        <asp:GridView ID="GrdView" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                            <Columns>

                                <asp:TemplateField HeaderText="Student Id">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%#Eval("STU_ID")%>' />
                                        <asp:Label ID="lblno" runat="server" Text='<%#Eval("STU_NO")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstufirst" runat="server" Text='<%#Eval("STU_FIRSTNAME")%>'></asp:Label>
                                        <%--<asp:Label ID="lblstulast" runat="server" Text='<%#Eval("STU_LASTNAME")%>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Grade">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstugrade" runat="server" Text='<%#Eval("grm_display")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Section">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstusection" runat="server" Text='<%#Eval("sct_descr")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Parent Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstuparentname" runat="server" Text='<%#Eval("parent_name")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstuparentemail" runat="server" Text='<%#Eval("ParentEmail")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mobile">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstuparentmobile" runat="server" Text='<%#Eval("ParentMobile")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Check">
                                    <HeaderTemplate>
                                        Confidential*<br />

                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkConf" runat="server" Text="" Checked="true" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkdelete" Text="Delete" CommandName="delete" CausesValidation="false" runat="server"></asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="CBE1" ConfirmText="Are you sure?" runat="server" TargetControlID="lnkdelete"></ajaxToolkit:ConfirmButtonExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />

                        </asp:GridView>

                    </td>
                </tr>
                <tr>
                    <td><span class="field-label">Report on the Event</span></td>
                    <td colspan="3">
                        <asp:Label ID="lblValidIncidentDate" runat="server" ForeColor="Red" CssClass="field-label" ></asp:Label></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3">
                        <asp:TextBox ID="txtReportonincident" runat="server" SkinID="MultiText"
                            TextMode="MultiLine" ></asp:TextBox></td>
                </tr>
            </table>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtstaff"
                Display="None" ErrorMessage="Please Select Staff" SetFocusOnError="True"  ValidationGroup="vg1"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtincidentdate"
                Display="None" ErrorMessage="Please Enter Incident Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtReportonincident"
                Display="None" ErrorMessage="Please Enter (Report on the incident)" SetFocusOnError="True" ValidationGroup="vg1"></asp:RequiredFieldValidator>&nbsp;&nbsp;
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                    ControlToValidate="cmbSubCategory" Display="None" ErrorMessage="Please Select Incident Category"
                                    InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" Height="53px" />
            <asp:HiddenField ID="h_SliblingID" runat="server" />


        </td>
    </tr>
    <tr>
        <td align="left" class="title-bg-lite">Witnesses of the Event</td>
    </tr>
    <tr>
        <td align="left">
            <table width="100%">
                <tr>
                    <td align="left" width="20%"><span class="field-label">Staff / Student Name</span></td>
                    <td align="left" width="20%">
                        <asp:RadioButton ID="radStaff" runat="server" GroupName="WitnessType" Text="Staff" CssClass="field-label"
                            AutoPostBack="True" /><asp:RadioButton ID="radStudent" runat="server" GroupName="WitnessType" Text="Student"  CssClass="field-label"
                                AutoPostBack="True" />
                    </td>
                    <td align="left" width="30%" >
                        <asp:TextBox ID="txtPar_Sib" runat="server" Enabled="False"></asp:TextBox><asp:ImageButton ID="imgbtnSibling" runat="server" ImageUrl="~/Images/forum_search.gif"
                            OnClientClick="WitnessType();return false;" />
                    </td>
                      <td align="left" width="30%" >&nbsp;</td>
                </tr>
                <tr>
                    <td><span class="field-label">What the witnesses said</span></td>
                    <td colspan="3">
                        <asp:Label ID="lblCaption" runat="server" CssClass="field-label" ></asp:Label></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3">
                        <asp:TextBox ID="txtwithnesssaid" runat="server" Height="62px"  TextMode="MultiLine"
                            Width="683px"></asp:TextBox>
                        <br />
                        <asp:Button ID="Add" runat="server" Text="Add" CausesValidation="False" CssClass="button" /></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:GridView ID="gvWitness" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField HeaderText="Student Id">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%# Eval("STUD_ID") %>' />
                                        <asp:Label ID="lblno" runat="server" Text='<%# Eval("STUD_ID") %>' Width="81px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server" Text='<%# Eval("STU_TYPE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstufirst" runat="server" Text='<%# Eval("STUD_NAME") %>'></asp:Label>&nbsp;
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Spoken">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSpoken" runat="server" Text='<%# Eval("SPOKEN") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Detete">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="delete"
                                            Text="Delete"></asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="CBE1" ConfirmText="Are you sure?" runat="server" TargetControlID="lnkdelete">
                                        </ajaxToolkit:ConfirmButtonExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle />
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                            <EmptyDataRowStyle />
                            <EditRowStyle />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center">
            <asp:Button ID="cmdSave" runat="server" Text="Save" CssClass="button" ValidationGroup="vg1"/></td>
    </tr>
</table>

<br />
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="followuptable" runat="server">
    <tr>
        <td class="title-bg">Follow Up/ Action</td>
    </tr>
    <tr>
        <td align="left">
            <table>
                <tr>
                    <td><span class="field-label">Student Name</span></td>
                    <td>
                        <asp:DropDownList ID="cmbStudent" runat="server" AutoPostBack="True">
                        </asp:DropDownList></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td><span class="field-label">Parents Called</span></td>

                    <td>
                        <asp:RadioButtonList ID="R1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td><span class="field-label">Date </span></td>
                    <td>
                        <asp:TextBox ID="txtCalledDate" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="imgCalledDate" runat="server" ImageUrl="~/Images/calendar.gif"
                            OnClientClick="javascript:return false;" /></td>
                </tr>
                <tr>
                    <td colspan="4">What was said
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbStudent"
                Display="None" ErrorMessage="Please Select Student Name" InitialValue="-1" SetFocusOnError="True"
                ValidationGroup="action"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="ReqPatentsCalled" runat="server" ControlToValidate="txtparentscalledsaid"
                            Display="None" ErrorMessage="Please Enter , What parent was Said!." InitialValue="-1"
                            SetFocusOnError="True" ValidationGroup="action"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="txtparentscalledsaid" runat="server" Height="62px" SkinID="MultiText" TextMode="MultiLine"
                            Width="682px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span class="field-label">Parents Interviewed</span></td>

                    <td>
                        <asp:RadioButtonList ID="R2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td><span class="field-label">Date</span> </td>
                    <td>
                        <asp:TextBox ID="txtIntvDate" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="imgIntvDate" runat="server" ImageUrl="~/Images/calendar.gif"
                            OnClientClick="javascript:return false;" /></td>
                </tr>
                <tr>
                    <td><span class="field-label">What was said</span>
                    </td>
                    <td>
                        <asp:Label ID="lblValidDAte" runat="server" ForeColor="Red"></asp:Label></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="txtparentsinterviewssaid" runat="server" Height="62px" SkinID="MultiText" TextMode="MultiLine"
                            Width="683px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><span class="field-label">Notes in student's planner</span></td>

                    <td>
                        <asp:RadioButtonList ID="R3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td><span class="field-label">Date</span></td>

                    <td>
                        <asp:TextBox ID="T1" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                            OnClientClick="javascript:return false;" />
                    </td>
                </tr>
                <tr>
                    <td><span class="field-label">Referred to students counsellor</span>
                    </td>

                    <td>
                        <asp:RadioButtonList ID="R7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td><span class="field-label">Date</span></td>

                    <td>
                        <asp:TextBox ID="T5" runat="server"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="CE6" Format="dd/MMM/yyyy" PopupButtonID="ImageButton6" runat="server" TargetControlID="T5">
                        </ajaxToolkit:CalendarExtender>
                        <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/calendar.gif"
                            OnClientClick="javascript:return false;" />
                    </td>
                </tr>
                <tr id="hidingrow1" runat="server">
                    <td><span class="field-label">Break detention given</span></td>

                    <td>
                        <asp:RadioButtonList ID="R4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td><span class="field-label">Date</span></td>

                    <td>
                        <asp:TextBox ID="T2" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif"
                            OnClientClick="javascript:return false;" />
                    </td>
                </tr>
                <tr id="hidingrow2" runat="server">
                    <td><span class="field-label">After school detention given</span></td>

                    <td>
                        <asp:RadioButtonList ID="R5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td><span class="field-label">Date</span></td>

                    <td>
                        <asp:TextBox ID="T3" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/calendar.gif"
                            OnClientClick="javascript:return false;" />
                    </td>
                </tr>
                <tr id="hidingrow3" runat="server">
                    <td><span class="field-label">Suspension</span></td>

                    <td>
                        <asp:RadioButtonList ID="R6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td><span class="field-label">Date</span></td>

                    <td>
                        <asp:TextBox ID="T4" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/calendar.gif"
                            OnClientClick="javascript:return false;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">Follow up by the Higher Authority :
            <asp:CheckBox ID="chkMail" runat="server" Text="Mail Alert" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox
                ID="CheckBox1" runat="server" Text="Mail Alert to the Parent" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:CheckBox ID="chkFwd" runat="server" AutoPostBack="True" />
                        <asp:DropDownList ID="cmbFwdTo" runat="server" Width="250px" AutoPostBack="True">
                        </asp:DropDownList>
                        <span class="field-label">To</span>
                        <asp:DropDownList ID="ddlStaff" runat="server" Width="274px" AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RFV_staff" runat="server" ControlToValidate="ddlStaff" InitialValue="-1" ValidationGroup="action" ErrorMessage="select forward authority"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="txtComments" runat="server" Height="62px" SkinID="MultiText" TextMode="MultiLine"
                            Width="683px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" ValidationGroup="action" />
                        <asp:Label ID="lblEmpName" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
                </tr>
            </table>
            <asp:LinkButton ID="lnkinsertnew" runat="server" CausesValidation="False" Enabled="False">Issue New Event Slip</asp:LinkButton>
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                ShowSummary="False" ValidationGroup="action" />
        </td>
    </tr>
</table>
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:HiddenField ID="HiddenFieldEntryId" runat="server" />
<asp:HiddenField ID="HidBMID" runat="server" />
<ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
    TargetControlID="txtincidentdate">
</ajaxToolkit:CalendarExtender>
<asp:HiddenField ID="hfEmp_ID" runat="server" />
<asp:HiddenField ID="HidCategoryPoints" runat="server" />
<asp:HiddenField ID="hidActionDet" runat="server" />
<asp:HiddenField ID="HidActionId" runat="server" />
<asp:HiddenField ID="hfstaff_id" runat="server" />
<asp:HiddenField ID="hf_incidentdate" runat="server" />
<br />
<ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2"
    TargetControlID="T1">
</ajaxToolkit:CalendarExtender>
<asp:HiddenField ID="HiDCategory" runat="server" />
<br />
<ajaxToolkit:CalendarExtender ID="CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton3"
    TargetControlID="T2">
</ajaxToolkit:CalendarExtender>
<br />
<ajaxToolkit:CalendarExtender ID="CE4" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton4"
    TargetControlID="T3">
</ajaxToolkit:CalendarExtender>
<br />
<ajaxToolkit:CalendarExtender ID="CE5" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton5"
    TargetControlID="T4">
</ajaxToolkit:CalendarExtender>
<br />
<ajaxToolkit:CalendarExtender ID="cALLEDdaTE" Format="dd/MMM/yyyy" PopupButtonID="imgCalledDate" runat="server" TargetControlID="txtCalledDate">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="InterViewDate" Format="dd/MMM/yyyy" PopupButtonID="imgIntvDate" runat="server" TargetControlID="txtIntvDate">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="EntryDate" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1" runat="server" TargetControlID="txtincidentdate">
</ajaxToolkit:CalendarExtender>

 <script type="text/javascript" lang="javascript">
     function ShowWindowWithClose(gotourl, pageTitle, w, h) {
         $.fancybox({
             type: 'iframe',
             //maxWidth: 300,
             href: gotourl,
             //maxHeight: 600,
             fitToView: true,
             padding: 6,
             width: w,
             height: h,
             autoSize: false,
             openEffect: 'none',
             showLoading: true,
             closeClick: true,
             closeEffect: 'fade',
             'closeBtn': true,
             afterLoad: function () {
                 this.title = '';//ShowTitle(pageTitle);
             },
             helpers: {
                 overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                 title: { type: 'inside' }
             },
             onComplete: function () {
                 $("#fancybox-wrap").css({ 'top': '90px' });
             },
             onCleanup: function () {
                 var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                 if (hfPostBack == "Y")
                     window.location.reload(true);
             }
         });

         return false;
     }
    </script>
