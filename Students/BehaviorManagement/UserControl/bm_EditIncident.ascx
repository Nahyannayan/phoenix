<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_EditIncident.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_EditIncident" %>
<%@ Register Src="bm_StudentSearch.ascx" TagName="bm_StudentSearch" TagPrefix="uc1" %>


<!-- Bootstrap core CSS-->
<link href="/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

  <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />




<%-- <style type="text/css">

         .style8
         {
             font-size: medium;
             height: 25px;
             border-left-color: #A0A0A0;
             border-right-color: #C0C0C0;
             border-top-color: #A0A0A0;
             border-bottom-color: #C0C0C0;
             padding: 1px;
         }
         .style9
         {
             height: 25px;
             font-weight: bold;
             border-left-color: #A0A0A0;
             border-right-color: #C0C0C0;
             border-top-color: #A0A0A0;
             border-bottom-color: #C0C0C0;
         }
         .style6
         {
             font-size: small;
             font-weight: bold;
             height: 25px;
             border-left-color: #A0A0A0;
             border-right-color: #C0C0C0;
             border-top-color: #A0A0A0;
             border-bottom-color: #C0C0C0;
             padding: 1px;
         }
         </style>--%>



 <%--<script type="text/javascript">

 function change_chk_statem(chkThis)
             {
            var chk_state= ! chkThis.checked ;
             for(i=0; i<document.forms[0].elements.length; i++)
                   {
                   var currentid =document.forms[0].elements[i].id; 
                   if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("ch1")!=-1)
                 {
                   //if (document.forms[0].elements[i].type=='checkbox' )
                      //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                        document.forms[0].elements[i].checked=chk_state;
                         document.forms[0].elements[i].click();//fire the click event of the child element
                     }
                  }
              }

</script>
--%>




     <table border="0" cellpadding="0" cellspacing="0" Width="100%" >
                        <tr>
                            <td class="title-bg-lite">
                                Incident Details</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td colspan="2" width="50%">
                                            <asp:RadioButtonList ID="RadioReportType" runat="server" RepeatDirection="Horizontal" CssClass="field-label">
                                                <asp:ListItem Selected="True" Value="FA" > <span class="field-label">For Action</span></asp:ListItem>
                                                <asp:ListItem Value="FI"><span class="field-label">For Information</span></asp:ListItem>
                                            </asp:RadioButtonList></td>
                                        <td width="20%">
                                           <span class="field-label"> Date</span></td>
                                       
                                        <td width="30%">
                                            <asp:Label ID="lbltodaydate" runat="server" CssClass="field-value"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%">
            <span class="field-label">  Staff Reporting the Incident</span></td>
                                    
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="ddstaff" runat="server" AutoPostBack="True" Width="308px">
                                            </asp:DropDownList></td>
                                        <td align="left"  ></td>
                                            <td align="left"  >  <asp:Label ID="lblDesignation" runat="server" CssClass="field-value"  Text="-"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="20%">
                                           <span class="field-label">   Main Category</span></td>
                               
                                        <td align="left" width="30%">
                                            <asp:DropDownList ID="cmbMainCategory" runat="server" AutoPostBack="True" >
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" width="50%" colspan="2" >
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="left" >
                                            <span class="field-label">  Sub Category</span></td>
                                     
                                        <td align="left" >
                                            <asp:DropDownList ID="cmbSubCategory" runat="server" AutoPostBack="True" >
                                            </asp:DropDownList>
                                        </td>
                                          <td align="left"  ></td>
                                        <td align="left" >
                                            <asp:Label ID="lblPoint" runat="server" Text="Total Points :" CssClass="field-value" ></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <span class="field-label">  Incident Date</span></td>
                                       
                                        <td>
                                            <asp:TextBox ID="txtincidentdate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="javascript:return false;" /></td>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <span class="field-label">  Incident Time</span></td>
                                   
                                        <td >
           <%-- <MKB:TimeSelector ID="TimeSelector1" runat="server" AllowSecondEditing="True">
            </MKB:TimeSelector>--%>
                                        <asp:DropDownList ID="ddlhourpick" runat="server" style="min-width:10% !important; width:25% !important; " >
            </asp:DropDownList>  
          <%--  <span class="style8" style="z-index: 1">&nbsp; <sup><span class="style9">:</span></sup>&nbsp;&nbsp; </span><span 
                class="style6" style="z-index: 1">--%>
                                             
          <asp:DropDownList ID="ddlminutepick" runat="server"  style="min-width:10% !important; width:25% !important; ">
            </asp:DropDownList>
       
                                        </td>
                                         <td >
                                            &nbsp;</td>
                                        <td >
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td >
             <span class="field-label"> Report on the incident</span></td>
                                         <td colspan="3">
                                            <asp:TextBox ID="txtReportonincident" runat="server" Height="62px" 
                                                TextMode="MultiLine" Width="683px"></asp:TextBox></td>
                                    </tr>
                                 <%--   <tr>
                                       
                                    </tr>--%>
                                </table>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddstaff"
                                    Display="None" ErrorMessage="Please Select Staff" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtincidentdate"
                                    Display="None" ErrorMessage="Please Enter Incident Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtReportonincident"
                                    Display="None" ErrorMessage="Please Enter (Report on the incident)" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:HiddenField ID="h_SliblingID" runat="server" />
                                &nbsp;&nbsp;
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                    ShowSummary="False" />


 </td>
                    </tr>
         <tr>
             <td align="left" class="title-bg-lite">
                 Witnesses of the Incident</td>
         </tr>
         <tr>
             <td align="left">
                 <table width="100%">
                    <%-- <tr>
                         <td >
                         </td>
                         <td >
                         </td>
                        
                         <td>
                         </td>
                     </tr>--%>
                     <tr>
                          <td align="left" width="20%">
                             <asp:RadioButton ID="radStaff" runat="server" AutoPostBack="True" GroupName="WitnessType" CssClass="field-label"
                                 Text="Staff"  />
                             <asp:RadioButton ID="radStudent" runat="server" AutoPostBack="True" GroupName="WitnessType" CssClass="field-label"
                                 Text="Student"  /></td>
                         <td align="left" width="20%">
                            <span class="field-label"> Staff / Student Name</span></td>
                     
                         <td align="left" width="30%">
                            <asp:TextBox ID="txtPar_Sib" runat="server" ></asp:TextBox>
                             <asp:ImageButton ID="imgbtnSibling" runat="server"  ImageUrl="~/Images/forum_search.gif"
                                 OnClientClick="getSibling('SI');return false;"  />
                             <asp:Button ID="btnEmp_Name" runat="server" CssClass="button" OnClientClick="getEmpID('EN');return false;"
                                 Text="..." /></td>
                            <td align="left" width="30%">&nbsp;</td>
                     </tr>
                         <tr>
                          <td colspan="4">    
                             <asp:Button ID="Add" runat="server" CausesValidation="False" Text="Add" CssClass="button"  /></td>
                     </tr>
                     <tr>
                         <td >
                           <span class="field-label">  What the witnesses said</span></td>
                          <td colspan="3">
                             <asp:TextBox ID="txtwithnesssaid" runat="server" Height="62px" 
                                 TextMode="MultiLine" Width="684px"></asp:TextBox></td>
                     </tr>
                   
                     <tr>
                         <td colspan="4">
                             <asp:GridView ID="gvWitness" runat="server" AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-row">
                                 <Columns>
                                     <asp:TemplateField HeaderText="Student Id" Visible="False">
                                         <ItemTemplate>
                                             <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%# Eval("STUD_ID") %>' />
                                             <asp:Label ID="lblno" runat="server" Text='<%# Eval("STUD_ID") %>' ></asp:Label>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Type">
                                         <ItemTemplate>
                                             <asp:Label ID="lblType" runat="server" Text='<%# Eval("STU_TYPE") %>'></asp:Label>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Name">
                                         <ItemTemplate>
                                             <asp:Label ID="lblstufirst" runat="server" Text='<%# Eval("STUD_NAME") %>'></asp:Label>&nbsp;
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Spoken">
                                         <ItemTemplate>
                                             <asp:Label ID="lblSpoken" runat="server" Text='<%# Eval("SPOKEN") %>'></asp:Label>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Delete">
                                         <ItemTemplate>
                                             <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="delete"
                                                 Text="Delete" Enabled="False"></asp:LinkButton>
                                             <ajaxToolkit:ConfirmButtonExtender ID="CBE1" runat="server" ConfirmText="Are you sure?"
                                                 TargetControlID="lnkdelete">
                                             </ajaxToolkit:ConfirmButtonExtender>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Edit">
                                         <ItemTemplate>
                                             <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("STUD_ID") %>'
                                                 CommandName="Edit" Text="Edit"></asp:LinkButton><ajaxToolkit:ConfirmButtonExtender ID="CBE2" runat="server" ConfirmText="Are you sure?"
                                                 TargetControlID="lnkdelete">
                                                 </ajaxToolkit:ConfirmButtonExtender>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                 </Columns>
                                 <HeaderStyle />
                                 <RowStyle CssClass="griditem"  />
                                 <SelectedRowStyle  />
                                 <AlternatingRowStyle CssClass="griditem_alternative"  />
                                 <EmptyDataRowStyle  />
                                 <EditRowStyle  />
                             </asp:GridView>
                         </td>
                     </tr>
                 </table>
             </td>
         </tr>
               </table>
&nbsp;
<table border="0"  cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td class="title-bg">
            Students Involved</td>
    </tr>
    <tr>
        <td align="left">
<uc1:bm_StudentSearch ID="Bm_StudentSearch1" OnbtnHandler="SearchStudents"  NONEWLINKBUTTON="true" runat="server" />
<asp:GridView id="GrdView" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
        <Columns>
            <asp:TemplateField HeaderText="Student Id">
                <ItemTemplate>
                    <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%#Eval("STU_ID")%>'  />
                    <asp:Label ID="lblno" runat="server" Text='<%#Eval("STU_NO")%>'></asp:Label>
                  
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                 <asp:Label ID="lblstufirst" runat="server" Text='<%#Eval("STU_FIRSTNAME")%>'></asp:Label>
                 <asp:Label ID="lblstulast" runat="server" Text='<%#Eval("STU_LASTNAME")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Grade">
            <ItemTemplate>
                     <asp:Label ID="lblstugrade" runat="server" Text='<%#Eval("grm_display")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Section">
            <ItemTemplate>
            <asp:Label ID="lblstusection" runat="server" Text='<%#Eval("sct_descr")%>'></asp:Label>
                   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Parent Name">
                <ItemTemplate>
                <asp:Label ID="lblstuparentname" runat="server" Text='<%#Eval("parent_name")%>'></asp:Label>
                   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <ItemTemplate>
                 <asp:Label ID="lblstuparentemail" runat="server" Text='<%#Eval("ParentEmail")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile">
                <ItemTemplate>
                <asp:Label ID="lblstuparentmobile" runat="server" Text='<%#Eval("ParentMobile")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
             <asp:TemplateField HeaderText="Mobile" Visible="false">
                <ItemTemplate>
                <asp:Label ID="lblSEC_EMP" runat="server" Text='<%#Eval("SEC_EMP")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            
            
              <asp:TemplateField HeaderText="Confdl" Visible="true">
                <ItemTemplate>
                <asp:Label ID="lblConfidential" runat="server" Text='<%#Eval("Confide")%>'></asp:Label>
                    
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            
            
            <asp:TemplateField HeaderText="Delete" >
            <ItemTemplate>
                <asp:LinkButton ID="lnkdelete" Text="Delete" CommandName="delete" CommandArgument='<%#Eval("STU_ID")%>' CausesValidation="false" runat="server" Enabled="False"></asp:LinkButton>
            <ajaxToolkit:ConfirmButtonExtender ID="CBE1" ConfirmText="Permanently delete this record?" runat="server" TargetControlID="lnkdelete"></ajaxToolkit:ConfirmButtonExtender>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Action" >
            <ItemTemplate>
                <asp:LinkButton ID="lnkAction" Text="Action" CommandName="Action" CommandArgument='<%#Eval("STU_ID")%>' CausesValidation="false" runat="server"></asp:LinkButton>
            </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
        <HeaderStyle   />
        <RowStyle CssClass="griditem"   />
        <SelectedRowStyle   />
        <AlternatingRowStyle CssClass="griditem_alternative"   />
        <EmptyDataRowStyle  />
        <EditRowStyle   />
    </asp:GridView>
        </td>
    </tr>
    
                                    
     <tr>
                                        <td align="center">
                                            <asp:Button ID="btnUPDATE" runat="server" CssClass="button" Text="Update" /></td>
                                    </tr>
                                    
</table>
<asp:Panel ID="Panel1" runat="server" Visible="False" CssClass="panel-cover">
 <table border="0"  cellpadding="5" cellspacing="0" width="100%" >
                        <tr>
                            <td class="title-bg">
                                Action Taken</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td>
                                           <span class="field-label"> Student Name</span></td>
                                      
                                        <td >
                                            <asp:DropDownList ID="cmbStudent" runat="server" AutoPostBack="True" >
                                            </asp:DropDownList></td>
                                          <td>
                                        </td>
                                          <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           <span class="field-label"> Parents Called</span></td>
                                   
                                        <td>
                                            <asp:RadioButtonList ID="R1" runat="server" AutoPostBack="True" 
                                                RepeatDirection="Horizontal" >
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                            </td>
                                        <td>
                                          <span class="field-label">  Date</span>
                                        </td>
                                       
                                        <td>
                                            <asp:TextBox ID="txtCalledDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgCalledDate" runat="server" 
                                                ImageUrl="~/Images/calendar.gif" OnClientClick="javascript:return false;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                         <span class="field-label">   What was said</span></td>
                                                  <td colspan="4">
                                            <asp:TextBox ID="txtparentscalledsaid" runat="server" Height="62px" 
                                                TextMode="MultiLine" Width="682px"></asp:TextBox></td>
                                                
                                    </tr>
                                    <tr>
                                        <td>
          <span class="field-label">   Parents Interviewed</span></td>
                                       
                                        <td valign="top">
                                            <asp:RadioButtonList ID="R2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                        <td>
                                          <span class="field-label">   Date </span></td>
                                
                                        <td>
                                            <asp:TextBox ID="txtIntvDate" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="imgIntvDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="javascript:return false;" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="height: 21px">
                                            What was said</td>
                                            <td colspan="4"><asp:Label ID="lblValidDAte" runat="server" ForeColor="Red" Width="123px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:TextBox ID="txtparentsinterviewssaid" runat="server" Height="62px" 
                                                TextMode="MultiLine" Width="683px"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
          <span class="field-label">   Notes in student's planner</span></td>
                                    
                                        <td>
                                            <asp:RadioButtonList ID="R3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                        <td>
                                         <span class="field-label">    Date</span></td>
                                       
                                        <td>
                                            <asp:TextBox ID="T1" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="javascript:return false;" style="height: 16px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
          <span class="field-label">   Break detention given</span></td>
                             
                                        <td>
                                            <asp:RadioButtonList ID="R4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                        <td>
                                           <span class="field-label">  Date</span></td>
                                      
                                        <td>
                                            <asp:TextBox ID="T2" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="javascript:return false;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
           <span class="field-label">  After school detention given</span></td>
                                      
                                        <td>
                                            <asp:RadioButtonList ID="R5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                        <td>
                                          <span class="field-label">   Date</span></td>
                                       
                                        <td>
                                            <asp:TextBox ID="T3" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="javascript:return false;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
          <span class="field-label">   Suspension</span></td>
                                     
                                        <td>
                                            <asp:RadioButtonList ID="R6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                        <td>
                                          <span class="field-label">   Date</span></td>
                                 
                                        <td>
                                            <asp:TextBox ID="T4" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="javascript:return false;" Height="16px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
          <span class="field-label">   Referred to students counsellor </span>
                                        </td>
                                     
                                        <td>
                                            <asp:RadioButtonList ID="R7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="No">No</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                        <td>
                                           <span class="field-label">  Date</span></td>
                                  
                                        <td>
                                            <asp:TextBox ID="T5" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/calendar.gif"
                                                OnClientClick="javascript:return false;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Update"  /></td>
                                    </tr>
                                    <tr>
                                        <td  >
           <span class="field-label">  Any Other Details / Follow up </span><asp:CheckBox ID="C1" runat="server" AutoPostBack="True" /></td>
                                         <td colspan="3" >
                                             </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <span class="field-label">  Forward To </span> </td>
                                        <td >
                                            <asp:DropDownList ID="cmbForwrdFrom" runat="server" AutoPostBack="True" Width="223px">
                                            </asp:DropDownList></td>
                                         <td >
                                            <span class="field-label">   Select Staff</span> </td>
                                        <td >
                                            <asp:DropDownList ID="CmbFwdTo" runat="server" AutoPostBack="True" Width="210px">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:TextBox ID="txtnotes" runat="server" Height="62px"  TextMode="MultiLine"
                                                Width="683px"></asp:TextBox></td>
                                    </tr>
                                    
                                     <tr>
                                        <td align="center" colspan="4">
                                            <asp:Button ID="btnSaveAction" runat="server" CssClass="button" Text="Save Action"/></td>
                                    </tr>
                                    
                                    
                                                                       <tr>
                                        <td align="center" colspan="4">
                                            <asp:GridView id="grdFollowup" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-row">
                                                <EmptyDataRowStyle   />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ActionId" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%# Eval("BM_ACTION_ID") %>'  />
                                                            <asp:Label ID="lblno" runat="server" Text='<%# Eval("BM_ACTION_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblActionDate" runat="server" Text='<%# Eval("BM_ACTIONDATE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comments">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblComments" runat="server" Text='<%# Eval("BM_COMMENTS") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action By" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ActionBy" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Designation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDesig" runat="server" Text='<%# Eval("DES_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Fwded To">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFwdBy" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="griditem"  />
                                                <EditRowStyle  />
                                                <SelectedRowStyle   />
                                                <HeaderStyle   />
                                                <AlternatingRowStyle CssClass="griditem_alternative"   />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                   
                                    <tr>
                                        <td align="center" colspan="4" >
                                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
                                    </tr>
                                </table>
                                <asp:LinkButton ID="lnkinsertnew" runat="server" CausesValidation="False">Issue New Incident Slip</asp:LinkButton></td>
                    </tr>
               </table>
</asp:Panel>
<br />
<asp:HiddenField ID="Hiddenbsuid" runat="server" />
<asp:HiddenField ID="HiddenIncidentId" runat="server" />
<asp:HiddenField ID="HidCategoryPoints" runat="server" />
<asp:HiddenField ID="HiddenStudentId" runat="server" />
<asp:HiddenField ID="hidActionId" runat="server" />
<asp:HiddenField ID="hid_witnessId" runat="server" />
<asp:HiddenField ID="hfEmp_ID" runat="server" />


<ajaxToolkit:CalendarExtender ID="CE7" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalledDate"
    TargetControlID="txtCalledDate">
    </ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CE8" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgIntvDate"
    TargetControlID="txtIntvDate">
    </ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
    TargetControlID="txtincidentdate">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CE2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton2"
    TargetControlID="T1">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CE3" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton3"
    TargetControlID="T2">
</ajaxToolkit:CalendarExtender>
<asp:HiddenField ID="hidActionFwd" runat="server" />
<ajaxToolkit:CalendarExtender ID="CE4" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton4"
    TargetControlID="T3">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CE5" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton5"
    TargetControlID="T4">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CE6" Format="dd/MMM/yyyy"  PopupButtonID="ImageButton6" runat="server" TargetControlID="T5" >
</ajaxToolkit:CalendarExtender>
