<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Students_BehaviorManagement_UserControl_Default" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="../../../chromejs/chrome.js"></script>
       <script  language="javascript" type="text/javascript">

 function change_chk_stateg(chkThis)
             {
            var chk_state= ! chkThis.checked ;
             for(i=0; i<document.forms[0].elements.length; i++)
                   {
                   var currentid =document.forms[0].elements[i].id; 
                   if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("ch2")!=-1)
                 {
                   //if (document.forms[0].elements[i].type=='checkbox' )
                      //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                        document.forms[0].elements[i].checked=chk_state;
                         document.forms[0].elements[i].click();//fire the click event of the child element
                     }
                  }
              }

   function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../../../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../../../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../../../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../../../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../../../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../../../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_Selected_menu_1.ClientID %>").value=val+'__'+path;
                }
                
      function test2(val)
                {
                var path;
                if (val=='LI')
                {
                path='../../../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../../../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../../../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {    
                path='../../../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../../../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../../../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_Selected_menu_2.ClientID %>").value=val+'__'+path;
                }
               
       </script>
</head>
<body>
    <form id="form1" runat="server">
 
<div class="matters">

      <div id="dropmenu2" class="dropmenudiv" style="left: 434px; width: 110px; top: 15px">
                <a href="javascript:test2('LI');">
                    <img alt="Any where" class="img_left" src="../../../Images/operations/like.gif" style="text-decoration: underline" />
                    Any Where</a> <a href="javascript:test2('NLI');">
                        <img alt="Not In" class="img_left" src="../../../Images/operations/notlike.gif" style="text-decoration: underline" />
                        Not In</a> <a href="javascript:test2('SW');">
                            <img alt="Starts With" class="img_left" src="../../../Images/operations/startswith.gif"
                                style="text-decoration: underline" />
                            Starts With</a> <a href="javascript:test2('NSW');">
                                <img alt="Like" class="img_left" src="../../../Images/operations/notstartwith.gif" style="text-decoration: underline" />
                                Not Start With</a> <a href="javascript:test2('EW');">
                                    <img alt="Like" class="img_left" src="../../../Images/operations/endswith.gif" style="text-decoration: underline" />
                                    Ends With</a> <a href="javascript:test2('NEW');">
                                        <img alt="Like" class="img_left" src="../../../Images/operations/notendswith.gif" style="text-decoration: underline" />
                                        Not Ends With</a>
            </div>
            
             
            <div id="dropmenu1" class="dropmenudiv" style="left: 435px; width: 110px; top: 26px">
                <a href="javascript:test1('LI');">
                    <img alt="Any where" class="img_left" src="../../../Images/operations/like.gif" style="text-decoration: underline" />
                    Any Where</a> <a href="javascript:test1('NLI');">
                        <img alt="Not In" class="img_left" src="../../../Images/operations/notlike.gif" style="text-decoration: underline" />
                        Not In</a> <a href="javascript:test1('SW');">
                            <img alt="Starts With" class="img_left" src="../../../Images/operations/startswith.gif"
                                style="text-decoration: underline" />
                            Starts With</a> <a href="javascript:test1('NSW');">
                                <img alt="Like" class="img_left" src="../../../Images/operations/notstartwith.gif" style="text-decoration: underline" />
                                Not Start With</a> <a href="javascript:test1('EW');">
                                    <img alt="Like" class="img_left" src="../../../Images/operations/endswith.gif" style="text-decoration: underline" />
                                    Ends With</a> <a href="javascript:test1('NEW');">
                                        <img alt="Like" class="img_left" src="../../../Images/operations/notendswith.gif" style="text-decoration: underline" />
                                        Not Ends With</a>
            </div>
            
    <asp:LinkButton ID="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;">Add Students</asp:LinkButton>

    <asp:Panel ID="Panel1" runat="server" >
 
<table>
            <%--<tr>
                <td >
                    Student Number</td>
                <td >
                    :</td>
                <td colspan="8">
                    <asp:TextBox ID="txtstudentsearchnumber" runat="server"></asp:TextBox></td>
            </tr>
    <tr>
        <td >
            Student Name</td>
        <td >
        </td>
        <td colspan="8">
            <asp:TextBox ID="txtstudentsearchname" runat="server"></asp:TextBox></td>
    </tr>--%>
    <tr>
        <td style="width: 69px">
                    Curriculum</td>
        <td style="width: 6px">
            :</td>
        <td>
                    <asp:DropDownList ID="ddclm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddclm_SelectedIndexChanged">
                    </asp:DropDownList></td>
        <td style="width: 46px">
                    Shift</td>
        <td style="width: 6px">
            :</td>
        <td>
                    <asp:DropDownList ID="ddshift" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
        <td>
                    Stream</td>
        <td style="width: 6px">
            :</td>
        <td>
                    <asp:DropDownList ID="ddstream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddstream_SelectedIndexChanged">
                    </asp:DropDownList></td>
    </tr>
            <tr>
                <td style="width: 69px" >
                    Grade</td>
                <td style="width: 6px" >
                    :</td>
                <td >
                    <asp:DropDownList ID="ddgrade" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td style="width: 46px" >
                    Section</td>
                <td style="width: 6px" >
                    :</td>
                <td >
                    <asp:DropDownList ID="ddsection" runat="server">
                    </asp:DropDownList></td>
                <td >
                    &nbsp;</td>
                <td style="width: 6px">
                    </td>
                <td >
                    </td>
            </tr>
    <tr>
        <td colspan="9" align="center">
            <asp:Button ID="btnsearch" CssClass="button" runat="server" Text="Search" CausesValidation="False" /></td>
    </tr>
</table>

    <asp:GridView ID="GrdView" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="680px" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords.">
        <Columns>
            <asp:TemplateField HeaderText="Check">
                <HeaderTemplate>
                    All<br />
                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);" ToolTip="Click here to select/deselect all rows" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="ch2" runat="server" Text="" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Student Id">
                <ItemTemplate>
                    <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%#Eval("STU_ID")%>' />
                    <asp:Label ID="lblno" runat="server" Text='<%#Eval("STU_NO")%>'></asp:Label>
                  
                </ItemTemplate>
                <HeaderTemplate>
                    <table style="width: 100%; height: 62%">
                        <tr>
                            <td align="center" style="width: 225px">
                                <asp:Label ID="lblFeeIDH" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                    Text="STUDENT ID"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 225px; height: 72px">
                                <table border="0" cellpadding="0" cellspacing="0" style="height: 58px" width="100%">
                                    <tr>
                                        <td style="width: 100px; height: 12px">
                                            <div id="Div1" class="chromestyle">
                                                <ul>
                                                    <li><a href="#" rel="dropmenu1">
                                                        <img id="mnu_1_img" runat="server" align="middle" alt="Menu" border="0" src="../../../Images/operations/like.gif" /><span
                                                            style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td style="width: 100px; height: 12px">
                                            <asp:TextBox ID="txtFeeId" runat="server" Width="82px"></asp:TextBox></td>
                                        <td style="width: 100px; height: 12px" valign="middle">
                                            <asp:ImageButton ID="btnSearchFeeID" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchFeeID_Click" />&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                 <asp:Label ID="lblstufirst" runat="server" Text='<%#Eval("STU_FIRSTNAME")%>'></asp:Label>
                 <asp:Label ID="lblstulast" runat="server" Text='<%#Eval("STU_LASTNAME")%>'></asp:Label>
                    
                </ItemTemplate>
                <HeaderTemplate>
                    <table style="width: 100%; height: 78%">
                        <tr>
                            <td align="center" style="width: 100px; height: 14px">
                                <asp:Label ID="lblStudNameH" runat="server" CssClass="gridheader_text" Text="Student Name"
                                    Width="214px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 100px; height: 65px">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="width: 100px; height: 12px">
                                            <div id="Div2" class="chromestyle">
                                                <ul>
                                                    <li><a href="#" rel="dropmenu2">
                                                        <img id="mnu_2_img" runat="server" align="middle" alt="Menu" border="0" src="../../../Images/operations/like.gif" /><span
                                                            style="font-size: 12pt; color: #0000ff; text-decoration: none">&nbsp;</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td style="width: 100px; height: 12px">
                                            <asp:TextBox ID="txtStudName" runat="server" Width="150px"></asp:TextBox></td>
                                        <td style="width: 100px; height: 12px" valign="middle">
                                            <asp:ImageButton ID="btnSearchStudName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchStudName_Click" CausesValidation="False" />&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Grade">
            <ItemTemplate>
                     <asp:Label ID="lblstugrade" runat="server" Text='<%#Eval("grm_display")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Section">
            <ItemTemplate>
            <asp:Label ID="lblstusection" runat="server" Text='<%#Eval("sct_descr")%>'></asp:Label>
                   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Parent Name">
                <ItemTemplate>
                <asp:Label ID="lblstuparentname" runat="server" Text='<%#Eval("parent_name")%>'></asp:Label>
                   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <ItemTemplate>
                 <asp:Label ID="lblstuparentemail" runat="server" Text='<%#Eval("ParentEmail")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile">
                <ItemTemplate>
                <asp:Label ID="lblstuparentmobile" runat="server" Text='<%#Eval("ParentMobile")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
        <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
        <SelectedRowStyle CssClass="Green" Wrap="False" />
        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
        <EmptyDataRowStyle Wrap="False" />
        <EditRowStyle Wrap="False" />
    </asp:GridView>
            <asp:Button ID="btnaddstu" runat="server" Width="80px" Text="Add" CssClass="button" CausesValidation="False" /></asp:Panel>
    <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
<asp:HiddenField ID="Hiddenacyid" runat="server" />
<asp:HiddenField ID="Hiddenacdid" runat="server" />
<asp:HiddenField ID="Hiddenbsu" runat="server" />
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdvanceSearch"
        Collapsed="true"  CollapsedSize="0" CollapsedText="Add Students" ExpandControlID="LinkAdvanceSearch"
         ExpandedText="Hide Search" ScrollContents="false" TargetControlID="Panel1"
        TextLabelID="LinkAdvanceSearch">
    </ajaxToolkit:CollapsiblePanelExtender>


<input id="h_Selected_menu_1" runat="server" type="hidden"  value="=" />
<input id="h_Selected_menu_2" runat="server" type="hidden"  value="=" />
<input id="h_Selected_menu_3" runat="server" type="hidden"  value="=" />
<input id="h_Selected_menu_4" runat="server" type="hidden"  value="=" />
<input id="h_Selected_menu_5" runat="server" type="hidden"  value="=" />
<script type="text/javascript">

cssdropdown.startchrome("Div1")
cssdropdown.startchrome("Div2")
</script>
</div>
    </form>
</body>
</html>
