Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.String
Partial Class Students_BehaviorManagement_UserControl_bm_EditIncident
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Session("ds1") = Nothing
                Hiddenbsuid.Value = Session("sbsuid")
                HiddenIncidentId.Value = Request.QueryString("inc_id")
                lbltodaydate.Text = Today.Date.ToString("dd/MMM/yyyy")
                BidStaff()
                BindMainCategory()
                fillpick()
                'BindCategory()
                IncidentDetails()
                'ActionTakendetails()
                BindGrid()
                CreateWitNessWiewTable()
                BindWitnessDetails()
                HiddenStudentId.Value = Nothing
                CmbFwdTo.Enabled = False
                Add.Text = "Add"
                btnSaveAction.Enabled = False
                ddstaff.Enabled = False
                DisableFWDSEction()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub fillpick()
        ddlhourpick.Items.Add("-")
        Dim i As Integer

        For i = 0 To 23
            If i >= 0 And i <= 9 Then
                ddlhourpick.Items.Add("0" + i.ToString)
            Else
                ddlhourpick.Items.Add(i.ToString)
            End If
        Next
        ddlminutepick.Items.Add("-")
        Dim j As Integer

        For j = 0 To 59
            If j >= 0 And j <= 9 Then
                ddlminutepick.Items.Add("0" + j.ToString)
            Else
                ddlminutepick.Items.Add(j.ToString)
            End If
        Next
    End Sub
    'Public Sub IncidentDetails()

    '    Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '    Dim str_query = " SELECT *, (ISNULL(EMP_FNAME,'') +' ' + ISNULL(EMP_LNAME,'')) AS STAFF from BM.BM_MASTER A  " & _
    '                    " INNER JOIN EMPLOYEE_M  B ON  A.BM_REPORTING_STAFF_ID = B.EMP_ID " & _
    '                    " WHERE A.BM_ID='" & HiddenIncidentId.Value & "'"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    '    If ds.Tables(0).Rows.Count >= 1 Then
    '        RadioReportType.SelectedValue = ds.Tables(0).Rows(0).Item("BM_INCIDENT_TYPE").ToString()
    '        ddstaff.SelectedValue = ds.Tables(0).Rows(0).Item("BM_REPORTING_STAFF_ID").ToString()
    '        txtincidentdate.Text = Format(ds.Tables(0).Rows(0).Item("BM_INCIDENT_DATE"), "dd-MMM-yyyy")
    '        txtReportonincident.Text = ds.Tables(0).Rows(0).Item("BM_REPORT_ON_INCIDENT").ToString()
    '    End If

    'End Sub

    Public Sub IncidentDetails()
        Dim ampm As String
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        'Dim str_query = " SELECT *, BM_CATEGORYID, (ISNULL(EMP_FNAME,'') +' ' + ISNULL(EMP_LNAME,'')) AS STAFF from BM.BM_MASTER A  " & _
        '                " INNER JOIN EMPLOYEE_M  B ON  A.BM_REPORTING_STAFF_ID = B.EMP_ID, BM.BM_ACTION_MASTER " & _
        '                " WHERE A.BM_ID='" & HiddenIncidentId.Value & "'"

        Dim str_query = " SELECT DISTINCT A.BM_ID, *, (ISNULL(EMP_FNAME,'') +' ' + ISNULL(EMP_LNAME,'')) AS STAFF from BM.BM_MASTER A  " & _
                " INNER JOIN EMPLOYEE_M  B ON  A.BM_REPORTING_STAFF_ID = B.EMP_ID LEFT JOIN BM.BM_ACTION_MASTER C ON C.BM_ID=A.BM_ID " & _
                " WHERE A.BM_ID='" & HiddenIncidentId.Value & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'Dim str_query1 = " SELECT * from BM.BM_ACTION_MASTER WHERE BM_ID='" & HiddenIncidentId.Value & "'"
        'Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query1)

        Dim str_query2 = " SELECT * FROM BM.BM_CATEGORY WHERE BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYID=" & ds.Tables(0).Rows(0).Item("BM_CATEGORYID").ToString
        Dim ds2 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query2)
        If ds.Tables(0).Rows.Count >= 1 Then
            RadioReportType.SelectedValue = ds.Tables(0).Rows(0).Item("BM_INCIDENT_TYPE").ToString()
            ddstaff.SelectedValue = ds.Tables(0).Rows(0).Item("BM_REPORTING_STAFF_ID").ToString()
            txtincidentdate.Text = Format(ds.Tables(0).Rows(0).Item("BM_INCIDENT_DATE"), "dd-MMM-yyyy")
            'TimeSelector1.Hour = Mid(ds.Tables(0).Rows(0).Item("BM_INCIDENT_TIME"), 1, 2)
            'TimeSelector1.Minute = Mid(ds.Tables(0).Rows(0).Item("BM_INCIDENT_TIME"), 6, 2)
            'TimeSelector1.Second = Mid(ds.Tables(0).Rows(0).Item("BM_INCIDENT_TIME"), 11, 2)
            'ampm = Right(ds.Tables(0).Rows(0).Item("BM_INCIDENT_TIME"), 2)
            'If ampm = "AM" Then
            '    TimeSelector1.AmPm = MKB.TimePicker.TimeSelector.AmPmSpec.AM
            'Else
            '    TimeSelector1.AmPm = MKB.TimePicker.TimeSelector.AmPmSpec.PM
            'End If

            ddlhourpick.Text = Left(ds.Tables(0).Rows(0).Item("BM_INCIDENT_TIME"), 2)
            ddlminutepick.Text = Right(ds.Tables(0).Rows(0).Item("BM_INCIDENT_TIME"), 2)
            txtReportonincident.Text = ds.Tables(0).Rows(0).Item("BM_REPORT_ON_INCIDENT").ToString()
        End If

        cmbMainCategory.SelectedValue = ds2.Tables(0).Rows(0).Item("BM_CATEGORYHRID").ToString()
        bindsub()
        cmbSubCategory.SelectedValue = ds.Tables(0).Rows(0).Item("BM_CATEGORYID").ToString
        lblPoint.Text = "Total Points  " + GetCategoryPoints().ToString()
    End Sub

    Public Sub ActionTakendetails(ByVal stu_id As String)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT * FROM BM.BM_ACTION_MASTER WHERE BM_STU_ID='" & stu_id & "' AND BM_ID='" & HiddenIncidentId.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            hidActionId.Value = ds.Tables(0).Rows(0).Item("BM_ACTION_ID").ToString()
            R1.SelectedValue = ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED").ToString()
            R2.SelectedValue = ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED").ToString()
            R3.SelectedValue = ds.Tables(0).Rows(0).Item("BM_NOTES_STU_PLANNER").ToString()
            R4.SelectedValue = ds.Tables(0).Rows(0).Item("BM_BREAK_DETENTION").ToString()
            R5.SelectedValue = ds.Tables(0).Rows(0).Item("BM_AFTER_SCHOOL_DETENTION").ToString()
            R6.SelectedValue = ds.Tables(0).Rows(0).Item("BM_SUSPENSION").ToString()
            R7.SelectedValue = ds.Tables(0).Rows(0).Item("BM_REF_STU_COUNSELLOR").ToString()
            T1.Text = Format(ds.Tables(0).Rows(0).Item("BM_NOTES_STU_PLANNER_DATE"), "dd-MMM-yyyy")
            T2.Text = Format(ds.Tables(0).Rows(0).Item("BM_BREAK_DETENTION_DATE"), "dd-MMM-yyyy")
            T3.Text = Format(ds.Tables(0).Rows(0).Item("BM_AFTER_SCHOOL_DETENTION_DATE"), "dd-MMM-yyyy")
            T4.Text = Format(ds.Tables(0).Rows(0).Item("BM_SUSPENSION_DATE"), "dd-MMM-yyyy")
            T5.Text = Format(ds.Tables(0).Rows(0).Item("BM_REF_STU_COUNSELLOR_DATE"), "dd-MMM-yyyy")
            txtCalledDate.Text = Format(ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED_DATE"), "dd-MMM-yyyy")
            txtIntvDate.Text = Format(ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED_DATE"), "dd-MMM-yyyy")
            txtparentscalledsaid.Text = ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED_SAID").ToString()
            txtparentsinterviewssaid.Text = ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED_SAID").ToString()
            cmbSubCategory.SelectedValue = ds.Tables(0).Rows(0).Item("BM_CATEGORYID")
            If cmbSubCategory.SelectedIndex >= 0 Then
                lblPoint.Text = "Total Points  " + GetCategoryPoints().ToString()
            End If
            btnSaveAction.Enabled = True
            If R1.SelectedValue = "No" Then
                txtCalledDate.Text = ""
                txtCalledDate.Enabled = False
                imgCalledDate.Enabled = False
            End If
            If R2.SelectedValue = "No" Then
                txtIntvDate.Text = ""
                txtIntvDate.Enabled = False
                imgIntvDate.Enabled = False
            End If
            If R3.SelectedValue = "No" Then
                T1.Text = ""
                T1.Enabled = False
                ImageButton2.Enabled = False
            End If
            If R4.SelectedValue = "No" Then
                T2.Text = ""
                T2.Enabled = False
                ImageButton3.Enabled = False
            End If
            If R5.SelectedValue = "No" Then
                T3.Text = ""
                T3.Enabled = False
                ImageButton4.Enabled = False
            End If
            If R6.SelectedValue = "No" Then
                T4.Text = ""
                T4.Enabled = False
                ImageButton5.Enabled = False
            End If
            If R7.SelectedValue = "No" Then
                T5.Text = ""
                T5.Enabled = False
                ImageButton6.Enabled = False
            End If
        Else
            btnSaveAction.Enabled = False
            R1.SelectedValue = "No"
            R2.SelectedValue = "No"
            R3.SelectedValue = "No"
            R4.SelectedValue = "No"
            R5.SelectedValue = "No"
            R6.SelectedValue = "No"
            R7.SelectedValue = "No"
            T1.Text = ""
            T1.Enabled = False
            T2.Text = ""
            T2.Enabled = False
            T3.Text = ""
            T3.Enabled = False
            T4.Text = ""
            T4.Enabled = False
            T5.Text = ""
            T5.Enabled = False
            'txtIntvDate.Text = ""
            'txtIntvDate.Enabled = False
            'txtCalledDate.Text = ""
            'txtCalledDate.Enabled = False
            ImageButton6.Enabled = False
            ImageButton5.Enabled = False
            ImageButton4.Enabled = False
            ImageButton3.Enabled = False
            ImageButton2.Enabled = False
            imgIntvDate.Enabled = False
            imgCalledDate.Enabled = False
            txtparentscalledsaid.Text = ""
            txtparentsinterviewssaid.Text = ""
            txtnotes.Text = ""
        End If

    End Sub



    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str1 As String = "SELECT COUNT(*) FROM BM.INCIDENT_ROLE_ACCESS WHERE ROLE_ID='" & Session("sroleid") & "' AND BSU_ID='" & Session("sBsuid") & "'"
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str1)
        Dim str_query As String

        If sbm = 0 Then
            str_query = "SELECT STU_ID, STU_NO, CAST(BM_STU_ID AS varchar(12)) +','+CAST(BM_ID AS varchar(12))as viewid ,(STUDENT_M.STU_FIRSTNAME + ' ' + ISNULL(STU_MIDNAME,'')) AS STU_FIRSTNAME ,STUDENT_M.STU_LASTNAME,sct_descr,grm_display,stm_descr,shf_descr " & _
                            " ,student_m.STU_PRIMARYCONTACT , " & _
                            " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FEMAIL  when 'M' then STUDENT_D.STS_MEMAIL  when 'G' then STUDENT_D.STS_GEMAIL  end) as ParentEmail  ," & _
                            " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FMOBILE  when 'M' then STUDENT_D.STS_MMOBILE  when 'G' then STUDENT_D.STS_GMOBILE  end) as ParentMobile , " & _
                            " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FFIRSTNAME + ' ' + STUDENT_D.STS_FLASTNAME when 'M' then STUDENT_D.STS_MFIRSTNAME + ' ' + STUDENT_D.STS_MLASTNAME when 'G' then STUDENT_D.STS_GFIRSTNAME + ' ' + STUDENT_D.STS_GLASTNAME end) as parent_name " & _
                            " ,'' as SEC_EMP , Case When isNULL(BM_CONFIDENTIAL,0) =0 Then 'No' Else 'Yes' End as Confide FROM STUDENT_M  " & _
                            " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID  " & _
                            " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                            " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                            " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                            " INNER JOIN STUDENT_D ON  STUDENT_D.STS_STU_ID= STUDENT_M.STU_ID " & _
                            " INNER JOIN BM.BM_STUDENTSINVOLVED ON BM_STUDENTSINVOLVED.BM_STU_ID=STUDENT_M.STU_ID " & _
                            " WHERE BM_STUDENTSINVOLVED.BM_ID='" & HiddenIncidentId.Value & "'"
        Else
            str_query = "SELECT STU_ID, STU_NO, CAST(BM_STU_ID AS varchar(12)) +','+CAST(BM_ID AS varchar(12))as viewid ,(STUDENT_M.STU_FIRSTNAME + ' ' + ISNULL(STU_MIDNAME,'')) AS STU_FIRSTNAME ,STUDENT_M.STU_LASTNAME,SECTION_M.sct_descr,grm_display,stm_descr,shf_descr " & _
                            " ,student_m.STU_PRIMARYCONTACT , " & _
                            " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FEMAIL  when 'M' then STUDENT_D.STS_MEMAIL  when 'G' then STUDENT_D.STS_GEMAIL  end) as ParentEmail  ," & _
                            " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FMOBILE  when 'M' then STUDENT_D.STS_MMOBILE  when 'G' then STUDENT_D.STS_GMOBILE  end) as ParentMobile , " & _
                            " (case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FFIRSTNAME + ' ' + STUDENT_D.STS_FLASTNAME when 'M' then STUDENT_D.STS_MFIRSTNAME + ' ' + STUDENT_D.STS_MLASTNAME when 'G' then STUDENT_D.STS_GFIRSTNAME + ' ' + STUDENT_D.STS_GLASTNAME end) as parent_name " & _
                            " ,isNULL(SEC_EMP_ID,'') as SEC_EMP , Case When isNULL(BM_CONFIDENTIAL,0) =0 Then 'No' Else 'Yes' End as Confide FROM STUDENT_M  " & _
                            " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID  " & _
                            " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                            " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                            " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                            " INNER JOIN STUDENT_D ON  STUDENT_D.STS_STU_ID= STUDENT_M.STU_ID " & _
                            " INNER JOIN BM.BM_STUDENTSINVOLVED ON BM_STUDENTSINVOLVED.BM_STU_ID=STUDENT_M.STU_ID " & _
                            " LEFT JOIN vw_BSU_TUTOR BSU_TUT ON STU_GRM_ID=BSU_TUT.GRM_ID AND SEC_EMP_ID='" & Session("EmployeeId") & "' AND SEC_BSU_ID='" & Session("sBsuid") & "' " & _
                            " WHERE BM_STUDENTSINVOLVED.BM_ID='" & HiddenIncidentId.Value & "'"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Session("ds1") = ds
        GrdView.DataSource = ds
        GrdView.DataBind()

        dt = New DataTable
        dt.Columns.Add("STU_ID") ''
        dt.Columns.Add("STU_NO") ''
        dt.Columns.Add("STU_FIRSTNAME") ''
        dt.Columns.Add("STU_LASTNAME") ''
        dt.Columns.Add("grm_display") ''
        dt.Columns.Add("sct_descr") ''
        dt.Columns.Add("parent_name") ''
        dt.Columns.Add("ParentEmail") ''
        dt.Columns.Add("ParentMobile") ''

        For Each row As GridViewRow In GrdView.Rows
            Dim dr As DataRow = dt.NewRow()

            dr.Item("STU_ID") = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            dr.Item("STU_NO") = DirectCast(row.FindControl("lblno"), Label).Text.Trim()
            dr.Item("STU_FIRSTNAME") = DirectCast(row.FindControl("lblstufirst"), Label).Text.Trim()
            dr.Item("STU_LASTNAME") = DirectCast(row.FindControl("lblstulast"), Label).Text.Trim()
            dr.Item("grm_display") = DirectCast(row.FindControl("lblstugrade"), Label).Text.Trim()
            dr.Item("sct_descr") = DirectCast(row.FindControl("lblstusection"), Label).Text.Trim()
            dr.Item("parent_name") = DirectCast(row.FindControl("lblstuparentname"), Label).Text.Trim()
            dr.Item("ParentEmail") = DirectCast(row.FindControl("lblstuparentemail"), Label).Text.Trim()
            dr.Item("ParentMobile") = DirectCast(row.FindControl("lblstuparentmobile"), Label).Text.Trim()


            dt.Rows.Add(dr)

        Next


        For Each row As GridViewRow In GrdView.Rows
            If sbm = 1 Then
                If DirectCast(row.FindControl("lblSEC_EMP"), Label).Text.Trim() = "0" Then
                    DirectCast(row.FindControl("lnkAction"), LinkButton).Enabled = False
                End If
            End If
        Next


    End Sub
    Protected Sub bindsub()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYHRID=" & cmbMainCategory.SelectedValue & "ORDER BY BM_CATEGORYID"
            Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            cmbSubCategory.DataSource = dsInitiator.Tables(0)
            cmbSubCategory.DataTextField = "BM_CATEGORYNAME"
            cmbSubCategory.DataValueField = "BM_CATEGORYID"
            cmbSubCategory.DataBind()
            cmbSubCategory.SelectedIndex = 0
            'Dim list As New ListItem
            'list.Text = "Select Category"
            'list.Value = "-1"
            'cmbSubCategory.Items.Insert(0, list)

        Catch ex As Exception

        End Try
    End Sub



    Protected Sub cmbMainCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMainCategory.SelectedIndexChanged
        bindsub()

        'Try
        '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        '    Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID=" & cmbMainCategory.SelectedValue & "ORDER BY BM_CATEGORYID"
        '    Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        '    cmbSubCategory.DataSource = dsInitiator.Tables(0)
        '    cmbSubCategory.DataTextField = "BM_CATEGORYNAME"
        '    cmbSubCategory.DataValueField = "BM_CATEGORYID"
        '    cmbSubCategory.DataBind()
        '    cmbSubCategory.SelectedIndex = 0
        '    'Dim list As New ListItem
        '    'list.Text = "Select Category"
        '    'list.Value = "-1"
        '    'cmbSubCategory.Items.Insert(0, list)

        'Catch ex As Exception

        'End Try
    End Sub

    Public Sub BidStaff()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "select emp_fname+ ' ' + emp_lname as staff , emp_id from employee_m where emp_bsu_id='" & Hiddenbsuid.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddstaff.DataSource = ds
        ddstaff.DataTextField = "staff"
        ddstaff.DataValueField = "emp_id"
        ddstaff.DataBind()
        Dim list As New ListItem
        list.Text = "Select Staff"
        list.Value = "-1"
        ddstaff.Items.Insert(0, list)
    End Sub

    Private Function BindDesignationWiseStaffs(ByVal DesigId As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT (ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) AS STAFF, " & _
                        "EMP_ID FROM EMPLOYEE_M WHERE EMP_DES_ID= " & DesigId & " AND  EMP_BSU_ID='" & Hiddenbsuid.Value & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cmbForwrdFrom.DataSource = ds
        cmbForwrdFrom.DataTextField = "STAFF"
        cmbForwrdFrom.DataValueField = "EMP_ID"
        cmbForwrdFrom.DataBind()
        
    End Function
    Private Function BindDesignationWiseStaffsTo(ByVal DesigId As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT (ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) AS STAFF, " & _
                        "EMP_ID FROM EMPLOYEE_M WHERE EMP_DES_ID=(SELECT BM_TO_DESIGID FROM BM.BM_DESIG_ROUTING WHERE BM_FROM_DESIGID=" & GetDesignationID(ddstaff.SelectedValue) & ") AND  EMP_BSU_ID='" & Hiddenbsuid.Value & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        CmbFwdTo.DataSource = ds
        CmbFwdTo.DataTextField = "STAFF"
        CmbFwdTo.DataValueField = "EMP_ID"
        CmbFwdTo.DataBind()

    End Function

    Public Function CheckPerviousId(ByVal id As String) As Boolean

        Dim returnvalue = False
        For Each row As GridViewRow In GrdView.Rows
            Dim preval As String = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            If preval = id Then
                returnvalue = True
            End If
        Next
        Return returnvalue

    End Function
    Shared dt As DataTable
    Public Sub SearchStudents(ByVal ds As DataSet)

        dt = New DataTable
        dt.Columns.Add("STU_ID") ''
        dt.Columns.Add("STU_NO") ''
        dt.Columns.Add("STU_FIRSTNAME") ''
        dt.Columns.Add("STU_LASTNAME") ''
        dt.Columns.Add("grm_display") ''
        dt.Columns.Add("sct_descr") ''
        dt.Columns.Add("parent_name") ''
        dt.Columns.Add("ParentEmail") ''
        dt.Columns.Add("ParentMobile") ''

        For Each row As GridViewRow In GrdView.Rows
            Dim dr As DataRow = dt.NewRow()

            dr.Item("STU_ID") = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            dr.Item("STU_NO") = DirectCast(row.FindControl("lblno"), Label).Text.Trim()
            dr.Item("STU_FIRSTNAME") = DirectCast(row.FindControl("lblstufirst"), Label).Text.Trim()
            dr.Item("STU_LASTNAME") = DirectCast(row.FindControl("lblstulast"), Label).Text.Trim()
            dr.Item("grm_display") = DirectCast(row.FindControl("lblstugrade"), Label).Text.Trim()
            dr.Item("sct_descr") = DirectCast(row.FindControl("lblstusection"), Label).Text.Trim()
            dr.Item("parent_name") = DirectCast(row.FindControl("lblstuparentname"), Label).Text.Trim()
            dr.Item("ParentEmail") = DirectCast(row.FindControl("lblstuparentemail"), Label).Text.Trim()
            dr.Item("ParentMobile") = DirectCast(row.FindControl("lblstuparentmobile"), Label).Text.Trim()
            dt.Rows.Add(dr)

        Next

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim id As String = ds.Tables(0).Rows(i).Item("STU_ID").ToString()
                If CheckPerviousId(id) Then
                Else
                    Dim dr As DataRow = dt.NewRow()

                    dr.Item("STU_ID") = ds.Tables(0).Rows(i).Item("STU_ID").ToString()
                    dr.Item("STU_NO") = ds.Tables(0).Rows(i).Item("STU_NO").ToString()
                    dr.Item("STU_FIRSTNAME") = ds.Tables(0).Rows(i).Item("STU_FIRSTNAME").ToString()
                    dr.Item("STU_LASTNAME") = ds.Tables(0).Rows(i).Item("STU_LASTNAME").ToString()
                    dr.Item("grm_display") = ds.Tables(0).Rows(i).Item("grm_display").ToString()
                    dr.Item("sct_descr") = ds.Tables(0).Rows(i).Item("sct_descr").ToString()
                    dr.Item("parent_name") = ds.Tables(0).Rows(i).Item("parent_name").ToString()
                    dr.Item("ParentEmail") = ds.Tables(0).Rows(i).Item("ParentEmail").ToString()
                    dr.Item("ParentMobile") = ds.Tables(0).Rows(i).Item("ParentMobile").ToString()
                    dt.Rows.Add(dr)
                End If
            Next
        End If

        GrdView.DataSource = dt
        GrdView.DataBind()
        Session("ds1") = dt

    End Sub
    Protected Sub GrdView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdView.RowCommand

        If e.CommandName = "Action" Then

            If RadioReportType.SelectedIndex = 0 And cmbMainCategory.SelectedItem.Text.ToUpper = "NEGATIVE BEHAVIOR" Then
                BIndInvolvedStudents(HiddenIncidentId.Value)
                Panel1.Visible = True
                cmbStudent.SelectedValue = e.CommandArgument
                cmbStudent.Enabled = False
                HiddenStudentId.Value = e.CommandArgument
                ActionTakendetails(e.CommandArgument)
                BindFollowUpDetails()
            End If
            
        End If
        If e.CommandName = "delete" Then
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "DELETE BM.BM_ACTION_MASTER WHERE BM_ID='" & HiddenIncidentId.Value & "' AND BM_STU_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            'Remove From Action Master
            str_query = "DELETE BM.BM_STUDENTSINVOLVED WHERE BM_ID='" & HiddenIncidentId.Value & "' AND BM_STU_ID='" & e.CommandArgument & "'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            lblmessage.Text = "Record Deleted"
        End If

    End Sub

    Protected Sub GrdView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdView.PageIndexChanging

        GrdView.PageIndex = e.NewPageIndex
        If Session("ds1") Is Nothing Then
            BindGrid()
        Else
            GrdView.DataSource = Session("ds1")
            GrdView.DataBind()
        End If

    End Sub
    Protected Sub lnkinsertnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkinsertnew.Click

        Response.Redirect("bm_CreateNewIncident.aspx")

    End Sub
    Protected Sub GrdView_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GrdView.RowDeleting

        dt.Rows.Item(e.RowIndex).Delete()
        Session("ds1") = dt
        GrdView.DataSource = Session("ds1")
        GrdView.DataBind()

    End Sub

    Public Sub SaveAllStudentDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        For Each row As GridViewRow In GrdView.Rows
            Dim stu_id As String = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            Dim pParms(17) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BM_ID", HiddenIncidentId.Value)
            pParms(1) = New SqlClient.SqlParameter("@BM_STU_ID", stu_id)
            pParms(2) = New SqlClient.SqlParameter("@BM_PARENT_CALLED", R1.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@BM_PARENT_CALLED_SAID", txtparentscalledsaid.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@BM_PARENT_INTERVIED", R2.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@BM_PARENT_INTERVIED_SAID", txtparentsinterviewssaid.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@BM_NOTES_STU_PLANNER", R3.SelectedValue)
            pParms(7) = New SqlClient.SqlParameter("@BM_NOTES_STU_PLANNER_DATE", T1.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@BM_BREAK_DETENTION", R4.SelectedValue)
            pParms(9) = New SqlClient.SqlParameter("@BM_BREAK_DETENTION_DATE", T2.Text.Trim())
            pParms(10) = New SqlClient.SqlParameter("@BM_AFTER_SCHOOL_DETENTION", R5.SelectedValue)
            pParms(11) = New SqlClient.SqlParameter("@BM_AFTER_SCHOOL_DETENTION_DATE", T3.Text.Trim())
            pParms(12) = New SqlClient.SqlParameter("@BM_SUSPENSION", R6.SelectedValue)
            pParms(13) = New SqlClient.SqlParameter("@BM_SUSPENSION_DATE", T4.Text.Trim())
            pParms(14) = New SqlClient.SqlParameter("@BM_REF_STU_COUNSELLOR", R7.SelectedValue)
            pParms(15) = New SqlClient.SqlParameter("@BM_REF_STU_COUNSELLOR_DATE", T5.Text.Trim())
            pParms(16) = New SqlClient.SqlParameter("@BM_FOLLOWUP", txtnotes.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM_UPDATE_BM_ACTION_MASTER", pParms)
            'End If
        Next
        lblmessage.Text = "Record(s) Updated"
    End Sub

    Public Sub SaveActionDetails()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        For Each row As GridViewRow In GrdView.Rows
            Dim stu_id As String = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            Dim pParms(11) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACTIONDET_ID", HiddenIncidentId.Value)
            pParms(1) = New SqlClient.SqlParameter("@BM_ID", stu_id)
            pParms(2) = New SqlClient.SqlParameter("@ACTION_ID", R1.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@ACTIONDATE", txtparentscalledsaid.Text.Trim())
            pParms(4) = New SqlClient.SqlParameter("@STAFF_ID", R2.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@ACTION_DESCR", txtparentsinterviewssaid.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@COMMENTS", R3.SelectedValue)
            pParms(7) = New SqlClient.SqlParameter("@FWDTO", T1.Text.Trim())
            pParms(8) = New SqlClient.SqlParameter("@FWDDATE", R4.SelectedValue)
            pParms(9) = New SqlClient.SqlParameter("@BM_DESIGID", cmbForwrdFrom.SelectedValue)
            pParms(10) = New SqlClient.SqlParameter("@DATAMODE", T2.Text.Trim())
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM_saveACTIONDETAILS", pParms)

        Next
        lblmessage.Text = "Record(s) Inserted"

    End Sub
    Private Sub SaveWitnessDetails()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim DtWitNess As DataTable
            DtWitNess = Session("WitnessTable")
            For Each DrRow As DataRow In DtWitNess.Rows
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@WITNESS_ID", hid_witnessId.Value)
                pParms(1) = New SqlClient.SqlParameter("@BM_ID", HiddenIncidentId.Value)
                pParms(2) = New SqlClient.SqlParameter("@WITNESSID", DrRow.Item("STUD_ID"))
                pParms(3) = New SqlClient.SqlParameter("@WITNESS_TYPE", DrRow.Item("STU_TYPE"))
                pParms(4) = New SqlClient.SqlParameter("@WITNESS_SAID", DrRow.Item("SPOKEN"))
                pParms(5) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM.BM_SaveINCIDENTWITNESS", pParms)
            Next
        Catch ex As Exception
        End Try
    End Sub




    Private Sub SaveActionFwd(ByVal ActionId As Integer)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "SELECT * FROM BM.BM_ACTIONDETAIL WHERE BM_ACTION_ID='" & hidActionId.Value & "' "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        hidActionFwd.Value = 0
        Dim pParms(11) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACTIONDET_ID", hidActionFwd.Value)
        pParms(1) = New SqlClient.SqlParameter("@BM_ID", HiddenIncidentId.Value)
        pParms(2) = New SqlClient.SqlParameter("@ACTION_ID", hidActionId.Value)
        pParms(3) = New SqlClient.SqlParameter("@ACTIONDATE", Date.Now())
        pParms(4) = New SqlClient.SqlParameter("@STAFF_ID", CmbFwdTo.SelectedItem.Value) 'Session("sUsr_name")
        pParms(5) = New SqlClient.SqlParameter("@ACTION_DESCR", txtnotes.Text.Trim)
        pParms(6) = New SqlClient.SqlParameter("@COMMENTS", txtnotes.Text.Trim)
        pParms(7) = New SqlClient.SqlParameter("@FWDTO", GetDesignationID(cmbForwrdFrom.SelectedItem.Value))
        'pParms(7) = New SqlClient.SqlParameter("@FWDTO", CmbFwdTo.SelectedValue)
        pParms(8) = New SqlClient.SqlParameter("@FWDDATE", Date.Now())
        pParms(9) = New SqlClient.SqlParameter("@BM_DESIGID", GetDesignationID(cmbForwrdFrom.SelectedValue))
        'pParms(9) = New SqlClient.SqlParameter("@BM_DESIGID", cmbForwrdFrom.SelectedValue)
        'pParms(10) = New SqlClient.SqlParameter("@DATAMODE", "Add")

        'If ds.Tables(0).Rows.Count >= 1 Then
        '    pParms(10) = New SqlClient.SqlParameter("@DATAMODE", "Edit")
        'Else
        pParms(10) = New SqlClient.SqlParameter("@DATAMODE", "Add")
        'End If

        hidActionFwd.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "BM.BM_saveACTIONDETAILS", pParms)
    End Sub

    Public Sub SaveSelectedStudentDetails(ByVal stu_id As String)

        If CheckStudetActionExist(stu_id, HiddenIncidentId.Value) = True Then
            ViewState("DataMode") = "Edit"
        Else
            ViewState("DataMode") = "Add"
            hidActionId.Value = 0
        End If
        If HidCategoryPoints.Value = "" Then
            HidCategoryPoints.Value = 0
        End If

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(23) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACTION_ID", hidActionId.Value)
        pParms(1) = New SqlClient.SqlParameter("@BM_ID", HiddenIncidentId.Value)
        pParms(2) = New SqlClient.SqlParameter("@STU_ID", cmbStudent.SelectedValue)
        pParms(3) = New SqlClient.SqlParameter("@CATEGORYID", cmbSubCategory.SelectedValue)
        pParms(4) = New SqlClient.SqlParameter("@PARENT_CALLED", R1.SelectedValue)
        pParms(5) = New SqlClient.SqlParameter("@PARENT_SAID", txtparentscalledsaid.Text.Trim())
        pParms(6) = New SqlClient.SqlParameter("@PARENT_CALLDATE", txtCalledDate.Text.Trim)
        pParms(7) = New SqlClient.SqlParameter("@PARENT_INTERVIED", R2.SelectedValue)
        pParms(8) = New SqlClient.SqlParameter("@PARENT_INTVSAID", txtparentsinterviewssaid.Text)
        pParms(9) = New SqlClient.SqlParameter("@PARENT_INTERVDATE", txtIntvDate.Text.Trim())
        pParms(10) = New SqlClient.SqlParameter("@NOTES_STUPLANNER", R3.SelectedValue)
        pParms(11) = New SqlClient.SqlParameter("@NOTES_STUPLANDATE", T1.Text.Trim())
        pParms(12) = New SqlClient.SqlParameter("@BREAK_DETENTION", R4.SelectedValue)
        pParms(13) = New SqlClient.SqlParameter("@BREAK_DETENTION_DATE", T2.Text.Trim())
        pParms(14) = New SqlClient.SqlParameter("@AFTER_SCH_DETN", R5.SelectedValue)
        pParms(15) = New SqlClient.SqlParameter("@AFTER_SCH_DETNDATE", T3.Text.Trim())
        pParms(16) = New SqlClient.SqlParameter("@SUSPENSION", R6.SelectedValue)
        pParms(17) = New SqlClient.SqlParameter("@SUSPENSION_DATE", T4.Text.Trim())
        pParms(18) = New SqlClient.SqlParameter("@STU_COUNSELLOR", R7.SelectedValue)
        pParms(19) = New SqlClient.SqlParameter("@STU_COUNSELLOR_DATE", T5.Text.Trim())
        pParms(20) = New SqlClient.SqlParameter("@ENTRY_DATE", lbltodaydate.Text.Trim)
        pParms(21) = New SqlClient.SqlParameter("@SCORE", HidCategoryPoints.Value)
        pParms(22) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM.BM_SaveACTIONMASTER", pParms)
        lblmessage.Text = "Record(s) Inserted"

    End Sub
    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click, btnUPDATE.Click
        Try

            If CheckDateIsValid() = True Then

                ViewState("DataMode") = "Edit"
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                'Dim pktime As String = Format(TimeSelector1.Hour, "00") & " : " & Format(TimeSelector1.Minute, "00") & " : " & Format(TimeSelector1.Second, "00") & "  " & TimeSelector1.AmPm.ToString
                Dim pktime As String = ddlhourpick.Text & " : " & ddlminutepick.Text
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BMID", HiddenIncidentId.Value)
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
                pParms(2) = New SqlClient.SqlParameter("@ENTRY_DATE", lbltodaydate.Text)
                pParms(3) = New SqlClient.SqlParameter("@INCIDENT_DATE", txtincidentdate.Text.Trim())
                pParms(4) = New SqlClient.SqlParameter("@INCIDENT_TIME", pktime)
                pParms(5) = New SqlClient.SqlParameter("@INCIDENT_TYPE", RadioReportType.SelectedValue)
                pParms(6) = New SqlClient.SqlParameter("@STAFF_ID", ddstaff.SelectedValue)
                pParms(7) = New SqlClient.SqlParameter("@INCIDENT_DESC", txtReportonincident.Text)
                pParms(8) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
                pParms(9) = New SqlClient.SqlParameter("@CATEGORYID", cmbSubCategory.SelectedValue)

                SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "BM.BM_saveBMMASTER", pParms)

                SaveSelectedStudentDetails(cmbStudent.SelectedValue)
                btnSaveAction.Enabled = True
                Response.Redirect("bm_EditIncident.aspx?inc_id=" & HiddenIncidentId.Value)

            Else
                lblValidDAte.Text = " The above dates Can't be greater than Today's date."
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Function GetDesignation(ByVal StaffID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DES_ID,DES_DESCR,DES_FLAG FROM EMPDESIGNATION_M WHERE DES_ID=" & _
             "(SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=" & StaffID & ")"
            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("DES_DESCR")), 0, dsCategory.Tables(0).Rows(0).Item("DES_DESCR"))
            End If
        Catch ex As Exception

        End Try
    End Function
    Public Function GetDesignationID(ByVal StaffID As String) As Integer
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DES_ID,DES_DESCR,DES_FLAG FROM EMPDESIGNATION_M WHERE DES_ID=" & _
             "(SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=" & StaffID & ")"
            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("DES_ID")), 0, dsCategory.Tables(0).Rows(0).Item("DES_ID"))
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Function BIndFWDDesignations()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim str_query As String = "SELECT DES_ID,DES_DESCR FROM EMPDESIGNATION_M WHERE DES_FLAG='SD' AND DES_DESCR<>'--' " & _
            '                        " AND DES_ID NOT IN(SELECT ISNULL(BM_DESIGID,'') FROM BM.BM_ACTIONDETAIL WHERE BM_ACTION_ID=" + hidActionId.Value + " )" & _
            '                        " AND DES_ID IN (SELECT BM_TO_DESIGID FROM BM.BM_DESIG_ROUTING  " & _
            '                        " WHERE BSU_ID=" & Session("sbsuid") & " AND BM_FROM_DESIGID=(" & _
            '                        " SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=( " & _
            '                        " SELECT BM_REPORTING_STAFF_ID FROM BM.BM_MASTER WHERE BM_ID=" & HiddenIncidentId.Value & ")))"
            'str_query = "SELECT BM_TO_DESIGID FROM BM.BM_DESIG_ROUTING WHERE BM_FROM_DESIGID=" & GetDesignationID(ddstaff.SelectedValue)

            Dim str_query As String = "SELECT DES_ID,DES_DESCR FROM EMPDESIGNATION_M WHERE DES_FLAG='SD' AND DES_DESCR <> '--' " & _
                                      " AND DES_ID IN (SELECT BM_TO_DESIGID FROM BM.BM_DESIG_ROUTING WHERE BSU_ID=" & Session("sbsuid") & " AND BM_FROM_DESIGID=( " & _
                                      " SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=( " & _
                                      " SELECT BM_REPORTING_STAFF_ID FROM BM.BM_MASTER WHERE BM_ID=" & HiddenIncidentId.Value & ")))"
            '                       

            Dim dsStudents As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            'If dsStudents.Tables(0).Rows.Count >= 1 Then
            '    BindDesignationWiseStaffs(dsStudents.Tables(0).Rows(0).Item(0))
            '    BindDesignationWiseStaffsTo(dsStudents.Tables(0).Rows(1).Item(0))

            '    'If dsStudents.Tables(0).Rows.Count >= 2 Then
            '    '    BindDesignationWiseStaffsTo(dsStudents.Tables(0).Rows(1).Item(0))
            '    'End If
            '    'If dsStudents.Tables(0).Rows.Count >= 3 Then
            '    '    BindDesignationWiseStaffsTo(dsStudents.Tables(0).Rows(2).Item(0))
            '    'End If
            '    'If dsStudents.Tables(0).Rows.Count >= 4 Then
            '    '    BindDesignationWiseStaffsTo(dsStudents.Tables(0).Rows(3).Item(0))
            '    'End If
            '    'BindDesignationWiseStaffsTo
            'End If
            cmbForwrdFrom.DataSource = dsStudents.Tables(0)
            cmbForwrdFrom.DataTextField = "DES_DESCR"
            cmbForwrdFrom.DataValueField = "DES_ID"
            cmbForwrdFrom.DataBind()
        Catch ex As Exception

        End Try
    End Function

    Private Function BIndInvolvedStudents(ByVal BMID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT STU_ID,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) AS STU_FIRSTNAME FROM STUDENT_M WHERE STU_ID IN " & _
             "( SELECT BM_STU_ID FROM BM.BM_STUDENTSINVOLVED WHERE BM_ID=" & BMID & ")"

            Dim dsStudents As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            cmbStudent.DataSource = dsStudents.Tables(0)
            cmbStudent.DataTextField = "STU_FIRSTNAME"
            cmbStudent.DataValueField = "STU_ID"
            cmbStudent.DataBind()
        Catch ex As Exception

        End Try
    End Function
    Private Sub BindMainCategory()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID <> 0 ORDER BY BM_CATEGORYID"
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID = 0 ORDER BY BM_CATEGORYID"
            Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            cmbMainCategory.DataSource = dsInitiator.Tables(0)
            cmbMainCategory.DataTextField = "BM_CATEGORYNAME"
            cmbMainCategory.DataValueField = "BM_CATEGORYID"
            cmbMainCategory.DataBind()
            Dim list As New ListItem
            list.Text = "Select Category"
            list.Value = "-1"
            cmbMainCategory.Items.Insert(0, list)

        Catch ex As Exception

        End Try
    End Sub
    'Private Sub BindCategory()
    '    Try
    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY ORDER BY BM_CATEGORYID"
    '        Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    '        cmbCategory.DataSource = dsInitiator.Tables(0)
    '        cmbCategory.DataTextField = "BM_CATEGORYNAME"
    '        cmbCategory.DataValueField = "BM_CATEGORYID"
    '        cmbCategory.DataBind()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub CreateWitNessWiewTable()

        Dim DtWitNess As DataTable
        DtWitNess = New DataTable
        DtWitNess.Columns.Add("STUD_ID")
        DtWitNess.Columns.Add("STU_TYPE")
        DtWitNess.Columns.Add("STUD_NAME")
        DtWitNess.Columns.Add("SPOKEN")
        Session("WitnessTable") = DtWitNess

    End Sub
    Private Sub BindWitnessDetails()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT BM_WITNESS_ID,BM_ID,BM_WITNESSID,BM_WITNESS_TYPE,BM_WITNESS_SAID FROM BM.BM_INCIDENTWITNESS " & _
                                  " WHERE BM_ID=" & HiddenIncidentId.Value
        Dim dsWitnesses As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim DtWitness As DataTable
        DtWitness = Session("WitnessTable")

        For Each DrRow As DataRow In dsWitnesses.Tables(0).Rows
            Dim drWitness1 As DataRow = DtWitness.NewRow()
            drWitness1.Item("STUD_ID") = DrRow.Item("BM_WITNESSID")
            drWitness1.Item("STU_TYPE") = DrRow.Item("BM_WITNESS_TYPE")
            If DrRow.Item("BM_WITNESS_TYPE") = "STF" Then
                drWitness1.Item("STUD_NAME") = GetSaffName(DrRow.Item("BM_WITNESSID"))
            Else
                drWitness1.Item("STUD_NAME") = GetStudentName(DrRow.Item("BM_WITNESSID"))
            End If
            drWitness1.Item("SPOKEN") = DrRow.Item("BM_WITNESS_SAID")
            DtWitness.Rows.Add(drWitness1)
        Next

        Session("WitnessTable") = DtWitness
        gvWitness.DataSource = DtWitness
        gvWitness.DataBind()
    End Sub

    Private Sub BindFollowUpDetails()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        If hidActionId.Value <> "" And HiddenIncidentId.Value <> "" Then

            Dim str_query As String = " SELECT BM_ID,BM_ACTION_ID,BM_ACTIONDATE,BM_COMMENTS,STAFF_ID,BM_FWDTO,B.EMPNO," & _
            "(ISNULL(B.EMP_FNAME,'') + ' '+ ISNULL(B.EMP_MNAME,'') + ' '+ ISNULL(B.EMP_LNAME,'')) As EmpName, C.DES_DESCR,T.DES_DESCR As ToName " & _
            "FROM BM.BM_ACTIONDETAIL A INNER JOIN EMPLOYEE_M B ON A.STAFF_ID=B.EMP_ID " & _
            "LEFT JOIN EMPDESIGNATION_M C ON A.BM_DESIGID=C.DES_ID " & _
            "LEFT JOIN EMPDESIGNATION_M T ON T.DES_ID=A.BM_FWDTO " & _
            " WHERE BM_ID=" & HiddenIncidentId.Value & " " & _
            " AND BM_ACTION_ID=" & hidActionId.Value

            Dim dsActionDet As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            grdFollowup.DataSource = dsActionDet.Tables(0)

            If dsActionDet.Tables(0).Rows.Count = 0 Then
                dsActionDet.Tables(0).Rows.Add(dsActionDet.Tables(0).NewRow())
                grdFollowup.DataBind()
                Dim columnCount As Integer = grdFollowup.Rows(0).Cells.Count
                grdFollowup.Rows(0).Cells.Clear()
                grdFollowup.Rows(0).Cells.Add(New TableCell)
                grdFollowup.Rows(0).Cells(0).ColumnSpan = columnCount
                grdFollowup.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                grdFollowup.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                grdFollowup.DataBind()
                For Each GrdvRow As GridViewRow In grdFollowup.Rows
                    If GrdvRow.RowType = DataControlRowType.DataRow Then
                        DirectCast(GrdvRow.FindControl("lblActionDate"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblActionDate"), Label).Text), "dd-MMM-yyyy")
                    End If
                Next
            End If

        End If

    End Sub
    Private Function GetSaffName(ByVal StaffID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT EMPNO,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EmpName FROM EMPLOYEE_M WHERE EMP_ID=" & StaffID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("EmpName")
            Else
                Return ""
            End If

        Catch ex As Exception
        End Try
    End Function

    Private Function GetStudentName(ByVal StudID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT STU_NO,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) As StudentName FROM STUDENT_M WHERE STU_ID=" & StudID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("StudentName")
            Else
                Return ""
            End If
        Catch ex As Exception
        End Try
    End Function
    Private Sub SaveAlert(ByVal AlertDesc As String)
        Try

            Dim strFwdTo As String
            If CmbFwdTo.SelectedItem.Text = "" Then
                strFwdTo = 0
            Else
                strFwdTo = CmbFwdTo.SelectedValue
            End If

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TRA_ID", 4)
            pParms(1) = New SqlClient.SqlParameter("@ALR_DESCR", AlertDesc)
            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            pParms(3) = New SqlClient.SqlParameter("@ALR_ID", 0)
            pParms(4) = New SqlClient.SqlParameter("@USR_ID", strFwdTo)
            pParms(5) = New SqlClient.SqlParameter("@ALR_ACTIVE", 1)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "saveALERTS_M", pParms)
        Catch ex As Exception

        End Try
    End Sub
    Sub PopulateClassTeacher()
        CmbFwdTo.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query
        If cmbForwrdFrom.SelectedIndex >= 0 Then
            str_query = "SELECT ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(emp_mname,'') + ' ' + ISNULL(EMP_LNAME,'') AS emp_name , EMP_ID FROM EMPLOYEE_M WHERE EMP_STATUS=1 AND EMP_BSU_ID='" & Session("sBsuid") & "' AND EMP_DES_ID = '" & cmbForwrdFrom.SelectedItem.Value & "' " _
                                      '" SELECT DES_ID FROM EMPDESIGNATION_M WHERE DES_FLAG='SD' AND DES_DESCR <> '--' " & _
            '" AND DES_ID IN (SELECT BM_TO_DESIGID FROM BM.BM_DESIG_ROUTING WHERE BSU_ID=" & Session("sbsuid") & " AND BM_FROM_DESIGID=( " & _
            '" SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=( " & _
            '" SELECT BM_REPORTING_STAFF_ID FROM BM.BM_MASTER WHERE BM_ID=" & HiddenIncidentId.Value & "))))"


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            CmbFwdTo.DataSource = ds
            CmbFwdTo.DataTextField = "emp_name"
            CmbFwdTo.DataValueField = "emp_id"
            CmbFwdTo.DataBind()
            Dim lstNew As New ListItem
            lstNew.Value = 0
            lstNew.Text = ""
            CmbFwdTo.Items.Add(lstNew)
            CmbFwdTo.SelectedValue = 0
        Else

        End If

        
    End Sub

    Private Function GetWintnessComments(ByVal STUDID As Integer, ByVal BMID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT BM_WITNESS_ID,BM_ID,BM_WITNESSID,BM_WITNESS_TYPE,BM_WITNESS_SAID FROM BM.BM_INCIDENTWITNESS WHERE BM_WITNESSID=" & STUDID & " AND BM_ID=" & BMID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                With dsStaff.Tables(0).Rows(0)
                    If .Item("BM_WITNESS_TYPE") = "STU" Then
                        radStudent.Checked = True
                        txtPar_Sib.Text = GetStudentName(.Item("BM_WITNESSID"))
                        btnEmp_Name.Enabled = False
                        imgbtnSibling.Enabled = True
                        radStaff.Checked = False
                    Else
                        radStaff.Checked = True
                        txtPar_Sib.Text = GetSaffName(.Item("BM_WITNESSID"))
                        btnEmp_Name.Enabled = True
                        imgbtnSibling.Enabled = False
                        radStudent.Checked = False
                    End If
                    txtwithnesssaid.Text = .Item("BM_WITNESS_SAID").ToString
                    hid_witnessId.Value = .Item("BM_WITNESS_ID").ToString
                    Add.Text = "Update"
                End With
            End If
        Catch ex As Exception
        End Try
    End Function

    Private Function CheckStudetActionExist(ByVal StudId As Integer, ByVal BMID As Integer) As Boolean
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT BM_ACTION_ID,BM_ID,BM_STU_ID FROM BM.BM_ACTION_MASTER WHERE BM_STU_ID=" & StudId & " AND BM_ID=" & BMID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
        End Try
    End Function

    Private Sub EnableIncidentMaster()
        RadioReportType.Enabled = True
        ddstaff.Enabled = True
        txtincidentdate.Enabled = True
        ImageButton1.Enabled = True
        'cmbCategory.Enabled = True
        txtReportonincident.Enabled = True
        radStaff.Enabled = True
        radStudent.Enabled = True
        txtPar_Sib.Enabled = True
        btnEmp_Name.Enabled = True
        imgbtnSibling.Enabled = True
        Add.Enabled = True
        txtwithnesssaid.Enabled = True
        gvWitness.Enabled = True
        GrdView.Enabled = True
    End Sub

    Private Sub DisableIncidentMaster()
        RadioReportType.Enabled = False
        ddstaff.Enabled = False
        txtincidentdate.Enabled = False
        ImageButton1.Enabled = False
        'cmbCategory.Enabled = False
        txtReportonincident.Enabled = False
        radStaff.Enabled = False
        radStudent.Enabled = False
        txtPar_Sib.Enabled = False
        btnEmp_Name.Enabled = False
        imgbtnSibling.Enabled = False
        Add.Enabled = False
        txtwithnesssaid.Enabled = False
        gvWitness.Enabled = False
        GrdView.Enabled = False
    End Sub
    '------------------ The Below Part is Exclusively for Behavieour Action Session -------------------------------------
    Private Sub EnableActionSection()
        cmbStudent.Enabled = True
        R1.Enabled = True
        txtparentscalledsaid.Enabled = True
        R2.Enabled = True
        txtparentsinterviewssaid.Enabled = True
        R3.Enabled = True
        R4.Enabled = True
        R5.Enabled = True
        R6.Enabled = True
        R7.Enabled = True
        T1.Enabled = True
        T2.Enabled = True
        T3.Enabled = True
        T4.Enabled = True
        T5.Enabled = True
        ImageButton1.Enabled = True
        ImageButton2.Enabled = True
        ImageButton3.Enabled = True
        ImageButton4.Enabled = True
        ImageButton5.Enabled = True
        ImageButton6.Enabled = True
        txtnotes.Enabled = True
        btnsave.Enabled = True
        txtCalledDate.Enabled = True
        txtincidentdate.Enabled = True
        imgCalledDate.Enabled = True
        imgIntvDate.Enabled = True
    End Sub

    Private Sub DisableActionSection()
        cmbStudent.Enabled = False
        R1.Enabled = False
        txtparentscalledsaid.Enabled = False
        R2.Enabled = False
        txtparentsinterviewssaid.Enabled = False
        R3.Enabled = False
        R4.Enabled = False
        R5.Enabled = False
        R6.Enabled = False
        R7.Enabled = False
        T1.Enabled = False
        T2.Enabled = False
        T3.Enabled = False
        T4.Enabled = False
        T5.Enabled = False
        ImageButton1.Enabled = False
        ImageButton2.Enabled = False
        ImageButton3.Enabled = False
        ImageButton4.Enabled = False
        ImageButton5.Enabled = False
        ImageButton6.Enabled = False
        txtnotes.Enabled = False
        btnsave.Enabled = False
        txtCalledDate.Enabled = False
        imgCalledDate.Enabled = False
        imgIntvDate.Enabled = False

    End Sub

    Private Sub DisableFWDSEction()
        cmbForwrdFrom.Enabled = False
        cmbForwrdFrom.Items.Clear()
        CmbFwdTo.Enabled = False
        txtnotes.Enabled = False
        btnSaveAction.Enabled = False
    End Sub

    Protected Sub gvWitness_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvWitness.RowCommand
        If e.CommandName = "Edit" Then
            GetWintnessComments(e.CommandArgument, HiddenIncidentId.Value)
        End If
    End Sub

    Protected Sub gvWitness_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvWitness.RowEditing
        e.Cancel = True
    End Sub

    Protected Sub C1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles C1.CheckedChanged
        If C1.Checked = True Then
            CmbFwdTo.Enabled = True
            cmbForwrdFrom.Enabled = True
            BIndFWDDesignations()
            txtnotes.Enabled = True
            btnSaveAction.Enabled = True
            PopulateClassTeacher()
        Else
            cmbForwrdFrom.Enabled = False
            CmbFwdTo.Enabled = False
            txtnotes.Enabled = False
            btnSaveAction.Enabled = False
            CmbFwdTo.Enabled = False
        End If
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload

    End Sub

    Protected Sub R3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R3.SelectedIndexChanged
        If R3.SelectedValue = "No" Then
            T1.Text = ""
            T1.Enabled = False
            ImageButton2.Enabled = False
        Else
            T1.Enabled = True
            ImageButton2.Enabled = True
        End If
    End Sub

    Protected Sub R4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R4.SelectedIndexChanged
        If R4.SelectedValue = "No" Then
            T2.Text = ""
            T2.Enabled = False
            ImageButton3.Enabled = False
        Else
            T2.Enabled = True
            ImageButton3.Enabled = True
        End If
    End Sub

    Protected Sub R5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R5.SelectedIndexChanged
        If R5.SelectedValue = "No" Then
            T3.Text = ""
            T3.Enabled = False
            ImageButton4.Enabled = False
        Else
            T3.Enabled = True
            ImageButton4.Enabled = True
        End If
    End Sub

    Protected Sub R6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R6.SelectedIndexChanged
        If R6.SelectedValue = "No" Then
            T4.Text = ""
            T4.Enabled = False
            ImageButton5.Enabled = False
        Else
            T4.Enabled = True
            ImageButton5.Enabled = True
        End If
    End Sub

    Protected Sub R7_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R7.SelectedIndexChanged
        If R7.SelectedValue = "No" Then
            T5.Text = ""
            T5.Enabled = False
            ImageButton6.Enabled = False
        Else
            T5.Enabled = True
            ImageButton6.Enabled = True
        End If
    End Sub

    Protected Sub R2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R2.SelectedIndexChanged
        If R2.SelectedValue = "No" Then
            txtIntvDate.Text = ""
            txtIntvDate.Enabled = False
            imgIntvDate.Enabled = False
        Else
            txtIntvDate.Enabled = True
            imgIntvDate.Enabled = True
        End If
    End Sub

    Protected Sub R1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R1.SelectedIndexChanged
        If R1.SelectedValue = "No" Then
            txtCalledDate.Text = ""
            txtCalledDate.Enabled = False
            imgCalledDate.Enabled = False
        Else
            txtCalledDate.Enabled = True
            imgCalledDate.Enabled = True
        End If
    End Sub

    'Protected Sub cmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.SelectedIndexChanged
    '    Try
    '        If cmbCategory.SelectedIndex >= 0 Then
    '            HidCategoryPoints.Value = GetCategoryPoints().ToString()
    '            lblPoint.Text = "Total Points : " + GetCategoryPoints().ToString()
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Public Function GetCategoryPoints() As Integer
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME,BM_CATEGORY_SCORE FROM BM.BM_CATEGORY WHERE BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYID=" & cmbSubCategory.SelectedValue
            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("BM_CATEGORY_SCORE")), 0, dsCategory.Tables(0).Rows(0).Item("BM_CATEGORY_SCORE"))
            End If

        Catch ex As Exception

        End Try
    End Function

    Protected Sub btnSaveAction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAction.Click
        Try
            btnSaveAction.Enabled = False
            If C1.Checked = True And cmbForwrdFrom.SelectedItem.Text <> "" Then
                SaveActionFwd(Convert.ToInt32(hidActionId.Value))
                'If CmbFwdTo.SelectedItem.Text <> "" Then
                '    SaveAlert("Student Behavoiur Added")
                'End If
                txtnotes.Text = ""
                C1.Checked = False
                BindFollowUpDetails()
                btnSaveAction.Enabled = True
                'Response.Redirect("bm_IncidentView.aspx")
            End If
        Catch ex As Exception
            '
        End Try
    End Sub

    Private Function CheckDateIsValid() As Boolean
        Try
            If R1.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, Date.Now(), CDate(txtCalledDate.Text)) >= 1 Then
                    Return False
                    Exit Function
                End If
            End If

            If R2.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, Date.Now(), CDate(txtIntvDate.Text)) >= 1 Then
                    Return False
                    Exit Function
                End If
            End If
            Return True
        Catch ex As Exception

        End Try
    End Function

    Private Function CahekDateWithTodate(ByVal CheckDate As Date) As Boolean
        Try
            If DateDiff(DateInterval.Day, CheckDate, Date.Now()) < 0 Then
                Return False
                Exit Function
            End If
            Return True
        Catch ex As Exception
        End Try
    End Function

    Protected Sub Add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Add.Click
        'Dim strWitNessType As String
        'Dim strStuID As String

        'If txtPar_Sib.Text.Trim <> "" Then
        '    Dim DtWitness As DataTable
        '    DtWitness = Session("WitnessTable")
        '    Dim DrWitness As DataRow = DtWitness.NewRow()

        '    If radStaff.Checked = True Then
        '        strWitNessType = "STF"
        '        strStuID = hfEmp_ID.Value
        '        DrWitness.Item("STUD_NAME") = GetSaffName(strStuID) ' txtPar_Sib.Text
        '    Else
        '        strWitNessType = "STU"
        '        strStuID = h_SliblingID.Value
        '        DrWitness.Item("STUD_NAME") = GetStudentName(strStuID) ' txtPar_Sib.Text
        '    End If

        '    DrWitness.Item("STUD_ID") = strStuID
        '    DrWitness.Item("STU_TYPE") = strWitNessType
        '    'DrWitness.Item("STUD_NAME") = GetStudentName(strStuID) ' txtPar_Sib.Text
        '    DrWitness.Item("SPOKEN") = txtwithnesssaid.Text
        '    DtWitness.Rows.Add(DrWitness)
        '    Session("WitnessTable") = DtWitness

        '    gvWitness.DataSource = DtWitness
        '    gvWitness.DataBind()

        '    txtwithnesssaid.Text = ""
        '    txtPar_Sib.Text = ""
        '    'lblCaption.Text = ""
        'End If
    End Sub

    Protected Sub cmbForwrdFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbForwrdFrom.SelectedIndexChanged
        PopulateClassTeacher()
    End Sub
End Class
