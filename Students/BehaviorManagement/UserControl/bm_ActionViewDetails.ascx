<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_ActionViewDetails.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_ActionViewDetails" %>


<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

<div align="center">

    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg">Primary Information&nbsp;<asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/Images/tick.gif"
                OnClientClick="javascript:window.print(); return false;" ToolTip="Print Page" /></td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td colspan="4" class="title-bg-lite">Student Information</td>
                    </tr>
                    <tr>
                        <td width="20%">
                            <span class="field-label">Student Name</span></td>
                        <td colspan="2" width="50%">
                            <asp:Label ID="lblstudentname" runat="server" CssClass="field-value"></asp:Label></td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td width="20%">
                            <span class="field-label">Grade</span></td>

                        <td width="30%">
                            <asp:Label ID="lblstudentgrade" runat="server" CssClass="field-value"></asp:Label></td>
                        <td width="20%">
                            <span class="field-label">Shift</span>
                        </td>

                        <td width="30%">
                            <asp:Label ID="lblstudentshift" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Section</span></td>

                        <td>
                            <asp:Label ID="lblstudentsection" runat="server" CssClass="field-value"></asp:Label></td>
                        <td><span class="field-label">Stream</span></td>

                        <td>
                            <asp:Label ID="lblstudentstream" runat="server" CssClass="field-value"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="title-bg-lite">Parent Information
                        </td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Parent Name</span></td>
                        <td width="30%"></td>
                        <td width="30%">
                            <asp:Label ID="lblparentname" runat="server" CssClass="field-value"></asp:Label></td>

                        <td width="20%"></td>

                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Email ID</span></td>
                        <td width="30%"></td>
                        <td width="30%">
                            <asp:Label ID="lblparentemailid" runat="server" CssClass="field-value"></asp:Label></td>

                        <td width="20%"></td>

                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Contact Number</span></td>
                        <td width="30%"></td>
                        <td width="30%">
                            <asp:Label ID="lblparentcontactnumber" runat="server" CssClass="field-value"></asp:Label></td>

                        <td width="20%"></td>

                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg">Incident Details-(<asp:Label ID="lbltype" runat="server" Text=""></asp:Label>
                )</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%" id="tabWitness">
                    <tr>
                        <td colspan="4" class="title-bg-lite">Reporting Informations
                        </td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Reported Date</span></td>
                        <td width="30%"></td>
                        <td width="30%">
                            <asp:Label ID="lbltodaydate" runat="server" CssClass="field-value"></asp:Label></td>

                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Staff Reporting the Incident</span></td>
                        <td width="30%"></td>
                        <td width="30%">
                            <asp:Label ID="lblreportingstaff" runat="server" CssClass="field-value"></asp:Label></td>

                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="20%"><span class="field-label">Incident Date</span></td>
                        <td width="30%"></td>
                        <td width="30%">
                            <asp:Label ID="lblincidentdate" runat="server" CssClass="field-value"></asp:Label></td>

                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <b><u>Report on the incident</u></b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <asp:Label ID="txtReportonincident" runat="server" CssClass="field-label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="title-bg-lite">witnesses spoken to
                        </td>
                    </tr>
                </table>
                <asp:Table ID="tabWitnessDet" runat="server" Width="100%" CssClass="table table-bordered table-row">
                </asp:Table>
            </td>
        </tr>
    </table>
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="title-bg">Action Taken</td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td colspan="4" class="title-bg-lite">Parents Called / Interviewed
                        </td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Parent Called</span></td>

                        <td colspan="2">
                            <asp:RadioButtonList ID="R1" runat="server" RepeatDirection="Horizontal" Enabled="False">
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList></td>

                        <td>
                            <asp:TextBox ID="txtCalldate" runat="server" ReadOnly="True"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <asp:Label ID="txtparentscalledsaid" runat="server" CssClass="field-label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Parent Interviewed</span></td>

                        <td colspan="2">
                            <asp:RadioButtonList ID="R2" runat="server" RepeatDirection="Horizontal" Enabled="False">
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList></td>

                        <td>
                            <asp:TextBox ID="TxtIntvDate" runat="server" ReadOnly="True"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <asp:Label ID="txtparentsinterviewssaid" runat="server" CssClass="field-label"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="title-bg-lite">Other Details
                        </td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Notes in student's planner</span></td>
                        <td>
                            <asp:RadioButtonList ID="R3" runat="server" RepeatDirection="Horizontal" Enabled="False">
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList></td>
                        <td><span class="field-label">Date</span></td>
                        <td>
                            <asp:TextBox ID="T1" runat="server" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Break detention given</span></td>

                        <td>
                            <asp:RadioButtonList ID="R4" runat="server" RepeatDirection="Horizontal" Enabled="False">
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList></td>
                        <td><span class="field-label">Date</span></td>

                        <td>
                            <asp:TextBox ID="T2" runat="server" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="field-label">After school detention given</span></td>

                        <td>
                            <asp:RadioButtonList ID="R5" runat="server" RepeatDirection="Horizontal" Enabled="False">
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList></td>
                        <td><span class="field-label">Date</span></td>

                        <td>
                            <asp:TextBox ID="T3" runat="server" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Suspension</span></td>

                        <td>
                            <asp:RadioButtonList ID="R6" runat="server" RepeatDirection="Horizontal" Enabled="False">
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList></td>
                        <td><span class="field-label">Date</span></td>

                        <td>
                            <asp:TextBox ID="T4" runat="server" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="field-label">Referred to students counsellor</span>
                        </td>

                        <td>
                            <asp:RadioButtonList ID="R7" runat="server" RepeatDirection="Horizontal" Enabled="False">
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList></td>
                        <td><span class="field-label">Date</span></td>

                        <td>
                            <asp:TextBox ID="T5" runat="server" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="title-bg-lite">Any Notes/Followup
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Table ID="tabFollowUp" runat="server" Width="100%">
                            </asp:Table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="Hiddenstuid" runat="server" />
    <asp:HiddenField ID="Hiddenincidentid" runat="server" />
    <asp:HiddenField ID="HidActionID" runat="server" />
</div>
