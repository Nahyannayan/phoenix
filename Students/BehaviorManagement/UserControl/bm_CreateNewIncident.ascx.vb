Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports EmailService
Partial Class Students_BehaviorManagement_UserControl_bm_CreateNewIncident
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            followuptable.Visible = False
            Session("ds1") = Nothing
            Hiddenbsuid.Value = Session("sbsuid") '"125016" '
            HidBMID.Value = "0"
            CreateWitNessWiewTable() 'Create Witness table Structure
            txtincidentdate.Text = Today.Date.ToString("dd/MMM/yyyy")
            lbltodaydate.Text = Today.Date.ToString("dd/MMM/yyyy")
            'binddesig()
            'BidStaff()
            BindMainCategory()
            fillpick()
            BIndInvolvedStudents(HidBMID.Value)
            HiddenFieldEntryId.Value = Nothing
            cmdSave.Enabled = True
            ClearActionSection()
            DisableActionSection()
            radStudent.Checked = True
        End If
    End Sub
    Public Sub fillpick()
        ddlhourpick.Items.Add("-")
        Dim i As Integer

        For i = 0 To 23
            If i >= 0 And i <= 9 Then
                ddlhourpick.Items.Add("0" + i.ToString)
            Else
                ddlhourpick.Items.Add(i.ToString)
            End If
        Next
        ddlminutepick.Items.Add("-")
        Dim j As Integer

        For j = 0 To 59
            If j >= 0 And j <= 9 Then
                ddlminutepick.Items.Add("0" + j.ToString)
            Else
                ddlminutepick.Items.Add(j.ToString)
            End If
        Next
    End Sub

    Public Function CheckPerviousId(ByVal id As String) As Boolean
        Dim returnvalue = False
        For Each row As GridViewRow In GrdView.Rows
            Dim preval As String = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            If preval = id Then
                returnvalue = True
            End If
        Next
        Return returnvalue
    End Function

    Shared dt As DataTable

    Public Sub SearchStudents(ByVal ds As DataSet)
        dt = New DataTable
        dt.Columns.Add("STU_ID") ''
        dt.Columns.Add("STU_NO") ''
        dt.Columns.Add("STU_FIRSTNAME") ''
        '' dt.Columns.Add("STU_LASTNAME") ''
        dt.Columns.Add("grm_display") ''
        dt.Columns.Add("sct_descr") ''
        dt.Columns.Add("parent_name") ''
        dt.Columns.Add("ParentEmail") ''
        dt.Columns.Add("ParentMobile") ''

        For Each row As GridViewRow In GrdView.Rows
            Dim dr As DataRow = dt.NewRow()

            dr.Item("STU_ID") = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            dr.Item("STU_NO") = DirectCast(row.FindControl("lblno"), Label).Text.Trim()
            dr.Item("STU_FIRSTNAME") = DirectCast(row.FindControl("lblstufirst"), Label).Text.Trim()
            ''dr.Item("STU_LASTNAME") = DirectCast(row.FindControl("lblstulast"), Label).Text.Trim()
            dr.Item("grm_display") = DirectCast(row.FindControl("lblstugrade"), Label).Text.Trim()
            dr.Item("sct_descr") = DirectCast(row.FindControl("lblstusection"), Label).Text.Trim()
            dr.Item("parent_name") = DirectCast(row.FindControl("lblstuparentname"), Label).Text.Trim()
            dr.Item("ParentEmail") = DirectCast(row.FindControl("lblstuparentemail"), Label).Text.Trim()
            dr.Item("ParentMobile") = DirectCast(row.FindControl("lblstuparentmobile"), Label).Text.Trim()
            dt.Rows.Add(dr)

        Next

        If ds.Tables(0).Rows.Count > 0 Then
            Dim i = 0
            For i = 0 To ds.Tables(0).Rows.Count - 1
                Dim id As String = ds.Tables(0).Rows(i).Item("STU_ID").ToString()
                If CheckPerviousId(id) Then
                Else
                    Dim dr As DataRow = dt.NewRow()
                    dr.Item("STU_ID") = ds.Tables(0).Rows(i).Item("STU_ID").ToString()
                    dr.Item("STU_NO") = ds.Tables(0).Rows(i).Item("STU_NO").ToString()
                    dr.Item("STU_FIRSTNAME") = ds.Tables(0).Rows(i).Item("STU_FIRSTNAME").ToString()
                    ''dr.Item("STU_LASTNAME") = ds.Tables(0).Rows(i).Item("STU_LASTNAME").ToString()
                    dr.Item("grm_display") = ds.Tables(0).Rows(i).Item("grm_display").ToString()
                    dr.Item("sct_descr") = ds.Tables(0).Rows(i).Item("sct_descr").ToString()
                    dr.Item("parent_name") = ds.Tables(0).Rows(i).Item("parent_name").ToString()
                    dr.Item("ParentEmail") = ds.Tables(0).Rows(i).Item("ParentEmail").ToString()
                    dr.Item("ParentMobile") = ds.Tables(0).Rows(i).Item("ParentMobile").ToString()
                    dt.Rows.Add(dr)
                End If
            Next
        End If

        GrdView.DataSource = dt
        GrdView.DataBind()
        ''Session("ds1") = dt
    End Sub

    Protected Sub GrdView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdView.PageIndexChanging
        GrdView.PageIndex = e.NewPageIndex
        If Session("ds1") Is Nothing Then
        Else
            GrdView.DataSource = Session("ds1")
            GrdView.DataBind()
        End If
    End Sub

    Protected Sub GrdView_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GrdView.RowDeleting
        dt.Rows.Item(e.RowIndex).Delete()
        Session("ds1") = dt
        GrdView.DataSource = Session("ds1")
        GrdView.DataBind()
    End Sub
    Public Function CheckSelected() As Boolean
        Dim returnvalue As Boolean = False
        For Each row As GridViewRow In GrdView.Rows
            Dim check As CheckBox = DirectCast(row.FindControl("ch23"), CheckBox)
            If check.Checked Then
                returnvalue = True
            End If
        Next
        Return returnvalue
    End Function
    Public Function GetCategoryPoints() As Integer
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME,BM_CATEGORY_SCORE FROM BM.BM_CATEGORY WHERE BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYID=" & cmbSubCategory.SelectedValue
            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("BM_CATEGORY_SCORE")), 0, dsCategory.Tables(0).Rows(0).Item("BM_CATEGORY_SCORE"))
            End If
        Catch ex As Exception

        End Try
    End Function
    Public Function GetDesignation(ByVal StaffID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DES_ID,DES_DESCR,DES_FLAG FROM EMPDESIGNATION_M WHERE DES_ID=" & _
             "( SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=" & StaffID & ")"
            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("DES_DESCR")), 0, dsCategory.Tables(0).Rows(0).Item("DES_DESCR"))
            End If
        Catch ex As Exception

        End Try
    End Function
    Public Function GetDesignationID(ByVal StaffID As String) As Integer
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DES_ID,DES_DESCR,DES_FLAG FROM EMPDESIGNATION_M WHERE DES_ID=" & _
             "( SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=" & StaffID & ")"
            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("DES_ID")), 0, dsCategory.Tables(0).Rows(0).Item("DES_ID"))
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Sub BIndInvolvedStudents(ByVal BMID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT STU_ID,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) AS STU_FIRSTNAME FROM STUDENT_M WHERE STU_ID IN " & _
             "(SELECT BM_STU_ID FROM BM.BM_STUDENTSINVOLVED WHERE BM_ID=" & BMID & " AND BM_STU_ID NOT IN (SELECT BM_STU_ID FROM BM.BM_ACTION_MASTER WHERE BM_ID=" & BMID & "))"

            Dim dsStudents As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            cmbStudent.DataSource = dsStudents.Tables(0)
            cmbStudent.DataTextField = "STU_FIRSTNAME"
            cmbStudent.DataValueField = "STU_ID"
            cmbStudent.DataBind() ' 
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BIndFWDDesignations(ByVal FromDesigNo As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim str_query As String = "SELECT DES_ID,DES_DESCR FROM EMPDESIGNATION_M WHERE DES_FLAG='SD' AND DES_DESCR <> '--'"
            'Dim str_query As String = "SELECT DES_ID,DES_DESCR FROM EMPDESIGNATION_M WHERE DES_FLAG='SD' AND DES_DESCR <> '--' " & _
            '                          " AND DES_ID IN (SELECT BM_TO_DESIGID FROM BM.BM_DESIG_ROUTING WHERE BSU_ID=" & Session("sbsuid") & ")"

            Dim str_query As String = "SELECT DES_ID,DES_DESCR FROM EMPDESIGNATION_M WHERE DES_FLAG='SD' AND DES_DESCR <> '--' " & _
                                     " AND DES_ID IN (SELECT BM_TO_DESIGID FROM BM.BM_DESIG_ROUTING WHERE BSU_ID=" & Session("sbsuid") & " AND BM_FROM_DESIGID=( " & _
                                     " SELECT EMP_DES_ID FROM EMPLOYEE_M WHERE EMP_ID=( " & _
                                     " SELECT BM_REPORTING_STAFF_ID FROM BM.BM_MASTER WHERE BM_ID=" & HidBMID.Value & ")))"



            Dim dsStudents As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            cmbFwdTo.DataSource = dsStudents.Tables(0)
            cmbFwdTo.DataTextField = "DES_DESCR"
            cmbFwdTo.DataValueField = "DES_ID"
            cmbFwdTo.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindMainCategory()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID <> 0 ORDER BY BM_CATEGORYID"
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID = 0 ORDER BY BM_CATEGORYID"
            Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            cmbMainCategory.DataSource = dsInitiator.Tables(0)
            cmbMainCategory.DataTextField = "BM_CATEGORYNAME"
            cmbMainCategory.DataValueField = "BM_CATEGORYID"
            cmbMainCategory.DataBind()
            Dim list As New ListItem
            list.Text = "Select Category"
            list.Value = "-1"
            cmbMainCategory.Items.Insert(0, list)

        Catch ex As Exception

        End Try
    End Sub

    Public Sub SaveActionMaster()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(23) As SqlClient.SqlParameter
            If ViewState("DataMode") = "Add" Then
                HiddenFieldEntryId.Value = "0"
            End If

            If HidCategoryPoints.Value = "" Then
                HidCategoryPoints.Value = 0
            End If
            pParms(0) = New SqlClient.SqlParameter("@ACTION_ID", HiddenFieldEntryId.Value)
            pParms(1) = New SqlClient.SqlParameter("@BM_ID", HidBMID.Value)
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", cmbStudent.SelectedValue)
            pParms(3) = New SqlClient.SqlParameter("@CATEGORYID", cmbSubCategory.SelectedValue)
            pParms(4) = New SqlClient.SqlParameter("@PARENT_CALLED", R1.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@PARENT_SAID", txtparentscalledsaid.Text.Trim())
            pParms(6) = New SqlClient.SqlParameter("@PARENT_CALLDATE", txtCalledDate.Text.Trim)
            pParms(7) = New SqlClient.SqlParameter("@PARENT_INTERVIED", R2.SelectedValue)
            pParms(8) = New SqlClient.SqlParameter("@PARENT_INTVSAID", txtparentsinterviewssaid.Text)
            pParms(9) = New SqlClient.SqlParameter("@PARENT_INTERVDATE", txtIntvDate.Text.Trim())
            pParms(10) = New SqlClient.SqlParameter("@NOTES_STUPLANNER", R3.SelectedValue)
            pParms(11) = New SqlClient.SqlParameter("@NOTES_STUPLANDATE", T1.Text.Trim())
            pParms(12) = New SqlClient.SqlParameter("@BREAK_DETENTION", R4.SelectedValue)
            pParms(13) = New SqlClient.SqlParameter("@BREAK_DETENTION_DATE", T2.Text.Trim())
            pParms(14) = New SqlClient.SqlParameter("@AFTER_SCH_DETN", R5.SelectedValue)
            pParms(15) = New SqlClient.SqlParameter("@AFTER_SCH_DETNDATE", T3.Text.Trim())
            pParms(16) = New SqlClient.SqlParameter("@SUSPENSION", R6.SelectedValue)
            pParms(17) = New SqlClient.SqlParameter("@SUSPENSION_DATE", T4.Text.Trim())
            pParms(18) = New SqlClient.SqlParameter("@STU_COUNSELLOR", R6.SelectedValue)
            pParms(19) = New SqlClient.SqlParameter("@STU_COUNSELLOR_DATE", T5.Text.Trim())
            pParms(20) = New SqlClient.SqlParameter("@ENTRY_DATE", lbltodaydate.Text.Trim)
            pParms(21) = New SqlClient.SqlParameter("@SCORE", Convert.ToInt32(HidCategoryPoints.Value))
            pParms(22) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
            HidActionId.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "BM.BM_SaveACTIONMASTER", pParms)
            lblmessage.Text = "Record(s) Inserted"
        Catch ex As Exception
            lblmessage.Text = "Record Is not Inserted"
        End Try
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        'To Save the Action Taken Infor mation aganist Each student.

        ' To Check the Validation in the Action Detail Section. If the Parents Said is Blanks Then 
        If R1.SelectedValue = "Yes" Then
            If txtCalledDate.Text.Trim = "" Then
                lblValidDAte.Text = "Please Select the Parents Called Date."
                Exit Sub
            End If
            If txtparentscalledsaid.Text.Trim = "" Then
                lblValidDAte.Text = "Please Enter what Parents was Said on Call."
                Exit Sub
            End If
        End If

        If R2.SelectedValue = "Yes" Then
            If txtIntvDate.Text.Trim = "" Then
                lblValidDAte.Text = "Please Select the Parents Interviewed Date."
                Exit Sub
            End If
            If txtparentsinterviewssaid.Text.Trim = "" Then
                lblValidDAte.Text = "Please Enter what Parents was Said on Interview."
                Exit Sub
            End If
        End If

        If CheckincidentDateIsValid() = False Then
            lblValidDAte.Text = "The Date can't be lesser than incident date"
            Exit Sub
        End If

        ' The Validations Finished on Action Details  -----------------------------------------------

        If cmbStudent.SelectedIndex >= 0 Then
            If CheckDateIsValid() = True Then
                SaveActionMaster()
                'To Save the Action Fwd Details
                If chkFwd.Checked = True Then
                    If cmbFwdTo.Text <> "" Then
                        hidActionDet.Value = 0
                        SaveActionFwd(Convert.ToInt32(HidActionId.Value))
                        SaveAlert("New Student Incidents Added : " & cmbStudent.SelectedItem.Text)
                    End If
                End If
                lnkinsertnew.Enabled = True
                btnsave.Enabled = False
                ClearActionSection()
                lblValidDAte.Text = ""
                txtComments.Enabled = False
                BIndInvolvedStudents(HidBMID.Value)
                'If cmbStudent.Items.Count >= 1 Then
                '    cmbStudent.Items.Remove(cmbStudent.SelectedItem.Text)
                'Else
                '     cmbStudent.Enabled = False
                '    'Response.Redirect("bm_CreateNewIncident.aspx")
                'End If

            Else
                lblValidDAte.Text = " The above dates Can't be greater than Today's date."
            End If
        Else
            lblmessage.Text = "Please select student(s)"
        End If

    End Sub

    Private Sub SaveActionFwd(ByVal ActioId As Integer)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim pParms(11) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACTIONDET_ID", hidActionDet.Value)
        pParms(1) = New SqlClient.SqlParameter("@BM_ID", HidBMID.Value)
        pParms(2) = New SqlClient.SqlParameter("@ACTION_ID", ActioId)
        pParms(3) = New SqlClient.SqlParameter("@ACTIONDATE", Date.Now())
        ''pParms(4) = New SqlClient.SqlParameter("@STAFF_ID", ddstaff.SelectedValue) 'Session("sUsr_name")
        pParms(4) = New SqlClient.SqlParameter("@STAFF_ID", hfstaff_id.Value)
        pParms(5) = New SqlClient.SqlParameter("@ACTION_DESCR", txtComments.Text)
        pParms(6) = New SqlClient.SqlParameter("@COMMENTS", txtComments.Text)
        pParms(7) = New SqlClient.SqlParameter("@FWDTO", cmbFwdTo.SelectedValue)
        pParms(8) = New SqlClient.SqlParameter("@FWDDATE", Date.Now())
        ''  pParms(9) = New SqlClient.SqlParameter("@BM_DESIGID", GetDesignationID(ddstaff.SelectedValue))
        pParms(9) = New SqlClient.SqlParameter("@BM_DESIGID", GetDesignationID(hfstaff_id.Value))
        pParms(10) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
        hidActionDet.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "BM.BM_saveACTIONDETAILS", pParms)

    End Sub

    Private Sub SaveBehaviourMaster()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim InCidentType As Integer
            If RadioReportType.Items(0).Selected = True Then
                InCidentType = 0 'For Action
            Else
                InCidentType = 1 'For information
            End If

            'Dim pktime As String = Format(TimeSelector1.Hour, "00") & " : " & Format(TimeSelector1.Minute, "00") & " : " & Format(TimeSelector1.Second, "00") & "  " & TimeSelector1.AmPm.ToString
            Dim pktime As String = ddlhourpick.Text & " : " & ddlminutepick.Text
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BMID", HidBMID.Value)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
            pParms(2) = New SqlClient.SqlParameter("@ENTRY_DATE", Format(CDate(lbltodaydate.Text), "dd-MMM-yyyy"))
            pParms(3) = New SqlClient.SqlParameter("@INCIDENT_DATE", Format(CDate(txtincidentdate.Text), "dd-MMM-yyyy"))
            pParms(4) = New SqlClient.SqlParameter("@INCIDENT_TIME", pktime)
            pParms(5) = New SqlClient.SqlParameter("@INCIDENT_TYPE", RadioReportType.SelectedValue)
            ''pParms(5) = New SqlClient.SqlParameter("@STAFF_ID", ddstaff.SelectedValue)
            pParms(6) = New SqlClient.SqlParameter("@STAFF_ID", hfstaff_id.Value)
            pParms(7) = New SqlClient.SqlParameter("@INCIDENT_DESC", txtReportonincident.Text)
            pParms(8) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
            pParms(9) = New SqlClient.SqlParameter("@CATEGORYID", cmbSubCategory.SelectedValue)
            HidBMID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "BM.BM_saveBMMASTER", pParms)
            If HidBMID.Value = "" Then
                HidBMID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, "select max(convert(int,BM_ID)) from BM.BM_MASTER", pParms)
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub SaveWitnessDetails()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim DtWitNess As DataTable
            DtWitNess = Session("WitnessTable")
            For Each DrRow As DataRow In DtWitNess.Rows
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@WITNESS_ID", HidBMID.Value)
                pParms(1) = New SqlClient.SqlParameter("@BM_ID", HidBMID.Value)
                pParms(2) = New SqlClient.SqlParameter("@WITNESSID", DrRow.Item("STUD_ID"))
                pParms(3) = New SqlClient.SqlParameter("@WITNESS_TYPE", DrRow.Item("STU_TYPE"))
                pParms(4) = New SqlClient.SqlParameter("@WITNESS_SAID", DrRow.Item("SPOKEN"))
                pParms(5) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM.BM_SaveINCIDENTWITNESS", pParms)
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Sub SaveStudentsInvolved()
        Dim lintConf
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            For Each DrRow As GridViewRow In GrdView.Rows
                lintConf = 0
                Dim ch1 As CheckBox = DirectCast(DrRow.FindControl("chkConf"), CheckBox)
                If ch1.Checked = True Then
                    lintConf = 1
                Else
                    lintConf = 0
                End If
                Dim pParms(13) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@INVOLVED_ID", HidBMID.Value)
                pParms(1) = New SqlClient.SqlParameter("@BM_ID", HidBMID.Value)
                pParms(2) = New SqlClient.SqlParameter("@STUD_ID", DirectCast(DrRow.FindControl("Hiddenstuid"), HiddenField).Value)
                pParms(3) = New SqlClient.SqlParameter("@DATAMODE", ViewState("DataMode"))
                pParms(4) = New SqlClient.SqlParameter("@Confidential", lintConf)

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM.BM_SaveSTUDENTSINVOLVED", pParms)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveAlert(ByVal AlertDesc As String)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TRA_ID", 4)
            pParms(1) = New SqlClient.SqlParameter("@ALR_DESCR", AlertDesc)
            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            pParms(3) = New SqlClient.SqlParameter("@ALR_ID", 0)
            pParms(4) = New SqlClient.SqlParameter("@USR_ID", ddlStaff.SelectedValue)
            pParms(5) = New SqlClient.SqlParameter("@ALR_ACTIVE", 1)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "saveALERTS_M", pParms)

            'Send A mail to To the emploee whose task assigned
            '--------------------------
            If chkMail.Enabled = True And chkMail.Checked = True Then
                lblmessage.Text = SendPlainText("1", "")
            End If
            '--------------------------
            ' Sending an Alert to The Head of Year of the Grade.

            'FindHeadOfYear
            Dim pParms1(5) As SqlClient.SqlParameter
            pParms1(0) = New SqlClient.SqlParameter("@TRA_ID", 4)
            pParms1(1) = New SqlClient.SqlParameter("@ALR_DESCR", AlertDesc)
            pParms1(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            pParms1(3) = New SqlClient.SqlParameter("@ALR_ID", 0)
            pParms1(4) = New SqlClient.SqlParameter("@USR_ID", FindHeadOfYear())
            pParms1(5) = New SqlClient.SqlParameter("@ALR_ACTIVE", 1)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "saveALERTS_M", pParms1)

        Catch ex As Exception

        End Try
    End Sub

    Public Function SendPlainText(ByVal templateid As String, ByVal email_id As String)
        Dim Retutnvalue As String = ""
        Dim str_query As String
        Dim MailBody As String

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            str_query = "Select * from  COM_MANAGE_EMAIL where EML_ID='" & templateid & "'"
            Dim ds1 As DataSet
            ds1 = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)

            Dim Subject = ds1.Tables(0).Rows(0).Item("EML_SUBJECT").ToString().Trim()
            Dim FromEmailid As String = ds1.Tables(0).Rows(0).Item("EML_FROM").ToString().Trim()
            Dim DisplayName = ds1.Tables(0).Rows(0).Item("EML_DISPLAY").ToString().Trim()
            Dim UserName = ds1.Tables(0).Rows(0).Item("EML_USERNAME").ToString().Trim()
            Dim Password = ds1.Tables(0).Rows(0).Item("EML_PASSWORD").ToString().Trim()
            Dim Host = ds1.Tables(0).Rows(0).Item("EML_HOST").ToString().Trim().Trim()
            Dim Port = Convert.ToInt16(ds1.Tables(0).Rows(0).Item("EML_PORT").ToString().Trim())

            MailBody = "This Is the Test Message"
            MailBody = MailBody.Replace("$", "")

            If isEmail(email_id) Then

                Retutnvalue = email.SendPlainTextEmails(FromEmailid, email_id, Subject, MailBody, UserName, Password, Host, Port, templateid, False)

            Else
                Retutnvalue = "Error: Email Id Not Valid"
            End If

        Catch ex As Exception
            Retutnvalue = "Error : <br> " & ex.Message
        End Try
        Return Retutnvalue
    End Function

    Private Function FindHeadOfYear() As Integer
        Try
            Dim pParms(1) As SqlClient.SqlParameter
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim STRSQL As String = "SELECT HODID,STAFF_ID FROM HEADOFYEAR WHERE GRADEID=(SELECT STU_GRD_ID FROM " & _
                                    " STUDENT_M WHERE STU_ID=" & cmbStudent.SelectedValue & ") AND BSU_ID=" & Session("sbsuid") 'AND ACCYEAR=" & Session("Accyear") & " 

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, STRSQL)
            If ds.Tables(0).Rows.Count >= 1 Then
                Return ds.Tables(0).Rows(0).Item("STAFF_ID")
            Else
                Return 0
            End If
        Catch ex As Exception

        End Try
    End Function

    Private Sub CreateWitNessWiewTable()

        Dim DtWitNess As DataTable
        DtWitNess = New DataTable
        DtWitNess.Columns.Add("STUD_ID")
        DtWitNess.Columns.Add("STU_TYPE")
        DtWitNess.Columns.Add("STUD_NAME")
        DtWitNess.Columns.Add("SPOKEN")
        Session("WitnessTable") = DtWitNess

    End Sub

    Sub PopulateClassTeacher()
        ddlStaff.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query = "SELECT ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(emp_mname,'') + ' ' + ISNULL(EMP_LNAME,'') AS emp_name , EMP_ID FROM EMPLOYEE_M WHERE EMP_STATUS=1 AND EMP_BSU_ID='" & Session("sBsuid") & "' AND EMP_DES_ID = '" & cmbFwdTo.SelectedItem.Value & "' "

        'Dim str_query = "SELECT ISNULL(EMP_FNAME,'')+ ' ' + ISNULL(emp_mname,'') + ' ' + ISNULL(EMP_LNAME,'') AS emp_name , EMP_ID FROM EMPLOYEE_M WHERE EMP_BSU_ID='" & Session("sBsuid") & "' " ' & _
        'Dim str_query As String = "SELECT ISNULL(emp_fname,'')+' '+ISNULL(emp_mname,'')+' '+ISNULL(emp_lname,'') as emp_name,emp_fname,emp_mname,emp_lname," _
        '                         & "emp_id FROM employee_m WHERE emp_ect_id=1 and emp_bsu_id='" + Session("sBsuid") + "' order by emp_fname,emp_mname,emp_lname"

        ''If cmbFwdTo.Items.Count > 0 Then
        ''    str_query += " and emp_des_id=" & cmbFwdTo.SelectedValue & ""

        ''End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query & "order by EMP_FNAME")
        ddlStaff.DataSource = ds
        ddlStaff.DataTextField = "emp_name"
        ddlStaff.DataValueField = "emp_id"
        ddlStaff.DataBind()
        Dim list As New ListItem
        list.Text = "---select---"
        list.Value = "-1"
        ddlStaff.Items.Insert(0, list)
    End Sub

    Public Sub ActionTakendetails(ByVal stu_id As String)

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT * FROM BM.BM_ACTION_MASTER WHERE BM_STU_ID='" & stu_id & "' AND BM_ID='" & HidBMID.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            HidActionId.Value = ds.Tables(0).Rows(0).Item("BM_ACTION_ID").ToString()
            R1.SelectedValue = ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED").ToString()
            R2.SelectedValue = ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED").ToString()
            R3.SelectedValue = ds.Tables(0).Rows(0).Item("BM_NOTES_STU_PLANNER").ToString()
            R4.SelectedValue = ds.Tables(0).Rows(0).Item("BM_BREAK_DETENTION").ToString()
            R5.SelectedValue = ds.Tables(0).Rows(0).Item("BM_AFTER_SCHOOL_DETENTION").ToString()
            R6.SelectedValue = ds.Tables(0).Rows(0).Item("BM_SUSPENSION").ToString()
            R7.SelectedValue = ds.Tables(0).Rows(0).Item("BM_REF_STU_COUNSELLOR").ToString()
            T1.Text = Format(ds.Tables(0).Rows(0).Item("BM_NOTES_STU_PLANNER_DATE"), "dd-MMM-yyyy")
            T2.Text = Format(ds.Tables(0).Rows(0).Item("BM_BREAK_DETENTION_DATE"), "dd-MMM-yyyy")
            T3.Text = Format(ds.Tables(0).Rows(0).Item("BM_AFTER_SCHOOL_DETENTION_DATE"), "dd-MMM-yyyy")
            T4.Text = Format(ds.Tables(0).Rows(0).Item("BM_SUSPENSION_DATE"), "dd-MMM-yyyy")
            T5.Text = Format(ds.Tables(0).Rows(0).Item("BM_REF_STU_COUNSELLOR_DATE"), "dd-MMM-yyyy")
            txtCalledDate.Text = Format(ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED_DATE"), "dd-MMM-yyyy")
            txtIntvDate.Text = Format(ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED_DATE"), "dd-MMM-yyyy")
            txtparentscalledsaid.Text = ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED_SAID").ToString()
            txtparentsinterviewssaid.Text = ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED_SAID").ToString()
            cmbSubCategory.SelectedValue = ds.Tables(0).Rows(0).Item("BM_CATEGORYID").ToString()
            If cmbSubCategory.SelectedIndex >= 0 Then
                lblPoint.Text = "Total Points : " + GetCategoryPoints().ToString()
            End If
            'btnSaveAction.Enabled = True
            If R1.SelectedValue = "No" Then
                txtCalledDate.Text = ""
                txtCalledDate.Enabled = False
                imgCalledDate.Enabled = False
            End If
            If R2.SelectedValue = "No" Then
                txtIntvDate.Text = ""
                txtIntvDate.Enabled = False
                imgIntvDate.Enabled = False
            End If
            If R3.SelectedValue = "No" Then
                T1.Text = ""
                T1.Enabled = False
                ImageButton2.Enabled = False
            End If
            If R4.SelectedValue = "No" Then
                T2.Text = ""
                T2.Enabled = False
                ImageButton3.Enabled = False
            End If
            If R5.SelectedValue = "No" Then
                T3.Text = ""
                T3.Enabled = False
                ImageButton4.Enabled = False
            End If
            If R6.SelectedValue = "No" Then
                T4.Text = ""
                T4.Enabled = False
                ImageButton5.Enabled = False
            End If
            If R7.SelectedValue = "No" Then
                T5.Text = ""
                T5.Enabled = False
                ImageButton6.Enabled = False
            End If
        Else
            'btnSaveAction.Enabled = False
            R1.SelectedValue = "No"
            R2.SelectedValue = "No"
            R3.SelectedValue = "No"
            R4.SelectedValue = "No"
            R5.SelectedValue = "No"
            R6.SelectedValue = "No"
            R7.SelectedValue = "No"
            T1.Text = ""
            T1.Enabled = False
            T2.Text = ""
            T2.Enabled = False
            T3.Text = ""
            T3.Enabled = False
            T4.Text = ""
            T4.Enabled = False
            T5.Text = ""
            T5.Enabled = False
            txtIntvDate.Text = ""
            txtIntvDate.Enabled = False
            txtCalledDate.Text = ""
            txtCalledDate.Enabled = False
            ImageButton6.Enabled = False
            ImageButton5.Enabled = False
            ImageButton4.Enabled = False
            ImageButton3.Enabled = False
            ImageButton2.Enabled = False
            imgIntvDate.Enabled = False
            imgCalledDate.Enabled = False
            txtparentscalledsaid.Text = ""
            txtparentsinterviewssaid.Text = ""
            ''txtnotes.Text = ""
        End If

    End Sub
    Private Function CheckActionExist(ByVal StudId As Integer) As Boolean
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "SELECT * FROM BM.BM_ACTION_MASTER WHERE BM_STU_ID='" & StudId & "' AND BM_ID='" & HidBMID.Value & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try
    End Function
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Try
            'followuptable.Visible = True
            If RadioReportType.SelectedIndex = 0 And cmbSubCategory.SelectedItem.Text.ToUpper = "NEGATIVE BEHAVIOR" Then
                followuptable.Visible = True
            End If

            checknegativebehavior()
            ' To Cheking the students Participated on the Incident
            If GrdView.Rows.Count <= 0 Then
                lblValidIncidentDate.Text = "Please select the students which are participated."
                Exit Sub
            End If
            If CahekDateWithTodate(CDate(txtincidentdate.Text)) = True Then
                ViewState("DataMode") = "Add"
                SaveBehaviourMaster()
                SaveStudentsInvolved()
                SaveWitnessDetails()
                'DisableIncidentMaster()
                'BIndInvolvedStudents(HidBMID.Value)
                Response.Redirect("bm_EditIncident.aspx?inc_id=" & HidBMID.Value)
                'EnableActionSection()
                'DisbaleActionNo()
                'cmdSave.Enabled = False
                'lblValidIncidentDate.Text = ""
                'bindincidentdate(HidBMID.Value)
            Else
                lblValidIncidentDate.Text = " Incident date Can't be greater than Today's date."
            End If

        Catch ex As Exception

        End Try
    End Sub
    Sub bindincidentdate(ByVal bmid As Integer)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query = "select BM_INCIDENT_DATE FROM BM.BM_MASTER WHERE BM_ID=" & bmid & ""
        Dim incident_date As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        hf_incidentdate.Value = incident_date.ToString()
    End Sub

    Protected Sub lnkinsertnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkinsertnew.Click
        Response.Redirect("bm_CreateNewIncident.aspx")
    End Sub

    Public Sub checknegativebehavior()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(*) FROM BM.BM_CATEGORY WHERE BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYHRID=3 AND BM_CATEGORYID=" & cmbSubCategory.SelectedValue
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'For I As Integer = 0 To dsInitiator.Tables(0).Rows.Count - 1
        '    If cmbSubCategory.SelectedValue = dsInitiator.Tables(0).Rows(I)("") Then
        '        FLAG = True
        '    End If
        'Next
        If sbm = 0 Then
            hidingrow1.Visible = False
            hidingrow2.Visible = False
            hidingrow3.Visible = False
        Else
            hidingrow1.Visible = True
            hidingrow2.Visible = True
            hidingrow3.Visible = True
        End If
    End Sub

    Protected Sub cmbSubCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubCategory.SelectedIndexChanged
        Try
            If cmbSubCategory.SelectedIndex >= 0 Then
                HidCategoryPoints.Value = GetCategoryPoints().ToString
                lblPoint.Text = "Total Points : " + HidCategoryPoints.Value
                HiDCategory.Value = GetCategoryPoints()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub radStaff_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStaff.CheckedChanged
        If radStaff.Checked = True Then
            'btnEmp_Name.Enabled = True
            'imgbtnSibling.Enabled = False
            txtPar_Sib.Text = ""
        Else
            'btnEmp_Name.Enabled = False
            'imgbtnSibling.Enabled = True
        End If
    End Sub

    Protected Sub radStudent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStudent.CheckedChanged
        If radStudent.Checked = True Then
            'imgbtnSibling.Enabled = True
            'btnEmp_Name.Enabled = False
            txtPar_Sib.Text = ""
        Else
            'imgbtnSibling.Enabled = False
            'btnEmp_Name.Enabled = True
        End If
    End Sub

    'Add Witness Details To The Grid.
    Protected Sub Add_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Add.Click
        Dim strWitNessType As String
        Dim strStuID As String

        If txtPar_Sib.Text.Trim <> "" Then
            Dim DtWitness As DataTable
            DtWitness = Session("WitnessTable")
            Dim DrWitness As DataRow = DtWitness.NewRow()

            If radStaff.Checked = True Then
                strWitNessType = "STF"
                strStuID = hfEmp_ID.Value
                DrWitness.Item("STUD_NAME") = GetSaffName(strStuID) ' txtPar_Sib.Text
            Else
                strWitNessType = "STU"
                strStuID = h_SliblingID.Value
                DrWitness.Item("STUD_NAME") = GetStudentName(strStuID) ' txtPar_Sib.Text
            End If

            DrWitness.Item("STUD_ID") = strStuID
            DrWitness.Item("STU_TYPE") = strWitNessType
            'DrWitness.Item("STUD_NAME") = GetStudentName(strStuID) ' txtPar_Sib.Text
            DrWitness.Item("SPOKEN") = txtwithnesssaid.Text
            DtWitness.Rows.Add(DrWitness)
            Session("WitnessTable") = DtWitness

            gvWitness.DataSource = DtWitness
            gvWitness.DataBind()

            txtwithnesssaid.Text = ""
            txtPar_Sib.Text = ""
            lblCaption.Text = ""
        End If
    End Sub

    Private Function GetSaffName(ByVal StaffID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT EMPNO,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_MNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EmpName FROM EMPLOYEE_M WHERE EMP_ID=" & StaffID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("EmpName")
            Else
                Return ""
            End If

        Catch ex As Exception
        End Try
    End Function
    Private Function GetSaffID(ByVal StaffID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT EMP_ID As ID FROM EMPLOYEE_M WHERE EMPNO='" & StaffID & "'"
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("ID")
            Else
                Return ""
            End If

        Catch ex As Exception
        End Try
    End Function

    Private Function GetStudentName(ByVal StudID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT STU_NO,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) As StudentName FROM STUDENT_M WHERE STU_ID=" & StudID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("StudentName")
            Else
                Return ""
            End If
        Catch ex As Exception
        End Try
    End Function

    Private Function GetStudentNameByNumber(ByVal StudID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT STU_NO,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) As StudentName FROM STUDENT_M WHERE STU_NO=" & StudID
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("StudentName")
            Else
                Return ""
            End If
        Catch ex As Exception
        End Try
    End Function

    Protected Sub gvWitness_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvWitness.RowDeleting
        Try
            Dim DtWitness As DataTable
            DtWitness = Session("WitnessTable")

            DtWitness.Rows.Item(e.RowIndex).Delete()
            Session("WitnessTable") = DtWitness
            gvWitness.DataSource = DtWitness
            gvWitness.DataBind()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub EnableIncidentMaster()
        RadioReportType.Enabled = True
        '' ddstaff.Enabled = True
        imgbtnstaff.Enabled = True
        txtincidentdate.Enabled = True
        ImageButton1.Enabled = True
        cmbSubCategory.Enabled = True
        txtReportonincident.Enabled = True
        radStaff.Enabled = True
        radStudent.Enabled = True
        txtPar_Sib.Enabled = True

        imgbtnSibling.Enabled = True
        Add.Enabled = True
        txtwithnesssaid.Enabled = True
        gvWitness.Enabled = True
        GrdView.Enabled = True
    End Sub

    Private Sub DisableIncidentMaster()
        RadioReportType.Enabled = False
        ''ddstaff.Enabled = False
        imgbtnstaff.Enabled = False
        cmbSubCategory.Enabled = False
        txtincidentdate.Enabled = False
        ImageButton1.Enabled = False
        txtReportonincident.Enabled = False
        radStaff.Enabled = False
        radStudent.Enabled = False
        txtPar_Sib.Enabled = False

        imgbtnSibling.Enabled = False
        Add.Enabled = False
        txtincidentdate.Enabled = False
        txtwithnesssaid.Enabled = False
        gvWitness.Enabled = False
        GrdView.Enabled = False

    End Sub
    '------------------ The Below Part is Exclusively for Behavieour Action Session -------------------------------------
    Private Sub EnableActionSection()
        cmbStudent.Enabled = True
        R1.Enabled = True
        txtparentscalledsaid.Enabled = True
        ReqPatentsCalled.Enabled = True
        R2.Enabled = True
        txtparentsinterviewssaid.Enabled = True
        R3.Enabled = True
        R4.Enabled = True
        R5.Enabled = True
        R6.Enabled = True
        R7.Enabled = True
        T1.Enabled = True
        T2.Enabled = True
        T3.Enabled = True
        T4.Enabled = True
        T5.Enabled = True
        'ImageButton1.Enabled = True
        ImageButton2.Enabled = True
        ImageButton3.Enabled = True
        ImageButton4.Enabled = True
        ImageButton5.Enabled = True
        ImageButton6.Enabled = True
        chkFwd.Enabled = True
        chkMail.Enabled = True
        cmbFwdTo.Enabled = False
        btnsave.Enabled = True
        txtCalledDate.Enabled = True
        imgCalledDate.Enabled = True
        imgIntvDate.Enabled = True
    End Sub

    Private Sub DisableActionSection()
        cmbStudent.Enabled = False
        R1.Enabled = False
        'cmbSubCategory.Enabled = False
        txtparentscalledsaid.Enabled = False
        'ReqPatentsCalled.Enabled = False
        R2.Enabled = False
        txtparentsinterviewssaid.Enabled = False
        R3.Enabled = False
        R4.Enabled = False
        R5.Enabled = False
        R6.Enabled = False
        R7.Enabled = False
        T1.Enabled = False
        T2.Enabled = False
        T3.Enabled = False
        T4.Enabled = False
        T5.Enabled = False
        'ImageButton1.Enabled = False
        ImageButton2.Enabled = False
        ImageButton3.Enabled = False
        ImageButton4.Enabled = False
        ImageButton5.Enabled = False
        ImageButton6.Enabled = False
        chkFwd.Enabled = False
        chkMail.Enabled = False
        cmbFwdTo.Enabled = False
        btnsave.Enabled = False
        txtCalledDate.Enabled = False
        txtComments.Enabled = False
        txtIntvDate.Enabled = False
        imgCalledDate.Enabled = False
        imgIntvDate.Enabled = False
    End Sub
    Private Sub ClearActionSection()
        cmbStudent.SelectedIndex = -1
        cmbSubCategory.SelectedIndex = -1
        txtparentscalledsaid.Text = ""
        txtparentsinterviewssaid.Text = ""
        txtCalledDate.Text = ""
        txtIntvDate.Text = ""
        txtComments.Text = ""
        cmbFwdTo.Items.Clear()
        ddlStaff.Items.Clear()
        cmbFwdTo.Enabled = False
        ddlStaff.Enabled = False
        txtComments.Enabled = False

        T1.Text = ""
        T2.Text = ""
        T3.Text = ""
        T4.Text = ""
        T5.Text = ""
        txtComments.Text = ""
        chkFwd.Checked = False
        chkMail.Enabled = False
        R1.Enabled = True
        R2.Enabled = True
        R3.Enabled = True
        R4.Enabled = True
        R5.Enabled = True
        R6.Enabled = True
        R7.Enabled = True

        R1.SelectedValue = "No"
        R2.SelectedValue = "No"
        R3.SelectedValue = "No"
        R4.SelectedValue = "No"
        R5.SelectedValue = "No"
        R6.SelectedValue = "No"
        R7.SelectedValue = "No"

        T1.Enabled = False
        T2.Enabled = False
        T3.Enabled = False
        T4.Enabled = False
        T5.Enabled = False
        ImageButton2.Enabled = False
        ImageButton3.Enabled = False
        ImageButton4.Enabled = False
        ImageButton5.Enabled = False
        ImageButton6.Enabled = False
        btnsave.Enabled = True
        txtCalledDate.Enabled = False
        txtComments.Enabled = True
        imgCalledDate.Enabled = False
        imgIntvDate.Enabled = False
    End Sub

    Private Sub DisbaleActionNo()
        T1.Enabled = False
        T2.Enabled = False
        T3.Enabled = False
        T4.Enabled = False
        T5.Enabled = False
        txtCalledDate.Enabled = False
        txtIntvDate.Enabled = False
        txtparentscalledsaid.Enabled = False
        ' ReqPatentsCalled.Enabled = False
        txtparentsinterviewssaid.Enabled = False

        ImageButton2.Enabled = False
        ImageButton3.Enabled = False
        ImageButton4.Enabled = False
        ImageButton5.Enabled = False
        ImageButton6.Enabled = False
        imgCalledDate.Enabled = False
        imgIntvDate.Enabled = False
    End Sub

    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Trim = "" Then
            Return (False)
        Else
            Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            Dim re As New Regex(strRegex)
            If re.IsMatch(inputEmail) Then
                Return (True)
            Else
                Return (False)
            End If
        End If
    End Function
    Private Function CheckincidentDateIsValid() As Boolean
        Try
            If R1.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(txtCalledDate.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R2.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(txtIntvDate.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R3.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T1.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R4.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T2.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R5.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T3.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R6.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T4.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R7.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(hf_incidentdate.Value), CDate(T5.Text)) < 0 Then
                    Return False
                    Exit Function
                End If
            End If
            Return True
        Catch ex As Exception

        End Try
    End Function
    Private Function CheckDateIsValid() As Boolean
        Try
            If R1.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(txtCalledDate.Text), Date.Now()) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            If R2.SelectedValue = "Yes" Then
                If DateDiff(DateInterval.Day, CDate(txtIntvDate.Text), Date.Now()) < 0 Then
                    Return False
                    Exit Function
                End If
            End If

            Return True
        Catch ex As Exception

        End Try
    End Function
    Private Function CahekDateWithTodate(ByVal CheckDate As Date) As Boolean
        Try
            If DateDiff(DateInterval.Day, CheckDate, Date.Now()) < 0 Then
                Return False
                Exit Function
            End If
            Return True
        Catch ex As Exception
        End Try
    End Function

    Public Sub Send(ByVal ds As DataSet)
        Dim MailBody As String = ""
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim serverpath As String = WebConfigurationManager.AppSettings("EmailAttachments").ToString()
            Dim str_query = "Select * FROM COM_MANAGE_EMAIL"

            Dim ds1 As DataSet
            ds1 = SqlHelper.ExecuteDataset(str_conn.ToString, CommandType.Text, str_query)

            If ds1.Tables(0).Rows.Count >= 1 Then
                Dim Subject = ds1.Tables(0).Rows(0).Item("EML_SUBJECT").ToString().Trim()
                Dim FromEmailid As String = ds1.Tables(0).Rows(0).Item("EML_FROM").ToString().Trim()
                Dim DisplayName = ds1.Tables(0).Rows(0).Item("EML_DISPLAY").ToString().Trim()
                Dim UserName = ds1.Tables(0).Rows(0).Item("EML_USERNAME").ToString().Trim()
                Dim Password = ds1.Tables(0).Rows(0).Item("EML_PASSWORD").ToString().Trim()
                Dim Host = ds1.Tables(0).Rows(0).Item("EML_HOST").ToString().Trim().Trim()
                Dim Port = Convert.ToInt16(ds1.Tables(0).Rows(0).Item("EML_PORT").ToString().Trim())
                Dim Hasattachments As Boolean = ds1.Tables(0).Rows(0).Item("EML_ATTACHMENT")

                If ds.Tables(0).Rows.Count > 0 Then '' If contain data 
                    Dim i
                    Dim rcount = 0
                    For i = Session("count") To ds.Tables(0).Rows.Count - 1
                        Dim email_id As New StringBuilder
                        email_id.Append(ds.Tables(0).Rows(i).Item("Parentemail").ToString())
                        Dim emailcheck As String = email_id.ToString()
                        Session("count") = i + 1
                        rcount = rcount + 1

                        If isEmail(emailcheck) Then
                            lblmessage.Text = email.SendPlainTextEmails(FromEmailid, emailcheck, Subject, MailBody, UserName, Password, Host, Port, 0, Hasattachments)
                            If lblmessage.Text.IndexOf("Error") > -1 Then
                                'Send Failed
                            Else
                                'Send Succedded
                            End If
                        Else
                            'Invalid Email Id
                        End If
                    Next

                End If
            End If
        Catch ex As Exception
            lblmessage.Text = "Error : <br>" & ex.Message ''& "<br> or <br> Unable to connect to the remote server.Please check network connectivity."
        End Try
    End Sub

    Protected Sub chkFwd_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFwd.CheckedChanged
        If chkFwd.Checked = False Then
            cmbFwdTo.Enabled = False
            ddlStaff.Items.Clear()
            ddlStaff.Enabled = False
            txtComments.Enabled = False
        Else
            BIndFWDDesignations(GetDesignationID(hfstaff_id.Value))
            PopulateClassTeacher()
            'BIndFWDDesignations(GetDesignationID(ddstaff.SelectedValue))

            cmbFwdTo.Enabled = True
            ddlStaff.Enabled = True
            txtComments.Enabled = True
        End If
    End Sub

    Protected Sub R3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R3.SelectedIndexChanged
        If R3.SelectedValue = "No" Then
            T1.Enabled = False
            ImageButton2.Enabled = False
        Else
            T1.Enabled = True
            ImageButton2.Enabled = True
        End If

    End Sub

    Protected Sub R4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R4.SelectedIndexChanged
        If R4.SelectedValue = "No" Then
            T2.Enabled = False
            ImageButton3.Enabled = False
        Else
            T2.Enabled = True
            ImageButton3.Enabled = True
        End If
    End Sub

    Protected Sub R5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R5.SelectedIndexChanged
        If R5.SelectedValue = "No" Then
            T3.Enabled = False
            ImageButton4.Enabled = False
        Else
            T3.Enabled = True
            ImageButton4.Enabled = True
        End If
    End Sub

    Protected Sub R6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R6.SelectedIndexChanged
        If R6.SelectedValue = "No" Then
            T4.Enabled = False
            ImageButton5.Enabled = False
        Else
            T4.Enabled = True
            ImageButton5.Enabled = True
        End If
    End Sub

    Protected Sub R7_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R7.SelectedIndexChanged
        If R7.SelectedValue = "No" Then
            T5.Enabled = False
            ImageButton6.Enabled = False
        Else
            T5.Enabled = True
            ImageButton6.Enabled = True
        End If
    End Sub

    Protected Sub R1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R1.SelectedIndexChanged
        If R1.SelectedValue = "No" Then
            txtCalledDate.Enabled = False
            imgCalledDate.Enabled = False
            txtparentscalledsaid.Enabled = False
            'ReqPatentsCalled.Enabled = False
        Else
            txtCalledDate.Enabled = True
            imgCalledDate.Enabled = True
            txtparentscalledsaid.Enabled = True
            'ReqPatentsCalled.Enabled = True
        End If
    End Sub

    Protected Sub R2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles R2.SelectedIndexChanged
        If R2.SelectedValue = "No" Then
            txtIntvDate.Enabled = False
            imgIntvDate.Enabled = False
            txtparentsinterviewssaid.Enabled = False
        Else
            txtparentsinterviewssaid.Enabled = True
            txtIntvDate.Enabled = True
            imgIntvDate.Enabled = True
        End If
    End Sub

    Protected Sub cmbStudent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbStudent.SelectedIndexChanged
        Try
            If cmbStudent.SelectedValue <> "" Then
                If CheckActionExist(cmbStudent.SelectedValue) = True Then
                    ActionTakendetails(cmbStudent.SelectedValue)
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub imgbtnSibling_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnSibling.Click
        Try
            If (radStudent.Checked = True) Then
                lblCaption.Text = GetStudentNameByNumber(txtPar_Sib.Text)
            Else
                lblCaption.Text = ""
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgbtnstaff_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnstaff.Click
        Try
            If hfstaff_id.Value <> "" Then

                lblDesignation.Text = GetDesignation(hfstaff_id.Value)
                BIndFWDDesignations(GetDesignationID(hfstaff_id.Value))
            End If
        Catch ex As Exception

        End Try
    End Sub



    Protected Sub cmbMainCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMainCategory.SelectedIndexChanged
        Try
            hfstaff_id.Value = GetSaffID(hfstaff_id.Value)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE  BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYHRID=" & cmbMainCategory.SelectedValue & "ORDER BY BM_CATEGORYID"
            Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            cmbSubCategory.DataSource = dsInitiator.Tables(0)
            cmbSubCategory.DataTextField = "BM_CATEGORYNAME"
            cmbSubCategory.DataValueField = "BM_CATEGORYID"
            cmbSubCategory.DataBind()
            cmbSubCategory.SelectedIndex = 0
            'Dim list As New ListItem
            'list.Text = "Select Category"
            'list.Value = "-1"
            'cmbSubCategory.Items.Insert(0, list)

        Catch ex As Exception

        End Try
    End Sub


End Class
