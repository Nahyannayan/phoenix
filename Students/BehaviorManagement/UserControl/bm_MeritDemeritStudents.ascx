﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_MeritDemeritStudents.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_MeritDemeritStudents" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table>
    <tr>
        <td align="left">
            <asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left" width="20%"><span class="field-label">Select Student</span></td>
        <td align="left" width="40%">
            <asp:TextBox ID="txtStudent" runat="server" AutoPostBack="true" placeholder="Type in or Search Student Id or Name"></asp:TextBox>
            <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/forum_search.gif"
                OnClientClick="GetStudent(); return false;" />
            <asp:LinkButton ID="lbtnClearStudent" runat="server" CausesValidation="False" Visible="false">Clear</asp:LinkButton>
            <asp:LinkButton ID="lbtnLoadStuDetail" runat="server" CausesValidation="False" OnClick="lbtnLoadStuDetail_Click"></asp:LinkButton>
            <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TBWE3" runat="server"
                                TargetControlID="txtStudent"
                                WatermarkText="Type in or Search Student Id or Name"
                                WatermarkCssClass="watermarked" />--%>
            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                TargetControlID="lbtnClearStudent"
                ConfirmText="Are you sure you want to remove the Student selection?" />
            <asp:HiddenField ID="h_STU_ID" runat="server" OnValueChanged="h_STU_ID_ValueChanged" />
        </td>
    </tr>

</table>
<asp:LinkButton ID="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;" CssClass="text-bold"></asp:LinkButton>
<%--<asp:Panel ID="Panel1" runat="server" >--%>
<asp:UpdatePanel ID="up3" runat="server">
    <ContentTemplate>
        <asp:GridView ID="GrdView" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="100%"
            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords.">
            <Columns>

                <asp:TemplateField HeaderText="Check">
                    <HeaderTemplate>
                        All<br />
                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);" ToolTip="Click here to select/deselect all rows" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="ch2" runat="server" Text="" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ID" HeaderText="SlNo" />
                <asp:TemplateField HeaderText="Student Id">
                    <ItemTemplate>
                        <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%#Eval("STU_ID")%>' />
                        <asp:Label ID="lblno" runat="server" Text='<%# Eval("STU_NO") %>'></asp:Label>

                    </ItemTemplate>
                    <HeaderTemplate>

                        <asp:Label ID="lblFeeIDH" runat="server" EnableViewState="False" Text="Student No"></asp:Label><br />
                        <asp:TextBox ID="txtFeeId" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="btnSearchFeeID" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                            OnClick="btnSearchFeeID_Click" CausesValidation="False" />
                    </HeaderTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name">
                    <ItemTemplate>
                        <asp:Label ID="lblstufirst" runat="server" Text='<%# Eval("StudName") %>'></asp:Label>&nbsp;
                    
                    </ItemTemplate>
                    <HeaderTemplate>

                        <asp:Label ID="lblStudNameH" runat="server" Text="Student Name"></asp:Label><br />
                        <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="btnSearchStudName" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                            OnClick="btnSearchStudName_Click" CausesValidation="False" />
                    </HeaderTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Grade">
                    <ItemTemplate>
                        <asp:Label ID="lblstugrade" runat="server" Text='<%#Eval("grm_display")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Section">
                    <ItemTemplate>
                        <asp:Label ID="lblstusection" runat="server" Text='<%#Eval("sct_descr")%>'></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="House">
                    <ItemTemplate>
                        <asp:Label ID="lblHouse" runat="server" Text='<%#Eval("house_description")%>'></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Parent Name">
                    <ItemTemplate>
                        <asp:Label ID="lblstuparentname" runat="server" Text='<%#Eval("parent_name")%>'></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label ID="lblstuparentemail" runat="server" Text='<%#Eval("ParentEmail")%>'></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mobile">
                    <ItemTemplate>
                        <asp:Label ID="lblstuparentmobile" runat="server" Text='<%#Eval("ParentMobile")%>'></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle />
            <RowStyle CssClass="griditem" />
            <SelectedRowStyle />
            <AlternatingRowStyle CssClass="griditem_alternative" />
            <EmptyDataRowStyle />
            <EditRowStyle />
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
<%--   </asp:Panel>--%>
<asp:Button ID="btnaddstu" runat="server" Text="Add" CssClass="button" CausesValidation="False" />
<%--<telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
    ReloadOnShow="true" runat="server" EnableShadow="true">
    <Windows>
        <telerik:RadWindow ID="pop_up" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
            OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
        </telerik:RadWindow>
    </Windows>

</telerik:RadWindowManager>--%>
<%--<ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="false" CollapseControlID="LinkAdvanceSearch"
        Collapsed="false"  CollapsedSize="0" CollapsedText="" ExpandControlID="LinkAdvanceSearch"
         ExpandedText="" ScrollContents="false" TargetControlID="Panel1"
        TextLabelID="LinkAdvanceSearch">
    </ajaxToolkit:CollapsiblePanelExtender>--%>
<script type="text/javascript" lang="javascript">
    $(document).ready(function () {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(InitializeRequest);
        prm.add_endRequest(EndRequest);
        InitStudentAutoComplete();

    });
    function InitializeRequest(sender, args) {
    }
    function EndRequest(sender, args) {
        // after update occur on UpdatePanel re-init the Autocomplete
        InitStudentAutoComplete();

    }
    function InitStudentAutoComplete() {

        $("#<%=txtStudent.ClientID%>").autocomplete({
             source: function (request, response) {
                 $.ajax({
                     url: "../../GlobalSearchService.asmx/GetStudent",
                     data: "{ 'BSUID': '" + GetBsuId() + "','STU_TYPE':'" + GetStuType() + "','prefix': '" + request.term + "','COMPID':'" + GetCompanyId() + "','STUBSUID':'" + GetBsuId() + "'}",
                     dataType: "json",
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     success: function (data) {
                         response($.map(data.d, function (item) {
                             return {
                                 label: item.split('$$')[1] + ' - ' + item.split('$$')[0],
                                 val: item.split('$$')[2]
                             }
                         }))
                     },
                     error: function (response) {
                         alert(response.responseText);
                     },
                     failure: function (response) {
                         alert(response.responseText);
                     }
                 });
             },
             select: function (e, i) {
                 $("#<%=h_STU_ID.ClientID%>").val(i.item.val);
                   $("#<%=txtStudent.ClientID%>").val('');
                   document.getElementById('<%=lbtnLoadStuDetail.ClientID%>').click();
               },
             minLength: 1
         });
       }

       function GetCompanyId() {

           var COMP_ID = "-1";

           return COMP_ID;
       }

       function GetBsuId() {
           //var dropFind = $find("<%= Session("sBsuId")%>");
                        //var valueFind = dropFind.get_value();
                        return '<%= Session("sBsuId")%>';
                    }
                    function GetCountryId() {

                        return '<%= Session("BSU_COUNTRY_ID")%>';
                    }
                    function GetStuType() {
                        var STU_TYPE = "S";

                        return STU_TYPE;
                    }

                    $(function () {

                        $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                            item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong style='background:yellow;'>$1</strong>");
                            return $("<li></li>")
                                    .data("item.autocomplete", item)
                                    .append("<a>" + item.label + "</a>")
                                    .appendTo(ul);
                        };
                    });


                    function GetStudent() {

                        var NameandCode;
                        var result;


                        var url = "";

                        url = "bm_MeritDemeritShowStudents.aspx";

                        result = radopen(url, "pop_up");
                        <%--if (result != '' && result != undefined) {
                                        NameandCode = result.split('||');
                                        if (NameandCode.length > 1) {
                                            document.getElementById('<%=h_STU_ID.ClientID%>').value = result;
                                            document.getElementById('<%=txtStudent.ClientID%>').value = "Multiple students selected";
                                        }
                                        else if (NameandCode.length = 1) {
                                            document.getElementById('<%=h_STU_ID.ClientID%>').value = NameandCode[0];
                                    }
                                return true;--%>

                        //else {
                        //    return false;
                        //}
                    }

    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;
        var height = body.scrollHeight;
        var width = body.scrollWidth;
        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;
        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }

    function OnClientClose(oWnd, args) {
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg) {
            NameandCode = arg.NameandCode.split('||');
            if (NameandCode.length > 1) {
              
                document.getElementById('<%=h_STU_ID.ClientID%>').value = arg.NameandCode;
                 document.getElementById('<%=txtStudent.ClientID%>').value = "Multiple Students Selected";
             }
             else if (NameandCode.length = 1) {
                 document.getElementById('<%=h_STU_ID.ClientID%>').value = NameandCode[0];
                            }
                        __doPostBack('<%= h_STU_ID.ClientID%>', 'ValueChanged');
         }
     }

</script>
