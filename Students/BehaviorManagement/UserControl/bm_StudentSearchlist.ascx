<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_StudentSearchlist.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_StudentSearchlist" %>
<%--<link href="../../../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script language="javascript" type="text/javascript" src="../../../chromejs/chrome.js"></script>
       <script  language="javascript" type="text/javascript">

 function change_chk_stateg(chkThis)
             {
            var chk_state= ! chkThis.checked ;
             for(i=0; i<document.forms[0].elements.length; i++)
                   {
                   var currentid =document.forms[0].elements[i].id; 
                   if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("ch2")!=-1)
                 {
                   //if (document.forms[0].elements[i].type=='checkbox' )
                      //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                        document.forms[0].elements[i].checked=chk_state;
                         document.forms[0].elements[i].click();//fire the click event of the child element
                     }
                  }
              }

   function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../../../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../../../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../../../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../../../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../../../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../../../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_Selected_menu_1.ClientID %>").value=val+'__'+path;
                }
                
      function test2(val)
                {
                var path;
                if (val=='LI')
                {
                path='../../../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../../../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../../../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {    
                path='../../../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../../../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../../../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid2()%>").src = path;
                document.getElementById("<%=h_Selected_menu_2.ClientID %>").value=val+'__'+path;
                }
               
       </script>
<div >

   
    <asp:LinkButton ID="LinkAdvanceSearch" runat="server" OnClientClick="javascript:return false;" CssClass="text-bold">Add Students</asp:LinkButton>

    <asp:Panel ID="Panel1" runat="server" >
 
<table width="100%">
            <%--<tr>
                <td >
                    Student Number</td>
                <td >
                    :</td>
                <td colspan="8">
                    <asp:TextBox ID="txtstudentsearchnumber" runat="server"></asp:TextBox></td>
            </tr>
    <tr>
        <td >
            Student Name</td>
        <td >
        </td>
        <td colspan="8">
            <asp:TextBox ID="txtstudentsearchname" runat="server"></asp:TextBox></td>
    </tr>--%>
    <tr>
        <td width="10%">
                  <span class="field-label">  Curriculum</span></td>
     
        <td width="20%">
                    <asp:DropDownList ID="ddclm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddclm_SelectedIndexChanged">
                    </asp:DropDownList></td>
        <td width="10%">
                    <span class="field-label">Shift</span></td>
    
        <td width="25%">
                    <asp:DropDownList ID="ddshift" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
        <td width="10%">
                    <span class="field-label">Stream</span></td>
       
        <td width="25%">
                    <asp:DropDownList ID="ddstream" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddstream_SelectedIndexChanged">
                    </asp:DropDownList></td>
    </tr>
            <tr>
                <td  >
                    <span class="field-label">Grade</span></td>
           
                <td >
                    <asp:DropDownList ID="ddgrade" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td  >
                    <span class="field-label">Section</span></td>
         
                <td >
                    <asp:DropDownList ID="ddsection" runat="server">
                    </asp:DropDownList></td>
                <td >
                    &nbsp;</td>
       
                <td >
                    </td>
            </tr>
    <tr>
        <td colspan="6" align="center">
            <asp:Button ID="btnsearch" CssClass="button" runat="server" Text="Search" CausesValidation="False" /></td>
    </tr>
</table>

    <asp:GridView ID="GrdView" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="100%" 
        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords.">
        <Columns>
            <asp:TemplateField HeaderText="Check">
                <HeaderTemplate>
                    All<br />
                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg(this);" ToolTip="Click here to select/deselect all rows" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="ch2" runat="server" Text="" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Student Id">
                <ItemTemplate>
                    <asp:HiddenField ID="Hiddenstuid" runat="server" Value='<%#Eval("STU_ID")%>' />
                    <asp:Label ID="lblno" runat="server" Text='<%# Eval("STU_NO") %>'></asp:Label>
                  
                </ItemTemplate>
                <HeaderTemplate>
                  
                                <asp:Label ID="lblFeeIDH" runat="server"  EnableViewState="False" Text="Student No"></asp:Label><br />
                                            <asp:TextBox ID="txtFeeId" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchFeeID" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchFeeID_Click" CausesValidation="False" />
                </HeaderTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                 <asp:Label ID="lblstufirst" runat="server" Text='<%# Eval("StudName") %>'></asp:Label>&nbsp;
                    
                </ItemTemplate>
                <HeaderTemplate>
                    
                                <asp:Label ID="lblStudNameH" runat="server" Text="Student Name" ></asp:Label><br />
                                            <asp:TextBox ID="txtStudName" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchStudName" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchStudName_Click" CausesValidation="False" />
                </HeaderTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Grade">
            <ItemTemplate>
                     <asp:Label ID="lblstugrade" runat="server" Text='<%#Eval("grm_display")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Section">
            <ItemTemplate>
            <asp:Label ID="lblstusection" runat="server" Text='<%#Eval("sct_descr")%>'></asp:Label>
                   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Parent Name">
                <ItemTemplate>
                <asp:Label ID="lblstuparentname" runat="server" Text='<%#Eval("parent_name")%>'></asp:Label>
                   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <ItemTemplate>
                 <asp:Label ID="lblstuparentemail" runat="server" Text='<%#Eval("ParentEmail")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mobile">
                <ItemTemplate>
                <asp:Label ID="lblstuparentmobile" runat="server" Text='<%#Eval("ParentMobile")%>'></asp:Label>
                    
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <HeaderStyle  />
        <RowStyle CssClass="griditem"  />
        <SelectedRowStyle  />
        <AlternatingRowStyle CssClass="griditem_alternative"  />
        <EmptyDataRowStyle  />
        <EditRowStyle  />
    </asp:GridView>
            <asp:Button ID="btnaddstu" runat="server"  Text="Add" CssClass="button" CausesValidation="False" /></asp:Panel>
<asp:HiddenField ID="Hiddenacyid" runat="server" />
<asp:HiddenField ID="Hiddenacdid" runat="server" />
<asp:HiddenField ID="Hiddenbsu" runat="server" />
    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        AutoCollapse="False" AutoExpand="False" CollapseControlID="LinkAdvanceSearch"
        Collapsed="true"  CollapsedSize="0" CollapsedText="Add Students" ExpandControlID="LinkAdvanceSearch"
         ExpandedText="Hide Search" ScrollContents="false" TargetControlID="Panel1"
        TextLabelID="LinkAdvanceSearch">
    </ajaxToolkit:CollapsiblePanelExtender>


<input id="h_Selected_menu_1" runat="server" type="hidden"  value="=" />
<input id="h_Selected_menu_2" runat="server" type="hidden"  value="=" />
<input id="h_Selected_menu_3" runat="server" type="hidden"  value="=" />
<input id="h_Selected_menu_4" runat="server" type="hidden"  value="=" />
<input id="h_Selected_menu_5" runat="server" type="hidden"  value="=" />

</div>