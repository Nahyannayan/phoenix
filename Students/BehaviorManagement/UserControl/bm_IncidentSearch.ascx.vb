Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_UserControl_bm_IncidentSearch
    Inherits System.Web.UI.UserControl

    ' Delegate declaration 
    Public Delegate Sub OnButtonClick(ByVal ds As DataSet)

    ' Event declaration 
    Public Event btnHandler As OnButtonClick

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindMainCategory()
        End If
    End Sub

    Private Sub BindMainCategory()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID <> 0 ORDER BY BM_CATEGORYID"
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID = 0 ORDER BY BM_CATEGORYID"
            Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            cmbMainCategory.DataSource = dsInitiator.Tables(0)
            cmbMainCategory.DataTextField = "BM_CATEGORYNAME"
            cmbMainCategory.DataValueField = "BM_CATEGORYID"
            cmbMainCategory.DataBind()
            Dim list As New ListItem
            list.Text = "Select Category"
            list.Value = "-1"
            cmbMainCategory.Items.Insert(0, list)

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim ds As New DataSet
        'If Not IsPostBack Then

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim pParms(13) As SqlClient.SqlParameter

        If ddType.SelectedIndex > 0 Then
            pParms(0) = New SqlClient.SqlParameter("@BM_INCIDENT_TYPE", ddType.SelectedValue)
        End If
        If txtReportFrom.Text.Trim() <> "" Then
            pParms(1) = New SqlClient.SqlParameter("@BM_FROM_DATE", txtReportFrom.Text)
        End If
        If txtReportto.Text.Trim() <> "" Then
            pParms(2) = New SqlClient.SqlParameter("@BM_TO_DATE", txtReportto.Text)
        End If

        pParms(3) = New SqlClient.SqlParameter("@BM_STU_ID", Session("sBsuid"))


        If txtStudentno.Text.Trim() <> "" Then
            pParms(4) = New SqlClient.SqlParameter("@BM_STU_NO", txtStudentno.Text.Trim)
        End If

        If ddOtherType.SelectedIndex >= 0 Then

            Select Case ddOtherType.SelectedValue
                Case "0"
                    pParms(5) = New SqlClient.SqlParameter("@BM_NOTES_STU_PLANNER", "Yes")
                Case "1"
                    pParms(6) = New SqlClient.SqlParameter("@BM_BREAK_DETENTION", "Yes")
                Case "2"
                    pParms(7) = New SqlClient.SqlParameter("@BM_AFTER_SCHOOL_DETENTION", "Yes")
                Case "3"
                    pParms(8) = New SqlClient.SqlParameter("@BM_SUSPENSION", "Yes")
                Case "4"
                    pParms(9) = New SqlClient.SqlParameter("@BM_REF_STU_COUNSELLOR", "Yes")

            End Select

        End If
        If txtIncidentDate.Text.Trim() <> "" Then
            pParms(10) = New SqlClient.SqlParameter("@BM_INCIDENT_DATE", txtIncidentDate.Text)
        End If

        'If cmbMainCategory.SelectedIndex >= 0 Then
        '    pParms(11) = New SqlClient.SqlParameter("@BM_MAIN_CATEGORY", cmbMainCategory.SelectedValue)
        'End If

        If cmbSubCategory.SelectedIndex >= 0 Then
            pParms(11) = New SqlClient.SqlParameter("@BM_SUB_CATEGORY", cmbSubCategory.SelectedValue)
        End If

        pParms(12) = New SqlClient.SqlParameter("@OPTION", 1)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BM.BM_SEARCH_INCIDENT_DETAILS", pParms)

        'End If
        RaiseEvent btnHandler(ds)
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        txtReportFrom.Text = ""
        txtReportto.Text = ""
        txtStudentId.Text = ""
        txtStudentno.Text = ""
        txtIncidentDate.Text = ""
        ddType.SelectedIndex = 0
        ddOtherType.SelectedIndex = 0
        cmbMainCategory.SelectedIndex = 0
        bindsubcategory()
    End Sub

    Protected Sub cmbMainCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMainCategory.SelectedIndexChanged
        bindsubcategory()
    End Sub

    Protected Sub bindsubcategory()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYHRID=" & cmbMainCategory.SelectedValue & "ORDER BY BM_CATEGORYID"
            Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            cmbSubCategory.DataSource = dsInitiator.Tables(0)
            cmbSubCategory.DataTextField = "BM_CATEGORYNAME"
            cmbSubCategory.DataValueField = "BM_CATEGORYID"
            cmbSubCategory.DataBind()
            cmbSubCategory.SelectedIndex = 0
            'Dim list As New ListItem
            'list.Text = "Select Category"
            'list.Value = "-1"
            'cmbSubCategory.Items.Insert(0, list)

        Catch ex As Exception

        End Try
    End Sub

End Class
