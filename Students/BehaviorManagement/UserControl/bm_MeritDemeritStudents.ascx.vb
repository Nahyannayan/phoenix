﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data


Partial Class Students_BehaviorManagement_UserControl_bm_MeritDemeritStudents
    Inherits System.Web.UI.UserControl

    ' Delegate declaration 
    Public Delegate Sub OnButtonClick(ByVal ds As DataSet)


    ' Event declaration 
    Public Event btnHandler As OnButtonClick
    Protected Sub h_STU_ID_ValueChanged(sender As Object, e As EventArgs)
        lbtnLoadStuDetail_Click(Nothing, Nothing)
    End Sub

    Protected Sub lbtnLoadStuDetail_Click(sender As Object, e As EventArgs)

        If sender Is Nothing Then            
            Session("gvStudents") = GET_STUDENT_LIST()
            BindGrid()
            h_STU_ID.Value = ""
        End If


        ' SearchStudent()
    End Sub
    Private Sub BindGrid()

        If Not Session("gvStudents") Is Nothing Then
            GrdView.DataSource = Session("gvStudents")
            GrdView.DataBind()
            ''on24feb
            GrdView.Visible = False
            btnaddstu.Visible = False
            btnaddstu_Click(Nothing, Nothing)
        Else
            btnaddstu.Visible = False
        End If

    End Sub
    Protected Sub btnaddstu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnaddstu.Click
        Dim dt As New DataTable
        dt.Columns.Add("STU_ID") ''
        dt.Columns.Add("STU_NO") ''
        dt.Columns.Add("STU_FIRSTNAME") ''
        dt.Columns.Add("STU_LASTNAME") ''
        dt.Columns.Add("grm_display") ''
        dt.Columns.Add("sct_descr") ''
        dt.Columns.Add("parent_name") ''
        dt.Columns.Add("ParentEmail") ''
        dt.Columns.Add("ParentMobile") ''

        For Each row As GridViewRow In GrdView.Rows
            Dim dr As DataRow = dt.NewRow()
            Dim ch1 As CheckBox = DirectCast(row.FindControl("ch2"), CheckBox)
            ''commented by nahyan on 24feb
            ''  If ch1.Checked Then
            dr.Item("STU_ID") = DirectCast(row.FindControl("Hiddenstuid"), HiddenField).Value
            dr.Item("STU_NO") = DirectCast(row.FindControl("lblno"), Label).Text.Trim()
            dr.Item("STU_FIRSTNAME") = DirectCast(row.FindControl("lblstufirst"), Label).Text.Trim()
            'dr.Item("STU_LASTNAME") = DirectCast(row.FindControl("lblstulast"), Label).Text.Trim()
            dr.Item("grm_display") = DirectCast(row.FindControl("lblstugrade"), Label).Text.Trim()
            dr.Item("sct_descr") = DirectCast(row.FindControl("lblstusection"), Label).Text.Trim()
            dr.Item("parent_name") = DirectCast(row.FindControl("lblstuparentname"), Label).Text.Trim()
            dr.Item("ParentEmail") = DirectCast(row.FindControl("lblstuparentemail"), Label).Text.Trim()
            dr.Item("ParentMobile") = DirectCast(row.FindControl("lblstuparentmobile"), Label).Text.Trim()
            dt.Rows.Add(dr)
            ''End If
        Next
        Dim ds As New DataSet
        ds.Tables.Add(dt)
        Session("ds1") = ds
        'CollapsiblePanelExtender1.AutoCollapse = True
        '  CollapsiblePanelExtender1.Collapsed = True
        RaiseEvent btnHandler(ds)
        '  Clear()

    End Sub

    Protected Sub btnSearchFeeID_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Session("gvStudents") = GET_STUDENT_LIST()
        BindGrid()
    End Sub
    Protected Sub btnSearchStudName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("gvStudents") = GET_STUDENT_LIST()
        BindGrid()
    End Sub

    Protected Sub lbtnClearStudent_Click(sender As Object, e As EventArgs) Handles lbtnClearStudent.Click
        h_STU_ID.Value = ""
        txtStudent.Text = ""
    End Sub


    Public Function GET_STUDENT_LIST() As DataTable
        '' EXCLUDE_DUPLICATE_STUDENT()
        Dim txtstudentsearchname As New TextBox
        Dim txtstudentsearchnumber As New TextBox
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PartAFilter As String = ""
        Dim pParms(4) As SqlClient.SqlParameter

        If (h_STU_ID.Value <> "") Then
            Dim studIDs() As String = h_STU_ID.Value.Split("||")
            For Each studid As String In studIDs
                If (studid <> "") Then
                    pParms(0) = New SqlClient.SqlParameter("@STU_ID", Convert.ToInt64(studid))
                    'Else
                    '    pParms(0) = New SqlClient.SqlParameter("@STU_ID", System.DBNull.Value)

                    pParms(3) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("sBsuId"))

                    If (GrdView.Rows.Count > 0) Then
                        txtstudentsearchnumber = GrdView.HeaderRow.FindControl("txtFeeId")
                        txtstudentsearchname = GrdView.HeaderRow.FindControl("txtStudName")

                        If txtstudentsearchnumber.Text <> "" Then
                            pParms(1) = New SqlClient.SqlParameter("@STU_NO", txtstudentsearchnumber.Text.Trim)
                            Session("gvStudents") = Nothing
                        Else
                            pParms(1) = New SqlClient.SqlParameter("@STU_NO", System.DBNull.Value)
                        End If
                        If txtstudentsearchname.Text <> "" Then
                            pParms(2) = New SqlClient.SqlParameter("@STU_NAME", txtstudentsearchname.Text.Trim)
                            Session("gvStudents") = Nothing
                        Else
                            pParms(2) = New SqlClient.SqlParameter("@STU_NAME", System.DBNull.Value)
                        End If
                    End If
                    Dim ds As DataSet
                    '  ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, PartAFilter & "order by GRD_DISPLAYORDER,SCT_DESCR,StudName")
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GET_StudentInfo_MeritDemerit", pParms)
                    If Not ds Is Nothing Then
                        If Not HttpContext.Current.Session("gvStudents") Is Nothing Then
                            Dim dt As DataTable = DirectCast(HttpContext.Current.Session("gvStudents"), DataTable)
                            If dt.Rows.Count >= 0 Then
                                dt.Merge(ds.Tables(0))
                                HttpContext.Current.Session("gvStudents") = dt
                                REARRANGE_DATATABLE()
                            End If
                            'Return dt
                        Else
                            HttpContext.Current.Session("gvStudents") = ds.Tables(0)
                            'Return ds.Tables(0)
                        End If
                    Else
                        HttpContext.Current.Session("gvStudents") = ""
                        ' Return DirectCast(HttpContext.Current.Session("gvStudents"), DataTable)
                    End If
                End If
            Next
        Else
            HttpContext.Current.Session("gvStudents") = ""
            ' Return DirectCast(HttpContext.Current.Session("gvStudents"), DataTable)
        End If
        Return DirectCast(HttpContext.Current.Session("gvStudents"), DataTable)
    End Function

    Private Sub Clear()
        Session("gvStudents") = Nothing
        ViewState("chkAll") = False
        BindGrid()
        txtStudent.Text = ""

        h_STU_ID.Value = ""
    End Sub
    Private Sub EXCLUDE_DUPLICATE_STUDENT()
        'If Not HttpContext.Current.Session("gvStudents") Is Nothing Then
        '    Dim strSTUID() As String = STU_IDs.Split("||")
        '    If strSTUID.Length > 0 Then
        '        For i As Int16 = 0 To strSTUID.Length - 1
        '            If strSTUID(i).ToString <> "" AndAlso IsNumeric(strSTUID(i).ToString) Then
        '                Dim filter As String = "STU_ID='" & strSTUID(i).ToString & "'"
        '                Dim view As New DataView(DirectCast(HttpContext.Current.Session("gvStudents"), DataTable))
        '                view.RowFilter = filter
        '                If Not view Is Nothing AndAlso view.ToTable().Rows.Count > 0 Then
        '                    STU_IDs = STU_IDs.Replace(strSTUID(i), "0")
        '                End If
        '            End If
        '        Next
        '    End If
        'End If
    End Sub
    Public Shared Sub REARRANGE_DATATABLE()
        If Not HttpContext.Current.Session("gvStudents") Is Nothing Then
            Dim dt As DataTable = DirectCast(HttpContext.Current.Session("gvStudents"), DataTable)
            Dim rows As Integer = dt.Rows.Count
            For i As Integer = 1 To rows Step 1
                dt.Rows(i - 1)("ID") = i
                'i = i + 1
            Next
            HttpContext.Current.Session("gvStudents") = dt
        End If
    End Sub
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "error"
            Else
                lblError.CssClass = "error"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub


End Class
