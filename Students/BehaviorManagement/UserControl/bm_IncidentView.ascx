<%@ Control Language="VB" AutoEventWireup="false" CodeFile="bm_IncidentView.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_IncidentView" %>
<%@ Register Src="bm_IncidentSearch.ascx" TagName="bm_IncidentSearch" TagPrefix="uc1" %>
<uc1:bm_IncidentSearch ID="Bm_IncidentSearch1" OnbtnHandler="Searchdate" runat="server" />

<!-- Bootstrap core CSS-->
<link href="/PHOENIXBETA/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

  <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="/PHOENIXBETA/cssfiles/Popup.css" rel="stylesheet" />



<div align="left">

<asp:LinkButton ID="lnkinsertnew" runat="server">Issue New Incident Slip</asp:LinkButton>
</div>

 <table border="0" cellpadding="0" cellspacing="0" Width="100%" >
                        <tr>
                            <td class="title-bg-lite">
                                Incident Lists</td>
                        </tr>
                        <tr>
                            <td >

<asp:GridView ID="GrdView" AutoGenerateColumns="False" Width="100%" runat="server"  CssClass="table table-bordered table-row">
<Columns>

<asp:TemplateField HeaderText="Incident Id"  Visible="false">
<ItemTemplate>
<asp:HiddenField ID="HiddenIncidentid" Value='<%#Eval("BM_ID")%>' runat="server" />
<%#Eval("BM_ID")%>
</ItemTemplate>
</asp:TemplateField>


    <asp:TemplateField HeaderText="Incident Date">
        <ItemTemplate>
            <%#Eval("BM_INCIDENT_DATE", "{0:dd/MMM/yyyy}")%>
        </ItemTemplate>
    </asp:TemplateField>


<asp:TemplateField HeaderText="Report Date" >
<ItemTemplate>
<%# Eval("BM_ENTRY_DATE","{0:dd/MMM/yyyy}") %>
</ItemTemplate>
</asp:TemplateField>

    <asp:TemplateField HeaderText="Incident Type">
        <ItemTemplate>
            <asp:Label ID="lblShift" runat="server" 
                Text='<%# Bind("BM_INCIDENT_TYPE") %>'></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>


<asp:TemplateField HeaderText="Reporting Staff" >
<ItemTemplate>
<%#Eval("EMP_FNAME")%> &nbsp;  <%#Eval("EMP_LNAME")%> 
</ItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="View" >
<ItemTemplate>
    <asp:LinkButton ID="lnkview" OnClientClick='<%# GetNavigateUrl(Eval("BM_ID").ToString()) %>' runat="server">View</asp:LinkButton>
</ItemTemplate>
</asp:TemplateField>

<asp:TemplateField HeaderText="Edit" >
<ItemTemplate>
    <asp:LinkButton ID="lnkEdit" CommandName="Editing" CommandArgument='<%#Eval("BM_ID")%>' runat="server">Edit</asp:LinkButton>
</ItemTemplate>
</asp:TemplateField>

</Columns>
        <HeaderStyle  />
        <RowStyle CssClass="griditem"   />
        <SelectedRowStyle   />
        <AlternatingRowStyle CssClass="griditem_alternative"   />
        <EmptyDataRowStyle  />
        <EditRowStyle  />
</asp:GridView>

 </td>
                    </tr>
               </table>