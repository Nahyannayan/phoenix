Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System.Xml
Partial Class Students_BehaviorManagement_BMSetup
    Inherits System.Web.UI.Page
    '-------------------------------------------------------------------------------------------------
    ' Page Name     : Behavoiur Manageement Setup
    ' Purpose       : To Eneter the Behaviour management setup
    ' Created By    : Sajesh Elias
    ' Created Date  : 04-12-08
    ' Description   :
    '-------------------------------------------------------------------------------------------------
    ' Module level variable declarations
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")

            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                Try
                    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                    Dim str_sql As String = ""
                    Dim CurBsUnit As String = Session("sBsuid")
                    Dim USR_NAME As String = Session("sUsr_name")

                    'Collect the url of the file to be redirected in view state
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If

                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                    If USR_NAME = "" Then
                        If Not Request.UrlReferrer Is Nothing Then
                            Response.Redirect(Request.UrlReferrer.ToString())
                        Else
                            Response.Redirect("~\noAccess.aspx")
                        End If
                    Else
                        DisplaySetup()
                        'Calling pageright class to get the access rights
                        ViewState("datamode") = "add"
                        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        If hfId1.Value <> "" Then
                            ViewState("datamode") = "edit"
                        End If
                    End If

                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub

#Region "Private Functions"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim intAlertMode As Integer = 0

                If chkEmail.Checked = True Then
                    intAlertMode = 1
                End If
                If chkMessage.Checked = True Then
                    intAlertMode = 2
                End If

                If chkEmail.Checked = True And chkMessage.Checked = True Then
                    intAlertMode = 3
                End If


                Dim str_query As String = "exec BM.BM_saveSETUP " + hfId1.Value.ToString + ",'0'," & Session("sBsuid") & "," + txtAlertPoints.Text + "," + txtActionPoints.Text + "," + intAlertMode + ",'" + ViewState("datamode") + "'"
                hfId1.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "BM_SETUPID(" + hfId1.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                txtActionPoints.Text = ""
                txtAlertPoints.Text = ""
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Private Sub DisplaySetup()

        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BM_SETUPID,BM_CATEGORYID,BM_BSU_ID,BM_ALERT_NOS,BM_ACTION_NOS,BM_ALERTMODE " & _
                    " FROM BM.BM_SETUP WHERE BM_BSU_ID= " & Session("sBsuid")

        Dim dsSurveyQuest As DataSet
        dsSurveyQuest = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If dsSurveyQuest.Tables(0).Rows.Count > 0 Then
            txtActionPoints.Text = dsSurveyQuest.Tables(0).Rows(0).Item("BM_ACTION_NOS")
            txtAlertPoints.Text = dsSurveyQuest.Tables(0).Rows(0).Item("BM_ALERT_NOS")
            hfId1.Value = dsSurveyQuest.Tables(0).Rows(0).Item("BM_SETUPID")
            Select Case dsSurveyQuest.Tables(0).Rows(0).Item("BM_SETUPID")
                Case 0
                    chkEmail.Checked = False
                    chkMessage.Checked = False
                Case 1
                    chkEmail.Checked = True
                    chkMessage.Checked = False
                Case 2
                    chkEmail.Checked = False
                    chkMessage.Checked = True
                Case 3
                    chkEmail.Checked = True
                    chkMessage.Checked = True
            End Select
        End If
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If hfId1.Value = "" Then
                ViewState("datamode") = "add"
            Else
                ViewState("datamode") = "edit"
            End If
            SaveData()
            ViewState("datamode") = "view"
            btnSave.Enabled = False
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Try
                ViewState("datamode") = "delete"
                SaveData()
                ViewState("datamode") = "none"
                hfId1.Value = ""
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Catch ex As Exception

            End Try
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            txtActionPoints.Text = ""
            txtAlertPoints.Text = ""
            hfId1.Value = ""
            lblError.Text = ""
            ViewState("datamode") = "add"
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception

        End Try
    End Sub
End Class
