<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="bmIncidentStatus.aspx.vb" Inherits="Students_BehaviorManagement_bmIncidentStatus" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../../cssfiles/Popup.css" rel="stylesheet" />

    <script type="text/javascript">

        function getSibling(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 845px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../ShowSiblingInfo_BM.aspx?id=' + mode;

            if (mode == 'SI') {
                return ShowWindowWithClose(url, 'search', '55%', '85%')
                return false;
                <%-- result = window.showModalDialog(url, "", sFeatures);

                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');

                document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[0];
     document.getElementById("<%=h_SliblingID.ClientID %>").value = NameandCode[1];--%>
            }

        }

        function setSiblingValue(result) {
            NameandCode = result.split('___');

            document.getElementById("<%=txtPar_Sib.ClientID %>").value = NameandCode[2];
           document.getElementById("<%=h_SliblingID.ClientID %>").value = NameandCode[0];
            CloseFrame();
            return false;
        }
        function CloseFrame() {
            jQuery.fancybox.close();
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text=" Incident Lists"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr>
            <td class="subheader_img" colspan="2">
                Incident Lists</td>
        </tr>--%>
                    <tr>
                        <td align="left" width="20%"> <span class="field-label">Academic Year</span></td>
                        <td align="left" width="30%">
            <asp:DropDownList ID="cmbAccadamicYear" runat="server"
                OnSelectedIndexChanged="cmbAccadamicYear_SelectedIndexChanged">
            </asp:DropDownList></td>
                        <td align="left"  width="30%">
                            <asp:TextBox ID="txtPar_Sib" runat="server" ></asp:TextBox>
                             <asp:ImageButton ID="imgbtnSibling" runat="server" ImageUrl="~/Images/forum_search.gif"
                                OnClick="imgbtnSibling_Click" OnClientClick="getSibling('SI');return false;"></asp:ImageButton>
                 
                            </td>
                               <td align="left" width="20%">
                                           <%--   <asp:Button ID="btnSibling" runat="server" OnClick="imgbtnSibling_Click" OnClientClick="getSibling('SI');return false;" Text="Select Sibling " CssClass="button" />--%>
                            <asp:Button ID="btnView" runat="server" OnClick="btnView_Click" Text="View " CssClass="button" /></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="GrdView" runat="server" AutoGenerateColumns="False" Width="100%" OnRowCommand="GrdView_RowCommand"
                                CssClass="table table-bordered table-row">
                                <EmptyDataRowStyle />
                                <Columns>
                                    <asp:TemplateField HeaderText="Incident Id">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenIncidentid" runat="server" Value='<%#Eval("BM_ID")%>'></asp:HiddenField>
                                            <%#Eval("BM_ID")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudName" runat="server" Text='<%# Eval("STUDNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Report Date">
                                        <ItemTemplate>
                                            <%# Eval("BM_ENTRY_DATE","{0:dd/MMM/yyyy}") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reporting Staff">
                                        <ItemTemplate>
                                            <%#Eval("EMP_FNAME")%> &nbsp;  <%#Eval("EMP_LNAME")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Incident Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblshift" runat="server" Text='<%# Eval("BM_INCIDENT_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="Stud Id">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudId" runat="server" Text='<%# Eval("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick='<%# GetNavigateUrl(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>' CommandArgument='<%# GetNavigateUrl(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'>View</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="griditem" />
                                <EditRowStyle />
                                <SelectedRowStyle />
                                <HeaderStyle />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_SliblingID" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>

