<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BmPrintAction.aspx.vb" Inherits="Students_BehaviorManagement_BmPrintAction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Action Details Report</title>
     <link href="cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../cssfiles/tabber.js"></script> 
    <link href="cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="cssfiles/example.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
     <div align="center">
         <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" style="width: 800px;">
             <tr>
                 <td class="subheader_img">
                     <asp:Label ID="lblBsuName" runat="server" Width="700px" Font-Bold="True" Font-Size="X-Large" ForeColor="#1B80B6"></asp:Label>&nbsp;<br />
                     <asp:Label ID="lblAddress1" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="#1B80B6"></asp:Label><br />
                     <asp:Label ID="lblAddress2" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="#1B80B6"></asp:Label><br />
                     <asp:Label ID="LblEmail" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="#1B80B6"></asp:Label></td>
             </tr>
             <tr>
                 <td class="subheader_img">
                     <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/footer.GIF" Width="873px" /></td>
             </tr>
             <tr>
                 <td class="subheader_img">
                     <asp:Label ID="lblHead" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="#1B80B6"></asp:Label>&nbsp;<asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/Images/tick.gif"
                         OnClientClick="javascript:window.print(); return false;" ToolTip="Print Page" /></td>
             </tr>
             <tr>
                 <td align="left">
                     <table style="width: 100%">
                         <tr>
                             <td bgcolor="#1b80b6" colspan="6">
                                 <strong><span style="font-size: 11pt; color: #1b80b6; font-family: Arial">
                                     <asp:Label ID="lblTitle" runat="server" ForeColor="White"></asp:Label></span></strong></td>
                         </tr>
                         <tr>
                             <td colspan="6">
                                 <asp:GridView ID="GrdView" runat="server" AutoGenerateColumns="False" Width="100%" PageSize="50">
                                     <EmptyDataRowStyle Wrap="False" />
                                     <Columns>
                                         <asp:TemplateField HeaderText="Sl No:">
                                             <ItemTemplate>
                                                 &nbsp;<asp:Label ID="lblSlNo" runat="server" Text='<%# Eval("BM_ID") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Student Name">
                                             <ItemTemplate>
                                                 <asp:Label ID="lblStudName" runat="server" Text='<%# Eval("STUDNAME") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Grade">
                                             <ItemTemplate>
                                                 <asp:Label ID="Label2" runat="server" Text='<%# Eval("GRM_DISPLAY") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Section">
                                             <ItemTemplate>
                                                 <asp:Label ID="Label1" runat="server" Text='<%# Eval("SCT_DESCR") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Stream">
                                             <ItemTemplate>
                                                 <asp:Label ID="Label3" runat="server" Text='<%# Eval("STM_DESCR") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Shift">
                                             <ItemTemplate>
                                                 <asp:Label ID="Label4" runat="server" Text='<%# Eval("SHF_DESCR") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Report Date">
                                             <ItemTemplate>
                                                 <asp:Label ID="lblDetenction" runat="server"></asp:Label><%# Eval("BM_ENTRY_DATE","{0:dd/MMM/yyyy}") %>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Action By">
                                             <ItemTemplate>
                                                 <%#Eval("EMPNAME")%>
                                                 &nbsp;
                                                 <asp:Label ID="lblDetenctionDate" runat="server"></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Stud Id" Visible="False">
                                             <ItemTemplate>
                                                 <asp:Label ID="lblStudId" runat="server" Text='<%# Eval("STU_ID") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Detn Date">
                                             <ItemTemplate>
                                                 &nbsp;<asp:Label ID="lblDetnDate" runat="server" Text='<%# Eval("DETNDATE","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                     </Columns>
                                     <RowStyle CssClass="griditem" Height="25px" Wrap="False" />
                                     <EditRowStyle Wrap="False" />
                                     <SelectedRowStyle CssClass="Green" Wrap="False" />
                                     <HeaderStyle CssClass="gridheader_pop" Height="30px" Wrap="False" />
                                     <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                 </asp:GridView>
                             </td>
                         </tr>
                     </table>
                 </td>
             </tr>
             <tr>
                 <td align="right">
                     <asp:Label ID="lblUser" runat="server" Font-Size="XX-Small" ForeColor="#1B80B6"></asp:Label></td>
             </tr>
         </table>
     
     </div>
    </form>
</body>
</html>
