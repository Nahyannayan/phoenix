<%@ Page Language="VB" AutoEventWireup="false" MaintainScrollPositionOnPostback="true" CodeFile="Default.aspx.vb" Inherits="Students_BehaviorManagement_Default" %>

<%@ Register Src="UserControl/bm_IncidentSearch.ascx" TagName="bm_IncidentSearch"
    TagPrefix="uc5" %>

<%@ Register Src="UserControl/bm_EditIncident.ascx" TagName="bm_EditIncident" TagPrefix="uc4" %>

<%@ Register Src="UserControl/bm_ActionViewDetails.ascx" TagName="bm_ActionViewDetails"
    TagPrefix="uc3" %>

<%@ Register Src="UserControl/bm_ActionView.ascx" TagName="bm_ActionView" TagPrefix="uc2" %>

<%@ Register Src="UserControl/bm_IncidentView.ascx" TagName="bm_IncidentView" TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <uc5:bm_IncidentSearch ID="Bm_IncidentSearch1" OnbtnHandler="Searchdate" runat="server" />
        <asp:GridView ID="GridView1" runat="server">
        </asp:GridView>
     <%--     <uc2:bm_ActionView ID="Bm_ActionView1" runat="server" />
        <uc3:bm_ActionViewDetails ID="Bm_ActionViewDetails1" runat="server" />
      <uc1:bm_IncidentView ID="Bm_IncidentView1" runat="server" />
   --%>
    
    </div>
    </form>
</body>
</html>
