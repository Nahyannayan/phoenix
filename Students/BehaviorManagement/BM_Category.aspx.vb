Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System.Xml
Imports System.Windows.Forms

Partial Class Students_BehaviorManagement_BM_Category
    Inherits System.Web.UI.Page
    ' Module level variable declarations
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If ViewState("datamode") = "add" Then
                btngradeavailable.Visible = False
                btnchkscore.Visible = False
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.SetExpires(Now.AddSeconds(-1))
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")

            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                Try
                    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                    Dim str_sql As String = ""
                    Dim CurBsUnit As String = Session("sBsuid")
                    Dim USR_NAME As String = Session("sUsr_name")
                    hfId1.Value = 0
                    cmbCategory.Enabled = True
                    'Collect the url of the file to be redirected in view state
                    If Not Request.UrlReferrer Is Nothing Then
                        ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                    End If

                    ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                    ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                    If USR_NAME = "" Then
                        If Not Request.UrlReferrer Is Nothing Then
                            Response.Redirect(Request.UrlReferrer.ToString())
                        Else
                            Response.Redirect("~\noAccess.aspx")
                        End If

                    Else
                        'Calling pageright class to get the access rights
                        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                        BindParentCategory()
                        PopulateGradeMaster()
                        If ViewState("datamode") = "add" Then
                            hfId1.Value = 0
                        ElseIf ViewState("datamode") = "view" Then
                            hfId1.Value = Encr_decrData.Decrypt(Request.QueryString("CategoryId").Replace(" ", "+"))
                            btnEdit.Enabled = True
                            btngradeavailable.Visible = False
                            btnchkscore.Visible = False
                            DisplayCategory()
                            DisableControls()
                        End If
                        'Disable the control buttons based on the rights
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    End If

                Catch ex As Exception
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try

            End If

        Catch ex As Exception

        End Try
    End Sub


    'public Sub CheckSubCategory()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID<>0 ORDER BY BM_CATEGORYID"
    '    Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    Dim FLAG As Boolean
    '    FLAG = False
    '    For Each item In dsInitiator.Tables(0).Columns("BM_CATEGORYNAME").ToString
    '        If txtCategory.Text = item Then
    '            FLAG = True
    '        End If
    '    Next
    '    If FLAG Then
    '        lblError.Text = "ITEM ALREADY EXISTS, PLEASE ENTER NEW SUB CATEGORY"
    '        txtCategory.Text = ""
    '        txtCategory.Focus()
    '    End If
    'End Sub

    Protected Sub cmbCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

#Region " Private Functions "

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub BindParentCategory()
        cmbCategory.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYHRID=0 ORDER BY BM_CATEGORYID"
        Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        cmbCategory.DataSource = dsInitiator.Tables(0)
        cmbCategory.DataTextField = "BM_CATEGORYNAME"
        cmbCategory.DataValueField = "BM_CATEGORYID"
        cmbCategory.DataBind()
        Dim LstItem As New ListItem
        LstItem.Value = 0
        LstItem.Text = ""
        cmbCategory.Items.Add(LstItem)
        cmbCategory.SelectedValue = 0

        'Dim list As New ListItem
        'list.Text = "Select Category"
        'list.Value = "-1"
        'cmbCategory.Items.Insert(0, list)

    End Sub
    Public Sub PopulateGradeMaster()
        chkboxlistgrade.ClearSelection()
        'cmbGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT GRD_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_M ORDER BY GRD_DISPLAYORDER" ' _
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'cmbGrade.DataSource = ds
        chkboxlistgrade.DataSource = ds
        'cmbGrade.DataTextField = "GRD_DISPLAY"
        chkboxlistgrade.DataTextField = "GRD_DISPLAY"
        'cmbGrade.DataValueField = "GRD_ID"
        chkboxlistgrade.DataValueField = "GRD_ID"
        'cmbGrade.DataBind()
        chkboxlistgrade.DataBind()
        'Dim LstItem As New ListItem
        'LstItem.Value = 0
        'LstItem.Text = ""
        'cmbGrade.Items.Add(LstItem)
        'cmbGrade.SelectedValue = 0
    End Sub

    Public Function isCategoryExists() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT count(*) FROM BM.BM_CATEGORY WHERE BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYHRID= " & cmbCategory.SelectedValue & " AND BM_CATEGORYNAME='" + txtCategory.Text.Replace("'", "''").Trim + "' "
        Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If sbm = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Dim gradestr As String = ""
            For Each i As ListItem In chkboxlistgrade.Items
                If i.Selected And i.Enabled Then
                    gradestr = gradestr + i.Value + "|"
                End If
            Next

            'hfId1.Value = cmbCategory.SelectedValue

            Dim str_query As String = "exec BM.BM_saveCATEGORY_M " + hfId1.Value.ToString + "," & _
            IIf(cmbCategory.SelectedValue = "", "0", cmbCategory.SelectedValue) + ",'" + txtCategory.Text + "'," & _
            IIf(txtScore.Text = "", "0", txtScore.Text) + ",'" & gradestr & "','" & ViewState("datamode") & "','" + Session("sBsuid") + "'"
            SqlHelper.ExecuteDataset(transaction, CommandType.Text, str_query)
            'hfId1.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

            Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "BM_CATEGORYID(" + hfId1.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)

            If flagAudit <> 0 Then
                Throw New ArgumentException("Could not process your request")
            End If
            transaction.Commit()
            lblError.Text = "Record Saved Successfully"
        End Using
    End Sub

    Private Sub DisplayCategory()

        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BM_CATEGORYID,BM_CATEGORYHRID,BM_CATEGORYNAME,BM_CATEGORY_SCORE " & _
                    " FROM BM.BM_CATEGORY " & _
                    " WHERE BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYID=" & hfId1.Value

        Dim dsSurveyQuest As DataSet
        dsSurveyQuest = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If dsSurveyQuest.Tables(0).Rows.Count > 0 Then
            cmbCategory.SelectedValue = dsSurveyQuest.Tables(0).Rows(0).Item("BM_CATEGORYHRID")
            txtCategory.Text = dsSurveyQuest.Tables(0).Rows(0).Item("BM_CATEGORYNAME")
            txtScore.Text = dsSurveyQuest.Tables(0).Rows(0).Item("BM_CATEGORY_SCORE")
            hfId1.Value = dsSurveyQuest.Tables(0).Rows(0).Item("BM_CATEGORYID")
            'cmbGrade.SelectedValue = dsSurveyQuest.Tables(0).Rows(0).Item("BM_GRADE")
            select_checklist()
        End If
    End Sub

    Private Sub ClearForm()
        cmbCategory.SelectedIndex = -1
        txtCategory.Text = ""
        txtScore.Text = ""
        chkboxselectall.Checked = False
        For Each i As ListItem In chkboxlistgrade.Items
            i.Enabled = True
            i.Selected = False
        Next i
    End Sub

#End Region

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            'If ViewState("datamode") = "add" And isCategoryExists() Then
            '    lblError.Text = "Category Name Already Exists. Please Enter New Category"
            '    Exit Sub
            'End If

            SaveData()
            ViewState("datamode") = "view"
            btnSave.Enabled = False
            ClearForm()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            divAge.Visible = False
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        Try
            divAge.Visible = False
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        Try
            divAge.Visible = False
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If ViewState("datamode") = "add" And isCategoryExists() Then
                lblUerror.Text = "Category Name Already Exists, So these grades will be added to existing Category Name. Category score will not be updated. Please use edit option to update the Score."
                'Exit Sub
                divAge.Visible = True
            Else
                SaveData()
                ViewState("datamode") = "view"
                btnSave.Enabled = False
                ClearForm()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If ViewState("datamode") = "edit" Then
                SaveData()
                ViewState("datamode") = "view"
                btnSave.Enabled = False
                ClearForm()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            ltLabel.Text = "Delete Sub Category Completely"
            ViewState("datamode") = "delete"
            SaveData()
            lblError.Text = "Records Deleted Successfully"
            ViewState("datamode") = "none"
            hfId1.Value = 0
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            ltLabel.Text = "Edit Score, Remove or Add Grades"
            'chkboxlistgrade.ClearSelection()
            btngradeavailable.Visible = True
            btngradeavailable.Enabled = True
            btnchkscore.Visible = True
            btnchkscore.Enabled = True
            lblError.Text = ""
            ViewState("datamode") = "edit"
            btnSave.Enabled = True
            'ClearForm()
            hfId1.Value = 0
            EnableControls1()
            'BindParentCategory()
            lblError.Text = ""
            'If hfId1.Value = 0 Then
            '    lblError.Text = "No records to Edit"
            '    Exit Sub
            'End If

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            UtilityObj.beforeLoopingControls(Me.Page)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ltLabel.Text = "Add New Sub Category"
            cmbCategory.Enabled = True
            chkboxlistgrade.ClearSelection()
            lblError.Text = ""
            ViewState("datamode") = "add"
            btnSave.Enabled = True
            ClearForm()
            hfId1.Value = 0
            EnableControls()
            BindParentCategory()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception

        End Try
    End Sub
    Public Sub disable_checklist()
        Dim FLAG As Boolean = False
        For Each i As ListItem In chkboxlistgrade.Items
            i.Enabled = True
            i.Selected = False
        Next i

        chkboxselectall.Checked = False
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        'Dim str1 As String = " SELECT BCD_GRD_ID FROM BM.BM_CATEGORY_GRADES " & _
        '" WHERE(SELECT BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYID=BCD_BM_CATEGORYID) = '" & txtCategory.Text & "' And BCD_BM_BSU_ID = " & Session("sBsuid")
        Dim str1 As String = " SELECT B2.BCD_GRD_ID FROM BM.BM_CATEGORY B1,BM.BM_CATEGORY_GRADES B2 " & _
        " WHERE B1.BM_CATEGORYID=B2.BCD_BM_CATEGORYID AND B1.BM_CATEGORYNAME = '" & txtCategory.Text & "' And " & _
        "BCD_BM_BSU_ID =" & Session("sBsuid") & "AND B1.BM_CATEGORYHRID=" & cmbCategory.SelectedValue

        '" SELECT BCD_GRD_ID FROM BM.BM_CATEGORY_GRADES " & _
        '" WHERE(SELECT BM_CATEGORYNAME FROM BM.BM_CATEGORY WHERE BM_CATEGORYID=BCD_BM_CATEGORYID) = '" & txtCategory.Text & "' And BCD_BM_BSU_ID = " & Session("sBsuid")
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str1)
        For Each i As ListItem In chkboxlistgrade.Items
            FLAG = False
            For j As Integer = 0 To ds.Tables(0).Rows.Count - 1
                If i.Text = ds.Tables(0).Rows(j)("BCD_GRD_ID").ToString Then
                    FLAG = True
                    'i.Enabled = False

                End If
            Next j
            If FLAG Then
                i.Selected = True
                i.Enabled = False
            Else
                i.Selected = False
                i.Enabled = True
            End If
        Next i
    End Sub
    Public Sub select_checklist()
        Dim flag As Boolean
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str1 As String = " SELECT B2.BCD_GRD_ID FROM BM.BM_CATEGORY B1,BM.BM_CATEGORY_GRADES B2 " & _
        " WHERE B1.BM_CATEGORYID=B2.BCD_BM_CATEGORYID AND B1.BM_CATEGORYNAME = '" & txtCategory.Text & "' And " & _
        "BCD_BM_BSU_ID =" & Session("sBsuid") & "AND B1.BM_CATEGORYHRID=" & cmbCategory.SelectedValue
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str1)
        chkboxselectall.Enabled = True
        For Each i As ListItem In chkboxlistgrade.Items
            flag = False
            For j As Integer = 0 To ds.Tables(0).Rows.Count - 1
                If i.Text = ds.Tables(0).Rows(j)("BCD_GRD_ID").ToString Then
                    'i.Selected = True
                    flag = True
                End If
            Next j
            If flag Then
                i.Selected = True
                i.Enabled = True
            Else
                i.Selected = False
                i.Enabled = True
            End If
        Next i
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            'ClearForm()
            hfId1.Value = 0
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'Disable Controls
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Else
                ViewState("datamode") = "none"
                'Encrypt the data that needs to be send through Query String
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                Dim mInfo As String = "?MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & ViewState("datamode").ToString()
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Response.Redirect("~\Students\BehaviorManagement\BMListCategory.aspx" & mInfo)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Sub EnableControls()
        txtCategory.Enabled = True
        txtScore.Enabled = True
        chkboxselectall.Enabled = True
        For Each i As ListItem In chkboxlistgrade.Items
            i.Enabled = True
            i.Selected = False
        Next i
    End Sub

    Private Sub EnableControls1()
        cmbCategory.Enabled = True
        txtCategory.Enabled = True
        txtScore.Enabled = True
        chkboxselectall.Enabled = True
        For Each i As ListItem In chkboxlistgrade.Items
            i.Enabled = True
        Next i
    End Sub


    Private Sub DisableControls()
        cmbCategory.Enabled = False
        txtCategory.Enabled = False
        txtScore.Enabled = False
        chkboxselectall.Enabled = False
        For Each i As ListItem In chkboxlistgrade.Items
            i.Enabled = False
        Next i
        btngradeavailable.Enabled = False
        btnAdd.Visible = False
    End Sub
    Protected Sub chkboxselectall_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxselectall.CheckedChanged
        For Each i As ListItem In chkboxlistgrade.Items
            If i.Enabled = True Then
                i.Selected = chkboxselectall.Checked
            End If
        Next i
    End Sub

    Protected Sub btngradeavailable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btngradeavailable.Click
        If ViewState("datamode") = "edit" Then
            select_checklist()
        Else
            disable_checklist()
        End If
    End Sub
    Public Sub bindscore()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BM_CATEGORY_SCORE FROM BM.BM_CATEGORY WHERE BM_BSU_ID='" & Session("sBsuid") & "' AND BM_CATEGORYHRID=" & cmbCategory.SelectedValue & " AND BM_CATEGORYNAME='" & txtCategory.Text & "' AND BM_BSU_ID = " & Session("sBsuid")
        Dim dsSurveyQuest As DataSet
        dsSurveyQuest = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If dsSurveyQuest.Tables(0).Rows.Count > 0 Then
            txtScore.Text = dsSurveyQuest.Tables(0).Rows(0).Item("BM_CATEGORY_SCORE").ToString
        Else
            lblError.Text = "No Records Found."
        End If
    End Sub
    Protected Sub btnchkscore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnchkscore.Click
        bindscore()
    End Sub

End Class
