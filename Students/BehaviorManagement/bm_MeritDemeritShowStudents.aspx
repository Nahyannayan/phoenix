﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="bm_MeritDemeritShowStudents.aspx.vb" Inherits="Students_BehaviorManagement_bm_MeritDemeritShowStudents" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
    <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 
    <%--<link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script language="javascript" type="text/javascript" src="../../cssfiles/chromejs/chrome.js">
    </script>
   
    <script language="javascript" type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function ChangeCheckBoxState(id, checkState) {
            var cb = document.getElementById(id);
            if (cb != null)
                cb.checked = checkState;
        }

        function ChangeAllCheckBoxStates(checkState) {
            // Toggles through all of the checkboxes defined in the CheckBoxIDs array
            // and updates their value to the checkState input parameter
            // var lstrChk = document.getElementById("chkAL").checked;
            var chk_state = document.getElementById("chkAL").checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].name.search(/chkSelAll/) != 0)
                    if (document.forms[0].elements[i].type == 'checkbox') {
                        document.forms[0].elements[i].checked = chk_state;
                    }
            }
        }

    </script>
</head>
<body onload="listen_window();" leftmargin="0" topmargin="0" bottommargin="0" rightmargin="0">
    <form id="form1" runat="server">
     <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
           <tr>
                <td >
                     <input id="h_SelectedId" runat="server" type="hidden" value="-1" />
                    <asp:GridView ID="GvStudents" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Data" AllowPaging="True" PageSize="17" CssClass="table table-bordered table-row"
                        OnPageIndexChanging="GvStudents_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <HeaderTemplate>
                                    <input id="chkAL" name="chkAL" onclick="ChangeAllCheckBoxStates(true);" type="checkbox"
                                        value="Check All" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkControl" runat="server" type="checkbox" value='<%# Bind("STU_ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STUDENT NO">
                                <HeaderTemplate>
                                    Student#<br />
                                     <asp:TextBox ID="txtFeeId" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchFeeID" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchFeeID_Click" CausesValidation="False" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STUDENT Name">
                                <HeaderTemplate>
                                    Student Name <br />
                                      <asp:TextBox ID="txtStudName" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchStudName" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchStudName_Click" CausesValidation="False" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbCodeSubmit" runat="server" Text='<%# Bind("StudName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="GRADE" >
                                <HeaderTemplate>
                                    Grade  <br />
                                    <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                   <asp:ImageButton ID="btnSearchGrade" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchGrade_Click" CausesValidation="False" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("grm_display")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                          <asp:TemplateField HeaderText="SECTION" >
                                <HeaderTemplate>
                                    Section  <br />
                                    <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                     <asp:ImageButton ID="btnSearchsection" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchsection_Click" CausesValidation="False" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label16" runat="server" Text='<%# Bind("sct_descr")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="HOUSE">
                                <HeaderTemplate>
                                    House#<br />
                                     <asp:TextBox ID="txtHouse" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="btnSearchHouse" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                OnClick="btnSearchHouse_Click" CausesValidation="False" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblHouse" runat="server" Text='<%# Bind("house_description") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
               
            </tr>
            <tr>
              
                <td align="center">
                     <asp:CheckBox ID="chkSelAll" runat="server" AutoPostBack="True" CssClass="radiobutton"
                        Text="Select All" />
                    <asp:Button ID="btnFinish" runat="server" CssClass="button" Text="Finish" />
                </td>
            </tr>
         </table>
    </form>
</body>
</html>
