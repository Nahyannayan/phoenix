<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="BMListCategory.aspx.vb" Inherits="Students_BehaviorManagement_BMListCategory"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Incident Category List
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
       <%-- <tr class="subheader_img">
            <td align="left" colspan="2" valign="middle">
                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                    Category List&nbsp;</span></font>
            </td>
        </tr>--%>
        <tr>
            <td align="left" width="20%">
                <asp:Label ID="lblAccText" runat="server" Text="Select Main Category" CssClass="field-label"></asp:Label>
            </td>
            <td align="left" width="30%">
                <asp:DropDownList ID="cmbCategory" runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="cmbCategory_SelectedIndexChanged" >
                </asp:DropDownList>
            </td>
            <td align="left" colspan="2" width="50%">
                </td>
        </tr>
        <tr>
            <td align="center"  colspan="4">
                <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" >
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvCategory" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                BorderStyle="None" CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                PageSize="20" Width="100%" OnRowCommand="gvCategory_RowCommand" OnPageIndexChanging="gvCategory_PageIndexChanging"
                                HorizontalAlign="Center">
                                <EmptyDataRowStyle />
                                <Columns>
                                    <asp:TemplateField Visible="False" HeaderText="CategoryId">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle> 
                                        <EditItemTemplate>
                                            <span ID="gvCategory_ctl01_lblsubcatsearch">Sub Category</span>
                                            <br />
                                       
                                            <input ID="gvCategory_ctl01_txtsubcatsearch" 
                                                name="gvCategory$ctl01$txtsubcatsearch" 
                                                type="text" />
                                            <input ID="gvCategory_ctl01_imgbtnsubcatsearch" 
                                                name="gvCategory$ctl01$imgbtnsubcatsearch" src="../../Images/forum_search.gif" 
                                                style="height:20px;width:23px;border-width:0px;" type="image" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            Score
                                        </FooterTemplate>
                                        <HeaderTemplate>
                                            <span ID="gvCategory_ctl01_lblgradesearch">Grade</span>
                                            <br />
                                        
                                            <input ID="gvCategory_ctl01_txtgradesearch" 
                                                name="gvCategory$ctl01$txtgradesearch" type="text" />
                                            <input ID="gvCategory_ctl01_imgbtngradesearch" 
                                                name="gvCategory$ctl01$imgbtngradesearch" src="../../Images/forum_search.gif" 
                                                style="height:21px;width:24px;border-width:0px;" type="image" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategoryId" runat="server" Text='<%# Bind("BM_CATEGORYID") %>'
                                                ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Main Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMainCategory" runat="server" Text='<%# Bind("BM_CATEGORYHRID") %>'></asp:Label>
                                            <br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sub Catagory">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblsubcatsearch" runat="server" Text="Sub Category"></asp:Label>
                                            <br />
                                       
                                            <asp:TextBox ID="txtsubcatsearch" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="imgbtnsubcatsearch" runat="server"  ImageUrl="../../Images/forum_search.gif"
                                                CommandName="search"    />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubCategory" runat="server" Text='<%# Bind("BM_CATEGORYNAME") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Grade">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblgradesearch" runat="server" Text="Grade"></asp:Label>
                                            <br />
                                         
                                            <asp:TextBox ID="txtgradesearch" runat="server" ></asp:TextBox>
                                            <asp:ImageButton ID="imgbtngradesearch" runat="server"  ImageUrl="../../Images/forum_search.gif"
                                                 CommandName="search" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("BCD_GRD_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScore" runat="server" Text='<%# Bind("BM_CATEGORY_SCORE") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField HeaderText="View" Text="View" CommandName="view">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                    </asp:ButtonField>
                                </Columns>
                                <RowStyle CssClass="griditem"  HorizontalAlign="Left"  VerticalAlign="Middle" />
                                <EditRowStyle  />
                                <SelectedRowStyle  />
                                <HeaderStyle  />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                <PagerStyle HorizontalAlign="Center" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

                
            </div>
        </div>
    </div>
</asp:Content>
