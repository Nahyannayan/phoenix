Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data

Partial Class Students_BehaviorManagement_bmIncidentStatus
    Inherits System.Web.UI.Page
    Dim studClass As New studClass

    Protected Sub imgbtnSibling_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            h_SliblingID.Value = txtPar_Sib.Text
            txtPar_Sib.Text = GetStudentNameByNumber(h_SliblingID.Value)
        Catch ex As Exception

        End Try
    End Sub
    'Protected Function GetNavigateUrl(ByVal pId As String) As String
    '    Return String.Format("javascript:var popup = window.showModalDialog('bm_ActionView.aspx?inc_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
    'End Function
    Protected Function GetNavigateUrl(ByVal pId As String) As String
        'Return String.Format("javascript:var popup = window.showModalDialog('bm_ActionViewDetails.aspx?Info_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
        Dim str As String = "ShowWindowWithClose('bm_ActionViewDetails.aspx?Info_id=" + pId + "', 'search', '55%', '85%'); "
        Return str
    End Function

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            Dim strStudentFilter As String
            If txtPar_Sib.Text <> "" Then
                strStudentFilter = " AND C.BM_STU_ID=(SELECT STU_ID FROM STUDENT_M WHERE STU_NO='" & h_SliblingID.Value & "')"
            Else
                strStudentFilter = ""
            End If
            If cmbAccadamicYear.SelectedIndex >= 0 Then


                Dim dtAccadamic As DataTable
                dtAccadamic = AccdaMicYearDetails(cmbAccadamicYear.SelectedValue)
                Dim FromDt As Date = dtAccadamic.Rows(0).Item(0)
                Dim ToDt As Date = dtAccadamic.Rows(0).Item(1)

                Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

                Dim str_query As String
                Dim str1 As String = "SELECT COUNT(*) FROM BM.INCIDENT_ROLE_ACCESS WHERE ROLE_ID=" & Session("sroleid") & " AND BSU_ID=" & Session("sBsuid")
                Dim sbm As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str1)
                If sbm = 0 Then
                    str_query = "SELECT A.BM_ID,BM_ENTRY_DATE,EMP_FNAME,EMP_LNAME,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) As STUDNAME, A.BM_INCIDENT_TYPE, STU_ID " & _
                                 "FROM BM.BM_MASTER A INNER JOIN " & _
                                 "EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID " & _
                                 "INNER JOIN BM.BM_STUDENTSINVOLVED C ON A.BM_ID=C.BM_ID " & _
                                 "INNER JOIN STUDENT_M D ON C.BM_STU_ID =D.STU_ID " & _
                                 " AND STU_BSU_ID=" & Session("sBsuId") & " " & _
                                 " AND CONVERT(datetime,BM_ENTRY_DATE)>= '" & FromDt & "' AND CONVERT(datetime,BM_ENTRY_DATE)<='" & ToDt & "'" & _
                                 " " & strStudentFilter & _
                                 "ORDER BY BM_INCIDENT_DATE DESC "
                    '"ORDER BY BM_ID ASC "
                Else
                    str_query = "SELECT A.BM_ID,BM_ENTRY_DATE,EMP_FNAME,EMP_LNAME,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) As STUDNAME, A.BM_INCIDENT_TYPE, STU_ID " & _
                                 "FROM BM.BM_MASTER A INNER JOIN " & _
                                 "EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID " & _
                                 "INNER JOIN BM.BM_STUDENTSINVOLVED C ON A.BM_ID=C.BM_ID " & _
                                 "INNER JOIN STUDENT_M D ON C.BM_STU_ID =D.STU_ID " & _
                                 " AND STU_BSU_ID=" & Session("sBsuId") & " " & _
                                 " AND CONVERT(datetime,BM_ENTRY_DATE)>= '" & FromDt & "' AND CONVERT(datetime,BM_ENTRY_DATE)<='" & ToDt & "'" & _
                                 " " & strStudentFilter & _
                                 " AND D.STU_GRM_ID " & _
                 "IN(select GRM_ID FROM vw_BSU_TUTOR WHERE SEC_EMP_ID='" & Session("EmployeeId") & "' AND SEC_bsu_id='" & Session("sBsuid") & "')" & "ORDER BY BM_INCIDENT_DATE DESC"

                End If


                ''str_query = "SELECT A.BM_ID,BM_ENTRY_DATE,EMP_FNAME,EMP_LNAME,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) As STUDNAME, A.BM_INCIDENT_TYPE, STU_ID " & _
                ''                "FROM BM.BM_MASTER A INNER JOIN " & _
                ''                "EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID " & _
                ''                "INNER JOIN BM.BM_STUDENTSINVOLVED C ON A.BM_ID=C.BM_ID " & _
                ''                "INNER JOIN STUDENT_M D ON C.BM_STU_ID =D.STU_ID " & _
                ''                " AND STU_BSU_ID=" & Session("sBsuId") & " " & _
                ''                " AND CONVERT(datetime,BM_ENTRY_DATE)>= '" & FromDt & "' AND CONVERT(datetime,BM_ENTRY_DATE)<='" & ToDt & "'" & _
                ''                " " & strStudentFilter & _
                ''                "ORDER BY BM_INCIDENT_DATE DESC "
                ' ''"ORDER BY BM_ID ASC "

                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                GrdView.DataSource = ds

                If ds.Tables(0).Rows.Count = 0 Then
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                    GrdView.DataBind()
                    Dim columnCount As Integer = GrdView.Rows(0).Cells.Count
                    GrdView.Rows(0).Cells.Clear()
                    GrdView.Rows(0).Cells.Add(New TableCell)
                    GrdView.Rows(0).Cells(0).ColumnSpan = columnCount
                    GrdView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    GrdView.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                Else
                    GrdView.DataBind()
                    For Each GrdvRow As GridViewRow In GrdView.Rows
                        If GrdvRow.RowType = DataControlRowType.DataRow Then
                            If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                                DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                                DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                                GrdvRow.ForeColor = Drawing.Color.Blue
                            Else
                                DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                                DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                                GrdvRow.ForeColor = Drawing.Color.Red
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Function AccdaMicYearDetails(ByVal YearId As Integer) As DataTable

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT ACD_STARTDT,ACD_ENDDT  " & _
                        "FROM ACADEMICYEAR_D WHERE ACD_ID=" & YearId & " AND ACD_BSU_ID='" & Session("sBsuid") & "'"

        Dim DSAccadamic As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Return DSAccadamic.Tables(0)
    End Function
    Protected Sub GrdView_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Try
            If e.CommandName = "View" Then
                CallReport()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Function GetAccadamicID(ByVal Year As String) As Integer
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT ACY_ID  " & _
                        "FROM ACADEMICYEAR_M WHERE ACY_DESCR='" & Year & "'"

        Dim DSAccadamic As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Return DSAccadamic.Tables(0).Rows(0).Item(0)
    End Function
    Private Sub CallReport()

        Dim str_bsu_ids As New StringBuilder
        Dim basedate As String = "15-09-" & Now.Year.ToString
        Dim DsAccYear As DataSet

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        'param.Add("@BSU_ID", Session("sBsuid")) '999998--- Corp Office
        'param.Add("@FROM_DATE", Format(Date.Parse(DsAccYear.Tables(0).Rows(0).Item(2)), "dd/MMM/yyyy")) 'Format(Date.Parse(Now.Date), "dd/MMM/yyyy")
        'param.Add("@TO_DATE", Format(Date.Parse(DsAccYear.Tables(0).Rows(0).Item(3)), "dd/MMM/yyyy"))
        'param.Add("USERNAME", Session("sUsr_name"))
        'param.Add("@RPTHEADER", "Academic Year : " + Session("F_Descr").ToString)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            '.reportParameters = param
            .reportPath = Server.MapPath("Reports/rptIncidentDetails.rpt") '../RPT/rptKHDAStatistics.rpt
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub
    Private Sub RetriveAccadamicYear()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query = ""
        str_query = "SELECT ACY_ID,ACY_DESCR FROM dbo.ACADEMICYEAR_M "

        Dim dsACADEMICYEAR As DataSet
        dsACADEMICYEAR = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cmbAccadamicYear.DataSource = dsACADEMICYEAR.Tables(0)
        cmbAccadamicYear.DataTextField = "ACY_DESCR"
        cmbAccadamicYear.DataValueField = "ACY_ID"
        cmbAccadamicYear.DataBind()

    End Sub

    Protected Sub lnkview_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                RetriveAccadamicYear()
                cmbAccadamicYear = studClass.PopulateAcademicYear(cmbAccadamicYear, Session("clm").ToString, Session("sbsuid").ToString)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cmbAccadamicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'GrdView.DataSource = Nothing
    End Sub

    Private Function GetStudentNameByNumber(ByVal StudID As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT STU_NO,(ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'')) As StudentName FROM STUDENT_M WHERE STU_NO='" & StudID & "'"
            Dim dsStaff As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsStaff.Tables(0).Rows.Count >= 1 Then
                Return dsStaff.Tables(0).Rows(0).Item("StudentName")
            Else
                Return ""
            End If
        Catch ex As Exception
        End Try
    End Function

End Class
 
