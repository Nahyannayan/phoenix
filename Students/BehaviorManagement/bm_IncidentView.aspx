<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="bm_IncidentView.aspx.vb" Inherits="Students_BehaviorManagement_bm_IncidentView" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Src="UserControl/bm_IncidentView.ascx" TagName="bm_IncidentView" TagPrefix="uc1" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
<div  align="center">
 <table class="title" width="100%">
                    <tr>
                        <td align="left" class="title-bg">
                            Incident View</td>
                    </tr>
                </table>
<br />
 <uc1:bm_IncidentView ID="Bm_IncidentView1" runat="server" />
 
</div>
</asp:Content> 
