Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System.Xml

Partial Class Students_BehaviorManagement_BMActionRoutingSetup
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        chkboxall.Attributes.Add("onclick", "checkAll(this)")

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'Collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                If USR_NAME = "" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    GetDesignationsSchools()
                    GetDesignations()
                    BindAssignDesig()
                    hfId1.Value = 0
                    ViewState("datamode") = "add"
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

        End If
    End Sub

#Region " Private Functions "
    'For Assigned Designation Combo List
    Private Sub BindAssignDesig()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT DISTINCT DES_ID, DES_DESCR From BM.BM_DESIG_ROUTING A " & _
                                  " INNER JOIN EMPDESIGNATION_M B ON BM_FROM_DESIGID=DES_ID " & _
                                  " WHERE DES_FLAG='SD' AND BSU_ID=" & Session("sBsuid") & "ORDER BY DES_DESCR"
        Dim dsInitiator As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlistboxassigneddesig.DataSource = dsInitiator.Tables(0)
        ddlistboxassigneddesig.DataTextField = "DES_DESCR"
        ddlistboxassigneddesig.DataValueField = "DES_ID"
        ddlistboxassigneddesig.DataBind()
    End Sub
    'To display Designation assigned to selected designation
    Private Sub DispAsgnDesig(ByVal desig As Long)
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = " SELECT DISTINCT BM_TO_DESIGID, DES_DESCR From BM.BM_DESIG_ROUTING A " & _
        '                          " INNER JOIN EMPDESIGNATION_M B ON BM_TO_DESIGID=DES_ID " & _
        '                          " WHERE DES_FLAG='SD' AND BSU_ID=" & Session("sBsuid") & " AND BM_FROM_DESIGID = (SELECT DISTINCT DES_ID FROM BM.BM_DESIG_ROUTING WHERE DES_ID= '" & ddlistboxassigneddesig.Text & "')"
        Dim str_query As String = "SELECT BM_TO_DESIGID, DES_DESCR FROM BM.BM_DESIG_ROUTING INNER JOIN EMPDESIGNATION_M ON BM_TO_DESIGID=DES_ID WHERE BM_FROM_DESIGID=" & desig _
                              & " AND BSU_ID='" + Session("SBSUID") + "'"
        Dim dsDesignation As DataSet
        dsDesignation = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        chkAssigned.DataSource = dsDesignation.Tables(0)
        chkAssigned.DataTextField = "DES_DESCR"
        chkAssigned.DataValueField = "BM_TO_DESIGID"
        chkAssigned.DataBind()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub SaveData()
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                ' To Delete UnSelected Records
                Dim str_DelQuery As String = " DELETE FROM BM.BM_DESIG_ROUTING WHERE BSU_ID=" & Session("sBsuid") & " AND BM_FROM_DESIGID=" & lstFromDesig.SelectedValue & " AND BM_TO_DESIGID NOT IN (" & getAssignedDesig() & ")"
                SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_DelQuery)

                For intCnt As Integer = 0 To chkAssigned.Items.Count - 1
                    If CheckDesignationRouteExists(lstFromDesig.SelectedValue, chkAssigned.Items(intCnt).Value, transaction) = False Then
                        'transaction.Rollback()
                        'lblError.Text = " This List Is already exist!. "
                        'Exit Sub
                        Dim str_query As String = "exec BM.BM_saveDESIGROUTING " + hfId1.Value.ToString + "," & _
       lstFromDesig.SelectedValue + "," + chkAssigned.Items(intCnt).Value & _
       "," + Session("sBsuid") + "," + (intCnt + 1).ToString + ",'" + ViewState("datamode") + "'"

                        hfTemp.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "BM_ROUT_ID(" + hfId1.Value.ToString + ")", IIf(ViewState("datamode") = "add", "Insert", ViewState("datamode")), Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                    End If
                Next
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                BindAssignDesig()
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    'To Bind Designation to "To Designation" Combo List

    Private Sub GetDesignations()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT DES_ID, UPPER(DES_DESCR) AS DES_DESCR " & _
                    " FROM dbo.EMPDESIGNATION_M " & _
                    " INNER JOIN EMPLOYEE_M ON DES_ID=EMP_DES_ID " & _
                    " WHERE DES_FLAG='SD' AND EMP_BSU_ID='" + Session("SBSUID") + "' AND DES_DESCR<>'--' ORDER BY DES_DESCR"

        Dim dsDesignation As DataSet
        dsDesignation = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        chkDesigList.DataSource = dsDesignation.Tables(0)
        chkDesigList.DataTextField = "DES_DESCR"
        chkDesigList.DataValueField = "DES_ID"
        chkDesigList.DataBind()

    End Sub

    'To Bind Designation to "From Designation" Combo List

    Private Sub GetDesignationsSchools()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT DES_ID, UPPER(DES_DESCR) AS DES_DESCR " & _
                    " FROM dbo.EMPDESIGNATION_M " & _
                    " INNER JOIN EMPLOYEE_M ON DES_ID=EMP_DES_ID " & _
                    " WHERE DES_FLAG='SD' AND DES_DESCR<>'--' " & _
                    " AND EMP_BSU_ID='" + Session("SBSUID") + "'" & _
                    " ORDER BY DES_DESCR"

        Dim dsDesignation As DataSet
        dsDesignation = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstFromDesig.DataSource = dsDesignation.Tables(0)
        lstFromDesig.DataTextField = "DES_DESCR"
        lstFromDesig.DataValueField = "DES_ID"
        lstFromDesig.DataBind()

    End Sub

    'Private Sub GetDesignations()

    '    Dim str_conn = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT DES_ID, UPPER(DES_DESCR) AS DES_DESCR " & _
    '                " FROM dbo.EMPDESIGNATION_M " & _
    '                " WHERE DES_FLAG='SD' AND DES_DESCR<>'--'"

    '    Dim dsDesignation As DataSet
    '    dsDesignation = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    '    chkDesigList.DataSource = dsDesignation.Tables(0)
    '    lstFromDesig.DataSource = dsDesignation.Tables(0)
    '    chkDesigList.DataTextField = "DES_DESCR"
    '    lstFromDesig.DataTextField = "DES_DESCR"
    '    chkDesigList.DataValueField = "DES_ID"
    '    lstFromDesig.DataValueField = "DES_ID"
    '    chkDesigList.DataBind()
    '    lstFromDesig.DataBind()

    'End Sub

    'Private Sub ClearForm()
    '    Try
    '        lstFromDesig.SelectedItem.Selected = False
    '        chkAssigned.SelectedItem.Selected = False
    '        chkDesigList.SelectedItem.Selected = False
    '    Catch ex As Exception

    '    End Try
    'End Sub

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If chkAssigned.Items.Count >= 1 Then
                SaveData()
                For Each i As ListItem In chkDesigList.Items
                    i.Selected = False
                Next i
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If lstFromDesig.SelectedIndex >= 0 Then
    '            For intItem As Integer = 0 To chkDesigList.Items.Count - 1
    '                If chkDesigList.Items(intItem).Selected = True Then
    '                    chkDesigList.Items(intItem).Selected = False
    '                End If
    '            Next
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        'getAssignedDesig()
    '    Catch ex As Exception

    '    End Try
    'End Sub


    'To add assigned designation
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If lstFromDesig.SelectedIndex >= 0 Then
                chkAssigned.SelectionMode = ListSelectionMode.Multiple
                For intItem As Integer = 0 To chkDesigList.Items.Count - 1
                    If chkDesigList.Items(intItem).Selected = True Then
                        If chkAssigned.Items.FindByText(chkDesigList.Items.Item(intItem).Text) Is Nothing Then
                            chkAssigned.Items.Add(chkDesigList.Items(intItem))
                        End If
                    End If
                Next
                chkAssigned.SelectedIndex = 0
            End If
            chkAssigned.SelectionMode = ListSelectionMode.Single
        Catch ex As Exception

        End Try
    End Sub
    'To remove assigned designation from the list.
    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            If chkAssigned.SelectedIndex >= 0 Then
                For intItem As Integer = 0 To chkAssigned.Items.Count - 1
                    If chkAssigned.Items(intItem).Selected = True Then
                        chkAssigned.Items.Remove(chkAssigned.Items(intItem))
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Private Function GetDesigantionId(ByVal DesigName As String) As String
    '    Try
    '        Dim str_conn = ConnectionManger.GetOASISConnectionString
    '        Dim str_query As String = "SELECT DES_ID,DES_DESCR " & _
    '                    " FROM dbo.EMPDESIGNATION_M " & _
    '                    " WHERE DES_FLAG='SD' AND DES_DESCR<>'--' AND DES_DESCR='" & DesigName & "'"
    '        Dim dsDesignation As DataSet
    '        dsDesignation = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '        If dsDesignation.Tables(0).Rows.Count >= 1 Then
    '            Return dsDesignation.Tables(0).Rows(0).Item(0).ToString
    '        Else
    '            Return 0
    '        End If
    '    Catch ex As Exception

    '    End Try

    'End Function

    Private Function getAssignedDesig() As String
        Try
            Dim StrAssignedItems As String = ""

            For intCnt As Integer = 0 To chkAssigned.Items.Count - 1
                StrAssignedItems = StrAssignedItems + chkAssigned.Items(intCnt).Value + ","
            Next
            If StrAssignedItems <> "" Then
                If StrAssignedItems.Substring(StrAssignedItems.Length - 1, 1) = "," Then
                    StrAssignedItems = StrAssignedItems.Substring(0, StrAssignedItems.Length - 1)
                End If
            End If
            Return StrAssignedItems
        Catch ex As Exception

        End Try
    End Function

    'Private Function GetDesigantion(ByVal DesigID As Integer) As String
    '    Try
    '        Dim str_conn = ConnectionManger.GetOASISConnectionString
    '        Dim str_query As String = "SELECT DES_ID,DES_DESCR " & _
    '                    " FROM dbo.EMPDESIGNATION_M " & _
    '                    " WHERE DES_FLAG='SD' AND DES_DESCR<>'--' AND DES_ID='" & DesigID & "'"
    '        Dim dsDesignation As DataSet
    '        dsDesignation = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '        If dsDesignation.Tables(0).Rows.Count >= 1 Then
    '            Return dsDesignation.Tables(0).Rows(0).Item("DES_DESCR").ToString
    '        Else
    '            Return 0
    '        End If
    '    Catch ex As Exception

    '    End Try

    'End Function

    Private Function CheckDesignationRouteExists(ByVal FromDesigId As Integer, ByVal ToDesigId As Integer, ByVal Transaction As SqlTransaction) As Boolean
        Try
            Dim str_conn = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT * " & _
                        " FROM BM.BM_DESIG_ROUTING " & _
                        " WHERE BM_FROM_DESIGID=" & FromDesigId & " AND  BM_TO_DESIGID=" & ToDesigId & " AND BSU_ID=" & Session("sBsuid")
            Dim dsDesignation As DataSet
            dsDesignation = SqlHelper.ExecuteDataset(Transaction, CommandType.Text, str_query)
            If dsDesignation.Tables(0).Rows.Count >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try
    End Function

    Protected Sub lstFromDesig_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstFromDesig.SelectedIndexChanged
        Try
            'Dim str_Conn = ConnectionManger.GetOASISConnectionString
            'Dim str_Query = "SELECT BM_ROUT_ID,BM_FROM_DESIGID,BM_TO_DESIGID,BM_FWD_ORDER " & _
            '                "FROM BM.BM_DESIG_ROUTING WHERE BM_FROM_DESIGID=" & lstFromDesig.SelectedValue & _
            '                " AND BSU_ID= " & Session("sBsuid") & " ORDER BY BM_FWD_ORDER"

            'Dim dsDesignation As DataSet
            'dsDesignation = SqlHelper.ExecuteDataset(str_Conn, CommandType.Text, str_Query)
            'chkAssigned.Items.Clear()
            'For IntCnt As Integer = 0 To dsDesignation.Tables(0).Rows.Count - 1
            '    Dim lstitem As New ListItem
            '    lstitem.Text = GetDesigantion(dsDesignation.Tables(0).Rows(IntCnt).Item("BM_TO_DESIGID"))
            '    lstitem.Value = dsDesignation.Tables(0).Rows(IntCnt).Item("BM_TO_DESIGID")
            '    chkAssigned.Items.Add(lstitem)
            'Next
            DispAsgnDesig(lstFromDesig.SelectedValue)
        Catch ex As Exception

        End Try
    End Sub
    'For Displaying assigned designation in the listbox
    Protected Sub btnassigneddesig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnassigneddesig.Click
        DispAsgnDesig(ddlistboxassigneddesig.SelectedValue)
    End Sub
    'For viewing the selected designation in From Designation List
    Protected Sub ddlistboxassigneddesig_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlistboxassigneddesig.SelectedIndexChanged
        lstFromDesig.SelectedValue = ddlistboxassigneddesig.SelectedValue
    End Sub

End Class
