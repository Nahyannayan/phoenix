<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master" CodeFile="bm_CreateNewIncident_SimpleV2.aspx.vb" Inherits="Students_BehaviorManagement_bm_CreateNewIncident_Simple" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <style>
        /** Customize the demo canvas */

        .demo-container label {
            padding-right: 10px;
            width: 100%;
            display: inline-block;
        }

        .demo-container .RadButton {
            margin-top: 20px;
        }

        /** Columns */
        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 100%;
            display: inline-block;
            list-style-type: none;
        }

        .exampleRadComboBox.RadComboBoxDropDown .rcbHeader {
            padding: 5px 27px 4px 7px;
        }

        .rcbScroll {
            overflow: scroll !important;
            overflow-x: hidden !important;
        }

        .col1 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 40%;
            line-height: 14px;
            float: left;
        }

        .col2 {
            margin: 0;
            padding: 0 5px 0 0;
            width: 60%;
            line-height: 14px;
            float: left;
        }


        /** Multiple rows and columns */
        .multipleRowsColumns .rcbItem,
        .multipleRowsColumns .rcbHovered {
            float: left;
            margin: 0 1px;
            min-height: 13px;
            overflow: hidden;
            padding: 2px 19px 2px 6px;
            width: 193px;
        }


        .results {
            display: block;
            margin-top: 20px;
        }

        .RadPicker {
            width: 100% !important;
        }

        .RadComboBox_Default .rcbInput {
            font-family: 'Raleway', sans-serif !important;
            font-size: inherit !important;
            font-style: normal;
        }

        html body .RadInput input.riEmpty, html body .RadInput textarea.riEmpty, html body input.RadInput_Empty {
            font-style: normal;
        }

        .riSingle .riTextBox[type="text"], .RadComboBox_Default .rcbInner {
            font-family: 'Raleway', sans-serif !important;
            display: block !important;
            width: 100% !important;
            padding: 0.375rem 0.75rem !important;
            font-size: 1rem !important;
            line-height: 1.5 !important;
            color: #495057 !important;
            background-color: #ffffff !important;
            background-clip: padding-box !important;
            border: 1px solid #ced4da !important;
            border-radius: 0.25rem !important;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out !important;
            font-size: 1rem !important;
            margin-bottom: 6px !important;
        }

        .riSingle {
            display: block !important;
            width: 100% !important;
        }

        .RadComboBox_Default .rcbEmptyMessage {
            color: #000;
        }

        .RadInput_Default, .RadInputMgr_Default {
            font-size: 14px !important;
        }

        .RadDropDownList {
            width: 100% !important;
        }

        button.rcbActionButton {
            height: 100% !important;
        }

        .form_control {
            display: block !important;
            width: 100% !important;
            padding: 0.375rem 0.75rem !important;
            font-size: 1rem !important;
            line-height: 1.5 !important;
            color: #495057 !important;
            background-color: #ffffff !important;
            background-clip: padding-box !important;
            border: 1px solid #ced4da !important;
            border-radius: 0.25rem !important;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out !important;
            font-size: 14px !important;
        }

        .alert {
            display: block;
        }
    </style>
    <script>
        $(document).ready(function () {
            //$("#alert-popup").hide();
            $("#btnSave").click(function () {
                var a = $(".alert")
                alert(a)
                $(".alert").fadeIN(2000, 500).slideUp(500, function () {
                    $(".alert").remove();
                });
            });
        });
    </script>

    <script >
         function popup(url1) {

             var url = url1;
             //alert(url);
            if ($("#modal").length > 0) {
            }
            else {
                $('<div style="display:none" class="loading" id="modal"></div>').appendTo('body');
            }
            var dialog = $("#modal");
            dialog.dialog({
                width: '90%',
                height: 700,
                title: "Student Details",
                buttons: {
                    Close: function () {
                        $('#modal').dialog('close');
                    }
                },
                modal: true
            });
            dialog.load(
                   url,
                   function (responseText, textStatus, XMLHttpRequest) {
                       dialog.removeClass('loading');
                   }
               );
          }
</script>

    <h2 class="merit-title " style="padding: 4px 4px 4px 10px;">Merit / Demerit</h2>
    <asp:Label ID="lblerror" runat="server" Text=""></asp:Label>

    <div class="container">
        <div class="row card">
            <div class="form-row card-body">
                <div class="form-group col-md-5 mb-0">
                    <telerik:RadDatePicker class="form-control mb-2" RenderMode="Lightweight" runat="server" ID="RadDatePicker1" EmptyMessage="Enter Date" DateInput-EmptyMessage="Enter Date" >
                        <DateInput DisplayDateFormat="dd MMM yyyy, ddd "></DateInput>
                    
                    </telerik:RadDatePicker>
                    <telerik:RadComboBox RenderMode="Lightweight" runat="server" ID="cmbStudent" Width="100%"
                        MarkFirstMatch="true" EnableLoadOnDemand="true"
                        HighlightTemplatedItems="true" OnClientItemsRequested="UpdateItemCountField"
                        OnDataBound="cmbStudent_DataBound" OnItemDataBound="cmbStudent_ItemDataBound"
                        OnItemsRequested="cmbStudent_ItemsRequested" EmptyMessage="Start typing to search...">
                        <HeaderTemplate>
                            <ul>
                                <li class="col1">StudentCode</li>
                                <li class="col2">Student Name</li>
                            </ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <ul>
                                <li class="col1">
                                    <%# DataBinder.Eval(Container.DataItem, "stu_no")%></li>
                                <li class="col2">
                                    <%# DataBinder.Eval(Container.DataItem, "StudentName")%></li>
                            </ul>
                        </ItemTemplate>
                        <FooterTemplate>
                            A total of
                                 <asp:Literal runat="server" ID="RadComboItemsCount" />
                            items
                        </FooterTemplate>
                    </telerik:RadComboBox>
                </div>
                <div class="form-group col-md-5 mb-0">
                    <asp:TextBox ID="txtDescription" class="form-control mb-2" runat="server" Placeholder="Enter Description"></asp:TextBox>
                    <div class="clearfix"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 p-0">
                                <asp:DropDownList RenderMode="Lightweight" ID="ddlCategory" runat="server"
                                    DefaultMessage="Select a Category" DropDownWidth="100%" Width="100%" Style="line-height: 1.5; padding: 6px; border-radius: 6px;">
                                    <Items>
                                        <asp:ListItem Text="Select a category" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Merit" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="De-merit" Value="2"></asp:ListItem>
                                    </Items>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6 p-0">
                                <asp:DropDownList RenderMode="Lightweight" ID="ddlSubCategory" runat="server"
                                    DefaultMessage="Select a Sub Category" DropDownWidth="100%" Width="100%" Style="line-height: 1.5; padding: 6px; border-radius: 6px;">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2">

                    <asp:Button RenderMode="Lightweight" ID="btnSave"  runat="server" Text="Log It" Width="100%" Height="100%" CssClass="button" ClientIDMode="Static"></asp:Button>
                </div>

            </div>
        </div>
    </div>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            function UpdateItemCountField(sender, args) {
                //Set the footer text.
                sender.get_dropDownElement().lastChild.innerHTML = "A total of " + sender.get_items().get_count() + " items";
            }
        </script>
    </telerik:RadScriptBlock>
    <asp:RequiredFieldValidator ID="rfvCategory" runat="server" ControlToValidate="ddlCategory" InitialValue="0"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="rfvSubCategory" runat="server" ControlToValidate="ddlSubCategory" InitialValue="NA"></asp:RequiredFieldValidator>
    <asp:HiddenField ID="hfDynamicStyles" runat="server" ClientIDMode="Static" />

    <div class="container mt-3">
        <div class="row">
            <div class="col-6"><span class="text-success font-weight-bold">My Logs</span></div>
            <div class="col-6 text-right"><span class="text-success font-weight-bold">Recent Logs<i class="fa fa-external-link-square ml-1"></i></span></div>
        </div>
    </div>
    <div id="Comments" runat="server">
    </div>
    <!-- comment starts here -->
      <%--<div class="container">
        <div class="row mt-3">
            <div class="col-6"><span class="font-weight-bold"><a href="#" onclick="popup('/Students/StudPro_details.aspx?id=13100100027368&status=EN'); return false;">Mohammed Zayan Shaib Mitayeegiri</a></span></div>
            <div class="col-6 text-right"><span class="font-weight-bold">Merit</span></div>
        </div>

        <div class="row card">
            <div class="card-body badge-success" style="border-radius: 0.75rem; ">
                Mohammed Zayan Shaib Mitayeegiri won the 2nd position in relay(4 x 100 m).
            </div>
        </div>

        <div class="row">
            <div class="col-6"></div>
            <div class="col-6 text-right">Last Modified Date : 18-03-2018</div>
        </div>

    </div>--%>
    <!-- comment ends here -->




</asp:Content>
