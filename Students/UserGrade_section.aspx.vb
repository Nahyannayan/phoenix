Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class Grade_Section_Access
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "D050028") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Session("USR_GRD_SCT") = CreateDataTable()
                    Session("USR_GRD_SCT").Rows.Clear()
                    ViewState("id") = 1
                    BindBusinessUnits()
                    CURRICULUM_BSU_4_Student()
                    BindTree_Role_User()
                    btnUpdate.Visible = False
                    enabledisable(True)
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If
    End Sub
#Region "USERS"

    Sub BindTree_Role_User() 'for orginating tree view for the grade and section
        Dim BSU_ID As String = String.Empty
        If ddlBsUnits.SelectedIndex = -1 Then
            BSU_ID = ""
        Else
            BSU_ID = ddlBsUnits.SelectedValue
        End If

        Dim dtTable As DataTable = AccessStudentClass.PopulateRole_User(BSU_ID)
        ' PROCESS Filter
        Dim dvGRD_DESCR As New DataView(dtTable, "", "ROL_DESCR", DataViewRowState.OriginalRows)
        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim drROL_DESCR As DataRow
        For i As Integer = 0 To dtTable.Rows.Count - 1
            drROL_DESCR = dtTable.Rows(i)
            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drROL_DESCR("ROL_DESCR") Then
                    contains = True
                End If
            End While
            Dim trNodeROL_DESCR As New TreeNode(drROL_DESCR("ROL_DESCR"), drROL_DESCR("ROL_ID"))
            If contains Then
                Continue For
            End If
            Dim strGRADE_SECT As String = "ROL_DESCR = '" & _
            drROL_DESCR("ROL_DESCR") & "'"
            Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "USR_NAME", DataViewRowState.OriginalRows)
            Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
            While (ienumGRADE_SECT.MoveNext())
                Dim drUSR_ROL As DataRowView = ienumGRADE_SECT.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drUSR_ROL("USR_NAME"), drUSR_ROL("USR_ID"))
                trNodeROL_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeROL_DESCR)
        Next
        tvUserRole.Nodes.Clear()
        tvUserRole.Nodes.Add(trSelectAll)
        tvUserRole.DataBind()

    End Sub

    '    Public Function GetSelectedNode(Optional ByVal seperator As String = "||") As String
    '        Dim strUsers As New StringBuilder
    '        For Each node As TreeNode In tvUserRole.CheckedNodes
    '            If node.Value.Length > 2 Then
    '                strUsers.Append(node.Value)
    '                strUsers.Append(seperator)
    '            End If
    '        Next
    '        Return strUsers.ToString()
    '    End Function
    '    Public Sub ExpandOne(ByVal Level As Integer)
    '        tvUserRole.ExpandDepth = Level
    '        tvUserRole.DataBind()
    '    End Sub
    '    Protected Sub tvUserRole_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvUserRole.TreeNodePopulate
    '        Dim str As String = e.Node.Value
    '        PopulateSubLevel(str, e.Node)
    '    End Sub
    '    Private Sub PopulateSubLevel(ByVal parentid As String, _
    '      ByVal parentNode As TreeNode)
    '        PopulateNodes(GetUSER_S(), parentNode.ChildNodes, True)
    '    End Sub

    '    Private Sub PopulateRootLevel()
    '        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '        Dim str_Sql As String
    '        Dim bsu_id As String = ddlBsUnits.SelectedValue
    '        tvUserRole.Nodes.Clear()
    '        str_Sql = " SELECT distinct ROLES_M.ROL_ID as UserId, ROLES_M.ROL_DESCR as USR_NAME FROM ROLES_M INNER JOIN USERS_M ON  " & _
    '" ROLES_M.ROL_ID = USERS_M.USR_ROL_ID where USERS_M.USR_BSU_ID='" & bsu_id & "' order by ROLES_M.ROL_DESCR "
    '        Dim ds As New DataSet
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '        PopulateNodes(ds.Tables(0), tvUserRole.Nodes, False)
    '    End Sub

    '    Private Sub PopulateNodes(ByVal dt As DataTable, _
    '        ByVal nodes As TreeNodeCollection, ByVal isChild As Boolean)
    '        For Each dr As DataRow In dt.Rows
    '            Dim tn As New TreeNode()
    '            tn.Text = dr("USR_NAME").ToString()
    '            tn.Value = dr("UserId").ToString()
    '            tn.Target = "_self"
    '            tn.NavigateUrl = "javascript:void(0)"
    '            nodes.Add(tn)
    '            If isChild = False Then
    '                tn.PopulateOnDemand = True
    '            End If

    '        Next
    '    End Sub

    '    Private Sub PopulateTree() 'Generate Tree
    '        tvUserRole.Nodes.Clear()
    '        PopulateRootLevel()
    '        tvUserRole.DataBind()
    '        tvUserRole.CollapseAll()
    '    End Sub
    '    Private Function SetSelectedNodes(ByVal strBSUid As String) As Boolean
    '        For i As Integer = 0 To tvUserRole.CheckedNodes.Count - 1
    '            tvUserRole.CheckedNodes(i).Checked = False
    '        Next
    '        Dim IDs As String() = strBSUid.Split("||")
    '        Dim condition As String = String.Empty
    '        For i As Integer = 0 To IDs.Length - 1
    '            CheckSelectedNode(IDs(i), tvUserRole.Nodes)
    '        Next
    '        Return True
    '    End Function

    '    Private Sub CheckSelectedNode(ByVal vBSU_ID As String, ByVal vNodes As TreeNodeCollection)
    '        Dim ienum As IEnumerator = vNodes.GetEnumerator()
    '        Dim trNode As TreeNode
    '        While (ienum.MoveNext())
    '            trNode = ienum.Current
    '            If trNode.ChildNodes.Count > 0 Then
    '                CheckSelectedNode(vBSU_ID, trNode.ChildNodes)
    '            ElseIf trNode.Value = vBSU_ID Then
    '                trNode.Checked = True
    '            End If
    '        End While
    '    End Sub
    '    Private Function GetUSER_S() As DataTable
    '        Dim bsu_id As String = ddlBsUnits.SelectedValue
    '        Dim sql_string As String = String.Empty
    '        sql_string = "SELECT    distinct USERS_M.USR_ID AS UserId, USERS_M.USR_NAME AS USR_NAME FROM ROLES_M INNER JOIN USERS_M ON " & _
    '" ROLES_M.ROL_ID = USERS_M.USR_ROL_ID where USERS_M.USR_BSU_ID='" & bsu_id & "'  order by USERS_M.USR_NAME "

    '        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
    '          CommandType.Text, sql_string)
    '        Return dsData.Tables(0)

    '    End Function
#End Region

#Region "GRADE SECTION"


    Sub BindTree_GradeSection() 'for orginating tree view for the grade and section
        Dim ACD_ID As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            ACD_ID = ""
        Else
            ACD_ID = ddlAca_Year.SelectedItem.Value
        End If

        Dim dtTable As DataTable = AccessStudentClass.PopulateGrade_Section(ACD_ID)
        ' PROCESS Filter
        Dim dvGRD_DESCR As New DataView(dtTable, "", "GRD_DESCR", DataViewRowState.OriginalRows)
        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim drGRD_DESCR As DataRow
        For i As Integer = 0 To dtTable.Rows.Count - 1
            drGRD_DESCR = dtTable.Rows(i)
            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drGRD_DESCR("GRD_DESCR") Then
                    contains = True
                End If
            End While
            Dim trNodeGRD_DESCR As New TreeNode(drGRD_DESCR("GRD_DESCR"), drGRD_DESCR("GRD_ID"))
            If contains Then
                Continue For
            End If
            Dim strGRADE_SECT As String = "GRD_DESCR = '" & _
            drGRD_DESCR("GRD_DESCR") & "'"
            Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "SCT_DESCR", DataViewRowState.OriginalRows)
            Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
            While (ienumGRADE_SECT.MoveNext())
                Dim drGRADE_SECT As DataRowView = ienumGRADE_SECT.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drGRADE_SECT("SCT_DESCR"), drGRADE_SECT("SCT_ID"))
                trNodeGRD_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeGRD_DESCR)
        Next
        tvGRD_SCT.Nodes.Clear()
        tvGRD_SCT.Nodes.Add(trSelectAll)
        tvGRD_SCT.DataBind()

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Page.IsPostBack = False Then
            If Not tvGRD_SCT Is Nothing Then
                DisableAutoPostBack(tvGRD_SCT.Nodes)
                DisableAutoPostBack_ROL(tvUserRole.Nodes)
            End If
        End If


    End Sub
    Sub DisableAutoPostBack_ROL(ByVal tnc As TreeNodeCollection)
        ' Loop over every node in the passed collection 
        For Each tn As TreeNode In tnc
            ' Set the node's NavigateUrl (which equates to A HREF) to javascript:void(0);, 
            ' effectively intercepting the click event and disabling PostBack. 
            tn.NavigateUrl = "javascript:void(0);"

            ' Set the node's SelectAction to Select in order to enable client-side 
            ' expansion/collapsing of parent nodes. Caution: SelectExpand causes 
            ' expand/collapse icon to disappear! 
            tn.SelectAction = TreeNodeSelectAction.[Select]

            ' If this node has children, recurse over them as well before returning 
            If tn.ChildNodes.Count > 0 Then

                DisableAutoPostBack_ROL(tn.ChildNodes)
            End If
        Next
    End Sub




    Sub DisableAutoPostBack(ByVal tnc As TreeNodeCollection)
        ' Loop over every node in the passed collection 
        For Each tn As TreeNode In tnc
            ' Set the node's NavigateUrl (which equates to A HREF) to javascript:void(0);, 
            ' effectively intercepting the click event and disabling PostBack. 
            tn.NavigateUrl = "javascript:void(0);"

            ' Set the node's SelectAction to Select in order to enable client-side 
            ' expansion/collapsing of parent nodes. Caution: SelectExpand causes 
            ' expand/collapse icon to disappear! 
            tn.SelectAction = TreeNodeSelectAction.[Select]

            ' If this node has children, recurse over them as well before returning 
            If tn.ChildNodes.Count > 0 Then

                DisableAutoPostBack(tn.ChildNodes)
            End If
        Next
    End Sub

    Sub SelectAllCheck(ByVal tnc As TreeNodeCollection)
        For Each tn As TreeNode In tnc
            tn.Checked = True

            If tn.ChildNodes.Count > 0 Then
                SelectAllCheck(tn.ChildNodes)
            End If
        Next
    End Sub


#End Region
    Private Sub BindBusinessUnits()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        ddlBsUnits.Items.Clear()

        str_Sql = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_bSHOW=1 AND BUS_BSG_ID NOT IN(4,6) AND BSU_ID NOT IN('300001','800015') ORDER BY BSU_NAME "

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlBsUnits.DataSource = ds.Tables(0)
        ddlBsUnits.DataTextField = "BSU_NAME"
        ddlBsUnits.DataValueField = "BSU_ID"
        ddlBsUnits.DataBind()

        If Not ddlBsUnits.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            ddlBsUnits.ClearSelection()
            ddlBsUnits.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
    End Sub

    Protected Sub ddlBsUnits_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CURRICULUM_BSU_4_Student()
        BindTree_Role_User()
    End Sub
    Sub CURRICULUM_BSU_4_Student()

        Using CURRICULUM_BSU_4_Student_reader As SqlDataReader = AccessStudentClass.GetCURRICULUM_BSU(Session("SBsuid"))
            Dim di_CURRICULUM_BSU As ListItem
            ddlCurri.Items.Clear()
            If CURRICULUM_BSU_4_Student_reader.HasRows = True Then
                While CURRICULUM_BSU_4_Student_reader.Read
                    di_CURRICULUM_BSU = New ListItem(CURRICULUM_BSU_4_Student_reader("CLM_DESCR"), CURRICULUM_BSU_4_Student_reader("CLM_ID"))
                    ddlCurri.Items.Add(di_CURRICULUM_BSU)
                End While

                If ddlCurri.Items.Count > 1 Then
                    clmid.Visible = True

                Else

                    clmid.Visible = False
                End If
                ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            Else
                ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using

    End Sub
    Protected Sub ddlCurri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GetAcademicYear_Bsu()
    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'PopulateTree_GRD_SCT()
        BindTree_GradeSection()
        GetUSER_GRD_SCT()
    End Sub
    Sub GetAcademicYear_Bsu()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim Curri As String = String.Empty
            Dim bsu_id As String = ddlBsUnits.SelectedValue
            If ddlCurri.SelectedIndex = -1 Then
                Curri = ""
            Else
                Curri = ddlCurri.SelectedItem.Value
            End If

            Dim str_Sql As String
            ddlAca_Year.Items.Clear()
            str_Sql = "SELECT   ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR " & _
" FROM  ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
 " where ACADEMICYEAR_D.ACD_BSU_ID='" & bsu_id & "'  and ACADEMICYEAR_D.ACD_CLM_ID='" & Curri & "' "

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAca_Year.DataSource = ds.Tables(0)
            ddlAca_Year.DataTextField = "Y_DESCR"
            ddlAca_Year.DataValueField = "ACD_ID"
            ddlAca_Year.DataBind()
            Call showActive_year()
            ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetAcademicYear_Bsu")
        End Try
    End Sub

    Sub showActive_year()

        Dim Curri As String = String.Empty
        Dim bsu_id As String = ddlBsUnits.SelectedValue

        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        Using readerAcademic_Cutoff As SqlDataReader = AccessStudentClass.GetActive_BsuAndCutOff(bsu_id, Curri)
            If readerAcademic_Cutoff.HasRows = True Then
                While readerAcademic_Cutoff.Read
                    ddlAca_Year.ClearSelection()
                    If Not ddlAca_Year.Items.FindByValue(Convert.ToString(readerAcademic_Cutoff("ACD_ID"))) Is Nothing Then
                        ddlAca_Year.Items.FindByValue(Convert.ToString(readerAcademic_Cutoff("ACD_ID"))).Selected = True
                    End If

                End While
            End If
        End Using
    End Sub



    Private Sub RecurseNodes(ByVal searchValue As String)
        For Each tn As TreeNode In tvUserRole.Nodes

            If tn.Text.ToUpper().Contains(searchValue.ToUpper()) Then
                tn.Text = "<font color='#800000'>" & tn.Text & "</font>"
                tn.Expand()
                tn.Select()
                Exit For
            End If
            If tn.ChildNodes.Count > 0 Then
                For Each cTn As TreeNode In tn.ChildNodes
                    recurseChildren(cTn, searchValue)
                Next
            End If
        Next

    End Sub

    Private Sub recurseChildren(ByVal tn As TreeNode, ByVal searchValue As String)
        If tn.Text.ToUpper().Contains(searchValue.ToUpper()) Then
            tn.Text = "<font color='#800000'>" & tn.Text & "</font>"
            tn.Parent.Expand()
            tn.Expand()
            tn.Select()
            Exit Sub

        End If

        If tn.ChildNodes.Count > 0 Then

            For Each tnC As TreeNode In tn.ChildNodes
                recurseChildren(tnC, searchValue)

            Next

        End If

    End Sub


    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' UncheckTreeView(tvGRD_SCT)
        UncheckTreeView(tvUserRole)

        Call RecurseNodes(txtFind.Text)
    End Sub


    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        enabledisable(True)
        ViewState("datamode") = "add"
        UtilityObj.beforeLoopingControls(Me.Page)
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim str As String = String.Empty
        Dim ACD_ID As String = ddlAca_Year.SelectedValue
        Dim errorMsg As String = String.Empty
        Dim status As Integer

        If Session("USR_GRD_SCT") IsNot Nothing Then
            For i = 0 To Session("USR_GRD_SCT").Rows.count - 1
                If Session("USR_GRD_SCT").Rows(i)("STATUS") = "ADD" Then
                    str += String.Format("<USER GSA_USR_ID='{0}' GSA_SCT_ID='{1}' Status='{2}' GSA_ID='{3}' />", Session("USR_GRD_SCT").Rows(i)("GSA_USR_ID"), Session("USR_GRD_SCT").Rows(i)("GSA_SCT_ID"), Session("USR_GRD_SCT").Rows(i)("STATUS"), Session("USR_GRD_SCT").Rows(i)("GSA_ID"))
                ElseIf (Session("USR_GRD_SCT").Rows(i)("STATUS") <> "old") Then
                    If Not ((Session("USR_GRD_SCT").Rows(i)("STATUS") = "DELETED") And (Session("USR_GRD_SCT").Rows(i)("GSA_ID") = "0")) Then
                        str += String.Format("<USER GSA_USR_ID='{0}' GSA_SCT_ID='{1}' Status='{2}' GSA_ID='{3}' />", Session("USR_GRD_SCT").Rows(i)("GSA_USR_ID"), Session("USR_GRD_SCT").Rows(i)("GSA_SCT_ID"), Session("USR_GRD_SCT").Rows(i)("STATUS"), Session("USR_GRD_SCT").Rows(i)("GSA_ID"))
                    End If

                End If
            Next
            If str <> "" Then
                str = "<USERS>" + str + "</USERS>"
            End If
        Else
            str = ""
        End If
        If str <> "" Then
            status = callsaveGRADE_SECTION_ACCESS(errorMsg, ACD_ID, str)
            If status <> 0 Then
                lblError.Text = errorMsg
            Else
                Session("USR_GRD_SCT").Rows.Clear()

                GetUSER_GRD_SCT()
                ViewState("datamode") = "view"
                enabledisable(False)
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                lblError.Text = "Record saved successfully"
            End If
        Else
            lblError.Text = "Please add/edit the user info !!!"
        End If

    End Sub


    Function callsaveGRADE_SECTION_ACCESS(ByRef errorMessage As String, ByVal ACD_ID As String, ByVal str As String) As Integer
        Dim transaction As SqlTransaction
        Dim status As Integer


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim pParms(8) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@STR_XML", str)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "saveGRADE_SECTION_ACCESS", pParms)
                status = pParms(2).Value

                If status <> 0 Then
                    callsaveGRADE_SECTION_ACCESS = "1"
                    errorMessage = "Error Occured While Saving."
                    UtilityObj.Errorlog("Error while saving records", "callsaveGRADE_SECTION_ACCESS")
                    Return "1"
                End If

                callsaveGRADE_SECTION_ACCESS = "0"

            Catch ex As Exception
                callsaveGRADE_SECTION_ACCESS = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If callsaveGRADE_SECTION_ACCESS <> "0" Then
                    UtilityObj.Errorlog(errorMessage, "callsaveGRADE_SECTION_ACCESS")
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            enabledisable(False)
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            Response.Redirect(ViewState("ReferrerUrl"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub



    Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim GSA_ID As New DataColumn("GSA_ID", System.Type.GetType("System.String"))
            Dim GSA_USR_NAME As New DataColumn("GSA_USR_NAME", System.Type.GetType("System.String"))
            Dim GSA_USR_ID As New DataColumn("GSA_USR_ID", System.Type.GetType("System.String"))
            Dim GSA_SCT_ID As New DataColumn("GSA_SCT_ID", System.Type.GetType("System.String"))
            Dim GRD_SCT As New DataColumn("GRD_SCT", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(GSA_ID)
            dtDt.Columns.Add(GSA_USR_NAME)
            dtDt.Columns.Add(GSA_USR_ID)
            dtDt.Columns.Add(GSA_SCT_ID)
            dtDt.Columns.Add(GRD_SCT)
            dtDt.Columns.Add(STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function
    Sub GetUSER_GRD_SCT()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'if the login user is a teacher then display only the groups for that teacher
        Dim ACD_ID As String = ddlAca_Year.SelectedValue

        Dim pPARAM(4) As SqlClient.SqlParameter
        pPARAM(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)

        Session("USR_GRD_SCT").Rows.Clear()
        Dim gvDataset As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GETGRADE_SECTION_ACCESS", pPARAM)
        If gvDataset.Tables(0).Rows.Count > 0 Then

            ViewState("id") = 1
            For iIndex As Integer = 0 To gvDataset.Tables(0).Rows.Count - 1
                Dim rDt As DataRow
                rDt = Session("USR_GRD_SCT").NewRow
                rDt("SRNO") = ViewState("id")
                rDt("GSA_ID") = gvDataset.Tables(0).Rows(iIndex)("GSA_ID")
                rDt("GSA_USR_NAME") = gvDataset.Tables(0).Rows(iIndex)("GSA_USR_NAME")
                rDt("GSA_USR_ID") = gvDataset.Tables(0).Rows(iIndex)("GSA_USR_ID")
                rDt("GSA_SCT_ID") = gvDataset.Tables(0).Rows(iIndex)("GSA_SCT_ID")
                rDt("GRD_SCT") = gvDataset.Tables(0).Rows(iIndex)("GRD_SCT")
                rDt("STATUS") = gvDataset.Tables(0).Rows(iIndex)("STATUS")
                ViewState("id") = ViewState("id") + 1
                Session("USR_GRD_SCT").Rows.Add(rDt)
            Next
        End If


        gridbind_USR_GRD_SCT()
    End Sub
    Sub gridbind_USR_GRD_SCT()
        Try
            Dim dtTempAtt_GROUP As New DataTable
            dtTempAtt_GROUP = CreateDataTable()

            If Session("USR_GRD_SCT").Rows.Count > 0 Then
                For i As Integer = 0 To Session("USR_GRD_SCT").Rows.Count - 1
                    If Session("USR_GRD_SCT").Rows(i)("STATUS") & "" <> "DELETED" Then
                        Dim rDt As DataRow
                        rDt = dtTempAtt_GROUP.NewRow
                        For j As Integer = 0 To Session("USR_GRD_SCT").Columns.Count - 1
                            rDt.Item(j) = Session("USR_GRD_SCT").Rows(i)(j)
                        Next

                        dtTempAtt_GROUP.Rows.Add(rDt)
                    End If
                Next

            End If



            gvUSR_SCT.DataSource = dtTempAtt_GROUP
            gvUSR_SCT.DataBind()


        Catch ex As Exception

        End Try
    End Sub
    Sub AddUSR_GRD_SCT()
        Try
            'Dim table As DataTable = Session("USR_GRD_SCT").Tables(0)

            '' '' Presuming the DataTable has a column named Date.

            'expression = "GSA_USR_ID=" & node.Value & " AND STATUS <> " & "DELETED"
            'Dim foundRows() As DataRow

            '' '' Use the Select method to find all rows matching the filter.
            'foundRows = table.Select(expression)
            'if foundrows.
            ''Dim i As Integer
            ' '' Print column 0 of each returned row.
            ''For i = 0 To foundRows.GetUpperBound(0)
            ''    Console.WriteLine(foundRows(i)(0))
            ''Next i
            'Dim dtTempAtt_GROUP As New DataTable
            'dtTempAtt_GROUP = CreateDataTable()
            Dim checkedNodes As TreeNodeCollection = tvUserRole.CheckedNodes
            Dim strAddSection As String = String.Empty
            Dim parentId As String = String.Empty
            Dim tempID As String = String.Empty
            Dim expression As String

            If tvUserRole.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In tvUserRole.CheckedNodes

                    If Not node Is Nothing Then
                        If node.Text <> "Select All" Then
                            If node.Parent.Text <> "Select All" Then
                                For i As Integer = 0 To Session("USR_GRD_SCT").Rows.Count - 1
                                    If (Session("USR_GRD_SCT").Rows(i)("GSA_USR_ID") = node.Value) And (Session("USR_GRD_SCT").Rows(i)("STATUS") & "" <> "DELETED") Then
                                        lblError.Text = "User is already added with grade & section !!!"
                                        Exit Sub
                                    End If
                                Next
                            End If
                        End If

                    End If
                Next
            End If

            Dim stringList_usrs As New ListItem
            Dim Sct_ids As String = String.Empty




            Dim checkedNodes_SCT As TreeNodeCollection = tvGRD_SCT.CheckedNodes

            If tvGRD_SCT.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In tvGRD_SCT.CheckedNodes

                    If Not node Is Nothing Then
                        If node.Text <> "Select All" Then
                            If node.Parent.Text <> "Select All" Then
                                If tempID = node.Parent.Text Then
                                    strAddSection = strAddSection + node.Text.Replace("#800000", "#1B80B6") + ","
                                    Sct_ids = Sct_ids + node.Value + "|"
                                Else
                                    strAddSection = strAddSection + "}" + node.Parent.Text + "{" + node.Text.Replace("#800000", "#1B80B6") + ","
                                    Sct_ids = Sct_ids + node.Value + "|"
                                    tempID = node.Parent.Text
                                End If
                            End If
                        End If

                    End If
                Next
                strAddSection = strAddSection.Replace(",}", "},").TrimStart("}") '.Remove(0, 1)
                strAddSection = strAddSection.TrimEnd(",") + "}"
                Sct_ids = Sct_ids.TrimStart("|")
            End If



            If tvUserRole.CheckedNodes.Count > 0 Then
                For Each node As TreeNode In tvUserRole.CheckedNodes

                    If Not node Is Nothing Then
                        If node.Text <> "Select All" Then
                            If node.Parent.Text <> "Select All" Then
                                Dim rDt As DataRow
                                rDt = Session("USR_GRD_SCT").NewRow
                                rDt("SRNO") = ViewState("id")
                                rDt("GSA_ID") = 0
                                rDt("GSA_USR_NAME") = node.Text.Replace("#800000", "#1B80B6")
                                rDt("GSA_USR_ID") = node.Value
                                rDt("GSA_SCT_ID") = Sct_ids
                                rDt("GRD_SCT") = strAddSection
                                rDt("STATUS") = "ADD"
                                ViewState("id") = ViewState("id") + 1
                                Session("USR_GRD_SCT").Rows.Add(rDt)

                            End If
                        End If

                    End If
                Next
            End If






            gridbind_USR_GRD_SCT()
        Catch ex As Exception
            lblError.Text = "Error in adding new teacher to a group"
        End Try
    End Sub


    Sub gvUSR_rowEdting(ByVal GSA_USR_ID As String, ByVal GSA_SCT_ID As String)
        Call checkme(tvUserRole.Nodes, GSA_USR_ID)

        Call checkme(tvGRD_SCT.Nodes, GSA_SCT_ID)
    End Sub

    Public Sub find_node_in_tree(ByVal nod_col As TreeNodeCollection, ByVal val As String, ByVal action As String)

        For Each child As TreeNode In nod_col
            If child.ChildNodes.Count > 0 Then
                find_node_in_tree(child.ChildNodes, val, action)
                
            Else
                If child.Value = val Then
                    Select Case action
                        Case "select"
                            child.Selected = True
                        Case "check"
                            child.Checked = True
                    End Select

                End If

                Dim nodecount As Integer = child.Parent.ChildNodes.Count
                Dim finalcount As Integer = 0
                For Each childs As TreeNode In child.Parent.ChildNodes
                    Dim strvalue As String = childs.Value
                    If childs.Checked = True Then
                        finalcount = finalcount + 1
                    End If
                Next
                If nodecount = finalcount And nodecount <> 0 Then
                    child.Parent.Checked = True
                End If
            End If
        Next
    End Sub
    Private Sub checkme(ByVal nodeList As TreeNodeCollection, ByVal Ids As String)

        Dim arSCT_IDS As String()
        Dim i As Integer

        Dim splitter As Char = "|"

        If Not nodeList Is Nothing Then

            If Ids.Contains("|") Then
                arSCT_IDS = Ids.Split(splitter)

                'Dim nodeList0 As TreeNodeCollection = nodeList

                For i = 0 To arSCT_IDS.Length - 1

                    'For Each nodec As TreeNode In nodeList
                    ''  If Not nodec.Parent Is Nothing Then
                    'Dim strNodeValue As String = nodec.Value
                    Dim strNodeValue As String = arSCT_IDS(i)


                    find_node_in_tree(nodeList, strNodeValue, "check")


                    ''   For Each node As TreeNode In nodec.Parent.ChildNodes
                    'If Array.IndexOf(arSCT_IDS, nodec.Value) <> -1 Then
                    '    nodec.Parent.Expand()
                    '    nodec.Expand()
                    '    nodec.Select()
                    '    nodec.Checked = True
                    '    nodec.Text = "<font color='#800000'>" & nodec.Text.Replace("#1B80B6", "#800000") & "</font>"
                    'End If

                    'checkme(nodec.ChildNodes, Ids)
                    'Next

                Next
            Else
                For Each nodec As TreeNode In nodeList
                    '  If Not nodec.Parent Is Nothing Then
                    Dim strNodeValue As String = nodec.Value
                    '   For Each node As TreeNode In nodec.Parent.ChildNodes
                    If strNodeValue = Ids Then
                        'tvmenu.SelectedNode.Checked
                        ' Label2.Text += nodec.Value & " "
                        nodec.Parent.Expand()
                        nodec.Expand()
                        nodec.Select()
                        nodec.Checked = True

                        nodec.Text = "<font color='#800000'>" & nodec.Text.Replace("#1B80B6", "#800000") & "</font>"
                    End If
                    ' Next
                    '  End If
                    checkme(nodec.ChildNodes, Ids)
                Next

            End If

        End If

    End Sub




    Protected Sub btnAddNEW_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            AddUSR_GRD_SCT()
            UncheckTreeView(tvGRD_SCT)
            UncheckTreeView(tvUserRole)


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvUSR_SCT_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        UncheckTreeView(tvGRD_SCT)
        UncheckTreeView(tvUserRole)

        btnSave.Enabled = False
        btnUpdate.Visible = True
        btnAddNEW.Visible = False
        gvUSR_SCT.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = gvUSR_SCT.Rows(e.NewEditIndex)
        Dim lblSRNO As New Label
        lblSRNO = TryCast(row.FindControl("lblSRNO"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblSRNO.Text)

        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("USR_GRD_SCT").Rows.Count - 1
            If iIndex = Session("USR_GRD_SCT").Rows(iEdit)("SRNO") Then

                Call gvUSR_rowEdting(Session("USR_GRD_SCT").Rows(iEdit)("GSA_USR_ID"), Session("USR_GRD_SCT").Rows(iEdit)("GSA_SCT_ID"))

            End If
        Next
        gvUSR_SCT.Columns(5).Visible = False
        gvUSR_SCT.Columns(6).Visible = False



    End Sub



    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim USR_ID As String = String.Empty
        Dim TEMP_USR_ID As String = String.Empty
        Dim TEMP_USR_Name As String = String.Empty
        btnSave.Enabled = True
        Dim row As GridViewRow = gvUSR_SCT.Rows(gvUSR_SCT.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblSRNO"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view
        Dim CheckCount As Integer = 0

        If tvUserRole.CheckedNodes.Count > 0 Then
            For Each node As TreeNode In tvUserRole.CheckedNodes

                If Not node Is Nothing Then
                    If node.Text <> "Select All" Then
                        If node.Parent.Text <> "Select All" Then
                            CheckCount = CheckCount + 1
                            For i As Integer = 0 To Session("USR_GRD_SCT").Rows.Count - 1
                                USR_ID = Session("USR_GRD_SCT").Rows(i)("GSA_USR_ID")

                                Dim status As String = Session("USR_GRD_SCT").Rows(i)("STATUS") & ""
                                If iIndex <> Session("USR_GRD_SCT").Rows(i)("SRNO") Then
                                    If (status <> "DELETED") And (USR_ID = node.Value) Then
                                        lblError.Text = "User is already added with grade & section !!!"
                                        Exit Sub
                                    End If
                                End If
                            Next
                            TEMP_USR_ID = node.Value
                            'TEMP_USR_Name = node.Text.Replace("#800000", "#1B80B6")

                            TEMP_USR_Name = node.Text.Replace("<font color='#800000'>", "")
                            TEMP_USR_Name = TEMP_USR_Name.Replace("</font>", "")
                        End If
                    End If

                End If
            Next
        End If

        Dim stringList_usrs As New ListItem
        Dim Sct_ids As String = String.Empty
        Dim strAddSection As String = String.Empty
        Dim parentId As String = String.Empty
        Dim tempID As String = String.Empty


        Dim checkedNodes_SCT As TreeNodeCollection = tvGRD_SCT.CheckedNodes

        If tvGRD_SCT.CheckedNodes.Count > 0 Then
            For Each node As TreeNode In tvGRD_SCT.CheckedNodes

                If Not node Is Nothing Then
                    If node.Text <> "Select All" Then
                        If node.Parent.Text <> "Select All" Then
                            If tempID = node.Parent.Text Then
                                strAddSection = strAddSection + node.Text.Replace("#800000", "#1B80B6") + ","
                                Sct_ids = Sct_ids + node.Value + "|"
                            Else
                                strAddSection = strAddSection + "}" + node.Parent.Text + "{" + node.Text.Replace("#800000", "#1B80B6") + ","
                                Sct_ids = Sct_ids + node.Value + "|"
                                tempID = node.Parent.Text
                            End If
                        End If
                    End If

                End If
            Next
            strAddSection = strAddSection.Replace(",}", "},").TrimStart("}") '.Remove(0, 1)
            strAddSection = strAddSection.TrimEnd(",") + "}"
            Sct_ids = Sct_ids.TrimStart("|")
            Sct_ids = Sct_ids.Replace("<font color='#1B80B6'>", "")
            Sct_ids = Sct_ids.Replace("</font>", "")
            strAddSection = strAddSection.Replace("<font color='#1B80B6'>", "")
            strAddSection = strAddSection.Replace("</font>", "")
        End If

        If CheckCount > 1 Then
            lblError.Text = "Can only update single user on edit !!!"
        Else

            For iEdit = 0 To Session("USR_GRD_SCT").Rows.Count - 1
                If iIndex = Session("USR_GRD_SCT").Rows(iEdit)("SRNO") Then

                    Session("USR_GRD_SCT").Rows(iEdit)("GSA_USR_NAME") = TEMP_USR_Name
                    Session("USR_GRD_SCT").Rows(iEdit)("GSA_USR_ID") = TEMP_USR_ID
                    Session("USR_GRD_SCT").Rows(iEdit)("GSA_SCT_ID") = Sct_ids
                    Session("USR_GRD_SCT").Rows(iEdit)("GRD_SCT") = strAddSection
                    If Session("USR_GRD_SCT").Rows(iEdit)("GSA_ID") <> "0" Then
                        Session("USR_GRD_SCT").Rows(iEdit)("STATUS") = "UPDATE"
                    End If
                End If
            Next

            btnUpdate.Visible = False
            btnAddNEW.Visible = True
            gvUSR_SCT.SelectedIndex = -1
            gridbind_USR_GRD_SCT()
            gvUSR_SCT.Columns(5).Visible = True
            gvUSR_SCT.Columns(6).Visible = True

        End If


        UncheckTreeView(tvGRD_SCT)
        UncheckTreeView(tvUserRole)

    End Sub
    Public Sub UncheckTreeView(ByVal tv As System.Web.UI.WebControls.TreeView)
        For Each tn As System.Web.UI.WebControls.TreeNode In tv.Nodes
            tn.Checked = False
            If tn.Text <> "Select All" Then
                If tn.Parent.Text <> "Select All" Then
                    tn.Collapse()
                End If
            End If
            tn.Text = tn.Text.Replace("#800000", "#1B80B6")
            UncheckTreeView(tn)
        Next
    End Sub

    Public Sub UncheckTreeView(ByVal node As System.Web.UI.WebControls.TreeNode)
        For Each tn As System.Web.UI.WebControls.TreeNode In node.ChildNodes
            tn.Checked = False
            If tn.Text <> "Select All" Then
                tn.Collapse()

            End If
            tn.Text = tn.Text.Replace("#800000", "#1B80B6")
            UncheckTreeView(tn)
        Next
    End Sub



    Protected Sub gvUSR_SCT_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)

        Dim COND_ID As Integer = CInt(gvUSR_SCT.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0

        For i = 0 To Session("USR_GRD_SCT").Rows.Count - 1
            If Session("USR_GRD_SCT").rows(i)("SRNO") = COND_ID Then
                If Session("USR_GRD_SCT").rows(i)("GSA_ID") = "0" Then
                    Session("USR_GRD_SCT").Rows(i).Delete()
                Else
                    Session("USR_GRD_SCT").Rows(i)("STATUS") = "DELETED"
                End If
                Exit For
            End If
        Next
        gridbind_USR_GRD_SCT()
    End Sub

    Sub enabledisable(ByVal flag As Boolean)
        ddlBsUnits.Enabled = flag
        ddlCurri.Enabled = flag
        ddlAca_Year.Enabled = flag
        txtFind.Enabled = flag
        btnFind.Enabled = flag
        plUserRole.Enabled = flag
        plGradeSection.Enabled = flag
        btnAddNEW.Enabled = flag
        plGRD.Enabled = flag
    End Sub

    Protected Sub gvUSR_SCT_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblStatus As Label = DirectCast(e.Row.FindControl("lblStatus"), Label)
                Dim imgstatus As Image = DirectCast(e.Row.FindControl("imgstatus"), Image)
                If lblStatus.Text = "UPDATE" Then
                    imgstatus.ImageUrl = "~\Images\update.png"
                ElseIf lblStatus.Text = "ADD" Then
                    imgstatus.ImageUrl = "~\Images\new.png"
                ElseIf lblStatus.Text = "old" Then
                    imgstatus.Visible = False
                Else
                    imgstatus.Visible = False
                End If

                'Dim ddlSTATUS As DropDownList = DirectCast(e.Row.FindControl("ddlStatus"), DropDownList)
                'Dim lblMinList As Label = DirectCast(e.Row.FindControl("lblMinList"), Label)
                'If lblMinList.Text.ToUpper <> "REGULAR" Then
                '    e.Row.BackColor = Drawing.Color.FromArgb(251, 204, 119)
                'End If




            End If

        Catch ex As Exception

        End Try

    End Sub
End Class
