﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.HttpContext

Imports System.Globalization
Imports GemBox.Spreadsheet

Partial Class Dashboard_StudentDashboardstatistics_F
    Inherits BasePage
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            Try
                If Not Request.QueryString("vid") Is Nothing AndAlso Not Request.QueryString("regionId") Is Nothing AndAlso Not Request.QueryString("stat") Is Nothing Then
                    Dim bsuCity As String = Convert.ToString(Request.QueryString("vid"))
                    Dim regionID As String = Convert.ToString(Request.QueryString("regionId"))
                    Dim Stat As String = Convert.ToString(Request.QueryString("stat"))

                    ViewState("bsuCity") = bsuCity
                    ViewState("regionID") = regionID
                    ViewState("Stat") = Stat
                    BindBsuInfo(bsuCity, regionID, Stat)
                    BindActualDashBoard()
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub lnkConsolodated_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        tblSecondTable.Visible = True
        tblFirstTable.Visible = False
        Dim lblBsuId As New Label()
        lblBsuId = TryCast(sender.FindControl("lblBsuId"), Label)
        ViewState("bsuSecondGrid") = lblBsuId.Text
        BindActualExcelReportGrade(lblBsuId.Text)
        BindBsuInfoByGRD(lblBsuId.Text)
        'DownlaodCustomizedExcel(lblBsuId.Text)
    End Sub

    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
        Try
            DownlaodCustomizedExcel(Convert.ToString(ViewState("bsuSecondGrid")))
        Catch ex As Exception
            UtilityObj.Errorlog("btnExportExcel_Click", ex.Message)
        End Try

    End Sub
    'Protected Sub lnkConsolodated2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim lblBsuId As New Label()
    '    lblBsuId = TryCast(sender.FindControl("lblBsuId"), Label)
    '    DownlaodCustomizedExcel(lblBsuId.Text)
    'End Sub
    'Protected Sub lnkCustomised2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim lblBsuId As New Label()
    '    lblBsuId = TryCast(sender.FindControl("lblBsuId"), Label)
    '    DownlaodCustomizedExcel(lblBsuId.Text)
    'End Sub

    Protected Sub lnkCustomised_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        tblSecondTable.Visible = True
        tblFirstTable.Visible = False
        Dim lblBsuId As New Label()
        lblBsuId = TryCast(sender.FindControl("lblBsuId"), Label)
        ViewState("bsuSecondGrid") = lblBsuId.Text
        BindBsuInfoByGRD(lblBsuId.Text)
        BindActualExcelReportGrade(lblBsuId.Text)
        'DownlaodCustomizedExcel(lblBsuId.Text)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        tblSecondTable.Visible = False
        tblFirstTable.Visible = True
    End Sub
    Sub imgExport1_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)


        Dim lblBsuId As New Label()
        lblBsuId = TryCast(sender.FindControl("lblBsuId"), Label)
        ViewState("Actual_BSU") = lblBsuId.Text

        DownloadActualExcelReport(lblBsuId.Text)
    End Sub


    Protected Sub btnExportBsuData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportBsuData.Click
        DownloadExcelReport()
    End Sub

    Protected Sub btnExportActual_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportActual.Click
        DownloadActualExcelReport("0")
    End Sub

    Protected Sub grvConsolidated_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grvConsolidated.PageIndexChanging
        grvConsolidated.PageIndex = e.NewPageIndex
        BindActualDashBoard()

    End Sub

    Protected Sub gvBsuWiseGrid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBsuWiseGrid.PageIndexChanging
        gvBsuWiseGrid.PageIndex = e.NewPageIndex
        BindBsuInfo(ViewState("bsuCity"), ViewState("regionID"), ViewState("Stat"))

    End Sub

    Private Sub BindBsuInfo(ByVal bsuCity As String, ByVal regionID As String, ByVal Stat As String)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim DS As New DataSet
            Dim param(9) As SqlParameter
            Dim param1(1) As SqlParameter
            Dim bsuCityCode As String = String.Empty
            param1(0) = New SqlParameter("@EMR_ID", bsuCity)

            bsuCityCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "CORP.GET_CITY_CODE_BY_ID", param1)

            Dim fromdat As Date = Date.Today
            Dim toDate As Date = Date.Today
            If Not Session("FusionFromDt") Is Nothing Then
                fromdat = Convert.ToDateTime(Session("FusionFromDt"))
            End If

            If Not Session("FusionToDt") Is Nothing Then
                toDate = Convert.ToDateTime(Session("FusionToDt"))
            End If




            param(0) = New SqlParameter("@ACT_LVL", "3")
            param(1) = New SqlParameter("@LVL_BSU", regionID)
            param(2) = New SqlParameter("@FromDate", fromdat)
            param(3) = New SqlParameter("@ToDate", toDate)
            param(4) = New SqlParameter("@BSU_CITY", bsuCityCode)
            param(5) = New SqlParameter("@Stat", Stat)
            param(6) = New SqlParameter("@BSU_ID", Convert.ToString(Session("sBsuid")))
            param(8) = New SqlParameter("@USR_ID", Session("sUsr_id"))
            If Not Session("strAcyId") Is Nothing Then
                param(7) = New SqlParameter("@ACY_IDS", Convert.ToString(Session("strAcyId")))
            End If

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[CORP].[STUDENT_DASHBOARD_STATISTICS]", param)
            gvBsuWiseGrid.DataSource = DS.Tables(0)
            gvBsuWiseGrid.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog("studdashboardpopupbsu", ex.Message)
        End Try
    End Sub


    Private Sub BindBsuInfoByGRD(ByVal bsuId As String)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim DS As New DataSet
            Dim param(7) As SqlParameter
            Dim param1(1) As SqlParameter


            Dim fromdat As Date = Date.Today
            Dim toDate As Date = Date.Today
            If Not Session("FusionFromDt") Is Nothing Then
                fromdat = Convert.ToDateTime(Session("FusionFromDt"))
            End If

            If Not Session("FusionToDt") Is Nothing Then
                toDate = Convert.ToDateTime(Session("FusionToDt"))
            End If




            param(0) = New SqlParameter("@Bsu_id", bsuId)

            param(1) = New SqlParameter("@FromDate", fromdat)
            param(2) = New SqlParameter("@ToDate", toDate)


            If Not Session("strAcyId") Is Nothing Then
                param(3) = New SqlParameter("@ACY_IDS", Convert.ToString(Session("strAcyId")))
            End If

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[CORP].[STUDENT_DASHBOARD_STATISTICS_BY_GRADE_BSU]", param)

            If Not DS.Tables Is Nothing Then
                grvBSUWISEGRD.DataSource = DS.Tables(0)
                grvBSUWISEGRD.DataBind()
            End If


        Catch ex As Exception
            UtilityObj.Errorlog("studdashboardpopupbsuSecondFristGrid", ex.Message)
        End Try
    End Sub

    Private Sub BindActualDashBoard()
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim DS As New DataSet
            Dim param(8) As SqlParameter

            Dim param1(1) As SqlParameter
            Dim bsuCityCode As String = String.Empty
            param1(0) = New SqlParameter("@EMR_ID", ViewState("bsuCity"))

            bsuCityCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "CORP.GET_CITY_CODE_BY_ID", param1)

            param(0) = New SqlParameter("@BSU_ID", "0")
            param(1) = New SqlParameter("@BSU_CITY_CODE", bsuCityCode)
            param(2) = New SqlParameter("@curBsuID", Convert.ToString(Session("sBsuid")))
            param(8) = New SqlParameter("@USR_ID", Session("sUsr_id"))
            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[CORP].[GET_ACTUAL_DASHBOARD_DINO]", param)

            If Not DS.Tables Is Nothing Then

                If DS.Tables(0).Rows.Count > 0 Then
                    grvConsolidated.DataSource = DS.Tables(0)
                    grvConsolidated.DataBind()

                End If

            End If
        Catch ex As Exception
            UtilityObj.Errorlog("studdashboardpopupbsuActual", ex.Message)
        End Try
    End Sub

    Private Sub DownlaodCustomizedExcel(ByVal bsuId As String)

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString


            Dim ds1 As New DataSet
            Dim dtEXCEL As New DataTable
            Dim param(8) As SqlParameter

            Dim fromdat As Date = Date.Today
            Dim toDate As Date = Date.Today
            If Not Session("FusionFromDt") Is Nothing Then
                fromdat = Convert.ToDateTime(Session("FusionFromDt"))
            End If

            If Not Session("FusionToDt") Is Nothing Then
                toDate = Convert.ToDateTime(Session("FusionToDt"))
            End If
            Dim strXmlAcd As String = String.Empty
            If Not Session("strAcyId") Is Nothing Then

                Dim sb As New StringBuilder()
                Dim strAcy As String = Convert.ToString(Session("strAcyId"))
                strAcy = strAcy.Remove(strAcy.Length - 1)
                For Each s As String In strAcy.Split("|"c)

                    sb.AppendFormat("<ID><ACD_ID>{0}</ACD_ID></ID>", s)
                Next
                strXmlAcd = sb.ToString
                strXmlAcd = "<IDS>" + strXmlAcd.ToString + "</IDS>"
            Else
                strXmlAcd = "<IDS><ID><ACD_ID>25</ACD_ID></ID><ID><ACD_ID>26</ACD_ID></ID></IDS>"
            End If

            param(0) = New SqlParameter("@BSU_ID", bsuId)
            param(1) = New SqlParameter("@ACD_XML", strXmlAcd)
            param(2) = New SqlParameter("@FromDate", fromdat)
            param(3) = New SqlParameter("@ToDate", toDate)


            ds1 = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[QRY].[ENQ_ENR_DATA_V3]", param)

            If Not ds1.Tables Is Nothing Then
                dtEXCEL = ds1.Tables(0)

            End If

            If dtEXCEL.Rows.Count > 0 Then
                Dim title As String = String.Empty
                If ds1.Tables(1).Rows(0).Item("SHEET1_NAME").ToString().Length > 30 Then
                    title = ds1.Tables(1).Rows(0).Item("SHEET1_NAME").ToString().Substring(0, 30)
                Else
                    title = ds1.Tables(1).Rows(0).Item("SHEET1_NAME").ToString()
                End If
                Dim Sheet1_Name As String = title
                Dim ws As ExcelWorksheet = ef.Worksheets.Add(Sheet1_Name)
                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                '  ws.HeadersFooters.AlignWithMargins = True
                'For rowLoop As Integer = 1 To 1
                '    For colLoop As Integer = 1 To 10
                '        ws.Cells(rowLoop, colLoop).Style = CellStyle
                '    Next
                'Next


                If ds1.Tables(2).Rows.Count > 0 Then
                    Dim dtEXCEL2 As New DataTable
                    dtEXCEL2 = ds1.Tables(2)

                    If dtEXCEL2.Rows.Count > 0 Then
                        Dim title2 As String = String.Empty
                        If ds1.Tables(3).Rows(0).Item("SHEET2_NAME").ToString().Length > 30 Then
                            title2 = ds1.Tables(3).Rows(0).Item("SHEET2_NAME").ToString().Substring(0, 30)
                        Else
                            title2 = ds1.Tables(3).Rows(0).Item("SHEET2_NAME").ToString()
                        End If
                        Dim Sheet2_Name As String = title2
                        Dim ws2 As ExcelWorksheet = ef.Worksheets.Add(Sheet2_Name)
                        ws2.InsertDataTable(dtEXCEL2, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                        '  ws2.HeadersFooters.AlignWithMargins = True
                    End If
                End If

                If ds1.Tables(4).Rows.Count > 0 Then
                    Dim dtEXCEL3 As New DataTable
                    dtEXCEL3 = ds1.Tables(4)

                    If dtEXCEL3.Rows.Count > 0 Then
                        Dim title3 As String = String.Empty
                        If ds1.Tables(5).Rows(0).Item("SHEET3_NAME").ToString().Length > 30 Then
                            title3 = ds1.Tables(5).Rows(0).Item("SHEET3_NAME").ToString().Substring(0, 30)
                        Else
                            title3 = ds1.Tables(5).Rows(0).Item("SHEET3_NAME").ToString()
                        End If

                        Dim Sheet3_Name As String = title3
                        Dim ws3 As ExcelWorksheet = ef.Worksheets.Add(Sheet3_Name)
                        ws3.InsertDataTable(dtEXCEL3, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                        ' ws3.HeadersFooters.AlignWithMargins = True
                    End If
                End If

                If ds1.Tables(6).Rows.Count > 0 Then
                    Dim dtEXCEL4 As New DataTable
                    dtEXCEL4 = ds1.Tables(6)

                    If dtEXCEL4.Rows.Count > 0 Then
                        Dim title4 As String = String.Empty
                        If ds1.Tables(7).Rows(0).Item("SHEET4_NAME").ToString().Length > 30 Then
                            title4 = ds1.Tables(7).Rows(0).Item("SHEET4_NAME").ToString().Substring(0, 30)
                        Else
                            title4 = ds1.Tables(7).Rows(0).Item("SHEET4_NAME").ToString()
                        End If

                        Dim Sheet4_Name As String = title4
                        Dim ws4 As ExcelWorksheet = ef.Worksheets.Add(Sheet4_Name)
                        ws4.InsertDataTable(dtEXCEL4, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                        ' ws4.HeadersFooters.AlignWithMargins = True
                    End If
                End If


                If ds1.Tables(8).Rows.Count > 0 Then
                    Dim dtEXCEL5 As New DataTable
                    dtEXCEL5 = ds1.Tables(8)

                    If dtEXCEL5.Rows.Count > 0 Then
                        Dim title5 As String = String.Empty
                        If ds1.Tables(9).Rows(0).Item("SHEET5_NAME").ToString().Length > 30 Then
                            title5 = ds1.Tables(9).Rows(0).Item("SHEET5_NAME").ToString().Substring(0, 30)
                        Else
                            title5 = ds1.Tables(9).Rows(0).Item("SHEET5_NAME").ToString()
                        End If

                        Dim Sheet5_Name As String = title5
                        Dim ws5 As ExcelWorksheet = ef.Worksheets.Add(Sheet5_Name)
                        ws5.InsertDataTable(dtEXCEL5, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                        ' ws5.HeadersFooters.AlignWithMargins = True
                    End If
                End If

                'Response.ContentType = "application/vnd.ms-excel"
                ' Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xls")
                Dim bsuFileName As String = String.Empty
                bsuFileName = Get_BSUSHORT_CODE_By_ID(bsuId)
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
                Dim pathSave As String
                pathSave = bsuFileName + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
                ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                Dim bytes() As Byte = File.ReadAllBytes(path)
                'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                Response.BinaryWrite(bytes)
                Response.Flush()
                Response.End()

                'HttpContext.Current.Response.ContentType = "application/octect-stream"
                'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                'HttpContext.Current.Response.Clear()
                'HttpContext.Current.Response.WriteFile(path)
                'HttpContext.Current.Response.End()
            Else
                'lblError.Text = "No Records To display with this filter condition....!!!"
                ' lblError.Focus()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog("ExprortDashboardCustom", ex.Message)
        End Try

    End Sub

    Private Sub DownloadExcelReport()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim params(7) As SqlParameter

            Dim ds As New DataSet
            Dim param(10) As SqlParameter
            Dim param1(1) As SqlParameter
            Dim bsuCityCode As String = String.Empty
            param1(0) = New SqlParameter("@EMR_ID", ViewState("bsuCity"))

            bsuCityCode = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "CORP.GET_CITY_CODE_BY_ID", param1)

            Dim fromdat As Date = Date.Today
            Dim toDate As Date = Date.Today
            If Not Session("FusionFromDt") Is Nothing Then
                fromdat = Convert.ToDateTime(Session("FusionFromDt"))
            End If

            If Not Session("FusionToDt") Is Nothing Then
                toDate = Convert.ToDateTime(Session("FusionToDt"))
            End If




            param(0) = New SqlParameter("@ACT_LVL", "3")
            param(1) = New SqlParameter("@LVL_BSU", ViewState("regionID"))
            param(2) = New SqlParameter("@FromDate", fromdat)
            param(3) = New SqlParameter("@ToDate", toDate)
            param(4) = New SqlParameter("@BSU_CITY", bsuCityCode)
            param(5) = New SqlParameter("@Stat", ViewState("Stat"))
            param(6) = New SqlParameter("@BSU_ID", Convert.ToString(Session("sBsuid")))
            param(8) = New SqlParameter("@USR_ID", Session("sUsr_id"))
            If Not Session("strAcyId") Is Nothing Then
                param(7) = New SqlParameter("@ACY_IDS", Convert.ToString(Session("strAcyId")))
            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[CORP].[STUDENT_DASHBOARD_STATISTICS]", param)




            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile

            Dim dtEXCEL As New DataTable
            If ds.Tables IsNot Nothing Then
                dtEXCEL = ds.Tables(0)


                If dtEXCEL.Rows.Count > 0 Then

                    Dim ws As ExcelWorksheet = ef.Worksheets.Add("DATA_EXPORT")

                    ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    '  ws.HeadersFooters.AlignWithMargins = True
                    ''
                    ws.Cells(0, 0).Value = "Business Unit ID"
                    ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("A").Width = 10000
                    ' ws.Columns("A").Delete()

                    ws.Cells(0, 1).Value = "School"
                    ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("B").Width = 12000

                    ws.Cells(0, 2).Value = "ENQUIRY"
                    ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("C").Width = 5000

                    ws.Cells(0, 3).Value = "OPEN ENQUIRY"
                    ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("D").Width = 7000

                    ws.Cells(0, 4).Value = "REGISTERED"
                    ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("E").Width = 7000

                    ws.Cells(0, 5).Value = "OFFERED"
                    ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("F").Width = 7000

                    ws.Cells(0, 6).Value = "Enrolled"
                    ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("G").Width = 7000


                    'ws.Columns("H").Delete()
                    'ws.Columns("I").Delete()
                    'ws.Columns("J").Delete()
                    'ws.Columns("K").Delete()
                    'ws.Columns("L").Delete()
                    'ws.Columns("M").Delete()
                    'ws.Columns("N").Delete()

                    'ws.Columns("H").Delete()
                    'ws.Columns("I").Delete()
                    'ws.Columns("J").Delete()



                    ''
                    'For i As Integer = 0 To dtEXCEL.Columns.Count - 1
                    '    ws.Cells(0, i).Style.Font.Color = Drawing.Color.White
                    '    ws.Cells(0, i).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    '    ws.Cells(0, i).Style.Font.Weight = ExcelFont.MaxWeight
                    'Next
                    Dim stuFilename As String = "ENQ_CONVERSION" & Today.Now().ToString().Replace("/", "-").Replace(":", "-") & ".xlsx"

                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                    Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
                    Dim pathSave As String
                    pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
                    ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                    Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                    Dim bytes() As Byte = File.ReadAllBytes(path)
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/octect-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                End If
            End If
        Catch ex As Exception
            Dim err As String = ex.Message
        End Try
    End Sub

    Private Sub DownloadActualExcelReport(ByVal bsuId As String)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim DS As New DataSet
            Dim param(5) As SqlParameter

         
            Dim param1(1) As SqlParameter
            Dim bsuCityCode As String = String.Empty
            param1(0) = New SqlParameter("@EMR_ID", ViewState("bsuCity"))

            bsuCityCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "CORP.GET_CITY_CODE_BY_ID", param1)

            param(0) = New SqlParameter("@BSU_ID", "0")
            param(1) = New SqlParameter("@BSU_CITY_CODE", bsuCityCode)
            param(2) = New SqlParameter("@curBsuID", Convert.ToString(Session("sBsuid")))

            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[CORP].[GET_ACTUAL_DASHBOARD_DINO]", param)




            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile

            Dim dtEXCEL As New DataTable
            If DS.Tables IsNot Nothing Then
                dtEXCEL = DS.Tables(0)


                If dtEXCEL.Rows.Count > 0 Then

                    Dim ws As ExcelWorksheet = ef.Worksheets.Add("DATA_EXPORT")

                    ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    ' ws.HeadersFooters.AlignWithMargins = True
                    ''
                    ws.Cells(0, 0).Value = "Business Unit ID"
                    ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("A").Width = 10000
                    ws.Columns.Remove(1)

                    ws.Cells(0, 0).Value = "Business Unit"
                    ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("A").Width = 12000

                    ws.Cells(0, 1).Value = "CURRENT ACTUALS"
                    ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("B").Width = 5000

                    ws.Cells(0, 2).Value = "CURRENT BUDGET"
                    ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("C").Width = 7000

                    ws.Cells(0, 3).Value = "LEAVERS"
                    ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("D").Width = 7000

                    ws.Cells(0, 4).Value = "LAST YR LEAVERS"
                    ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("E").Width = 7000

                    ws.Cells(0, 5).Value = "NET of LEAVERS"
                    ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("F").Width = 7000

                    ws.Cells(0, 6).Value = "NET of LEAVERS PREV"
                    ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("G").Width = 7000

                    ws.Cells(0, 7).Value = "NEW ENROLLMENTS"
                    ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("H").Width = 7000

                    ws.Cells(0, 8).Value = "LIKELY ACTUALS"
                    ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("I").Width = 7000

                    ws.Cells(0, 9).Value = "LIKELY ACTUALS PREV"
                    ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("J").Width = 7000

                    ws.Cells(0, 10).Value = "BUDGET"
                    ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("K").Width = 7000

                    ''
                    'For i As Integer = 0 To dtEXCEL.Columns.Count - 1
                    '    ws.Cells(0, i).Style.Font.Color = Drawing.Color.White
                    '    ws.Cells(0, i).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    '    ws.Cells(0, i).Style.Font.Weight = ExcelFont.MaxWeight
                    'Next
                    Dim stuFilename As String = "ENQ_CONVERSION_ACTUAL" & Today.Now().ToString().Replace("/", "-").Replace(":", "-") & ".xlsx"

                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + stuFilename)
                    Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
                    Dim pathSave As String
                    pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
                    ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
                    Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                    Dim bytes() As Byte = File.ReadAllBytes(path)
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/octect-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                End If
            End If
        Catch ex As Exception
            Dim err As String = ex.Message
        End Try
    End Sub


    Private Sub BindActualExcelReportGrade(ByVal bsuId As String)
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim DS As New DataSet
            Dim param(7) As SqlParameter

            param(0) = New SqlParameter("@BSU_ID", bsuId)


            DS = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "[CORP].[GET_ACTUAL_DASHBOARD_DINO]", param)

            If Not DS.Tables Is Nothing Then
                grvConsolidatedGRD.DataSource = DS.Tables(0)
                grvConsolidatedGRD.DataBind()
            End If


        Catch ex As Exception
            UtilityObj.Errorlog("studdashboardpopupbsuSecondSecGrid", ex.Message)
        End Try
    End Sub

    Private Function Get_City_CODE_By_ID(ByVal bsuCity As String) As String
        Dim cityCode As String = String.Empty
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(24) As SqlParameter
            param(0) = New SqlParameter("@EMR_ID", bsuCity)
            cityCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "CORP.GET_CITY_CODE_BY_ID", param)


        Catch ex As Exception
            UtilityObj.Errorlog("Get_City_CODE_By_ID", ex.Message)
        End Try
        Return cityCode
    End Function

    Private Function Get_BSUSHORT_CODE_By_ID(ByVal bsuID As String) As String
        Dim bsuCode As String = String.Empty
        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@bsu_id_ID", bsuID)
            bsuCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "CORP.GETBSU_SHORTCODE_BYID", param)


        Catch ex As Exception
            UtilityObj.Errorlog("Get_BSUSHORT_CODE_By_ID", ex.Message)
        End Try
        Return bsuCode
    End Function
End Class
