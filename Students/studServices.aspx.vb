Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studServices
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            ' Try

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = "add"
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100015") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                txtName.Text = Encr_decrData.Decrypt(Request.QueryString("name").Replace(" ", "+"))
                txtSen.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                GetACD_ID()
                BindServices()
                GetRows()
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetACD_ID()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_ACD_ID FROM STUDENT_M WHERE STU_ID=" + hfSTU_ID.Value
        hfACD_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim keys() As DataColumn
        ReDim keys(0)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SSV_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SSV_SVC_ID"
        dt.Columns.Add(column)
        keys(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SVC_DESCRIPTION"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SSV_FROMDATE"
        dt.Columns.Add(column)
        
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SSV_TODATE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "bRemove"
        dt.Columns.Add(column)

        dt.PrimaryKey = keys
        Return dt
    End Function

    Private Function SetHisotryDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SSV_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SSV_SVC_ID"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SVC_DESCRIPTION"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SSV_FROMDATE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SSV_TODATE"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "mode"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "bRemove"
        dt.Columns.Add(column)

        Return dt
    End Function

    Sub GetRows()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SSV_ID,SSV_SVC_ID,SVC_DESCRIPTION,SSV_FROMDATE,ISNULL(SSV_TODATE,'2100-01-01') " _
                                 & " FROM STUDENT_SERVICES_D AS A INNER JOIN SERVICES_SYS_M AS B ON A.SSV_SVC_ID=B.SVC_ID " _
                                 & " WHERE SSV_SVC_ID<>1 AND SSV_ACD_ID=" + hfACD_ID.Value + " AND SSV_STU_ID=" + hfSTU_ID.Value

       
        Dim dt As DataTable

        If lnkHistory.Text = "Show History" Then
            str_query += " AND CONVERT(datetime, ISNULL(SSV_TODATE,'2100-01-01')) >= CONVERT(datetime,'" + Format(Now, "yyyy-MM-dd") + "') "
            dt = SetDataTable()
        Else
            str_query += " AND CONVERT(datetime, ISNULL(SSV_TODATE,'2100-01-01')) < CONVERT(datetime,'" + Format(Now, "yyyy-MM-dd") + "') "
            dt = SetHisotryDataTable()
        End If

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        Dim dr As DataRow

        While reader.Read
            dr = dt.NewRow
            With reader
                dr.Item(0) = .GetValue(0).ToString
                dr.Item(1) = .GetValue(1).ToString
                dr.Item(2) = .GetString(2)
                dr.Item(3) = Format(.GetDateTime(3), "dd/MMM/yyyy")
                dr.Item(4) = Format(.GetDateTime(4), "dd/MMM/yyyy").Replace("01/Jan/2100", "")
                dr.Item(5) = "edit"
                dr.Item(6) = "0"
                dt.Rows.Add(dr)
            End With
        End While

        reader.Close()

        gvService.DataSource = dt
        gvService.DataBind()

        Session("dtService") = dt

        If lnkHistory.Text = "Show History" Then
            gvService.Columns(5).Visible = True
        Else
            gvService.Columns(5).Visible = False
        End If
    End Sub

    Sub BindServices()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SVC_ID,SVC_DESCRIPTION FROM SERVICES_SYS_M AS A " _
                                  & " INNER JOIN SERVICES_BSU_M AS B ON A.SVC_ID=B.SVB_SVC_ID " _
                                  & " WHERE SVC_ID<>1 AND SVB_ACD_ID=" + hfACD_ID.Value + " AND SVB_BSU_ID='" + Session("sbsuid") + "' AND SVB_BAVAILABLE=1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlService.DataSource = ds
        ddlService.DataTextField = "SVC_DESCRIPTION"
        ddlService.DataValueField = "SVC_ID"
        ddlService.DataBind()
    End Sub

    Sub AddRows()
        Dim dt As New DataTable
        Dim dr As DataRow
        dt = Session("dtService")
        Dim keys As Object()
        ReDim keys(0)
        keys(0) = ddlService.SelectedValue.ToString


        Dim row As DataRow = dt.Rows.Find(keys)
        If Not row Is Nothing Then
            lblError.Text = "This service is already added"
            Exit Sub
        End If
        dr = dt.NewRow
        dr.Item(0) = 0
        dr.Item(1) = ddlService.SelectedValue.ToString
        dr.Item(2) = ddlService.SelectedItem.Text
        dr.Item(3) = txtDate.Text.ToString.Trim
        dr.Item(4) = ""
        dr.Item(5) = "add"
        dr.Item(6) = "0"
        dt.Rows.Add(dr)

        Session("dtService") = dt
        GridBind(dt)

    End Sub

    Sub GridBind(ByVal dt As DataTable)
        Dim dtTemp As New DataTable
        dtTemp = SetDataTable()
        Dim drTemp As DataRow
        Dim i, j As Integer
        For i = 0 To dt.Rows.Count - 1
            drTemp = dtTemp.NewRow
            If dt.Rows(i)(5) <> "delete" Then
                For j = 0 To dt.Columns.Count - 1
                    drTemp.Item(j) = dt.Rows(i)(j)
                Next
                dtTemp.Rows.Add(drTemp)
            End If
        Next

        gvService.DataSource = dtTemp
        gvService.DataBind()
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        'Try

          lblError.Text = ""
     
        If lnkHistory.Text = "Show History" Then
            RecollectData()
            AddRows()
        Else
            lnkHistory.Text = "Show History"
            GetRows()
            AddRows()
        End If

        RestoreGrid()

        ViewState("SelectedRow") = -1
        btnAddNew.Text = "Add"
        txtDate.Text = ""
        btnSave.Visible = True
        btnCancel.Visible = True
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'End Try
    End Sub


    Sub RecollectData()
        Dim dt As DataTable
        Dim i As Integer
        dt = Session("dtService")
        Dim txtToDate As TextBox
        For i = 0 To gvService.Rows.Count - 1
            txtToDate = gvService.Rows(i).FindControl("txtDate")
            If txtToDate.Text <> "" Then
                dt.Rows(i).Item(4) = txtToDate.Text
            End If
        Next
        Session("dtService") = dt
    End Sub

    Sub RestoreGrid()

        Dim txtToDate As TextBox
        Dim imgToDate As Image
        Dim bRemove As Label
        Dim lblDate As Label

        Dim i As Integer
        For i = 0 To gvService.Rows.Count - 1
            bRemove = gvService.Rows(i).FindControl("bRemove")
            If bRemove.Text = "1" Then
                txtToDate = gvService.Rows(i).FindControl("txtDate")
                imgToDate = gvService.Rows(i).FindControl("imgDate")
                lblDate = gvService.Rows(i).FindControl("lblDate")
                txtToDate.Visible = True
                imgToDate.Visible = True
                txtToDate.Text = lblDate.Text
                lblDate.Visible = False
            End If
        Next

    End Sub
    Protected Sub gvService_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvService.RowCommand
        If e.CommandName = "edit" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvService.Rows(index), GridViewRow)
            Dim keys As Object()
            Dim dt As DataTable
            dt = Session("dtService")

            Dim lblSvcId As Label
            lblSvcId = selectedRow.FindControl("lblSvcId")

            Dim txtToDate As TextBox = selectedRow.FindControl("txtDate")
            Dim imgTodate As Image = selectedRow.FindControl("imgDate")
            Dim lblDate As Label = selectedRow.FindControl("lblDate")

            txtToDate.Visible = True
            imgTodate.Visible = True

            If lblDate.Text <> "" And txtDate.Text = "" Then
                txtDate.Text = lblDate.Text
            End If

            lblDate.Visible = False

            Dim bRemove As Label
            bRemove = selectedRow.FindControl("bRemove")
            ReDim keys(0)

            keys(0) = lblSvcId.Text
            index = dt.Rows.IndexOf(dt.Rows.Find(keys))
            ViewState("SelectedRow") = index

            bRemove.Text = "1"
            dt.Rows(index).Item(6) = "1"

            If dt.Rows(index).Item(5) = "add" Then
                dt.Rows(index).Item(5) = "delete"
                RecollectData()
                GridBind(dt)
                RestoreGrid()
            Else
                dt.Rows(index).Item(5) = "update"
            End If

        End If
    End Sub

    Function SaveData() As Boolean
           Dim svcAddIds As String = ""
        Dim Id As String = ""
        Dim dt As DataTable
        Dim toDate As String

        RecollectData()

        dt = Session("dtService")

        Dim dr As DataRow
        Dim str_query As String
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For Each dr In dt.Rows
                    With dr
                        If .Item(5) = "add" Or .Item(5) = "update" Then
                            If .Item(4) = "" Then
                                toDate = "NULL"
                            Else
                                toDate = "'" + Format(Date.Parse(.Item(4)), "yyyy-MM-dd") + "'"
                            End If
                            str_query = "exec saveSTUDENTSERVICES " _
                                       & "'" + Session("sbsuid") + "'," _
                                       & hfSTU_ID.Value + "," _
                                       & .Item(0).ToString + "," _
                                       & .Item(1).Trim + "," _
                                       & "'" + Format(Date.Parse(.Item(3)), "yyyy-MM-dd") + "'," _
                                       & toDate + "," _
                                       & "'" + .Item(5) + "'"
                            SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)

                        End If

                    End With
                Next
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function


#End Region

    Protected Sub lnkHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkHistory.Click
        If lnkHistory.Text = "Show History" Then
            lnkHistory.Text = "Hide History"
        Else
            lnkHistory.Text = "Show History"
        End If
        GetRows()
    End Sub

    Protected Sub gvService_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvService.RowEditing

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SaveData() = True Then
            GetRows()
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
       
            Response.Redirect(ViewState("ReferrerUrl"))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
End Class
