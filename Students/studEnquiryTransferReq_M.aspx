<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnquiryTransferReq_M.aspx.vb" Inherits="Students_studEnquiryTransferReq_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">



    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Enquiry Transfer Accept
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">

                    <%-- <tr style="font-size: 12pt;">
                <td width="50%" align="left" class="title" style="height: 50px">
                </td>
                </tr>--%>

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td   valign="top">
                            <table align="center"  cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" class="title-bg" colspan="4"  >Shift</td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblText" runat="server" Text="Label" CssClass="field-label"></asp:Label>
                                    </td>
                                  
                                    <td align="left"><asp:Label ID="lblShift" runat="server" Text="Label" CssClass="field-value" ></asp:Label>
                                        <asp:DropDownList ID="ddlShift" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg" colspan="4" >Curriculum</td>
                                </tr>


                                <tr>
                                    <td align="left" width="20%"  ><asp:Label ID="lblCrText" runat="server" CssClass="field-label" Text="Label"></asp:Label></td>
                                    
                                    <td align="left"  ><asp:Label ID="lblCurr" CssClass="field-value" runat="server" Text="Label" ></asp:Label>
                                        <asp:DropDownList ID="ddlCurriculum" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <br /><br />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" class="title-bg" colspan="4" >Transport Details</td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"  ><span class="field-label">Transport Required</span> </td>
                                   
                                    <td align="left"  >
                                        <asp:RadioButton ID="rdTYes" Text="Yes" GroupName="GroupTRS" CssClass="field-label" runat="server" AutoPostBack="True" />
                                        <asp:RadioButton ID="rdTNo" Text="No" GroupName="GroupTRS" runat="server" AutoPostBack="True" CssClass="field-label" />
                                    </td>
                                    <td align="left" width="20%"  ><span class="field-label">Location</span> </td>
                                    
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="left" width="20%"  ><span class="field-label">Sub Location</span> </td>
                                  
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlSubLocation" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"  ><span class="field-label">Pick Up Point </span></td>
                                   
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlPickUp" runat="server" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>


                                <tr>
                                    <td align="left" width="20%"  ><span class="field-label">Remarks</span> </td>
                                    
                                    <td align="left"   >
                                        <asp:TextBox SkinID="MultiText" ID="txtTptRemarks" runat="server"  TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <br /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg" colspan="4" >Other Services</td>
                                </tr>


                                <tr>

                                    <td align="center" colspan="4"  >

                                        <asp:GridView ID="gvServices" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            HeaderStyle-Height="30"  BorderStyle="None">
                                            <Columns>

                                                <asp:TemplateField Visible="False">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSvcId" runat="server" Text='<%# Bind("SVC_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Service">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblService" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:RadioButton ID="rdYes" Text="Yes" runat="server" CssClass="field-label" GroupName='<%# Eval("SVC_ID") %>' />
                                                        <asp:RadioButton ID="rdNo" Text="No" Checked="true" CssClass="field-label" runat="server" GroupName='<%# Eval("SVC_ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                                            <RowStyle CssClass="griditem"  Wrap="False" />
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                            <EmptyDataRowStyle Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" CausesValidation="False" />
                     <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" />
                         <asp:HiddenField ID="hfEQS_ID" runat="server" />
                            <asp:HiddenField ID="hfEQS_TRANSFERID" runat="server" />
                            <asp:HiddenField ID="hfACD_TRANSFERID" runat="server" />
                            <asp:HiddenField ID="hfGRD_ID" runat="server" />
                            <asp:HiddenField ID="hfEQS_ACY_ID" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

