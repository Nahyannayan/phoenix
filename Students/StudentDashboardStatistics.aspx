﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudentDashboardStatistics.aspx.vb" Inherits="Dashboard_StudentDashboardStatistics" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">
    <link href="../cssfiles/Dashboard.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../FusionCharts/fusioncharts.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">


        function showprogress() {
            $('#dvLoading').fadeOut(2000);
        }
        function AddDetails(vid, regionID, stat) {

            $.fancybox({
                type: 'iframe',

                href: 'StudentDashboardstatistics_F.aspx?vid=' + vid + '&regionId=' + regionID + '&stat=' + stat,
                maxHeight: 400,
                fitToView: false,
                width: '950',
                height: '90%',
                autoSize: true,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                hideOnOverlayClick: false,
                hideOnContentClick: false,
                topRatio: 0
            });
        }
    </script>
    <style type="text/css">
        .progMsg_Show {
            position: fixed;
            top: 30%;
            left: 33%;
            text-align: center;
            height: 180px;
            padding: 10px;
            width: 40%;
            z-index: 1001;
            /*font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 17px;*/
            font-style: italic;
            background: transparent;
        }

        #dvLoading {
            background: #000 url(../images/loadingEnqDash.gif) no-repeat center center;
            height: 100px;
            width: 100px;
            position: fixed;
            z-index: 1000;
            left: 50%;
            top: 50%;
            margin: -25px 0 0 -25px;
        }

        .chkListYear label {
            padding-left: 3px;
            vertical-align: top;
            line-height: 20px !important;
        }

        .chkTxtSearch {
            
            border: 1px solid #a9bed8 !important;
            border-collapse: collapse;
            /*font-family: Helvetica,Andale Mono,Sans-Serif,sans;
            font-size: 11px !important;*/
            margin: 1px 0px 1px 0px;
            padding-left: 2px;
            line-height: 20px;
            color: #555152;
            background: #fff url('../Images/arrDown.png') no-repeat 1px 3px !important;
            padding-left: 20px;
        }



        #Progress {
            position: absolute;
            width: 100%;
            z-index: 10000;
        }

        .screenCenter {
            background-color: gray;
            color: white;
            padding-left: 3%;
            z-index: 1000;
        }

        .progBgFilter_Show {
            width: 100%;
            height: 100%;
            position: absolute;
            margin-left: -670px;
            margin-top: -310px;
            background: url(../Images/dark.png) 0 0 !important;
            display: block;
            z-index: 10000;
        }

        .progMsg_Show {
            position: fixed;
            top: 30%;
            left: 33%;
            text-align: center;
            height: 180px;
            padding: 10px;
            width: 40%;
            z-index: 1001;
            /*font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 17px;*/
            font-style: italic;
            background: transparent;
        }
    </style>

</head>
<body>
    <script language="javascript" type="text/javascript">
        function ViewDetails() {

            var Acyid;
            var FromDate;
            var Todate;
            var ActLvl;
            var lvlBSu;

            Acyid = document.getElementById("<%=HDNaCYiD.ClientID %>").value;
            FromDate = document.getElementById("<%=txtIncFromDT.ClientID %>").value;
            Todate = document.getElementById("<%=txtIncDateTo.ClientID %>").value;
            ActLvl = '10';
            lvlBSu = '1';

            $.fancybox({
                type: 'iframe',
                href: 'StudentDashboardstatistics_View.aspx?Acyid=' + Acyid + '&ActLvl=' + ActLvl + '&LvlBsu=' + lvlBSu,
                //maxHeight: 200,
                fitToView: false,
                width: '50%',
                height: '45%',
                autoSize: true,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                hideOnOverlayClick: false,
                hideOnContentClick: false,
                topRatio: 0
            });
        }
        <%--sFeatures = "dialogWidth: 480px; ";
        sFeatures += "dialogHeight: 300px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var Acyid;
        var FromDate;
        var Todate;
        var ActLvl;
        var lvlBSu;
        ActLvl = '10';
        lvlBSu = '1';


        Acyid = document.getElementById("<%=HDNaCYiD.ClientID %>").value;
        FromDate = document.getElementById("<%=txtIncFromDT.ClientID %>").value;
        Todate = document.getElementById("<%=txtIncDateTo.ClientID %>").value;
            url = "StudentDashboardstatistics_View.aspx?Acyid=" + Acyid  + "&ActLvl=" + ActLvl + "&LvlBsu=" + lvlBSu;
        result = window.showModalDialog(url, "", sFeatures);
            
                }--%>

    </script>




    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>

        <div >
            <table width="100%">
                <tr>
                    <td style="vertical-align: middle; width: 20%;"><span class="field-label">From </span></td>
                    <td width="30%"><asp:TextBox ID="txtIncFromDT" runat="server" CssClass="txtDateIR"
                        ToolTip="Incident From Date"></asp:TextBox><asp:ImageButton
                            ID="imgBtnTXTIcDateFrm" runat="server" ImageUrl="~/Images/calendar.gif" ToolTip="Incident From Date" />
                         <ajaxToolkit:TextBoxWatermarkExtender ID="twmFrmDT" runat="server"
                            TargetControlID="txtIncFromdt" WatermarkText="From Date" WatermarkCssClass="watermarked">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                        <ajaxToolkit:CalendarExtender ID="cxIncDateFrm" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgBtnTXTIcDateFrm"
                            TargetControlID="txtIncFromDT">
                        </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="cxIncDateFrm2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtIncFromDT"
                            TargetControlID="txtIncFromDT">
                        </ajaxToolkit:CalendarExtender>
                        </td>
                    <td width="20%">
                      <span class="field-label">  To</span> </td>  
                    <td width="30%">
                        <asp:TextBox ID="txtIncDateTo" runat="server" CssClass="txtDateIR"
                            ToolTip="Incident To Date"></asp:TextBox><asp:ImageButton ID="imgBtnTXTIcDateTo" runat="server" ImageUrl="~/Images/calendar.gif"
                                ToolTip="Incident To Date" />
                    <ajaxToolkit:TextBoxWatermarkExtender ID="twmToDT" runat="server"
                            TargetControlID="txtIncDateTo" WatermarkText="To Date" WatermarkCssClass="watermarked">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                        <ajaxToolkit:CalendarExtender ID="cxIncDateTo" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgBtnTXTIcDateTo"
                            TargetControlID="txtIncDateTo">
                        </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="cxIncDateToC" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtIncDateTo"
                            TargetControlID="txtIncDateTo">
                        </ajaxToolkit:CalendarExtender>
                       
                    </td>
                  
                    <%-- <td style="vertical-align:middle;width:180px;">
                 
                </td>--%>
                    </tr>
                <tr>
                    <td style="text-align: left; width: 20%;"><span class="field-label">Academic Year</span> </td><td>
                    <asp:TextBox ID="txtFilterYear" runat="server" ReadOnly="true" CssClass="chkTxtSearch" ></asp:TextBox>
                        <div id="plYear" runat="server" style="display: none; background-color: white;  text-align: left; overflow-y: auto; overflow-x: hidden;">
                            <asp:CheckBoxList ID="chklACY_IDs" runat="server" RepeatColumns="1" RepeatDirection="Vertical"  ForeColor="#555152" CssClass="chkListYear" AutoPostBack="true"></asp:CheckBoxList>
                        </div>
                        <asp:HiddenField ID="HDNaCYiD" runat="server" />
                        <ajaxToolkit:PopupControlExtender ID="PopEx" runat="server"
                            TargetControlID="txtFilterYear"
                            PopupControlID="plYear"
                            Position="Bottom" />
                    </td>
                    <td colspan="2"></td>
                     </tr>
                <tr>
                    <td style="text-align: center;" colspan="4">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button"  /> 
                        <asp:Button ID="btnResetDate" runat="server" Text="Reset" CssClass="button" /></td>
               
                </tr>
            </table>
        </div>       
        <hr />      
        <div style="display: block; width: 90%; margin-top: 2px; margin-left: 10px; padding: 0; text-align: left; color: #1B80B6;  font-size: 17px; font-weight: bold; letter-spacing: 1px;">
            Enquiry Conversion         
        </div>
        <div class="dbcontainerMain">
            <div class="dbULcontainer">

                <asp:Repeater ID="rptTabDB" runat="server">
                    <HeaderTemplate>
                        <div style="list-style: none; text-align: left; padding: 0px; margin: 0px 8px 0px 0px;">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="dbinactiveCat" id="divTabHolder" runat="server">
                            <asp:HiddenField ID="hfWidthENQ" runat="server" Value='<%# Eval("PER_ENQ_WIDTH")%>' />
                            <asp:HiddenField ID="hfwidthREG" runat="server" Value='<%# Eval("PER_REG_WIDTH")%>' />
                            <asp:HiddenField ID="hfwidthTC" runat="server" Value='<%# Eval("PER_TCS_WIDTH")%>' />

                            <asp:HiddenField ID="hfZoneID" runat="server" Value='<%# Eval("BSU_ID")%>' />
                            <asp:LinkButton ID="lbtnZoneID" runat="server" CssClass="dblinkbtnCat" CommandName='<%# Eval("BSU_NAME")%>' CommandArgument='<%# Eval("BSU_ID")%>'>
                                <div class='<%# Eval("cssLine")%>'></div>
                                <div style="margin-top: 2px; margin-left: 4px;"><%# Eval("BSU_NAME")%></div>
                                <div class="dbCatSubContainer">
                                    <!--Enrolled  -->
                                    <div class='<%# Eval("StuTotCountCSS")%>'><%# Eval("TCS")%></div>
                                    <div class="dbCatSubProgContainer">
                                        <div class="dbprogIR">
                                            <span class="dbprogTitle" style="color: #40C0CB; font-weight: bold;">ENQ</span><span class="dbProgCount"><%# Eval(" ENQS")%></span><div class="barStudENQ"
                                                id="dbbarENQ" runat="server">
                                            </div>
                                        </div>
                                        <div class="dbprogIR">
                                            <span class="dbprogTitle" style="color: #E97F02; font-weight: bold;">REG</span><span class="dbProgCount"><%# Eval("REGS")%></span><div class="barStudREG"
                                                id="dbbarREG" runat="server">
                                            </div>
                                        </div>
                                        <div class="dbprogIR">
                                            <span class="dbprogTitle" style="color: #ED303C; font-weight: bold;">OFR</span><span class="dbProgCount"><%#Eval("STUDS")%></span><div class="barStudTC"
                                                id="dbbarTC" runat="server">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="arrRightInactive" id="divRightArrow" runat="server"></div>
                                </div>


                            </asp:LinkButton>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="dbStudDetailcontainer">
                <div id="divTitle" runat="server" style="border-bottom: 1px solid #88BCE2; text-align: left; color: #1B80B6;  font-size: 15px; font-weight: bold; letter-spacing: 1px; line-height: 24px; padding: 4px 0px 2px 1px; width: 99%;">
                </div>
                <div id="PrintView" class="printImage">
                    <div id="Print">
                        <a href="Javascript:window.print();">
                            <img src="../Images/print.gif" alt="Print" width="20px" height="20px" /></a>
                        <img src="../Images/NewView.ico" alt="View" onclick="ViewDetails();" width="20px" height="20px" />
                    </div>

                    <%--<div id="View">
            <asp:Button ID="btnView" runat="server" Text="View"  CssClass="button"  Width="80px" Height="26px" OnClientClick="ViewDetails();" />

        </div>--%>
                </div>

                <br />
                <br />

                <div style="width: 680px; border: 0px none; padding: 4px 0px 5px 0px; margin: 0px; overflow: hidden;">
                    <div style="border: 0px none; width: 705px; overflow-x: hidden; overflow-y: auto; max-height: 497px; margin: 0px 0px 0px -6px;">
                        <br />



                        <div id="divChart" runat="server" style="padding: 6px 2px 2px 2px; width: 100%;">
                            <table style="width: 100%; margin: 0px; padding: 0px;" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Literal ID="ltPieEnq" runat="server"></asp:Literal></td>
                                    <td>
                                        <asp:Literal ID="ltPieReg" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="ltPieStud" runat="server"></asp:Literal></td>
                                    <td>
                                        <asp:Literal ID="ltPieTC" runat="server"></asp:Literal></td>
                                </tr>
                            </table>

                        </div>

                    </div>
                </div>
                <br />
                <br />
            </div>
        </div>

        <%--<div id="dvLoading"></div>--%>
    </form>
</body>
</html>
