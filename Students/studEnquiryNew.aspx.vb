Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.io
Partial Class Students_studEnquiryNew
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Session("BSU_ENQ_SELECT_MANAGE") = "0"
            Session("CONTACTME_SELECT2") = "ALL"
            Session("PAID_SELECT2") = "ALL"
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100010") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    'code modified by lijo
                    rbOpen.Checked = True
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    If Session("ACD_SELECT") IsNot Nothing Then
                        If ddlAcademicYear.Items.FindByValue(Session("ACD_SELECT")) IsNot Nothing Then
                            ddlAcademicYear.ClearSelection()
                            ddlAcademicYear.Items.FindByValue(Session("ACD_SELECT")).Selected = True
                        End If
                    End If

                    bindPrevSchools()
                    Session("sEnqStatus") = 2
                    'For Each item As ListItem In ddlEnqStatus.Items
                    '    If item.Value = 2 Then
                    '        item.Selected = True
                    '    End If
                    'Next





                    'Dim cb As New CheckBox
                    'For Each gvr As GridViewRow In gvStudEnquiry.Rows
                    '    cb = gvr.FindControl("chkSelect")
                    '    ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    'Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_12.Value = "LI__../Images/operations/like.gif"

                    h_Selected_menu_13.Value = "LI__../Images/operations/like.gif"
                    Dim pParms(3) As SqlClient.SqlParameter
                    Dim lstrPWD As String = String.Empty
                    Dim lstrSelGrade As String = String.Empty


                    pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBsuid"))
                    pParms(1) = New SqlClient.SqlParameter("@FORM_CODE", ViewState("MainMnu_code"))
                    Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BSU_FORM_SORT", pParms)
                        While reader.Read
                            Session("SORTORDER") = Convert.ToString(reader("SORT_TYPE"))
                        End While
                    End Using


                    bind_cur_status()
                    BIND_BSU_DETAILS()
                    GridBind()
                    ShowAccNo()
                    get_Del_visible()
                    If ViewState("isDELPERMIT") = 0 Then
                        btnReject.Visible = False
                    End If

                End If
            Catch ex As Exception
                ' lblError.Text = "Request could not be processed"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else
            highlight_grid()
        End If
    End Sub
    Private Sub get_Del_visible()
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        pParms(1) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "ENQ.GET_NOT_DEL_RIGHTUSERS", pParms)
        If ds.Tables(0).Rows.Count >= 1 Then
            ViewState("isDELPERMIT") = ds.Tables(0).Rows(0).Item("PERMITS")
        End If

    End Sub
    Sub bindPrevSchools()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_CORP")


            ddlPrevSchool_BSU.Items.Clear()
            ddlPrevSchool_BSU.DataSource = ds.Tables(0)
            ddlPrevSchool_BSU.DataTextField = "BSU_NAME"
            ddlPrevSchool_BSU.DataValueField = "BSU_ID"
            ddlPrevSchool_BSU.DataBind()
            ddlPrevSchool_BSU.Items.Add(New ListItem("ALL", "0"))
            ddlPrevSchool_BSU.Items.Add(New ListItem("ALL GEMS", "1"))
            ddlPrevSchool_BSU.Items.Add(New ListItem("ALL NON GEMS", "2"))

            ddlPrevSchool_BSU.ClearSelection()
            If Not ddlPrevSchool_BSU.Items.FindByText("ALL") Is Nothing Then
                ddlPrevSchool_BSU.Items.FindByText("ALL").Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnEnqid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlPaid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPaid.SelectedIndexChanged
        Try

            Session("PAID_SELECT2") = ddlPaid.SelectedItem.Value
            GridBind()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlContactMe_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContactMe.SelectedIndexChanged
        Try

            Session("CONTACTME_SELECT2") = ddlContactMe.SelectedItem.Value
            GridBind()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnEnq_Date_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAppl_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnGender_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
            Session("ACD_SELECT") = ddlAcademicYear.SelectedValue
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlPrevSchool_BSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrevSchool_BSU.SelectedIndexChanged
        Try

            Session("BSU_ENQ_SELECT_MANAGE") = ddlPrevSchool_BSU.SelectedItem.Value
            GridBind()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub ShowAccNo()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT isnull(ACD_bGENFEEID,'true') FROM ACADEMICYEAR_D WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Dim bShow As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If bShow = False Then
            gvStudEnquiry.Columns(3).Visible = True
        Else
            gvStudEnquiry.Columns(3).Visible = True
        End If
    End Sub

    Protected Sub reselectedcheckboxes()
        Dim ENQID As String = String.Empty

        ' string array hiddenIDs to store already checked/selected checkboxes 
        Dim hiddenIDs As String() = New String() {}

        ' if condition to check whether any checkbox has been selected or not since last postback. 
        ' if hiddenCatIDs control has any value then its values are converted into string array by spliting them from '|' 
        If hiddenENQIDs.Value <> String.Empty Then
            hiddenIDs = hiddenENQIDs.Value.Split(New Char() {"|"c})
        End If

        ' ArrayList collection variable arrIDs to store the categoryIDs related to previously checked checkboxes 
        Dim arrIDs As New ArrayList()

        ' if condition to fill the arrayList variable with previosly selected categoryIDs using checkboxes 
        If hiddenIDs.Length <> 0 Then
            ' AddRange method of ArrayList accepts the string array to add the values into its collection. 
            arrIDs.AddRange(hiddenIDs)
        End If

        Dim chk As CheckBox

        For Each rowItem As GridViewRow In gvStudEnquiry.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkSelect")), CheckBox)

            ENQID = gvStudEnquiry.DataKeys(rowItem.RowIndex)("eqm_enqid").ToString()

            ' maintain the checkbox state inside the GridView ItemTemplate column. 
            ' check the categoryID in the arraylist collection if it was being selected by the user before page index changed. 
            If arrIDs.Contains(ENQID) Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If

        Next

    End Sub
    Protected Sub selectedCheckboxes()
        Dim hiddenIDs As String() = New String() {}



        If hiddenENQIDs.Value <> String.Empty Then
            hiddenIDs = hiddenENQIDs.Value.Split(New Char() {"|"c})
        End If



        Dim arrIDs As New ArrayList()



        Dim ENQID As String = "0"



        If hiddenIDs.Length <> 0 Then
            arrIDs.AddRange(hiddenIDs)
        End If



        Dim chk As CheckBox

        For Each rowItem As GridViewRow In gvStudEnquiry.Rows

            chk = DirectCast((rowItem.Cells(0).FindControl("chkSelect")), CheckBox)


            ENQID = gvStudEnquiry.DataKeys(rowItem.RowIndex)("eqm_enqid").ToString()


            If chk.Checked Then
                If Not arrIDs.Contains(ENQID) Then
                    arrIDs.Add(ENQID)
                End If
            Else
                If arrIDs.Contains(ENQID) Then
                    arrIDs.Remove(ENQID)
                End If
            End If


        Next



        ' ArrayList collection converted into string array. 
        hiddenIDs = DirectCast(arrIDs.ToArray(GetType(String)), String())



        ' Join function of string array is used here to create a '|' separated string that can be stored in the hiddenENQIDs control. 
        ' HiddenField Control is used here to maintain the checkbox state while GridView paging. 
        hiddenENQIDs.Value = String.Join("|", hiddenIDs)

        ' Response.Write(hiddenENQIDs.Value)
    End Sub

    Protected Sub lnkApplName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("action") = "select"
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudEnquiry_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudEnquiry.RowCommand
        Try

            If e.CommandName = "Offer" Or e.CommandName = "Approval" Or _
             e.CommandName = "Register" Or e.CommandName = "View" Or e.CommandName = "Screening" Or e.CommandName = "Print" Or e.CommandName = "Followup" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStudEnquiry.Rows(index), GridViewRow)
                Dim eqsid As New Label
                Dim enqid As New Label
                Dim enqno As New Label
                Dim enqApplNo As New Label


                Dim url As String
                'define the datamode to view if view is clicked
                ViewState("datamode") = "edit"
                Session("sRegister") = ""
                'Encrypt the data that needs to be send through Query String
                eqsid = selectedRow.FindControl("lblEqsid")
                enqid = selectedRow.FindControl("lblEnqId")
                enqApplNo = selectedRow.FindControl("lblEnqApplNo")
                ''added by suriya on 25-jan-2010
                Session("enqApplNo") = enqApplNo.Text
                ''over

                Dim lblGrade As Label = selectedRow.FindControl("lblGrade")
                Dim lblShift As Label = selectedRow.FindControl("lblShift")
                Dim lblStream As Label = selectedRow.FindControl("lblStream")
                Dim lblCompReg As Label = selectedRow.FindControl("lblSTG3COMP")
                Dim lblGender As Label = selectedRow.FindControl("lblGender")

                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                'modified by dhanya
                Session("eqm_enqid") = enqid

                Dim editString As String = Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text + "|" + ddlAcademicYear.SelectedValue + "|" + lblGrade.Text + "|" + lblShift.Text + "|" + lblStream.Text)

                ''Redirecting to Offer Page Approval
                If e.CommandName = "Offer" Then
                    Response.Redirect("~/Students/studConfirmOffer.aspx?eqsid=" + Encr_decrData.Encrypt(eqsid.Text) + "&enqid=" + Encr_decrData.Encrypt(enqid.Text))
                End If

                ''Redirecting to Screening Page

                If e.CommandName = "Screening" Then
                    Response.Redirect("~/Students/studirectscreening.aspx?eqsid=" + Encr_decrData.Encrypt(eqsid.Text) + "&enqid=" + Encr_decrData.Encrypt(enqid.Text) + "&editstring=" + editString)
                End If
                ''Redirecting to Followup Page
                If e.CommandName = "Followup" Then

                    Dim eqs_id = Encr_decrData.Encrypt(eqsid.Text)
                    Dim enq_id = Encr_decrData.Encrypt(enqid.Text)
                    Dim accyear = ddlAcademicYear.SelectedItem.Text
                    Dim accyearval = ddlAcademicYear.SelectedValue
                    Dim grade = lblGrade.Text
                    Dim shift = lblShift.Text
                    Dim stream = lblStream.Text
                    Dim gender = lblGender.Text
                    editString = Encr_decrData.Encrypt(accyear + "|" + accyearval + "|" + grade + "|" + shift + "|" + stream)
                    Response.Redirect("~/Students/StuFollowUpDisplay.aspx?eqs_id=" & eqs_id & "&editstring=" & editString & "&enq_id=" & enq_id & "&MainMnu_code=T6V+LqgVMFI=&datamode=Zo4HhpVNpXc=")

                End If

                If e.CommandName = "Approval" Then
                    Response.Redirect("~/Students/studirectAPPROVAL.aspx?eqsid=" + Encr_decrData.Encrypt(eqsid.Text) + "&enqid=" + Encr_decrData.Encrypt(enqid.Text) + "&editstring=" + editString)
                End If

                If e.CommandName = "Register" Then
                    ''mode =1 to show the date
                    Session("sRegister") = "REG"
                    url = String.Format("~\Students\stuEnquiry_Edit.aspx?MainMnu_code={0}&datamode={1}&enqid=" + Encr_decrData.Encrypt(enqid.Text) + "&enqno=" + Encr_decrData.Encrypt(enqApplNo.Text) + "&stgcomp=" + Encr_decrData.Encrypt(lblCompReg.Text) + "&eqsid=" + Encr_decrData.Encrypt(eqsid.Text) + "&editstring=" + editString, ViewState("MainMnu_code"), ViewState("datamode"))
                    Response.Redirect(url)
                End If

                ''stuEnquiry_Edit.aspx
                If e.CommandName = "View" Then
                    url = String.Format("~\Students\stuEnquiry_Edit.aspx?MainMnu_code={0}&datamode={1}&enqid=" + Encr_decrData.Encrypt(enqid.Text) + "&enqno=" + Encr_decrData.Encrypt(enqApplNo.Text) + "&stgcomp=" + Encr_decrData.Encrypt(lblCompReg.Text) + "&eqsid=" + Encr_decrData.Encrypt(eqsid.Text) + "&editstring=" + editString, ViewState("MainMnu_code"), ViewState("datamode"))
                    Response.Redirect(url)
                End If


                If e.CommandName = "Print" Then
                    url = String.Format("../Students/stuEnquiryPrint.aspx?MainMnu_code={0}&datamode={1}&eqsid=" + Encr_decrData.Encrypt(eqsid.Text) _
                                                         & "&enqid=" + Encr_decrData.Encrypt(enqid.Text) + "&accyear=" + Encr_decrData.Encrypt(ddlAcademicYear.SelectedItem.Text) _
                                                        , Encr_decrData.Encrypt(ViewState("MainMnu_code")), ViewState("datamode"))
                    ' Response.Redirect(url)
                    'Dim jscript As New StringBuilder()
                    'jscript.Append("<script>window.open('")
                    'jscript.Append(url)
                    'jscript.Append("');</script>")
                    'Page.RegisterStartupScript("OpenWindows", jscript.ToString())
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" + url + "','_blank');", True)
                End If
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudEnquiry_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudEnquiry.RowDataBound

        Try
            If ViewState("norow") = "yes" Then
                Exit Sub
            End If
            If e.Row.RowType = DataControlRowType.Header Then


                e.Row.Cells(8).Text = ViewState("CutDate")

            ElseIf e.Row.RowType = DataControlRowType.DataRow Then

                ' check if the documents has been submitted for the nextstage

                'if the documents are not submited for the current stage then the next stage will be disabled
                Dim lblCurrStatus As New Label
                Dim lblDocStage As New Label
                Dim enableNextStage As Boolean = True
                Dim lblEQS_bFOLLOWUP As New Label
                Dim lblDocStageOrder As New Label
                Dim lblCurrStageOrder As New Label
                Dim docStageOrder As String
                Dim currStageOrder As String
                Dim lblReason As Label
                Dim lblTooltip As Label
                lblEQS_bFOLLOWUP = e.Row.FindControl("lblEQS_bFOLLOWUP")
                lblReason = e.Row.FindControl("lblReason")
                lblTooltip = e.Row.FindControl("lblTooltip")
                If lblEQS_bFOLLOWUP IsNot Nothing Then

                    If lblEQS_bFOLLOWUP.Text = "True" Then

                        Dim ib As ImageButton = e.Row.Cells(61).Controls(0)

                        If lblReason.Text = "" Then
                            ib.ImageUrl = "~\images\tick.gif"
                        Else
                            ib.ImageUrl = "~\images\" & lblReason.Text & ""
                            ib.ToolTip = lblTooltip.Text
                        End If

                    Else
                        Dim ib As ImageButton = e.Row.Cells(61).Controls(0)
                        ib.ImageUrl = "~\images\cross.gif"
                    End If
                End If



                lblCurrStatus = e.Row.FindControl("lblCurrStatus")
                lblDocStage = e.Row.FindControl("lblDocStage")

                Dim lblNextStage As New Label
                lblNextStage = e.Row.FindControl("lblNextStage")

                'if the applicationdecision is diabled the next stage has to be disabled
                Dim lblAplDis As Label = e.Row.FindControl("lblAplDis")
                Dim lblAplID As Label = e.Row.FindControl("lblAPL_ID")
                Dim lblApl_Image As Label = e.Row.FindControl("lblAPL_Image")
                Dim lblApl_Type As Label = e.Row.FindControl("lblApl_Type")

                If Not lblAplDis Is Nothing Then
                    If lblAplDis.Text.ToUpper = "TRUE" Then
                        enableNextStage = False
                    End If
                End If

                docStageOrder = "lblORD" + lblDocStage.Text  'the stage where the documents are submitted

                currStageOrder = "lblORD" + lblCurrStatus.Text 'current stage of the enquiry

                lblDocStageOrder = e.Row.FindControl(docStageOrder)
                lblCurrStageOrder = e.Row.FindControl(currStageOrder)

                If Not lblCurrStatus Is Nothing Then
                    If lblCurrStatus.Text <> "2" Then
                        If Val(lblCurrStageOrder.Text) > Val(lblDocStageOrder.Text) Then
                            enableNextStage = False
                        End If
                    End If
                End If

                '   === Check The ORD3 To Get What Is There At The 3rd Stage    ====

                Dim lblORD3 As New Label
                Dim lstrORD3 As Integer
                Dim lstrCell3 As Integer
                Dim lstrSTG3REQD As String
                Dim lstrSTG3COMP As String

                lstrSTG3REQD = "lblSTG3REQD"
                lstrSTG3COMP = "lblSTG3COMP"

                lblORD3 = e.Row.FindControl("lblORD3")

                If lblORD3 IsNot Nothing Then

                    Select Case lblORD3.Text

                        Case "3"    '   === Registration

                            lstrORD3 = 4
                            lstrCell3 = 13 + 1
                            lstrSTG3REQD = "lblSTG3REQD"
                            lstrSTG3COMP = "lblSTG3COMP"

                        Case "4"    '   === Screening

                            lstrORD3 = 5
                            lstrCell3 = 14 + 1
                            lstrSTG3REQD = "lblSTG4REQD"
                            lstrSTG3COMP = "lblSTG4COMP"

                        Case "5"    '   === Approval

                            lstrORD3 = 6
                            lstrCell3 = 15 + 1
                            lstrSTG3REQD = "lblSTG5REQD"
                            lstrSTG3COMP = "lblSTG5COMP"

                        Case "6"    '   === Offer Letter

                            lstrORD3 = 7
                            lstrCell3 = 16 + 1
                            lstrSTG3REQD = "lblSTG6REQD"
                            lstrSTG3COMP = "lblSTG6COMP"

                    End Select

                End If

                '   === END 
                '   === Check The ORD3 To Get What Is There At The 3rd Stage    ====

                Dim lblORD4 As New Label
                Dim lstrORD4 As Integer
                Dim lstrCell4 As Integer
                Dim lstrSTG4REQD As String
                Dim lstrSTG4COMP As String

                lstrSTG4REQD = "lblSTG4REQD"
                lstrSTG4COMP = "lblSTG4COMP"

                lblORD4 = e.Row.FindControl("lblORD4")

                If lblORD4 IsNot Nothing Then

                    Select Case lblORD4.Text

                        Case "3"    '   === Registration

                            lstrORD4 = 4
                            lstrCell4 = 13 + 1
                            lstrSTG4REQD = "lblSTG3REQD"
                            lstrSTG4COMP = "lblSTG3COMP"

                        Case "4"    '   === Screening

                            lstrORD4 = 5
                            lstrCell4 = 14 + 1
                            lstrSTG4REQD = "lblSTG4REQD"
                            lstrSTG4COMP = "lblSTG4COMP"

                        Case "5"    '   === Approval

                            lstrORD4 = 6
                            lstrCell4 = 15 + 1
                            lstrSTG4REQD = "lblSTG5REQD"
                            lstrSTG4COMP = "lblSTG5COMP"

                        Case "6"    '   === Offer Letter

                            lstrORD4 = 7
                            lstrCell4 = 16 + 1
                            lstrSTG4REQD = "lblSTG6REQD"
                            lstrSTG4COMP = "lblSTG6COMP"

                    End Select

                End If



                '   === END 

                '   === Check The ORD3 To Get What Is There At The 3rd Stage    ====

                Dim lblORD5 As New Label
                Dim lstrORD5 As Integer
                Dim lstrCell5 As Integer
                lblORD5 = e.Row.FindControl("lblORD5")

                Dim lstrSTG5REQD As String
                Dim lstrSTG5COMP As String

                lstrSTG5REQD = "lblSTG5REQD"
                lstrSTG5COMP = "lblSTG5COMP"


                If lblORD5 IsNot Nothing Then

                    Select Case lblORD5.Text

                        Case "3"    '   === Registration

                            lstrORD5 = 5
                            lstrCell5 = 13 + 1
                            lstrSTG5REQD = "lblSTG3REQD"
                            lstrSTG5COMP = "lblSTG3COMP"

                        Case "4"    '   === Screening

                            lstrORD5 = 6
                            lstrCell5 = 14 + 1
                            lstrSTG5REQD = "lblSTG4REQD"
                            lstrSTG5COMP = "lblSTG4COMP"

                        Case "5"    '   === Approval

                            lstrORD5 = 7
                            lstrCell5 = 15 + 1
                            lstrSTG5REQD = "lblSTG5REQD"
                            lstrSTG5COMP = "lblSTG5COMP"

                        Case "6"    '   === Offer Letter

                            lstrORD5 = 8
                            lstrCell5 = 16 + 1
                            lstrSTG5REQD = "lblSTG6REQD"
                            lstrSTG5COMP = "lblSTG6COMP"

                    End Select

                End If

                '   === END 
                '   === Check The ORD3 To Get What Is There At The 3rd Stage    ====

                Dim lblORD6 As New Label
                Dim lstrORD6 As Integer
                Dim lstrCell6 As Integer
                lblORD6 = e.Row.FindControl("lblORD6")

                Dim lstrSTG6REQD As String
                Dim lstrSTG6COMP As String

                lstrSTG6REQD = "lblSTG6REQD"
                lstrSTG6COMP = "lblSTG6COMP"

                If lblORD6 IsNot Nothing Then

                    Select Case lblORD6.Text

                        Case "3"    '   === Registration

                            lstrORD6 = 4
                            lstrCell6 = 13 + 1
                            lstrSTG6REQD = "lblSTG3REQD"
                            lstrSTG6COMP = "lblSTG3COMP"

                        Case "4"    '   === Screening

                            lstrORD6 = 5
                            lstrCell6 = 14 + 1
                            lstrSTG6REQD = "lblSTG4REQD"
                            lstrSTG6COMP = "lblSTG4COMP"

                        Case "5"    '   === Approval

                            lstrORD6 = 6
                            lstrCell6 = 15 + 1
                            lstrSTG6REQD = "lblSTG5REQD"
                            lstrSTG6COMP = "lblSTG5COMP"

                        Case "6"    '   === Offer Letter

                            lstrORD6 = 7
                            lstrCell6 = 16 + 1
                            lstrSTG6REQD = "lblSTG6REQD"
                            lstrSTG6COMP = "lblSTG6COMP"

                    End Select

                End If

                '   === END 
                ' === Check if Screening is required    ===

                Dim lblREQDSTG3 As New Label
                Dim lblCOMPSTG3 As New Label

                lblREQDSTG3 = e.Row.FindControl(lstrSTG3REQD)
                lblCOMPSTG3 = e.Row.FindControl(lstrSTG3COMP)

                If lblREQDSTG3 IsNot Nothing Then

                    If lblREQDSTG3.Text = 0 Then

                        Dim ib As ImageButton = e.Row.Cells(lstrCell3).Controls(0)
                        ib.ImageUrl = "~\images\cross.gif"

                        '   === Enable If Screening & Approval is there and over    ===

                        e.Row.Cells(lstrCell3).Enabled = False
                        e.Row.Cells(lstrCell3).ToolTip = "This stage is not required"
                    Else

                        If lblCOMPSTG3.Text = 0 Then

                            Dim ib As ImageButton = e.Row.Cells(lstrCell3).Controls(0)


                            ib.ImageUrl = "~\images\cross.gif"

                            If enableNextStage = True Then
                                e.Row.Cells(lstrCell3).Enabled = True
                            End If

                            e.Row.Cells(lstrCell4).Enabled = False
                            e.Row.Cells(lstrCell4).ToolTip = "Formalities for previous stage is not completed"
                            e.Row.Cells(lstrCell5).Enabled = False
                            e.Row.Cells(lstrCell5).ToolTip = "Formalities for previous stage is not completed"
                            e.Row.Cells(lstrCell6).Enabled = False
                            e.Row.Cells(lstrCell6).ToolTip = "Formalities for previous stage is not completed"

                        Else

                            Dim ib As ImageButton = e.Row.Cells(lstrCell3).Controls(0)
                            ib.ImageUrl = "~\images\tick.gif"
                            'If Val(lblORD3.Text) > Val(lblDocStageOrder.Text) Then
                            '    ib.ImageUrl = "~\images\tickDP.gif"
                            'Else
                            '    ib.ImageUrl = "~\images\tick.gif"
                            'End If


                            e.Row.Cells(lstrCell3).Enabled = True

                        End If

                    End If

                End If



                ' === Check if Screening is required    ===

                Dim lblREQDSTG4 As New Label
                Dim lblCOMPSTG4 As New Label

                lblREQDSTG4 = e.Row.FindControl(lstrSTG4REQD)
                lblCOMPSTG4 = e.Row.FindControl(lstrSTG4COMP)

                If lblREQDSTG4 IsNot Nothing Then

                    If lblREQDSTG4.Text = 0 Then

                        Dim ib As ImageButton = e.Row.Cells(lstrCell4).Controls(0)
                        ib.ImageUrl = "~\images\cross.gif"
                        e.Row.Cells(lstrCell4).Enabled = False
                        e.Row.Cells(lstrCell4).ToolTip = "This stage is not required"
                    Else

                        If lblCOMPSTG4.Text = 0 Then

                            Dim ib As ImageButton = e.Row.Cells(lstrCell4).Controls(0)

                            ib.ImageUrl = "~\images\cross.gif"

                            If lblCOMPSTG3.Text = 1 And lblREQDSTG3.Text = 1 Then
                                If enableNextStage = True Then
                                    e.Row.Cells(lstrCell4).Enabled = True
                                Else
                                    e.Row.Cells(lstrCell4).Enabled = False
                                    lblNextStage.Text = lstrCell4.ToString
                                    e.Row.Cells(lstrCell4).ToolTip = "Please submit the documents for the previous stage to proceed to the next stage"
                                End If
                            ElseIf lblCOMPSTG3.Text = 0 Then

                                e.Row.Cells(lstrCell4).Enabled = False
                                e.Row.Cells(lstrCell4).ToolTip = "Formalities for previous stage is not completed"
                            End If

                        Else

                            Dim ib As ImageButton = e.Row.Cells(lstrCell4).Controls(0)
                            If lblAplID.Text <> "0" And lblApl_Type.Text = "SCR" Then
                                ib.ImageUrl = Convert.ToString(lblApl_Image.Text)
                                If ib.ImageUrl = "~\images\tick.gif" Then
                                    e.Row.Cells(lstrCell4).Enabled = False
                                Else
                                    e.Row.Cells(lstrCell5).Enabled = True
                                End If
                            Else
                                ib.ImageUrl = "~\images\tick.gif"
                                e.Row.Cells(lstrCell4).Enabled = False

                            End If

                            'If Val(lblORD4.Text) > Val(lblDocStageOrder.Text) Then
                            '    ib.ImageUrl = "~\images\tickDP.gif"
                            'Else
                            '    ib.ImageUrl = "~\images\tick.gif"
                            'End If





                        End If

                    End If

                End If



                Dim lblREQDSTG5 As New Label

                Dim lblCOMPSTG5 As New Label

                lblREQDSTG5 = e.Row.FindControl(lstrSTG5REQD)

                lblCOMPSTG5 = e.Row.FindControl(lstrSTG5COMP)

                If lblREQDSTG5 IsNot Nothing Then

                    If lblREQDSTG5.Text = 0 Then

                        Dim ib As ImageButton = e.Row.Cells(lstrCell5).Controls(0)

                        ib.ImageUrl = "~\images\cross.gif"

                        e.Row.Cells(lstrCell5).Enabled = False
                        e.Row.Cells(lstrCell5).ToolTip = "This stage is not required"
                    Else

                        If lblCOMPSTG5.Text = 0 Then

                            Dim ib As ImageButton = e.Row.Cells(lstrCell5).Controls(0)

                            ib.ImageUrl = "~\images\cross.gif"

                            e.Row.Cells(lstrCell5).Enabled = False

                            If lblCOMPSTG4.Text = 1 And lblREQDSTG4.Text = 1 Then
                                If enableNextStage = True Then
                                    e.Row.Cells(lstrCell5).Enabled = True
                                Else
                                    e.Row.Cells(lstrCell5).Enabled = False
                                    lblNextStage.Text = lstrCell5.ToString
                                    e.Row.Cells(lstrCell5).ToolTip = "Please submit the documents for the previous stage to proceed to the next stage"
                                End If
                            ElseIf lblCOMPSTG4.Text = 0 Then

                                e.Row.Cells(lstrCell5).Enabled = False
                                e.Row.Cells(lstrCell5).ToolTip = "Formalities for previous stage is not completed"
                            End If



                        Else

                            Dim ib As ImageButton = e.Row.Cells(lstrCell5).Controls(0)

                            'If Val(lblORD5.Text) > Val(lblDocStageOrder.Text) Then
                            '    ib.ImageUrl = "~\images\tickDP.gif"
                            'Else
                            '    ib.ImageUrl = "~\images\tick.gif"
                            'End If
                            If lblAplID.Text <> "0" And lblApl_Type.Text = "APR" Then
                                ib.ImageUrl = Convert.ToString(lblApl_Image.Text)
                                If ib.ImageUrl = "~\images\tick.gif" Then
                                    e.Row.Cells(lstrCell5).Enabled = False
                                Else
                                    e.Row.Cells(lstrCell5).Enabled = True
                                End If
                            Else
                                ib.ImageUrl = "~\images\tick.gif"
                                e.Row.Cells(lstrCell5).Enabled = False

                            End If



                        End If

                    End If

                End If





                Dim lblREQDSTG6 As New Label

                Dim lblCOMPSTG6 As New Label

                lblREQDSTG6 = e.Row.FindControl(lstrSTG6REQD)

                lblCOMPSTG6 = e.Row.FindControl(lstrSTG6COMP)

                If lblREQDSTG6 IsNot Nothing Then

                    If lblREQDSTG6.Text = 0 Then

                        Dim ib As ImageButton = e.Row.Cells(lstrCell6).Controls(0)

                        ib.ImageUrl = "~\images\cross.gif"

                        e.Row.Cells(lstrCell6).Enabled = False
                        e.Row.Cells(lstrCell6).ToolTip = "This stage is not required"
                    Else

                        If lblCOMPSTG6.Text = 0 Then

                            Dim ib As ImageButton = e.Row.Cells(lstrCell6).Controls(0)

                            ib.ImageUrl = "~\images\cross.gif"



                            If lblCOMPSTG5.Text = 1 And lblREQDSTG5.Text = 1 Then
                                If enableNextStage = True Then
                                    e.Row.Cells(lstrCell6).Enabled = True
                                Else
                                    e.Row.Cells(lstrCell6).Enabled = False
                                    lblNextStage.Text = lstrCell6.ToString
                                    e.Row.Cells(lstrCell6).ToolTip = "Please submit the documents for the previous stage to proceed to the next stage"
                                End If
                            ElseIf lblCOMPSTG5.Text = 0 Then

                                e.Row.Cells(lstrCell6).Enabled = False
                                e.Row.Cells(lstrCell6).ToolTip = "Formalities for previous stage is not completed"
                            End If





                        Else

                            Dim ib As ImageButton = e.Row.Cells(lstrCell6).Controls(0)
                            ib.ImageUrl = "~\images\tick.gif"
                            'If Val(lblORD6.Text) > Val(lblDocStageOrder.Text) Then
                            '    ib.ImageUrl = "~\images\tickDP.gif"
                            'Else
                            '    ib.ImageUrl = "~\images\tick.gif"
                            'End If



                            e.Row.Cells(lstrCell6).Enabled = True





                        End If

                    End If

                End If



                Dim lstrSTGREQD As String
                Dim lstrSTGCOMP As String
                Dim lstrORDER As String


                Dim stgReqD As Label
                Dim stgCOMP As Label
                Dim stgOrder As Label

                Dim i As Integer

                Dim lstrCell As Integer
                Dim ib1 As ImageButton
                lblCurrStatus = e.Row.FindControl("lblCurrStatus")
                If Not lblCurrStatus Is Nothing Then
                    If lblCurrStatus.Text <> "2" Then
                        lblDocStageOrder = e.Row.FindControl(docStageOrder)
                        lblCurrStageOrder = e.Row.FindControl(currStageOrder)
                        If Val(lblCurrStageOrder.Text) > Val(lblDocStageOrder.Text) Then
                            If lblAplDis.Text.ToUpper = "FALSE" Then
                                e.Row.Cells(Val(lblNextStage.Text)).Enabled = False
                            End If
                        Else
                            If lblAplDis.Text.ToUpper = "FALSE" Then
                                e.Row.Cells(Val(lblNextStage.Text)).Enabled = True
                            End If
                        End If

                        For i = 3 To 6
                            lstrSTGREQD = "lblSTG" + i.ToString + "REQD"
                            lstrSTGCOMP = "lblSTG" + i.ToString + "COMP"
                            lstrORDER = "lblORD" + i.ToString
                            stgReqD = e.Row.FindControl(lstrSTGREQD)
                            stgCOMP = e.Row.FindControl(lstrSTGCOMP)
                            stgOrder = e.Row.FindControl(lstrORDER)

                            If Val(stgOrder.Text) > Val(lblDocStageOrder.Text) Then
                                If stgReqD.Text = 1 Then
                                    If stgCOMP.Text = 1 Then
                                        lstrCell = 8 + i + 1
                                        ib1 = e.Row.Cells(lstrCell).Controls(0)
                                        ib1.ImageUrl = "~/images/tickDP.gif"
                                    End If
                                End If
                            Else
                                If stgReqD.Text = 1 Then
                                    If stgCOMP.Text = 1 Then
                                        lstrCell = 8 + i + 1
                                        ib1 = e.Row.Cells(lstrCell).Controls(0)
                                        ib1.ImageUrl = "~/images/tick.gif"
                                    End If
                                End If
                            End If

                        Next

                    End If
                End If

                Dim lblEQS_STATUS As New Label
                Dim lbtnReopen As New LinkButton
                Dim lbtnCancel As New LinkButton

                lblEQS_STATUS = e.Row.FindControl("lblEQS_STATUS")
                lbtnReopen = e.Row.FindControl("lbtnReopen")
                lbtnCancel = e.Row.FindControl("lbtnCancel")

               

                If lblEQS_STATUS.Text = "DEL" Then
                    e.Row.Cells(11 + 1).Enabled = False
                    e.Row.Cells(11 + 1).ToolTip = ""
                    e.Row.Cells(12 + 1).Enabled = False
                    e.Row.Cells(12 + 1).ToolTip = ""
                    e.Row.Cells(13 + 1).Enabled = False
                    e.Row.Cells(13 + 1).ToolTip = ""
                    e.Row.Cells(14 + 1).Enabled = False
                    e.Row.Cells(14 + 1).ToolTip = ""
                    If Not lbtnReopen Is Nothing Then
                        lbtnReopen.Enabled = True
                    End If
                    If Not lbtnCancel Is Nothing Then
                        lbtnCancel.Enabled = False
                    End If
                ElseIf lblCurrStatus.Text = "1" Then
                    If Not lbtnReopen Is Nothing Then
                        lbtnReopen.Enabled = False
                    End If
                    If Not lbtnCancel Is Nothing Then
                        lbtnCancel.Enabled = False
                    End If
                Else
                    If Not lbtnReopen Is Nothing Then
                        lbtnReopen.Enabled = False
                    End If
                    If Not lbtnCancel Is Nothing Then
                        lbtnCancel.Enabled = True
                    End If
                End If


            End If

        Catch ex As Exception
            ' lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddlEnqStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnqStatus.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudEnquiry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudEnquiry.SelectedIndexChanged
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String
            Dim docStgId As Integer
            If ViewState("action") = "select" Then
                ViewState("action") = ""
                Dim lbleqsId As Label
                With gvStudEnquiry.SelectedRow
                    lbleqsId = .FindControl("lbleqsid")
                    str_query = "SELECT ISNULL(EQS_DOC_STG_ID,0) FROM ENQUIRY_SCHOOLPRIO_S WHERE EQS_ID=" + lbleqsId.Text
                    docStgId = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)


                    Dim lblCurrStatus As New Label
                    Dim lblDocStageOrder As New Label
                    Dim lblCurrStageOrder As New Label
                    Dim docStageOrder As String
                    Dim currStageOrder As String

                    lblCurrStatus = .FindControl("lblCurrStatus")

                    Dim lstrSTGREQD As String
                    Dim lstrSTGCOMP As String
                    Dim lstrORDER As String

                    Dim lblNextStage As New Label

                    lblNextStage = .FindControl("lblNextStage")

                    'if the applicationdecision is diabled the next stage has to be disabled
                    Dim lblAplDis As Label = .FindControl("lblAplDis")

                    Dim stgReqD As Label
                    Dim stgCOMP As Label
                    Dim stgOrder As Label

                    Dim i As Integer

                    Dim lstrCell As Integer
                    Dim ib As ImageButton


                    If Not lblCurrStatus Is Nothing Then
                        If lblCurrStatus.Text <> "2" Then
                            docStageOrder = "lblORD" + docStgId.ToString
                            currStageOrder = "lblORD" + lblCurrStatus.Text

                            lblDocStageOrder = .FindControl(docStageOrder)
                            lblCurrStageOrder = .FindControl(currStageOrder)
                            If Val(lblCurrStageOrder.Text) > Val(lblDocStageOrder.Text) Then
                                If lblAplDis.Text.ToUpper = "FALSE" Then
                                    .Cells(Val(lblNextStage.Text)).Enabled = False
                                End If
                            Else
                                If lblAplDis.Text.ToUpper = "FALSE" Then
                                    .Cells(Val(lblNextStage.Text)).Enabled = True
                                End If
                            End If

                            For i = 3 To 6
                                lstrSTGREQD = "lblSTG" + i.ToString + "REQD"
                                lstrSTGCOMP = "lblSTG" + i.ToString + "COMP"
                                lstrORDER = "lblORD" + i.ToString
                                stgReqD = .FindControl(lstrSTGREQD)
                                stgCOMP = .FindControl(lstrSTGCOMP)
                                stgOrder = .FindControl(lstrORDER)

                                If Val(stgOrder.Text) > Val(lblDocStageOrder.Text) Then
                                    If stgReqD.Text = 1 Then
                                        If stgCOMP.Text = 1 Then
                                            lstrCell = 8 + i
                                            ib = .Cells(lstrCell).Controls(0)
                                            ib.ImageUrl = "~/images/tickDP.gif"
                                        End If
                                    End If
                                Else
                                    If stgReqD.Text = 1 Then
                                        If stgCOMP.Text = 1 Then
                                            lstrCell = 8 + i
                                            ib = .Cells(lstrCell).Controls(0)
                                            ib.ImageUrl = "~/images/tick.gif"
                                        End If
                                    End If
                                End If

                            Next

                        End If
                    End If


                End With
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub btnAcc_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#Region "Private Methods"
    Protected Sub rbOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub rbCancel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Session("liUserList") = New List(Of String)
            SaveData("DEL")
            GridBind()

        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Sub SaveData(ByVal mode As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim i As Integer
            Dim str_query As String
            Dim chkSelect As New CheckBox
            Dim eqsID As New Label
            Dim st As String = "", totstr As String = ""
            Dim st_status As String = "", totstr_status As String = ""

            Dim chk As CheckBox
            Dim ENQID As String = String.Empty
            Dim hash As New Hashtable
            If Not Session("hashCheck") Is Nothing Then
                hash = Session("hashCheck")
            End If
            For Each rowItem As GridViewRow In gvStudEnquiry.Rows

                chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

                ENQID = DirectCast(rowItem.FindControl("lblEqsId"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(ENQID) = False Then
                        hash.Add(ENQID, DirectCast(rowItem.FindControl("lblEqsId"), Label).Text)
                    End If
                Else
                    If hash.Contains(ENQID) = True Then
                        hash.Remove(ENQID)
                    End If
                End If



            Next

            Session("hashCheck") = hash


            If Not Session("hashCheck") Is Nothing Then
                hash = Session("hashCheck")
                Dim hashloop As DictionaryEntry

                For Each hashloop In hash
                    eqsID.Text = hashloop.Value

                    If mode = "DEL" Then
                        st = check_followup_remark(eqsID.Text)


                        If ViewState("fee_paid_eqs") = 1 Then

                            st_status = ViewState("applnno")
                            st = ""
                        Else
                            If st = "" Then
                                str_query = "exec studSaveStudEnqAcceptReject " + eqsID.Text + ",'" + mode + "'," + ddlAcademicYear.SelectedValue.ToString
                                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                            End If

                        End If

                        If totstr = "" Then
                            totstr = st
                        Else
                            totstr = totstr & "," & st
                        End If

                        If totstr_status = "" Then
                            totstr_status = st_status
                        Else
                            totstr_status = totstr_status & "," & st_status
                        End If
                    End If

                Next
                Session.Remove("hashCheck")

            Else
                lblError.Text = "No record selected for shortlisting"

            End If
            If hash.Count > 0 Then
                If mode = "DEL" Then
                    If totstr <> "" Then
                        totstr = "Enquiry number(s) " & totstr & " cannot be deleted. It doesnot have valid follow up status with remarks."
                    
                    End If
                    If totstr_status <> "" Or totstr <> "" Then
                        lblError.Text = totstr
                        If totstr_status <> "" Then
                            lblError.Text = lblError.Text & "<br/>  Enquiry number(s)  " & totstr_status & " cannot be deleted since they are already paid the fees."
                        End If
                    Else
                        lblError.Text = "Records removed successfully"
                    End If
                End If
            Else
                lblError.Text = "No record selected for Deleting"
            End If

            'For i = 0 To gvStudEnquiry.Rows.Count - 1
            '    chkSelect = gvStudEnquiry.Rows(i).Cells(0).FindControl("chkSelect")
            '    If chkSelect.Checked = True Then
            '        eqsID = gvStudEnquiry.Rows(i).Cells(1).FindControl("lblEqsId")
            '        str_query = "exec studSaveStudEnqAcceptReject " + eqsID.Text + ",'" + mode + "'," + ddlAcademicYear.SelectedValue.ToString
            '        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            '    End If
            'Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Function check_followup_remark(ByVal eqsid As Integer) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim str As String = "", applnno As String = "", v As Integer
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@eqs_id", eqsid)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "enq.check_enq_followup_remark", param)


        If ds.Tables(0).Rows.Count >= 1 Then
            v = ds.Tables(0).Rows(0).Item("VALID")
            applnno = ds.Tables(0).Rows(0).Item("EQS_APPLNO")
            ViewState("cur_status") = ds.Tables(0).Rows(0).Item("EQS_CURRSTATUS")
            ViewState("fee_paid_eqs") = ds.Tables(0).Rows(0).Item("FEE_PAID")
            ViewState("applnno") = applnno
        End If
        If v = 0 Then
            str = applnno
        Else
            str = ""
        End If
        Return str
    End Function
    Private Sub BIND_BSU_DETAILS()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter

        param(0) = New SqlParameter("@BSU_ID", Session("sbsuid"))
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ENQ.GET_EMAIL_RESEND_BSU", param)
            While datareader.Read
                ViewState("ACK1") = Convert.ToString(datareader("ERT_ACK1"))
                ViewState("ACK2") = Convert.ToString(datareader("ERT_ACK2"))


            End While
        End Using
    End Sub



    Private Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = String.Empty
            Dim lstrMale
            Dim lstrFemale
            Dim lstrTotal
            Dim lstrTop10 As String

            If gvStudEnquiry.Rows.Count > 0 Then
                If Session("ACD_SELECT_CLEAR") <> "ACD" Then
                    lstrTop10 = ""
                    Session("ACD_SELECT_CLEAR") = ""
                Else
                    lstrTop10 = "Top 10"
                    Session("ACD_SELECT_CLEAR") = ""
                End If
            Else
                lstrTop10 = "Top 10"
                Session("ACD_SELECT_CLEAR") = ""
            End If

            If ViewState("ACK1") Then
                gvStudEnquiry.Columns(57).Visible = True
            Else
                gvStudEnquiry.Columns(57).Visible = False
            End If


            If ViewState("ACK2") Then
                gvStudEnquiry.Columns(58).Visible = True
            Else
                gvStudEnquiry.Columns(58).Visible = False
            End If

            If rbOpen.Checked = True Then
                gvStudEnquiry.Columns(56).Visible = False
                str_query = "SELECT " & lstrTop10 & " row_number()OVER(order by isNULL(EQS_REGNDATE,eqm_enqdate)  ,isNULL(EQS_ACCNO,b.eqs_applno) ) AS RowNumber,eqs_id,eqs_applno,eqm_enqid,eqm_enqdate,grm_display,shf_descr,stm_descr,isnull(EFR_ICONS,'')EFR_ICONS,EFR_REASON, " _
                                                   & " appl_name=isnull(eqm_applfirstname,'') + ' '+isnull(eqm_applmidname,'') + ' ' " _
                                                   & " +isnull(eqm_appllastname,''),F.*,G.*,eqs_applno,EQS_CURRSTATUS,isnull(EQS_DOC_STG_ID,0) as  EQS_DOC_STG_ID ," _
                                                   & " ISNULL(EQS_APL_DISABLE,'FALSE') AS EQS_APL_DISABLE,B.EQS_APL_ID as APL_ID, B.EQS_STATUS as EQS_STATUS," _
                                                   & " case  when (ISNULL(B.EQS_APL_DISABLE, 'FALSE')='FALSE') AND(B.EQS_STATUS='OFR') THEN 'TRUE' ELSE 'FALSE' END AS FLAG ,isnull(Eqs_AccNo,'') as Eqs_AccNo,isnull(B.EQS_bFOLLOWUP,0) as EQS_bFOLLOWUP,EQS_REGNDATE,EQM_APPLGENDER as GENDER,APL_IMAGE,APL_TYPE,CASE when isnull(EQS_bSEN,0)=1  THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END  as EQS_bSEN ,case when rtrim(ltrim(isNULL(EQS_SEN,'')))='' then 0 else 1 end as bSEN, EQS_SEN, DBO.GETCAL_AGE( EQM_APPLDOB,ACD_AGE_CUTOFF)  	as Appl_Age,'Age on '+ replace(convert(varchar(12),ACD_AGE_CUTOFF,106),' ','/') as CUTOFF_DT ,isNULL(PREVSCHOOL.BSU_SHORTNAME,'')  as Prev_School_GEMS  FROM enquiry_m A WITH (NOLOCK)" _
                                                   & " INNER JOIN enquiry_schoolprio_s B WITH (NOLOCK) ON A.eqm_enqid=B.eqs_eqm_enqid " _
                                                   & " INNER JOIN vw_OSO_STUDAPPLSTAGES F ON B.EQS_ID=F.PRA_EQS_ID " _
                                                   & " INNER JOIN vw_OSO_STUDSTAGEORDER G ON B.EQS_ID=G.PRA_EQS_ID " _
                                                   & " INNER JOIN grade_bsu_m C WITH (NOLOCK) ON B.eqs_grm_id=C.grm_id " _
                                                   & " INNER JOIN shifts_m D WITH (NOLOCK) ON B.eqs_shf_id=D.shf_id" _
                                                   & " INNER JOIN stream_m  E WITH (NOLOCK) ON B.eqs_stm_id=E.stm_id " _
                                                   & " INNER JOIN ACADEMICYEAR_D AS ACD WITH (NOLOCK) ON B.EQS_ACD_ID = ACD.ACD_ID " _
                                                & " INNER JOIN vw_ENQ_SIBLINGS P ON B.EQS_ID=P.EQS " _
                                                 & " LEFT JOIN ApplicationDecision_M APPL ON B.EQS_APL_ID=APPL.APL_ID " _
                                                  & " LEFT JOIN BUSINESSUNIT_M  AS PREVSCHOOL ON A.EQM_PREVSCHOOL_BSU_ID = PREVSCHOOL.BSU_ID " _
                                                  & " LEFT OUTER JOIN ENQUIRY_FOLLOW_UP_REASON AS FR ON FR.ENR_EQS_ID=B.eqs_id AND FR.ENR_bACTIVE=1  " _
                                                  & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=FR.ENR_EFR_ID " _
                                                 & " WHERE  B.eqs_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                                   & " AND B.eqs_bsu_id='" + Session("sbsuid").ToString + "' AND EQS_CANCELDATE IS NULL AND EQS_STATUS<>'DEL' AND EQS_STATUS<>'ENR' "

            ElseIf rbCancel.Checked = True Then
                gvStudEnquiry.Columns(56).Visible = True
                str_query = "SELECT " & lstrTop10 & " row_number()OVER(order by isNULL(EQS_REGNDATE,eqm_enqdate)  ,isNULL(EQS_ACCNO,b.eqs_applno) ) AS RowNumber,eqs_id,eqs_applno,eqm_enqid,eqm_enqdate,grm_display,shf_descr,stm_descr,isnull(EFR_ICONS,'')EFR_ICONS,EFR_REASON, " _
                                                   & " appl_name=isnull(eqm_applfirstname,'') + ' '+isnull(eqm_applmidname,'') + ' ' " _
                                                   & " +isnull(eqm_appllastname,''),F.*,G.*,eqs_applno,EQS_CURRSTATUS,isnull(EQS_DOC_STG_ID,0) as  EQS_DOC_STG_ID ," _
                                                   & " ISNULL(EQS_APL_DISABLE,'FALSE') AS EQS_APL_DISABLE,B.EQS_APL_ID as APL_ID, B.EQS_STATUS as EQS_STATUS," _
                                                   & " case  when (ISNULL(B.EQS_APL_DISABLE, 'FALSE')='FALSE') AND(B.EQS_STATUS='OFR') THEN 'TRUE' ELSE 'FALSE' END AS FLAG ,isnull(Eqs_AccNo,'') as Eqs_AccNo,isnull(B.EQS_bFOLLOWUP,0) as EQS_bFOLLOWUP,EQS_REGNDATE,EQM_APPLGENDER as GENDER ,APL_IMAGE,APL_TYPE,CASE when isnull(EQS_bSEN,0)=1  THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END  as EQS_bSEN,case when rtrim(ltrim(EQS_SEN))='' then 0 else 1 end as bSEN, EQS_SEN, DBO.GETCAL_AGE( EQM_APPLDOB,ACD_AGE_CUTOFF)  	as Appl_Age ,'Age on '+ replace(convert(varchar(12),ACD_AGE_CUTOFF,106),' ','/') as CUTOFF_DT  ,isNULL(PREVSCHOOL.BSU_SHORTNAME,'')  as Prev_School_GEMS  FROM enquiry_m A WITH (NOLOCK) " _
                                                   & " INNER JOIN enquiry_schoolprio_s B WITH (NOLOCK) ON A.eqm_enqid=B.eqs_eqm_enqid " _
                                                   & " INNER JOIN vw_OSO_STUDAPPLSTAGES F ON B.EQS_ID=F.PRA_EQS_ID " _
                                                   & " INNER JOIN vw_OSO_STUDSTAGEORDER G ON B.EQS_ID=G.PRA_EQS_ID " _
                                                   & " INNER JOIN grade_bsu_m C WITH (NOLOCK) ON B.eqs_grm_id=C.grm_id " _
                                                   & " INNER JOIN shifts_m D WITH (NOLOCK) ON B.eqs_shf_id=D.shf_id" _
                                                   & " INNER JOIN stream_m  E WITH (NOLOCK) ON B.eqs_stm_id=E.stm_id " _
                                                    & " INNER JOIN vw_ENQ_SIBLINGS P ON B.EQS_ID=P.EQS " _
                                                    & " INNER JOIN ACADEMICYEAR_D AS ACD WITH (NOLOCK) ON B.EQS_ACD_ID = ACD.ACD_ID " _
                                                     & " LEFT JOIN ApplicationDecision_M APPL ON B.EQS_APL_ID=APPL.APL_ID " _
                                                       & " LEFT JOIN BUSINESSUNIT_M  AS PREVSCHOOL ON A.EQM_PREVSCHOOL_BSU_ID = PREVSCHOOL.BSU_ID " _
                                                       & " LEFT OUTER JOIN ENQUIRY_FOLLOW_UP_REASON AS FR ON FR.ENR_EQS_ID=B.eqs_id AND FR.ENR_bACTIVE=1 " _
                                                  & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=FR.ENR_EFR_ID " _
                                                   & " WHERE B.eqs_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                                   & " AND B.eqs_bsu_id='" + Session("sbsuid").ToString + "' AND  EQS_STATUS='DEL' "
            ElseIf rbAll.Checked = True Then
                gvStudEnquiry.Columns(56).Visible = True

                str_query = "SELECT " & lstrTop10 & " row_number()OVER(order by isNULL(EQS_REGNDATE,eqm_enqdate)  ,isNULL(EQS_ACCNO,b.eqs_applno) ) AS RowNumber,eqs_id,eqs_applno,eqm_enqid,eqm_enqdate,grm_display,shf_descr,stm_descr,isnull(EFR_ICONS,'')EFR_ICONS,EFR_REASON, " _
                                                   & " appl_name=isnull(eqm_applfirstname,'') + ' '+isnull(eqm_applmidname,'') + ' ' " _
                                                   & " +isnull(eqm_appllastname,''),F.*,G.*,eqs_applno,EQS_CURRSTATUS,isnull(EQS_DOC_STG_ID,0) as  EQS_DOC_STG_ID ," _
                                                   & " ISNULL(EQS_APL_DISABLE,'FALSE') AS EQS_APL_DISABLE,B.EQS_APL_ID as APL_ID, B.EQS_STATUS as EQS_STATUS," _
                                                   & " case  when (ISNULL(B.EQS_APL_DISABLE, 'FALSE')='FALSE') AND(B.EQS_STATUS='OFR') THEN 'TRUE' ELSE 'FALSE' END AS FLAG ,isnull(Eqs_AccNo,'') as Eqs_AccNo,isnull(B.EQS_bFOLLOWUP,0) as EQS_bFOLLOWUP,EQS_REGNDATE,EQM_APPLGENDER as GENDER ,APL_IMAGE ,APL_TYPE,CASE when isnull(EQS_bSEN,0)=1  THEN '~/Images/tick.gif' ELSE '~/Images/cross.gif' END  as EQS_bSEN,case when rtrim(ltrim(EQS_SEN))='' then 0 else 1 end as bSEN, EQS_SEN,DBO.GETCAL_AGE( EQM_APPLDOB,ACD_AGE_CUTOFF) 	as Appl_Age ,'Age on '+ replace(convert(varchar(12),ACD_AGE_CUTOFF,106),' ','/') as CUTOFF_DT ,isNULL(PREVSCHOOL.BSU_SHORTNAME,'')  as Prev_School_GEMS  FROM enquiry_m A " _
                                                   & " INNER JOIN enquiry_schoolprio_s B WITH (NOLOCK) ON A.eqm_enqid=B.eqs_eqm_enqid " _
                                                   & " INNER JOIN vw_OSO_STUDAPPLSTAGES F ON B.EQS_ID=F.PRA_EQS_ID " _
                                                   & " INNER JOIN vw_OSO_STUDSTAGEORDER G ON B.EQS_ID=G.PRA_EQS_ID " _
                                                   & " INNER JOIN grade_bsu_m C WITH (NOLOCK) ON B.eqs_grm_id=C.grm_id " _
                                                   & " INNER JOIN shifts_m D WITH (NOLOCK) ON B.eqs_shf_id=D.shf_id" _
                                                   & " INNER JOIN stream_m  E WITH (NOLOCK) ON B.eqs_stm_id=E.stm_id " _
                                                    & " INNER JOIN vw_ENQ_SIBLINGS P ON B.EQS_ID=P.EQS " _
                                                    & " INNER JOIN ACADEMICYEAR_D AS ACD WITH (NOLOCK) ON B.EQS_ACD_ID = ACD.ACD_ID " _
                                                     & " LEFT JOIN ApplicationDecision_M APPL ON B.EQS_APL_ID=APPL.APL_ID " _
                                                       & " LEFT JOIN BUSINESSUNIT_M  AS PREVSCHOOL ON A.EQM_PREVSCHOOL_BSU_ID = PREVSCHOOL.BSU_ID " _
                                                       & " LEFT OUTER JOIN ENQUIRY_FOLLOW_UP_REASON AS FR ON FR.ENR_EQS_ID=B.eqs_id AND FR.ENR_bACTIVE=1 " _
                                                  & " LEFT OUTER JOIN ENQ_FOLLOW_UP_REASON_M ON EFR_ID=FR.ENR_EFR_ID " _
                                                   & " WHERE B.eqs_acd_id=" + ddlAcademicYear.SelectedValue.ToString _
                                                   & " AND B.eqs_bsu_id='" + Session("sbsuid").ToString + "' AND  (EQS_STATUS='DEL' or(EQS_STATUS<>'ENR' and EQS_CANCELDATE IS NULL)) "

            End If

            Session("sEnqStatus") = ddlEnqStatus.SelectedItem.Value


            If ddlEnqStatus.SelectedItem.Text <> "ALL" Then
                str_query = str_query & " AND B.EQS_CURRSTATUS='" & Session("sEnqStatus") & "'"
            Else
                str_query += " AND EQS_STATUS<>'NEW' "
            End If

            If Session("BSU_ENQ_SELECT_MANAGE") = "0" Then
                str_query = str_query + " "
            ElseIf Session("BSU_ENQ_SELECT_MANAGE") = "1" Then
                str_query = str_query + " and EQM_PREVSCHOOL_BSU_ID IN (SELECT BSU_ID From BusinessUnit_M WITH (NOLOCK))"
            ElseIf Session("BSU_ENQ_SELECT_MANAGE") = "2" Then
                str_query = str_query + " and EQM_PREVSCHOOL_BSU_ID NOT IN (SELECT BSU_ID From BusinessUnit_M WITH (NOLOCK))"
            Else
                str_query = str_query + " and EQM_PREVSCHOOL_BSU_ID='" + Session("BSU_ENQ_SELECT_MANAGE") + "'"
            End If



            If Session("CONTACTME_SELECT2") = "ALL" Then
                str_query = str_query + ""
            ElseIf Session("CONTACTME_SELECT2") = "YES" Then
                str_query = str_query + " and EQM_APPL_CONTACTME='Y' "
            ElseIf Session("CONTACTME_SELECT2") = "NO" Then
                str_query = str_query + " and isNULL(EQM_APPL_CONTACTME,'N')='N' "
            End If


            If Session("PAID_SELECT2") = "ALL" Then
                str_query = str_query + ""
            ElseIf Session("PAID_SELECT2") = "YES" Then
                str_query = str_query + " and EQS_REC_NO IS NOT NULL "
            ElseIf Session("PAID_SELECT2") = "NO" Then
                str_query = str_query + " and isNULL(EQS_REC_NO,'')='' "
            ElseIf Session("PAID_SELECT2") = "AtSchool" Then
                str_query = str_query + " and EQS_REC_NO IS NOT NULL and EQS_bRegisteredonline=0 "
            ElseIf Session("PAID_SELECT2") = "Online" Then
                str_query = str_query + " and EQS_REC_NO IS NOT NULL and EQS_bRegisteredonline=1 "
            ElseIf Session("PAID_SELECT2") = "NotPaidReg" Then
                str_query = str_query + " and isNULL(EQS_REC_NO,'')='' AND EQS_CURRSTATUS BETWEEN 3 AND 6 "
            End If

            If ddlFollowupStatus.SelectedValue <> "0" Then
                str_query += " AND EFR_ID=" & ddlFollowupStatus.SelectedValue
            End If


            If btnSIB.Text = "Show All" Then
                str_query += " AND Sibling='SIB' "
            End If
            Dim strFilter As String = ""
            Dim strSidsearch As String()
            Dim strSearch As String

            Dim enqSearch As String = ""
            Dim nameSearch As String = ""
            Dim dateSearch As String = ""
            Dim ddlgvShift As New DropDownList
            Dim ddlgvStream As New DropDownList

            Dim accSearch As String = ""

            Dim selectedGrade As String = ""
            Dim selectedGender As String = ""
            Dim selectedShift As String = ""
            Dim selectedStream As String = ""
            Dim txtSearch As New TextBox







            If gvStudEnquiry.Rows.Count > 0 Then


                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = GetSearchString("eqs_applno", txtSearch.Text, strSearch)
                enqSearch = txtSearch.Text

                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtAccSearch")
                strSidsearch = h_Selected_menu_12.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("eqs_accno", txtSearch.Text, strSearch)
                accSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("convert(varchar,eqm_enqdate,106)", txtSearch.Text.Replace("/", " "), strSearch)
                dateSearch = txtSearch.Text


                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("grm_display", txtSearch.Text, strSearch)
                selectedGrade = txtSearch.Text


                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGender")
                strSidsearch = h_Selected_menu_13.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("EQM_APPLGENDER", txtSearch.Text, strSearch)
                selectedGender = txtSearch.Text


                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                If ddlgvShift.Text <> "ALL" And ddlgvShift.Text <> "" Then
                    strFilter = strFilter + " and shf_descr='" + ddlgvShift.Text + "'"
                    selectedShift = ddlgvShift.Text
                End If


                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")
                If ddlgvStream.Text <> "ALL" And ddlgvStream.Text <> "" Then
                    strFilter = strFilter + " and stm_descr='" + ddlgvStream.Text + "'"
                    selectedStream = ddlgvStream.Text
                End If


                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
                strSidsearch = h_Selected_menu_3.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("isnull(eqm_applfirstname,' ')+' '+isnull(eqm_applmidname,' ')+' '+isnull(eqm_appllastname,' ')", txtSearch.Text, strSearch)
                nameSearch = txtSearch.Text

                If strFilter.Trim <> "" Then
                    str_query = str_query + strFilter
                End If


            End If
            Dim ds As DataSet
            Dim dv As DataView

            

            'str_query += " order by isNULL(EQS_REGNDATE,eqm_enqdate) desc ,isNULL(EQS_ACCNO,b.eqs_applno) desc "
            str_query += Session("SORTORDER")

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudEnquiry.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)("FLAG") = True
                ds.Tables(0).Rows(0)("EQS_bSEN") = False
                ds.Tables(0).Rows(0)("bSEN") = False
                gvStudEnquiry.DataSource = ds.Tables(0)
                Try
                    gvStudEnquiry.DataBind()
                Catch ex As Exception
                End Try
                ViewState("norow") = "yes"
                btnPrintOffer.Visible = False
                btnExport.Visible = False

                Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
                gvStudEnquiry.Rows(0).Cells.Clear()
                gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
                gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                ViewState("norow") = "no"
                Dim i = 0
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim lstrGender = ds.Tables(0).Rows(i).Item("Gender")
                    ViewState("CutDate") = ds.Tables(0).Rows(i).Item("CUTOFF_DT")
                    lstrTotal = lstrTotal + 1
                    If lstrGender = "M" Then
                        lstrMale = lstrMale + 1

                    Else
                        lstrFemale = lstrFemale + 1
                    End If

                Next
                lblSTAT.Text = "Total : " & lstrTotal & " , Male : " & lstrMale & " , Female : " & lstrFemale
                gvStudEnquiry.DataBind()
                'btnPrintOffer.Visible = True
                btnExport.Visible = True
            End If

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
            txtSearch.Text = enqSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
            txtSearch.Text = dateSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
            txtSearch.Text = nameSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = selectedGrade


            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGender")
            txtSearch.Text = selectedGender

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtAccSearch")
            txtSearch.Text = accSearch

            Session("eqm_enqid") = ds.Tables(0).Rows(0).Item("eqm_enqid").ToString()

            Dim dt As DataTable = ds.Tables(0)

            If gvStudEnquiry.Rows.Count > 0 Then
                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")

                Dim dr As DataRow



                ddlgvShift.Items.Clear()
                ddlgvShift.Items.Add("ALL")


                ddlgvStream.Items.Clear()
                ddlgvStream.Items.Add("ALL")




                For Each dr In dt.Rows
                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If
                    With dr

                        ''check for duplicate values and add to dropdownlist 
                        If ddlgvShift.Items.FindByText(.Item("shf_descr")) Is Nothing Then
                            ddlgvShift.Items.Add(.Item("shf_descr"))
                        End If
                        If ddlgvStream.Items.FindByText(.Item("stm_descr")) Is Nothing Then
                            ddlgvStream.Items.Add(.Item("stm_descr"))
                        End If


                    End With
                Next

                If selectedShift <> "" Then
                    ddlgvShift.Text = selectedShift
                End If

                If selectedStream <> "" Then
                    ddlgvStream.Text = selectedStream
                End If
            End If
            set_Menu_Img()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Function GetNavigateUrl(ByVal eqsid As String) As String
        'Dim str As String = "javascript:var popup = window.showModalDialog('studJoinDocuments.aspx?eqsid=" + eqsid + "', '','dialogHeight:500px;dialogWidth:905px;scroll:no;resizable:no;');"
        Dim str As String = "javascript:ShowWindowWithClose('studJoinDocuments.aspx?eqsid=" + eqsid + "',  'search', '55%', '85%'); return false;"
        Return str
    End Function
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_12.Value.Split("__")
        getid12(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_13.Value.Split("__")
        getid13(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid13(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_13_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid12(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_12_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getmenuid(Optional ByVal p_imgsrc As String = "", Optional ByVal imagename As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudEnquiry.HeaderRow.FindControl(imagename)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Sub highlight_grid()
        'For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1
        '    Dim row As GridViewRow = gvStudEnquiry.Rows(i)
        '    Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
        '    If isSelect Then
        '        row.BackColor = Drawing.Color.FromName("#f6deb2")
        '    Else
        '        row.BackColor = Drawing.Color.Transparent
        '    End If
        'Next
    End Sub



#End Region

    Protected Sub btnPrintOffer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim ChkState As Boolean = False
        'If gvStudEnquiry.Rows.Count > 0 Then
        '    For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1
        '        Dim row As GridViewRow = gvStudEnquiry.Rows(i)
        '        Dim ckList As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
        '        If ckList Then
        '            ChkState = True
        '        End If
        '    Next

        '    If ChkState = True Then
        CallReport_offer()
        '    Else
        'lblError.Text = "No Enquiry selected for print"
        '    End If
        'Else
        'lblError.Text = "No Enquiry No available for print"
        'End If
    End Sub
    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudEnquiry.PageIndexChanging
        Try
            gvStudEnquiry.PageIndex = e.NewPageIndex
            Dim APL_ID As String = String.Empty

            Dim hash As New Hashtable
            If Not Session("hashCheck_eqsIds") Is Nothing Then
                hash = Session("hashCheck_eqsIds")
            End If
            Dim chk As CheckBox
            Dim Eqs_id As String = String.Empty
            For Each rowItem As GridViewRow In gvStudEnquiry.Rows
                chk = DirectCast((rowItem.Cells(0).FindControl("chkSelect")), CheckBox)

                Eqs_id = gvStudEnquiry.DataKeys(rowItem.RowIndex)("Eqs_id").ToString()
                APL_ID = DirectCast(rowItem.FindControl("lblAPL_ID"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(Eqs_id) = False Then
                        hash.Add(Eqs_id, Eqs_id & "_" & APL_ID)
                    End If
                Else
                    If hash.Contains(Eqs_id) = True Then
                        hash.Remove(Eqs_id)
                    End If
                End If
            Next
            Session("hashCheck_eqsIds") = hash
            GridBind()

            'selectedCheckboxes()
            'GridBind()
            'reselectedcheckboxes()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub gvStudEnquiry_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hash As New Hashtable
        If Not Session("hashCheck_eqsIds") Is Nothing Then
            hash = Session("hashCheck_eqsIds")
        End If
        Dim chk As CheckBox
        Dim Eqs_id As String = String.Empty
        For Each rowItem As GridViewRow In gvStudEnquiry.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkSelect")), CheckBox)
            Eqs_id = gvStudEnquiry.DataKeys(rowItem.RowIndex)("Eqs_id").ToString()
            If hash.Contains(Eqs_id) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If

        Next
    End Sub
    Private Sub CallReport_offer()

        '-------------------------------------------------------


        'For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1

        '    Dim row As GridViewRow = gvStudEnquiry.Rows(i)

        '    Dim ckList As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked

        '    If ckList Then
        '        EQS_IDs.Append((DirectCast(row.FindControl("lblEqsId"), Label).Text) & "_" & (DirectCast(row.FindControl("lblAPL_ID"), Label).Text))
        '        EQS_IDs.Append("|")
        '    End If
        'Next
        '----------------------------------------------
        Dim EQS_IDs As New StringBuilder

        Dim chk As CheckBox
        Dim Eqs_id As String = String.Empty
        Dim APL_ID As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("hashCheck_eqsIds") Is Nothing Then
            hash = Session("hashCheck_eqsIds")
        End If
        For Each rowItem As GridViewRow In gvStudEnquiry.Rows

            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

            Eqs_id = DirectCast(rowItem.FindControl("lblEqsId"), Label).Text
            APL_ID = DirectCast(rowItem.FindControl("lblAPL_ID"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(Eqs_id) = False Then
                    hash.Add(Eqs_id, Eqs_id & "_" & APL_ID)
                End If
            Else
                If hash.Contains(Eqs_id) = True Then
                    hash.Remove(Eqs_id)
                End If
            End If

        Next

        Session("hashCheck_eqsIds") = hash


        If Not Session("hashCheck_eqsIds") Is Nothing Then
            hash = Session("hashCheck_eqsIds")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                EQS_IDs.Append(hashloop.Value)
                EQS_IDs.Append("|")

            Next


        End If

        Dim param As New Hashtable

        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@EQS_IDs", EQS_IDs.ToString)
        param.Add("@BSU_ID", Session("sBsuid"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("~\Students\Reports\RPT\rptOffer_letter_Group.rpt")


        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub


    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim ROW As GridViewRow
    '    Dim I As Integer = 0

    '    For Each ROW In gvStudEnquiry.Rows
    '        I = I + 1
    '    Next
    '    Button1.Text = CStr(I)

    'End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim EQS_IDs As New StringBuilder

            Dim chk As CheckBox
            Dim Eqs_id As String = String.Empty
            Dim APL_ID As String = String.Empty
            Dim hash As New Hashtable
            If Not Session("hashCheck_eqsIds") Is Nothing Then
                hash = Session("hashCheck_eqsIds")
            End If
            For Each rowItem As GridViewRow In gvStudEnquiry.Rows

                chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

                Eqs_id = DirectCast(rowItem.FindControl("lblEqsId"), Label).Text
                APL_ID = DirectCast(rowItem.FindControl("lblAPL_ID"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(Eqs_id) = False Then
                        hash.Add(Eqs_id, Eqs_id & "_" & APL_ID)
                    End If
                Else
                    If hash.Contains(Eqs_id) = True Then
                        hash.Remove(Eqs_id)
                    End If
                End If



            Next

            Session("hashCheck_eqsIds") = hash


            If Not Session("hashCheck_eqsIds") Is Nothing Then
                hash = Session("hashCheck_eqsIds")
                Dim hashloop As DictionaryEntry

                For Each hashloop In hash
                    EQS_IDs.Append(hashloop.Value)
                    EQS_IDs.Append("|")

                Next


            End If


            If Trim(EQS_IDs.ToString) <> "" Then







                Dim str_conn = ConnectionManger.GetOASISConnectionString

                Dim str_query As String = String.Empty

                str_query = " SELECT DISTINCT  ENQUIRY_SCHOOLPRIO_S.EQS_APPLNO AS 'Enquiry Number',ISNULL(ENQUIRY_M.EQM_APPLFIRSTNAME, '') AS StudentFirstName, ISNULL(ENQUIRY_M.EQM_APPLMIDNAME, '') AS StudentMiddleName, " & _
                    " ISNULL(ENQUIRY_M.EQM_APPLLASTNAME, '') AS StudentLastName,replace(CONVERT( CHAR(12), isnull(ENQUIRY_M.EQM_APPLDOB,''), 106 ),' ','/') as 'Date of Birth', " & _
                 " ACADEMICYEAR_M.ACY_DESCR AS 'Year',replace(CONVERT( CHAR(12), isnull(ENQUIRY_SCHOOLPRIO_S.EQS_DOJ,''), 106 ),' ','/') as 'Date of join'," & _
            " upper(TRM_M.TRM_DESCRIPTION) as 'Term of Join',GRADE_BSU_M.GRM_DESCR AS 'Grade', " & _
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FFIRSTNAME, '')" & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MFIRSTNAME, '') " & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GFIRSTNAME, '') END AS PARENT_FIRSTNAME, " & _
                         "  CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FMIDNAME, ' ') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MMIDNAME, ' ') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GMIDNAME, ' ') END AS PARENT_MIDDLENAME, " & _
                         "  CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FLASTNAME, '') " & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MLASTNAME, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GLASTNAME, '') END AS PARENT_LASTNAME," & _
         "  CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMSTREET, '') " & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMSTREET, '') " & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMSTREET, '') END AS PARENT_STREET," & _
                        " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMAREA, '') " & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MCOMAREA, '') " & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GCOMAREA, '') END AS 'Area'," & _
                        " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMBLDG, '') " & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MCOMBLDG, '') " & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GCOMBLDG, '') END AS 'Building'," & _
                        " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMAPARTNO, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MCOMAPARTNO, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GCOMAPARTNO, '') END AS 'Apartment No', " & _
                         " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FRESPHONE, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MRESPHONE, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GRESPHONE, '') END AS PARENT_RESPHONE, " & _
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FOFFPHONE, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MOFFPHONE, '') " & _
                         "  WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GOFFPHONE, '') END AS PARENT_OFFICE, " & _
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FMOBILE, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MMOBILE, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GMOBILE, '') END AS PARENT_MOBILE, " & _
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FEMAIL, '')" & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MEMAIL, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GEMAIL, '') END AS PARENT_MAIL, " & _
                          " CASE WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'F' THEN ISNULL(ENQUIRY_PARENT_M.EQP_FCOMPOBOX, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'M' THEN ISNULL(ENQUIRY_PARENT_M.EQP_MCOMPOBOX, '') " & _
                          " WHEN ENQUIRY_M.EQM_PRIMARYCONTACT = 'G' THEN ISNULL(ENQUIRY_PARENT_M.EQP_GCOMPOBOX, '') END AS PARENT_POBOX " & _
                          " FROM  ENQUIRY_M INNER JOIN  ENQUIRY_SCHOOLPRIO_S ON ENQUIRY_M.EQM_ENQID = ENQUIRY_SCHOOLPRIO_S.EQS_EQM_ENQID INNER JOIN " & _
                          " ENQUIRY_PARENT_M ON ENQUIRY_M.EQM_ENQID = ENQUIRY_PARENT_M.EQP_EQM_ENQID INNER JOIN " & _
                          " GRADE_BSU_M ON ENQUIRY_SCHOOLPRIO_S.EQS_GRD_ID = GRADE_BSU_M.GRM_GRD_ID AND " & _
                          " ENQUIRY_SCHOOLPRIO_S.EQS_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
                          " ACADEMICYEAR_M INNER JOIN   ACADEMICYEAR_D ON ACADEMICYEAR_M.ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID ON " & _
                          " ENQUIRY_SCHOOLPRIO_S.EQS_ACD_ID = ACADEMICYEAR_D.ACD_ID left outer JOIN " & _
                          " TRM_M ON ACADEMICYEAR_D.ACD_ID = TRM_M.TRM_ACD_ID AND ENQUIRY_M.EQM_TRM_ID = TRM_M.TRM_ID where  ENQUIRY_SCHOOLPRIO_S.EQS_ID in (SELECT str_data1 FROM fn_Split_to_table_2Col('" & EQS_IDs.ToString & "','|'))"



                Dim ds As New DataSet
                'Dim dt As New DataTable()
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                'If ds.Tables(0).Rows.Count > 0 Then


                Dim grdView As New GridView
                grdView.DataSource = ds
                grdView.DataBind()

                Dim str_err As String = String.Empty
                Dim errorMessage As String = String.Empty

                Response.Clear()
                Response.ClearHeaders()
                Response.ContentType = "application/vnd.ms-excel"
                'Response.Clear()
                'Response.AddHeader("content-disposition", String.Format("inline;filename={0}.xls", "student"))
                Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", "student"))

                'Response.Charset = ""
                'Response.ContentType = "application/vnd.xls"
                'Response.ContentType = "application/ms-excel"

                Dim stringWrite As New StringWriter()
                Dim htmlWrite As New HtmlTextWriter(stringWrite)
                grdView.RenderControl(htmlWrite)
                Dim htmlString As String = stringWrite.ToString()
                Response.Write(htmlString)
                Response.Flush()

            Else

                lblError.Text = "No record selected for export!!!"
            End If

        Catch ex As Exception
            lblError.Text = "Unexpected error"
            UtilityObj.Errorlog("btnExport", ex.Message)
        Finally
            Response.End()
        End Try
    End Sub
    Protected Sub lbtnReopen_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim transaction As SqlTransaction
        Dim calltransaction As String = String.Empty
        Dim errorMessage As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                Dim lblEqsId As New Label
                lblEqsId = TryCast(sender.FindControl("lblEqsId"), Label)
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EQS_ACD_ID", ddlAcademicYear.SelectedValue.ToString)
                pParms(1) = New SqlClient.SqlParameter("@EQS_ID", lblEqsId.Text)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ENQ].[SaveReopen_Cancel_Enquiry]", pParms)
                Dim ReturnFlag As Integer = pParms(2).Value
                
                If ReturnFlag <> 0 Then
                    calltransaction = "1"

                Else
                    calltransaction = "0"

                    lblError.Text = "Applicant enquiry reopened sucessfully!!! "

                End If


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Request could not be processed "
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else

                    errorMessage = ""
                    transaction.Commit()
                    GridBind()
                End If
            End Try


           
        End Using
    End Sub

    Protected Sub lbtnResend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim transaction As SqlTransaction
        Dim calltransaction As String = String.Empty
        Dim errorMessage As String = String.Empty
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                Dim lblEqsId As New Label
                lblEqsId = TryCast(sender.FindControl("lblEqsId"), Label)
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim pParms(2) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@EQS_ID", lblEqsId.Text)
                pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(1).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENQ.RESEND_EMAIL_GWS", pParms)
                Dim ReturnFlag As Integer = pParms(1).Value

                If ReturnFlag <> 0 Then
                    calltransaction = "1"

                Else
                    calltransaction = "0"

                    lblError.Text = "Applicant mail resend sucessfully!!! "

                End If


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Request could not be processed "
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else

                    errorMessage = ""
                    transaction.Commit()
                    GridBind()
                End If
            End Try



        End Using
    End Sub
    Protected Sub lbtnResend1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim transaction As SqlTransaction
        Dim calltransaction As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim str As String = ""
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try


                Dim lblEnqId As New Label
                lblEnqId = TryCast(sender.FindControl("lblEnqId"), Label)

                Dim lblEqsId As New Label
                lblEqsId = TryCast(sender.FindControl("lblEqsId"), Label)

                str = lblEnqId.Text + "|" + Encr_decrData.Encrypt(lblEnqId.Text + "|" + lblEqsId.Text) + ","

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim pParms(4) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@delimitedStr", str)
                pParms(1) = New SqlClient.SqlParameter("@ENQ_ID", lblEnqId.Text)
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENQ.RESEND_EMAIL_ACK1", pParms)
                Dim ReturnFlag As Integer = pParms(2).Value

                If ReturnFlag <> 0 Then
                    calltransaction = "1"

                Else
                    calltransaction = "0"

                    lblError.Text = "Applicant mail resend sucessfully!!! "

                End If


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Request could not be processed "
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else

                    errorMessage = ""
                    transaction.Commit()
                    GridBind()
                End If
            End Try



        End Using
    End Sub

    Protected Sub lbtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblEqsId As New Label
        lblEqsId = TryCast(sender.FindControl("lblEqsId"), Label)
        Dim lblStatus As New Label
        Dim lblCurrStatus As New Label

        Dim EQS_STATUS As String = String.Empty
        Dim EQS_CURRSTATUS As String = String.Empty
        Dim ACD_ID As String = ddlAcademicYear.SelectedValue.ToString
        Dim url As String = String.Empty
        lblStatus = TryCast(sender.FindControl("lblEQS_STATUS"), Label)
        lblCurrStatus = TryCast(sender.FindControl("lblCurrStatus"), Label)
        Dim MainMnu_code As String = String.Empty
        Dim datamode As String = Encr_decrData.Encrypt("view")
       
        If lblStatus.Text = "OFR" And lblCurrStatus.Text = "6" Then
            'cancelling offer
            MainMnu_code = Encr_decrData.Encrypt("S100035")
            url = String.Format("~\Students\studRegOfferCancel_M.aspx?MainMnu_code={0}&datamode={1}&eqsid={2}&mode={3}", MainMnu_code, datamode, Encr_decrData.Encrypt(lblEqsId.Text), "OFFER")

        ElseIf lblStatus.Text <> "DEL" And CInt(lblCurrStatus.Text) >= 3 Then
            MainMnu_code = Encr_decrData.Encrypt("S100040")

            url = String.Format("~\Students\studRegOfferCancel_M.aspx?MainMnu_code={0}&datamode={1}&eqsid={2}&mode={3}", MainMnu_code, datamode, Encr_decrData.Encrypt(lblEqsId.Text), "REG")
            'register stage
        ElseIf lblStatus.Text <> "DEL" And CInt(lblCurrStatus.Text) = 2 Then
            MainMnu_code = Encr_decrData.Encrypt("S100034")
            ACD_ID = Encr_decrData.Encrypt(ACD_ID)
            url = String.Format("~\Students\studRegOfferCancel_M.aspx?MainMnu_code={0}&datamode={1}&eqsid={2}&mode={3}&ACD_ID={4}", MainMnu_code, datamode, Encr_decrData.Encrypt(lblEqsId.Text), "ENQ", ACD_ID)

        End If
        'EQS_CANCELDATE IS NULL AND EQS_STATUS<>'ENR' AND EQS_STATUS<>'DEL'"
        'EQS_STATUS<>'DEL'  AND EQS_CURRSTATUS=2

        Response.Redirect(url)
    End Sub

    Protected Sub btnSIB_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If btnSIB.Text = "Show All" Then
            btnSIB.Text = "Show Enquiry With Sibling"
        Else
            btnSIB.Text = "Show All"
        End If

        GridBind()

    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim ChkState As Boolean = False

        Dim chk As CheckBox
        Dim ENQID As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
        End If
        For Each rowItem As GridViewRow In gvStudEnquiry.Rows

            chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)

            ENQID = DirectCast(rowItem.FindControl("lblEnqId"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(ENQID) = False Then
                    hash.Add(ENQID, DirectCast(rowItem.FindControl("lblEnqId"), Label).Text)
                End If
            Else
                If hash.Contains(ENQID) = True Then
                    hash.Remove(ENQID)
                End If
            End If
        Next

        Session("hashCheck") = hash

        If Not Session("hashCheck") Is Nothing Then
            CallReport()
        Else
            lblError.Text = "No Enquiry selected for print"
        End If
    End Sub


    Private Sub CallReport()

        Dim EQS_EQM_ENQIDs As New StringBuilder
        Dim hash As New Hashtable

        If Not Session("hashCheck") Is Nothing Then
            hash = Session("hashCheck")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                EQS_EQM_ENQIDs.Append(hashloop.Value)
                EQS_EQM_ENQIDs.Append("|")
            Next
        End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@EQS_EQM_ENQIDs", EQS_EQM_ENQIDs.ToString)
        param.Add("@STU_BSU_ID", Session("sBsuid"))
        param.Add("ACD_YEAR", ddlAcademicYear.SelectedItem.Text)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("..\Students\Reports\RPT\rptENQUIRY_NEW_LIST.rpt")
        End With
        Session.Remove("hashCheck")
        Session("rptClass") = rptClass
        'Dim jscript As New StringBuilder()

        'Dim URL As String = "../Reports/ASPX Report/rptReportViewer.aspx"
        'jscript.Append("<script>window.open('")
        'jscript.Append(URL)
        'jscript.Append("');</script>")
        'Page.RegisterStartupScript("OpenWindows", jscript.ToString())
        ReportLoadSelection()
    End Sub
    Sub bind_cur_status()
        ddlFollowupStatus.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT EFR_ID,EFR_REASON   FROM ENQ_FOLLOW_UP_REASON_M  "



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlFollowupStatus.DataSource = ds
        ddlFollowupStatus.DataTextField = "EFR_REASON"
        ddlFollowupStatus.DataValueField = "EFR_ID"
        ddlFollowupStatus.DataBind()
        ddlFollowupStatus.Items.Add(New ListItem("ALL", "0"))
        ddlFollowupStatus.Items.FindByText("ALL").Selected = True
    End Sub

    Protected Sub ddlFollowupStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFollowupStatus.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


End Class
