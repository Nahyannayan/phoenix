﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports Telerik.Web.UI
Partial Class Students_Covid_Relief_Parentportal
    Inherits System.Web.UI.Page
    Dim encr_decr As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            ViewState("RRH_ID") = 0
            ViewState("mode") = "add"
            disable_ctrl()
            If Request.QueryString("RRHID") IsNot Nothing AndAlso Request.QueryString("RRHID").Trim <> "" Then
                ViewState("RRH_ID") = GetIntegerVal(encr_decr.Decrypt(Request.QueryString("RRHID").Replace(" ", "+")))
                GetSavedRequest()
                ViewState("mode") = "edit"
            End If

            bind_covid_parent(ViewState("parent_username"))
            bind_covid_Student(ViewState("parent_username"))

            ddlFReasonRelief.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlMReasonRelief.Items.Insert(0, New ListItem("--Select--", "0"))
        End If
    End Sub
    Public Function GetIntegerVal(ByVal Value As Object) As Integer
        GetIntegerVal = 0
        Try
            If IsNumeric(Value) Then
                GetIntegerVal = Convert.ToInt32(Value)
            End If
        Catch ex As Exception
            GetIntegerVal = 0
        End Try
    End Function
    Sub disable_ctrl()
        ddlFempType.Enabled = False
        ddlFReasonRelief.Enabled = False
        txtFNationID.Enabled = False
        txtFCompany.Enabled = False
        txtFHousing.Enabled = False
        txtFOthers.Enabled = False
        txtFsalAllow.Enabled = False
        txtFTution.Enabled = False

        ddlMEmpType.Enabled = False
        ddlMReasonRelief.Enabled = False
        txtMNationID.Enabled = False
        txtMCompany.Enabled = False
        txtMHousing.Enabled = False
        txtMOthers.Enabled = False
        txtMSalAllow.Enabled = False
        txtMTution.Enabled = False
    End Sub
    Private Sub GetSavedRequest()
        If GetIntegerVal(ViewState("RRH_ID")) > 0 Then
            Dim pParm(0) As SqlClient.SqlParameter
            pParm(0) = New SqlClient.SqlParameter("@RRH_ID", SqlDbType.Int) With
                       {.Value = ViewState("RRH_ID")}
            Dim dsReq As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, "[COV].[GET_COV_RELIEF_REQUEST_H]", pParm)
            If Not dsReq Is Nothing AndAlso dsReq.Tables(0).Rows.Count > 0 Then
                '----------Father info
                ddlFempType.SelectedValue = dsReq.Tables(0).Rows(0)("RRH_F_EMPLOYMENT_TYPE")
                lblFempType.Text = ddlFempType.SelectedItem.Text
                ddlFempType_SelectedIndexChanged(Nothing, Nothing)
                ddlFReasonRelief.SelectedValue = dsReq.Tables(0).Rows(0)("RRH_F_RELIEF_REASON")
                lblFreasonrelief.Text = IIf(ddlFReasonRelief.SelectedValue <> 0, ddlFReasonRelief.SelectedItem.Text, "")
                txtFNationID.Text = dsReq.Tables(0).Rows(0)("RRH_F_NATIONAL_ID")
                txtFCompany.Text = dsReq.Tables(0).Rows(0)("RRH_F_COMPANY_NAME")
                txtFsalAllow.Text = dsReq.Tables(0).Rows(0)("RRH_F_SALARY_ALLOWANCE")
                txtFHousing.Text = dsReq.Tables(0).Rows(0)("RRH_F_HOUSING")
                txtFTution.Text = dsReq.Tables(0).Rows(0)("RRH_F_TUITION")
                txtFOthers.Text = dsReq.Tables(0).Rows(0)("RRH_F_OTHERS")
                lblFSalary.Text = dsReq.Tables(0).Rows(0)("RRH_F_TOT_SALARY")
                '----------Mother info
                ddlMEmpType.SelectedValue = dsReq.Tables(0).Rows(0)("RRH_M_EMPLOYMENT_TYPE")
                lblMemptype.Text = ddlMEmpType.SelectedItem.Text
                ddlMEmpType_SelectedIndexChanged(Nothing, Nothing)
                ddlMReasonRelief.SelectedValue = dsReq.Tables(0).Rows(0)("RRH_M_RELIEF_REASON")
                lblMreasonforrelief.Text = IIf(ddlMReasonRelief.SelectedValue <> 0, ddlMReasonRelief.SelectedItem.Text, "")
                txtMNationID.Text = dsReq.Tables(0).Rows(0)("RRH_M_NATIONAL_ID")
                txtMCompany.Text = dsReq.Tables(0).Rows(0)("RRH_M_COMPANY_NAME")
                txtMSalAllow.Text = dsReq.Tables(0).Rows(0)("RRH_M_SALARY_ALLOWANCE")
                txtMHousing.Text = dsReq.Tables(0).Rows(0)("RRH_M_HOUSING")
                txtMTution.Text = dsReq.Tables(0).Rows(0)("RRH_M_TUITION")
                txtMOthers.Text = dsReq.Tables(0).Rows(0)("RRH_M_OTHERS")
                lblMSalary.Text = dsReq.Tables(0).Rows(0)("RRH_M_TOT_SALARY")
                '----------oTHER INFO
                txtRemarks.Text = dsReq.Tables(0).Rows(0)("RRH_REMARKS")
                ViewState("parent_username") = dsReq.Tables(0).Rows(0)("RRH_LOG_PAR_USER_ID")
                'GetUploadedFiles()
            End If
        End If
    End Sub
    Public Sub bind_covid_parent(ByVal username As String)
        Dim s As String = ""
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@OLU_NAME", username)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_PARENT_INFO", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblFName.Text = ds.Tables(0).Rows(0)("F_NAME").ToString
            lblMName.Text = ds.Tables(0).Rows(0)("M_NAME").ToString
            lblEmail.Text = ds.Tables(0).Rows(0)("F_EMAIL").ToString
            lblMobNo.Text = ds.Tables(0).Rows(0)("F_MOBILE").ToString
        End If
    End Sub
    Public Sub bind_covid_Student(ByVal username As String)
        Dim s As String = ""
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@OLU_NAME", username)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_HEADFER_INFO", param)
        If ds.Tables(0).Rows.Count > 0 Then
            rptStudDet.DataSource = ds.Tables(0)
            rptStudDet.DataBind()
        End If
    End Sub
    Public Sub bind_covid_FDocument(ByVal id As String)
        Dim s As String = ""
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@EMPL_TYPES", id)
        param(1) = New SqlParameter("@PARENT_TYPE", "F")
        param(2) = New SqlParameter("@RRH_ID", ViewState("RRH_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_APPLICABLE_DOCTYPES", param)
        If ds.Tables(0).Rows.Count > 0 Then
            rptFDocument.DataSource = ds.Tables(0)
            rptFDocument.DataBind()
        Else
            rptFDocument.DataSource = Nothing
            rptFDocument.DataBind()
        End If
        bind_covid_FReason(id)

    End Sub
    Public Sub bind_covid_FReason(ByVal id As String)
        Dim s As String = ""
        ddlFReasonRelief.Items.Clear()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@EMPL_TYPES", id)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_APPLICABLE_REASONS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlFReasonRelief.DataSource = ds
            ddlFReasonRelief.DataValueField = "RSN_ID"
            ddlFReasonRelief.DataTextField = "RSN_DESC"
            ddlFReasonRelief.DataBind()
        End If
        ddlFReasonRelief.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Public Sub bind_covid_MDocument(ByVal id As String)
        Dim s As String = ""
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@EMPL_TYPES", id)
        param(1) = New SqlParameter("@PARENT_TYPE", "M")
        param(2) = New SqlParameter("@RRH_ID", ViewState("RRH_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_APPLICABLE_DOCTYPES", param)
        If ds.Tables(0).Rows.Count > 0 Then
            rptMDocument.DataSource = ds.Tables(0)
            rptMDocument.DataBind()
        Else
            rptMDocument.DataSource = Nothing
            rptMDocument.DataBind()
        End If
        bind_covid_MReason(id)

    End Sub
    Public Sub bind_covid_MReason(ByVal id As String)
        Dim s As String = ""
        ddlMReasonRelief.Items.Clear()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@EMPL_TYPES", id)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_APPLICABLE_REASONS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlMReasonRelief.DataSource = ds

            ddlMReasonRelief.DataValueField = "RSN_ID"
            ddlMReasonRelief.DataTextField = "RSN_DESC"

            ddlMReasonRelief.DataBind()
        End If
        ddlMReasonRelief.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlFempType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFempType.SelectedIndexChanged
        bind_covid_FDocument(ddlFempType.SelectedValue)

        'lblFNationID.Text = ""
        'lblFCompany.Text = ""
        'lblFsalAllow.Text = ""
        'lblFDocError.Text = ""
        'lblDocError.Text = ""
        'spError.Attributes.Add("class", "")

        If ddlFempType.SelectedValue = 1 Then
            'ddlFReasonRelief.Enabled = True
            'txtFNationID.Enabled = True
            'txtFCompany.Enabled = True
            txtFHousing.Enabled = False
            txtFOthers.Enabled = False
            txtFsalAllow.Enabled = False
            txtFTution.Enabled = False
            txtFHousing.Text = "0"
            txtFOthers.Text = "0"
            txtFsalAllow.Text = "0"
            txtFTution.Text = "0"
        ElseIf ddlFempType.SelectedValue = 2 Then
            'ddlFReasonRelief.Enabled = True
            'txtFNationID.Enabled = True
            'txtFCompany.Enabled = True
            'txtFHousing.Enabled = True
            'txtFOthers.Enabled = True
            'txtFsalAllow.Enabled = True
            'txtFTution.Enabled = True
        Else
            ddlFReasonRelief.Enabled = False
            txtFNationID.Enabled = False
            txtFCompany.Enabled = False
            txtFHousing.Enabled = False
            txtFOthers.Enabled = False
            txtFsalAllow.Enabled = False
            txtFTution.Enabled = False
            txtFNationID.Text = ""
            txtFCompany.Text = ""
            txtFHousing.Text = "0"
            txtFOthers.Text = "0"
            txtFsalAllow.Text = "0"
            txtFTution.Text = "0"
        End If

    End Sub
    Protected Sub ddlMEmpType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMEmpType.SelectedIndexChanged
        bind_covid_MDocument(ddlMEmpType.SelectedValue)

        'lblMNationID.Text = ""
        'lblMCompany.Text = ""
        'lblMSalAllow.Text = ""
        'lblMDocError.Text = ""
        'lblMempType.Text = ""
        'lblMReasonRelief.Text = ""
        'lblDocError.Text = ""
        'spError.Attributes.Add("class", "")
        If ddlMEmpType.SelectedValue = 1 Then
            'ddlMReasonRelief.Enabled = True
            'txtMNationID.Enabled = True
            'txtMCompany.Enabled = True
            txtMHousing.Enabled = False
            txtMOthers.Enabled = False
            txtMSalAllow.Enabled = False
            txtMTution.Enabled = False
            txtMHousing.Text = "0"
            txtMOthers.Text = "0"
            txtMSalAllow.Text = "0"
            txtMTution.Text = "0"
        ElseIf ddlMEmpType.SelectedValue = 2 Then
            'ddlMReasonRelief.Enabled = True
            'txtMNationID.Enabled = True
            'txtMCompany.Enabled = True
            'txtMHousing.Enabled = True
            'txtMOthers.Enabled = True
            'txtMSalAllow.Enabled = True
            'txtMTution.Enabled = True
        Else
            ddlMReasonRelief.Enabled = False
            txtMNationID.Enabled = False
            txtMCompany.Enabled = False
            txtMHousing.Enabled = False
            txtMOthers.Enabled = False
            txtMSalAllow.Enabled = False
            txtMTution.Enabled = False
            txtMNationID.Text = ""
            txtMCompany.Text = ""
            txtMHousing.Text = "0"
            txtMOthers.Text = "0"
            txtMSalAllow.Text = "0"
            txtMTution.Text = "0"
        End If
    End Sub

    Protected Sub rptFDocument_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptFDocument.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If e.Item.DataItem Is Nothing Then
                Return
            Else
                Dim lblFileName As Label = e.Item.FindControl("lblFileName")
                If Not lblFileName Is Nothing AndAlso lblFileName.Text.Trim <> "" Then
                    lblFileName.Text = "Submitted"
                End If
                Dim lblFileName1 As Label = e.Item.FindControl("lblFileName1")
                If Not lblFileName1 Is Nothing AndAlso lblFileName1.Text.Trim <> "" Then
                    lblFileName1.Text = "Submitted"
                End If
            End If
        End If




    End Sub
End Class
