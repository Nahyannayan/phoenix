Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports InfoSoftGlobal
Partial Class Students_studAtt_Registration
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    '    Protected Sub lbtnStudName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '        Try


    '            '@ACD_ID INT='80',
    '            '@GRD_ID varchar(50)='KG1',
    '            '@SCT_ID BIGINT='6958',
    '            '@STARTDATE datetime='01/JUN/2008',
    '            '@ENDDATE datetime='31/MAY/2009',
    '            '@STU_ID bigint ='61421',
    '            '@ATT_TYPE VARCHAR(15)='Session1',
    '            '@WEEKEND1 varchar(12)='FRIDAY',
    '            '@WEEKEND2 varchar(12)='SATURDAY',
    '            '@TILL_DATE datetime='12/JUL/2008'


    '            Dim lblStud_ID As New Label
    '            Dim lbtnStudName As New LinkButton
    '            lbtnStudName = sender
    '            lblStudName.Text = lbtnStudName.Text

    '            lblStud_ID = TryCast(sender.FindControl("lblStud_ID"), Label)

    '            ViewState("G_STUID") = lblStud_ID.Text
    '            ViewState("G_ACD") = ddlAcdYear.SelectedItem.Value
    '            ViewState("G_SCT") = ddlSection.SelectedItem.Value
    '            ViewState("G_GRD") = ddlGrade.SelectedItem.Value
    '            ViewState("G_ATTTYPE") = Trim(ddlAttType.SelectedItem.Value)
    '            If ViewState("ATT_OPEN") = True Then
    '                ViewState("G_TILLDATE") = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
    '            Else
    '                If ddlAttDate.SelectedIndex = -1 Then
    '                    ViewState("G_TILLDATE") = ""
    '                Else
    '                    ViewState("G_TILLDATE") = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
    '                End If
    '            End If

    '            'need to be removed
    '            'ViewState("ACD_STARTDT") = "01/JUN/2008"
    '            ' ViewState("ACD_ENDDT") = "31/MAY/2009"
    '            'ViewState("BSU_WEEKEND1")
    '            'ViewState("BSU_WEEKEND2")


    '            lblSession.Text = Trim(ddlAttType.SelectedItem.Text)
    '            lblTotWorking.Text = GETTotal_WorkingDay(ViewState("G_ACD"), ViewState("G_GRD"), ViewState("G_SCT"), ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"))
    '            lblTillDateWorking.Text = GETTotal_WorkingDay_TillDate(ViewState("G_ACD"), ViewState("G_GRD"), ViewState("G_SCT"), ViewState("ACD_STARTDT"), ViewState("G_TILLDATE"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"))
    '            lblDate.Text = ViewState("G_TILLDATE")
    '            lblDayAbsent.Text = GETTotal_ATTENDANCE(ViewState("G_ACD"), ViewState("G_GRD"), ViewState("G_SCT"), ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"), ViewState("G_TILLDATE"), ViewState("G_ATTTYPE"), ViewState("G_STUID"))
    '            lblDayPres.Text = CStr(CInt(GetTotal_Present(ViewState("G_ACD"), ViewState("G_GRD"), ViewState("G_SCT"), ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"), ViewState("G_TILLDATE"), ViewState("G_ATTTYPE"))) - CInt(lblDayAbsent.Text))
    '            lblTotalAttMarked.Text = CInt(lblDayPres.Text) + CInt(lblDayAbsent.Text)
    '            bindAttChart()
    '            Dim urlString As String
    '            'urlString = String.Format("~/Students/StudAtt_Chart.aspx?G_ACD={0}&G_GRD={1}&G_SCT={2}&G_STUID={3}&G_ATTTYPE={4}&G_TILLDATE={5}&ACD_STARTDT={6}&ACD_ENDDT={7}&W1={8}&W2={9}", ViewState("G_ACD"), ViewState("G_GRD"), ViewState("G_SCT"), ViewState("G_STUID"), ViewState("G_ATTTYPE"), ViewState("G_TILLDATE"), ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"))
    '            '  Image1.ImageUrl = urlString
    '            imgStud.ImageUrl = TransferPath(lblStud_ID.Text)
    '            Me.mdlPopup.Show()
    '            Call backGround()
    '        Catch ex As Exception
    '            lblError.Text = "Unexpected error"
    '            '~/Students/StudAtt_Chart.aspx?G_ACD=<%=ViewState("G_ACD") %>&G_GRD=<%=ViewState("G_GRD") %>&G_SCT=<%=ViewState("G_SCT") %>&G_STUID=<%=ViewState("G_STUID") %>&G_ATTTYPE=<%=ViewState("G_ATTTYPE") %>&G_TILLDATE=<%=ViewState("G_TILLDATE") %>&ACD_STARTDT=<%=ViewState("ACD_STARTDT") %>&ACD_ENDDT=<%=ViewState("ACD_ENDDT") %>
    '        End Try
    '    End Sub
    '    Sub bindAttChart()
    '        Try

    '            Dim ACD As String = ViewState("G_ACD")
    '            Dim GRD As String = ViewState("G_GRD")
    '            Dim SCT As String = ViewState("G_SCT")
    '            Dim VSDT As String = ViewState("ACD_STARTDT")
    '            Dim EDT As String = ViewState("ACD_ENDDT")
    '            Dim W1 As String = ViewState("BSU_WEEKEND1")
    '            Dim W2 As String = ViewState("BSU_WEEKEND2")
    '            Dim TillDate As String = ViewState("G_TILLDATE")
    '            Dim Att_Type As String = ViewState("G_ATTTYPE")
    '            Dim STUID As String = ViewState("G_STUID")


    '            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

    '            Dim strXML As String
    '            Dim TMonth As String = String.Empty
    '            Dim TOT_ATT As String = String.Empty


    '            Dim arr() As String
    '            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}
    '            Dim i As Integer = 0

    '            Dim SDT As Date = VSDT

    '            strXML = ""
    '            strXML = strXML & "<graph caption='Yearly Attedance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"

    '            Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GETSTUATT_YEAR_STU_ID_SESSION(ACD, GRD, SCT, VSDT, EDT, STUID, Att_Type, W1, W2, TillDate)

    '                If readerStudent_Detail.HasRows = True Then
    '                    While readerStudent_Detail.Read



    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 0, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT1")) & "'  color='AFD8F8'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 1, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT2")) & "' color='F6BD0F'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 2, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT3")) & "' color='8BBA00'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 3, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT4")) & "' color='FF8E46'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 4, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT5")) & "' color='008E8E'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 5, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT6")) & "' color='D64646'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 6, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT7")) & "' color='8E468E'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 7, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT8")) & "' color='588526'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 8, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT9")) & "' color='B3AA00'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 9, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT10")) & "' color='008ED6'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 10, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT11")) & "' color='9D080D'/>"
    '                        strXML = strXML & "<set name=" + "'" & DateAdd(DateInterval.Month, 11, SDT).ToString("MMM") & "' value=" + "'" & Convert.ToString(readerStudent_Detail("MNT12")) & "' color='A186BE'/>"


    '                    End While
    '                    strXML = strXML & "</graph>"
    '                    FCLiteral.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "460", "300", False)
    '                Else
    '                    'lblerror.Text = "No Records Found "

    '                    strXML = ""
    '                    strXML = strXML & "<graph caption='Yearly Attedance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"
    '                    strXML = strXML & "<set name='Jan' value='' color='AFD8F8'/>"
    '                    strXML = strXML & "<set name='Feb' value='' color='F6BD0F'/>"
    '                    strXML = strXML & "<set name='Mar' value='' color='8BBA00'/>"
    '                    strXML = strXML & "<set name='Apr' value='' color='FF8E46'/>"
    '                    strXML = strXML & "<set name='May' value='' color='008E8E'/>"
    '                    strXML = strXML & "<set name='Jun' value='' color='D64646'/>"
    '                    strXML = strXML & "<set name='Jul' value='' color='8E468E'/>"
    '                    strXML = strXML & "<set name='Aug' value='' color='588526'/>"
    '                    strXML = strXML & "<set name='Sep' value='' color='B3AA00'/>"
    '                    strXML = strXML & "<set name='Oct' value='' color='008ED6'/>"
    '                    strXML = strXML & "<set name='Nov' value='' color='9D080D'/>"
    '                    strXML = strXML & "<set name='Dec' value='' color='A186BE'/>"
    '                    strXML = strXML & "</graph>"
    '                    FCLiteral.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "460", "300", False)
    '                End If
    '            End Using


    '        Catch ex As Exception
    '            lblError.Text = "ERROR WHILE RETREVING DATA"
    '        End Try
    '    End Sub
    '    Function TransferPath(ByVal STU_ID As String) As String


    '        Dim FilePath As String = GETStud_photoPath(STU_ID) 'get the image
    '        Dim Virtual_Path As String = Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
    '        If FilePath.Trim = "" Then
    '            TransferPath = Virtual_Path + "/NOIMG/no_image.jpg"
    '        Else
    '            TransferPath = Virtual_Path + FilePath
    '        End If


    '    End Function
    '    Private Function GETStud_photoPath(ByVal STU_ID As String) As String

    '        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"

    '        Dim RESULT As String
    '        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

    '            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
    '            command.CommandType = Data.CommandType.Text
    '            RESULT = command.ExecuteScalar
    '            SqlConnection.ClearPool(connection)
    '        End Using
    '        Return RESULT

    '    End Function
    '    Private Function GetTotal_Present(ByVal ACD As String, ByVal GRD As String, ByVal SCT As String, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String, ByVal TillDate As String, ByVal Att_Type As String) As String

    '        Dim sqlString As String = " SELECT  Count(ALG_ATTDT) FROM ATTENDANCE_LOG_GRADE where " & _
    '" ALG_ATTDT in(select WDATE from fn_TWORKINGDAYS_XLOGBOOK('" & ACD & "','" & GRD & "','" & SCT & "','" & SDT & "','" & EDT & "','" & W1 & "','" & W2 & "')  " & _
    '" WHERE WDATE<='" & TillDate & "') and ALG_ACD_ID='" & ACD & "' and ALG_GRD_ID='" & GRD & "' and ALG_SCT_ID='" & SCT & "' and ALG_ATT_TYPE='" & Att_Type & "'"

    '        Dim result As Object
    '        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

    '            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
    '            command.CommandType = Data.CommandType.Text
    '            result = command.ExecuteScalar
    '            SqlConnection.ClearPool(connection)
    '        End Using
    '        Return CStr(result)

    '    End Function
    '    Private Function GETTotal_ATTENDANCE(ByVal ACD As String, ByVal GRD As String, ByVal SCT As String, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String, ByVal TillDate As String, ByVal Att_Type As String, ByVal STUID As String) As String
    '        Dim CONN As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '        '        Dim sqlString As String = " SELECT   count([ALS_ID])  FROM [ATTENDANCE_LOG_STUDENT]where [ALS_ALG_ID] in " & _
    '        '" (SELECT DISTINCT ALG_ID FROM ATTENDANCE_LOG_GRADE where  ALG_ATTDT in( SELECT  ALG_ATTDT FROM ATTENDANCE_LOG_GRADE where " & _
    '        ' "  ALG_ATTDT in(select WDATE from fn_TWORKINGDAYS_XLOGBOOK('" & ACD & "','" & GRD & "','" & SCT & "','" & SDT & "','" & EDT & "','" & W1 & "','" & W2 & "') " & _
    '        ' " WHERE WDATE<='" & TillDate & "') and ALG_ACD_ID='" & ACD & "' and ALG_GRD_ID='" & GRD & "' and ALG_SCT_ID='" & SCT & "' and " & _
    '        '" ALG_ATT_TYPE='" & Att_Type & "')) and [ALS_STU_ID]='" & STUID & "' and ALS_APD_ID IN (SELECT DISTINCT [APD_ID] FROM [ATTENDANCE_PARAM_D]" & _
    '        '" WHERE [APD_ACD_ID]='" & ACD & "' AND [APD_APM_ID]=2 AND [APD_bSHOW]=1) "

    '        Dim pParms(16) As SqlClient.SqlParameter
    '        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD)
    '        pParms(1) = New SqlClient.SqlParameter("@GRD_ID", GRD)
    '        pParms(2) = New SqlClient.SqlParameter("@SCT_ID", SCT)
    '        pParms(3) = New SqlClient.SqlParameter("@STARTDATE", SDT)
    '        pParms(4) = New SqlClient.SqlParameter("@ENDDATE", EDT)
    '        pParms(5) = New SqlClient.SqlParameter("@WEEKEND1", W1)
    '        pParms(6) = New SqlClient.SqlParameter("@WEEKEND2", W1)
    '        pParms(7) = New SqlClient.SqlParameter("@STU_ID", STUID)
    '        pParms(8) = New SqlClient.SqlParameter("@TillDate", TillDate)
    '        pParms(9) = New SqlClient.SqlParameter("@bsu_id", Session("sBsuid"))
    '        pParms(10) = New SqlClient.SqlParameter("@Att_Type", Att_Type)
    '        Dim result As Object
    '        result = SqlHelper.ExecuteScalar(CONN, CommandType.StoredProcedure, "[ATT].[CALC_STUD_ABS_COUNT]", pParms)

    '        '" SELECT   count([ALS_ID])  FROM [ATTENDANCE_LOG_STUDENT]where [ALS_ALG_ID] in (SELECT  ALG_ID FROM ATTENDANCE_LOG_GRADE where " & _
    '        '" ALG_ATTDT in( SELECT  ALG_ATTDT FROM ATTENDANCE_LOG_GRADE where " & _
    '        '" ALG_ATTDT in(select WDATE from fn_TWORKINGDAYS_XLOGBOOK('" & ACD & "','" & GRD & "','" & SCT & "','" & SDT & "','" & EDT & "','" & W1 & "','" & W2 & "')  " & _
    '        '" WHERE WDATE<='" & TillDate & "') and ALG_ACD_ID='" & ACD & "' and ALG_GRD_ID='" & GRD & "' and ALG_SCT_ID='" & SCT & "' and ALG_ATT_TYPE='" & Att_Type & "') and [ALS_STU_ID]='" & STUID & "' and upper([ALS_STATUS])='ABSENT') "

    '        'Dim result As Object
    '        'Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

    '        '    Dim command As SqlCommand = New SqlCommand(sqlString, connection, )
    '        '    command.CommandType = Data.CommandType.StoredProcedure

    '        '    result = command.ExecuteScalar
    '        '    SqlConnection.ClearPool(connection)
    '        'End Using
    '        Return CStr(result)

    '    End Function
    '    
    '    Private Function GETTotal_WorkingDay(ByVal ACD As String, ByVal GRD As String, ByVal SCT As String, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String) As String

    '        Dim sqlString As String = "select count(WDATE)  from fn_TWORKINGDAYS_XLOGBOOK('" & ACD & "','" & GRD & "','" & SCT & "','" & SDT & "','" & EDT & "','" & W1 & "','" & W2 & "')"

    '        Dim result As Object
    '        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

    '            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
    '            command.CommandType = Data.CommandType.Text
    '            result = command.ExecuteScalar
    '            SqlConnection.ClearPool(connection)
    '        End Using
    '        Return CStr(result)

    '    End Function
    '    Private Function GETTotal_WorkingDay_TillDate(ByVal ACD As String, ByVal GRD As String, ByVal SCT As String, ByVal SDT As String, ByVal CurrentDate As String, ByVal W1 As String, ByVal W2 As String) As String

    '        Dim sqlString As String = "select count(WDATE)  from fn_TWORKINGDAYS_XLOGBOOK('" & ACD & "','" & GRD & "','" & SCT & "','" & SDT & "','" & CurrentDate & "','" & W1 & "','" & W2 & "')"

    '        Dim result As Object
    '        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

    '            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
    '            command.CommandType = Data.CommandType.Text
    '            result = command.ExecuteScalar
    '            SqlConnection.ClearPool(connection)
    '        End Using
    '        Return CStr(result)

    '    End Function
    'Sub bindAtt_Type()
    '    Try
    '        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
    '        Dim CheckDate As String
    '        ddlAttType.Items.Clear()
    '        If ViewState("ATT_OPEN") = True Then
    '            If ValidateDate() = "0" Then
    '                CheckDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
    '            End If

    '        Else
    '            If ddlAttDate.SelectedIndex = -1 Then
    '                CheckDate = ""
    '            Else
    '                CheckDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
    '                ' ddlAttDate.ClearSelection()
    '            End If
    '        End If
    '        Dim ATT_Types As String = String.Empty
    '        Dim GRD_ID As String = ddlGrade.SelectedItem.Value

    '        Dim separator As String() = New String() {"|"}


    '        Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GetAtt_type_NEW(ACD_ID, GRD_ID, CheckDate)

    '            While readerSTUD_ATT_ADD.Read
    '                ATT_Types = ATT_Types + Convert.ToString(readerSTUD_ATT_ADD("ATT_TYPE")) + "|"
    '            End While
    '        End Using

    '        Dim strSplitArr As String() = ATT_Types.Split(separator, StringSplitOptions.RemoveEmptyEntries)

    '        For Each arrStr As String In strSplitArr

    '            If Array.IndexOf(strSplitArr, "Ses1&Ses2") >= 0 Then
    '                ddlAttType.Items.Add(New ListItem("Session1", "Session1"))
    '                ddlAttType.Items.Add(New ListItem("Session2", "Session2"))
    '                Exit For
    '            Else
    '                ddlAttType.Items.Add(New ListItem(arrStr, arrStr))
    '            End If

    '        Next

    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub

    Sub GetAcademicSTARTDT_ENDDT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            str_Sql = " select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_StartDt"))
                ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_ENDDT"))
            Else
                ViewState("ACD_STARTDT") = ""
                ViewState("ACD_ENDDT") = ""

            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059016") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    hfDate.Value = DateTime.Now
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    btnSave.Attributes.Add("onclick", "hideButton()")
                    btnSave2.Attributes.Add("onclick", "hideButton()")
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call GetEmpID()
                    Call bindAcademic_Year()
                    Call bindMonthPRESENT()
                    Call bindDate()
                    Call BINDGROUP()
                    btnSave2.Visible = False
                    btnCancel.Visible = False
                    btnCancel2.Visible = False
                    Session("dt_ATT_GROUP") = CreateDataTable()
                    Session("dt_ATT_GROUP").Rows.Clear()
                    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                    Dim sql_query = "select BSU_ATT_OPEN from BUSINESSUNIT_M where BSU_ID='" & Session("sBsuid") & "'"
                    Dim ds As DataSet
                    ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
                    If ds.Tables(0).Rows.Count > 0 Then
                        ViewState("ATT_OPEN") = ds.Tables(0).Rows(0).Item("BSU_ATT_OPEN")

                    End If

                    If ViewState("ATT_OPEN") = True Then
                        ddlAttDate.Visible = False
                        txtDate.Visible = True
                        txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        imgCalendar.Visible = True
                        rfvDate.Visible = True
                    Else
                        ddlAttDate.Visible = True
                        txtDate.Visible = False
                        imgCalendar.Visible = False
                        rfvDate.Visible = False
                    End If
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub

    Sub bindCommon_DataReader()
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " Select * from(SELECT     ACADEMICYEAR_D.ACD_STARTDT,ACADEMICYEAR_D.ACD_ENDDT, " & _
" BUSINESSUNIT_M.BSU_WEEKEND1,BUSINESSUNIT_M.BSU_WEEKEND2, ISNULL(BUSINESSUNIT_M.BSU_bAPP_LEAVE_ABSENT,'False') AS BSU_bAPP_LEAVE_ABSENT, " & _
" isnull(BUSINESSUNIT_M.BSU_ATT_OPEN,'False') as BSU_ATT_OPEN,(select  usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "') as emp_id " & _
" FROM   ACADEMICYEAR_D INNER JOIN   BUSINESSUNIT_M ON ACADEMICYEAR_D.ACD_BSU_ID =  " & _
           " BUSINESSUNIT_M.BSU_ID WHERE(ACADEMICYEAR_D.ACD_ID = '" & ddlAcdYear.SelectedValue & "'))A"


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToString(readerStudent_Detail("ACD_STARTDT")))
                        ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToString(readerStudent_Detail("ACD_ENDDT")))
                        ViewState("BSU_WEEKEND1") = Convert.ToString(readerStudent_Detail("BSU_WEEKEND1"))
                        ViewState("BSU_WEEKEND2") = Convert.ToString(readerStudent_Detail("BSU_WEEKEND2"))
                        ViewState("bAPP_LEAVE_ABS") = Convert.ToBoolean(readerStudent_Detail("BSU_bAPP_LEAVE_ABSENT"))
                        ViewState("EMP_ID") = Convert.ToString(readerStudent_Detail("emp_id"))
                        ViewState("ATT_OPEN") = Convert.ToBoolean(readerStudent_Detail("BSU_ATT_OPEN"))

                    End While
                Else

                End If
            End Using
        Catch ex As Exception

        End Try

    End Sub

    Sub bindMonthPRESENT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2,ISNULL(BSU_bAPP_LEAVE_ABSENT,'False') AS bAPP_LEAVE_ABS " _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
                ViewState("bAPP_LEAVE_ABS") = ds.Tables(0).Rows(0)(2)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindDate()
        Dim preDate As Date = Now.AddDays(-7).Date
        Dim di As ListItem
        Dim i As Integer = 1
        Do While i <= 7


            'If Not (UCase(preDate.AddDays(i).DayOfWeek.ToString) = ViewState("BSU_WEEKEND1").ToUpper.Trim Or UCase(preDate.AddDays(i).DayOfWeek.ToString) = ViewState("BSU_WEEKEND2").ToUpper.Trim) Then
            Dim Display_DT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", preDate.AddDays(i).Date)
            di = New ListItem(Display_DT, Display_DT)
            ddlAttDate.Items.Add(di)
            '  End If
            i = i + 1
        Loop
        '   If Not (UCase(Now.Date.DayOfWeek.ToString) = ViewState("BSU_WEEKEND1").ToUpper.Trim Or UCase(Now.Date.DayOfWeek.ToString) = ViewState("BSU_WEEKEND2").ToUpper.Trim) Then
        Dim Select_DT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", Now.Date)
        ddlAttDate.ClearSelection()
        ddlAttDate.Items.FindByValue(Select_DT).Selected = True
        '  End If

    End Sub
    Sub GetEmpID()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcdYear_SelectedIndexChanged(ddlAcdYear, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BINDGROUP()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT     T.AVT_AVG_ID, G.AVG_GROUP_NAME FROM  ATT.ATTENDANCE_VGROUP_TEACHER  AS T INNER JOIN " & _
" ATT.ATTENDANCE_VERTICAL_GROUPS  AS G ON  T.AVT_AVG_ID = G.AVG_ID WHERE (T.AVT_EMP_ID =  '" & ViewState("EMP_ID") & "' ) " & _
" AND (T.AVT_TODT IS NULL) AND G.AVG_ACD_ID='" & ddlAcdYear.SelectedValue & "' ORDER BY AVG_GROUP_NAME "


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGroup.Items.Clear()
            ddlGroup.DataSource = ds.Tables(0)
            ddlGroup.DataTextField = "AVG_GROUP_NAME"
            ddlGroup.DataValueField = "AVT_AVG_ID"
            ddlGroup.DataBind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call GetAcademicSTARTDT_ENDDT()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            Dim t1 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            If t1 > t2 Then
                lblError.Text = "You cannot select a day greater than today!"
                txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Else


                If ddlAcdYear.SelectedIndex = -1 Then
                    lblError.Text = "Academic Year not selected"

                ElseIf ddlGroup.SelectedIndex = -1 Then
                    lblError.Text = "Group not selected"
                Else
                    GETPARAM_D()
                    If ViewState("ATT_OPEN") = True Then
                        If ValidateDate() = "0" Then
                            Call Add_clicked()
                            Call backGround()
                        End If
                    Else
                        Call Add_clicked()
                        Call backGround()

                    End If
                End If

            End If
        Catch ex As Exception
            lblError.Text = "You cannot select a day greater than today and must be a valid date!"
            txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        End Try
    End Sub
    Sub Add_clicked()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

        Dim AVG_ID As String
        If ddlGroup.SelectedIndex = -1 Then
            AVG_ID = ""
        Else
            AVG_ID = ddlGroup.SelectedItem.Value
        End If
       
        Dim EMP_ID As String = ViewState("EMP_ID")


        Dim lastAttDate As String
        If ViewState("ATT_OPEN") = True Then
            lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
        Else
            If ddlAttDate.SelectedIndex = -1 Then
                lastAttDate = ""
            Else
                lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                'ddlAttDate.ClearSelection()
            End If
        End If

        Try
            Dim DupCount As Integer = RecordCount(AVG_ID, lastAttDate)

            If DupCount = 0 Then
                'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                'Dim str_Sql As String

                'str_Sql = "select count(AVSG_STU_ID) from  ATT.ATTENDANCE_VSTUDENT_GROUP where AVSG_AVG_ID='" & AVG_ID & "' and AVSG_bREMOVE=0 "

                'totRow = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
                'If totRow > 0 Then
                ViewState("datamode") = "add"
                Call BindAdd_Attendance()
                If ViewState("ROWCHECK") = "YES" Then
                    If ViewState("Vaild_state") = "yes" Then
                        btnCancel.Visible = True
                        btnCancel2.Visible = True
                        btnSave2.Visible = True

                        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Else
                        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                        btnCancel.Visible = True
                        btnSave.Visible = False
                    End If

                    setControl()

                Else

                    lblError.Text = "Record currently not updated. Please Contact System Admin"
                End If
            Else

                lblError.Text = "Attendances already marked for the given date & Group!!!"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
   
    Function RecordCount(ByVal AVG_ID As String, ByVal lastAttDate As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String

        str_Sql = "select count(ALVG_ID)  from ATT.ATTENDANCE_LOG_VGROUP where ALVG_AVG_ID='" & AVG_ID & "' and ALVG_ATTDT='" & lastAttDate & "'  and  ALVG_ATT_TYPE='" & ddlAttType.SelectedValue & "'"


        RecordCount = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
    End Function
    Sub BindAdd_Attendance()
        Try
            Dim ACD_ID As String = ddlAcdYear.SelectedValue
            Dim AVG_ID As String = ddlGroup.SelectedValue
            Dim EMP_ID As String = ViewState("EMP_ID")
            Dim SessionType As String = ddlAttType.SelectedValue
           Dim lastAttDate As String
            If ViewState("ATT_OPEN") = True Then
                lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    lastAttDate = ""
                Else
                    lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                    'ddlAttDate.ClearSelection()
                End If
            End If

            Session("dt_ATT_GROUP").Rows.Clear()
            ViewState("idTr") = 1
            Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GETSTUDATT_REGIST_GROUP(ACD_ID, AVG_ID, lastAttDate, ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"), SessionType)
                If readerSTUD_ATT_ADD.HasRows = True Then
                    ViewState("ROWCHECK") = "YES"

                    While readerSTUD_ATT_ADD.Read
                        Dim rDt As DataRow
                        rDt = Session("dt_ATT_GROUP").NewRow
                        rDt("SRNO") = ViewState("idTr")
                        rDt("ALG_ID") = "0"
                        rDt("STU_ID") = Convert.ToString(readerSTUD_ATT_ADD("STU_ID"))
                        rDt("STU_NO") = Convert.ToString(readerSTUD_ATT_ADD("STU_NO"))
                        rDt("STUDNAME") = Convert.ToString(readerSTUD_ATT_ADD("STUDNAME"))
                        rDt("STATUS") = ""
                        rDt("REMARKS") = ""
                        rDt("SGENDER") = Convert.ToString(readerSTUD_ATT_ADD("SGENDER"))
                        rDt("APPLEAVE") = ""
                        rDt("minList") = Convert.ToString(readerSTUD_ATT_ADD("MINLIST"))
                        rDt("DAY1") = Convert.ToString(readerSTUD_ATT_ADD(11))
                        rDt("DAY2") = Convert.ToString(readerSTUD_ATT_ADD(12))
                        rDt("DAY3") = Convert.ToString(readerSTUD_ATT_ADD(13))
                        hfDay1.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(11))
                        hfDay2.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(12))
                        hfDay3.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(13))

                        Session("dt_ATT_GROUP").Rows.Add(rDt)
                        ViewState("idTr") = ViewState("idTr") + 1
                    End While
                Else
                    ViewState("ROWCHECK") = "NO"

                End If

            End Using


            If ViewState("ROWCHECK") <> "YES" Then
                Exit Sub
            End If
            Using readerSTUD_GRADE_SECTION_HOLIDAY As SqlDataReader = AccessStudentClass.GetSTUD_GRADE_SECTION_HOLIDAY_GROUP(AVG_ID, lastAttDate)
                If readerSTUD_GRADE_SECTION_HOLIDAY.HasRows Then
                    While readerSTUD_GRADE_SECTION_HOLIDAY.Read
                        ViewState("STYPE") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("STYPE"))
                        ViewState("WEEKEND1") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND1"))
                        ViewState("bWEEKEND1") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND1"))
                        ViewState("WEEKEND2") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND2"))
                        ViewState("bWEEKEND2") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND2"))
                    End While

                Else
                    ViewState("STYPE") = "Empty"
                    ViewState("WEEKEND1") = ""
                    ViewState("bWEEKEND1") = False
                    ViewState("WEEKEND2") = ""
                    ViewState("bWEEKEND2") = False

                End If

            End Using

            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindAdd_Attendance_APPROVED()
        Try
            Dim ACD_ID As String = ddlAcdYear.SelectedValue
            Dim AVG_ID As String = ddlGroup.SelectedValue
            Dim EMP_ID As String = ViewState("EMP_ID")

   
            Dim ALS_STU_ID As String = String.Empty
            Dim ALS_APD_ID As String = String.Empty
            Dim ALS_REMARKS As String = String.Empty
            Dim lastAttDate As String
            If ViewState("ATT_OPEN") = True Then
                lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    lastAttDate = ""
                Else
                    lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                    '  ddlAttDate.ClearSelection()
                End If
            End If


            Using readerGetALG_StudentID As SqlDataReader = AccessStudentClass.GetALG_StudentID_GROUP(AVG_ID, lastAttDate, "Session1")
                While readerGetALG_StudentID.Read
                    ALS_STU_ID = Convert.ToString(readerGetALG_StudentID("ALS_STU_ID"))
                    ALS_APD_ID = Convert.ToString(readerGetALG_StudentID("ALS_APD_ID"))
                    ALS_REMARKS = Convert.ToString(readerGetALG_StudentID("ALS_REMARKS"))
                    For j As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(j)
                        Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                        If Trim(ALS_STU_ID) = Trim(Temp_stud_ID) Then
                            ddlStatus.ClearSelection()
                            DirectCast(row.FindControl("txtRemarks"), TextBox).Text = ALS_REMARKS

                            If Not ddlStatus.Items.FindByValue(ALS_APD_ID) Is Nothing Then
                                ddlStatus.Items.FindByValue(ALS_APD_ID).Selected = True
                            End If

                        End If
                    Next
                End While
            End Using







            Dim Astud_ID As New ArrayList
            Using readerSTUD_Leave_Approval As SqlDataReader = AccessStudentClass.GetSTUD_Leave_Approval_Group(ACD_ID, AVG_ID, lastAttDate)
                While readerSTUD_Leave_Approval.Read
                    Dim Stud_ID As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_STU_ID"))
                    Dim Remarks As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_REMARKS"))
                    Dim AppLeave As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_APPRLEAVE"))
                    Dim SLA_APD_ID As Integer = Convert.ToInt64(readerSTUD_Leave_Approval("SLA_APD_ID"))
                    'Astud_ID.Add(Stud_ID)

                    For j As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(j)

                        Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)

                        If Trim(Stud_ID) = Trim(Temp_stud_ID) Then
                            ddlStatus.ClearSelection()
                            DirectCast(row.FindControl("txtRemarks"), TextBox).Text = Remarks
                            DirectCast(row.FindControl("lblAppLeave"), Label).Text = AppLeave
                            If AppLeave = "APPROVED" Then
                                Astud_ID.Add(Stud_ID)
                                If ViewState("bAPP_LEAVE_ABS") = "True" Then
                                    If SLA_APD_ID <> 0 Then
                                        If Not ddlStatus.Items.FindByValue(SLA_APD_ID) Is Nothing Then
                                            ddlStatus.Items.FindByValue(SLA_APD_ID).Selected = True
                                        Else
                                            ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                                        End If
                                    Else
                                        ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                                    End If
                                Else
                                    If SLA_APD_ID <> 0 Then
                                        If Not ddlStatus.Items.FindByValue(SLA_APD_ID) Is Nothing Then
                                            ddlStatus.Items.FindByValue(SLA_APD_ID).Selected = True
                                        Else
                                            ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                                        End If
                                    Else
                                        ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                                    End If
                                End If
                            ElseIf AppLeave = "NOT APPROVED" Then
                                ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                            End If
                        End If

                    Next
                End While
            End Using
            
            Session("Astud_ID_GROUP") = Astud_ID
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub gridbind()
        Try
            gvInfo.DataSource = Session("dt_ATT_GROUP")
            gvInfo.DataBind()
            Call SetSTYPE()
            Call CheckWeekend_Attendance()
            If ViewState("Vaild_state") = "yes" Then
                If ViewState("datamode") = "add" Then
                    Call BindAdd_Attendance_APPROVED()
                ElseIf ViewState("datamode") = "edit" Then
                    Call BindEdit_AttendanceAPPROVED()
                End If

                Call DisableControls()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub CheckWeekend_Attendance()
        ViewState("Vaild_state") = "yes"
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

        Dim AVG_ID As String
        If ddlGroup.SelectedIndex = -1 Then
            AVG_ID = ""
        Else
            AVG_ID = ddlGroup.SelectedItem.Value
        End If

        Dim TDATE As String
        Dim T_DATE As Date
        If ViewState("ATT_OPEN") = True Then
            TDATE = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
        Else

            TDATE = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
            ' ddlAttDate.ClearSelection()
        End If

        Dim Check_date As Integer

        Check_date = GETVALID_ATTDay(AVG_ID, ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"), TDATE)

        If Check_date > 0 Then
            T_DATE = TDATE
            If UCase(T_DATE.DayOfWeek.ToString) = ViewState("BSU_WEEKEND1").ToUpper.Trim Then
                If ViewState("WEEKEND1").Trim <> "" Then
                    ViewState("Vaild_state") = "yes"
                    Call SetSTYPE()
                Else
                    gvInfo.Visible = False
                    btnSave.Visible = False
                    btnSave2.Visible = False
                    ViewState("Vaild_state") = "no"
                End If
            ElseIf UCase(T_DATE.DayOfWeek.ToString) = ViewState("BSU_WEEKEND2").ToUpper.Trim Then
                If ViewState("WEEKEND2").Trim <> "" Then
                    ViewState("Vaild_state") = "yes"
                    Call SetSTYPE()
                Else
                    gvInfo.Visible = False
                    btnSave.Visible = False
                    btnSave2.Visible = False
                    ViewState("Vaild_state") = "no"
                End If
            ElseIf Not (UCase(T_DATE.DayOfWeek.ToString) = ViewState("BSU_WEEKEND1").ToUpper.Trim Or UCase(T_DATE.DayOfWeek.ToString) = ViewState("BSU_WEEKEND2").ToUpper.Trim) Then
                ViewState("Vaild_state") = "yes"
                gvInfo.Visible = True
                btnSave.Visible = True
                btnSave2.Visible = True
                Call SetSTYPE()
            End If

        Else
            lblError.Text = "Date entered is not a working day"
            gvInfo.Visible = False
            btnSave.Visible = False
            btnSave2.Visible = False
            ViewState("Vaild_state") = "no"

        End If
    End Sub
    Private Function GETVALID_ATTDay(ByVal AVG_ID As String, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String, ByVal Check_DT As String) As Integer

        Dim sqlString As String = "declare @temp_acd_id int,@temp_grd_id varchar(10),@temp_sct_id int;" & _
" SELECT     @temp_grd_id=STUDENT_M.STU_GRD_ID, @temp_sct_id=STUDENT_M.STU_SCT_ID,@temp_acd_id= STUDENT_M.STU_ACD_ID " & _
" FROM         ATT.ATTENDANCE_VSTUDENT_GROUP INNER JOIN STUDENT_M ON ATT.ATTENDANCE_VSTUDENT_GROUP.AVSG_STU_ID = STUDENT_M.STU_ID AND  " & _
" ATT.ATTENDANCE_VSTUDENT_GROUP.AVSG_ACD_ID = STUDENT_M.STU_ACD_ID WHERE (ATT.ATTENDANCE_VSTUDENT_GROUP.AVSG_AVG_ID = 3) " & _
" AND (ATT.ATTENDANCE_VSTUDENT_GROUP.AVSG_bREMOVE = 0);" & _
" select count(WDATE)  from fn_TWORKINGDAYS_XLOGBOOK(@temp_acd_id ,@temp_grd_id,@temp_sct_id,'" & SDT & "','" & EDT & "','" & W1 & "','" & W2 & "') where WDATE='" & Check_DT & "'"

        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return CInt(result)

    End Function
    Sub SetSTYPE()
        If StrConv(ViewState("STYPE").Trim, VbStrConv.ProperCase) = "None" Then
            gvInfo.Visible = False
            btnSave.Visible = False
            btnSave2.Visible = False
            lblError.Text = "Date entered is not a working day"
            ViewState("Vaild_state") = "no"
        ElseIf StrConv(ViewState("STYPE").Trim, VbStrConv.ProperCase) = "All" Then
            gvInfo.Visible = True
            btnSave.Visible = True
            btnSave2.Visible = True
            ViewState("Vaild_state") = "yes"
        ElseIf StrConv(ViewState("STYPE").ToString.Trim, VbStrConv.ProperCase) = "Female" Then
            For j As Integer = 0 To gvInfo.Rows.Count - 1
                Dim row As GridViewRow = gvInfo.Rows(j)
                Dim SGENDER As String = DirectCast(row.FindControl("lblSGENDER"), Label).Text
                Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                'ddlStatus.ClearSelection()
                'ddlStatus.SelectedIndex = 0

                'Dim STR1 As String = ddlStatus.SelectedItem.Text   'UCase(Trim(ddlStatus.SelectedItem.Text))
                'ddlStatus.ClearSelection()
                ''STR1 = STR1.Substring(0, STR1.Length - 3)

                If (StrConv(SGENDER.Trim, VbStrConv.ProperCase) = "Male" Or StrConv(SGENDER.Trim, VbStrConv.Uppercase) = "M") Then
                    'All male will be checked by default
                    'DirectCast(row.FindControl("chkPresent"), CheckBox).Checked = True
                    'DirectCast(row.FindControl("chkLate"), CheckBox).Checked = False

                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                    ddlStatus.Enabled = False
                    'DirectCast(row.FindControl("ddlStatus"), DropDownList).SelectedIndex = 0
                    'DirectCast(row.FindControl("ddlStatus"), DropDownList).Enabled = False

                    'DirectCast(row.FindControl("chkPresent"), CheckBox).Enabled = False
                    'DirectCast(row.FindControl("chkLate"), CheckBox).Enabled = False
                    DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
                End If
            Next
            gvInfo.Visible = True
            btnSave.Visible = True
            btnSave2.Visible = True
            ViewState("Vaild_state") = "yes"
        ElseIf StrConv(ViewState("STYPE").Trim, VbStrConv.ProperCase) = "Male" Then
            For j As Integer = 0 To gvInfo.Rows.Count - 1
                Dim row As GridViewRow = gvInfo.Rows(j)
                Dim SGENDER As String = DirectCast(row.FindControl("lblSGENDER"), Label).Text
                Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)

                If (StrConv(SGENDER.Trim, VbStrConv.ProperCase) = "Female" Or StrConv(SGENDER.Trim, VbStrConv.Uppercase) = "F") Then
                    'All male will be checked by default
                    'DirectCast(row.FindControl("chkPresent"), CheckBox).Checked = True
                    'DirectCast(row.FindControl("chkLate"), CheckBox).Checked = False


                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                    ddlStatus.Enabled = False
                    'DirectCast(row.FindControl("ddlStatus"), DropDownList).SelectedIndex = 0
                    'DirectCast(row.FindControl("ddlStatus"), DropDownList).Enabled = False

                    'DirectCast(row.FindControl("chkPresent"), CheckBox).Enabled = False
                    'DirectCast(row.FindControl("chkLate"), CheckBox).Enabled = False
                    DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
                End If
            Next
            gvInfo.Visible = True
            btnSave.Visible = True
            btnSave2.Visible = True
            ViewState("Vaild_state") = "yes"
        End If
    End Sub
    Sub DisableControls()
        For j As Integer = 0 To gvInfo.Rows.Count - 1
            Dim row As GridViewRow = gvInfo.Rows(j)
            Dim G_Stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
            If Session("Astud_ID_GROUP").Contains(G_Stud_ID) Then

                DirectCast(row.FindControl("ddlStatus"), DropDownList).Enabled = False

                'DirectCast(row.FindControl("chkPresent"), CheckBox).Enabled = False
                'DirectCast(row.FindControl("chkLate"), CheckBox).Enabled = False

                DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
            End If
        Next
    End Sub

    Sub bindWeekEndstatus()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim ALG_ID As New DataColumn("ALG_ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STUDNAME As New DataColumn("STUDNAME", System.Type.GetType("System.String"))
            'Dim PRESENT As New DataColumn("PRESENT", System.Type.GetType("System.Boolean"))
            'Dim LATE As New DataColumn("LATE", System.Type.GetType("System.Boolean"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim SGENDER As New DataColumn("SGENDER", System.Type.GetType("System.String"))
            Dim APPLEAVE As New DataColumn("APPLEAVE", System.Type.GetType("System.String"))
            Dim MinList As New DataColumn("MinList", System.Type.GetType("System.String"))
            Dim DAY1 As New DataColumn("DAY1", System.Type.GetType("System.String"))
            Dim DAY2 As New DataColumn("DAY2", System.Type.GetType("System.String"))
            Dim DAY3 As New DataColumn("DAY3", System.Type.GetType("System.String"))
            'Dim DAY4 As New DataColumn("DAY4", System.Type.GetType("System.String"))
            'Dim DAY5 As New DataColumn("DAY5", System.Type.GetType("System.String"))


            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(ALG_ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NO)
            dtDt.Columns.Add(STUDNAME)
            'dtDt.Columns.Add(PRESENT)
            'dtDt.Columns.Add(LATE)
            dtDt.Columns.Add(STATUS)
            dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(SGENDER)
            dtDt.Columns.Add(APPLEAVE)
            dtDt.Columns.Add(MinList)

            dtDt.Columns.Add(DAY1)
            dtDt.Columns.Add(DAY2)
            dtDt.Columns.Add(DAY3)
            'dtDt.Columns.Add(DAY4)
            'dtDt.Columns.Add(DAY5)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        If ddlAcdYear.SelectedIndex = -1 Then
            lblError.Text = "Academic Year not selected"

        ElseIf ddlGroup.SelectedIndex = -1 Then
            lblError.Text = "Group not selected"
        Else
            GETPARAM_D()
            If ViewState("ATT_OPEN") = True Then
                If ValidateDate() = "0" Then

                    Call Edit_clicked()
                    Call backGround()
                End If
            Else
                Call Edit_clicked()
                Call backGround()

            End If
        End If
    End Sub

    Sub Edit_clicked()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

        Dim AVG_ID As String
        If ddlGroup.SelectedIndex = -1 Then
            AVG_ID = ""
        Else
            AVG_ID = ddlGroup.SelectedValue
        End If
        
        Dim EMP_ID As String = ViewState("EMP_ID")
        Dim lastAttDate As String



        If ViewState("ATT_OPEN") = True Then
            lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
        Else
            If ddlAttDate.SelectedIndex = -1 Then
                lastAttDate = ""
            Else
                lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                'ddlAttDate.ClearSelection()
            End If

        End If


        Try
            Using readerALS_ID As SqlDataReader = AccessStudentClass.GetALVG_ID(AVG_ID, lastAttDate, ddlAttType.SelectedValue)
                If readerALS_ID.HasRows Then
                    While readerALS_ID.Read

                        ViewState("ALVG_ID") = Convert.ToString(readerALS_ID("ALVG_ID"))

                    End While
                Else
                    ViewState("ALVG_ID") = ""
                End If

            End Using


            If ViewState("ALVG_ID") <> "" Then



                'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                'Dim str_Sql As String

                'str_Sql = "select count(AVSG_STU_ID) from  ATT.ATTENDANCE_VSTUDENT_GROUP where AVSG_AVG_ID='" & AVG_ID & "' and AVSG_bREMOVE=0 "
                'totRow = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
                ViewState("datamode") = "edit"
                Call BindEdit_Attendance()
                If ViewState("ROWCHECK") = "YES" Then



                    btnCancel.Visible = True
                    btnCancel2.Visible = True
                    btnSave2.Visible = True
                    setControl()
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                Else

                    lblError.Text = "Record currently not updated. Please Contact System Admin"
                End If
            Else

                lblError.Text = "Attendance not marked for the given date & group!!!"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub BindEdit_AttendanceAPPROVED()
        Try

            Dim ACD_ID As String = ddlAcdYear.SelectedValue
            Dim AVG_ID As String = ddlGroup.SelectedValue
            Dim EMP_ID As String = ViewState("EMP_ID")
            Dim ALS_STU_ID As String = String.Empty
            Dim ALS_APD_ID As String = String.Empty
            Dim ALS_REMARKS As String = String.Empty
            Dim lastAttDate As String
            If ViewState("ATT_OPEN") = True Then
                lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    lastAttDate = ""
                Else
                    lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                End If
            End If
            Using readerGetALG_StudentID As SqlDataReader = AccessStudentClass.GetALG_StudentID_GROUP(AVG_ID, lastAttDate, ddlAttType.SelectedValue)
                While readerGetALG_StudentID.Read
                    ALS_STU_ID = Convert.ToString(readerGetALG_StudentID("ALS_STU_ID"))
                    ALS_APD_ID = Convert.ToString(readerGetALG_StudentID("ALS_APD_ID"))
                    ALS_REMARKS = Convert.ToString(readerGetALG_StudentID("ALS_REMARKS"))
                    For j As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(j)
                        Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                        If Trim(ALS_STU_ID) = Trim(Temp_stud_ID) Then
                            ddlStatus.ClearSelection()
                            DirectCast(row.FindControl("txtRemarks"), TextBox).Text = ALS_REMARKS
                            ddlStatus.Items.FindByValue(ALS_APD_ID).Selected = True
                        End If
                    Next
                End While
            End Using

            Dim Astud_ID As New ArrayList
            Using readerSTUD_Leave_Approval As SqlDataReader = AccessStudentClass.GetSTUD_Leave_Approval_Group(ACD_ID, AVG_ID, lastAttDate)
                While readerSTUD_Leave_Approval.Read
                    Dim Stud_ID As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_STU_ID"))
                    Dim Remarks As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_REMARKS"))
                    Dim AppLeave As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_APPRLEAVE"))
                    Dim SLA_APD_ID As Integer = Convert.ToInt64(readerSTUD_Leave_Approval("SLA_APD_ID"))
                    'Astud_ID.Add(Stud_ID)
                    For j As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(j)
                        Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                        ' Dim STR1 As String = ddlStatus.Items(0).Text   'UCase(Trim(ddlStatus.SelectedItem.Text))

                        'STR1 = STR1.Substring(0, STR1.Length - 3)
                        '  If STR1.Substring(0, STR1.Length - 3) <> "PRESENT" Then

                        If Trim(Stud_ID) = Trim(Temp_stud_ID) Then
                            ddlStatus.ClearSelection()
                            DirectCast(row.FindControl("txtRemarks"), TextBox).Text = Remarks
                            DirectCast(row.FindControl("lblAppLeave"), Label).Text = AppLeave
                            Astud_ID.Add(Stud_ID)
                            If AppLeave = "APPROVED" Then
                                If ViewState("bAPP_LEAVE_ABS") = "True" Then
                                    If SLA_APD_ID <> 0 Then
                                        If Not ddlStatus.Items.FindByValue(SLA_APD_ID) Is Nothing Then
                                            ddlStatus.Items.FindByValue(SLA_APD_ID).Selected = True
                                        Else
                                            ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                                        End If
                                    Else
                                        ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                                    End If
                                Else
                                    If SLA_APD_ID <> 0 Then
                                        If Not ddlStatus.Items.FindByValue(SLA_APD_ID) Is Nothing Then
                                            ddlStatus.Items.FindByValue(SLA_APD_ID).Selected = True
                                        Else
                                            ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                                        End If
                                    Else
                                        ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                                    End If
                                End If
                            ElseIf AppLeave = "NOT APPROVED" Then
                                ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                            End If
                        End If
                    Next


                End While
            End Using
            ' gvInfo.DataSource =Session("dt_ATT_GROUP")
            ' gvInfo.DataBind()

            Session("Astud_ID_GROUP") = Astud_ID

        Catch ex As Exception

        End Try
    End Sub
    Sub BindEdit_Attendance()
        Try
            Dim ACD_ID As String = ddlAcdYear.SelectedValue
            Dim AVG_ID As String = ddlGroup.SelectedValue
            Dim EMP_ID As String = ViewState("EMP_ID")
            Dim SessionType As String = ddlAttType.SelectedValue

            Dim lastAttDate As String
            If ViewState("ATT_OPEN") = True Then
                lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Else
                If ddlAttDate.SelectedIndex = -1 Then
                    lastAttDate = ""
                Else
                    lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
                End If
            End If

            Session("dt_ATT_GROUP").Rows.Clear()
            ViewState("idTr") = 1

            Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GETSTUDATT_REGIST_GROUP(ACD_ID, AVG_ID, lastAttDate, ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"), SessionType)
                If readerSTUD_ATT_ADD.HasRows Then
                    ViewState("ROWCHECK") = "YES"

                    While readerSTUD_ATT_ADD.Read
                        Dim rDt As DataRow
                        rDt = Session("dt_ATT_GROUP").NewRow
                        rDt("SRNO") = ViewState("idTr")
                        rDt("ALG_ID") = "0"
                        rDt("STU_ID") = Convert.ToString(readerSTUD_ATT_ADD("STU_ID"))
                        rDt("STU_NO") = Convert.ToString(readerSTUD_ATT_ADD("STU_NO"))
                        rDt("STUDNAME") = Convert.ToString(readerSTUD_ATT_ADD("STUDNAME"))
                        'rDt("PRESENT") = True
                        'rDt("LATE") = False
                        rDt("STATUS") = ""
                        rDt("REMARKS") = ""
                        rDt("SGENDER") = Convert.ToString(readerSTUD_ATT_ADD("SGENDER"))
                        rDt("APPLEAVE") = ""
                        rDt("minList") = Convert.ToString(readerSTUD_ATT_ADD("MINLIST"))
                        rDt("DAY1") = Convert.ToString(readerSTUD_ATT_ADD(11))
                        rDt("DAY2") = Convert.ToString(readerSTUD_ATT_ADD(12))
                        rDt("DAY3") = Convert.ToString(readerSTUD_ATT_ADD(13))
                        'rDt("DAY4") = Convert.ToString(readerSTUD_ATT_ADD(14))
                        'rDt("DAY5") = Convert.ToString(readerSTUD_ATT_ADD(15))
                        hfDay1.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(11))
                        hfDay2.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(12))
                        hfDay3.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(13))
                        'hfDay4.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(14))
                        'hfDay5.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(15))
                        Session("dt_ATT_GROUP").Rows.Add(rDt)
                        ViewState("idTr") = ViewState("idTr") + 1
                    End While
                Else
                    ViewState("ROWCHECK") = "NO"

                End If
            End Using

            If ViewState("ROWCHECK") <> "YES" Then
                Exit Sub
            End If


            Using readerSTUD_GRADE_SECTION_HOLIDAY As SqlDataReader = AccessStudentClass.GetSTUD_GRADE_SECTION_HOLIDAY_GROUP(AVG_ID, lastAttDate)
                If readerSTUD_GRADE_SECTION_HOLIDAY.HasRows Then
                    While readerSTUD_GRADE_SECTION_HOLIDAY.Read
                        ViewState("STYPE") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("STYPE"))
                        ViewState("WEEKEND1") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND1"))
                        ViewState("bWEEKEND1") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND1"))
                        ViewState("WEEKEND2") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND2"))
                        ViewState("bWEEKEND2") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND2"))
                    End While

                Else
                    ViewState("STYPE") = "Empty"
                    ViewState("WEEKEND1") = ""
                    ViewState("bWEEKEND1") = False
                    ViewState("WEEKEND2") = ""
                    ViewState("bWEEKEND2") = False

                End If
            End Using

            gridbind()
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSave2.Click
        System.Threading.Thread.Sleep(20)

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        str_err = calltransaction(errorMessage)
        If str_err = "0" Then

            lblError.Text = "Record Saved Successfully"
            btnSave2.Visible = False
        Else
            lblError.Text = errorMessage
            ' Call backGround()
        End If

    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer

        Dim ALVG_ID As String = String.Empty
        Dim ALG_CREATEDDT As String = String.Empty
        Dim ALG_CREATEDTM As String = String.Empty
        Dim ALG_MODIFIEDDT As String = String.Empty
        Dim ALG_MODIFIEDTM As String = String.Empty
        Dim STR_XML As String = String.Empty
        Dim ALG_ATTDT As String
        Dim EMP_ID As String = ViewState("EMP_ID")
        Dim ALG_BSU_ID As String = Session("sBsuid")
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim ALS_STU_ID As String = String.Empty
        Dim ALS_APD_ID As String = String.Empty
        Dim ALS_REMARKS As String = String.Empty
        Dim AppLeave As String = String.Empty
        Dim ALVG_AVG_ID As String = ddlGroup.SelectedValue

        If ViewState("ATT_OPEN") = True Then
            ALG_ATTDT = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
        Else
            If ddlAttDate.SelectedIndex = -1 Then
                ALG_ATTDT = ""
            Else
                ALG_ATTDT = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
            End If
        End If

        Dim transaction As SqlTransaction

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Dim status As Integer
                    For i As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(i)

                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                        ALS_STU_ID = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        ALS_REMARKS = DirectCast(row.FindControl("txtRemarks"), TextBox).Text
                        AppLeave = DirectCast(row.FindControl("lblAppLeave"), Label).Text
                        If (ddlStatus.SelectedIndex <> 0) Then
                            If (AppLeave <> "APPROVED") Then
                                ALS_APD_ID = ddlStatus.SelectedItem.Value
                                STR_XML += String.Format("<STUDENT STUID='{0}' ALS_APD_ID='{1}' ALS_REMARKS='{2}'> </STUDENT>", ALS_STU_ID, ALS_APD_ID, ALS_REMARKS)
                            End If
                        End If
                    Next


                    If STR_XML <> "" Then
                        STR_XML = "<STUDENTS>" + STR_XML + "</STUDENTS>"
                    Else
                        STR_XML = ""
                    End If




                    If ViewState("datamode") = "add" Then
                        ALG_CREATEDDT = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        ALG_CREATEDTM = String.Format("{0:T}", DateTime.Now)

                    status = AccessStudentClass.SAVEATT_LOG_VGROUP(STR_XML, ACD_ID, ALG_ATTDT, ddlAttType.SelectedValue, _
                                                      ALVG_AVG_ID, ALVG_ID, EMP_ID, ALG_BSU_ID, ALG_CREATEDDT, _
                                                        ALG_CREATEDTM, ALG_MODIFIEDDT, ALG_MODIFIEDTM, _
                                                         ViewState("datamode"), transaction)


                    ElseIf ViewState("datamode") = "edit" Then
                        ALG_MODIFIEDDT = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                        ALG_MODIFIEDTM = String.Format("{0:T}", DateTime.Now)


                    status = AccessStudentClass.SAVEATT_LOG_VGROUP(STR_XML, ACD_ID, ALG_ATTDT, ddlAttType.SelectedValue, _
                               ALVG_AVG_ID, ViewState("ALVG_ID"), EMP_ID, ALG_BSU_ID, ALG_CREATEDDT, _
                                 ALG_CREATEDTM, ALG_MODIFIEDDT, ALG_MODIFIEDTM, _
                                  ViewState("datamode"), transaction)

                    End If



                    ViewState("datamode") = "view"

                    Session("dt_ATT_GROUP").Rows.Clear()

                    gvInfo.DataSource = Session("dt_ATT_GROUP")
                    gvInfo.DataBind()

                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    btnCancel.Visible = False
                    btnCancel2.Visible = False
                    btnSave2.Visible = False
                    calltransaction = "0"
                    ResetControl()


                Catch ex As Exception
                    calltransaction = "1"
                    errorMessage = "Error Occured While Saving."
                Finally
                    If calltransaction <> "0" Then
                        UtilityObj.Errorlog(errorMessage)
                        transaction.Rollback()
                    Else
                        errorMessage = ""
                        transaction.Commit()
                    End If
                End Try

            End Using
       
    End Function
    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtDate.Text <> "" Then
                Dim strfDate As String = txtDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"

            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
            Return "-1"
        End Try

    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnCancel2.Click
        Try

            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "view"
                Session("dt_ATT_GROUP").Rows.Clear()
                gvInfo.DataSource = Session("dt_ATT_GROUP")
                gvInfo.DataBind()

                ' gvInfo.Visible = False
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                btnSave2.Visible = False
                btnCancel.Visible = False
                btnCancel2.Visible = False
                ResetControl()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub setControl()
        ddlAcdYear.Enabled = False
        ddlGroup.Enabled = False
        ddlAttType.Enabled = False

        If ViewState("ATT_OPEN") = True Then
            txtDate.Enabled = False
            imgCalendar.Enabled = False
            rfvDate.Enabled = False
        Else
            ddlAttDate.Enabled = False
        End If

    End Sub
    Sub ResetControl()
        ddlAcdYear.Enabled = True
        ddlGroup.Enabled = True
        ddlAttType.Enabled = True

        If ViewState("ATT_OPEN") = True Then
            txtDate.Enabled = True
            imgCalendar.Enabled = True
            rfvDate.Enabled = True
        Else
            ddlAttDate.Enabled = True
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Try


        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True

        'smScriptManager.RegisterPostBackControl(gvInfo)
        'Catch ex As Exception

        'End Try
    End Sub
    Sub GETPARAM_D()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim BSU_ID As String = Session("sBsuid")
        Session("Att_param") = Nothing
        Session("Att_param") = AccessStudentClass.GetATTENDANCE_PARAM_D(ACD_ID, BSU_ID, "SESSION1")



    End Sub
    Protected Sub gvInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvInfo.RowDataBound
        Try


            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim BSU_ID As String = Session("sBsuid")


            Dim DS As DataSet '= AccessStudentClass.GetATTENDANCE_PARAM_D(ACD_ID, BSU_ID, "SESSION1")
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ddlSTATUS As DropDownList = DirectCast(e.Row.FindControl("ddlStatus"), DropDownList)
                Dim lblMinList As Label = DirectCast(e.Row.FindControl("lblMinList"), Label)
                If lblMinList.Text.ToUpper <> "REGULAR" Then
                    e.Row.BackColor = Drawing.Color.FromArgb(251, 204, 119)
                End If

                If Not Session("Att_param") Is Nothing Then
                    ddlSTATUS.Items.Clear()
                    DS = Session("Att_param")
                    If DS.Tables(0).Rows.Count > 0 Then
                        ddlSTATUS.DataSource = DS.Tables(0)
                        ddlSTATUS.DataTextField = "APD_PARAM_DESCR"
                        ddlSTATUS.DataValueField = "APD_ID"
                        ddlSTATUS.DataBind()
                        Call backGround()
                    End If
                End If
            ElseIf e.Row.RowType = DataControlRowType.Header Then
                'Dim HeaderGrid As GridView = DirectCast(sender, GridView)
                'Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                'Dim HeaderCell As New TableCell()
                'HeaderCell.Text = ""
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)
                'HeaderCell = New TableCell()
                'HeaderCell.ForeColor = Drawing.Color.Black
                'HeaderCell.Height = 20
                'HeaderCell.Text = "Last Five Day's Attendance History"
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)


                'gvInfo.Controls(0).Controls.AddAt(0, HeaderGridRow)

                e.Row.Cells(10).Text = hfDay1.Value
                e.Row.Cells(11).Text = hfDay2.Value
                e.Row.Cells(12).Text = hfDay3.Value
                'e.Row.Cells(15).Text = hfDay4.Value
                'e.Row.Cells(16).Text = hfDay5.Value
            End If

        Catch ex As Exception

        End Try


    End Sub

    Sub backGround()
        For i As Integer = 0 To gvInfo.Rows.Count - 1
            Dim row As GridViewRow = gvInfo.Rows(i)
            Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)

            ddlStatus.Items(0).Attributes.Add("style", "background-color:#fdf4a8") 'GREEN
            ddlStatus.Items(1).Attributes.Add("style", "background-color:#feb4a8") 'PINK
        Next

    End Sub

    Protected Sub gvInfo_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then
            
            Dim HeaderGrid As GridView = DirectCast(sender, GridView)
            Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderCell As New TableCell()
            HeaderCell.Text = ""
            HeaderCell.ColumnSpan = 5
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
            HeaderCell.ForeColor = Drawing.Color.Black
            HeaderCell.Height = 20
            HeaderCell.Text = "Last Three Day's Attendance History"
            HeaderCell.ColumnSpan = 3
            HeaderGridRow.Cells.Add(HeaderCell)

            gvInfo.Controls(0).Controls.AddAt(0, HeaderGridRow)
        End If

    End Sub
End Class
