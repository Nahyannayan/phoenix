<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studYearEnd.aspx.vb" Inherits="Students_studYearEnd" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function confirm_change() {

            if (confirm("You are about to change the cademic year for the student records.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Year End Promotion
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table align="center" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>


                    <tr align="center">
                        <tr align="center">
                            <td >
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                   <%-- <tr >
                                        <td align="center" >
                                      
                                               
                                    <asp:Label ID="lblCaption" runat="server" Text="Year End Promotion"></asp:Label>
                                           
                                        </td>
                                    </tr>--%>



                                    <tr>
                                        <td align="center" ><asp:Button ID="btnChange" runat="server" CssClass="button" Text="Change Academic Year" OnClick="btnChange_Click1"
                                            OnClientClick="return confirm_change();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

