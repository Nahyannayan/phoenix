﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EnrWeeklyAnalysis.aspx.vb" Inherits="Students_EnrWeeklyAnalysis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>

    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <script type="text/javascript">

        function fnSelectAll(master_box) {
            var curr_elem;
            var checkbox_checked_status;
            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.id.substring(0, 26) == 'ctl00_cphMasterpage_lstBsu') {
                    curr_elem.checked = master_box.checked;
                }
            }
            // master_box.checked=!master_box.checked;
        }

        function FnCheckUnselected() {

            var ControlRef = document.getElementById('<%= lstBsu.ClientID%>');



            var CheckBoxListArray = ControlRef.getElementsByTagName('input');
            var spanArray = ControlRef.getElementsByTagName('span');
            var checkedValues = '';
            var nIndex = 0;
            var sValue = '';

            for (var i = 0; i < CheckBoxListArray.length; i++) {
                var checkBoxRef = CheckBoxListArray[i];

                if (checkBoxRef.checked == false) {
                    document.getElementById('<%= chkSelectBsu.ClientID%>').checked = false;
                }
            }
        }

        //not using 
        function Get_Selected_Value() {
            //document.getElementById('CheckBoxList1_0').value;
            var ControlRef = document.getElementById('<%= lstBsu.ClientID%>');

            var CheckBoxListArray = ControlRef.getElementsByTagName('input');
            var spanArray = ControlRef.getElementsByTagName('span');
            var checkedValues = '';
            var nIndex = 0;
            var sValue = '';

            for (var i = 0; i < CheckBoxListArray.length; i++) {
                var checkBoxRef = CheckBoxListArray[i];

                if (checkBoxRef.checked == true) {
                    var labelArray = checkBoxRef.parentNode.getElementsByTagName('label');


                    if (labelArray.length > 0) {
                        if (checkedValues.length > 0) {
                            checkedValues += ', ';
                            nIndex += ', ';
                            sValue += ', ';
                        }
                        checkedValues += labelArray[0].innerHTML;
                        nIndex += i;
                        //  sValue += spanArray[i].alt;
                    }
                }
                else {

                }
            }

            if (CheckBoxListArray[0].checked == true) {
                checkedValues = "";
                var curr_elem;
                var checkbox_checked_status;
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    curr_elem = document.forms[0].elements[i];

                    if (curr_elem.id.substring(0, 26) == 'ctl00_cphMasterpage_lstBsu') {


                        curr_elem.checked = true;

                    }
                }
            }
            else {
                checkedValues = "";
                var curr_elem;
                var checkbox_checked_status;
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    curr_elem = document.forms[0].elements[i];

                    if (curr_elem.id.substring(0, 26) == 'ctl00_cphMasterpage_lstBsu') {

                        if (curr_elem.checked == true) {
                            curr_elem.checked = true;
                        }
                        else {

                            curr_elem.checked = false;
                        }

                    }
                }
            }

        }

        function GoToPage(BSU, AsonDate) {

            $.fancybox({
                type: 'iframe',
                maxWidth: 300,
                href: 'EnrWeeklyAnalysis_F.aspx?BSU=' + BSU + '&AsonDate=' + AsonDate,
                maxHeight: 400,
                fitToView: false,
                width: '65%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                topRatio: 0,
                'onComplete': function () {
                    $("#fancybox-wrap").css({ 'top': '20px', 'bottom': 'auto' });
                }
            });
        }




        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=lstBsu.ClientID%>");
            if (checkBoxList != null) {
                var checkboxes = checkBoxList.getElementsByTagName("input");
                var isValid = false;
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked) {
                        isValid = true;
                        break;
                    }
                }
            }
            args.IsValid = isValid;
        }


    </script>
    <style type="text/css">
        .wrapControlTitle {
            white-space: pre-wrap;
        }

        #fancybox-wrap {
            position: fixed;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Enrollment Weekly Analysis"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr runat="server" id="trCurriculum" width="20%">
                        <td align="left" ><span class="field-label">Curriculum</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlCurriculum" runat="server" AutoPostBack="True">
                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                <asp:ListItem Text="International" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Asian" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfCurriculum" runat="server" InitialValue="0" ControlToValidate="ddlCurriculum" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td width="50%"></td>
                    </tr>
                    <tr runat="server" id="trbsu">
                        <td align="left" width="20%"><span class="field-label">School</span></td>
                        <td align="left" width="30%">
                            <asp:CheckBox ID="chkSelectBsu" onclick="javascript:fnSelectAll(this);" runat="server"
                                Text="Select All" AutoPostBack="True" /><br />
                            <asp:CheckBoxList ID="lstBsu" onclick="javascript:FnCheckUnselected();" runat="server" Style="vertical-align: middle; overflow: auto; text-align: left; border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid;" BorderStyle="Solid" BorderWidth="1px" Height="149px" Width="50%" RepeatLayout="Flow" CssClass="allowSelectAll">
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server" />
                        </td>
                        <td width="50%"></td>
                    </tr>
                    <tr runat="server" id="trReportType">
                        <td align="left" width="20%"><span class="field-label">Report Type</span></td>
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="true">
                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Consolidated" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Details" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0" ControlToValidate="ddlReportType" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td width="50%"></td>
                    </tr>
                    <tr runat="server" id="trDate">
                        <td align="left" width="20%"><span class="field-label">As on Date</span></td>
                        <td align="left" width="30%">
                             <asp:TextBox ID="txtDate" runat="server" CssClass="inputbox">
                                        </asp:TextBox><asp:ImageButton ID="imgDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton>
                        </td>
                        <td width="50%"></td>
                    </tr>
                    <tr>
                        <td align="center"  colspan="2">
                            <asp:Button ID="btnGenerateGrid" runat="server" CssClass="button" Text="Generate" />
                            &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Export to Excel" /></td>
                    </tr>
                    <tr>

                        <td colspan="8">
                            <table width="100%">
                                <tr class="subheader_img">
                                    <td>
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                            </span></font>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvEnroll" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                           CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%" OnPageIndexChanging="gvEnroll_PageIndexChanging" DataKeyNames="BSU">
                                            <RowStyle CssClass="griditem" Height="25px" Wrap="False" />

                                            <EmptyDataRowStyle Wrap="False" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>

                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="School Name" HeaderStyle-CssClass="wrapControlTitle">

                                                    <ItemTemplate>
                                                        <a id="frameSubActivity" onclick="GoToPage('<%#Eval("BSU")%>','<%# ViewState("AsonDAte")%>');" class="frameActivity" href="#">
                                                            <asp:Label ID="lblBsu" runat="server" Text='<%# Bind("[SCHOOL NAME]")%>' __designer:wfdid="w40"></asp:Label></a>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCName" runat="server" Text='<%# Bind("ASONDATE")%>' __designer:wfdid="w40"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="Class" HeaderText="Class"></asp:BoundField>

                                                <asp:BoundField DataField="Student Capacity/Class" HeaderText="Student Capacity/Class" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>
                                                <asp:BoundField DataField="Student Budget Sep 2016" HeaderText="Student Budget Sep 2016" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>
                                                <asp:BoundField DataField="Current Enrolments" HeaderText="Current Enrolments" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>
                                                <asp:BoundField DataField="OPEN Seats" HeaderText="OPEN Seats" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>
                                                <asp:BoundField DataField="Future Dated Admission" HeaderText="Future Dated Admission" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>
                                                <asp:BoundField DataField="Offers Pending" HeaderText="Offers Pending" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>
                                                <asp:BoundField DataField="Waitlisted" HeaderText="Waitlisted" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>

                                                <asp:BoundField DataField="Assessments" HeaderText="Assessments" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>
                                                <asp:BoundField DataField="Registrations" HeaderText="Registrations" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>
                                                <asp:BoundField DataField="Enquiries" HeaderText="Enquiries" HeaderStyle-CssClass="wrapControlTitle"></asp:BoundField>

                                            </Columns>
                                            <PagerStyle HorizontalAlign="Right" CssClass="GridPager" />
                                            <SelectedRowStyle CssClass="Green" Wrap="False" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" Wrap="False" />
                                            <EditRowStyle Wrap="False" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                        </asp:GridView>
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calDate" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender> 

            </div>
        </div>
    </div>
</asp:Content>

