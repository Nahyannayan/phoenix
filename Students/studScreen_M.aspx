<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studScreen_M.aspx.vb" Inherits="Students_studScreen_M" Debug="true"  title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
  
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Screening Set Up
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">
           <tr>
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table align="center" width="100%" class="BlueTableView" cellpadding="0" cellspacing="0">
                    
                      <tr>
                        <td align="left" width="25%">
                            <span class="field-label">Select Academic Year</span></td>
                        
                        <td align="left" width="25%">
                                                       <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList>               
                      </td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                    </tr>
                      <tr>
                     <td align="center"  colspan="4"  valign="top">
                     <asp:GridView ID="gvStudScreen" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                     CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                      PageSize="20" Width="100%">
                     <Columns>
                     
                      <asp:TemplateField HeaderText="Grade" Visible="False">
                      <ItemTemplate>
                      <asp:Label ID="lblGrdId" runat="server" text='<%# Bind("grm_grd_id") %>'  ></asp:Label>
                      </ItemTemplate>
                           <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                       </asp:TemplateField>
                      
                       <asp:TemplateField HeaderText="Grade">
                      <ItemTemplate>
                      <asp:Label ID="lblGrade" runat="server" text='<%# Bind("grm_display") %>'></asp:Label>
                      </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                      </asp:TemplateField>
                      
                      <asp:TemplateField HeaderText="stmid" Visible="False">
                       <ItemTemplate>
                      <asp:Label ID="lblStmId" runat="server" text='<%# Bind("grm_stm_id") %>'  ></asp:Label>
                      </ItemTemplate>
                           <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                       </asp:TemplateField>
                       
                      <asp:TemplateField HeaderText="Stream">
                      <ItemTemplate>
                      <asp:Label ID="lblStream" runat="server" text='<%# Bind("stm_descr") %>'></asp:Label>
                      </ItemTemplate>
                           <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                      </asp:TemplateField>
                                          
                       
                       <asp:TemplateField HeaderText="Screening Test">
                      <ItemTemplate>
                      <asp:Image ID="imgAvailable" runat="server" ImageUrl='<%# returnpath(Container.DataItem("grm_grd_id"),Container.DataItem("grm_stm_id")) %>' />
                      </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                     </asp:TemplateField>
                     
                       <asp:ButtonField CommandName="View" HeaderText="View" Text="View">
                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"  />
                       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                       </asp:ButtonField>
                       </Columns>  
                         <HeaderStyle   CssClass="gridheader_pop" />
                         <RowStyle CssClass="griditem"   />
                         <SelectedRowStyle CssClass="Green" />
                         <AlternatingRowStyle CssClass="griditem_alternative" />
                      
                    </asp:GridView>
                    
                    </td></tr>
                    </table>
                <asp:HiddenField ID="hfACD_ID" runat="server" />
                <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                        type="hidden" value="=" /><asp:HiddenField ID="hfCLM_ID" runat="server" />
    </td></tr>
</table>

                </div>
            </div>
        </div>
</asp:Content>

