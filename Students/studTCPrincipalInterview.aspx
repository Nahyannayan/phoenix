﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTCPrincipalInterview.aspx.vb" Inherits="Students_studTCPrincipalInterview" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>TC Principal Interview
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 100%;" valign="top">
                            <table id="tblClm" runat="server" align="center"
                                cellpadding="0" cellspacing="0" width="100%">

                                <tr id="trBsu" runat="server">
                                    <td align="left" id="td1" runat="server" width="10%"><span class="field-label">Business Unit</span>
                                    </td>

                                    <td align="left" id="td3" runat="server" width="15%">
                                        <asp:DropDownList ID="ddlBsu" runat="server" AutoPostBack="True" Width="408px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" id="td4" runat="server" width="10%"><span class="field-label">Curriculum</span>
                                    </td>

                                    <td align="left" id="td6" runat="server" width="15%">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True" Width="100px">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="10%"></td>
                                    <td align="left" width="15%"></td>
                                    <td align="left" width="10%"></td>
                                    <td align="left" width="15%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="10%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="15%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="10%"><span class="field-label">Interview Status</span>
                                    </td>

                                    <td align="left" width="15%">
                                        <asp:DropDownList ID="ddlInterviewStatus" runat="server" AutoPostBack="true">
                                            <asp:ListItem>COMPLETED</asp:ListItem>
                                            <asp:ListItem Selected="True">NOT COMPLETED</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="10%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" width="15%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="10%"><span class="field-label">Section</span>
                                    </td>

                                    <td align="left" width="15%">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trInterview" runat="server">
                                    <td align="left"><span class="field-label">Interview Date From</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtInterviewStart" runat="server" > </asp:TextBox>
                                        <asp:ImageButton ID="imgApplyStart" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false;" />
                                    </td>
                                    <td align="left"><span class="field-label">Interview Date To</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtInterviewEnd" runat="server" > </asp:TextBox>
                                        <asp:ImageButton ID="imgApplyEnd" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false;" />
                                    </td>
                                    <td><span class="field-label">Outcome</span>
                                    </td>

                                    <td colspan="3" align="left">
                                        <asp:DropDownList ID="ddlOutcome" runat="server"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student ID</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStudentID" runat="server" > </asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Student Name</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server" > </asp:TextBox>
                                    </td>
                                    <td align="left" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8" align="left">
                                        <table style="border-collapse: collapse; background: #EBFAFF; width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:DataList ID="dlCount" runat="server" RepeatColumns="10">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblOutcome" runat="server" Text='<%# Bind("TPO_DESCR") %>'></asp:Label>
                                                                    </td>
                                                                    <td>:
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblOutcomeCount" runat="server" Text='<%# Bind("TPO_COUNT") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trGrid" runat="server">
                                    <td align="center" colspan="8" valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" Style="cursor: pointer" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="History">
                                                    <ItemTemplate>
                                                        <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="false" BorderStyle="None" GridLines="None">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTPS" runat="server" Text='<%# Bind("TPS_ID")%>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="HideID">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkHistory" runat="server" OnClick="lnkHistory_Click" Text='<%# Bind("HISTORY_DATE") %>'></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Principal">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkPrincipal" OnClick="lnkPrincipal_Click" runat="server" Text='Add Feedback'></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" />
                                                </asp:TemplateField>


                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>

                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlParentExit" runat="server" Style="background: #fff;" HorizontalAlign="center" CssClass="panel-cover">
                    <asp:Label ID="lblExitOnlineMsg" runat="server" SkinID="Error" />
                    <table>
                        <tr>
                            <td>
                                <div id="printableArea">
                                    <asp:Label ID="lblErrorText" SkinID="Error" runat="server"></asp:Label>
                                    <table style="width: 100%; border-collapse: collapse;">
                                        <tr class="title-bg">
                                            <td colspan="4">ParentInterview
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="3"><span class="field-label">Name of Student :&nbsp;&nbsp;</span>
                                                <asp:Label ID="lblExitStudent" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;
                                    <span class="field-label">Grade & Section :&nbsp;&nbsp;</span>
                                                <asp:Label ID="lblExitClass" runat="server" Text="Label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Date and Time of Interview</span> <font color="red">*</font></td>

                                            <td align="left">
                                                <asp:TextBox ID="txtInterviewDate" runat="server"> </asp:TextBox>
                                                <asp:ImageButton ID="ImgInterviewDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                    OnClientClick="return false;" />

                                                <asp:DropDownList ID="ddlInterviewTime" runat="server">
                                                    <asp:ListItem>7:30</asp:ListItem>
                                                    <asp:ListItem>7:45</asp:ListItem>
                                                    <asp:ListItem>8:00</asp:ListItem>
                                                    <asp:ListItem>8:15</asp:ListItem>
                                                    <asp:ListItem>8:30</asp:ListItem>
                                                    <asp:ListItem>8:45</asp:ListItem>
                                                    <asp:ListItem>9:00</asp:ListItem>
                                                    <asp:ListItem>9:15</asp:ListItem>
                                                    <asp:ListItem>9:30</asp:ListItem>
                                                    <asp:ListItem>9:45</asp:ListItem>
                                                    <asp:ListItem>10:00</asp:ListItem>
                                                    <asp:ListItem>10:15</asp:ListItem>
                                                    <asp:ListItem>10:30</asp:ListItem>
                                                    <asp:ListItem>10:45</asp:ListItem>
                                                    <asp:ListItem>11:00</asp:ListItem>
                                                    <asp:ListItem>11:15</asp:ListItem>
                                                    <asp:ListItem>11:30</asp:ListItem>
                                                    <asp:ListItem>11:45</asp:ListItem>
                                                    <asp:ListItem>12:00</asp:ListItem>
                                                    <asp:ListItem>12:15</asp:ListItem>
                                                    <asp:ListItem>12:30</asp:ListItem>
                                                    <asp:ListItem>12:45</asp:ListItem>
                                                    <asp:ListItem>13:00</asp:ListItem>
                                                    <asp:ListItem>13:15</asp:ListItem>
                                                    <asp:ListItem>13:30</asp:ListItem>
                                                    <asp:ListItem>13:45</asp:ListItem>
                                                    <asp:ListItem>14:00</asp:ListItem>
                                                    <asp:ListItem>14:15</asp:ListItem>
                                                    <asp:ListItem>14:30</asp:ListItem>
                                                    <asp:ListItem>14:45</asp:ListItem>
                                                    <asp:ListItem>15:00</asp:ListItem>
                                                    <asp:ListItem>15:15</asp:ListItem>
                                                    <asp:ListItem>15:30</asp:ListItem>
                                                    <asp:ListItem>15:45</asp:ListItem>
                                                    <asp:ListItem>16:00</asp:ListItem>
                                                    <asp:ListItem>16:15</asp:ListItem>
                                                    <asp:ListItem>16:30</asp:ListItem>
                                                    <asp:ListItem>16:45</asp:ListItem>
                                                    <asp:ListItem>17:00</asp:ListItem>
                                                    <asp:ListItem>17:15</asp:ListItem>
                                                    <asp:ListItem>17:30</asp:ListItem>
                                                    <asp:ListItem>17:45</asp:ListItem>
                                                    <asp:ListItem>18:00</asp:ListItem>
                                                    <asp:ListItem>18:15</asp:ListItem>
                                                    <asp:ListItem>18:30</asp:ListItem>
                                                    <asp:ListItem>18:45</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Reason for Withdrawal</span><font color="red">*</font>
                                            </td>

                                            <td align="left">
                                                <asp:DropDownList ID="ddlReason" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Transferring to</span> <font color="red">*</font>
                                            </td>

                                            <td align="left">
                                                <asp:DropDownList ID="ddlTrans_School" runat="server" Width="403px">
                                                </asp:DropDownList>
                                                <br />
                                                <span class="field-label">(If you choose other,please specify the school)</span><br />
                                                <asp:TextBox ID="txtOthers" runat="server" Width="394px"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left"><span class="field-label">Interview Outcome</span><font color="red">*</font></td>

                                            <td align="left">
                                                <asp:DropDownList ID="ddlInterviewOutcome" AutoPostBack="true" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"><span class="field-label">Feedback</span><font color="red">*</font></td>

                                            <td align="left">
                                                <asp:TextBox ID="txtExitFeedback" TextMode="MultiLine" SkinID="MultiText" Width="600px"
                                                    runat="server"></asp:TextBox>
                                                <ajaxToolkit:TextBoxWatermarkExtender ID="wFeedback" runat="server" TargetControlID="txtExitFeedback"
                                                    WatermarkText="Please enter you feedback comments here" WatermarkCssClass="watermarked">
                                                </ajaxToolkit:TextBoxWatermarkExtender>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left"><span class="field-label">Ok to Proceed for TC</span></td>

                                            <td align="left">
                                                <asp:CheckBox ID="chkProceed" runat="server"></asp:CheckBox></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnExitSave" runat="server" Text="Submit" CssClass="button" Width="60px" />
                                <asp:Button ID="btnExitClose" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Close" Width="60px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfACD_ID" runat="server" />
    <asp:HiddenField ID="hfSTU_ID" runat="server" />
    <asp:HiddenField ID="hfTPS_ID" runat="server" />
    <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgApplyStart" TargetControlID="txtInterviewStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtInterviewStart">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgApplyEnd" TargetControlID="txtInterviewEnd">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtInterviewEnd">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calInterviewDate" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtInterviewDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="calInterviewDate2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtInterviewDate" PopupButtonID="ImgInterviewDate">
    </ajaxToolkit:CalendarExtender>

    <ajaxToolkit:ModalPopupExtender ID="mdlSchool" runat="server" TargetControlID="hfACD_ID"
        PopupControlID="pnlParentExit" BackgroundCssClass="modalBackground" DropShadow="false"
        RepositionMode="RepositionOnWindowResizeAndScroll">
    </ajaxToolkit:ModalPopupExtender>


    &nbsp;
     <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
     </CR:CrystalReportSource>
</asp:Content>

