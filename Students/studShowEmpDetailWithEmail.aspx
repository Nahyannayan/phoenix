<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studShowEmpDetailWithEmail.aspx.vb" Inherits="ShowEmpDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::"%>
<%@ OutputCache  Duration="1" VaryByParam="none"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title></title>
    <base target="_self" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" /> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css" />


    <script language="javascript" type="text/javascript">
       // function settitle(){
        //alert('fgfg');
        //document.title ='sfsdsd';
         // alert('fgfg');
          //      }
        function listen_window() {
        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
              
             function test(val)
                {
                //alert(val);
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid()%>").src = path;
                document.getElementById("<%=h_selected_menu_1.ClientID %>").value=val+'__'+path;
                }
          function test1(val)
                {
                var path;
                if (val=='LI')
                {
                path='../Images/operations/like.gif';
                }else if (val=='NLI')
                {
                path='../Images/operations/notlike.gif';
                }else if (val=='SW')
                {
                path='../Images/operations/startswith.gif';
                }else if (val=='NSW')
                {
                path='../Images/operations/notstartwith.gif';
                }else if (val=='EW')
                {
                path='../Images/operations/endswith.gif';
                }else if (val=='NEW')
                {
                path='../Images/operations/notendswith.gif';
                }
                document.getElementById("<%=getid1()%>").src = path;
                document.getElementById("<%=h_selected_menu_2.ClientID %>").value=val+'__'+path;
                }
    </script>
    
</head>
<body onload="listen_window();"> 
    <form id="form1" runat="server">
    <!--1st drop down menu -->                                                   
                                           
    <div>
        <div>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td  colspan="2" valign="top" align="center" style="width: 100%">
                                   <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; ">
                                        <tr>
                                            <td>
                                                <table align="center" border="0"  cellpadding="0" cellspacing="0"
                                                    width="100%">
                                                    <tr>
                                                        <td  colspan="4" style="height: 140px" valign="top" align="center">
                                                            <asp:GridView ID="gvEmpInfo" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" DataKeyNames="EMP_ID" CssClass="table table-bordered table-row">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ID" Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("EMP_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="ID" Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMD_EMAIL") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Employee ID" SortExpression="EMP_ID">
                                                                        <EditItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("EMP_ID") %>'></asp:Label>
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" Text='<%# Bind("EMPNO") %>'></asp:LinkButton>&nbsp;
                                                                        </ItemTemplate>
                                                                        <HeaderTemplate>
                                                                            
                                                                                        <asp:Label ID="lblID" runat="server" EnableViewState="False" Text="Employee Id" CssClass="gridheader_text"></asp:Label><br />
                                                                                                    <asp:TextBox ID="txtcode" runat="server" Width="92px"></asp:TextBox>
                                                                                                    <asp:ImageButton ID="btnSearchEmpId" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                                        OnClick="btnSearchEmpId_Click" />&nbsp;</td>
                                                                                           
                                                                        </HeaderTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Employee Full Name" ShowHeader="False">
                                                                        <HeaderTemplate>
                                                                            
                                                                                        <asp:Label ID="lblName" runat="server" Text="Employee Name" Width="214px" CssClass="gridheader_text"></asp:Label><br />
                                                                                                    <asp:TextBox ID="txtName" runat="server" Width="150px"></asp:TextBox>
                                                                                                    <asp:ImageButton ID="btnSearchEmpName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                                        OnClick="btnSearchEmpName_Click" />&nbsp;
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Selected"
                                                                                Text='<%# Eval("EMP_NAME") %>' OnClick="LinkButton1_Click"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="gridheader_pop" />
                                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <RowStyle Height="25px" />
                                                            </asp:GridView>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="height: 12px; width: 511px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"  colspan="4" style="width: 567px; height: 28px"
                                    valign="middle">
                        <input type="hidden" id="h_SelectedId" runat="server" value="0" /><input id="h_Selected_menu_2" runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server" type="hidden" value="=" /></td>
                            </tr>
                        </table>
        </div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 95%">
        </table>
    </div>
  <script type="text/javascript">

cssdropdown.startchrome("Div1")
cssdropdown.startchrome("Div2")

</script>
                

</form>
    
</body>
</html>