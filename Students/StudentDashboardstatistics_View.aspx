﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudentDashboardstatistics_View.aspx.vb" Inherits="Students_StudentDashboardstatistics_View" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../cssfiles/title.css" rel="stylesheet" />
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <div>
            <table width="100%" id="tblFirstTable" runat="server">
                <tr id="trGridv" >
                    <td align="left" valign="top" colspan="3">
                        <asp:GridView ID="gvBsuWiseGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CssClass="table table-bordered table-row" EmptyDataText="Record not available !!!" 
                            PageSize="50" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBsuId" runat="server" Text='<%# Bind("bsu_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="School">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkCustom" runat="server" Text='<%# bind("BSU_NAME") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enquiry">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnq" runat="server" Text='<%# bind("ENQS") %>'></asp:Label>
                                        (<asp:Label ID="lblEnqLast" runat="server" Text='<%# Bind("ENQS")%>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Open Enquiry">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOpenEnq" runat="server" Text='<%# Bind("REGS")%>'></asp:Label>
                                        (<asp:Label ID="lblOpenEnqLast" runat="server" Text='<%# Bind("REGS")%>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Registered">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReg" runat="server" Text='<%# Bind("STUDS")%>'></asp:Label>
                                        (<asp:Label ID="lblRegLast" runat="server" Text='<%# Bind("STUDS")%>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Offered">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnrolles" runat="server" Text='<%# Bind("PER_ENQ_WIDTH")%>'></asp:Label>
                                        (<asp:Label ID="lblEnrollesLast" runat="server" Text='<%# Bind("PER_ENQ_WIDTH")%>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Enrolled">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTC" runat="server" Text='<%# Bind("PER_REG_WIDTH")%>'></asp:Label>
                                         (<asp:Label ID="lblTCLast" runat="server" Text='<%# Bind("PER_REG_WIDTH")%>'></asp:Label>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle BackColor="Aqua" />
                            <HeaderStyle CssClass="gridheader_pop" Height="55px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                        <asp:HiddenField ID="hdnID" runat="server" />
                    </td>
                    
                </tr>
                <tr>
                    <td align="left" style="width: 80%">
                        &nbsp;</td>
                    <td>
                        &nbsp;
                    </td>
                    <td align="left" style="width: 50%">
                        &nbsp;</td>
                </tr>
            </table>
            <br />
            <table width="100%" id="tblSecondTable" visible="false" runat="server">
                <tr id="tr1">
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td>
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnExportExcel" runat="server" Text="Export Excel" CssClass="button" Visible="false" />
                        &nbsp;<asp:Button ID="btnBack" runat="server" Text="BACK" CssClass="button" Visible="false" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
