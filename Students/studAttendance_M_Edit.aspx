<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAttendance_M_Edit.aspx.vb" Inherits="Students_studAttendance_M_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFrom.ClientID %>').value, "", sFeatures)
            else if (mode == 2)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtTo.ClientID %>').value, "", sFeatures)

        if (result == '' || result == undefined) {
            //            document.getElementById("txtDate").value=''; 
            return false;
        }
        if (mode == 1)
            document.getElementById('<%=txtFrom.ClientID %>').value = result;
           else if (mode == 2)
               document.getElementById('<%=txtTo.ClientID %>').value = result;
         return true;
     }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="Literal1" runat="server" Text="Set Attendance Type"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                         ></asp:Label><span style="  color: #800000">&nbsp;</span>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"   ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                    <span style="  color: #800000">&nbsp;</span>
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr style="  color: red" valign="bottom">
                        <td align="center" class="matters" valign="middle">Fields Marked with(<span style="  color: red">*</span>) are mandatory</td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top">
                            <table  width="100%">   
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year <span style="  color: red">*</span></span></td>
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left"  width="30%">
                                       </td>
                                </tr>
                                <tr>
                                    <td><span class="field-label">Grade</span></td>
                                    <td> <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" ><span class="field-label">Attendance Type</span></td>
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlAttType" runat="server">
                                            <asp:ListItem>Session1 &amp; Session2</asp:ListItem>
                                            <asp:ListItem>Session1</asp:ListItem>
                                            <asp:ListItem>Session2</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">From <font   color="red">*</font></span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtFrom" runat="server"  >
                                        </asp:TextBox>&nbsp;<asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                           ></asp:ImageButton><asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                                CssClass="text-danger" Display="Dynamic" ErrorMessage="From Date required" ForeColor=""
                                                ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left"  ><span class="field-label">To <span style="  color: red"></span></span></td>
                                    <td align="left" >
                                        <asp:TextBox ID="txtTo" runat="server" >
                                        </asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                            ></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTo"
                                            CssClass="text-danger" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                            ValidationGroup="groupM1"></asp:RequiredFieldValidator></td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"   valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnAdd_Click" Text="Add" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False"
                                    CssClass="button" OnClick="btnEdit_Click" Text="Edit" /><asp:Button ID="btnSave"
                                        runat="server" CssClass="button" OnClick="btnSave_Click" Text="Save" ValidationGroup="groupM1" /><asp:Button
                                            ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" OnClick="btnCancel_Click"
                                            Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
          <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar"
        TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
      <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
        TargetControlID="txtTo">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

