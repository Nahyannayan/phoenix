﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_encTemplate_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try


                'Dim MainMnu_code As String = String.Empty
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S200559") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'GetActive_ACD_4_Grade


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Bind_school()
                    gridbind()

                End If
            Catch ex As Exception

                ' lblm.Text = "Request could not be processed "
            End Try

        End If

    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub Bind_school()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            Dim PARAM(2) As SqlParameter


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENC.GET_SCHOOLS", PARAM)
            ddlBSU.DataSource = dsDetails
            ddlBSU.DataTextField = "BSU_NAME"
            ddlBSU.DataValueField = "BSU_ID"
            ddlBSU.DataBind()

            ddlBsu.Items.Add(New ListItem("--All--", "0"))
            ddlBSU.Items.FindByText("--All--").Selected = True


        Catch ex As Exception

        End Try
    End Sub
    Private Sub gridbind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As New DataSet
        Dim str_query As String
        If ddlBsu.SelectedValue = "0" Then
            str_query = "SELECT ETS_ID,BSU_SHORTNAME,ETS_NAME,ETS_BSU_ID FROM [ENC].[EMAILTEMPLATE_S] INNER JOIN BUSINESSUNIT_M ON ETS_BSU_ID=BSU_ID " _
                                            & " UNION " _
                               & " SELECT ETS_ID,'ALL',ETS_NAME,ETS_BSU_ID FROM [ENC].[EMAILTEMPLATE_S]  WHERE ETS_BSU_ID=0"
        Else
            str_query = "SELECT ETS_ID,BSU_SHORTNAME,ETS_NAME,ETS_BSU_ID FROM [ENC].[EMAILTEMPLATE_S] INNER JOIN BUSINESSUNIT_M ON ETS_BSU_ID=BSU_ID " _
                                    & " WHERE ETS_BSU_ID = '" + ddlBsu.SelectedValue + "' " _
                                    & " UNION " _
                                    & " SELECT ETS_ID,'ALL',ETS_NAME,ETS_BSU_ID FROM [ENC].[EMAILTEMPLATE_S]  WHERE ETS_BSU_ID=0"

        End If



        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            gvEmail.DataSource = ds.Tables(0)
            gvEmail.DataBind()

        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvEmail.DataSource = ds.Tables(0)
            Try
                gvEmail.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvEmail.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvEmail.Rows(0).Cells.Clear()
            gvEmail.Rows(0).Cells.Add(New TableCell)
            gvEmail.Rows(0).Cells(0).ColumnSpan = columnCount
            gvEmail.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvEmail.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        End If
    End Sub
    Protected Sub lnkadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkadd.Click
   

        ViewState("bsuid") = ddlBsu.SelectedValue
     
        Response.Redirect("~/Students/encTemplate.aspx?bsuid=" + ViewState("bsuid").ToString + "&id=0", False)
    End Sub
    Protected Sub gvEmail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmail.PageIndexChanging
        Try
            gvEmail.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception

            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub gvEmail_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvEmail.RowCommand
        If e.CommandName = "view" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvEmail.Rows(index), GridViewRow)

            Dim lblEMTID As Label
            Dim lblBSU As Label

            lblEMTID = selectedRow.FindControl("lblEMTID")
            lblBSU = selectedRow.FindControl("lblBSU")
            ViewState("bsuid") = lblBSU.Text
            
            ViewState("id") = lblEMTID.Text
            Response.Redirect("~/Students/encTemplate.aspx?bsuid=" + ViewState("bsuid").ToString + "&id=" + ViewState("id").ToString, False)

        End If
    End Sub
    Protected Sub gvEmail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEmail.RowDeleting
        Try
            gvEmail.SelectedIndex = e.RowIndex


            Dim row As GridViewRow = gvEmail.Rows(e.RowIndex)
            Dim lblID As New Label
            lblID = TryCast(row.FindControl("lblEMTID"), Label)

            If lblID.Text <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_query As String = ""

                str_query = "DELETE FROM [ENC].[EMAILTEMPLATE_S] WHERE ETS_ID=" & lblID.Text
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            End If
            gridbind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlBsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsu.SelectedIndexChanged

        gridbind()
    End Sub
End Class
