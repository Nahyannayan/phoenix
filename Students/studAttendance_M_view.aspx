<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAttendance_M_view.aspx.vb" Inherits="Students_studAttendance_M_view" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Set Attendance Type"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table id="tbl_test" runat="server" width="100%">
                                <tr>
                                    <td align="left" class="matters" colspan="9" valign="top">
                                        <asp:GridView ID="gvATTRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%"  >
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ACAD. YEAR">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("EMP_Name") %>' __designer:wfdid="w62"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Academic Year<br />
                                                        <asp:DropDownList ID="ddlY_DESCRH" runat="server" CssClass="listbox" AutoPostBack="True" OnSelectedIndexChanged="ddlACY_DESCRH_SelectedIndexChanged" __designer:wfdid="w63">
                                                        </asp:DropDownList></td>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:Label ID="lblY_DESCR" runat="server" Text='<%# Bind("Y_DESCR") %>' __designer:wfdid="w24"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("GRD_DESCR") %>' __designer:wfdid="w26"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblGrdH" runat="server" Text="Grade" CssClass="gridheader_text" __designer:wfdid="w27"></asp:Label><br />
                                                        <asp:TextBox ID="txtGRD_DESCR" runat="server" __designer:wfdid="w28"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchGRD" OnClick="btnSearchGRD_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w29"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRD_DESCR" runat="server" Text='<%# Bind("GRD_DESCR") %>' __designer:wfdid="w25"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("FromDT", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblGradeH" runat="server" Text="From Date" CssClass="gridheader_text"></asp:Label><br />
                                                        <asp:TextBox ID="txtFromDT" runat="server" __designer:wfdid="w11"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchFromDT" OnClick="btnSearchFromDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                                        <asp:Label ID="lblFromDTH" runat="server" Text='<%# Bind("FromDT", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w8"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox3" runat="server" __designer:wfdid="w71"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblToDateH" runat="server" Text="To Date" CssClass="gridheader_text" __designer:wfdid="w72"></asp:Label><br />
                                                        <asp:TextBox ID="txtToDT" runat="server" __designer:wfdid="w73"></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchToDT" OnClick="btnSearchToDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w74"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToDTH" runat="server" Text='<%# Bind("ToDT", "{0:dd/MMM/yyyy}") %>' __designer:wfdid="w70"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ATTENDANCE">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("SCT_DESCR") %>' __designer:wfdid="w86"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblSTUD_IDH" runat="server" Text="Att. Type" CssClass="gridheader_text" __designer:wfdid="w77"></asp:Label></td>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbATT_TYPE" runat="server" Text='<%# Bind("ATT_TYPE") %>' __designer:wfdid="w85"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" __designer:wfdid="w80"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View" __designer:wfdid="w81"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server" __designer:wfdid="w79">View</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ATT ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("SAL_ID") %>' __designer:wfdid="w83"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblATT_ID" runat="server" Text='<%# Bind("ATT_ID") %>' __designer:wfdid="w82"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">

                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_selected_menu_2" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

