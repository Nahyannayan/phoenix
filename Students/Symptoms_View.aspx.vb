﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Partial Class Students_Symptoms_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Session("SelectedRow") = -1
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"


                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "S900025" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    GridBind()
                    '    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If
        set_Menu_Img()
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))


    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvStudLang.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvGroup.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvStudLang.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Private Sub GridBind()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString
        Dim str_query As String = "SELECT SYMPTOMS_ID,SYMPTOMS_DESCR FROM SYMPTOMS_M WHERE SYMPTOMS_bDELETE = 0 AND SYMPTOMS_ID<>''  "

        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String

        If gvStudLang.Rows.Count > 0 Then

            Dim txtSearch As New TextBox


            txtSearch = gvStudLang.HeaderRow.FindControl("txtSYMPTOMS_DESCR")
            strSidsearch = h_selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("SYMPTOMS_DESCR", txtSearch.Text, strSearch)



            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If

        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query & " order by SYMPTOMS_DESCR")
        gvStudLang.DataSource = ds
        gvStudLang.DataBind()

    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND  NOT " + field + "T LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Protected Sub gvStudLang_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudLang.RowCommand
        Try
            If e.CommandName = "View" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim selectedRow As GridViewRow = DirectCast(gvStudLang.Rows(index), GridViewRow)
                Dim lblSYMPTOMS_ID As New Label
                Dim lblSYMPTOMS_DESCR As New Label
                Dim url As String
                'define the datamode to view if view is clicked
                ViewState("datamode") = "view"
                'Encrypt the data that needs to be send through Query String
                lblSYMPTOMS_ID = selectedRow.Cells(0).FindControl("lblSYMPTOMS_ID")
                lblSYMPTOMS_DESCR = selectedRow.Cells(1).FindControl("lblSYMPTOMS_DESCR")

                ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))


                url = String.Format("~\Students\Symptoms_View_M.aspx?MainMnu_code={0}&datamode={1}&SYMPTOMSID=" + Encr_decrData.Encrypt(lblSYMPTOMS_ID.Text) + "&SYMPTOMSDESCR=" + Encr_decrData.Encrypt(lblSYMPTOMS_DESCR.Text), ViewState("MainMnu_code"), ViewState("datamode"))
                Response.Redirect(url)
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Dim url As String
        ViewState("datamode") = "add"
        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
        url = String.Format("~\Students\Symptoms_View_M.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
        Response.Redirect(url)

    End Sub

    Protected Sub btnSearchDescr_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub gvStudLang_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudLang.PageIndexChanging

        gvStudLang.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

End Class
