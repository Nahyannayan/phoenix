<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudATT_PendingMail_edit.aspx.vb" Inherits="Students_StudATT_PendingMail_edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">

        function ShowHDetails(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 370px; ";
            sFeatures += "dialogHeight: 300px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var result;


            result = window.showModalDialog(url, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }

            return false;
        }
        function onCancel() {
            // var postBack = new Sys.WebForms.PostBackAction(); 
            //postBack.set_target('CancelButton'); 
            //postBack.set_eventArgument(''); 
            // postBack.performAction();
        }

        function onOk() {
            var postBack = new Sys.WebForms.PostBackAction();
            postBack.set_target('okButton');
            postBack.set_eventArgument('');
            postBack.performAction();
        }



        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }


        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFrom.ClientID %>').value, "", sFeatures)
            else if (mode == 2)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtTo.ClientID %>').value, "", sFeatures)

        if (result == '' || result == undefined) {
            //            document.getElementById("txtDate").value=''; 
            return false;
        }
        if (mode == 1)
            document.getElementById('<%=txtFrom.ClientID %>').value = result;
        else if (mode == 2)
            document.getElementById('<%=txtTo.ClientID %>').value = result;
       return true;
   }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Mail Pending Attendance"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <span style="float: left; display: block;">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" Font-Size="10px" />
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                        ValidationGroup="AttGroup" DisplayMode="List" Font-Size="10px" ForeColor="" />
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top">
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">School</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlBSU_ID" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">From Date <font color="red">*</font></span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal1" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup"   ForeColor="">*</asp:RequiredFieldValidator></td>
                                    <td align="left" class="matters"><span class="field-label">To Date <font color="red">*</font></span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal2" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvTo" runat="server" ControlToValidate="txtTo" Display="Dynamic"
                                            ErrorMessage="To Date required" ValidationGroup="AttGroup"  ForeColor="">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Schedule <font color="red">*</font></span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlSCHEDULE" runat="server">
                                            <asp:ListItem Value="8">8 AM</asp:ListItem>
                                            <asp:ListItem Value="9">9 AM</asp:ListItem>
                                            <asp:ListItem Value="10" Selected="True">10 AM</asp:ListItem>
                                            <asp:ListItem Value="11">11 AM</asp:ListItem>
                                            <asp:ListItem Value="12">12 PM</asp:ListItem>
                                            <asp:ListItem Value="16">4 PM</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" class="matters"><span class="field-label">Notify Pending Attendance To</span></td>
                                    <td align="left" class="matters">
                                        <asp:DropDownList ID="ddlEMP" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">

                                        <asp:Button ID="btnAddGrd" runat="server" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnUpdateGrd" runat="server" CssClass="button" Text="Update" />
                                        <asp:Button ID="btnCancelGRD" runat="server" CssClass="button"
                                            Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <div>
                                <asp:Label ID="lblError_grid" runat="server"  
                                    EnableViewState="False"></asp:Label>
                                <asp:GridView ID="gvEMP_NAME" runat="server" AutoGenerateColumns="False"   CssClass="table table-bordered table-row"
                                    DataKeyNames="ID"
                                    EmptyDataText="No items added yet"  
                                    
                                    Width="100%">
                                    <RowStyle CssClass="griditem" Height="23px" />
                                    <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>' __designer:wfdid="w47"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNMPA_EMP_NAME" runat="server" Text='<%# Bind("NMPA_EMP_NAME") %>'
                                                    __designer:wfdid="w38"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="EditBtn" runat="server" CausesValidation="False" __designer:wfdid="w41" CommandArgument='<%# Eval("id") %>' CommandName="Edit">           
    Edit </asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="DeleteBtn" runat="server" __designer:wfdid="w42" CommandArgument='<%# Eval("id") %>' CommandName="Delete">
         Delete</asp:LinkButton>
                                            </ItemTemplate>

                                            
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NMPA_ID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNMPA_ID" runat="server" Text='<%# bind("NMPA_ID") %>'
                                                    __designer:wfdid="w34"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="STATUS" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("STATUS") %>' __designer:wfdid="w46"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSTATUS" runat="server" Text='<%# Bind("STATUS") %>' __designer:wfdid="w45"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NMPA_MPA_ID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNMPA_MPA_ID" runat="server" Text='<%# bind("NMPA_MPA_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NMPA_EMP_ID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNMPA_EMP_ID" runat="server" Text='<%# Bind("NMPA_EMP_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle BackColor="Khaki" />
                                    <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" OnClick="btnAdd_Click" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Edit" OnClick="btnEdit_Click" /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                        ValidationGroup="AttGroup" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                            CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />&nbsp;
                <asp:HiddenField ID="hfNMPA_ID" runat="server" />
                        </td>
                    </tr>
                </table>

                <ajaxToolkit:CalendarExtender ID="cal1" runat="server" TargetControlID="txtfrom" Format="dd/MMM/yyyy" PopupButtonID="imgcal1">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="Cal2" runat="server" TargetControlID="txtTo" Format="dd/MMM/yyyy" PopupButtonID="imgcal2">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

