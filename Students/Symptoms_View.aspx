﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="Symptoms_View.aspx.vb" Inherits="Students_Symptoms_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Injury / Symptoms
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>

                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton></td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gvStudLang" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="gvStudLang_PageIndexChanging"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" HorizontalAlign="Left">
                                                        <RowStyle CssClass="griditem" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SYMPTOMS_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSYMPTOMS_ID" runat="server" Text='<%# Bind("SYMPTOMS_ID") %>'></asp:Label>
                                                                </ItemTemplate>

                                                                <ItemStyle></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SYMPTOMS_DESCR">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblDescrH" runat="server">Description</asp:Label><br />
                                                                    <asp:TextBox ID="txtSYMPTOMS_DESCR" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSearchDescr" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle" OnClick="btnSearchDescr_Click"></asp:ImageButton>

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSYMPTOMS_DESCR" runat="server" Text='<%# Bind("SYMPTOMS_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:ButtonField>
                                                             
                                                        </Columns>
                                                        <SelectedRowStyle />
                                                        <HeaderStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_SelectedId" runat="server" type="hidden" value="0" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_selected_menu_1" runat="server"
                                    type="hidden" value="=" /></td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4" style="width: 567px; height: 28px"
                            valign="middle"></td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</asp:Content>

