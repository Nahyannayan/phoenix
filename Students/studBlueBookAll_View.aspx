<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studBlueBookAll_View.aspx.vb" Inherits="Students_studBlueBook_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script language="javascript" type="text/javascript">
 function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 250px; ";
            sFeatures+="dialogHeight: 270px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/calendar.aspx","", sFeatures);
            if(result != '' && result != undefined)
            {
                switch(txtControl)
                {
                  case 0:
                    document.getElementById('<%=txtAsOnDate.ClientID %>').value=result;
                    break;
                   case 1:
                    document.getElementById('<%=txtAcdDate.ClientID %>').value=result;
                    break;
                   }
            }
            return false;    
           }
         
    </script>

    

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> Blue Book
        </div>
        <div class="card-body">
            <div class="table-responsive">

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" >
        <tr>
            <td align="left">
               </td>
        </tr>
        <tr >
            <td align="left" >
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
        <tr>
            <td >
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                 <%--   <tr >
                        <td align="left" colspan="15" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Blue Book </span></font>
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="20%" >
                            <span class="field-label"> Grade</span></td>
                     
                        <td align="left" width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server"  AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" width="20%" >
                            <span class="field-label"> Section</span></td>
                       
                        <td align="left" width="30%" >
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    
                    
                      <tr id="tr_ACD" runat="server">
                        <td align="left"  >
                            <span class="field-label"> As On</span></td>
                       
                        <td align="left"  >
                            <asp:TextBox ID="txtAcdDate" runat="server" ></asp:TextBox>
                      <%--      <asp:ImageButton ID="imgShowDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return getDate(550, 310, 1)" />--%>
                              <asp:ImageButton ID="imgBtnAc_date" runat="server"
                                                            ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                           
                            <br />
                            <span style="font-size: 9pt">(dd/mmm/yyyy)</span></td>

                           <td align="left" colspan="2" >
                            </td>
                    </tr>
                    
                    
                    
                    <tr>
                        <td align="left" >
                           <span class="field-label">  Print Date</span></td>
                       
                        <td align="left"  >
                            <asp:TextBox ID="txtAsOnDate" runat="server" ></asp:TextBox>
                         <%--   <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return getDate(550, 310, 0)" />--%>
                                     <asp:ImageButton ID="imgBtnAsOn_date" runat="server"
                                                            ImageUrl="~/Images/calendar.gif"></asp:ImageButton>


                           
                            <br />
                            <span style="font-size: 9pt">(dd/mmm/yyyy)</span></td>

                         <td align="left" colspan="2" >
                            </td>
                    </tr>
                    
                    
                </table>
            </td>
        </tr>
        
         
                    
                    
        <tr>
            <td >
        
            </td>
        </tr>
        <tr>
            <td align="center">
  
                <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" ValidationGroup="groupM1" />&nbsp;
                  <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export Data To Excel"  />&nbsp;
            </td>
           
                
              
            
        </tr>
        <tr>
            <td >
                
            </td>
        </tr>
    </table>
                 <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnAc_date" TargetControlID="txtAcdDate">
                </ajaxToolkit:CalendarExtender>
                   <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnAsOn_date" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

