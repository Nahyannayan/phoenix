<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudATT_Freeze.aspx.vb" Inherits="Students_StudATT_Freeze" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Freeze Attendance Date "></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr align="left">
                        <td  >
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td >
                            <table align="center" width="100%" >
                                <tr>
                                    <td align="left" class="matters" width="20%" ><span class="field-label"> Academic Year</span></td>
  
                                    <td align="left" width="30%"">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Att. Freeze Date</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="As on Date required" ForeColor=""
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <br />
                                        <span >(dd/mmm/yyyy)</span></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" ><span class="field-label">Grade</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <div class="checkbox-list"
                                        <asp:TreeView ID="tvGrade" runat="server" onclick="client_OnTreeNodeChecked();"
                                            ShowCheckBoxes="All">
                                            <NodeStyle CssClass="treenode" />
                                        </asp:TreeView>
                                            </div>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;
                        </td>
                    </tr>
                    <tr align="left">
                        <td   align="center">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add"
                                 /><asp:Button ID="btnEdit" runat="server"
                                    CssClass="button" Text="Edit"   /><asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                      /><asp:Button ID="btnCancel" runat="server"
                                            CssClass="button" Text="Cancel"    />
                        </td>
                    </tr>
                    <tr align="left">
                        <td  >&nbsp;</td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1"
                    runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

