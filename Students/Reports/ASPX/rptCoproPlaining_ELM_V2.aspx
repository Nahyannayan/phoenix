<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCoproPlaining_ELM_V2.aspx.vb" Inherits="ptCoproPlaining_ELM_V2" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtAsOnDate.ClientID%>').value = result;
                      break;
              }
          }
          return false;
      }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Planning Tool V2"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">School Group</span></td>
                                    <td align="left"  style="text-align: left" width="30%">
                                        <asp:DropDownList ID="ddlSch_Grp" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="ASIAN">Asian Schools</asp:ListItem>
                                            <asp:ListItem Value="INTERNATIONAL">International Schools</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left"  width="20%"><span class="field-label">As on</span></td>
                                    <td align="left"  style="text-align: left" width="30%">
                                        <asp:TextBox ID="txtAsOnDate" runat="server" CssClass="inputbox">
                                        </asp:TextBox><asp:ImageButton ID="imgAsOnDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton>
                                    </td>

                                </tr>
                                <tr id="tr_ACD" runat="Server">
                                    <td align="left"  width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left"  style="text-align: left" width="30%">
                                        <asp:DropDownList ID="ddlAcd_year" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    
                                </tr>
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Projected Month</span></td>
                                    <td align="left"  style="text-align: left" width="30%">
                                        <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                            <asp:ListItem Value="JAN">January</asp:ListItem>
                                            <asp:ListItem Value="FEB">February</asp:ListItem>
                                            <asp:ListItem Value="MAR">March</asp:ListItem>
                                            <asp:ListItem Value="APR">April</asp:ListItem>
                                            <asp:ListItem Value="MAY">May</asp:ListItem>
                                            <asp:ListItem Value="JUN">June</asp:ListItem>
                                            <asp:ListItem Value="JUL">July</asp:ListItem>
                                            <asp:ListItem Value="AUG">August</asp:ListItem>
                                            <asp:ListItem Value="SEP">September</asp:ListItem>
                                            <asp:ListItem Value="OCT">October</asp:ListItem>
                                            <asp:ListItem Value="NOV">November</asp:ListItem>
                                            <asp:ListItem Value="DEC">December</asp:ListItem>
                                        </asp:DropDownList><br/>
                                        <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4">
                                        <asp:Button ID="btnDownloadReport" runat="server" CssClass="button" Text="Download as PDF"
                                            ValidationGroup="dayBook" />
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                                ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calAsOnDate" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgAsOnDate" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender> 
            </div>
        </div>
    </div>
    <CR:CrystalReportSource ID="rs1" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>

</asp:Content>

