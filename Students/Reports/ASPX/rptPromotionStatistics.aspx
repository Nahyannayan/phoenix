<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPromotionStatistics.aspx.vb" Inherits="Students_Reports_ASPX_rptPromotionStatistics" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Promotion Statistics"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0"  cellpadding="0" cellspacing="0" width="100%">
                    <tr align="left">
                        <td >
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td >
                            <table id="studTbl" runat="server" align="center" border="0"  cellpadding="0" cellspacing="0" width="100%">
                                <%--  <tr class="subheader_img">
                        <td align="center" colspan="3" style="height: 1px" valign="middle">
                            <div align="left">
                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                    </font></div>
                        </td>
                    </tr>--%>
                                <tr>                                    
                                    <td align="left" width="20%"><span class="field-label" >Select Academic Year</span></td>
                             
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                      <td align="left" width="20%"><span class="field-label" >Select Shift</span></td>
                           
                                    <td align="left" width="30%" >
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                               
                                <tr id="rowGrade" runat="server">                                   
                                    <td align="left" width="20%"><span class="field-label" >Select Grade</span></td>
                           
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2"></td>
                                </tr>
                                <tr>                                   
                                    <td align="center" colspan="4" ><asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" /></td>
                                    
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

