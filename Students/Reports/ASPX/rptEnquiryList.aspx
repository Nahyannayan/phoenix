<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptEnquiryList.aspx.vb" Inherits="Students_Reports_ASPX_rptEnquiryList" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Enquiry List"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook"></asp:ValidationSummary>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%">
                                        <span class="field-label" width="30%">Academic Year</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters" width="20%">
                                        <span class="field-label">Shift</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters">
                                        <span class="field-label">Stream</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters">
                                        <span class="field-label">Grade</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters">
                                        <span class="field-label">Enquiry Status</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlEnquiry" runat="server">
                                            <asp:ListItem Value="0">ALL</asp:ListItem>
                                            <asp:ListItem Value="2">APPLICANT</asp:ListItem>
                                            <asp:ListItem Value="5">APPROVAL</asp:ListItem>
                                            <asp:ListItem Value="1">ENQUIRY</asp:ListItem>
                                            <asp:ListItem Value="6">OFFER LETTER GENERATION</asp:ListItem>
                                            <asp:ListItem Value="3">REGISTRATION</asp:ListItem>
                                            <asp:ListItem Value="4">SCREENING TEST</asp:ListItem>
                                            <asp:ListItem Value="8">WAIT LIST</asp:ListItem>
                                            <asp:ListItem Value="9">DENIED ADMISSION</asp:ListItem>
                                            <asp:ListItem Value="11">DECLINED SEAT</asp:ListItem>
                                            <asp:ListItem Value="12">APPLICATION WITHDRAWN</asp:ListItem>

                                        </asp:DropDownList></td>
                                    <td align="left" class="matters">
                                        <span class="field-label">Prev School Board</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlPrevBoard" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters">
                                        <span class="field-label">Enquiry List of</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlEnqList" runat="server">
                                            <asp:ListItem Value="1">All</asp:ListItem>
                                            <asp:ListItem Value="2">Sibling</asp:ListItem>
                                            <asp:ListItem Value="3">Gems Staff</asp:ListItem>
                                            <asp:ListItem Value="4">General Public</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters">
                                        <span class="field-label">Details of Siblings ?</span></td>
                                    <td>
                                        <asp:CheckBox ID="chkSib" runat="server"
                                            Text="Yes"></asp:CheckBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" colspan="4" style="text-align: center"><asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" OnClick="btnGenerateReport_Click" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_Mode" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

