<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstudATT_Profile.aspx.vb" Inherits="rptstudATT_Profile" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function SelectAllTR(CheckBox) {
            TotalChkBx = parseInt('<%=gvStud.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvStud.ClientID %>');
            var TargetChildControl = "chkSelectTR";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[iCount].checked = CheckBox.checked;
            }
        }
        function SelectDeSelectHeaderTR(CheckBox) {
            TotalChkBx = parseInt('<%=gvStud.Rows.Count %>');
            var TargetBaseControl = document.getElementById('<%=gvStud.ClientID %>');
            var TargetChildControl = "chkSelectTR";
            var TargetHeaderControl = "chkSelectAllTR";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            var flag = false;
            var HeaderCheckBox;
            for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                    HeaderCheckBox = Inputs[iCount];
                if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                    if (CheckBox.checked) {
                        if (!Inputs[iCount].checked) {
                            flag = false;
                            HeaderCheckBox.checked = false;
                            return;
                        }
                        else
                            flag = true;
                    }
                    else if (!CheckBox.checked)
                        HeaderCheckBox.checked = false;
                }
            }
            if (flag)
                HeaderCheckBox.checked = CheckBox.checked
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Students Attendance Profile"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" width="100%">
                    <tr align="left">
                        <td>

                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left"  style="text-align: left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%" width="30%"><span class="field-label">Grade</span></td>
                                    <td align="left"  style="text-align: left;">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="i32" runat="server">
                                    <td align="left"><span class="field-label">Shift</span></td>
                                    <td align="left"  style="text-align: left">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlShift_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Section</span></td>
                                    <td align="left"  style="text-align: left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">From Dated</span></td>
                                    <td align="left"  style="text-align: left">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="90px"></asp:TextBox><ajaxToolkit:CalendarExtender ID="CBEfdate"
                                            runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">To Date</span></td>
                                    <td align="left"  style="text-align: left">
                                        <asp:TextBox ID="txtTodate" runat="server" Width="90px">
                                        </asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="cbetdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgbtntdate"
                                            TargetControlID="txtTodate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgbtntdate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                    </td>
                                    <td align="left" ><span class="field-label">Student Id </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStu_id" runat="server"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td  align="left"><span class="field-label">Student Name </span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSname" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" /></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Width="100%" Height="340px">
                                            <asp:GridView ID="gvStud" runat="server" CssClass="table table-bordered table-row"
                                                AutoGenerateColumns="False" EmptyDataText="No Records Found"
                                                Width="100%" EnableModelValidation="True">

                                                <RowStyle CssClass="griditem" Height="26px" />
                                                <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                                <Columns>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelectTR" runat="server"></asp:CheckBox>

                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAllTR" runat="server" onclick="SelectAllTR(this);"
                                                                ToolTip="Click here to select/deselect all rows" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle Width="30px" />
                                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSTU_NO" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="50px" />
                                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Student Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSNAME" runat="server"
                                                                Text='<%# BIND("SNAME") %>'></asp:Label>
                                                            <asp:Label ID="lblSTU_ID" runat="server"
                                                                Text='<%# BIND("STU_ID") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Grade & Section">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSCT_DESCR" runat="server" Text='<%# bind("SCT_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle/>
                                                <HeaderStyle />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>

                                        </asp:Panel>
                                        <asp:HiddenField ID="hfFromDate" runat="server" />
                                        <asp:HiddenField ID="hfTodate" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="h_STU_IDs" runat="server" />
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center"  colspan="4">&nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" />
                                        <i style="color: Maroon; font-size: 10px; display: block;">Note:Report is generated only for the selected student(s) only.</i>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>



                </table>
                </td></tr></table>
            </div>
        </div>
    </div>
</asp:Content>

