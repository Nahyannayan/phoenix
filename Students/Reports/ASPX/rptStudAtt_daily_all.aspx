<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptStudAtt_daily_all.aspx.vb" Inherits="StudAtt_daily_all" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Daily Attendance Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr align="left">
                        <td colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                          <td align="left" class="matters" width="20%"><span class="field-label" >Stream</span></td>
                                     <td align="left" class="matters" width="30%">  <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                   
                    </tr>
                    <tr>
                             <td align="left" class="matters" width="20%"><span class="field-label">Grade</span></td>
                        <td align="left" class="matters" width="30%">

                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters"><span class="field-label">Section</span></td>
                        <td align="left" class="matters" style="text-align: left">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                   
                    </tr>
                    <tr>
                        <td align="left" class="matters"><span class="field-label">Date</span></td>
                        <td align="left" class="matters" style="text-align: left">
                            <asp:TextBox ID="txtTillDate" runat="server" CssClass="inputbox"  >
                            </asp:TextBox><asp:ImageButton ID="imgTillDate" runat="server" CausesValidation="False"
                                ImageUrl="~/Images/calendar.gif"  ></asp:ImageButton></td>
                    
                    </tr>

                    <tr>
                        <td align="left" class="matters" colspan="4" style="text-align: center"> <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                            ValidationGroup="dayBook" /></td>
                    </tr>
                    <asp:HiddenField ID="h_BSUID" runat="server" />
                    <asp:HiddenField ID="h_Mode" runat="server" />
                    <ajaxToolkit:CalendarExtender ID="calTillDate"
                        runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgTillDate" TargetControlID="txtTillDate">
                    </ajaxToolkit:CalendarExtender>

                </table>
            </div>
        </div>
    </div>
</asp:Content>

