Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Partial Class Students_rptstud_School_Att_unmark
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            If ValidateDate() <> "-1" Then
                CallReport()
            End If

        End If
    End Sub
    Sub bindAtt_Type()
        Try
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(2) As SqlParameter
            ddlAttType.Items.Clear()
            
            Dim ATT_Types As String = String.Empty
            Dim GRD_ID As String = ddlGrade.SelectedItem.Value

            Dim separator As String() = New String() {"|"}
            PARAM(0) = New SqlParameter("@ACD_ID", ACD_ID)

            PARAM(1) = New SqlParameter("@GRD_ID", IIf(ddlGrade.SelectedItem.Value = "0", System.DBNull.Value, ddlGrade.SelectedItem.Value))

            Using readerSTUD_ATT_ADD As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "ATT.GET_ATTENDANCE_TYPE", PARAM)
                While readerSTUD_ATT_ADD.Read
                    ATT_Types = ATT_Types + Convert.ToString(readerSTUD_ATT_ADD("ATT_TYPE")) + "|"
                End While
            End Using

            Dim strSplitArr As String() = ATT_Types.Split(separator, StringSplitOptions.RemoveEmptyEntries)

            For Each arrStr As String In strSplitArr

                If Array.IndexOf(strSplitArr, "Ses1&Ses2") >= 0 Then
                    ddlAttType.Items.Add(New ListItem("Session1", "Session1"))
                    ddlAttType.Items.Add(New ListItem("Session2", "Session2"))
                    Exit For
                Else
                    ddlAttType.Items.Add(New ListItem(arrStr, arrStr))
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CallReport()

        Call GetAcademicWeekend()
        'Call GetAcademicSTARTDT_ENDDT()


        Dim param As New Hashtable
        Dim FROMDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDT.Text) '"25/NOV/2008" '
        Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtToDT.Text) '"25/NOV/2008" '

        Dim ardate As String() = TODT.Split("/")

        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@GRD_ID", Nothing)
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        End If
        If ddlSection.SelectedItem.Value = "0" Then
            param.Add("@SCT_ID", Nothing)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedValue)
        End If

        param.Add("@FROMDT", Format(Date.Parse(FROMDT), "dd/MMM/yyyy"))
        param.Add("@TODT", Format(Date.Parse(TODT), "dd/MMM/yyyy"))
        param.Add("@ATT_TYPE", ddlAttType.SelectedValue)
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("STARTDATE", Format(Date.Parse(txtFromDT.Text), "dd/MMM/yyyy"))
        param.Add("ENDDATE", Format(Date.Parse(txtToDT.Text), "dd/MMM/yyyy"))
     
     
        Dim Current_Year As String = ddlAcademicYear.SelectedItem.Text
        param.Add("Acad_Year", Current_Year)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param



            .reportPath = Server.MapPath("~\Students\Reports\RPT\rptAtt_NotMarked.rpt")





        End With
        'Session.Remove("hashCheck")
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub
    Private Function GetCurrentACY_DESCR(ByVal ACD_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "SELECT  [ACY_DESCR]  FROM [ACADEMICYEAR_M] where ACY_ID=(SELECT [ACD_ACY_ID]   FROM [ACADEMICYEAR_D] where ACD_ID='" & ACD_ID & "')"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function
    Private Function GetGRADES(ByVal ACD_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "DECLARE @GRDS VARCHAR(500);" & _
" SELECT  @GRDS=COALESCE(@GRDS+ '|'+ GRM_GRD_ID ,GRM_GRD_ID)  FROM GRADE_BSU_M WHERE GRM_ACD_ID='" & ACD_ID & "'" & _
" SELECT @GRDS ACD_ID "
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function

    Sub GetAcademicWeekend()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GetAcademicSTARTDT_ENDDT()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcademicYear.SelectedValue


            str_Sql = " select ACD_STARTDT,ACD_ENDDT,replace(convert(varchar(12),acd_startdt,106),' ','/') as fromdt from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = ds.Tables(0).Rows(0)(0).ToString

                ViewState("ACD_ENDDT") = ds.Tables(0).Rows(0)(1).ToString

                txtFromDT.Text = ds.Tables(0).Rows(0)(2).ToString
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059048") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                callYEAR_DESCRBind()
                ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)


                txtToDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                ' di = 
                ddlGrade.Items.Add(New ListItem("ALL", "0"))
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_TEMP(Session("sBsuid"), ACD_ID, GRD_ID)
                ddlSection.Items.Clear()

                ddlSection.Items.Add(New ListItem("ALL", "0"))
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDT.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDT.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDT.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDT.Text.Trim <> "" Then

                Dim strfDate As String = txtToDT.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDT.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDT.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            Else
                CommStr = CommStr & "<li>To Date required"

            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        GetAcademicSTARTDT_ENDDT()
        callGrade_ACDBind()
        bindAtt_Type()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
