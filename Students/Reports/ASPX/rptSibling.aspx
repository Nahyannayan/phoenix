<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSibling.aspx.vb" Inherits="Students_studAttendance_Report" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Student Sibling Report
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--  <table id="Table1" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr >
                        <td align="left" class="title" style="width: 48%">STUDENT SIBLING REPORT</td>
                    </tr>
                </table>--%>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- <tr class="subheader_img">
                                    <td align="center" colspan="3" style="height: 1px" valign="middle">
                                       
                                    <asp:Label id="lblCaption" runat="server" Text="SIBLING REPORT"></asp:Label>
                                    </td>
                                </tr>--%>
                                   <tr>
                                   <td align="left" class="matters" width="20%"><span class="field-label" >Stream</span></td>
                                     <td align="left" class="matters" width="30%">  <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label"></span></td>
                                    <td align="left" width="30%" style="text-align: left">
                                    </td>
                                </tr>
                                <tr>
                                   
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Sibling Count</span></td>
                                     <td align="left" width="30%"> <asp:DropDownList ID="ddlSib" runat="server">
                                            <asp:ListItem>Any</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                         
                                <tr>
                                    <td align="left" width="20%"></td>
                                    <td align="center" colspan="2">&nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" /></td>
                                    <td align="left" width="20%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

