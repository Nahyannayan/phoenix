<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstudAttendance_Report.aspx.vb" Inherits="Students_studAttendance_Report" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Attendance Report">
            </asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook"></asp:ValidationSummary>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                       <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%" style="text-align: left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                   <td align="left" class="matters" width="20%"><span class="field-label" >Stream</span></td>
                                     <td align="left" class="matters" width="30%">  <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>
                                    <td align="left" width="30%" style="text-align: left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Shift</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Section</td>
                        </td>
                        <td align="left" style="text-align: left">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                            ValidationGroup="dayBook" /></td>
                    </tr>
                    </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

