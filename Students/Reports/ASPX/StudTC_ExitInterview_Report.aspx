﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudTC_ExitInterview_Report.aspx.vb" Inherits="Students_StudTC_ExitInterview_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Exit Interview Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tblGetTC" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="vsDates" runat="server" CssClass="error" EnableViewState="False"
                                Font-Size="11px" ForeColor="" ValidationGroup="groupM1" />

                            <asp:RequiredFieldValidator ID="rfFromDate" runat="server" ControlToValidate="txtFromDate" Display="None"
                                ErrorMessage="Please choose the FROM date" SetFocusOnError="True" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfToDate" runat="server" ControlToValidate="txtToDate" Display="None"
                                ErrorMessage="Please choose the TO date" SetFocusOnError="True" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table id="tblStud" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr>
                       
                       <td align="left" class="subheader_img" colspan="4">       </td>
                    
                    </tr>--%>
                                <tr runat="server">
                                 
                                    <td align="left" colspan="2">
                                        <span class="field-label">
                                        <asp:RadioButtonList ID="rbtnBSUTypeTab2" runat="server" CssClass="field-label"
                                            RepeatDirection="Horizontal" AutoPostBack="True">
                                            <asp:ListItem Text="Asian Schools" Value="0" ></asp:ListItem>
                                            <asp:ListItem Text="International Schools" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="All" Selected="True" Value="2"></asp:ListItem>
                                        </asp:RadioButtonList></span>
                                    </td>
                                       <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr id="trBSUforCorporate" runat="server">

                                    <td align="left" width="20%">
                                        <asp:Label ID="lblBSU" runat="server" Text="Business Unit" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlBSU" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:CheckBox ID="chkExclude" Text="Exclude Highest Grade" runat="server" CssClass="field-label" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblEntryType" runat="server" Text="Choose Entry Type" CssClass="field-label"></asp:Label></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlEntryType" runat="server" Width="186px" AutoPostBack="True"
                                            Height="18px">
                                            <asp:ListItem Text="All" Value="0" />
                                            <asp:ListItem Text="Online" Value="1" />
                                            <asp:ListItem Text="School" Value="2" />
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr runat="server" id="trCLM" class="title-bg-lite">
                                    <td align="left" colspan="4">Apply Date</td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">From</span><span class="text-danger font-small">*</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton></td>
                                    <td align="left"><span class="field-label">To</span><span class="text-danger font-small">
                            *</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton></td>
                                </tr>
                                <tr>


                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnGetReport" runat="server" Text="Generate Report"
                                            CssClass="button" TabIndex="4"
                                            CausesValidation="True" ValidationGroup="groupM1" />&nbsp;
                                <asp:Button ID="btnReportPDF" runat="server" Text="Download in PDF"
                                    CssClass="button" TabIndex="4"
                                    CausesValidation="True" ValidationGroup="groupM1" />
                                    </td>


                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <ajaxToolkit:CalendarExtender ID="CBEfdate"
                    runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="cbetdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgToDate"
                    TargetControlID="txtTodate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>

            </div>
        </div>
    </div>
</asp:Content>

