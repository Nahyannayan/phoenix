Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class studAtt_tilldate
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200818") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights

                callYEAR_DESCRBind()
                ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
                BindCol_Field()

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                ' di = 
                ddlGrade.Items.Add(New ListItem("ALL", "0"))
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_TEMP(Session("sBsuid"), ACD_ID, GRD_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

   
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If Page.IsValid Then
            CallReport()
        End If

       
       

    End Sub



    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim STR_COL As String = String.Empty
        Dim STR_SELECT As String = String.Empty
        Dim STR_WHERE As String = String.Empty
        Dim i As Integer = 1
        Dim j As Integer = 0
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@GRD_ID", "NULL")
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        End If

        If ddlSection.SelectedItem.Value = "0" Then
            param.Add("@SCT_ID", "NULL")
        Else
            param.Add("@SCT_ID", ddlSection.SelectedValue)
        End If
        Dim checkedNodes_SCT As TreeNodeCollection = tvCol_Field.CheckedNodes

        If tvCol_Field.CheckedNodes.Count > 0 Then
            For Each node As TreeNode In tvCol_Field.CheckedNodes

                If Not node Is Nothing Then
                    If node.Text <> "Select All" Then
                        If node.Parent.Text <> "Select All" Then
                            STR_COL += "Col" & i & ","
                            STR_SELECT += node.Value & ", "
                            STR_WHERE += "b" & node.Value & " =1 | "
                            param.Add("@Col" & i, node.Text)
                            i += 1

                        End If
                    End If

                End If

            Next
            STR_COL = STR_COL.Trim.TrimEnd(",")
            STR_SELECT = STR_SELECT.Trim.TrimEnd(",")
            STR_WHERE = STR_WHERE.Trim.TrimEnd("|").Replace("|", " OR ")

        End If

        If STR_SELECT = "" Then

            lblError.Text = "Select the field to be displayed."
            Exit Sub
        End If
        j = i
        For j = i To 11
            param.Add("@Col" & j, Nothing)
        Next

        param.Add("@STR_COL", STR_COL)
        param.Add("@STR_SELECT", STR_SELECT)
        param.Add("@STR_WHERE", STR_WHERE)


        param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("../RPT/rptStudSEN_EAL.rpt")


        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub

    Sub BindCol_Field() 'for orginating tree view for the grade and section
        Dim BSU_ID As String = Session("sBsuid")
        Dim STR_CONN As String = ConnectionManger.GetOASISConnectionString
        Dim STR_QUERY As String = "SELECT     SSE_ID, SSE_DESCR, SSE_SUB_DESCR, SSE_COL_NAME, SSE_DISPLAY_ORDER " & _
                                  " FROM  STU.STUDENT_SEN_EAL ORDER BY SSE_DISPLAY_ORDER "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(STR_CONN, CommandType.Text, STR_QUERY)


        Dim dtTable As DataTable = dsData.Tables(0)
        ' PROCESS Filter
        Dim dvGRD_DESCR As New DataView(dtTable, "", "SSE_DESCR", DataViewRowState.OriginalRows)
        Dim trSelectAll As New TreeNode("Select All", "ALL")
        Dim drROL_DESCR As DataRow
        For i As Integer = 0 To dtTable.Rows.Count - 1
            drROL_DESCR = dtTable.Rows(i)
            Dim ienumSelectAll As IEnumerator = trSelectAll.ChildNodes.GetEnumerator()
            Dim contains As Boolean = False
            While (ienumSelectAll.MoveNext())
                If ienumSelectAll.Current.Text = drROL_DESCR("SSE_DESCR") Then
                    contains = True
                End If
            End While
            Dim trNodeROL_DESCR As New TreeNode(drROL_DESCR("SSE_DESCR"), drROL_DESCR("SSE_ID"))
            If contains Then
                Continue For
            End If
            Dim strGRADE_SECT As String = "SSE_DESCR = '" & _
            drROL_DESCR("SSE_DESCR") & "'"
            Dim dvSCT_DESCR As New DataView(dtTable, strGRADE_SECT, "SSE_SUB_DESCR", DataViewRowState.OriginalRows)
            Dim ienumGRADE_SECT As IEnumerator = dvSCT_DESCR.GetEnumerator
            While (ienumGRADE_SECT.MoveNext())
                Dim drUSR_ROL As DataRowView = ienumGRADE_SECT.Current
                Dim trNodeMONTH_DESCR As New TreeNode(drUSR_ROL("SSE_SUB_DESCR"), drUSR_ROL("SSE_COL_NAME"))
                trNodeROL_DESCR.ChildNodes.Add(trNodeMONTH_DESCR)
            End While
            trSelectAll.ChildNodes.Add(trNodeROL_DESCR)
        Next
        tvCol_Field.Nodes.Clear()
        tvCol_Field.Nodes.Add(trSelectAll)
        tvCol_Field.DataBind()

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Page.IsPostBack = False Then
            If Not tvCol_Field Is Nothing Then

                DisableAutoPostBack_ROL(tvCol_Field.Nodes)
            End If
        End If


    End Sub
    Sub DisableAutoPostBack_ROL(ByVal tnc As TreeNodeCollection)
        ' Loop over every node in the passed collection 
        For Each tn As TreeNode In tnc
            ' Set the node's NavigateUrl (which equates to A HREF) to javascript:void(0);, 
            ' effectively intercepting the click event and disabling PostBack. 
            tn.NavigateUrl = "javascript:void(0);"

            ' Set the node's SelectAction to Select in order to enable client-side 
            ' expansion/collapsing of parent nodes. Caution: SelectExpand causes 
            ' expand/collapse icon to disappear! 
            tn.SelectAction = TreeNodeSelectAction.[Select]

            ' If this node has children, recurse over them as well before returning 
            If tn.ChildNodes.Count > 0 Then

                DisableAutoPostBack_ROL(tn.ChildNodes)
            End If
        Next
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
