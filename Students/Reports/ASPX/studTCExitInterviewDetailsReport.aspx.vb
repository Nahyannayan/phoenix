﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Net.Mail
Imports System.Web.Configuration
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class Students_Reports_ASPX_studTCExitInterviewDetailsReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            BindBSU()
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGetReport)

    End Sub

    Sub BindBSU()

        ddlBSU.Items.Clear()
        Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_TYPE <> 'O' AND BSU_BSCHOOL = '1' AND BSU_BGEMSSCHOOL = '1' " _
                                  & " AND BSU_bONLINETC='1' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStr, CommandType.Text, str_query)
        ddlBSU.DataSource = ds
        ddlBSU.DataTextField = "BSU_NAME"
        ddlBSU.DataValueField = "BSU_ID"
        ddlBSU.DataBind()

        'Dim li As New ListItem
        'li.Text = "ALL"
        'li.Value = "0"
        'ddlBSU.Items.Insert(0, li)
        'ddlBSU.SelectedIndex = 0

    End Sub

    ReadOnly Property conStr() As String
        Get
            Return System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString
        End Get
    End Property

    Sub CallReport()



        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", ddlBSU.SelectedItem.Value)
        param(1) = New SqlParameter("@FROMDATE", Format(CDate(txtFromDate.Text), "yyyy-MM-dd"))
        param(2) = New SqlParameter("@TODATE", Format(CDate(txtToDate.Text), "yyyy-MM-dd"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[EXITINTERVIEWSTUDENTDETAILS]", param)

        Dim dt As DataTable

        dt = ds.Tables(0)

        Dim tempFileName As String = Server.MapPath("~/Curriculum/ReportDownloads/") + Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + ".xlsx"
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim ws As ExcelWorksheet = ef.Worksheets.Add("Sheet1")
        ws.InsertDataTable(dt, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        '  ws.HeadersFooters.AlignWithMargins = True
        ef.Save(tempFileName)
        Dim bytes() As Byte = File.ReadAllBytes(tempFileName)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileName))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(tempFileName)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()
    End Sub

    Protected Sub btnGetReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetReport.Click
        CallReport()
    End Sub
End Class
