<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptGenderRel.aspx.vb" Inherits="Students_Reports_ASPX_rptGenderRel" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server">Gender / Religion Report</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr align="left">
                        <td colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"  width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left"  width="20%"><span class="field-label">As on</span></td>
                        <td align="left"  width="30%" style="text-align: left;">
                            <asp:TextBox ID="txtAsOnDate" runat="server"  ></asp:TextBox><span>
                            </span>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtAsOnDate"
                                CssClass="error" Display="Dynamic" ErrorMessage="As on Date required" ForeColor=""
                                ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <br />
                            <span>(dd/mmm/yyyy)</span></td>
                    </tr>


                    <tr>
                        <td align="left" ><span class="field-label">Shift</span></td>
                        <td align="left"  style="text-align: left">
                            <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" ><span class="field-label">Grade</span></td>
                        <td align="left"  style="text-align: left;">

                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" ><span class="field-label">Section</span></td>
                        <td align="left"  style="text-align: left">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                        <td colspan="2"></td>
                        </tr>
                    <tr>
                        <td align="left"  colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="dayBook" /></td>
                    </tr>
                </table>
                <asp:HiddenField ID="h_BSUID" runat="server" />
                <asp:HiddenField ID="h_Mode" runat="server" />

                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                    TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

