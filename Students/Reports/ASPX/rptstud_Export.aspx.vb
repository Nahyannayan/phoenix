Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports GemBox.Spreadsheet
Imports ResponseHelper
Imports System.Security.Cryptography
Partial Class rptstud_Export
    Inherits System.Web.UI.Page
    Private key() As Byte = {}
    Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200211") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights




                callYEAR_DESCRBind()



                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = Export_GRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        ddlGrade.Items.Add(New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID")))
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Function Export_GRM_GRD_ID(ByVal ACD_ID As String, ByVal CLM As String) As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim cmd As New SqlCommand("Export_GRM_GRD_ID", connection)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add(New SqlClient.SqlParameter("@ACD_ID", ACD_ID))
        cmd.Parameters.Add(New SqlClient.SqlParameter("@CLM", CLM))


        Dim reader As SqlDataReader = cmd.ExecuteReader()
        SqlConnection.ClearPool(connection)
        Return reader
    End Function




    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If

            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click


        CallReport()


    End Sub

    Sub CallReport()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim lstrExportType As Integer

        Dim acd_id As String = ddlAcademicYear.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim SCT_ID As String = String.Empty
        If ddlGrade.SelectedItem.Text = "ALL" Then
            GRD_ID = ""
        Else
            GRD_ID = ddlGrade.SelectedValue

        End If

        If ddlSection.SelectedItem.Text = "ALL" Then
            SCT_ID = ""
        Else
            SCT_ID = ddlSection.SelectedValue

        End If



        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", acd_id)
        pParms(1) = New SqlClient.SqlParameter("@GRD_ID", GRD_ID)
        pParms(2) = New SqlClient.SqlParameter("@SCT_ID", SCT_ID)
        pParms(3) = New SqlClient.SqlParameter("@USER_ID", Session("sUsr_name"))
        pParms(4) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
        pParms(5) = New SqlClient.SqlParameter("@pType", "1")
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ICT_Export_OASIS_QUERY", pParms)
        Dim lstrPWD As String

        If ds1.Tables(0).Rows.Count > 0 Then
            For i = 0 To ds1.Tables(0).Rows.Count - 1
                lstrPWD = Decrypt(ds1.Tables(0).Rows(i).Item("STUPASSWORD").Trim.Replace("''", "'"))
                ds1.Tables(0).Rows(i).Item("STUPASSWORD") = lstrPWD
                lstrPWD = ""
            Next
        End If

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim stitle As String = String.Empty

            stitle = "Export"
        
        Dim ws As ExcelWorksheet = ef.Worksheets.Add(stitle)
        ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
        'ws.HeadersFooters.AlignWithMargins = True
       

        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim pathSave As String
        pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
        Dim path = cvVirtualPath & "\StaffExport\" + pathSave

        Dim bytes() As Byte = File.ReadAllBytes(path)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Clear()
        Response.ClearHeaders()
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
        'HttpContext.Current.Response.ContentType = "application/octect-stream"
        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
        'HttpContext.Current.Response.Clear()
        'HttpContext.Current.Response.WriteFile(path)
        'HttpContext.Current.Response.End()
        Else
        lblError.Text = "No Records To display with this filter condition....!!!"
        lblError.Focus()
        End If

    End Sub
    Public Function Decrypt(ByVal stringToDecrypt As String, _
        Optional ByVal sEncryptionKey As String = "!#$a54?W") As String
        Dim inputByteArray(stringToDecrypt.Length) As Byte
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(sEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(stringToDecrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function
    Function GetAllStudentsInSection() As String
        Dim str_sql As String = String.Empty
        Dim acd_id As String = ddlAcademicYear.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim SCT_ID As String = String.Empty
        If ddlGrade.SelectedItem.Text = "ALL" Then
            GRD_ID = " AND STU_GRD_ID<>'' "
        Else
            GRD_ID = " AND STU_GRD_ID='" & ddlGrade.SelectedValue & "'"

        End If

        If ddlSection.SelectedItem.Text = "ALL" Then
            SCT_ID = " AND STU_SCT_ID<>'' "
        Else
            SCT_ID = " AND STU_SCT_ID=" & ddlSection.SelectedValue

        End If

        str_sql = " SELECT  ISNULL(STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
           " STUDENT_M WHERE STU_ACD_ID = " & acd_id & SCT_ID & GRD_ID & " AND STU_CURRSTATUS<>'CN'  for xml path('')),1,0,''),0) "



        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function


    Function GetPhotoClass() As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO
        Dim arrList As ArrayList = New ArrayList(h_STU_IDs.Value.Split("|"))
        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub



End Class
