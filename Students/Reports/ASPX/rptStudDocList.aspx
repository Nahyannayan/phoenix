<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudDocList.aspx.vb" Inherits="Students_Reports_ASPX_rptStudDocList" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Document List"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive mr-3">
                <table align="center" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" runat="server" cellpadding="2" cellspacing="0" id="tblrpt" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span></td>
                                    
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>                  
                                    <td align="left" width="20%"><span class="field-label">Select Grade</span></td>
                                   
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList></td>                
                                </tr>                                                          
                                <tr>
                                    <td align="left"  colspan="4" >
                                        <asp:RadioButton ID="rdEnquiry" runat="server" GroupName="g1" Text="Enquiry" Checked="True" CssClass="field-label" />
                                        <asp:RadioButton ID="rdStudent" runat="server" GroupName="g2" Text="Student" CssClass="field-label" /></td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4" >
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
               </table>

            </div>
        </div>

    </div>
</asp:Content>

