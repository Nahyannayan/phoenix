<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudSchoolStatistic.aspx.vb" Inherits="Students_Reports_ASPX_rptAdmissionDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                        break;
                    case 1:
                        document.getElementById('<%=txtToDate.ClientID %>').value = result;
                      break;
              }
          }
          return false;
      }


    </script>
    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Statistics"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr class="subheader_img">
            <td align="center"  colspan="8" style="height: 1px" valign="middle">
                <div align="left">
                    <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                       </font></div>
            </td>
        </tr>--%>
                                <tr>

                                    <td align="left" width="20%">
                                        <span class="field-label">From Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" /><%--OnClientClick="return getDate(550, 310, 0)"--%>
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtFromDate"
                                            ErrorMessage="From Date required" ValidationGroup="dayBook" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator>
                                        <br />
                                        <span>(dd/mmm/yyyy)</span></td>
                                    <td align="left" width="20%"><span class="field-label">To Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" /><%--OnClientClick="return getDate(550, 310, 1)"--%>
                                        <asp:RequiredFieldValidator ID="rfvToDate"
                                            runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                            ValidationGroup="dayBook" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator><br />
                                        <span>(dd/mmm/yyyy)</span></td>
                                </tr>
                            
                                <tr>
                                   
                                    <td align="left">
                                        <span class="field-label">Shift</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlShift" runat="server">
                                        </asp:DropDownList></td>
                                   <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"><asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" /></td>
                                </tr>
                              
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>

                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgToDate" TargetControlID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>



</asp:Content>

