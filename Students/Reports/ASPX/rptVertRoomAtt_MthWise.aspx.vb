Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class studAtt_tilldate
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S298516") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights

                callYEAR_DESCRBind()



                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlACD_ID.Items.Clear()
                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlACD_ID.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlACD_ID.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlACD_ID.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlACD_ID.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            ddlAcademicYear_SelectedIndexChanged(ddlACD_ID, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlACD_ID.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlACD_ID.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGRD_ID.Items.Clear()
                'di = New ListItem("ALL", "0")
                'ddlGRD_ID.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read

                        ddlGRD_ID.Items.Add(New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID")))
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGRD_ID, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlACD_ID.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlACD_ID.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGRD_ID.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGRD_ID.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_TEMP(Session("sBsuid"), ACD_ID, GRD_ID)
                ddlSct_id.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSct_id.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read

                        ddlSct_id.Items.Add(New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID")))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If Page.IsValid Then
            CallReport()
        End If

    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim param As New Hashtable
        param.Add("@BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@IMG_BSU_ID", Session("sbsuid"))

        param.Add("@ACD_ID", ddlACD_ID.SelectedValue)
      
        param.Add("@GRD_ID", ddlGRD_ID.SelectedItem.Value)


        If ddlSct_id.SelectedItem.Value = "0" Then
            param.Add("@SCT_ID", Nothing)
        Else
            param.Add("@SCT_ID", ddlSct_id.SelectedValue)
        End If
      
        param.Add("@FORMONTH", ddlMTH.SelectedValue)
        param.Add("DISPLAY_MTH", ddlMTH.SelectedItem.Text)
        param.Add("GRADES", ddlGRD_ID.SelectedItem.Text)
        'param.Add("Acad_Year", ddlACD_ID.SelectedItem.Text)
        'param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")

            .reportParameters = param

            .reportPath = Server.MapPath("../RPT/rptVertRoomAtt_MonthWise.rpt")


        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlACD_ID.SelectedIndexChanged
        callGrade_ACDBind()

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGRD_ID.SelectedIndexChanged
        Call callGrade_Section()
    End Sub



   
End Class
