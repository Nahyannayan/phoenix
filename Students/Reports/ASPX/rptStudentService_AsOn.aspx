﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentService_AsOn.aspx.vb" Inherits="Students_Reports_ASPX_rptStudentService_AsOn" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Service Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- <tr class="subheader_img">
                                    <td align="center" colspan="3" style="height: 1px" valign="middle">
                                        <div align="left">
                                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                   </font>
                                        </div>
                                    </td>
                                </tr>--%>
                                <tr>
                                    
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Shift</span></td>
                                    <td align="left" width="30%"> <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                             
                                <tr>
                                   
                                    <td align="left"><span class="field-label">Grade</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Section</span></td>
                                     <td align="left" width="30%"> <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>
                                </tr>                            



                                <tr>
                               
                                    <td align="left"><span class="field-label">As On Date </span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtFromdate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="txtFromdate_CalendarExtender" runat="server"
                                            Enabled="True" TargetControlID="txtFromdate" Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                            Enabled="True" TargetControlID="txtFromdate" PopupButtonID="imgFromDate"
                                            Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                    <td colspan="2"></td>
                                     
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                </tr>

                               
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

