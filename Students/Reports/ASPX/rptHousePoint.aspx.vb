﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptHousePoint
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            txtFromDate.Attributes.Add("onFocus", "this.blur()")
            txtToDate.Attributes.Add("onFocus", "this.blur()")

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200011" And ViewState("MainMnu_code") <> "S200015" And ViewState("MainMnu_code") <> "S200020" And ViewState("MainMnu_code") <> "S200025" And ViewState("MainMnu_code") <> "S200026" And ViewState("MainMnu_code") <> "S200491") And ViewState("MainMnu_code") <> "S200016" Then
            '    If Not Request.UrlReferrer Is Nothing Then
            '        Response.Redirect(Request.UrlReferrer.ToString())
            '    Else

            '        Response.Redirect("~\noAccess.aspx")
            '    End If

            'Else
            'calling pageright class to get the access rights

            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                callStream_Bind()

            'End If

        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function






    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If Page.IsValid Then
            CallReport()
        End If

    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim FromTo_Date As String = ""
        Dim i As Integer = 0


        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@GRD_ID", 0)
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        End If

        If ddlSection.SelectedItem.Value = "0" Then
            param.Add("@SCT_ID", 0)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
        End If
        param.Add("@ACD_ID", Session("Current_ACD_ID"))

        'If ddlStream.SelectedItem.Value = "0" Then
        '    param.Add("@Stream", 0)
        'Else
        '    param.Add("@Stream", ddlStream.SelectedItem.Value)
        'End If

        param.Add("UserName", Session("sUsr_name"))

        param.Add("Grade_Dis", ddlGrade.SelectedItem.Text)
        param.Add("Section_Dis", ddlSection.SelectedItem.Text)



        If txtFromDate.Text <> "" Then
            param.Add("@DTE_FROM", txtFromDate.Text.Trim())
        End If

        If txtToDate.Text <> "" Then
            param.Add("@DTE_TO", txtToDate.Text.Trim())
        End If

        If radReportType.SelectedItem.Value = "3" Then
            param.Add("@POINTS", txtFormTutorPoints.Text.Trim())
        Else
            param.Add("@POINTS", 0)
        End If

        If radReportType.SelectedItem.Value <> "" Then
            param.Add("@OPTION", radReportType.SelectedItem.Value)
        Else
            param.Add("@OPTION", Nothing)
        End If

        If radReportType.SelectedItem.Value = "1" Then
            param("RPT_CAPTION") = "House Point - Class Wise"
        ElseIf radReportType.SelectedItem.Value = "2" Then
            param("RPT_CAPTION") = "House Point - Student Wise"
        ElseIf radReportType.SelectedItem.Value = "3" Then
            param("RPT_CAPTION") = "House Point - Form Tutor Points"
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If radReportType.SelectedItem.Value = "1" Then
                .reportPath = Server.MapPath(("../RPT/rptHousePointClassWise.rpt"))
            ElseIf radReportType.SelectedItem.Value = "2" Then
                .reportPath = Server.MapPath("../RPT/rptHousePointStudentWise.rpt")
            ElseIf radReportType.SelectedItem.Value = "3" Then
                .reportPath = Server.MapPath("../RPT/rptHousePointStudentWiseTutorPoints.rpt")
            End If
            'file:///C:\Documents and Settings\lijo.joseph.GEMSEDUCATION\Desktop\oasis Project\OASISMAR16\Students\Reports\RPT\rptAdmission_Detail.rpt
        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub



    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('" & "/Phoenixbeta/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('" & "/Phoenixbeta/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        callGrade_ACDBind()

    End Sub


    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty

            ACD_ID = Session("Current_ACD_ID").ToString()

            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim STM_ID As String = String.Empty
            If ddlStream.SelectedIndex = -1 Then
                STM_ID = ""
            Else
                STM_ID = ddlStream.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty

            SHF_ID = "" 'Regular

            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_STM(Session("sBsuid"), ACD_ID, GRD_ID, STM_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub

    Protected Sub radReportType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles radReportType.SelectedIndexChanged
        If radReportType.SelectedValue = "3" Then
            trTutorPoints.Visible = True
        Else
            trTutorPoints.Visible = False

        End If
    End Sub

    Public Sub callStream_Bind()
        Try
            ddlSection.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub
    Sub callGrade_ACDBind()
        Try
            ddlSection.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
            PARAM(2) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlGrade.Items.Add(di)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub

End Class
