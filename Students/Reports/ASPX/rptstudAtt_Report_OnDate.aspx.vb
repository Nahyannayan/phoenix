Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studAttendance_Report
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059043") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                rbIgn.Checked = True
                rbGndrAll.Checked = True
                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Call bindAcademic_Year()
                callCurrent_BsuShift()
                Call callStream_Bind()
                'Call bindAcademic_Section()
                Call GetAcademicWeekend()
                Call GetAcademicSTARTDT_ENDDT()

                Call callBSU_HOUSE() 'Added By Nikunj - 14-Jan-2020
                Call bindNationality() 'Added By Nikunj - 14-Jan-2020

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), ACD_ID)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlShift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            Dim Str_ValidateDate As String = String.Empty
            Str_ValidateDate = ValidateDate()
            If Str_ValidateDate = "" Then
                If ddlAcademicYear.SelectedIndex = -1 Then
                    lblError.Text = "Academic Year Required"
                ElseIf ddlGrade.SelectedIndex = -1 Then
                    lblError.Text = "Grade Required"
                ElseIf ddlSection.SelectedIndex = -1 Then
                    lblError.Text = "Section Required"
                Else
                    CallReport()
                End If


            End If
        End If
    End Sub
    Sub bindWeekEndstatus()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GetAcademicWeekend()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GetAcademicSTARTDT_ENDDT()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value


            str_Sql = " select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = ds.Tables(0).Rows(0)(0).ToString
                ViewState("ACD_ENDDT") = ds.Tables(0).Rows(0)(1).ToString
                txtFromDate.Text = Format(Date.Parse(ViewState("ACD_STARTDT")), "dd/MMM/yyyy")
                txtToDate.Text = Format(Date.Parse(ViewState("ACD_ENDDT")), "dd/MMM/yyyy")
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Ason_Date As String = ""
        Dim i As Integer = 0

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        Dim GRD As String = String.Empty
        Dim SCT As String = String.Empty
        If ddlGrade.SelectedItem.Value = "All" Then
            GRD = ""
        Else
            GRD = ddlGrade.SelectedItem.Value
        End If
        If ddlSection.SelectedItem.Value = "All" Then
            SCT = ""
        Else
            SCT = ddlSection.SelectedItem.Value
        End If

        param.Add("@GRD_ID", GRD)
        param.Add("@SCT_ID", SCT)
        param.Add("@STARTDATE", Format(Date.Parse(ViewState("ACD_STARTDT")), "dd/MMM/yyyy"))
        param.Add("@ENDDATE", Format(Date.Parse(ViewState("ACD_ENDDT")), "dd/MMM/yyyy"))
        param.Add("@FROMDT", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
        param.Add("@TODT", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
        param.Add("@WEEKEND1", ViewState("BSU_WEEKEND1"))
        param.Add("@WEEKEND2", ViewState("BSU_WEEKEND2"))

        If rbIgn.Checked Then
            param.Add("@OPR", "0")
        ElseIf rbEqual.Checked Then
            param.Add("@OPR", "1")
        ElseIf rbGreaterThan.Checked Then
            param.Add("@OPR", "2")
        ElseIf rbLessThan.Checked Then
            param.Add("@OPR", "3")
        End If

        If txtPer.Text <> "" Then
            param.Add("@PER", txtPer.Text)
        Else
            param.Add("@PER", "0")
        End If

        'Added By Nikunj - 14-Jan-2020 (Add more filter option)
        param.Add("@STU_HOUSE_ID", ddlHouse.SelectedItem.Value)
        param.Add("@STU_NATIONALITY", ddlNationality.SelectedItem.Value)
        If rbGndrAll.Checked Then
            param.Add("@GENDER", "0")
        ElseIf rbMale.Checked Then
            param.Add("@GENDER", "M")
        ElseIf rbFemale.Checked Then
            param.Add("@GENDER", "F")
        End If

        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        Dim tempFdate As Date = Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy")
        Dim tempTdate As Date = Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy")
        Dim FROMTO As String = String.Empty
        FROMTO = " From " & String.Format("{0:" & OASISConstants.DateFormat & "}", tempFdate) & " to " & String.Format("{0:" & OASISConstants.DateFormat & "}", tempTdate)
        param.Add("FROMTO", FROMTO)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptAtt_GRD_SCT_DATE.rpt")
        End With
        Session("rptClass") = rptClass

        ReportLoadSelection()
    End Sub
    Private Function GetCurrentACY_DESCR(ByVal ACD_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "SELECT  [ACY_DESCR]  FROM [ACADEMICYEAR_M] where ACY_ID=(SELECT [ACD_ACY_ID]   FROM [ACADEMICYEAR_D] where ACD_ID='" & ACD_ID & "')"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call bindAcademic_Section()
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Call callStream_Bind()
        GetAcademicSTARTDT_ENDDT()
    End Sub
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " &
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " &
            " where acd_bsu_id ='" & Session("sBsuid") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcademicYear.Items.Clear()
            ddlAcademicYear.DataSource = ds.Tables(0)
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACD_ID"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            'get the start date and end date
            GetAcademicSTARTDT_ENDDT()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    '   Sub bindAcademic_Grade()
    '       Try
    '           Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '           Dim str_Sql As String
    '           Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
    '           Dim ds As New DataSet
    '           str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
    '" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

    '           ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '           ddlGrade.Items.Clear()
    '           If ds.Tables(0).Rows.Count > 0 Then
    '               ddlGrade.DataSource = ds.Tables(0)
    '               ddlGrade.DataTextField = "GRM_DISPLAY"
    '               ddlGrade.DataValueField = "GRD_ID"
    '               ddlGrade.DataBind()
    '           End If
    '           ddlGrade.Items.Add(New ListItem("All", "All"))
    '           'ddlGrade.ClearSelection()
    '           'ddlGrade.Items.FindByText("All").Selected = True
    '           ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
    '       Catch ex As Exception
    '           UtilityObj.Errorlog(ex.Message)
    '       End Try
    '   End Sub
    Public Sub callStream_Bind()
        Try
            ddlStream.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub
    Sub BindGrade()
        Try
            ddlGrade.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "All")
            ddlGrade.Items.Add(di)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        BindGrade()

    End Sub
    Sub bindAcademic_Section()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim GRD_ID As String
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = ""
            End If



            Dim SHF_ID As String
            If ddlShift.SelectedIndex <> -1 Then
                SHF_ID = ddlShift.SelectedItem.Value
            Else
                SHF_ID = ""
            End If

            Dim str_Sql As String = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " &
 " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID  AND GRADE_BSU_M.GRM_ID=SECTION_M.SCT_GRM_ID " &
" WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND (GRADE_BSU_M.GRM_SHF_ID = '" & SHF_ID & "') AND" &
 " (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "') 	AND (GRADE_BSU_M.GRM_STM_ID = '" + ddlStream.SelectedValue + "')  order by SECTION_M.SCT_DESCR "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSection.Items.Clear()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
            End If
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlSection.Items.Insert(0, di)
            'ddlSection.Items.Add(New ListItem("All", "All"))
            ddlSection.ClearSelection()
            ddlSection.Items.FindByText("All").Selected = True

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDate.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If

            If txtToDate.Text.Trim <> "" Then

                Dim strfDate As String = txtToDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDate.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDate.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE REPORT", "rptstudAtt_Report_OnDate")
            Return "-1"
        End Try

    End Function
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Call bindAcademic_Section()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    'Added by Nikunj - 14-Jan-2020
    Public Sub callBSU_HOUSE()
        Try
            Dim di As ListItem
            Using BSU_HOUSEReader As SqlDataReader = AccessStudentClass.GetBSU_House((Session("sBsuid")))
                ddlHouse.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlHouse.Items.Add(di)
                If BSU_HOUSEReader.HasRows = True Then
                    While BSU_HOUSEReader.Read
                        di = New ListItem(BSU_HOUSEReader("ID"), BSU_HOUSEReader("HideID"))
                        ddlHouse.Items.Add(di)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindNationality()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String

            Dim ds As New DataSet
            str_Sql = " SELECT CTY_ID, CASE CTY_ID WHEN '5' THEN 'Not Available' ELSE CTY_NATIONALITY END CTY_NATIONALITY FROM COUNTRY_M WITH (NOLOCK) WHERE CTY_NATIONALITY <> '' ORDER BY CTY_NATIONALITY ;"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlNationality.Items.Clear()
            ddlNationality.DataSource = ds.Tables(0)
            ddlNationality.DataTextField = "CTY_NATIONALITY"
            ddlNationality.DataValueField = "CTY_ID"
            ddlNationality.DataBind()
            ddlNationality.ClearSelection()

            Dim li As New ListItem
            li.Text = "ALL"
            li.Value = "0"
            ddlNationality.Items.Insert(0, li)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
