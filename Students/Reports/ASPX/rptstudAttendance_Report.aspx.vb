Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studAttendance_Report
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059045") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else

                Call bindAcademic_Year()
                callCurrent_BsuShift()
                Call callStream_Bind()
                Call bindAcademic_Section()
                Call GetAcademicWeekend()
                Call GetAcademicSTARTDT_ENDDT()
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            CallReport()
        End If
    End Sub
    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), ACD_ID)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlShift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using

            ddlShift_SelectedIndexChanged(ddlShift, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindWeekEndstatus()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GetAcademicWeekend()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetAcademicSTARTDT_ENDDT()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value


            str_Sql = " select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = ds.Tables(0).Rows(0)(0).ToString
                ViewState("ACD_ENDDT") = ds.Tables(0).Rows(0)(1).ToString
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Ason_Date As String = ""
        Dim i As Integer = 0

        Dim strdate As String = Format(Date.Parse(ViewState("ACD_STARTDT")), "dd/MMM/yyyy")

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        param.Add("@SCT_ID", ddlSection.SelectedItem.Value)

        param.Add("@STARTDATE", Format(Date.Parse(ViewState("ACD_STARTDT")), "dd/MMM/yyyy"))
        param.Add("@ENDDATE", Format(Date.Parse(ViewState("ACD_ENDDT")), "dd/MMM/yyyy"))

        ' param.Add("@STARTDATE", Format(Date.Parse("01/JUN/2008"), "dd/MMM/yyyy"))
        ' param.Add("@ENDDATE", Format(Date.Parse("31/MAY/2009"), "dd/MMM/yyyy"))

        param.Add("@WEEKEND1", ViewState("BSU_WEEKEND1"))
        param.Add("@WEEKEND2", ViewState("BSU_WEEKEND2"))
        param.Add("UserName", Session("sUsr_name"))
        Dim GradeSection As String = "Grade : " & ddlGrade.SelectedItem.Text & "   Section :  " & ddlSection.SelectedItem.Text
        param.Add("GradeSect", GradeSection)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        Dim Current_Year As String = GetCurrentACY_DESCR(ddlAcademicYear.SelectedItem.Value)

        param.Add("Acad_Year", Current_Year)


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If ViewState("MainMnu_code") = "S059045" Then
                .reportPath = Server.MapPath("../RPT/rptStudAttYear.rpt")

            End If

        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Function GetCurrentACY_DESCR(ByVal ACD_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "SELECT  [ACY_DESCR]  FROM [ACADEMICYEAR_M] where ACY_ID=(SELECT [ACD_ACY_ID]   FROM [ACADEMICYEAR_D] where ACD_ID='" & ACD_ID & "')"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        callCurrent_BsuShift()


    End Sub
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Call bindAcademic_Section()
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Call GetAcademicSTARTDT_ENDDT()
        Call callStream_Bind()
    End Sub
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id ='" & Session("sbsuid").ToString & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcademicYear.Items.Clear()
            ddlAcademicYear.DataSource = ds.Tables(0)
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACD_ID"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            'get the start date and end date
            GetAcademicSTARTDT_ENDDT()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    '   Sub bindAcademic_Grade()
    '       Try
    '           Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '           Dim str_Sql As String
    '           Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
    '           Dim ds As New DataSet
    '           str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
    '" GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

    '           ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
    '           ddlGrade.Items.Clear()
    '           If ds.Tables(0).Rows.Count > 0 Then
    '               ddlGrade.DataSource = ds.Tables(0)
    '               ddlGrade.DataTextField = "GRM_DISPLAY"
    '               ddlGrade.DataValueField = "GRD_ID"
    '               ddlGrade.DataBind()
    '           End If
    '           ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
    '       Catch ex As Exception
    '           UtilityObj.Errorlog(ex.Message)
    '       End Try
    '   End Sub
    Public Sub callStream_Bind()
        Try
            ddlStream.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub
    Sub BindGrade()
        Try
            ddlGrade.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "All")
            ddlGrade.Items.Add(di)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            'ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        BindGrade()

    End Sub
    Sub bindAcademic_Section()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim GRD_ID As String
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = ""
            End If
            Dim SHF_ID As String
            If ddlShift.SelectedIndex <> -1 Then
                SHF_ID = ddlShift.SelectedItem.Value
            Else
                SHF_ID = ""
            End If

            Dim str_Sql As String = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " &
 " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID  AND GRADE_BSU_M.GRM_ID=SECTION_M.SCT_GRM_ID " &
" WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND (GRADE_BSU_M.GRM_SHF_ID = '" & SHF_ID & "') AND" &
 " (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "')  and SECTION_M.SCT_descr<>'TEMP' AND (GRADE_BSU_M.GRM_STM_ID = '" + ddlStream.SelectedValue + "') order by SECTION_M.SCT_DESCR "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSection.Items.Clear()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
   
    
    
End Class
