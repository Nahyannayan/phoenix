<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptKHDAStatistics.aspx.vb" Inherits="Students_Reports_ASPX_rptKHDAStatistics" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                        break;
                    case 1:
                        document.getElementById('<%=txtToDate.ClientID %>').value = result;
                        break;
                }
            }
            return false;
        }


    </script>
    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>
    <%--    <table border="1" bordercolor="#1b80b6" cellpadding="2" cellspacing="0" >
        <tr class="subheader_img">
            <td align="left" colspan="4">
                KHDA Statitics Report</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                Accadamic Year</td>
                <td align="left" colspan="2"> </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                Business Unit</td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                </td>
        </tr>
        <tr>
            <td align="center" colspan="4" style="height: 30px">
                </td>
        </tr>
    </table>--%>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="KHDA Statitics Report"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>
                                    <td align="left" width="30%" style="text-align: left">
                                        <asp:DropDownList ID="cmbBusUnits" runat="server" AutoPostBack="True" BackColor="White"
                                            Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Black"
                                            Width="309px" OnSelectedIndexChanged="cmbBusUnits_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Accadamic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList AutoPostBack="True" BackColor="White" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Black" ID="cmbAccadamicYear" runat="server" Width="209px" OnSelectedIndexChanged="cmbAccadamicYear_SelectedIndexChanged"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">From Date</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputbox">
                                        </asp:TextBox><asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton>
                                    </td>
                                    <td align="left"><span class="field-label">To Date</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="inputbox">
                                        </asp:TextBox><asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnExport" runat="server" CausesValidation="False" CssClass="button"
                                            OnClick="btnExport_Click" Text="Exort to Excel - Genderwise" />
                                        &nbsp;<asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                                            OnClick="btnSearch_Click" Text="Show Report" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calToDate" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgToDate" TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="calFromDate" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

