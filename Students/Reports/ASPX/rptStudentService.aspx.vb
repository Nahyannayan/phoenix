﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptStudentService
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200051") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights

                callYEAR_DESCRBind()
                callCurrent_BsuShift()
                callGrade_ACDBind()
                callGrade_Section()
                bindservices()
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            CallReport()
        End If
    End Sub
    Public Sub callCurrent_BsuShift()
        Try

            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), Session("Current_ACD_ID"))
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlShift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("sbsuid"))
        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@GRD_ID", DBNull.Value)
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        End If

        If ddlSection.SelectedItem.Value = "0" Or ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@SCT_ID", DBNull.Value)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
        End If
        If txtFromdate.Text = "" Then
            param.Add("@FROMDATE", DBNull.Value)
        Else
            param.Add("@FROMDATE", txtFromdate.Text)
        End If
        If txtTodate.Text = "" Then
            param.Add("@TODATE", DBNull.Value)
        Else
            param.Add("@TODATE", txtTodate.Text)
        End If
        Dim service As String = ""
        For Each dl As ListItem In cbServicesList.Items
            If dl.Selected = True Then
                service = dl.Value
            End If
        Next
        If service = "" Then
            param.Add("@SVC_ID", DBNull.Value)
        Else
            param.Add("@SVC_ID", service)
        End If
        param.Add("@STATUS", rbStatus.SelectedValue)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptstudentServicesList.rpt")

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            If ddlAcademicYear.SelectedIndex <> -1 Then
                ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
                ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_ACDBind()
        Try

            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ddlAcademicYear.SelectedItem.Value, Session("CLM"), ddlShift.SelectedValue)
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While
                End If
            End Using
            If ddlGrade.SelectedIndex <> -1 Then
                ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Public Sub callGrade_Section()
        Try

            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex <> -1 Then

                If ddlGrade.SelectedItem.Value = "0" Then
                    GRD_ID = ""
                Else
                    GRD_ID = ddlGrade.SelectedItem.Value
                End If
            End If

            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value, GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub
    Private Function GetSERVICES_Details(Optional ByVal SVCNOt As Integer = 0) As DataTable
        Dim sql_query As String = "SELECT SVB_ID,SVC_ID, SVC_DESCRIPTION FROM SERVICES_BSU_M " & _
                                  " INNER JOIN SERVICES_SYS_M ON SVB_SVC_ID=SVC_ID WHERE " & _
                                  " SVB_ACD_ID=" & ddlAcademicYear.SelectedValue & " AND SVB_BSU_ID='" & Session("sBsuId") & "'" & _
                                  " AND SVB_bAvailable='True'"

        If SVCNOt >= 1 Then
            sql_query += " AND SVC_ID<>" & SVCNOt.ToString
        End If
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
    Sub bindservices()
        cbServicesList.DataSource = GetSERVICES_Details(1)
        cbServicesList.DataTextField = "SVC_DESCRIPTION"
        cbServicesList.DataValueField = "SVC_ID"
        cbServicesList.DataBind()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class

