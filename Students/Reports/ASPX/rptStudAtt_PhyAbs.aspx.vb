Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class studAtt_tilldate
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059049") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                txtAsonDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                callYEAR_DESCRBind()
                ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub bindPhy_Absent()
        Try
            Dim ACD_ID As String = ddlAcademicYear.SelectedValue
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim sql_string As String = String.Empty
            sql_string = " SELECT APD_ID,APD_PARAM_DESCR FROM(SELECT APD_ID,APD_PARAM_DESCR,APD_bPHY_ABS FROM( " & _
" select  APD_ID,APD_PARAM_DESCR,CASE WHEN ATTENDANCE_PARAM_D.APD_bPHY_ABS IS NULL THEN CASE WHEN ATTENDANCE_PARAM_D.APD_APM_ID=1 THEN 0 ELSE 1 END " & _
" ELSE ATTENDANCE_PARAM_D.APD_bPHY_ABS END AS APD_bPHY_ABS from ATTENDANCE_PARAM_D where APD_ACD_ID='" & ACD_ID & "') A WHERE APD_bPHY_ABS=1 UNION " & _
" SELECT * FROM(SELECT APD_ID,APD_PARAM_DESCR, 1 AS bAPP_LEAVE_ABSENT " & _
" FROM ATTENDANCE_PARAM_D  WHERE APD_ACD_ID='" & ACD_ID & "' AND " & _
" APD_PARAM_DESCR='APPROVED LEAVE') B WHERE bAPP_LEAVE_ABSENT=1)C ORDER BY APD_PARAM_DESCR "

            Using readerStudent_ABSENT As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sql_string)
                ddlFilterBy.Items.Clear()

                ddlFilterBy.Items.Add(New ListItem("ALL", "0"))
                If readerStudent_ABSENT.HasRows = True Then


                    While readerStudent_ABSENT.Read

                        ddlFilterBy.Items.Add(New ListItem(readerStudent_ABSENT("APD_PARAM_DESCR"), readerStudent_ABSENT("APD_ID")))
                    End While
                    ddlFilterBy.ClearSelection()
                    ddlFilterBy.Items.FindByText("ALL").Selected = True

                    'ddlFilterBy.DataSource = readerStudent_ABSENT

                    'ddlFilterBy.DataTextField = "APD_PARAM_DESCR"
                    'ddlFilterBy.DataValueField = "APD_ID"

                End If

            End Using


        Catch ex As Exception

        End Try

    End Sub

    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read

                        ddlGrade.Items.Add(New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID")))
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_TEMP(Session("sBsuid"), ACD_ID, GRD_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If Page.IsValid Then
            CallReport()
        End If

    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@GRD_ID", Nothing)
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        End If

        If ddlSection.SelectedItem.Value = "0" Then
            param.Add("@SCT_ID", Nothing)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedValue)
        End If
        If ddlFilterBy.SelectedValue = "0" Then
            param.Add("@FType", Nothing)
        Else
            param.Add("@FType", ddlFilterBy.SelectedValue)


        End If

        param.Add("@ASonDate", Format(Date.Parse(txtAsonDate.Text), "dd/MMM/yyyy"))
        param.Add("fromdt", Format(Date.Parse(txtAsonDate.Text), "dd/MMM/yyyy"))

        param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("../RPT/rptAtt_PhyAbs.rpt")


        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
        bindPhy_Absent()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub



   
End Class
