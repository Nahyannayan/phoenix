Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system

Partial Class Students_Reports_ASPX_rptPrintUserNames
    Inherits System.Web.UI.Page
    '-------------------------------------------------------------------------------------------------
    ' Page Name     : KHDAStatiticsReport.aspx
    ' Purpose       : Criteria screen for KHDAStatiticsReport
    ' Created By    : Sajesh Elias
    ' Created Date  : 30-11-08
    ' Description   : 
    '-------------------------------------------------------------------------------------------------
    ' Module level variable declarations
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            CallReport()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub RetriveBusnsUnits()
        'Dim str_conn = ConnectionManger.GetOASISConnectionString
        'Dim str_query = ""
        'str_query = "SELECT BSU_ID,BSU_NAME FROM dbo.BUSINESSUNIT_M "
        'Dim dsSurveyBUSUnits As DataSet
        'dsSurveyBUSUnits = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'cmbBusUnits.DataSource = dsSurveyBUSUnits.Tables(0)
        'cmbBusUnits.DataTextField = "BSU_NAME"
        'cmbBusUnits.DataValueField = "BSU_ID"
        'cmbBusUnits.DataBind()

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        cmbBusUnits.DataSource = ds.Tables(0)
        cmbBusUnits.DataTextField = "BSU_NAME"
        cmbBusUnits.DataValueField = "BSU_ID"
        cmbBusUnits.DataBind()

        If Not cmbBusUnits.Items.FindByValue(Session("sBsuid")) Is Nothing Then
            cmbBusUnits.Items.FindByValue(Session("sBsuid")).Selected = True
        End If
    End Sub

    

    Private Sub CallReport()

        Dim str_bsu_ids As New StringBuilder
        Dim basedate As String = "15-09-" & Now.Year.ToString




        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", cmbBusUnits.SelectedItem.Value)
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", cmbBusUnits.SelectedItem.Value) '999998--- Corp Office
       
        param.Add("USERNAME", Session("sUsr_name"))
        param.Add("@RPTHEADER", "Academic Year : " + Session("F_Descr").ToString)

        'Session("F_YEAR") = ddFinancialYear.SelectedItem.Value
        'Session("F_Descr") = ddFinancialYear.SelectedItem.Text
        'Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(ddFinancialYear.SelectedItem.Value)

        'Format(Date.Parse(DsAccYear.Tables(0).Rows(0).Item(3)), "dd/MMM/yyyy")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptPrintUserNames_TPT.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()

    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString

        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then

                RetriveBusnsUnits()
                cmbBusUnits.SelectedValue = Session("sBsuid")


            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
   
End Class
