﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudLatepass.aspx.vb" Inherits="Students_Reports_ASPX_rptStudLatepass" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function GetSTUDENTS() {
            var sFeatures;
            sFeatures = "dialogWidth: 729px; ";
            sFeatures += "dialogHeight: 600px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var GRD_IDs = document.getElementById('<%=ddlGrade.ClientID %>').value;
            var SCT_IDs = document.getElementById('<%=ddlSection.ClientID %>').value;
            var ACD_IDs = document.getElementById('<%=ddlAcademicYear.ClientID %>').value;
            var gender = 'ALL';
            if (GRD_IDs == '') {
                alert('Please select atleast one Grade')
                return false;
            }
            //result = window.showModalDialog("../../studPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs + "&gender=" + gender, "", sFeatures)
            var oWnd = radopen("../../studPopupForm.aspx?multiselect=true&ID=STUDENT_GRADE&GRD_IDs=" + GRD_IDs + "&SCT_IDs=" + SCT_IDs + "&ACD_ID=" + ACD_IDs + "&gender=" + gender, "pop_rel");
            <%--if (result != '' && result != undefined) {
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = result;//NameandCode[0];
            }
            else {
                return false;
            }--%>
        }

        function OnClientClose(oWnd, args) {
          
           var arg = args.get_argument();
          
           if (arg) {
                
                NameandCode = arg.Students.split('___');                              
                document.getElementById('<%=h_STU_IDs.ClientID %>').value = arg.Students;
                document.getElementById("<%=txtStudIDs.ClientID%>").value = NameandCode[0];
                __doPostBack('<%= txtStudIDs.ClientID%>', 'TextChanged');               
            }
          
        }



        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;
            var height = body.scrollHeight;
            var width = body.scrollWidth;
            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;
            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Late Pass Report"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    <tr>
                        <td align="center" valign="bottom" style="height: 20px; width: 516px;">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Width="133px" Style="text-align: center"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" align="center" width="100%">
                                <tr>
                                    <td align="left"><span class="field-label">Accademic Year</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trAcd2" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrade" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Section</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="True" Width="108px">
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Date From</span>
                                    </td>
                                    <td align="left">

                                        <asp:TextBox ID="txtCDate" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgCDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgCDate" TargetControlID="txtCDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left"><span class="field-label">Date To</span>
                                    </td>
                                    <td align="left">

                                        <asp:TextBox ID="txtSDate" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgSDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgSDate" TargetControlID="txtSDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStudIDs" runat="server" OnTextChanged="txtStudIDs_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgStudent" runat="server" ImageUrl="~/Images/cal.gif" OnClientClick="GetSTUDENTS();return false;" OnClick="imgStudent_Click"></asp:ImageButton>
                                        <asp:GridView ID="grdStudent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" PageSize="5" Width="100%" OnPageIndexChanging="grdStudent_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Stud. No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbstu_no" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DESCR" HeaderText="Student Name"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_new" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:CheckBox class="field-label" ID="chkCon" runat="server" AutoPostBack="true" Text="Consolidated" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="groupM1" />

                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="h_GRD_IDs" runat="server" />
    <asp:HiddenField ID="h_STU_IDs" runat="server" />
    <asp:HiddenField ID="hfbDownload" runat="server" />
    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_rel" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

</asp:Content>

