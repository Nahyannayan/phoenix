Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Partial Class Students_Reports_ASPX_rptAgeStatistics
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            If ValidateDate() <> "-1" Then
                CallReport()
            End If

        End If
    End Sub
    Sub CallReport()

        Call GetAcademicWeekend()
        Call GetAcademicSTARTDT_ENDDT()


        Dim param As New Hashtable
        Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtAsOnDate.Text) '"25/NOV/2008" '

        Dim ardate As String() = TODT.Split("/")
        ' If ViewState("MainMnu_code") <> "S059057" Then
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        If ViewState("MainMnu_code") <> "S059057" Then
            param.Add("@ACD_ID", Session("Current_ACD_ID"))
        Else
            
        End If

        param.Add("@TODT", Format(Date.Parse(TODT), "dd/MMM/yyyy"))
        If ViewState("MainMnu_code") = "S059044" Or ViewState("MainMnu_code") = "S059054" Then
            param.Add("@BSU_ID", Session("sBsuid"))
            param.Add("@GRD_IDs", GetGRADES(Session("Current_ACD_ID")))
        End If

        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        If ViewState("MainMnu_code") <> "S059057" Then
            param.Add("STARTDATE", Format(Date.Parse(ViewState("ACD_STARTDT")), "dd/MMM/yyyy"))
            param.Add("ENDDATE", Format(Date.Parse(ViewState("ACD_ENDDT")), "dd/MMM/yyyy"))
            param.Add("WEEKEND1", ViewState("BSU_WEEKEND1"))
            param.Add("WEEKEND2", ViewState("BSU_WEEKEND2"))
            param.Add("TDAY", ardate(0))
            param.Add("FORMONTH", ardate(1))
            Dim Current_Year As String = GetCurrentACY_DESCR(Session("Current_ACD_ID"))
            param.Add("Acad_Year", Current_Year)
        End If


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            If ViewState("MainMnu_code") = "S059044" Then

                .reportPath = Server.MapPath("~\Students\Reports\RPT\rptAtt_School_Perc.rpt")
            ElseIf ViewState("MainMnu_code") = "S059054" Then

                .reportPath = Server.MapPath("~\Students\Reports\RPT\rptAtt_School_Perc_Digital.rpt")
            ElseIf ViewState("MainMnu_code") = "S059057" Then

                .reportPath = Server.MapPath("~\Students\Reports\RPT\rptAtt_Corp_Perc.rpt")
            Else

                .reportPath = Server.MapPath("~\Students\Reports\RPT\rptAtt_NotMarked.rpt")
            End If




        End With
        'Session.Remove("hashCheck")
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
    Private Function GetCurrentACY_DESCR(ByVal ACD_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "SELECT  [ACY_DESCR]  FROM [ACADEMICYEAR_M] where ACY_ID=(SELECT [ACD_ACY_ID]   FROM [ACADEMICYEAR_D] where ACD_ID='" & ACD_ID & "')"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function
    Private Function GetGRADES(ByVal ACD_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "DECLARE @GRDS VARCHAR(500);" & _
" SELECT  @GRDS=COALESCE(@GRDS+ '|'+ GRM_GRD_ID ,GRM_GRD_ID)  FROM GRADE_BSU_M WHERE GRM_ACD_ID='" & ACD_ID & "'" & _
" SELECT @GRDS ACD_ID "
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function

    Sub GetAcademicWeekend()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GetAcademicSTARTDT_ENDDT()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = Session("Current_ACD_ID")


            str_Sql = " select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = ds.Tables(0).Rows(0)(0).ToString
                ViewState("ACD_ENDDT") = ds.Tables(0).Rows(0)(1).ToString
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059044") And (ViewState("MainMnu_code") <> "S059054") And (ViewState("MainMnu_code") <> "S059048") And (ViewState("MainMnu_code") <> "S059057") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                txtAsOnDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtAsOnDate.Text.Trim <> "" Then
                Dim strfDate As String = txtAsOnDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>As on Date format is Invalid"
                Else
                    txtAsOnDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtAsOnDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>As on Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>As on Date required"

            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function
End Class
