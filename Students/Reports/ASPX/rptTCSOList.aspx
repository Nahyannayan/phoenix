<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptTCSOList.aspx.vb" Inherits="Students_Reports_ASPX_rptTCSOList" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                        break;
                    case 1:
                        document.getElementById('<%=txtToDate.ClientID %>').value = result;
                      break;
              }
          }
          return false;
      }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            TC/SO List
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                               <%-- <tr class="title-bg-lite">
                                    <td align="center" colspan="4">

                                        <asp:Label ID="lblCaption" runat="server" Text="Select Date Range"></asp:Label>
                                    </td>
                                </tr>--%>
                                <tr>                                    
                                    <td align="left" width="20%">
                                        <span class="field-label">From Date</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                        <%--OnClientClick="return getDate(550, 310, 0)"--%>
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtFromDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="From Date required" ForeColor=""
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <br />
                                        <span>(dd/mmm/yyyy)</span></td>
                                    <td align="left" width="20%">  <span class="field-label">To Date</span></td>
                                    <td align="left" width="30%"><asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                        <%--OnClientClick="return getDate(550, 310, 1)"--%>
                                        <asp:RequiredFieldValidator ID="rfvToDate"
                                            runat="server" ControlToValidate="txtToDate" CssClass="error" Display="Dynamic"
                                            ErrorMessage="To Date required" ForeColor="" ValidationGroup="dayBook">*</asp:RequiredFieldValidator><br />
                                        <span style="font-size: 7pt">(dd/mmm/yyyy)</span></td>
                                </tr>                         
                                <tr>                                   
                                    <td align="left">
                                        <span class="field-label">Grade</span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"> <span class="field-label">Exclude unapproved TC </span></td>
                                     <td align="left" width="30%"> <asp:RadioButtonList ID="rblTC" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                </tr>                           
                                <tr>
                                    
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                   
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CBEfdate"
                                runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="cbetdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgToDate"
                                TargetControlID="txtToDate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</asp:Content>

