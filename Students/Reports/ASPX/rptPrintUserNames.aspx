<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptPrintUserNames.aspx.vb" Inherits="Students_Reports_ASPX_rptPrintUserNames" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-desktop mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Print Usernames"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <%--    <table border="1" bordercolor="#1b80b6" cellpadding="2" cellspacing="0" class="matters">
        <tr class="subheader_img">
            <td align="left" colspan="4">
                KHDA Statitics Report</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label id="lblError" runat="server" CssClass="error"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                Accadamic Year</td>
                <td align="left" colspan="2"> </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                Business Unit</td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                </td>
        </tr>
        <tr>
            <td align="center" colspan="4" style="height: 30px">
                </td>
        </tr>
    </table>--%>


                <table align="center" width="100%">

                    <tr>
                        <td align="left">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Unit</span></td>

                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="cmbBusUnits" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>

                                <tr>
                                    <td align="left" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" CssClass="button"
                                            OnClick="btnSearch_Click" Text="Show Report" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

