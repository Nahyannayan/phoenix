<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstud_School_Att_unmark.aspx.vb" Inherits="Students_rptstud_School_Att_unmark" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Attendance Missing Date"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table  width="100%">
                    <tr align="left">
                        <td colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td align="left"  width="20%"><span class="field-label">Academic Year</span></td>
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left"  width="20%"><span class="field-label">Grade</span></td>
                        <td align="left"  width="30%">

                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="20%"  ><span class="field-label">Section</span></td>
                        <td align="left" width="30%"  style="text-align: left">
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" width="20%"><span class="field-label">Attendance Type</span>
                        </td>
                        <td align="left" width="30%" style="text-align: left">
                            <asp:DropDownList ID="ddlAttType" runat="server">
                            </asp:DropDownList>
                        </td>                        
                    </tr>
                    <tr>
                        <td align="left" width="20%" ><span class="field-label">From Date</span></td>
                        <td align="left" width="30%"  style="text-align: left">
                            <asp:TextBox ID="txtFromDT" runat="server"  ></asp:TextBox>
                            <asp:ImageButton ID="imgFROMDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFromDT"
                                CssClass="error" Display="Dynamic" ErrorMessage="From  Date required" ForeColor=""
                                ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <br />
                            <span >(dd/mmm/yyyy)</span></td>
                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                        <td align="left" width="30%" style="text-align: left">
                            <asp:TextBox ID="txtToDT" runat="server" ></asp:TextBox> 
                            <asp:ImageButton ID="imgtoDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtToDT"
                                CssClass="error" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                            <br />
                            <span  >(dd/mmm/yyyy)</span></td>
                        
                    </tr>
                    <tr>
                        <td align="left"  colspan="4" style="text-align: center"> <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                            ValidationGroup="dayBook" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CBEfdate"
                    runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFROMDate" TargetControlID="txtFromDT">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="cbetdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgtoDate"
                    TargetControlID="txtToDT">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>
</asp:Content>

