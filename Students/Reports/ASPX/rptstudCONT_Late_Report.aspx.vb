Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class studCONT_Absent_Report
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059082") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights

                txtTotdays.Attributes.Add("onKeyPress", " return Numeric_Only()")
                'rbcdate.Attributes.Add("onclick", "chk_checked(1,this)")
                'rbt1date.Attributes.Add("onclick", "chk_checked(0,this)")
                'rbt2date.Attributes.Add("onclick", "chk_checked(0,this)")
                'rbt3date.Attributes.Add("onclick", "chk_checked(0,this)")
                'rbAdate.Attributes.Add("onclick", "chk_checked(0,this)")

                callYEAR_DESCRBind()

                rbcdate.Checked = True

                rbEqual.Checked = True

                'txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Call getACDstart_dt()
                txtTodate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Sub getACDstart_dt()
        Try

        
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim acd_id As String = ddlAcademicYear.SelectedValue
            Dim sqlString As String = String.Empty
            sqlString = "select acd_startdt,acd_enddt from academicyear_d where acd_id='" & acd_id & "' "




            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sqlString)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", readerStudent_Detail(0))

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
           
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If

            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim sqlstring As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            sqlstring = "SELECT distinct GRADE_BSU_M.GRM_SHF_ID as SHF_ID , SHIFTS_M.SHF_DESCR as SHF_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
" ACADEMICYEAR_D ON  GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
 " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID where (ACD_ID = '" & ACD_ID & "' and GRADE_BSU_M.GRM_GRD_ID='" & GRD_ID & "') order by SHIFTS_M.SHF_DESCR"

            Using Current_BsuShiftReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sqlstring)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read

                        ddlShift.Items.Add(New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID")))
                    End While

                End If
            End Using

            If ddlShift.Items.Count > 1 Then
                i32.Visible = True
            Else
                i32.Visible = False
            End If

            ddlShift_SelectedIndexChanged(ddlShift, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub LATE_PAR()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If

            Dim sqlstring As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            sqlstring = " SELECT APD_ID,APD_PARAM_DESCR FROM  ATTENDANCE_PARAM_D WHERE    " & _
" (APD_ACD_ID = '" & ACD_ID & "') AND (APD_PARAM_DESCR LIKE '%LATE%') AND (APD_bSHOW = 1)"


            Using LATE_PARReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sqlstring)
                ddllate.Items.Clear()
                If LATE_PARReader.HasRows = True Then
                    While LATE_PARReader.Read

                        ddllate.Items.Add(New ListItem(LATE_PARReader("APD_PARAM_DESCR"), LATE_PARReader("APD_ID")))
                    End While

                End If
            End Using
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddllate.Items.Add(di)

            ddllate.ClearSelection()
            ddllate.Items.FindByText("ALL").Selected = True

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If rbcdate.Checked = True Then
            If ValidateDate() <> "-1" Then
                CallReport()
            End If
        Else
            CallReport()
        End If

    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDate.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtTodate.Text.Trim <> "" Then

                Dim strfDate As String = txtTodate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtTodate.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDate.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtTodate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            Else
                CommStr = CommStr & "<li>To Date required"

            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim FromTo_Date As String = ""
        Dim i As Integer = 0

        Dim from_date As String = String.Empty
        Dim to_date As String = String.Empty
        Dim DAYSFILTER As Integer

        If rbcdate.Checked = True Then
            from_date = Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy")
            to_date = Format(Date.Parse(txtTodate.Text), "dd/MMM/yyyy")
        Else

            from_date = Format(Date.Parse(hfFromDate.Value), "dd/MMM/yyyy")
            to_date = Format(Date.Parse(hfTodate.Value), "dd/MMM/yyyy")

        End If
        If rbEqual.Checked = True Then
            DAYSFILTER = 0
        ElseIf rbGreaterThan.Checked = True Then
            DAYSFILTER = 1
        ElseIf rbLessThan.Checked = True Then
            DAYSFILTER = 2
        End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@GRD_ID", ddlGrade.SelectedValue)

        If ddlSection.SelectedItem.Text = "ALL" Then
            param.Add("@GRD_SCT", Nothing)
        Else
            param.Add("@GRD_SCT", ddlSection.SelectedItem.Value)
        End If
        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param.Add("@BSU_ID", Session("sbsuid"))

        param.Add("@TEMP_FROMDT", from_date)
        param.Add("@TEMP_TODT", to_date)

        param.Add("@bWEEKEND", "0")
        param.Add("@bHOLIDAYS", "0")
        param.Add("@TOTDAYS", txtTotdays.Text)
        param.Add("@DAYSFILTER", DAYSFILTER)
        If ddllate.SelectedItem.Text = "ALL" Then
            param.Add("@LateType", Nothing)
        Else
            param.Add("@LateType", ddllate.SelectedValue)
        End If

        param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        param.Add("fromdt", Format(Date.Parse(from_date), "dd/MMM/yyyy"))
        param.Add("todt", Format(Date.Parse(to_date), "dd/MMM/yyyy"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptAtt_studCont_late.rpt")

        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        LATE_PAR()
        Call getACDstart_dt()
        callGrade_ACDBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callCurrent_BsuShift()
    End Sub
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call callGrade_Section()
    End Sub



    Protected Sub rbt1date_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAdate.CheckedChanged, rbt2date.CheckedChanged, rbt3date.CheckedChanged, rbcdate.CheckedChanged

        If rbcdate.Checked = True Then

            i30.Visible = True
            i31.Visible = True
        Else
            Call getFrom_ToDate()
            i30.Visible = False
            i31.Visible = False
        End If

    End Sub

    Sub getFrom_ToDate()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim sqlString As String = String.Empty
            Dim acd_id As String = ddlAcademicYear.SelectedValue

            If rbt1date.Checked Then
                sqlString = "select trm_startdate,trm_enddate from term_master " & _
    " where trm_id =(select trm_id FROM(select r1,trm_id from(select row_number() over(order by trm_id) r1, trm_id " & _
     " from dbo.TERM_MASTER where trm_acd_id='" & acd_id & "' ) A WHERE R1=1)B)"

            ElseIf rbt2date.Checked Then
                sqlString = "select trm_startdate,trm_enddate from term_master " & _
    " where trm_id =(select trm_id FROM(select r1,trm_id from(select row_number() over(order by trm_id) r1, trm_id " & _
     " from dbo.TERM_MASTER where trm_acd_id='" & acd_id & "' ) A WHERE R1=2)B)"
            ElseIf rbt3date.Checked Then
                sqlString = "select trm_startdate,trm_enddate from term_master " & _
                    " where trm_id =(select trm_id FROM(select r1,trm_id from(select row_number() over(order by trm_id) r1, trm_id " & _
                     " from dbo.TERM_MASTER where trm_acd_id='" & acd_id & "' ) A WHERE R1=3)B)"

            ElseIf rbAdate.Checked Then
                sqlString = "select acd_startdt,acd_enddt from academicyear_d where acd_id='" & acd_id & "' "

            End If


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sqlString)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        hfFromDate.Value = Convert.ToString(readerStudent_Detail(0))
                        hfTodate.Value = Convert.ToString(readerStudent_Detail(1))
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
