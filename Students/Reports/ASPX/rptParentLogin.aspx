<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptParentLogin.aspx.vb" Inherits="Students_Reports_ASPX_rptParentLogin" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
          
         
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>

            Parent login details report
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td style="width: 457px">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr class="title">
                        <td align="left" colspan="3"  style="height: 2px" valign="middle">Parent login details report</td>
                 </tr>--%>
                               <%-- <tr class="title-bg-lite">
                                    <td align="center" colspan="4">
                                        <asp:Label ID="lblCaption" runat="server" Text="Select Grade/Section"></asp:Label>
                                    </td>
                                </tr>--%>
                                <tr>

                                    <td align="left" width="20%">
                                        <span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Shift</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                            
                                <tr>
                                  
                                    <td align="left">
                                        <span class="field-label">Grade</span></td>

                                    <td align="left">

                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"> <span class="field-label">Section</span></td>
                                      <td align="left" width="30%"><asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                              

                                <tr>
                                    <td align="left" width="20%"></td>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" OnClick="btnGenerateReport_Click1" /></td>
                                    <td align="left" width="20%"></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

