<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstudAtt_Romm_WeeklyReport.aspx.vb" Inherits="Students_Reports_ASPX_rptClasslistTemplate" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" width="100%">
                               
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span><span style="color: #800000">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcd" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcd_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Grade</span><span style="color: #000000">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGrd" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrd_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Subject</span><span
                                        style="color: #000000">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSubject_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Subject Group</span><span style="font-size: 8pt; color: #800000">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlSubjectGroup" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="left">
                                        <asp:RadioButton ID="rbPRS" runat="server" GroupName="gp" OnCheckedChanged="rbPRS_CheckedChanged" Text="Present" AutoPostBack="True"></asp:RadioButton>
                                        <asp:RadioButton ID="rbABS" runat="server" GroupName="gp" OnCheckedChanged="rbAbs_CheckedChanged" Text="Absent" AutoPostBack="True"></asp:RadioButton>
                                        <asp:RadioButton ID="rbLATE" runat="server" GroupName="gp" OnCheckedChanged="rbLate_CheckedChanged" Text="Late" AutoPostBack="True"></asp:RadioButton></td>
                                    <td align="left" width="20%"><span class="field-label">Filter By</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlFilterType" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span><span style="color: #800000">*</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtfromDT" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDt" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        
                                    <td align="left" width="20%"><span class="field-label">To Date</span><font class="error" color="red">*</font></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDT" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton ID="imgTODT" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgTODT" TargetControlID="txtToDT">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1"
                                runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDT" TargetControlID="txtFROMDT">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

