<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentBirthday.aspx.vb" Inherits="Students_Reports_ASPX_rptStudentBirthday" Title="Untitled Page" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label id="lblCaption" runat="server" Text="Birthday's List"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" style="width:100%">
                    <tr>
                        <td align="left" style="width:20%;"><span class="field-label">FirstDate</span></td>
                        <td align="left" style="text-align: left;width:30%">
                            <asp:TextBox ID="txtfirstdate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                runat="server" ControlToValidate="txtfirstdate" Display="None" ErrorMessage="Enter date"
                                ForeColor="White" ValidationGroup="dt"></asp:RequiredFieldValidator></td>
                  
                        <td align="left"  style="width:20%"><span class="field-label">Second Date</span></td>
                        <td align="left" style="text-align: left; width:30%;">
                            <asp:TextBox ID="txtseconddate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imagecalen" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtseconddate"
                                Display="None" ErrorMessage="Enter date" ForeColor="White" ValidationGroup="dt">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" OnClick="btnGenerateReport_Click"
                            Text="Generate Report" ValidationGroup="dt" /></td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtfirstdate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imagecalen" TargetControlID="txtseconddate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
                    TargetControlID="RequiredFieldValidator2">
                </ajaxToolkit:ValidatorCalloutExtender>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server"
                    TargetControlID="RequiredFieldValidator1">
                </ajaxToolkit:ValidatorCalloutExtender>
            </div>
        </div>
    </div>
</asp:Content>

