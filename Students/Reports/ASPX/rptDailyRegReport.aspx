<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptDailyRegReport.aspx.vb" Inherits="Students_Reports_ASPX_rptDailyRegReport" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Daily Registrar Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">

                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">As on</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtAsOnDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtAsOnDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="As on Date required"  
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <br />
                                        <span>(dd/mmm/yyyy)</span></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                    <td align="left" class="matters" colspan="4" style="text-align: center">&nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" />
                                        &nbsp;
                            <asp:Button ID="btncancel" runat="server" CssClass="button" Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                    TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

