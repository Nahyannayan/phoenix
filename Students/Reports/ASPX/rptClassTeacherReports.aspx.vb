Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Partial Class Students_Reports_ASPX_rptClassTeacherReports
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491


            Dim pParms(3) As SqlClient.SqlParameter
            Dim lstrALLOW As String = String.Empty
            Dim lstrREPORTPATH As String = String.Empty


            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBsuid"))
            pParms(1) = New SqlClient.SqlParameter("@REPORT_TYPE", "CLASS_TEACHER")
            pParms(2) = New SqlClient.SqlParameter("@MNU_CODE", ViewState("MainMnu_code"))
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_VALIDATE_MENUS", pParms)
                While reader.Read
                    lstrALLOW = Convert.ToString(reader("ALLOW"))
                    ViewState("lstrREPORTPATH") = Convert.ToString(reader("REPORTPATH"))
                End While
            End Using


            If USR_NAME = "" Or (lstrALLOW = "NO") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                callYEAR_DESCRBind()
                callCurrent_BsuShift()
                callGrade_ACDBind()
                callGrade_Section()



                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                txtAsOnDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                'If Session("sBsuid") <> "125011" And Session("sBsuid") <> "125010" Then
                '    TR1.Visible = False
                'End If
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next

            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = GetGRM_GRD_ID(ACD_ID, Session("CLM"))

                ddlGrade.Items.Clear()
                'di = New ListItem("ALL", "0")
                'ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRM_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Function GetGRM_GRD_ID(ByVal ACD_ID As String, ByVal CLM As String) As SqlDataReader
        ''''Check the related Function references


        'Author(--Lijo)
        'Date   --16/JAN/2008
        'Purpose--Get DRQ_GRM_GRD_ID data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGRM_GRD_ID As String = ""

        sqlGetGRM_GRD_ID = "SELECT  distinct   GRADE_BSU_M.GRM_ID AS GRM_ID,GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,GRADE_M.GRD_DISPLAYORDER as GRD_DISPLAYORDER " & _
                        " FROM GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID INNER JOIN SECTION_M SCT ON GRM_ID=SCT_GRM_ID AND SCT_EMP_ID='" & Session("EmployeeId") & "'  INNER JOIN " & _
                      " ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND " & _
                     " GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "'  order by GRADE_M.GRD_DISPLAYORDER"



        '"SELECT GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY " & _
        '" FROM ACADEMICYEAR_D INNER JOIN  GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
        '                     " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER"


        Dim command As SqlCommand = New SqlCommand(sqlGetGRM_GRD_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID, SHF_ID)
                ddlSection.Items.Clear()

                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Function GetGrade_Section(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, Optional ByVal SHF_ID As String = "") As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Section As String = ""
        If SHF_ID = "" Then
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where SCT_EMP_ID='" & Session("EmployeeId") & "'  AND [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_ID='" & GRD_ID & "') order by SCT_DESCR"
        Else
            sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where SCT_EMP_ID='" & Session("EmployeeId") & "'  AND [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_ID='" & GRD_ID & "'  AND [GRM_SHF_ID]='" & SHF_ID & "' ) order by SCT_DESCR"
        End If

        Dim command As SqlCommand = New SqlCommand(sqlGrade_Section, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), ACD_ID)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlShift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If Page.IsValid Then
            CallReport()
        End If

    End Sub
    Sub CallReport()

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile



        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        pParms(1) = New SqlClient.SqlParameter("@GRD_ID", ddlGrade.SelectedItem.Value)
        pParms(5) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
        pParms(6) = New SqlClient.SqlParameter("@SCT_ID", ddlSection.SelectedItem.Value)
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, ViewState("lstrREPORTPATH"), pParms)


        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            '  ws.HeadersFooters.AlignWithMargins = True
           

            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Date.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave
            HttpContext.Current.Response.ContentType = "application/octect-stream"
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.WriteFile(path)
            HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If


    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub



    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        callGrade_ACDBind()
        Call callGrade_Section()
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub

End Class
