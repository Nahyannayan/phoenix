Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptClasslistTemplate
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059041" ) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                callYEAR_DESCRBind()
                callCurrent_BsuShift()
                rbAbs.Checked = True
                BindAbsent()
                
                FillGrades()
                rowGrade.Visible = False

                getACDstart_dt()
                txtToDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtfromDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub FillGrades()
        ddlGrdaes.Items.Clear()
        BindGrades()
        If ddlGrdaes.Items.Count >= 1 Then
            ddlGradesAll.Items.Clear()
            Dim lstItem As New ListItem
            lstItem.Text = "SELECT ALL"
            lstItem.Value = "0"
            ddlGradesAll.Items.Insert(0, lstItem)
        Else
            ddlGradesAll.Items.Clear()
        End If

    End Sub
    Sub BindGrades()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
        Dim ds As New DataSet



        str_Sql = " SELECT DISTINCT  GRADE_BSU_M.GRM_DISPLAY, GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_M.GRD_DISPLAYORDER " & _
              " FROM  GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID WHERE GRM_ACD_ID=" & ACD_ID & " ORDER BY GRD_DISPLAYORDER"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlGrdaes.Items.Clear()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlGrdaes.DataSource = ds.Tables(0)
            ddlGrdaes.DataTextField = "GRM_DISPLAY"
            ddlGrdaes.DataValueField = "GRD_ID"
            ddlGrdaes.DataBind()
        End If

    End Sub

    Sub BindAbsent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
        Dim ds As New DataSet

        If rbAbs.Checked = True Then
            str_Sql = "SELECT    APD_ID,APD_PARAM_DESCR FROM ATTENDANCE_PARAM_D  WHERE  (APD_ACD_ID = " & ACD_ID & " ) " & _
                      " AND (APD_APM_ID = '2') AND (APD_bSHOW = 1) ORDER BY APD_PARAM_DESCR "

        Else
            str_Sql = "SELECT    APD_ID,APD_PARAM_DESCR FROM ATTENDANCE_PARAM_D  WHERE  (APD_ACD_ID = " & ACD_ID & " ) " & _
                      " AND APD_PARAM_DESCR like '%late%' ORDER BY APD_PARAM_DESCR "

        End If

        

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlFilterType.Items.Clear()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlFilterType.DataSource = ds.Tables(0)
            ddlFilterType.DataTextField = "APD_PARAM_DESCR"
            ddlFilterType.DataValueField = "APD_ID"
            ddlFilterType.DataBind()
        End If



        ddlFilterType.Items.Add(New ListItem("ALL", "0"))
        ddlFilterType.Items.FindByText("ALL").Selected = True
        If ddlFilterType.Items.Count = 2 Then

            trType.Visible = False
        Else
            trType.Visible = True
        End If

    End Sub
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If

            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), ACD_ID)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read

                        ddlShift.Items.Add(New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID")))
                    End While
                    
                End If
            End Using
            If ddlShift.Items.Count > 1 Then
                trshf.Visible = True
            Else
                trshf.Visible = False
            End If

           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then


            If ValidateDate() = "" Then
                Dim todt As DateTime = txtToDT.Text
                Dim fromdt As DateTime = txtfromDT.Text

                Dim startdt As DateTime = ViewState("startDt")
                Dim enddt As DateTime = ViewState("endDt")

                If ((fromdt >= startdt) And (fromdt <= enddt)) And ((todt >= startdt) And (todt <= enddt)) Then
                    CallReport()
                Else
                    lblError.Text = "Date must be within the academic year!!!"
                End If

            End If

        End If

    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim FromTo_Date As String = ""
        Dim i As Integer = 0

        'If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
        '    FromTo_Date = " AND STUDENT_M.STU_DOJ BETWEEN '" + Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd") + "'"
        'End If
        Dim strGrades As String = ""
        For Each LstItem As ListItem In ddlGrdaes.Items
            If LstItem.Selected = True Then
                strGrades += LstItem.Value + "|"
            End If
        Next
        'If strGrades.Length >= 2 Then
        '    strGrades = strGrades.Substring(0, Len(strGrades) - 1)
        'End If


        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        If radSingle.Checked = True Then
            If ddlGrade.SelectedItem.Text = "ALL" Then
                param.Add("@Grade", Nothing)
            Else
                param.Add("@Grade", ddlGrade.SelectedItem.Value & "|")
            End If
        Else

            If strGrades = "" Then
                param.Add("@Grade", Nothing)
            Else
                param.Add("@Grade", strGrades)
            End If
        End If

        If ddlSection.SelectedItem.Text = "ALL" Then
            param.Add("@SECTION", Nothing)
        Else
            param.Add("@SECTION", ddlSection.SelectedItem.Value)
        End If
        If ddlShift.SelectedItem.Text = "ALL" Then
            param.Add("@SHF_ID", Nothing)
        Else
            param.Add("@SHF_ID", ddlShift.SelectedItem.Value)
        End If

        If ddlFilterType.SelectedItem.Text = "ALL" Then
            param.Add("@FilterType", Nothing)
        Else
            param.Add("@FilterType", ddlFilterType.SelectedItem.Value & "|")
        End If


        If rbAbs.Checked = True Then
            param.Add("@ABS_LATE", "AB")
        ElseIf rbLate.Checked = True Then
            param.Add("@ABS_LATE", "LT")
        End If

        param.Add("@ACD", ddlAcademicYear.SelectedItem.Value)
        param.Add("@FROMDT", Format(Date.Parse(txtfromDT.Text), "dd/MMM/yyyy"))
        param.Add("@TODT", Format(Date.Parse(txtToDT.Text), "dd/MMM/yyyy"))
        param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        param.Add("fromdt", Format(Date.Parse(txtfromDT.Text), "dd/MMM/yyyy"))
        param.Add("todt", Format(Date.Parse(txtToDT.Text), "dd/MMM/yyyy"))

        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        If rbAbs.Checked = True Then
            param.Add("titleHead", "List of Absentees")
        ElseIf rbLate.Checked = True Then
            param.Add("titleHead", "List of Late Students")
        End If




        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptAtt_student _absent.rpt")

        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

  
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
        getACDstart_dt()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub
    Protected Sub ddlGradesAll_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlGradesAll.Items.Count = 1 Then
                If ddlGradesAll.Items(0).Selected = True Then
                    CheckALL(ddlGrdaes)
                Else
                    UnCheckALL(ddlGrdaes)
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub getACDstart_dt()
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim acd_id As String = ddlAcademicYear.SelectedValue
            Dim sqlString As String = String.Empty
            sqlString = "select acd_startdt,acd_enddt from academicyear_d where acd_id='" & acd_id & "' "




            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sqlString)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        ViewState("startDt") = String.Format("{0:" & OASISConstants.DateFormat & "}", readerStudent_Detail(0))
                        ViewState("endDt") = String.Format("{0:" & OASISConstants.DateFormat & "}", readerStudent_Detail(1))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"





            If txtFromDt.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDt.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDt.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"

                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtTODT.Text.Trim <> "" Then

                Dim strfDate As String = txtTODT.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtTODT.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDt.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtTODT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function

   
    

    Private Function CheckALL(ByVal ChkList As CheckBoxList)
        Try
            For Each LstItem As ListItem In ChkList.Items
                LstItem.Selected = True
            Next
        Catch ex As Exception

        End Try
    End Function
    Private Function UnCheckALL(ByVal ChkList As CheckBoxList)
        Try
            For Each LstItem As ListItem In ChkList.Items
                LstItem.Selected = False
            Next
        Catch ex As Exception

        End Try
    End Function

    Protected Sub radSingle_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If radSingle.Checked = True Then
                rowGradSingle.Visible = True
                rowGrade.Visible = False
                rowSection.Visible = True
            Else
                rowGradSingle.Visible = False
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub radMultiple_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If radMultiple.Checked = True Then
                rowGrade.Visible = True
                rowGradSingle.Visible = False
                rowSection.Visible = False
            Else
                rowGrade.Visible = False
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub rbAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindAbsent()
    End Sub

    Protected Sub rbLate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindAbsent()
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Call callGrade_Section()
    End Sub
End Class
