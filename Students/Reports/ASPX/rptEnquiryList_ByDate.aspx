<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptEnquiryList_ByDate.aspx.vb" Inherits="Students_Reports_ASPX_rptEnquiryList" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Enquiry List By Date"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook"></asp:ValidationSummary>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcademicYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Shift</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Stream</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters"><span class="field-label">Grade</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Enquiry Status</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlEnquiry" runat="server">
                                            <asp:ListItem Value="0">ALL</asp:ListItem>
                                            <asp:ListItem Value="2">APPLICANT</asp:ListItem>
                                            <asp:ListItem Value="5">APPROVAL</asp:ListItem>
                                            <asp:ListItem Value="1">ENQUIRY</asp:ListItem>
                                            <asp:ListItem Value="6">OFFER LETTER GENERATION</asp:ListItem>
                                            <asp:ListItem Value="3">REGISTRATION</asp:ListItem>
                                            <asp:ListItem Value="4">SCREENING TEST</asp:ListItem>
                                        </asp:DropDownList></td>

                                    <td align="left" class="matters"><span class="field-label">Prev School Board</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlPrevBoard" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Enquiry List of</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlEnqList" runat="server">
                                            <asp:ListItem Value="1">All</asp:ListItem>
                                            <asp:ListItem Value="2">Sibling</asp:ListItem>
                                            <asp:ListItem Value="3">Gems Staff</asp:ListItem>
                                            <asp:ListItem Value="4">General Public</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters"><span class="field-label">From Date</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:TextBox ID="txtFromDate" runat="server">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtFromDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="From Date required" ForeColor=""
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <br />
                                        <span>(dd/mmm/yyyy)</span></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">To Date</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:TextBox ID="txtToDate" runat="server">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator><br />
                                        <span>(dd/mmm/yyyy)</span></td>
                                    <td align="left" class="matters"><span class="field-label">Details of Siblings ? </span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSib" runat="server" CssClass="field-label"
                                            Text="Yes"></asp:CheckBox></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" OnClick="btnGenerateReport_Click" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="h_Mode" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgToDate"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

