<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstud_School_Att.aspx.vb" Inherits="Students_Reports_ASPX_rptAgeStatistics" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtAsOnDate.ClientID %>').value = result;
                        break;
                }
            }
            return false;
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="School Attendance As on date"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">As on</span></td>
                                    <td align="left" style="text-align: left" width="30%">
                                        <asp:TextBox ID="txtAsOnDate" runat="server" CssClass="inputbox">
                                        </asp:TextBox><asp:ImageButton ID="imgTillDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                             PopupButtonID="imgTillDate" TargetControlID="txtAsOnDate">
                                            </ajaxToolkit:CalendarExtender>
                                    </td>
                        </td>
                        <td width="50%"></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2s">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="dayBook" /></td>
                    </tr>
                </table>
                &nbsp;
                        </td>
                    </tr>
                </table>
                
            </div>
        </div>
    </div>
</asp:Content>

