<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptEnquiryStatistic.aspx.vb" Inherits="Students_Reports_ASPX_rptEnquiryStatistic" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Enquiry Statistics"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFrom"
                                Display="None" ErrorMessage="Please enter the date" ValidationGroup="groupM1"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr valign="bottom" >
                        <td align="left" colspan="4" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>

                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="20%">
                                        <asp:Label ID="lblShift" runat="server" Text="Date" CssClass="field-label"></asp:Label></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnEnqDate" runat="server" ImageUrl="~/Images/calendar.gif" /><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtFrom"
                                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Date entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label" >Search type</span></td>
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlType" runat="server">
                                            <asp:ListItem Value="1">As of Date</asp:ListItem>
                                            <asp:ListItem Value="2">Since Date</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td  colspan="4" align="center">
                            <asp:Button ID="btnGenerate" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr>
                        <td  valign="bottom">
                            <asp:HiddenField ID="hfSEC_ID" runat="server" />
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgBtnEnqDate"
                    TargetControlID="txtFrom">
                </ajaxToolkit:CalendarExtender>
            </div>
        </div>
    </div>
</asp:Content>

