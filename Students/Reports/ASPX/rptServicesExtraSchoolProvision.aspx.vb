﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptServicesExtraSchoolProvision
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If (Request.QueryString("MainMnu_code") IsNot Nothing) Then
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200453") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'Session("sBsuid") = "131001"
                    'Session("sUsr_name") = "arun.g"
                    BindBusinessUnit()
                    'bind_Academic_year()
                    'BindGrade()
                    BindServiceCategory()
                    BindApprovalStages()
                    'BindServices()
                    'BindTerms()
                End If

                Dim li As Telerik.Web.UI.RadComboBoxItem = New Telerik.Web.UI.RadComboBoxItem("--ALL--", "")
                ddlTerms.Items.Insert(0, li)
            Else
                Response.Redirect("~\noAccess.aspx")
            End If
        End If
    End Sub
    Sub BindBusinessUnit()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.Text, "select BSU_ID , BSU_NAME from [fn_GetBusinessUnits] " _
        & " ('" & Session("sUsr_name") & "') WHERE ISNULL(BSU_bSHOW,1)=1 order by BSU_NAME")
        ddlBUnit.DataSource = ds.Tables(0)
        ddlBUnit.DataValueField = "BSU_ID"
        ddlBUnit.DataTextField = "BSU_NAME"
        ddlBUnit.DataBind()
        ddlBUnit.SelectedIndex = -1
        If ddlBUnit.Items.Count > 0 Then
            Dim di As Telerik.Web.UI.RadComboBoxItem = New Telerik.Web.UI.RadComboBoxItem("--ALL--", "")
            ddlBUnit.Items.Add(di)
        End If
        If Not ddlBUnit.Items.FindItemByValue(Session("sBsuid")) Is Nothing Then
            ddlBUnit.Items.FindItemByValue(Session("sBsuid")).Selected = True 'FindByValue
        End If
        bind_Academic_year()
    End Sub
    Sub bind_Academic_year()
        If ddlBUnit.SelectedValue <> "" Then
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@BSU_ID", ddlBUnit.SelectedValue)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETACADEMICYEAR_BSU]", param)
            Dim CURRENT As String = ""
            If ds IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 1 Then
                    CURRENT = ds.Tables(0).Rows(0)("CURRENT_ACD")
                End If

                ddl_acdYear.DataSource = ds.Tables(0)
                ddl_acdYear.DataTextField = "ACY_DESCR"
                ddl_acdYear.DataValueField = "ACD_ID"
                ddl_acdYear.DataBind()

                If Not ddl_acdYear.Items.FindItemByValue(CURRENT) Is Nothing Then
                    ddl_acdYear.Items.FindItemByValue(CURRENT).Selected = True 'FindByValue
                End If
                BindGrade()
            End If
        End If
    End Sub
    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddl_acdYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddl_acdYear.SelectedItem.Value
            End If
            ' Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                ' di = New ListItem("ALL", "0")
                Dim di As Telerik.Web.UI.RadComboBoxItem = New Telerik.Web.UI.RadComboBoxItem("--ALL--", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New Telerik.Web.UI.RadComboBoxItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindGrade()
        If ddl_acdYear.SelectedValue <> "" Then
            callGrade_ACDBind()
            'Dim str_Sql As String
            'Dim ds As New DataSet
            'str_Sql = "select DISTINCT GRD_ID,GRD.GRD_DESCR,GRD_DISPLAYORDER  from GRADE_M GRD inner join GRADE_BSU_M  GRM on GRM.GRM_GRD_ID= GRD.GRD_ID WHERE(GRM.GRM_ACD_ID = '" + ddl_acdYear.SelectedValue + "') ORDER BY GRD.GRD_DISPLAYORDER"

            'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            'ddlgrade.DataTextField = "GRD_ID"
            'ddlgrade.DataValueField = "GRD_ID"
            'ddlgrade.DataSource = ds.Tables(0)
            'ddlgrade.DataBind()
            BindServices()
        End If
    End Sub
    Sub BindServiceCategory()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        'param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSERVICES_CATEGORY]", param)
        ddlSVCategory.DataSource = ds.Tables(0)
        ddlSVCategory.DataTextField = "SSC_DESC"
        ddlSVCategory.DataValueField = "SSC_ID"
        ddlSVCategory.DataBind()
        'Dim li As Telerik.Web.UI.RadComboBoxItem = New Telerik.Web.UI.RadComboBoxItem("--ALL--", "0")
        'ddlSVCategory.Items.Insert(0, li)
        BindServices()
    End Sub
    Sub BindServices()
        If ddlgrade.SelectedValue <> "" And ddl_acdYear.SelectedValue <> "" And ddlSVCategory.SelectedValue <> "" Then
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SSC_ID", ddlSVCategory.SelectedValue)
            param(1) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            param(2) = New SqlClient.SqlParameter("@GRD_ID", ddlgrade.SelectedValue)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSERVICES]", param)

            ddlServices.DataTextField = "SVC_DESCRIPTION"
            ddlServices.DataValueField = "SVC_ID"
            ddlServices.DataSource = ds.Tables(0)
            ddlServices.DataBind()
            Dim li As Telerik.Web.UI.RadComboBoxItem = New Telerik.Web.UI.RadComboBoxItem("--ALL--", "0")
            ddlServices.Items.Insert(0, li)
            BindTerms()
        End If
    End Sub
    Sub BindTerms()
        If ddlServices.SelectedValue <> "" And ddl_acdYear.SelectedValue <> "" And ddlgrade.SelectedValue <> "0" Then
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SVC_ID", ddlServices.SelectedValue)
            param(1) = New SqlClient.SqlParameter("@ACD_ID", ddl_acdYear.SelectedValue)
            param(2) = New SqlClient.SqlParameter("@GRD_ID", ddlgrade.SelectedValue)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GET_TERMS]", param)
            ddlTerms.DataSource = ds.Tables(0)
            ddlTerms.DataTextField = "TRM_DESCRIPTION"
            ddlTerms.DataValueField = "STM_ID"
            ddlTerms.DataBind()
            Dim li As Telerik.Web.UI.RadComboBoxItem = New Telerik.Web.UI.RadComboBoxItem("--ALL--", "")
            ddlTerms.Items.Insert(0, li)
         
        End If
    End Sub
    Sub BindApprovalStages()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
        CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVAL_STAGES]")
        ddlSvApprovalStage.DataTextField = "SAS_DESC"
        ddlSvApprovalStage.DataValueField = "SAS_CODE"
        ddlSvApprovalStage.DataSource = ds.Tables(0)
        ddlSvApprovalStage.DataBind()
        Dim li As Telerik.Web.UI.RadComboBoxItem = New Telerik.Web.UI.RadComboBoxItem("--ALL--", "")

        ddlSvApprovalStage.Items.Insert(0, li)
      
    End Sub

    
    Protected Sub ddl_acdYear_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddl_acdYear.SelectedIndexChanged
        BindGrade()
    End Sub
    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlBUnit.SelectedIndexChanged
        bind_Academic_year()
    End Sub
    Protected Sub ddlgrade_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlgrade.SelectedIndexChanged
        BindServices()
    End Sub
    Protected Sub ddlServices_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlServices.SelectedIndexChanged
        BindTerms()
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If Page.IsValid Then
            CallReport()
        End If
    End Sub
    Sub CallReport()
      
        'Dim param(10) As SqlClient.SqlParameter

        'param(0) = New SqlClient.SqlParameter("@BSU_ID ", IIf(ddlBUnit.SelectedValue <> "", ddlBUnit.SelectedValue, DBNull.Value))
        'param(1) = New SqlClient.SqlParameter("@ACD_ID ", IIf(ddl_acdYear.SelectedValue <> "", ddl_acdYear.SelectedValue, DBNull.Value))
        'param(2) = New SqlClient.SqlParameter("@GRD_ID ", IIf(ddlgrade.SelectedValue <> "0", ddlgrade.SelectedValue, DBNull.Value))
        'param(3) = New SqlClient.SqlParameter("@SVC_ID ", IIf(ddlServices.SelectedValue <> "", ddlServices.SelectedValue, DBNull.Value))
        'param(4) = New SqlClient.SqlParameter("@STM_ID ", IIf(ddlTerms.SelectedValue <> "", ddlTerms.SelectedValue, DBNull.Value))
        'param(5) = New SqlClient.SqlParameter("@SAS_ID ", IIf(ddlSvApprovalStage.SelectedValue <> "0", ddlSvApprovalStage.SelectedValue, DBNull.Value))
        'param(6) = New SqlClient.SqlParameter("@SSC_ID ", IIf(ddlSVCategory.SelectedValue <> "", ddlSVCategory.SelectedValue, DBNull.Value))
        'param(7) = New SqlClient.SqlParameter("@STU_NO ", IIf(txtStudNo.Text <> "", txtStudNo.Text, DBNull.Value))
        'param(8) = New SqlClient.SqlParameter("@STU_NAME ", IIf(txtStudName.Text <> "", txtStudName.Text, DBNull.Value))
        'param(9) = New SqlClient.SqlParameter("@MAINMNU_CODE ", ViewState("MainMnu_code"))

        'Dim ds As New DataSet
        'ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[RPT_GET_STUSVC_DETAILS]", param)




        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", IIf(ddlBUnit.SelectedValue <> "", ddlBUnit.SelectedValue, Session("sbsuid")))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", IIf(ddlBUnit.SelectedValue <> "", ddlBUnit.SelectedValue, DBNull.Value))
        param.Add("@ACD_ID", IIf(ddl_acdYear.SelectedValue <> "", ddl_acdYear.SelectedValue, DBNull.Value))
        param.Add("@GRD_ID", IIf(ddlgrade.SelectedValue <> "0", ddlgrade.SelectedValue, DBNull.Value))
        param.Add("@SVC_ID", IIf(ddlServices.SelectedValue <> "0", ddlServices.SelectedValue, DBNull.Value))
        param.Add("@SSC_ID", IIf(ddlSVCategory.SelectedValue <> "", ddlSVCategory.SelectedValue, DBNull.Value))
        param.Add("@STM_ID", IIf(ddlTerms.SelectedValue <> "", ddlTerms.SelectedValue, DBNull.Value))
        param.Add("@SAS_ID", IIf(ddlSvApprovalStage.SelectedValue <> "", IIf(ddlSvApprovalStage.SelectedValue = "N", 0, 1), DBNull.Value))
        param.Add("@STU_NO", IIf(txtStudNo.Text <> "", txtStudNo.Text, DBNull.Value))
        param.Add("@STU_NAME", IIf(txtStudName.Text <> "", txtStudName.Text, DBNull.Value))
        param.Add("@FROMDATE", IIf(txtfromdate.Text <> "", txtfromdate.Text, DBNull.Value))
        param.Add("@TODATE", IIf(txtfromdate.Text <> "", txttodate.Text, DBNull.Value))


        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptServicesExtraSchoolProvision.rpt")

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
