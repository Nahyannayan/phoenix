<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptstudAudit.aspx.vb" Inherits="Transport_Reports_Aspx_rptstudAudit"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">



        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../../../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../../../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../../../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../../../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../../../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../../../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
            document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
        }

        function test2(val) {
            var path;
            if (val == 'LI') {
                path = '../../../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../../../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../../../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../../../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../../../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid2()%>").src = path;
            document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
        }



        function test7(val) {
            var path;
            if (val == 'LI') {
                path = '../../../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../../../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../../../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../../../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../../../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../../../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid7()%>").src = path;
            document.getElementById("<%=h_selected_menu_7.ClientID %>").value = val + '__' + path;
        }


        function test8(val) {
            var path;
            if (val == 'LI') {
                path = '../../../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../../../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../../../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../../../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../../../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../../../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid8()%>").src = path;
            document.getElementById("<%=h_selected_menu_8.ClientID %>").value = val + '__' + path;
        }


    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--   <tr >
            <td width="50%" align="left" class="title" style="height: 50px">
                
            </td>
        </tr>--%>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table id="tblTPT" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  
                                    <td align="left" width="20%"><span class="field-label">Select Curriculum</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                      <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Select Academic Year</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                      <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Select Grade</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Select Section</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Student ID</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStuNo" runat="server">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Student Name</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    </td>
                                      </tr>
                                        <tr>
                                    <td colspan="4" align="center" >
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnRowEditing="gvStud_RowEditing"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" OnRowDataBound="gvStud_RowDataBound">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShfId" runat="server" Text='<%# Bind("Stu_shf_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <HeaderTemplate>

                                                        <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>

                                                        <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchStuNo_Click" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle ></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label>
                                                             <br />
                                                                                <asp:TextBox ID="txtStuName" runat="server" ></asp:TextBox>
                                                                           
                                                                                <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                                    OnClick="btnSearchStuName_Click" />
                                                                           
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle ></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                                               <br />
                                                                                <asp:TextBox ID="txtGrade" runat="server" ></asp:TextBox>
                                                                           
                                                                                    <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                                        OnClick="btnGrade_Search_Click" />
                                                                              
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblH123" runat="server" CssClass="gridheader_text" Text="Section"></asp:Label>
                                                               <br />
                                                                                <asp:TextBox ID="txtSection" runat="server" ></asp:TextBox>
                                                                         
                                                                                    <asp:ImageButton ID="btnSection_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif"
                                                                                        OnClick="btnSection_Search_Click" />
                                                                               
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="edit" Text="Print" HeaderText="Print">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                </asp:ButtonField>
                                                <asp:ButtonField CommandName="excelDownload" Text="Excel" HeaderText="Export">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                </asp:ButtonField>
                                            </Columns>
                                            <SelectedRowStyle  />
                                            <HeaderStyle  />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                            <asp:HiddenField ID="hfACD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSCT_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfGRD_ID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfSTUNO" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hfNAME" runat="server"></asp:HiddenField>
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>


    <%--  <div id="dropmenu1" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test1('LI');">
            <img alt="Any where" class="img_left" src="../../../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test1('NLI');">
                <img alt="Not In" class="img_left" src="../../../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test1('SW');">
                    <img alt="Starts With" class="img_left" src="../../../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test1('NSW');">
                        <img alt="Like" class="img_left" src="../../../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test1('EW');">
                            <img alt="Like" class="img_left" src="../../../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test1('NEW');">
                                <img alt="Like" class="img_left" src="../../../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>
    <div id="dropmenu2" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test2('LI');">
            <img alt="Any where" class="img_left" src="../../../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test2('NLI');">
                <img alt="Not In" class="img_left" src="../../../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test2('SW');">
                    <img alt="Starts With" class="img_left" src="../../../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test2('NSW');">
                        <img alt="Like" class="img_left" src="../../../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test2('EW');">
                            <img alt="Like" class="img_left" src="../../../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test2('NEW');">
                                <img alt="Like" class="img_left" src="../../../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>
    <div id="dropmenu7" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test8('LI');">
            <img alt="Any where" class="img_left" src="../../../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test8('NLI');">
                <img alt="Not In" class="img_left" src="../../../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test8('SW');">
                    <img alt="Starts With" class="img_left" src="../../../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test8('NSW');">
                        <img alt="Like" class="img_left" src="../../../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test8('EW');">
                            <img alt="Like" class="img_left" src="../../../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test8('NEW');">
                                <img alt="Like" class="img_left" src="../../../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>
    <div id="dropmenu8" class="dropmenudiv" style="width: 110px">
        <a href="javascript:test8('LI');">
            <img alt="Any where" class="img_left" src="../../../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test8('NLI');">
                <img alt="Not In" class="img_left" src="../../../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test8('SW');">
                    <img alt="Starts With" class="img_left" src="../../../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test8('NSW');">
                        <img alt="Like" class="img_left" src="../../../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test8('EW');">
                            <img alt="Like" class="img_left" src="../../../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test8('NEW');">
                                <img alt="Like" class="img_left" src="../../../Images/operations/notendswith.gif" />Not
                                Ends With</a>
    </div>
    <script type="text/javascript">


        cssdropdown.startchrome("Div1")
        cssdropdown.startchrome("Div2")
        cssdropdown.startchrome("Div3")
        cssdropdown.startchrome("Div4")


    </script>--%>
</asp:Content>
