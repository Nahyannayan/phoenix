<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptELL_REG_EN_Details.aspx.vb" Inherits="Students_rptELL_REG_EN_Details"
    Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script type="text/javascript">
        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click(); //fire the click event of the child element
                }
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="ELL REG / ENROLL DETAILS"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table align="center" style="width: 100%;">
                                <tr>
                                    <td align="left"><span class="field-label">Type</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:RadioButton ID="rbEnq" runat="server" CssClass="field-label" Text="Registered" GroupName="ell" AutoPostBack="true" />
                                        <asp:RadioButton ID="rbEnroll" GroupName="ell" CssClass="field-label" AutoPostBack="true" runat="server"
                                            Text="Enrolled" />
                                    </td>
                                    <td align="left"><span class="field-label">Academic year</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Category</span>
                                    </td>
                                    <td align="left" style="text-align: left" id="tdCur" runat="server">
                                        <asp:DropDownList ID="ddlCurriculum" runat="server" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                    <td align="left" id="td1" runat="server" visible="false"><span class="field-label">Course</span>
                                    </td>
                                    <td align="left" style="text-align: left" id="td3" runat="server" visible="false">
                                        <asp:DropDownList ID="ddlCourse" runat="server" AutoPostBack="true">
                                            <asp:ListItem Value="0">All</asp:ListItem>
                                            <asp:ListItem Value="1">Arabic A</asp:ListItem>
                                            <asp:ListItem Value="2">Arabic B</asp:ListItem>
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Grade</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Section</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Main Location</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlLocM" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Sub Location</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlLocS" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Block Filter</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="ddlBlock" runat="server" RepeatDirection="Horizontal">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td align="left"><span class="field-label">Student name</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtSname" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Contact Timings</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlContacttime" runat="server"></asp:DropDownList>
                                    </td>
                                    <td align="left"><span class="field-label">Reg. Date</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtRegDT" runat="server" Width="180px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Reg No /Student No</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtRegNo" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Passport no</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtPassNo" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Parent name</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtPNAME" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Parent mobile No</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtPMOB_NO" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Parent email address</span>
                                    </td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtPEMAIL" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search" ValidationGroup="dayBook" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="30%">
                                        <asp:Button ID="btnGen_detail" runat="server" CssClass="button" Text="Generate Detail Report" />
                                        <asp:Button ID="btnGen_List" runat="server" CssClass="button" Text="Generate List" />
                                        <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export List" />
                                    </td>
                                    <td width="20%">
                                        <asp:CheckBox CssClass="field-label" ID="chkLoc" runat="server" Text="Change Location" AutoPostBack="true" />
                                    </td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlLoc" runat="server" Visible="false">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="10%">
                                        <asp:Button ID="btnUpdateLoc" runat="server" CssClass="button"
                                            Text="Update Location" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <%-- <div style="color:Red">Select the record(s) to be printed before clicking on the Generate Report button !!!</div>--%>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Width="100%" Height="400px">

                                <asp:GridView ID="gvELL" runat="server" CssClass="table table-bordered table-row"
                                    AutoGenerateColumns="False" EmptyDataText="No Record Available " Width="100%" PageSize="20">
                                    <RowStyle CssClass="griditem" Height="20px" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Select">
                                            <EditItemTemplate>
                                                &nbsp;
                                            </EditItemTemplate>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAll" runat="server" Visible="true" ToolTip="Click here to select/deselect all rows"
                                                    onclick="javascript:change_chk_state(this);"></asp:CheckBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ch1" runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" Font-Bold="True"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sr.No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSrNo" runat="server" Text='<%# bind("r1") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Loc">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoc" runat="server" Text='<%# bind("Loc") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reg. Date">
                                            <EditItemTemplate>
                                                &nbsp;
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("ENQ_DATE") %>' __designer:wfdid="w12"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reg / Stud No">
                                            <EditItemTemplate>
                                                &nbsp;
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAPPLNO" runat="server" Text='<%# Bind("APPLNO") %>' __designer:wfdid="w94"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Student Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblSNAME" runat="server" Text='<%# Bind("SNAME") %>' __designer:wfdid="w92" CommandName="Select" CommandArgument='<%# Eval("r1") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Parent Name">
                                            <EditItemTemplate>
                                                &nbsp;
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPName" runat="server" Text='<%# bind("PName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Parent Mob No">
                                            <ItemTemplate>
                                                <asp:Label ID="LBLPMOB_NO" runat="server" Text='<%# BIND("PMOB_NO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Parent Email Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPEMAIL" runat="server" Text='<%# BIND("PEMAIL") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Weeks" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeeks" runat="server" Text='<%# bind("Weeks") %>' __designer:wfdid="w36"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SL_ID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSL_ID" runat="server" Text='<%# bind("SL_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Block1">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblBlock1Header" runat="server" Text="Block1"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBlock1" runat="server" Text='<%# BIND("BLOCK1") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Block2">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblBlock2Header" runat="server" Text="Block2"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBlock2" runat="server" Text='<%# BIND("BLOCK2") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Block3">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblBlock3Header" runat="server" Text="Block3"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBlock3" runat="server" Text='<%# BIND("BLOCK3") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Block4">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblBlock4Header" runat="server" Text="Block4"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblBlock4" runat="server" Text='<%# BIND("BLOCK4") %>'></asp:Label>
                                                <asp:HiddenField ID="hfTotalFee" runat="server" Value='<%# Bind("TotalFee")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Paid Amount">
                                            <EditItemTemplate>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaid" runat="server" Text='<%# Bind("Paid", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle BackColor="Wheat" />
                                    <HeaderStyle CssClass="gridheader_pop" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="plCourseDetails" runat="server" CssClass="darkPanelM" Visible="false">
                    <div class="darkPanelMTop" style="width: 660px!important; height: 360px !important;">
                        <div style="float: right; vertical-align: top;">
                            <asp:ImageButton ID="imgCourseDetailsClose" runat="server" ToolTip="click here to close" ImageUrl="~/Images/Common/PageBody/close.png" />
                        </div>
                        <div style="color: #03578d; font-size: 14pt; font-family: Verdana, Arial, Helvetica, sans-serif; border-bottom: dotted 1px #084777;">
                            &nbsp;Edit Block(s)
                        </div>
                        <div style="padding-top: 4px;">
                            <div class="holderInner">
                                <div style="padding-left: 20%;">
                                    <table class="tableNoborder">
                                        <tr align="center">
                                            <td align="center">
                                                <table width="100%" cellpadding="3" cellspacing="0" class="BlueTable_simple">
                                                    <tr class="tdblankAll">
                                                        <td class="tdblankAll" style="margin: 0px; padding: 0px;">

                                                            <asp:GridView ID="gvFeeDetails" runat="server"
                                                                AutoGenerateColumns="False"
                                                                EmptyDataText="No Dates Available !!!"
                                                                Width="100%" CssClass="gridstyle" BorderStyle="None" BorderWidth="0px"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <RowStyle CssClass="griditem" Height="20px" />
                                                                <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Blocks">
                                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblWeek" runat="server" Text='<%# Bind("TF_WEEK_DESCR") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="From Date">
                                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTF_STARTDATE" runat="server" Text='<%# Bind("TF_STARTDATE") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="To Date">
                                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTF_ENDDATE" runat="server" Text='<%# Bind("TF_ENDDATE") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>



                                                                    <asp:TemplateField HeaderText="Fees">
                                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTF_FEE_AMT" runat="server" Text='<%# Bind("TF_FEE_AMT") %>'></asp:Label>&nbsp;<asp:Literal
                                                                                ID="ltAED" runat="server" Text="AED"></asp:Literal>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>



                                                                    <asp:TemplateField HeaderText="Select">

                                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect" runat="server" Enabled='<%# Bind("TF_bFLAG") %>'
                                                                                Checked='<%# Bind("bSELECTED") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>



                                                                    <asp:TemplateField HeaderText="TF_ID" Visible="False">

                                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTF_STATUS" runat="server" Text='<%# Bind("TF_STATUS") %>'></asp:Label>
                                                                            <asp:Label ID="lblTF_ID" runat="server" Text='<%# Bind("TF_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <SelectedRowStyle BackColor="Wheat" />
                                                                <HeaderStyle CssClass="gridheader_pop" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                            </asp:GridView>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="width: 100%!important; display: none;">
                                                                <font color="red">*</font>Block 3 is scheduled for 9 days to finish before the expected first day of Eid Al Fitr. Timing will be adjusted to provide 3 hours and 20 minutes of teaching per day. 
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <div style="text-align: center; width: 100%; border-top: dotted 1px #084777; padding-top: 12px;">
                                                                <asp:Button ID="btnUpdateCourse" runat="server" CssClass="button" Text="Update" CausesValidation="false" />
                                                                <asp:Button ID="btnSend" runat="server" CssClass="button" Text="Resend Email" CausesValidation="false" />
                                                                <asp:Button ID="btnCloseCourse" runat="server" CssClass="button" Text="Close" CausesValidation="false" />
                                                            </div>


                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:Label ID="lblPOPUPMsg" runat="server"></asp:Label>

                            </div>
                        </div>

                    </div>

                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>
