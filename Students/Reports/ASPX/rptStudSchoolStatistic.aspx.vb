Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Students_Reports_ASPX_rptAdmissionDetail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "S200041") And (ViewState("MainMnu_code") <> "S200047")) Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                bindAcademic_SHIFT()


            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Sub bindAcademic_SHIFT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = Session("sBsuid")
            

            Dim ds As New DataSet
            str_Sql = " SELECT distinct SHF_ID as SHF_ID , SHF_DESCR FROM  SHIFTS_M where shf_bsu_id='" & BSU_ID & "' order by SHF_DESCR"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlShift.Items.Clear()
            ddlShift.DataSource = ds.Tables(0)
            ddlShift.DataTextField = "SHF_DESCR"
            ddlShift.DataValueField = "SHF_ID"
            ddlShift.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlShift.Items.Add(New ListItem("All", "0"))
                ddlShift.ClearSelection()
                ddlShift.Items.FindByText("All").Selected = True
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            Dim Str_ValidateDate As String = String.Empty
            Str_ValidateDate = ValidateDate()
            If Str_ValidateDate = "" Then
             
                CallReport()
            

            End If



        End If

    End Sub

    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim FromTo_Date As String = ""
        Dim i As Integer = 0

        Dim str_bsu_ids As New StringBuilder



        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        'param.Add("@ToDT", Format(Date.Parse(txtToDate.Text), "dd-MM-yyyy"))
        'param.Add("@FROMDT", Format(Date.Parse(txtFromDate.Text), "dd-MM-yyyy"))


        param.Add("@ToDT", Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd"))
        param.Add("@FROMDT", Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd"))

        param.Add("@ACD_ID", Session("Current_ACD_ID"))
        param.Add("@ACD_CLM_ID", Session("CLM"))
        If ddlShift.SelectedValue = "0" Then
            param.Add("@SHF_ID", Nothing)
        Else
            param.Add("@SHF_ID", ddlShift.SelectedValue)
        End If


        param.Add("Todt", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
        param.Add("Fromdt", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))


        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        'param.Add("To_Date", txtToDate.Text)
        'param.Add("From_Date", txtFromDate.Text)
        param.Add("UserName", Session("sUsr_name"))

        ''code added for describceny report in the report footer
        param.Add("@STA_BSU_ID", Session("sbsuid"))
        param.Add("@FROMDATE", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        param.Add("@TODATE", Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy"))
        Session("rptdesFROMDATE") = Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy")
        Session("rptdesTODATE") = Format(Date.Parse(txtToDate.Text.Trim), "dd/MMM/yyyy")
        ''end

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("../RPT/rptStud_SchoolStat.rpt")
            If ViewState("MainMnu_code") = "S200041" Then
                .reportPath = Server.MapPath("../RPT/rptStud_SchoolStat.rpt")
            ElseIf ViewState("MainMnu_code") = "S200047" Then
                .reportPath = Server.MapPath("../RPT/rptStud_SchoolStat_ALL.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Function ValidateDate() As String
        Try

        
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDate.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDate.Text.Trim <> "" Then

                Dim strfDate As String = txtToDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDate.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDate.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return "-1"
        End Try

    End Function
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

   
End Class
