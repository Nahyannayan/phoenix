<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstudAbsent_Report.aspx.vb" Inherits="Students_Reports_ASPX_rptClasslistTemplate" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Absent/Late  Report"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%" style="text-align: left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2" style="text-align: left">
                                        <asp:RadioButton ID="radSingle" CssClass="field-label" runat="server" Text="Single Grade" AutoPostBack="True" GroupName="Grade" OnCheckedChanged="radSingle_CheckedChanged" Checked="True"></asp:RadioButton>
                                        <asp:RadioButton ID="radMultiple" CssClass="field-label" runat="server" Text="MultipleGrade" AutoPostBack="True" GroupName="Grade" OnCheckedChanged="radMultiple_CheckedChanged"></asp:RadioButton></td>
                                </tr>
                                <tr id="rowGradSingle" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>
                                    <td align="left" style="text-align: left;">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="rowGrade" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Grade</span></td>
                                    <td align="left" style="text-align: left">
                                        <div align="left" style="overflow: auto;">
                                            <div class="checkbox-list">
                                                <asp:CheckBoxList ID="ddlGradesAll" CssClass="field-label" runat="server" AutoPostBack="True" ForeColor="Red"
                                                    OnSelectedIndexChanged="ddlGradesAll_SelectedIndexChanged1">
                                                </asp:CheckBoxList>
                                                <asp:CheckBoxList ID="ddlGrdaes" CssClass="field-label" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr id="trshf" runat="server">
                                    <td align="left" width="100%"><span class="field-label">Shift</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>


                                <tr id="rowSection" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Section</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="3">
                                        <asp:RadioButton ID="rbAbs" CssClass="field-label" runat="server" GroupName="gp" OnCheckedChanged="rbAbs_CheckedChanged" Text="Absent" AutoPostBack="True"></asp:RadioButton>
                                        <asp:RadioButton ID="rbLate" CssClass="field-label" runat="server" GroupName="gp" OnCheckedChanged="rbLate_CheckedChanged" Text="Late" AutoPostBack="True"></asp:RadioButton></td>
                                </tr>

                                <tr id="trType" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Filter By</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlFilterType" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                    <td align="left" width="30%" style="text-align: left">
                                        <asp:TextBox ID="txtfromDT" runat="server"></asp:TextBox><span> </span>
                                        <asp:ImageButton ID="imgFromDt" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnDob_date" TargetControlID="txtfromDT">
                                        </ajaxToolkit:CalendarExtender>
                                        <td align="left" width="20%"><span class="field-label">To Date</span></td>
                                        <td align="left" width="30%" style="text-align: left">
                                            <asp:TextBox ID="txtToDT" runat="server" Width="123px">
                                            </asp:TextBox><span style="font-size: 7pt"> </span>
                                            <asp:ImageButton ID="imgTODT" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                                PopupButtonID="imgBtnDob_date" TargetControlID="txtToDT">
                                            </ajaxToolkit:CalendarExtender>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2" style="text-align: right">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1"
                                runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDT" TargetControlID="txtFROMDT">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgTODT" TargetControlID="txtToDT">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

