﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptTCStatusConsolidated.aspx.vb" Inherits="Students_Reports_ASPX_rptTCStatusConsolidated"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
   
    <script language="javascript" type="text/javascript">
        function fnSelectAll_TFrom(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {

                    if (curr_elem.id.substring(0, 43) == 'ctl00_cphMasterpage_tabPopup_HT1_lstBsuFrom') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_TTo(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 41) == 'ctl00_cphMasterpage_tabPopup_HT1_lstBsuTo') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }


        function fnSelectAll_RC(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 41) == 'ctl00_cphMasterpage_tabPopup_HT3_lstBsuRC') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Summary(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 46) == 'ctl00_cphMasterpage_tabPopup_HT0_chkSummaryBSU') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Subject4(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {

                    if (curr_elem.id.substring(0, 44) == 'ctl00_cphMasterpage_tabPopup_HT4_lstSubject4') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }



        function fnSelectAll_Grade(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 41) == 'ctl00_cphMasterpage_tabPopup_HT2_lstGrade') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Month2(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 43) == 'ctl00_cphMasterpage_tabPopup_HT2_lstMonths2') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Month3(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 43) == 'ctl00_cphMasterpage_tabPopup_HT3_lstMonths3') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Month4(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 43) == 'ctl00_cphMasterpage_tabPopup_HT4_lstMonths4') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }

        function fnSelectAll_Group(master_box) {
            var curr_elem;
            var checkbox_checked_status;


            for (var i = 0; i < document.forms[0].elements.length; i++) {
                curr_elem = document.forms[0].elements[i];
                if (curr_elem.type == 'checkbox') //&& (curr_elem.id.substring(0, 28)=='ctl100_cphMasterpage_lstGroup')
                {
                    if (curr_elem.id.substring(0, 41) == 'ctl00_cphMasterpage_tabPopup_HT3_lstGroup') {
                        curr_elem.checked = master_box.checked;
                    }
                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            TC Status Consolidated
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="5">
                    <ajaxToolkit:TabPanel ID="HT0" runat="server">
                        <ContentTemplate>


                            <table id="tblSummaryReport" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr id="trSummary" runat="server">
                                    <td id="tdSummary" runat="server">
                                        <asp:ValidationSummary ID="vsSummaryDates" runat="server" CssClass="error" EnableViewState="False"
                                            ValidationGroup="groupM0" />
                                        <asp:RequiredFieldValidator ID="rfSummaryFrom" runat="server" ControlToValidate="txtSumFromDate"
                                            Display="None" ErrorMessage="Please choose the FROM date" SetFocusOnError="True"
                                            ValidationGroup="groupM0"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="rfSummaryTo" runat="server" ControlToValidate="txtSumToDate"
                                            Display="None" ErrorMessage="Please choose the TO date" SetFocusOnError="True"
                                            ValidationGroup="groupM0"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="Tr6" runat="server">
                                    <td id="Td2" align="left" runat="server">
                                        <table id="Table7" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr runat="server">
                                                <td id="Td20" align="left" runat="server" width="20%"></td>
                                                <td colspan="2" runat="server">
                                                    <span class="field-label">
                                                        <asp:RadioButtonList ID="rbtnBSUType" runat="server"
                                                            RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Text="Asian Schools" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="International Schools" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="All" Selected="True" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList></span>
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr id="tr7" runat="server">
                                                <td id="Td3" align="left" runat="server" width="20%">
                                                    <asp:Label ID="Label2" runat="server" Text="Business Unit" CssClass="field-label"></asp:Label>
                                                </td>
                                                <td id="Td4" align="left" colspan="2" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkSelectAllSummaryBSU" runat="server" Text="Select All " onclick="javascript:fnSelectAll_Summary(this);"></asp:CheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="checkbox-list">
                                                                    <asp:CheckBoxList ID="chkSummaryBSU" runat="server" RepeatLayout="Flow">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr runat="server" id="tr8">
                                                <td id="Td5" align="left" class="title-bg-lite" colspan="4" runat="server">Apply Date
                                                </td>
                                            </tr>
                                            <tr id="Tr9" runat="server">
                                                <td id="Td6" align="left" runat="server" width="20%">
                                                    <span class="field-label">From</span><span class="text-danger font-small">*</span>
                                                </td>
                                                <td id="Td7" align="left" runat="server" width="30%">
                                                    <asp:TextBox ID="txtSumFromDate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgSummFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>
                                                </td>
                                                <td id="Td8" align="left" runat="server" width="20%">
                                                    <span class="field-label">To</span><span class="text-danger font-small"> *</span>
                                                </td>
                                                <td id="Td9" align="left" runat="server" width="30%">
                                                    <asp:TextBox ID="txtSumToDate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgSummToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>

                                                </td>
                                            </tr>
                                            <tr id="Tr10" runat="server">
                                                <td id="Td10" colspan="4" align="center" runat="server">
                                                    <asp:Button ID="btnSummaryReport" OnClick="btnSummaryReport_Click"
                                                        runat="server" Text="Generate Report" CssClass="button"
                                                        TabIndex="4" ValidationGroup="groupM0" />&nbsp;
                                         <asp:Button ID="btnSummaryReportPDF" runat="server" Text="Download in PDF" CssClass="button"
                                             TabIndex="4" ValidationGroup="groupM0" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender0" runat="server"
                                Format="dd/MMM/yyyy" PopupButtonID="imgSummFromDate"
                                TargetControlID="txtSumFromDate" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender01" runat="server"
                                Format="dd/MMM/yyyy" PopupButtonID="imgSummToDate"
                                TargetControlID="txtSumToDate" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                        </ContentTemplate>
                        <HeaderTemplate>
                            TC Summary Report
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="HT2" runat="server" TabIndex="1">
                        <ContentTemplate>
                            <table id="tblGetTC" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="vsDates" runat="server" CssClass="error" EnableViewState="False"
                                            ValidationGroup="groupM2" />
                                        <asp:RequiredFieldValidator ID="rfFromDate" runat="server" ControlToValidate="txtFromDate"
                                            Display="None" ErrorMessage="Please choose the FROM date" SetFocusOnError="True"
                                            ValidationGroup="groupM2"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="rfToDate" runat="server" ControlToValidate="txtToDate"
                                            Display="None" ErrorMessage="Please choose the TO date" SetFocusOnError="True"
                                            ValidationGroup="groupM2"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <table id="tblStud" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr runat="server">
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2">
                                                    <span class="field-label">
                                                        <asp:RadioButtonList ID="rbtnBSUTypeTab2" runat="server"
                                                            RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Text="Asian Schools" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="International Schools" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="All" Selected="True" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList></span>
                                                </td>
                                                <td align="left" width="20%"></td>

                                            </tr>
                                            <tr id="trBSUforCorporate" runat="server">
                                                <td align="left" width="20%">
                                                    <asp:Label ID="lblBSU" runat="server" Text="Business Unit" CssClass="field-label"></asp:Label>
                                                </td>
                                                <td align="left" class="matters" colspan="2">
                                                    <asp:DropDownList ID="ddlBSUExit" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>

                                            <tr>
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2">
                                                    <asp:CheckBox ID="chkExclude" Text="Exclude Highest Grade" runat="server" CssClass="field-label" />
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%">
                                                    <asp:Label ID="lblEntryType" runat="server" Text="Choose Entry Type" CssClass="field-label"></asp:Label>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlEntryType" runat="server" AutoPostBack="True">
                                                        <asp:ListItem Text="All" Value="0" />
                                                        <asp:ListItem Text="Online" Value="1" />
                                                        <asp:ListItem Text="School" Value="2" />
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="30%"></td>

                                            </tr>
                                            <tr runat="server" id="trCLM">
                                                <td align="left" class="title-bg-lite" colspan="4">Apply Date
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%">
                                                    <span class="field-label">From</span><span class="text-danger font-small">*</span>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>
                                                </td>
                                                <td align="left" width="20%">
                                                    <span class="field-label">To</span><span class="text-danger font-small"> *</span>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Button ID="btnGenExit" runat="server" Text="Generate Report" CssClass="button"
                                                        TabIndex="4" CausesValidation="True" ValidationGroup="groupM2" />&nbsp;
                                        <asp:Button ID="btnDownLoadExit" runat="server" Text="Download in PDF" CssClass="button"
                                            TabIndex="4" CausesValidation="True" ValidationGroup="groupM2" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="CBEfdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                                TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="cbetdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgToDate"
                                TargetControlID="txtTodate">
                            </ajaxToolkit:CalendarExtender>
                        </ContentTemplate>
                        <HeaderTemplate>
                            Exit Interview Status
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="HT1" runat="server" TabIndex="2">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ValidationGroup="groupM1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtApplyStart"
                                Display="None" ErrorMessage="Please choose the FROM date" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtApplyEnd"
                                Display="None" ErrorMessage="Please choose the TO date" SetFocusOnError="True"
                                ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" runat="server" colspan="2">
                                        <span class="field-label">
                                            <asp:RadioButtonList ID="rbtnBSUTypeTab3a" runat="server"
                                                RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Text="Asian Schools" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="International Schools" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="All" Selected="True" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList></span>
                                    </td>
                                    <td id="Td1" align="left" runat="server" colspan="2">
                                        <span class="field-label">
                                            <asp:RadioButtonList ID="rbtnBSUTypeTab3b" runat="server"
                                                RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Text="Asian Schools" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="International Schools" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="All" Selected="True" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList></span>
                                    </td>
                                </tr>
                                <tr runat="server">
                                    <td align="left" runat="server">
                                        <span class="field-label">Transferred From School</span>
                                    </td>

                                    <td align="left" runat="server">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkTFrom" runat="server" Text="Select All " onclick="javascript:fnSelectAll_TFrom(this);"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox-list">
                                                        <asp:CheckBoxList ID="lstBsuFrom" runat="server" AutoPostBack="True" RepeatLayout="Flow">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="matters" align="left" runat="server">
                                        <span class="field-label">Transfer To School</span>
                                    </td>

                                    <td align="left" runat="server">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <span class="field-label">
                                                        <asp:RadioButton ID="rdAll" AutoPostBack="true" GroupName="g1" runat="server" Text="All Schools"
                                                            Checked="true" />
                                                        <asp:RadioButton ID="rdGems" AutoPostBack="true" GroupName="g1" runat="server" Text="GEMS Schools" />
                                                        <asp:RadioButton ID="rdNonGems" AutoPostBack="true" GroupName="g1" runat="server"
                                                            Text="Competitor Schools" /></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkTto" runat="server" Text="Select All " onclick="javascript:fnSelectAll_TTo(this);"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox-list">
                                                        <asp:CheckBoxList ID="lstBsuTo" runat="server" AutoPostBack="True" RepeatLayout="Flow">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>

                                    <td align="left" colspan="4">
                                        <asp:CheckBox ID="chkExcludeTransferSchool" Text="Exclude Highest Grade" runat="server" CssClass="field-label" />
                                    </td>

                                </tr>
                                <tr runat="server">
                                    <td align="left" runat="server">
                                        <span class="field-label">Apply Date From</span><span class="text-danger font-small">*</span>
                                    </td>

                                    <td runat="server" align="left">
                                        <asp:TextBox ID="txtApplyStart" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgApplyStart" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false;" />
                                    </td>
                                    <td runat="server" align="left">
                                        <span class="field-label">Apply Date To</span><span class="text-danger font-small">*</span>
                                    </td>

                                    <td runat="server" align="left">
                                        <asp:TextBox ID="txtApplyEnd" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgApplyEnd" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return false;" />
                                    </td>
                                </tr>
                                <tr runat="server">
                                    <td runat="server" align="center" colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" />
                                        <asp:Button ID="btnReportPDF" runat="server" CausesValidation="True" CssClass="button"
                                            TabIndex="4" Text="Download in PDF" ValidationGroup="groupM1" />
                                        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                                        </CR:CrystalReportSource>
                                        <asp:HiddenField ID="hfbDownload" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="calFromDate1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgApplyStart" TargetControlID="txtApplyStart" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calFromDate2" runat="server" Format="dd/MMM/yyyy"
                                TargetControlID="txtApplyStart" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calToDate1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgApplyEnd" TargetControlID="txtApplyEnd" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calToDate2" runat="server" Format="dd/MMM/yyyy"
                                TargetControlID="txtApplyEnd" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                        </ContentTemplate>
                        <HeaderTemplate>
                            TC Count By Transfer School
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>

                    <ajaxToolkit:TabPanel ID="HT3" runat="server" TabIndex="3">
                        <ContentTemplate>
                            <table id="Table4" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr runat="server">
                                    <td runat="server">
                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="error" EnableViewState="False"
                                            ValidationGroup="groupM3" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtRCStart"
                                            Display="None" ErrorMessage="Please choose the FROM date" SetFocusOnError="True"
                                            ValidationGroup="groupM3"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtRCEnd"
                                            Display="None" ErrorMessage="Please choose the TO date" SetFocusOnError="True"
                                            ValidationGroup="groupM3"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr runat="server">
                                    <td align="left" runat="server">
                                        <table id="Table5" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2" runat="server">
                                                    <span class="field-label">
                                                        <asp:RadioButtonList ID="rbtnBSUTypeTab4" runat="server"
                                                            RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Text="Asian Schools" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="International Schools" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="All" Selected="True" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList></span>
                                                </td>
                                                <td align="left" width="20%"></td>

                                            </tr>

                                            <tr id="tr3" runat="server">
                                                <td align="left" runat="server">
                                                    <asp:Label ID="Label3" runat="server" Text="Business Unit" CssClass="field-label"></asp:Label>
                                                </td>
                                                <td align="left" colspan="2" runat="server">
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkBsuRC" runat="server" Text="Select All " onclick="javascript:fnSelectAll_RC(this);"></asp:CheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="checkbox-list">
                                                                    <asp:CheckBoxList ID="lstBsuRC" runat="server" AutoPostBack="True" RepeatLayout="Flow">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr runat="server" id="trExclude">
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2">
                                                    <asp:CheckBox ID="chkExcludeCountByReason" Text="Exclude Highest Grade" runat="server" CssClass="field-label" />
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>

                                            <tr runat="server" id="tr4">
                                                <td align="left" class="title-bg-lite" colspan="4" runat="server">Apply Date
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td align="left" runat="server" width="20%">
                                                    <span class="field-label">From</span><span class="text-danger font-small">*</span>
                                                </td>
                                                <td align="left" runat="server" width="30%">
                                                    <asp:TextBox ID="txtRCStart" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgRCStart" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>
                                                </td>
                                                <td align="left" runat="server" width="20%">
                                                    <span class="field-label">To</span><span class="text-danger font-small"> *</span>
                                                </td>
                                                <td align="left" runat="server" width="30%">
                                                    <asp:TextBox ID="txtRCEnd" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgRCEnd" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td colspan="4" align="center" runat="server">
                                                    <asp:Button ID="btnRCGen" runat="server" Text="Generate Report" CssClass="button"
                                                        TabIndex="4" ValidationGroup="groupM3" />&nbsp;
                                        <asp:Button ID="btnRCDownload" runat="server" Text="Download in PDF" CssClass="button"
                                            TabIndex="4" ValidationGroup="groupM3" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                Format="dd/MMM/yyyy" PopupButtonID="imgRCStart"
                                TargetControlID="txtRCStart" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                Format="dd/MMM/yyyy" PopupButtonID="imgRCEnd"
                                TargetControlID="txtRCEnd" Enabled="True">
                            </ajaxToolkit:CalendarExtender>
                        </ContentTemplate>
                        <HeaderTemplate>
                            TC Count By Reason
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="HT4" runat="server" TabIndex="4">
                        <ContentTemplate>
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                            ValidationGroup="groupM4" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRSStart"
                                            Display="None" ErrorMessage="Please choose the FROM date" SetFocusOnError="True"
                                            ValidationGroup="groupM4"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtRSEnd"
                                            Display="None" ErrorMessage="Please choose the TO date" SetFocusOnError="True"
                                            ValidationGroup="groupM4"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold;" valign="top">
                                        <table id="Table3" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2">
                                                    <span class="field-label">
                                                        <asp:RadioButtonList ID="rbtnBSUTypeTab5" runat="server"
                                                            RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Text="Asian Schools" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="International Schools" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="All" Selected="True" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList></span>
                                                </td>
                                                <td align="left" width="20%"></td>

                                            </tr>
                                            <tr id="tr1" runat="server">
                                                <td align="left" width="20%">
                                                    <asp:Label ID="Label1" runat="server" Text="Business Unit" CssClass="field-label"></asp:Label>
                                                </td>
                                                <td align="left" colspan="2">
                                                    <asp:DropDownList ID="ddlBsuRS" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%">
                                                    <span class="field-label">Grade</span>
                                                </td>
                                                <td align="left" width="30%">
                                                    <asp:DropDownList ID="ddlGrade" runat="server"></asp:DropDownList>
                                                </td>
                                                <td align="left" width="30%"></td>
                                                <td align="left" width="20%"></td>

                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2">
                                                    <asp:CheckBox ID="chkExcludeListByReason" Text="Exclude Highest Grade" runat="server" CssClass="field-label" />
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr runat="server" id="tr2">
                                                <td align="left" class="title-bg-lite" colspan="4">Apply Date
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <span class="field-label">From</span><span class="text-danger font-small">*</span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRSStart" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgRSStart" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>
                                                </td>
                                                <td align="left">
                                                    <span class="field-label">To</span><span class="text-danger font-small"> *</span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRSEnd" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgRSEnd" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Button ID="btnRSGen" runat="server" Text="Generate Report" CssClass="button"
                                                        TabIndex="4" CausesValidation="True" ValidationGroup="groupM4" />&nbsp;
                                        <asp:Button ID="btnRSDownload" runat="server" Text="Download in PDF" CssClass="button"
                                            TabIndex="4" CausesValidation="True" ValidationGroup="groupM4" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgRSStart"
                                TargetControlID="txtRSStart">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgRSEnd"
                                TargetControlID="txtRSEnd">
                            </ajaxToolkit:CalendarExtender>
                        </ContentTemplate>
                        <HeaderTemplate>
                            TC List By Reason
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="HT5" runat="server" TabIndex="5">
                        <ContentTemplate>
                            <table id="Table8" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="vsStatusCompare" runat="server" CssClass="error" EnableViewState="False"
                                            ValidationGroup="groupM5" />
                                        <asp:RequiredFieldValidator ID="rfStatusCompareFrom" runat="server" ControlToValidate="txtStatusCompareFromDate"
                                            Display="None" ErrorMessage="Please choose the FROM date" SetFocusOnError="True"
                                            ValidationGroup="groupM5"></asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="rfStatusCompareTo" runat="server" ControlToValidate="txtStatusCompareToDate"
                                            Display="None" ErrorMessage="Please choose the TO date" SetFocusOnError="True"
                                            ValidationGroup="groupM5"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <table id="Table9" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2">
                                                    <span class="field-label">
                                                        <asp:RadioButtonList ID="rbtnBSUTypeTab6" runat="server"
                                                            RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Text="Asian Schools" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="International Schools" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="All" Selected="True" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList></span>
                                                </td>

                                                <td align="left" width="20%"></td>

                                            </tr>
                                            <tr id="tr5" runat="server">
                                                <td align="left" width="20%">
                                                    <asp:Label ID="lblStatusCompareBSU" runat="server" Text="Business Unit" CssClass="field-label"></asp:Label>
                                                </td>
                                                <td align="left" colspan="2">
                                                    <asp:DropDownList ID="ddlStatusCompareBSU" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" width="20%"></td>

                                            </tr>
                                            <tr>
                                                <td align="left" width="20%">
                                                    <span class="field-label">Grade</span>
                                                </td>
                                                <td width="30%">
                                                    <asp:DropDownList ID="ddlStatusCompareGrade" runat="server"></asp:DropDownList>
                                                </td>
                                                <td align="left" width="20%"></td>
                                                <td align="left" width="30%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2">
                                                    <asp:CheckBox ID="chkExcludeStatusCompare" Text="Exclude Highest Grade" runat="server" CssClass="field-label" />
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr runat="server" id="tr12">
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2">
                                                    <span class="field-label">
                                                        <asp:RadioButtonList ID="rbtnTCSOType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                            <asp:ListItem Text="TC" Value="0" Selected="true"></asp:ListItem>
                                                            <asp:ListItem Text="Strike Off" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="TC and Strike Off" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList></span>
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr runat="server" id="tr11">
                                                <td align="left" width="20%"></td>
                                                <td align="left" colspan="2">
                                                    <span class="field-label">
                                                        <asp:RadioButtonList ID="rbtnDateType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                            <asp:ListItem Text="Apply Date" Value="0" Selected="true"></asp:ListItem>
                                                            <asp:ListItem Text="Last Attendance Date" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Leaving Date" Value="2"></asp:ListItem>
                                                        </asp:RadioButtonList></span>
                                                </td>
                                                <td align="left" width="20%"></td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <span class="field-label">From</span><span class="text-danger font-small">*</span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtStatusCompareFromDate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgStatusCompareFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>
                                                </td>
                                                <td align="left">
                                                    <span class="field-label">To</span><span class="text-danger font-small"> *</span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtStatusCompareToDate" runat="server"></asp:TextBox>
                                                    <asp:ImageButton ID="imgStatusCompareToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                                        OnClientClick="return false;"></asp:ImageButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <asp:Button ID="btnStatusCompare" runat="server" Text="Generate Report" CssClass="button"
                                                        TabIndex="4" CausesValidation="True" ValidationGroup="groupM5" />
                                                    <asp:Button ID="btnStatusComparePDF" runat="server" Text="Download in PDF" CssClass="button"
                                                        TabIndex="4" CausesValidation="True" ValidationGroup="groupM5" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgStatusCompareFromDate"
                                TargetControlID="txtStatusCompareFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgStatusCompareToDate"
                                TargetControlID="txtStatusCompareToDate">
                            </ajaxToolkit:CalendarExtender>



                        </ContentTemplate>
                        <HeaderTemplate>
                            TC Status Comparison
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>


            </div>
        </div>
    </div>
</asp:Content>
