Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class rptstudPhotos
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200580") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights




                callYEAR_DESCRBind()



                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        ddlGrade.Items.Add(New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID")))
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click


        CallReport()


    End Sub
   
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim i As Integer = 0
        If h_STU_IDs.Value = "" Then
            h_STU_IDs.Value = GetAllStudentsInSection()
        End If
        ' Dim pParms(9) As SqlClient.SqlParameter
        ' pParms(0) = New SqlClient.SqlParameter("@IMG_BSU_ID", Session("sbsuid"))
        ' pParms(1) = New SqlClient.SqlParameter("@IMG_TYPE", "LOGO")
        '   pParms(2) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        If ddlGrade.SelectedItem.Text = "ALL" Then
            param.Add("@GRD_ID", Nothing)
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedValue)
            '  pParms(3) = New SqlClient.SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
        End If

        If ddlSection.SelectedItem.Text = "ALL" Then
            param.Add("@SCT_ID", Nothing)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
            '  pParms(4) = New SqlClient.SqlParameter("@SCT_ID", ddlSection.SelectedItem.Value)
        End If

        param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        ' pParms(5) = New SqlClient.SqlParameter("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        '  pParms(6) = New SqlClient.SqlParameter("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        '  pParms(7) = New SqlClient.SqlParameter("UserName", Session("sUsr_name"))
        Session("noimg") = "yes"
        Dim rptClass As New rptClass
        With rptClass
            .Photos = GetPhotoClass()
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptstud_photos.rpt")

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
        'Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[STU].GETSTUDENT_PHOTOS_2", param)

        'If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
        '    Dim dt As DataTable
        '    dt = ds.Tables(0)

        '    lblError.Text = dt.Rows(0)("stu_id").ToString & "-" & dt.Rows(0)("stu_no").ToString & "-" & dt.Rows(0)("sname").ToString & "-" & dt.Rows(0)("grm_display").ToString & "-" & dt.Rows(0)("sct_descr").ToString & "-" & dt.Rows(0)("stu_photopath").ToString & "-" & dt.Rows(0)("grd_displayorder").ToString & "-" & dt.Rows(0)("DOB").ToString

        'Else
        '    lblError.Text = "0"
        'End If
    End Sub
    Function GetAllStudentsInSection() As String
        Dim str_sql As String = String.Empty
        Dim acd_id As String = ddlAcademicYear.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim SCT_ID As String = String.Empty
        If ddlGrade.SelectedItem.Text = "ALL" Then
            GRD_ID = " AND STU_GRD_ID<>'' "
        Else
            GRD_ID = " AND STU_GRD_ID='" & ddlGrade.SelectedValue & "'"

        End If

        If ddlSection.SelectedItem.Text = "ALL" Then
            SCT_ID = " AND STU_SCT_ID<>'' "
        Else
            SCT_ID = " AND STU_SCT_ID=" & ddlSection.SelectedValue

        End If

        str_sql = " SELECT  ISNULL(STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
           " STUDENT_M WHERE STU_ACD_ID = " & acd_id & SCT_ID & GRD_ID & " AND STU_CURRSTATUS<>'CN' AND (CONVERT(datetime, STU_LASTATTDATE) >= CONVERT(datetime, getdate()) OR CONVERT(datetime, STU_LASTATTDATE) IS NULL)   for xml path('')),1,0,''),0) "




        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
    End Function


    Function GetPhotoClass() As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO
        Dim arrList As ArrayList = New ArrayList(h_STU_IDs.Value.Split("|"))
        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub
   
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
