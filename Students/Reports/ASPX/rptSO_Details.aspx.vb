Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Students_Reports_ASPX_rptSO_Details
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")


            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200060" And ViewState("MainMnu_code") <> "S200065" And ViewState("MainMnu_code") <> "S200070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                bindAcademic_Grade()
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            Dim Str_ValidateDate As String = String.Empty
            Str_ValidateDate = ValidateDate()
            If Str_ValidateDate = "" Then
                CallReport()
            End If
        End If

    End Sub
    Sub bindAcademic_Grade()
        Try
            Dim ACD_ID As String = String.Empty
            
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(Session("Current_ACD_ID"), Session("CLM"))
                ddlGrade.Items.Clear()

                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)

                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim FromTo_Date As String = ""
        Dim TC_Cancel As Boolean
        Dim TCSO As String = String.Empty
        Dim i As Integer = 0

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@bsu_id", Session("sbsuid"))
        param.Add("@option", ddlOption.SelectedItem.Value)
        If ViewState("MainMnu_code") = "S200060" Or ViewState("MainMnu_code") = "S200065" Then
            TC_Cancel = False
            param.Add("@tc_cancel", TC_Cancel)
            If ViewState("MainMnu_code") = "S200060" Then
                param.Add("@tc_so", "SO")
            ElseIf ViewState("MainMnu_code") = "S200065" Then
                param.Add("@tc_so", "TC")
            End If


        ElseIf ViewState("MainMnu_code") = "S200070" Then
            TC_Cancel = True
            param.Add("@tc_cancel", TC_Cancel)
            param.Add("@tc_so", "TC")
        End If


        param.Add("@date2", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
        param.Add("@date1", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
        param.Add("ToDt", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
        param.Add("FromDt", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)

        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If ViewState("MainMnu_code") = "S200060" Then 'SO Details
                .reportPath = Server.MapPath("../RPT/rptSO_Detail.rpt")
            ElseIf ViewState("MainMnu_code") = "S200065" Then 'TC Details
                .reportPath = Server.MapPath("../RPT/rptTC_Detail.rpt")
            ElseIf ViewState("MainMnu_code") = "S200070" Then 'Cancelled TC
                .reportPath = Server.MapPath("../RPT/rptCancelled_TC.rpt")
            End If

        End With
        Session("rptClass") = rptClass

        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDate.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDate.Text.Trim <> "" Then

                Dim strfDate As String = txtToDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDate.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDate.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class
