Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptHouseReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200055") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights

                callCurrent_BsuShift()
                callGrade_ACDBind()
                callGrade_Section()
                callBSU_HOUSE()
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Public Sub callCurrent_BsuShift()
        Try

            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), Session("Current_ACD_ID"))
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlShift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            CallReport()
        End If
    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim FromTo_Date As String = ""
        Dim i As Integer = 0

        'If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
        '    FromTo_Date = " AND STUDENT_M.STU_DOJ BETWEEN '" + Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd") + "'"
        'End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@Grade", Nothing)
        Else
            param.Add("@Grade", ddlGrade.SelectedItem.Value)
        End If

        If ddlSection.SelectedItem.Value = "0" Then
            param.Add("@Section", Nothing)
        Else
            param.Add("@Section", ddlSection.SelectedItem.Value)
        End If
        If ddlHouse.SelectedItem.Value = "0" Then
            param.Add("@House_ID", Nothing)
        Else
            param.Add("@House_ID", ddlHouse.SelectedItem.Value)
        End If

        param.Add("@ACCYEAR", Session("Current_ACD_ID"))
        Dim Current_Year As String = GetCurrentACY_DESCR(Session("Current_ACY_ID"))

        param.Add("Acad_Year", Current_Year)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If ViewState("MainMnu_code") = "S200055" Then
                .reportPath = Server.MapPath("../RPT/rptHouseReports.rpt")

            End If

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Function GetCurrentACY_DESCR(ByVal ACY_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_sql As String = "SELECT  [ACY_DESCR]  FROM [OASIS].[dbo].[ACADEMICYEAR_M]where [ACY_ID] = '" & ACY_ID & "'"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function
    Public Sub callGrade_ACDBind()
        Try

            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(Session("Current_ACD_ID"), Session("CLM"), ddlShift.SelectedValue)
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While
                End If
            End Using
            If ddlGrade.SelectedItem.Value <> 0 Then
                ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Public Sub callGrade_Section()
        Try

            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedItem.Value = "0" Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If

            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If

            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), Session("Current_ACD_ID"), GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callBSU_HOUSE()
        Try

            Dim di As ListItem
            Using BSU_HOUSEReader As SqlDataReader = AccessStudentClass.GetBSU_House((Session("sBsuid")))
                ddlHouse.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlHouse.Items.Add(di)
                If BSU_HOUSEReader.HasRows = True Then
                    While BSU_HOUSEReader.Read
                        di = New ListItem(BSU_HOUSEReader("ID"), BSU_HOUSEReader("HideID"))
                        ddlHouse.Items.Add(di)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub
    ' GetBSU_House(ByVal BSUID As String)

    Protected Sub ddlHouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlHouse.SelectedIndexChanged

    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        callGrade_ACDBind()
        callGrade_Section()
        callBSU_HOUSE()
    End Sub
End Class
