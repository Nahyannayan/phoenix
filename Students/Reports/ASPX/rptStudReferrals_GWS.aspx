﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudReferrals_GWS.aspx.vb" Inherits="Students_Reports_ASPX_rptStudReferrals_GWS" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Referrals List"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_ShowScreen" runat="server" width="100%">
                    <tr colspan="4">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>

                        <td align="left" class="matters"><span class="field-label">From Date</span>
                        </td>
                        <td align="left"  >
                                        <asp:TextBox ID="txtFrom" runat="server"  ></asp:TextBox>
                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgFrom" TargetControlID="txtFrom">
                                        </ajaxToolkit:CalendarExtender>
                        </td>


                        <td align="left" class="matters"><span class="field-label">To Date</span>
                        </td>
                        <td align="left">
                                        <asp:TextBox ID="txtTo" runat="server"  ></asp:TextBox>
                                        <asp:ImageButton ID="imgTo" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgTo" TargetControlID="txtTo">
                                        </ajaxToolkit:CalendarExtender>
                        </td>

                    </tr>
                    <tr>
                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnreport" runat="server" Text="Generate Report" class="button" />
                            <asp:Button ID="btnDownload" runat="server" Text="Download Report in Pdf" class="button" />
                        </td>
                    </tr>

                </table>

                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>

