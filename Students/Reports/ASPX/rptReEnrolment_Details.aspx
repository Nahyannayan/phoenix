<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptReEnrolment_Details.aspx.vb" Inherits="Students_rptELL_REG_EN_Details" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript">
        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Re Enrollment Details"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic year</span></td>
                                    <td align="left" style="text-align: left" width="30">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20"><span class="field-label">Grade</span></td>
                                    <td align="left" style="text-align: left" width="30">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Section</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">NBAD Card Request</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlCard" runat="server">
                                            <asp:ListItem Value="-1">All</asp:ListItem>
                                            <asp:ListItem Value="1">Requested</asp:ListItem>
                                            <asp:ListItem Value="0">Not Requested</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Re-Enrolment Status</span></td>
                                    <td align="left" style="text-align: left" colspan="3">
                                        <asp:RadioButton CssClass="field-label" ID="rbEnroll" runat="server" Text="Enrolled" GroupName="ell" />
                                        <asp:RadioButton CssClass="field-label" ID="rbNotEnroll" GroupName="ell" runat="server" Text="Not Enrolling" />
                                        <asp:RadioButton CssClass="field-label" ID="rbPending" GroupName="ell" runat="server" Text="Pending" />
                                        <asp:RadioButton CssClass="field-label" ID="rbRegPend" GroupName="ell" runat="server" Text="Registered - Payment Pending " />
                                        <asp:RadioButton CssClass="field-label" ID="rbBlocked" GroupName="ell" runat="server" Text="Blocked " />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Fee Paid By</span></td>
                                    <td align="left" style="text-align: left" colspan="3">
                                        <span class="field-label">
                                        <asp:CheckBoxList ID="chkFEE_SPONSOR" runat="server" RepeatDirection="Horizontal"
                                            RepeatColumns="4">
                                            <asp:ListItem Value="1">GEMS Staff</asp:ListItem>
                                            <asp:ListItem Value="2">Company</asp:ListItem>
                                            <asp:ListItem Value="3">Parent</asp:ListItem>
                                            <asp:ListItem Value="4">Verified GEMS Staff</asp:ListItem>
                                        </asp:CheckBoxList>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">From Date</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox><span style="font-size: 7pt"> </span>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><span style="font-size: 7pt">
                                        </span>
                                        <td align="left"><span class="field-label">To Date</span></td>
                                        <td align="left" style="text-align: left">
                                            <asp:TextBox ID="txtTodate" runat="server">
                                            </asp:TextBox><span style="font-size: 7pt"> </span>
                                            <asp:ImageButton ID="imgbtntdate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnGen_detail" runat="server" CssClass="button" Height="28px"
                                            Text="Generate Detail Report" /></td>
                                </tr>

                            </table>
                            <ajaxToolkit:CalendarExtender ID="CBEfdate"
                                runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="cbetdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgbtntdate"
                                TargetControlID="txtTodate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
</asp:Content>

