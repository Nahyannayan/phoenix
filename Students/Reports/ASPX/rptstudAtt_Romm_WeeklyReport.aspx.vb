Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptClasslistTemplate
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S298505" And ViewState("MainMnu_code") <> "S298510") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                If ViewState("MainMnu_code") = "S298505" Then
                    ltLabel.Text = "Weekly Attendance Pattern"
                Else
                    ltLabel.Text = "Period Wise Attendance Pattern"
                End If

                callYEAR_DESCRBind()

                rbPRS.Checked = True
                BindAbsent()




                txtToDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtfromDT.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    

    Sub BindAbsent()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim ACD_ID As String = ddlAcd.SelectedItem.Value
        Dim ds As New DataSet
        Dim BSU_ID As String = Session("sBsuid")

        If rbPRS.Checked = True Then

            str_Sql = " SELECT APD_ID,APD_PARAM_DESCR,APD_APM_ID FROM (" & _
 " SELECT  ROW_NUMBER() OVER(ORDER BY ATTENDANCE_PARAM_D.APD_ID) AS R1,  CAST(ATTENDANCE_PARAM_D.APD_ID AS VARCHAR)+'|0' AS [APD_ID],ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ isnull(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' AS [APD_PARAM_DESCR] ,ATTENDANCE_PARAM_D.APD_APM_ID " & _
 " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID " & _
 " WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "') AND (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) UNION " & _
 "SELECT ROW_NUMBER() OVER(ORDER BY RAP_ID)+100 AS R1,CAST(RAP_ID AS VARCHAR)+'|1' AS [APD_ID], RAP_PARAM_DESCR+'('+ ISNULL(RAP_DISP,'') +')' " & _
 " AS [APD_PARAM_DESCR] ,rap_apm_id  FROM ATT.ROOM_ATTENDANCE_PARAM_D WHERE RAP_BSU_ID='" & BSU_ID & "') A " & _
  " WHERE(APD_APM_ID = 1)"



        ElseIf rbABS.Checked = True Then
            str_Sql = " SELECT APD_ID,APD_PARAM_DESCR,APD_APM_ID FROM (" & _
 " SELECT  ROW_NUMBER() OVER(ORDER BY ATTENDANCE_PARAM_D.APD_ID) AS R1,  CAST(ATTENDANCE_PARAM_D.APD_ID AS VARCHAR)+'|0' AS [APD_ID],ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ isnull(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' AS [APD_PARAM_DESCR] ,ATTENDANCE_PARAM_D.APD_APM_ID " & _
 " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID " & _
 " WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "') AND (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) UNION " & _
 "SELECT ROW_NUMBER() OVER(ORDER BY RAP_ID)+100 AS R1,CAST(RAP_ID AS VARCHAR)+'|1' AS [APD_ID], RAP_PARAM_DESCR+'('+ ISNULL(RAP_DISP,'') +')' " & _
 " AS [APD_PARAM_DESCR] ,rap_apm_id  FROM ATT.ROOM_ATTENDANCE_PARAM_D WHERE RAP_BSU_ID='" & BSU_ID & "') A " & _
  " WHERE(APD_APM_ID = 2)"

        Else
            str_Sql = " SELECT APD_ID,APD_PARAM_DESCR,APD_APM_ID FROM (" & _
 " SELECT  ROW_NUMBER() OVER(ORDER BY ATTENDANCE_PARAM_D.APD_ID) AS R1,  CAST(ATTENDANCE_PARAM_D.APD_ID AS VARCHAR)+'|0' AS [APD_ID],ATTENDANCE_PARAM_D.APD_PARAM_DESCR +'('+ isnull(ATTENDANCE_REPORT_PARAM.ARP_DISP,'') +')' AS [APD_PARAM_DESCR] ,ATTENDANCE_PARAM_D.APD_APM_ID " & _
 " FROM ATTENDANCE_PARAM_D LEFT OUTER JOIN  ATTENDANCE_REPORT_PARAM ON ATTENDANCE_PARAM_D.APD_ID = ATTENDANCE_REPORT_PARAM.ARP_APD_ID " & _
 " WHERE (ATTENDANCE_PARAM_D.APD_BSU_ID = '" & BSU_ID & "') AND (ATTENDANCE_PARAM_D.APD_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_PARAM_D.APD_bSHOW = 1) UNION " & _
 "SELECT ROW_NUMBER() OVER(ORDER BY RAP_ID)+100 AS R1,CAST(RAP_ID AS VARCHAR)+'|1' AS [APD_ID], RAP_PARAM_DESCR+'('+ ISNULL(RAP_DISP,'') +')' " & _
 " AS [APD_PARAM_DESCR] ,rap_apm_id  FROM ATT.ROOM_ATTENDANCE_PARAM_D WHERE RAP_BSU_ID='" & BSU_ID & "') A " & _
  " WHERE(APD_APM_ID = 1) AND APD_PARAM_DESCR LIKE '%late%'"



        End If



        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlFilterType.Items.Clear()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlFilterType.DataSource = ds.Tables(0)
            ddlFilterType.DataTextField = "APD_PARAM_DESCR"
            ddlFilterType.DataValueField = "APD_ID"
            ddlFilterType.DataBind()
        End If

        ddlFilterType.Items.Add(New ListItem("ALL", "0"))
        ddlFilterType.Items.FindByText("ALL").Selected = True

    End Sub
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcd.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcd.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcd.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcd.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcd.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            ddlAcd_SelectedIndexChanged(ddlAcd, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcd.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcd.SelectedItem.Value
            End If


            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrd.Items.Clear()
              
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read

                        ddlGrd.Items.Add(New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID")))
                    End While

                End If
            End Using
            ddlGRD_SelectedIndexChanged(ddlGrd, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then


            If ValidateDate() = "" Then
                Dim todt As DateTime = txtToDT.Text
                Dim fromdt As DateTime = txtfromDT.Text

                Dim startdt As DateTime = ViewState("startDt")
                Dim enddt As DateTime = ViewState("endDt")

                If ((fromdt >= startdt) And (fromdt <= enddt)) And ((todt >= startdt) And (todt <= enddt)) Then
                    lblError.Text = ""
                    CallReport()
                Else
                    lblError.Text = "Date must be within the academic year!!!"
                End If

            End If

        End If

    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim FromTo_Date As String = ""
        Dim i As Integer = 0




        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcd.SelectedItem.Value)
        param.Add("@BSU_ID", Session("sbsuid"))
        param.Add("@GRD_ID", ddlGrd.SelectedValue)
        param.Add("@SGR_ID", ddlSubjectGroup.SelectedValue)

        If ddlFilterType.SelectedItem.Text = "ALL" Then
            param.Add("@PAR_ID", Nothing)
        Else
            param.Add("@PAR_ID", ddlFilterType.SelectedItem.Value)
        End If

        If rbPRS.Checked = True Then
            param.Add("@Type", "1")
        ElseIf rbABS.Checked = True Then
            param.Add("@Type", "2")
        ElseIf rbLATE.Checked = True Then
            param.Add("@Type", "3")
        End If

        param.Add("@FROMDT", Format(Date.Parse(txtfromDT.Text), "dd/MMM/yyyy"))
        param.Add("@TODT", Format(Date.Parse(txtToDT.Text), "dd/MMM/yyyy"))
        param.Add("Acad_Year", ddlAcd.SelectedItem.Text)
        param.Add("fromdt", Format(Date.Parse(txtfromDT.Text), "dd/MMM/yyyy"))
        param.Add("todt", Format(Date.Parse(txtToDT.Text), "dd/MMM/yyyy"))

        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        If rbPRS.Checked = True Then
            If ddlFilterType.SelectedItem.Text = "ALL" Then
                param.Add("titleHead", "Present")
            Else
                param.Add("titleHead", ddlFilterType.SelectedItem.Text)
            End If

        ElseIf rbABS.Checked = True Then
            If ddlFilterType.SelectedItem.Text = "ALL" Then
                param.Add("titleHead", "Absent")
            Else
                param.Add("titleHead", ddlFilterType.SelectedItem.Text)
            End If

        ElseIf rbLATE.Checked = True Then
            If ddlFilterType.SelectedItem.Text = "ALL" Then
                param.Add("titleHead", "Late")
            Else
                param.Add("titleHead", ddlFilterType.SelectedItem.Text)
            End If
        End If

        param.Add("Subject", ddlSubject.SelectedItem.Text)
        param.Add("SubjectGrp", ddlSubjectGroup.SelectedItem.Text)




        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            If ViewState("MainMnu_code") = "S298505" Then
                .reportPath = Server.MapPath("../RPT/rptAtt_studAttRoom_Weekly.rpt")
            Else
                .reportPath = Server.MapPath("../RPT/rptAtt_studAttRoom_PeriodWise.rpt")
            End If


        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub



    Protected Sub ddlGRD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSubject()
    End Sub

    Sub getACDstart_dt()
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim acd_id As String = ddlAcd.SelectedValue
            Dim sqlString As String = String.Empty
            sqlString = "select acd_startdt,acd_enddt from academicyear_d where acd_id='" & acd_id & "' "




            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sqlString)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        ViewState("startDt") = String.Format("{0:" & OASISConstants.DateFormat & "}", readerStudent_Detail(0))
                        ViewState("endDt") = String.Format("{0:" & OASISConstants.DateFormat & "}", readerStudent_Detail(1))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"





            If txtfromDT.Text.Trim <> "" Then
                Dim strfDate As String = txtfromDT.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtfromDT.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtfromDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"

                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDT.Text.Trim <> "" Then

                Dim strfDate As String = txtToDT.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDT.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtfromDT.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtfromDT.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function
    Protected Sub rbPRS_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindAbsent()
    End Sub
    Protected Sub rbAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindAbsent()
    End Sub

    Protected Sub rbLate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindAbsent()
    End Sub
    Sub BindSubject()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " SELECT DISTINCT SBG_ID ,CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 " & _
" FROM SUBJECTS_GRADE_S  INNER JOIN GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = GRADE_BSU_M.GRM_GRD_ID " & _
" AND SUBJECTS_GRADE_S.SBG_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = STREAM_M.STM_ID " & _
" WHERE SBG_BSU_ID = '" & Session("sBsuid") & "' AND SBG_GRD_ID= '" & ddlGrd.SelectedValue & "'  and SUBJECTS_GRADE_S.SBG_ACD_ID='" & ddlAcd.SelectedValue & "' order by DESCR2 "

            ddlSubject.Items.Clear()

         

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlSubject.Items.Add(New ListItem(reader("DESCR2"), reader("SBG_ID")))
            End While
            reader.Close()
            ddlSubject_SelectedIndexChanged(ddlSubject, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindSubjectGroup()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim SBG_ID As String = String.Empty

            If ddlSubject.SelectedIndex = -1 Then
                SBG_ID = ""
            Else
                SBG_ID = ddlSubject.SelectedValue
            End If

            str_Sql = " SELECT SGR_ID,SGR_DESCR  FROM dbo.GROUPS_M WHERE SGR_ACD_ID='" & ddlAcd.SelectedValue & "' AND SGR_GRD_ID='" & ddlGrd.SelectedValue & "'" & _
                    " AND SGR_SBG_ID ='" & SBG_ID & "' ORDER BY SGR_ID "

            ddlSubjectGroup.Items.Clear()

            

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlSubjectGroup.Items.Add(New ListItem(reader("SGR_DESCR"), reader("SGR_ID")))
            End While
            reader.Close()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    
    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSubjectGroup()
    End Sub

    
    Protected Sub ddlAcd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callGrade_ACDBind()
        getACDstart_dt()
    End Sub
End Class
