﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSibLinking.aspx.vb" Inherits="Students_Reports_ASPX_rptSibLinking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Sibling Linking/Delinking Report
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">



                <table id="tbl_AddGroup" runat="server" align="center" style="width: 100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Width="133px" Style="text-align: center"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%" id="Table2" runat="server" align="center">
                                <tr>
                                    <td align="left" style="width: 20%"><span class="field-label">Date From</span>
                                    </td>
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtCDate" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgCDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgCDate" TargetControlID="txtCDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left" style="width: 20%"><span class="field-label">Date To</span>
                                    </td>
                                    <td align="left" style="width: 30%">
                                        <asp:TextBox ID="txtSDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgSDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgSDate" TargetControlID="txtSDate">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <span class="field-label">
                                            <asp:RadioButtonList ID="rbList" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Linked" Value="0" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Delinked" Value="1"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="groupM1"
                                            TabIndex="7" />

                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>

                <asp:HiddenField ID="hfbDownload" runat="server" />
                <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
                </CR:CrystalReportSource>
            </div>
        </div>
    </div>
</asp:Content>

