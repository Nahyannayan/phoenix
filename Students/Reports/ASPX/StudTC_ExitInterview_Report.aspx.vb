﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Imports System.Net.Mail
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_StudTC_ExitInterview_Report
    Inherits System.Web.UI.Page

    Protected Overrides Sub OnInit(ByVal e As EventArgs)

        MyBase.OnInit(e)

        '   Default properties settings for message box control

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim trBSU As HtmlTableRow = tblStud.FindControl("trBSUforCorporate")
        If Page.IsPostBack = False Then

            Dim bsuID As String = Session("sBsuid")
            BindBSU()
            If bsuID = "999998" Then
                trBSU.Visible = True
                BindBSU()
            Else
                trBSU.Visible = False
            End If

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnReportPDF)
    End Sub

    Sub BindBSU()

        Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_TYPE <> 'O' AND BSU_BSCHOOL = '1' AND BSU_BGEMSSCHOOL = '1' ORDER BY BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conStr, CommandType.Text, str_query)
        ddlBSU.DataSource = ds
        ddlBSU.DataTextField = "BSU_NAME"
        ddlBSU.DataValueField = "BSU_ID"
        ddlBSU.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlBSU.Items.Insert(0, li)
        ddlBSU.SelectedIndex = 0
       
    End Sub

    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Ason_Date As String = ""
        Dim i As Integer = 0
        Dim bsu_ids As String
        Dim reportPath As String
        Dim bsuID As String = Session("sBsuid")
        If bsuID = "999998" Then
            bsu_ids = ddlBSU.SelectedItem.Value.ToString
            reportPath = "../RPT/rptExitInterviewStatusCorporate.rpt"
        Else
            bsu_ids = bsuID
            reportPath = "../RPT/rptExitInterviewStatus.rpt"
        End If

        Dim excludeFlag As String
        If chkExclude.Checked Then
            excludeFlag = "Y"
        Else
            excludeFlag = "N"
        End If

        Dim param As New Hashtable
        param.Add("@BSUID", bsu_ids)
        param.Add("@FROMDATE", Format(CDate(txtFromDate.Text), "yyyy-MM-dd"))
        param.Add("@TODATE", Format(CDate(txtToDate.Text), "yyyy-MM-dd"))
        param.Add("@FLAG", excludeFlag)
        param.Add("@ENTRYTYPE",ddlEntryType.SelectedItem.Text)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        If bsuID = "999998" Then
            param.Add("@BSUTYPE", rbtnBSUTypeTab2.SelectedIndex)
        Else
            param.Add("@BSUTYPE", 0)
        End If


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath(reportPath)


        End With

        If hfbDownload.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload

            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If

    End Sub

    ReadOnly Property conStr() As String
        Get
            Return System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString
        End Get
    End Property

    Protected Sub btnGetReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetReport.Click
        hfbDownload.Value = 0
        CallReport()
    End Sub

    Protected Sub btnReportPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReportPDF.Click
        hfbDownload.Value = 1
        CallReport()
        
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
