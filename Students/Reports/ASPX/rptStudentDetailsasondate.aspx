<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptStudentDetailsasondate.aspx.vb" Inherits="Students_Reports_ASPX_rptStudentDetailsasondate" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Student Details as on Date
 
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook"></asp:ValidationSummary>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr class="title-bg-lite">
                                    <td align="center" colspan="4">
                                        <asp:Label ID="lblCaption" runat="server" Text="Select Date Range"></asp:Label>
                                    </td>
                                </tr>--%>
                                <tr>

                                    <td align="left" width="20%"><span class="field-label">As on</span></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtAsOnDate" runat="server">
                                        </asp:TextBox>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtAsOnDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="As on Date required" ForeColor=""
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                        <br />
                                        <span>(dd/mmm/yyyy)<ajaxToolkit:CalendarExtender ID="CalendarExtender1"
                                            runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtAsOnDate">
                                        </ajaxToolkit:CalendarExtender>
                                        </span>
                                    </td>
                                    <td align="center" colspan="2">&nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" OnClick="btnGenerateReport_Click"
                                        Text="Generate Report" ValidationGroup="dayBook" /></td>
                                </tr>

                            </table>
                            &nbsp;
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>


</asp:Content>

