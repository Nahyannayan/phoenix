<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstudCONT_Late_Report.aspx.vb" Inherits="studCONT_Absent_Report" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">


        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 45)
                { return false; }
                event.keyCode = 0
            }

        }


        function chk_checked(var1, obj) {

            if (var1 == "1") {
                styleObj1 = document.getElementById(30).style;
                styleObj2 = document.getElementById(31).style;
                styleObj1.display = '';
                styleObj2.display = '';


            }
            else {

                styleObj1 = document.getElementById(30).style;
                styleObj2 = document.getElementById(31).style;
                styleObj1.display = 'none';
                styleObj2.display = 'none';



            }

        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Student Continuous  Late Report"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>

                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" width="100% ">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Year</span></td>
                                    <td align="left"  style="text-align: left"  width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left"  width="20%"><span class="field-label">Grade</span></td>
                                    <td align="left"  style="text-align: left;" width="30%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="i32" runat="server">
                                    <td align="left"><span class="field-label">Shift</span></td>
                                    <td align="left"  style="text-align: left">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlShift_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Section</span></td>
                                    <td align="left"  style="text-align: left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Late</span></td>
                                    <td align="left"  style="text-align: left">
                                        <asp:DropDownList ID="ddllate" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="title-bg" colspan="4">Filter Date</td>
                                </tr>
                                <tr>
                                    <td align="left"  colspan="4">
                                        <asp:RadioButton CSSClass="field-label" ID="rbcdate" runat="server" GroupName="gp" Text="Custom Date" AutoPostBack="True"></asp:RadioButton>
                                        <asp:RadioButton CSSClass="field-label" ID="rbt1date" runat="server" GroupName="gp" Text="Term 1" AutoPostBack="True" OnCheckedChanged="rbt1date_CheckedChanged"></asp:RadioButton>
                                        <asp:RadioButton CSSClass="field-label" ID="rbt2date" runat="server" GroupName="gp" Text="Term 2" AutoPostBack="True"></asp:RadioButton>
                                        <asp:RadioButton CSSClass="field-label" ID="rbt3date" runat="server" GroupName="gp" Text="Term 3" AutoPostBack="True"></asp:RadioButton>
                                        <asp:RadioButton CSSClass="field-label" ID="rbAdate" runat="server" GroupName="gp" Text="Annual" AutoPostBack="True"></asp:RadioButton></td>
                                </tr>
                                <tr id="i30" runat="server">
                                    <td align="left"><span class="field-label">From Dated</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox><span style="font-size: 7pt"> </span>
                                        <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><span style="font-size: 7pt">
                                        </span>
                                    </td>
                                </tr>
                                <tr id="i31" runat="server">
                                    <td align="left"><span class="field-label">To Date</span></td>
                                    <td align="left"  style="text-align: left">
                                        <asp:TextBox ID="txtTodate" runat="server">
                                        </asp:TextBox><span style="font-size: 7pt"> </span>
                                        <asp:ImageButton ID="imgbtntdate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                    </td>
                                </tr>
                                <tr>
                                     <td class="title-bg" colspan="4">Filter Total Days</td>
                                </tr>
                                <tr>
                                    <td align="left"  colspan="4">
                                        <asp:RadioButton CSSClass="field-label" ID="rbEqual" runat="server" GroupName="days" Text="Equal"></asp:RadioButton>
                                        <asp:RadioButton CSSClass="field-label" ID="rbGreaterThan" runat="server" GroupName="days" Text="Greater and Equal To"></asp:RadioButton>
                                        <asp:RadioButton CSSClass="field-label" ID="rbLessThan" runat="server" GroupName="days" Text="Less and Equal To"></asp:RadioButton></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Total Days</span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTotdays"  runat="server" MaxLength="3">
                                        </asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hfFromDate" runat="server" />
                            <asp:HiddenField ID="hfTodate" runat="server"></asp:HiddenField>
                            <ajaxToolkit:CalendarExtender ID="CBEfdate"
                                runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="cbetdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgbtntdate"
                                TargetControlID="txtTodate">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

