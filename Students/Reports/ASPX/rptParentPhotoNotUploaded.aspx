﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptParentPhotoNotUploaded.aspx.vb" Inherits="Students_Reports_ASPX_rptParentPhotoNotUploaded" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
          
         
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Parent Photo Upload Status"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" class="matters" style="text-align: left" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Shift</span></td>
                                    <td align="left" class="matters" style="text-align: left" width="30%">
                                        <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>


                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Grade</span></td>
                                    <td align="left" class="matters" style="text-align: left;">

                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters"><span class="field-label">Section</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Parent</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlParentType" runat="server">
                                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Father" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Mother" Value="2"></asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters"><span class="field-label">Photo Available</span></td>
                                    <td align="left" class="matters" style="text-align: left">
                                        <asp:DropDownList ID="ddlPhotoAvailable" runat="server">

                                            <asp:ListItem Text="YES" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="center" class="matters" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

