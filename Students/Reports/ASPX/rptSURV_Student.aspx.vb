Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Partial Class Students_Reports_ASPX_rptAdmissionDetail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try
            'txtToDate.Attributes.Add("Readonly", "Readonly")
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200445") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                'txtToDate.Text = "15/MAR/2010" 'String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                loadBsu_ID()
                ' imgToDate.Visible = False
                CURRICULUM_BSU_4_Student()
                callYEAR_DESCRBind()



            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next

            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
  

    Sub loadBsu_ID()
        Dim ItemTypeCounter As Integer = 0
        Dim tblbUsr_id As String = Session("sUsr_id")
        Dim tblbUSuper As Boolean = Session("sBusper")

        Dim buser_id As String = Session("sBsuid")
        Try

       
            'if user is super admin give access to all the Business Unit
            If tblbUSuper = True Then
                Using BUnitreaderSuper As SqlDataReader = AccessRoleUser.GetBusinessUnits()
                    'create a list item to bind records from reader to dropdownlist ddlBunit
                    Dim di As ListItem
                    ddlBUnit.Items.Clear()
                    'check if it return rows or not
                    If BUnitreaderSuper.HasRows = True Then
                        While BUnitreaderSuper.Read
                            di = New ListItem(BUnitreaderSuper(1), BUnitreaderSuper(0))
                            'adding listitems into the dropdownlist
                            ddlBUnit.Items.Add(di)

                            For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                    ddlBUnit.SelectedIndex = ItemTypeCounter
                                End If
                            Next
                        End While
                    End If
                End Using

            Else
                'if user is not super admin get access to the selected  Business Unit
                Using BUnitreader As SqlDataReader = AccessRoleUser.GetTotalBUnit(tblbUsr_id)
                    Dim di As ListItem
                    ddlBUnit.Items.Clear()
                    If BUnitreader.HasRows = True Then
                        While BUnitreader.Read
                            di = New ListItem(BUnitreader(2), BUnitreader(0))
                            ddlBUnit.Items.Add(di)
                            For ItemTypeCounter = 0 To ddlBUnit.Items.Count - 1
                                'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                If ddlBUnit.Items(ItemTypeCounter).Value = buser_id Then
                                    ddlBUnit.SelectedIndex = ItemTypeCounter
                                End If
                            Next
                        End While
                    End If
                End Using
            End If

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            Dim Str_ValidateDate As String = String.Empty
            Str_ValidateDate = ValidateDate()
            If Str_ValidateDate = "" Then
                Dim GEMSSchool As String = String.Empty
                Dim Curr As String = String.Empty
                Dim GRD As String = String.Empty

                If ddlBUnit.SelectedIndex = -1 Then
                    lblError.Text = "Select the School"
                ElseIf ddlCurr.SelectedIndex = -1 Then
                    lblError.Text = "Curriculum required"
                ElseIf ddlGrade.SelectedIndex = -1 Then
                    lblError.Text = "Grade required"
                ElseIf ddlSection.SelectedIndex = -1 Then
                    lblError.Text = "Section required"
                Else
                    CallReport()
                End If

            End If

        End If

    End Sub

    'Sub CallReport()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString

    '    Dim GEMSSchool As String = String.Empty
    '    Dim Curr As String = String.Empty
    '    Dim GRD As String = String.Empty
    '    Dim param As New Hashtable
    '    If ddlBUnit.SelectedIndex = -1 Then
    '        GEMSSchool = ""
    '    Else
    '        GEMSSchool = ddlBUnit.SelectedItem.Value

    '    End If

    '    If ddlCurr.SelectedIndex = -1 Then
    '        Curr = ""
    '    Else
    '        Curr = ddlCurr.SelectedItem.Value

    '    End If

    '    Dim cirno As String = txtCir.Text.Trim

    '    param.Add("@IMG_BSU_ID", GEMSSchool)
    '    param.Add("@IMG_TYPE", "LOGO")
    '    param.Add("@BSU_ID", GEMSSchool)
    '    param.Add("cirNo", cirno)
    '    param.Add("CirDT", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
    '    param.Add("RetDT", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
    '    param.Add("CurrentDate", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
    '    param.Add("@tmp_fld1", Curr)

    '    If ddlGrade.SelectedValue = "0" Then
    '        param.Add("@tmp_fld2", Nothing)
    '    Else
    '        param.Add("@tmp_fld2", ddlGrade.SelectedItem.Value)
    '    End If


    '    If ddlSection.SelectedItem.Value = "0" Then
    '        param.Add("@tmp_fld3", Nothing)
    '    Else
    '        param.Add("@tmp_fld3", ddlSection.SelectedItem.Value)
    '    End If



    '    Dim rptClass As New rptClass
    '    With rptClass
    '        .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
    '        .reportParameters = param

    '        .reportPath = Server.MapPath("../RPT/rptParent_Circ.rpt")


    '    End With
    '    Session("rptClass") = rptClass
    '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    '    ' LoadReports()
    'End Sub

    Private Sub CallReport()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim GEMSSchool As String = String.Empty
        Dim Curr As String = String.Empty
        Dim GRD As String = String.Empty
        Dim param As New Hashtable
        If ddlBUnit.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlBUnit.SelectedItem.Value

        End If

        If ddlCurr.SelectedIndex = -1 Then
            Curr = ""
        Else
            Curr = ddlCurr.SelectedItem.Value

        End If

        Dim cirno As String = txtCir.Text.Trim


        Dim cmd As New SqlCommand("[rptPRINT_USERNAMES]", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpBSU_ID As New SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        sqlpBSU_ID.Value = GEMSSchool
        cmd.Parameters.Add(sqlpBSU_ID)

        Dim sqlptmp_fld1 As New SqlParameter("@tmp_fld1", SqlDbType.VarChar)
        sqlptmp_fld1.Value = Curr
        cmd.Parameters.Add(sqlptmp_fld1)

        Dim sqlptmp_fld2 As New SqlParameter("@tmp_fld2", SqlDbType.VarChar)
        If ddlGrade.SelectedValue = "0" Then
            sqlptmp_fld2.Value = Nothing
        Else
            sqlptmp_fld2.Value = ddlGrade.SelectedItem.Value
        End If
        cmd.Parameters.Add(sqlptmp_fld2)

        Dim sqlptmp_fld3 As New SqlParameter("@tmp_fld3", SqlDbType.VarChar)
        If ddlSection.SelectedValue = "0" Then
            sqlptmp_fld3.Value = Nothing
        Else
            sqlptmp_fld3.Value = ddlSection.SelectedItem.Value
        End If
        cmd.Parameters.Add(sqlptmp_fld3)




        Dim sqlptmp_fld5 As New SqlParameter("@tmp_fld5", SqlDbType.VarChar)
        If ddlAcademicYear.SelectedValue = "0" Then
            sqlptmp_fld5.Value = Nothing
        Else
            sqlptmp_fld5.Value = ddlAcademicYear.SelectedItem.Text
        End If
        cmd.Parameters.Add(sqlptmp_fld5)



        Dim sqlptmp_fld4 As New SqlParameter("@tmp_fld4", SqlDbType.VarChar)
        If chkNew.Checked = False Then
            sqlptmp_fld4.Value = "False"
        Else
            sqlptmp_fld4.Value = "True"
        End If
        cmd.Parameters.Add(sqlptmp_fld4)

        objConn.Close()
        objConn.Open()
        'If cmd.ExecuteScalar() IsNot Nothing Then
        If 1 = 1 Then
            cmd.Connection = objConn
            Dim repSource As New MyReportClass
            Dim params As New Hashtable
            param.Add("@IMG_BSU_ID", Session("sbsuid"))
            param.Add("@IMG_TYPE", "LOGO")

            params("cirNo") = cirno
            params("CirDT") = Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy")
            params("RetDT") = Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy")
            params("CurrentDate") = Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy")
            repSource.Parameter = params
            repSource.Command = cmd
            If GEMSSchool = "125016" Then
                repSource.ResourceName = "../../STUDENTS/REPORTS/RPT/rptParent_Circ_RDS.rpt"
            ElseIf GEMSSchool = "125002" Then
                repSource.ResourceName = "../../STUDENTS/REPORTS/RPT/rptParent_Circ_JC.rpt"
            ElseIf GEMSSchool = "121013" Then
                repSource.ResourceName = "../../STUDENTS/REPORTS/RPT/rptParent_Circ_OOD.rpt"
            ElseIf GEMSSchool = "125015" Then
                repSource.ResourceName = "../../STUDENTS/REPORTS/RPT/rptParent_Circ_WIS.rpt"
            Else
                repSource.ResourceName = "../../STUDENTS/REPORTS/RPT/rptParent_Circ.rpt"
            End If

            repSource.IncludeBSUImage = True
            Session("ReportSource") = repSource
            objConn.Close()
            'If Not ViewState("isExport") Is Nothing AndAlso ViewState("isExport") = True Then
            '    Response.Redirect("~/Reports/ASPX Report/rptviewer.aspx?isExport=true", True)
            'Else
            '    Response.Redirect("~/Reports/ASPX Report/rptviewer.aspx?isCircular=true", True)
            'End If
            ReportLoadSelection()
        Else
            lblError.Text = "No Records with specified condition"
        End If
        objConn.Close()

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

   

    Function ValidateDate() As String
        Try

        
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDate.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>Circular Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>Circular Date required"

            End If


            'If txtToDate.Text.Trim <> "" Then

            '    Dim strfDate As String = txtToDate.Text.Trim
            '    Dim str_err As String = DateFunctions.checkdate(strfDate)
            '    If str_err <> "" Then
            '        ErrorStatus = "-1"
            '        CommStr = CommStr & "<li>Returned Date format is Invalid"
            '    Else
            '        txtToDate.Text = strfDate
            '        Dim DateTime2 As Date
            '        Dim dateTime1 As Date
            '        Dim strfDate1 As String = txtFromDate.Text.Trim
            '        Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
            '        If str_err1 <> "" Then
            '        Else
            '            DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            '            dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            '            'check for the leap year date
            '            If IsDate(DateTime2) Then
            '                If DateTime.Compare(dateTime1, DateTime2) > 0 Then
            '                    ErrorStatus = "-1"
            '                    CommStr = CommStr & "<li>Returned Date entered is not a valid date and must be greater than or equal to Circular Date"
            '                End If
            '            Else
            '                ErrorStatus = "-1"
            '                CommStr = CommStr & "<li>Returned Date format is Invalid"
            '            End If
            '        End If
            '    End If
            'Else
            '    CommStr = CommStr & "<li>Returned Date required"

            'End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptSurv_Student")
            Return "-1"
        End Try

    End Function
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub
    Protected Sub ddlBUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CURRICULUM_BSU_4_Student()
    End Sub
    Protected Sub ddlCurr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        callGrade_ACDBind()
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callGrade_Section()
    End Sub
    Sub CURRICULUM_BSU_4_Student()


        Dim GEMSSchool As String = String.Empty
        If ddlBUnit.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlBUnit.SelectedItem.Value

        End If

        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim dsOpt As New DataSet
        Dim Enq_opt As String = "SELECT  distinct  tmp_fld1   FROM   SURV_OFFLINE where BSU_ID='" & GEMSSchool & "'"
        dsOpt = SqlHelper.ExecuteDataset(conn, CommandType.Text, Enq_opt)
        ddlCurr.Items.Clear()
        If dsOpt.Tables(0).Rows.Count > 0 Then
            ddlCurr.DataSource = dsOpt.Tables(0)
            ddlCurr.DataTextField = "tmp_fld1"
            ddlCurr.DataValueField = "tmp_fld1"
            ddlCurr.DataBind()
        End If
        If ddlCurr.SelectedIndex <> -1 Then
            ddlCurr_SelectedIndexChanged(ddlCurr, Nothing)
        End If

    End Sub

    Public Sub callGrade_ACDBind()
        Try

            Dim GEMSSchool As String = String.Empty
            Dim Curr As String = String.Empty
            Dim GRD As String = String.Empty
            If ddlBUnit.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlBUnit.SelectedItem.Value

            End If

            If ddlCurr.SelectedIndex = -1 Then
                Curr = ""
            Else
                Curr = ddlCurr.SelectedItem.Value

            End If

            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim dsOpt As New DataSet
            Dim Enq_opt As String = "SELECT tmp_fld2 From (SELECT  distinct    tmp_fld2   FROM   SURV_OFFLINE where BSU_ID='" & GEMSSchool & "' AND tmp_fld1='" & Curr & "') A Order By 1"

            dsOpt = SqlHelper.ExecuteDataset(conn, CommandType.Text, Enq_opt)
            ddlGrade.Items.Clear()
            If dsOpt.Tables(0).Rows.Count > 0 Then
                ddlGrade.DataSource = dsOpt.Tables(0)
                ddlGrade.DataTextField = "tmp_fld2"
                ddlGrade.DataValueField = "tmp_fld2"
                ddlGrade.DataBind()
               
            End If

            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            ddlGrade.Items.FindByText("ALL").Selected = True

            If ddlGrade.SelectedIndex <> -1 Then
                ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Public Sub callGrade_Section()
        Try

            Dim GEMSSchool As String = String.Empty
            Dim Curr As String = String.Empty
            Dim GRD As String = String.Empty

            If ddlBUnit.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlBUnit.SelectedItem.Value

            End If

            If ddlCurr.SelectedIndex = -1 Then
                Curr = ""
            Else
                Curr = ddlCurr.SelectedItem.Value

            End If
            If ddlGrade.SelectedIndex = -1 Then
                GRD = ""
            ElseIf ddlGrade.SelectedValue = "0" Then
                GRD = " tmp_fld2<>''"
            Else
                GRD = ddlGrade.SelectedItem.Value
                GRD = " tmp_fld2 ='" & GRD & "'"
            End If


            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim dsOpt As New DataSet
            Dim Enq_opt As String = "SELECT tmp_fld3 From (SELECT  distinct    tmp_fld3   FROM   SURV_OFFLINE where BSU_ID='" & GEMSSchool & "' AND tmp_fld1='" & Curr & "' AND " & GRD & " ) A ORDER BY 1 "

            dsOpt = SqlHelper.ExecuteDataset(conn, CommandType.Text, Enq_opt)
            ddlSection.Items.Clear()
            If dsOpt.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = dsOpt.Tables(0)
                ddlSection.DataTextField = "tmp_fld3"
                ddlSection.DataValueField = "tmp_fld3"
                ddlSection.DataBind()
                ddlSection.Items.Add(New ListItem("ALL", "0"))
                ddlSection.ClearSelection()
                ddlSection.Items.FindByText("ALL").Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    'Sub LoadReports(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try



    '        Dim rptClass As New rptClass
    '        Dim iRpt As New DictionaryEntry
    '        Dim i As Integer
    '        Dim newWindow As String

    '        Try
    '            newWindow = Request.QueryString("newWindow")
    '        Catch ex As Exception

    '        End Try
    '        Dim rptStr As String = ""
    '        If Not newWindow Is Nothing Then
    '            rptStr = "rptClass" + newWindow
    '        Else
    '            rptStr = "rptClass"
    '        End If


    '        ' rptClass = Session("rptClass")
    '        rptClass = Session.Item(rptStr)

    '        Dim crParameterDiscreteValue As ParameterDiscreteValue
    '        Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    '        Dim crParameterFieldLocation As ParameterFieldDefinition
    '        Dim crParameterValues As ParameterValues

    '        With rptClass




    '            'If Not Session("prevClass") Is Nothing And newWindow Is Nothing Then
    '            '    Session("rptClass") = Session("prevClass")
    '            '    rptClass = Session("prevClass")
    '            '    Session("prevClass") = Nothing
    '            '    Page_Load(sender, e)
    '            '    Exit Sub
    '            'End If

    '            rs.ReportDocument.Load(.reportPath)
    '            rs.ReportDocument.DataSourceConnections(0).SetConnection(.crInstanceName, .crDatabase, .crUser, .crPassword) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
    '            rs.ReportDocument.SetDatabaseLogon(.crUser, .crPassword, .crInstanceName, .crDatabase, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '
    '            rs.ReportDocument.VerifyDatabase()

    '            'Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
    '            'myConnectionInfo.ServerName = .crInstanceName
    '            'myConnectionInfo.DatabaseName = .crDatabase
    '            'myConnectionInfo.UserID = .crUser
    '            'myConnectionInfo.Password = .crPassword


    '            'SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
    '            'SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)


    '            'If .subReportCount <> 0 Then
    '            '    For i = 0 To .subReportCount - 1
    '            '        rs.ReportDocument.Subreports(i).VerifyDatabase()
    '            '    Next
    '            'End If

    '            crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
    '            If .reportParameters.Count <> 0 Then
    '                For Each iRpt In .reportParameters
    '                    crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
    '                    crParameterValues = crParameterFieldLocation.CurrentValues
    '                    crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
    '                    crParameterDiscreteValue.Value = iRpt.Value
    '                    crParameterValues.Add(crParameterDiscreteValue)
    '                    crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
    '                Next
    '            End If




    '            If .selectionFormula <> "" Then
    '                rs.ReportDocument.RecordSelectionFormula = .selectionFormula
    '            End If


    '        End With
    '    Catch ex As Exception


    '    End Try
    'End Sub

    'Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
    '    Dim myTables As Tables = myReportDocument.Database.Tables
    '    Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '    Dim crParameterDiscreteValue As ParameterDiscreteValue
    '    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    '    Dim crParameterFieldLocation As ParameterFieldDefinition
    '    Dim crParameterValues As ParameterValues
    '    Dim iRpt As New DictionaryEntry
    '    For Each myTable In myTables
    '        Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '        myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '        myTable.ApplyLogOnInfo(myTableLogonInfo)
    '    Next

    '    'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
    '    'If reportParameters.Count <> 0 Then
    '    '    For Each iRpt In reportParameters
    '    '        Try
    '    '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
    '    '            crParameterValues = crParameterFieldLocation.CurrentValues
    '    '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
    '    '            crParameterDiscreteValue.Value = iRpt.Value
    '    '            crParameterValues.Add(crParameterDiscreteValue)
    '    '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
    '    '        Catch ex As Exception
    '    '        End Try
    '    '    Next
    '    'End If
    '    myReportDocument.VerifyDatabase()

    'End Sub
    
End Class
