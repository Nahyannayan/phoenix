<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptCoproPlaining_ELM.aspx.vb" Inherits="ptCoproPlaining_ELM" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Planning Tool - New"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">School Group</span></td>
                                    <td align="left"  style="text-align: left" width="30%">
                                        <asp:DropDownList ID="ddlSch_Grp" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="ASIAN">Asian Schools</asp:ListItem>
                                            <asp:ListItem Value="INTERNATIONAL">International Schools</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">As on</span></td>
                                    <td align="left"  style="text-align: left" width="30%">
                                         <asp:TextBox ID="txtAsOnDate" runat="server" CssClass="inputbox">
                                        </asp:TextBox><asp:ImageButton ID="imgOnDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton>

                                        
                                </tr>
                                <tr id="tr_ACD" runat="Server">
                                    <td align="left" ><span class="field-label">Academic Year</span></td>
                                    <td align="left"  style="text-align: left">
                                        <asp:DropDownList ID="ddlAcd_year" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook"/></td>
                                </tr>
                            </table>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calAsOnDate" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgOnDate" TargetControlID="txtAsOnDate">
                </ajaxToolkit:CalendarExtender> 

            </div>
        </div>
    </div>
</asp:Content>

