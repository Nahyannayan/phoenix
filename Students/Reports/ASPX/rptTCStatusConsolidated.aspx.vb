﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Students_Reports_ASPX_rptTCStatusConsolidated
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            '  Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            'ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ' ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200067") Then
            '    If Not Request.UrlReferrer Is Nothing Then
            '        Response.Redirect(Request.UrlReferrer.ToString())
            '    Else

            '        Response.Redirect("~\noAccess.aspx")
            '    End If

            'Else
            'calling pageright class to get the access rights


            'ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights
            'use content if the page is comming from master page else use me.Page

            'disable the control buttons based on the rights
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            '     BindSummaryBSU()
            ' BindStatusCompareBSu()

            BindTransferFromBsu("", 0)
            BindTransferToBsu()
            BindGrade()
            BindStatusCompareGradeInitial()
        End If
        'End If

        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnReportPDF)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnDownLoadExit)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnRCDownload)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnRSDownload)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnStatusComparePDF)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSummaryReportPDF)
    End Sub

#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindStatusCompareBSu()

        'ddlStatusCompareBSU.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE (ISNULL(BSU_bONLINETC,0)=1 OR BSU_SHORTNAME IN ('AAQ','WSQ')) ORDER BY BSU_NAME"
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'ddlStatusCompareBSU.DataSource = ds
        'ddlStatusCompareBSU.DataTextField = "BSU_NAME"
        'ddlStatusCompareBSU.DataValueField = "BSU_ID"
        'ddlStatusCompareBSU.DataBind()
        'ddlStatusCompareBSU.Items.Insert(0, New ListItem("ALL", "0"))

    End Sub

    Sub BindSummaryBSU()

        'chkSummaryBSU.Items.Clear()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE (ISNULL(BSU_bONLINETC,0)=1 OR BSU_SHORTNAME IN ('AAQ','WSQ')) ORDER BY BSU_NAME"
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'chkSummaryBSU.DataSource = ds
        'chkSummaryBSU.DataTextField = "BSU_NAME"
        'chkSummaryBSU.DataValueField = "BSU_ID"
        'chkSummaryBSU.DataBind()
        'chkSummaryBSU.Visible = True
    End Sub

    Sub BindTransferFromBsu(ByVal strBSUType As String, ByVal status As Integer)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSU_ID,BSU_NAME FROM BUSINESSUNIT_M WHERE (ISNULL(BSU_bONLINETC,0)=1 OR BSU_SHORTNAME IN ('AAQ','WSQ')) "
        If Session("sbsuid") <> "999998" Then
            str_query += " AND BSU_ID='" + Session("SBSUID") + "'"
        End If
        str_query += strBSUType
        str_query += "ORDER BY BSU_NAME"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If status = 0 Then

            chkSummaryBSU.DataSource = ds
            chkSummaryBSU.DataTextField = "BSU_NAME"
            chkSummaryBSU.DataValueField = "BSU_ID"
            chkSummaryBSU.DataBind()
            chkSummaryBSU.Visible = True

            ddlBSUExit.DataSource = ds
            ddlBSUExit.DataTextField = "BSU_NAME"
            ddlBSUExit.DataValueField = "BSU_ID"
            ddlBSUExit.DataBind()

            lstBsuFrom.DataSource = ds
            lstBsuFrom.DataTextField = "BSU_NAME"
            lstBsuFrom.DataValueField = "BSU_ID"
            lstBsuFrom.DataBind()

            lstBsuRC.DataSource = ds
            lstBsuRC.DataTextField = "BSU_NAME"
            lstBsuRC.DataValueField = "BSU_ID"
            lstBsuRC.DataBind()

            ddlBsuRS.DataSource = ds
            ddlBsuRS.DataTextField = "BSU_NAME"
            ddlBsuRS.DataValueField = "BSU_ID"
            ddlBsuRS.DataBind()

            ddlStatusCompareBSU.DataSource = ds
            ddlStatusCompareBSU.DataTextField = "BSU_NAME"
            ddlStatusCompareBSU.DataValueField = "BSU_ID"
            ddlStatusCompareBSU.DataBind()

            If Session("sbsuid") = "999998" Then
                Dim li As New ListItem
                li.Text = "ALL"
                li.Value = "0"
                ddlBSUExit.Items.Insert(0, li)
                ddlBSUExit.SelectedIndex = 0
                ddlStatusCompareBSU.Items.Insert(0, New ListItem("ALL", "0"))
            End If
        Else
            If tabPopup.ActiveTab.TabIndex = 0 Then
                chkSummaryBSU.DataSource = ds
                chkSummaryBSU.DataTextField = "BSU_NAME"
                chkSummaryBSU.DataValueField = "BSU_ID"
                chkSummaryBSU.DataBind()
                chkSummaryBSU.Visible = True
            ElseIf tabPopup.ActiveTab.TabIndex = 1 Then
                ddlBSUExit.DataSource = ds
                ddlBSUExit.DataTextField = "BSU_NAME"
                ddlBSUExit.DataValueField = "BSU_ID"
                ddlBSUExit.DataBind()
                If Session("sbsuid") = "999998" Then
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = "0"
                    ddlBSUExit.Items.Insert(0, li)
                    ddlBSUExit.SelectedIndex = 0
                End If
            ElseIf tabPopup.ActiveTab.TabIndex = 2 Then
                lstBsuFrom.DataSource = ds
                lstBsuFrom.DataTextField = "BSU_NAME"
                lstBsuFrom.DataValueField = "BSU_ID"
                lstBsuFrom.DataBind()
            ElseIf tabPopup.ActiveTab.TabIndex = 3 Then
                lstBsuRC.DataSource = ds
                lstBsuRC.DataTextField = "BSU_NAME"
                lstBsuRC.DataValueField = "BSU_ID"
                lstBsuRC.DataBind()
            ElseIf tabPopup.ActiveTab.TabIndex = 4 Then
                ddlBsuRS.DataSource = ds
                ddlBsuRS.DataTextField = "BSU_NAME"
                ddlBsuRS.DataValueField = "BSU_ID"
                ddlBsuRS.DataBind()
            ElseIf tabPopup.ActiveTab.TabIndex = 5 Then
                ddlStatusCompareBSU.DataSource = ds
                ddlStatusCompareBSU.DataTextField = "BSU_NAME"
                ddlStatusCompareBSU.DataValueField = "BSU_ID"
                ddlStatusCompareBSU.DataBind()
                If Session("sbsuid") = "999998" Then
                    Dim li As New ListItem
                    li.Text = "ALL"
                    li.Value = "0"
                    ddlStatusCompareBSU.Items.Insert(0, New ListItem("ALL", "0"))
                End If
            End If
        End If

        
    End Sub

    Sub BindTransferToBsu()
        Dim str_query As String = ""


        If rdAll.Checked = True Then
            str_query = "select bsu_name,BSU_ID from (select BSU_ID+'|GEMS' BSU_ID,bsu_name from businessunit_m where (ISNULL(BSU_bONLINETC,0)=1 OR BSU_SHORTNAME IN ('AAQ','WSQ'))"
            If rbtnBSUTypeTab3b.SelectedIndex = 0 Then
                str_query += "AND BSU_bASIANSCHOOL = 1 "
            ElseIf rbtnBSUTypeTab3b.SelectedIndex = 1 Then
                str_query += "AND BSU_bASIANSCHOOL = 0 "
            End If
            str_query += " union "
            str_query += "SELECT CONVERT(VARCHAR(100),BCC_ID)+'|NONGEMS' ,UPPER(BCC_NAME) FROM BSU_COMPETITORS ) P"
            str_query += " order by bsu_name"
        End If

        If rdGems.Checked = True Then
            str_query = "select bsu_name,BSU_ID from (select BSU_ID+'|GEMS' BSU_ID,bsu_name from businessunit_m where (ISNULL(BSU_bONLINETC,0)=1 OR BSU_SHORTNAME IN ('AAQ','WSQ')))P"
            If rbtnBSUTypeTab3b.SelectedIndex = 0 Then
                str_query += "AND BSU_bASIANSCHOOL = 1 "
            ElseIf rbtnBSUTypeTab3b.SelectedIndex = 1 Then
                str_query += "AND BSU_bASIANSCHOOL = 0 "
            End If
            str_query += " order by bsu_name"
        End If

        If rdNonGems.Checked = True Then
            str_query += "select bsu_name,BSU_ID from (SELECT CONVERT(VARCHAR(100),BCC_ID)+'|NONGEMS' AS BSU_ID ,UPPER(BCC_NAME) AS bsu_name FROM BSU_COMPETITORS ) P"
            str_query += " order by bsu_name"
        End If


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lstBsuTo.DataSource = ds
        lstBsuTo.DataTextField = "bsu_name"
        lstBsuTo.DataValueField = "BSU_ID"
        lstBsuTo.DataBind()

        If rdAll.Checked = True Or rdNonGems.Checked = True Then
            Dim li As New ListItem
            li.Text = "OTHERS"
            li.Value = "0"
            lstBsuTo.Items.Add(li)
        End If

    End Sub

    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M " _
                              & " INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID " _
                              & " INNER JOIN ACADEMICYEAR_D ON GRM_ACD_ID=ACD_ID " _
                              & " WHERE ACD_BSU_ID='" + ddlBsuRS.SelectedValue.ToString + "' AND ACD_CURRENT=1" _
                              & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRM_GRD_ID"
        ddlGrade.DataBind()

        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        ddlGrade.Items.Insert(0, li)
    End Sub

    Sub BindStatusCompareGradeInitial()
        ddlStatusCompareGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT GRD_ID,GRD_DISPLAY FROM GRADE_M"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStatusCompareGrade.DataSource = ds
        ddlStatusCompareGrade.DataTextField = "GRD_DISPLAY"
        ddlStatusCompareGrade.DataValueField = "GRD_ID"
        ddlStatusCompareGrade.DataBind()

        ddlStatusCompareGrade.Items.Insert(0, New ListItem("ALL", "0"))

    End Sub

    Sub BindStatusCompareGrade()

        ddlStatusCompareGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRM_GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M " _
                              & " INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID " _
                              & " INNER JOIN ACADEMICYEAR_D ON GRM_ACD_ID=ACD_ID " _
                              & " WHERE ACD_BSU_ID='" + ddlStatusCompareBSU.SelectedValue.ToString + "' AND ACD_CURRENT=1" _
                              & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStatusCompareGrade.DataSource = ds
        ddlStatusCompareGrade.DataTextField = "GRM_DISPLAY"
        ddlStatusCompareGrade.DataValueField = "GRM_GRD_ID"
        ddlStatusCompareGrade.DataBind()

        ddlStatusCompareGrade.Items.Insert(0, New ListItem("ALL", "0"))

    End Sub

    Sub CallStatusCompareReport()

        Dim excludeFlag As String
        If chkExcludeStatusCompare.Checked Then
            excludeFlag = "Y"
        Else
            excludeFlag = "N"
        End If


        Dim grade As String = ""
        Dim rptGrade As String = ""
        Dim i As Integer
        Dim reportPath As String = ""
        If ddlStatusCompareBSU.SelectedIndex = 0 Then
            grade = ddlStatusCompareGrade.SelectedItem.Value
            reportPath = Server.MapPath("../RPT/rptExitInterviewStatusComparison.rpt")
        Else
            grade = ddlStatusCompareGrade.SelectedItem.Value
            'For i = 1 To ddlStatusCompareGrade.Items.Count - 1
            '    If grade <> "" Then
            '        grade += "|"
            '        rptGrade += ","
            '    End If
            '    grade += ddlStatusCompareGrade.Items(i).Value
            '    rptGrade += ddlStatusCompareGrade.Items(i).Value
            'Next
            reportPath = Server.MapPath("../RPT/rptExitInterviewStatusComparisonSingleBSU.rpt")
        End If

        Dim param As New Hashtable
        param.Add("@BSU_ID", ddlStatusCompareBSU.SelectedItem.Value)
        param.Add("@GRD_ID", grade)
        param.Add("@FROMDATE", Format(CDate(txtStatusCompareFromDate.Text), "yyyy-MM-dd"))
        param.Add("@TODATE", Format(CDate(txtStatusCompareToDate.Text), "yyyy-MM-dd"))
        param.Add("@FLAG", excludeFlag)
        param.Add("@TYPE", rbtnDateType.SelectedItem.Value)
        param.Add("@TCSOType", rbtnTCSOType.SelectedItem.Value)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")

        If ddlStatusCompareBSU.SelectedIndex = 0 Then
            param.Add("@BSUTYPE", rbtnBSUTypeTab6.SelectedIndex)
        End If
       
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = reportPath
        End With
        

        If hfbDownload.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload

            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If

        'Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")


    End Sub

    Sub CallTCByTransferSchoolReport()

        Dim strFrom As String = ""
        Dim strTo As String = ""
        Dim i As Integer

        For i = 0 To lstBsuFrom.Items.Count - 1
            If lstBsuFrom.Items(i).Selected = True Then
                If strFrom <> "" Then
                    strFrom += "|"
                End If
                strFrom += lstBsuFrom.Items(i).Value
            End If
        Next

        For i = 0 To lstBsuTo.Items.Count - 1
            If lstBsuTo.Items(i).Selected = True Then
                If strTo <> "" Then
                    strTo += "|"
                End If
                strTo += lstBsuTo.Items(i).Text
            End If
        Next

        Dim excludeFlag As String
        If chkExcludeTransferSchool.Checked Then
            excludeFlag = "Y"
        Else
            excludeFlag = "N"
        End If

        Dim param As New Hashtable
        param.Add("@BSU_FROM", strFrom)
        param.Add("@BSU_TO", strTo)
        param.Add("@APPLYSTART", Format(CDate(txtApplyStart.Text), "yyyy-MM-dd"))
        param.Add("@APPLYEND", Format(CDate(txtApplyEnd.Text), "yyyy-MM-dd"))
        param.Add("@FLAG", excludeFlag)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptTCCountbyTransferSchool.rpt")
        End With

        If hfbDownload.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload

            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If

    End Sub

    Sub CallTCCountByReason()

        Dim strBsu As String = ""
        Dim i As Integer

        For i = 0 To lstBsuFrom.Items.Count - 1
            If lstBsuRC.Items(i).Selected = True Then
                If strBsu <> "" Then
                    strBsu += "|"
                End If
                strBsu += lstBsuRC.Items(i).Value
            End If
        Next

        Dim excludeFlag As String
        If chkExcludeCountByReason.Checked Then
            excludeFlag = "Y"
        Else
            excludeFlag = "N"
        End If

        Dim param As New Hashtable
        param.Add("@BSU_IDS", strBsu)
        param.Add("@APPLYSTART", Format(CDate(txtRCStart.Text), "yyyy-MM-dd"))
        param.Add("@APPLYEND", Format(CDate(txtRCEnd.Text), "yyyy-MM-dd"))
        param.Add("@FLAG", excludeFlag)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptTCCountByReason.rpt")
        End With

        If hfbDownload.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload

            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If

    End Sub

    Sub CallTCDetailsByReason()

        Dim strBsu As String = ""
        Dim i As Integer

        Dim excludeFlag As String
        If chkExcludeListByReason.Checked Then
            excludeFlag = "Y"
        Else
            excludeFlag = "N"
        End If

        Dim param As New Hashtable
        param.Add("@BSU_ID", ddlBsuRS.SelectedValue.ToString)
        param.Add("@APPLYSTART", Format(CDate(txtRSStart.Text), "yyyy-MM-dd"))
        param.Add("@APPLYEND", Format(CDate(txtRSEnd.Text), "yyyy-MM-dd"))
        param.Add("@GRD_ID", ddlGrade.SelectedValue.ToString)
        param.Add("@FLAG", excludeFlag)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptTCDetailstByReason.rpt")
        End With

        If hfbDownload.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload

            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If

    End Sub

    Sub CallSummaryReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Ason_Date As String = ""
        

        Dim tmpstr As String = ""


        For i As Integer = 0 To chkSummaryBSU.Items.Count - 1
            If chkSummaryBSU.Items(i).Selected = True Then

                tmpstr &= chkSummaryBSU.Items(i).Value & "|"
            End If
        Next


        Dim param As New Hashtable
        param.Add("@BSU_IDS", tmpstr)
        param.Add("@FROMDATE", Format(CDate(txtSumFromDate.Text), "yyyy-MM-dd"))
        param.Add("@TODATE", Format(CDate(txtSumToDate.Text), "yyyy-MM-dd"))
        



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptTCSummaryReport.rpt")


        End With

        If hfbDownload.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload

            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If

    End Sub


    Sub CallExitStatusReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Ason_Date As String = ""
        Dim i As Integer = 0
        Dim bsu_ids As String
        Dim reportPath As String
        Dim bsuID As String = Session("sBsuid")
        If bsuID = "999998" Then
            bsu_ids = ddlBSUExit.SelectedItem.Value.ToString
            reportPath = "../RPT/rptExitInterviewStatusCorporate.rpt"
        Else
            bsu_ids = bsuID
            reportPath = "../RPT/rptExitInterviewStatus.rpt"
        End If

        Dim excludeFlag As String
        If chkExclude.Checked Then
            excludeFlag = "Y"
        Else
            excludeFlag = "N"
        End If

        Dim param As New Hashtable
        param.Add("@BSUID", bsu_ids)
        param.Add("@FROMDATE", Format(CDate(txtFromDate.Text), "yyyy-MM-dd"))
        param.Add("@TODATE", Format(CDate(txtToDate.Text), "yyyy-MM-dd"))
        param.Add("@FLAG", excludeFlag)
        param.Add("@ENTRYTYPE", ddlEntryType.SelectedItem.Text)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        If bsuID = "999998" Then
            param.Add("@BSUTYPE", rbtnBSUTypeTab2.SelectedIndex)
        Else
            param.Add("@BSUTYPE", 0)
        End If



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath(reportPath)


        End With

        If hfbDownload.Value = 1 Then
            'Session("rptClass") = rptClass
            Dim rptDownload As New ReportDownload

            rptDownload.LoadReports(rptClass, rs)
            rptDownload = Nothing
        Else
            Session("rptClass") = rptClass
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        End If

    End Sub
#End Region

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        hfbDownload.Value = 0
        CallTCByTransferSchoolReport()
    End Sub

    Protected Sub btnReportPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReportPDF.Click
        hfbDownload.Value = 1
        CallTCByTransferSchoolReport()
    End Sub

    Protected Sub rdAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAll.CheckedChanged
        If rdAll.Checked = True Then
            BindTransferToBsu()
        End If
    End Sub

    Protected Sub rdGems_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGems.CheckedChanged
        If rdGems.Checked = True Then
            BindTransferToBsu()
        End If
    End Sub

    Protected Sub rdNonGems_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdNonGems.CheckedChanged
        If rdNonGems.Checked = True Then
            BindTransferToBsu()
        End If
    End Sub

    Protected Sub btnSummaryReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSummaryReport.Click
        hfbDownload.Value = 0
        CallSummaryReport()
    End Sub

    Protected Sub btnGenExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenExit.Click
        hfbDownload.Value = 0
        CallExitStatusReport()
    End Sub

    Protected Sub btnDownLoadExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoadExit.Click
        hfbDownload.Value = 1
        CallExitStatusReport()
    End Sub

    Protected Sub btnRCGen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRCGen.Click
        hfbDownload.Value = 0
        CallTCCountByReason()
    End Sub

    Protected Sub btnRCDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRCDownload.Click
        hfbDownload.Value = 1
        CallTCCountByReason()
    End Sub

    Protected Sub btnRSGen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRSGen.Click
        hfbDownload.Value = 0
        CallTCDetailsByReason()
    End Sub

    Protected Sub btnRSDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRSDownload.Click
        hfbDownload.Value = 1
        CallTCDetailsByReason()
    End Sub

    Protected Sub btnStatusCompare_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatusCompare.Click
        hfbDownload.Value = 0
        CallStatusCompareReport()
    End Sub

    Protected Sub btnSummaryReportPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSummaryReportPDF.Click
        hfbDownload.Value = 1
        CallSummaryReport()
    End Sub

    Protected Sub btnStatusComparePDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatusComparePDF.Click
        hfbDownload.Value = 1
        CallStatusCompareReport()
    End Sub

    Protected Sub ddlBsuRS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBsuRS.SelectedIndexChanged
        BindGrade()
    End Sub

    Protected Sub ddlStatusCompareBSU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatusCompareBSU.SelectedIndexChanged
        BindStatusCompareGrade()
    End Sub

    Protected Sub rbtnBSUType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnBSUType.SelectedIndexChanged
        Dim str_query As String = ""
        If rbtnBSUType.SelectedIndex = 0 Then
            str_query += "AND BSU_bASIANSCHOOL = 1 "
        ElseIf rbtnBSUType.SelectedIndex = 1 Then
            str_query += "AND BSU_bASIANSCHOOL = 0 "
        End If
        BindTransferFromBsu(str_query, 1)
        
    End Sub
    Protected Sub rbtnBSUTypeTab2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnBSUTypeTab2.SelectedIndexChanged
        Dim str_query As String = ""
        If rbtnBSUTypeTab2.SelectedIndex = 0 Then
            str_query += "AND BSU_bASIANSCHOOL = 1 "
        ElseIf rbtnBSUTypeTab2.SelectedIndex = 1 Then
            str_query += "AND BSU_bASIANSCHOOL = 0 "
        End If
        BindTransferFromBsu(str_query, 1)
    End Sub
    Protected Sub rbtnBSUTypeTab3a_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnBSUTypeTab3a.SelectedIndexChanged
        Dim str_query As String = ""
        If rbtnBSUTypeTab3a.SelectedIndex = 0 Then
            str_query += "AND BSU_bASIANSCHOOL = 1 "
        ElseIf rbtnBSUTypeTab3a.SelectedIndex = 1 Then
            str_query += "AND BSU_bASIANSCHOOL = 0 "
        End If
        BindTransferFromBsu(str_query, 1)
    End Sub

    Protected Sub rbtnBSUTypeTab4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnBSUTypeTab4.SelectedIndexChanged
        Dim str_query As String = ""
        If rbtnBSUTypeTab4.SelectedIndex = 0 Then
            str_query += "AND BSU_bASIANSCHOOL = 1 "
        ElseIf rbtnBSUTypeTab4.SelectedIndex = 1 Then
            str_query += "AND BSU_bASIANSCHOOL = 0 "
        End If
        
        BindTransferFromBsu(str_query, 1)
    End Sub
    Protected Sub rbtnBSUTypeTab5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnBSUTypeTab5.SelectedIndexChanged
        Dim str_query As String = ""
        If rbtnBSUTypeTab5.SelectedIndex = 0 Then
            str_query += "AND BSU_bASIANSCHOOL = 1 "
        ElseIf rbtnBSUTypeTab5.SelectedIndex = 1 Then
            str_query += "AND BSU_bASIANSCHOOL = 0 "
        End If
       
        BindTransferFromBsu(str_query, 1)
    End Sub

    Protected Sub rbtnBSUTypeTab6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnBSUTypeTab6.SelectedIndexChanged
        Dim str_query As String = ""
        If rbtnBSUTypeTab6.SelectedIndex = 0 Then
            str_query += "AND BSU_bASIANSCHOOL = 1 "
        ElseIf rbtnBSUTypeTab6.SelectedIndex = 1 Then
            str_query += "AND BSU_bASIANSCHOOL = 0 "
        End If
        BindTransferFromBsu(str_query, 1)
    End Sub
    Protected Sub rbtnBSUTypeTab3b_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnBSUTypeTab3b.SelectedIndexChanged
        BindTransferToBsu()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
