<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstudAttendance_Report_MTH.aspx.vb" Inherits="Students_studAttendance_Report_MTH" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Monthwise Attendance Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook"></asp:ValidationSummary>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Month</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:DropDownList ID="ddlMNTH" runat="server">
                                            <asp:ListItem Value="Jan">January</asp:ListItem>
                                            <asp:ListItem Value="Feb">February</asp:ListItem>
                                            <asp:ListItem Value="Mar">March</asp:ListItem>
                                            <asp:ListItem Value="Apr">April</asp:ListItem>
                                            <asp:ListItem>May</asp:ListItem>
                                            <asp:ListItem Value="Jun">June</asp:ListItem>
                                            <asp:ListItem Value="Jul">July</asp:ListItem>
                                            <asp:ListItem Value="Aug">August</asp:ListItem>
                                            <asp:ListItem Value="Sep">September</asp:ListItem>
                                            <asp:ListItem Value="Oct">October</asp:ListItem>
                                            <asp:ListItem Value="Nov">November</asp:ListItem>
                                            <asp:ListItem Value="Dec">December</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table style="width: 100%">
                                            <tr>
                                                <td align="left" width="40%"><span class="field-label">Stream</span></td>
                                                <td align="left" width="60%">
                                                    <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="40%" class="matters"><span class="field-label">Grade</span></td>
                                                <td align="left" width="60%" class="matters" style="text-align: left">
                                                    <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList></td>
                                            </tr>
                                        </table>
                        </td>

                        <td align="left" class="matters"><span class="field-label">Section</span></td>
                        <td align="left" class="matters" style="text-align: left">

                            <div class="checkbox-list">
                                <asp:CheckBoxList ID="chkSec" runat="server" CssClass="matters">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" colspan="4">
                            <font color="red">Note:</font>By default all section will be listed if no checkbox selected </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4">
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="dayBook" /></td>
                    </tr>
                    </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

