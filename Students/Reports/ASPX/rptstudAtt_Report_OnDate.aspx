<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptstudAtt_Report_OnDate.aspx.vb" Inherits="Students_studAttendance_Report" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Attendance Report Based On Date"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr align="left">
                        <td colspan="4">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook"></asp:ValidationSummary>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                         <td align="left" class="matters" width="20%" ><span class="field-label">Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                          <td align="left" class="matters" width="20%"><span class="field-label" >Stream</span></td>
                                     <td align="left" class="matters" width="30%">  <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                        <td align="left" class="matters" width="20%" ><span class="field-label">Grade</span></td>
                        <td align="left" class="matters"  width="30%" >
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td align="left" class="matters" ><span class="field-label">Shift</span></td>
                        <td align="left" class="matters"  >
                            <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" ><span class="field-label">Section</span></td>
                        <td align="left" class="matters"  >
                            <asp:DropDownList ID="ddlSection" runat="server">
                            </asp:DropDownList></td>
                    </tr>

                    <tr>
                        <td align="left" class="matters"  ><span class="field-label">From Date</span></td>
                        <td align="left" class="matters"  >
                            <asp:TextBox ID="txtFromDate" runat="server"  >
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False"
                                ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator
                                    ID="rfvFromDate" runat="server" ControlToValidate="txtFromDate" CssClass="error"
                                    Display="Dynamic" ErrorMessage="From Date required" ForeColor="" ValidationGroup="dayBook">*</asp:RequiredFieldValidator><br />
                            <span
                                 >(dd/mmm/yyyy)</span></td>

                        <td align="left" class="matters"><span class="field-label">To Date</span></td>
                        <td align="left" class="matters" style="text-align: left">
                            <asp:TextBox ID="txtToDate" runat="server">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False"
                                ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator
                                    ID="rfvToDate" runat="server" ControlToValidate="txtToDate" CssClass="error"
                                    Display="Dynamic" ErrorMessage="To Date required" ForeColor="" ValidationGroup="dayBook">*</asp:RequiredFieldValidator><br />
                            <span >(dd/mmm/yyyy)</span></td>
                    </tr>


                      <tr>
                        <td align="left" class="matters" ><span class="field-label">House</span></td>
                        <td align="left" class="matters"  >
                            <asp:DropDownList ID="ddlHouse" runat="server">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" ><span class="field-label">Nationality</span></td>
                        <td align="left" class="matters"  >
                            <asp:DropDownList ID="ddlNationality" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" ><span class="field-label">Gender</span>                           
                        </td>
                        <td align="left" class="matters"  >
                             <asp:RadioButton ID="rbGndrAll" runat="server" GroupName="gender" Text="All" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rbMale" runat="server" GroupName="gender" Text="Male" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rbFemale" runat="server" GroupName="gender" Text="Female" CssClass="field-label"></asp:RadioButton>
                        </td>
                        <td align="left" class="matters" ><span class="field-label"></span></td>
                        <td align="left" class="matters"  ></td>
                    </tr>



                    <tr>
                        <td   colspan="4" class="title-bg">
                            Filter By Percentage 
                        </td>
                    </tr>
                    <tr  >
                        <td align="left" class="matters" colspan="2">
                            <asp:RadioButton ID="rbIgn" runat="server" GroupName="days" Text="Ignore" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rbEqual" runat="server" GroupName="days" Text="Equal" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rbGreaterThan" runat="server" GroupName="days" Text="Greater and Equal To" CssClass="field-label"></asp:RadioButton>
                            <asp:RadioButton ID="rbLessThan" runat="server" GroupName="days" Text="Less and Equal To" CssClass="field-label"></asp:RadioButton></td>
                        <td align="left" class="matters"  ><span class="field-label">Percentage</span></td>
                        <td align="left" class="matters"  >
                            <asp:TextBox ID="txtPer" runat="server" MaxLength="5"  ></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="matters" colspan="4"  >
                            <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                ValidationGroup="dayBook" /></td>
                    </tr>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="txtPer" FilterMode="ValidChars">
                    </ajaxToolkit:FilteredTextBoxExtender>
                    <ajaxToolkit:CalendarExtender ID="CBEfdate"
                        runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                    </ajaxToolkit:CalendarExtender>
                    <ajaxToolkit:CalendarExtender ID="cbetdate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgToDate"
                        TargetControlID="txtTodate">
                    </ajaxToolkit:CalendarExtender>
                </table>

            </div>
        </div>
    </div>
</asp:Content>

