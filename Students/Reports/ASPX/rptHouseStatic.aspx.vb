Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO

Partial Class Students_Reports_ASPX_rptHouseStatic
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200045") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights

                callYEAR_DESCRBind()
                callCurrent_BsuShift()
                callGrade_ACDBind()
                'callGrade_Section()

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            CallReport()
        End If
    End Sub
    Public Sub callCurrent_BsuShift()
        Try

            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), Session("Current_ACD_ID"))
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlShift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim FromTo_Date As String = ""
        Dim i As Integer = 0
        Dim H_ID As String = String.Empty
        Dim H_Descr As String = String.Empty
        Dim H_Color As String = String.Empty
        Dim House_ID1, House_ID2, House_ID3, House_ID4 As String
        Dim House_Descr1, House_Descr2, House_Descr3, House_Descr4 As String
        Dim House_Color1, House_Color3, House_Color4 As String
        Dim House_Color2 As String

        Dim House_det As STU_HOUSEDET()
        House_det = getBSU_HouseDetail()

        If Not House_det Is Nothing Then
            If House_det.Length > 3 Then

                House_ID1 = House_det(0).House_ID
                House_ID2 = House_det(1).House_ID
                House_ID3 = House_det(2).House_ID
                House_ID4 = House_det(3).House_ID

                House_Descr1 = House_det(0).House_Descr
                House_Descr2 = House_det(1).House_Descr
                House_Descr3 = House_det(2).House_Descr
                House_Descr4 = House_det(3).House_Descr

                House_Color1 = House_det(0).House_Color
                House_Color2 = House_det(1).House_Color
                House_Color3 = House_det(2).House_Color
                House_Color4 = House_det(3).House_Color

            End If
            'For icount As Integer = 0 To House_det.Length - 1
            '    House_det(icount).House_Color
            'Next
        End If





        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        'param.Add("@BSU_ID", Session("sbsuid"))
        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@Grade", Nothing)
        Else
            param.Add("@Grade", ddlGrade.SelectedItem.Value)
        End If

        If ddlSection.SelectedItem.Value = "0" Or ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@Section", Nothing)
        Else
            param.Add("@Section", ddlSection.SelectedItem.Value)
        End If

        param.Add("@ACCYEAR", ddlAcademicYear.SelectedItem.Value)

        param.Add("@House1", House_ID1)
        param.Add("@House2", House_ID2)
        param.Add("@House3", House_ID3)
        param.Add("@House4", House_ID4)

        param.Add("House_Descr1", House_Descr1)
        param.Add("House_Descr2", House_Descr2)
        param.Add("House_Descr3", House_Descr3)
        param.Add("House_Descr4", House_Descr4)



        'House_Color1 = Convert.ToInt64(Mid(House_Color1, 2), 16)
        'House_Color2 = Convert.ToInt64(Mid(House_Color2, 2), 16)
        'House_Color3 = Convert.ToInt64(Mid(House_Color3, 2), 16)
        'House_Color4 = Convert.ToInt64(Mid(House_Color4, 2), 16)


        House_Color1 = HexToDec(Mid(House_Color1, 2, 2)) & "," & HexToDec(Mid(House_Color1, 4, 2)) & "," & HexToDec(Mid(House_Color1, 6, 2))
        House_Color2 = HexToDec(Mid(House_Color2, 2, 2)) & "," & HexToDec(Mid(House_Color2, 4, 2)) & "," & HexToDec(Mid(House_Color2, 6, 2))

        House_Color3 = HexToDec(Mid(House_Color3, 2, 2)) & "," & HexToDec(Mid(House_Color3, 4, 2)) & "," & HexToDec(Mid(House_Color3, 6, 2))
        House_Color4 = HexToDec(Mid(House_Color4, 2, 2)) & "," & HexToDec(Mid(House_Color4, 4, 2)) & "," & HexToDec(Mid(House_Color4, 6, 2))

        param.Add("House_Color1", House_Color1)

        param.Add("House_Color2", House_Color2)
        param.Add("House_Color3", House_Color3)
        param.Add("House_Color4", House_Color4)


        param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        ' param.Add("Grade_Dis", ddlGrade.SelectedItem.Text)
        '' added by nahyan
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        '' added by nahyan
        param.Add("@Asondate", Format(Date.Parse(txtAsondate.Text), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))




        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If ViewState("MainMnu_code") = "S200045" Then
                .reportPath = Server.MapPath("../RPT/rptHouseStatistics.rpt")

            End If

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    'Function HexToDec1(ByVal value As String) As Long
    '    ' we just need a call to the Convert.ToInt64 static method
    '    Return Convert.ToInt64(value, 16)
    'End Function

    Private Function HexToDec(ByVal Input As String) As String
        Dim a As Integer
        Dim b As Integer


        Select Case Mid(Input, 1, 1)
            Case "A"
                a = 10
            Case "B"
                a = 11
            Case "C"
                a = 12
            Case "D"
                a = 13
            Case "E"
                a = 14
            Case "F"
                a = 15
            Case Else
                a = 0
        End Select

        Select Case Mid(Input, 2, 1)
            Case "A"
                b = 10
            Case "B"
                b = 11
            Case "C"
                b = 12
            Case "D"
                b = 13
            Case "E"
                b = 14
            Case "F"
                b = 15
            Case Else
                b = 0
        End Select

        HexToDec = (a * 16) + b


    End Function
    Private Function getBSU_HouseDetail() As STU_HOUSEDET()
        Try
            Dim stud_det As STU_HOUSEDET()
            Dim i As Integer = 0
            Using BSU_HOUSEReader As SqlDataReader = AccessStudentClass.GetBSU_House((Session("sBsuid")))
                If BSU_HOUSEReader.HasRows = True Then
                    While BSU_HOUSEReader.Read
                        ReDim Preserve stud_det(i)
                        stud_det(i) = New STU_HOUSEDET
                        stud_det(i).House_ID = BSU_HOUSEReader("HideID")
                        stud_det(i).House_Descr = Convert.ToString(BSU_HOUSEReader("ID"))
                        stud_det(i).House_Color = Convert.ToString(BSU_HOUSEReader("HColor"))
                        i += 1
                    End While
                End If
            End Using
            Return stud_det
        Catch ex As Exception
            Return Nothing
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function

    'Private Sub getBSU_HouseDetail(ByRef H_ID As String, ByRef H_Descr As String, ByRef H_Color As String)
    '    Try


    '        Using BSU_HOUSEReader As SqlDataReader = AccessStudentClass.GetBSU_House((Session("sBsuid")))

    '            If BSU_HOUSEReader.HasRows = True Then
    '                While BSU_HOUSEReader.Read
    '                    H_ID = H_ID & Convert.ToString(BSU_HOUSEReader("HideID")) & "-"
    '                    H_Descr = H_Descr & Convert.ToString(BSU_HOUSEReader("ID")) & "-"
    '                    H_Color = H_Color & Convert.ToString(BSU_HOUSEReader("HColor")) & "-"
    '                End While
    '            End If
    '        End Using

    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            If ddlAcademicYear.SelectedIndex <> -1 Then
                ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
                ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_ACDBind()
        Try

            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ddlAcademicYear.SelectedItem.Value, Session("CLM"), ddlShift.SelectedValue)
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While
                End If
            End Using
            If ddlGrade.SelectedIndex <> -1 Then
                ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

   
    Public Sub callGrade_Section()
        Try
           
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex <> -1 Then

                If ddlGrade.SelectedItem.Value = "0" Then
                    GRD_ID = ""
                Else
                    GRD_ID = ddlGrade.SelectedItem.Value
                End If
            End If

            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ddlAcademicYear.SelectedItem.Value, GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub
   
  
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub
End Class

Class STU_HOUSEDET
    Public House_ID As String
    Public House_Descr As String
    Public House_Color As String
End Class