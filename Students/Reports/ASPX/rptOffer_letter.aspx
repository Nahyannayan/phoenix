<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptOffer_letter.aspx.vb" Inherits="Students_Reports_ASPX_rptAgeStatistics" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Select Date Range"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" cellpadding="5" cellspacing="0"
                                style="width: 100%">
                                <%-- <tr class="subheader_img">
                        <td align="center" colspan="8" style="height: 1px" valign="middle">
                            <div align="left">
                                <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2">
                                    <asp:Label ID="lblCaption" runat="server" Text="Select Date Range"></asp:Label></font>&nbsp;</div>
                        </td>
                    </tr>--%>
                                <tr>
                                    <td align="left">As on</td>

                                    <td align="left" style="text-align: left">

                                        <br />
                                        (dd/mmm/yyyy)</td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" >
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</asp:Content>

