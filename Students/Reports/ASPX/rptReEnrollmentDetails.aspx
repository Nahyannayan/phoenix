﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptReEnrollmentDetails.aspx.vb" Inherits="Students_Reports_ASPX_rptReEnrollmentDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
         
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Re Enrollment Details Report"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_AddGroup" runat="server" align="center" width="100%">
                    <tr>
                        <td align="center" valign="bottom" style="height: 20px; width: 516px;">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error" Width="133px" Style="text-align: center"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="Table2" runat="server" align="center" width="100%">
                                <tr id="AcademicYear" runat="server">
                                    <td align="left">Accademic Year
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Date From</span>
                                    </td>
                                    <td align="left" width="30%">

                                        <asp:TextBox ID="txtCDate" runat="server"></asp:TextBox>

                                        <asp:ImageButton
                                            ID="imgCDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgCDate" TargetControlID="txtCDate">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                    <td align="left" width="20%"><span class="field-label">Date To</span>
                                    </td>
                                    <td align="left" width="30%">

                                        <asp:TextBox ID="txtSDate" runat="server" Width="100px"></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgSDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgSDate" TargetControlID="txtSDate">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>

                                </tr>

                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btReport" runat="server" CssClass="button" Text="Generate Report" ValidationGroup="groupM1"/>

                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfbDownload" runat="server" />
    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
</asp:Content>


