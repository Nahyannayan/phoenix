Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class StudAtt_daily_all
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059058") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                txtTillDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                callYEAR_DESCRBind()
                ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    'Public Sub callGrade_ACDBind()
    '    Try
    '        Dim ACD_ID As String = String.Empty
    '        If ddlAcademicYear.SelectedIndex = -1 Then
    '            ACD_ID = ""
    '        Else
    '            ACD_ID = ddlAcademicYear.SelectedItem.Value
    '        End If
    '        Dim di As ListItem
    '        Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
    '            ddlGrade.Items.Clear()
    '            di = New ListItem("ALL", "0")
    '            ddlGrade.Items.Add(di)
    '            If Grade_ACDReader.HasRows = True Then
    '                While Grade_ACDReader.Read
    '                    di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
    '                    ddlGrade.Items.Add(di)
    '                End While

    '            End If
    '        End Using
    '        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)

    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub
    Public Sub callStream_Bind()
        Try
            ddlStream.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub
    Sub BindGrade()
        Try
            ddlGrade.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddlGrade.Items.Add(di)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        BindGrade()

    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_TEMP_STM(Session("sBsuid"), ACD_ID, GRD_ID, ddlStream.SelectedValue)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If Page.IsValid Then
            CallReport()
        End If

    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@GRD_ID", Nothing)
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        End If

        If ddlSection.SelectedItem.Value = "0" Then
            param.Add("@SCT_ID", Nothing)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedValue)
        End If

        param.Add("@TODATE", Format(Date.Parse(txtTillDate.Text), "dd/MMM/yyyy"))
        param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        Dim Displaydate As Date = Format(Date.Parse(txtTillDate.Text), "dd/MMM/yyyy")

        param.Add("DisplayDate", "Date : " & String.Format("{0:" & OASISConstants.DateFormat & "}", Displaydate))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptStudATT_Daily_all.rpt")

        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callStream_Bind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub


End Class
