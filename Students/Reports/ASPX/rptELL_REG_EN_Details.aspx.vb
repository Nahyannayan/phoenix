Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports GemBox.Spreadsheet
Imports ResponseHelper
Partial Class Students_rptELL_REG_EN_Details
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S108105") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                getCurriculum()
                Call bindAcademic_Year()
                Session("SL_ID_LIST") = Nothing
                rbEnq.Checked = True
                ddlSection.Enabled = False
                getMainLoc()
                GetLocation_Bind()
                BIND_CALLBACK_TIME()
                Call gridbind()

                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Session("SL_ID_LIST") = Nothing

        gridbind()
        'If Page.IsValid Then
        '    CallReport()
        'End If
    End Sub
    Private Sub gridbind()
        Try

            Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim acd_id As String = String.Empty
            Dim GRD_ID As String = String.Empty
            Dim SCT_ID As String = String.Empty


            Dim Info_bEnq As Boolean
            If rbEnq.Checked = True Then
                Info_bEnq = True
            ElseIf rbEnroll.Checked = True Then
                Info_bEnq = False
            End If




            Dim ds As DataSet
            Dim PARAM(18) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(2) = New SqlParameter("@INFO_bENQ", Info_bEnq)

            PARAM(3) = New SqlParameter("@SNAME", IIf(txtSname.Text.Trim <> "", txtSname.Text.Trim, System.DBNull.Value))
            PARAM(4) = New SqlParameter("@PNAME", IIf(txtPNAME.Text.Trim <> "", txtPNAME.Text.Trim, System.DBNull.Value))
            PARAM(5) = New SqlParameter("@GRD_ID", IIf(ddlGrade.SelectedValue <> "0", ddlGrade.SelectedValue, System.DBNull.Value))
            PARAM(6) = New SqlParameter("@SCT_ID", IIf(ddlSection.SelectedValue <> "0", ddlSection.SelectedValue, System.DBNull.Value))
            PARAM(7) = New SqlParameter("@PEMAIL", IIf(txtPEMAIL.Text.Trim <> "", txtPEMAIL.Text.Trim, System.DBNull.Value))
            PARAM(8) = New SqlParameter("@APPLNO", IIf(txtRegNo.Text.Trim <> "", txtRegNo.Text.Trim, System.DBNull.Value))
            PARAM(9) = New SqlParameter("@PMOB_NO", IIf(txtPMOB_NO.Text.Trim <> "", txtPMOB_NO.Text.Trim, System.DBNull.Value))
            PARAM(10) = New SqlParameter("@REG_DATE", IIf(txtRegDT.Text.Trim <> "", txtRegDT.Text.Trim, System.DBNull.Value))
            PARAM(11) = New SqlParameter("@PASPRTNO", IIf(txtPassNo.Text.Trim <> "", txtPassNo.Text.Trim, System.DBNull.Value))
            PARAM(12) = New SqlParameter("@LM_ID", IIf(ddlLocM.SelectedValue <> "0", ddlLocM.SelectedValue, System.DBNull.Value))
            PARAM(13) = New SqlParameter("@LS_BSU_ID", IIf(ddlLocS.SelectedValue <> "0", ddlLocS.SelectedValue, System.DBNull.Value))
            PARAM(14) = New SqlParameter("@TF_ID", GetBlock()) 'IIf(ddlBlock.SelectedValue <> "0", ddlBlock.SelectedValue, System.DBNull.Value))
            PARAM(15) = New SqlParameter("@CLM_ID", ddlCurriculum.SelectedValue)
            PARAM(16) = New SqlParameter("@LANG", IIf(ddlCourse.SelectedValue <> "0", ddlCourse.SelectedValue, System.DBNull.Value))
            PARAM(17) = New SqlParameter("@CALLBACK_ID", ddlContacttime.SelectedValue)

            ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.StoredProcedure, "ELL.GETELL_STUDENT_DETAILS_GRIDBIND", PARAM)


            If ds.Tables(0).Rows.Count > 0 Then
                gvELL.DataSource = ds
                gvELL.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                'start the count from 1 no matter gridcolumn is visible or not
                ' ds.Tables(0).Rows(0)(6) = True
                gvELL.DataSource = ds.Tables(0)
                Try
                    gvELL.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvELL.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvELL.Rows(0).Cells.Clear()
                gvELL.Rows(0).Cells.Add(New TableCell)
                gvELL.Rows(0).Cells(0).ColumnSpan = columnCount
                gvELL.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvELL.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                'BindNull()

            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvELL_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvELL.PageIndexChanged
        Dim hash As New Hashtable
        If Not Session("SL_ID_LIST") Is Nothing Then
            hash = Session("SL_ID_LIST")
        End If


        Dim chk As CheckBox
        Dim SL_ID As String = String.Empty
        For Each rowItem As GridViewRow In gvELL.Rows
            chk = DirectCast(rowItem.FindControl("ch1"), CheckBox)

            SL_ID = DirectCast(rowItem.FindControl("lblSL_ID"), Label).Text
            If hash.ContainsValue(SL_ID) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next

    End Sub

    Protected Sub gvELL_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            gvELL.PageIndex = e.NewPageIndex
            Dim hash As New Hashtable
            If Not Session("SL_ID_LIST") Is Nothing Then
                hash = Session("SL_ID_LIST")
            End If


            Dim chk As CheckBox
            Dim SL_ID As String = String.Empty
            For Each rowItem As GridViewRow In gvELL.Rows
                ' chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
                chk = DirectCast(rowItem.FindControl("ch1"), CheckBox)
                'DirectCast(row.FindControl("lblEnqId"), Label).Text)
                SL_ID = DirectCast(rowItem.FindControl("lblSL_ID"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(SL_ID) = False Then
                        hash.Add(SL_ID, DirectCast(rowItem.FindControl("lblSL_ID"), Label).Text)
                    End If
                Else
                    If hash.Contains(SL_ID) = True Then
                        hash.Remove(SL_ID)
                    End If
                End If
            Next
            Session("SL_ID_LIST") = hash
            gridbind()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnGen_detail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGen_detail.Click
        CallReport(True)
    End Sub
    Protected Sub btnGen_List_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGen_List.Click
        CallReport(False)
    End Sub
    Sub CallReport(ByVal bDetail As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim chk As CheckBox
        Dim SL_ID As String = String.Empty
        Dim SL_IDS As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("SL_ID_LIST") Is Nothing Then
            hash = Session("SL_ID_LIST")
        End If
        For Each rowItem As GridViewRow In gvELL.Rows

            chk = DirectCast(rowItem.FindControl("ch1"), CheckBox)

            SL_ID = DirectCast(rowItem.FindControl("lblSL_ID"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(SL_ID) = False Then
                    hash.Add(SL_ID, DirectCast(rowItem.FindControl("lblSL_ID"), Label).Text)
                End If
            Else
                If hash.Contains(SL_ID) = True Then
                    hash.Remove(SL_ID)
                End If
            End If

        Next
        Session("SL_ID_LIST") = hash

        If Not Session("SL_ID_LIST") Is Nothing Then
            hash = Session("SL_ID_LIST")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                SL_IDS += hashloop.Value & "|"
            Next
        End If
        If SL_IDS <> "" Then
            lblError.Text = ""
            Dim param As New Hashtable
            param.Add("@BSU_ID", Session("sbsuid"))
            param.Add("@INFO_bENQ", IIf(rbEnq.Checked = True, True, False))
            param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
            param.Add("@SL_IDS", SL_IDS)
            param.Add("@CLM_ID", ddlCurriculum.SelectedValue)
            param.Add("@LANG", ddlCourse.SelectedValue)
            param.Add("Info_Type", IIf(rbEnq.Checked = True, "ENQ", "ENR"))
            param.Add("IMG_TYPE", "LOGO")
            param.Add("UserName", Session("sUsr_name"))
            param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = param
                If bDetail = True Then
                    .reportPath = Server.MapPath("../RPT/rptELL_REG_STU_Detail.rpt")
                Else
                    .reportPath = Server.MapPath("../RPT/rptELL_REG_STU_Short.rpt")

                End If

            End With
            Session("rptClass") = rptClass

            '   Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
            ReportLoadSelection()
        Else
            lblError.Text = "Please select the records to be printed !!"
        End If
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Function GetCurrentACY_DESCR(ByVal ACD_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "SELECT  [ACY_DESCR]  FROM [ACADEMICYEAR_M] where ACY_ID=(SELECT [ACD_ACY_ID]   FROM [ACADEMICYEAR_D] where ACD_ID='" & ACD_ID & "')"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        bindAcademic_Section()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Call bindAcademic_Grade()
        Call bindAcademic_Block()
        gridbind()
    End Sub


    Private Sub getCurriculum()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String = "SELECT CLM_ID,upper(CLM_DESCR)CLM_DESCR  FROM  [OASIS].[DBO].[ACADEMICYEAR_D] INNER JOIN CURRICULUM_M ON ACD_CLM_ID=CLM_ID " _
                                 & " WHERE ACD_BSU_ID='" & Session("sbsuid").ToString & "' AND ACD_CURRENT=1"
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, strQuery)
            If DATAREADER.HasRows = True Then
                ddlCurriculum.DataSource = DATAREADER
                ddlCurriculum.DataTextField = "CLM_DESCR"
                ddlCurriculum.DataValueField = "CLM_ID"
                ddlCurriculum.DataBind()
            End If
        End Using
        ddlCurriculum.Items.Add(New ListItem("ALL", "0"))
        ddlCurriculum.ClearSelection()
        ddlCurriculum.Items.FindByValue("0").Selected = True

    End Sub
    Private Sub getMainLoc()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String = "select LM_ID,LM_NAME FROM  ELL.LOCATION_M WITH(NOLOCK) ORDER BY LM_NAME"
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, strQuery)
            If DATAREADER.HasRows = True Then
                ddlLocM.DataSource = DATAREADER
                ddlLocM.DataTextField = "LM_NAME"
                ddlLocM.DataValueField = "LM_ID"
                ddlLocM.DataBind()
            End If
        End Using
        ddlLocM.Items.Add(New ListItem("All", "0"))
        ddlLocM.ClearSelection()
        ddlLocM.Items.FindByValue("0").Selected = True
        ddlLocM_SelectedIndexChanged(ddlLocM, Nothing)
    End Sub
    Private Sub GetLocation_Bind()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlParameter

        param(0) = New SqlParameter("@INFO_FOR", "OASIS")
        param(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ELL.GETLOCATION_BIND_V2", param)
            If datareader.HasRows = True Then
                ddlLoc.DataSource = datareader
                ddlLoc.DataTextField = "LOC_NAME"
                ddlLoc.DataValueField = "LS_ID"
                ddlLoc.DataBind()
            End If
        End Using
        'ddlLoc.Items.Add(New ListItem("Select", "0"))
        'ddlLoc.ClearSelection()
        'ddlLoc.Items.FindByValue("0").Selected = True
    End Sub

    Private Sub getSubLoc()
        ddlLocS.Items.Clear()
        Dim conn As String = ConnectionManger.GetOASISConnectionString

        '        Dim strQuery As String = "SELECT LS_ID,BSU_NAME FROM  ELL.LOCATION_M WITH(NOLOCK) INNER JOIN ELL.LOCATION_S WITH(NOLOCK)" & _
        '" ON LM_ID=LS_LM_ID INNER JOIN BUSINESSUNIT_M  WITH(NOLOCK) ON BSU_ID=LS_BSU_ID WHERE LM_ID ='" & ddlLocM.SelectedValue & "' ORDER BY LM_NAME,BSU_NAME"
        '        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, strQuery)
        '            If DATAREADER.HasRows = True Then
        '                ddlLocS.DataSource = DATAREADER
        '                ddlLocS.DataTextField = "BSU_NAME"
        '                ddlLocS.DataValueField = "LS_ID"
        '                ddlLocS.DataBind()
        '            End If
        '        End Using

        Dim strQuery As String = "SELECT DISTINCT(LS_BSU_ID),BSU_NAME FROM   ELL.LOCATION_S WITH(NOLOCK)" _
                                        & " INNER JOIN BUSINESSUNIT_M  WITH(NOLOCK) ON BSU_ID=LS_BSU_ID WHERE " _
                                       & " LS_LM_ID ='" & ddlLocM.SelectedValue & "' "

        If ddlCurriculum.SelectedValue = "0" Then
            strQuery = strQuery + "  AND LS_ACD_ID IN (SELECT ACD_ID FROM ACADEMICYEAR_D WHERE ACD_BSU_ID='" & Session("sbsuid").ToString & "' AND ACD_CURRENT=1)"
        Else
            strQuery = strQuery + "  AND LS_ACD_ID=" & ddlAcademicYear.SelectedValue
        End If

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, strQuery)
            If DATAREADER.HasRows = True Then
                ddlLocS.DataSource = DATAREADER
                ddlLocS.DataTextField = "BSU_NAME"
                ddlLocS.DataValueField = "LS_BSU_ID"
                ddlLocS.DataBind()
            End If
        End Using




        ddlLocS.Items.Add(New ListItem("All", "0"))
        ddlLocS.ClearSelection()
        ddlLocS.Items.FindByValue("0").Selected = True


    End Sub
    Sub BIND_CALLBACK_TIME()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT PT_ID,PT_DESCR FROM ELL.PREFERED_TIME where  PT_ID<>0 "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)


        ddlContacttime.DataSource = ds
        ddlContacttime.DataTextField = "PT_DESCR"
        ddlContacttime.DataValueField = "PT_ID"
        ddlContacttime.DataBind()

        ddlContacttime.Items.Add(New ListItem("NA", "0"))


        ddlContacttime.Items.Add(New ListItem("All", "-1"))
        ddlContacttime.ClearSelection()
        ddlContacttime.Items.FindByValue("-1").Selected = True
        

    End Sub
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            If ddlCurriculum.SelectedValue = 0 Then
                str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id ='" & Session("sbsuid").ToString & "' AND ACD_CLM_ID='" & Session("CLM") & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            Else
                str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id ='" & Session("sbsuid").ToString & "' AND ACD_CLM_ID='" & ddlCurriculum.SelectedValue & "' ORDER BY ACADEMICYEAR_D.ACD_ACY_ID"
            End If
            
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcademicYear.Items.Clear()
            ddlAcademicYear.DataSource = ds.Tables(0)
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACD_ID"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim ds As New DataSet
            str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGrade.Items.Clear()

            If ds.Tables(0).Rows.Count > 0 Then
                ddlGrade.DataSource = ds.Tables(0)
                ddlGrade.DataTextField = "GRM_DISPLAY"
                ddlGrade.DataValueField = "GRD_ID"
                ddlGrade.DataBind()
            End If
            ddlGrade.Items.Add(New ListItem("All", "0"))
            ddlGrade.Items.FindByValue("0").Selected = True
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Block()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim ds As New DataSet
            str_Sql = "select TF_ID,TF_WEEK_DESCR from ELL.TUITION_FEE WITH(NOLOCK) WHERE TF_ACD_ID='" & ACD_ID & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlBlock.Items.Clear()

            If ds.Tables(0).Rows.Count > 0 Then
                ddlBlock.DataSource = ds.Tables(0)
                ddlBlock.DataTextField = "TF_WEEK_DESCR"
                ddlBlock.DataValueField = "TF_ID"
                ddlBlock.DataBind()
            End If
            'ddlBlock.Items.Add(New ListItem("All", "0"))
            'ddlBlock.Items.FindByValue("0").Selected = True

            Dim i As Integer
            For i = 0 To ddlBlock.Items.Count
                If ddlBlock.Items(i).Text = "Spring" Then
                    ddlBlock.Items(i).Selected = False
                Else
                    ddlBlock.Items(i).Selected = True
                End If

            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function GetBlock() As String
        Dim i As Integer, str As String = ""
        For i = 0 To ddlBlock.Items.Count - 1
            If ddlBlock.Items(i).Selected = True Then
                If str = "" Then
                    str = ddlBlock.Items(i).Text
                Else
                    str = str + "|" + ddlBlock.Items(i).Text
                End If
            End If

        Next
        Return str
    End Function
    Sub bindAcademic_Section()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim GRD_ID As String
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = "0"
            End If


            Dim str_Sql As String = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
 " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID  AND GRADE_BSU_M.GRM_ID=SECTION_M.SCT_GRM_ID " & _
" WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND" & _
 " (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "')  and SECTION_M.SCT_descr<>'TEMP' order by SECTION_M.SCT_DESCR "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSection.Items.Clear()
            ddlSection.Items.Add(New ListItem("All", "0"))
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
            End If
            ddlSection.Items.FindByValue("0").Selected = True
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub



    Protected Sub rbEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnq.CheckedChanged

        If rbEnq.Checked = True Then
            ddlSection.ClearSelection()
            ddlSection.Enabled = False
            ddlSection.Items.FindByValue("0").Selected = True

        End If

        gridbind()

    End Sub

    Protected Sub rbEnroll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnroll.CheckedChanged
        If rbEnroll.Checked = True Then
            ddlSection.ClearSelection()
            ddlSection.Enabled = True
            ddlSection.Items.FindByValue("0").Selected = True

        End If
        gridbind()
    End Sub



    Protected Sub ddlLocM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocM.SelectedIndexChanged
        getSubLoc()
    End Sub

    Protected Sub chkLoc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkLoc.CheckedChanged
        If chkLoc.Checked = True Then
            btnUpdateLoc.Visible = True
            ddlLoc.Visible = True
            HideShow_ChkAll(False)
        Else
            btnUpdateLoc.Visible = False
            ddlLoc.Visible = False
            HideShow_ChkAll(True)
        End If

    End Sub
    Private Sub HideShow_ChkAll(ByVal Bshow As Boolean)
        Dim chkAll As CheckBox = DirectCast(gvELL.HeaderRow.FindControl("chkAll"), CheckBox)
        chkAll.Checked = False
        chkAll.Visible = Bshow

        For Each gv As GridViewRow In gvELL.Rows
            If gv.RowType = DataControlRowType.DataRow Then
                Dim ch1 As CheckBox = DirectCast(gv.FindControl("ch1"), CheckBox)
                ch1.Checked = False
            End If
        Next

    End Sub
    Protected Sub btnUpdateLoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateLoc.Click
        Call UpdateLoc()
    End Sub

    Sub UpdateLoc()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim chk As CheckBox
        Dim SL_ID As String = String.Empty
        Dim SL_IDS As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("SL_ID_LIST") Is Nothing Then
            hash = Session("SL_ID_LIST")
        End If
        For Each rowItem As GridViewRow In gvELL.Rows

            chk = DirectCast(rowItem.FindControl("ch1"), CheckBox)

            SL_ID = DirectCast(rowItem.FindControl("lblSL_ID"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(SL_ID) = False Then
                    hash.Add(SL_ID, DirectCast(rowItem.FindControl("lblSL_ID"), Label).Text)
                End If
            Else
                If hash.Contains(SL_ID) = True Then
                    hash.Remove(SL_ID)
                End If
            End If

        Next
        Session("SL_ID_LIST") = hash

        If Not Session("SL_ID_LIST") Is Nothing Then
            hash = Session("SL_ID_LIST")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                SL_IDS += hashloop.Value & "|"
            Next
        End If


        If SL_IDS <> "" Then

            Dim trans As SqlTransaction

            'code to check for duplicate entry in the enquiry form

            Dim ReturnFlag As Integer
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                trans = conn.BeginTransaction("SampleTransaction")

                Try


                    Dim param(6) As SqlParameter

                    param(0) = New SqlParameter("@BSU_ID", Session("sbsuid"))
                    param(1) = New SqlParameter("@INFO_bENQ", IIf(rbEnq.Checked = True, True, False))
                    param(2) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
                    param(3) = New SqlParameter("@SL_IDS", SL_IDS)
                    param(4) = New SqlParameter("@USR_ID", Session("sUsr_id"))
                    param(5) = New SqlParameter("@LS_ID", ddlLoc.SelectedValue)
                    param(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    param(6).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[ELL].[SaveStudent_Loc_Update]", param)
                    ReturnFlag = param(6).Value

                Catch ex As Exception
                    ReturnFlag = -1
                Finally
                    If ReturnFlag = 0 Then
                        trans.Commit()
                        chkLoc.Checked = False
                        chkLoc_CheckedChanged(chkLoc, Nothing)
                        gridbind()
                        lblError.Text = "Record updated successfully"


                    Else
                        trans.Rollback()
                        lblError.Text = "Error occured while saving."
                    End If
                End Try
            End Using


        Else

            lblError.Text = "Please select the records to be updated"
        End If
    End Sub
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")
    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim acd_id As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim SCT_ID As String = String.Empty

        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile


        Dim Info_bEnq As Boolean
        If rbEnq.Checked = True Then
            Info_bEnq = True
        ElseIf rbEnroll.Checked = True Then
            Info_bEnq = False
        End If




        Dim ds As DataSet
        Dim PARAM(18) As SqlParameter
        PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
        PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(2) = New SqlParameter("@INFO_bENQ", Info_bEnq)

        PARAM(3) = New SqlParameter("@SNAME", IIf(txtSname.Text.Trim <> "", txtSname.Text.Trim, System.DBNull.Value))
        PARAM(4) = New SqlParameter("@PNAME", IIf(txtPNAME.Text.Trim <> "", txtPNAME.Text.Trim, System.DBNull.Value))
        PARAM(5) = New SqlParameter("@GRD_ID", IIf(ddlGrade.SelectedValue <> "0", ddlGrade.SelectedValue, System.DBNull.Value))
        PARAM(6) = New SqlParameter("@SCT_ID", IIf(ddlSection.SelectedValue <> "0", ddlSection.SelectedValue, System.DBNull.Value))
        PARAM(7) = New SqlParameter("@PEMAIL", IIf(txtPEMAIL.Text.Trim <> "", txtPEMAIL.Text.Trim, System.DBNull.Value))
        PARAM(8) = New SqlParameter("@APPLNO", IIf(txtRegNo.Text.Trim <> "", txtRegNo.Text.Trim, System.DBNull.Value))
        PARAM(9) = New SqlParameter("@PMOB_NO", IIf(txtPMOB_NO.Text.Trim <> "", txtPMOB_NO.Text.Trim, System.DBNull.Value))
        PARAM(10) = New SqlParameter("@REG_DATE", IIf(txtRegDT.Text.Trim <> "", txtRegDT.Text.Trim, System.DBNull.Value))
        PARAM(11) = New SqlParameter("@PASPRTNO", IIf(txtPassNo.Text.Trim <> "", txtPassNo.Text.Trim, System.DBNull.Value))
        PARAM(12) = New SqlParameter("@LM_ID", IIf(ddlLocM.SelectedValue <> "0", ddlLocM.SelectedValue, System.DBNull.Value))
        PARAM(13) = New SqlParameter("@LS_BSU_ID", IIf(ddlLocS.SelectedValue <> "0", ddlLocS.SelectedValue, System.DBNull.Value))
        PARAM(14) = New SqlParameter("@CLM_ID", ddlCurriculum.SelectedValue)
        PARAM(15) = New SqlParameter("@LANG", IIf(ddlCourse.SelectedValue <> "0", ddlCourse.SelectedValue, System.DBNull.Value))
        PARAM(16) = New SqlParameter("@TF_ID", GetBlock())

        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.StoredProcedure, "ELL.GETELL_STUDENT_DETAILS_EXPORT", PARAM)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_ELL_EXPORT")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            ' ws.HeadersFooters.AlignWithMargins = True


            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Date.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()


            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If

    End Sub

    Protected Sub gvELL_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvELL.RowCommand

        If e.CommandName = "Select" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument) - 1
            Dim selectedRow As GridViewRow = DirectCast(gvELL.Rows(index), GridViewRow)
            Dim lblSL_ID As New Label

            lblSL_ID = selectedRow.FindControl("lblSL_ID")
            ViewState("SL_ID") = lblSL_ID.Text
            lblPOPUPMsg.CssClass = ""
            lblPOPUPMsg.Text = ""
            gridbind_EditCourse(ViewState("SL_ID"))
            plCourseDetails.Visible = True

        End If
    End Sub

    Protected Sub gvELL_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvELL.RowDataBound

        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim str_Sql As String
                Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
                Dim ds As New DataSet
                str_Sql = "select TF_BLOCK_NO,TF_WEEK_DESCR from ELL.TUITION_FEE WITH(NOLOCK) WHERE TF_ACD_ID='" & ACD_ID & "'"
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
                Dim lblHeader As Label
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Select("TF_BLOCK_NO=1").Length > 0 Then
                        lblHeader = e.Row.FindControl("lblBlock1Header")
                        If Not lblHeader Is Nothing Then
                            lblHeader.Text = ds.Tables(0).Select("TF_BLOCK_NO=1")(0).Item(1)
                        End If
                    End If
                    If ds.Tables(0).Select("TF_BLOCK_NO=2").Length > 0 Then
                        lblHeader = e.Row.FindControl("lblBlock2Header")
                        If Not lblHeader Is Nothing Then
                            lblHeader.Text = ds.Tables(0).Select("TF_BLOCK_NO=2")(0).Item(1)
                        End If
                    End If
                    If ds.Tables(0).Select("TF_BLOCK_NO=3").Length > 0 Then
                        lblHeader = e.Row.FindControl("lblBlock3Header")
                        If Not lblHeader Is Nothing Then
                            lblHeader.Text = ds.Tables(0).Select("TF_BLOCK_NO=3")(0).Item(1)
                        End If
                    End If
                    If ds.Tables(0).Select("TF_BLOCK_NO=4").Length > 0 Then
                        lblHeader = e.Row.FindControl("lblBlock4Header")
                        If Not lblHeader Is Nothing Then
                            lblHeader.Text = ds.Tables(0).Select("TF_BLOCK_NO=4")(0).Item(1)
                        End If
                    End If
                End If
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim hfTotalFee As HiddenField = DirectCast(e.Row.FindControl("hfTotalFee"), HiddenField)

                Dim lblPaid As Label = DirectCast(e.Row.FindControl("lblPaid"), Label)
                Dim lblSNAME As LinkButton = DirectCast(e.Row.FindControl("lblSNAME"), LinkButton)

                If ((rbEnroll.Checked) And (Session("Current_ACD_ID") = ddlAcademicYear.SelectedValue)) Then

                    lblSNAME.Enabled = True
                    lblSNAME.ForeColor = System.Drawing.ColorTranslator.FromHtml("#A04141")
                    lblSNAME.ToolTip = "Clicking on this link to modify blocks"

                Else
                    lblSNAME.Enabled = False
                    lblSNAME.ForeColor = System.Drawing.ColorTranslator.FromHtml("#1B80B6")
                    lblSNAME.ToolTip = ""
                End If

                If Not hfTotalFee Is Nothing AndAlso Not lblPaid Is Nothing Then
                    If rbEnroll.Checked And (CDbl(lblPaid.Text) <> CDbl(hfTotalFee.Value)) Then
                        e.Row.ForeColor = Drawing.Color.DarkMagenta
                    End If
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlCurriculum_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurriculum.SelectedIndexChanged
        If ddlCurriculum.SelectedItem.Text.Contains("ARABIC") Then
            td1.Visible = True
            td3.Visible = True
            tdCur.ColSpan = "0"
        Else
            td1.Visible = False
            td3.Visible = False
            'tdCur.ColSpan = "4"
        End If
        bindAcademic_Year()
        bindAcademic_Block()
        gridbind()
    End Sub

    Private Sub gridbind_EditCourse(ByVal sl_id As Long)
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@SL_ID", sl_id)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ELL.GETENQ_COURSE_EDIT", param)
            If ds.Tables(0).Rows.Count > 0 Then
                gvFeeDetails.DataSource = ds.Tables(0)
                gvFeeDetails.DataBind()
            Else
                Try
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                    gvFeeDetails.DataSource = ds
                    gvFeeDetails.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvFeeDetails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvFeeDetails.Rows(0).Cells.Clear()
                gvFeeDetails.Rows(0).Cells.Add(New TableCell)
                gvFeeDetails.Rows(0).Cells(0).ColumnSpan = columnCount
                gvFeeDetails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvFeeDetails.Rows(0).Cells(0).Text = "No Dates Available !!!"
            End If
        Catch ex As Exception
        End Try

    End Sub
    Function Calltransaction_edit(ByRef errorMessage As String, ByVal TF_IDs As String) As Integer

        Dim STATUS As Integer = 0
        Dim FCT_ID As Integer = 0
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@TF_IDs", TF_IDs)
                pParms(1) = New SqlClient.SqlParameter("@SL_ID", ViewState("SL_ID"))
                pParms(2) = New SqlClient.SqlParameter("@USR_NAME", Session("sUsr_name"))
                pParms(3) = New SqlClient.SqlParameter("@FCT_ID", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.Output
                pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ELL].[SAVEELL_COURSE_EDIT_V2]", pParms)
                FCT_ID = pParms(3).Value
                Dim ReturnFlag As Integer = pParms(4).Value
                If ReturnFlag = 0 Then
                    If FCT_ID > 0 Then
                        ReturnFlag = SEND_FEE_DUES_EMAIL(transaction, FCT_ID, 1)
                    Else
                        ReturnFlag = SEND_FEE_DUES_EMAIL(transaction, FCT_ID, 0)
                    End If
                End If
                If ReturnFlag <> 0 Then
                    errorMessage = "Error occured while saving."
                    STATUS = 1
                End If


            Catch ex As Exception
                STATUS = 1
                errorMessage = "Error occured while saving."
            Finally
                If STATUS <> 0 Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""

                    transaction.Commit()



                End If
            End Try
        End Using
    End Function
    Private Function SEND_FEE_DUES_EMAIL(ByVal sqltran As SqlTransaction, ByVal FCT_ID As String, ByVal TYPE As Integer) As Integer

        Dim pParms(4) As SqlClient.SqlParameter
        Try
            pParms(0) = New SqlClient.SqlParameter("@SL_ID", ViewState("SL_ID"))
            pParms(1) = New SqlClient.SqlParameter("@FCT_ID_ENCRYPT", Encr_decrData.Encrypt(FCT_ID))
            pParms(2) = New SqlClient.SqlParameter("@TYPE", TYPE)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "ELL.SEND_FEE_DUES_EMAIL", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag
        Catch ex As Exception

        End Try

    End Function
    Protected Sub imgCourseDetailsClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCourseDetailsClose.Click
        lblPOPUPMsg.CssClass = ""
        lblPOPUPMsg.Text = ""
        plCourseDetails.Visible = False
    End Sub

    Protected Sub btnCloseCourse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseCourse.Click
        lblPOPUPMsg.CssClass = ""
        lblPOPUPMsg.Text = ""
        plCourseDetails.Visible = False
    End Sub
    Protected Sub btnUpdateCourse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateCourse.Click
        Dim Str_TF_ID As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim err As Integer = 0
        For Each gvrow As GridViewRow In gvFeeDetails.Rows

            If TryCast(gvrow.FindControl("chkSelect"), CheckBox).Checked = True Then
                Str_TF_ID += TryCast(gvrow.FindControl("lblTF_ID"), Label).Text & "|"
            End If
        Next

        If Str_TF_ID <> "" Then
            err = Calltransaction_edit(errorMessage, Str_TF_ID)
            If err = 0 Then

                lblPOPUPMsg.CssClass = "divvalidInnerPopup"
                lblPOPUPMsg.Text = "Selected block(s) updated successfully."

            Else
                lblPOPUPMsg.CssClass = "diverrorInnerPopup"
                lblPOPUPMsg.Text = errorMessage

            End If
        Else
            lblPOPUPMsg.CssClass = "divInfoInnerPopup"
            lblPOPUPMsg.Text = "Please select your preferred dates for the course."
        End If

    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Dim Str_TF_ID As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim err As Integer = 0
        For Each gvrow As GridViewRow In gvFeeDetails.Rows

            If TryCast(gvrow.FindControl("chkSelect"), CheckBox).Checked = True Then
                Str_TF_ID += TryCast(gvrow.FindControl("lblTF_ID"), Label).Text & "|"
            End If
        Next

        If Str_TF_ID <> "" Then
            err = Calltransaction_edit(errorMessage, Str_TF_ID)
            If err = 0 Then

                lblPOPUPMsg.CssClass = "divvalidInnerPopup"
                lblPOPUPMsg.Text = "Selected block(s) updated successfully."

            Else
                lblPOPUPMsg.CssClass = "diverrorInnerPopup"
                lblPOPUPMsg.Text = errorMessage

            End If
        Else
            lblPOPUPMsg.CssClass = "divInfoInnerPopup"
            lblPOPUPMsg.Text = "Please select your preferred dates for the course."
        End If
    End Sub
End Class
