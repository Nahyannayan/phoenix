<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptClassWiseStrength.aspx.vb" Inherits="Students_Reports_ASPX_rptClassWiseStrength" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
       

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
             <asp:Label ID="lblCaption" runat="server" Text="Classwise Strength"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" style="width: 100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" style="width: 100%">
                                <tr>
                                    
                                    <td align="left" style="width:10%"><span class="field-label">As on</span></td>
                                    <td align="left" style="text-align: left; width: 30%;">
                                        <asp:TextBox ID="txtAsOnDate" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgAsOnDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            />
                                        <ajaxToolkit:CalendarExtender ID="calAsOnDate" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgAsOnDate" TargetControlID="txtAsOnDate">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtAsOnDate"
                                            CssClass="error" Display="Dynamic" ErrorMessage="As on Date required" ForeColor=""
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator>
                                    </td>
                                     <td  style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" /></td>
                                </tr>                                
                            </table>
                            
                        </td>
                    </tr>
                </table>
                 
            </div>
        </div>
    </div>
</asp:Content>

