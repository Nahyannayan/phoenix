<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="rptRoomAtt_MthWise.aspx.vb" Inherits="studAtt_tilldate" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            <asp:Label ID="lblCaption" runat="server" Text="Month Wise Attendance"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td >
                            <table align="center" width="100%"
                                cellpadding="2" cellspacing="0">
                                
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>
                                    <td align="left"  style="text-align: left">
                                        <asp:DropDownList ID="ddlACD_ID" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                     <td align="left"  width="20%"><span class="field-label">Grade</span>
                                    </td>
                                    <td align="left"  style="text-align: left;">
                                        <asp:DropDownList ID="ddlGRD_ID" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  width="20%"><span class="field-label">Section</span>
                                    </td>
                                    <td align="left"  style="text-align: left;">
                                        <asp:DropDownList ID="ddlSct_id" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"  width="20%"><span class="field-label">Month</span>
                                    </td>
                                    <td align="left"  style="text-align: left">
                                        <asp:DropDownList ID="ddlMTH" runat="server">
                                            <asp:ListItem Value="1">January</asp:ListItem>
                                            <asp:ListItem Value="2">February</asp:ListItem>
                                            <asp:ListItem Value="3">March</asp:ListItem>
                                            <asp:ListItem Value="4">April</asp:ListItem>
                                            <asp:ListItem Value="5">May</asp:ListItem>
                                            <asp:ListItem Value="6">June</asp:ListItem>
                                            <asp:ListItem Value="7">July</asp:ListItem>
                                            <asp:ListItem Value="8">August</asp:ListItem>
                                            <asp:ListItem Value="9">September</asp:ListItem>
                                            <asp:ListItem Value="10">October</asp:ListItem>
                                            <asp:ListItem Value="11">November</asp:ListItem>
                                            <asp:ListItem Value="12">December</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        ValidationGroup="dayBook" />
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
