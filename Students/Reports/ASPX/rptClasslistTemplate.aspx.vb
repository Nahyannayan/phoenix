Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptClasslistTemplate
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200011" And ViewState("MainMnu_code") <> "S200015" And ViewState("MainMnu_code") <> "S200020" And ViewState("MainMnu_code") <> "S200025" And ViewState("MainMnu_code") <> "S200026" And ViewState("MainMnu_code") <> "S200491") And ViewState("MainMnu_code") <> "S200016" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                callYEAR_DESCRBind()
                callCurrent_BsuShift()
                callStream_Bind()
                callGrade_ACDBind()
                callGrade_Section()



                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                txtAsOnDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                'If Session("sBsuid") <> "125011" And Session("sBsuid") <> "125010" Then
                '    TR1.Visible = False
                'End If
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            ddlStream_SelectedIndexChanged(ddlStream, Nothing)
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim STM_ID As String = String.Empty
            If ddlStream.SelectedIndex = -1 Then
                STM_ID = ""
            Else
                STM_ID = ddlStream.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = GetGRM_GRD_ID_By_STM_ID(ACD_ID, Session("CLM"), STM_ID)
                ddlGrade.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlGrade.Items.Add(di)
                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callStream_Bind()
        'get_ddl_Stream_Registrar(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal SHF_ID As String)
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Stream_SectionReader As SqlDataReader = get_ddl_Stream_Registrar(ACD_ID, Session("CLM"), Session("sBsuid"), SHF_ID)
                ddlStream.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlStream.Items.Add(di)
                If Stream_SectionReader.HasRows = True Then
                    While Stream_SectionReader.Read
                        di = New ListItem(Stream_SectionReader("STM_DESCR"), Stream_SectionReader("STM_ID"))
                        ddlStream.Items.Add(di)
                    End While
                End If
            End Using
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim STM_ID As String = String.Empty
            If ddlStream.SelectedIndex = -1 Then
                STM_ID = ""
            Else
                STM_ID = ddlStream.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_STM_ID(Session("sBsuid"), ACD_ID, GRD_ID, STM_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), ACD_ID)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlShift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlShift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlShift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If Page.IsValid Then
            CallReport()
        End If

    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim FromTo_Date As String = ""
        Dim i As Integer = 0

        'If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
        '    FromTo_Date = " AND STUDENT_M.STU_DOJ BETWEEN '" + Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd") + "'"
        'End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("sbsuid"))
        'If ViewState("MainMnu_code") <> "S200491" Then
        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@Grade", Nothing)
        Else
            param.Add("@Grade", ddlGrade.SelectedItem.Value)
        End If
        'Else
        'param.Add("@Grade", ddlGrade.SelectedItem.Value)
        'End If


        If ddlSection.SelectedItem.Value = "0" Then
            param.Add("@Section", Nothing)
        Else
            param.Add("@Section", ddlSection.SelectedItem.Value)
        End If
        param.Add("@ACCYEAR", ddlAcademicYear.SelectedItem.Value)
        param.Add("@SHIFT", ddlShift.SelectedItem.Value)
        If ddlStream.SelectedItem.Value = "0" Then
            param.Add("@Stream", Nothing)
        Else
            param.Add("@Stream", ddlStream.SelectedItem.Value)
        End If

        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        If (ViewState("MainMnu_code") <> "S200016") Then
            param.Add("@AsOnDate", txtAsOnDate.Text)
            param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
            param.Add("Grade_Dis", ddlGrade.SelectedItem.Text)
            param.Add("Section_Dis", ddlSection.SelectedItem.Text)
        End If



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If ViewState("MainMnu_code") = "S200011" Then
                .reportPath = Server.MapPath("../RPT/rptClassListTemplate.rpt")
            ElseIf ViewState("MainMnu_code") = "S200016" Then
                .reportPath = Server.MapPath("../RPT/rptPrimarycontactdetails.rpt")
            ElseIf ViewState("MainMnu_code") = "S200015" Then
                .reportPath = Server.MapPath("../RPT/rptStudentList.rpt")
            ElseIf ViewState("MainMnu_code") = "S200020" Then
                .reportPath = Server.MapPath("../RPT/rptParentDetails.rpt")
            ElseIf ViewState("MainMnu_code") = "S200025" Then 'student details
                .reportPath = Server.MapPath("../RPT/rptStudentDetails.rpt")
            ElseIf ViewState("MainMnu_code") = "S200026" Then 'student details
                .reportPath = Server.MapPath("../RPT/rptStudentDetails_BusInfo.rpt")
            ElseIf ViewState("MainMnu_code") = "S200491" Then 'Observer details
                .reportPath = Server.MapPath("../RPT/rptObserverList.rpt")
            End If
            'file:///C:\Documents and Settings\lijo.joseph.GEMSEDUCATION\Desktop\oasis Project\OASISMAR16\Students\Reports\RPT\rptAdmission_Detail.rpt
        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callGrade_ACDBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Call callGrade_ACDBind()
    End Sub



    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        callGrade_ACDBind()
        Call callGrade_Section()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/phoenixbeta/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/phoenixbeta/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Public Shared Function get_ddl_Stream_Registrar(ByVal ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal SHF_ID As String) As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetPick_Stream As String

        sqlGetPick_Stream = " SELECT DISTINCT STREAM_M.STM_DESCR,GRADE_BSU_M.GRM_STM_ID AS STM_ID FROM GRADE_BSU_M " &
                            " INNER JOIN ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACY_ID = ACADEMICYEAR_D.ACD_ACY_ID AND GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID " &
                      " AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID " &
                      " AND GRADE_BSU_M.GRM_BSU_ID = SHIFTS_M.SHF_BSU_ID INNER JOIN STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID " &
                     " WHERE (ACADEMICYEAR_D.ACD_ID = '" + ACY_ID + "') AND (ACADEMICYEAR_D.ACD_CLM_ID = '" + CLM_ID + "') " &
                     " AND (ACADEMICYEAR_D.ACD_BSU_ID = '" + BSU_ID + "') AND GRADE_BSU_M.GRM_SHF_ID = '" + SHF_ID + "' ORDER BY STREAM_M.STM_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetPick_Stream, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function
    Public Shared Function GetGRM_GRD_ID_By_STM_ID(ByVal ACD_ID As String, ByVal CLM As String, ByVal STM_ID As String) As SqlDataReader
        ''''Check the related Function references


        'Author(--Lijo)
        'Date   --16/JAN/2008
        'Purpose--Get DRQ_GRM_GRD_ID data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetGRM_GRD_ID As String = ""

        sqlGetGRM_GRD_ID = "SELECT  distinct   GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY,GRADE_M.GRD_DISPLAYORDER as GRD_DISPLAYORDER " &
                        " FROM GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID left OUTER JOIN " &
                      " ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND " &
                     " GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' And GRADE_BSU_M.GRM_STM_ID ='" + STM_ID + "'  order by GRADE_M.GRD_DISPLAYORDER"



        '"SELECT GRADE_BSU_M.GRM_GRD_ID as GRD_ID, GRADE_BSU_M.GRM_DISPLAY as GRM_DISPLAY " & _
        '" FROM ACADEMICYEAR_D INNER JOIN  GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
        '                     " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where ACADEMICYEAR_D.ACD_CLM_ID = '" & CLM & "' And GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER"


        Dim command As SqlCommand = New SqlCommand(sqlGetGRM_GRD_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
End Class
