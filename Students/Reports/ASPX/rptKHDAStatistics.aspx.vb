Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports System.Web.Configuration
Imports system
Imports System.Collections.Generic
Imports System.Collections
Imports System.Xml
Imports GemBox.Spreadsheet
Imports ResponseHelper
Imports System.Data.SqlTypes

Partial Class Students_Reports_ASPX_rptKHDAStatistics
    Inherits System.Web.UI.Page
    '-------------------------------------------------------------------------------------------------
    ' Page Name     : KHDAStatiticsReport.aspx
    ' Purpose       : Criteria screen for KHDAStatiticsReport
    ' Created By    : Sajesh Elias
    ' Created Date  : 30-11-08
    ' Description   : 
    '-------------------------------------------------------------------------------------------------
    ' Module level variable declarations
    Dim Encr_decrData As New Encryption64
    Dim MainMnu_code As String
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            CallReport()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim lstrExportType As Integer

        Dim param(4) As SqlClient.SqlParameter




        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
        param(2) = New SqlClient.SqlParameter("@FROM_DATE", txtFromDate.Text) 'Format(Date.Parse(Now.Date), "dd/MMM/yyyy")
        param(3) = New SqlClient.SqlParameter("@TO_DATE", txtToDate.Text)
        param(4) = New SqlClient.SqlParameter("@ACYID", cmbAccadamicYear.SelectedValue)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU_KHDA_STATISTICS_INT_GENDER_COPY", param)
        Dim dtEXCEL As New DataTable
        dtEXCEL = ds.Tables(0)
        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("KHDA")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            ' ws.HeadersFooters.AlignWithMargins = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "BB_DATA.xlsx")

            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"

            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)

            Dim path = cvVirtualPath & "\StaffExport\" + pathSave
            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If



    End Sub


    Private Sub RetriveBusnsUnits()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query = ""
        str_query = "SELECT BSU_ID,BSU_NAME FROM dbo.BUSINESSUNIT_M "
        Dim dsSurveyBUSUnits As DataSet
        dsSurveyBUSUnits = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cmbBusUnits.DataSource = dsSurveyBUSUnits.Tables(0)
        cmbBusUnits.DataTextField = "BSU_NAME"
        cmbBusUnits.DataValueField = "BSU_ID"
        cmbBusUnits.DataBind()
    End Sub

    Private Sub RetriveAccadamicYear()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query = ""
        str_query = "SELECT ACY_ID,ACY_DESCR FROM dbo.ACADEMICYEAR_M "

        Dim dsACADEMICYEAR As DataSet
        dsACADEMICYEAR = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        cmbAccadamicYear.DataSource = dsACADEMICYEAR.Tables(0)
        cmbAccadamicYear.DataTextField = "ACY_DESCR"
        cmbAccadamicYear.DataValueField = "ACY_ID"
        cmbAccadamicYear.DataBind()

    End Sub

    Private Function GetAccadamicYearDetails(ByVal ACYID As Integer) As DataSet
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query = ""
        str_query = "SELECT ACD_ACY_ID,ACD_BSU_ID,ACD_STARTDT,ACD_ENDDT,ACD_CURRENT FROM dbo.ACADEMICYEAR_D WHERE ACD_ID= " & ACYID

        Dim dsAcdMicYear As DataSet
        dsAcdMicYear = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Return dsAcdMicYear
    End Function

    Private Sub CallReport()

        Dim str_bsu_ids As New StringBuilder
        Dim basedate As String = "15-09-" & Now.Year.ToString
        Dim DsAccYear As DataSet

        DsAccYear = GetAccadamicYearDetails(cmbAccadamicYear.SelectedValue)

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sBsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("sBsuid")) '999998--- Corp Office
        param.Add("@FROM_DATE", txtFromDate.Text) 'Format(Date.Parse(Now.Date), "dd/MMM/yyyy")
        param.Add("@TO_DATE", txtToDate.Text)
        param.Add("USERNAME", Session("sUsr_name"))
        param.Add("@RPTHEADER", "Academic Year : " + Session("F_Descr").ToString)
        param.Add("@ACYID", cmbAccadamicYear.SelectedValue)
        'Session("F_YEAR") = ddFinancialYear.SelectedItem.Value
        'Session("F_Descr") = ddFinancialYear.SelectedItem.Text
        'Session("F_TODT") = AccessRoleUser.GetFinancialYearDate(ddFinancialYear.SelectedItem.Value)

        'Format(Date.Parse(DsAccYear.Tables(0).Rows(0).Item(3)), "dd/MMM/yyyy")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptKHDAStatistics.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub CheckMenuRights()
        Dim Encr_decrData As New Encryption64
        Dim CurBsUnit As String = Session("sBsuid")
        Dim USR_NAME As String = Session("sUsr_name")
        If isPageExpired() Then
            Response.Redirect("expired.htm")
        Else
            Session("TimeStamp") = Now.ToString
            ViewState("TimeStamp") = Now.ToString

        End If

        Try
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "H000070") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Response.Redirect("~\noAccess.aspx")

        End Try
        ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                RetriveAccadamicYear()
                RetriveBusnsUnits()
                cmbBusUnits.SelectedValue = Session("sBsuid")
                callYEAR_DESCRBind()
                bind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(cmbBusUnits.SelectedValue, Session("CLM"))
                cmbAccadamicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        cmbAccadamicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To cmbAccadamicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If cmbAccadamicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        cmbAccadamicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub cmbBusUnits_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        callYEAR_DESCRBind()

    End Sub
    Sub bind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query = ""
        str_query = "SELECT convert(varchar,ACD_STARTDT,106)as ACD_STARTDT,convert(varchar,ACD_ENDDT,106) as ACD_ENDDT FROM ACADEMICYEAR_D where ACD_BSU_ID='" & cmbBusUnits.SelectedValue & "' and ACD_ID='" & cmbAccadamicYear.SelectedValue & "' and ACD_CLM_ID='" & Session("CLM") & "'"

        Dim dsacd As DataSet
        dsacd = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        txtFromDate.Text = Format(Date.Parse(dsacd.Tables(0).Rows(0)("ACD_STARTDT")), "dd/MMM/yyyy")
        'dsacd.Tables(0).Rows(0)("ACD_STARTDT").ToString()
        txtToDate.Text = Format(Date.Parse(dsacd.Tables(0).Rows(0)("ACD_ENDDT")), "dd/MMM/yyyy")
        'dsacd.Tables(0).Rows(0)("ACD_ENDDT").ToString()
    End Sub

    Protected Sub cmbAccadamicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bind()
    End Sub
End Class
