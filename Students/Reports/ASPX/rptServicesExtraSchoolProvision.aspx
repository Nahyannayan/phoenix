﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptServicesExtraSchoolProvision.aspx.vb" Inherits="Students_Reports_ASPX_rptServicesExtraSchoolProvision" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
     <style>

        .RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}

.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            School Extra Provision Report
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--  <tr><td colspan="6"  class="subheader_img" >School Extra Provision Report</td></tr>--%>
                    <tr>
                        <td align="left" width="20%">
                            <span class="field-label">School</span></td>

                        <td align="left" width="30%">

                            <telerik:RadComboBox ID="ddlBUnit" runat="server" Width="100%" 
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>
                         <td align="left" width="20%"></td>
                         <td align="left" width="30%"></td>
                    </tr>
                    <tr>
                        <td  align="left"> <span class="field-label">Academic Year</span></td>
             
                        <td  align="left">
                            <telerik:RadComboBox ID="ddl_acdYear" runat="server" AutoPostBack="True" Width="100%" >
                            </telerik:RadComboBox>
                        </td>
                        <td align="left"> <span class="field-label">Grade</span></td>
              
                        <td  align="left">

                            <telerik:RadComboBox ID="ddlgrade" runat="server" AutoPostBack="True" Width="100%" >
                            </telerik:RadComboBox>

                        </td>
                    </tr>

                    <tr>

                        <td align="left"> <span class="field-label">Service Category</span></td>
            
                        <td align="left">

                            <telerik:RadComboBox ID="ddlSVCategory" runat="server" AutoPostBack="True"  Width="100%" >
                            </telerik:RadComboBox>

                        </td>
                        <td align="left"> <span class="field-label">Service</span></td>
                        <td align="left">
                            <telerik:RadComboBox ID="ddlServices" runat="server" AutoPostBack="True"  Width="100%" >
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"> <span class="field-label">Term</span></td>
        
                        <td align="left">

                            <telerik:RadComboBox ID="ddlTerms" runat="server"  Width="100%" >
                            </telerik:RadComboBox>

                        </td>

                        <td align="left"> <span class="field-label">Approval Stages</span></td>
               
                        <td align="left">

                            <telerik:RadComboBox ID="ddlSvApprovalStage" runat="server"  Width="100%" 
                                AutoPostBack="True">
                            </telerik:RadComboBox>
                        </td>

                    </tr>
                    <%--    <tr>
          
            <td class="matters" align="left">
               No Of Instruments</td>
            <td class="matters" align="left">
                :</td>
            <td class="matters" align="left">
               
                <telerik:RadComboBox ID="RadComboBox7" Runat="server">
                </telerik:RadComboBox>
               
            </td>
        </tr>--%>
                    <tr>
                      
                        <td align="left"> <span class="field-label">Student No</span></td>
              
                        <td align="left">

                            <telerik:RadNumericTextBox ID="txtStudNo" runat="server"
                                EmptyMessage="Student Number">
                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                            </telerik:RadNumericTextBox>
                        </td>
                        <td align="left"> <span class="field-label">Student Name</span></td>
        
                        <td align="left">

                            <telerik:RadTextBox ID="txtStudName" runat="server" EmptyMessage="Name">
                            </telerik:RadTextBox>

                        </td>
                    </tr>
                    <tr>
                        <td align="left"> <span class="field-label">From Date</span></td>
        
                        <td align="left">
                            <asp:TextBox ID="txtfromdate" runat="server"
                                 AutoPostBack="false"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server"
                                ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            &nbsp;
                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" Format="dd/MMM/yyyy"
                            PopupButtonID="imgFrom" TargetControlID="txtfromdate" PopupPosition="BottomRight">
                        </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                Format="dd/MMM/yyyy" PopupButtonID="txtfromdate" TargetControlID="txtfromdate" PopupPosition="BottomRight">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                        <td align="left"> <span class="field-label">To Date</span></td>
      
                        <td align="left">
                            <asp:TextBox ID="txttodate" runat="server"
                                 AutoPostBack="false"></asp:TextBox>
                            <asp:ImageButton ID="imgtodate" runat="server"
                                ImageUrl="~/Images/calendar.gif" TabIndex="4" />
                            &nbsp;<ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgtodate" TargetControlID="txttodate" PopupPosition="BottomRight">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                Format="dd/MMM/yyyy" PopupButtonID="txttodate" TargetControlID="txttodate" PopupPosition="BottomRight">
                            </ajaxToolkit:CalendarExtender>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" />
                        </td>

                    </tr>

                    <tr>
                        <td  align="center" colspan="4">
                            <br />
                            <asp:Label ID="lblerror" runat="server" EnableViewState="False" CssClass="error"></asp:Label>
                        </td>
                    </tr>


                    <%--  <tr>
            <td class="matters" align="left" colspan="6">
                &nbsp;</td>
        </tr>--%>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

