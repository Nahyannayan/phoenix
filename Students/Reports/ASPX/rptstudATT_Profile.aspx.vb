Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class rptstudATT_Profile
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059060") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights

                txtTodate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)


                callYEAR_DESCRBind()


                'txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
    Sub getACDstart_dt()
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim acd_id As String = ddlAcademicYear.SelectedValue
            Dim sqlString As String = String.Empty
            sqlString = "select acd_startdt,acd_enddt from academicyear_d where acd_id='" & acd_id & "' "




            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sqlString)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", readerStudent_Detail(0))

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callGrade_ACDBind()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_ACDReader As SqlDataReader = AccessStudentClass.GetGRM_GRD_ID(ACD_ID, Session("CLM"))
                ddlGrade.Items.Clear()

                If Grade_ACDReader.HasRows = True Then
                    While Grade_ACDReader.Read
                        di = New ListItem(Grade_ACDReader("GRM_DISPLAY"), Grade_ACDReader("GRD_ID"))
                        ddlGrade.Items.Add(di)
                    End While

                End If
            End Using
            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SHF_ID As String = String.Empty
            If ddlShift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlShift.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Session("sBsuid"), ACD_ID, GRD_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
            bindStudentData()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub callCurrent_BsuShift()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If

            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim sqlstring As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            sqlstring = "SELECT distinct GRADE_BSU_M.GRM_SHF_ID as SHF_ID , SHIFTS_M.SHF_DESCR as SHF_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
" ACADEMICYEAR_D ON  GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID AND GRADE_BSU_M.GRM_BSU_ID = ACADEMICYEAR_D.ACD_BSU_ID INNER JOIN " & _
 " SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID where (ACD_ID = '" & ACD_ID & "' and GRADE_BSU_M.GRM_GRD_ID='" & GRD_ID & "') order by SHIFTS_M.SHF_DESCR"

            Using Current_BsuShiftReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, sqlstring)
                ddlShift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read

                        ddlShift.Items.Add(New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID")))
                    End While

                End If
            End Using

            If ddlShift.Items.Count > 1 Then
                i32.Visible = True
            Else
                i32.Visible = False
            End If

            ddlShift_SelectedIndexChanged(ddlShift, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If ValidateDate() <> "-1" Then
            CallReport()
        Else
            lblError.Text = ""
        End If
      
    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDate.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDate.Text.Trim <> "" Then

                Dim strfDate As String = txtToDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDate.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDate.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            Else
                CommStr = CommStr & "<li>To Date required"

            End If
            lblError.Text = CommStr

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim FromTODT As String = ""
        Dim i As Integer = 0
        Dim FROMDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text) '"25/NOV/2008" '
        Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtTodate.Text) '"25/NOV/2008" '


        h_STU_IDs.Value = GetAllStudentsInSection()


        If h_STU_IDs.Value = "" Then
            lblError.Text = "please select the student(s)"
            Exit Sub

        End If



        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@GRD_ID", ddlGrade.SelectedValue)

        If ddlSection.SelectedItem.Text = "ALL" Then
            param.Add("@SCT_ID", Nothing)
        Else
            param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
        End If
        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param.Add("@FROMDT", Format(Date.Parse(FROMDT), "dd/MMM/yyyy"))
        param.Add("@TODT", Format(Date.Parse(TODT), "dd/MMM/yyyy"))
        param.Add("@STU_IDS", h_STU_IDs.Value)
        param.Add("Acad_Year", ddlAcademicYear.SelectedItem.Text)
        param.Add("fromdt", Format(Date.Parse(FROMDT), "dd/MMM/yyyy"))
        param.Add("todt", Format(Date.Parse(TODT), "dd/MMM/yyyy"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        Session("noimg") = "yes"
        Dim rptClass As New rptClass
        With rptClass
            .Photos = GetPhotoClass()
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptAtt_Profile.rpt")

        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Function GetAllStudentsInSection() As String
        Dim chkSelect As CheckBox
        Dim lblSTU_ID As Label
        Dim stu_ids As String = String.Empty
        For Each Irow As GridViewRow In gvStud.Rows
            chkSelect = DirectCast(Irow.FindControl("chkSelectTR"), CheckBox)
            lblSTU_ID = DirectCast(Irow.FindControl("lblSTU_ID"), Label)
            If chkSelect.Checked = True Then
                stu_ids += lblSTU_ID.Text & "|"

            End If

        Next



        '    Dim str_sql As String = String.Empty
        '    Dim acd_id As String = ddlAcademicYear.SelectedValue
        '    Dim GRD_ID As String = ddlGrade.SelectedValue


        '    If ddlSection.SelectedItem.Text = "ALL" Then

        '        str_sql = " SELECT  ISNULL(STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
        '" STUDENT_M WHERE STU_ACD_ID = '" & acd_id & "' AND STU_GRD_ID =  '" & GRD_ID & "' AND STU_CURRSTATUS<>'CN'  for xml path('')),1,0,''),0) "

        '    Else

        '        str_sql = " SELECT  ISNULL(STUFF((SELECT  CAST(ISNULL(STU_ID,'') as varchar) + '|' FROM " & _
        '" STUDENT_M WHERE STU_ACD_ID = '" & acd_id & "' AND STU_GRD_ID =  '" & GRD_ID & "' AND STU_CURRSTATUS<>'CN' AND STU_SCT_ID='" & ddlSection.SelectedValue & "'  for xml path('')),1,0,''),0) "

        '    End If



        '    Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        Return stu_ids
    End Function


    Function GetPhotoClass() As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO
        Dim arrList As ArrayList = New ArrayList(h_STU_IDs.Value.Split("|"))
        For Each vVal As Object In arrList
            If vVal.ToString <> "" Then
                vPhoto.IDs.Add(vVal)
            End If
        Next
        Return vPhoto
    End Function

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Call getACDstart_dt()
        callGrade_ACDBind()
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callCurrent_BsuShift()
    End Sub
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call callGrade_Section()
    End Sub

    Private Sub bindStudentData()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim ACD_ID As String = String.Empty
            Dim SHF_ID As String = String.Empty
            Dim SCT_ID As String = String.Empty
            Dim FROMDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtFromDate.Text) '"25/NOV/2008" '
            Dim TODT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtTodate.Text) '"25/NOV/2008" '
            Dim FILTER_QUERY As String = String.Empty

            If txtSname.Text.Trim <> "" Then
                FILTER_QUERY += " AND SNAME LIKE '%" & txtSname.Text.Trim & "%'"
            End If
            If txtStu_id.Text.Trim <> "" Then
                FILTER_QUERY += " AND STU_NO LIKE '%" & txtStu_id.Text.Trim & "%'"
            End If


            Dim PARAM(6) As SqlParameter

            ACD_ID = ddlAcademicYear.SelectedValue


            PARAM(0) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(1) = New SqlParameter("@GRD_ID", ddlGrade.SelectedValue)
            PARAM(2) = New SqlParameter("@SHF_ID", ddlShift.SelectedValue)

            If ddlSection.SelectedItem.Text = "ALL" Then
                PARAM(3) = New SqlParameter("@SCT_ID", System.DBNull.Value)
            Else
                PARAM(3) = New SqlParameter("@SCT_ID", ddlSection.SelectedItem.Value)
            End If

            PARAM(4) = New SqlParameter("@FROMDT", FROMDT)
            PARAM(5) = New SqlParameter("@TODT", TODT)
            PARAM(6) = New SqlParameter("@FILTER_BY", FILTER_QUERY)

            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "ATT.GETSTU_PROF_GRIDBIND", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvStud.DataSource = ds.Tables(0)
                gvStud.DataBind()





            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow)
                gvStud.DataSource = ds.Tables(0)
                Try
                    gvStud.DataBind()

                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvStud.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvStud.Rows(0).Cells.Clear()
                gvStud.Rows(0).Cells.Add(New TableCell)
                gvStud.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStud.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStud.Rows(0).Cells(0).Text = "No Records Available !!!"

            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvStud_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStud.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strScript As String = "SelectDeSelectHeaderTR(" + DirectCast(e.Row.Cells(0).FindControl("chkSelectTR"), CheckBox).ClientID & ");"
                DirectCast(e.Row.Cells(0).FindControl("chkSelectTR"), CheckBox).Attributes.Add("onclick", strScript)

              

                Dim chkSelect As CheckBox = DirectCast(e.Row.FindControl("chkSelectTR"), CheckBox)

                If Not chkSelect Is Nothing Then
                    If chkSelect.Visible = True Then
                        chkSelect.Checked = True
                    End If
                End If
            ElseIf e.Row.RowType = DataControlRowType.Header Then
                Dim chkSelectAll As CheckBox = DirectCast(e.Row.FindControl("chkSelectAllTR"), CheckBox)
                If Not chkSelectAll Is Nothing Then
                    chkSelectAll.Checked = True
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        bindStudentData()
    End Sub
End Class
