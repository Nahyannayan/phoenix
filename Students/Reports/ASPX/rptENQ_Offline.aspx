<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptENQ_Offline.aspx.vb" Inherits="Students_Reports_ASPX_rptENQ_Offline" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblCaption" runat="server" Text="Enquiry Offline List"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%"  >
                    <tr align="left">
                        <td  >
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td  >
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters"><span class="field-label" >From Date</span></td>
                                    <td align="left" class="matters"   style="text-align: left">
                                        <asp:TextBox ID="txtFromDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton
                                            ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtFromDate"
                                            ErrorMessage="From Date required" ValidationGroup="dayBook" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator>
                                        <br />
                                        <span  >(dd/mmm/yyyy)</span></td>
                                    <td align="left" class="matters"><span class="field-label" >To Date</span></td>
                                    <td align="left" class="matters"  style="text-align: left">
                                        <asp:TextBox ID="txtToDate" runat="server"  ></asp:TextBox>
                                        <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif" /><asp:RequiredFieldValidator ID="rfvToDate"
                                            runat="server" ControlToValidate="txtToDate" ErrorMessage="To Date required"
                                            ValidationGroup="dayBook" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator><br />
                                        <span  >(dd/mmm/yyyy)</span></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters">
                                        <asp:Label ID="lblTC" runat="server" Text="TC Included" CssClass="field-label"  ></asp:Label></td>
                                    <td align="left" class="matters"   style="text-align: left">
                                        <asp:RadioButtonList ID="rgpTC" runat="server" RepeatDirection="Horizontal" CssClass="field-label" >
                                            <asp:ListItem Selected="True">Yes</asp:ListItem>
                                            <asp:ListItem>NO</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters" colspan="4" style="text-align: center">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFromDate"
                    TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgToDate"
                    TargetControlID="txtToDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>


</asp:Content>

