Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class ptCoproPlaining_ELM_V2
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.GetCurrent(Page).RegisterPostBackControl(btnDownloadReport)
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            tr_ACD.Visible = False
            txtAsOnDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim str_sql As String = ""

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Then 'Or (ViewState("MainMnu_code") <> "S880011") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                'GETALL_YEAR()
                'GETACTIVE_YEAR()
                'GETACY_DATE()
                lblError.Text = ""
            End If
            Me.ddlYear.Items.Add(Now.Year)
            Me.ddlYear.Items.Add(Now.Year + 1)
        End If
    End Sub



    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        If Page.IsValid Then
            If ValidateDate() <> "-1" Then
                CallReport()
            End If
        End If
    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtAsOnDate.Text.Trim <> "" Then
                Dim strfDate As String = txtAsOnDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "As on Date format is Invalid"
                Else
                    txtAsOnDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtAsOnDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "As on Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "As on Date required"
            End If
            Dim lstrProj As String
            lstrProj = "01/" & Me.ddlMonth.SelectedValue & "/" & Me.ddlYear.SelectedValue
            If ddlMonth.SelectedValue = "0" OrElse CDate(lstrProj) <= CDate(txtAsOnDate.Text) Then
                ErrorStatus = "-1"
                CommStr = CommStr & "Projected month should be greater than As on Date"
            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN AS ON DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try
    End Function
    Private Sub CallReport(Optional ByVal DownloadReport As Boolean = False)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", "999998")
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACY_ID", DBNull.Value)
        param.Add("@GRP_TYPE", ddlSch_Grp.SelectedValue)
        param.Add("@ASON", Format(Date.Parse(txtAsOnDate.Text.Trim), "dd/MMM/yyyy"))
        'param.Add("@PROJ_MONTH", Format(Date.Parse(txtAsOnDate.Text.Trim), "dd/MMM/yyyy"))
        param.Add("@PROJ_MONTH", Format(Date.Parse("01/" & Me.ddlMonth.SelectedValue & "/" & Me.ddlYear.SelectedValue), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("AsOnDate", Format(Date.Parse(txtAsOnDate.Text.Trim), "dd/MMM/yyyy"))
        param.Add("COL_HEAD", Getmonth_yearInfo())
        param.Add("Title", ddlSch_Grp.SelectedItem.Text)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            .reportPath = Server.MapPath("../RPT/rptStudent_plainingtool_ELM_V2.rpt")
        End With
        Session("rptClass") = rptClass

        If DownloadReport = False Then
            ReportLoadSelection()
            ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        Else
            Dim rptDownload As New ReportDownload
            rptDownload.LoadReports(rptClass, rs1)
            rptDownload = Nothing
            'Response.Redirect("~/Reports/ASPX Report/rptReportViewerDownloader.aspx?filename=Student_Planning_" & Me.ddlSch_Grp.Text)
        End If

    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Private Function Getmonth_yearInfo() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim output As String = String.Empty

        Dim PARAM(2) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@ASON", Format(Date.Parse(txtAsOnDate.Text.Trim), "dd/MMM/yyyy"))
        PARAM(1) = New SqlClient.SqlParameter("@GRP_TYPE", ddlSch_Grp.SelectedValue)
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SPT.GETYEAR_INFO", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read
                    output = Convert.ToString(DATAREADER("NEW_YEAR"))
                End While
            End If
        End Using
        Return output
    End Function

    Private Sub GETALL_YEAR()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = " SELECT ACY_ID,ACY_DESCR FROM ACADEMICYEAR_M WHERE ACY_ID>16 ORDER BY ACY_ID"
        ddlAcd_year.DataSource = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_sql)
        ddlAcd_year.DataTextField = "ACY_DESCR"
        ddlAcd_year.DataValueField = "ACY_ID"
        ddlAcd_year.DataBind()

    End Sub

    Private Sub GETACTIVE_YEAR()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(2) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@GRP_TYPE", ddlSch_Grp.SelectedValue)
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SPT.GETACTIVE_YEAR", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read

                    ddlAcd_year.ClearSelection()
                    ddlAcd_year.Items.FindByValue(DATAREADER("ACD_ACY_ID")).Selected = True
                End While
            End If
        End Using
    End Sub

    Protected Sub ddlSch_Grp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSch_Grp.SelectedIndexChanged
        'GETACTIVE_YEAR()
        'GETACY_DATE()
    End Sub

    Private Sub GETACY_DATE()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(2) As SqlClient.SqlParameter
        PARAM(0) = New SqlClient.SqlParameter("@GRP_TYPE", ddlSch_Grp.SelectedValue)
        PARAM(1) = New SqlClient.SqlParameter("@ACY_ID", ddlAcd_year.SelectedValue)
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SPT.GETACY_DATE", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read
                    txtAsOnDate.Text = Convert.ToString(DATAREADER("YEAR_DATE"))
                End While
            End If
        End Using

    End Sub

    Protected Sub ddlAcd_year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcd_year.SelectedIndexChanged
        GETACY_DATE()
    End Sub

    Protected Sub btnDownloadReport_Click(sender As Object, e As EventArgs) Handles btnDownloadReport.Click
        If Page.IsValid Then
            If ValidateDate() <> "-1" Then
                CallReport(True)
            End If
        End If
    End Sub
End Class
