<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="rptSURV_Student.aspx.vb" Inherits="Students_Reports_ASPX_rptAdmissionDetail" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../../../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfromDate.ClientID %>').value = result;
                      break;

              }
          }
          return false;
      }


    </script>
    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }
    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Print Circular - Usernames"></asp:Label>
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">


                <table align="center" width="100%">
                    <tr align="left">
                        <td>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="Following condition required" ValidationGroup="dayBook" />
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr align="left">
                        <td  style="font-size: 12px; width: 557px; font-style: italic">&nbsp;</td>
                    </tr>
                    <tr align="left">
                        <td>
                            <table align="center" style="width: 100%;">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Circular No</span></td>
                                    <td align="left" style="text-align: left" width="30%">
                                        <asp:TextBox ID="txtCir" runat="server">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCir"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Circular No required" ForeColor=""
                                            ValidationGroup="dayBook">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"><span class="field-label">Circular Date</span></td>
                                    <td align="left"  width="30%" style="text-align: left">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="123px"></asp:TextBox><span style="font-size: 7pt">
                                        </span>
                                        <asp:ImageButton
                                            ID="imgFromDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="return getDate(550, 310, 0)" />
                                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtFromDate"
                                            ErrorMessage="Circular Date required" ValidationGroup="dayBook" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator>
                                </tr>
                                <%--  <tr>
            <td align="left" >
                Returned Date</td>
            <td  style="width: 2px" >
                :</td>
            <td align="left"  colspan="9" style=" text-align: left">
                <asp:TextBox ID="txtToDate" runat="server" Width="122px"></asp:TextBox>
                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                    OnClientClick="return getDate(550, 310, 1)" /><asp:RequiredFieldValidator ID="rfvToDate"
                        runat="server" ControlToValidate="txtToDate" ErrorMessage="Returned Date required"
                        ValidationGroup="dayBook" CssClass="error" Display="Dynamic" ForeColor="">*</asp:RequiredFieldValidator><br />
                <span style="font-size: 7pt">(dd/mmm/yyyy)</span></td>
        </tr>--%>
                                <tr>
                                    <td align="left" ><span class="field-label">School</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlBUnit" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBUnit_SelectedIndexChanged"></asp:DropDownList></td>
                                
                                     <td align="left" ><span class="field-label">Curriculum</span></td>
                                    <td align="left"  colspan="9" style="text-align: left">
                                        <asp:DropDownList ID="ddlCurr" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurr_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                   

                                    </td>
                                </tr>



                                <tr>
                                    <td align="left" ><span class="field-label">Academic Year</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <td></td>
                                        <td><asp:CheckBox CssClass="field-label" ID="chkNew" runat="server" AutoPostBack="False"
                                            Text="New Admissions Only" /></td>
                                        
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Grade</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGrade_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" style="text-align: left"><span class="field-label">Section</span></td>
                                    <td align="left" style="text-align: left">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td  colspan="4" style="text-align: center">
                                        <asp:Button ID="btnPdf" runat="server" CssClass="button" Text="PDF Format" Visible="False" />
                                        &nbsp;<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                            ValidationGroup="dayBook" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_BSUID" runat="server" />
                            <asp:HiddenField ID="h_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgFromDate" TargetControlID="txtFromDate">
                </ajaxToolkit:CalendarExtender> 

            </div>
        </div>
    </div>
</asp:Content>

