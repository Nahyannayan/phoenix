﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptRoomAttendance
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            txtFromDate.Attributes.Add("onFocus", "this.blur()")
            txtToDate.Attributes.Add("onFocus", "this.blur()")
            callYEAR_DESCRBind()
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page
            'S200491
            'If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200011" And ViewState("MainMnu_code") <> "S200015" And ViewState("MainMnu_code") <> "S200020" And ViewState("MainMnu_code") <> "S200025" And ViewState("MainMnu_code") <> "S200026" And ViewState("MainMnu_code") <> "S200491") And ViewState("MainMnu_code") <> "S200016" Then
            '    If Not Request.UrlReferrer Is Nothing Then
            '        Response.Redirect(Request.UrlReferrer.ToString())
            '    Else

            '        Response.Redirect("~\noAccess.aspx")
            '    End If

            'Else
            'calling pageright class to get the access rights

            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

            'disable the control based on the rights
            'use content if the page is comming from master page else use me.Page

            'disable the control buttons based on the rights
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



            'End If

        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If Page.IsValid Then
            CallReport()
        End If

    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim FromTo_Date As String = ""
        Dim i As Integer = 0




        '@GRD_ID varchar(50), S
        '@SCT_ID bigint,S
        '@DTEFROM varchar(25),
        '@DTETO varchar(25),
        '@option int


        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")

        If ddlGrade.SelectedItem.Value = "0" Then
            param.Add("@GRD_ID", 0)
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        End If


        param.Add("@ACD_ID", ddlAcademicYear.SelectedItem.Value)

        'If ddlStream.SelectedItem.Value = "0" Then
        '    param.Add("@Stream", 0)
        'Else
        '    param.Add("@Stream", ddlStream.SelectedItem.Value)
        'End If

        param.Add("UserName", Session("sUsr_name"))

        'param.Add("Grade_Dis", ddlGrade.SelectedItem.Text)
        'param.Add("Section_Dis", ddlSection.SelectedItem.Text)



        If txtFromDate.Text <> "" Then
            param.Add("@DTE_FROM", txtFromDate.Text.Trim())
        End If

        If txtToDate.Text <> "" Then
            param.Add("@DTE_TO", txtToDate.Text.Trim())
        End If



        If radReportType.SelectedItem.Value <> "" Then
            param.Add("@OPTION", radReportType.SelectedItem.Value)
        Else
            param.Add("@OPTION", Nothing)
        End If

        If radReportType.SelectedItem.Value = "1" Then
            param("RPT_CAPTION") = "Room Attendance - Week Wise"
            If ddlSection.SelectedItem.Value = "0" Then
                param.Add("@SCT_ID", 0)
            Else
                param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
            End If
        ElseIf radReportType.SelectedItem.Value = "2" Then
            param("RPT_CAPTION") = "Room Attendance - Month Wise"
            If ddlGrade.SelectedItem.Value = "0" Then
                param.Add("@SCT_ID", 0)
            Else
                param.Add("@SCT_ID", ddlGroup.SelectedItem.Value)
            End If
        ElseIf radReportType.SelectedItem.Value = "3" Then
            param("RPT_CAPTION") = "Room Attendance - Consolidated"
            If ddlSection.SelectedItem.Value = "0" Then
                param.Add("@SCT_ID", 0)
            Else
                param.Add("@SCT_ID", ddlSection.SelectedItem.Value)
            End If
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param
            If radReportType.SelectedItem.Value = "1" Then
                .reportPath = Server.MapPath(("../RPT/rptRoomAttendacneWeekWise.rpt"))
            ElseIf radReportType.SelectedItem.Value = "2" Then
                .reportPath = Server.MapPath("../RPT/rptRoomAttendacneMonthWise.rpt")
            ElseIf radReportType.SelectedItem.Value = "3" Then
                .reportPath = Server.MapPath("../RPT/rptRoomAttendacneConsolidated.rpt")
            End If
            'file:///C:\Documents and Settings\lijo.joseph.GEMSEDUCATION\Desktop\oasis Project\OASISMAR16\Students\Reports\RPT\rptAdmission_Detail.rpt
        End With
        Session("rptClass") = rptClass
        '  Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub



    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/PHOENIXBETA/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub



    Public Sub callGrade_Section()
        Try
            Dim ACD_ID As String = String.Empty

            ACD_ID = ddlAcademicYear.SelectedItem.Value

            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim STM_ID As String = String.Empty

            Dim SHF_ID As String = ""
            If ddlStream.SelectedIndex = -1 Then
                STM_ID = ""
            Else
                STM_ID = ddlStream.SelectedItem.Value
            End If


            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section_STM(Session("sBsuid"), ACD_ID, GRD_ID, STM_ID, SHF_ID)
                ddlSection.Items.Clear()
                di = New ListItem("ALL", "0")
                ddlSection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlSection.Items.Add(di)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call callGrade_Section()
        BindSubjects()
    End Sub



    Public Sub callStream_Bind()
        Try
            ddlSection.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Stream_Data", PARAM)
            ddlStream.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlStream.Items.Add(di)
            ddlStream.DataSource = ds
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()
            'ddlStream.Items.Insert(0, di)
            If ddlStream.Items.Count > 0 Then
                ddlStream.SelectedValue = "1"
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

        ddlStream_SelectedIndexChanged(ddlStream, Nothing)

    End Sub

    'Sub callGrade_ACDBind()
    '    Try
    '        ddlSection.Items.Clear()

    '        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '        Dim PARAM(5) As SqlParameter

    '        PARAM(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
    '        PARAM(2) = New SqlParameter("@CLM_ID", Session("clm"))
    '        PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))


    '        Dim ds As DataSet
    '        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

    '        ddlGrade.Items.Clear()
    '        Dim di As ListItem
    '        di = New ListItem("ALL", "0")
    '        'ddlGrade.Items.Add(di)
    '        ddlGrade.DataSource = ds
    '        ddlGrade.DataTextField = "grm_display"
    '        ddlGrade.DataValueField = "GRD_ID"
    '        ddlGrade.DataBind()
    '        ddlGrade.Items.Insert(0, di)
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try


    '    ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    'End Sub

    Sub BindGrade()
        Try
            ddlSection.Items.Clear()

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(4) As SqlParameter

            PARAM(1) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(2) = New SqlParameter("@CLM_ID", Session("clm"))
            PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(4) = New SqlParameter("@STM_ID", ddlStream.SelectedValue)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Grade_Data_By_Stream", PARAM)

            ddlGrade.Items.Clear()
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            'ddlGrade.Items.Add(di)
            ddlGrade.DataSource = ds
            ddlGrade.DataTextField = "grm_display"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
            ddlGrade.Items.Insert(0, di)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try


        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)


    End Sub


    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next

            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindSubjects()
        Dim grd_id() As String = ddlGrade.SelectedValue.ToString.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = ""

        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S  " _
                       & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                       & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                       & " AND SBG_STM_ID=" + ddlStream.SelectedValue
        Else
            str_query = "SELECT DISTINCT convert(varchar(100),SBG_ID)+'|'+convert(varchar(100),SBG_SBM_ID) AS SBG_ID,SBG_DESCR FROM SUBJECTS_GRADE_S AS A " _
                & " INNER JOIN GROUPS_M AS B ON A.SBG_ID=B.SGR_SBG_ID" _
                & " INNER JOIN GROUPS_TEACHER_S AS C ON B.SGR_ID=C.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " WHERE SBG_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                & " AND SBG_GRD_ID='" + ddlGrade.SelectedValue + "'" _
                & " AND SBG_STM_ID=" + ddlStream.SelectedValue _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()
    End Sub

    Sub BindGroup()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String

        Dim sbg_id As String() = ddlSubject.SelectedValue.ToString.Split("|")


        If Session("CurrSuperUser") = "Y" Then
            str_query = "SELECT SGR_ID,SGR_DESCR FROM GROUPS_M  " _
                       & " WHERE SGR_SBG_ID='" + sbg_id(0) + "'"

        Else
            str_query = "SELECT DISTINCT SGR_ID,SGR_DESCR FROM GROUPS_M AS A " _
                & " INNER JOIN GROUPS_TEACHER_S AS B ON A.SGR_ID=B.SGS_SGR_ID AND SGS_TODATE IS NULL" _
                & " AND SGR_SBG_ID='" + sbg_id(0) + "'" _
                & " AND SGS_EMP_ID=" + Session("EMPLOYEEID")
        End If


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGroup.DataSource = ds
        ddlGroup.DataTextField = "SGR_DESCR"
        ddlGroup.DataValueField = "SGR_ID"
        ddlGroup.DataBind()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        callStream_Bind()
    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Call BindGrade()

    End Sub

    Protected Sub radReportType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles radReportType.SelectedIndexChanged
        If radReportType.Items(0).Selected Then
            trgroup.Visible = False
            trSection.Visible = True
        ElseIf radReportType.Items(1).Selected Then
            trgroup.Visible = True
            trSection.Visible = False
        ElseIf radReportType.Items(2).Selected Then
            trgroup.Visible = False
            trSection.Visible = True
        End If
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSubject.SelectedIndexChanged
        BindGroup()
    End Sub
End Class
