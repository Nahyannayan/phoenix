Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Students_Reports_ASPX_rptCorp_EnqStatistic
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S135053" And ViewState("MainMnu_code") <> "S135053") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                If Session("USR_MIS") <> "2" Then

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                BindAcad()

                PopulateTree()






            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim i As Integer
        Dim str As String = ""

        If Page.IsValid Then
            Dim Str_ValidateDate As String = String.Empty
            Str_ValidateDate = ValidateDate()
            If Str_ValidateDate = "" Then
                If tvBusinessunit.CheckedNodes.Count < 1 Then
                    lblError.Text = "At least one school needs to be selected"
                ElseIf tvBusinessunit.CheckedNodes.Count > 0 Then
                    For Each node As TreeNode In tvBusinessunit.CheckedNodes
                        If node.Value.Length < 2 Then
                            lblError.Text = "Atleast one school needs to be selected"
                        Else
                            lblError.Text = ""
                            CallReport()
                        End If
                    Next
                End If


                With lstACAD
                    For i = 0 To .Items.Count - 1
                        If .Items(i).Selected = True Then
                            If str <> "" Then
                                str += ","
                            End If
                            str += "'" + .Items(i).Value + "'"
                        End If
                    Next
                End With

                If str = "" Then
                    lblError.Text = "Atleast one academic year needs to be selected"
                End If

            End If



        End If

    End Sub
    Function GetACDFilter() As String
        Dim i As Integer
        Dim str As String = ""
        With lstACAD
            For i = 0 To .Items.Count - 1
                If .Items(i).Selected = True Then
                    str += "<ID><ACD_ID>" + .Items(i).Value + "</ACD_ID></ID>"
                End If
            Next
        End With
        Return "<IDS>" + str + "</IDS>"
    End Function
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim FromTo_Date As String = ""
        Dim i As Integer = 0

        Dim str_bsu_ids As New StringBuilder
        Dim bsu_xml As String = ""
        Dim ACD_XML As String = ""
        For Each node As TreeNode In tvBusinessunit.CheckedNodes
            If node.Value.Length > 2 Then
                str_bsu_ids.Append(node.Value)
                str_bsu_ids.Append("|")
                bsu_xml += "<ID><BSU_ID>" + node.Value + "</BSU_ID></ID>"
            End If
        Next


        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", "999998")
        param.Add("@IMG_TYPE", "LOGO")

        'CODE ADDED BY DHANYA 03-MAR-09
        If ViewState("MainMnu_code") = "S135053" Then
            If bsu_xml <> "" Then
                param.Add("@BSU_XML", "<IDS>" + bsu_xml + "</IDS>")
            Else
                param.Add("@BSU_XML", DBNull.Value)
            End If
            ACD_XML = GetACDFilter()
            param.Add("@ACD_XML", ACD_XML)
            Session("Corp_ACD_XML") = ACD_XML
            param.Add("@DATE2", Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd"))
            param.Add("@DATE1", Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd"))

            param.Add("date2", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
            param.Add("date1", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
        Else
            param.Add("@BSU_ID", str_bsu_ids.ToString)
            param.Add("@ToDT", Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd"))
            param.Add("@FROMDT", Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd"))

            param.Add("Todt", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
            param.Add("Fromdt", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
        End If



        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        'param.Add("To_Date", txtToDate.Text)
        'param.Add("From_Date", txtFromDate.Text)
        param.Add("UserName", Session("sUsr_name"))


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            'CODE ADDED BY DHANYA 03-MAR-09
            If ViewState("MainMnu_code") = "S135053" Then
                .reportPath = Server.MapPath("../RPT/rptCorp_ENQREGENR.rpt")
            Else
                .reportPath = Server.MapPath("../RPT/rptCorpo_SchoolStat.rpt")
            End If


        End With
        Session("rptClass") = rptClass
        ReportLoadSelection()
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDate.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDate.Text.Trim <> "" Then

                Dim strfDate As String = txtToDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDate.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDate.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If
            lblError.Text = CommStr
            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function

    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        tvBusinessunit.Nodes.Clear()
        str_Sql = "SELECT  0 AS BSU_ID,'All' AS BSU_NAME,  COUNT (*)  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvBusinessunit.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("BSU_NAME").ToString()
            tn.Value = dr("BSU_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        tvBusinessunit.DataBind()
        tvBusinessunit.CollapseAll()
    End Sub

    Protected Sub tvBusinessunit_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvBusinessunit.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
        ByVal parentNode As TreeNode)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String

        If parentid = "0" Then

            str_Sql = "SELECT   BSU_ID,BSU_NAME,  0  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"



        Else
            str_Sql = "SELECT   BSU_ID,BSU_NAME,  0  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"
        End If



        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_CORP")
        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)



    End Sub
    Private Sub BindAcad()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        'Dim strQuery As String = "SELECT BSU_SHORT_RESULT,BSU_ID FROM BUSINESSUNIT_M" _
        '                         & " WHERE  BSU_bASIANSCHOOL=1 AND BSU_TYPE<>'O' AND BSU_ID IN (SELECT USA_BSU_ID FROM USERACCESS_S WHERE USA_USR_ID='" + Session("sUsr_id").ToString + "') and bsu_short_result is not null"
        'Dim ds As DataSet

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_ACD")


        lstACAD.DataSource = ds
        lstACAD.DataTextField = "ACY_DESCR"
        lstACAD.DataValueField = "ACY_ID"
        lstACAD.DataBind()
    End Sub
End Class
