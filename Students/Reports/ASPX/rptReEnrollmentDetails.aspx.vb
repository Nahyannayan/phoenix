﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptReEnrollmentDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S889500") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sBSUID"))
                    BindAcademicYear()
                    AcademicYear.Visible = "false"


                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btReport.Click
        If (LTrim(Trim(txtCDate.Text)) = "" Or LTrim(RTrim(txtSDate.Text)) = "") Then
            lblError.Text = "please select date.... "
        Else
            CallReport()
        End If
    End Sub
    Sub BindAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        If Session("SBSUID") <> "999998" Then

            Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                      & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("SBSUID") + "' AND ACD_CLM_ID=1" _
                                      & " ORDER BY ACY_ID"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlAcademicYear.DataSource = ds
            ddlAcademicYear.DataTextField = "acy_descr"
            ddlAcademicYear.DataValueField = "acd_id"
            ddlAcademicYear.DataBind()
            str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
            & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("SBSUID") + "' AND ACD_CLM_ID=1"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim li As New ListItem
            li.Text = ds.Tables(0).Rows(0).Item(0)
            li.Value = ds.Tables(0).Rows(0).Item(1)
            ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        Else
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_ACD")
            ddlAcademicYear.DataSource = ds
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACY_ID"
            ddlAcademicYear.DataBind()
        End If
    End Sub
    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue)
        param.Add("@DATEFROM", txtCDate.Text)
        param.Add("@DATETO", txtSDate.Text)
        param.Add("@BSU_ID", Session("sBSUID"))
        'param.Add("dates", " Between " + txtCDate.Text + " And " + txtSDate.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptReEnrollmentDetails.rpt")
        End With
        Session("rptClass") = rptClass
        'Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub

    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub

End Class

