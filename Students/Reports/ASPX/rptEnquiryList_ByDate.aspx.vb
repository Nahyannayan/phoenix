Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_Reports_ASPX_rptEnquiryList
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            'Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200130") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights
                callYEAR_DESCRBind()
                Call bindAcademic_SHIFT()
                Call bindAcademic_STREAM()
                bindAcademic_Grade()
                callCurriculum()

                txtToDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                txtFromDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub

    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACY_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the Country to Not Available into  the SelectedIndex
                If Trim(ddlAcademicYear.Items(ItemTypeCounter).Value) = Trim(Session("Current_ACY_ID")) Then
                    ddlAcademicYear.SelectedIndex = ItemTypeCounter
                End If
            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACY_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim SHF_ID As String = ddlShift.SelectedValue
            Dim STM_ID As String = ddlStream.SelectedValue
            Dim ds As New DataSet
            str_Sql = " SELECT DISTINCT GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_BSU_M.GRM_DISPLAY, GRADE_M.GRD_DISPLAYORDER " & _
" FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_BSU_ID='" & BSU_ID & "' " & _
" and GRADE_BSU_M.GRM_ACY_ID= '" & ACY_ID & "' And GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "' AND GRM_STM_ID=isnull(case when 0='" & STM_ID & "' then NULL else '" & STM_ID & "' END,GRM_STM_ID) order by GRADE_M.GRD_DISPLAYORDER "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGrade.Items.Clear()
            ddlGrade.DataSource = ds.Tables(0)
            ddlGrade.DataTextField = "GRM_DISPLAY"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()

            ddlGrade.Items.Add(New ListItem("ALL", "0"))
            ddlGrade.ClearSelection()
            If Not ddlGrade.Items.FindByText("ALL") Is Nothing Then
                ddlGrade.Items.FindByText("ALL").Selected = True
            End If

            'ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAcademic_SHIFT()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACY_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim ds As New DataSet
            str_Sql = " SELECT DISTINCT SHIFTS_M.SHF_DESCR, SHIFTS_M.SHF_ID FROM  GRADE_BSU_M INNER JOIN " & _
" SHIFTS_M ON GRADE_BSU_M.GRM_SHF_ID = SHIFTS_M.SHF_ID WHERE (GRADE_BSU_M.GRM_ACY_ID = '" & ACY_ID & "') " & _
" AND (SHIFTS_M.SHF_BSU_ID = '" & BSU_ID & "') order by  SHIFTS_M.SHF_DESCR "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlShift.Items.Clear()
            ddlShift.DataSource = ds.Tables(0)
            ddlShift.DataTextField = "SHF_DESCR"
            ddlShift.DataValueField = "SHF_ID"
            ddlShift.DataBind()
            ddlShift.Items.Add(New ListItem("ALL", "0"))
            ddlShift.ClearSelection()
            If Not ddlShift.Items.FindByText("ALL") Is Nothing Then
                ddlShift.Items.FindByText("ALL").Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_STREAM()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACY_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim SHF_ID As String = ddlShift.SelectedValue
            Dim ds As New DataSet
            str_Sql = " SELECT DISTINCT STREAM_M.STM_DESCR, STREAM_M.STM_ID " & _
" FROM GRADE_BSU_M INNER JOIN  STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID where " & _
" GRADE_BSU_M.GRM_ACY_ID='" & ACY_ID & "' and GRADE_BSU_M.GRM_BSU_ID='" & BSU_ID & "' And GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "' order by STREAM_M.STM_DESCR "

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlStream.Items.Clear()
            ddlStream.DataSource = ds.Tables(0)
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()

            ddlStream.Items.Add(New ListItem("ALL", "0"))
            ddlStream.ClearSelection()
            If Not ddlStream.Items.FindByText("ALL") Is Nothing Then
                ddlStream.Items.FindByText("ALL").Selected = True
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Public Sub callCurriculum()
        Try
            Using CurriculumReader As SqlDataReader = AccessStudentClass.GetCurriculum()
                ddlPrevBoard.Items.Clear()
                ddlPrevBoard.Items.Add(New ListItem("ALL", "0"))
                If CurriculumReader.HasRows = True Then
                    While CurriculumReader.Read
                        ddlPrevBoard.Items.Add(New ListItem(CurriculumReader("CLM_DESCR"), CurriculumReader("CLM_ID")))
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click

        If Page.IsValid Then
            Dim Str_ValidateDate As String = String.Empty
            Str_ValidateDate = ValidateDate()
            If Str_ValidateDate = "" Then


                CallReport()




            End If
        End If


    End Sub
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Sibl As Boolean

        Dim i As Integer = 0

        'If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
        '    FromTo_Date = " AND STUDENT_M.STU_DOJ BETWEEN '" + Format(Date.Parse(txtFromDate.Text), "yyyy-MM-dd") + "' AND '" + Format(Date.Parse(txtToDate.Text), "yyyy-MM-dd") + "'"
        'End If

        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("sbsuid"))
        param.Add("@ACY_ID", ddlAcademicYear.SelectedItem.Value)
        If ddlGrade.SelectedItem.Text = "ALL" Then
            param.Add("@GRD_ID", Nothing)
        Else
            param.Add("@GRD_ID", ddlGrade.SelectedItem.Value)
        End If
        If ddlShift.SelectedItem.Text = "ALL" Then
            param.Add("@SHF_ID", Nothing)
        Else
            param.Add("@SHF_ID", ddlShift.SelectedItem.Value)
        End If
        If ddlStream.SelectedItem.Text = "ALL" Then
            param.Add("@STM_ID", Nothing)
        Else
            param.Add("@STM_ID", ddlStream.SelectedItem.Value)
        End If
        param.Add("@EQS_STATUS", ddlEnquiry.SelectedItem.Value)
        param.Add("@PREV_CLM_ID", ddlPrevBoard.SelectedItem.Value)
        param.Add("@ENQ_OF", ddlEnqList.SelectedItem.Value)
        param.Add("@FROMDT", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
        param.Add("@TODT", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
        param.Add("Todt", Format(Date.Parse(txtToDate.Text), "dd/MMM/yyyy"))
        param.Add("Fromdt", Format(Date.Parse(txtFromDate.Text), "dd/MMM/yyyy"))
        If chkSib.Checked Then
            Sibl = True
        Else
            Sibl = False
        End If
        param.Add("@bSIBL", Sibl)
        param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
        param.Add("UserName", Session("sUsr_name"))



        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportParameters = param

            .reportPath = Server.MapPath("../RPT/rptEnquiryList_ByDate.rpt")



        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        ReportLoadSelection()
    End Sub
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDate.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDate.Text.Trim <> "" Then

                Dim strfDate As String = txtToDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDate.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtFromDate.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ENQUIRY LIST BY DATE")
            Return "-1"
        End Try

    End Function

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Call bindAcademic_SHIFT()
        'Call bindAcademic_STREAM()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call bindAcademic_SHIFT()
        Call bindAcademic_STREAM()
    End Sub

    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        Call bindAcademic_STREAM()
        Call bindAcademic_Grade()
    End Sub

    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Call bindAcademic_Grade()
    End Sub
    Sub ReportLoadSelection()
        If Session("ReportSel") = "POP" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FANCYBOX POPUP", "Popup('/Reports/ASPX Report/rptReportViewerNew.aspx');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "NEW TAB", "window.open ('/Reports/ASPX Report/rptReportViewerNew.aspx','_blank');", True)
        End If
    End Sub
End Class
