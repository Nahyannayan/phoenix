﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTC_ReleaseOnline.aspx.vb" Inherits="Students_studTC_ReleaseOnline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>TC Release Online
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 100%;" valign="top">
                            <table id="tblClm" runat="server" align="center"
                                cellpadding="4" cellspacing="0" width="100%">

                                <tr id="trBsu" runat="server">

                                    <td align="left" id="td4" runat="server" width="10%"><span class="field-label">Curriculum</span>
                                    </td>

                                    <td align="left" id="td6" runat="server" width="25%">
                                        <asp:DropDownList ID="ddlClm" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="10%"></td>
                                    <td align="left" width="25%"></td>
                                    <td align="left" width="10%"></td>
                                    <td align="left" width="20%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="10%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left" width="10%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" width="10%"><span class="field-label">Section</span>
                                    </td>

                                    <td align="left" width="20%">
                                        <asp:DropDownList ID="ddlSection" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Student ID</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtStudentID" runat="server"> </asp:TextBox>
                                    </td>
                                    <td align="left"><span class="field-label">Student Name</span>
                                    </td>

                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server"> </asp:TextBox>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:DropDownList ID="ddlType" runat="server">
                                            <asp:ListItem>OK to Proceed to TC</asp:ListItem>
                                            <asp:ListItem>ALL</asp:ListItem>
                                        </asp:DropDownList>


                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="6">
                                        <asp:Button ID="btnSearch" runat="server" Text="List" CssClass="button" TabIndex="4" />
                                    </td>
                                </tr>
                                <tr id="trGrid" runat="server">
                                    <td align="center" colspan="6" valign="top">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdId" runat="server" Text='<%# Bind("STU_GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" Style="cursor: pointer" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Release">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRelease" Checked='<%# Bind("TSR_bRELEASE") %>' runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="trRelease" runat="server">
                                    <td colspan="6" align="center">
                                        <asp:Button ID="btnRelease" runat="server" Text="Save" CssClass="button" TabIndex="4" />

                                    </td>

                                </tr>
                            </table>

                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfACD_ID" runat="server" />
</asp:Content>

