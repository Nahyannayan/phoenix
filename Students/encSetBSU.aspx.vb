﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic

Imports Telerik.Web.UI
Partial Class Students_encSetBSU
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                Dim MainMnu_code As String
                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                If USR_NAME = "" Or (MainMnu_code <> "S200556") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    Response.CacheControl = "no-cache"
                    'bind_Users()
                    Call TREEVIEWBIND()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub
    'Private Sub bind_Users()
    '    Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim PARAM(1) As SqlParameter

    '    ddlUsers.Items.Clear()
    '    Try
    '        Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ENC.GET_USERS", PARAM)

    '            ddlUsers.Items.Add(New ListItem("-Select User-", "-1"))
    '            If AREA_reader.HasRows = True Then
    '                While AREA_reader.Read
    '                    ddlUsers.Items.Add(New ListItem(AREA_reader("USR_NAME"), AREA_reader("USR_EMP_ID")))
    '                End While
    '            End If


    '        End Using
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, "bind_Users")
    '    End Try
    'End Sub
    Public Sub TREEVIEWBIND()
        Try
            Session("sBSURIGHTS") = Nothing

            Dim htMenuRights As New Hashtable
            Dim IEnumerableMenuRights As HoldBsuRights

            Dim ds As New DataSet

            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(2) As SqlParameter

            PARAM(0) = New SqlParameter("@USR_NAME", txtUsers.Text)

            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "ENC.GETBSU_TREEBIND", PARAM)
            rtvMenuRights.DataTextField = "BSU_NAME"
            rtvMenuRights.DataValueField = "BSU_ID"
            rtvMenuRights.DataFieldID = "BSU_ID"
            rtvMenuRights.DataFieldParentID = "BSU_PARENT_ID"

            rtvMenuRights.DataSource = ds.Tables(0)
            rtvMenuRights.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                For iIndex As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    IEnumerableMenuRights = New HoldBsuRights
                    IEnumerableMenuRights.MNU_CODE = ds.Tables(0).Rows(iIndex)("BSU_ID")
                    IEnumerableMenuRights.MNU_PARENTID = Convert.ToString(ds.Tables(0).Rows(iIndex)("BSU_PARENT_ID"))
                    IEnumerableMenuRights.MNU_CODE_RIGHTS_NEW = ds.Tables(0).Rows(iIndex)("BSU_VISIBLE")

                    htMenuRights(Convert.ToString(ds.Tables(0).Rows(iIndex)("BSU_ID"))) = IEnumerableMenuRights
                Next
            End If
            Session("sBSURIGHTS") = htMenuRights

            LoopNodes(rtvMenuRights.Nodes)
            get_selected_values()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub selected_values()
        Try
            Dim htMenuRights As Hashtable = Session("sBSURIGHTS")
            Dim vMNURGT As HoldBsuRights
            For Each node As RadTreeNode In rtvMenuRights.GetAllNodes
                vMNURGT = htMenuRights(node.Value)
                If (Not vMNURGT Is Nothing) And (node.Checked = True) Then
                    vMNURGT.MNU_CODE_RIGHTS_NEW = 1

                    vMNURGT.MNU_STATUS = "UPDATE"
                    htMenuRights(node.Value) = vMNURGT
                Else
                    vMNURGT.MNU_CODE_RIGHTS_NEW = 0

                    vMNURGT.MNU_STATUS = "UPDATE"
                    htMenuRights(node.Value) = vMNURGT
                End If
            Next
            Session("sBSURIGHTS") = htMenuRights


            'rtvMenuRights.ClearCheckedNodes()
            'LoopNodes(rtvMenuRights.Nodes)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Record cannot be inserted"
        End Try
    End Sub
    Private Sub get_selected_values()
        Try
            rtvMenuRights.ClearCheckedNodes()
            Dim htMenuRights As Hashtable = Session("sBSURIGHTS")
            Dim vMNURGT As HoldBsuRights
            For Each node As RadTreeNode In rtvMenuRights.GetAllNodes
                vMNURGT = htMenuRights(node.Value)
                If (vMNURGT.MNU_CODE_RIGHTS_NEW = 1) Then
                    node.Checked = True

                    vMNURGT.MNU_STATUS = "UPDATE"
                    htMenuRights(node.Value) = vMNURGT

                End If
            Next
            Session("sBSURIGHTS") = htMenuRights


            'rtvMenuRights.ClearCheckedNodes()
            'LoopNodes(rtvMenuRights.Nodes)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            lblErrorMessage.Text = "Record cannot be inserted"
        End Try
    End Sub
    Private Function CallTransactions(ByRef ErrorMessage As String) As String
        Dim str_menu As String = String.Empty
        If Session("sBSURIGHTS") IsNot Nothing Then
            Dim tempHash As Hashtable = Session("sBSURIGHTS")
            For Each vMNURGT As HoldBsuRights In tempHash.Values
                If vMNURGT.MNU_CODE_RIGHTS_NEW <> 0 Then
                    str_menu += String.Format("<MENU BSU_ID='{0}' BSU_VISIBLE='{1}'   />", _
                                                  vMNURGT.MNU_CODE, vMNURGT.MNU_CODE_RIGHTS_NEW)
                End If

            Next
        End If

        If str_menu.Trim <> "" Then

            str_menu = "<MENUS>" + str_menu + "</MENUS>"
        Else
            ErrorMessage = "Please reset and do the process again."
            Return "1000"
        End If




        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim transaction As SqlTransaction
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim StatusFlag As Integer
                Dim param(4) As SqlParameter

                
                param(0) = New SqlParameter("@STR_MENUS", str_menu)
                param(1) = New SqlParameter("@USR_NAME", txtUsers.Text)
                param(2) = New SqlParameter("@USR_ID", Session("sUsr_name"))
                param(3) = New SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                param(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ENC.SAVE_BSU_ACCESS_RIGHTS", param)
                StatusFlag = param(3).Value
                If StatusFlag = 0 Then
                    transaction.Commit()
                Else
                    transaction.Rollback()
                End If
                Return StatusFlag
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
                Return "1000"
            End Try
        End Using
    End Function
    Private Sub LoopNodes(ByVal nodes As RadTreeNodeCollection)
        Dim Red As Integer
        Dim Green As Integer
        Dim Blue As Integer

        For Each node As RadTreeNode In nodes

            
            getColor(node.Value, Red, Green, Blue)

            node.ForeColor = System.Drawing.Color.FromArgb(Red, Green, Blue)


            node.Checked = True

            If node.Nodes.Count > 0 Then
                LoopNodes(node.Nodes)
            End If
        Next

    End Sub
    Private Sub getColor(ByVal Mnu_code As String, ByRef red As Integer, ByRef green As Integer, ByRef blue As Integer)
        Dim htTab As Hashtable = Session("sBSURIGHTS")
        Dim vMNURGT As HoldBsuRights

        Dim str_color As String = "#f03333"
        vMNURGT = htTab(Mnu_code)
        If Not vMNURGT Is Nothing Then
            
            red = 10
            green = 11
            blue = 255
        End If


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try

                selected_values()
                If Not Session("sBSURIGHTS") Is Nothing Then
                    Dim str_err As String
                    Dim errorMessage As String = String.Empty
                    str_err = CallTransactions(errorMessage)
                    If str_err = "0" Then

                        Session("sBSURIGHTS") = Nothing

                        lblErrorMessage.Text = "BSU Rights added successfully"
                    Else
                        lblErrorMessage.Text = UtilityObj.getErrorMessage(str_err)
                    End If
                Else
                    lblErrorMessage.Text = "Please reset and do the process again."
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblErrorMessage.Text = "Process could not be completed"
            End Try
        End If
    End Sub

    'Protected Sub ddlUsers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUsers.SelectedIndexChanged
    '    Call TREEVIEWBIND()
    'End Sub

    

    Protected Sub btnTest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTest.Click
        Call TREEVIEWBIND()
    End Sub
End Class
Public Class HoldBsuRights

    Dim _MNU_CODE As String
    Dim _MNU_PARENTID As String
    Dim _MNU_TEXT As String
    Dim _MNU_CODE_RIGHTS_OLD As Integer
    Dim _MNU_CODE_RIGHTS_NEW As Integer
    Dim _MNU_MODULE As String
    Dim _MNU_STATUS As String
    Public Property MNU_CODE() As String
        Get
            Return _MNU_CODE
        End Get
        Set(ByVal value As String)
            _MNU_CODE = value
        End Set
    End Property

    Public Property MNU_PARENTID() As String
        Get
            Return _MNU_PARENTID
        End Get
        Set(ByVal value As String)
            _MNU_PARENTID = value
        End Set
    End Property

    Public Property MNU_TEXT() As String
        Get
            Return _MNU_TEXT
        End Get
        Set(ByVal value As String)
            _MNU_TEXT = value
        End Set
    End Property

    Public Property MNU_CODE_RIGHTS_OLD() As Integer
        Get
            Return _MNU_CODE_RIGHTS_OLD
        End Get
        Set(ByVal value As Integer)
            _MNU_CODE_RIGHTS_OLD = value
        End Set
    End Property

    Public Property MNU_CODE_RIGHTS_NEW() As Integer
        Get
            Return _MNU_CODE_RIGHTS_NEW
        End Get
        Set(ByVal value As Integer)
            _MNU_CODE_RIGHTS_NEW = value
        End Set
    End Property
    Public Property MNU_MODULE() As String
        Get
            Return _MNU_MODULE
        End Get
        Set(ByVal value As String)
            _MNU_MODULE = value
        End Set
    End Property
    Public Property MNU_STATUS() As String
        Get
            Return _MNU_STATUS
        End Get
        Set(ByVal value As String)
            _MNU_STATUS = value
        End Set
    End Property
    'Public Sub New(ByVal MNU_CODE As String, ByVal MNU_PARENTID As String, ByVal MNU_TEXT As String, ByVal MNU_CODE_RIGHTS_OLD As Integer, ByVal MNU_CODE_RIGHTS_NEW As Integer, ByVal MNU_STATUS As String)
    '    _MNU_CODE = MNU_CODE
    '    _MNU_PARENTID=MNU_PARENTID
    '    _MNU_TEXT = MNU_TEXT
    '    _MNU_CODE_RIGHTS_OLD = MNU_CODE_RIGHTS_OLD
    '    _MNU_CODE_RIGHTS_NEW = MNU_CODE_RIGHTS_NEW
    '    _MNU_STATUS = MNU_STATUS
    'End Sub
End Class
