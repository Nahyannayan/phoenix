﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_stuBudgetMaster
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ' Dim STR As String = Encr_decrData.Decrypt("wr92h2HbWq4=")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050037") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    Bind_Accademicyear()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
            GridBind()


        Else


        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub Bind_Accademicyear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,CONVERT(VARCHAR(100),ACD_ID)+'|'+CONVERT(VARCHAR(100),ACD_ACY_ID) AS ACD_ID  FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID=" + Session("clm") _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "ACD_ID"
        ddlAcademicYear.DataBind()
        str_query = " SELECT ACY_DESCR,CONVERT(VARCHAR(100),ACD_ID)+'|'+CONVERT(VARCHAR(100),ACD_ACY_ID) AS ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID=" + Session("clm")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
    End Sub
    Private Sub GridBind()
        Try
            Dim str_query As String = ""
            Dim acdid As String()
            acdid = ddlAcademicYear.SelectedValue.Split("|")
            Dim str_conn = ConnectionManger.GetOASISConnectionString

       

            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@ACD_ID", acdid(0))
    

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GET_BUDGETS_M", param)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    gvStudBudg.DataSource = ds
                    gvStudBudg.DataBind()

                    Dim i, j As Integer
                   
                    For i = 0 To gvStudBudg.Rows.Count - 1
                        For j = 4 To gvStudBudg.Rows(i).Cells.Count - 1
                            Dim str As String() = gvStudBudg.Rows(i).Cells(j).Text.Split("|")
                            Dim txtMarks As New TextBox
                            txtMarks.ID = "txtMarks" + j.ToString
                            txtMarks.Text = str(0) ' gvMarks.Rows(i).Cells(j).Text.Replace("&nbsp;", "")
                            txtMarks.Width = 80
                            'If ((j = 4) Or (j = 5) Or (j = 6) Or (j = 7)) Then
                            '    txtMarks.Attributes.Add("style", "display:none")
                            'End If
                            'txtMarks.Attributes.Add("onblur", "javascript:return checkNum(this);")
                            'txtMarks.Attributes.Add("onkeydown", "javascript:ScrollDown(" + (i + 2).ToString + "," + j.ToString + ",event);")
                            'txtMarks.CssClass = "inputmark"
                            txtMarks.Attributes.Add("style", "text-align:right;")
                            txtMarks.CssClass = "text-right"
                            gvStudBudg.Rows(i).Cells(j).Controls.Add(txtMarks)





                        Next
                    Next
                    gvStudBudg.FooterRow.Visible = True
                Else
                    gvStudBudg.DataBind()
                End If
            Else
                gvStudBudg.DataBind()
            End If


            
            
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
 

    Protected Sub gvStudBudg_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudBudg.RowCreated
        e.Row.Cells(0).Visible = False
        Dim i As Integer
        If e.Row.RowType = DataControlRowType.DataRow Then


            For i = 4 To e.Row.Cells.Count - 1

                Dim txtMarks As New TextBox
                txtMarks.ID = "txtMarks" + i.ToString

                txtMarks.Width = 80

                e.Row.Cells(i).Controls.Add(txtMarks)

            Next
        
        End If
    End Sub

    Protected Sub gvStudBudg_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudBudg.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(4).Visible = False
            e.Row.Cells(5).Visible = False
            e.Row.Cells(6).Visible = False
            e.Row.Cells(7).Visible = False
        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Visible = False
            e.Row.Cells(5).Visible = False
            e.Row.Cells(6).Visible = False
            e.Row.Cells(7).Visible = False
        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(4).Visible = False
            e.Row.Cells(5).Visible = False
            e.Row.Cells(6).Visible = False
            e.Row.Cells(7).Visible = False
        End If
    End Sub

    Protected Sub lnkCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopy.Click
        Dim txtMarks As TextBox
        Dim lblvalue As TextBox
        Dim index As Integer = 8
        For i = 0 To gvStudBudg.Rows.Count - 1

            lblvalue = gvStudBudg.Rows(i).Cells(3).FindControl("lblvalue")
            If lblvalue.Text <> "" Then
                For index = 8 To gvStudBudg.Rows(i).Cells.Count - 1
                    Dim S As String = gvStudBudg.HeaderRow.Cells(index).Text
                    txtMarks = gvStudBudg.Rows(i).Cells(index).FindControl("txtMarks" + index.ToString)
                    txtMarks.Text = lblvalue.Text
                Next
            End If
        Next
    End Sub

    Function get_month(ByVal str As String) As Integer
        Dim MonArray As String() = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}

        Return (Array.IndexOf(MonArray, str))

    End Function

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim txtMarks As TextBox
        Dim lblGrdId As Label

        Dim index As Integer = 8
        For i = 0 To gvStudBudg.Rows.Count - 1
            lblGrdId = gvStudBudg.Rows(i).Cells(1).FindControl("lblGrdId")
          
            For index = 8 To gvStudBudg.Rows(i).Cells.Count - 1
                Dim S As String = gvStudBudg.HeaderRow.Cells(index).Text
                txtMarks = gvStudBudg.Rows(i).Cells(index).FindControl("txtMarks" + index.ToString)
                If txtMarks.Text = "" Then txtMarks.Text = "0"
                If txtMarks.Text <> "0" Then
                    save_budget(lblGrdId.Text, S, txtMarks.Text)
                End If

            Next

        Next
    End Sub

    Sub save_budget(ByVal BBG_GRD_ID As String, ByVal BBG_YEAR As String, ByVal BBG_COUNT As Integer)
        Dim Status As Integer
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim acdid As String()
                acdid = ddlAcademicYear.SelectedValue.Split("|")
                Dim m As Integer, y As Integer

                m = get_month(Trim(BBG_YEAR.Substring(0, 4))) + 1
                y = BBG_YEAR.Substring(4)
                param(0) = New SqlParameter("@BBG_BSU_ID", Session("sBsuid"))
                param(1) = New SqlParameter("@BBG_ACD_ID", acdid(0))
                param(2) = New SqlClient.SqlParameter("@BBG_CLM_ID", Session("clm"))
                param(3) = New SqlClient.SqlParameter("@BBG_ACY_ID", acdid(1))
                param(4) = New SqlClient.SqlParameter("@BBG_GRD_ID", BBG_GRD_ID)
                param(5) = New SqlClient.SqlParameter("@BBG_MNTH1", m)
                param(6) = New SqlClient.SqlParameter("@BBG_YEAR", y)
                param(7) = New SqlClient.SqlParameter("@BBG_COUNT", BBG_COUNT)


                param(8) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(8).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "save_BusinessUnit_Budgets_M ", param)
                Status = param(8).Value





            Catch ex As Exception

                lblError.Text = "Record could not be updated"
            Finally
                If Status <> 0 Then
                    UtilityObj.Errorlog(lblError.Text)
                    transaction.Rollback()
                Else
                    lblError.Text = "Updated Sucessfully."
                    transaction.Commit()
                End If
            End Try

        End Using
    End Sub

End Class
