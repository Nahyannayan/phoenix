<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/mainMasterPage.master"
    CodeFile="studPrintOffer.aspx.vb" Inherits="Students_studPrintOffer" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <script language="javascript" type="text/javascript">
        function popup() {
            var sFeatures;
            sFeatures = "dialogWidth: 850px; ";
            sFeatures += "dialogHeight: 600px; ";
            //            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var result;
            result = window.showModalDialog("stuOfferLetterPrint.aspx", "", sFeatures);
            //            if(result == '' && result == undefined)
            return false;
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            <asp:Label ID="lblTitle" runat="server">
                <asp:Label ID="lblCaption" runat="server" Text="Offer Letter Print"></asp:Label></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table align="center" width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="left"  width="20%"><span class="field-label">Letter Type<asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label></span></td>
                        <td align="left"  width="30%">
                            <asp:DropDownList ID="ddlOFFR_TYPE" runat="server">
                            </asp:DropDownList>

                        </td>

                        <td align="left" width="20%"><span class="field-label">Letter Date<asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label></span></td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtLDate" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgBtnLDate" runat="server"
                                ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLDate"
                                    Display="Dynamic" ErrorMessage="Letter Date cannot be left empty"
                                    ValidationGroup="groupM1">*</asp:RequiredFieldValidator><span><span>
                                        <br />
                                        <font><span>(dd/mmm/yyyy)</span></font></span></span>


                        </td>
                    </tr>


                    <tr>
                        <td align="left" ><span class="field-label">Date of Join / Start Date<asp:Label ID="ltReg" runat="server" ForeColor="Red" Text="*"></asp:Label></span></td>
                        <td align="left" style="text-align: left">
                            <asp:TextBox ID="txtDoj" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="imgBtnDoj_date" runat="server"
                                ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvDatefrom" runat="server" ControlToValidate="txtDoj"
                                    Display="Dynamic" ErrorMessage="Date of Join cannot be left empty"
                                    ValidationGroup="groupM1">*</asp:RequiredFieldValidator><span><span>
                                        <br />
                                        <span>(dd/mmm/yyyy)</span></span></span>


                        </td>
                        <td align="left" ><span class="field-label">Additional Forms (if any) to be submitted</span></td>
                        <td align="left"  style="text-align: left">
                            <asp:TextBox ID="txtNOTE" runat="server" Rows="2" TabIndex="22" TextMode="MultiLine"
                                MaxLength="255"></asp:TextBox>


                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Preview Offer "
                                ValidationGroup="groupM1" />
                            <asp:Button ID="btnMail" runat="server" CssClass="button" Text="Email Offer Letter"
                                ValidationGroup="groupM1" />
                            <asp:CheckBox ID="chkEmailCopy" runat="server" Text="Copy to Registrar" CssClass="field-label" /></td>
                    </tr>
                </table>


                <table style="width: 100%">
                    <tr>
                        <td id="Td1" visible="false" runat="server">Email the offer letter to&nbsp;<asp:Label ID="lblname" runat="server" Text=""></asp:Label>
                            &nbsp;<asp:LinkButton ID="lnkmail" runat="server"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="clikcoffer" runat="server">

                        <td>Please
                            <asp:LinkButton ID="lnkOffer" runat="server" OnClientClick="javascript:popup()">Click Here</asp:LinkButton>
                            to print the offer letter.
            <asp:LinkButton ID="lnkRegs" OnClientClick="javascript:history.go(-1)" runat="server">Click Here</asp:LinkButton>
                            to return back to registration form.</td>
                    </tr>

                    <tr id="tr_dupli" runat="server">
                        <td align="left">
                            <br />
                            <br />
                            Possible Duplicate Records
                        </td>

                        <td align="left" style="padding-left: 20px;">

                            <asp:GridView ID="GridViewShowDetails" runat="server" CssClass="table table-bordered table-row" Width="100%" EmptyDataText="No Data Found">
                                <PagerStyle BorderStyle="Solid" />
                                <HeaderStyle BorderStyle="Solid" />
                            </asp:GridView>

                        </td>

                        <td>
                            <asp:Literal ID="ltOffer" runat="server">
                
                            </asp:Literal>
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnDoj_date" TargetControlID="txtDoj">
    </ajaxToolkit:CalendarExtender>

    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnLDate" TargetControlID="txtLDate">
    </ajaxToolkit:CalendarExtender>
</asp:Content>
