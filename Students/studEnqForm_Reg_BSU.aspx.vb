Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports UtilityObj
Imports System
Imports System.Text.RegularExpressions
Partial Class StudRecordEdit_bsu
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
    Protected Sub mnuMaster_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs)

        Try
            mvMaster.ActiveViewIndex = Int32.Parse(e.Item.Value)

            Dim i As Integer = Int32.Parse(e.Item.Value)


            '  mnuMaster.Items(0).ImageUrl = "~/Images/Tab_studEdit/btnMain.jpg"
            ' mnuMaster.Items(1).ImageUrl = "~/Images/Tab_studEdit/btnPassport.jpg"
            ' mnuMaster.Items(2).ImageUrl = "~/Images/Tab_studEdit/btnContact.jpg"
            ' mnuMaster.Items(3).ImageUrl = "~/Images/Tab_studEdit/btnCurr1.jpg"
            '' mnuMaster.Items(4).ImageUrl = "~/Images/Tab_studEdit/btnTransport.jpg"
            ' mnuMaster.Items(5).ImageUrl = "~/Images/Tab_studEdit/btnOther.jpg"

            Select Case i
                Case 0
                    '   mnuMaster.Items(0).ImageUrl = "~/Images/Tab_studEdit/btnMain2.jpg"
                    '
                Case 1
                    ' mnuMaster.Items(1).ImageUrl = "~/Images/Tab_studEdit/btnPassport2.jpg"

                Case 2
                    ' mnuMaster.Items(2).ImageUrl = "~/Images/Tab_studEdit/btnContact2.jpg"
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                          "<script language=javascript>fill_Sib();</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                           "<script language=javascript>fill_Staff();</script>")
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                           "<script language=javascript>fill_Stud();</script>")

                Case 3
                    ' mnuMaster.Items(3).ImageUrl = "~/Images/Tab_studEdit/btnCurr2.jpg"
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                           "<script language=javascript>Sel_Grade_KG1();</script>")

                Case 4
                    '  mnuMaster.Items(4).ImageUrl = "~/Images/Tab_studEdit/btnTransport2.jpg"

                Case 5
                    ' mnuMaster.Items(5).ImageUrl = "~/Images/Tab_studEdit/btnOther2.jpg"

            End Select

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "mnuMaster_MenuItemClick")
        End Try
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.MaintainScrollPositionOnPostBack = True
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = "add"

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100066") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    ViewState("menu_rights") = AccessRight2.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    chkSibling.Attributes.Add("onclick", "fill_Sib()")
                    chkStaff_GEMS.Attributes.Add("onclick", "fill_Staff()")
                    chkExStud_Gems.Attributes.Add("onclick", "fill_Stud()")
                    txtPassIss_date.Attributes.Add("onblur", "fillPassport()")
                    txtVisaIss_date.Attributes.Add("onblur", "fillVisa()")
                    chkOLang.Attributes.Add("onclick", "GetSelectedItem()")
                    ddlFLang.Attributes.Add("onChange", "chkFirst_lang()")
                    'plPopUp.Visible = False
                    ''plPopUp2.Visible = False

                    'chkSibling.Attributes.Add("onclick", "fill_Sibl(this)")
                    'chkTran_Req.Attributes.Add("onclick", "fill_Tran(this)")
                    chksms.Checked = True
                    chkEAdd.Checked = True
                    rdPri_Father.Checked = True
                    rbParentF.Checked = True
                    tbReloc.Visible = False
                    rdMale.Checked = True
                    rdGemsGr.Checked = True
                    txtPre_School.Visible = True
                    ddlGemsGr.Visible = False
                    rdMisc_No.Checked = True

                    ViewState("V_Staff_Check") = 0
                    ViewState("V_Sibl_Check") = 0
                    ViewState("V_Sister_check") = 0
                    Call GEMS_BSU_4_Student()
                    ' txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    ddlGEMSSchool.ClearSelection()
                    ddlGEMSSchool.Items.FindByValue(Session("sBsuid")).Selected = True
                    Call callAll_fun_OnPageLoad()
                    If Not ddlGemsGr.Items.FindByValue(ddlGEMSSchool.SelectedValue) Is Nothing Then
                        ddlGemsGr.ClearSelection()
                        ddlGemsGr.Items.FindByValue(ddlGEMSSchool.SelectedValue).Selected = True
                    End If

                    ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)
                    Call ControlTable_row()
                    '  disable_panel()
                    bindbsu()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try

        End If


    End Sub
  

    Private Sub bindbsu()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT   BSU_ID,BSU_NAME  FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlbsu.DataSource = ds.Tables(0)
        ddlbsu.DataValueField = "BSU_ID"
        ddlbsu.DataTextField = "BSU_NAME"
        ddlbsu.DataBind()
        'Dim lst As New ListItem
        'lst.Text = "--------Select--------"
        'lst.Value = 0
        'ddlbsu.Items.Insert(0, lst)
    End Sub
    Sub BSU_Enq_validation()

        Try

            Dim Temp_Rfv As New RequiredFieldValidator
            Dim LTSTAR As New Label
            Dim arInfo As String() = New String(2) {}
            Dim rfv As String = String.Empty
            Dim lt As String = String.Empty
            Dim splitter As Char = "|"

            Dim Enq_hash As New Hashtable
            If Not Session("BSU_Enq_Valid") Is Nothing Then
                Enq_hash = Session("BSU_Enq_Valid")
                Dim hashloop As DictionaryEntry
                For Each hashloop In Enq_hash
                    If hashloop.Value <> "" Then
                        arInfo = hashloop.Value.Split(splitter)

                        If arInfo.Length = 2 Then
                            rfv = arInfo(0)
                            lt = arInfo(1)

                        Else
                            rfv = arInfo(0)
                        End If
                        If Not Page.Master.FindControl("cphMasterpage").FindControl(rfv) Is Nothing Then
                            Temp_Rfv = Page.Master.FindControl("cphMasterpage").FindControl(rfv)
                            Temp_Rfv.EnableClientScript = True
                            Temp_Rfv.Enabled = True
                            Temp_Rfv.Visible = True
                        End If
                        If Not Page.FindControl(rfv) Is Nothing Then
                            LTSTAR = Page.FindControl(lt)
                            LTSTAR.Visible = True
                        End If

                    End If
                Next
            End If
            Enq_hash.Clear()
            Session("BSU_Enq_Valid") = Nothing

            Dim BSU_ID As String = ddlGEMSSchool.SelectedValue

            Using Enq_validation_reader As SqlDataReader = AccessStudentClass.GetEnquiry_Validation(BSU_ID, "2")
                While Enq_validation_reader.Read
                    If Enq_validation_reader.HasRows Then
                        Enq_hash.Add(Enq_validation_reader("EQV_CODE"), Enq_validation_reader("CONTROL_ID") & "|" & Enq_validation_reader("STAR_ID"))

                        If Convert.ToString(Enq_validation_reader("CONTROL_ID")) <> "" Then
                            If Not Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("CONTROL_ID")) Is Nothing Then
                                Temp_Rfv = Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("CONTROL_ID"))
                                Temp_Rfv.EnableClientScript = False
                                Temp_Rfv.Enabled = False
                                Temp_Rfv.Visible = False
                            End If
                        End If
                        If Convert.ToString(Enq_validation_reader("STAR_ID")) <> "" Then
                            If Not Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("STAR_ID")) Is Nothing Then
                                LTSTAR = Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("STAR_ID"))
                                LTSTAR.Visible = False

                            End If
                        End If
                    End If


                End While

            End Using

            Session("BSU_Enq_Valid") = Enq_hash

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try



    End Sub
    Function RefCodeIsvalid() As Boolean
        Dim refcode As String = txtRefCode.Text.Trim
        Dim Bsu_id As String = ddlGEMSSchool.SelectedValue
        Dim email As String = txtREFEmail.Text.Trim

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = " select TOP 1 D.RFD_ID, M.RFM_CODE,S.RFS_REF_EMAIL from REF.REFERRAL_M as M inner join REF.REFERRAL_S AS S " & _
 " on S.RFS_RFM_ID=M.RFM_ID INNER JOIN REF.REFERRAL_D AS D ON D.RFD_RFS_ID=S.RFS_ID " & _
 " WHERE RFM_CODE='" & refcode & "' and RFS_REF_EMAIL='" & email & "' and RFD_BSU_ID='" & Bsu_id & "'"

        Using USER_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            If USER_reader.HasRows Then


                While USER_reader.Read
                    ViewState("RFD_ID") = Convert.ToString(USER_reader("RFD_ID"))
                End While
                Return True

            Else
                Return False
            End If
        End Using

    End Function
    Sub AboutUs()
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim AboutUSString As String = "select MODE_ID,MODE_DESCR from ENQUIRY_MODE_M"

        Dim ds1 As New DataSet

        ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, AboutUSString)

        conn.Close()
        Dim row As DataRow
        For Each row In ds1.Tables(0).Rows
            chkAboutUs.Items.Add(New ListItem(row("MODE_DESCR"), row("MODE_ID").ToString.Trim))
        Next
    End Sub
#Region "Transaction function"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Final_validation() = True Then

            Dim Final_Result As Integer
            Dim Dup_count As Integer

            Dim EQM_APPLPASPRTNO As String = txtPassport.Text
            Dim EQS_ACY_ID As String = ddlAca_Year.SelectedItem.Value
            Dim EQS_BSU_ID As String = ddlGEMSSchool.SelectedItem.Value
            Dim EQS_GRD_ID As String = ddlGrade.SelectedItem.Value
            Dim EQS_SHF_ID As String = ddlShift.SelectedItem.Value
            Dim EQS_STM_ID As String = ddlStream.SelectedItem.Value
            Dim CLM_ID As String = ddlCurri.SelectedItem.Value
            Dim URL_Enquiry As String = String.Empty

            'modfield lijo 9 mar 09 passport no
            If Trim(txtPassport.Text) <> "" Then
                Dup_count = AccessStudentClass.GetDuplicate_Enquiry_M(EQM_APPLPASPRTNO, EQS_SHF_ID, EQS_STM_ID, EQS_BSU_ID, EQS_GRD_ID, EQS_ACY_ID, CLM_ID)
            Else
                Dup_count = 0
            End If


            If Dup_count = 0 Then


                Final_Result = Enquiry_transaction()


                If Final_Result = 0 Then
                    ViewState("datamode") = "add"
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



                    If chkPrint.Checked = True Then
                        Dim jscript As New StringBuilder()
                        Dim url As String = String.Empty
                        url = String.Format("../Students/StudEnqApplAck_reg.aspx")

                        jscript.Append("<script>window.open('")
                        jscript.Append(url)
                        jscript.Append("');</script>")
                        Page.RegisterStartupScript("OpenWindows", jscript.ToString())

                        'Response.Redirect(url)
                    End If
                    ' clearAll()

                    Call disable_panel()

                    btnNo2.Visible = False

                    btnYes2.Visible = False
                    btnNo.Visible = True

                    btnYes.Visible = True
                    lblText.Text = "Would like to continue with the same Primary Contact info for the next applicant? "

                    mpe.Show()
                    'btnAdd.Enabled = False
                    'btnSave.Visible = False
                    'btnCancel.Visible = False
                    'mnuMaster.Enabled = False


                End If

            Else
                lblError.Text = "Record already exists"

            End If

        End If
    End Sub

    Sub clearAll()
        ddlGEMSSchool.ClearSelection()
        ddlGEMSSchool.Items.FindByValue(Session("sBsuid")).Selected = True
        ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)

        txtFname.Text = ""
        txtMname.Text = ""
        txtLname.Text = ""
        rdMale.Checked = True
        ddlReligion.ClearSelection()
        ddlReligion.Items.FindByText("")
        txtDob.Text = ""
        txtPob.Text = ""

        ddlCountry.ClearSelection()
        ddlNational.ClearSelection()
        ddlCountry.Items.FindByText("")
        ddlNational.Items.FindByText("")

        txtPassport.Text = ""
        txtStud_Contact_Country.Text = ""
        txtStud_Contact_Area.Text = ""
        txtStud_Contact_No.Text = ""
        txtM_Country.Text = ""
        txtM_Area.Text = ""
        txtMobile_Pri.Text = ""
        txtEmail_Pri.Text = ""
        'txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtPassportIssue.Text = ""
        txtPassportIssue.Text = ""
        txtPassExp_Date.Text = ""
        txtVisaNo.Text = ""
        txtIss_Place.Text = ""
        txtVisaIss_date.Text = ""
        txtVisaExp_date.Text = ""
        txtIss_Auth.Text = ""
        chkSibling.Checked = False

        txtSib_Name.Text = ""
        txtSib_ID.Text = ""
        rdPri_Father.Checked = True
        ddlCont_Status.ClearSelection()
        ddlCont_Status.SelectedIndex = 0
        txtPri_Fname.Text = ""
        txtPri_Mname.Text = ""
        txtPri_Lname.Text = ""
        ddlPri_National1.ClearSelection()
        ddlPri_National2.ClearSelection()
        ddlPri_National1.Items.FindByText("")
        ddlPri_National2.Items.FindByText("")
        txtAdd1_overSea.Text = ""
        txtAdd2_overSea.Text = ""
        txtOverSeas_Add_City.Text = ""
        ddlOverSeas_Add_Country.Text = ""
        txtPhone_Oversea_Country.Text = ""
        txtPhone_Oversea_Area.Text = ""
        txtPhone_Oversea_No.Text = ""
        txtPoBox_Pri.Text = ""
        txtApartNo.Text = ""
        txtBldg.Text = ""
        txtStreet.Text = ""
        txtArea.Text = ""
        txtCity_pri.Text = ""

        ddlCountry_Pri.ClearSelection()
        ddlCountry_Pri.Items.FindByValue("172").Selected = True
        txtPoboxLocal.Text = ""
        ddlEmirate.ClearSelection()
        ddlEmirate.SelectedIndex = 0
        txtHPhone_Country.Text = ""
        txtHPhone_Area.Text = ""
        txtHPhone.Text = ""
        txtOPhone_Country.Text = ""
        txtOPhone_Area.Text = ""
        txtOPhone.Text = ""
        txtFaxNo_country.Text = ""
        txtFaxNo_Area.Text = ""
        txtFaxNo.Text = ""
        ddlPref_contact.ClearSelection()
        ddlPref_contact.SelectedIndex = 0
        txtOccup.Text = ""
        ddlCompany.ClearSelection()
        ddlCompany.SelectedIndex = 0
        txtComp.Text = ""
        chkStaff_GEMS.Checked = False
        txtStaffID.Text = ""
        txtStaff_Name.Text = ""
        chkExStud_Gems.Checked = False
        txtExStudName_Gems.Text = ""
        ddlExYear.ClearSelection()
        ddlExYear.Items.FindByText(Now.Year)
        ddlExStud_Gems.ClearSelection()
        chkAboutUs.ClearSelection()
        txtRegNo.Text = ""
        txtPre_School.Text = ""
        txtPre_City.Text = ""
        txtMedInst.Text = ""
        txtLast_Att.Text = ""
        rdMisc_Yes.Text = ""
        txtNote.Text = ""
        chkTran_Req.Checked = False
        ddlMainLocation.ClearSelection()
        ddlMainLocation.SelectedIndex = 0
        txtOthers.Text = ""
        rbParentF.Checked = True
        chkPrint.Checked = False
        ddlFLang.ClearSelection()
        ddlFLang.Items.FindByText("").Selected = True
        chkOLang.ClearSelection()


    End Sub
    Sub ClearApplic()
        'ddlGEMSSchool.ClearSelection()
        'ddlGEMSSchool.Items.FindByValue(Session("sBsuid")).Selected = True
        'ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)

        txtFname.Text = ""
        txtMname.Text = ""
        txtLname.Text = ""
        rdMale.Checked = True
        ' ddlReligion.ClearSelection()
        ' ddlReligion.Items.FindByText("")
        txtDob.Text = ""
        txtPob.Text = ""

        ddlCountry.ClearSelection()
        ddlNational.ClearSelection()
        ' ddlCountry.Items.FindByText("")
        ' ddlNational.Items.FindByText("")

        txtPassport.Text = ""
        'txtStud_Contact_Country.Text = ""
        'txtStud_Contact_Area.Text = ""
        'txtStud_Contact_No.Text = ""
        'txtM_Country.Text = ""
        'txtM_Area.Text = ""
        'txtMobile_Pri.Text = ""
        'txtEmail_Pri.Text = ""
        ' txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtPassportIssue.Text = ""
        txtPassportIssue.Text = ""
        txtPassExp_Date.Text = ""
        txtVisaNo.Text = ""
        txtIss_Place.Text = ""
        txtVisaIss_date.Text = ""
        txtVisaExp_date.Text = ""
        txtIss_Auth.Text = ""
        'chkSibling.Checked = False

        ' txtSib_Name.Text = ""
        'txtSib_ID.Text = ""
        'rdPri_Father.Checked = True
        'ddlCont_Status.ClearSelection()
        ' ddlCont_Status.SelectedIndex = 0
        'txtPri_Fname.Text = ""
        'txtPri_Mname.Text = ""
        'txtPri_Lname.Text = ""
        'ddlPri_National1.ClearSelection()
        'ddlPri_National2.ClearSelection()
        'ddlPri_National1.Items.FindByText("")
        'ddlPri_National2.Items.FindByText("")
        'txtAdd1_overSea.Text = ""
        'txtAdd2_overSea.Text = ""
        'txtOverSeas_Add_City.Text = ""
        'ddlOverSeas_Add_Country.Text = ""
        'txtPhone_Oversea_Country.Text = ""
        'txtPhone_Oversea_Area.Text = ""
        'txtPhone_Oversea_No.Text = ""
        'txtPoBox_Pri.Text = ""
        'txtApartNo.Text = ""
        'txtBldg.Text = ""
        'txtStreet.Text = ""
        'txtArea.Text = ""
        'txtCity_pri.Text = ""

        'ddlCountry_Pri.ClearSelection()
        'ddlCountry_Pri.Items.FindByValue("172").Selected = True
        'txtPoboxLocal.Text = ""
        'ddlEmirate.ClearSelection()
        'ddlEmirate.SelectedIndex = 0
        'txtHPhone_Country.Text = ""
        'txtHPhone_Area.Text = ""
        'txtHPhone.Text = ""
        'txtOPhone_Country.Text = ""
        'txtOPhone_Area.Text = ""
        'txtOPhone.Text = ""
        'txtFaxNo_country.Text = ""
        'txtFaxNo_Area.Text = ""
        'txtFaxNo.Text = ""
        'ddlPref_contact.ClearSelection()
        'ddlPref_contact.SelectedIndex = 0
        'txtOccup.Text = ""
        'ddlCompany.ClearSelection()
        'ddlCompany.SelectedIndex = 0
        'txtComp.Text = ""
        'chkStaff_GEMS.Checked = False
        'txtStaffID.Text = ""
        'txtStaff_Name.Text = ""
        'chkExStud_Gems.Checked = False
        'txtExStudName_Gems.Text = ""
        'ddlExYear.ClearSelection()
        'ddlExYear.Items.FindByText(Now.Year)
        'ddlExStud_Gems.ClearSelection()
        'chkAboutUs.ClearSelection()
        txtRegNo.Text = ""
        txtPre_School.Text = ""
        txtPre_City.Text = ""
        txtMedInst.Text = ""
        txtLast_Att.Text = ""
        rdMisc_Yes.Text = ""
        txtNote.Text = ""
        'chkTran_Req.Checked = False
        'ddlMainLocation.ClearSelection()
        'ddlMainLocation.SelectedIndex = 0
        'txtOthers.Text = ""
        'rbParentF.Checked = True
        'chkPrint.Checked = False

    End Sub

    'Protected Sub btnNo2_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Session("SIB_APPLICANT") = ""

    '    '' plPopUp2.Visible = False
    '    btnAdd.Enabled = True
    '    btnSave.Visible = True
    '    btnCancel.Visible = True
    '    mnuMaster.Enabled = True
    '    enable_panel()




    'End Sub

    'Protected Sub btnYes2_Click(ByVal sender As Object, ByVal e As System.EventArgs)


    '    ' plPopUp2.Visible = False
    '    btnAdd.Enabled = True
    '    btnSave.Visible = True
    '    btnCancel.Visible = True
    '    mnuMaster.Enabled = True


    '    enable_panel()




    'End Sub


    Function Enquiry_transaction() As Integer

        Dim status As String = String.Empty
        Dim TEMP_ApplNo As String = String.Empty
        Dim TEMP_EQM_ENQID As Integer

        Dim EQM_PREVSCHOOL_GRD_ID As String = String.Empty
        Dim EQM_EQP_ID As String = String.Empty
        Dim EQM_EQMID As String = String.Empty
        Dim EQM_PREVSCHOOL_NURSERY As String = String.Empty
        Dim EQM_PREVSCHOOL_REG_ID As String = String.Empty
        Dim EQM_APPLNO As String = String.Empty
        Dim EQM_PREVSCHOOL_BSU_ID As String = String.Empty
        Dim EQM_PREVSCHOOL As String = String.Empty
        Dim EQM_PREVSCHOOL_CITY As String = String.Empty
        Dim EQM_PREVSCHOOL_CTY_ID As String = String.Empty
        Dim EQM_PREVSCHOOL_CLM_ID As String = String.Empty
        Dim EQM_PREVSCHOOL_MEDIUM As String = String.Empty
        Dim EQM_PREVSCHOOL_LASTATTDATE As Date
        Dim EQM_bPREVSCHOOLGEMS As Boolean
        Dim EQM_SIBLINGFEEID As String = String.Empty
        Dim EQM_SIBLINGSCHOOL As String = String.Empty
        Dim EQM_EX_STU_ID As String = String.Empty
        Dim EQM_EXUNIT As String = String.Empty
        Dim EQM_MODE_ID As String = String.Empty
        Dim EQM_ENQTYPE As String = "R"
        Dim EQM_STATUS As String = String.Empty
        Dim EQM_SAUDI_ID As String = String.Empty

        Dim EQM_PREFCONTACT As String = String.Empty
        Dim EQM_PRIMARYCONTACT As String = String.Empty
        Dim EQM_STAFFUNIT As String = String.Empty
        Dim EQM_STAFF_EMP_ID As String = String.Empty

        Dim EQM_ENQDATE As String = String.Format("{0:" & OASISConstants.DateFormat & "}", Date.Today)
        Dim EQM_APPLFIRSTNAME As String = Trim(txtFname.Text)
        Dim EQM_APPLMIDNAME As String = Trim(txtMname.Text)
        Dim EQM_APPLLASTNAME As String = Trim(txtLname.Text)
        Dim ENQ_APPLPASSPORTNAME As String = Trim(txtFname.Text) & " " & Trim(txtMname.Text) & " " & Trim(txtLname.Text)
        Dim EQM_APPLDOB As Date = txtDob.Text


        Dim EQM_APPLGENDER As String = String.Empty
        If rdMale.Checked = True Then
            EQM_APPLGENDER = "M"
        ElseIf rdFemale.Checked = True Then
            EQM_APPLGENDER = "F"
        End If

        Dim EQM_REL_ID As String = ddlReligion.SelectedItem.Value
        Dim EQM_APPLNATIONALITY As String = ddlNational.SelectedItem.Value
        Dim EQM_APPLPOB As String = txtPob.Text
        Dim EQM_APPLCOB As String = ddlCountry.SelectedItem.Value
        Dim EQM_APPLPASPRTNO As String = txtPassport.Text
        Dim EQM_APPLPASPRTISSDATE As Date = Date.MinValue 'txtPassIss_date.Text
        Dim EQM_APPLPASPRTEXPDATE As Date = Date.MinValue ' txtPassExp_Date.Text
        Dim EQM_APPLPASPRTISSPLACE As String = txtPassportIssue.Text
        Dim EQM_APPLVISANO As String = txtVisaNo.Text
        Dim EQM_APPLVISAISSDATE As Date = Date.MinValue
        Session("PassportNo_Enq") = txtPassport.Text

        If txtPassIss_date.Text <> "" Then
            EQM_APPLPASPRTISSDATE = txtPassIss_date.Text
        End If

        If txtPassExp_Date.Text <> "" Then
            EQM_APPLPASPRTEXPDATE = txtPassExp_Date.Text
        End If


        If txtVisaIss_date.Text <> "" Then
            EQM_APPLVISAISSDATE = txtVisaIss_date.Text
        End If

        Dim EQM_APPLVISAEXPDATE As Date = Date.MinValue
        If txtVisaExp_date.Text <> "" Then
            EQM_APPLVISAEXPDATE = txtVisaExp_date.Text
        End If


        Dim EQM_APPLVISAISSPLACE As String = txtIss_Place.Text
        Dim EQM_APPLVISAISSAUTH As String = txtIss_Auth.Text
        'check 4 is it KG1 or not 
        ' code modified on 22-apr-2008
        'EQM_PREVSCHOOL_NURSERY
        If (ViewState("Table_row") = "KG1") Or (ViewState("Table_row") = "PK") Then

            EQM_PREVSCHOOL_NURSERY = ddlPreSchool_Nursery.SelectedItem.Value
            EQM_PREVSCHOOL_REG_ID = txtRegNo.Text
        Else
            EQM_bPREVSCHOOLGEMS = rdGemsGr.Checked

            If rdGemsGr.Checked = True Then
                EQM_PREVSCHOOL = ddlGemsGr.SelectedItem.Text
                EQM_PREVSCHOOL_BSU_ID = ddlGemsGr.SelectedItem.Value
                EQM_PREVSCHOOL_CITY = txtPre_City.Text
            ElseIf rdOther.Checked = True Then
                EQM_PREVSCHOOL = txtPre_School.Text
                EQM_PREVSCHOOL_CITY = txtPre_City.Text
            End If
            EQM_PREVSCHOOL_CTY_ID = ddlPre_Country.SelectedItem.Value
            EQM_PREVSCHOOL_CLM_ID = ddlPre_Curriculum.SelectedItem.Value
            EQM_PREVSCHOOL_MEDIUM = txtMedInst.Text

            If txtLast_Att.Text <> "" Then
                EQM_PREVSCHOOL_LASTATTDATE = txtLast_Att.Text
            End If
            EQM_PREVSCHOOL_GRD_ID = ddlPre_Grade.SelectedItem.Value
        End If


        Dim EQM_bAPPLSIBLING As Boolean = chkSibling.Checked
        Session("bAPPLSIBLING") = chkSibling.Checked
        Session("SIBLINGFEEID") = ""
        Session("SIBLINGSCHOOL") = ""
        If EQM_bAPPLSIBLING = True Then
            EQM_SIBLINGFEEID = txtSib_ID.Text
            Session("SIBLINGFEEID") = EQM_SIBLINGFEEID
            EQM_SIBLINGSCHOOL = ddlSib_BSU.SelectedItem.Value
            Session("SIBLINGSCHOOL") = EQM_SIBLINGSCHOOL
            Session("Sibling_FeeID") = UCase(txtSib_Name.Text) & "/" & UCase(txtSib_ID.Text)
        Else
            Session("Sibling_FeeID") = ""
        End If
        Session("ExStudName_Gems") = ""
        Dim EQM_bEXSTUDENT As String = chkExStud_Gems.Checked
        If EQM_bEXSTUDENT = True Then
            Session("ExStudName_Gems") = txtExStudName_Gems.Text
            'code modified by removing txtExStudFeeID_Gems.Text
            EQM_EX_STU_ID = "" 'txtExStudFeeID_Gems.Text


            EQM_EXUNIT = ddlExStud_Gems.SelectedItem.Value
        End If
        Session("StaffID_ENQ") = ""
        Session("STAFF_NAME") = ""
        Dim EQM_bSTAFFGEMS As Boolean = chkStaff_GEMS.Checked
        If EQM_bSTAFFGEMS = True Then
            EQM_STAFFUNIT = ddlStaff_BSU.SelectedItem.Value
            Session("STAFFUNIT") = ddlStaff_BSU.SelectedItem.Value
            Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtStaffID.Text, ddlStaff_BSU.SelectedItem.Value)
                Session("StaffID_ENQ") = txtStaffID.Text
                Session("STAFF_NAME") = txtStaff_Name.Text
                If ValidStaff_reader.HasRows = True Then

                    While ValidStaff_reader.Read
                        EQM_STAFF_EMP_ID = Convert.ToString(ValidStaff_reader("EMP_ID"))
                    End While

                End If
            End Using

        End If
        Session("EQM_REMARKS") = txtNote.Text
        Session("EQM_bRCVSPMEDICATION") = rdMisc_Yes.Checked
        Dim EQM_REMARKS As String = Session("EQM_REMARKS")

        Dim EQM_bRCVSPMEDICATION As Boolean = Session("EQM_bRCVSPMEDICATION")
        Dim EQM_SPMEDICN As String = String.Empty

        Dim EQM_bRCVMAIL As Boolean = chkEAdd.Checked
        Dim EQM_bRCVSMS As Boolean = chksms.Checked

        EQM_PREFCONTACT = ddlPref_contact.SelectedItem.Value

        If rdPri_Father.Checked Then
            EQM_PRIMARYCONTACT = "F"
        ElseIf rbPri_Mother.Checked Then
            EQM_PRIMARYCONTACT = "M"
        ElseIf rdPri_Guard.Checked Then
            EQM_PRIMARYCONTACT = "G"
        End If

        Dim EQM_EMGCONTACT As String = String.Empty
        ' If txtStud_Contact_Country.Text <> "" And txtStud_Contact_Area.Text <> "" And txtStud_Contact_No.Text <> "" Then
        EQM_EMGCONTACT = txtStud_Contact_Country.Text & "-" & txtStud_Contact_Area.Text & "-" & txtStud_Contact_No.Text
        'Else
        ' EQM_EMGCONTACT = ""
        'End If

        Dim EQM_FILLED_BY As String = String.Empty
        Dim EQM_AGENT_NAME As String = String.Empty
        Dim EQM_AGENT_MOB As String = String.Empty
        If rbParentF.Checked = True Then
            EQM_FILLED_BY = "P"

        ElseIf rbRelocAgent.Checked = True Then
            EQM_FILLED_BY = "A"
            EQM_AGENT_NAME = txtAgentName.Text
            EQM_AGENT_MOB = txtMAgent_country.Text & "-" & txtMAgent_Area.Text & "-" & txtMAgent_No.Text
        End If

        Dim EQM_TRM_ID As String = String.Empty
        If ddlTerm.SelectedIndex = -1 Then
            EQM_TRM_ID = ""
        Else
            EQM_TRM_ID = ddlTerm.SelectedItem.Value
        End If
        Dim EQM_FIRSTLANG As String = ddlFLang.SelectedValue
        Dim EQM_OTHLANG As String = String.Empty

        For Each item As ListItem In chkOLang.Items
            If (item.Selected) Then

                EQM_OTHLANG = EQM_OTHLANG + item.Value + "|"
            End If
        Next
        Dim EQM_bRCVPUBL As Boolean = chkPubl.Checked


        Dim trans As SqlTransaction

        'code to check for duplicate entry in the enquiry form
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            trans = conn.BeginTransaction("SampleTransaction")

            Try

                status = AccessStudentClass.SaveStudENQUIRY_M(EQM_EQMID, UCase(EQM_ENQDATE), UCase(EQM_APPLFIRSTNAME), UCase(EQM_APPLMIDNAME), UCase(EQM_APPLLASTNAME), UCase(ENQ_APPLPASSPORTNAME), _
                        UCase(EQM_APPLDOB), UCase(EQM_APPLGENDER), EQM_REL_ID, UCase(EQM_APPLNATIONALITY), UCase(EQM_APPLPOB), UCase(EQM_APPLCOB), UCase(EQM_APPLPASPRTNO), UCase(EQM_APPLPASPRTISSDATE), _
                          UCase(EQM_APPLPASPRTEXPDATE), UCase(EQM_APPLPASPRTISSPLACE), UCase(EQM_APPLVISANO), UCase(EQM_APPLVISAISSDATE), UCase(EQM_APPLVISAEXPDATE), _
                          UCase(EQM_APPLVISAISSPLACE), UCase(EQM_APPLVISAISSAUTH), EQM_bPREVSCHOOLGEMS, EQM_PREVSCHOOL_BSU_ID, UCase(EQM_PREVSCHOOL_NURSERY), UCase(EQM_PREVSCHOOL_REG_ID), UCase(EQM_PREVSCHOOL), _
                          UCase(EQM_PREVSCHOOL_CITY), EQM_PREVSCHOOL_CTY_ID, UCase(EQM_PREVSCHOOL_GRD_ID), EQM_PREVSCHOOL_CLM_ID, EQM_PREVSCHOOL_MEDIUM, UCase(EQM_PREVSCHOOL_LASTATTDATE), _
                          EQM_bAPPLSIBLING, UCase(EQM_SIBLINGFEEID), UCase(EQM_SIBLINGSCHOOL), EQM_bEXSTUDENT, UCase(EQM_EXUNIT), UCase(EQM_EX_STU_ID), _
                          EQM_bSTAFFGEMS, UCase(EQM_STAFFUNIT), EQM_STAFF_EMP_ID, EQM_bRCVSPMEDICATION, UCase(EQM_SPMEDICN), UCase(EQM_REMARKS), _
                          EQM_EQP_ID, EQM_bRCVSMS, EQM_bRCVMAIL, UCase(EQM_PRIMARYCONTACT), UCase(EQM_PREFCONTACT), UCase(EQM_MODE_ID), _
                         UCase(EQM_ENQTYPE), UCase(EQM_STATUS), EQM_EMGCONTACT, EQM_TRM_ID, EQM_FILLED_BY, UCase(EQM_AGENT_NAME), EQM_AGENT_MOB, EQM_FIRSTLANG, EQM_OTHLANG, EQM_bRCVPUBL, EQM_SAUDI_ID, TEMP_EQM_ENQID, trans)

                If status <> 0 Then
                    Throw New ArgumentException("Record could not be Inserted")

                Else


                    Dim EQP_ID As String = TEMP_EQM_ENQID
                    Dim EQP_FFIRSTNAME As String = String.Empty
                    Dim EQP_FMIDNAME As String = String.Empty
                    Dim EQP_FLASTNAME As String = String.Empty
                    Dim EQP_FNATIONALITY As String = String.Empty
                    Dim EQP_FNATIONALITY2 As String = String.Empty



                    Dim EQP_FCOMSTREET As String = String.Empty
                    Dim EQP_FCOMAREA As String = String.Empty
                    Dim EQP_FCOMBLDG As String = String.Empty
                    Dim EQP_FCOMAPARTNO As String = String.Empty




                    Dim EQP_FCOMPOBOX As String = String.Empty
                    Dim EQP_FCOMCITY As String = String.Empty
                    Dim EQP_FCOMSTATE As String = String.Empty
                    Dim EQP_FCOMCOUNTRY As String = String.Empty
                    Dim EQP_FOFFPHONECODE As String = String.Empty
                    Dim EQP_FOFFPHONE As String = String.Empty
                    Dim EQP_FRESPHONECODE As String = String.Empty
                    Dim EQP_FRESPHONE As String = String.Empty
                    Dim EQP_FFAXCODE As String = String.Empty
                    Dim EQP_FFAX As String = String.Empty
                    Dim EQP_FMOBILECODE As String = String.Empty
                    Dim EQP_FMOBILE As String = String.Empty
                    Dim EQP_FPRMADDR1 As String = String.Empty
                    Dim EQP_FPRMADDR2 As String = String.Empty
                    Dim EQP_FPRMPOBOX As String = String.Empty
                    'field added
                    Dim EQP_FCOMPOBOX_EMIR As String = String.Empty

                    Dim EQP_FPRMCITY As String = String.Empty
                    Dim EQP_FPRMCOUNTRY As String = String.Empty
                    Dim EQP_FPRMPHONE As String = String.Empty
                    Dim EQP_FOCC As String = String.Empty
                    Dim EQP_FCOMPANY As String = String.Empty
                    Dim EQP_FEMAIL As String = String.Empty
                    'FILED ADDED
                    Dim EQP_FACD_YEAR As String = String.Empty
                    Dim EQP_FMODE_ID As String = String.Empty

                    Dim EQP_BSU_ID_STAFF As String = String.Empty
                    Dim EQP_bFGEMSSTAFF As Boolean
                    Dim EQP_MFIRSTNAME As String = String.Empty
                    Dim EQP_MMIDNAME As String = String.Empty
                    Dim EQP_MLASTNAME As String = String.Empty
                    Dim EQP_MNATIONALITY As String = String.Empty
                    Dim EQP_MNATIONALITY2 As String = String.Empty


                    Dim EQP_MCOMSTREET As String = String.Empty
                    Dim EQP_MCOMAREA As String = String.Empty
                    Dim EQP_MCOMBLDG As String = String.Empty
                    Dim EQP_MCOMAPARTNO As String = String.Empty



                    Dim EQP_MCOMPOBOX As String = String.Empty
                    Dim EQP_MCOMCITY As String = String.Empty
                    Dim EQP_MCOMSTATE As String = String.Empty
                    Dim EQP_MCOMCOUNTRY As String = String.Empty
                    Dim EQP_MOFFPHONECODE As String = String.Empty
                    Dim EQP_MOFFPHONE As String = String.Empty
                    Dim EQP_MRESPHONECODE As String = String.Empty
                    Dim EQP_MRESPHONE As String = String.Empty
                    Dim EQP_MFAXCODE As String = String.Empty
                    Dim EQP_MFAX As String = String.Empty
                    Dim EQP_MMOBILECODE As String = String.Empty
                    Dim EQP_MMOBILE As String = String.Empty
                    Dim EQP_MPRMADDR1 As String = String.Empty
                    Dim EQP_MPRMADDR2 As String = String.Empty
                    Dim EQP_MPRMPOBOX As String = String.Empty
                    'field added
                    Dim EQP_MCOMPOBOX_EMIR As String = String.Empty
                    Dim EQP_MMODE_ID As String = String.Empty

                    Dim EQP_MPRMCITY As String = String.Empty
                    Dim EQP_MPRMCOUNTRY As String = String.Empty
                    Dim EQP_MPRMPHONE As String = String.Empty
                    Dim EQP_MOCC As String = String.Empty
                    Dim EQP_MCOMPANY As String = String.Empty
                    Dim EQP_MEMAIL As String = String.Empty
                    Dim EQP_MBSU_ID As String = String.Empty
                    'FIELD ADDED
                    Dim EQP_MACD_YEAR As String = String.Empty

                    Dim EQP_bMGEMSSTAFF As Boolean
                    Dim EQP_GFIRSTNAME As String = String.Empty
                    Dim EQP_GMIDNAME As String = String.Empty
                    Dim EQP_GLASTNAME As String = String.Empty
                    Dim EQP_GNATIONALITY As String = String.Empty
                    Dim EQP_GNATIONALITY2 As String = String.Empty


                    Dim EQP_GCOMSTREET As String = String.Empty
                    Dim EQP_GCOMAREA As String = String.Empty
                    Dim EQP_GCOMBLDG As String = String.Empty
                    Dim EQP_GCOMAPARTNO As String = String.Empty

                    Dim EQP_GCOMPOBOX As String = String.Empty
                    Dim EQP_GCOMCITY As String = String.Empty
                    Dim EQP_GCOMSTATE As String = String.Empty
                    Dim EQP_GCOMCOUNTRY As String = String.Empty
                    Dim EQP_GOFFPHONECODE As String = String.Empty
                    Dim EQP_GOFFPHONE As String = String.Empty
                    Dim EQP_GRESPHONECODE As String = String.Empty
                    Dim EQP_GRESPHONE As String = String.Empty
                    Dim EQP_GFAXCODE As String = String.Empty
                    Dim EQP_GFAX As String = String.Empty
                    Dim EQP_GMOBILECODE As String = String.Empty
                    Dim EQP_GMOBILE As String = String.Empty
                    Dim EQP_GPRMADDR1 As String = String.Empty
                    Dim EQP_GPRMADDR2 As String = String.Empty
                    Dim EQP_GPRMPOBOX As String = String.Empty

                    'field added
                    Dim EQP_GCOMPOBOX_EMIR As String = String.Empty
                    Dim EQP_GMODE_ID As String = String.Empty

                    Dim EQP_GPRMCITY As String = String.Empty
                    Dim EQP_GPRMCOUNTRY As String = String.Empty
                    Dim EQP_GPRMPHONE As String = String.Empty
                    Dim EQP_GOCC As String = String.Empty
                    Dim EQP_GCOMPANY As String = String.Empty
                    Dim EQP_GEMAIL As String = String.Empty
                    Dim EQP_GBSU_ID As String = String.Empty
                    'FIELD ADDED
                    Dim EQP_GACD_YEAR As String = String.Empty



                    Dim EQP_bGGEMSSTAFF As Boolean
                    Dim EQP_FCOMP_ID As String = String.Empty
                    Dim EQP_MCOMP_ID As String = String.Empty
                    Dim EQP_GCOMP_ID As String = String.Empty

                    Dim str_Aboutus As String = String.Empty
                    For Each item As ListItem In chkAboutUs.Items
                        If (item.Selected) Then

                            str_Aboutus = str_Aboutus & item.Value
                            str_Aboutus = str_Aboutus & "|"
                        End If
                    Next

                    Session("str_Aboutus") = str_Aboutus
                    If rdPri_Father.Checked Then


                        EQP_FFIRSTNAME = txtPri_Fname.Text
                        EQP_FMIDNAME = txtPri_Mname.Text
                        EQP_FLASTNAME = txtPri_Lname.Text
                        EQP_FNATIONALITY = ddlPri_National1.SelectedItem.Value
                        EQP_FNATIONALITY2 = ddlPri_National2.SelectedItem.Value

                        'Field added 19/aug/2008

                        EQP_FCOMSTREET = txtStreet.Text
                        EQP_FCOMAREA = txtArea.Text
                        EQP_FCOMBLDG = txtBldg.Text
                        EQP_FCOMAPARTNO = txtApartNo.Text





                        EQP_FCOMPOBOX = txtPoboxLocal.Text
                        'Field added 19/aug/2008
                        EQP_FCOMPOBOX_EMIR = ddlEmirate.SelectedItem.Value

                        EQP_FCOMCITY = txtCity_pri.Text
                        EQP_FCOMSTATE = txtCity_pri.Text
                        EQP_FCOMCOUNTRY = ddlCountry_Pri.SelectedItem.Value
                        EQP_FOFFPHONECODE = txtOPhone_Country.Text


                        ' If txtOPhone_Country.Text <> "" And txtOPhone_Area.Text <> "" And txtOPhone.Text <> "" Then
                        EQP_FOFFPHONE = txtOPhone_Country.Text & "-" & txtOPhone_Area.Text & "-" & txtOPhone.Text
                        'Else
                        '  EQP_FOFFPHONE = txtOPhone.Text
                        '  End If

                        EQP_FRESPHONECODE = txtHPhone_Country.Text


                        If txtHPhone_Country.Text <> "" And txtHPhone_Area.Text <> "" And txtHPhone.Text <> "" Then
                            EQP_FRESPHONE = txtHPhone_Country.Text & "-" & txtHPhone_Area.Text & "-" & txtHPhone.Text
                        Else
                            EQP_FRESPHONE = txtHPhone.Text
                        End If


                        EQP_FFAXCODE = txtFaxNo_country.Text

                        ' If txtFaxNo_country.Text <> "" And txtFaxNo_Area.Text <> "" And txtFaxNo.Text <> "" Then
                        EQP_FFAX = txtFaxNo_country.Text & "-" & txtFaxNo_Area.Text & " " & txtFaxNo.Text
                        'Else
                        '   EQP_FFAX = txtFaxNo.Text
                        'End If

                        EQP_FMOBILECODE = txtM_Country.Text

                        'If txtM_Country.Text <> "" And txtM_Area.Text <> "" And txtMobile_Pri.Text <> "" Then

                        EQP_FMOBILE = txtM_Country.Text & "-" & txtM_Area.Text & "-" & txtMobile_Pri.Text
                        'Else
                        ' EQP_FMOBILE = txtMobile_Pri.Text
                        ' End If

                        EQP_FPRMADDR1 = txtAdd1_overSea.Text
                        EQP_FPRMADDR2 = txtAdd2_overSea.Text
                        EQP_FPRMPOBOX = txtPoBox_Pri.Text
                        EQP_FPRMCITY = txtOverSeas_Add_City.Text
                        EQP_FPRMCOUNTRY = ddlOverSeas_Add_Country.SelectedItem.Value

                        'If txtPhone_Oversea_Country.Text <> "" And txtPhone_Oversea_Area.Text <> "" And txtPhone_Oversea_No.Text <> "" Then

                        EQP_FPRMPHONE = txtPhone_Oversea_Country.Text & "-" & txtPhone_Oversea_Area.Text & "-" & txtPhone_Oversea_No.Text
                        'Else
                        '  EQP_FPRMPHONE = txtPhone_Oversea_No.Text
                        ' End If


                        EQP_FOCC = txtOccup.Text

                        'code updated 25-may-2008


                        If ddlCompany.SelectedItem.Text = "Other" Then
                            EQP_FCOMP_ID = "0"
                            EQP_FCOMPANY = txtComp.Text
                        Else
                            EQP_FCOMP_ID = ddlCompany.SelectedItem.Value
                            EQP_FCOMPANY = ""
                        End If



                        EQP_FEMAIL = txtEmail_Pri.Text.ToString.ToLower
                        EQP_bFGEMSSTAFF = chkExStud_Gems.Checked
                        If EQP_bFGEMSSTAFF Then
                            EQP_FACD_YEAR = ddlExYear.SelectedItem.Value
                            EQP_BSU_ID_STAFF = ddlExStud_Gems.SelectedItem.Value
                        End If

                        EQP_FMODE_ID = str_Aboutus.ToString

                    ElseIf rbPri_Mother.Checked Then
                        EQP_MFIRSTNAME = txtPri_Fname.Text
                        EQP_MMIDNAME = txtPri_Mname.Text
                        EQP_MLASTNAME = txtPri_Lname.Text
                        EQP_MNATIONALITY = ddlPri_National1.SelectedItem.Value
                        EQP_MNATIONALITY2 = ddlPri_National2.SelectedItem.Value

                        EQP_MCOMSTREET = txtStreet.Text
                        EQP_MCOMAREA = txtArea.Text
                        EQP_MCOMBLDG = txtBldg.Text
                        EQP_MCOMAPARTNO = txtApartNo.Text

                        EQP_MCOMCITY = txtCity_pri.Text
                        EQP_MCOMSTATE = txtCity_pri.Text
                        EQP_MCOMCOUNTRY = ddlCountry_Pri.SelectedItem.Value

                        EQP_MCOMPOBOX = txtPoboxLocal.Text
                        'Field added 19/aug/2008
                        EQP_MCOMPOBOX_EMIR = ddlEmirate.SelectedItem.Value

                        EQP_MOFFPHONECODE = txtOPhone_Country.Text

                        ' If txtOPhone_Country.Text <> "" And txtOPhone_Area.Text <> "" And txtOPhone.Text <> "" Then

                        EQP_MOFFPHONE = txtOPhone_Country.Text & "-" & txtOPhone_Area.Text & "-" & txtOPhone.Text
                        'Else
                        '    EQP_MOFFPHONE = txtOPhone.Text
                        'End If


                        EQP_MRESPHONECODE = txtHPhone_Country.Text


                        ' If txtHPhone_Country.Text <> "" And txtHPhone_Area.Text <> "" And txtHPhone.Text <> "" Then

                        EQP_MRESPHONE = txtHPhone_Country.Text & "-" & txtHPhone_Area.Text & "-" & txtHPhone.Text
                        'Else
                        '    EQP_MRESPHONE = txtHPhone.Text
                        'End If



                        EQP_MFAXCODE = txtFaxNo_country.Text



                        ' If txtFaxNo_country.Text <> "" And txtFaxNo_Area.Text <> "" And txtFaxNo.Text <> "" Then

                        EQP_MFAX = txtFaxNo_country.Text & "-" & txtFaxNo_Area.Text & "-" & txtFaxNo.Text
                        'Else
                        '    EQP_MFAX = txtFaxNo.Text
                        'End If


                        EQP_MMOBILECODE = txtM_Country.Text


                        ' If txtM_Country.Text <> "" And txtM_Area.Text <> "" And txtMobile_Pri.Text <> "" Then

                        EQP_MMOBILE = txtM_Country.Text & "-" & txtM_Area.Text & "-" & txtMobile_Pri.Text
                        'Else
                        ' EQP_MMOBILE = txtMobile_Pri.Text
                        'End If



                        EQP_MPRMADDR1 = txtAdd1_overSea.Text
                        EQP_MPRMADDR2 = txtAdd2_overSea.Text
                        EQP_MPRMPOBOX = txtPoBox_Pri.Text
                        EQP_MPRMCITY = txtOverSeas_Add_City.Text
                        EQP_MPRMCOUNTRY = ddlOverSeas_Add_Country.SelectedItem.Value


                        If txtPhone_Oversea_Country.Text <> "" And txtPhone_Oversea_Area.Text <> "" And txtPhone_Oversea_No.Text <> "" Then
                            EQP_MPRMPHONE = txtPhone_Oversea_Country.Text & "-" & txtPhone_Oversea_Area.Text & "-" & txtPhone_Oversea_No.Text
                        Else
                            EQP_MPRMPHONE = txtPhone_Oversea_No.Text
                        End If


                        EQP_MOCC = txtOccup.Text

                        'code updated 25-may-2008


                        If ddlCompany.SelectedItem.Text = "Other" Then
                            EQP_MCOMP_ID = ""
                            EQP_MCOMPANY = txtComp.Text
                        Else
                            EQP_MCOMP_ID = ddlCompany.SelectedItem.Value
                            EQP_MCOMPANY = ""
                        End If





                        EQP_MEMAIL = txtEmail_Pri.Text.ToString.ToLower


                        EQP_bMGEMSSTAFF = chkExStud_Gems.Checked
                        If EQP_bMGEMSSTAFF Then
                            EQP_MACD_YEAR = ddlExYear.SelectedItem.Value
                            EQP_BSU_ID_STAFF = ddlExStud_Gems.SelectedItem.Value
                        End If



                        EQP_MMODE_ID = str_Aboutus.ToString
                    ElseIf rdPri_Guard.Checked Then
                        EQP_GFIRSTNAME = txtPri_Fname.Text
                        EQP_GMIDNAME = txtPri_Mname.Text
                        EQP_GLASTNAME = txtPri_Lname.Text
                        EQP_GNATIONALITY = ddlPri_National1.SelectedItem.Value
                        EQP_GNATIONALITY2 = ddlPri_National2.SelectedItem.Value



                        EQP_GCOMSTREET = txtStreet.Text
                        EQP_GCOMAREA = txtArea.Text
                        EQP_GCOMBLDG = txtBldg.Text
                        EQP_GCOMAPARTNO = txtApartNo.Text






                        EQP_GCOMPOBOX = txtPoboxLocal.Text
                        EQP_GCOMCITY = txtCity_pri.Text
                        EQP_GCOMSTATE = txtCity_pri.Text
                        EQP_GCOMCOUNTRY = ddlCountry_Pri.SelectedItem.Value
                        EQP_GOFFPHONECODE = txtOPhone_Country.Text


                        'If txtOPhone_Country.Text <> "" And txtOPhone_Area.Text <> "" And txtOPhone.Text <> "" Then
                        EQP_GOFFPHONE = txtOPhone_Country.Text & "-" & txtOPhone_Area.Text & "-" & txtOPhone.Text
                        'Else
                        EQP_GOFFPHONE = txtOPhone.Text
                        'End If

                        'EQP_GRESPHONECODE = txtHPhone_Country.Text


                        ' If txtHPhone_Country.Text <> "" And txtHPhone_Area.Text <> "" And txtHPhone.Text <> "" Then

                        EQP_GRESPHONE = txtHPhone_Country.Text & "-" & txtHPhone_Area.Text & "-" & txtHPhone.Text
                        'Else
                        '    EQP_GRESPHONE = txtHPhone.Text
                        'End If


                        EQP_GFAXCODE = txtFaxNo_country.Text




                        'If txtFaxNo_country.Text <> "" And txtFaxNo_Area.Text <> "" And txtFaxNo.Text <> "" Then

                        EQP_GFAX = txtFaxNo_country.Text & "-" & txtFaxNo_Area.Text & "-" & txtFaxNo.Text
                        'Else
                        '    EQP_GFAX = txtFaxNo.Text
                        'End If

                        EQP_GMOBILECODE = txtM_Country.Text



                        'If txtM_Country.Text <> "" And txtM_Area.Text <> "" And txtMobile_Pri.Text <> "" Then
                        EQP_GMOBILE = txtM_Country.Text & "-" & txtM_Area.Text & "-" & txtMobile_Pri.Text
                        'Else
                        '    EQP_GMOBILE = txtMobile_Pri.Text
                        'End If

                        EQP_GPRMADDR1 = txtAdd1_overSea.Text
                        EQP_GPRMADDR2 = txtAdd2_overSea.Text
                        EQP_GPRMPOBOX = txtPoBox_Pri.Text
                        'Field added 19/aug/2008
                        EQP_GCOMPOBOX_EMIR = ddlEmirate.SelectedItem.Value

                        EQP_GPRMCITY = txtOverSeas_Add_City.Text
                        EQP_GPRMCOUNTRY = ddlOverSeas_Add_Country.SelectedItem.Value




                        If txtPhone_Oversea_Country.Text <> "" And txtPhone_Oversea_Area.Text <> "" And txtPhone_Oversea_No.Text <> "" Then
                            EQP_GPRMPHONE = txtPhone_Oversea_Country.Text & " " & txtPhone_Oversea_Area.Text & " " & txtPhone_Oversea_No.Text
                        Else
                            EQP_GPRMPHONE = txtPhone_Oversea_No.Text
                        End If


                        EQP_GOCC = txtOccup.Text



                        'code updated 25-may-2008

                        If ddlCompany.SelectedItem.Text = "Other" Then
                            EQP_GCOMP_ID = ""
                            EQP_GCOMPANY = txtComp.Text
                        Else
                            EQP_GCOMP_ID = ddlCompany.SelectedItem.Value
                            EQP_GCOMPANY = ""
                        End If


                        EQP_GEMAIL = txtEmail_Pri.Text.ToString.ToLower

                        EQP_bGGEMSSTAFF = chkExStud_Gems.Checked
                        If EQP_bGGEMSSTAFF Then
                            EQP_GACD_YEAR = ddlExYear.SelectedItem.Value
                            EQP_BSU_ID_STAFF = ddlExStud_Gems.SelectedItem.Value
                        End If




                        EQP_GMODE_ID = str_Aboutus.ToString
                    End If

                    status = AccessStudentClass.SaveStudENQUIRY_PARENT_M(TEMP_EQM_ENQID, UCase(EQP_FFIRSTNAME), UCase(EQP_FMIDNAME), UCase(EQP_FLASTNAME), UCase(EQP_FNATIONALITY), UCase(EQP_FNATIONALITY2), _
                                               UCase(EQP_FCOMSTREET), UCase(EQP_FCOMAREA), UCase(EQP_FCOMBLDG), UCase(EQP_FCOMAPARTNO), EQP_FCOMPOBOX, UCase(EQP_FCOMCITY), UCase(EQP_FCOMSTATE), UCase(EQP_FCOMCOUNTRY), EQP_FOFFPHONECODE, EQP_FOFFPHONE, _
                                               EQP_FRESPHONECODE, EQP_FRESPHONE, EQP_FFAXCODE, EQP_FFAX, EQP_FMOBILECODE, EQP_FMOBILE, UCase(EQP_FPRMADDR1), UCase(EQP_FPRMADDR2), EQP_FPRMPOBOX, _
                                               UCase(EQP_FPRMCITY), UCase(EQP_FPRMCOUNTRY), EQP_FPRMPHONE, UCase(EQP_FOCC), UCase(EQP_FCOMPANY), EQP_FEMAIL, EQP_bFGEMSSTAFF, EQP_BSU_ID_STAFF, EQP_FACD_YEAR, _
                                               UCase(EQP_MFIRSTNAME), UCase(EQP_MMIDNAME), UCase(EQP_MLASTNAME), UCase(EQP_MNATIONALITY), UCase(EQP_MNATIONALITY2), UCase(EQP_MCOMSTREET), UCase(EQP_MCOMAREA), UCase(EQP_MCOMBLDG), UCase(EQP_MCOMAPARTNO), EQP_MCOMPOBOX, _
                                               UCase(EQP_MCOMCITY), UCase(EQP_MCOMSTATE), UCase(EQP_MCOMCOUNTRY), EQP_MOFFPHONECODE, EQP_MOFFPHONE, EQP_MRESPHONECODE, EQP_MRESPHONE, _
                                               EQP_MFAXCODE, EQP_MFAX, EQP_MMOBILECODE, EQP_MMOBILE, UCase(EQP_MPRMADDR1), UCase(EQP_MPRMADDR2), EQP_MPRMPOBOX, _
                                               UCase(EQP_MPRMCITY), UCase(EQP_MPRMCOUNTRY), EQP_MPRMPHONE, UCase(EQP_MOCC), UCase(EQP_MCOMPANY), EQP_bMGEMSSTAFF, _
                                               EQP_MBSU_ID, EQP_MACD_YEAR, UCase(EQP_GFIRSTNAME), UCase(EQP_GMIDNAME), UCase(EQP_GLASTNAME), UCase(EQP_GNATIONALITY), UCase(EQP_GNATIONALITY2), _
                                               UCase(EQP_GCOMSTREET), UCase(EQP_GCOMAREA), UCase(EQP_GCOMBLDG), UCase(EQP_GCOMAPARTNO), EQP_GCOMPOBOX, UCase(EQP_GCOMCITY), UCase(EQP_GCOMSTATE), UCase(EQP_GCOMCOUNTRY), EQP_GOFFPHONECODE, _
                                               EQP_GOFFPHONE, EQP_GRESPHONECODE, EQP_GRESPHONE, EQP_GFAXCODE, EQP_GFAX, EQP_GMOBILECODE, EQP_GMOBILE, UCase(EQP_GPRMADDR1), _
                                               UCase(EQP_GPRMADDR2), EQP_GPRMPOBOX, UCase(EQP_GPRMCITY), UCase(EQP_GPRMCOUNTRY), EQP_GPRMPHONE, UCase(EQP_GOCC), UCase(EQP_GCOMPANY), EQP_bGGEMSSTAFF, _
                                               EQP_GBSU_ID, EQP_GACD_YEAR, EQP_GEMAIL, EQP_MEMAIL, UCase(EQP_FCOMP_ID), UCase(EQP_MCOMP_ID), UCase(EQP_GCOMP_ID), EQP_FCOMPOBOX_EMIR, EQP_MCOMPOBOX_EMIR, EQP_GCOMPOBOX_EMIR, EQP_FMODE_ID, EQP_MMODE_ID, EQP_GMODE_ID, trans)



                    If status <> 0 Then
                        Throw New ArgumentException("Record could not be Inserted")

                    Else



                        Dim EQS_EQM_ENQID As String = TEMP_EQM_ENQID
                        Dim EQS_ACY_ID As String = ddlAca_Year.SelectedItem.Value
                        Dim EQS_BSU_ID As String = ddlGEMSSchool.SelectedItem.Value
                        Dim EQS_GRD_ID As String = ddlGrade.SelectedItem.Value
                        Dim EQS_SHF_ID As String = ddlShift.SelectedItem.Value
                        Dim EQS_bTPTREQD As Boolean = chkTran_Req.Checked
                        Dim EQS_LOC_ID As String = String.Empty
                        Dim EQS_SBL_ID As String = String.Empty
                        Dim EQS_PNT_ID As String = String.Empty
                        Dim EQS_TPTREMARKS As String = String.Empty
                        Dim EQS_STATUS As String = "NEW"
                        Dim EQS_STM_ID As String = ddlStream.SelectedItem.Value
                        Dim EQS_CLM_ID As Integer = ddlCurri.SelectedItem.Value
                        Dim EQS_DOJ As String = txtDOJ.Text

                        If EQS_bTPTREQD Then
                            If ddlMainLocation.SelectedIndex <> -1 Then
                                EQS_LOC_ID = ddlMainLocation.SelectedItem.Value
                            End If
                            If ddlSubLocation.SelectedIndex <> -1 Then
                                EQS_SBL_ID = ddlSubLocation.SelectedItem.Value
                            End If
                            If ddlPickup.SelectedIndex <> -1 Then
                                EQS_PNT_ID = ddlPickup.SelectedItem.Value
                            End If


                            EQS_TPTREMARKS = UCase(Trim(txtOthers.Text))

                        End If
                        Dim EQS_RFD_ID As String = ViewState("RFD_ID")
                        status = AccessStudentClass.SaveStudENQUIRY_SCHOOLPRIO_S(EQS_EQM_ENQID, EQS_ACY_ID, EQS_BSU_ID, UCase(EQS_GRD_ID), _
                     EQS_SHF_ID, EQS_bTPTREQD, EQS_LOC_ID, EQS_SBL_ID, EQS_PNT_ID, EQS_STM_ID, EQM_bAPPLSIBLING, UCase(EQM_SIBLINGFEEID), UCase(EQM_SIBLINGSCHOOL), _
                     UCase(EQS_TPTREMARKS), UCase(EQS_STATUS), EQM_bRCVSPMEDICATION, UCase(EQM_SPMEDICN), UCase(EQM_REMARKS), EQS_CLM_ID, UCase(EQS_DOJ), EQS_RFD_ID, TEMP_ApplNo, trans)



                        If status <> 0 Then

                            Throw New ArgumentException(getErrorMessage(status))
                        End If

                    End If

                End If
                'return the ApplNo & Enq_ID
                Session("TEMP_ApplNo") = TEMP_ApplNo
                Session("Enq_ID") = TEMP_EQM_ENQID

                trans.Commit()

                lblErr.Text = "Record Inserted Successfully"
                'Adding transaction info

                Return 0
            Catch myex As ArgumentException
                trans.Rollback()

                UtilityObj.Errorlog(myex.Message)
                lblError.Text = myex.Message
                Return -1
            Catch ex As Exception

                trans.Rollback()

                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Record could not be Inserted"
                Return -1
            End Try


        End Using
    End Function
#End Region
#Region "User defined functions"
    Sub GetSchoolButton_Enable()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim Grade As String = String.Empty
        Dim SHF_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlGrade.SelectedIndex = -1 Then
            Grade = ""
        Else
            Grade = ddlGrade.SelectedItem.Value
        End If


        If ddlShift.SelectedIndex = -1 Then
            SHF_ID = ""
        Else
            SHF_ID = ddlShift.SelectedItem.Value
        End If
        If ddlStream.SelectedIndex = -1 Then
            STM_ID = ""
        Else
            STM_ID = ddlStream.SelectedItem.Value
        End If
        If rdGemsGr.Checked = True Then
            rdGemsGr_CheckedChanged(rdGemsGr, Nothing)
        Else
            rdOther_CheckedChanged(rdOther, Nothing)
        End If

        If Not (Request.QueryString("MainMnu_code")) Is Nothing Then

            Using EnableButton_reader As SqlDataReader = AccessStudentClass.getPick_Stream_Registrar(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID, STM_ID)

                If EnableButton_reader.HasRows = True Then

                    ViewState("Acd_Valid") = "1" 'if open for registration

                Else
                    ViewState("Acd_Valid") = "-1" 'if open for registration

                End If

            End Using

        Else
            Using EnableButton_reader As SqlDataReader = AccessStudentClass.getPick_Stream(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID, STM_ID)

                If EnableButton_reader.HasRows = True Then

                    ViewState("Acd_Valid") = "1" 'if open for registration

                Else
                    ViewState("Acd_Valid") = "-1" 'if open for registration

                End If

            End Using


        End If




    End Sub
    Sub GetBusinessUnits_info_student()
        Try


            Using AllStudent_BSU_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_Student()

                Dim di_Student_BSU As ListItem
                ddlExStud_Gems.Items.Clear()
                ddlSib_BSU.Items.Clear()
                ddlGemsGr.Items.Clear()
                'di_Religion = New ListItem("--", "--")
                ' ddlReligion.Items.Add(di_Religion)

                If AllStudent_BSU_reader.HasRows = True Then
                    While AllStudent_BSU_reader.Read

                        di_Student_BSU = New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id"))
                        ddlExStud_Gems.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))
                        ddlSib_BSU.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))
                        ddlGemsGr.Items.Add(New ListItem(AllStudent_BSU_reader("bsu_name"), AllStudent_BSU_reader("bsu_id")))
                    End While


                End If
            End Using

            If Not ddlGemsGr.Items.FindByValue(ddlGEMSSchool.SelectedValue) Is Nothing Then
                ddlGemsGr.ClearSelection()
                ddlGemsGr.Items.FindByValue(ddlGEMSSchool.SelectedValue).Selected = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_student()")
        End Try
    End Sub
    'first GEMS BSU is Called
    Sub GEMS_BSU_4_Student()

        Using BSU_4_Student_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_Student()

            Dim di_Grade_BSU As ListItem
            ddlGEMSSchool.Items.Clear()

            If BSU_4_Student_reader.HasRows = True Then
                While BSU_4_Student_reader.Read
                    di_Grade_BSU = New ListItem(BSU_4_Student_reader("bsu_name"), BSU_4_Student_reader("bsu_id"))
                    ddlGEMSSchool.Items.Add(di_Grade_BSU)

                End While
                'ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using

    End Sub
    Sub CURRICULUM_BSU_4_Student()

        Dim GEMSSchool As String = String.Empty
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If




        Using CURRICULUM_BSU_4_Student_reader As SqlDataReader = AccessStudentClass.GetCURRICULUM_BSU(GEMSSchool)
            Dim di_CURRICULUM_BSU As ListItem
            ddlCurri.Items.Clear()
            If CURRICULUM_BSU_4_Student_reader.HasRows = True Then
                While CURRICULUM_BSU_4_Student_reader.Read
                    di_CURRICULUM_BSU = New ListItem(CURRICULUM_BSU_4_Student_reader("CLM_DESCR"), CURRICULUM_BSU_4_Student_reader("CLM_ID"))
                    ddlCurri.Items.Add(di_CURRICULUM_BSU)
                End While

                ViewState("Acd_Valid") = "1" 'if open for registration
                'ddlCurri.SelectedIndex = 0
                ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            Else

                ViewState("Acd_Valid") = "-1" 'if not open for registration



                ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using

    End Sub
    Sub GetAcademicYear_Bsu()
        Try

            Dim Curri As String = String.Empty
            Dim GEMSSchool As String = String.Empty


            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If
            If ddlCurri.SelectedIndex = -1 Then
                Curri = ""
            Else
                Curri = ddlCurri.SelectedItem.Value
            End If

            If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
                Using OpenBsu_reader As SqlDataReader = AccessStudentClass.GetActive_BSU_Year_Registrar(GEMSSchool, Curri)
                    Dim di_Aca_Year As ListItem
                    ddlAca_Year.Items.Clear()

                    If OpenBsu_reader.HasRows = True Then
                        While OpenBsu_reader.Read
                            di_Aca_Year = New ListItem(OpenBsu_reader("ACY_DESCR"), OpenBsu_reader("ACY_ID"))
                            ddlAca_Year.Items.Add(di_Aca_Year)

                        End While


                        ViewState("Acd_Valid") = "1" 'if open for registration

                        Call showActive_year()


                        ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)


                    Else

                        ViewState("Acd_Valid") = "-1" 'if not open for registration

                        ' ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
                        ddlAca_Year.Items.Clear()
                        ddlGrade.Items.Clear()
                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If
                End Using

            Else

                Using OpenBsu_reader As SqlDataReader = AccessStudentClass.GetActive_BSU_Year(GEMSSchool, Curri)
                    Dim di_Aca_Year As ListItem
                    ddlAca_Year.Items.Clear()

                    If OpenBsu_reader.HasRows = True Then
                        While OpenBsu_reader.Read
                            di_Aca_Year = New ListItem(OpenBsu_reader("ACY_DESCR"), OpenBsu_reader("ACY_ID"))
                            ddlAca_Year.Items.Add(di_Aca_Year)

                        End While

                        ViewState("Acd_Valid") = "1" 'if open for registration
                        ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)


                    Else

                        ViewState("Acd_Valid") = "-1" 'if not open for registration



                        ddlAca_Year.Items.Clear()
                        ddlGrade.Items.Clear()
                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If
                End Using

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetAcademicYear_Bsu")
        End Try
    End Sub
    Sub showActive_year()

        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty


        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        Using readerAcademic_Cutoff As SqlDataReader = AccessStudentClass.GetActive_BsuAndCutOff(GEMSSchool, Curri)
            If readerAcademic_Cutoff.HasRows = True Then
                While readerAcademic_Cutoff.Read
                    ddlAca_Year.ClearSelection()
                    ddlAca_Year.Items.FindByValue(Convert.ToString(readerAcademic_Cutoff("ACD_ACY_ID"))).Selected = True
                End While
            End If
        End Using
    End Sub
    Sub GetAllGrade_Bsu()
        Try
            Dim Aca_Year As String = String.Empty
            Dim Curri As String = String.Empty
            Dim GEMSSchool As String = String.Empty

            If ddlAca_Year.SelectedIndex = -1 Then
                Aca_Year = ""
            Else
                Aca_Year = ddlAca_Year.SelectedItem.Value
            End If
            If ddlCurri.SelectedIndex = -1 Then
                Curri = ""
            Else
                Curri = ddlCurri.SelectedItem.Value
            End If
            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If


            If Not (Request.QueryString("MainMnu_code")) Is Nothing Then

                Using AllGrade_reader As SqlDataReader = AccessStudentClass.GetAllGrade_Bsu_Registrar(Aca_Year, GEMSSchool, Curri)
                    Dim di_Grade As ListItem
                    ddlGrade.Items.Clear()

                    If AllGrade_reader.HasRows = True Then
                        While AllGrade_reader.Read
                            di_Grade = New ListItem(AllGrade_reader("GRM_Display"), AllGrade_reader("GRD_ID"))
                            ddlGrade.Items.Add(di_Grade)

                        End While

                        ViewState("Acd_Valid") = "1" 'if open for registration
                        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
                    Else


                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If


                End Using


            Else
                Using AllGrade_reader As SqlDataReader = AccessStudentClass.GetAllGrade_Bsu(Aca_Year, GEMSSchool, Curri)
                    Dim di_Grade As ListItem
                    ddlGrade.Items.Clear()

                    If AllGrade_reader.HasRows = True Then
                        While AllGrade_reader.Read
                            di_Grade = New ListItem(AllGrade_reader("GRM_Display"), AllGrade_reader("GRD_ID"))
                            ddlGrade.Items.Add(di_Grade)

                        End While

                        ViewState("Acd_Valid") = "1" 'if open for registration
                        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
                    Else



                        ddlShift.Items.Clear()
                        ddlStream.Items.Clear()
                        ddlTerm.Items.Clear()
                    End If


                End Using
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetAllGrade_Bsu")
        End Try
    End Sub
    'Getting all the grade of currently open BSU
    Sub GetAllGrade_Shift()

        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim Grade As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        If ddlGrade.SelectedIndex = -1 Then
            Grade = ""
        Else
            Grade = ddlGrade.SelectedItem.Value
        End If

        If Not (Request.QueryString("MainMnu_code")) Is Nothing Then

            Using AllGrade_Shift_reader As SqlDataReader = AccessStudentClass.getAllShift_4_Grade_Registrar(Aca_Year, Curri, GEMSSchool, Grade)
                Dim di_Grade_Shift As ListItem
                Dim Shift_count As Integer
                ddlShift.Items.Clear()

                If AllGrade_Shift_reader.HasRows = True Then
                    While AllGrade_Shift_reader.Read
                        di_Grade_Shift = New ListItem(AllGrade_Shift_reader("SHF_DESCR"), AllGrade_Shift_reader("SHF_ID"))
                        ddlShift.Items.Add(di_Grade_Shift)
                        Shift_count += 1
                    End While
                    'code modified on 20th apr-2008
                    'If Shift_count > 1 Then
                    '    ddlShift.Enabled = True
                    'Else
                    '    ddlShift.Enabled = False
                    'End If
                    'end


                    ViewState("Acd_Valid") = "1" 'if open for registration
                    ddlShift_SelectedIndexChanged(ddlShift, Nothing)
                Else


                    ddlStream.Items.Clear()
                    ddlTerm.Items.Clear()
                End If

            End Using

        Else

            Using AllGrade_Shift_reader As SqlDataReader = AccessStudentClass.getAllShift_4_Grade(Aca_Year, Curri, GEMSSchool, Grade)
                Dim di_Grade_Shift As ListItem
                Dim Shift_count As Integer
                ddlShift.Items.Clear()

                If AllGrade_Shift_reader.HasRows = True Then
                    While AllGrade_Shift_reader.Read
                        di_Grade_Shift = New ListItem(AllGrade_Shift_reader("SHF_DESCR"), AllGrade_Shift_reader("SHF_ID"))
                        ddlShift.Items.Add(di_Grade_Shift)
                        Shift_count += 1
                    End While
                    'code modified on 20th apr-2008
                    'If Shift_count > 1 Then
                    '    ddlShift.Enabled = True
                    'Else
                    '    ddlShift.Enabled = False
                    'End If
                    'end


                    ViewState("Acd_Valid") = "1" 'if open for registration
                    ddlShift_SelectedIndexChanged(ddlShift, Nothing)
                Else

                    ddlStream.Items.Clear()
                    ddlTerm.Items.Clear()
                End If

            End Using

        End If






    End Sub
    Sub GetStudent_Stream()

        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim Grade As String = String.Empty
        Dim SHF_ID As String = String.Empty
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If


        If ddlGrade.SelectedIndex = -1 Then
            Grade = ""
        Else
            Grade = ddlGrade.SelectedItem.Value
        End If

        If ddlShift.SelectedIndex = -1 Then
            SHF_ID = ""
        Else
            SHF_ID = ddlShift.SelectedItem.Value
        End If

        If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
            Using Student_Stream_reader As SqlDataReader = AccessStudentClass.getPick_Stream_Only_Registrar(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID)
                Dim di_Stream As ListItem
                Dim Stream_Count As Integer
                ddlStream.Items.Clear()

                If Student_Stream_reader.HasRows = True Then
                    While Student_Stream_reader.Read
                        di_Stream = New ListItem(Student_Stream_reader("STM_DESCR"), Student_Stream_reader("STM_ID"))
                        ddlStream.Items.Add(di_Stream)
                        Stream_Count = Stream_Count + 1
                    End While
                    'Code Modified 20-apr-2008
                    'If Stream_Count > 1 Then
                    '    ddlStream.Enabled = True
                    'Else
                    '    ddlStream.Enabled = False
                    'End If
                    'End

                    ViewState("Acd_Valid") = "1" 'if open for registration
                Else

                    ddlTerm.Items.Clear()
                End If

            End Using


        Else
            Using Student_Stream_reader As SqlDataReader = AccessStudentClass.getPick_Stream_Only(Aca_Year, Curri, GEMSSchool, Grade, SHF_ID)
                Dim di_Stream As ListItem
                Dim Stream_Count As Integer
                ddlStream.Items.Clear()

                If Student_Stream_reader.HasRows = True Then
                    While Student_Stream_reader.Read
                        di_Stream = New ListItem(Student_Stream_reader("STM_DESCR"), Student_Stream_reader("STM_ID"))
                        ddlStream.Items.Add(di_Stream)
                        Stream_Count = Stream_Count + 1
                    End While
                    'Code Modified 20-apr-2008
                    'If Stream_Count > 1 Then
                    '    ddlStream.Enabled = True
                    'Else
                    '    ddlStream.Enabled = False
                    'End If
                    'End

                    ViewState("Acd_Valid") = "1" 'if open for registration
                Else

                    ddlTerm.Items.Clear()
                End If

            End Using
        End If
    End Sub
    Sub GEMS_BSU_AcadStart()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty

        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If

        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If
        Using BSU_AcadStart_reader As SqlDataReader = AccessStudentClass.GetBSU_M_AcadStart(GEMSSchool, Aca_Year, Curri)



            While BSU_AcadStart_reader.Read
                txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", BSU_AcadStart_reader("ACD_STARTDT"))
            End While
        End Using

    End Sub
    Sub CheckTermDate()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim TRMID As String = String.Empty

        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value
        End If

        If ddlTerm.SelectedIndex = -1 Then
            TRMID = ""
        Else
            TRMID = ddlTerm.SelectedItem.Value
        End If
        Using Term_4ACD_reader As SqlDataReader = AccessStudentClass.GetTerm_CheckDate(Aca_Year, Curri, GEMSSchool, TRMID)

            If Term_4ACD_reader.HasRows = True Then
                While Term_4ACD_reader.Read

                    If IsDate(Term_4ACD_reader("Trm_startDate")) = True Then
                        ViewState("TRM_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("Trm_startDate")))).Replace("01/Jan/1900", "")
                    End If

                    If IsDate(Term_4ACD_reader("trm_endDate")) = True Then
                        ViewState("TRM_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("trm_endDate")))).Replace("01/Jan/1900", "")
                    End If


                End While


            End If
        End Using

    End Sub
    'getting the term for which currently opened BSU_ID academic year ID
    Sub GetTerm_4ACD()
        Dim Aca_Year As String = String.Empty
        Dim Curri As String = String.Empty
        Dim GEMSSchool As String = String.Empty
        Dim termDate()
        Dim i As Integer = 0
        If ddlAca_Year.SelectedIndex = -1 Then
            Aca_Year = ""
        Else
            Aca_Year = ddlAca_Year.SelectedItem.Value
        End If
        If ddlCurri.SelectedIndex = -1 Then
            Curri = ""
        Else
            Curri = ddlCurri.SelectedItem.Value
        End If
        If ddlGEMSSchool.SelectedIndex = -1 Then
            GEMSSchool = ""
        Else
            GEMSSchool = ddlGEMSSchool.SelectedItem.Value

        End If

        Using Term_4ACD_reader As SqlDataReader = AccessStudentClass.GetTerm_4ACD(Aca_Year, Curri, GEMSSchool)
            Dim di_Term As ListItem
            ddlTerm.Items.Clear()

            If Term_4ACD_reader.HasRows = True Then
                While Term_4ACD_reader.Read
                    di_Term = New ListItem(Term_4ACD_reader("TDescr"), Term_4ACD_reader("TRM_ID"))
                    ddlTerm.Items.Add(di_Term)
                    If IsDate(Term_4ACD_reader("Trm_startDate")) = True Then
                        ViewState("TRM_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("Trm_startDate")))).Replace("01/Jan/1900", "")
                        ReDim Preserve termDate(i)
                        termDate(i) = ViewState("TRM_STARTDT")
                        i = i + 1
                    End If

                    If IsDate(Term_4ACD_reader("trm_endDate")) = True Then


                        ViewState("TRM_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((Term_4ACD_reader("trm_endDate")))).Replace("01/Jan/1900", "")
                    End If


                End While
            Else


                ViewState("TRM_STARTDT") = ""
                ViewState("TRM_ENDDT") = ""


            End If
        End Using
        Session("termSTDT") = termDate

        ddlTerm_SelectedIndexChanged(ddlTerm, Nothing)
    End Sub
    'Sub EnquryOpen_for()
    '    Dim ACY_ID As String = String.Empty
    '    Dim CLM_ID As String = String.Empty
    '    Dim BSU_ID As String = String.Empty
    '    Dim GRD_ID As String = String.Empty


    '    If ddlAca_Year.SelectedIndex = -1 Then
    '        ACY_ID = ""
    '    Else
    '        ACY_ID = ddlAca_Year.SelectedItem.Value
    '    End If
    '    If ddlCurri.SelectedIndex = -1 Then
    '        CLM_ID = ""
    '    Else
    '        CLM_ID = ddlCurri.SelectedItem.Value
    '    End If
    '    If ddlGEMSSchool.SelectedIndex = -1 Then
    '        BSU_ID = ""
    '    Else
    '        BSU_ID = ddlGEMSSchool.SelectedItem.Value

    '    End If
    '    If ddlGrade.SelectedIndex = -1 Then
    '        GRD_ID = ""
    '    Else
    '        GRD_ID = ddlGrade.SelectedItem.Value
    '    End If
    '    Dim separator As String() = New String() {"|"}

    '    Dim varsib_BSU As String = String.Empty
    '    Dim varstaff_BSU As String = String.Empty
    '    Dim varsister_BSU As String = String.Empty
    '    Using readerEnquryOpen_for As SqlDataReader = AccessStudentClass.GetEnquiry_open_for(GRD_ID, ACY_ID, CLM_ID, BSU_ID)
    '        If readerEnquryOpen_for.HasRows = True Then
    '            ViewState("OpenFor") = 1
    '            While readerEnquryOpen_for.Read

    '                If Convert.ToString(readerEnquryOpen_for("EQO_DESC")) = "GEMS Staff" Then
    '                    ViewState("S_STAFF_GEMS") = Convert.ToString(readerEnquryOpen_for("EQS_BSU_IDS"))
    '                    varstaff_BSU = Convert.ToString(readerEnquryOpen_for("EQO_DESC"))

    '                    Dim strSplitArr As String() = ViewState("S_STAFF_GEMS").Split(separator, StringSplitOptions.RemoveEmptyEntries)
    '                    Dim BSU_IDs As String = String.Empty
    '                    For Each arrStr As String In strSplitArr
    '                        BSU_IDs += "'" & arrStr & "'"
    '                    Next

    '                    Call OpenStaff_BSU_Populate(BSU_IDs)
    '                End If

    '                If Convert.ToString(readerEnquryOpen_for("EQO_DESC")) = "Sibling" Then
    '                    ViewState("S_GEMS_SIBIL") = Convert.ToString(readerEnquryOpen_for("EQS_BSU_IDS"))
    '                    varsib_BSU = Convert.ToString(readerEnquryOpen_for("EQO_DESC"))

    '                    Dim strSplitArr As String() = ViewState("S_GEMS_SIBIL").Split(separator, StringSplitOptions.RemoveEmptyEntries)
    '                    Dim BSU_IDs As String = String.Empty
    '                    For Each arrStr As String In strSplitArr
    '                        BSU_IDs += "'" & arrStr & "'"

    '                    Next
    '                    Call OpenSib_BSU_Populate(BSU_IDs)
    '                End If

    '                If Convert.ToString(readerEnquryOpen_for("EQO_DESC")) = "Sister Concern Schools" Then
    '                    Session("S_SISTER_GEMS") = Convert.ToString(readerEnquryOpen_for("EQS_BSU_IDS"))
    '                    varsister_BSU = Convert.ToString(readerEnquryOpen_for("EQO_DESC"))

    '                End If
    '            End While
    '        Else
    '            ViewState("OpenFor") = 0

    '        End If
    '        Dim strOpenFor As String = String.Empty
    '        Dim strDetail As String = String.Empty
    '        Dim orCheck As Integer = 0
    '        If varstaff_BSU <> "" Then
    '            orCheck = 1
    '            ViewState("V_STAFF") = varstaff_BSU
    '            strDetail = "Staff ID(in Primary Contact)"
    '            strOpenFor = varstaff_BSU & ","
    '        Else
    '            ViewState("V_STAFF") = ""
    '            ViewState("S_STAFF_GEMS") = ""
    '        End If
    '        If varsib_BSU <> "" Then
    '            orCheck += 1
    '            ViewState("V_SIB") = varsib_BSU
    '            strOpenFor += varsib_BSU & ","
    '            If orCheck > 1 Then
    '                strDetail += " or Sibling Fee ID(in Student Details)"
    '            Else
    '                strDetail += "Sibling Fee ID(in Student Details)"
    '            End If
    '        Else
    '            ViewState("V_SIB") = ""
    '            ViewState("S_GEMS_SIBIL") = ""

    '        End If
    '        If varsister_BSU <> "" Then
    '            orCheck += 1
    '            ViewState("V_SISTER") = varsister_BSU
    '            strOpenFor += varsister_BSU & ","
    '            If orCheck > 1 Then
    '                strDetail += " or School Name(in Current School)"
    '            Else
    '                strDetail += "School Name(in Current School)"
    '            End If
    '        Else
    '            ViewState("V_SISTER") = ""
    '            Session("S_SISTER_GEMS") = ""
    '        End If


    '        ViewState("orCheck") = orCheck
    '        strOpenFor = strOpenFor.TrimEnd(",")
    '        strOpenFor = strOpenFor.TrimStart(",")
    '        strDetail = strDetail.TrimStart(",")
    '        strDetail = strDetail.TrimEnd(",")

    '        Dim flag As Integer = 0
    '        If varstaff_BSU <> "" Then
    '            flag = 1
    '        ElseIf varsib_BSU <> "" Then
    '            flag = 1
    '        ElseIf varsister_BSU <> "" Then
    '            flag = 1
    '        Else
    '            flag = 0
    '        End If
    '        If flag = 1 Then

    '            If Not (Request.QueryString("MainMnu_code")) Is Nothing Then
    '                ltMessage.Visible = False
    '            Else
    '                ltMessage.Visible = True
    '                ltMessage.Text = "<marquee direction='left'  behavior='SCROLL' scrolldelay='180' VSPACE='5'><font color='#1B80B6' size=1px' face='Verdana, Times New Roman'><FONT COLOR='RED' >*</FONT><B>Online Enquiry is currently open for " & strOpenFor & " only</B><div><I> &nbsp;&nbsp;Note: " & strDetail & "  required</I></div></font></marquee>"
    '                ViewState("ERROR_DETAIL") = strDetail & "  required"
    '            End If

    '        Else
    '            ltMessage.Visible = False
    '        End If



    '    End Using
    'End Sub

    Sub callAll_fun_OnPageLoad()
        Try
            Call Populate_YearList()
            Call GetBusinessUnits_info_student()
            Call GetPRENURSERY()
            Call GetBusinessUnits_info_staff()
            Call GetReligion_info()
            'Call GetCity_info()
            Call GetCountry_info()
            Call GetNational_info()

            'Call GetTelePhone_info_Mobile("M")
            'Call GetTelePhone_info("L")
            Call GetCurriculum_Info()
            Call GetGrade_info()
            Call GetMain_Loc()
            Call GetSub_Main_Loc()
            Call GetSub_PickUp_point()
            Call GetCompany_Name()
            Call GetEmirate_info()
            Call AboutUs()
            Call bindLanguage_dropDown()
            'Call GEMS_BSU_AcadStart()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "callAll_fun_OnPageLoad")
        End Try

    End Sub
    Private Sub bindLanguage_dropDown()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT LNG_ID,LNG_DESCR  FROM LANGUAGE_M order by LNG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlFLang.DataSource = ds
        ddlFLang.DataTextField = "LNG_DESCR"
        ddlFLang.DataValueField = "LNG_ID"
        ddlFLang.DataBind()
        chkOLang.DataSource = ds
        chkOLang.DataTextField = "LNG_DESCR"
        chkOLang.DataValueField = "LNG_ID"
        chkOLang.DataBind()
        ddlFLang.Items.Add(New ListItem("", ""))
        ddlFLang.ClearSelection()
        ddlFLang.Items.FindByText("").Selected = True
    End Sub
    Sub GetCompany_Name()
        Try

            Using GetComp_Name_reader As SqlDataReader = AccessStudentClass.GetCompany_Name()

                Dim di_Company As ListItem
                ddlCompany.Items.Clear()
                di_Company = New ListItem("Other", "0")
                ddlCompany.Items.Add(di_Company)
                If GetComp_Name_reader.HasRows = True Then
                    While GetComp_Name_reader.Read

                        di_Company = New ListItem(GetComp_Name_reader("comp_Name"), GetComp_Name_reader("comp_ID"))
                        ddlCompany.Items.Add(di_Company)

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCompany_Name()")
        End Try
    End Sub
    'Getting Business Unit info
    Sub GetBusinessUnits_info_staff()
        Try
            Using AllStaff_BSU_reader As SqlDataReader = AccessStudentClass.GetBSU_M_form_staff()

                Dim di_Staff_BSU As ListItem
                ddlStaff_BSU.Items.Clear()

                If AllStaff_BSU_reader.HasRows = True Then
                    While AllStaff_BSU_reader.Read

                        di_Staff_BSU = New ListItem(AllStaff_BSU_reader("bsu_name"), AllStaff_BSU_reader("bsu_id"))
                        ddlStaff_BSU.Items.Add(di_Staff_BSU)

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_staff()")
        End Try
    End Sub
    'Getting Religion info
    'code modified on 12/AUG/2008
    Sub GetReligion_info()
        Try
            Dim BSU_ID As String = ddlGEMSSchool.SelectedItem.Value

            Using AllReligion_reader As SqlDataReader = AccessStudentClass.GetStud_BSU_Religion(BSU_ID)

                Dim di_Religion As ListItem
                ddlReligion.Items.Clear()
                ddlReligion.Items.Add(New ListItem("", ""))
                If AllReligion_reader.HasRows = True Then
                    While AllReligion_reader.Read

                        di_Religion = New ListItem(AllReligion_reader("RLG_DESCR"), AllReligion_reader("RLG_ID"))
                        ddlReligion.Items.Add(di_Religion)

                    End While
                Else
                    di_Religion = New ListItem("", "")
                    ddlReligion.Items.Add(di_Religion)
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetReligion_info")
        End Try
    End Sub
    Sub GetCountry_info()
        Try


            Using AllCountry_reader As SqlDataReader = AccessRoleUser.GetCountry()
                Dim di_Country As ListItem
                ddlCountry.Items.Clear()
                ddlPre_Country.Items.Clear()
                ddlCountry_Pri.Items.Clear()
                ddlOverSeas_Add_Country.Items.Clear()

                ddlPre_Country.Items.Add(New ListItem("", ""))
                ddlCountry.Items.Add(New ListItem("", ""))
                ddlCountry_Pri.Items.Add(New ListItem("", ""))
                ddlOverSeas_Add_Country.Items.Add(New ListItem("", ""))

                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID"))

                        ddlPre_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlCountry_Pri.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlOverSeas_Add_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                    End While
                End If

                ddlCountry_Pri.ClearSelection()
                ddlCountry_Pri.Items.FindByValue("172").Selected = True

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub
    'Getting national info
    Sub GetNational_info()
        Try
            Using AllNational_reader As SqlDataReader = AccessStudentClass.GetNational()
                Dim di_National As ListItem
                ddlPri_National1.Items.Clear()
                ddlPri_National2.Items.Clear()
                ddlNational.Items.Clear()



                ddlPri_National1.Items.Add(New ListItem("", ""))
                ddlPri_National2.Items.Add(New ListItem("", ""))
                ddlNational.Items.Add(New ListItem("", ""))


                If AllNational_reader.HasRows = True Then
                    While AllNational_reader.Read()
                        di_National = New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID"))
                        ddlPri_National1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlPri_National2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlNational.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetNational_info")
        End Try
    End Sub
    ' Getting Emirates info
    Sub GetEmirate_info()
        Try
            Using AllEmirate_reader As SqlDataReader = AccessStudentClass.GetEmirate()
                Dim di_Emirate As ListItem
                ddlEmirate.Items.Clear()

                If AllEmirate_reader.HasRows = True Then
                    While AllEmirate_reader.Read
                        di_Emirate = New ListItem(AllEmirate_reader("EMR_DESCR"), AllEmirate_reader("EMR_CODE"))
                        ddlEmirate.Items.Add(di_Emirate)

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetEmirate_info")
        End Try
    End Sub
    Sub GetCurriculum_Info()
        Try
            Using AllCurriculum_reader As SqlDataReader = AccessStudentClass.GetCURRICULUM_M()

                Dim di_Curriculum As ListItem
                ddlPre_Curriculum.Items.Clear()

                If AllCurriculum_reader.HasRows = True Then
                    While AllCurriculum_reader.Read
                        di_Curriculum = New ListItem(AllCurriculum_reader("CLM_DESCR"), AllCurriculum_reader("CLM_ID"))
                        ddlPre_Curriculum.Items.Add(di_Curriculum)

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetBusinessUnits_info_staff()")
        End Try
    End Sub
    'Getting Grade Info
    Sub GetPRENURSERY()
        'ddlPreSchool_Nursery
        Try
            Dim defaultNursery As String = "--"


            Dim di As ListItem
            Using PRENURSERYreader As SqlDataReader = AccessStudentClass.GetPRENURSERY_M()
                ddlPreSchool_Nursery.Items.Clear()


                If PRENURSERYreader.HasRows = True Then
                    While PRENURSERYreader.Read
                        di = New ListItem(PRENURSERYreader("SCH_DESCR"), PRENURSERYreader("SCH_CODE"))
                        ddlPreSchool_Nursery.Items.Add(di)
                    End While
                End If
            End Using

            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlPreSchool_Nursery.Items.Count - 1
                If ddlPreSchool_Nursery.Items(ItemTypeCounter).Value = "4" Then
                    ddlPreSchool_Nursery.SelectedIndex = ItemTypeCounter
                End If
            Next


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try





    End Sub
    'SCH_CODE,SCH_DESCR
    Sub GetGrade_info()
        Try


            Using AllGrade_reader As SqlDataReader = AccessStudentClass.GetGrade_M()

                'Dim di_Grade As ListItem
                ddlPre_Grade.Items.Clear()
                If AllGrade_reader.HasRows = True Then
                    While AllGrade_reader.Read
                        ' di_Grade = New ListItem(AllGrade_reader("GRD_ID"), AllGrade_reader("GRD_ID"))
                        ddlPre_Grade.Items.Add(New ListItem(AllGrade_reader("GRD_ID"), AllGrade_reader("GRD_ID")))

                    End While
                End If
            End Using


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetGrade_info")
        End Try
    End Sub
    Sub GetMain_Loc()
        Try

            Dim GEMSSchool As String = String.Empty

            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If

            Using Main_Loc_reader As SqlDataReader = AccessStudentClass.getMain_Location(GEMSSchool)
                Dim di_Main_Loc As ListItem
                ddlMainLocation.Items.Clear()
                ddlMainLocation.Items.Add(New ListItem("", ""))
                If Main_Loc_reader.HasRows = True Then
                    While Main_Loc_reader.Read
                        di_Main_Loc = New ListItem(Main_Loc_reader("LOC_DESCRIPTION"), Main_Loc_reader("LOC_ID"))
                        ddlMainLocation.Items.Add(di_Main_Loc)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetMain_Loc")

        End Try
    End Sub
    Sub GetSub_Main_Loc()
        Try
            Dim MainLocation As String = String.Empty

            Dim GEMSSchool As String = String.Empty

            If ddlMainLocation.SelectedIndex = -1 Then
                MainLocation = ""
            Else
                MainLocation = ddlMainLocation.SelectedItem.Value
            End If

            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If


            Using Sub_Main_Loc_reader As SqlDataReader = AccessStudentClass.getSubMain_Location(GEMSSchool, MainLocation)
                Dim di_Sub_Main_Loc As ListItem
                ddlSubLocation.Items.Clear()
                ddlSubLocation.Items.Add(New ListItem("", ""))
                If Sub_Main_Loc_reader.HasRows = True Then
                    While Sub_Main_Loc_reader.Read
                        di_Sub_Main_Loc = New ListItem(Sub_Main_Loc_reader("SBL_DESCRIPTION"), Sub_Main_Loc_reader("SBL_ID"))
                        ddlSubLocation.Items.Add(di_Sub_Main_Loc)
                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetSub_Main_Loc")

        End Try
    End Sub
    Sub GetSub_PickUp_point()
        Try

            Dim SubLocation As String = String.Empty

            Dim GEMSSchool As String = String.Empty

            If ddlSubLocation.SelectedIndex = -1 Then
                SubLocation = ""
            Else
                SubLocation = ddlSubLocation.SelectedItem.Value
            End If

            If ddlGEMSSchool.SelectedIndex = -1 Then
                GEMSSchool = ""
            Else
                GEMSSchool = ddlGEMSSchool.SelectedItem.Value

            End If


            Using PickUp_point_reader As SqlDataReader = AccessStudentClass.getPickUP_Points(GEMSSchool, SubLocation)
                Dim di_PickUp As ListItem
                ddlPickup.Items.Clear()
                ' di_PickUp = New ListItem("Other", "")
                ddlPickup.Items.Add(New ListItem("", ""))
                If PickUp_point_reader.HasRows = True Then
                    While PickUp_point_reader.Read
                        di_PickUp = New ListItem(PickUp_point_reader("PNT_DESCRIPTION"), PickUp_point_reader("PNT_ID"))
                        ddlPickup.Items.Add(di_PickUp)
                    End While
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Sub_PickUp_point")

        End Try
    End Sub
    Sub ControlTable_row()
        If (ViewState("Table_row") = "KG1") Or (ViewState("Table_row") = "PK") Then
            'rdGemsGr.Checked = True
            ' ddlPreSchool_Nursery.SelectedIndex = 0

            h_Grade_KG1.Value = "1"



        Else
            h_Grade_KG1.Value = "2"

            '  ddlGemsGr_SelectedIndexChanged(ddlGemsGr, Nothing)

            If rdOther.Checked = True Then
                txtPre_School.Visible = True
                ddlGemsGr.Visible = False
            Else
                ddlGemsGr.Visible = True
                txtPre_School.Visible = False
            End If


            If Not ddlGemsGr.Items.FindByValue(ddlGEMSSchool.SelectedValue) Is Nothing Then
                ddlGemsGr.ClearSelection()
                ddlGemsGr.Items.FindByValue(ddlGEMSSchool.SelectedValue).Selected = True
            End If

            ddlPre_Grade.ClearSelection()
            ddlPre_Grade.Items.FindByText(GetCurrGrade()).Selected = True


            ' txtPre_School.Text = ""

            ' rfv_Pre_school.Visible = False

        End If
    End Sub
#End Region
#Region "For Selected Index change event"
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        ' Call GetTerm_4ACD(ddlAca_Year.SelectedValue, ddlGrade.SelectedValue)
        'check if grade is open or not
        ViewState("Table_row") = ddlGrade.SelectedItem.Value


        Call ControlTable_row()
        'fifth shift is called
        Call GetAllGrade_Shift()

        ' Call EnquryOpen_for()

        ' Call GetSchoolButton_Enable()
    End Sub
    Protected Sub ddlAca_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAca_Year.SelectedIndexChanged
        'forth Grade is called
        Call GetAllGrade_Bsu()
        Call GetTerm_4ACD()
        '  Call ControlTable_row()
        Call GetSchoolButton_Enable()
        Call GEMS_BSU_AcadStart()
    End Sub
    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged

        Dim trm_ID As String = String.Empty

        If ddlTerm.SelectedIndex = -1 Then
            trm_ID = ""
        Else
            trm_ID = ddlTerm.SelectedItem.Value
        End If
        Call GEMS_BSU_AcadStart()
        If Not Session("termSTDT") Is Nothing Then
            If ddlTerm.SelectedIndex <> -1 Then
                txtDOJ.Text = Session("termSTDT")(ddlTerm.SelectedIndex)
            End If
        End If

        ' Dim sdate As String = AccessStudentClass.GetTermDate(trm_ID)
        ' txtDOJ.Text = ""
        'txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
    End Sub
#End Region
    Public Function GetPrevGrade() As String

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select GRD_DISPLAY from GRADE_M where GRD_DISPLAYORDER=(SELECT GRD_DISPLAYORDER FROM GRADE_M where GRD_DISPLAY='" & ddlGrade.SelectedItem.Value & "')-1"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString


    End Function
    Public Function GetCurrGrade() As String

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select GRD_DISPLAY from GRADE_M where GRD_DISPLAYORDER=(SELECT GRD_DISPLAYORDER FROM GRADE_M where GRD_DISPLAY='" & ddlGrade.SelectedItem.Value & "')"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql).ToString

    End Function

    Sub SiblingInfo()
        Try
            Dim mobNumber As String()
            Dim hNumber As String()
            Dim oNumber As String()
            Dim fNumber As String()
            Using sibl_reader As SqlDataReader = AccessStudentClass.GetSiblingDetails(txtSib_ID.Text, ddlSib_BSU.SelectedValue)
                While sibl_reader.Read
                    If sibl_reader("STU_PRIMARYCONTACT") = "F" Then

                        rdPri_Father.Checked = True
                        txtPri_Fname.Text = sibl_reader("STS_FFIRSTNAME")
                        txtPri_Mname.Text = sibl_reader("STS_FMIDNAME")
                        txtPri_Lname.Text = sibl_reader("STS_FLASTNAME")
                        If Not ddlPri_National1.Items.FindByValue(sibl_reader("STS_FNATIONALITY")) Is Nothing Then
                            ddlPri_National1.ClearSelection()
                            ddlPri_National1.Items.FindByValue(sibl_reader("STS_FNATIONALITY")).Selected = True
                        End If
                        If Not ddlPri_National2.Items.FindByValue(sibl_reader("STS_FNATIONALITY2")) Is Nothing Then
                            ddlPri_National2.ClearSelection()
                            ddlPri_National2.Items.FindByValue(sibl_reader("STS_FNATIONALITY2")).Selected = True
                        End If
                        mobNumber = sibl_reader("STS_FMOBILE").ToString.Split(" ")
                        If mobNumber.Length = 3 Then
                            txtM_Country.Text = mobNumber(0)
                            txtM_Area.Text = mobNumber(1)
                            txtMobile_Pri.Text = mobNumber(2)
                        ElseIf mobNumber.Length = 2 Then
                            txtM_Area.Text = mobNumber(0)
                            txtMobile_Pri.Text = mobNumber(1)
                        ElseIf mobNumber.Length = 1 Then
                            txtMobile_Pri.Text = mobNumber(0)
                        End If

                        txtPoBox_Pri.Text = sibl_reader("STS_FPRMPOBOX")
                        txtEmail_Pri.Text = sibl_reader("STS_FEMAIL").ToString.ToLower
                        txtAdd1_overSea.Text = sibl_reader("STS_FPRMADDR1")
                        txtAdd2_overSea.Text = sibl_reader("STS_FPRMADDR2")


                        If Not ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_FPRMCOUNTRY")) Is Nothing Then
                            ddlOverSeas_Add_Country.ClearSelection()
                            ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_FPRMCOUNTRY")).Selected = True
                        End If

                        txtStreet.Text = sibl_reader("STS_FCOMADDR1")
                        txtArea.Text = sibl_reader("STS_FCOMADDR2")
                        txtCity_pri.Text = sibl_reader("STS_FCOMCITY")
                        'If Not ddlCity_pri.Items.FindByValue(sibl_reader("STS_FCOMCITY")) Is Nothing Then
                        '    ddlCity_pri.ClearSelection()
                        '    ddlCity_pri.Items.FindByValue(sibl_reader("STS_FCOMCITY")).Selected = True
                        'End If

                        If Not ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_FCOMCOUNTRY")) Is Nothing Then
                            ddlCountry_Pri.ClearSelection()
                            ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_FCOMCOUNTRY")).Selected = True
                        End If

                        hNumber = sibl_reader("STS_FRESPHONE").ToString.Replace(" ", "-").Split("-")
                        If hNumber.Length = 3 Then
                            txtHPhone_Country.Text = hNumber(0)
                            txtHPhone_Area.Text = hNumber(1)
                            txtHPhone.Text = hNumber(2)
                        ElseIf hNumber.Length = 2 Then
                            txtHPhone_Area.Text = hNumber(0)
                            txtHPhone.Text = hNumber(1)
                        ElseIf hNumber.Length = 1 Then
                            txtHPhone.Text = hNumber(0)
                        End If

                        oNumber = sibl_reader("STS_FOFFPHONE").ToString.Replace(" ", "-").Split("-")
                        If oNumber.Length = 3 Then
                            txtOPhone_Country.Text = oNumber(0)
                            txtOPhone_Area.Text = oNumber(1)
                            txtOPhone.Text = oNumber(2)
                        ElseIf oNumber.Length = 2 Then
                            txtOPhone_Area.Text = oNumber(0)
                            txtOPhone.Text = oNumber(1)
                        ElseIf oNumber.Length = 1 Then
                            txtOPhone.Text = oNumber(0)
                        End If


                        fNumber = sibl_reader("STS_FFAX").ToString.Replace(" ", "-").Split("-")
                        If fNumber.Length = 3 Then
                            txtFaxNo_country.Text = fNumber(0)
                            txtFaxNo_Area.Text = fNumber(1)
                            txtFaxNo.Text = fNumber(2)
                        ElseIf fNumber.Length = 2 Then
                            txtFaxNo_Area.Text = fNumber(0)
                            txtFaxNo.Text = fNumber(1)
                        ElseIf fNumber.Length = 1 Then
                            txtFaxNo.Text = fNumber(0)
                        End If
                        txtPoboxLocal.Text = sibl_reader("STS_FCOMPOBOX")
                        txtOccup.Text = sibl_reader("STS_FOCC")
                        If Not ddlCompany.Items.FindByValue(sibl_reader("STS_F_COMP_ID")) Is Nothing Then
                            ddlCompany.ClearSelection()
                            ddlCompany.Items.FindByValue(sibl_reader("STS_F_COMP_ID")).Selected = True
                        End If


                    ElseIf sibl_reader("STU_PRIMARYCONTACT") = "M" Then

                        rbPri_Mother.Checked = True
                        txtPri_Fname.Text = sibl_reader("STS_MFIRSTNAME")
                        txtPri_Mname.Text = sibl_reader("STS_MMIDNAME")
                        txtPri_Lname.Text = sibl_reader("STS_MLASTNAME")
                        If Not ddlPri_National1.Items.FindByValue(sibl_reader("STS_MNATIONALITY")) Is Nothing Then
                            ddlPri_National1.ClearSelection()
                            ddlPri_National1.Items.FindByValue(sibl_reader("STS_MNATIONALITY")).Selected = True
                        End If
                        If Not ddlPri_National2.Items.FindByValue(sibl_reader("STS_MNATIONALITY2")) Is Nothing Then
                            ddlPri_National2.ClearSelection()
                            ddlPri_National2.Items.FindByValue(sibl_reader("STS_MNATIONALITY2")).Selected = True
                        End If

                        mobNumber = sibl_reader("STS_MMOBILE").ToString.Split(" ")
                        If mobNumber.Length = 3 Then
                            txtM_Country.Text = mobNumber(0)
                            txtM_Area.Text = mobNumber(1)
                            txtMobile_Pri.Text = mobNumber(2)
                        ElseIf mobNumber.Length = 2 Then
                            txtM_Area.Text = mobNumber(0)
                            txtMobile_Pri.Text = mobNumber(1)
                        ElseIf mobNumber.Length = 1 Then
                            txtMobile_Pri.Text = mobNumber(0)
                        End If

                        txtPoBox_Pri.Text = sibl_reader("STS_MPRMPOBOX")
                        txtEmail_Pri.Text = sibl_reader("STS_MEMAIL").ToString.ToLower
                        txtAdd1_overSea.Text = sibl_reader("STS_MPRMADDR1")
                        txtAdd2_overSea.Text = sibl_reader("STS_MPRMADDR2")

                        If Not ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_MPRMCOUNTRY")) Is Nothing Then
                            ddlOverSeas_Add_Country.ClearSelection()
                            ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_MPRMCOUNTRY")).Selected = True
                        End If
                        txtStreet.Text = sibl_reader("STS_MCOMADDR1")
                        txtArea.Text = sibl_reader("STS_MCOMADDR2")
                        txtCity_pri.Text = Convert.ToString(sibl_reader("STS_MCOMCITY"))

                        'If Not ddlCity_pri.Items.FindByValue(sibl_reader("STS_MCOMCITY")) Is Nothing Then
                        '    ddlCity_pri.ClearSelection()
                        '    ddlCity_pri.Items.FindByValue(sibl_reader("STS_MCOMCITY")).Selected = True
                        'End If

                        If Not ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_MCOMCOUNTRY")) Is Nothing Then
                            ddlCountry_Pri.ClearSelection()
                            ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_MCOMCOUNTRY")).Selected = True
                        End If

                        hNumber = sibl_reader("STS_MRESPHONE").Replace(" ", "-").Split("-")
                        If hNumber.Length = 3 Then
                            txtHPhone_Country.Text = hNumber(0)
                            txtHPhone_Area.Text = hNumber(1)
                            txtHPhone.Text = hNumber(2)
                        ElseIf hNumber.Length = 2 Then
                            txtHPhone_Area.Text = hNumber(0)
                            txtHPhone.Text = hNumber(1)
                        ElseIf hNumber.Length = 1 Then
                            txtHPhone.Text = hNumber(0)
                        End If

                        oNumber = sibl_reader("STS_MOFFPHONE").Replace(" ", "-").Split("-")
                        If oNumber.Length = 3 Then
                            txtOPhone_Country.Text = oNumber(0)
                            txtOPhone_Area.Text = oNumber(1)
                            txtOPhone.Text = oNumber(2)
                        ElseIf oNumber.Length = 2 Then
                            txtOPhone_Area.Text = oNumber(0)
                            txtOPhone.Text = oNumber(1)
                        ElseIf oNumber.Length = 1 Then
                            txtOPhone.Text = oNumber(0)
                        End If



                        fNumber = sibl_reader("STS_MFAX").Replace(" ", "-").Split("-")
                        If fNumber.Length = 3 Then
                            txtFaxNo_country.Text = fNumber(0)
                            txtFaxNo_Area.Text = fNumber(1)
                            txtFaxNo.Text = fNumber(2)
                        ElseIf fNumber.Length = 2 Then
                            txtFaxNo_Area.Text = fNumber(0)
                            txtFaxNo.Text = fNumber(1)
                        ElseIf fNumber.Length = 1 Then
                            txtFaxNo.Text = fNumber(0)
                        End If

                        txtPoboxLocal.Text = sibl_reader("STS_MCOMPOBOX")
                        txtOccup.Text = sibl_reader("STS_MOCC")
                        If Not ddlCompany.Items.FindByValue(sibl_reader("STS_M_COMP_ID")) Is Nothing Then
                            ddlCompany.ClearSelection()
                            ddlCompany.Items.FindByValue(sibl_reader("STS_M_COMP_ID")).Selected = True
                        End If

                    ElseIf sibl_reader("STU_PRIMARYCONTACT") = "G" Then

                        rdPri_Guard.Checked = True
                        txtPri_Fname.Text = sibl_reader("STS_GFIRSTNAME")
                        txtPri_Mname.Text = sibl_reader("STS_GMIDNAME")
                        txtPri_Lname.Text = sibl_reader("STS_GLASTNAME")
                        If Not ddlPri_National1.Items.FindByValue(sibl_reader("STS_GNATIONALITY")) Is Nothing Then
                            ddlPri_National1.ClearSelection()
                            ddlPri_National1.Items.FindByValue(sibl_reader("STS_GNATIONALITY")).Selected = True
                        End If
                        If Not ddlPri_National2.Items.FindByValue(sibl_reader("STS_GNATIONALITY2")) Is Nothing Then
                            ddlPri_National2.ClearSelection()
                            ddlPri_National2.Items.FindByValue(sibl_reader("STS_GNATIONALITY2")).Selected = True
                        End If

                        mobNumber = sibl_reader("STS_GMOBILE").ToString.Split(" ")
                        If mobNumber.Length = 3 Then
                            txtM_Country.Text = mobNumber(0)
                            txtM_Area.Text = mobNumber(1)
                            txtMobile_Pri.Text = mobNumber(2)
                        ElseIf mobNumber.Length = 2 Then
                            txtM_Area.Text = mobNumber(0)
                            txtMobile_Pri.Text = mobNumber(1)
                        ElseIf mobNumber.Length = 1 Then
                            txtMobile_Pri.Text = mobNumber(0)
                        End If

                        txtPoBox_Pri.Text = sibl_reader("STS_GCOMPOBOX")
                        txtEmail_Pri.Text = sibl_reader("STS_GEMAIL").ToString.ToLower
                        txtAdd1_overSea.Text = sibl_reader("STS_GPRMADDR1")
                        txtAdd2_overSea.Text = sibl_reader("STS_GPRMADDR2")
                        txtOverSeas_Add_City.Text = sibl_reader("STS_FPRMCITY")

                        If Not ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_GPRMCOUNTRY")) Is Nothing Then
                            ddlOverSeas_Add_Country.ClearSelection()
                            ddlOverSeas_Add_Country.Items.FindByValue(sibl_reader("STS_GPRMCOUNTRY")).Selected = True
                        End If

                        txtStreet.Text = sibl_reader("STS_GCOMADDR1")
                        txtArea.Text = sibl_reader("STS_GCOMADDR2")
                        txtCity_pri.Text = sibl_reader("STS_GCOMCITY")
                        'If Not ddlCity_pri.Items.FindByValue(sibl_reader("STS_GCOMCITY")) Is Nothing Then
                        '    ddlCity_pri.ClearSelection()
                        '    ddlCity_pri.Items.FindByValue(sibl_reader("STS_GCOMCITY")).Selected = True
                        'End If

                        If Not ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_GCOMCOUNTRY")) Is Nothing Then
                            ddlCountry_Pri.ClearSelection()
                            ddlCountry_Pri.Items.FindByValue(sibl_reader("STS_GCOMCOUNTRY")).Selected = True
                        End If

                        hNumber = sibl_reader("STS_GRESPHONE").ToString.Split(" ")
                        If hNumber.Length = 3 Then
                            txtHPhone_Country.Text = hNumber(0)
                            txtHPhone_Area.Text = hNumber(1)
                            txtHPhone.Text = hNumber(2)
                        ElseIf hNumber.Length = 2 Then
                            txtHPhone_Area.Text = hNumber(0)
                            txtHPhone.Text = hNumber(1)
                        ElseIf hNumber.Length = 1 Then
                            txtHPhone.Text = hNumber(0)
                        End If

                        oNumber = sibl_reader("STS_GOFFPHONE").ToString.Split(" ")
                        If oNumber.Length = 3 Then
                            txtOPhone_Country.Text = oNumber(0)
                            txtOPhone_Area.Text = oNumber(1)
                            txtOPhone.Text = oNumber(2)
                        ElseIf oNumber.Length = 2 Then
                            txtOPhone_Area.Text = oNumber(0)
                            txtOPhone.Text = oNumber(1)
                        ElseIf oNumber.Length = 1 Then
                            txtOPhone.Text = oNumber(0)
                        End If

                        fNumber = sibl_reader("STS_GFAX").ToString.Split(" ")
                        If fNumber.Length = 3 Then
                            txtFaxNo_country.Text = fNumber(0)
                            txtFaxNo_Area.Text = fNumber(1)
                            txtFaxNo.Text = fNumber(2)
                        ElseIf fNumber.Length = 2 Then
                            txtFaxNo_Area.Text = fNumber(0)
                            txtFaxNo.Text = fNumber(1)
                        ElseIf fNumber.Length = 1 Then
                            txtFaxNo.Text = fNumber(0)
                        End If

                        txtPoboxLocal.Text = sibl_reader("STS_GCOMPOBOX")
                        txtOccup.Text = sibl_reader("STS_GOCC")
                        If Not ddlCompany.Items.FindByValue(sibl_reader("STS_G_COMP_ID")) Is Nothing Then
                            ddlCompany.ClearSelection()
                            ddlCompany.Items.FindByValue(sibl_reader("STS_G_COMP_ID")).Selected = True
                        End If

                    End If
                End While

            End Using
        Catch ex As Exception

        End Try
    End Sub



    Function ValidStaff_IDCheck() As Boolean
        Dim BSU_ID As String
        Dim Flag As Boolean
        If ddlStaff_BSU.SelectedIndex = -1 Then
            BSU_ID = ""
        Else
            BSU_ID = ddlStaff_BSU.SelectedItem.Value

        End If


        Using staffIDdatareader As SqlDataReader = AccessStudentClass.CheckStaffID_BSU(BSU_ID)
            While staffIDdatareader.Read
                Flag = Convert.ToBoolean(staffIDdatareader("BSU_bCheck_StaffID"))
            End While
        End Using
        ValidStaff_IDCheck = Flag
    End Function
    'Function Check_KG1() As Boolean
    '    Dim Check_Flag As Boolean = True
    '    Dim ErrorString As String
    '    Dim DateValid_L_Date As Date
    '    ErrorString = "<UL>"
    '    If (ddlGrade.SelectedItem.Value = "KG1" Or ddlGrade.SelectedItem.Value = "PK") Then

    '        If ddlPreSchool_Nursery.SelectedItem.Text = "--" Then
    '            ErrorString = ErrorString & "<LI>Select current school name" & "</UL>"
    '            Check_Flag = False
    '        End If
    '    Else

    '        If ddlPre_Curriculum.SelectedItem.Text = "--" Then
    '            ErrorString = ErrorString & "<LI>Curriculum needs to be selected"
    '            Check_Flag = False
    '        End If
    '        'If txtLast_Att.Text.Trim = "" Then
    '        '    ErrorString = ErrorString & "<LI>Last Attendance Date required"

    '        '    Check_Flag = False
    '        'Else
    '        If txtLast_Att.Text.Trim <> "" Then
    '            If IsDate(txtLast_Att.Text) = False Then
    '                ErrorString = ErrorString & "<LI>Last Attendance Date is not a valid date"

    '                Check_Flag = False
    '            End If
    '        ElseIf txtLast_Att.Text.Trim <> "" And IsDate(txtLast_Att.Text) = True Then
    '            'code modified 4 date
    '            Dim strfDate As String = txtLast_Att.Text.Trim
    '            Dim str_err As String = DateFunctions.checkdate(strfDate)
    '            If str_err <> "" Then
    '                Check_Flag = False
    '                ErrorString = ErrorString & "<LI>Last Attendance Date is not a valid date"
    '            Else
    '                txtLast_Att.Text = strfDate
    '                DateValid_L_Date = Date.ParseExact(txtLast_Att.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
    '                If IsDate(DateValid_L_Date) = False Then
    '                    ErrorString = ErrorString & "<LI>Last Attendance Date is not a valid date"
    '                    Check_Flag = False
    '                End If
    '            End If
    '        ElseIf txtLast_Att.Text.Trim <> "" And IsDate(txtLast_Att.Text) = False Then
    '            ErrorString = ErrorString & "<LI>Last Attendance Date format is invalid"

    '            Check_Flag = False
    '        End If
    '    End If
    '    lblError.Text = ErrorString
    '    Check_KG1 = Check_Flag
    'End Function
    Sub OpenStaff_BSU_Populate(ByVal BSU_IDs As String)
        'Dim Bsu As String = Replace(BSU_IDs, "|", ",")
        'Bsu = Bsu.TrimEnd(",")
        'Bsu = Bsu.TrimStart(",")
        Dim ds1 As DataSet = AccessStudentClass.GetOpen_BSU_Staff(BSU_IDs)
        'BSU_ID,BSU_NAME

        If ds1.Tables(0).Rows.Count > 0 Then

            ddlStaff_BSU.Items.Clear()
            ddlStaff_BSU.DataSource = ds1.Tables(0)
            ddlStaff_BSU.DataTextField = "BSU_NAME"
            ddlStaff_BSU.DataValueField = "BSU_ID"
            ddlStaff_BSU.DataBind()
        End If
    End Sub
    Sub OpenSib_BSU_Populate(ByVal BSU_IDs As String)

        Dim ds1 As DataSet = AccessStudentClass.GetOpen_BSU_Sib(BSU_IDs)
        'BSU_ID,BSU_NAME

        If ds1.Tables(0).Rows.Count > 0 Then
            ddlSib_BSU.Items.Clear()
            ddlSib_BSU.DataSource = ds1.Tables(0)
            ddlSib_BSU.DataTextField = "BSU_NAME"
            ddlSib_BSU.DataValueField = "BSU_ID"
            ddlSib_BSU.DataBind()
        End If

    End Sub
    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Length < 1 Then
            Return False
        Else
            Dim strRegex As String = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" '"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            Dim re As New Regex(strRegex)
            If re.IsMatch(inputEmail) Then
                Return (True)
            Else
                Return (False)
            End If
        End If
    End Function
    Protected Sub ddlGEMSSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGEMSSchool.SelectedIndexChanged
        Call GetMain_Loc()
        ddlMainLocation_SelectedIndexChanged(ddlMainLocation, Nothing)
        'Second Curriculum 
        Call BSU_Enq_validation()
        Call CURRICULUM_BSU_4_Student()
        ' Call GetTerm_4ACD()
        '  Call GetSchoolButton_Enable()
        Call GetReligion_info()
        'new code added
        If Not ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedItem.Value) Is Nothing Then
            ddlSib_BSU.ClearSelection()
            ddlSib_BSU.Items.FindByValue(ddlGEMSSchool.SelectedItem.Value).Selected = True
            ddlSib_BSU.Enabled = False
        Else
            ddlSib_BSU.Enabled = False
        End If
        If Not ddlGemsGr.Items.FindByValue(ddlGEMSSchool.SelectedValue) Is Nothing Then
            ddlGemsGr.ClearSelection()
            ddlGemsGr.Items.FindByValue(ddlGEMSSchool.SelectedValue).Selected = True
        End If

        ltAboutUs.Text = StrConv(ddlGEMSSchool.SelectedItem.Text, VbStrConv.ProperCase) & "? [please tick]"
    End Sub
    Protected Sub ddlCurri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurri.SelectedIndexChanged
        'third Academic year
        Call GetAcademicYear_Bsu()
        ' Call GetTerm_4ACD()
        'Call GetSchoolButton_Enable()
        'new code added
    End Sub
    Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged
        'sixth stream is called
        Call GetStudent_Stream()
        '  Call GetSchoolButton_Enable()
    End Sub
    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStream.SelectedIndexChanged
        Call GetSchoolButton_Enable()
    End Sub
    Protected Sub ddlMainLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMainLocation.SelectedIndexChanged
        Call GetSub_Main_Loc()
        Call GetSub_PickUp_point()
    End Sub
    Protected Sub ddlSubLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubLocation.SelectedIndexChanged
        Call GetSub_PickUp_point()

        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
        '               "<script language=javascript>fill_Sibls();</script>")
        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINTRANS", _
        '                            "<script language=javascript>fill_Tran();</script>")


    End Sub
    Sub Populate_YearList()
        'Year list can be extended
        Dim intYear As Integer
        For intYear = DateTime.Now.Year - 40 To DateTime.Now.Year + 5
            ddlExYear.Items.Add(intYear)
        Next
        ddlExYear.Items.FindByValue(DateTime.Now.Year).Selected = True
    End Sub
    Protected Sub rbRelocAgent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbRelocAgent.CheckedChanged
        If rbRelocAgent.Checked = True Then
            tbReloc.Visible = True
        End If

    End Sub
    Protected Sub rbParentF_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbParentF.CheckedChanged
        If rbParentF.Checked = True Then
            tbReloc.Visible = False
        End If

    End Sub
#Region "Radio Button events"
    Protected Sub rdFather_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdPri_Father.CheckedChanged
        ddlCont_Status.SelectedIndex = 0
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                                 "<script language=javascript>fill_Sib();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                               "<script language=javascript>fill_Staff();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                               "<script language=javascript>fill_Stud();</script>")
    End Sub
    Protected Sub rbPri_Mother_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPri_Mother.CheckedChanged
        ddlCont_Status.SelectedIndex = 1
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                                 "<script language=javascript>fill_Sib();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                               "<script language=javascript>fill_Staff();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                               "<script language=javascript>fill_Stud();</script>")
    End Sub
    Protected Sub rdPri_Guard_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdPri_Guard.CheckedChanged
        ddlCont_Status.SelectedIndex = 0
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                                 "<script language=javascript>fill_Sib();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                               "<script language=javascript>fill_Staff();</script>")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                               "<script language=javascript>fill_Stud();</script>")
    End Sub
    Protected Sub rdGemsGr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdGemsGr.CheckedChanged
        ddlGemsGr.Visible = True
        'txtPre_School.Text = ""
        'txtPre_City.Text = ""
        txtPre_School.Visible = False
        '  rfv_Pre_school.Visible = False
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                          "<script language=javascript>Sel_Grade_KG1();</script>")
        'ddlGemsGr_SelectedIndexChanged(ddlGemsGr, Nothing)
    End Sub
    Protected Sub rdOther_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdOther.CheckedChanged
        '  ddlCont_Status.SelectedIndex = 0
        ddlGemsGr.Visible = False
        'txtPre_School.Text = ""
        ' txtPre_City.Text = ""
        txtPre_School.Visible = True
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                          "<script language=javascript>Sel_Grade_KG1();</script>")
    End Sub
#End Region
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        Call enable_panel()
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Sub enable_panel()
        pnlCurrent.Enabled = True
        pnlMain.Enabled = True
        pnlHeader.Enabled = True
        pnlOther.Enabled = True
        pnlParent.Enabled = True
        pnlPassPort.Enabled = True
        pnlTrans.Enabled = True

    End Sub
    Sub disable_panel()
        pnlCurrent.Enabled = False
        pnlMain.Enabled = False
        pnlHeader.Enabled = False
        pnlOther.Enabled = False
        pnlParent.Enabled = False
        pnlPassPort.Enabled = False
        pnlTrans.Enabled = False

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "add" Then
                'disable_panel()
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub txtSib_ID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        validate_Sib_ID()
    End Sub
    Sub validate_Sib_ID()
        Dim CommStr As String = String.Empty
        CommStr = "<UL>"
        If chkSibling.Checked = True Then
            If Trim(txtSib_Name.Text) = "" Then
                CommStr = CommStr & "<LI>Sibling name cannot be left empty"
            End If
            If Trim(txtSib_ID.Text) = "" Then
                CommStr = CommStr & "<LI>Sibling Fee ID required"
            Else
                Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)
                    If ValidSibl_reader.HasRows = False Then
                        CommStr = CommStr & "<LI>Sibling ID is not valid for the selected School"
                    End If
                End Using
            End If
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                                            "<script language=javascript>fill_Sib();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                   "<script language=javascript>fill_Staff();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                   "<script language=javascript>fill_Stud();</script>")
            lblError.Text = CommStr & "</UL>"

        End If

    End Sub


    Function Final_validation() As Boolean
        Dim Final_error As Boolean = True
        Dim DateValid_DOB As Date
        Dim DateValid_PIss_D As Date
        Dim DateValid_PExp_D As Date
        'Dim DateValid_VIss_D As Date
        'Dim DateValid_VExp_D As Date

        Dim commString As String = "The following fields needs to be validated: <UL>"

        'Select Schools validation
        Dim ChkCounter As Integer = 0
        If ddlAca_Year.SelectedIndex = -1 Or ddlGrade.SelectedIndex = -1 Or ddlCurri.SelectedIndex = -1 Or ddlShift.SelectedIndex = -1 Then    'And ddlTerm.Items.Count > 0 
            commString = commString & "<LI>Registration is CLOSED for this Search query"
            Final_error = False
        End If
        If txtDOJ.Text.Trim <> "" Then
            Dim strfDate As String = txtDOJ.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                Final_error = False
                commString = commString & "<LI>Invalid tentative date of join"
            Else
                txtDOJ.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtDOJ.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                'check for the leap year date
                If Not IsDate(dateTime1) Then
                    Final_error = False
                    commString = commString & "<LI>Invalid tentative date of join"
                Else
                    Call CheckTermDate()
                    Dim TRM_SDate As Date = ViewState("TRM_STARTDT")
                    Dim TRM_EDate As Date = ViewState("TRM_ENDDT")
                    Dim TENT_date As Date = txtDOJ.Text
                    If Not (TENT_date >= TRM_SDate And TENT_date <= TRM_EDate) Then
                        commString = commString & "<LI>Tentative date of join  must be with in the Term Date"
                        Final_error = False
                    End If
                End If
            End If
        End If

        If Trim(txtFname.Text) <> "" Then
            If Not Regex.Match(txtFname.Text, "^(([a-zA-Z]{0,100})([a-zA-Z\s-'\\\/]*))[a-zA-Z]+$").Success Then
                commString = commString & "<LI>Invalid Student first"
                Final_error = False
            End If
        End If
        If Trim(txtMname.Text) <> "" Then
            If Len(txtMname.Text) = 1 Then
                If Not Regex.Match(txtMname.Text, "^([a-zA-Z])").Success Then
                    commString = commString & "<LI>Invalid Student middle name"
                    Final_error = False
                End If
            Else
                If Not Regex.Match(txtMname.Text, "^(([a-zA-Z]{0,100})([a-zA-Z\s-'\\\/]*))[a-zA-Z]+$").Success Then
                    commString = commString & "<LI>Invalid Student middle name"
                    Final_error = False
                End If
            End If
        End If


        If Trim(txtLname.Text) <> "" Then
            If Len(txtLname.Text) = 1 Then
                If Not Regex.Match(txtLname.Text, "^([a-zA-Z])").Success Then
                    commString = commString & "<LI>Invalid Student last name"
                    Final_error = False
                End If
            Else
                If Not Regex.Match(txtLname.Text, "^(([a-zA-Z]{0,100})([a-zA-Z\s-'\\\/]*))[a-zA-Z]+$").Success Then
                    commString = commString & "<LI>Invalid Student last name"
                    Final_error = False
                End If
            End If
        End If


        If chkSibling.Checked = True Then
            If Trim(txtSib_Name.Text) = "" Then
                commString = commString & "<LI>Sibling name cannot be left empty"
            End If
            If Trim(txtSib_ID.Text) = "" Then
                commString = commString & "<LI>Sibling Fee ID required"
            Else
                Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSib_ID.Text, ddlSib_BSU.SelectedItem.Value)
                    If ValidSibl_reader.HasRows = False Then
                        commString = commString & "<LI>Sibling ID is not valid for the selected School"
                    End If
                End Using
            End If
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
            '                                                "<script language=javascript>fill_Sib();</script>")
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
            '                       "<script language=javascript>fill_Staff();</script>")
            'Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
            '                       "<script language=javascript>fill_Stud();</script>")
        End If



        Dim Enq_hash As New Hashtable
        If Not Session("BSU_Enq_Valid") Is Nothing Then
            Enq_hash = Session("BSU_Enq_Valid")
        End If



        If Enq_hash.ContainsKey("SFN") = False Then
            If txtFname.Text.Trim = "" Then
                commString = commString & "<LI>Student name cannot be left empty"

                Final_error = False
            End If
        End If


        If Enq_hash.ContainsKey("SRE") = False Then

            If ddlReligion.SelectedItem.Text = "" Then
                commString = commString & "<LI>Please Select Religion"
                Final_error = False
            End If
        End If
        If Enq_hash.ContainsKey("COB") = False Then
            If ddlCountry.SelectedItem.Text = "" Then
                commString = commString & "<LI>Please select Country of Birth"
                Final_error = False
            End If
        End If

        If Enq_hash.ContainsKey("NAT") = False Then

            If ddlNational.SelectedItem.Text = "" Then
                commString = commString & "<LI>Please select Student Nationality"
                Final_error = False
            End If
        End If


        If Enq_hash.ContainsKey("DOB") = False Then
            If txtDob.Text.Trim = "" Then
                commString = commString & "<LI>Student date of birth required"
                Final_error = False

            ElseIf txtDob.Text.Trim <> "" Then
                'code modified 4 date
                Dim strfDate As String = txtDob.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    Final_error = False
                    commString = commString & "<li>Invalid Date of birth"
                Else
                    txtDob.Text = strfDate
                    DateValid_DOB = Date.ParseExact(txtDob.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    If IsDate(DateValid_DOB) = False Then
                        commString = commString & "<LI>Invalid Date of birth"

                        Final_error = False
                    End If
                End If

            End If
        End If

        If Enq_hash.ContainsKey("POB") = False Then
            If txtPob.Text.Trim = "" Then
                commString = commString & "<LI>Student place of birth required"
                Final_error = False

            End If
        End If

        If txtStud_Contact_No.Text.Trim = "" Then
            commString = commString & "<LI>Student contact cannot be left empty"
            Final_error = False
        End If

        ' End of select validation of Application Info


        'Validation  for passport/visa  Details

        'If txtPassport.Text.Trim = "" Then
        '    commString = commString & "<LI>Passport No. required"
        '    Final_error = False
        'End If

        'If txtPassportIssue.Text.Trim = "" Then
        '    commString = commString & "<LI>Passport issue place required"
        '    'ViewState("Valid_Pass") = "-1"
        '    ViewState("Valid_Appl") = "-1"
        '    Final_error = False
        'End If

        If txtPassport.Text.Trim <> "" Then
            If txtPassIss_date.Text.Trim <> "" Then
                If IsDate(txtPassIss_date.Text) = False Then
                    commString = commString & "<LI>Passport Issue Date is not a vaild date"
                    Final_error = False
                ElseIf txtPassIss_date.Text.Trim <> "" Then
                    'code modified 4 date
                    Dim strfDate As String = txtPassIss_date.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        Final_error = False
                        commString = commString & "<li>Passport Issue Date format is Invalid"
                    Else
                        txtPassIss_date.Text = strfDate
                        DateValid_PIss_D = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        If IsDate(DateValid_PIss_D) = False Then
                            commString = commString & "<LI>Passport Issue Date Invalid"
                            Final_error = False
                        End If
                    End If

                ElseIf txtPassIss_date.Text.Trim <> "" And IsDate(txtPassIss_date.Text) = False Then
                    commString = commString & "<LI>Passport Issue Date format is Invalid"
                    Final_error = False
                ElseIf IsDate(txtPassIss_date.Text) = True Then
                    If IsDate(txtDob.Text) = True Then
                        'code modified 4 date
                        Dim strfDate As String = txtPassIss_date.Text.Trim
                        Dim str_err As String = DateFunctions.checkdate(strfDate)
                        If str_err <> "" Then
                            Final_error = False
                            commString = commString & "<li>Passport Issue Date format is Invalid"
                        Else
                            txtPassIss_date.Text = strfDate
                            Dim strfDate1 As String = txtDob.Text.Trim
                            Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                            If str_err1 <> "" Then
                            Else
                                txtDob.Text = strfDate1
                                DateValid_DOB = Date.ParseExact(txtDob.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                                DateValid_PIss_D = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                                If DateTime.Compare(DateValid_DOB, DateValid_PIss_D) > 0 Then
                                    commString = commString & "<LI>Passport Issue Date entered is not a valid date and must be greater than Date of Birth"
                                    Final_error = False
                                End If
                            End If
                        End If
                    End If

                End If

            End If


            ' ========================VALIDATE THE PERM ADDRESSS ===================
            'If (txtAdd1_overSea.Text.Trim = "") And (txtAdd2_overSea.Text.Trim = "") Then
            '    commString = commString & "<LI>Overseas address cannot be left empty"
            '    Final_error = False
            'End If


            'If txtOverSeas_Add_City.Text.Trim = "" Then
            '    commString = commString & "<LI>Overseas city cannot be left empty"
            '    Final_error = False
            'End If



            'If ddlOverSeas_Add_Country.SelectedItem.Text = "" Then
            '    commString = commString & "<LI>Overseas country cannot be left empty"
            '    Final_error = False
            'End If


            'If txtPhone_Oversea_Country.Text.Trim = "" Or txtPhone_Oversea_Area.Text.Trim = "" Or txtPhone_Oversea_No.Text.Trim = "" Then
            '    commString = commString & "<LI>Overseas Contact number required"
            '    Final_error = False

            'ElseIf Not Regex.Match(txtPhone_Oversea_Country.Text, "^[0-9]{1,3}$").Success Then
            '    commString = commString & "<LI>Enter valid Overseas country code"
            '    Final_error = False
            'ElseIf Not Regex.Match(txtPhone_Oversea_Area.Text, "^[0-9]{1,4}$").Success Then
            '    commString = commString & "<LI>Enter valid Overseas phone area code"
            '    Final_error = False
            'ElseIf Not Regex.Match(txtPhone_Oversea_No.Text, "^[0-9]{1,10}$").Success Then
            '    commString = commString & "<LI>Enter valid Overseas phone no."
            '    Final_error = False
            'End If


            'If txtPoBox_Pri.Text.Trim = "" Then
            '    commString = commString & "<LI>Overseas PO Box/Zip Code cannot be left empty"
            '    Final_error = False
            'End If









            '==============================================================================













            'validating passport expire  date

            'DateValid_PExp_D = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            If txtPassExp_Date.Text.Trim <> "" Then
                If IsDate(txtPassExp_Date.Text) = False Then
                    commString = commString & "<LI>Passport Expiry Date is not a vaild date"
                    Final_error = False
                ElseIf txtPassExp_Date.Text.Trim <> "" And IsDate(txtPassExp_Date.Text) = True Then
                    Dim strfDate As String = txtPassExp_Date.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        Final_error = False
                        commString = commString & "<li>Passport Expiry Date format is Invalid"
                    Else
                        txtPassExp_Date.Text = strfDate
                        DateValid_PExp_D = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        If IsDate(DateValid_PExp_D) = False Then
                            commString = commString & "<LI>Date of birth format is invalid"
                            Final_error = False
                        End If
                    End If
                ElseIf IsDate(txtPassExp_Date.Text) = True Then
                    If IsDate(txtPassIss_date.Text) = True Then
                        Dim strfDate As String = txtPassExp_Date.Text.Trim
                        Dim str_err As String = DateFunctions.checkdate(strfDate)
                        If str_err <> "" Then
                            Final_error = False
                            commString = commString & "<li>Passport Expiry Date format is Invalid"
                        Else
                            txtPassExp_Date.Text = strfDate
                            Dim strfDate1 As String = txtPassIss_date.Text.Trim
                            Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                            If str_err1 <> "" Then
                            Else
                                txtPassIss_date.Text = strfDate1
                                DateValid_PExp_D = Date.ParseExact(txtPassExp_Date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                                DateValid_PIss_D = Date.ParseExact(txtPassIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                                If DateTime.Compare(DateValid_PIss_D, DateValid_PExp_D) > 0 Then
                                    commString = commString & "<LI>Passport Expiry  Date entered must be greater than Passport Issue Date"
                                    Final_error = False
                                End If
                            End If
                        End If
                    End If
                End If
            End If

        End If
        If txtVisaIss_date.Text.Trim <> "" Then
            If IsDate(txtVisaIss_date.Text) = False Then
                commString = commString & "<LI>Visa Issue Date is not a vaild date"
                Final_error = False
            Else
                Dim strfDate As String = txtVisaIss_date.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    Final_error = False
                    commString = commString & "<li>Visa Issue Date format is Invalid"
                End If
            End If
        End If

        If txtVisaExp_date.Text.Trim <> "" Then
            ' DateValid_VExp_D = Date.ParseExact(txtVisaExp_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            If IsDate(txtVisaExp_date.Text) = False Then
                commString = commString & "<LI>Visa Expiry Date is not a vaild date"
                Final_error = False
            ElseIf IsDate(txtVisaExp_date.Text) = True Then
                If IsDate(txtVisaIss_date.Text) = True Then
                    Dim strfDate As String = txtVisaExp_date.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        Final_error = False
                        commString = commString & "<li>Visa Expiry Date format is Invalid"
                    Else
                        txtVisaExp_date.Text = strfDate
                        Dim DateTime2 As Date
                        Dim dateTime1 As Date
                        Dim strfDate1 As String = txtVisaIss_date.Text.Trim
                        Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                        If str_err1 <> "" Then
                        Else
                            txtVisaIss_date.Text = strfDate1
                            DateTime2 = Date.ParseExact(txtVisaExp_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            dateTime1 = Date.ParseExact(txtVisaIss_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                            'check for the leap year date
                            If IsDate(DateTime2) Then
                                If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                    Final_error = False
                                    commString = commString & "<li>Visa Expiry Date entered  must be greater than Visa Issue Date"
                                End If
                            Else
                                Final_error = False
                                commString = commString & "<li>Invalid Visa Expiry Date"
                            End If
                        End If
                    End If


                End If
            End If
        End If

        ' End for passport/visa  Details



        'Validation  for primary School Details




        'If txtPri_Fname.Text.Trim = "" Then
        '    commString = commString & "<LI>Primary contact name cannot be left empty"
        '    ViewState("Valid_Prim_cont") = "-1"
        '    Final_error = False

        'End If

        If txtPri_Fname.Text <> "" Then
            If Not Regex.Match(txtPri_Fname.Text, "^(([a-zA-Z]{0,100})([a-zA-Z\s-'\\\/]*))[a-zA-Z]+$").Success Then
                commString = commString & "<LI>Invalid Primary contact first name"

                Final_error = False
            End If
        End If

        If txtPri_Mname.Text <> "" Then
            If Not Regex.Match(txtPri_Mname.Text, "^(([a-zA-Z]{0,100})([a-zA-Z\s-'\\\/]*))[a-zA-Z]+$").Success Then
                commString = commString & "<LI>Invalid Primary contact middle name "
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If
        If txtPri_Lname.Text <> "" Then
            If Not Regex.Match(txtPri_Lname.Text, "^(([a-zA-Z]{0,100})([a-zA-Z\s-'\\\/]*))[a-zA-Z]+$").Success Then
                commString = commString & "<LI>Invalid Primary contact last name"
                ViewState("Valid_Prim_cont") = "-1"
                Final_error = False
            End If
        End If

        '^[0-9]{1,3}$


        If ddlPref_contact.SelectedItem.Text = "Home Phone" Then
            If txtHPhone_Country.Text.Trim = "" Or txtHPhone_Area.Text.Trim = "" Or txtHPhone.Text.Trim = "" Then
                commString = commString & "<LI>Home Contact number required"
                Final_error = False
            ElseIf Not Regex.Match(txtHPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Enter valid home phone country code"
                Final_error = False
            ElseIf Not Regex.Match(txtHPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Enter valid home phone area code"
                Final_error = False
            ElseIf Not Regex.Match(txtHPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Enter valid home phone no."
                Final_error = False
            End If

        ElseIf ddlPref_contact.SelectedItem.Text = "Office Phone" Then
            If txtOPhone_Country.Text.Trim = "" Or txtOPhone_Area.Text.Trim = "" Or txtOPhone.Text.Trim = "" Then
                commString = commString & "<LI>Office Contact number required"
                Final_error = False

            ElseIf Not Regex.Match(txtOPhone_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Enter valid office phone country code"
                Final_error = False
            ElseIf Not Regex.Match(txtOPhone_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Enter valid office phone area code"
                Final_error = False
            ElseIf Not Regex.Match(txtOPhone.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Enter valid office phone no."
                Final_error = False
            End If

        ElseIf ddlPref_contact.SelectedItem.Text = "Mobile" Then
            If txtM_Country.Text.Trim = "" Or txtM_Area.Text.Trim = "" Or txtMobile_Pri.Text.Trim = "" Then
                commString = commString & "<LI>Mobile Contact number required"
                Final_error = False

            ElseIf Not Regex.Match(txtM_Country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Enter valid mobile country code"
                Final_error = False
            ElseIf Not Regex.Match(txtM_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Enter valid mobile phone area code"
                Final_error = False
            ElseIf Not Regex.Match(txtMobile_Pri.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Enter valid mobile phone no."
                Final_error = False
            End If
        End If

        'If Enq_hash.ContainsKey("PCEID") = False Then
        If txtEmail_Pri.Text.Trim = "" Then
            commString = commString & "<LI>E-mail ID required"
            Final_error = False

        ElseIf isEmail(txtEmail_Pri.Text) = False Then
            commString = commString & "<LI>E-mail entered is not valid"
            Final_error = False
        End If


        'End If


        If chkStaff_GEMS.Checked = True Then
            If Trim(txtStaff_Name.Text) = "" Then
                commString = commString & "<LI>Staff name required"
                Final_error = False
            End If

            If ValidStaff_IDCheck() = True Then
                If Trim(txtStaffID.Text) = "" Then
                    commString = commString & "<LI>Staff ID required"
                    Final_error = False
                Else
                    Using ValidStaff_reader As SqlDataReader = AccessStudentClass.getValid_staff(txtStaffID.Text, ddlStaff_BSU.SelectedItem.Value)
                        If ValidStaff_reader.HasRows = False Then
                            commString = commString & "<LI>Staff ID is not valid  for the selected Business Unit"
                            Final_error = False
                        End If
                    End Using
                End If

            End If
        End If

        'If chkExStud_Gems.Checked Then
        '    If Trim(txtExStudName_Gems.Text) = "" Then
        '        commString = commString & "<LI>Ex-student detail cannot left empty"

        '        Final_error = False
        '    End If
        'End If

        If ddlGrade.SelectedIndex <> -1 Then
            If (ddlGrade.SelectedItem.Value = "KG1" Or ddlGrade.SelectedItem.Value = "PK") Then
                txtLast_Att.Text = ""
                'If ddlPreSchool_Nursery.SelectedItem.Text = "--" Then
                '    commString = commString & "<LI>Kindly select previous school name"
                '    Final_error = False
                'End If
            Else
                'If rdOther.Checked = True Then
                '    If txtPre_School.Text.Trim = "" Then
                '        commString = commString & "<LI>Kindly enter previous school name"
                '        Final_error = False
                '    End If
                'End If

                'If txtPre_City.Text.Trim = "" Then
                '    commString = commString & "<LI>Previous city name cannot be left empty"
                '    Final_error = False
                'End If
                'If ddlPre_Curriculum.SelectedIndex <> -1 Then
                '    If ddlPre_Curriculum.SelectedItem.Text = "--" Then
                '        commString = commString & "<LI>Kindly select the curriculum"
                '        Final_error = False
                '    End If
                'End If

                'code modified by lijo on 26may08 post back disabled


                If txtLast_Att.Text.Trim <> "" Then
                    If IsDate(txtLast_Att.Text) = False Then
                        commString = commString & "<LI>Last Attendance Date is not a vaild date"
                        Final_error = False
                    End If

                ElseIf txtLast_Att.Text.Trim <> "" And IsDate(txtLast_Att.Text) = True Then
                    'code modified 4 date
                    Dim strfDate As String = txtLast_Att.Text.Trim
                    Dim str_err As String = DateFunctions.checkdate(strfDate)
                    If str_err <> "" Then
                        Final_error = False
                        commString = commString & "<LI>Last Attendance Date is not a vaild date"
                    Else
                        txtLast_Att.Text = strfDate
                        DateValid_DOB = Date.ParseExact(txtLast_Att.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        If IsDate(DateValid_DOB) = False Then
                            commString = commString & "<LI>Last Attendance Date is not a vaild date"
                            Final_error = False
                        End If
                    End If
                ElseIf txtLast_Att.Text.Trim <> "" And IsDate(txtLast_Att.Text) = False Then
                    commString = commString & "<LI>Last Attendance Date format is invalid"
                    Final_error = False
                End If

            End If

        End If

        If rbRelocAgent.Checked = True Then
            If Enq_hash.ContainsKey("AGN") = False Then
                If txtAgentName.Text.Trim = "" Then
                    commString = commString & "<LI>Agent contact name cannot be left empty"
                    Final_error = False
                End If
            End If
        End If

        If rbRelocAgent.Checked = True Then
            If txtMAgent_country.Text.Trim = "" Or txtMAgent_Area.Text.Trim = "" Or txtMAgent_No.Text.Trim = "" Then
                commString = commString & "<LI>Agent Mobile Contact number required"
                Final_error = False

            ElseIf Not Regex.Match(txtMAgent_country.Text, "^[0-9]{1,3}$").Success Then
                commString = commString & "<LI>Enter valid Agent mobile country code"
                Final_error = False
            ElseIf Not Regex.Match(txtMAgent_Area.Text, "^[0-9]{1,4}$").Success Then
                commString = commString & "<LI>Enter valid Agent mobile phone area code"
                Final_error = False
            ElseIf Not Regex.Match(txtMAgent_No.Text, "^[0-9]{1,10}$").Success Then
                commString = commString & "<LI>Enter valid Agent mobile phone no."

                Final_error = False
            End If
        End If

        If chkRef.Checked Then
            If RefCodeIsvalid() = False Then
                commString = commString & "<LI>Please verify the Ref. Code and email you have entered."
                Final_error = False
            End If
        End If

        If Final_error Then
            Final_validation = Final_error
        Else

            lblError.Text = commString & "</UL>"
            Final_validation = Final_error
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                                            "<script language=javascript>fill_Sib();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                   "<script language=javascript>fill_Staff();</script>")
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "MAINCC", _
                                   "<script language=javascript>fill_Stud();</script>")
        End If


    End Function




    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("SIB_APPLICANT") = ""
        Call clearAll()
        mpe.Hide()

        btnAdd.Enabled = True
        btnSave.Visible = True
        btnCancel.Visible = True
        mnuMaster.Enabled = True
        enable_panel()
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Session("SIB_APPLICANT") = Session("TEMP_ApplNo")
        lblText.Text = "Would you like to set as sibiling?"
        Call ClearApplic()
        btnNo2.Visible = True

        btnYes2.Visible = True
        btnNo.Visible = False

        btnYes.Visible = False


        mpe.Show()
        btnAdd.Enabled = True
        btnSave.Visible = True
        btnCancel.Visible = True
        mnuMaster.Enabled = True
        enable_panel()

    End Sub

    Protected Sub btnYes2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("SIB_APPLICANT") = Session("TEMP_ApplNo")
        mpe.Hide()
        lblText.Text = "Would like to continue with the same Primary Contact info for the next applicant? "
        btnNo2.Visible = False

        btnYes2.Visible = False
        btnNo.Visible = True

        btnYes.Visible = True

    End Sub

    Protected Sub btnNo2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("SIB_APPLICANT") = ""

        mpe.Hide()
        btnNo2.Visible = False
        btnYes2.Visible = False
        btnNo.Visible = True
        btnYes.Visible = True

    End Sub

    Protected Sub ddlbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub txtstudno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtstudno.TextChanged
        bind_StudDetails()
    End Sub
    Public Sub bind_StudDetails()
        On Error Resume Next

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_NO", txtstudno.Text)
        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "student_Details_ENQ_ALL", param)
            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read
                    'handle the null value returned from the reader incase  convert.tostring
                    ddlbsu.SelectedValue = Convert.ToString(readerStudent_Detail("STU_BSU_ID"))

                    ''main Details
                    'ddlGEMSSchool.SelectedValue = Convert.ToString(readerStudent_Detail("STU_BSU_ID"))
                    'ddlGEMSSchool_SelectedIndexChanged(ddlGEMSSchool, Nothing)
                    'ddlCurri.SelectedValue = Convert.ToString(readerStudent_Detail("ACD_CLM_ID"))
                    'ddlAca_Year.SelectedValue = Convert.ToString(readerStudent_Detail("ACD_ACY_ID"))
                    'ddlAca_Year_SelectedIndexChanged(ddlAca_Year, Nothing)
                    'ddlShift.SelectedValue = Convert.ToString(readerStudent_Detail("STU_SHF_ID"))
                    'ddlShift_SelectedIndexChanged(ddlShift, Nothing)
                    'ddlStream.SelectedValue = Convert.ToString(readerStudent_Detail("STU_STM_ID"))
                    'ddlGrade.SelectedValue = Convert.ToString(readerStudent_Detail("STU_GRD_ID"))
                    'ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)

                    Dim temp_Gender As String
                    temp_Gender = Convert.ToString(readerStudent_Detail("STU_GENDER"))
                    If UCase(temp_Gender) = "F" Then
                        rdFemale.Checked = True
                    Else
                        rdMale.Checked = True
                    End If
                    txtFname.Text = Convert.ToString(readerStudent_Detail("STU_FIRSTNAME"))
                    txtMname.Text = Convert.ToString(readerStudent_Detail("STU_MIDNAME"))
                    txtLname.Text = Convert.ToString(readerStudent_Detail("STU_LASTNAME"))
                    GetReligion_info()
                    ddlReligion.SelectedValue = Convert.ToString(readerStudent_Detail("STU_RLG_ID"))
                    If IsDate(readerStudent_Detail("STU_DOB")) = True Then
                        txtDob.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_DOB"))))
                    End If

                    txtPob.Text = Convert.ToString(readerStudent_Detail("STU_POB"))
                    ddlCountry.SelectedValue = Convert.ToString(readerStudent_Detail("STU_NATIONALITY"))
                    ddlNational.SelectedValue = Convert.ToString(readerStudent_Detail("STU_NATIONALITY"))
                    txtPassport.Text = Convert.ToString(readerStudent_Detail("STU_PASPRTNO"))
                    Dim emgcont As String = Convert.ToString(readerStudent_Detail("STU_EMGCONTACT")).Replace("-", "").Trim()
                    If emgcont.Length > 5 Then
                        txtStud_Contact_Country.Text = emgcont.Substring(0, 3).ToString()
                        txtStud_Contact_Area.Text = emgcont.Substring(3, 2).ToString()
                        txtStud_Contact_No.Text = emgcont.Substring(5).ToString()
                    End If


                    txtM_Country.Text = ""
                    txtM_Area.Text = ""
                    txtMobile_Pri.Text = ""


                    Dim P_CONTACT As String
                    P_CONTACT = Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT"))
                    If UCase(P_CONTACT) = "F" Then
                        rdPri_Father.Checked = True
                    ElseIf UCase(P_CONTACT) = "M" Then
                        rbPri_Mother.Checked = True
                    Else
                        rdPri_Guard.Checked = True
                    End If

                    Dim passname As String()
                    passname = Convert.ToString(readerStudent_Detail("STU_PASPRTNAME")).Split(" ")
                    txtPri_Fname.Text = Convert.ToString(readerStudent_Detail("STU_FIRSTNAME"))
                    txtPri_Mname.Text = Convert.ToString(readerStudent_Detail("STU_MIDNAME"))
                    txtPri_Lname.Text = Convert.ToString(readerStudent_Detail("STU_LASTNAME"))
                    ddlFLang.SelectedValue = Convert.ToString(readerStudent_Detail("STU_LANG_ID"))
                    ddlPri_National1.SelectedValue = Convert.ToString(readerStudent_Detail("STU_NATIONALITY"))
                    ddlPri_National2.SelectedValue = Convert.ToString(readerStudent_Detail("STU_NATIONALITY"))
                    txtPassportIssue.Text = Convert.ToString(readerStudent_Detail("STU_PASPRTISSPLACE"))

                    If IsDate(readerStudent_Detail("STU_PASPRTISSDATE")) = True Then
                        txtPassIss_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTISSDATE"))))
                    End If

                    If IsDate(readerStudent_Detail("STU_PASPRTEXPDATE")) = True Then
                        txtPassExp_Date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTEXPDATE"))))
                    End If

                    txtVisaNo.Text = Convert.ToString(readerStudent_Detail("STU_VISANO"))
                    txtIss_Place.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSPLACE"))

                    If IsDate(readerStudent_Detail("STU_VISAISSDATE")) = True Then
                        txtVisaIss_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAISSDATE")))).Replace("01/Jan/1900", "")
                    End If

                    If IsDate(readerStudent_Detail("STU_VISAEXPDATE")) = True Then
                        txtVisaExp_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAEXPDATE")))).Replace("01/Jan/1900", "")
                    End If
                    txtIss_Auth.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSAUTH"))
                    ddlPref_contact.SelectedValue = Convert.ToString(readerStudent_Detail("STU_PREFCONTACT")).ToUpper()

                    'contactDetails

                    If UCase(P_CONTACT) = "F" Then
                        txtAdd1_overSea.Text = Convert.ToString(readerStudent_Detail("STS_FPRMADDR1"))
                        txtAdd2_overSea.Text = Convert.ToString(readerStudent_Detail("STS_FPRMADDR2"))
                        txtOverSeas_Add_City.Text = Convert.ToString(readerStudent_Detail("STS_FPRMCITY"))
                        ddlOverSeas_Add_Country.SelectedValue = Convert.ToString(readerStudent_Detail("STS_FPRMCOUNTRY"))
                        txtPoBox_Pri.Text = Convert.ToString(readerStudent_Detail("STS_FPRMPOBOX"))

                        Dim OVERSEA_PHONE As String = Convert.ToString(readerStudent_Detail("STS_FPRMPHONE")).Replace("-", "").Trim()
                        If OVERSEA_PHONE.Length > 5 Then
                            txtPhone_Oversea_Country.Text = OVERSEA_PHONE.Substring(0, 3)
                            txtPhone_Oversea_Area.Text = OVERSEA_PHONE.Substring(3, 2)
                            txtPhone_Oversea_No.Text = OVERSEA_PHONE.Substring(5)
                        End If

                        ''Current Address 
                        txtApartNo.Text = Convert.ToString(readerStudent_Detail("STS_FCOMAPARTNO"))
                        txtBldg.Text = Convert.ToString(readerStudent_Detail("STS_FCOMBLDG"))
                        txtStreet.Text = Convert.ToString(readerStudent_Detail("STS_FCOMSTREET"))

                        txtArea.Text = Convert.ToString(readerStudent_Detail("STS_FCOMAREA"))
                        txtCity_pri.Text = Convert.ToString(readerStudent_Detail("STS_FCOMCITY"))
                        ddlCountry_Pri.SelectedValue = Convert.ToString(readerStudent_Detail("STS_FCOMCOUNTRY"))
                        txtPoboxLocal.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPOBOX"))
                        txtEmail_Pri.Text = Convert.ToString(readerStudent_Detail("STS_FEMAIL"))
                        If Convert.ToString(readerStudent_Detail("STS_FEMIR")) <> "" And Convert.ToString(readerStudent_Detail("STS_FEMIR")) <> " " Then
                            ddlEmirate.SelectedValue = Convert.ToString(readerStudent_Detail("STS_FEMIR"))
                        End If
                        Dim HPhoneF As String = Convert.ToString(readerStudent_Detail("STS_FRESPHONE")).Replace("-", "").Trim()
                        If HPhoneF.Length > 5 Then
                            txtHPhone_Country.Text = HPhoneF.Substring(0, 3).ToString()
                            txtHPhone_Area.Text = HPhoneF.Substring(3, 2).ToString()
                            txtHPhone.Text = HPhoneF.Substring(5).ToString()
                        End If
                        Dim OPhoneF As String = Convert.ToString(readerStudent_Detail("STS_FOFFPHONE")).Replace("-", "").Trim()
                        If OPhoneF.Length > 5 Then
                            txtOPhone_Country.Text = OPhoneF.Substring(0, 3).ToString()
                            txtOPhone_Area.Text = OPhoneF.Substring(3, 2).ToString()
                            txtOPhone.Text = OPhoneF.Substring(5).ToString()
                        End If
                        Dim FaxNoF As String = Convert.ToString(readerStudent_Detail("STS_FFAX")).Replace("-", "").Trim()
                        If FaxNoF.Length > 5 Then
                            txtFaxNo_country.Text = FaxNoF.Substring(0, 3).ToString()
                            txtFaxNo_Area.Text = FaxNoF.Substring(3, 2).ToString()
                            txtFaxNo.Text = FaxNoF.Substring(5).ToString()
                        End If


                        '' ddlPref_contact.Items.FindByValue(readerStudent_Detail("STU_PREFCONTACT").ToUpper).Selected = True
                        If Convert.ToString(readerStudent_Detail("STS_F_COMP_ID")) <> "" Then
                            ddlCompany.SelectedValue = Convert.ToString(readerStudent_Detail("STS_F_COMP_ID"))
                        End If
                        txtOccup.Text = Convert.ToString(readerStudent_Detail("STS_FOCC"))
                        txtComp.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPANY"))

                    ElseIf UCase(P_CONTACT) = "M" Then
                        txtAdd1_overSea.Text = Convert.ToString(readerStudent_Detail("STS_MPRMADDR1"))
                        txtAdd2_overSea.Text = Convert.ToString(readerStudent_Detail("STS_MPRMADDR2"))
                        txtOverSeas_Add_City.Text = Convert.ToString(readerStudent_Detail("STS_MPRMCITY"))
                        ddlOverSeas_Add_Country.SelectedValue = Convert.ToString(readerStudent_Detail("STS_MPRMCOUNTRY"))
                        txtPoBox_Pri.Text = Convert.ToString(readerStudent_Detail("STS_MPRMPOBOX"))
                        Dim OVERSEA_PHONE As String = Convert.ToString(readerStudent_Detail("STS_MPRMPHONE")).Replace("-", "").Trim()
                        If OVERSEA_PHONE.Length > 5 Then
                            txtPhone_Oversea_Country.Text = OVERSEA_PHONE.Substring(0, 3)
                            txtPhone_Oversea_Area.Text = OVERSEA_PHONE.Substring(3, 2)
                            txtPhone_Oversea_No.Text = OVERSEA_PHONE.Substring(5)
                        End If



                        txtEmail_Pri.Text = Convert.ToString(readerStudent_Detail("STS_MEMAIL"))
                        ''Current Address 
                        txtApartNo.Text = Convert.ToString(readerStudent_Detail("STS_MCOMAPARTNO"))
                        txtBldg.Text = Convert.ToString(readerStudent_Detail("STS_MCOMBLDG"))
                        txtStreet.Text = Convert.ToString(readerStudent_Detail("STS_MCOMSTREET"))

                        txtArea.Text = Convert.ToString(readerStudent_Detail("STS_MCOMAREA"))
                        txtCity_pri.Text = Convert.ToString(readerStudent_Detail("STS_MCOMCITY"))
                        ddlCountry_Pri.SelectedValue = Convert.ToString(readerStudent_Detail("STS_MCOMCOUNTRY"))
                        txtPoboxLocal.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPOBOX"))
                        If Convert.ToString(readerStudent_Detail("STS_MEMIR")) <> "" And Convert.ToString(readerStudent_Detail("STS_MEMIR")) <> " " Then
                            ddlEmirate.SelectedValue = Convert.ToString(readerStudent_Detail("STS_MEMIR"))
                        End If

                        Dim HPhoneF As String = Convert.ToString(readerStudent_Detail("STS_MRESPHONE")).Replace("-", "").Trim()
                        If HPhoneF.Length > 5 Then
                            txtHPhone_Country.Text = HPhoneF.Substring(0, 3).ToString()
                            txtHPhone_Area.Text = HPhoneF.Substring(3, 2).ToString()
                            txtHPhone.Text = HPhoneF.Substring(5).ToString()
                        End If
                        Dim OPhoneF As String = Convert.ToString(readerStudent_Detail("STS_MOFFPHONE")).Replace("-", "").Trim()
                        If OPhoneF.Length > 5 Then
                            txtOPhone_Country.Text = OPhoneF.Substring(0, 3).ToString()
                            txtOPhone_Area.Text = OPhoneF.Substring(3, 2).ToString()
                            txtOPhone.Text = OPhoneF.Substring(5).ToString()
                        End If
                        Dim FaxNoF As String = Convert.ToString(readerStudent_Detail("STS_MFAX")).Replace("-", "").Trim()
                        If FaxNoF.Length > 5 Then
                            txtFaxNo_country.Text = FaxNoF.Substring(0, 3).ToString()
                            txtFaxNo_Area.Text = FaxNoF.Substring(3, 2).ToString()
                            txtFaxNo.Text = FaxNoF.Substring(5).ToString()
                        End If


                        txtOccup.Text = Convert.ToString(readerStudent_Detail("STS_MOCC"))

                        If Convert.ToString(readerStudent_Detail("STS_M_COMP_ID")) <> "" And Convert.ToString(readerStudent_Detail("STS_M_COMP_ID")) <> " " Then
                            ddlCompany.SelectedValue = Convert.ToString(readerStudent_Detail("STS_M_COMP_ID"))
                        End If
                        txtComp.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPANY"))
                    Else
                        txtAdd1_overSea.Text = Convert.ToString(readerStudent_Detail("STS_GPRMADDR1"))
                        txtAdd2_overSea.Text = Convert.ToString(readerStudent_Detail("STS_GPRMADDR2"))
                        txtOverSeas_Add_City.Text = Convert.ToString(readerStudent_Detail("STS_GPRMCITY"))
                        ddlOverSeas_Add_Country.SelectedValue = Convert.ToString(readerStudent_Detail("STS_GPRMCOUNTRY"))
                        txtPoBox_Pri.Text = Convert.ToString(readerStudent_Detail("STS_GPRMPOBOX"))
                        txtPhone_Oversea_Country.Text = Convert.ToString(readerStudent_Detail("STS_GPRMPHONE"))
                        txtPhone_Oversea_Area.Text = Convert.ToString(readerStudent_Detail("STS_GPRMPHONE"))
                        txtPhone_Oversea_No.Text = Convert.ToString(readerStudent_Detail("STS_GPRMPHONE"))

                        ''Current Address 
                        txtApartNo.Text = Convert.ToString(readerStudent_Detail("STS_GCOMAPARTNO"))
                        txtBldg.Text = Convert.ToString(readerStudent_Detail("STS_GCOMBLDG"))
                        txtStreet.Text = Convert.ToString(readerStudent_Detail("STS_GCOMSTREET"))
                        txtArea.Text = Convert.ToString(readerStudent_Detail("STS_GCOMAREA"))
                        txtCity_pri.Text = Convert.ToString(readerStudent_Detail("STS_GCOMCITY"))
                        ddlCountry_Pri.SelectedValue = Convert.ToString(readerStudent_Detail("STS_GCOMCOUNTRY"))
                        txtPoboxLocal.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPOBOX"))
                        txtEmail_Pri.Text = Convert.ToString(readerStudent_Detail("STS_GEMAIL"))

                        If Convert.ToString(readerStudent_Detail("STS_GEMIR")) <> "" And Convert.ToString(readerStudent_Detail("STS_GEMIR")) <> " " Then
                            ddlEmirate.SelectedValue = Convert.ToString(readerStudent_Detail("STS_GEMIR"))
                        End If


                        Dim HPhoneF As String = Convert.ToString(readerStudent_Detail("STS_GRESPHONE")).Replace("-", "").Trim()
                        If HPhoneF.Length > 5 Then
                            txtHPhone_Country.Text = HPhoneF.Substring(0, 3).ToString()
                            txtHPhone_Area.Text = HPhoneF.Substring(3, 2).ToString()
                            txtHPhone.Text = HPhoneF.Substring(5).ToString()
                        End If
                        Dim OPhoneF As String = Convert.ToString(readerStudent_Detail("STS_GOFFPHONE")).Replace("-", "").Trim()
                        If OPhoneF.Length > 5 Then
                            txtOPhone_Country.Text = OPhoneF.Substring(0, 3).ToString()
                            txtOPhone_Area.Text = OPhoneF.Substring(3, 2).ToString()
                            txtOPhone.Text = OPhoneF.Substring(5).ToString()
                        End If
                        Dim FaxNoF As String = Convert.ToString(readerStudent_Detail("STS_GFAX")).Replace("-", "").Trim()
                        If FaxNoF.Length > 5 Then
                            txtFaxNo_country.Text = FaxNoF.Substring(0, 3).ToString()
                            txtFaxNo_Area.Text = FaxNoF.Substring(3, 2).ToString()
                            txtFaxNo.Text = FaxNoF.Substring(5).ToString()
                        End If


                        txtOccup.Text = Convert.ToString(readerStudent_Detail("STS_MOCC"))
                        If Convert.ToString(readerStudent_Detail("STS_G_COMP_ID")) <> "" Then
                            ddlCompany.SelectedValue = Convert.ToString(readerStudent_Detail("STS_G_COMP_ID"))
                        End If
                        txtComp.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPANY"))
                    End If
                    Dim bgemsemp As String
                    bgemsemp = Convert.ToString(readerStudent_Detail("STS_bfGEMSEMP"))
                    bgemsemp = Convert.ToString(readerStudent_Detail("STS_bMGEMSEMP"))
                    'If bgemsemp = "True" Then
                    '    chkStaff_GEMS.Checked = True
                    '    'txtStaff_Name
                    '    'txtStaffID
                    '    'ddlStaff_BSU
                    'End If

                    ''Trensport details
                    ''chkTran_Req.Checked = readerStudent_Detail("STU_bUSETPT")
                    ''If chkTran_Req.Checked = True Then
                    ''    Dim sqlcon As String = ConnectionManger.GetOASISTRANSPORTConnectionString
                    ''    Dim tran_qry As String = "select * from  TRANSPORT.SUBLOCATION_M where SBL_ID=" & readerStudent_Detail("STU_SBL_ID_PICKUP") & ""
                    ''    Using sqlreadertransport As SqlDataReader = SqlHelper.ExecuteReader(sqlcon, CommandType.Text, tran_qry)
                    ''        While sqlreadertransport.Read
                    ''            If sqlreadertransport.HasRows = True Then
                    ''                ddlMainLocation.SelectedValue = Convert.ToString(sqlreadertransport("SBL_LOC_ID"))
                    ''                ddlMainLocation_SelectedIndexChanged(ddlMainLocation, Nothing)
                    ''            End If
                    ''        End While
                    ''    End Using

                    ''    If Convert.ToString(readerStudent_Detail("STU_SBL_ID_PICKUP")) <> "" And Convert.ToString(readerStudent_Detail("STU_SBL_ID_PICKUP")) <> " " Then
                    ''        ddlSubLocation.SelectedValue = Convert.ToString(readerStudent_Detail("STU_SBL_ID_PICKUP"))
                    ''        ddlSubLocation_SelectedIndexChanged(ddlSubLocation, Nothing)

                    ''    End If
                    ''    If Convert.ToString(readerStudent_Detail("STU_PICKUP")) <> "" And Convert.ToString(readerStudent_Detail("STU_PICKUP")) <> " " Then
                    ''        ddlPickup.SelectedValue = Convert.ToString(readerStudent_Detail("STU_PICKUP"))
                    ''    End If

                    ''End If

                    ''current school details
                    rdGemsGr.Checked = True
                    rdGemsGr_CheckedChanged(rdGemsGr, Nothing)
                    If rdGemsGr.Checked = True Then

                        txtPre_City.Text = Convert.ToString(readerStudent_Detail("BSU_CITY"))
                        ddlPre_Grade.SelectedValue = Convert.ToString(readerStudent_Detail("STU_GRD_ID"))
                        ddlPre_Curriculum.SelectedValue = Convert.ToString(readerStudent_Detail("ACD_CLM_ID"))
                        txtLast_Att.Text = Convert.ToString(readerStudent_Detail("STU_LASTATTDATE"))
                        ddlGemsGr.SelectedValue = Convert.ToString(readerStudent_Detail("STU_BSU_ID"))
                        ddlPre_Country.SelectedValue = "6"
                        ddlPre_Country.SelectedValue = Convert.ToString(readerStudent_Detail("BSU_COUNTRY_ID")).Trim()
                    End If

                End While
            Else
            End If
        End Using
    End Sub

End Class
