<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowStudentAll.aspx.vb" Inherits="ShowStudentAll" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Student Info</title>
   <base target="_self" />
 <%-- <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/> 
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/> 
    <link rel="stylesheet" type="text/css" href="../cssfiles/all-ie-only.css"/>

     <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script> 
      <script language="javascript" type="text/javascript">
         

          function GetRadWindow() {
              var oWindow = null;
              if (window.radWindow) oWindow = window.radWindow;
              else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
              return oWindow;
          }
</script>
   
</head>
<body onload="listen_window();">
    <form id="form1" runat="server">
    
     <div>
  
            <table align="center" cellpadding="0" cellspacing="0"
                width="100%">
                <tr>
                   
                    <td align="center"  valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr class="title-bg">
                                <td >
                                    Student Details</td>
                            </tr>
                            <tr>
                                <td style="width: 511px">
                                    <table align="center" cellpadding="0" cellspacing="0"
                                        width="100%">
                                        <tr>
                                            <td align="center" valign="top">
                                                <asp:GridView ID="gvSiblingInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False" Width="100%" CaptionAlign="Top" PageSize="15" CssClass="table table-bordered table-row">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Fee ID">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <HeaderTemplate>
                                                                 <asp:Label ID="lblFeeIDH" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                                Text="Fee ID"></asp:Label>
                                                                            <br />
                                                                            <asp:TextBox ID="txtFeeId" runat="server" ></asp:TextBox>
                                                                            <asp:ImageButton ID="btnSearchFeeID" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchFeeID_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFeeID" runat="server" Text='<%# Bind("FeeID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Student No">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("FeeID") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                &nbsp;
                                                                <asp:Label ID="lblStudNo" runat="server" Text='<%# Bind("StudNo") %>'></asp:Label>                                                           
                                                             <asp:HiddenField ID="HF_STU_ID" runat="server" Value='<%# BIND("STU_ID") %>' />
                                                            </ItemTemplate>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblStudNoH" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                    Text="Student No"></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtStudNo" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnSearchStudNo" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchStudNo_Click" />
                                                            </HeaderTemplate>
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Student Name" ShowHeader="False">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblStudNameH" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnSearchStudName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchStudName_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lblStudName" runat="server" CausesValidation="False" CommandName="Selected"
                                                                    OnClick="lblStudName_Click" Text='<%# Eval("StudName") %>'></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Grade">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Stud_ID") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRADE") %>'></asp:Label>&nbsp;
 
                                                            </ItemTemplate>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblGrade" runat="server" CssClass="gridheader_text" Text="Grade" ></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtGrd_search" runat="server" ></asp:TextBox>
                                                                <asp:ImageButton ID="btnSearchGrade" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchGrade_Click" />
                                                            </HeaderTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="School Name">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblSchoolNameH" runat="server" CssClass="gridheader_text" Text="School Name"
                                                                    ></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtSchoolName" runat="server" ></asp:TextBox>
                                                                <asp:ImageButton ID="btnSearchSchoolName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchSchoolName_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSchoolName" runat="server" Text='<%# Bind("SchoolName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="gridheader_pop"  />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    <RowStyle CssClass="griditem"  />
                                                </asp:GridView>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center"  colspan="2" valign="middle">
                        <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                        <input id="h_Selected_menu_0"  runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_1" runat="server"  type="hidden" value="=" />
                        <input id="h_Selected_menu_2" runat="server"  type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server"  type="hidden" value="=" />
                        <input id="h_Selected_menu_4" runat="server"  type="hidden" value="=" /></td>
                </tr>
            </table>
              
     </div>
    
    </form>
</body>
</html>
