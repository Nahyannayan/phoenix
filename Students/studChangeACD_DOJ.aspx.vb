Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_studChangeACD_DOJ
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))

                End If

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100169") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)

                    Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString



                    'ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                    'ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
                    'Populatestream()
                    'PopulateSection()
                    'Dim cb As New CheckBox
                    'For Each gvr As GridViewRow In gvStudChange.Rows
                    '    cb = gvr.FindControl("chkSelect")
                    '    ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    'Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()

                    ViewState("slno") = 0

                    'tblStud.Rows(4).Visible = False
                    tblStud.Rows(5).Visible = False
                    tblStud.Rows(6).Visible = False
                    tblStud.Rows(7).Visible = False
                    tblStud.Rows(8).Visible = False
                    tblStud.Rows(9).Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try

        Else

            highlight_grid()

        End If

        ViewState("slno") = 0
    End Sub


    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub chkAcademicYear_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAcademicYear.CheckedChanged
        If chkAcademicYear.Checked = True Then
            ddlCAcademicYear = studClass.PopulateAcademicYear(ddlCAcademicYear, Session("clm"), Session("sbsuid"))
            ddlCAcademicYear.Enabled = True
        Else
            ddlCAcademicYear.Enabled = False
        End If
    End Sub

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getSerialNo()
        ViewState("slno") += 1
        Return ViewState("slno")
    End Function


    Private Sub PopulateSection()
        ddlSection.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
                                 & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_STM_ID=" + ddlStream.SelectedValue.ToString + " AND GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "')  AND SCT_DESCR<>'TEMP' ORDER BY SCT_DESCR"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSection.DataSource = ds
        ddlSection.DataTextField = "SCT_DESCR"
        ddlSection.DataValueField = "SCT_ID"
        ddlSection.DataBind()
        Dim li As New ListItem
        li.Text = "All"
        li.Value = "0"
        ddlSection.Items.Insert(0, li)
    End Sub
    Public Sub bindShift()
        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = String.Empty
        Dim ds As DataSet
        ddlStream.Items.Clear()
        Dim Acd_id As String = String.Empty
        If ddlAcademicYear.SelectedIndex <> -1 Then
            Acd_id = ddlAcademicYear.SelectedValue
        Else
            Acd_id = 0
        End If
        Dim GRD_ID As String = String.Empty
        If ddlGrade.SelectedIndex <> -1 Then
            GRD_ID = ddlGrade.SelectedValue
        Else
            GRD_ID = ""
        End If
        sql_query = "select SHF_ID,SHF_DESCR from SHIFTS_M WHERE SHF_ID IN(select distinct " _
        & " grm_shf_id from grade_bsu_m where  GRM_GRD_ID='" & GRD_ID & "'  AND  grm_acd_id='" & Acd_id & "')"
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        ddlShift.DataSource = ds
        ddlShift.DataTextField = "SHF_DESCR"
        ddlShift.DataValueField = "SHF_ID"
        ddlShift.DataBind()
    End Sub
    Public Sub bindStream()
        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = String.Empty
        Dim ds As DataSet
        ddlStream.Items.Clear()
        Dim Acd_id As String = String.Empty
        If ddlAcademicYear.SelectedIndex <> -1 Then
            Acd_id = ddlAcademicYear.SelectedValue
        Else
            Acd_id = 0
        End If
        Dim GRD_ID As String = String.Empty
        If ddlGrade.SelectedIndex <> -1 Then
            GRD_ID = ddlGrade.SelectedValue
        Else
            GRD_ID = ""
        End If
        sql_query = "SELECT STM_ID,STM_DESCR  FROM STREAM_M WHERE STM_ID IN( " _
        & " select distinct grm_STM_id from grade_bsu_m where   GRM_GRD_ID='" & GRD_ID & "'  AND  grm_acd_id='" & Acd_id & "')"
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        ddlStream.DataSource = ds
        ddlStream.DataTextField = "STM_DESCR"
        ddlStream.DataValueField = "STM_ID"
        ddlStream.DataBind()
    End Sub
    '   Private Sub Populatestream()
    '       ddlStream.Items.Clear()
    '       Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '       '' Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_GRM_ID IN" _
    '       '                         & "(SELECT GRM_ID FROM GRADE_BSU_M WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "')  AND SCT_DESCR<>'TEMP' ORDER BY SCT_DESCR"
    '       Dim str_query As String = "SELECT STM_ID,STM_DESCR FROM dbo.STREAM_M " _
    '& " WHERE stm_id IN (SELECT DISTINCT grm_stm_id FROM GRADE_BSU_M " _
    '& " WHERE GRM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND GRM_GRD_ID='" + ddlGrade.SelectedValue + "')"

    '       Dim ds As DataSet
    '       ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    '       ddlStream.DataSource = ds
    '       ddlStream.DataTextField = "STM_DESCR"
    '       ddlStream.DataValueField = "STM_ID"
    '       ddlStream.DataBind()
    '       ' Dim li As New ListItem
    '       ' li.Text = "All"
    '       ' li.Value = "0"
    '       ' ddlSection.Items.Insert(0, li)
    '   End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudChange.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudChange.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudChange.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudChange.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Sub highlight_grid()
        For i As Integer = 0 To gvStudChange.Rows.Count - 1
            Dim row As GridViewRow = gvStudChange.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

    Sub GridBind()

        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String = "  SELECT STU_ID,STP_ID,STU_NO,SNAME, GRM_DISPLAY,STP_SCT_ID,SCT_DESCR,stu_DOJ, STM_ID,STM_DESCR,ACY_DESCR FROM (SELECT DISTINCT STU_ID,STP_ID,STU_NO,SNAME=(ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' '))," _
       & " GRM_DISPLAY='" + ddlGrade.SelectedItem.Text + "'," _
 & " STP_SCT_ID AS STP_SCT_ID,SCT_DESCR,REPLACE(CONVERT(VARCHAR(11), STU_DOJ, 106), ' ', '/') AS STU_DOJ," _
 & " STM_ID,STM_DESCR,ACADEMICYEAR_M.ACY_DESCR,STU_bPROMOTED FROM  STUDENT_M AS A INNER JOIN STUDENT_PROMO_S AS B ON A.STU_ID = B.STP_STU_ID INNER JOIN " _
 & " ACADEMICYEAR_D ON A.STU_ACD_ID = ACADEMICYEAR_D.ACD_ID INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID LEFT OUTER JOIN " _
 & "  SECTION_M AS C ON B.STP_SCT_ID = C.SCT_ID LEFT OUTER JOIN STREAM_M AS S ON B.STP_STM_ID = S.STM_ID " _
 & " WHERE  STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
& " AND STP_GRD_ID='" + ddlGrade.SelectedValue + "' AND STP_STM_ID='" + ddlStream.SelectedValue + "' " _
 & " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) AND STU_CURRSTATUS<>'CN' " _
 & " AND STU_ID IN(select stp_stu_id from student_promo_s where stp_bsu_id='" + Session("sBsuid") + "'  group by stp_stu_id having count(stp_stu_id)=1))A WHERE STP_ID<>''"

        If ddlSection.SelectedValue.ToString <> "0" Then
            strQuery += " AND STP_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

        If txtStuNo.Text <> "" Then
            strQuery += " AND STU_NO LIKE '%" + txtStuNo.Text.Trim + "%'"
        End If

        If txtName.Text <> "" Then
            strQuery += " AND SNAME LIKE '%" + txtName.Text.Trim + "%'"
        End If

        If chkAcademicYear.Checked = True Then
            strQuery += " AND ISNULL(STU_bPROMOTED,'FALSE') ='FALSE'"
        End If

        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""


        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox

        If gvStudChange.Rows.Count > 0 Then

            txtSearch = gvStudChange.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("SNAME", txtSearch.Text, strSearch)
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudChange.HeaderRow.FindControl("txtStuNoSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            strNo = txtSearch.Text

            If strFilter <> "" Then
                strQuery += strFilter
            End If
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strQuery)
        gvStudChange.DataSource = ds

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStudChange.DataBind()
            Dim columnCount As Integer = gvStudChange.Rows(0).Cells.Count
            gvStudChange.Rows(0).Cells.Clear()
            gvStudChange.Rows(0).Cells.Add(New TableCell)
            gvStudChange.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudChange.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudChange.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudChange.DataBind()
        End If


        txtSearch = New TextBox
        txtSearch = gvStudChange.HeaderRow.FindControl("txtStuNoSearch")
        txtSearch.Text = strNo

        txtSearch = New TextBox
        txtSearch = gvStudChange.HeaderRow.FindControl("txtStudName")
        txtSearch.Text = strName

    End Sub

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub SaveData()
        Dim i As Integer
        Dim transaction As SqlTransaction
        Dim strQuery As String

        Dim lblstuId As Label
        Dim lblstpId As Label
        Dim lbldoj As Label
        Dim lblStuNo As Label
        Dim lblSTM_ID As Label
        Dim chkSelect As CheckBox

        Dim acdChange As String = String.Empty
        Dim grdChange As String = String.Empty
        Dim STMChange As String = String.Empty
        Dim flagAudit As Integer

        Dim stuIds As String = ""

        Dim updateMaster As Boolean = False

        If chkAcademicYear.Checked = True Then
            acdChange = ddlCAcademicYear.SelectedValue.ToString
        Else
            acdChange = hfACD_ID.Value
        End If

        grdChange = hfGRD_ID.Value


        STMChange = hfSTM_ID.Value

        updateMaster = True
        'if the students acdemic year is the current academic year then update student master
        'If hfACD_ID.Value = Session("Current_ACD_ID") Then
        '    updateMaster = True
        'End If

        'If chkAcademicYear.Checked = True Then
        '    If ddlCAcademicYear.SelectedValue = Session("Current_ACD_ID") Then
        '        updateMaster = True
        '    End If
        'End If


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try
                For i = 0 To gvStudChange.Rows.Count - 1

                    chkSelect = gvStudChange.Rows(i).FindControl("chkSelect")

                    If chkSelect.Checked = True Then
                        lblstuId = gvStudChange.Rows(i).FindControl("lblStuId")
                        lblstpId = gvStudChange.Rows(i).FindControl("lblStpId")
                        lbldoj = gvStudChange.Rows(i).FindControl("lbldoj")
                        lblStuNo = gvStudChange.Rows(i).FindControl("lblstuno")
                        lblSTM_ID = gvStudChange.Rows(i).FindControl("lblSTM_ID")
                        transaction = conn.BeginTransaction("SampleTransaction")

                        'If txtDoj.Text Then

                        'End If
                        ' If updateMaster = True Then
                        UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + lblstuId.Text)
                        UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_SERVICES_D", "SSV_ID", "SSV_STU_ID", "SSV_STU_ID=" + lblstuId.Text)
                        'If
                        UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_PROMO_S", "STP_ID", "STP_STU_ID", "STP_ID=" + lblstpId.Text)

                        strQuery = "exec studCHANGEACD_DOJ " _
                                   & lblstuId.Text + "," _
                                   & lblstpId.Text + "," _
                                   & hfACD_ID.Value + "," _
                                   & acdChange + "," _
                                   & "'" + grdChange + "'," _
                                   & "'" + chkJoin.Checked.ToString + "'," _
                                   & "'" + updateMaster.ToString + "'," _
                                   & "'" + Session("sUsr_name") + "'," _
                                   & IIf(txtDoj.Text = "", "NULL", "'" + txtDoj.Text + "'")

                        SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, strQuery)



                        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "STU_ID(" + lblstuId.Text + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If
                        lblError.Text = "Record Saved Successfully"
                        transaction.Commit()
                    End If

                Next
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

    Sub HIDE_ALL()
        'tblStud.Rows(4).Visible = False
        tblStud.Rows(5).Visible = False
        tblStud.Rows(6).Visible = False
        tblStud.Rows(7).Visible = False
        tblStud.Rows(8).Visible = False
        tblStud.Rows(9).Visible = False
        lblError.Text = ""
        gvStudChange.Visible = False
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        tblStud.Rows(4).Visible = True
        tblStud.Rows(5).Visible = True
        tblStud.Rows(6).Visible = True
        tblStud.Rows(7).Visible = True
        tblStud.Rows(8).Visible = True
        tblStud.Rows(9).Visible = True
        lblError.Text = ""
        GridBind()
        gvStudChange.Visible = True
        hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        hfGRD_ID.Value = ddlGrade.SelectedValue
        hfSTM_ID.Value = ddlStream.SelectedValue.ToString
        chkAcademicYear.Enabled = True
        ddlCAcademicYear.Items.Clear()
        ddlCAcademicYear.Enabled = False
       
        chkAcademicYear.Checked = False
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged

        
        Dim acc As New studClass
        Dim accid = ddlAcademicYear.SelectedValue
        Dim SHF_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        'If ddlShift.SelectedIndex <> -1 Then
        '    SHF_ID = ddlShift.SelectedValue
        'Else
        '    SHF_ID = ""
        'End If
        'If ddlStream.SelectedIndex <> -1 Then
        '    STM_ID = ddlStream.SelectedValue
        'Else
        '    STM_ID = ""
        'End If
        ddlGrade = studClass.PopulateGrade(ddlGrade, accid)

        ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString

        HIDE_ALL()
        'ddlGrade = studClass.PopulateGrade(ddlGrade, ddlAcademicYear.SelectedValue.ToString)
        'If ddlGrade.Items.Count <> 0 Then
        '    Populatestream()
        '    PopulateSection()
        'End If
        'HIDE_ALL()
    End Sub


    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Dim accid = ddlAcademicYear.SelectedValue
        Dim SHF_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        Dim GRD_ID As String = String.Empty
        bindShift()
        bindStream()
        If ddlShift.SelectedIndex <> -1 Then
            SHF_ID = ddlShift.SelectedValue
        Else
            SHF_ID = ""
        End If
        If ddlStream.SelectedIndex <> -1 Then
            STM_ID = ddlStream.SelectedValue
        Else
            STM_ID = ""
        End If
        If ddlGrade.SelectedIndex <> -1 Then
            GRD_ID = ddlGrade.SelectedValue
        Else
            GRD_ID = ""
        End If

        PopulateSection()

        Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString

        HIDE_ALL()


        'Populatestream()
        'PopulateSection()
        'HIDE_ALL()
    End Sub
    Function CheckDOJ() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim acdChange As String = String.Empty
      

        If chkAcademicYear.Checked = True Then
            acdChange = ddlCAcademicYear.SelectedValue.ToString
        Else
            acdChange = hfACD_ID.Value
        End If

        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + acdChange + " AND '" + txtDoj.Text + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The Date of Join should be within the academic year"
            Return False
        Else
            Return True
        End If
    End Function

   
    Protected Sub btnChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChange.Click, btnUpdate2.Click

        'If chkJoin.Checked = True Then
        If txtDoj.Text = "" Then
            lblError.Text = "Please enter the date of join"
            Exit Sub
        End If
        'End If

        If CheckDOJ() = True Then
            SaveData()
            GridBind()
        End If

    End Sub

    Protected Sub gvStudChange_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudChange.PageIndexChanging
        gvStudChange.PageIndex = e.NewPageIndex
        GridBind()
    End Sub


    Protected Sub ddlStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim accid = ddlAcademicYear.SelectedValue
        Dim SHF_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        If ddlShift.SelectedIndex <> -1 Then
            SHF_ID = ddlShift.SelectedValue
        Else
            SHF_ID = ""
        End If
        If ddlStream.SelectedIndex <> -1 Then
            STM_ID = ddlStream.SelectedValue
        Else
            STM_ID = ""
        End If


        '  ddlGrade = studClass.PopulateGrade(ddlGrade, accid, SHF_ID, STM_ID)

        PopulateSection()
        Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString

        HIDE_ALL()


        'PopulateSection()
        'HIDE_ALL()
    End Sub

    Protected Sub ddlSHF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim accid = ddlAcademicYear.SelectedValue
        Dim SHF_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        If ddlShift.SelectedIndex <> -1 Then
            SHF_ID = ddlShift.SelectedValue
        Else
            SHF_ID = ""
        End If
        If ddlStream.SelectedIndex <> -1 Then
            STM_ID = ddlStream.SelectedValue
        Else
            STM_ID = ""
        End If


        'ddlGrade = studClass.PopulateGrade(ddlGrade, accid, SHF_ID, STM_ID)

        PopulateSection()
        Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString
        HIDE_ALL()
    End Sub
End Class
