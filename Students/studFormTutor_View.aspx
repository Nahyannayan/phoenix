<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studFormTutor_View.aspx.vb" Inherits="Students_studFormTutor_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(
                    function CheckForPrint() {
                        if (document.getElementById('<%= h_print.ClientID %>').value != '') {
                            document.getElementById('<%= h_print.ClientID %>').value = '';
                            window.open('../Reports/ASPX Report/rptReportViewerModel.aspx?paging=1', "_blank", "toolbar=no,scrollbars=yes, resizable=yes, top=60, left=200, width=900, height=800");
                        }
                    }
                    );

                    function test1(val) {
                        var path;
                        if (val == 'LI') {
                            path = '../Images/operations/like.gif';
                        } else if (val == 'NLI') {
                            path = '../Images/operations/notlike.gif';
                        } else if (val == 'SW') {
                            path = '../Images/operations/startswith.gif';
                        } else if (val == 'NSW') {
                            path = '../Images/operations/notstartwith.gif';
                        } else if (val == 'EW') {
                            path = '../Images/operations/endswith.gif';
                        } else if (val == 'NEW') {
                            path = '../Images/operations/notendswith.gif';
                        }
                        document.getElementById("<%=getid1()%>").src = path;
                        document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
                    }

                    function test7(val) {
                        var path;
                        if (val == 'LI') {
                            path = '../Images/operations/like.gif';
                        } else if (val == 'NLI') {
                            path = '../Images/operations/notlike.gif';
                        } else if (val == 'SW') {
                            path = '../Images/operations/startswith.gif';
                        } else if (val == 'NSW') {
                            path = '../Images/operations/notstartwith.gif';
                        } else if (val == 'EW') {
                            path = '../Images/operations/endswith.gif';
                        } else if (val == 'NEW') {
                            path = '../Images/operations/notendswith.gif';
                        }
                        document.getElementById("<%=getid7()%>").src = path;
                        document.getElementById("<%=h_selected_menu_7.ClientID %>").value = val + '__' + path;
                    }

                    function test8(val) {
                        var path;
                        if (val == 'LI') {
                            path = '../Images/operations/like.gif';
                        } else if (val == 'NLI') {
                            path = '../Images/operations/notlike.gif';
                        } else if (val == 'SW') {
                            path = '../Images/operations/startswith.gif';
                        } else if (val == 'NSW') {
                            path = '../Images/operations/notstartwith.gif';
                        } else if (val == 'EW') {
                            path = '../Images/operations/endswith.gif';
                        } else if (val == 'NEW') {
                            path = '../Images/operations/notendswith.gif';
                        }
                        document.getElementById("<%=getid8()%>").src = path;
                        document.getElementById("<%=h_selected_menu_8.ClientID %>").value = val + '__' + path;
                    }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="Form Tutor Set Up"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>




                    <tr>
                        <td align="center">
                            <table id="Table1" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- <tr class="subheader_img">
                                    <td align="left" colspan="18" valign="middle">
                                        <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                Form Tutor Set Up&nbsp;</span></font></td>
                                </tr>--%>

                                <tr>
                                    <td align="left" width="15%">
                                        <asp:Label ID="lblAccText" runat="server" Text="Select Academic Year" CssClass="field-label"></asp:Label></td>

                                    <td align="left" width="35%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td align="left" colspan="2" width="50%"></td>

                                </tr>

                                <tr>
                                    <td colspan="4" align="center">
                                        <table id="Table2" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvStudTutor" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                        CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        PageSize="20" Width="100%" BorderStyle="None" OnPageIndexChanging="gvStudTutor_PageIndexChanging" OnSelectedIndexChanging="gvStudTutor_SelectedIndexChanging">
                                                        <RowStyle CssClass="griditem"  />
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SEC_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSecid" runat="server" Text='<%# Bind("sec_id") %>' ></asp:Label>
                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSctid" runat="server" Text='<%# Bind("sec_sct_id") %>' ></asp:Label>
                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrmId" runat="server" Text='<%# Bind("sec_grm_id") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Grade">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblH12" runat="server" Text="Grade"></asp:Label><br />
                                                                    <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Shift">
                                                                <HeaderTemplate>
                                                                    Shift<br />
                                                                    <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>' __designer:wfdid="w36"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Stream">
                                                                <HeaderTemplate>
                                                                    Stream<br />
                                                                    <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True"
                                                                        OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Section">
                                                                <HeaderTemplate>

                                                                    <asp:Label ID="lblH123" runat="server" Text="Section"></asp:Label><br />
                                                                    <asp:TextBox ID="txtSection" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSection_Search" OnClick="btnSection_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>' __designer:wfdid="w32"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Class Teacher">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblCt" runat="server" Text="Class Teacher"></asp:Label><br />
                                                                    <asp:TextBox ID="txtClassTeacher" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnClassTeacher_Search" OnClick="btnClassTeacher_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClassTeacher" runat="server" Text='<%# Bind("emp_name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TeacherId" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTeacherId" runat="server" Visible="False" Text='<%# Bind("sec_emp_id") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="Select" Text="View" HeaderText="View">
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <SelectedRowStyle />
                                                        <HeaderStyle />
                                                        <EditRowStyle />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnprint" runat="server" CssClass="button" OnClick="btnprint_Click"
                                            Text="Print" /></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="h_print" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>



            </div>
        </div>
    </div>

</asp:Content>

