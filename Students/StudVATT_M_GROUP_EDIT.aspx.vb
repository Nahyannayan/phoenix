Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Curriculum_clmGroup_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059014") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    PopulateTeacher()

                    Session("GROUP_TEAC") = CreateDataTable()
                    Session("GROUP_TEAC").Rows.Clear()
                   
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))

                    GetstartDate()
                    ViewState("id") = 1
                    If ViewState("datamode") = "add" Then
                        hfAVG_ID.Value = ""
                        txtFrom.Text = ViewState("ACD_STARTDT")
                        lbView.Visible = False
                        lbAdd.Visible = False
                        hfAVG_ID.Value = ""
                        ViewState("GPmode") = "add"
                        tbSearch.Visible = False
                        btnRemove.Visible = False
                        btnRemove2.Visible = False
                        btnAllocate.Visible = False
                        btnAllocate2.Visible = False
                        btnEdit_T.Visible = False
                        btnCancel_T.Visible = False
                        btnSave_T.Visible = False
                        btnUpdate.Visible = False
                        btnAddNew.Visible = True
                    Else

                        ViewState("GPmode") = "edit"

                        hfAVG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        lbView.Visible = False
                        lbAdd.Visible = True

                        GetTeacher()
                        txtFrom.Text = ViewState("ACD_STARTDT")
                        ' hfAVG_ID.Value = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        txtDescr.Text = Encr_decrData.Decrypt(Request.QueryString("gn").Replace(" ", "+"))
                        If Not ddlAcademicYear.Items.FindByValue(Session("ATT_GP_ACD_ID")) Is Nothing Then
                            ddlAcademicYear.Items.FindByValue(Session("ATT_GP_ACD_ID")).Selected = True
                        End If
                        enabledisable_save()
                    End If
                    PopulateGrade()
                    PopulateSection()
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        '        highlight_grid()
        ViewState("slno") = 0
    End Sub


    Sub GetstartDate()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(1) = New SqlParameter("@INFO_TYPE", "ACD_DATE")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[VA].[GETMASTER_VERTICAL_GRP_BIND]", PARAM)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = ds.Tables(0).Rows(0).Item("ACD_StartDt")
                ViewState("ACD_ENDDT") = ds.Tables(0).Rows(0).Item("ACD_ENDDT")
            Else
                ViewState("ACD_STARTDT") = ""
                ViewState("ACD_ENDDT") = ""

            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnStuNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If ViewState("GPmode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            If ViewState("GPmode") = "add" Then
                GridBind(gvAdd)
            Else
                GridBind(gvView)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim AVT_ID As New DataColumn("AVT_ID", System.Type.GetType("System.String"))
            Dim AVT_AVG_ID As New DataColumn("AVT_AVG_ID", System.Type.GetType("System.String"))
            Dim AVT_EMP_ID As New DataColumn("AVT_EMP_ID", System.Type.GetType("System.String"))
            Dim AVT_ENAME As New DataColumn("AVT_ENAME", System.Type.GetType("System.String"))
            Dim AVT_FROMDT As New DataColumn("AVT_FROMDT", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(AVT_ID)
            dtDt.Columns.Add(AVT_AVG_ID)
            dtDt.Columns.Add(AVT_EMP_ID)
            dtDt.Columns.Add(AVT_ENAME)
            dtDt.Columns.Add(AVT_FROMDT)
            dtDt.Columns.Add(STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function
    Sub GetTeacher()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'if the login user is a teacher then display only the groups for that teacher
        Dim AVG_ID As String = hfAVG_ID.Value

        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@AVG_ID", AVG_ID)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AVG_EMP")


        Dim gvDataset As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[VA].[GETMASTER_VERTICAL_GRP_BIND]", PARAM)
        If gvDataset.Tables(0).Rows.Count > 0 Then
            Session("GROUP_TEAC").Rows.Clear()
            ViewState("id") = 1
            For iIndex As Integer = 0 To gvDataset.Tables(0).Rows.Count - 1
                Dim rDt As DataRow
                rDt = Session("GROUP_TEAC").NewRow
                rDt("SRNO") = ViewState("id")
                rDt("AVT_ID") = gvDataset.Tables(0).Rows(iIndex)("AVT_ID")
                rDt("AVT_AVG_ID") = gvDataset.Tables(0).Rows(iIndex)("AVT_AVG_ID")
                rDt("AVT_EMP_ID") = gvDataset.Tables(0).Rows(iIndex)("AVT_EMP_ID")
                rDt("AVT_ENAME") = gvDataset.Tables(0).Rows(iIndex)("AVT_ENAME")
                rDt("AVT_FROMDT") = gvDataset.Tables(0).Rows(iIndex)("AVT_FROMDT")
                rDt("STATUS") = gvDataset.Tables(0).Rows(iIndex)("STATUS")
                ViewState("id") = ViewState("id") + 1
                Session("GROUP_TEAC").Rows.Add(rDt)
            Next
        End If
        gridbind_TEACHER()
    End Sub
    Sub gridbind_TEACHER()
        Try
            Dim dtTempAtt_GROUP As New DataTable
            dtTempAtt_GROUP = CreateDataTable()

            If Session("GROUP_TEAC").Rows.Count > 0 Then
                For i As Integer = 0 To Session("GROUP_TEAC").Rows.Count - 1
                    If Session("GROUP_TEAC").Rows(i)("STATUS") & "" <> "DELETED" Then
                        Dim rDt As DataRow
                        rDt = dtTempAtt_GROUP.NewRow
                        For j As Integer = 0 To Session("GROUP_TEAC").Columns.Count - 1
                            rDt.Item(j) = Session("GROUP_TEAC").Rows(i)(j)
                        Next

                        dtTempAtt_GROUP.Rows.Add(rDt)
                    End If
                Next

            End If

            If dtTempAtt_GROUP.Rows.Count > 0 Then
                btnSave_T.Visible = True
            End If

            gvTeacher.DataSource = dtTempAtt_GROUP
            gvTeacher.DataBind()


        Catch ex As Exception

        End Try
    End Sub
    Sub AddTeacher()
        Try
            For i As Integer = 0 To Session("GROUP_TEAC").Rows.Count - 1
                If (Session("GROUP_TEAC").Rows(i)("AVT_EMP_ID") = ddlTeacher.SelectedValue) And (Session("GROUP_TEAC").Rows(i)("STATUS") & "" <> "DELETED") Then
                    lblError.Text = "Teacher is already added to this group !!!"
                    Exit Sub
                End If
            Next
            Dim rDt As DataRow
            rDt = Session("GROUP_TEAC").NewRow
            rDt("SRNO") = ViewState("id")
            rDt("AVT_ID") = 0
            rDt("AVT_AVG_ID") = ""
            rDt("AVT_EMP_ID") = ddlTeacher.SelectedValue
            rDt("AVT_ENAME") = ddlTeacher.SelectedItem.Text
            rDt("AVT_FROMDT") = txtFrom.Text
            rDt("STATUS") = "ADD"
            ViewState("id") = ViewState("id") + 1
            Session("GROUP_TEAC").Rows.Add(rDt)
            gridbind_TEACHER()
        Catch ex As Exception
            lblError.Text = "Error in adding new teacher to a group"
        End Try
    End Sub
    Protected Sub gvTeacher_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        btnSave_T.Enabled = False
        btnUpdate.Visible = True
        btnAddNew.Visible = False
        gvTeacher.SelectedIndex = e.NewEditIndex
        Dim row As GridViewRow = gvTeacher.Rows(e.NewEditIndex)
        Dim lblSRNO As New Label
        lblSRNO = TryCast(row.FindControl("lblSRNO"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblSRNO.Text)

        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("GROUP_TEAC").Rows.Count - 1
            If iIndex = Session("GROUP_TEAC").Rows(iEdit)("SRNO") Then
                txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Session("GROUP_TEAC").Rows(iEdit)("AVT_FROMDT"))
                txtFrom.Enabled = True
                If Not ddlTeacher.Items.FindByValue(Session("GROUP_TEAC").Rows(iEdit)("AVT_EMP_ID")) Is Nothing Then

                    ddlTeacher.ClearSelection()
                    ddlTeacher.Items.FindByValue(Session("GROUP_TEAC").Rows(iEdit)("AVT_EMP_ID")).Selected = True
                End If
            End If
        Next
        gvTeacher.Columns(7).Visible = False
        gvTeacher.Columns(8).Visible = False

    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        btnSave_T.Enabled = True
        Dim row As GridViewRow = gvTeacher.Rows(gvTeacher.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblSRNO"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(idRow.Text)
        'loop through the data table row  for the selected rowindex item in the grid view

        For iEdit = 0 To Session("GROUP_TEAC").Rows.Count - 1

            Dim EMP_ID As String = Session("GROUP_TEAC").Rows(iEdit)("AVT_EMP_ID")
            Dim status As String = Session("GROUP_TEAC").Rows(iEdit)("STATUS") & ""
            If iIndex <> Session("GROUP_TEAC").Rows(iEdit)("SRNO") Then
                If (status <> "DELETED") And (EMP_ID = ddlTeacher.SelectedValue) Then
                    lblError.Text = "Teacher already add to this group !!!"
                    Exit Sub
                End If
            End If

        Next
        For iEdit = 0 To Session("GROUP_TEAC").Rows.Count - 1
            If iIndex = Session("GROUP_TEAC").Rows(iEdit)("SRNO") Then

                Session("GROUP_TEAC").Rows(iEdit)("AVT_FROMDT") = txtFrom.Text
                Session("GROUP_TEAC").Rows(iEdit)("AVT_EMP_ID") = ddlTeacher.SelectedValue

                If Session("GROUP_TEAC").Rows(iEdit)("AVT_ID") <> "0" Then
                    Session("GROUP_TEAC").Rows(iEdit)("STATUS") = "UPDATE"
                End If
            End If
        Next
        btnUpdate.Visible = False
        btnAddNew.Visible = True
        gvTeacher.SelectedIndex = -1
        gridbind_TEACHER()
        gvTeacher.Columns(7).Visible = True
        gvTeacher.Columns(8).Visible = True
    End Sub
    Protected Sub gvTeacher_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTeacher.RowDeleting
        Dim COND_ID As Integer = CInt(gvTeacher.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0

        For i = 0 To Session("GROUP_TEAC").Rows.Count - 1
            If Session("GROUP_TEAC").rows(i)("SRNO") = COND_ID Then
                If Session("GROUP_TEAC").rows(i)("AVT_ID") = "0" Then
                    Session("GROUP_TEAC").Rows(i).Delete()
                Else
                    Session("GROUP_TEAC").Rows(i)("STATUS") = "DELETED"
                End If
                Exit For
            End If
        Next

        gridbind_TEACHER()
    End Sub
    
    Sub PopulateTeacher()
        ddlTeacher.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim BSU_ID As String = Session("sBsuid")
        Dim str_Sql As String = " SELECT  ISNULL(EMP_FNAME, '') + ' ' + ISNULL(EMP_MNAME, '') + ' ' + ISNULL(EMP_LNAME, '') AS EMP_NAME,EMP_ID " & _
       " FROM EMPLOYEE_M  where EMP_bACTIVE=1 and EMP_BSU_ID='" & BSU_ID & "' and EMP_DES_ID in(select ATC_DES_ID from AUTHORIZED_CATEGORY where ATC_BSU_ID='" & BSU_ID & "')  order by EMP_NAME"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlTeacher.DataSource = ds
        ddlTeacher.DataTextField = "emp_name"
        ddlTeacher.DataValueField = "emp_id"
        ddlTeacher.DataBind()
        ddlTeacher.Items.Add(New ListItem("", ""))
        If Not ddlTeacher.Items.FindByText("") Is Nothing Then
            ddlTeacher.Items.FindByText("").Selected = True
        End If
    End Sub
    Sub GridBind(ByVal gvGrid As GridView)
         ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strQuery As String = String.Empty
        Dim AVG_ID As String = hfAVG_ID.Value
        Dim ACD_ID As String = ddlAcademicYear.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim strFilter As String = ""
        Dim strName As String = ""
        Dim strNo As String = ""
        Dim txtSearch As New TextBox
        Dim selectedSection As String = ""
        Dim selectedOption As String = ""

        If ViewState("GPmode") = "add" Then
            If ddlSect.SelectedValue <> "" Then
                strFilter += " AND SCT_ID= " + ddlSect.SelectedValue
            End If
            If txtStuNo.Text <> "" Then
                strFilter += " AND STU_NO LIKE '%" + txtStuNo.Text.Trim + "%'"
            End If
            If txtNAME.Text <> "" Then
                strFilter += " AND SNAME LIKE '%" + txtNAME.Text.Trim + "%'"
            End If
        End If


        Dim ds As DataSet

        Dim PARAM(8) As SqlParameter
        PARAM(0) = New SqlParameter("@AVG_ID", AVG_ID)
        PARAM(1) = New SqlParameter("@INFO_TYPE", ViewState("GPmode"))
        PARAM(2) = New SqlParameter("@ACD_ID", ACD_ID)
        PARAM(3) = New SqlParameter("@GRD_ID", GRD_ID)
        PARAM(4) = New SqlParameter("@FILTER_COND", strFilter)

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "VA.GETSTUD_VERTICAL_GRP_GRIDBIND", PARAM)

        gvGrid.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvGrid.DataBind()
            Dim columnCount As Integer = gvGrid.Rows(0).Cells.Count
            gvGrid.Rows(0).Cells.Clear()
            gvGrid.Rows(0).Cells.Add(New TableCell)
            gvGrid.Rows(0).Cells(0).ColumnSpan = columnCount
            gvGrid.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            If ViewState("GPmode") = "view" Then
                gvGrid.Rows(0).Cells(0).Text = "No records to view"
            Else
                gvGrid.Rows(0).Cells(0).Text = "No records to add"
            End If
        Else
            gvGrid.DataBind()
        End If



    End Sub
    
    Sub highlight_grid()
        If ViewState("stumode") = "view" Then
            For i As Integer = 0 To gvView.Rows.Count - 1
                Dim row As GridViewRow = gvView.Rows(i)
                Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
                If isSelect Then
                    row.BackColor = Drawing.Color.FromName("#f6deb2")
                Else
                    row.BackColor = Drawing.Color.Transparent
                End If
            Next
        Else
            For i As Integer = 0 To gvAdd.Rows.Count - 1
                Dim row As GridViewRow = gvAdd.Rows(i)
                Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
                If isSelect Then
                    row.BackColor = Drawing.Color.FromName("#f6deb2")
                Else
                    row.BackColor = Drawing.Color.Transparent
                End If
            Next
        End If
    End Sub
    Sub enabledisable_save()
        tbSearch.Visible = False
        txtDescr.Enabled = False
        ddlAcademicYear.Enabled = False
        ddlTeacher.Enabled = False
        txtFrom.Enabled = False
        gvTeacher.Enabled = False
        btnAddNew.Visible = False
        btnUpdate.Visible = False
        btnSave_T.Visible = False
        btnCancel_T.Visible = False
        btnEdit_T.Visible = True
        GridBind(gvView)
    End Sub
    Protected Sub btnSave_T_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave_T.Click
        Try
            lblError.Text = ""
            If ValidateTeacher() = True Then
                If ViewState("datamode") = "add" Then
                    mvMaster.ActiveViewIndex = 1
                    If isGroupNameExists() = False Then
                        SaveTeacher("add")
                        GetTeacher()
                        enabledisable_save()
                        tbSearch.Visible = True
                    Else
                        lblError.Text = "The Attendance group " + txtDescr.Text + " already exists for the academicyear" + ddlAcademicYear.SelectedItem.Text
                        Exit Sub
                    End If
                    'GridBind(gvAdd)
                End If
                If ViewState("datamode") = "edit" Then

                    If isGroupNameExists_Dup() = False Then
                        SaveTeacher("edit")
                        GetTeacher()
                        enabledisable_save()
                        tbSearch.Visible = False
                    Else
                        lblError.Text = "The group " + txtDescr.Text + " already exists for the academicyear" + ddlAcademicYear.SelectedItem.Text
                        Exit Sub
                    End If
                End If
                ViewState("datamode") = "view"
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                ' EnableDisableControls(False)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub SaveTeacher(ByVal datamode As String)
        Dim i As Integer
        Dim str As String = String.Empty
        Dim AVG_ID As String = String.Empty
        Dim errorMsg As String = String.Empty
        Dim status As Integer
        If datamode = "add" Then
            If Session("GROUP_TEAC") IsNot Nothing Then
                For i = 0 To Session("GROUP_TEAC").Rows.count - 1
                    If Session("GROUP_TEAC").Rows(i)("STATUS") = "ADD" Then
                        str += String.Format("<Teacher Emp_id='{0}' FromDT='{1}' Status='{2}' AVT_ID='{3}' />", Session("GROUP_TEAC").Rows(i)("AVT_EMP_ID"), Session("GROUP_TEAC").Rows(i)("AVT_FROMDT"), Session("GROUP_TEAC").Rows(i)("STATUS"), Session("GROUP_TEAC").Rows(i)("AVT_ID"))
                    End If
                Next
                If str <> "" Then
                    str = "<Teachers>" + str + "</Teachers>"
                End If
            Else
                str = ""
            End If
            If str <> "" Then
                status = calltransaction_saveTeacher(errorMsg, datamode, AVG_ID, str)
                If status <> 0 Then
                    lblError.Text = errorMsg
                Else
                    lblError.Text = "Record saved successfully"
                End If
            Else
                lblError.Text = "Please add teacher to the group"
            End If

        ElseIf datamode = "edit" Then
            AVG_ID = hfAVG_ID.Value

            If Session("GROUP_TEAC") IsNot Nothing Then
                For i = 0 To Session("GROUP_TEAC").Rows.count - 1
                    If (Session("GROUP_TEAC").Rows(i)("STATUS") <> "old") Then
                        If Not ((Session("GROUP_TEAC").Rows(i)("STATUS") = "DELETED") And (Session("GROUP_TEAC").Rows(i)("AVT_ID") = "0")) Then
                            str += String.Format("<Teacher Emp_id='{0}' FromDT='{1}' Status='{2}' AVT_ID='{3}' />", Session("GROUP_TEAC").Rows(i)("AVT_EMP_ID"), Session("GROUP_TEAC").Rows(i)("AVT_FROMDT"), Session("GROUP_TEAC").Rows(i)("STATUS"), Session("GROUP_TEAC").Rows(i)("AVT_ID"))
                        End If

                    End If

                Next
                If str <> "" Then
                    str = "<Teachers>" + str + "</Teachers>"

                End If

                If str <> "" Then
                    status = calltransaction_saveTeacher(errorMsg, datamode, AVG_ID, str)
                    If status <> 0 Then
                        lblError.Text = errorMsg
                    Else
                        lblError.Text = "Record saved successfully"
                    End If
                Else
                    lblError.Text = "No teacher add to the group"
                End If
            End If
        End If
    End Sub
    Function calltransaction_saveTeacher(ByRef errorMessage As String, ByVal DataMode As String, ByVal AVG_ID As String, ByVal str As String) As Integer
        Dim transaction As SqlTransaction
        Dim status As Integer

        Dim ACD_ID As String = ddlAcademicYear.SelectedValue
        Dim GroupName As String = txtDescr.Text

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim pParms(8) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@STR_XML", str)
                If AVG_ID <> "" Then
                    pParms(2) = New SqlClient.SqlParameter("@AVG_ID", AVG_ID)
                Else
                    pParms(2) = New SqlClient.SqlParameter("@AVG_ID", System.DBNull.Value)
                End If
                pParms(3) = New SqlClient.SqlParameter("@DataMode", DataMode)
                pParms(4) = New SqlClient.SqlParameter("@GroupName", GroupName.Trim)
                pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(5).Direction = ParameterDirection.ReturnValue
                pParms(6) = New SqlClient.SqlParameter("@TEMP_AVG_ID", SqlDbType.BigInt)
                pParms(6).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ATT].[SAVETEACHER_GROUP]", pParms)
                status = pParms(5).Value
                If status = 0 Then
                    hfAVG_ID.Value = IIf(TypeOf (pParms(6).Value) Is DBNull, String.Empty, pParms(6).Value)
                End If

                If status <> 0 Then
                    calltransaction_saveTeacher = "1"
                    errorMessage = "Error Occured While Saving."
                    UtilityObj.Errorlog("Error while saving records", "calltransaction_saveTeacher")
                    Return "1"
                End If

                calltransaction_saveTeacher = "0"

            Catch ex As Exception
                calltransaction_saveTeacher = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction_saveTeacher <> "0" Then
                    UtilityObj.Errorlog(errorMessage, "calltransaction_saveTeacher")
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function
    Function ValidateTeacher() As Boolean
        'Dim table As DataTable = DataSet1.Tables("Orders")

        '' Presuming the DataTable has a column named Date.
        'Dim expression As String
        'expression = "Date > #1/1/00#"
        'Dim foundRows() As DataRow

        '' Use the Select method to find all rows matching the filter.
        'foundRows = table.Select(expression)
        'Dim i As Integer
        '' Print column 0 of each returned row.
        'For i = 0 To foundRows.GetUpperBound(0)
        '    Console.WriteLine(foundRows(i)(0))
        'Next i
        Dim dtTempAtt_GROUP As New DataTable
        dtTempAtt_GROUP = CreateDataTable()

        If Session("GROUP_TEAC").Rows.Count > 0 Then
            For i As Integer = 0 To Session("GROUP_TEAC").Rows.Count - 1
                If Session("GROUP_TEAC").Rows(i)("STATUS") & "" <> "DELETED" Then
                    Dim rDt As DataRow
                    rDt = dtTempAtt_GROUP.NewRow
                    For j As Integer = 0 To Session("GROUP_TEAC").Columns.Count - 1
                        rDt.Item(j) = Session("GROUP_TEAC").Rows(i)(j)
                    Next

                    dtTempAtt_GROUP.Rows.Add(rDt)
                End If
            Next

        End If

        If dtTempAtt_GROUP.Rows.Count = 0 Then
            lblError.Text = "Please add teacher to the group"
            Return False
        End If
        Return True
    End Function
    Function isGroupNameExists() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT COUNT(AVG_ID) FROM ATT.ATTENDANCE_VERTICAL_GROUPS WHERE AVG_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND  AVG_GROUP_NAME LIKE '" + txtDescr.Text + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Function isGroupNameExists_Dup() As Boolean
        Dim AVG_ID As String = hfAVG_ID.Value
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT COUNT(AVG_ID) FROM ATT.ATTENDANCE_VERTICAL_GROUPS WHERE AVG_ACD_ID='" + ddlAcademicYear.SelectedValue.ToString + "' AND  AVG_GROUP_NAME LIKE '" + txtDescr.Text + "' AND  AVG_ID<>'" + AVG_ID + "'"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Function isStudentsExits() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(SSD_ID) FROM STUDENT_GROUPS_S WHERE SSD_SGR_ID=" + ""
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Sub EnableDisableControls(ByVal value As Boolean)
        ddlAcademicYear.Enabled = value
        ddlGrade.Enabled = value

        txtDescr.Enabled = value
        'ddlSubject.Enabled = value
        ddlTeacher.Enabled = value
        txtFrom.Enabled = value
        btnAddNew.Enabled = value
        gvTeacher.Enabled = value
        imgFrom.Enabled = value
    End Sub
    Sub SaveStudGroup(ByVal datamode As String)


        Dim str As String = String.Empty
        Dim AVG_ID As String = String.Empty
        Dim errorMsg As String = String.Empty
        Dim status As Integer
        Dim c As CheckBox
        Dim lblSTU_ID As Label
        If datamode = "add" Then

            For Each row As GridViewRow In gvAdd.Rows
                c = DirectCast(row.FindControl("chkSelect"), CheckBox)
                lblSTU_ID = DirectCast(row.FindControl("lblStuId"), Label)
                If c.Checked Then
                    str += String.Format("<STUDENT STU_ID='{0}'/>", lblSTU_ID.Text)
                End If
            Next
            If str <> "" Then
                str = "<STUDENTS>" + str + "</STUDENTS>"
            Else
                str = ""
            End If

            If str <> "" Then
                status = calltransaction_saveStudentGroup(errorMsg, datamode, str)
                If status <> 0 Then
                    lblErrorStu.Text = errorMsg
                Else
                    lblErrorStu.Text = "Records saved successfully"
                End If
            Else
                lblErrorStu.Text = "Please select the students to be added to the group"
            End If

        ElseIf datamode = "edit" Then

            For Each row As GridViewRow In gvView.Rows
                c = DirectCast(row.FindControl("chkSelect"), CheckBox)
                lblSTU_ID = DirectCast(row.FindControl("lblStuId"), Label)
                If c.Checked Then
                    str += String.Format("<STUDENT STU_ID='{0}'/>", lblSTU_ID.Text)
                End If
            Next
            If str <> "" Then
                str = "<STUDENTS>" + str + "</STUDENTS>"
            Else
                str = ""
            End If
            If str <> "" Then
                status = calltransaction_saveStudentGroup(errorMsg, datamode, str)
                If status <> 0 Then
                    lblErrorStu.Text = errorMsg
                Else
                    lblErrorStu.Text = "Records removed successfully"
                End If
            Else
                lblErrorStu.Text = "Please select the students to be removed from the group"
            End If
        End If

    End Sub
    Function calltransaction_saveStudentGroup(ByRef errorMessage As String, ByVal DataMode As String, ByVal str As String) As Integer
        Dim transaction As SqlTransaction
        Dim status As Integer

        Dim ACD_ID As String = ddlAcademicYear.SelectedValue
        Dim AVG_ID As String = hfAVG_ID.Value

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim pParms(8) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@STR_XML", str)
                pParms(2) = New SqlClient.SqlParameter("@AVG_ID", AVG_ID)
                pParms(3) = New SqlClient.SqlParameter("@DataMode", DataMode)
                pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ATT].[SAVEATT_VSTUDENT_GROUP]", pParms)
                status = pParms(4).Value
                If status <> 0 Then
                    calltransaction_saveStudentGroup = "1"
                    errorMessage = "Error Occured While Saving."
                    UtilityObj.Errorlog("Error while saving records", "calltransaction_saveStudentGroup")
                    Return "1"
                End If

                calltransaction_saveStudentGroup = "0"

            Catch ex As Exception
                calltransaction_saveStudentGroup = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction_saveStudentGroup <> "0" Then
                    UtilityObj.Errorlog(errorMessage, "calltransaction_saveStudentGroup")
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using
    End Function

    Protected Sub gvAdd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAdd.PageIndexChanging
        Try
            gvAdd.PageIndex = e.NewPageIndex
            GridBind(gvAdd)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub gvView_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvView.PageIndexChanging
        Try
            gvView.PageIndex = e.NewPageIndex
            GridBind(gvView)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            If ViewState("datamode") = "add" Then
                PopulateGrade()
                PopulateSection()
            End If
            Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Sub PopulateGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_filter As String = String.Empty
        Dim acd_id As String = ddlAcademicYear.SelectedValue

        Dim ds As DataSet

        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@ACD_ID", acd_id)
        PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(2) = New SqlParameter("@INFO_TYPE", "VERT_GRD")


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[VA].[GETMASTER_VERTICAL_GRP_BIND]", PARAM)


        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

    End Sub
    Sub PopulateSection()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_filter As String = String.Empty
        Dim acd_id As String = ddlAcademicYear.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim ds As DataSet
        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@ACD_ID", acd_id)
        PARAM(1) = New SqlParameter("@GRD_ID", GRD_ID)
        PARAM(2) = New SqlParameter("@INFO_TYPE", "VERT_SCT")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[VA].[GETMASTER_VERTICAL_GRP_BIND]", PARAM)

        ddlSect.Items.Clear()
        ddlSect.DataSource = ds
        ddlSect.DataTextField = "sct_descr"
        ddlSect.DataValueField = "sct_id"
        ddlSect.DataBind()
        ddlSect.Items.Add(New ListItem("ALL", ""))
        ddlSect.ClearSelection()
        ddlSect.Items.FindByText("ALL").Selected = True
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Try

            PopulateSection()
            GridBind(gvAdd)
            Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllocate.Click, btnAllocate2.Click
        Try
            lbView.Visible = True
            ''lbView.Width = 70
            SaveStudGroup("add")
            GridBind(gvAdd)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click, btnRemove2.Click
        Try
            SaveStudGroup("edit")
            GridBind(gvView)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try

            AddTeacher()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub lbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAdd.Click
        Try
            mvMaster.ActiveViewIndex = 1
            lbView.Visible = True
            tbSearch.Visible = True
            PopulateGrade()
            PopulateSection()
            enable_disable_edit(False)
            Session("WEB_SER_VAR") = ddlAcademicYear.SelectedValue.ToString & "|" & ddlGrade.SelectedValue.ToString
            ViewState("GPmode") = "add"
            GridBind(gvAdd)
            ' gvAdd.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbView.Click
        Try
            mvMaster.ActiveViewIndex = 0
            lblError.Text = ""
            lbAdd.Visible = True
            enable_disable_edit(False)
            tbSearch.Visible = False
            ViewState("GPmode") = "edit"
            btnAllocate.Visible = True
            btnAllocate2.Visible = True
            btnRemove.Visible = True
            btnRemove2.Visible = True
            gvView.DataBind()
            GridBind(gvView)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub
    Protected Sub btnEdit_T_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        enable_disable_edit(True)
        ViewState("datamode") = "edit"
        mvMaster.ActiveViewIndex = 0

    End Sub
    Sub enable_disable_edit(ByVal flag As Boolean)
        btnAddNew.Visible = flag
        btnSave_T.Visible = flag
        btnCancel_T.Visible = flag
        btnEdit_T.Visible = Not flag
        gvTeacher.Enabled = flag
        ddlTeacher.Enabled = flag
        txtDescr.Enabled = flag
        txtFrom.Enabled = flag
        tbSearch.Visible = Not flag
    End Sub
    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("GPmode") = "add" Then
            btnAllocate.Visible = True
            btnAllocate2.Visible = True
            btnRemove.Visible = False
            btnRemove2.Visible = False
        Else
            btnAllocate.Visible = False
            btnAllocate2.Visible = False
            btnRemove.Visible = True
            btnRemove2.Visible = True
        End If


        GridBind(gvAdd)
    End Sub


    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' Response.Redirect(Request.Url.AbsoluteUri.Replace("4xCdg%2fcr4Xw%3d", Encr_decrData.Encrypt("add")))
            ViewState("datamode") = Encr_decrData.Encrypt("add")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim url As String
            url = String.Format("~\Students\StudVATT_M_GROUP_EDIT.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_T_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        tbSearch.Visible = False
        txtDescr.Enabled = False
        ddlAcademicYear.Enabled = False
        ddlTeacher.Enabled = False
        txtFrom.Enabled = False
        gvTeacher.Enabled = False
        btnAddNew.Visible = False
        btnUpdate.Visible = False
        btnSave_T.Visible = False
        btnCancel_T.Visible = False
        btnEdit_T.Visible = True
        ViewState("GPmode") = "edit"
        GridBind(gvView)

        mvMaster.ActiveViewIndex = 0
    End Sub
End Class
