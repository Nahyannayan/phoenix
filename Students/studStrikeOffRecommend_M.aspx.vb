Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studStrikeOffRecommend_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100250") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    txtName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    txtSEN.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    hfSTK_ID.Value = 0
                    txtRec.Text = Format(Now.Date, "dd/MMM/yyyy")


                    GetData()
                    If hfSTK_ID.Value = 0 Then
                        btnRemove.Visible = False
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try


        Else


        End If
    End Sub

#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TCM_STU_ID", hfSTU_ID.Value)

        Using readerGet As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "TCSO_Get_LastAttDate", pParms)
            While readerGet.Read
                txtLast.Text = Format(readerGet.GetDateTime(0), "dd/MMM/yyyy")
            End While
        End Using





        Dim str_query As String = "SELECT STK_ID,STK_LASTATTDATE,STK_RECREMARKS,STK_RECDATE FROM " _
                                 & " STRIKEOFF_RECOMMEND_M WHERE STK_STU_ID=" + hfSTU_ID.Value _
                                 & " AND STK_bAPPROVED IS NULL AND STK_CANCELDATE IS NULL"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            hfSTK_ID.Value = reader.GetValue(0)
            txtLast.Text = Format(reader.GetDateTime(1), "dd/MMM/yyyy")
            txtRemarks.Text = reader.GetString(2).Replace("''", "'")
            txtRec.Text = Format(reader.GetDateTime(3), "dd/MMM/yyyy")
        End While
        reader.Close()
    End Sub

    Function CheckLastAttDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value + " AND '" + txtLast.Text + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The last attendance date should be within the academic year"
            Return False
        Else
            Return True
        End If
    End Function

    Sub SaveData(ByVal bCancel As Boolean)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "exec saveStrikeOffRecommend_M " _
                                 & hfSTK_ID.Value + "," _
                                 & hfSTU_ID.Value + "," _
                                 & "'" + Format(Date.Parse(txtLast.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + Format(Date.Parse(txtRec.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + txtRemarks.Text.Replace("'", "''") + "'," _
                                 & "'" + bCancel.ToString + "'"
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                If hfSTK_ID.Value <> 0 Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "STRIKEOFF_RECOMMEND_M", "STK_ID", "STK_STU_ID", "STK_ID=" + hfSTK_ID.Value.ToString, "SO_RECOMMEND")
                End If
                hfSTK_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
               
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

#End Region


 
   


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ' as per the discussion with charan i am removing the check for last date of attendance,for some schools they
        'want to give the last date of attendance in the previous academic year after the student is promoted

        'If CheckDate() = True Then
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TCM_STU_ID", hfSTU_ID.Value)
        pParms(1) = New SqlClient.SqlParameter("@TCM_LASTATTDATE", Format(Date.Parse(txtLast.Text), "yyyy-MM-dd"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "TCSO_LastAttDate_Validate", pParms)
            While reader.Read
                Dim lstrRet = Convert.ToString(reader("Ret"))
                If lstrRet = "0" Then
                    lblError.Text = "Last date of attendance cannot be less  than the actual attended date"
                    Exit Sub
                End If
            End While
        End Using

        'If CheckLastAttDate() = False Then
        '    lblError.Text = "Last date of attendance should be within the academic year"
        '    Exit Sub
        'End If



        SaveData(False)
        btnRemove.Visible = True
        ' Else
        'lblError.Text = "The service start date should be within the academic year"
        ' Exit Sub

        ' End If
    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SaveData(True)
        hfSTK_ID.Value = 0
        txtRec.Text = Format(Now.Date, "dd/MMM/yyyy")
        txtLast.Text = ""
        txtRemarks.Text = ""
    End Sub


    Function CheckDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value + " AND '" + Format(Date.Parse(txtLast.Text), "yyyy-MM-dd") + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The service start date should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function


End Class
