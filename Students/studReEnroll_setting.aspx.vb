﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Students_studReEnroll_setting
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then




            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050430") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



                    GridBind()
                    gvOption.Attributes.Add("bordercolor", "#1b80b6")
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
    Private Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_query As String = "SELECT row_number() over(order by BSU_NAME) as ID,BSU_NAME , CLM_DESCR , isnull(PLMS_bSHOW_MENU,0)PLMS_bSHOW_MENU " _
                                  & " , isnull(replace(CONVERT(varchar,RED_STARTDT,106),' ','/'),'')RED_STARTDT " _
                                  & " , isnull(replace(CONVERT(varchar,RED_ENDDT,106),' ','/'),'') RED_ENDDT " _
                                  & " ,ACD_ID,BSU_ID,ACD_CLM_ID " _
                                  & " FROM dbo.vw_CORP_REP_BSU A WITH (NOLOCK)" _
                                  & " INNER JOIN dbo.ACADEMICYEAR_D B WITH (NOLOCK) ON BSU_ID=ACD_BSU_ID AND ACD_CURRENT=1" _
                                  & " INNER JOIN OPL.REENROLL_DATE C WITH (NOLOCK) ON ACD_ID=RED_ACD_ID " _
                                  & " INNER JOIN OPL.PARENTLOGIN_MENU_S D WITH (NOLOCK) ON BSU_ID=PLMS_BSU_ID AND PLMS_OPLM_ID=32 " _
                                  & " INNER JOIN dbo.CURRICULUM_M E WITH (NOLOCK) ON ACD_CLM_ID=CLM_ID " _
                                  & " ORDER BY BSU_NAME"


            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvOption.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvOption.DataBind()
                Dim columnCount As Integer = gvOption.Rows(0).Cells.Count
                gvOption.Rows(0).Cells.Clear()
                gvOption.Rows(0).Cells.Add(New TableCell)
                gvOption.Rows(0).Cells(0).ColumnSpan = columnCount
                gvOption.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvOption.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvOption.DataBind()
            End If

    End Sub
    Protected Sub gvOption_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOption.PageIndexChanging
        Try
            gvOption.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvOption_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvOption.RowCommand
        If e.CommandName = "Add" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvOption.Rows(index), GridViewRow)

            Dim lblbsu As Label = selectedRow.FindControl("lblbsuid")
            Dim lblacd As Label = selectedRow.FindControl("lblacdid")
            Dim lblclm As Label = selectedRow.FindControl("lblclmid")

            hd_bsu_id.Value = lblbsu.Text
            hd_acd_id.Value = lblacd.Text
            hd_clm_id.Value = lblclm.Text

            Panel_MSG.Visible = True
            BindSchool()
            BindCLM()
            display_items()
        End If
    End Sub

    Protected Sub gvOption_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOption.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lblOnline As New Label
            lblOnline = e.Row.FindControl("lblOnline")

            If lblOnline IsNot Nothing Then

                If lblOnline.Text = "True" Then

                    Dim ib As ImageButton = e.Row.Cells(4).Controls(0)
                    ib.ImageUrl = "~\images\tick.gif"
                Else
                    Dim ib As ImageButton = e.Row.Cells(4).Controls(0)
                    ib.ImageUrl = "~\images\cross.gif"
                End If
            End If
        End If
    End Sub

    Protected Sub btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn.Click
        Panel_MSG.Visible = False
        GridBind()
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Panel_MSG.Visible = False
        GridBind()
    End Sub

    
    Sub BindSchool()


        ddlbsu.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "SELECT distinct(BSU_ID)BSU_ID ,BSU_NAME " _
                                  & " FROM dbo.vw_CORP_REP_BSU A WITH (NOLOCK)" _
                                  & " INNER JOIN dbo.ACADEMICYEAR_D B WITH (NOLOCK) ON BSU_ID=ACD_BSU_ID AND ACD_CURRENT=1" _
                                  & " INNER JOIN OPL.REENROLL_DATE C WITH (NOLOCK) ON ACD_ID=RED_ACD_ID " _
                                  & " INNER JOIN OPL.PARENTLOGIN_MENU_S D WITH (NOLOCK) ON BSU_ID=PLMS_BSU_ID AND PLMS_OPLM_ID=32 " _
                                  & " INNER JOIN dbo.CURRICULUM_M E WITH (NOLOCK) ON ACD_CLM_ID=CLM_ID " _
                                  & " ORDER BY BSU_NAME"



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlbsu.DataSource = ds
        ddlbsu.DataTextField = "BSU_NAME"
        ddlbsu.DataValueField = "BSU_ID"
        ddlbsu.DataBind()
        If hd_bsu_id.Value <> "" Then
            ddlbsu.Items.FindByValue(hd_bsu_id.Value).Selected = True
        End If
    End Sub
    Sub BindCLM()


        ddlCLM.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        str_query = "select ACD_CLM_ID,CLM_DESCR  from dbo.ACADEMICYEAR_D " _
                    & " INNER JOIN dbo.CURRICULUM_M E WITH (NOLOCK) ON ACD_CLM_ID=CLM_ID  AND ACD_CURRENT=1 " _
                    & " where ACD_BSU_ID='" & ddlbsu.SelectedValue & "'"



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCLM.DataSource = ds
        ddlCLM.DataTextField = "CLM_DESCR"
        ddlCLM.DataValueField = "ACD_CLM_ID"
        ddlCLM.DataBind()
        If hd_clm_id.Value <> "" Then
            ddlCLM.Items.FindByValue(hd_clm_id.Value).Selected = True
        End If
    End Sub

    Protected Sub ddlbsu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlbsu.SelectedIndexChanged
        hd_bsu_id.Value = ""
        hd_acd_id.Value = ""
        hd_clm_id.Value = ""
        BindCLM()
        display_items()
    End Sub
    Sub display_items()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT row_number() over(order by BSU_NAME) as ID,BSU_NAME , CLM_DESCR , isnull(PLMS_bSHOW_MENU,0)PLMS_bSHOW_MENU " _
                                  & " , isnull(replace(CONVERT(varchar,RED_STARTDT,106),' ','/'),'')RED_STARTDT " _
                                  & " , isnull(replace(CONVERT(varchar,RED_ENDDT,106),' ','/'),'') RED_ENDDT " _
                                  & " ,ACD_ID,BSU_ID,ACD_CLM_ID,isnull(RED_bOnlinePayment,0) OnlinePayment,isnull(RED_bSchoolPayment,0) SchoolPayment " _
                                  & " FROM dbo.vw_CORP_REP_BSU A WITH (NOLOCK)" _
                                  & " INNER JOIN dbo.ACADEMICYEAR_D B WITH (NOLOCK) ON BSU_ID=ACD_BSU_ID AND ACD_CURRENT=1" _
                                  & " INNER JOIN OPL.REENROLL_DATE C WITH (NOLOCK) ON ACD_ID=RED_ACD_ID " _
                                  & " INNER JOIN OPL.PARENTLOGIN_MENU_S D WITH (NOLOCK) ON BSU_ID=PLMS_BSU_ID AND PLMS_OPLM_ID=32 " _
                                  & " INNER JOIN dbo.CURRICULUM_M E WITH (NOLOCK) ON ACD_CLM_ID=CLM_ID " _
                                  & " WHERE BSU_ID='" & ddlbsu.SelectedValue & "' AND ACD_CLM_ID=" & ddlCLM.SelectedValue & " ORDER BY BSU_NAME"

        rdOnline.Items(0).Selected = False
        rdOnline.Items(1).Selected = False
        txtFrom.Text = ""
        txtTo.Text = ""
        If str_query <> "" Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If ds.Tables(0).Rows.Count >= 1 Then
                If ds.Tables(0).Rows(0).Item("PLMS_bSHOW_MENU") Then
                    rdOnline.Items(0).Selected = True
                Else
                    rdOnline.Items(1).Selected = True
                End If
                txtFrom.Text = ds.Tables(0).Rows(0).Item("RED_STARTDT")
                txtTo.Text = ds.Tables(0).Rows(0).Item("RED_ENDDT")
                chkOnlinePayment.Checked = ds.Tables(0).Rows(0).Item("OnlinePayment")
                chkSchoolPayment.Checked = ds.Tables(0).Rows(0).Item("SchoolPayment")
            Else
                rdOnline.Items(0).Selected = False
                rdOnline.Items(1).Selected = False
                txtFrom.Text = ""
                txtTo.Text = ""
                chkOnlinePayment.Checked = False
                chkSchoolPayment.Checked = False
            End If
        End If


    End Sub

    Protected Sub ddlCLM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCLM.SelectedIndexChanged
        display_items()
    End Sub

    Protected Sub btnOnline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOnline.Click

        Dim Status As Integer
        Dim param(8) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction
        
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                param(0) = New SqlParameter("@BSU_ID", ddlbsu.SelectedValue)
                param(1) = New SqlParameter("@CLM_ID", ddlCLM.SelectedValue)
                param(2) = New SqlClient.SqlParameter("@ONLINE", IIf(rdOnline.Items(0).Selected, True, False))
                param(3) = New SqlClient.SqlParameter("@FRMDTE", IIf(txtFrom.Text = "", DBNull.Value, txtFrom.Text))
                param(4) = New SqlClient.SqlParameter("@TODTE", IIf(txtTo.Text = "", DBNull.Value, txtTo.Text))
                param(5) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(5).Direction = ParameterDirection.ReturnValue

                param(6) = New SqlClient.SqlParameter("@bOnlinePayment", chkOnlinePayment.Checked)
                param(7) = New SqlClient.SqlParameter("@bSchoolPayment", chkSchoolPayment.Checked)
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "OPL.SAVE_REENROLL_SET", param)
                Status = param(5).Value





            Catch ex As Exception

                lblError1.Text = "Record could not be updated"
            Finally
                If Status <> 0 Then
                    UtilityObj.Errorlog(lblError.Text)
                    transaction.Rollback()
                Else
                    lblError1.Text = "Saved.."
                    transaction.Commit()
                End If
            End Try

        End Using
    End Sub
End Class
