Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_studSubject_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim temp As Integer
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

                Dim str_sql As String = ""
               
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state
                If ViewState("datamode") = "view" Then

                    ViewState("Eid") = Encr_decrData.Decrypt(Request.QueryString("Eid").Replace(" ", "+"))


                    
                End If


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050015" And ViewState("MainMnu_code") <> "S050020" And ViewState("MainMnu_code") <> "S050025") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))



                    If ViewState("MainMnu_code") = "S050015" Then 'Subject master
                        ' ro1.Visible = False
                        ro2.Visible = False

                        ro4.Visible = False

                        lblTitle.Text = "Subject Master"

                        ltHeader.Text = UCase("Subject Master")

                        ViewState("Etype") = "SM"
                        If ViewState("datamode") = "view" Then
                            txtID.Attributes.Add("Readonly", "Readonly")
                            txtDescr.Attributes.Add("Readonly", "Readonly")
                            Dim bflag As Boolean
                            Using Subject_Mreader As SqlDataReader = AccessStudentClass.GetSubject_M(ViewState("Eid"))
                                While Subject_Mreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring
                                    txtID.Text = Convert.ToString(Subject_Mreader("ID"))

                                    txtDescr.Text = Convert.ToString(Subject_Mreader("DESCR"))

                                    bflag = Convert.ToString(Subject_Mreader("bFlag"))
                                End While

                                'clear of the resource end using
                            End Using

                            If bflag = True Then

                                rdYes.Checked = True
                            Else
                                rdNo.Checked = True
                            End If



                        ElseIf ViewState("datamode") = "add" Then
                            rdNo.Checked = True


                        End If


                    ElseIf ViewState("MainMnu_code") = "S050020" Then 'House Master
                        Dim myList As New ArrayList()


                        Using ColorReader As SqlDataReader = AccessStudentClass.GetColor()
                            'create a list item to bind records from reader to dropdownlist ddlBunit
                            Dim di As ListItem
                            ddlColor.Items.Clear()
                            'check if it return rows or not
                            If ColorReader.HasRows = True Then
                                While ColorReader.Read
                                    di = New ListItem(ColorReader("COLOR_DESCR"), ColorReader("COLOR_HEX"))
                                    'adding listitems into the dropdownlist
                                    ddlColor.Items.Add(di)
                                End While
                            End If
                        End Using
                        'setting background color
                        backGround()


                        lblTitle.Text = "House Master"
                        ltHeader.Text = UCase("House Master")
                        ro1.Visible = False
                        ro2.Visible = False
                        ro5.Visible = False

                        ViewState("Etype") = "HM"
                        If ViewState("datamode") = "view" Then
                            txtDescr.Attributes.Add("Readonly", "Readonly")

                            Dim temp As String
                            Using House_Mreader As SqlDataReader = AccessStudentClass.GetHouse_M(ViewState("Eid"))
                                While House_Mreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring

                                    txtDescr.Text = Convert.ToString(House_Mreader("Descr"))

                                    temp = Convert.ToString(House_Mreader("HColor"))
                                End While
                                'clear of the resource end using
                            End Using
                            Dim ItemTypeCounter As Integer = 0 'for counting Items in the listitem
                            For ItemTypeCounter = 0 To ddlColor.Items.Count - 1
                                'keep loop until you get the counter for default Color into  the SelectedIndex
                                If ddlColor.Items(ItemTypeCounter).Value = temp Then
                                    ddlColor.SelectedIndex = ItemTypeCounter
                                End If
                            Next

                        ElseIf ViewState("datamode") = "add" Then



                        End If

                    ElseIf ViewState("MainMnu_code") = "S050025" Then 'Document Master



                        lblTitle.Text = "Document Master"
                        ltHeader.Text = UCase("Document Master")
                        ro1.Visible = False
                        ro4.Visible = False
                        ro5.Visible = False
                        lblCode.Text = "Doc. Type"


                        ViewState("Etype") = "DT"
                        If ViewState("datamode") = "view" Then
                            txtCode.Attributes.Add("Readonly", "Readonly")
                            txtDescr.Attributes.Add("Readonly", "Readonly")
                            btnAcademic.Visible = False
                            ' Dim temp As String
                            Using Documentreader As SqlDataReader = AccessStudentClass.GetDOCREQD_M(ViewState("Eid"))
                                While Documentreader.Read

                                    'handle the null value returned from the reader incase  convert.tostring

                                    hfTemp.Value = Convert.ToString(Documentreader("Doc_ID"))

                                    txtCode.Text = Convert.ToString(Documentreader("DType"))
                                    txtDescr.Text = Convert.ToString(Documentreader("Descr"))

                                    'hf = Convert.ToString(Documentreader("Doc_ID"))
                                End While

                            End Using


                        ElseIf ViewState("datamode") = "add" Then

                            If ViewState("Etype") = "DT" Then
                                txtDescr.Attributes.Remove("readonly")
                                txtCode.Attributes.Remove("Readonly")
                                btnAcademic.Visible = True
                            End If

                        End If

                    End If
                End If


            Catch ex As Exception

            End Try

        End If
    End Sub
    Sub backGround()
        Dim i As Integer
        For i = 0 To ddlColor.Items.Count - 1
            ddlColor.Items(i).Attributes.Add("style", "background-color:" + ddlColor.Items(i).Value)
        Next
    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        txtDescr.Attributes.Remove("Readonly")
        If ViewState("Etype") = "DT" Then
            txtCode.Attributes.Remove("Readonly")
            btnAcademic.Visible = True
        End If
        Call backGround()
        ViewState("datamode") = "edit"
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Sub clearMe()
        txtID.Text = ""
        txtDescr.Text = ""
        txtCode.Text = ""
        ViewState("Eid") = ""
        hfId1.Value = ""
        hfTemp.Value = ""
        txtDescr.Attributes.Add("Readonly", "Readonly")
        If ViewState("Etype") = "DT" Then
            txtCode.Attributes.Add("Readonly", "Readonly")
            btnAcademic.Visible = False
        End If

        If ViewState("Etype") = "SM" Then
            rdYes.Checked = False
            rdNo.Checked = True
        End If
        If ViewState("Etype") = "HM" Then
            ddlColor.SelectedIndex = 0
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearMe()
            'clear the textbox and set the default settings
            Call backGround()
            ViewState("datamode") = "none"
            txtDescr.Attributes.Add("Readonly", "Readonly")
            If ViewState("Etype") = "DT" Then
                txtCode.Attributes.Add("Readonly", "Readonly")
                btnAcademic.Visible = False
            End If
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then
            Dim Status As Integer
            Dim lang As Boolean
            Dim bEdit As Boolean
            Dim Comm_ID As Integer = 0 'commonly used ID
            Dim CurBsUnit As String = Session("sBsuid")
            Dim Descr As String = txtDescr.Text 'Common Description
            Dim Color_HEX As String 'Color Code in HEX

            'check the data mode to do the required operation

            If ViewState("datamode") = "edit" Then
                bEdit = True
                Comm_ID = ViewState("Eid")
                Dim transaction As SqlTransaction

                '
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection

                    transaction = conn.BeginTransaction("SampleTransaction")

                    Try

                        If ViewState("Etype") = "SM" Then 'Subject Master
                            lang = rdYes.Checked

                            Status = AccessStudentClass.SaveSTUDSubject_M(Comm_ID, Descr, lang, ViewState("Etype"), bEdit, transaction)


                        ElseIf ViewState("Etype") = "HM" Then 'House Master
                            Color_HEX = ddlColor.SelectedValue

                            Status = AccessStudentClass.SaveSTUDHouse_M(Comm_ID, CurBsUnit, Descr, Color_HEX, ViewState("Etype"), bEdit, transaction)
                        ElseIf ViewState("Etype") = "DT" Then 'Document Master


                            Status = AccessStudentClass.SaveSTUDDOCREQD_M(Comm_ID, hfTemp.Value, Descr, ViewState("Etype"), bEdit, transaction)

                        End If

                        'If error occured during the process  throw exception and rollback the process

                        If Status = -1 Then

                            Throw New ArgumentException("Record does not exist for updating")
                        ElseIf Status = -2 Then
                            Throw New ArgumentException("Duplicate House Name!!!!")
                        ElseIf Status <> 0 Then


                            Throw New ArgumentException("Error while updating the current record")

                        Else
                            'Store the required information into the Audit Trial table when Edited

                            Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "edit", Page.User.Identity.Name.ToString, Me.Page)


                            If Status <> 0 Then

                                Throw New ArgumentException("Could not complete your request")

                            End If
                            ViewState("datamode") = "none"
                            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            transaction.Commit()
                            Call clearMe()
                            lblError.Text = "Record Updated Successfully"

                        End If


                    Catch myex As ArgumentException

                        transaction.Rollback()
                        lblError.Text = myex.Message

                    Catch ex As Exception

                        transaction.Rollback()
                        lblError.Text = "Record could not be Updated"

                    End Try

                End Using

                Call backGround()
            ElseIf ViewState("datamode") = "add" Then
                bEdit = False
                Dim transaction As SqlTransaction


                Using conn As SqlConnection = ConnectionManger.GetOASISConnection

                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try


                        If ViewState("Etype") = "SM" Then
                            lang = rdYes.Checked

                            Status = AccessStudentClass.SaveSTUDSubject_M(txtID.Text, Descr, lang, ViewState("Etype"), bEdit, transaction)

                        ElseIf ViewState("Etype") = "HM" Then
                            Color_HEX = ddlColor.SelectedValue

                            Status = AccessStudentClass.SaveSTUDHouse_M(Comm_ID, CurBsUnit, Descr, Color_HEX, ViewState("Etype"), bEdit, transaction)


                        ElseIf ViewState("Etype") = "DT" Then


                            Status = AccessStudentClass.SaveSTUDDOCREQD_M(Comm_ID, hfTemp.Value, Descr, ViewState("Etype"), bEdit, transaction)

                        End If

                        If Status = -1 Then

                            Throw New ArgumentException("Record already exist")
                        ElseIf Status <> 0 Then

                            Throw New ArgumentException("Error while inserting new record")

                        Else
                            'Store the required information into the Audit Trial table when inserted

                            Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "insert", Page.User.Identity.Name.ToString, Me.Page)


                            If Status <> 0 Then

                                Throw New ArgumentException("Could not complete your request")

                            End If

                            ViewState("datamode") = "none"
                            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                            transaction.Commit()
                            Call clearMe()
                            lblError.Text = "Record Inserted Successfully"

                        End If

                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()

                        lblError.Text = "Record could not be Inserted"

                    End Try

                End Using
                Call backGround()
            End If

        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Call clearMe()
            Call backGround()
            ViewState("datamode") = "add"
            txtDescr.Attributes.Remove("readonly")

            If ViewState("Etype") = "DT" Then

                txtCode.Attributes.Remove("Readonly")
                btnAcademic.Visible = True
            End If
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog("studComm_M", ex.Message)
        End Try
    End Sub

End Class
