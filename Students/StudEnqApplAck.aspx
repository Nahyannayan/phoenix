<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudEnqApplAck.aspx.vb" Inherits="Students_StudEnqApplAck" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 
    <title>Online Enquiry Acknowledgement</title>
     </head>
<body style="margin-top:-1px;font-size:12px;font-family:Verdana; ">
    <form id="form1" runat="server">


    <div>
        <div>
            <div align="center">
            <table style="border-collapse: collapse" width="700px">
            <tr>
            <td  valign="top" style="margin:0px;padding:0px;">
            <table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td align="left"><a href="#" target="_blank" title="Print Online Enquiry Acknowledgement"
                                runat="server" id="hrefAck" ><img id="printAck" alt="Print Online Enquiry Acknowledgement" src="../IMAGES/PrintAck.png" style="border-style:none;"/></a>
            

         
            <div id="spTime" runat="server" style="font-size:14px"></div>
            </td>
            <td style="color:#03578d;font-size:20px;" align="center"> Online Enquiry Acknowledgement</td>
            <td align="right">   <asp:Image ID="imglogo" runat="server" AlternateText="Logo"  />
            </td></tr></table>
            
            </td>
            
            </tr>
            
            <tr>
            
            <td style="border-bottom:solid 1px #003a63;">

            </td>
            </tr>
            <tr>
            <td  align="left" style="margin:0px;padding:0px;">
                                       <table align="left" width="100%" cellpadding="2" cellspacing="0">
                            <tr style="padding-top:2px;">
                            <td style="font-size:18px;">Enquiry Number  :
                                <asp:Label ID="lblEnqNo" runat="server" ></asp:Label></td>
                            <td align="right">                           
                                <asp:Image ID="imgBarCode" runat="server" />
                                                        </td>
                            </tr>
                            </table>
                    
            </td>
            
            </tr>
            
            <tr align="left">
            <td style="padding-top:4px;">
           <asp:Literal ID="ltHeadmain" runat="server"></asp:Literal>
            
            </td>
            </tr>
                <tr align="left">
                    <td>&nbsp;<br />
                    </td>
                </tr>
            <tr>            
            <td align="left">
                        <table style="width: 699px;font-size:9pt;color:#3e3e3e;" border="0" 
             align="left" cellpadding="2" cellspacing="0">
                                               <tr bgcolor="#003a63">
                                                <td  colspan="2"  valign="middle" style="height: 22px;color:#fff;font-weight:bold;" >Student Details</td>
                                            </tr>
                                            <tr bgcolor="#f1f1f1" >
                                                <td style="padding-left:5px;width: 300px;height: 20px;">
                                                 Name</td>
                                                <td ><asp:Label ID="lblWardName" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td  style="padding-left:5px;height: 20px;"> Date of Birth</td>
                                                <td  ><asp:Label ID="lblDOB" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr bgcolor="#f1f1f1">
                                                <td style="padding-left:5px;height: 20px;"><asp:Literal ID="ltClass_lb" runat="server">
                                                </asp:Literal></td>
                                                <td ><asp:Label ID="lblClassYearGrade" runat="server"></asp:Label></td>
                                            </tr>
                                           <tr  >
                                                <td style="padding-left:5px;height: 20px;">
                                                Sibling Name and Student ID</td>
                                                <td ><asp:Label ID="lblSibName" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr bgcolor="#003a63">
                                         
                                              <td  colspan="2"  valign="middle" style="height: 22px;color:#fff;font-weight:bold;" >
                                                   Parent Details</td>
                                            </tr>
                                         <tr  bgcolor="#f1f1f1">
                                                <td  style="padding-left:5px;height: 20px;">Name</td>
                                                <td  ><asp:Label ID="lblParent" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td 
                                                style="padding-left:5px;height: 20px;" >
                                               City/State</td>
                                                <td ><asp:Label ID="lblCity" runat="server"></asp:Label></td>
                                            </tr>
                                           <tr  bgcolor="#f1f1f1">
                                                <td  style="padding-left:5px;height: 20px;">Country</td>
                                                <td ><asp:Label ID="lblCountry" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td  style="padding-left:5px;height: 20px;">
                                                Phone</td>
                                                <td ><asp:Label ID="lblPhone" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr  bgcolor="#f1f1f1">
                                                <td style="padding-left:5px;height: 20px;" >
                                                    Email</td>
                                                <td ><asp:Label ID="lblEmail" runat="server"></asp:Label></td>
                                            </tr>
            </table>
              </td>
            </tr>       
            <tr align="left">
            
            <td >
                                                    <asp:Panel ID="Panel1" runat="server" Height="100%"  Width="100%">
                                                       <asp:Literal ID="ltMain" runat="server"></asp:Literal>
                                                        <asp:DataList ID="dlSub" runat="server" >
                                                            <ItemTemplate>
                                                           <%# DataBinder.Eval(Container.DataItem, "EAD_TEXT") %>
                                                            </ItemTemplate>
                                                        </asp:DataList></asp:Panel>
                                                </td>
                
            
            </tr>
                            <tr align="left">
                                                <td  style="padding-top:4px; height: 3px;"> 
                                               &nbsp;
                                                  </td>
                                            </tr>
            <tr align="left">                                                <td >
                                               <asp:Literal ID="ltFooter" runat="server"></asp:Literal>
                                                  </td>
                                            </tr>
                                            
                                             
                                            
                                            
                                           <tr align="left" >
                                                    <td style="padding:0px;margin-bottom:0px;border-bottom-style:solid;border-width:1px;border-color:#003a63; ">
                                                    <div style="width:100%;float:left;display:inline; "> <asp:Literal ID="ltAcknow" runat="server"></asp:Literal>
                                                    <br /><br /> <b runat="server" 
                                                       id="bold1" style="color:#ff0000;"><asp:Literal ID="lt_Footer3" runat="server"></asp:Literal></b>
                                                    </div>
                                                    <div style="float:left;display:inline;padding-top:18px;font-size:11pt;">
                                                      Registrar<br />
                                                       <div id="spReg" runat="server" style="padding-top:12px;font-size:9pt;"> </div> 
                                                       <div style="float:left; padding-top:7px;font-size:9pt;">
                                                       <asp:Literal id="liRegMail" runat="server"  ></asp:Literal></div>
                                                    </div>   <asp:Literal ID="ltFooter2" runat="server"></asp:Literal>
                                                       
                                                         <img src="../Images/SWOOSH.jpg"  width="200px" height="200px"
                                                        alt="Swoosh Image"  style="float:right;"/>  
                                                      
                                                        </td>
                                                        </tr>
                <tr align="left" >
                    <td style="font-weight: bold;  padding-top: 2px; height: 10px" align="center">
                    <asp:Literal id="liAddr" runat="server"  ></asp:Literal>
                   
                    </td>
                </tr>
                                           <tr>
                                                <td id="18" style="display:block;" valign="bottom">
                                                <span style="clear: left;display: inline; float: left; visibility: visible;">
                                                 <asp:LinkButton ID="lbtnBack" runat="server" Font-Bold="True"  Font-Italic="True">Back to Home page</asp:LinkButton>
                                                 <asp:LinkButton ID="lbtnTran"  Visible="false" runat="server" Font-Bold="True"  Font-Italic="True">Apply for transport</asp:LinkButton></span>
                                                  <span style="clear: right;display: inline; float: right; visibility: visible" id="spAppl" runat="server"><asp:LinkButton ID="lbApplic" runat="server"  Font-Bold="True" Font-Italic="True">Click here</asp:LinkButton>&nbsp;
                                                    to apply for more applicants&nbsp;&nbsp;
                                                    <span style="display:none"> <asp:LinkButton ID="lbClickHere" runat="server"  Font-Bold="True" Font-Italic="True">Click here</asp:LinkButton>&nbsp;
                                                    to apply for more schools</span></span>
                                                    
                                                    
                                                 </td>
                                            </tr>
                                    
                          
                 
                    </table>
            
       
             
            </div>
        </div>
    
    </div>
    
       
<asp:HiddenField ID="hfFilePath" runat="server" />
<asp:HiddenField ID="hfFileName" runat="server" />
<asp:HiddenField ID="hfReg" runat="server" value=""/>
<asp:HiddenField ID="hfVid" runat="server" value=""/>
    </form>
</body>
</html>
