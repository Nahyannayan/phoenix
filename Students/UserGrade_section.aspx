<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="UserGrade_section.aspx.vb" Inherits="Grade_Section_Access" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function change_chk_state(src) {
            var chk_state = (src.checked);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type == 'checkbox') {
                    document.forms[0].elements[i].checked = chk_state;
                }
            }
        }
        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                //                //
                //                if  ( obj.title=='All' && obj.checked ){ //alert(obj.checked);
                //                change_chk_state(obj); }
                //                //
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }

        function client_OnTreeNodeChecked_grd() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }


    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-graduation mr-3"></i>
            Grade Section Access

        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="Table1" border="0" width="100%">
                    <tr>
                        <td align="left"></td>
                    </tr>
                </table>
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">

                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False" Font-Size="10px" ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">
                                <tr>
                                    <td class="title-bg" colspan="4">
                                        <asp:Literal ID="ltLabel" runat="server" Text="Set Access Rights"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Business Units</span>
                                    </td>

                                    <td align="left" colspan="3">
                                        <asp:DropDownList ID="ddlBsUnits" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBsUnits_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="clmid" runat="server">
                                    <td align="left" width="20%"><span class="field-label">Curriculum</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlCurri" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCurri_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>

                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Find User</span>
                                    </td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFind" runat="server">
                                        </asp:TextBox></td>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnFind" runat="server" CssClass="button" TabIndex="1" Text="Find"
                                            OnClick="btnFind_Click" /></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <table align="center" width="100%">
                                            <tr class="title-bg">
                                                <td align="center" width="50%">Users with Roles
                                                </td>
                                                <td align="center" width="50%">Grade &amp; Section
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="50%">
                                                    <div class="checkbox-list-full">
                                                        <asp:Panel ID="plUserRole" runat="server">
                                                            <asp:TreeView ID="tvUserRole" runat="server" onclick="client_OnTreeNodeChecked();" ExpandDepth="1"
                                                                ShowCheckBoxes="All">
                                                                <DataBindings>
                                                                    <asp:TreeNodeBinding DataMember="RoleItem" SelectAction="SelectExpand"
                                                                        TextField="Text" ValueField="ValueField"></asp:TreeNodeBinding>
                                                                </DataBindings>
                                                                <NodeStyle CssClass="treenode" />
                                                            </asp:TreeView>
                                                        </asp:Panel>
                                                    </div>
                                                </td>
                                                <td width="50%">
                                                    <div class="checkbox-list-full">
                                                        <asp:Panel ID="plGradeSection" runat="server">
                                                            <asp:TreeView ID="tvGRD_SCT" runat="server" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked_grd();" ExpandDepth="1">
                                                                <DataBindings>
                                                                    <asp:TreeNodeBinding DataMember="GradeItem" SelectAction="SelectExpand"
                                                                        TextField="Text" ValueField="ValueField"></asp:TreeNodeBinding>
                                                                </DataBindings>
                                                                <NodeStyle CssClass="treenode" />
                                                            </asp:TreeView>
                                                        </asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnAddNEW" runat="server" CssClass="button" TabIndex="1" Text="Add"
                                            OnClick="btnAddNEW_Click" />
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="button"
                                            Text="Update" OnClick="btnUpdate_Click" /></td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left" colspan="4">Add Details</td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="table-responsive m-auto">
            <table width="100%">
                <tr>
                    <td align="center" colspan="4">
                        <div align="left">

                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                        </div>
                        <asp:Panel ID="plGRD" runat="server" Width="100%">

                            <asp:GridView ID="gvUSR_SCT" runat="server" CssClass="table table-bordered table-row" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                Width="100%" DataKeyNames="SRNO" OnRowEditing="gvUSR_SCT_RowEditing" OnRowDeleting="gvUSR_SCT_RowDeleting" OnRowDataBound="gvUSR_SCT_RowDataBound">
                                <RowStyle CssClass="griditem" />
                                <EmptyDataRowStyle />
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSRNO" runat="server" Text='<%# Bind("SRNO") %>'></asp:Label>
                                            <asp:Image ID="imgstatus" runat="server"></asp:Image>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="GSA_USR_NAME" HeaderText="User Name" HtmlEncode="False">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GRD_SCT" HeaderText="Grade &amp; Section" HtmlEncode="False">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GSA_USR_ID" HeaderText="usr_id" Visible="False">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GSA_SCT_ID" HeaderText="SCT" Visible="False"></asp:BoundField>
                                    <asp:ButtonField CommandName="Edit" Text="Edit" HeaderText="Edit">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:ButtonField CommandName="delete" Text="Delete" HeaderText="Delete">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="GSA_ID" HeaderText="GSA_ID" Visible="False"></asp:BoundField>
                                    <asp:TemplateField HeaderText="status" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="griditem_hilight" />
                                <HeaderStyle CssClass="gridheader_new" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>


            </table>
        </div>
    </div>

    <div class="card-body">
        <div class="table-responsive m-auto">
            <table width="100%">
                <tr>
                    <td align="center">
                        <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                            OnClick="btnEdit_Click" Text="Add/Edit" />
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" OnClick="btnCancel_Click" /></td>
                </tr>

            </table>
        </div>
    </div>
</asp:Content>

