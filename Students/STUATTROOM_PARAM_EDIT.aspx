<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="STUATTROOM_PARAM_EDIT.aspx.vb" Inherits="Students_STUATTROOM_PARAM_EDIT" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Room Attendance Parameter Setting(s)"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                         ></asp:Label>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"  ForeColor="" ValidationGroup="List_valid"></asp:ValidationSummary>
                                    <span style="  color: #800000">&nbsp;</span>
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr style="color: #800000" valign="bottom">
                        <td align="center"  valign="middle">Fields Marked with (<span
                            class="text-danger font-small" >*</span>) are mandatory</td>
                    </tr>
                    <tr>
                        <td >
                            <table   width="100%">

                                <tr>
                                    <td align="left" width="20%" ><span class="field-label">Attendance Category <span style="color: #800000">*</span></span></td>
                                    <td    width="30%">
                                        <asp:DropDownList ID="ddlCat" runat="server">
                                        </asp:DropDownList></td>
                                    <td  width="20%"></td>
                                    <td  width="30%"></td>
                                </tr>
                                <tr class="title-bg">
                                    <td align="left" class="matters" colspan="4"  >Add room attendance parameter for the above category</td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Parameter<span style="color: #800000"> *</span></span></td>
                                    <td align="left"   >
                                        <asp:TextBox ID="txtParam" runat="server"   MaxLength="40"></asp:TextBox></td>
                               
                                    <td align="left"  ><span class="field-label">Display Character</span></td>
                                    <td align="left"   >
                                        <asp:TextBox ID="txtDispChar" runat="server" MaxLength="1"  >
                                        </asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="center"   colspan="4">&nbsp;
                            <asp:Button ID="btnAddPARAM" runat="server" CssClass="button"
                                Text="Add" ValidationGroup="List_valid" OnClick="btnAddPARAM_Click" /><asp:Button ID="btnUpdate" runat="server" CssClass="button"
                                    Text="Update" ValidationGroup="List_valid" OnClick="btnUpdate_Click" /><asp:Button ID="btnCancelPARAM" runat="server" CssClass="button"
                                        Text="Cancel" OnClick="btnCancelPARAM_Click" /></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4" valign="top">
                                        <asp:GridView ID="gvAtt_Param" runat="server" AutoGenerateColumns="False"  class="table table-bordered table-row"
                                            DataKeyNames="id" EmptyDataText="No attendance parameter added yet"  
                                            
                                            Width="100%" OnRowEditing="gvAtt_Param_RowEditing" OnRowDeleting="gvAtt_Param_RowDeleting">
                                            <RowStyle CssClass="griditem"   />
                                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Att. Category">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPM_DESCR" runat="server" Text='<%# Bind("APM_DESCR") %>' __designer:wfdid="w48"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Att. Parameter">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRAP_PARAM_DESCR" runat="server" Text='<%# Bind("RAP_PARAM_DESCR") %>' __designer:wfdid="w38"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Display Char">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" __designer:wfdid="w13"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRAP_DISP" runat="server" Text='<%# bind("RAP_DISP") %>' __designer:wfdid="w12"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="EditBtn" runat="server" CausesValidation="False" __designer:wfdid="w41" CommandArgument='<%# Eval("id") %>' CommandName="Edit">           Edit </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="DeleteBtn" runat="server" __designer:wfdid="w42" CommandArgument='<%# Eval("id") %>' CommandName="Delete">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Att_CAT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPM_ID" runat="server" Text='<%# bind("APM_ID") %>' __designer:wfdid="w34"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Att_PARA_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRAP_ID" runat="server" Text='<%# Bind("RAP_ID") %>' __designer:wfdid="w31"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STATUS" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTATUS" runat="server" Text='<%# Bind("STATUS") %>' __designer:wfdid="w15"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Khaki" />
                                            <HeaderStyle CssClass="gridheader_new"   />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters"   valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add/Edit" OnClick="btnAdd_Click" /><asp:Button ID="btnSave"
                                runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" OnClick="btnSave_Click" /><asp:Button
                                    ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfParamID" runat="server"></asp:HiddenField>
            </div>
        </div>
    </div>
</asp:Content>

