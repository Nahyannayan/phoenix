<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studListStudentsInfo.aspx.vb" Inherits="FEEAdjustmentView" Theme="General" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js">
    </script>
    <script language="javascript" type="text/javascript">
        function divIMG(pId, val, ctrl1, pImg) {
            var path;

            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }

            if (pId == 1) {
                document.getElementById("<%=getid("mnu_1_img") %>").src = path;
            }
            else if (pId == 2) {
                document.getElementById("<%=getid("mnu_2_img") %>").src = path;
            }
            else if (pId == 3) {
                document.getElementById("<%=getid("mnu_3_img") %>").src = path;
            }
            else if (pId == 4) {
                document.getElementById("<%=getid("mnu_4_img") %>").src = path;
            }
            else if (pId == 5) {
                document.getElementById("<%=getid("mnu_5_img") %>").src = path;
            }

    document.getElementById(ctrl1).value = val + '__' + path;
}


function switchViews(obj, row) {
    var div = document.getElementById(obj);
    var img = document.getElementById('img' + obj);

    if (div.style.display == "none") {
        div.style.display = "inline";
        if (row == 'alt') {
            img.src = "../Images/expand_button_white_alt_down.jpg";
        }
        else {
            img.src = "../Images/Expand_Button_white_Down.jpg";
        }
        img.alt = "Click to close";
    }
    else {
        div.style.display = "none";
        if (row == 'alt') {
            img.src = "../Images/Expand_button_white_alt.jpg";
        }
        else {
            img.src = "../Images/Expand_button_white.jpg";
        }
        img.alt = "Click to expand";
    }
}

    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Students/Enquiry Info..."></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <asp:Button ID="btnShow" runat="server" Style="display: none;" Text="show" />
                <ajaxToolkit:ModalPopupExtender ID="mdlRef" runat="server" TargetControlID="btnShow"
                    BehaviorID="mdlRef" PopupControlID="plref_tran" BackgroundCssClass="modalBackground" />
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">&nbsp;<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                </table>
                <a id='top'></a>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr >
                        <td align="left" colspan="2" >
                           </td>
                    </tr>--%>
                    <tr>
                        <td align="center">


                            <asp:GridView ID="gvStudentInfo" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                EmptyDataText="No Data Found" Width="100%" AllowPaging="True" PageSize="30">
                                <Columns>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:switchViews('div<%# Eval("STU_ID") %>_<%# Eval("STU_ID") %>', 'one');">
                                                <img id="imgdiv<%# Eval("STU_ID") %>_<%# Eval("STU_ID") %>" alt="Click to show/hide " border="0" src="../Images/expand_button_white.jpg" />
                                            </a>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <a href="javascript:switchViews('div<%# Eval("STU_ID") %>_<%# Eval("STU_ID") %>', 'alt');">
                                                <img id="img1" alt="Click to show/hide " border="0" src="../Images/expand_button_white_alt.jpg" />
                                            </a>
                                        </AlternatingItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_ID" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGender" runat="server" Text='<%# Bind("STU_GENDER") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDob" runat="server" Text='<%# Bind("STU_DOB") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblParentName" runat="server" Text='<%# Bind("PARENT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblParentMobile" runat="server" Text='<%# Bind("PARENT_MOBILE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblParentEmail" runat="server" Text='<%# Bind("PARENT_EMAIL")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTcEnqDate" runat="server" Text='<%# Bind("TC_ENQ_DATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTranStatus" runat="server" Text='<%# Bind("TRAN_STATUS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransportation" runat="server" Text='<%# Bind("TRANSPORTATION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBusNo" runat="server" Text='<%# Bind("BUS_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField Visible="False" HeaderText="STU_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLoc" runat="server" Text='<%# Bind("TRANS_LOC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="TYPE">
                                        <HeaderTemplate>
                                            Type<br />
                                            <asp:DropDownList ID="ddlStudType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStudType_SelectedIndexChanged">
                                                <asp:ListItem Selected="True" Value="'STUDENT','ENQUIRY'">ALL</asp:ListItem>
                                                <asp:ListItem Value="'STUDENT'">STUDENT</asp:ListItem>
                                                <asp:ListItem Value="'ENQUIRY'">ENQUIRY</asp:ListItem>
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudType" runat="server" Text='<%# Bind("STU_TYPE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_NO">
                                        <HeaderTemplate>
                                            No<br />
                                            <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDocNoSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>' Visible='<%# Bind("bREF_Not") %>'></asp:Label>
                                            <asp:LinkButton ID="lbtnStuNo" runat="server" Text='<%# Bind("STU_NO") %>'
                                                Visible='<%# Bind("bREF") %>' OnClick="lbtnStuNo_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_NAME">
                                        <HeaderTemplate>
                                            Name<br />
                                            <asp:TextBox ID="txtstudname" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnStuNameSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="lblPrint" runat="server" Text='<%# Bind("STU_NAME") %>' OnClientClick='<%# GetNavigateUrl(Eval("STU_NO").tostring(),Eval("STU_ID").tostring()) %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GRADE">
                                        <HeaderTemplate>
                                            Grade<br />
                                            <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnDateSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRD_SCT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACD_YEAR">
                                        <HeaderTemplate>
                                            Academic Year<br />
                                            <asp:TextBox ID="txtAcademicYear" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAcademicYearSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>
                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblACD_YEAR" runat="server" Text='<%# Bind("ACY_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STATUS">
                                        <HeaderTemplate>
                                            Status<br />
                                            <asp:TextBox ID="txtStatus" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnRemarksSearch" OnClick="ImageButton1_Click" runat="server" ImageUrl="../Images/forum_search.gif" ImageAlign="Middle"></asp:ImageButton>

                                        </HeaderTemplate>

                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("CURR_STATUS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            </td></tr>
            <tr>
                <td colspan="100%" align="left">
                    <div id="div<%# Eval("STU_ID") %>_<%# Eval("STU_ID") %>" style="display: none; position: relative; left: 20px;">
                        <asp:GridView ID="gvStudInfo" runat="server" Width="95%" CssClass="table table-bordered table-row"
                            AutoGenerateColumns="false" EmptyDataText="No Info available.">

                            <Columns>

                                <asp:BoundField DataField="FEE_ID" HeaderText="Fee ID" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>

                                <asp:BoundField DataField="STU_GENDER" HeaderText="Gender" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>

                                <asp:BoundField DataField="STU_DOB" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="D.O.B" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>

                                <asp:BoundField DataField="PARENT_NAME" HeaderText="Parent Name" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:BoundField>

                                <asp:BoundField DataField="PARENT_MOBILE" HeaderText="Parent Mobile" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="PARENT_EMAIL" HeaderText="Parent Email" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TC_ENQ_DATE" HeaderText="TC\Enq.Date" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TRAN_STATUS" HeaderText="Transport Status" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TRANSPORTATION" HeaderText="Transport" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="BUS_NO" HeaderText="Bus No" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TRANS_LOC" HeaderText="Area" HtmlEncode="False">
                                    <ItemStyle HorizontalAlign="center" />
                                </asp:BoundField>
                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <HeaderStyle />
                        </asp:GridView>
                </td>
            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <asp:Panel ID="plref_tran" runat="server" CssClass="panel-cover"
                    ScrollBars="None">
                    <div class="msg_header" style="width: 958px; margin-top: 1px; vertical-align: middle; background-color: White;">
                        <div class="title-bg">
                            Medical Updates <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: -4px; vertical-align: top;">
                                <asp:ImageButton ID="imgclose" runat="server" ImageUrl="../Images/closeme.png"
                                    AlternateText="Close" />
                            </span>
                        </div>
                    </div>
                  <%--  <iframe src="../Refer/referral_Link.aspx" frameborder="0" style="margin: 0px; padding: 0px;"
                        scrolling="no" width="748" height="475" id="ifAlert" runat="server" />--%>

                      <iframe src="stuMedicalinfoquick.aspx" frameborder="0" style="margin: 0px; padding: 0px;"
                        scrolling="no" width="958" height="475" id="ifAlert" runat="server" />

                </asp:Panel>

                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />


            </div>
        </div>
    </div>

    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
    </script>

</asp:Content>
