<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studCert_Comm_Merge_new.aspx.vb" Inherits="Students_studCert_Comm_merge_new" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Src="UserControl/studExportToExcel_Merge.ascx" TagName="studExportToExcel_Merge"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Export to Excel
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <uc1:studExportToExcel_Merge ID="StudExportToExcel_Merge1" runat="server" />
            </div>
        </div>
    </div>

</asp:Content>

