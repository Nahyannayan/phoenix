﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports ResponseHelper
Imports UtilityObj
Partial Class Students_studAtt_Rediker
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "add"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059115") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    Call bindAcademic_Year()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
        Dim ToolkitScriptManager1 As AjaxControlToolkit.ToolkitScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me.Page)
        ToolkitScriptManager1.RegisterPostBackControl(btnExport)
        ToolkitScriptManager1.RegisterPostBackControl(btnUpload)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_id in('" & Session("prev_ACD_ID").ToString & "','" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcdYear.Items.Clear()
            ddlAcdYear.DataSource = ds.Tables(0)
            ddlAcdYear.DataTextField = "ACY_DESCR"
            ddlAcdYear.DataValueField = "ACD_ID"
            ddlAcdYear.DataBind()
            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        CallReport()
    End Sub
    Sub CallReport()
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim lstrExportType As Integer




        Dim pParms(10) As SqlClient.SqlParameter


        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", ddlAcdYear.SelectedValue)


        pParms(2) = New SqlClient.SqlParameter("@STARTDATE", Format(Date.Parse(txtCDate.Text.Trim), "dd/MMM/yyyy"))
        pParms(3) = New SqlClient.SqlParameter("@ENDDATE", Format(Date.Parse(txtSDate.Text.Trim), "dd/MMM/yyyy"))


        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ATT_EXPORT_REDIKKER ", pParms)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim ws As ExcelWorksheet = ef.Worksheets.Add("OASIS_DATA_EXPORT_")
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})



            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)
            Response.Flush()
            Response.End()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If

    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Dim strFileName As String
        If flUpExcel.HasFile Then
            strFileName = flUpExcel.PostedFile.FileName
            Dim ext As String = Path.GetExtension(strFileName)
            If ((ext = ".xlsx") Or (ext = ".xls")) Then

                UpLoadDBF()


            Else
                lblerror_imp.Text = "Please upload .xls or .xlsx files only."
            End If

        Else
            lblerror_imp.Text = "File not uploaded"
        End If
    End Sub
    Private Sub UpLoadDBF()
        If flUpExcel.HasFile Then
            Dim FName As String = "Online" & Session("sUsr_name").ToString() & Date.Now.ToString("dd-MMM-yyyy ") & "-" & Date.Now.ToLongTimeString()
            FName += flUpExcel.FileName.ToString().Substring(flUpExcel.FileName.Length - 5)
            Dim filePath As String = WebConfigurationManager.ConnectionStrings("Documentpath").ConnectionString
            If Not Directory.Exists(filePath & "\OnlineExcel") Then
                Directory.CreateDirectory(filePath & "\OnlineExcel")
            End If
            Dim FolderPath As String = filePath & "\OnlineExcel\"
            filePath = filePath & "\OnlineExcel\" & FName.Replace(":", "@")

            If flUpExcel.HasFile Then
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
                flUpExcel.SaveAs(filePath)
                Try
                    getdataExcel(filePath)
                    File.Delete(filePath)
                Catch ex As Exception
                    Errorlog(ex.Message)
                    lblerror_imp.Text = getErrorMessage(OASISConstants.ERRORMSG_UNEXPECTED)
                End Try
            End If
        End If
    End Sub
    Public Sub getdataExcel(ByVal filePath As String)
        Try

            Dim CurBsUnit As String = Session("sBsuid")
            Dim xltable As DataTable
            'xltable = Mainclass.FetchFromExcel("Select * From [TableName]", filePath)
            xltable = Mainclass.FetchFromExcelIntoDataTable(filePath, 1, 1, 5)
            Dim xlRow As DataRow
            Dim ColName As String
            ColName = xltable.Columns(1).ColumnName
            For Each xlRow In xltable.Select(ColName & "='' or " & ColName & " is null or " & ColName & "='0'", "")
                xlRow.Delete()
            Next
            xltable.AcceptChanges()
            ColName = xltable.Columns(1).ColumnName
            Dim strEmpNos As String = ""
            For Each xlRow In xltable.Rows
                If xlRow(ColName).ToString <> "" Then
                    strEmpNos &= IIf(strEmpNos <> "", ",'", "'") & xlRow(ColName).ToString & "'"
                Else
                    lblerror_imp.Text = "Student No. cannot be blank."
                End If
            Next

            If strEmpNos = "" Then
                lblerror_imp.Text = "No Data to Import"
                Exit Sub
            End If

            Dim ValidStudent As New DataTable
            Dim ValidSession As New DataTable
            Dim validecode As New DataTable
            Dim validALG As New DataTable
            Dim validleave As New DataTable

            'Dim ValidEligibility As New DataTable
            Dim mTable As New DataTable
            mTable = CreateDataTable()
            Dim rowId As Integer = 0

            For Each xlRow In xltable.Rows
                Dim mRow As DataRow

                Dim grd_id As String, acd_id As Integer, sct_id As Integer, dte As Date, session As String, tme As String, strdte As String


                ' Dim APPLEAVE As New DataColumn("APPLEAVE", System.Type.GetType("System.String"))
                ' Dim MinList As New DataColumn("MinList", System.Type.GetType("System.String"))


                ValidStudent = Mainclass.getDataTable("select stu_no,stu_id,stu_name,stu_grd_id,stu_sct_id,stu_acd_id,stu_gender from VW_OSO_STUDENT_M where stu_no in (" & strEmpNos & ") and stu_bsu_id='" & CurBsUnit & "' ", ConnectionManger.GetOASISConnectionString)
                mRow = mTable.NewRow
                mRow("SRNO") = xlRow(0)

                mRow("STU_NO") = xlRow(1).ToString
                Dim dt_str As String = Convert.ToString(xlRow(3))
                strdte = dt_str.Substring(3, 2) & "/" & dt_str.Substring(0, 2) & "/" & dt_str.Substring(6, 4)

                dte = xlRow(3).ToString 'strdte
                session = "Session1"
                tme = xlRow(5).ToString
                mRow("time") = xlRow(5).ToString
                Dim dateTime1 As String
                dateTime1 = Format(dte, "dd/MMM/yyyy") 'Date.ParseExact(dte, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                mRow("dat") = dateTime1
                If Not IsDate(dateTime1) Then

                    lblerror_imp.Text = "Attendance Date format is Invalid , Sl No. :" & mRow("SRNO")
                    Exit Sub
                End If

                If ValidStudent.Select("stu_no='" & xlRow(1) & "'", "").Length = 0 Then

                    lblerror_imp.Text = "Invalid Student No:" & mRow("STU_NO") & ", Sl No. :" & mRow("SRNO") & " for current Unit"
                    Exit Sub
                Else
                    Dim drEmp As DataRow
                    drEmp = ValidStudent.Select("stu_no='" & mRow("STU_NO") & "'", "")(0)
                    If ValidStudent.Rows.Count > 0 Then
                        mRow("STU_ID") = drEmp("STU_ID")
                        mRow("STUDNAME") = drEmp("stu_name")
                        mRow("SGENDER") = drEmp("stu_gender")
                        grd_id = drEmp("stu_grd_id")
                        acd_id = drEmp("stu_acd_id")
                        sct_id = drEmp("stu_sct_id")
                    End If
                End If
                'acd_id = 1391
                mRow("grdid") = grd_id
                mRow("acdid") = acd_id
                mRow("sctid") = sct_id


                validALG = Mainclass.getDataTable(" SELECT ALG_GRD_ID,alg_id FROM ATTENDANCE_LOG_GRADE WHERE ALG_ACD_ID=" & acd_id & " AND ALG_GRD_ID='" & grd_id & "' AND ALG_SCT_ID=" & sct_id & " AND ALG_ATTDT='" & dateTime1 & "'", ConnectionManger.GetOASISConnectionString)
                If validALG.Rows.Count <= 0 Then
                    mRow("ALG_ID") = "0"
                Else
                    Dim drALG As DataRow
                    drALG = validALG.Select("ALG_GRD_ID='" & mRow("grdid") & "'", "")(0)
                    If validALG.Rows.Count > 0 Then
                        mRow("ALG_ID") = drALG("alg_id")


                    End If
                End If



                ValidSession = Mainclass.getDataTable(" SELECT distinct [ATT_TYPE]  FROM [ATTENDANCE_M] where  ATT_TYPE='" & session & "' and [ATT_ACD_ID]='" & acd_id & "' AND ATT_GRD_ID='" & grd_id & "' AND '" & dateTime1 & "' BETWEEN ATT_FROMDT AND ATT_TODT", ConnectionManger.GetOASISConnectionString)
                If ValidSession.Rows.Count <= 0 Then

                    lblerror_imp.Text = "Incorrect Session : Sl No. " & mRow("SRNO")
                    Exit Sub
                End If

                validecode = Mainclass.getDataTable(" SELECT ARP_ACD_ID,ARP_APD_ID FROM dbo.ATTENDANCE_REPORT_PARAM WHERE  ARP_ACD_ID=" & acd_id & " AND arp_ext_disp='" & xlRow(4).ToString & "'", ConnectionManger.GetOASISConnectionString)
                If validecode.Rows.Count <= 0 Then

                    lblerror_imp.Text = "Incorrect Att Code : Sl No. " & mRow("SRNO")
                    Exit Sub
                Else
                    Dim drCode As DataRow
                    drCode = validecode.Select("ARP_ACD_ID='" & mRow("acdid") & "'", "")(0)
                    If validecode.Rows.Count > 0 Then
                        mRow("SLA_APD_ID") = drCode("ARP_APD_ID")


                    End If
                End If

                validleave = Mainclass.getDataTable("SELECT SLA_GRD_ID,SLA_APPRLEAVE FROM STUDENT_LEAVE_APPROVAL AS S  WITH(NOLOCK) WHERE S.SLA_GRD_ID='" & grd_id & "' and   S.SLA_ACD_ID=" & acd_id & " and '" & dateTime1 & "' BETWEEN S.SLA_FROMDT AND S.SLA_TODT AND  (S.SLA_APPRLEAVE='APPROVED') AND S.SLA_STU_ID=" & mRow("STU_ID"), ConnectionManger.GetOASISConnectionString)
                If validleave.Rows.Count <= 0 Then

                    mRow("APPLEAVE") = ""
                Else
                    Dim drleave As DataRow
                    drleave = validleave.Select("SLA_GRD_ID='" & mRow("grdid") & "'", "")(0)
                    If validleave.Rows.Count > 0 Then
                        mRow("APPLEAVE") = drleave("SLA_APPRLEAVE")

                    End If
                End If

                mRow("STATUS") = ""


                rowId = rowId + 1
                mTable.Rows.Add(mRow)

            Next
            mTable.AcceptChanges()




            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty
            str_err = calltransaction(errorMessage, mTable)
            If str_err = "0" Then
                lblerror_imp.Text = "Record Saved Successfully"

            Else
                lblerror_imp.Text = errorMessage

            End If



        Catch ex As Exception
            lblerror_imp.Text = ex.Message
        End Try
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim ALG_ID As New DataColumn("ALG_ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STUDNAME As New DataColumn("STUDNAME", System.Type.GetType("System.String"))
            'Dim PRESENT As New DataColumn("PRESENT", System.Type.GetType("System.Boolean"))
            'Dim LATE As New DataColumn("LATE", System.Type.GetType("System.Boolean"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            'Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim SGENDER As New DataColumn("SGENDER", System.Type.GetType("System.String"))
            Dim APPLEAVE As New DataColumn("APPLEAVE", System.Type.GetType("System.String"))
            Dim time As New DataColumn("time", System.Type.GetType("System.String"))
            Dim SLA_APD_ID As New DataColumn("SLA_APD_ID", System.Type.GetType("System.String"))

            Dim dat As New DataColumn("dat", System.Type.GetType("System.String"))
            Dim acdid As New DataColumn("acdid", System.Type.GetType("System.String"))
            Dim grdid As New DataColumn("grdid", System.Type.GetType("System.String"))
            Dim sctid As New DataColumn("sctid", System.Type.GetType("System.String"))
            'Dim DAY5 As New DataColumn("DAY5", System.Type.GetType("System.String"))


            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(ALG_ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NO)
            dtDt.Columns.Add(STUDNAME)
            'dtDt.Columns.Add(PRESENT)
            'dtDt.Columns.Add(LATE)
            dtDt.Columns.Add(STATUS)
            ' dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(SGENDER)
            dtDt.Columns.Add(APPLEAVE)
            dtDt.Columns.Add(time)
            dtDt.Columns.Add(SLA_APD_ID)
            dtDt.Columns.Add(dat)
            dtDt.Columns.Add(acdid)
            dtDt.Columns.Add(grdid)
            dtDt.Columns.Add(sctid)
            'dtDt.Columns.Add(DAY5)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function
    Private Function checkduplicate(ByVal ALG_ACD_ID As String, ByVal ALG_GRD_ID As String, ByVal ALG_SCT_ID As String, _
ByVal ALG_ATT_TYPE As String, ByVal ALG_ATTDT As String) As Boolean
        Dim status As Boolean = False

        Dim conn As String = ConnectionManger.GetOASISConnectionString

        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@ALG_ACD_ID", ALG_ACD_ID)
        param(1) = New SqlParameter("@ALG_GRD_ID", ALG_GRD_ID)
        param(2) = New SqlParameter("@ALG_SCT_ID", ALG_SCT_ID)
        param(3) = New SqlParameter("@ALG_ATT_TYPE", ALG_ATT_TYPE)
        param(4) = New SqlParameter("@ALG_ATTDT", ALG_ATTDT)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ATT.CHECKDUPLICATE_ATT", param)
            If datareader.HasRows = True Then
                While datareader.Read
                    status = Convert.ToBoolean(datareader("ISEXITS"))
                    ViewState("DUP_ALG_ID") = (datareader("ALG_ID"))
                End While

            End If
        End Using
        checkduplicate = status

    End Function
    Private Sub AUDIT_ATTENDANCE(ByVal USR_ID As String, ByVal ALG_ID As String, _
ByVal ACTION As String, ByVal XML_DATA As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ClientBrs As String = String.Empty
            Dim ClientIP As String = HttpContext.Current.Request.UserHostAddress()
            With HttpContext.Current.Request.Browser
                ClientBrs &= "Browser Capabilities" & vbCrLf
                ClientBrs &= "Type = " & .Type & vbCrLf
                ClientBrs &= "Name = " & .Browser & vbCrLf
                ClientBrs &= "Version = " & .Version & vbCrLf
                ClientBrs &= "Major Version = " & .MajorVersion & vbCrLf
                ClientBrs &= "Minor Version = " & .MinorVersion & vbCrLf
                ClientBrs &= "Platform = " & .Platform & vbCrLf
                ClientBrs &= "Is Beta = " & .Beta & vbCrLf
                ClientBrs &= "Is Crawler = " & .Crawler & vbCrLf
                ClientBrs &= "Is AOL = " & .AOL & vbCrLf
                ClientBrs &= "Is Win16 = " & .Win16 & vbCrLf
                ClientBrs &= "Is Win32 = " & .Win32 & vbCrLf
                ClientBrs &= "Supports Frames = " & .Frames & vbCrLf
                ClientBrs &= "Supports Tables = " & .Tables & vbCrLf
                ClientBrs &= "Supports Cookies = " & .Cookies & vbCrLf
                ClientBrs &= "Supports VBScript = " & .VBScript & vbCrLf
                ClientBrs &= "Supports JavaScript = " & _
                    .EcmaScriptVersion.ToString() & vbCrLf
                ClientBrs &= "Supports Java Applets = " & .JavaApplets & vbCrLf
                ClientBrs &= "Supports ActiveX Controls = " & .ActiveXControls & _
                    vbCrLf
                ClientBrs &= "Supports JavaScript Version = " & _
                .JScriptVersion.ToString & vbCrLf
            End With
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@SAA_USR_ID", USR_ID)
            pParms(1) = New SqlClient.SqlParameter("@SAA_ALG_ID", ALG_ID)
            pParms(2) = New SqlClient.SqlParameter("@SAA_ACTION", ACTION)
            pParms(3) = New SqlClient.SqlParameter("@SAA_XML_DATA", XML_DATA)
            pParms(4) = New SqlClient.SqlParameter("@SAA_BROWSER_INFO", ClientBrs)
            pParms(5) = New SqlClient.SqlParameter("@SAA_IP_ADDRESS", ClientIP)


            SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "[ATT].[SAVESTUDENT_ATT_AUDIT]", pParms)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "AUDIT_ATTENDANCE")
        End Try
    End Sub
    Function calltransaction(ByRef errorMessage As String, dtt As DataTable) As Integer
        Dim bEdit As Boolean
        Dim ALG_ID As String
        Dim ALG_CREATEDDT As String = String.Empty
        Dim ALG_CREATEDTM As String = String.Empty
        Dim ALG_MODIFIEDDT As String = String.Empty
        Dim ALG_MODIFIEDTM As String = String.Empty
        Dim XML_DATA As New StringBuilder
        Dim XML_STRING As String = String.Empty

        Dim ALG_ATTDT As String
        'If ViewState("ATT_OPEN") = True Then
        '    ALG_ATTDT = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
        'Else
        '    If ddlAttDate.SelectedIndex = -1 Then
        '        ALG_ATTDT = ""
        '    Else
        '        ALG_ATTDT = String.Format("{0:" & OASISConstants.DateFormat & "}", ddlAttDate.SelectedItem.Text)
        '    End If
        'End If
        Dim ALG_EMP_ID As String = Session("EmployeeId")
        Dim ALG_BSU_ID As String = Session("sBsuid")
        Dim ALG_ACD_ID As String = "" 'ddlAcdYear.SelectedItem.Value
        Dim ALG_GRD_ID As String = "" ''ddlGrade.SelectedItem.Value
        Dim ALG_SCT_ID As String = "" ''ddlSection.SelectedItem.Value
        Dim ALG_SHF_ID As String = "" 'ddlShift.SelectedItem.Value
        Dim ALG_STM_ID As String = "" 'ddlStream.SelectedItem.Value
        Dim ALG_ATT_TYPE As String = "" ''ddlAttType.Text
        Dim ddlStatus As Integer
        Dim ALS_STU_ID As String = String.Empty
        'Dim ALS_STATUS As String = String.Empty
        Dim ALS_APD_ID As String = String.Empty
        Dim ALS_REMARKS As String = String.Empty
        Dim New_ALG_ID As String = String.Empty
        Dim AppLeave As String = String.Empty

        Dim transaction As SqlTransaction





        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer

                bEdit = False
                For i As Integer = 0 To dtt.Rows.Count - 1
                    Dim row As DataRow = dtt.Rows(i)

                    ALG_CREATEDDT = row.Item("dat")
                    ALG_CREATEDTM = row.Item("time")
                    ALG_ATTDT = row.Item("dat")
                    ALG_ACD_ID = row.Item("acdid")
                    ALG_GRD_ID = row.Item("grdid")
                    ALG_SCT_ID = row.Item("sctid")
                    ALG_ATT_TYPE = "Session1"

                    If (checkduplicate(ALG_ACD_ID, ALG_GRD_ID, ALG_SCT_ID, ALG_ATT_TYPE, ALG_ATTDT) = True) Then
                        bEdit = True
                    Else
                        bEdit = False
                    End If

                    If bEdit = False Then

                        status = AccessStudentClass.SaveAttendance_Log_Grade(ALG_ID, ALG_CREATEDDT, ALG_CREATEDTM, ALG_MODIFIEDDT, ALG_MODIFIEDTM, ALG_ATTDT, _
                                         ALG_EMP_ID, ALG_BSU_ID, ALG_ACD_ID, ALG_GRD_ID, ALG_SCT_ID, ALG_SHF_ID, ALG_STM_ID, ALG_ATT_TYPE, New_ALG_ID, bEdit, transaction)
                        If status <> 0 Then
                            calltransaction = "1"
                            errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                            Return "1"
                        Else
                            'the returned value
                            ALG_ID = New_ALG_ID
                            Dim sqlstring As String
                            sqlstring = "DELETE ATTENDANCE_LOG_STUDENT WHERE ALS_ALG_ID='" & ALG_ID & "'"
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sqlstring)





                            'ddlStatus = ""
                            ALS_STU_ID = row.Item("STU_ID")
                            ALS_REMARKS = "bulk update"
                            AppLeave = row.Item("APPLEAVE")


                            If (AppLeave <> "APPROVED") Then
                                ALS_APD_ID = row.Item("SLA_APD_ID")

                                XML_DATA.Append(String.Format("<STU_ID='{0}' ATT_PARAM='{1}' APP_LEAVE ='{2}'" & _
                                                    " ATT_REMARKS='{3}' />", ALS_STU_ID, _
ddlStatus, AppLeave, ALS_REMARKS.ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")))



                                status = AccessStudentClass.SaveAttendance_Log_Student(ALG_ID, ALS_STU_ID, ALS_APD_ID, ALS_REMARKS, transaction)
                                If status <> 0 Then
                                    calltransaction = "1"
                                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                                    Return "1"
                                End If

                            End If
                        End If
                    Else
                        'rajesh

                        ALG_MODIFIEDDT = row.Item("dat")
                        ALG_MODIFIEDTM = row.Item("time")
                        ALG_ID = row.Item("ALG_ID")
                        If ALG_ID = "0" Then
                            ALG_ID = ViewState("DUP_ALG_ID")
                        End If
                        status = AccessStudentClass.SaveAttendance_Log_Grade(ALG_ID, ALG_CREATEDDT, ALG_CREATEDTM, ALG_MODIFIEDDT, ALG_MODIFIEDTM, ALG_ATTDT, _
                                         ALG_EMP_ID, ALG_BSU_ID, ALG_ACD_ID, ALG_GRD_ID, ALG_SCT_ID, ALG_SHF_ID, ALG_STM_ID, ALG_ATT_TYPE, New_ALG_ID, bEdit, transaction)
                        If status <> 0 Then
                            calltransaction = "1"
                            errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                            Return "1"
                        Else
                            'the returned value
                            ALG_ID = row.Item("ALG_ID")
                            If ALG_ID = "0" Then
                                ALG_ID = ViewState("DUP_ALG_ID")
                            End If
                            Dim sqlstring As String
                            sqlstring = "DELETE ATTENDANCE_LOG_STUDENT WHERE ALS_ALG_ID='" & ALG_ID & "'"
                            SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, sqlstring)
                            ' ddlStatus = ""
                            ALS_STU_ID = row.Item("STU_ID")
                            ALS_REMARKS = "bulk update"
                            AppLeave = row.Item("APPLEAVE")



                            If (AppLeave <> "APPROVED") Then
                                ALS_APD_ID = row.Item("SLA_APD_ID")

                                XML_DATA.Append(String.Format("<STU_ID='{0}' ATT_PARAM='{1}' APP_LEAVE ='{2}'" & _
                                                    " ATT_REMARKS='{3}' />", ALS_STU_ID, _
ddlStatus, AppLeave, ALS_REMARKS.ToString.Replace("""", "quot;").Replace("'", "apos;").Replace("<", "lt;").Replace(">", "gt;").Replace("&", "amp;")))



                                status = AccessStudentClass.SaveAttendance_Log_Student(ALG_ID, ALS_STU_ID, ALS_APD_ID, ALS_REMARKS, transaction)
                                If status <> 0 Then
                                    calltransaction = "1"
                                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                                    Return "1"
                                End If

                            End If
                        End If



                    End If

                Next



                'NEW AUDIT CODE ADDED FOR ATTENDANCE
                If XML_DATA.ToString <> "" Then
                    XML_STRING = "<ATT>" & XML_DATA.ToString & "</ATT>"
                End If



                ViewState("datamode") = "view"
                Dim dt As DataTable = CreateDataTable()
                Dim dr As DataRow
                dr = dt.NewRow
                dt.Rows.Add(dr)


                'NEW AUDIT CODE ADDED FOR ATTENDANCE
                AUDIT_ATTENDANCE(Session("sUsr_id"), ALG_ID, "ADD", XML_STRING)




            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving." & ALS_STU_ID
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function
End Class
