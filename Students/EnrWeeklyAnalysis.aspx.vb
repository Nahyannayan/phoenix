﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports GemBox.Spreadsheet
Imports System.IO


Partial Class Students_EnrWeeklyAnalysis
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then

            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            If Request.QueryString("datamode") IsNot Nothing AndAlso Request.QueryString("MainMnu_code") IsNot Nothing Then
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            Else
              
                        Response.Redirect("~\noAccess.aspx")


            End If
           
            chkSelectBsu.Visible = False
            Try
                If (Convert.ToString(Session("sBsuid") = "999998")) Then
                    trCurriculum.Visible = True
                    trReportType.Visible = True
                    trbsu.Visible = True
                Else
                    trCurriculum.Visible = False
                    trReportType.Visible = False
                    trbsu.Visible = False
                End If

                txtDate.Text = Format(Now.Date, "dd/MMM/yyyy")
            Catch ex As Exception

            End Try
        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnGenerateReport)
    End Sub
    Protected Sub ddlCurriculum_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurriculum.SelectedIndexChanged
        bindBusinessunit()
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        If ddlReportType.SelectedItem.Value = "2" Then
            btnGenerateGrid.Visible = False
        Else
            btnGenerateGrid.Visible = True

        End If
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Try
            DownloadEnrollmentExcel()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnGenerateGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateGrid.Click
        Try
            BindGrid()
        Catch ex As Exception

        End Try

    End Sub

   

    Protected Sub gvEnroll_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvEnroll.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
    Private Sub bindBusinessunit()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(5) As SqlClient.SqlParameter
            Dim ds As DataSet
            param(0) = New SqlClient.SqlParameter("@CurriculumType", ddlCurriculum.SelectedItem.Value)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GetBsuForWeeklyEnroll", param)
            If ds.Tables(0).Rows.Count > 0 Then
                lstBsu.DataTextField = "BSU_NAME"
                lstBsu.DataValueField = "BSU_ID"
                lstBsu.DataSource = ds.Tables(0)
                lstBsu.DataBind()
                '' lstBsu.Items.Insert(0, New ListItem("All", "All"))
                chkSelectBsu.Visible = True
            Else
                chkSelectBsu.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub DownloadEnrollmentExcel()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(5) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable

            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile
            Dim ws As ExcelWorksheet
            Dim strBsu As String = String.Empty
            strBsu = "<IDS>"
            If (Convert.ToString(Session("sBsuid") = "999998")) Then


                With lstBsu
                    For i = 0 To .Items.Count - 1
                        If .Items(i).Selected = True Then
                            If .Items(i).Value <> "All" Then
                                strBsu += "<ID><BSU_ID>" + .Items(i).Value + "</BSU_ID></ID>"
                            End If

                        End If
                    Next
                End With
            Else
                strBsu += "<ID><BSU_ID>" + Convert.ToString(Session("sBsuid")) + "</BSU_ID></ID>"
            End If
            strBsu = strBsu + "</IDS>"
            param(0) = New SqlClient.SqlParameter("@BSU_XML", strBsu)
            If (Convert.ToString(Session("sBsuid") = "999998")) Then
                param(1) = New SqlClient.SqlParameter("@RPT_TYPE", ddlReportType.SelectedItem.Text)
            Else
                param(1) = New SqlClient.SqlParameter("@RPT_TYPE", "")
            End If
            If (Convert.ToString(Session("sBsuid") = "999998")) Then
                param(2) = New SqlClient.SqlParameter("@SCHOOL_TYPE", ddlCurriculum.SelectedItem.Text)
            Else
                param(2) = New SqlClient.SqlParameter("@SCHOOL_TYPE", "")
            End If
            param(3) = New SqlClient.SqlParameter("@ASON_DATE", txtDate.Text)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[QRY].[WEEKLY_ANALYSIS]", param)
            If ds.Tables(0).Rows.Count = 0 Then
            Else
                DT = ds.Tables(0)

                ''splitting datattable - group by bsu code
                Dim dict11 = DT.AsEnumerable().GroupBy(Function(r) r.Field(Of String)("BSU")).ToDictionary(Function(g) g.Key, Function(g) g.CopyToDataTable()).ToList
                Dim pathSave As String
                Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
                pathSave = "ENRWEEKLY" + "_" + Date.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"

                Dim path = cvVirtualPath & "\StaffExport\" + pathSave

                If ddlReportType.SelectedItem.Text = "Consolidated" Then
                    ws = ef.Worksheets.Add("Consolidated")
                    ws.InsertDataTable(DT, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

                    ws.Cells(0, 0).Value = "SCHOOL CODE"
                    ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("A").Width = 7000

                    ws.Cells(0, 1).Value = "SCHOOL NAME"
                    ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("B").Width = 10000

                    ws.Cells(0, 2).Value = "CURRICULUM"
                    ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("C").Width = 7000

                    ws.Cells(0, 3).Value = "ACADEMIC YEAR"
                    ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("D").Width = 7000

                    ws.Cells(0, 4).Value = "GRADE"
                    ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("E").Width = 7000

                    ws.Cells(0, 5).Value = "DATE"
                    ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("F").Width = 7000

                    ws.Cells(0, 6).Value = "CLASS"
                    ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("G").Width = 7000

                    ws.Cells(0, 7).Value = "Student Capacity/class"
                    ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("H").Width = 7000

                    ws.Cells(0, 8).Value = "Student Budget Sep 2016"
                    ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("I").Width = 7000

                    ws.Cells(0, 9).Value = "Current Enrolments"
                    ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("J").Width = 7000

                    ws.Cells(0, 10).Value = "OPEN Seats"
                    ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("K").Width = 7000

                    ws.Cells(0, 11).Value = "Future Dated Admission"
                    ws.Cells(0, 11).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 11).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("L").Width = 7000

                    ws.Cells(0, 12).Value = "Offers Pending"
                    ws.Cells(0, 12).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 12).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("M").Width = 7000

                    ws.Cells(0, 13).Value = "Waitlisted"
                    ws.Cells(0, 13).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 13).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("N").Width = 7000

                    ws.Cells(0, 14).Value = "Assessments"
                    ws.Cells(0, 14).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 14).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("O").Width = 7000

                    ws.Cells(0, 15).Value = "Registrations"
                    ws.Cells(0, 15).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 15).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("P").Width = 7000

                    ws.Cells(0, 16).Value = "Enquiries"
                    ws.Cells(0, 16).Style.Font.Color = Drawing.Color.White
                    ws.Cells(0, 16).Style.FillPattern.SetSolid(Drawing.Color.Black)
                    ws.Columns("Q").Width = 7000
                Else
                    For Each pair As KeyValuePair(Of String, DataTable) In dict11
                        Dim key As String = pair.Key
                        Dim value As DataTable = pair.Value

                        ws = ef.Worksheets.Add(pair.Key)
                        ws.InsertDataTable(value, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})

                        ws.Cells(0, 0).Value = "SCHOOL CODE"
                        ws.Cells(0, 0).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 0).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("A").Width = 7000

                        ws.Cells(0, 1).Value = "SCHOOL NAME"
                        ws.Cells(0, 1).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 1).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("B").Width = 10000

                        ws.Cells(0, 2).Value = "CURRICULUM"
                        ws.Cells(0, 2).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 2).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("C").Width = 7000

                        ws.Cells(0, 3).Value = "ACADEMIC YEAR"
                        ws.Cells(0, 3).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 3).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("D").Width = 7000

                        ws.Cells(0, 4).Value = "GRADE"
                        ws.Cells(0, 4).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 4).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("E").Width = 7000

                        ws.Cells(0, 5).Value = "DATE"
                        ws.Cells(0, 5).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 5).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("F").Width = 7000

                        ws.Cells(0, 6).Value = "CLASS"
                        ws.Cells(0, 6).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 6).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("G").Width = 7000

                        ws.Cells(0, 7).Value = "Student Capacity/class"
                        ws.Cells(0, 7).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 7).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("H").Width = 7000

                        ws.Cells(0, 8).Value = "Student Budget Sep 2016"
                        ws.Cells(0, 8).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 8).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("I").Width = 7000

                        ws.Cells(0, 9).Value = "Current Enrolments"
                        ws.Cells(0, 9).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 9).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("J").Width = 7000

                        ws.Cells(0, 10).Value = "OPEN Seats"
                        ws.Cells(0, 10).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 10).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("K").Width = 7000

                        ws.Cells(0, 11).Value = "Future Dated Admission"
                        ws.Cells(0, 11).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 11).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("L").Width = 7000

                        ws.Cells(0, 12).Value = "Offers Pending"
                        ws.Cells(0, 12).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 12).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("M").Width = 7000

                        ws.Cells(0, 13).Value = "Waitlisted"
                        ws.Cells(0, 13).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 13).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("N").Width = 7000

                        ws.Cells(0, 14).Value = "Assessments"
                        ws.Cells(0, 14).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 14).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("O").Width = 7000

                        ws.Cells(0, 15).Value = "Registrations"
                        ws.Cells(0, 15).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 15).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("P").Width = 7000

                        ws.Cells(0, 16).Value = "Enquiries"
                        ws.Cells(0, 16).Style.Font.Color = Drawing.Color.White
                        ws.Cells(0, 16).Style.FillPattern.SetSolid(Drawing.Color.Black)
                        ws.Columns("Q").Width = 7000

                    Next
                End If
               

                If ef.Worksheets.Count > 0 Then
                    ef.Save(cvVirtualPath & "StaffExport\" + pathSave)

                    Dim bytes() As Byte = File.ReadAllBytes(path)
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/octect-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()

                    System.IO.File.Delete(path)
                End If

            End If
        Catch ex As Exception
            Dim msg As String = ex.Message
        End Try

    End Sub

    Private Sub BindGrid()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(5) As SqlClient.SqlParameter
            Dim ds As DataSet
            Dim DT As DataTable

           
            Dim strBsu As String = String.Empty
            strBsu = "<IDS>"
            If (Convert.ToString(Session("sBsuid") = "999998")) Then


                With lstBsu
                    For i = 0 To .Items.Count - 1
                        If .Items(i).Selected = True Then
                            If .Items(i).Value <> "All" Then
                                strBsu += "<ID><BSU_ID>" + .Items(i).Value + "</BSU_ID></ID>"
                            End If

                        End If
                    Next
                End With
            Else
                strBsu += "<ID><BSU_ID>" + Convert.ToString(Session("sBsuid")) + "</BSU_ID></ID>"
            End If
            strBsu = strBsu + "</IDS>"
            param(0) = New SqlClient.SqlParameter("@BSU_XML", strBsu)
            If (Convert.ToString(Session("sBsuid") = "999998")) Then
                param(1) = New SqlClient.SqlParameter("@RPT_TYPE", ddlReportType.SelectedItem.Text)
            Else
                param(1) = New SqlClient.SqlParameter("@RPT_TYPE", "")
            End If
            If (Convert.ToString(Session("sBsuid") = "999998")) Then
                param(2) = New SqlClient.SqlParameter("@SCHOOL_TYPE", ddlCurriculum.SelectedItem.Text)
            Else
                param(2) = New SqlClient.SqlParameter("@SCHOOL_TYPE", "")
            End If
            param(3) = New SqlClient.SqlParameter("@ASON_DATE", txtDate.Text)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[QRY].[WEEKLY_ANALYSIS]", param)
            If ds.Tables.Count <= 0 AndAlso ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEnroll.DataSource = ds
                gvEnroll.DataBind()
                Dim columnCount As Integer = gvEnroll.Rows(0).Cells.Count
                gvEnroll.Rows(0).Cells.Clear()
                gvEnroll.Rows(0).Cells.Add(New TableCell)
                gvEnroll.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEnroll.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEnroll.Rows(0).Cells(0).Text = "Currently there is no record Exists"
            Else

                ViewState("AsonDAte") = txtDate.Text
                DT = ds.Tables(0)

                gvEnroll.DataSource = DT
                gvEnroll.DataBind()


            End If
        Catch ex As Exception
            Dim msg As String = ex.Message
        End Try

    End Sub
End Class
