<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studTRM_M_View.aspx.vb" Inherits="Students_studTRM_M_View" Title="Untitled Page" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Term Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td align="left" colspan="4">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:LinkButton ID="lbAddNew" runat="server"  >Add New</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <table id="tbl_test" runat="server" width="100%">
                                <tr>
                                    <td align="right" class="matters" colspan="9" valign="top">
                                        <asp:CheckBox ID="ChkCopy" runat="server" Text="Copy Term Master" AutoPostBack="True" CssClass="field-label"
                                            OnCheckedChanged="ChkCopy_CheckedChanged1"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr id="TrNew" runat="server" visible="false">
                                    <td align="left" class="matters" colspan="9" valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td align="left">Academic Year
                                        <asp:DropDownList ID="ddlAcademic" runat="server" AutoPostBack="True" DataSourceID="odsGetBSUAcademicYear"
                                            DataTextField="ACY_DESCR" DataValueField="ACD_ID" SkinID="DropDownListNormal">
                                        </asp:DropDownList>
                                                    
                                                </td>
                                                <td align="right">New Academic Year
                                                    <asp:DropDownList ID="ddlAcdYearCopy" runat="server" SkinID="DropDownListNormal">
                                                    </asp:DropDownList>
                                                    <asp:Button ID="btnCopy" runat="server" CssClass="button" OnClick="btnSave_Click"
                                                        Text="Copy"   />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr  >
                                    <td align="left" class="matters" colspan="9" valign="top">
                                        <asp:GridView ID="gvAcademic" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-row table-bordered" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="TRM_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("ACD_ID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                   
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTRM_ID" runat="server" Text='<%# Bind("TRM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Y_DESCR">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Y_DESCR") %>' ></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        Acad. Year<br />
                                                        <asp:DropDownList ID="ddlY_DESCRH" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlACY_DESCRH_SelectedIndexChanged" >
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblY_DESCR" runat="server" Text='<%# Bind("Y_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TRM_DESCR">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("C_DESCR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblC_DESCRH" runat="server" CssClass="gridheader_text" Text="Term Description"></asp:Label><br />
                                                        <asp:TextBox ID="txtTRM_DESCR" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchTRM_DESCR" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchTRM_DESCR_Click" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTRM_DESCR" runat="server" Text='<%# Bind("TRM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STARTDT">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("STARTDT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblSTARTDTH" runat="server" CssClass="gridheader_text" Text="Start Date"></asp:Label><br />
                                                        <asp:TextBox ID="txtSTARTDT" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchSTARTDT" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchSTARTDT_Click" />
                                                    </HeaderTemplate>
                                                   <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTARTDT" runat="server" Text='<%# Bind("STARTDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ENDDT">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("ENDDT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblENDDTH" runat="server" CssClass="gridheader_text" Text="End Date"></asp:Label><br />
                                                        <asp:TextBox ID="txtENDDT" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchENDDT" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                            OnClick="btnSearchENDDT_Click" />
                                                    </HeaderTemplate>
                                                    <ControlStyle Width="45%" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblENDDT" runat="server" Text='<%# Bind("ENDDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" Height="20px" />
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>

                <asp:ObjectDataSource ID="odsGetBSUAcademicYear" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetBSUAcademicYear" TypeName="FeeCommon">
                    <SelectParameters>
                        <asp:SessionParameter Name="BSU_ID" SessionField="sBsuid" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>

</asp:Content>
