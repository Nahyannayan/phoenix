Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports SmsService
Imports Telerik.Web.UI

Partial Class Students_studGrade_Att
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("datamode") = "add"
                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059065") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"

                    h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_7.Value = "LI__../Images/operations/like.gif"
                    ' h_Selected_menu_8.Value = "LI__../Images/operations/like.gif"

                    Session("SMS_SelALL") = New List(Of String)
                    Session("SMS_SelALL").clear()
                    ltPrev.Text = "Student First Name (Grade Section) typed message"
                    txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    Call bindAcademic_Year()

                    btnCancel.Visible = False
                    btnSave.Visible = False
                    chkSelAll.Visible = False
                    gridbind()
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

        For Each gvr As GridViewRow In gvAlloc.Rows
            'Get a programmatic reference to the CheckBox control
            Dim cb As HtmlInputCheckBox = CType(gvr.FindControl("chkControl"), HtmlInputCheckBox)
            If cb IsNot Nothing Then
                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            End If
        Next
        set_Menu_Img()
        SetChk(Me.Page)
    End Sub
    Sub FillGrades()
        ddlGrdaes.Items.Clear()
        BindGrades()


    End Sub

    Private Function CheckALL(ByVal ChkList As CheckBoxList)
        Try
            For Each LstItem As ListItem In ChkList.Items
                LstItem.Selected = True
            Next
        Catch ex As Exception

        End Try
    End Function
    Private Function UnCheckALL(ByVal ChkList As CheckBoxList)
        Try
            For Each LstItem As ListItem In ChkList.Items
                LstItem.Selected = False
            Next
        Catch ex As Exception

        End Try
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_7.Value.Split("__")
        getid7(str_Sid_img(2))


    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvAlloc.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvAlloc.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAlloc.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ACD_ID As String = String.Empty
            Dim strGrades As String = String.Empty

            'For Each LstItem As ListItem In ddlGrdaes.Items
            '    If LstItem.Selected = True Then
            '        strGrades += LstItem.Value.ToString + "|"
            '    End If
            'Next

            Dim collection As IList(Of RadComboBoxItem) = ddlGrdaes.CheckedItems
            If (collection.Count <> 0) Then
                For Each item As RadComboBoxItem In collection
                    strGrades += "" + item.Value + "|"
                Next
            End If

            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim TDATE As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)

            Dim str_Sql As String = ""
            Dim str_filter_STU_NO As String = String.Empty
            Dim str_filter_studName As String = String.Empty
            Dim str_filter_GRM_DISPLAY As String = String.Empty
            Dim str_filter_MOB_NO As String = String.Empty
            Dim str_filter_Status As String = String.Empty
            Dim str_filter_SMS_Text As String = String.Empty
            Dim str_filter_SMS_STATUS As String = String.Empty
            Dim str_filter_ATTEMPT As String = String.Empty
            Dim str_filter_Attendance_Status As String = String.Empty
            Dim FILTERCOND As String = String.Empty
            Dim ds As New DataSet



            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_STU_NO As String = String.Empty
            Dim str_studName As String = String.Empty
            Dim str_GRM_DISPLAY As String = String.Empty
            Dim str_MOB_NO As String = String.Empty
            Dim str_Status As String = String.Empty
            Dim str_SMS_Text As String = String.Empty
            Dim str_SMS_STATUS As String = String.Empty
            Dim str_ATTEMPT As String = String.Empty
            If gvAlloc.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtSTU_NO")
                str_STU_NO = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_STU_NO = " AND G.STU_NO LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STU_NO = "  AND  NOT G.STU_NO LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STU_NO = " AND G.STU_NO  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STU_NO = " AND G.STU_NO NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STU_NO = " AND G.STU_NO LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STU_NO = " AND G.STU_NO NOT LIKE '%" & txtSearch.Text & "'"
                End If




                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtGRM_DISPLAY")
                str_GRM_DISPLAY = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_GRM_DISPLAY = "  AND  NOT G.GRM_DISPLAY LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAlloc.HeaderRow.FindControl("txtMOB_NO")
                str_MOB_NO = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_MOB_NO = " AND G.MOB_NO LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_MOB_NO = "  AND  NOT G.MOB_NO  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_MOB_NO = " AND G.MOB_NO   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_MOB_NO = " AND G.MOB_NO  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_MOB_NO = " AND G.MOB_NO LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_MOB_NO = " AND G.MOB_NO  NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAlloc.HeaderRow.FindControl("txtStatus")
                str_Status = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_Status = " AND G.Status LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_Status = "  AND  NOT G.Status  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_Status = " AND G.Status   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_Status = " AND G.Status  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_Status = " AND G.Status LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_Status = " AND G.Status  NOT LIKE '%" & txtSearch.Text & "'"
                End If

                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtstudName")
                str_studName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_studName = " AND G.StudName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_studName = "  AND  NOT G.StudName LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_studName = " AND G.StudName  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_studName = " AND G.StudName NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_studName = " AND G.StudName LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_studName = " AND G.StudName NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_6.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtSMS_Text")
                str_SMS_Text = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_SMS_Text = " AND SMS_Text LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_SMS_Text = "  AND  NOT SMS_Text LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_SMS_Text = " AND SMS_Text  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_SMS_Text = " AND SMS_Text NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_SMS_Text = " AND SMS_Text LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_SMS_Text = " AND SMS_Text NOT LIKE '%" & txtSearch.Text & "'"
                End If



                str_Sid_search = h_Selected_menu_7.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtSMS_STATUS")
                str_SMS_STATUS = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_SMS_STATUS = " AND SMS_STATUS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_SMS_STATUS = "  AND  NOT SMS_STATUS LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_SMS_STATUS = " AND SMS_STATUS  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_SMS_STATUS = " AND SMS_STATUS NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_SMS_STATUS = " AND SMS_STATUS LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_SMS_STATUS = " AND SMS_STATUS NOT LIKE '%" & txtSearch.Text & "'"
                End If

                If ddl_Attendance_Status.SelectedValue = "0" Or ddl_Attendance_Status.SelectedValue = "" Then
                Else
                    str_filter_Attendance_Status = " AND APD_ID = " + ddl_Attendance_Status.SelectedValue + "  "
                End If


            End If
            FILTERCOND = str_filter_Attendance_Status + str_filter_STU_NO & str_filter_studName & str_filter_GRM_DISPLAY & str_filter_MOB_NO & str_filter_Status & str_filter_SMS_Text & str_filter_SMS_STATUS & str_filter_ATTEMPT


            If Check_If_BUS_ID_ISV() Then
                Dim param(6) As SqlParameter
                param(0) = New SqlParameter("@ACD_ID", ACD_ID)
                param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                param(2) = New SqlParameter("@GRD_IDS", strGrades)
                param(3) = New SqlParameter("@ATT_DATE", TDATE)
                param(4) = New SqlParameter("@FILTER_COND", FILTERCOND)
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[ATT].[GETSTU_ATT_SMS_LIST_BY_APD_ID]", param)
            Else
                Dim param(6) As SqlParameter
                param(0) = New SqlParameter("@ACD_ID", ACD_ID)
                param(1) = New SqlParameter("@GRD_IDS", strGrades)
                param(2) = New SqlParameter("@ATT_DATE", TDATE)
                param(3) = New SqlParameter("@FILTER_COND", FILTERCOND)
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ATT.GETSTU_ATT_SMS_LIST", param)
            End If



            If ds.Tables(0).Rows.Count > 0 Then


                gvAlloc.DataSource = ds.Tables(0)
                gvAlloc.DataBind()
                btnCancel.Visible = True
                btnSave.Visible = True
                chkSelAll.Visible = True


            Else
                Session("SMS_SelALL").clear()

                btnCancel.Visible = False
                btnSave.Visible = False
                chkSelAll.Visible = False
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvAlloc.DataSource = ds.Tables(0)
                Try
                    gvAlloc.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAlloc.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAlloc.Rows(0).Cells.Clear()
                gvAlloc.Rows(0).Cells.Add(New TableCell)
                gvAlloc.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAlloc.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAlloc.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvAlloc.HeaderRow.FindControl("txtSTU_NO")
            txtSearch.Text = str_STU_NO
            txtSearch = gvAlloc.HeaderRow.FindControl("txtSTUDNAME")
            txtSearch.Text = str_studName

            txtSearch = gvAlloc.HeaderRow.FindControl("txtGRM_DISPLAY")
            txtSearch.Text = str_GRM_DISPLAY

            txtSearch = gvAlloc.HeaderRow.FindControl("txtMOB_NO")
            txtSearch.Text = str_MOB_NO
            txtSearch = gvAlloc.HeaderRow.FindControl("txtStatus")
            txtSearch.Text = str_Status

            txtSearch = gvAlloc.HeaderRow.FindControl("txtSMS_Text")
            txtSearch.Text = str_SMS_Text

            txtSearch = gvAlloc.HeaderRow.FindControl("txtSMS_STATUS")
            txtSearch.Text = str_SMS_STATUS

            'txtSearch = gvAlloc.HeaderRow.FindControl("txtAttempt")
            'txtSearch.Text = str_ATTEMPT
            SetChk(Me.Page)
            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " &
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " &
            " where acd_id in('" & Session("Current_ACD_ID") & "','" & Session("next_ACD_ID").ToString & "" & "')"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlAcademicYear.DataSource = ds.Tables(0)
            ddlAcademicYear.DataTextField = "ACY_DESCR"
            ddlAcademicYear.DataValueField = "ACD_ID"
            ddlAcademicYear.DataBind()
            ddlAcademicYear.ClearSelection()
            ddlAcademicYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcademicYear_SelectedIndexChanged(ddlAcademicYear, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindGrades()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
        Dim ds As New DataSet

        str_Sql = " SELECT DISTINCT  GRADE_BSU_M.GRM_DISPLAY, GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_M.GRD_DISPLAYORDER " &
              " FROM  GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID WHERE GRM_BSU_ID=" & Session("SBsuID") & " and GRM_ACD_ID=" & ACD_ID & " ORDER BY GRD_DISPLAYORDER"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlGrdaes.Items.Clear()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlGrdaes.DataSource = ds.Tables(0)
            ddlGrdaes.DataTextField = "GRM_DISPLAY"
            ddlGrdaes.DataValueField = "GRD_ID"
            ddlGrdaes.DataBind()
        End If

    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGrades()
        Populateddl_Attendance_Status()
    End Sub
    Sub setControl()
        ddlAcademicYear.Enabled = True

    End Sub
    Sub ResetControl()
        ddlAcademicYear.Enabled = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call setControl()
        btnSave.Visible = False
        btnCancel.Visible = False
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then
            Dim count, dscount, sendsuccess, sendfailed As Integer
            Dim chk As HtmlInputCheckBox
            Dim STU_ID As String = String.Empty
            Dim Grade As String = String.Empty
            Dim sFname As String = String.Empty
            Dim MobNo As String = String.Empty
            Dim hash As New Hashtable
            Dim vhash As SMS_HASH
            If Not Session("hashCheck_sms") Is Nothing Then
                hash = Session("hashCheck_sms")
            End If
            For Each rowItem As GridViewRow In gvAlloc.Rows
                chk = DirectCast(rowItem.FindControl("chkControl"), HtmlInputCheckBox)

                STU_ID = DirectCast(rowItem.FindControl("lblStu_ID"), Label).Text
                Grade = DirectCast(rowItem.FindControl("lblGRM_DISPLAY"), Label).Text
                sFname = DirectCast(rowItem.FindControl("lblFname"), Label).Text
                MobNo = DirectCast(rowItem.FindControl("lblMobNo"), Label).Text
                If chk.Checked = True Then
                    If hash.Contains(STU_ID) = False Then
                        ' hash.Add(STU_ID, DirectCast(rowItem.FindControl("lblStu_ID"), Label).Text)

                        vhash = New SMS_HASH
                        vhash.STU_ID = STU_ID
                        vhash.StudFname = StrConv(sFname, VbStrConv.ProperCase)
                        vhash.Grade = Grade
                        vhash.Mob_No = MobNo
                        hash(STU_ID) = vhash


                    End If
                Else
                    If hash.Contains(STU_ID) = True Then
                        hash.Remove(STU_ID)
                    End If
                End If



            Next
            Dim transaction As SqlTransaction
            Dim status As String


            Dim from As String = Session("BSU_SMS_FROM")
            Dim password As String = "manoj"
            Dim ATTDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Dim SendDate As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now())

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    Session("hashCheck_sms") = hash

                    Dim tempHash As Hashtable = Session("hashCheck_sms")
                    Dim i = 0
                    Dim rcount = 0
                    For Each vloop As SMS_HASH In tempHash.Values
                        Dim mobileNo As String = vloop.Mob_No
                        Dim message As String = vloop.StudFname & " ( " & vloop.Grade & " ) " & txtMessage.Text



                        status = "PENDING" ''sms.SendMessage(mobileNo, message, from, "gemseducation", password)

                        SaveLog(ddlAcademicYear.SelectedItem.Value, vloop.STU_ID, status, ATTDT, SendDate, message, Session("SBsuID"), mobileNo)

                        If status.IndexOf("Error") > -1 Then
                            sendfailed = sendfailed + 1
                        Else
                            sendsuccess = sendsuccess + 1
                        End If
                        Session("count") = i + 1
                        rcount = rcount + 1
                        'If rcount = 50 Then
                        '    GoTo lb
                        'End If

                    Next
lb:
                    Dim percentage As String = ""

                    count = Session("count")
                    dscount = tempHash.Count

                    lblTotalSend.Text = "Total Sent :" & count
                    lblTotalMessage.Text = "Total Message :" & count
                    lblFailed.Text = "Sending Failed :" & sendfailed
                    lblSuccess.Text = "Successfully Sent :" & sendsuccess

                    percentage = (count / dscount) * 100
                    Session("hashCheck_sms") = Nothing
                    Session("SMS_SelALL") = Nothing

                    gridbind()
                Catch ex As Exception

                    lblError.Text = "Error Occured While Sending."

                End Try

            End Using

        End If


    End Sub
    Public Sub SaveLog(ByVal ASL_ACD_ID As String, ByVal ASL_STU_ID As String, ByVal ASL_LOG_STATUS As String, ByVal ASL_ATTDT As String, ByVal ASL_SEND_DATE As String, ByVal ASL_SMS_TEXT As String, ByVal ASL_BSU_ID As String, ByVal ASL_MOBILE_NO As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(9) As SqlClient.SqlParameter
        Try
            pParms(0) = New SqlClient.SqlParameter("@ASL_ACD_ID", ASL_ACD_ID)
            pParms(1) = New SqlClient.SqlParameter("@ASL_STU_ID", ASL_STU_ID)
            pParms(2) = New SqlClient.SqlParameter("@ASL_LOG_STATUS", ASL_LOG_STATUS)
            pParms(3) = New SqlClient.SqlParameter("@ASL_ATTDT", ASL_ATTDT)
            pParms(4) = New SqlClient.SqlParameter("@ASL_SEND_DATE", ASL_SEND_DATE)
            pParms(5) = New SqlClient.SqlParameter("@ASL_SMS_TEXT", ASL_SMS_TEXT)
            pParms(6) = New SqlClient.SqlParameter("@ASL_USR_NAME", Session("sUsr_name"))
            pParms(7) = New SqlClient.SqlParameter("@ASL_BSU_ID", ASL_BSU_ID)
            pParms(8) = New SqlClient.SqlParameter("@ASL_MOBILE_NO", ASL_MOBILE_NO)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "ATT.SaveATTENDANCE_SMS_LOG", pParms)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "absentsms")

        End Try
    End Sub
    Function BSU_ShortName(ByVal BSUID As String) As String
        Dim sqlString As String = "select BSU_SHORTNAME  from BUSINESSUNIT_M where BSU_ID='" & BSUID & "'"

        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return CStr(result)
    End Function
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub gvAlloc_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hash As New Hashtable
        If Not Session("hashCheck_sms") Is Nothing Then
            hash = Session("hashCheck_sms")
        End If
        Dim chk As HtmlInputCheckBox

        Dim STU_ID As String = String.Empty
        For Each rowItem As GridViewRow In gvAlloc.Rows
            chk = DirectCast((rowItem.Cells(0).FindControl("chkControl")), HtmlInputCheckBox)
            STU_ID = gvAlloc.DataKeys(rowItem.RowIndex)("STU_ID").ToString()

            If hash.Contains(chk.Value) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next

    End Sub
    Protected Sub gvAlloc_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAlloc.PageIndexChanging
        gvAlloc.PageIndex = e.NewPageIndex
        Dim chk As HtmlInputCheckBox
        Dim STU_ID As String = String.Empty
        Dim Grade As String = String.Empty
        Dim sFname As String = String.Empty
        Dim MobNo As String = String.Empty
        Dim hash As New Hashtable
        Dim vhash As SMS_HASH
        If Not Session("hashCheck_sms") Is Nothing Then
            hash = Session("hashCheck_sms")
        End If
        For Each rowItem As GridViewRow In gvAlloc.Rows
            chk = DirectCast(rowItem.FindControl("chkControl"), HtmlInputCheckBox)
            STU_ID = DirectCast(rowItem.FindControl("lblStu_ID"), Label).Text
            Grade = DirectCast(rowItem.FindControl("lblGRM_DISPLAY"), Label).Text
            sFname = DirectCast(rowItem.FindControl("lblFname"), Label).Text
            MobNo = DirectCast(rowItem.FindControl("lblMobNo"), Label).Text
            If chk.Checked = True Then
                If hash.Contains(chk.Value.ToString) = False Then
                    vhash = New SMS_HASH
                    vhash.STU_ID = STU_ID
                    vhash.StudFname = StrConv(sFname, VbStrConv.ProperCase)
                    vhash.Grade = Grade
                    vhash.Mob_No = MobNo
                    hash(STU_ID) = vhash
                End If
            Else
                If hash.Contains(STU_ID) = True Then
                    hash.Remove(STU_ID)
                End If
            End If
        Next
        Session("hashCheck_sms") = hash
        gridbind()
    End Sub
    Protected Sub btnSearchSTU_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchStudName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchSTATUS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchGRM_DISPLAY_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchSCT_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchAttempt_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchSMS_STATUS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchSMS_TEXT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub btnSearchMOB_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ValidateDate() <> "-1" Then
            gridbind()
        End If
    End Sub
    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtDate.Text <> "" Then
                Dim strfDate As String = txtDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"

            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
            Return "-1"
        End Try

    End Function
    Protected Sub gvAlloc_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAlloc.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If chkSelAll.Checked = True Then
                If Session("SMS_SelALL").count > 0 Then
                    TryCast(e.Row.FindControl("chkControl"), HtmlInputCheckBox).Checked = True
                Else
                    TryCast(e.Row.FindControl("chkControl"), HtmlInputCheckBox).Checked = False
                End If
            End If


            Dim lbl As Label = DirectCast(e.Row.FindControl("lblSMS_STATUS"), Label)

            If lbl.Text = "Error While Sending" Then
                e.Row.ForeColor = System.Drawing.Color.Red
                lbl.ForeColor = System.Drawing.Color.Red
            ElseIf lbl.Text = "Successfully Send" Then
                e.Row.ForeColor = System.Drawing.Color.Green
                lbl.ForeColor = System.Drawing.Color.Green
            End If
        End If

    End Sub


    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            gridbind()
        Catch ex As Exception

        End Try
    End Sub

    '    Private Sub SelectAll_Student()

    '        Try
    '            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '            Dim ACD_ID As String = String.Empty
    '            Dim strGrades As String = ""

    '            'For Each LstItem As ListItem In ddlGrdaes.Items
    '            '    If LstItem.Selected = True Then
    '            '        strGrades += "'" + LstItem.Value + "',"
    '            '    End If
    '            'Next
    '            Dim collection As IList(Of RadComboBoxItem) = ddlGrdaes.CheckedItems
    '            If (collection.Count <> 0) Then
    '                For Each item As RadComboBoxItem In collection
    '                    strGrades += "'" + item.Value + "',"
    '                Next
    '            End If


    '            If strGrades.Length >= 2 Then
    '                strGrades = strGrades.Substring(0, Len(strGrades) - 1)
    '            End If

    '            If ddlAcademicYear.SelectedIndex = -1 Then
    '                ACD_ID = ""
    '            Else
    '                ACD_ID = ddlAcademicYear.SelectedItem.Value
    '            End If
    '            Dim TDATE As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)

    '            Dim str_Sql As String = ""
    '            Dim str_filter_STU_NO As String = String.Empty
    '            Dim str_filter_studName As String = String.Empty
    '            Dim str_filter_GRM_DISPLAY As String = String.Empty
    '            Dim str_filter_MOB_NO As String = String.Empty
    '            Dim str_filter_Status As String = String.Empty
    '            Dim str_filter_SMS_Text As String = String.Empty
    '            Dim str_filter_SMS_STATUS As String = String.Empty
    '            Dim str_filter_ATTEMPT As String = String.Empty
    '            Dim ds As New DataSet

    '            str_Sql = "SELECT STU_ID,STU_FEE_ID,STUDNAME,Status,GRM_DISPLAY,STU_NO,GRD_ID,SCT_ID,MOB_NO, GRD_DISPLAYORDER,STU_ACD_ID,FNAME,ATTEMPT, SMS_TEXT,SMS_STATUS FROM( SELECT distinct STU_ID,STU_FEE_ID,STUDNAME,Status,GRM_DISPLAY,STU_NO,GRD_ID,SCT_ID, " & _
    '" CASE WHEN  MOB_NO IS NULL THEN '' WHEN RTRIM(MOB_NO)='' OR RTRIM(MOB_NO)='0' THEN '' ELSE '971'+ right(replace(MOB_NO,'-',''),9) END as MOB_NO," & _
    '" GRD_DISPLAYORDER,STU_ACD_ID,FNAME ," & _
    '" ISNULL(B.ASL_ATTEMPT,0) AS ATTEMPT,ISNULL(B.ASL_SMS_TEXT,'') AS SMS_TEXT , " & _
    '" CASE WHEN (RTRIM(isnull(B.ASL_LOG_STATUS,''))='')  then 'Pending' " & _
    '"  WHEN (charindex('error',B.ASL_LOG_STATUS)>0 )THEN 'Error While Sending' else 'Successfully Send' end AS SMS_STATUS " & _
    '" FROM ( SELECT   STUDENT_M.STU_ID, STUDENT_M.STU_FEE_ID, ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME), '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_MIDNAME), '') + ' ' + ISNULL(UPPER(STUDENT_M.STU_LASTNAME), '') AS STUDNAME, " & _
    ' "   (SELECT     APD_PARAM_DESCR FROM   ATTENDANCE_PARAM_D  WHERE  (APD_ID = ATTENDANCE_LOG_STUDENT.ALS_APD_ID)) AS Status, GRADE_BSU_M.GRM_DISPLAY+' '+ SECTION_M.SCT_DESCR AS GRM_DISPLAY, " & _
    '                     "   STUDENT_M.STU_NO, CASE WHEN STUDENT_M.STU_PRIMARYCONTACT = 'F' THEN " & _
    '                          "  (SELECT     ISNULL(D .STS_FMOBILE, 0)  FROM    STUDENT_D D INNER JOIN " & _
    '                          "   STUDENT_M M ON D .STS_STU_ID = M.STU_SIBLING_ID " & _
    '                           "   WHERE      (M.STU_ID = STUDENT_M.STU_ID) AND (M.STU_PRIMARYCONTACT = 'F')) " & _
    '                       " WHEN STUDENT_M.STU_PRIMARYCONTACT = 'M' THEN " & _
    '                           " (SELECT     ISNULL(D .STS_MMOBILE, 0)  FROM   STUDENT_D D INNER JOIN " & _
    '                          "    STUDENT_M M ON D .STS_STU_ID = M.STU_SIBLING_ID " & _
    '                           "   WHERE      (M.STU_ID = STUDENT_M.STU_ID) AND (M.STU_PRIMARYCONTACT = 'M')) " & _
    '                       "  WHEN STUDENT_M.STU_PRIMARYCONTACT = 'G' THEN   (SELECT     ISNULL(D .STS_GMOBILE, 0) " & _
    '                             "  FROM STUDENT_D D INNER JOIN  STUDENT_M M ON D .STS_STU_ID = M.STU_SIBLING_ID " & _
    '                            "   WHERE (M.STU_ID = STUDENT_M.STU_ID) AND (M.STU_PRIMARYCONTACT = 'G')) END  as MOB_NO, " & _
    '  "  GRADE_M.GRD_DISPLAYORDER, STUDENT_M.STU_ACD_ID,ISNULL(UPPER(STUDENT_M.STU_FIRSTNAME), '')  AS FNAME ,STUDENT_M.STU_GRD_ID AS GRD_ID,STUDENT_M.STU_SCT_ID AS SCT_ID " & _
    ' "  FROM   ATTENDANCE_LOG_GRADE INNER JOIN ATTENDANCE_LOG_STUDENT ON ATTENDANCE_LOG_GRADE.ALG_ID = ATTENDANCE_LOG_STUDENT.ALS_ALG_ID INNER JOIN " & _
    ' "  STUDENT_M ON ATTENDANCE_LOG_GRADE.ALG_ACD_ID = STUDENT_M.STU_ACD_ID AND ATTENDANCE_LOG_GRADE.ALG_GRD_ID = STUDENT_M.STU_GRD_ID AND ATTENDANCE_LOG_GRADE.ALG_SCT_ID = STUDENT_M.STU_SCT_ID AND " & _
    '    "  ATTENDANCE_LOG_STUDENT.ALS_STU_ID = STUDENT_M.STU_ID INNER JOIN  GRADE_BSU_M ON STUDENT_M.STU_GRM_ID = GRADE_BSU_M.GRM_ID AND STUDENT_M.STU_ACD_ID = GRADE_BSU_M.GRM_ACD_ID AND " & _
    '   " STUDENT_M.STU_GRD_ID = GRADE_BSU_M.GRM_GRD_ID INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID = SECTION_M.SCT_ID AND STUDENT_M.STU_GRM_ID = SECTION_M.SCT_GRM_ID AND " & _
    '  "  STUDENT_M.STU_ACD_ID = SECTION_M.SCT_ACD_ID INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID  LEFT OUTER JOIN TCM_M  ON TCM_M.TCM_STU_ID=STUDENT_M.STU_ID  AND  (TCM_M.TCM_CANCELDATE IS NULL)" & _
    '  " WHERE  (ATTENDANCE_LOG_GRADE.ALG_ACD_ID = '" & ACD_ID & "') AND (ATTENDANCE_LOG_GRADE.ALG_ATT_TYPE = 'Session1') AND " & _
    '  " (ATTENDANCE_LOG_GRADE.ALG_ATTDT = '" & TDATE & "') AND (ATTENDANCE_LOG_STUDENT.ALS_APD_ID IN " & _
    '  " (SELECT APD_ID    FROM  ATTENDANCE_PARAM_D AS ATTENDANCE_PARAM_D_1  WHERE   (APD_ACD_ID = '" & ACD_ID & "') AND (APD_APM_ID = '2') " & _
    '  " AND (APD_bSHOW = 1))) AND (STUDENT_M.STU_CURRSTATUS <> 'CN')  AND (CONVERT(datetime, TCM_M.TCM_LASTATTDATE) > CONVERT(datetime, '" & TDATE & "') OR " & _
    '  "   CONVERT(datetime, TCM_M.TCM_LASTATTDATE) IS NULL) AND (STUDENT_M.STU_DOJ <= CONVERT(datetime, '" & TDATE & "')))A " & _
    '" LEFT OUTER JOIN ATTENDANCE_SMS_LOG B ON A.STU_ACD_ID=B.ASL_ACD_ID AND A.STU_ID=B.ASL_STU_ID AND B.ASL_ATTDT='" & TDATE & "')G " & _
    '" where   G.stu_id<>'' AND G.GRD_ID IN(" & strGrades & ")"





    '            Dim txtSearch As New TextBox
    '            Dim str_search As String
    '            Dim str_STU_NO As String = String.Empty
    '            Dim str_studName As String = String.Empty
    '            Dim str_GRM_DISPLAY As String = String.Empty
    '            Dim str_MOB_NO As String = String.Empty
    '            Dim str_Status As String = String.Empty
    '            Dim str_SMS_Text As String = String.Empty
    '            Dim str_SMS_STATUS As String = String.Empty
    '            Dim str_ATTEMPT As String = String.Empty
    '            If gvAlloc.Rows.Count > 0 Then

    '                Dim str_Sid_search() As String

    '                str_Sid_search = h_Selected_menu_1.Value.Split("__")
    '                str_search = str_Sid_search(0)

    '                txtSearch = gvAlloc.HeaderRow.FindControl("txtSTU_NO")
    '                str_STU_NO = txtSearch.Text

    '                If str_search = "LI" Then
    '                    str_filter_STU_NO = " AND G.STU_NO LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NLI" Then
    '                    str_filter_STU_NO = "  AND  NOT G.STU_NO LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "SW" Then
    '                    str_filter_STU_NO = " AND G.STU_NO  LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NSW" Then
    '                    str_filter_STU_NO = " AND G.STU_NO NOT LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "EW" Then
    '                    str_filter_STU_NO = " AND G.STU_NO LIKE  '%" & txtSearch.Text & "'"
    '                ElseIf str_search = "NEW" Then
    '                    str_filter_STU_NO = " AND G.STU_NO NOT LIKE '%" & txtSearch.Text & "'"
    '                End If




    '                str_Sid_search = h_Selected_menu_2.Value.Split("__")
    '                str_search = str_Sid_search(0)

    '                txtSearch = gvAlloc.HeaderRow.FindControl("txtGRM_DISPLAY")
    '                str_GRM_DISPLAY = txtSearch.Text

    '                If str_search = "LI" Then
    '                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NLI" Then
    '                    str_filter_GRM_DISPLAY = "  AND  NOT G.GRM_DISPLAY LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "SW" Then
    '                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NSW" Then
    '                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY NOT LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "EW" Then
    '                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY LIKE  '%" & txtSearch.Text & "'"
    '                ElseIf str_search = "NEW" Then
    '                    str_filter_GRM_DISPLAY = " AND G.GRM_DISPLAY NOT LIKE '%" & txtSearch.Text & "'"
    '                End If


    '                str_Sid_search = h_Selected_menu_3.Value.Split("__")
    '                str_search = str_Sid_search(0)
    '                txtSearch = gvAlloc.HeaderRow.FindControl("txtMOB_NO")
    '                str_MOB_NO = txtSearch.Text

    '                If str_search = "LI" Then
    '                    str_filter_MOB_NO = " AND G.MOB_NO LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NLI" Then
    '                    str_filter_MOB_NO = "  AND  NOT G.MOB_NO  LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "SW" Then
    '                    str_filter_MOB_NO = " AND G.MOB_NO   LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NSW" Then
    '                    str_filter_MOB_NO = " AND G.MOB_NO  NOT LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "EW" Then
    '                    str_filter_MOB_NO = " AND G.MOB_NO LIKE  '%" & txtSearch.Text & "'"
    '                ElseIf str_search = "NEW" Then
    '                    str_filter_MOB_NO = " AND G.MOB_NO  NOT LIKE '%" & txtSearch.Text & "'"
    '                End If


    '                str_Sid_search = h_Selected_menu_4.Value.Split("__")
    '                str_search = str_Sid_search(0)
    '                txtSearch = gvAlloc.HeaderRow.FindControl("txtStatus")
    '                str_Status = txtSearch.Text

    '                If str_search = "LI" Then
    '                    str_filter_Status = " AND G.Status LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NLI" Then
    '                    str_filter_Status = "  AND  NOT G.Status  LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "SW" Then
    '                    str_filter_Status = " AND G.Status   LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NSW" Then
    '                    str_filter_Status = " AND G.Status  NOT LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "EW" Then
    '                    str_filter_Status = " AND G.Status LIKE  '%" & txtSearch.Text & "'"
    '                ElseIf str_search = "NEW" Then
    '                    str_filter_Status = " AND G.Status  NOT LIKE '%" & txtSearch.Text & "'"
    '                End If

    '                str_Sid_search = h_Selected_menu_5.Value.Split("__")
    '                str_search = str_Sid_search(0)

    '                txtSearch = gvAlloc.HeaderRow.FindControl("txtstudName")
    '                str_studName = txtSearch.Text

    '                If str_search = "LI" Then
    '                    str_filter_studName = " AND G.StudName LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NLI" Then
    '                    str_filter_studName = "  AND  NOT G.StudName LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "SW" Then
    '                    str_filter_studName = " AND G.StudName  LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NSW" Then
    '                    str_filter_studName = " AND G.StudName NOT LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "EW" Then
    '                    str_filter_studName = " AND G.StudName LIKE  '%" & txtSearch.Text & "'"
    '                ElseIf str_search = "NEW" Then
    '                    str_filter_studName = " AND G.StudName NOT LIKE '%" & txtSearch.Text & "'"
    '                End If



    '                str_Sid_search = h_Selected_menu_6.Value.Split("__")
    '                str_search = str_Sid_search(0)

    '                txtSearch = gvAlloc.HeaderRow.FindControl("txtSMS_Text")
    '                str_SMS_Text = txtSearch.Text

    '                If str_search = "LI" Then
    '                    str_filter_SMS_Text = " AND SMS_Text LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NLI" Then
    '                    str_filter_SMS_Text = "  AND  NOT SMS_Text LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "SW" Then
    '                    str_filter_SMS_Text = " AND SMS_Text  LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NSW" Then
    '                    str_filter_SMS_Text = " AND SMS_Text NOT LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "EW" Then
    '                    str_filter_SMS_Text = " AND SMS_Text LIKE  '%" & txtSearch.Text & "'"
    '                ElseIf str_search = "NEW" Then
    '                    str_filter_SMS_Text = " AND SMS_Text NOT LIKE '%" & txtSearch.Text & "'"
    '                End If



    '                str_Sid_search = h_Selected_menu_7.Value.Split("__")
    '                str_search = str_Sid_search(0)

    '                txtSearch = gvAlloc.HeaderRow.FindControl("txtSMS_STATUS")
    '                str_SMS_STATUS = txtSearch.Text

    '                If str_search = "LI" Then
    '                    str_filter_SMS_STATUS = " AND SMS_STATUS LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NLI" Then
    '                    str_filter_SMS_STATUS = "  AND  NOT SMS_STATUS LIKE '%" & txtSearch.Text & "%'"
    '                ElseIf str_search = "SW" Then
    '                    str_filter_SMS_STATUS = " AND SMS_STATUS  LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "NSW" Then
    '                    str_filter_SMS_STATUS = " AND SMS_STATUS NOT LIKE '" & txtSearch.Text & "%'"
    '                ElseIf str_search = "EW" Then
    '                    str_filter_SMS_STATUS = " AND SMS_STATUS LIKE  '%" & txtSearch.Text & "'"
    '                ElseIf str_search = "NEW" Then
    '                    str_filter_SMS_STATUS = " AND SMS_STATUS NOT LIKE '%" & txtSearch.Text & "'"
    '                End If




    '            End If

    '            Dim sqlreader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql & str_filter_STU_NO & str_filter_studName & str_filter_GRM_DISPLAY & str_filter_MOB_NO & str_filter_Status & str_filter_SMS_Text & str_filter_SMS_STATUS & str_filter_ATTEMPT & " ORDER BY G.GRD_DISPLAYORDER")


    '            Dim hash As New Hashtable
    '            Dim vhash As SMS_HASH
    '            If Not Session("hashCheck_sms") Is Nothing Then
    '                hash = Session("hashCheck_sms")
    '            End If
    '            Dim i As Integer = 0

    '            If sqlreader.HasRows = True Then
    '                While (sqlreader.Read())
    '                    i = i + 1
    '                    Session("SMS_SelALL").Remove(sqlreader(0))
    '                    Session("SMS_SelALL").Add(sqlreader(0))

    '                    If hash.Contains(sqlreader(0).ToString) = False Then

    '                        vhash = New SMS_HASH
    '                        vhash.STU_ID = sqlreader(0).ToString
    '                        vhash.StudFname = StrConv(sqlreader("FNAME"), VbStrConv.ProperCase)
    '                        vhash.Grade = sqlreader("GRM_DISPLAY")
    '                        vhash.Mob_No = sqlreader("MOB_NO")
    '                        hash(sqlreader(0).ToString) = vhash
    '                    End If


    '                End While

    '                Session("hashCheck_sms") = hash
    '            End If
    '            gridbind()

    '        Catch ex As Exception

    '        End Try
    '    End Sub

    Private Sub SelectAll_Student()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ACD_ID As String = String.Empty
            Dim strGrades As String = ""

            'For Each LstItem As ListItem In ddlGrdaes.Items
            '    If LstItem.Selected = True Then
            '        strGrades += "'" + LstItem.Value + "',"
            '    End If
            'Next
            Dim collection As IList(Of RadComboBoxItem) = ddlGrdaes.CheckedItems
            If (collection.Count <> 0) Then
                'For Each item As RadComboBoxItem In collection
                '    strGrades += "'" + item.Value + "',"
                'Next
                For Each item As RadComboBoxItem In collection
                    strGrades += item.Value + ","
                Next
            End If


            If strGrades.Length >= 2 Then
                strGrades = strGrades.Substring(0, Len(strGrades) - 1)
            End If

            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim TDATE As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)

            Dim str_Sql As String = ""
            'Dim str_filter_STU_NO As String = String.Empty
            'Dim str_filter_studName As String = String.Empty
            'Dim str_filter_GRM_DISPLAY As String = String.Empty
            'Dim str_filter_MOB_NO As String = String.Empty
            'Dim str_filter_Status As String = String.Empty
            'Dim str_filter_SMS_Text As String = String.Empty
            'Dim str_filter_SMS_STATUS As String = String.Empty
            'Dim str_filter_ATTEMPT As String = String.Empty
            Dim str_filter_Notlike As String = ""
            Dim ds As New DataSet

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_STU_NO As String = ""
            Dim str_studName As String = ""
            Dim str_GRM_DISPLAY As String = ""
            Dim str_MOB_NO As String = ""
            Dim str_Status As String = ""
            Dim str_SMS_Text As String = ""
            Dim str_SMS_STATUS As String = ""
            Dim str_ATTEMPT As String = ""
            Dim AttendanceStatus As String = ""
            If gvAlloc.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtSTU_NO")
                str_STU_NO = txtSearch.Text.Trim()

                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtGRM_DISPLAY")
                str_GRM_DISPLAY = txtSearch.Text.Trim()

                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAlloc.HeaderRow.FindControl("txtMOB_NO")
                str_MOB_NO = txtSearch.Text.Trim()

                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAlloc.HeaderRow.FindControl("txtStatus")
                str_Status = txtSearch.Text.Trim()

                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtstudName")
                str_studName = txtSearch.Text.Trim()

                str_Sid_search = h_Selected_menu_6.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtSMS_Text")
                str_SMS_Text = txtSearch.Text.Trim()

                str_Sid_search = h_Selected_menu_7.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAlloc.HeaderRow.FindControl("txtSMS_STATUS")
                str_SMS_STATUS = txtSearch.Text.Trim()

                If str_search = "LI" Then
                    str_filter_Notlike = ""
                ElseIf str_search = "NLI" Then
                    str_filter_Notlike = "NOT"
                ElseIf str_search = "SW" Then
                    str_filter_Notlike = ""
                ElseIf str_search = "NSW" Then
                    str_filter_Notlike = "NOT"
                ElseIf str_search = "EW" Then
                    str_filter_Notlike = ""
                ElseIf str_search = "NEW" Then
                    str_filter_Notlike = "NOT"
                Else : str_filter_Notlike = ""
                End If
                If ddl_Attendance_Status.SelectedValue = "0" Or ddl_Attendance_Status.SelectedValue = "" Then
                Else
                    AttendanceStatus = ddl_Attendance_Status.SelectedValue
                End If
            End If

            Dim param(11) As SqlParameter
            param(0) = New SqlParameter("@ACD_ID", ACD_ID)
            param(1) = New SqlParameter("@ALG_ATTDT", TDATE)
            param(2) = New SqlParameter("@GRD_IDs", strGrades)
            param(3) = New SqlParameter("@STU_NO", str_STU_NO)
            param(4) = New SqlParameter("@GRM_DISPLAY", str_GRM_DISPLAY)
            param(5) = New SqlParameter("@MOB_NO", str_MOB_NO)
            param(6) = New SqlParameter("@Status", str_Status)
            param(7) = New SqlParameter("@StudName", str_studName)
            param(8) = New SqlParameter("@SMS_Text", str_SMS_Text)
            param(9) = New SqlParameter("@SMS_STATUS", str_SMS_STATUS)
            param(10) = New SqlParameter("@Notlike", str_filter_Notlike)
            param(11) = New SqlParameter("@ATT_STATUS", AttendanceStatus)

            Dim sqlreader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "dbo.GET_STU_ATTANDANCE_SMS_DETAILS", param)


            Dim hash As New Hashtable
            Dim vhash As SMS_HASH
            If Not Session("hashCheck_sms") Is Nothing Then
                hash = Session("hashCheck_sms")
            End If
            Dim i As Integer = 0

            If sqlreader.HasRows = True Then
                While (sqlreader.Read())
                    i = i + 1
                    Session("SMS_SelALL").Remove(sqlreader(0))
                    Session("SMS_SelALL").Add(sqlreader(0))

                    If hash.Contains(sqlreader(0).ToString) = False Then

                        vhash = New SMS_HASH
                        vhash.STU_ID = sqlreader(0).ToString
                        vhash.StudFname = StrConv(sqlreader("FNAME"), VbStrConv.ProperCase)
                        vhash.Grade = sqlreader("GRM_DISPLAY")
                        vhash.Mob_No = sqlreader("MOB_NO")
                        hash(sqlreader(0).ToString) = vhash
                    End If


                End While

                Session("hashCheck_sms") = hash
            End If
            gridbind()

        Catch ex As Exception

        End Try
    End Sub
    Private Sub SetChk(ByVal Page As Control)
        For Each ctrl As Control In Page.Controls
            If TypeOf ctrl Is HtmlInputCheckBox Then
                Dim chk As HtmlInputCheckBox = CType(ctrl, HtmlInputCheckBox)
                If chk.Checked = True Then
                    If list_add(chk.Value.ToString) = False Then
                        chk.Checked = True
                    End If
                Else
                    If list_exist(chk.Value.ToString) = True Then
                        chk.Checked = True
                    End If
                    list_remove(chk.Value.ToString)
                End If
            Else
                If ctrl.Controls.Count > 0 Then
                    SetChk(ctrl)
                End If
            End If
        Next
    End Sub

    Private Function list_exist(ByVal Stu_id As String) As Boolean
        If Session("SMS_SelALL").Contains(Stu_id) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal Stu_id As String) As Boolean
        If Session("SMS_SelALL").Contains(Stu_id) Then
            Return False
        Else
            Session("SMS_SelALL").Add(Stu_id)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal Stu_id As String)
        If Session("SMS_SelALL").Contains(Stu_id) Then
            Session("SMS_SelALL").Remove(Stu_id)
        End If
    End Sub

    Protected Sub chkSelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelAll.CheckedChanged

        If chkSelAll.Checked Then
            SelectAll_Student()
        Else
            Session("SMS_SelALL").clear()
            Session("hashCheck_sms") = Nothing
        End If
        gridbind()
    End Sub
    Protected Sub ddl_Attendance_Status_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub
    Public Sub Populateddl_Attendance_Status()
        If Check_If_BUS_ID_ISV() Then
            TR_Attendance_Status.Visible = True
            Try
                ddl_Attendance_Status.Items.Clear()

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim PARAM(5) As SqlParameter

                PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
                PARAM(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))

                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[STU].[GETATT_PARAM_GRIDBIND_BY_ASP_APD_ID]", PARAM)
                ddl_Attendance_Status.Items.Clear()
                Dim di As ListItem
                di = New ListItem("ALL", "0")
                ddl_Attendance_Status.Items.Add(di)
                ddl_Attendance_Status.DataSource = ds
                ddl_Attendance_Status.DataTextField = "APD_PARAM_DESCR"
                ddl_Attendance_Status.DataValueField = "APD_ID"
                ddl_Attendance_Status.DataBind()
                ddl_Attendance_Status.Items.Insert(0, di)

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        Else
            Dim di As ListItem
            di = New ListItem("ALL", "0")
            ddl_Attendance_Status.Items.Insert(0, di)
        End If
    End Sub
    Function Check_If_BUS_ID_ISV() As Boolean
        Dim Flag As Boolean = False
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn)
        Using objConn
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            Flag = SqlHelper.ExecuteScalar(objConn, CommandType.StoredProcedure, "[ATT].[Check_sBsuid_For_ATT_SMS]", pParms)
        End Using
        Return Flag
    End Function
End Class
