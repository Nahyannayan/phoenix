<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" 
CodeFile="OnlineReEnroll_create.aspx.vb" Inherits="OnlineReEnroll_create" title="Untitled Page" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i> <asp:Literal id="ltHeader" runat="server" Text="Online Re-Enrolment Text"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
   
    <table id="Table1" border="0" width="100%">
        
          <tr>
            <td   style="height: 28px" valign="bottom" align="left">
          <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    
   <table align="center" width="100%" cellpadding="5" cellspacing="0" >
      
        
        
         <tr>
            <td align="left" width="20%">
               <span class="field-label">Content Type</span>
            </td>
            <td align="left" width="30%"><asp:DropDownList ID="ddlInfoType" runat="server"  AutoPostBack="true">
                <asp:ListItem Value="Yes">Yes Statement</asp:ListItem>
                <asp:ListItem Value="No">No statement</asp:ListItem>
                <asp:ListItem Value="ENROLL_MAIL">Re-Enroling Mail Statement</asp:ListItem>
                <asp:ListItem Value="NOT_ENROLL_MAIL">Not Re-Enroling Mail Statment</asp:ListItem>
                 <asp:ListItem Value="GEMS_ENROLL_MAIL">GEMS Staff Re-Enroling Mail Statment</asp:ListItem>
                  <asp:ListItem Value="COMP_ENROLL_MAIL">Company Staff Re-Enroling Mail Statment</asp:ListItem>
                  <asp:ListItem Value="PAYATSCHOOL_MAIL">Parent Paying at School Mail Statment</asp:ListItem>
                </asp:DropDownList></td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
        </tr>
       <tr>
           <td colspan="4">&nbsp;</td>
       </tr>
          <tr>
              <td colspan="4" align="left">
                                                <telerik:RadEditor ID="txtOfferText" runat="server" EditModes="All" 
                                                    Height="350px"  Width="100%">
                                                </telerik:RadEditor>

                                            </td>
                                        </tr>
                                        
                                         
                                         <tr>
            <td   valign="bottom" align="left"  colspan="4">
          <asp:CheckBox ID="chkCopy" runat="server"  Text="Copy to all GEMS Schools"/>
            </td>
        </tr>
        
        
                                         <tr>
            <td valign="bottom" align="center"  colspan="4">
               <asp:Button id="btnSave" runat="server" CssClass="button"  Text="Save" />
            </td>
        </tr>
        
        
   </table>

            </div>
        </div>
    </div>
  
</asp:Content>

