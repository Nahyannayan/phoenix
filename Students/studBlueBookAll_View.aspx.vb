Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Xml
Imports GemBox.Spreadsheet
Imports ResponseHelper
Imports system
Imports System.Data.SqlTypes
Partial Class Students_studBlueBook_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Dim url As String
        Dim MainMnu_code As String = ViewState("MainMnu_code")
        Dim Grade As String = ddlGrade.SelectedItem.Value
        Dim Section As String = ddlSection.SelectedItem.Value
        Dim PrintDate As String
        Session("PrintDate") = txtAsOnDate.Text
        Session("Blue_ACD_Date") = txtAcdDate.Text


        Dim strRedirect As String
        If Session("sbsuid") <> "133006" And Session("sbsuid") <> "131001" And Session("sbsuid") <> "131002" And Session("sbsuid") <> "135010" And Session("sbsuid") <> "111001" And Session("sbsuid") <> "115002" Then
            If ViewState("MainMnu_code") = "S100199" Then
                strRedirect = "~/Students/StudBlueBookAll_ReprintAsOnDate.aspx"
            Else
                strRedirect = "~/Students/StudBlueBookAll_M.aspx"
            End If
        ElseIf Session("sbsuid") <> "111001" And Session("sbsuid") <> "115002" Then
            strRedirect = "~/Students/studBlueBookSharjah.aspx"
        Else
            strRedirect = "~/Students/studBlueBookAUH.aspx"
        End If
        MainMnu_code = Encr_decrData.Encrypt(MainMnu_code)
        Grade = Encr_decrData.Encrypt(Grade)
        Section = Encr_decrData.Encrypt(Section)
        url = String.Format("{0}?MainMnu_code={1}&Grade={2}&Section={3}", strRedirect, MainMnu_code, Grade, Section)
        ResponseHelper.Redirect(url, "_blank", "")

    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If GetBSU_ShowExpButton() = "0" Then
                    btnExport.Visible = "FALSE"
                Else
                    btnExport.Visible = "TRUE"
                End If


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If ViewState("MainMnu_code") = "S100199" Then
                    tr_ACD.Visible = True
                    btnExport.Visible = "FALSE"
                Else
                    tr_ACD.Visible = False

                End If
                txtAcdDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "S100190") And (ViewState("MainMnu_code") <> "S100199")) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("Current_ACD_ID") = "-1"
                    'Session("Current_ACY_ID") = "-1"
                    'Session("Cutoff_Age") = ""
                    'Call Academic_Cutoff()
                    Call Grade_Bind()
                    Call Section_Bind()
                    ' Call Shift_Bind()
                    ' Call Prev_Next()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub

   
    Sub Grade_Bind()
        Using readerBSU_Grade As SqlDataReader = AccessStudentClass.GetCurrent_Grades(Session("sBsuid"), Session("Current_ACD_ID"))
            ddlGrade.Items.Clear()
            ddlGrade.DataSource = readerBSU_Grade
            ddlGrade.DataValueField = "GRM_GRD_ID"
            ddlGrade.DataTextField = "GRM_DISPLAY"
            ddlGrade.DataBind()


        End Using

    End Sub

    Sub Section_Bind()
        Using readerGrade_Section As SqlDataReader = GetGrade_Section(Session("sBsuid"), Session("Current_ACD_ID"), ddlGrade.SelectedItem.Value)
            ddlSection.Items.Clear()
            ddlSection.DataSource = readerGrade_Section
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataBind()

        End Using
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "ALL"
        ddlSection.Items.Insert(0, li)
    End Sub
    Public Function GetBSU_bbsct_status() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select BSU_bMOEGRADE FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBSUID") & "'"
        Return IIf(IsDBNull(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)) = False, SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql), "False")

    End Function
    Public Function GetBSU_ShowExpButton() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select BSU_SHOW_BB_BUTTON FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBSUID") & "'"
        Return IIf(IsDBNull(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)) = False, SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql), "0")

    End Function


    Public Function GetGrade_Section(ByVal BSU_ID As String, ByVal ACD_ID As String, ByVal GRD_ID As String, Optional ByVal SHF_ID As String = "") As SqlDataReader
        'Author(--Lijo)
        'Date   --27/mar/2008--modified
        'Purpose--Get the Academic year for the active BSU and cutoff date
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGrade_Section As String = ""

        If GetBSU_bbsct_status() = False Then
            If SHF_ID = "" Then
                sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "') order by SCT_DESCR"
            Else
                sqlGrade_Section = "SELECT [SCT_ID],[SCT_DESCR] FROM [SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "'  AND [GRM_SHF_ID]='" & SHF_ID & "' ) order by SCT_DESCR"
            End If
        Else
            If SHF_ID = "" Then
                sqlGrade_Section = "select distinct STU_BB_SECTION_DES as SCT_ID,STU_BB_SECTION_DES as SCT_DESCR from student_m where   [STU_BSU_ID]='" & BSU_ID & "' and [STU_ACD_ID]='" & ACD_ID & "' and STU_BB_GRADE_DES='" & ddlGrade.SelectedItem.Text & "'  order by STU_BB_SECTION_DES"
            Else
                sqlGrade_Section = "select distinct STU_BB_SECTION_DES as SCT_ID,STU_BB_SECTION_DES as SCT_DESCR from student_m where [STU_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & BSU_ID & "' and [GRM_ACD_ID]='" & ACD_ID & "' and GRM_GRD_ID='" & GRD_ID & "'  AND [GRM_SHF_ID]='" & SHF_ID & "' ) order by SCT_DESCR"
            End If
        End If


        Dim command As SqlCommand = New SqlCommand(sqlGrade_Section, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function


    'Sub Shift_Bind()
    '    Using readerGrade_Section As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), Session("Current_ACD_ID"))
    '        Dim di_Shift As ListItem
    '        ddlShift.Items.Clear()
    '        If readerGrade_Section.HasRows = True Then
    '            While readerGrade_Section.Read
    '                di_Shift = New ListItem(readerGrade_Section("SHF_DESCR"), readerGrade_Section("SHF_ID"))
    '                ddlShift.Items.Add(di_Shift)
    '            End While
    '            For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
    '                'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
    '                If ddlShift.Items(ItemTypeCounter).Text = "NORMAL" Then
    '                    ddlShift.SelectedIndex = ItemTypeCounter
    '                End If
    '            Next
    '        Else
    '            di_Shift = New ListItem("Not Selected", "-1")
    '            ddlShift.Items.Add(di_Shift)
    '        End If

    '    End Using

    'End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        Call Section_Bind()
    End Sub

    'Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged

    'End Sub

    'Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged

    'End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty
            ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
            '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
            SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
            Dim ef As ExcelFile = New ExcelFile

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String
            Dim lstrExportType As Integer


            Dim param(30) As SqlClient.SqlParameter
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))

            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_BB_Export_Type", pParms)
                While reader.Read
                    lstrExportType = Convert.ToString(reader("BSU_BB_Export_Type"))
                End While
            End Using

            If lstrExportType = 0 Then
                If ddlSection.SelectedItem.Value = "ALL" Then
                    str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + ddlGrade.SelectedItem.Value _
                                                            & "' AND SCT_ACD_ID=" + Session("Current_ACD_ID") + " ORDER BY SCT_DESCR"
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                    Dim i As Integer = 0
                    With ds.Tables(0)
                        For i = 0 To .Rows.Count - 1
                            param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
                            param(2) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
                            param(3) = New SqlClient.SqlParameter("@SCT_ID", .Rows(i).Item(0).ToString)
                            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Export_BlueBook_Data", param)
                            Dim dtEXCEL As New DataTable
                            dtEXCEL = ds1.Tables(0)
                            If dtEXCEL.Rows.Count > 0 Then

                                Dim title3 = String.Empty
                                title3 = ds1.Tables(1).Rows(0)("Section_Descr").ToString
                                If title3.Length > 30 Then
                                    title3 = ds1.Tables(1).Rows(0)("Section_Descr").Substring(0, 30)
                                Else
                                    title3 = ds1.Tables(1).Rows(0)("Section_Descr")
                                End If

                                Dim ws As ExcelWorksheet = ef.Worksheets.Add(title3)
                                ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                                'ws.HeadersFooters.AlignWithMargins = True
                            Else
                                lblError.Text = "No Records To display with this filter condition....!!!"
                                lblError.Focus()
                            End If
                        Next
                    End With
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + "BB_DATA.xlsx")

                    Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

                    Dim pathSave As String
                    pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
                    ef.Save(cvVirtualPath & "StaffExport\" + pathSave)

                    Dim path = cvVirtualPath & "\StaffExport\" + pathSave
                    Dim bytes() As Byte = File.ReadAllBytes(path)
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/octect-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                    'HttpContext.Current.Response.ContentType = "application/octect-stream"
                    'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    'HttpContext.Current.Response.Clear()
                    'HttpContext.Current.Response.WriteFile(path)
                    'HttpContext.Current.Response.End()
                Else
                    param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
                    param(2) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
                    param(3) = New SqlClient.SqlParameter("@SCT_ID", ddlSection.SelectedItem.Value)
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Export_BlueBook_Data", param)
                    Dim dtEXCEL As New DataTable
                    dtEXCEL = ds.Tables(0)
                    If dtEXCEL.Rows.Count > 0 Then
                        Dim title4 = String.Empty
                        title4 = ds.Tables(1).Rows(0)("Section_Descr").ToString
                        If title4.Length > 30 Then
                            title4 = ds.Tables(1).Rows(0)("Section_Descr").Substring(0, 30)
                        Else
                            title4 = ds.Tables(1).Rows(0)("Section_Descr")
                        End If

                        Dim ws As ExcelWorksheet = ef.Worksheets.Add(title4)
                        ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                        '   ws.HeadersFooters.AlignWithMargins = True
                        Response.ContentType = "application/vnd.ms-excel"
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + "BB_DATA.xlsx")

                        Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

                        Dim pathSave As String
                        pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"

                        ef.Save(cvVirtualPath & "StaffExport\" + pathSave)

                        Dim path = cvVirtualPath & "\StaffExport\" + pathSave
                        Dim bytes() As Byte = File.ReadAllBytes(path)
                        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                        Response.Clear()
                        Response.ClearHeaders()
                        Response.ContentType = "application/octect-stream"
                        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                        Response.BinaryWrite(bytes)
                        Response.Flush()
                        Response.End()
                        'HttpContext.Current.Response.ContentType = "application/octect-stream"
                        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                        'HttpContext.Current.Response.Clear()
                        'HttpContext.Current.Response.WriteFile(path)
                        'HttpContext.Current.Response.End()
                    Else
                        lblError.Text = "No Records To display with this filter condition....!!!"
                        lblError.Focus()
                    End If
                End If
            Else
                param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
                param(2) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
                param(3) = New SqlClient.SqlParameter("@SCT_ID", ddlGrade.SelectedItem.Value)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Export_BlueBook_Data_ALL", param)
                Dim dtEXCEL As New DataTable
                dtEXCEL = ds.Tables(0)
                If dtEXCEL.Rows.Count > 0 Then

                    Dim title5 = String.Empty
                    title5 = ds.Tables(1).Rows(0)("Section_Descr").ToString
                    If title5.Length > 30 Then
                        title5 = ds.Tables(1).Rows(0)("Section_Descr").Substring(0, 30)
                    Else
                        title5 = ds.Tables(1).Rows(0)("Section_Descr")
                    End If

                    Dim ws As ExcelWorksheet = ef.Worksheets.Add(title5)
                    ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    ' ws.HeadersFooters.AlignWithMargins = True
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + "BB_DATA.xlsx")

                    Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()

                    Dim pathSave As String
                    pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"

                    ef.Save(cvVirtualPath & "StaffExport\" + pathSave)

                    Dim path = cvVirtualPath & "\StaffExport\" + pathSave
                    Dim bytes() As Byte = File.ReadAllBytes(path)
                    'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/octect-stream"
                    Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    Response.BinaryWrite(bytes)
                    Response.Flush()
                    Response.End()
                    'HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                    'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
                    'HttpContext.Current.Response.Clear()
                    'HttpContext.Current.Response.WriteFile(path)
                    'HttpContext.Current.Response.End()
                Else
                    lblError.Text = "No Records To display with this filter condition....!!!"
                    lblError.Focus()
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "btnExport_ClickBluebook")
        End Try


    End Sub
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
End Class
