<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studStrikeOffRecommend_View.aspx.vb" Inherits="Students_studStrikeOffRecommend_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblTitle" runat="server"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
                    cellspacing="0">

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center" cellpadding="0" cellspacing="0"  Width="100%">

                                <tr>
                                    <td align="left" width="25%">
                                        <span class="field-label">Select Academic Year</span> </td>

                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>



                             <td align="center" colspan="4">
                                        <asp:GridView ID="gvStud" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student No">
                                                    <HeaderTemplate>
                                                              <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label>
                                                            <br />
                                                              <asp:TextBox ID="txtStuNo" runat="server" Width="75%"></asp:TextBox>
                                                              <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuNo_Click" />
                                                                            

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>

                                                    </ItemTemplate>

                                                   <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                    <HeaderTemplate>
                                                          <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label>
                                                          <br /> 
                                                          <asp:TextBox ID="txtStuName" runat="server" Width="75%"></asp:TextBox>
                                                          <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click" />
                                                                            

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>

                                                    </ItemTemplate>

                                                  <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                            <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtGrade" runat="server" Width="75%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                                                       

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <HeaderTemplate>
                                                     
                                                        <asp:Label ID="lblH123" runat="server" CssClass="gridheader_text" Text="Section"></asp:Label>
                                                           <br />
                                                       <asp:TextBox ID="txtSection" runat="server" Width="75%"></asp:TextBox>
                                                       <asp:ImageButton ID="btnSection_Search" runat="server" ImageAlign="middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSection_Search_Click" />
                                                                        

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:ButtonField CommandName="view" Text="View" HeaderText="View">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                </asp:ButtonField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server"
                                type="hidden" value="=" />
                        </td>
                    </tr>


                </table>



            </div>
        </div>
    </div>

</asp:Content>

