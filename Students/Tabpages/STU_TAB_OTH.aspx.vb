Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class STU_TAB_OTH
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    'Protected Sub FeeSponsor_Update(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If e.ToString = "1" Then
    '        mpxFeeSponsor.Show()
    '    Else
    '        mpxFeeSponsor.Hide()
    '    End If
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        ''str_query = "SELECT BM_ID,BM_ENTRY_DATE,EMP_FNAME,EMP_LNAME " & _
        ''                "FROM BM.BM_MASTER A INNER JOIN " & _
        ''                "EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID ORDER BY BM_ENTRY_DATE DESC"

        'Dim str_query = "SELECT DISTINCT A.BM_ID, A.BM_INCIDENT_DATE,A.BM_ENTRY_DATE,D.EMP_FNAME,D.EMP_LNAME FROM BM.BM_MASTER A " & _
        '         "INNER JOIN BM.BM_ACTION_MASTER B ON A.BM_ID= B.BM_ID " & _
        '         "INNER JOIN STUDENT_M C ON C.STU_ID= B.BM_STU_ID " & _
        '         "INNER JOIN EMPLOYEE_M D ON D.EMP_ID= A.BM_REPORTING_STAFF_ID and C.stu_id='20131750'"

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'gvBadDetails.DataSource = ds
        'gvBadDetails.DataBind()
        Try
            If Page.IsPostBack = False Then
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ViewState("stu_id") = Session("DB_Stu_ID").ToString
                HF_stuid.Value = ViewState("stu_id")

                BindGrid_POS() ' Psoitive

            End If
        Catch ex As Exception

        End Try


    End Sub
    Protected Function GetNavigateUrl1(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('stu_ActionViewDetails.aspx?Info_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
    End Function
    Function GetDef_EQSID(ByVal STU_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetEQS_ID", pParms)
        Return reader
    End Function
    Private Sub BindGrid_POS()
        Dim EQS_ID
        Try

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString



            Using DefReader As SqlDataReader = GetDef_EQSID(HF_stuid.Value)
                While DefReader.Read
                    EQS_ID = Convert.ToString(DefReader("EQS_ID"))


                End While
            End Using



            Dim str_query = " SELECT STM_ENR_COMMENTS,STM_ENTRY_DATE, " & _
                            " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide, " & _
                            "  (substring(STM_ENR_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1 " & _
                            " From Student_M A INNER JOIN Student_MISC B ON A.STU_ID=B.STM_STU_ID " & _
                            " WHERE STU_ID='" & HF_stuid.Value & "'"
            '" order by STM_LOG_ID desc "
            Dim str_query2 = " SELECT STM_ENR_COMMENTS,STM_ENTRY_DATE, " & _
                            " '<span style=''color: red;font-weight:bold''>Hide</span>' as hide, " & _
                            "  (substring(STM_ENR_COMMENTS,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview1 " & _
                            " From Enquiry_SchoolPrio_S A INNER JOIN Student_MISC B ON A.EQS_ID=B.STM_STU_ID AND MSC_BSU_ID='" & Session("sBSUID") & "'" & _
                            " WHERE EQS_ID='" & EQS_ID & "'"


            str_query = str_query + " UNION " + str_query2 + "order by STM_ENTRY_DATE desc"
            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade_POS.DataBind()
                Dim columnCount As Integer = gvStudGrade_POS.Rows(0).Cells.Count
                gvStudGrade_POS.Rows(0).Cells.Clear()
                gvStudGrade_POS.Rows(0).Cells.Add(New TableCell)
                gvStudGrade_POS.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade_POS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade_POS.Rows(0).Cells(0).Text = "No Details...."

            Else
                gvStudGrade_POS.DataSource = dsDetails
                gvStudGrade_POS.DataBind()

            End If

        Catch ex As Exception

        End Try

    End Sub





End Class
