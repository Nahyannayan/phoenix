Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class STU_TAB_MED
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ViewState("viewid") = Session("DB_Stu_ID")
                HF_stuid.Value = ViewState("viewid")
                binddetails(HF_stuid.Value)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub binddetails(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Dim temp_Gender, temp_PrimContact As String
        Dim temp_Trans, temp_SMS, temp_Mail, temp_Health, temp_bActive As Boolean
        'Dim temp_HOUSE, temp_Blood, temp_Type, temp_Rlg_ID, temp_Nationality, temp_COB, temp_MINLIST, temp_MINLISTTYPE, temp_PrefContact As String
        Dim arInfo As String() = New String(2) {}
        Dim Temp_Phone_Split As String = String.Empty
        Dim splitter As Char = "-"
        'arInfo = info.Split(splitter)
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString

            Dim PARAM(1) As SqlClient.SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("viewid"))




            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "STU.GETHEALTHINFO", PARAM)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        'Literal2.Text = Convert.ToString(readerStudent_Detail("SHEALTH"))
                        'Literal3.Text = Convert.ToString(readerStudent_Detail("SPHYSICAL"))
                        'Literal4.Text = Convert.ToString(readerStudent_Detail("SPMEDICATION"))
                        'Ltl_bGroup.Text = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        'Literal1.Text = Convert.ToString(readerStudent_Detail("STU_HCNO"))
                        'temp_Health = Convert.ToBoolean(readerStudent_Detail("SbRCVSPMEDICATION"))
                        'If temp_Health Then
                        '    Literal5.Text = "YES" + " " + Convert.ToString(readerStudent_Detail("STU_REMARKS"))
                        'Else
                        '    Literal5.Text = "NO"
                        'End If



                        ltHth_No.Text = Convert.ToString(readerStudent_Detail("STU_HCNO"))
                        ltB_grp.Text = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        ltAlg_Detail.Text = Convert.ToString(readerStudent_Detail("STU_bALLERGIES"))
                        divAlg.InnerText = Convert.ToString(readerStudent_Detail("STU_ALLERGIES"))

                        ltSp_med.Text = Convert.ToString(readerStudent_Detail("STU_bRCVSPMEDICATION"))
                        divSp_med.InnerText = Convert.ToString(readerStudent_Detail("STU_SPMEDICATION"))

                        ltPhy_edu.Text = Convert.ToString(readerStudent_Detail("STU_bPRESTRICTIONS"))
                        divPhy_edu.InnerText = Convert.ToString(readerStudent_Detail("STU_PHYSICAL"))

                        ltHth_Info.Text = Convert.ToString(readerStudent_Detail("STU_bHRESTRICTIONS"))
                        divHth_Info.InnerText = Convert.ToString(readerStudent_Detail("STU_HEALTH"))

                        ltLS_Ther.Text = Convert.ToString(readerStudent_Detail("STU_bTHERAPHY"))
                        divLS_Ther.InnerText = Convert.ToString(readerStudent_Detail("STU_THERAPHY"))

                        ltSEN.Text = Convert.ToString(readerStudent_Detail("STU_bSEN"))
                        divSEN.InnerText = Convert.ToString(readerStudent_Detail("STU_SEN_REMARK"))

                        ltEAL.Text = Convert.ToString(readerStudent_Detail("STU_bEAL"))
                        divEAL.InnerText = Convert.ToString(readerStudent_Detail("STU_EAL_REMARK"))

                        'ltSEA.Text = Convert.ToString(readerStudent_Detail("STU_bENRICH"))
                        'divSEA.InnerText = Convert.ToString(readerStudent_Detail("STU_ENRICH"))

                        'ltMus_Prof.Text = Convert.ToString(readerStudent_Detail("STU_bMUSICAL"))
                        'divMus_Prof.InnerText = Convert.ToString(readerStudent_Detail("STU_MUSICAL"))

                        'ltSport.Text = Convert.ToString(readerStudent_Detail("STU_bSPORTS"))
                        'divSport.InnerText = Convert.ToString(readerStudent_Detail("STU_SPORTS"))
                        ltPrev_sch.Text = Convert.ToString(readerStudent_Detail("STU_bBEHAVIOUR"))
                        divPrev_sch.InnerText = Convert.ToString(readerStudent_Detail("STU_BEHAVIOUR"))

                    End While

                Else
                End If

            End Using
        Catch ex As Exception

        End Try

    End Sub

End Class
