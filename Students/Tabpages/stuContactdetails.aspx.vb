Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_stuContactdetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ViewState("viewid") = Session("DB_Stu_ID")
                HF_stuid.Value = ViewState("viewid")
                binddetails(HF_stuid.Value)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub binddetails(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim ds As DataSet
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "stu_contactDetails", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        Ltl_Fname.Text = Convert.ToString(readerStudent_Detail("STS_FNAME"))
                        Ltl_Fnationality.Text = Convert.ToString(readerStudent_Detail("FNATIONALITY"))
                        Ltl_Fpob.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPOBOX"))
                        Ltl_FEmirate.Text = Convert.ToString(readerStudent_Detail("STS_FEMIR"))
                        Ltl_FPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_FRESPHONE"))
                        Ltl_FOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_FOFFPHONE"))
                        Ltl_FMobile.Text = Convert.ToString(readerStudent_Detail("STS_FMOBILE"))
                        If Convert.ToString(readerStudent_Detail("STS_FEMAIL")).Trim <> "" Then
                            Ltl_FEmail.Text = "<a href='mailto:" & Convert.ToString(readerStudent_Detail("STS_FEMAIL")) & "' >" & Convert.ToString(readerStudent_Detail("STS_FEMAIL")) & "</a>"
                        End If

                        Ltl_FFax.Text = Convert.ToString(readerStudent_Detail("STS_FFAX"))
                        Ltl_FOccupation.Text = Convert.ToString(readerStudent_Detail("STS_FOCC"))
                        Ltl_FCompany.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPANY"))

                        Ltl_Mname.Text = Convert.ToString(readerStudent_Detail("STS_MNAME"))
                        Ltl_Mnationality.Text = Convert.ToString(readerStudent_Detail("MNATIONALITY"))
                        Ltl_Mpob.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPOBOX"))
                        Ltl_MEmirate.Text = Convert.ToString(readerStudent_Detail("STS_MEMIR"))
                        Ltl_MPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_MRESPHONE"))
                        Ltl_MOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_MOFFPHONE"))
                        Ltl_MMobile.Text = Convert.ToString(readerStudent_Detail("STS_MMOBILE"))
                        ' Ltl_MEmail.Text = Convert.ToString(readerStudent_Detail("STS_MEMAIL"))

                        If Convert.ToString(readerStudent_Detail("STS_MEMAIL")).Trim <> "" Then
                            Ltl_MEmail.Text = "<a href='mailto:" & Convert.ToString(readerStudent_Detail("STS_MEMAIL")) & "' >" & Convert.ToString(readerStudent_Detail("STS_MEMAIL")) & "</a>"
                        End If

                        Ltl_MFax.Text = Convert.ToString(readerStudent_Detail("STS_MFAX"))
                        Ltl_MOccupation.Text = Convert.ToString(readerStudent_Detail("STS_MOCC"))
                        Ltl_MCompany.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPANY"))

                        Ltl_Gname.Text = Convert.ToString(readerStudent_Detail("STS_GNAME"))
                        Ltl_Gnationality.Text = Convert.ToString(readerStudent_Detail("GNATIONALITY"))
                        Ltl_Gpob.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPOBOX"))
                        Ltl_GEmirate.Text = Convert.ToString(readerStudent_Detail("STS_GEMIR"))
                        Ltl_GPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_GRESPHONE"))
                        Ltl_GOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_GOFFPHONE"))
                        Ltl_GMobile.Text = Convert.ToString(readerStudent_Detail("STS_GMOBILE"))
                        'Ltl_GEmail.Text = Convert.ToString(readerStudent_Detail("STS_GEMAIL"))
                        If Convert.ToString(readerStudent_Detail("STS_GEMAIL")).Trim <> "" Then
                            Ltl_GEmail.Text = "<a href='mailto:" & Convert.ToString(readerStudent_Detail("STS_GEMAIL")) & "' >" & Convert.ToString(readerStudent_Detail("STS_GEMAIL")) & "</a>"
                        End If

                        Ltl_GFax.Text = Convert.ToString(readerStudent_Detail("STS_GFAX"))
                        Ltl_GOccupation.Text = Convert.ToString(readerStudent_Detail("STS_GOCC"))
                        Ltl_GCompany.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPANY"))

                        Dim col As Integer
                        If Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")) = "F" Then
                            col = 2
                        ElseIf Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")) = "M" Then
                            col = 3

                        ElseIf Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")) = "G" Then
                            col = 4
                        Else
                            col = 2
                        End If
                        table1.Rows(0).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(1).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(2).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(3).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(4).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(5).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(6).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(7).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(8).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(9).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(10).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(11).Cells(col).BgColor = "#FFFFC"

                    End While
                Else
                    lblerror.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

End Class
