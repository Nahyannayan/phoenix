<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Dash_ManageSMS.ascx.vb"
    Inherits="Dash_ManageSMS" %>

<script language="javascript">
    function openPopup(strOpen) {


        var sFeatures;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 600px; ";

        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";


        var result;
        result = window.showModalDialog(strOpen, "", sFeatures);
        if (result == "1") {
            window.location.reload(true);
        }

    }
     
</script>

  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">
            SMS Sent
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:GridView ID="GridMSMS" AutoGenerateColumns="false" Width="100%" runat="server"  CssClass="table table-bordered table-row"
                AllowPaging="True" EmptyDataText="No SMS sent"
                OnPageIndexChanging="GridMSMS_PageIndexChanging" PageSize="20">
                <Columns>
                  <asp:TemplateField HeaderText="ID">
                        <HeaderTemplate>
                            
                                      ID
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("LOG_CMS_ID")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sms Text">
                        <HeaderTemplate>
                           
                                        Sms Text
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                            <asp:Panel ID="T12Panel1" runat="server" >
                                <%#Eval("cms_sms_text")%>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                TextLabelID="T12lblview">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                     <asp:TemplateField HeaderText="Mobile Number">
                        <HeaderTemplate>
                           
                                       Mobile Number
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("LOG_MOBILE_NUMBER")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Sent Date">
                        <HeaderTemplate>
                            
                                        Sent Date
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("LOG_ENTRY_DATE")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Status">
                        <HeaderTemplate>
                           
                                      Status
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("status")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <HeaderStyle />
                <RowStyle CssClass="griditem"  />
                <SelectedRowStyle  />
                <AlternatingRowStyle CssClass="griditem_alternative"  />
                <EmptyDataRowStyle />
                <EditRowStyle  />
            </asp:GridView>
        </td>
    </tr>
</table>
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">
           Newsletter and Plain Text Emails
        </td>
    </tr>
    <tr>
        <td align="center" >
            <asp:GridView ID="GridEmails" AutoGenerateColumns="false" Width="100%" runat="server"  CssClass="table table-bordered table-row"
                AllowPaging="True" EmptyDataText="No Emails Sent"
               PageSize="20">
                <Columns>
                  <asp:TemplateField HeaderText="ID">
                        <HeaderTemplate>
                           
                                      ID
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("LOG_EML_ID")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sms Text">
                        <HeaderTemplate>
                           
                                      Email Title
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                           <%#Eval("EML_TITLE")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Type">
                        <HeaderTemplate>
                           
                                    Type
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("EmailType")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                     <asp:TemplateField HeaderText="Email ID">
                        <HeaderTemplate>
                           
                                     Email ID
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("LOG_EMAIL_ID")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Sent Date">
                        <HeaderTemplate>
                           
                                        Sent Date
                                  
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("LOG_ENTRY_DATE")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Status">
                        <HeaderTemplate>
                           
                                      Status
                                   
                        </HeaderTemplate>
                        <ItemTemplate>
                            <center>
                                <%#Eval("status")%></center>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <HeaderStyle />
                <RowStyle CssClass="griditem"  />
                <SelectedRowStyle  />
                <AlternatingRowStyle CssClass="griditem_alternative"  />
                <EmptyDataRowStyle  />
                <EditRowStyle  />
            </asp:GridView>
            
        </td>
    </tr>
</table>

