<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comManageSMS.ascx.vb" Inherits="masscom_UserControls_comManageSMS" %>
  <script language="javascript">
    function openPopup(strOpen)
    {
   
       
         var sFeatures;
            sFeatures="dialogWidth: 600px; ";
            sFeatures+="dialogHeight: 600px; ";
          
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

          
            var result;
            result = window.showModalDialog(strOpen,"", sFeatures);
            if (result=="1")
            {
            window.location.reload(true);
            }

    }
     
    </script>

    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr class="title">
            <td align="left">
                SMS TEMPLATES
            </td>
        </tr>
    </table>
    <br />
    <table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700px">
        <tr>
            <td class="subheader_img">
                SMS Templates</td>
        </tr>
        <tr>
            <td>
                <table border="0" cellpadding="0" class="matters" cellspacing="0" width="700px">
                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="lnkAddNew" OnClientClick="javascript:openPopup('comCreateSmsText.aspx')"
                                runat="server">Add New</asp:LinkButton>
                            <asp:GridView ID="GridMSMS" AutoGenerateColumns="false" width="100%" runat="server" AllowPaging="True"
                                EmptyDataText="No SMS templates added yet (or) Search query did not produce any results"
                                OnPageIndexChanging="GridMSMS_PageIndexChanging" PageSize="20" OnRowCommand="GridMSMS_RowCommand">
                                <columns>
<asp:TemplateField HeaderText="Sl No">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                               Sl No
                                <br />
                                <asp:TextBox ID="Txt1" Width="50px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch1" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                                
                                </td>
                              </tr>
                          </table>
</HeaderTemplate>

<ItemTemplate>
         <asp:HiddenField ID="Hiddenid" Value='<%# Eval("CMS_ID") %>' runat="server" />
    <center><%# Eval("CMS_ID") %></center>  
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Create Date">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                                Create Date
                                <br />
                                <asp:TextBox ID="Txt2" width="100px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch2" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                                <ajaxToolkit:CalendarExtender ID="CE1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Txt2" TargetControlID="Txt2"></ajaxToolkit:CalendarExtender>

                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>

<ItemTemplate>
     <center>   <%#Eval("CMS_DATE", "{0:dd/MMM/yyyy}")%></center> 
        
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Sms Text">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">
                                Sms Text
                                <br />
                                <asp:TextBox ID="Txt3" width="300px" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageSearch3" runat="server" CausesValidation="false" CommandName="search" ImageUrl="~/Images/forum_search.gif"/>
                                
                                </td>
                              </tr>
                          </table>
                          </HeaderTemplate>

<ItemTemplate>
      
        <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label><%-- <asp:LinkButton ID="T3lnkView" OnClientClick="javascript:return false;" runat="server">View</asp:LinkButton>--%>
                            <asp:Panel ID="T12Panel1" runat="server" Height="50px">
                                <%#Eval("cms_sms_text")%>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                AutoCollapse="False" AutoExpand="False" CollapseControlID="T12lblview" Collapsed="true"
                                CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="T12lblview"
                                ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                TextLabelID="T12lblview">
                            </ajaxToolkit:CollapsiblePanelExtender>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="View">
<HeaderTemplate>
                          <table class="BlueTable" width="100%">
                               <tr class="matterswhite">
                                 <td align="center" colspan="2">

                                 <br />
                                  View
                                 <br />
                                 <br />
                                 </td>
                              </tr>
                          </table>
</HeaderTemplate>
<ItemTemplate>



    <a href="javascript:openPopup('comViewSmsText.aspx?cmsid=<%# Eval("CMS_ID") %>')"><asp:Image ID="Image1"  ImageUrl="~/Images/View.png" Height="25" Width="50" runat="server" /></a>

     

</ItemTemplate>
</asp:TemplateField>
</columns>
                                <headerstyle height="30px" cssclass="gridheader_pop" wrap="False" />
                                <rowstyle cssclass="griditem" height="25px" wrap="False" />
                                <selectedrowstyle cssclass="Green" wrap="False" />
                                <alternatingrowstyle cssclass="griditem_alternative" wrap="False" />
                                <emptydatarowstyle wrap="False" />
                                <editrowstyle wrap="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>