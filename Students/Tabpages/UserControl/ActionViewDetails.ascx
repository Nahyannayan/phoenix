<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ActionViewDetails.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_ActionViewDetails" %>
 <div align="center">
 <%--<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700px" >
                        <tr>
                            <td class="subheader_img">
                                Primary Information&nbsp;<asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/Images/tick.gif"
                                    OnClientClick="javascript:window.print(); return false;" ToolTip="Print Page" /></td>
                        </tr>
                        <tr>
                            <td align="left" >
<table width="100%">
    <tr>
        <td colspan="6" bgcolor="#003366">
            <strong><span style="font-size: 11pt; color: #ffffff; font-family: Arial">Student Information</span></strong></td>
    </tr>
    <tr>
        <td >
            Student Name</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentname" runat="server"></asp:Label></td>
        <td >
        </td>
        <td >
        </td>
        <td >
        </td>
    </tr>
    <tr>
        <td >
            Grade</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentgrade" runat="server"></asp:Label></td>
        <td >
            Shift
        </td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentshift" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td >
            Section</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentsection" runat="server"></asp:Label></td>
        <td >
            Stream</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentstream" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="6" bgcolor="#003366"><strong><span style="font-size: 11pt; color: #ffffff; font-family: Arial">Parent Information</span></strong>
            </td>
    </tr>
    <tr>
        <td >
            Parent Name</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblparentname" runat="server"></asp:Label></td>
        <td >
        </td>
        <td >
        </td>
        <td >
        </td>
    </tr>
    <tr>
        <td >
            Email ID</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblparentemailid" runat="server"></asp:Label></td>
        <td >
        </td>
        <td >
        </td>
        <td >
        </td>
    </tr>
    <tr>
        <td >
            Contact Number</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblparentcontactnumber" runat="server"></asp:Label></td>
        <td >
        </td>
        <td >
        </td>
        <td >
        </td>
    </tr>
</table>
 </td>
                    </tr>
               </table>--%>
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
<tr>
        <td class="subheader_img" style="width: 709px">
            Incident Details-(<asp:Label ID="lbltype" runat="server" Text=""></asp:Label>
            )<asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/Images/Misc/print.gif"
                                    OnClientClick="javascript:window.print(); return false;" ToolTip="Print Page" Height="18px" Width="26px" /></td>
    </tr>
    <tr>
        <td align="left" style="width: 709px">
            <table width="100%" id="tabWitness">
                <tr>
                    <td colspan="6" bgcolor="#003366">
                    <strong><span style="font-size: 11pt; color: #ffffff; font-family: Arial">Reporting Informations</span></strong>
                        </td>
                </tr>
                <tr>
                    <td>
                        Reported
                        Date</td>
                    <td>
                        :</td>
                    <td colspan="4">
                        <asp:Label ID="lbltodaydate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        Staff Reporting the Incident</td>
                    <td>
                        :</td>
                    <td colspan="4">
                        <asp:Label ID="lblreportingstaff" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        Incident Date</td>
                    <td>
                        :</td>
                    <td colspan="4">
                        <asp:Label ID="lblincidentdate" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="6" >
                  <b><u>Report on the incident</u></b>
                       </td>
                </tr>
                <tr>
                    <td colspan="6">
             
                        <asp:Label ID="txtReportonincident" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="6" bgcolor="#003366">
                 <strong><span style="font-size: 11pt; color: #ffffff; font-family: Arial">&nbsp;Witnesses
                     spoken to</span></strong>
                    </td>
                </tr>
            </table>
            <asp:Table ID="tabWitnessDet" runat="server" Width="686px">
            </asp:Table>
        </td>
    </tr>
</table>
<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" width="700">
    <tr>
        <td class="subheader_img">
            Action Taken</td>
    </tr>
    <tr>
        <td align="left">
            <table width="100%">
             <tr>
                    <td colspan="6" bgcolor="#003366">
                     <strong><span style="font-size: 11pt; color: #ffffff; font-family: Arial">Parents Called
                         / Interviewed</span></strong>
                       </td>
                </tr>
                <tr>
                    <td>
                        Parent Called</td>
                    <td>
                        :</td>
                    <td colspan="2">
                        <asp:RadioButtonList ID="R1" runat="server"  RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem><asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                        <td>
                            :</td>
                            <td><asp:TextBox ID="txtCalldate" runat="server" BorderWidth="0px" ReadOnly="True"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="6">
                       
                        <asp:Label ID="txtparentscalledsaid" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        Parent Interviewed</td>
                    <td>
                        :</td>
                    <td colspan="2">
                        <asp:RadioButtonList ID="R2" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem><asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                        <td>
                            :</td>
                            <td><asp:TextBox ID="TxtIntvDate" runat="server" BorderWidth="0px" ReadOnly="True"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="6">
                      
                        <asp:Label ID="txtparentsinterviewssaid" runat="server"></asp:Label></td>
                </tr>
                 <tr>
                    <td colspan="6" bgcolor="#003366">
                     <strong><span style="font-size: 11pt; color: #ffffff; font-family: Arial">Other Details</span></strong>
                       </td>
                </tr>
                <tr>
                    <td>
                        Notes in student's planner</td>
                    <td>
                        :</td>
                    <td>
                        <asp:RadioButtonList ID="R3" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem  Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td>
                        Date</td>
                    <td>
                        :</td>
                    <td>
                        <asp:TextBox ID="T1" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Break detention given</td>
                    <td>
                        :</td>
                    <td>
                        <asp:RadioButtonList ID="R4" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem  Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td>
                        Date</td>
                    <td>
                        :</td>
                    <td>
                        <asp:TextBox ID="T2" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        After school detention given</td>
                    <td>
                        :</td>
                    <td>
                        <asp:RadioButtonList ID="R5" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td>
                        Date</td>
                    <td>
                        :</td>
                    <td>
                        <asp:TextBox ID="T3" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Suspension</td>
                    <td>
                        :</td>
                    <td>
                        <asp:RadioButtonList ID="R6" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td>
                        Date</td>
                    <td>
                        :</td>
                    <td>
                        <asp:TextBox ID="T4" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Referred to students counsellor
                    </td>
                    <td>
                        :</td>
                    <td>
                        <asp:RadioButtonList ID="R7" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td>
                        Date</td>
                    <td>
                        :</td>
                    <td>
                        <asp:TextBox ID="T5" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="6" bgcolor="#003366">
                     <strong><span style="font-size: 11pt; color: #ffffff; font-family: Arial">Any Notes/Followup</span></strong>
                        </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:Table ID="tabFollowUp" runat="server" Width="686px">
                        </asp:Table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenincidentid" runat="server" />
     <asp:HiddenField ID="HidActionID" runat="server" />
</div>