Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Dash_ManageSMS
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            BindGrid()
            BindGridEmail()
        End If

    End Sub


    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " select LOG_CMS_ID,CMS_SMS_TEXT,LOG_MOBILE_NUMBER,REPLACE(CONVERT(VARCHAR(11), LOG_ENTRY_DATE, 106), ' ', '/')LOG_ENTRY_DATE,'<span style=''color: red;font-weight:bold''>Hide</span>' as hide,(substring(cms_sms_text,0,50)+ '</br><span style=''color: red;font-weight:bold''> more... </span>')tempview, " & _
                        " case charindex('Error',log_status) when '0' then'Success' else 'Failed' end status from dbo.COM_LOG_SMS_TABLE  A " & _
                        " inner join dbo.COM_MANAGE_SMS B ON A.LOG_CMS_ID=B.CMS_ID " & _
                        " where log_unique_id=( select STU_NO from dbo.STUDENT_M where STU_ID='" & Session("DB_Stu_ID") & "' ) "
        str_query += " order by LOG_CMS_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GridMSMS.DataSource = ds
        GridMSMS.DataBind()


    End Sub

    Public Sub BindGridEmail()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " select LOG_EML_ID,EML_TITLE,LOG_EMAIL_ID,REPLACE(CONVERT(VARCHAR(11), LOG_ENTRY_DATE, 106), ' ', '/')LOG_ENTRY_DATE, " & _
                        " case EML_NEWS_LETTER when '1' then'Newsletter' else 'Plain Text' end EmailType, " & _
                        " case charindex('Error',log_status) when '0' then'Success' else 'Failed' end status " & _
                        " from  dbo.COM_LOG_EMAIL_TABLE A INNER JOIN dbo.COM_MANAGE_EMAIL B ON A.LOG_EML_ID=B.EML_ID " & _
                        " WHERE LOG_UNIQUE_ID=(select STU_NO from dbo.STUDENT_M where STU_ID='" & Session("DB_Stu_ID") & "' ) "

        str_query += " order by LOG_EML_ID desc "


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GridEmails.DataSource = ds
        GridEmails.DataBind()


    End Sub

    Protected Sub GridMSMS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridMSMS.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    
    Protected Sub GridEmails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridEmails.PageIndexChanging
        GridEmails.PageIndex = e.NewPageIndex
        BindGridEmail()
    End Sub
End Class
