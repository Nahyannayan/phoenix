Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class STU_TAB_TPT
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ViewState("viewid") = Session("DB_Stu_ID")
                HF_stuid.Value = ViewState("viewid")
                binddetails(HF_stuid.Value)
                BindServices(HF_stuid.Value)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub binddetails(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", stu_id)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.TRANSPORT_DETAILS", param)
            If ds.Tables(0).Rows.Count > 0 Then
                ltPickUp_Loc.Text = ds.Tables(0).Rows(0)("PAREA").ToString
                ltDropOff_Loc.Text = ds.Tables(0).Rows(0)("DAREA").ToString


                ltPickUp_SubLoc.Text = ds.Tables(0).Rows(0)("PICKUP").ToString
                ltDropOff_SubLoc.Text = ds.Tables(0).Rows(0)("DROPOFF").ToString


                ltPickUp_BusNo.Text = ds.Tables(0).Rows(0)("ONWARDBUS").ToString
                ltDropOff_BusNo.Text = ds.Tables(0).Rows(0)("RETURNBUS").ToString



                ltPickUp_Trip.Text = ds.Tables(0).Rows(0)("ONWARDTRIP").ToString
                ltDropOff_Trip.Text = ds.Tables(0).Rows(0)("RETURNTRIP").ToString
            
            End If
        Catch ex As Exception
            lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub

    Sub BindServices(ByVal Stu_id As String)

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", Stu_id)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STU.GETSERVICE_DETAILS", param)
            If ds.Tables(0).Rows.Count > 0 Then

                gvServices.DataSource = ds.Tables(0)
                gvServices.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ' ds.Tables(0).Rows(0)(4) = True

                gvServices.DataSource = ds.Tables(0)
                Try
                    gvServices.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvServices.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvServices.Rows(0).Cells.Clear()
                gvServices.Rows(0).Cells.Add(New TableCell)
                gvServices.Rows(0).Cells(0).ColumnSpan = columnCount
                gvServices.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvServices.Rows(0).Cells(0).Text = "No record found."
            End If

        Catch ex As Exception
            lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub



    Protected Sub lnkinsertnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkinsertnew.Click


        Dim lblStuId As Label
        Dim lblStuName As Label
        Dim lblStuNo As Label
        Dim lblGrade As Label
        Dim lblSection As Label
        Dim lblStatus As Label



        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))


        param.Add("stu_id", ViewState("viewid"))
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../../Transport/Reports/RPT/rptstudTransportAudit1.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        Response.Redirect("~/Reports/ASPX Report/rptReportViewer_Princi.aspx")

    End Sub

End Class
