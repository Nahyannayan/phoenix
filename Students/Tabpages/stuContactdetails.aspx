<%@ Page Language="VB" AutoEventWireup="false" CodeFile="stuContactdetails.aspx.vb" Inherits="Students_stuContactdetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
<%--  <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>

    
    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr >
                <td align="left" class="title-bg-lite">
                    Contact Details</td>
            </tr>
            <tr>
                <td rowspan="4" valign="top">
                    <table id="table1" runat="server"  width="100%"  class="table table-bordered">
                        <tr>
                            <td align="left">
                            </td>
                           
                            <td align="left">
                               <span  class="field-label"> Father</span></td>
                            <td align="left">
                               <span  class="field-label"> Mother</span></td>
                            <td align="left">
                               <span  class="field-label"> Guardian</span></td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span  class="field-label"> Name</span></td>
                        
                            <td align="left">
                                <asp:Literal ID="Ltl_Fname" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_Mname" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_Gname" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span  class="field-label"> Nationality</span></td>
                       
                            <td align="left">
                                <asp:Literal ID="Ltl_Fnationality" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_Mnationality" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_Gnationality" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span  class="field-label"> Comm.POB</span></td>
                         
                            <td align="left">
                                <asp:Literal ID="Ltl_Fpob" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_Mpob" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_Gpob" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                              <span  class="field-label">  City/Emirate</span></td>
                       
                            <td align="left">
                                <asp:Literal ID="Ltl_FEmirate" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_MEmirate" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_GEmirate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span  class="field-label"> Phone Res</span></td>
                        
                            <td align="left">
                                <asp:Literal ID="Ltl_FPhoneRes" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_MPhoneRes" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_GPhoneRes" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                              <span  class="field-label">  Office Phone</span></td>
                      
                            <td align="left">
                                <asp:Literal ID="Ltl_FOfficePhone" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_MOfficePhone" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_GOfficePhone" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                              <span  class="field-label">  Mobile</span></td>
                      
                            <td align="left">
                                <asp:Literal ID="Ltl_FMobile" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_MMobile" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_GMobile" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                              <span  class="field-label">  Email</span></td>
                        
                            <td align="left">
                                <asp:Literal ID="Ltl_FEmail" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_MEmail" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_GEmail" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                               <span  class="field-label"> Fax</span></td>
                        
                            <td align="left">
                                <asp:Literal ID="Ltl_FFax" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_MFax" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_GFax" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td align="left">
                              <span  class="field-label">  Occupation</span></td>
                         
                            <td align="left">
                                <asp:Literal ID="Ltl_FOccupation" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_MOccupation" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_GOccupation" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                        
                            <td align="left">
                               <span  class="field-label"> Company</span></td>
                      
                            <td align="left">
                                <asp:Literal ID="Ltl_FCompany" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_MCompany" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:Literal ID="Ltl_GCompany" runat="server"></asp:Literal></td>
                        </tr>
                    </table>
                   </td>
            </tr>
        </table>
    
    </div>
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <asp:HiddenField ID="HF_stuid" runat="server" />
    </form>
</body>
</html>
