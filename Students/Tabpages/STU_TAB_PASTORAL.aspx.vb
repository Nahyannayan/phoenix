Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class STU_TAB_PASTORAL
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    'Protected Sub FeeSponsor_Update(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If e.ToString = "1" Then
    '        mpxFeeSponsor.Show()
    '    Else
    '        mpxFeeSponsor.Hide()
    '    End If
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        ''str_query = "SELECT BM_ID,BM_ENTRY_DATE,EMP_FNAME,EMP_LNAME " & _
        ''                "FROM BM.BM_MASTER A INNER JOIN " & _
        ''                "EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID ORDER BY BM_ENTRY_DATE DESC"

        'Dim str_query = "SELECT DISTINCT A.BM_ID, A.BM_INCIDENT_DATE,A.BM_ENTRY_DATE,D.EMP_FNAME,D.EMP_LNAME FROM BM.BM_MASTER A " & _
        '         "INNER JOIN BM.BM_ACTION_MASTER B ON A.BM_ID= B.BM_ID " & _
        '         "INNER JOIN STUDENT_M C ON C.STU_ID= B.BM_STU_ID " & _
        '         "INNER JOIN EMPLOYEE_M D ON D.EMP_ID= A.BM_REPORTING_STAFF_ID and C.stu_id='20131750'"

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'gvBadDetails.DataSource = ds
        'gvBadDetails.DataBind()
        Try
            If Page.IsPostBack = False Then
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ViewState("stu_id") = Session("DB_Stu_ID").ToString
                HF_stuid.Value = ViewState("stu_id")
                BindGrid() ' Negative
                BindGrid_POS() ' Psoitive
                BindGrid_ACHV()
            End If
        Catch ex As Exception

        End Try


    End Sub
    Protected Function GetNavigateUrl1(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('stu_ActionViewDetails.aspx?Info_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
    End Function
    Private Sub BindGrid()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
                            " BM_INCIDENT_TYPE,BM_CATEGORY_SCORE as BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
                            " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName FROM BM.BM_MASTER A " & _
                            " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
                            " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
                            " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
                            " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=A.BM_CATEGORYID " & _
                            " WHERE S.STU_ID =" & HF_stuid.Value & "  AND BM_CATEGORYHRID=3 ORDER BY BM_ENTRY_DATE DESC"


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudGrade.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade.DataBind()
                Dim columnCount As Integer = gvStudGrade.Rows(0).Cells.Count
                gvStudGrade.Rows(0).Cells.Clear()
                gvStudGrade.Rows(0).Cells.Add(New TableCell)
                gvStudGrade.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade.Rows(0).Cells(0).Text = "No Details...."

            Else
                gvStudGrade.DataBind()
                For Each GrdvRow As GridViewRow In gvStudGrade.Rows
                    If GrdvRow.RowType = DataControlRowType.DataRow Then
                        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
                        If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                            GrdvRow.ForeColor = Drawing.Color.Blue
                        Else
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                            GrdvRow.ForeColor = Drawing.Color.Red
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub

   
    Private Sub BindGrid_POS()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
                            " BM_INCIDENT_TYPE,BM_CATEGORY_SCORE as BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
                            " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName FROM BM.BM_MASTER A " & _
                            " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
                            " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
                            " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
                            " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=A.BM_CATEGORYID " & _
                            " WHERE S.STU_ID =" & HF_stuid.Value & "  AND BM_CATEGORYHRID=2 ORDER BY BM_ENTRY_DATE DESC"


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudGrade_POS.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade_POS.DataBind()
                Dim columnCount As Integer = gvStudGrade_POS.Rows(0).Cells.Count
                gvStudGrade_POS.Rows(0).Cells.Clear()
                gvStudGrade_POS.Rows(0).Cells.Add(New TableCell)
                gvStudGrade_POS.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade_POS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade_POS.Rows(0).Cells(0).Text = "No Details...."

            Else
                gvStudGrade_POS.DataBind()
                For Each GrdvRow As GridViewRow In gvStudGrade_POS.Rows
                    If GrdvRow.RowType = DataControlRowType.DataRow Then
                        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
                        If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                            GrdvRow.ForeColor = Drawing.Color.Blue
                        Else
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                            GrdvRow.ForeColor = Drawing.Color.Red
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub


    Private Sub BindGrid_ACHV()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
                            " BM_INCIDENT_TYPE,BM_CATEGORY_SCORE as BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
                            " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName FROM BM.BM_MASTER A " & _
                            " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
                            " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
                            " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
                            " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=A.BM_CATEGORYID " & _
                            " WHERE S.STU_ID =" & HF_stuid.Value & "  AND BM_CATEGORYHRID=1 ORDER BY BM_ENTRY_DATE DESC"


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudGrade_ACHV.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade_ACHV.DataBind()
                Dim columnCount As Integer = gvStudGrade_ACHV.Rows(0).Cells.Count
                gvStudGrade_ACHV.Rows(0).Cells.Clear()
                gvStudGrade_ACHV.Rows(0).Cells.Add(New TableCell)
                gvStudGrade_ACHV.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade_ACHV.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade_ACHV.Rows(0).Cells(0).Text = "No Details...."

            Else
                gvStudGrade_ACHV.DataBind()
                For Each GrdvRow As GridViewRow In gvStudGrade_ACHV.Rows
                    If GrdvRow.RowType = DataControlRowType.DataRow Then
                        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
                        If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                            GrdvRow.ForeColor = Drawing.Color.Blue
                        Else
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                            GrdvRow.ForeColor = Drawing.Color.Red
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gvStudGrade_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudGrade.PageIndexChanging
        gvStudGrade.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub


    Protected Sub gvStudGrade_ACHV_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudGrade_ACHV.PageIndexChanging
        gvStudGrade_ACHV.PageIndex = e.NewPageIndex
        BindGrid_ACHV()
    End Sub

    Protected Sub gvStudGrade_POS_PageIndexChanging1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudGrade_POS.PageIndexChanging
        gvStudGrade_POS.PageIndex = e.NewPageIndex
        BindGrid_POS()
    End Sub
End Class
