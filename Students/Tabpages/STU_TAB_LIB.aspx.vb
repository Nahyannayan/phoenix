Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class STU_TAB_LIB
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    'Protected Sub FeeSponsor_Update(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If e.ToString = "1" Then
    '        mpxFeeSponsor.Show()
    '    Else
    '        mpxFeeSponsor.Hide()
    '    End If
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BindMemerships()
       BindTransactions()
        BindItemDue()
        BindReservations()

    End Sub

    Public Sub BindMemerships()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim query = " SELECT RECORD_ID,LIBRARY_DIVISION_DES,MEMBERSHIP_DES FROM  LIBRARY_MEMBERSHIP_USERS A" & _
                          " INNER JOIN LIBRARY_MEMBERSHIPS B on A.MEMBERSHIP_ID = B.MEMBERSHIP_ID " & _
                          " INNER JOIN LIBRARY_DIVISIONS C ON C.LIBRARY_DIVISION_ID=A.LIBRARY_DIVISION_ID  WHERE A.USER_TYPE='STUDENT' AND USER_ID='" & Session("DB_Stu_ID") & "'"
        GridMemberships.DataSource = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        GridMemberships.DataBind()

    End Sub


    Public Sub BindTransactions()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT *, CASE ITEM_ACTUAL_RETURN_DATE WHEN '01/Jan/1900' THEN '' ELSE ITEM_ACTUAL_RETURN_DATE END AS RTN_DATE FROM VIEW_USER_TRANSACTIONS " & _
                        " WHERE USER_TYPE='STUDENT' AND USER_ID='" & Session("DB_Stu_ID") & "'" & _
                        " ORDER BY  ITEM_TAKEN_DATE DESC"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GrdUserTransaction.DataSource = ds
        GrdUserTransaction.DataBind()


    End Sub

    Public Sub BindItemDue()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " SELECT *, CASE ITEM_ACTUAL_RETURN_DATE WHEN '01/Jan/1900' THEN '' ELSE ITEM_ACTUAL_RETURN_DATE END AS RTN_DATE FROM VIEW_USER_TRANSACTIONS " & _
                        " WHERE USER_TYPE='STUDENT' AND USER_ID='" & Session("DB_Stu_ID") & "' AND RETURN_STATUS='Not Returned'" & _
                        " ORDER BY  ITEM_TAKEN_DATE DESC"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GridDue.DataSource = ds
        GridDue.DataBind()

    End Sub

    Public Sub BindReservations()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim str_query = " select PRODUCT_IMAGE_URL,ITEM_TITLE,LIBRARY_DIVISION_DES,ENTRY_DATE,RESERVE_START_DATE,RESERVE_END_DATE,CANCEL_RESERVATION from dbo.VIEW_USER_RESERVATION_HISTORY " & _
                        " where GET_USER_ID='" & Session("DB_Stu_ID") & "'order by ENTRY_DATE desc "


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GridReservations.DataSource = ds
        GridReservations.DataBind()

    End Sub



    Protected Sub GrdUserTransaction_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdUserTransaction.PageIndexChanging
        GrdUserTransaction.PageIndex = e.NewPageIndex
        BindTransactions()

    End Sub

    Protected Sub GridDue_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridDue.PageIndexChanging
        GridReservations.PageIndex = e.NewPageIndex
        BindItemDue()
    End Sub

    Protected Sub GridReservations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridReservations.PageIndexChanging
        GridReservations.PageIndex = e.NewPageIndex
        BindReservations()
    End Sub
End Class
