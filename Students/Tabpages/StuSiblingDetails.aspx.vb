Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_StuSiblingDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ViewState("stu_id") = Session("DB_Stu_ID").ToString
                HF_stuid.Value = ViewState("stu_id")
                BindSiblings()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindSiblings()
        Dim dsSibling As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        'lblSibling
        Dim strSQL As String = ""
        strSQL = "SELECT STUDENT_M.STU_ID ,BSU_NAME,STUDENT_M.STU_NO,isNull(STU_FIRSTNAME,'')+' '+isNull(STU_MIDNAME,'')+' '+isNull(STU_LASTNAME,'') as STU_FIRSTNAME," _
                & "STU_PRIMARYCONTACT,STU_DOB," _
                & " (select SCT_DESCR from SECTION_M where SCT_ID= STUDENT_M.stu_sct_id ) as SECTION " _
                & ",(select GRM_DISPLAY from GRADE_BSU_M where grm_id =STUDENT_M.STU_GRM_ID) as GRADE " _
                & ",(select STM_DESCR FROM STREAM_M WHERE STM_ID=STUDENT_M.STU_STM_ID)AS STREAM " _
                & ",(select SHF_DESCR from SHIFTS_M where SHF_ID=STUDENT_M.stu_SHF_ID) AS SHIFT " _
                & ",(select CTY_DESCR from country_m where CTY_ID =STUDENT_M.STU_NATIONALITY ) AS Nationality " _
                & " FROM STUDENT_M " _
                & "left JOIN STUDENT_D ON STUDENT_M.STU_ID =STUDENT_D.STS_STU_ID  " _
                & "INNER JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID=STUDENT_M.STU_BSU_ID " _
                & "INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID=  SECTION_M.sct_id " _
                & "WHERE STU_CURRSTATUS<>'CN' " _
                & " AND stu_sibling_id IN (select stu_sibling_id from  STUDENT_M  group by stu_sibling_id having count(stu_sibling_id)>=1) " _
                & " AND stu_sibling_id in( select stu_sibling_id from student_M where stu_id='" & HF_stuid.Value & "') and stu_id<>'" & HF_stuid.Value & "' ORDER BY STUDENT_M.STU_ID "

        dsSibling = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        gvStudEnquiry.DataSource = dsSibling.Tables(0)
        'h_SliblingID
        If dsSibling.Tables(0).Rows.Count = 0 Then
            dsSibling.Tables(0).Rows.Add(dsSibling.Tables(0).NewRow())
            gvStudEnquiry.DataBind()
            Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
            gvStudEnquiry.Rows(0).Cells.Clear()
            gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
            gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudEnquiry.DataBind()

        End If
    End Sub

    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudEnquiry.PageIndexChanging

        gvStudEnquiry.PageIndex = e.NewPageIndex
        BindSiblings()

    End Sub
End Class
