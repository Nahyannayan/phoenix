<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StuAttDetail.aspx.vb" Inherits="Students_stuContactdetails" %>

<%@ Import Namespace="InfoSoftGlobal" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <%--  <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Attendance Details</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table id="table1" runat="server" width="100%" class="table table-bordered">
                            <tr>
                                <td align="left">
                                    <span class="field-label">Academic year</span></td>

                                <td align="left">
                                    <asp:DropDownList ID="ddlAttAcd_id" runat="server" AutoPostBack="true"></asp:DropDownList></td>

                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="ltTitleAcd" runat="server" CssClass="field-label"></asp:Label></td>

                                <td align="left">
                                    <asp:Literal ID="ltAcdWorkingDays" runat="server"></asp:Literal></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="ltTotTilldate" runat="server" CssClass="field-label"></asp:Label></td>

                                <td align="left">
                                    <asp:Literal ID="ltTotWorkTilldate" runat="server"></asp:Literal></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="ltMrkTilldate" runat="server" CssClass="field-label"></asp:Label></td>

                                <td align="left">
                                    <asp:Literal ID="ltAttMarkTilldate" runat="server"></asp:Literal></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Days Present</span></td>

                                <td align="left">
                                    <asp:Literal ID="ltDayPresent" runat="server"></asp:Literal></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Days Absent</span></td>

                                <td align="left">
                                    <asp:Literal ID="ltDayAbsent" runat="server"></asp:Literal></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Days on Leave</span></td>

                                <td align="left">
                                    <asp:Literal ID="ltDayLeave" runat="server"></asp:Literal></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left"><span class="field-label">Percentage of Attendance</span></td>

                                <td align="left">
                                    <asp:Literal ID="ltPerAtt" runat="server"></asp:Literal></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            <table width="100%" align="center">
                <tr>

                    <td align="left" colspan="4">
                        <asp:Literal ID="FCLiteral" runat="server"></asp:Literal>&nbsp;</td>

                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Literal ID="FCAbsent" runat="server"></asp:Literal>&nbsp;
                    </td>

                    <td align="left" colspan="2">
                        <asp:Literal ID="FCLate" runat="server"></asp:Literal>&nbsp;
                    </td>

                </tr>
            </table>
        </div>
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <br />
    </form>
</body>
</html>
