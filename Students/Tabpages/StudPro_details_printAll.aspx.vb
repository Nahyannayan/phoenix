Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports InfoSoftGlobal
Partial Class StudRecordEdit
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
   
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function



    Sub tabDis()
        If Session("sBusper") = False Then

            If Session("tabDis") = 1 Then

                t22.Visible = False
                t23.Visible = False
                t24.Visible = False
                t25.Visible = False
                t27.Visible = False
                t28.Visible = False
                t29.Visible = False
                t30.Visible = False
                t31.Visible = False
                
            End If
        End If
    End Sub


    Sub menuShow()

        If (Session("BSU_TAB_SUPER") = False) Then
            If Session("sBusper") = False Then
                Dim i As Integer
                Dim FirstFlag As Boolean = True
                '0 to 6 is mnuMaster
                For i = 20 To 32
                    Dim str1 As String = i
                    If Session("tab_Right_User").ContainsKey(str1) Then
                        If FirstFlag And (Session("tab_Right_User").Item(str1) = "1" Or Session("tab_Right_User").Item(str1) = "2") Then
                            Select Case i
                                Case 20

                                    t20.Visible = True
                                Case 21
                                    t21.Visible = True
                                Case 22
                                    t22.Visible = True
                                Case 23
                                    t23.Visible = True
                                Case 24
                                    t24.Visible = True
                                Case 25
                                    t25.Visible = True

                                Case 27
                                    t27.Visible = True

                                Case 28
                                    t28.Visible = True


                                Case 29
                                    t29.Visible = True

                                Case 30
                                    t30.Visible = True

                                Case 31
                                    t31.Visible = True

                            End Select

                        End If
                        
                    Else
                        Select Case i
                            Case 20

                                t20.Visible = False
                            Case 21
                                t21.Visible = False
                            Case 22
                                t22.Visible = False
                            Case 23
                                t23.Visible = False
                            Case 24
                                t24.Visible = False
                            Case 25
                                t25.Visible = False

                            Case 27
                                t27.Visible = False

                            Case 28
                                t28.Visible = False


                            Case 29
                                t29.Visible = False

                            Case 30
                                t30.Visible = False

                            Case 31
                                t31.Visible = False

                        End Select

                        'mnuMaster.Items(i).ImageUrl = ""
                    End If
                Next
              

            End If

        End If
    End Sub

   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.MaintainScrollPositionOnPostBack = True
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
      

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")



                ViewState("stu_status") = Session("stu_status")
                If (UCase(ViewState("stu_status")) = "TC" Or UCase(ViewState("stu_status")) = "SO") Then

                    Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim sqlqry = "select * from users_m join employee_m on emp_id=usr_emp_id where USR_NAME='" & Session("sUsr_name") & "' and emp_des_id in(298,340,315,361)"
                    Using reader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, sqlqry)
                        If reader.HasRows = True Then
                            Session("tabsuperuser") = True
                            Session("tabDis") = 0
                        Else
                            'mnuMaster.Items(2).Enabled = False
                            'mnuMaster.Items(3).Enabled = False
                            'mnuMaster.Items(4).Enabled = False
                            'mnuMaster.Items(5).Enabled = False
                            'mnuMaster.Items(6).Enabled = False
                            'mnuChild.Items(0).Enabled = False
                            'mnuChild.Items(1).Enabled = False
                            'mnuChild.Items(2).Enabled = False
                            'mnuChild.Items(3).Enabled = False
                            'mnuChild.Items(4).Enabled = False
                            'mnuChild.Items(5).Enabled = False
                            Session("tabDis") = 1
                            tabDis()
                            Session("tabsuperuser") = False
                        End If

                    End Using
                End If

                Dim MNu_code As String = Session("MainMnu_code_pro")
                Dim RoleID As String = String.Empty
                Using reader_Rol_ID As SqlDataReader = AccessRoleUser.GetRoleID_user(Session("sUsr_name"))
                    If reader_Rol_ID.HasRows = True Then
                        While reader_Rol_ID.Read()
                            RoleID = Convert.ToString(reader_Rol_ID("USR_ROL_ID"))
                        End While
                    End If
                End Using
                Dim T_code As String = String.Empty
                Dim T_right As String = String.Empty
                Dim ht_tab As New Hashtable()
                Using readertab_Access As SqlDataReader = AccessRoleUser.GetTabRights(RoleID, Session("sBsuid"), MNu_code)
                    If readertab_Access.HasRows = True Then
                        While readertab_Access.Read()
                            T_code = CInt(Convert.ToString(readertab_Access("TAB_CODE")))
                            T_right = Convert.ToString(readertab_Access("TAR_RIGHT"))
                            ht_tab.Add(T_code, T_right)
                        End While
                    End If
                End Using

                If BSU_RIGHTS() = 1 Then
                    Session("BSU_TAB_SUPER") = True
                    Session("tabsuperuser") = True
                    Session("tabDis") = 0
                Else
                    Session("BSU_TAB_SUPER") = False
                End If
                Session("tab_Right_User") = ht_tab
                Call menuShow()


                getStudentGrade()
                pageHeader_bind(Session("DB_Stu_ID"))
                studentdetails(Session("DB_Stu_ID"))
                binddetails_parent(Session("DB_Stu_ID"))
                BindSiblings()



                GridBindFeeDetails()
                GridBindFeeaGEING()
                GridBindPayHist()

                bindMain_details(Session("DB_Stu_ID"))
                bindAttChart(Session("DB_Stu_ID"))
                bindABSChart(Session("DB_Stu_ID"))
                bindLATEChart(Session("DB_Stu_ID"))


                BindSubjectList()
                BindProgressReport()

                BindGrid()
                binddetails_med(Session("DB_Stu_ID"))
                ' binddetails(Session("DB_Stu_ID"))
                GridBindLIB_TRANS()

                getPageno()



            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Sub getpageno()
        Dim i As Integer
        Dim f22 As Boolean = True
        Dim f23 As Boolean = True
        Dim f24 As Boolean = True
        Dim f25 As Boolean = True
        Dim f27 As Boolean = True
        Dim f28 As Boolean = True
        Dim f29 As Boolean = True
        Dim f30 As Boolean = True
        Dim f31 As Boolean = True
        Dim j As Integer = 2
        'ltusrcont.Text = "Printed By: " & Session("sUsr_Display_Name")
        ltPNomain.Text = "Page No: 1"
        ltPNocont.Text = "Page No: 2"

        For i = 20 To 32
            If t22.Visible = True And f22 = True Then
                j = j + 1
                ' ltusrsib.Text = "Printed By: " & Session("sUsr_Display_Name")
                ltPnosib.Text = "Page No: " & j
                f22 = False

            ElseIf t23.Visible = True And f23 = True Then
                j = j + 1
                ' ltusrfee.Text = "Printed By: " & Session("sUsr_Display_Name")
                ltPnofee.Text = "Page No: " & j
                f23 = False
            ElseIf t24.Visible = True And f24 = True Then
                j = j + 1
                'ltusrAtt.Text = "Printed By: " & Session("sUsr_Display_Name")
                ltPnoAtt.Text = "Page No: " & j
                f24 = False

            ElseIf t25.Visible = True And f25 = True Then
                j = j + 1
                'ltUsrCurr.Text = "Printed By: " & Session("sUsr_Display_Name")
                ltPnoCurr.Text = "Page No: " & j
                f25 = False
            ElseIf t27.Visible = True And f27 = True Then
                j = j + 1
                ' ltUsrBeh.Text = "Printed By: " & Session("sUsr_Display_Name")
                ltPnoBeh.Text = "Page No: " & j
                f27 = False
            ElseIf t28.Visible = True And f28 = True Then
                j = j + 1
                'ltUsrSP.Text = "Printed By: " & Session("sUsr_Display_Name")
                ltPnoSp.Text = "Page No: " & j
                f28 = False
            ElseIf t29.Visible = True And f29 = True Then
                j = j + 1
                ' ltUsrTran.Text = "Printed By: " & Session("sUsr_Display_Name")
                ltPnoTran.Text = "Page No: " & j
                f29 = False

            ElseIf t30.Visible = True And f30 = True Then
                j = j + 1
                'ltusrLib.Text = "Printed By: " & Session("sUsr_Display_Name")
                ltPnoLib.Text = "Page No: " & j
                f30 = False

            ElseIf t31.Visible = True And f31 = True Then
                j = j + 1
                ' ltUSrSMS.Text = "Printed By: " & Session("sUsr_Display_Name")
                ltPnoSMS.Text = "Page No: " & j
                f31 = False

            End If
        Next


    End Sub

    Function BSU_RIGHTS() As Integer

        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlString As String = ""
        sqlString = " select count(bsu_id) from(select bsu_id from businessunit_m where bsu_clm_id in(19,13,14,10,18,2,22,4,6) " & _
" and bus_bsg_id<>4 and bsu_id<>'125017'  and bsu_bShow=1)A WHERE BSU_ID='" & Session("sBsuid") & "'"
        Dim STATUS As Integer = CInt(SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlString))
        Return STATUS
    End Function
    
    Sub studentdetails(ByVal stu_id As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "student_Details", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        'handle the null value returned from the reader incase  convert.tostring
                        ltEmiratesID.Text = Convert.ToString(readerStudent_Detail("STU_EMIRATES_ID"))
                        ltPremisesId.Text = Convert.ToString(readerStudent_Detail("stu_PremisesID"))
                        LTpob.Text = Convert.ToString(readerStudent_Detail("STU_POB"))
                        LTcunbirth.Text = Convert.ToString(readerStudent_Detail("COB"))
                        txtJoin_Shift.Text = Convert.ToString(readerStudent_Detail("SHF_DESCR_JOIN"))
                        txtJoin_Stream.Text = Convert.ToString(readerStudent_Detail("STM_DESCR_JOIN"))
                        txtACD_ID_Join.Text = Convert.ToString(readerStudent_Detail("ACD_ID_JOIN_Y"))
                        txtGRD_ID_Join.Text = Convert.ToString(readerStudent_Detail("GRD_ID_JOIN"))
                        txtSCT_ID_JOIN.Text = Convert.ToString(readerStudent_Detail("SCT_DESCR_JOIN"))
                        txtFee_ID.Text = Convert.ToString(readerStudent_Detail("FEE_ID"))
                        txtMOE_No.Text = Convert.ToString(readerStudent_Detail("BLUEID"))
                        txtFee_Spon.Text = Convert.ToString(readerStudent_Detail("SFEESPONSOR"))
                        txtPNo.Text = Convert.ToString(readerStudent_Detail("SPASPRTNO"))
                        txtPIssPlace.Text = Convert.ToString(readerStudent_Detail("SPASPRTISSPLACE"))
                        txtVNo.Text = Convert.ToString(readerStudent_Detail("STU_VISANO"))
                        txtVIssPlace.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSPLACE"))
                        txtVIssAuth.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSAUTH"))
                        TLtftype.Text = Convert.ToString(readerStudent_Detail("TFR_DESCR"))
                        LTreligion.Text = Convert.ToString(readerStudent_Detail("RLG_ID"))
                        LTnationality.Text = Convert.ToString(readerStudent_Detail("SNATIONALITY"))
                        ltminlist.Text = Convert.ToString(readerStudent_Detail("MINLIST"))
                        LTminlistType.Text = Convert.ToString(readerStudent_Detail("SMINLISTTYPE"))
                        txtFee_Spon.Text = Convert.ToString(readerStudent_Detail("SFEESPONSOR"))
                        LTemgcon.Text = Convert.ToString(readerStudent_Detail("SEMGCONTACT"))

                        ViewState("temp_COB") = Convert.ToString(readerStudent_Detail("COB"))
                        ViewState("temp_HOUSE") = Convert.ToString(readerStudent_Detail("HOUSE"))
                        ViewState("temp_PrefContact") = Convert.ToString(readerStudent_Detail("STU_PREFCONTACT"))
                        ViewState("temp_Blood") = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        ViewState("ACD_ID") = Convert.ToString(readerStudent_Detail("ACD_ID"))
                        'Setting date
                        If IsDate(readerStudent_Detail("SDOJ")) = True Then
                            txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("SDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("MINDOJ")) = True Then
                            txtMINDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("MINDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("DOB")) = True Then
                            txtDob.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("DOB"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_PASPRTISSDATE")) = True Then
                            txtPIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTISSDATE"))))
                        End If
                        If IsDate(readerStudent_Detail("STU_PASPRTEXPDATE")) = True Then
                            txtPExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTEXPDATE"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_VISAISSDATE")) = True Then
                            txtVIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAISSDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_VISAEXPDATE")) = True Then
                            txtVExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAEXPDATE")))).Replace("01/Jan/1900", "")
                        End If

                        Dim temp_Gender As String
                        temp_Gender = Convert.ToString(readerStudent_Detail("GENDER"))
                        If UCase(temp_Gender) = "F" Then
                            LTgender.Text = "Female"
                        ElseIf UCase(temp_Gender) = "M" Then
                            LTgender.Text = "Male"
                        End If

                        ltFirstLang.Text = Convert.ToString(readerStudent_Detail("STU_FIRSTLANG"))
                        ltOthLang.Text = Convert.ToString(readerStudent_Detail("STU_OTHLANG"))
                        ltProEng_R.Text = Convert.ToString(readerStudent_Detail("STU_ENG_READING"))
                        ltProEng_W.Text = Convert.ToString(readerStudent_Detail("STU_ENG_WRITING"))
                        ltProEng_S.Text = Convert.ToString(readerStudent_Detail("STU_ENG_SPEAKING"))
                    End While
                Else
                End If
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Sub binddetails_parent(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim ds As DataSet
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "stu_contactDetails", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        Ltl_Fname.Text = Convert.ToString(readerStudent_Detail("STS_FNAME"))
                        Ltl_Fnationality.Text = Convert.ToString(readerStudent_Detail("FNATIONALITY"))
                        Ltl_Fpob.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPOBOX"))
                        Ltl_FEmirate.Text = Convert.ToString(readerStudent_Detail("STS_FEMIR"))
                        Ltl_FPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_FRESPHONE"))
                        Ltl_FOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_FOFFPHONE"))
                        Ltl_FMobile.Text = Convert.ToString(readerStudent_Detail("STS_FMOBILE"))
                        Ltl_FEmail.Text = Convert.ToString(readerStudent_Detail("STS_FEMAIL"))
                        Ltl_FFax.Text = Convert.ToString(readerStudent_Detail("STS_FFAX"))
                        Ltl_FOccupation.Text = Convert.ToString(readerStudent_Detail("STS_FOCC"))
                        Ltl_FCompany.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPANY"))

                        Ltl_Mname.Text = Convert.ToString(readerStudent_Detail("STS_MNAME"))
                        Ltl_Mnationality.Text = Convert.ToString(readerStudent_Detail("MNATIONALITY"))
                        Ltl_Mpob.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPOBOX"))
                        Ltl_MEmirate.Text = Convert.ToString(readerStudent_Detail("STS_MEMIR"))
                        Ltl_MPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_MRESPHONE"))
                        Ltl_MOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_MOFFPHONE"))
                        Ltl_MMobile.Text = Convert.ToString(readerStudent_Detail("STS_MMOBILE"))
                        Ltl_MEmail.Text = Convert.ToString(readerStudent_Detail("STS_MEMAIL"))
                        Ltl_MFax.Text = Convert.ToString(readerStudent_Detail("STS_MFAX"))
                        Ltl_MOccupation.Text = Convert.ToString(readerStudent_Detail("STS_MOCC"))
                        Ltl_MCompany.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPANY"))

                        Ltl_Gname.Text = Convert.ToString(readerStudent_Detail("STS_GNAME"))
                        Ltl_Gnationality.Text = Convert.ToString(readerStudent_Detail("GNATIONALITY"))
                        Ltl_Gpob.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPOBOX"))
                        Ltl_GEmirate.Text = Convert.ToString(readerStudent_Detail("STS_GEMIR"))
                        Ltl_GPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_GRESPHONE"))
                        Ltl_GOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_GOFFPHONE"))
                        Ltl_GMobile.Text = Convert.ToString(readerStudent_Detail("STS_GMOBILE"))
                        Ltl_GEmail.Text = Convert.ToString(readerStudent_Detail("STS_GEMAIL"))
                        Ltl_GFax.Text = Convert.ToString(readerStudent_Detail("STS_GFAX"))
                        Ltl_GOccupation.Text = Convert.ToString(readerStudent_Detail("STS_GOCC"))
                        Ltl_GCompany.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPANY"))
                        Dim col As Integer
                        If readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "F" Then
                            col = 2
                        ElseIf readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "M" Then
                            col = 3

                        ElseIf readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "G" Then
                            col = 4
                        Else
                            col = 2
                        End If
                        table1.Rows(0).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(1).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(2).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(3).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(4).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(5).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(6).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(7).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(8).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(9).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(10).Cells(col).BgColor = "#FFFFC"
                        table1.Rows(11).Cells(col).BgColor = "#FFFFC"

                    End While
                Else
                    lblError.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub
    Private Sub BindSiblings()
        Dim dsSibling As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        'lblSibling
        Dim strSQL As String = ""
        strSQL = "SELECT STUDENT_M.STU_ID ,BSU_NAME,STUDENT_M.STU_NO,isNull(STU_FIRSTNAME,'')+' '+isNull(STU_MIDNAME,'')+' '+isNull(STU_LASTNAME,'') as STU_FIRSTNAME," _
                & "STU_PRIMARYCONTACT,STU_DOB," _
                & " (select SCT_DESCR from SECTION_M where SCT_ID= STUDENT_M.stu_sct_id ) as SECTION " _
                & ",(select GRM_DISPLAY from GRADE_BSU_M where grm_id =STUDENT_M.STU_GRM_ID) as GRADE " _
                & ",(select STM_DESCR FROM STREAM_M WHERE STM_ID=STUDENT_M.STU_STM_ID)AS STREAM " _
                & ",(select SHF_DESCR from SHIFTS_M where SHF_ID=STUDENT_M.stu_SHF_ID) AS SHIFT " _
                & ",(select CTY_DESCR from country_m where CTY_ID =STUDENT_M.STU_NATIONALITY ) AS Nationality " _
                & " FROM STUDENT_M " _
                & "left JOIN STUDENT_D ON STUDENT_M.STU_ID =STUDENT_D.STS_STU_ID  " _
                & "INNER JOIN BUSINESSUNIT_M ON BUSINESSUNIT_M.BSU_ID=STUDENT_M.STU_BSU_ID " _
                & "INNER JOIN SECTION_M ON STUDENT_M.STU_SCT_ID=  SECTION_M.sct_id " _
                & "WHERE STU_CURRSTATUS<>'CN' " _
                & " AND stu_sibling_id IN (select stu_sibling_id from  STUDENT_M  group by stu_sibling_id having count(stu_sibling_id)>=1) " _
                & " AND stu_sibling_id in( select stu_sibling_id from student_M where stu_id='" & Session("DB_Stu_ID") & "') and stu_id<>'" & Session("DB_Stu_ID") & "' ORDER BY STUDENT_M.STU_ID "

        dsSibling = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        gvStudEnquiry.DataSource = dsSibling.Tables(0)
        'h_SliblingID
        If dsSibling.Tables(0).Rows.Count = 0 Then
            dsSibling.Tables(0).Rows.Add(dsSibling.Tables(0).NewRow())
            gvStudEnquiry.DataBind()
            Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
            gvStudEnquiry.Rows(0).Cells.Clear()
            gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
            gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudEnquiry.DataBind()

        End If
    End Sub

    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudEnquiry.PageIndexChanging

        gvStudEnquiry.PageIndex = e.NewPageIndex
        BindSiblings()

    End Sub
    Private Sub GridBindFeeDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
                pParms(0).Value = Session("sBsuid")
                pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = Session("db_stu_id")

                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "MIS.GET_FEEDTLS", pParms)
                gvFeeDetails.DataSource = ds
                gvFeeDetails.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Private Sub GridBindFeeaGEING()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
                pParms(0).Value = Session("sBsuid")
                pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = Session("db_stu_id")

                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "MIS.GET_FEEAGEING", pParms)
                gvFeeAgeing.DataSource = ds
                gvFeeAgeing.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Private Sub GridBindPayHist()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
                pParms(0).Value = Session("db_stu_id")

                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "FEES.Dashboard_PaymentHistory", pParms)
                gvPaymentHist.DataSource = ds
                gvPaymentHist.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Sub bindMain_details(ByVal stu_id As String)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim tot_mrk As Double
            Dim DayPrs As Double

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStud_dashBoard_Att", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        ltAcdWorkingDays.Text = Convert.ToString(readerStudent_Detail("tot_acddays"))
                        ltTotWorkTilldate.Text = readerStudent_Detail("tot_wrkdays").ToString
                        ltAttMarkTilldate.Text = readerStudent_Detail("tot_marked").ToString
                        ltDayAbsent.Text = readerStudent_Detail("tot_abs").ToString
                        ltDayPresent.Text = readerStudent_Detail("tot_att").ToString
                        ltDayLeave.Text = readerStudent_Detail("tot_leave").ToString
                        ltTitleAcd.Text = "Total working days for the academic year " + readerStudent_Detail("acd_year").ToString
                        ltTotTilldate.Text = "Total working days till " + todayDT
                        ltMrkTilldate.Text = "Total Attendance marked till " + todayDT

                        tot_mrk = Convert.ToDecimal(readerStudent_Detail("tot_marked"))
                        DayPrs = Convert.ToDecimal(readerStudent_Detail("tot_att"))
                        If tot_mrk = 0 Then
                            tot_mrk = 1
                        End If
                        ltPerAtt.Text = Math.Round(((DayPrs / tot_mrk) * 100), 2)
                    End While
                Else
                    lblError.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Sub bindAttChart(ByVal stu_id)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim strXML As String
            Dim TMonth As String = String.Empty
            Dim TOT_ATT As String = String.Empty


            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}
            Dim i As Integer = 0



            strXML = ""
            strXML = strXML & "<graph caption='Yearly Attendance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_Att_Patten", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        TMonth = readerStudent_Detail("TMONTH").ToString
                        TOT_ATT = readerStudent_Detail("TOT_ATT").ToString

                        strXML = strXML & "<set name=" + "'" & TMonth & "' value=" + "'" & TOT_ATT & "' color=" + "'" & arr(i) & "' />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"
                    FCLiteral.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "600", "300", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Yearly Attendance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"
                    strXML = strXML & "<set name='Jan' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Feb' value='' color='F6BD0F'/>"
                    strXML = strXML & "<set name='Mar' value='' color='8BBA00'/>"
                    strXML = strXML & "<set name='Apr' value='' color='FF8E46'/>"
                    strXML = strXML & "<set name='May' value='' color='008E8E'/>"
                    strXML = strXML & "<set name='Jun' value='' color='D64646'/>"
                    strXML = strXML & "<set name='Jul' value='' color='8E468E'/>"
                    strXML = strXML & "<set name='Aug' value='' color='588526'/>"
                    strXML = strXML & "<set name='Sep' value='' color='B3AA00'/>"
                    strXML = strXML & "<set name='Oct' value='' color='008ED6'/>"
                    strXML = strXML & "<set name='Nov' value='' color='9D080D'/>"
                    strXML = strXML & "<set name='Dec' value='' color='A186BE'/>"
                    strXML = strXML & "</graph>"
                    FCLiteral.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "600", "300", False)
                End If
            End Using



            'strXML = ""
            'strXML = strXML & "<graph caption='Yearly Attedance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"
            'strXML = strXML & "<set name='Jan' value='462' color='AFD8F8'/>"
            'strXML = strXML & "<set name='Feb' value='857' color='F6BD0F'/>"
            'strXML = strXML & "<set name='Mar' value='671' color='8BBA00'/>"
            'strXML = strXML & "<set name='Apr' value='494' color='FF8E46'/>"
            'strXML = strXML & "<set name='May' value='761' color='008E8E'/>"
            'strXML = strXML & "<set name='Jun' value='960' color='D64646'/>"
            'strXML = strXML & "<set name='Jul' value='629' color='8E468E'/>"
            'strXML = strXML & "<set name='Aug' value='622' color='588526'/>"
            'strXML = strXML & "<set name='Sep' value='376' color='B3AA00'/>"
            'strXML = strXML & "<set name='Oct' value='494' color='008ED6'/>"
            'strXML = strXML & "<set name='Nov' value='761' color='9D080D'/>"
            'strXML = strXML & "<set name='Dec' value='960' color='A186BE'/>"
            'strXML = strXML & "</graph>"

            'Create the chart - Column 3D Chart with data from strXML variable using dataXML method


        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub


    Sub bindABSChart(ByVal stu_id)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim strXML As String
            Dim Tweek As String = String.Empty
            Dim TOT_ABSCOUNT As String = String.Empty
            Dim i As Integer

            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}


            strXML = ""
            strXML = strXML & "<graph caption='Weekly Absent Pattern'  xAxisName='Week' yAxisName='Count' pieSliceDepth='25' decimalPrecision='0'>"

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_ABS_Patten", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        Tweek = readerStudent_Detail("TWEEK").ToString
                        'If Tweek = "SUN" Then
                        '    Tweek = "Sunday"
                        'ElseIf Tweek = "MON" Then
                        '    Tweek = "Monday"
                        'ElseIf Tweek = "TUE" Then
                        '    Tweek = "Tuesday"
                        'ElseIf Tweek = "WED" Then
                        '    Tweek = "Wednesday"
                        'ElseIf Tweek = "THU" Then
                        '    Tweek = "Thursday"
                        'ElseIf Tweek = "FRI" Then
                        '    Tweek = "Friday"
                        'ElseIf Tweek = "SAT" Then
                        '    Tweek = "Saturday"
                        'End If
                        TOT_ABSCOUNT = readerStudent_Detail("TOT_ABSCOUNT").ToString
                        If TOT_ABSCOUNT = "0" Then
                            TOT_ABSCOUNT = ""
                        End If
                        strXML = strXML & "<set name=" + "'" & Tweek & "' value=" + "'" & TOT_ABSCOUNT & "' color=" + "'" & arr(i) & "' />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"
                    FCAbsent.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "450", "250", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Weekly Absent Pattern'  pieSliceDepth='25' decimalPrecision='0' showNames='1'>"
                    strXML = strXML & "<set name='Sun' value=''  color='AFD8F8'/>"
                    strXML = strXML & "<set name='Mon' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Tue' value='' color='AFD8F8' />"
                    strXML = strXML & "<set name='Wed' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Thu' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Fri' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Sat' value='' color='AFD8F8'/>"

                    strXML = strXML & "</graph>"
                    FCAbsent.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "450", "250", False)
                End If
            End Using

        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub

    Sub bindLATEChart(ByVal stu_id As String)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim strXML As String
            Dim Tweek As String = String.Empty
            Dim TOT_late As String = String.Empty
            Dim i As Integer
            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}


            strXML = ""
            strXML = strXML & "<graph caption='Weekly Late Pattern'  xAxisName='Week' yAxisName='Count' pieSliceDepth='25' decimalPrecision='0'>"
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_LATE_Patten", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        Tweek = readerStudent_Detail("TWEEK").ToString
                        'If Tweek = "SUN" Then
                        '    Tweek = "Sunday"
                        'ElseIf Tweek = "MON" Then
                        '    Tweek = "Monday"
                        'ElseIf Tweek = "TUE" Then
                        '    Tweek = "Tuesday"
                        'ElseIf Tweek = "WED" Then
                        '    Tweek = "Wednesday"
                        'ElseIf Tweek = "THU" Then
                        '    Tweek = "Thursday"
                        'ElseIf Tweek = "FRI" Then
                        '    Tweek = "Friday"
                        'ElseIf Tweek = "SAT" Then
                        '    Tweek = "Saturday"
                        'End If
                        TOT_late = readerStudent_Detail("TOT_LCOUNT").ToString
                        If TOT_late = "0" Then
                            TOT_late = ""
                        End If
                        strXML = strXML & "<set name=" + "'" & Tweek & "' value=" + "'" & TOT_late & "' color=" + "'" & arr(i) & "' />"

                        'strXML = strXML & "<set name=" + "'" & Tweek & "' value=" + "'" & TOT_late & "' />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"
                    FCLate.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "Late", "450", "250", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Weekly Late Pattern'  pieSliceDepth='25' decimalPrecision='0' showNames='1'>"
                    strXML = strXML & "<set name='Sun' value=''  color='AFD8F8'/>"
                    strXML = strXML & "<set name='Mon' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Tue' value='' color='AFD8F8' />"
                    strXML = strXML & "<set name='Wed' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Thu' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Fri' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Sat' value='' color='AFD8F8'/>"

                    strXML = strXML & "</graph>"
                    FCLate.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "late", "450", "250", False)
                End If
            End Using

        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub
    Sub getStudentGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_GRD_ID,stu_acd_id,Stu_sct_id FROM STUDENT_M WHERE STU_ID='" + Session("DB_Stu_ID") + "'"
        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read
                    ViewState("GRD_ID") = readerStudent_Detail("STU_GRD_ID").ToString
                    ViewState("acd_id") = readerStudent_Detail("stu_acd_id").ToString
                    ViewState("sct_id") = readerStudent_Detail("Stu_sct_id").ToString

                End While
            End If

        End Using


    End Sub


    Sub BindSubjectList()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim str_query As String = " SELECT DISTINCT SBG_DESCR,SGR_DESCR, TEACHERS=ISNULL((SELECT " _
                               & "  STUFF((SELECT ', '+ ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,' ')+' '+ " _
                               & " ISNULL(EMP_LNAME,'') FROM EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S " _
                               & " AS L ON K.EMP_ID=L.SGS_EMP_ID   WHERE SGS_SGR_ID = C.SGR_ID" _
                               & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,'')),'')" _
                               & " FROM  STUDENT_GROUPS_S AS A INNER JOIN" _
                               & " SUBJECTS_GRADE_S AS B ON A.SSD_SBG_ID=B.SBG_ID" _
                               & " LEFT OUTER JOIN GROUPS_M AS C ON A.SSD_SGR_ID=C.SGR_ID" _
                               & " WHERE SSD_STU_ID=" + Session("DB_Stu_ID") + " AND SSD_ACD_ID='" + Session("CURRENT_ACD_ID") + "' ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjects.DataSource = ds
        gvSubjects.DataBind()
    End Sub

    Sub BindProgressReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim str_query As String = "SELECT DISTINCT RPF_ID,RPF_DESCR,RPF_DISPLAYORDER,RPF_RSM_ID FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                              & " INNER JOIN rpt.REPORT_STUDENT_S AS B ON A.RPF_ID=B.RST_RPF_ID " _
                              & " WHERE RST_STU_ID=" + Session("DB_Stu_ID") _
                              & " AND RST_ACD_ID=" + ViewState("acd_id") _
                              & " ORDER BY RPF_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvProgress.DataSource = ds
        gvProgress.DataBind()
    End Sub
    Protected Sub lnkPrinted_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If Session("sbsuid") = "" Or Session("sbsuid") Is Nothing Then
                Panel1.Visible = False
                lblSession.Text = "Your session has expired!!!.Please login again"
                Exit Sub
            End If

            Dim lblRpfId As New Label
            Dim lblRsmId As New Label
            Dim url As String
            Dim viewid As String
            lblRpfId = TryCast(sender.FindControl("lblRpfId"), Label)
            lblRsmId = TryCast(sender.FindControl("lblRsmId"), Label)


            ViewState("MainMnu_code") = "StudentProfile"

            Dim str As String = "~/Curriculum/Reports/Aspx/rptMonthlyProgressReportStudWise.aspx?" _
                              & "MainMnu_code=" + Encr_decrData.Encrypt(ViewState("MainMnu_code")) _
                              & "&datamode=" + Encr_decrData.Encrypt("view") _
                              & "&rsmid=" + Encr_decrData.Encrypt(lblRsmId.Text) _
                              & "&rpfid=" + Encr_decrData.Encrypt(lblRpfId.Text) _
                              & "&acdid=" + Encr_decrData.Encrypt(ViewState("acd_id")) _
                              & "&stuid=" + Encr_decrData.Encrypt(Session("DB_Stu_ID")) _
                              & "&grdid=" + Encr_decrData.Encrypt(ViewState("GRD_ID"))
            Response.Redirect(str)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Protected Function GetNavigateUrl1(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('stu_ActionViewDetails.aspx?Info_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
    End Function
    Private Sub BindGrid()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
                            " BM_INCIDENT_TYPE,C.BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
                            " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName FROM BM.BM_MASTER A " & _
                            " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
                            " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
                            " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
                            " RIGHT OUTER JOIN BM.BM_ACTION_MASTER C ON C.BM_STU_ID=S.STU_ID " & _
                            " LEFT OUTER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=C.BM_CATEGORYID " & _
                            " WHERE S.STU_ID =" & Session("DB_Stu_ID") & "  ORDER BY BM_ENTRY_DATE DESC"


            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudGrade.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade.DataBind()
                Dim columnCount As Integer = gvStudGrade.Rows(0).Cells.Count
                gvStudGrade.Rows(0).Cells.Clear()
                gvStudGrade.Rows(0).Cells.Add(New TableCell)
                gvStudGrade.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade.Rows(0).Cells(0).Text = "No Details...."

            Else
                gvStudGrade.DataBind()
                For Each GrdvRow As GridViewRow In gvStudGrade.Rows
                    If GrdvRow.RowType = DataControlRowType.DataRow Then
                        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
                        If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                            GrdvRow.ForeColor = Drawing.Color.Blue
                        Else
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                            DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                            GrdvRow.ForeColor = Drawing.Color.Red
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub binddetails_med(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)

        Dim temp_Health As Boolean
        'Dim temp_HOUSE, temp_Blood, temp_Type, temp_Rlg_ID, temp_Nationality, temp_COB, temp_MINLIST, temp_MINLISTTYPE, temp_PrefContact As String
        Dim arInfo As String() = New String(2) {}
        Dim Temp_Phone_Split As String = String.Empty
        Dim splitter As Char = "-"
        'arInfo = info.Split(splitter)
        Try


            Dim CONN As String = ConnectionManger.GetOASISConnectionString

            Dim PARAM(1) As SqlClient.SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@STU_ID", stu_id)




            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "STU.GETHEALTHINFO", PARAM)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        'Literal2.Text = Convert.ToString(readerStudent_Detail("SHEALTH"))
                        'Literal3.Text = Convert.ToString(readerStudent_Detail("SPHYSICAL"))
                        'Literal4.Text = Convert.ToString(readerStudent_Detail("SPMEDICATION"))
                        'Ltl_bGroup.Text = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        'Literal1.Text = Convert.ToString(readerStudent_Detail("STU_HCNO"))
                        'temp_Health = Convert.ToBoolean(readerStudent_Detail("SbRCVSPMEDICATION"))
                        'If temp_Health Then
                        '    Literal5.Text = "YES" + " " + Convert.ToString(readerStudent_Detail("STU_REMARKS"))
                        'Else
                        '    Literal5.Text = "NO"
                        'End If



                        ltHth_No.Text = Convert.ToString(readerStudent_Detail("STU_HCNO"))
                        ltB_grp.Text = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        ltAlg_Detail.Text = Convert.ToString(readerStudent_Detail("STU_bALLERGIES"))
                        divAlg.InnerText = Convert.ToString(readerStudent_Detail("STU_ALLERGIES"))

                        ltSp_med.Text = Convert.ToString(readerStudent_Detail("STU_bRCVSPMEDICATION"))
                        divSp_med.InnerText = Convert.ToString(readerStudent_Detail("STU_SPMEDICATION"))

                        ltPhy_edu.Text = Convert.ToString(readerStudent_Detail("STU_bPRESTRICTIONS"))
                        divPhy_edu.InnerText = Convert.ToString(readerStudent_Detail("STU_PHYSICAL"))

                        ltHth_Info.Text = Convert.ToString(readerStudent_Detail("STU_bHRESTRICTIONS"))
                        divHth_Info.InnerText = Convert.ToString(readerStudent_Detail("STU_HEALTH"))

                        ltLS_Ther.Text = Convert.ToString(readerStudent_Detail("STU_bTHERAPHY"))
                        divLS_Ther.InnerText = Convert.ToString(readerStudent_Detail("STU_THERAPHY"))

                        ltSEN.Text = Convert.ToString(readerStudent_Detail("STU_bSEN"))
                        divSEN.InnerText = Convert.ToString(readerStudent_Detail("STU_SEN_REMARK"))

                        ltEAL.Text = Convert.ToString(readerStudent_Detail("STU_bEAL"))
                        divEAL.InnerText = Convert.ToString(readerStudent_Detail("STU_EAL_REMARK"))

                        'ltSEA.Text = Convert.ToString(readerStudent_Detail("STU_bENRICH"))
                        'divSEA.InnerText = Convert.ToString(readerStudent_Detail("STU_ENRICH"))

                        'ltMus_Prof.Text = Convert.ToString(readerStudent_Detail("STU_bMUSICAL"))
                        'divMus_Prof.InnerText = Convert.ToString(readerStudent_Detail("STU_MUSICAL"))

                        'ltSport.Text = Convert.ToString(readerStudent_Detail("STU_bSPORTS"))
                        'divSport.InnerText = Convert.ToString(readerStudent_Detail("STU_SPORTS"))
                        ltPrev_sch.Text = Convert.ToString(readerStudent_Detail("STU_bBEHAVIOUR"))
                        divPrev_sch.InnerText = Convert.ToString(readerStudent_Detail("STU_BEHAVIOUR"))


                    End While

                Else
                End If

            End Using
        Catch ex As Exception

        End Try

    End Sub

    Sub binddetails(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "MIS.GET_TPTDTLS", param)
            If ds.Tables(0).Rows.Count > 0 Then
                ltminlist.Text = ds.Tables(0).Rows(0)("PAREA").ToString
                'Literal6.Text = ds.Tables(0).Rows(0)("DAREA").ToString


                'Literal2.Text = ds.Tables(0).Rows(0)("PICKUP").ToString
                'Literal3.Text = ds.Tables(0).Rows(0)("DROPOFF").ToString


                'Literal4.Text = ds.Tables(0).Rows(0)("ONWARDBUS").ToString
                'Literal5.Text = ds.Tables(0).Rows(0)("RETURNBUS").ToString



                Literal7.Text = ds.Tables(0).Rows(0)("ONWARDTRIP").ToString
                Literal8.Text = ds.Tables(0).Rows(0)("RETURNTRIP").ToString
            Else
                lblError.Text = "No Records Found "
            End If
        Catch ex As Exception
            lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try


    End Sub

    Protected Sub lnkinsertnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkinsertnew.Click


        Dim lblStuId As Label
        Dim lblStuName As Label
        Dim lblStuNo As Label
        Dim lblGrade As Label
        Dim lblSection As Label
        Dim lblStatus As Label



        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("UserName", Session("sUsr_name"))
        param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))


        param.Add("stu_id", ViewState("viewid"))
        Dim rptClass As New rptClass


        With rptClass
            .crDatabase = "Oasis_Transport"
            .reportPath = Server.MapPath("../../Transport/Reports/RPT/rptstudTransportAudit1.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        Response.Redirect("~/Reports/ASPX Report/rptReportViewer_Princi.aspx")

    End Sub

    Private Sub GridBindLIB_TRANS()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
                pParms(0).Value = Session("sBSUID")
                pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = Session("DB_Stu_ID")

                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "MIS.GET_LIBDTLS", pParms)
                gvLibDetails.DataSource = ds
                gvLibDetails.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Sub pageHeader_bind(ByVal Stu_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STP_HEAD,STP_MAIN FROM STU.STU_PROFILE "
        Dim strHead As String = String.Empty
        Dim strMain As String = String.Empty

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                strHead = reader.GetString(0)
                strMain = reader.GetString(1)

            End While
        End Using



        str_query = "select BSU_NAME,BSU_ADDRESS,BSU_POBOX,BSU_CITY,BSU_TEL,BSU_FAX,BSU_EMAIL,replace(BSU_MOE_LOGO,'..\','..\..\') as BSU_MOE_LOGO,BSU_URL from BUSINESSUNIT_M where BSU_ID='" & Session("sBsuid") & "'"
        Using readerBSU As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While readerBSU.Read

                strHead = strHead.Replace("%BsuName%", readerBSU("BSU_NAME").ToString())
                strHead = strHead.Replace("%bsuAddress%", readerBSU("BSU_ADDRESS").ToString())
                strHead = strHead.Replace("%bsupostbox%", readerBSU("BSU_POBOX").ToString())
                strHead = strHead.Replace("%bsucity%", readerBSU("BSU_CITY").ToString())
                strHead = strHead.Replace("%bsutelephone%", readerBSU("BSU_TEL").ToString())
                strHead = strHead.Replace("%bsufax%", readerBSU("BSU_FAX").ToString())
                strHead = strHead.Replace("%bsuemail%", readerBSU("BSU_EMAIL").ToString())
                strHead = strHead.Replace("%bsuwebsite%", readerBSU("BSU_URL").ToString())
                strHead = strHead.Replace("%imglogo%", readerBSU("BSU_MOE_LOGO").ToString())
            End While
        End Using

        str_query = "SELECT  case when ISNULL(STUDENT_M.STU_PHOTOPATH,'')='' then '../../Images/Photos/no_image.gif' else STUDENT_M.STU_PHOTOPATH end  AS STU_PHOTOPATH,student_M.stu_acd_id,  STUDENT_M.STU_NO, GRADE_BSU_M.GRM_DISPLAY, SECTION_M.SCT_DESCR, " & _
"ISNULL(STUDENT_D.STS_FFIRSTNAME, '') + ' ' + ISNULL(STUDENT_D.STS_FMIDNAME, '') + ' ' + " & _
"ISNULL(STUDENT_D.STS_FLASTNAME, '') AS Fname, ISNULL(STUDENT_D.STS_MFIRSTNAME, '') + ' ' + " & _
"ISNULL(STUDENT_D.STS_MMIDNAME, '') + ' ' + ISNULL(STUDENT_D.STS_MLASTNAME, '') AS Mname, " & _
"case when (select count(*) from tcm_m where tcm_stu_id=stu_id)>=1	then   case when (select top(1)tcm_tcso from tcm_m where tcm_stu_id=stu_id and TCM_bpreApproved=0 and TCM_bCANCELLED=0 order by tcm_id desc)='TC'  then 'ACTIVE - But applied for TC' when (select top(1)tcm_tcso from tcm_m where tcm_stu_id=stu_id and TCM_bpreApproved=1 and TCM_bCANCELLED=0 order by tcm_id desc)='TC'  then 'INACTIVE - Issued TC' when (select top(1)tcm_tcso from tcm_m join STRIKEOFF_RECOMMEND_M ON STK_STU_ID=tcm_stu_id where tcm_stu_id=stu_id 	and STK_bAPPROVED=0 and TCM_bCANCELLED=0 AND tcm_tcso='so' order by tcm_id desc)='SO'  then 'ACTIVE - Recommended for Strike Off' when (select top(1)tcm_tcso from tcm_m join STRIKEOFF_RECOMMEND_M ON STK_STU_ID=tcm_stu_id  where tcm_stu_id=stu_id	 and STK_bAPPROVED=1 AND tcm_tcso='so' and TCM_bCANCELLED=0 order by tcm_id desc)='SO'  then 'INACTIVE - Strike Off' else case STU_CURRSTATUS when 'SO' then 'INACTIVE -Strike Off' when 'EN' then 'ACTIVE' when 'TC' then 'INACTIVE - TC' when 'CN' then 'CANCELLED' end  End 	else case STU_CURRSTATUS when 'EN' then 'ACTIVE' when 'SO' then 'INACTIVE -Strike Off' when 'TC' then 'INACTIVE - TC' 	when 'CN' then 'CANCELLED' end End STU_CURRSTATUS, CASE WHEN  (SELECT     isnull(BSU_bSTUD_DISPLAYPASSPRTNAME, 0) " & _
"FROM  businessunit_m  WHERE      bsu_id = STUDENT_M.STU_BSU_ID) = 1 THEN STUDENT_M.STU_PASPRTNAME " & _
"ELSE STUDENT_M.STU_FIRSTNAME + ' ' + isnull(STUDENT_M.STU_MIDNAME, '')  + ' ' + isnull(STUDENT_M.STU_LASTNAME, '') " & _
" END AS sname, case when (select count(distinct grm_shf_id) from grade_bsu_m " & _
" where grm_grd_id=student_m.stu_grd_id and grm_acd_id=student_M.stu_acd_id)>1 then " & _
"(select shf_descr from shifts_m where shf_id=student_m.stu_shf_id) else '' end as shf," & _
" case when (select count( distinct grm_stm_id) from grade_bsu_m " & _
"where grm_grd_id=student_m.stu_grd_id and grm_acd_id=student_M.stu_acd_id)>1 then " & _
" (select stm_descr from stream_m where stm_id=student_m.stu_stm_id) else '' end as stm, " & _
" case when (select count(distinct acd_clm_id) from academicyear_d where acd_acy_id= " & _
"SECTION_M.SCT_ACY_ID and acd_bsu_id=STUDENT_M.STU_BSU_ID)>1 then " & _
" (select clm_descr from dbo.CURRICULUM_M where clm_id=(select acd_clm_id from academicyear_d " & _
" where acd_id=STUDENT_M.STU_ACD_ID)) else '' end  as clm ,STU_MINLIST,STU_MINLISTTYPE" & _
" FROM         STUDENT_M INNER JOIN   GRADE_BSU_M ON STUDENT_M.STU_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN " & _
" SECTION_M ON STUDENT_M.STU_SCT_ID = SECTION_M.SCT_ID INNER JOIN " & _
 " STUDENT_D ON STUDENT_M.STU_SIBLING_ID = STUDENT_D.STS_STU_ID " & _
" WHERE     STUDENT_M.STU_ID = '" & Stu_ID & "'"
        Dim clm As String = String.Empty
        Dim shf As String = String.Empty
        Dim stm As String = String.Empty

        Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read

                    clm = Convert.ToString(readerStudent_Detail("clm"))
                    shf = Convert.ToString(readerStudent_Detail("shf"))
                    stm = Convert.ToString(readerStudent_Detail("stm"))

                    strMain = strMain.Replace("%ltStudName%", Convert.ToString(readerStudent_Detail("Sname")))
                    strMain = strMain.Replace("%ltStudId%", Convert.ToString(readerStudent_Detail("stu_no")))
                    strMain = strMain.Replace("%ltGrd%", Convert.ToString(readerStudent_Detail("grm_display")))
                    strMain = strMain.Replace("%ltSct%", Convert.ToString(readerStudent_Detail("sct_descr")))
                    strMain = strMain.Replace("%ltStatus%", Convert.ToString(readerStudent_Detail("stu_currstatus")))
                    strMain = strMain.Replace("%ltFather%", Convert.ToString(readerStudent_Detail("fname")))
                    strMain = strMain.Replace("%ltMother%", Convert.ToString(readerStudent_Detail("mname")))
                    strMain = strMain.Replace("%ltminlist%", Convert.ToString(readerStudent_Detail("STU_MINLIST")))
                    strMain = strMain.Replace("%ltmintype%", Convert.ToString(readerStudent_Detail("STU_MINLISTTYPE")))

                    If Trim(clm) = "" Then
                        strMain = strMain.Replace("%styCLM%", "display:none;")
                    Else

                        strMain = strMain.Replace("%styCLM%", "display:table-row;")
                        strMain = strMain.Replace("%ltCLM%", Trim(clm))

                    End If

                    If Trim(shf) = "" Then
                        strMain = strMain.Replace("%stySHF%", "display:none;")
                    Else
                        strMain = strMain.Replace("%stySHF%", "display:table-row;")
                        strMain = strMain.Replace("%ltShf%", Trim(shf))

                    End If
                    If Trim(stm) = "" Then
                        strMain = strMain.Replace("%stySTM%", "display:none;")
                    Else
                        strMain = strMain.Replace("%stySTM%", "display:table-row;")
                        strMain = strMain.Replace("%ltStm%", Trim(stm))

                    End If

                    Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
                    Dim strImagePath As String = String.Empty
                    If strPath.Contains("no_image") = False Then
                        strImagePath = connPath & strPath
                        strMain = strMain.Replace("%imgEmpImage%", strImagePath)

                    Else
                        strMain = strMain.Replace("%imgEmpImage%", strPath)

                    End If

                End While

            End If

        End Using


        strHead = strHead.Replace("''", "'")
        strMain = strMain.Replace("''", "'")
        header1.InnerHtml = strHead + strMain
        header2.InnerHtml = strHead + strMain
        header3.InnerHtml = strHead + strMain
        header4.InnerHtml = strHead + strMain
        header5.InnerHtml = strHead + strMain
        header6.InnerHtml = strHead + strMain
        header7.InnerHtml = strHead + strMain
        header8.InnerHtml = strHead + strMain
        header9.InnerHtml = strHead + strMain
        header10.InnerHtml = strHead + strMain
        header11.InnerHtml = strHead + strMain

    End Sub
   
End Class
