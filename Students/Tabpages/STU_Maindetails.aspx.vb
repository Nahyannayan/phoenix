Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_Tabpages_STU_Maindetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'ViewState("stu_id") = "20131707"
                ViewState("stu_id") = Session("DB_Stu_ID").ToString
                HF_stuid.Value = ViewState("stu_id")
                studentdetails()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub studentdetails()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", HF_stuid.Value)
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "student_Details", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        'handle the null value returned from the reader incase  convert.tostring

                        LTpob.Text = Convert.ToString(readerStudent_Detail("STU_POB"))
                        LTcunbirth.Text = Convert.ToString(readerStudent_Detail("COB"))
                        txtJoin_Shift.Text = Convert.ToString(readerStudent_Detail("SHF_DESCR_JOIN"))
                        txtJoin_Stream.Text = Convert.ToString(readerStudent_Detail("STM_DESCR_JOIN"))
                        txtACD_ID_Join.Text = Convert.ToString(readerStudent_Detail("ACD_ID_JOIN_Y"))
                        txtGRD_ID_Join.Text = Convert.ToString(readerStudent_Detail("GRD_ID_JOIN"))
                        txtSCT_ID_JOIN.Text = Convert.ToString(readerStudent_Detail("SCT_DESCR_JOIN"))
                        txtFee_ID.Text = Convert.ToString(readerStudent_Detail("FEE_ID"))
                        txtMOE_No.Text = Convert.ToString(readerStudent_Detail("BLUEID"))
                        txtFee_Spon.Text = Convert.ToString(readerStudent_Detail("SFEESPONSOR"))
                        txtPNo.Text = Convert.ToString(readerStudent_Detail("SPASPRTNO"))
                        txtPIssPlace.Text = Convert.ToString(readerStudent_Detail("SPASPRTISSPLACE"))
                        txtVNo.Text = Convert.ToString(readerStudent_Detail("STU_VISANO"))
                        txtVIssPlace.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSPLACE"))
                        txtVIssAuth.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSAUTH"))
                        TLtftype.Text = Convert.ToString(readerStudent_Detail("TFR_DESCR"))
                        LTreligion.Text = Convert.ToString(readerStudent_Detail("RLG_ID"))
                        LTnationality.Text = Convert.ToString(readerStudent_Detail("SNATIONALITY"))
                        LTminlist.Text = Convert.ToString(readerStudent_Detail("MINLIST"))
                        LTminlistType.Text = Convert.ToString(readerStudent_Detail("SMINLISTTYPE"))
                        txtFee_Spon.Text = Convert.ToString(readerStudent_Detail("SFEESPONSOR"))
                        LTemgcon.Text = Convert.ToString(readerStudent_Detail("SEMGCONTACT"))
                        ltEmiratesID.Text = Convert.ToString(readerStudent_Detail("STU_EMIRATES_ID"))
                        ltPremisesId.Text = Convert.ToString(readerStudent_Detail("stu_PremisesID"))


                        ViewState("temp_COB") = Convert.ToString(readerStudent_Detail("COB"))
                        ViewState("temp_HOUSE") = Convert.ToString(readerStudent_Detail("HOUSE"))
                        ViewState("temp_PrefContact") = Convert.ToString(readerStudent_Detail("STU_PREFCONTACT"))
                        ViewState("temp_Blood") = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        ViewState("ACD_ID") = Convert.ToString(readerStudent_Detail("ACD_ID"))
                        'Setting date
                        If IsDate(readerStudent_Detail("SDOJ")) = True Then
                            txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("SDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("MINDOJ")) = True Then
                            txtMINDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("MINDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("DOB")) = True Then
                            txtDob.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("DOB"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_PASPRTISSDATE")) = True Then
                            txtPIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTISSDATE"))))
                        End If
                        If IsDate(readerStudent_Detail("STU_PASPRTEXPDATE")) = True Then
                            txtPExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTEXPDATE"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_VISAISSDATE")) = True Then
                            txtVIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAISSDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_VISAEXPDATE")) = True Then
                            txtVExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAEXPDATE")))).Replace("01/Jan/1900", "")
                        End If

                        Dim temp_Gender As String
                        temp_Gender = Convert.ToString(readerStudent_Detail("GENDER"))
                        If UCase(temp_Gender) = "F" Then
                            LTgender.Text = "Female"
                        ElseIf UCase(temp_Gender) = "M" Then
                            LTgender.Text = "Male"
                        End If

                        ltFirstLang.Text = Convert.ToString(readerStudent_Detail("STU_FIRSTLANG"))
                        ltOthLang.Text = Convert.ToString(readerStudent_Detail("STU_OTHLANG"))
                        ltProEng_R.Text = Convert.ToString(readerStudent_Detail("STU_ENG_READING"))
                        ltProEng_W.Text = Convert.ToString(readerStudent_Detail("STU_ENG_WRITING"))
                        ltProEng_S.Text = Convert.ToString(readerStudent_Detail("STU_ENG_SPEAKING"))

                    End While
                Else
                End If
            End Using
        Catch ex As Exception

        End Try
    End Sub
End Class
