Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports InfoSoftGlobal
Partial Class Students_stuContactdetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                GetStu_AcdYear()

              
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetStu_AcdYear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", Session("DB_Stu_ID"))
        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.GETSTU_ACDYEAR", param)

            If readerStudent_Detail.HasRows = True Then
                ddlAttAcd_id.DataSource = readerStudent_Detail
                ddlAttAcd_id.DataTextField = "ACY_DESCR"
                ddlAttAcd_id.DataValueField = "ACD_ID"
                ddlAttAcd_id.DataBind()

                If Not ddlAttAcd_id.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then

                    ddlAttAcd_id.ClearSelection()
                    ddlAttAcd_id.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                End If
            Else
                ddlAttAcd_id.Items.Clear()

            End If

            ddlAttAcd_id_SelectedIndexChanged(ddlAttAcd_id, Nothing)
        End Using
    End Sub

    Protected Sub ddlAttAcd_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAttAcd_id.SelectedIndexChanged
        bindMain_details(Session("DB_Stu_ID"))
        bindAttChart(Session("DB_Stu_ID"))
        bindABSChart(Session("DB_Stu_ID"))
        bindLATEChart(Session("DB_Stu_ID"))
    End Sub

    Sub bindMain_details(ByVal stu_id As String)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            param(1) = New SqlClient.SqlParameter("@Tacd_id", ddlAttAcd_id.SelectedValue)
            Dim tot_mrk As Double
            Dim DayPrs As Double


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStud_dashBoard_Att", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        ltAcdWorkingDays.Text = Convert.ToString(readerStudent_Detail("tot_acddays"))
                        ltTotWorkTilldate.Text = readerStudent_Detail("tot_wrkdays").ToString
                        ltAttMarkTilldate.Text = readerStudent_Detail("tot_marked").ToString
                        ltDayAbsent.Text = readerStudent_Detail("tot_abs").ToString
                        ltDayPresent.Text = readerStudent_Detail("tot_att").ToString
                        ltDayLeave.Text = readerStudent_Detail("tot_leave").ToString
                        ltTitleAcd.Text = "Total working days for the academic year " + readerStudent_Detail("acd_year").ToString
                        ltTotTilldate.Text = "Total working days till " + todayDT
                        ltMrkTilldate.Text = "Total Attendance marked till " + todayDT
                        tot_mrk = Convert.ToDecimal(readerStudent_Detail("tot_marked"))
                        DayPrs = Convert.ToDecimal(readerStudent_Detail("tot_att"))
                        If tot_mrk = 0 Then
                            tot_mrk = 1
                        End If
                        ltPerAtt.Text = Math.Round(((DayPrs / tot_mrk) * 100), 2)
                    End While
                Else
                    lblerror.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub


    Sub bindAttChart(ByVal stu_id)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            param(1) = New SqlClient.SqlParameter("@Tacd_id", ddlAttAcd_id.SelectedValue)
            Dim strXML As String
            Dim TMonth As String = String.Empty
            Dim TOT_ATT As String = String.Empty


            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}
            Dim i As Integer = 0



            strXML = ""
            strXML = strXML & "<graph caption='Yearly Attendance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_Att_Patten", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        TMonth = readerStudent_Detail("TMONTH").ToString
                        TOT_ATT = readerStudent_Detail("TOT_ATT").ToString

                        strXML = strXML & "<set name=" + "'" & TMonth & "' value=" + "'" & TOT_ATT & "' color=" + "'" & arr(i) & "' />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"
                    FCLiteral.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "600", "280", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Yearly Attendance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"
                    strXML = strXML & "<set name='Jan' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Feb' value='' color='F6BD0F'/>"
                    strXML = strXML & "<set name='Mar' value='' color='8BBA00'/>"
                    strXML = strXML & "<set name='Apr' value='' color='FF8E46'/>"
                    strXML = strXML & "<set name='May' value='' color='008E8E'/>"
                    strXML = strXML & "<set name='Jun' value='' color='D64646'/>"
                    strXML = strXML & "<set name='Jul' value='' color='8E468E'/>"
                    strXML = strXML & "<set name='Aug' value='' color='588526'/>"
                    strXML = strXML & "<set name='Sep' value='' color='B3AA00'/>"
                    strXML = strXML & "<set name='Oct' value='' color='008ED6'/>"
                    strXML = strXML & "<set name='Nov' value='' color='9D080D'/>"
                    strXML = strXML & "<set name='Dec' value='' color='A186BE'/>"
                    strXML = strXML & "</graph>"
                    FCLiteral.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "600", "280", False)
                End If
            End Using



            'strXML = ""
            'strXML = strXML & "<graph caption='Yearly Attedance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"
            'strXML = strXML & "<set name='Jan' value='462' color='AFD8F8'/>"
            'strXML = strXML & "<set name='Feb' value='857' color='F6BD0F'/>"
            'strXML = strXML & "<set name='Mar' value='671' color='8BBA00'/>"
            'strXML = strXML & "<set name='Apr' value='494' color='FF8E46'/>"
            'strXML = strXML & "<set name='May' value='761' color='008E8E'/>"
            'strXML = strXML & "<set name='Jun' value='960' color='D64646'/>"
            'strXML = strXML & "<set name='Jul' value='629' color='8E468E'/>"
            'strXML = strXML & "<set name='Aug' value='622' color='588526'/>"
            'strXML = strXML & "<set name='Sep' value='376' color='B3AA00'/>"
            'strXML = strXML & "<set name='Oct' value='494' color='008ED6'/>"
            'strXML = strXML & "<set name='Nov' value='761' color='9D080D'/>"
            'strXML = strXML & "<set name='Dec' value='960' color='A186BE'/>"
            'strXML = strXML & "</graph>"

            'Create the chart - Column 3D Chart with data from strXML variable using dataXML method


        Catch ex As Exception
            lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub


    Sub bindABSChart(ByVal stu_id)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            param(1) = New SqlClient.SqlParameter("@Tacd_id", ddlAttAcd_id.SelectedValue)
            Dim strXML As String
            Dim Tweek As String = String.Empty
            Dim TOT_ABSCOUNT As String = String.Empty
            Dim i As Integer

            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}


            strXML = ""
            strXML = strXML & "<graph caption='Weekly Absent Pattern'  xAxisName='Week' yAxisName='Count' pieSliceDepth='25' decimalPrecision='0'>"

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_ABS_Patten", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        Tweek = readerStudent_Detail("TWEEK").ToString
                        'If Tweek = "SUN" Then
                        '    Tweek = "Sunday"
                        'ElseIf Tweek = "MON" Then
                        '    Tweek = "Monday"
                        'ElseIf Tweek = "TUE" Then
                        '    Tweek = "Tuesday"
                        'ElseIf Tweek = "WED" Then
                        '    Tweek = "Wednesday"
                        'ElseIf Tweek = "THU" Then
                        '    Tweek = "Thursday"
                        'ElseIf Tweek = "FRI" Then
                        '    Tweek = "Friday"
                        'ElseIf Tweek = "SAT" Then
                        '    Tweek = "Saturday"
                        'End If
                        TOT_ABSCOUNT = readerStudent_Detail("TOT_ABSCOUNT").ToString
                        If TOT_ABSCOUNT = "0" Then
                            TOT_ABSCOUNT = ""
                        End If
                        strXML = strXML & "<set name=" + "'" & Tweek & "' value=" + "'" & TOT_ABSCOUNT & "' color=" + "'" & arr(i) & "' />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"
                    FCAbsent.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "430", "250", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Weekly Absent Pattern'  pieSliceDepth='25' decimalPrecision='0' showNames='1'>"
                    strXML = strXML & "<set name='Sun' value=''  color='AFD8F8'/>"
                    strXML = strXML & "<set name='Mon' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Tue' value='' color='AFD8F8' />"
                    strXML = strXML & "<set name='Wed' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Thu' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Fri' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Sat' value='' color='AFD8F8'/>"

                    strXML = strXML & "</graph>"
                    FCAbsent.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "430", "250", False)
                End If
            End Using

        Catch ex As Exception
            lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub

    Sub bindLATEChart(ByVal stu_id)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            param(1) = New SqlClient.SqlParameter("@Tacd_id", ddlAttAcd_id.SelectedValue)
            Dim strXML As String
            Dim Tweek As String = String.Empty
            Dim TOT_late As String = String.Empty
            Dim i As Integer
            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}


            strXML = ""
            strXML = strXML & "<graph caption='Weekly Late Pattern'  xAxisName='Week' yAxisName='Count' pieSliceDepth='25' decimalPrecision='0'>"
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_LATE_Patten", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        Tweek = readerStudent_Detail("TWEEK").ToString
                        'If Tweek = "SUN" Then
                        '    Tweek = "Sunday"
                        'ElseIf Tweek = "MON" Then
                        '    Tweek = "Monday"
                        'ElseIf Tweek = "TUE" Then
                        '    Tweek = "Tuesday"
                        'ElseIf Tweek = "WED" Then
                        '    Tweek = "Wednesday"
                        'ElseIf Tweek = "THU" Then
                        '    Tweek = "Thursday"
                        'ElseIf Tweek = "FRI" Then
                        '    Tweek = "Friday"
                        'ElseIf Tweek = "SAT" Then
                        '    Tweek = "Saturday"
                        'End If
                        TOT_late = readerStudent_Detail("TOT_LCOUNT").ToString
                        If TOT_late = "0" Then
                            TOT_late = ""
                        End If
                        strXML = strXML & "<set name=" + "'" & Tweek & "' value=" + "'" & TOT_late & "' color=" + "'" & arr(i) & "' />"

                        'strXML = strXML & "<set name=" + "'" & Tweek & "' value=" + "'" & TOT_late & "' />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"
                    FCLate.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "Late", "430", "250", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Weekly Late Pattern'  pieSliceDepth='25' decimalPrecision='0' showNames='1'>"
                    strXML = strXML & "<set name='Sun' value=''  color='AFD8F8'/>"
                    strXML = strXML & "<set name='Mon' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Tue' value='' color='AFD8F8' />"
                    strXML = strXML & "<set name='Wed' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Thu' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Fri' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Sat' value='' color='AFD8F8'/>"

                    strXML = strXML & "</graph>"
                    FCLate.Text = FusionCharts.RenderChartHTML("../../FusionCharts/FCF_Column3D.swf", "", strXML, "late", "430", "250", False)
                End If
            End Using

        Catch ex As Exception
            lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub

   
End Class
