<%@ Page Language="VB"  AutoEventWireup="true" CodeFile="StudPro_details_printAll.aspx.vb" Inherits="StudRecordEdit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
     <%@ Register Src="UserControl/Dash_ManageSMS.ascx" TagName="Dash_ManageSMS" TagPrefix="uc1" %>
   <%@ Import Namespace="InfoSoftGlobal" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>:::: GEMS :::: Student Profile ::::</title>
   <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StudDashBoard.css" rel="stylesheet" type="text/css" />
   <script language="javascript" type="text/javascript">
  function printpage()
  {
  window.print();
  }
   </script>
    <style type="text/css">
      #footer{
 position: absolute;
top:auto;
bottom:10px;
margin:2;
height:15px;
display:block;
width:100%;
text-align:right;
}
    
    </style>
    
        </head>
<body onload="setTimeout('printpage()',3000);" style="margin-top:0px;margin-left:0px;margin-right:0px;">
<form id="Form1" runat="server">
 <ajaxToolkit:ToolkitScriptManager id="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
        <table align="center" runat="server" id="maintab"  width="940px">
         <tr>
       <td align="left" class="BlueTable_simple"  style="text-align: left">
        <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary4" runat="server" CssClass="error" DisplayMode="SingleParagraph"
                    EnableViewState="False" ForeColor="" ValidationGroup="groupM1" />
<span style="color: #c00000"></span></td></tr><tr>
              <td>
             
              
              </td>
              
              </tr>
      
       <tr>
       <td style="height: 11px;" align="right">&nbsp;&nbsp;&nbsp;</td>
       </tr>
       <tr >
       <td runat="server" id="t20" >
        
      <div ID="header1" runat="server"></div> <br /><br />
      
  <table align="center"  bordercolor="#1b80b6" border="1" cellspacing="0"
                            style="border-top-color:#d0e0ff;border-style:solid; border-top-width:1px;border-bottom-width:1px; border-left-width:0px;border-right-width:0px;border-top-width:0px; height: 30px;" width="100%">
                            
                           
                            <tr style="border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px;" >
                                <td align="left"  colspan="6" style="font-weight: bold; font-size: 8pt; background-image: url(../../Images/bgblue.gif);
                                    color: white; font-family: Verdana;border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px; height: 24px;">
                                    Personal Details</td>
                            </tr>
           <tr>
               <td  class="matters" colspan="6" rowspan="4" valign="top">
                   <table class="BlueTable_simple" width="100%">
                       <tr>
                           <td class="matters" style="width: 100px">
                                    Date of Birth<span style="color: #1b80b6"></span></td>
                           <td class="matters" style="width: 1px" align="center">
                               :</td>
                           <td class="matters" style="width: 117px">
                               <asp:Literal ID="txtDob" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                               Place of Birth</td>
                           <td class="matters" style="width: 1px" align="center">
                               :</td>
                           <td class="matters" style="width: 100px">
                               <asp:Literal ID="LTpob" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                                    Country of Birth</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                               <asp:Literal ID="LTcunbirth" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                                    Nationality</td>
                            <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                               <asp:Literal ID="LTnationality" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                                    Gender</td>
                           <td class="matters" style="width: 1px" align="center">
                               :</td>
                           <td class="matters" style="width: 117px">
                               <asp:Literal ID="LTgender" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                                    Religion</td>
                           <td class="matters" style="width: 1px" align="center">
                               :</td>
                           <td class="matters" style="width: 100px">
                               <asp:Literal ID="LTreligion" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" colspan="6" style="border-top-width: 0px; font-weight: bold; border-left-width: 0px; font-size: 8pt; background-image: url(../../Images/bgblue.gif); color: white; font-family: Verdana; height: 24px; border-right-width: 0px;">
                               Join Details</td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                                Fee ID</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                                <asp:Literal ID="txtFee_ID" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                                    MOE Reg#<span style="color: #c00000"></span></td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                               <asp:Literal ID="txtMOE_No" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                               Date of Join</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                                <asp:Literal ID="txtDOJ" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 140px">
                                    Date of Join(Ministry)</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                                <asp:Literal ID="txtMINDOJ" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px;">
                                    Tranfer Type<font color="#c00000"><span style="color: #1b80b6"></span></font></td>
                           <td align="center" class="matters" style="width: 1px; ">
                               :</td>
                           <td class="matters" style="width: 117px; ">
                                <asp:Literal ID="TLtftype" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 120px;">
                                    Join Academic Year</td>
                           <td align="center" class="matters" style="width: 1px;">
                               :</td>
                           <td class="matters" style="width: 100px;">
                                <asp:Literal ID="txtACD_ID_Join" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                                    Join Grade</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                               <asp:Literal ID="txtGRD_ID_Join" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                                    Join Section</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                               <asp:Literal ID="txtSCT_ID_JOIN" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                                    Join Shift</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                               
                                <asp:Literal ID="txtJoin_Shift" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                                    Join Stream</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                               
                                <asp:Literal ID="txtJoin_Stream" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                               Ministry List</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                                <asp:Literal ID="ltminlist" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                               Ministry List Type</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                                <asp:Literal ID="LTminlistType" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 140px">
                                    Emergency Contact</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                               <asp:Literal ID="LTemgcon" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                                    Fee Sponsor</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                                  <asp:Literal ID="txtFee_Spon" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" colspan="6" style="border-top-width: 0px; font-weight: bold; border-left-width: 0px; font-size: 8pt; background-image: url(../../Images/bgblue.gif); color: white; font-family: Verdana; height: 24px; border-right-width: 0px;">
                   Passport/Visa Details</td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                               Passport No</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                               <asp:Literal ID="txtPNo" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                               Passport Issue Place</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                                   <asp:Literal ID="txtPIssPlace" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                               Passport Issue Date</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                                 <asp:Literal ID="txtPIssDate" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                               Passport Expiry Date</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                                
                                <asp:Literal ID="txtPExpDate" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                               Visa No</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                               <asp:Literal ID="txtVNo" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                               Visa Issue Place</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                                <asp:Literal ID="txtVIssPlace" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                               Visa Issue Date</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                              
                                <asp:Literal ID="txtVIssDate" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                               Visa Expiry Date</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                             
                                <asp:Literal ID="txtVExpDate" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 110px">
                                Issuing Authority</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" colspan="4">
                              
                                <asp:Literal ID="txtVIssAuth" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="matters" style="width: 100px">
                                Emirates ID</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 117px">
                              
                                <asp:Literal ID="ltEmiratesID" runat="server"></asp:Literal></td>
                           <td class="matters" style="width: 100px">
                               Premises Id</td>
                           <td align="center" class="matters" style="width: 1px">
                               :</td>
                           <td class="matters" style="width: 100px">
                             
                                <asp:Literal ID="ltPremisesId" runat="server"></asp:Literal></td>
                       </tr>
                         <tr>
    <td colspan="6" style="border-top-width: 0px; font-weight: bold; border-left-width: 0px; font-size: 8pt; background-image: url(../../Images/bgblue.gif); color: white; font-family: Verdana; height: 24px; border-right-width: 0px;">
    Language Details </td></tr>
      
        <tr >
          <td class="matters" width="160px">
              First Language</td>
          <td  align="center">
              :</td>
          <td class="matters" width="160px">
            <asp:Literal ID="ltFirstLang" runat="server"></asp:Literal></td>
          <td class="matters" width="160px">
              Other Languages</td>
          <td  align="center">
              :</td>
          <td class="matters" width="160px">
              <asp:Literal ID="ltOthLang" runat="server"></asp:Literal></td>
      </tr>
      
       <tr >
          <td class="matters" width="160px">
              Proficiency in English</td>
          <td class="matters" align="center">
              :</td>
          <td   colspan="4">
          
          <div style="width:100%">
          <div>
          <font style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #1B80B6;">
           Reading&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<asp:Literal ID="ltProEng_R" runat="server"></asp:Literal></font>&nbsp;&nbsp;&nbsp;
          <font style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #1B80B6;">
            Writing&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<asp:Literal ID="ltProEng_W" runat="server"></asp:Literal></font>&nbsp;&nbsp;&nbsp;
            <font style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #1B80B6;">
           Speaking&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<asp:Literal ID="ltProEng_S" runat="server"></asp:Literal></font>&nbsp;&nbsp;&nbsp;
          </div>
          
         
          
          </div>
          
          
         </td>
      </tr>
                   </table>
               </td>
           </tr>
       </table>
       
          <div id="footer">
       <font color="#1b80b6" size="2px"><asp:Literal ID="ltPNomain" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                                  </font>
       
       
       
        
   
         </div>
   <div style="page-break-after:always"></div>
       </td>
       </tr>
       <tr >
       <td runat="server" id="t21">
       
       
     
        <div ID="header2" runat="server"></div> <br />
        <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
                      <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 30px">
                    Contact Details</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
                    <table id="table1" runat="server" class="BlueTable_simple" width="100%">
                        <tr>
                            <td class="matters" style="width: 100px">
                            </td>
                            <td align="center" class="matters" style="width: 1px">
                            </td>
                            <td class="matters">
                                Father</td>
                            <td class="matters">
                                Mother</td>
                            <td class="matters">
                                Guardian</td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                Name</td>
                            <td align="left" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters">
                                <asp:Literal ID="Ltl_Fname" runat="server"></asp:Literal></td>
                            <td class="matters">
                                <asp:Literal ID="Ltl_Mname" runat="server"></asp:Literal></td>
                            <td class="matters">
                                <asp:Literal ID="Ltl_Gname" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                Nationality</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_Fnationality" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_Mnationality" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_Gnationality" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                Comm.POB</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_Fpob" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_Mpob" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_Gpob" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                City/Emirate</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_FEmirate" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_MEmirate" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_GEmirate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                Phone Res</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_FPhoneRes" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_MPhoneRes" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_GPhoneRes" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                Office Phone</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_FOfficePhone" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_MOfficePhone" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_GOfficePhone" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                Mobile</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_FMobile" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_MMobile" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_GMobile" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                Email</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_FEmail" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_MEmail" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_GEmail" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                Fax</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_FFax" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_MFax" runat="server"></asp:Literal></td>
                            <td class="matters" style="width: 100px">
                                <asp:Literal ID="Ltl_GFax" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 100px">
                                Occupation</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters">
                                <asp:Literal ID="Ltl_FOccupation" runat="server"></asp:Literal></td>
                            <td class="matters">
                                <asp:Literal ID="Ltl_MOccupation" runat="server"></asp:Literal></td>
                            <td class="matters">
                                <asp:Literal ID="Ltl_GOccupation" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                        
                            <td class="matters">
                                Company</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" style="width: 200px">
                                <asp:Literal ID="Ltl_FCompany" runat="server"></asp:Literal></td>
                            <td class="matters">
                                <asp:Literal ID="Ltl_MCompany" runat="server"></asp:Literal></td>
                            <td class="matters">
                                <asp:Literal ID="Ltl_GCompany" runat="server"></asp:Literal></td>
                        </tr>
                    </table>
                   </td>
            </tr>
        </table>
        
          <div id="footer">
       <font color="#1b80b6" size="2px"> <asp:Literal ID="ltPNocont" runat="server"></asp:Literal>
                                                  &nbsp;&nbsp;&nbsp;</font>
       
       
       
        
      
         </div>
          <div style="page-break-after:always"></div>
       </td>
       </tr>
       
       <tr>
       
       <td  runat="server" id="t22">
      <div ID="header3" runat="server"></div> <br /><br />
       <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
                    
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
        <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
             EmptyDataText="No Records Found" HeaderStyle-Height="30"
            SkinID="GridViewNormal" Width="100%">
            <RowStyle  Height="25px" />
            <Columns>
                <asp:TemplateField HeaderText="Business Unit">
                    <ItemStyle Width="300px" />
                    <ItemTemplate>
                        <asp:Label ID="lblBsuName" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Student No">
                    <ItemTemplate>
                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                        <asp:Label ID="stuId" runat="server" Text='<%# bind("Stu_Id") %>' Visible="False"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Applicant Name">
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblApplName" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label></td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemStyle Width="300px" />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkApplName1" runat="server" CommandName="Select" Text='<%# Bind("STU_FIRSTNAME") %>'
                            Visible="False"></asp:LinkButton>
                        <asp:Label ID="lnkApplName" runat="server" Text='<%# Bind("STU_FIRSTNAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Grade">
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label></td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemStyle Width="100px" />
                    <ItemTemplate>
                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRADE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Shift">
                    <HeaderTemplate>
                        Shift
                    </HeaderTemplate>
                    <ItemStyle Width="80px" />
                    <ItemTemplate>
                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("SHIFT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Stream">
                    <HeaderTemplate>
                        Stream
                    </HeaderTemplate>
                    <ItemStyle Width="80px" />
                    <ItemTemplate>
                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("STREAM") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle  Height="30px" />
        </asp:GridView>
        </td>
        </tr>
                </table>
                             
                <div id="footer">
                 <font color="#1b80b6" size="2px">       
                                                   <asp:Literal ID="ltPnosib" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                                  </font>
       
       
           
       </div>
       <div style="page-break-after:always"></div>
       </td>
       </tr>
       
       <tr >
       <td runat="server" id="t23">
        <div ID="header4" runat="server"></div> <br />
                             <div>
<table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
                   
            <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 24px">
                    &nbsp;&nbsp;
                Fee Details</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
                        <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                            Width="100%">
                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                            <Columns>
                                
                                <asp:BoundField DataField="Sl_No" HeaderText="Sl No" ReadOnly="True" />
                                <asp:TemplateField SortExpression="FEE_TYPE" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    FEE TYPE</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="left"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_TYPE"  runat="server" Text='<%# Bind("FEE_TYPE") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                                
                                 <asp:TemplateField SortExpression="FEE_CHARGED" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Amount</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_CHARGED"  runat="server" Text='<%# Bind("FEE_CHARGED") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                              
                                 
                                 
                                 
                                  <asp:TemplateField SortExpression="FEE_PAID" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    PAID</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_PAID"  runat="server" Text='<%# Bind("FEE_PAID") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                 
                                 
                                 
                                 
                                 
                                 <asp:TemplateField SortExpression="FEE_BALANCE" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    BALANCE</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_BALANCE"  runat="server" Text='<%# Bind("FEE_BALANCE") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle CssClass="griditem_hilight" />
                            <HeaderStyle CssClass="gridheader_new" Height="25px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
    </td>
            </tr>
        </table>
 </div>
                             <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
            <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 24px">
                    &nbsp;&nbsp;
               Payment History</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
        
                       <asp:GridView ID="gvPaymentHist" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                            Width="100%">
                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                            <Columns>
                                
                                <asp:BoundField DataField="Sl_No" HeaderText="Sl No" ReadOnly="True" />
                                <asp:TemplateField SortExpression="FCL_DATE" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Rct. Date</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="left"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFCL_DATE"  runat="server" Text='<%# Bind("FCL_DATE") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                                
                                 <asp:TemplateField SortExpression="FCL_RECNO" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                   Rct No</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFCL_RECNO"  runat="server" Text='<%# Bind("FCL_RECNO") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                              
                                 
                                 
                                 
                                  <asp:TemplateField SortExpression="FCL_NARRATION" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Narration</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFCL_NARRATION"  runat="server" Text='<%# Bind("FCL_NARRATION") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                 
                                 
                                 
                                 
                                 
                                 <asp:TemplateField SortExpression="FCL_AMOUNT" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Amount</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFCL_AMOUNT"  runat="server" Text='<%# Bind("FCL_AMOUNT") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle CssClass="griditem_hilight" />
                            <HeaderStyle CssClass="gridheader_new" Height="25px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
                            </table>
                            <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
            <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 24px">
                    &nbsp;&nbsp;
                Fee Ageing Summary</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
                
                        <asp:GridView ID="gvFeeAgeing" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                            Width="100%">
                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                            <Columns>
                                
                                <asp:BoundField DataField="Sl_No" HeaderText="Sl No" ReadOnly="True" />
                                <asp:TemplateField SortExpression="FEE_TYPE" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    FEE TYPE</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="left"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_TYPE"  runat="server" Text='<%# Bind("FEE_TYPE") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                                
                                 <asp:TemplateField SortExpression="FEE_AGE1" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    (0-30)</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_AGE1"  runat="server" Text='<%# Bind("FEE_AGE1") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                              
                                 
                                 
                                 
                                  <asp:TemplateField SortExpression="FEE_AGE2" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    (30-60)</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_AGE2"  runat="server" Text='<%# Bind("FEE_AGE2") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                 
                                 
                                 
                                 
                                 
                                 <asp:TemplateField SortExpression="FEE_AGE3" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                     (60-90)</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_AGE3"  runat="server" Text='<%# Bind("FEE_AGE3") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                    
                    
                    
                     <asp:TemplateField SortExpression="FEE_AGE4" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                     (90-120)</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_AGE4"  runat="server" Text='<%# Bind("FEE_AGE4") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                    
                    
                      <asp:TemplateField SortExpression="FEE_AGE5" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                     (120-180)</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_AGE5"  runat="server" Text='<%# Bind("FEE_AGE5") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                    
                    
                                        <asp:TemplateField SortExpression="FEE_AGE6" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                     (>180)</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblFEE_AGE6"  runat="server" Text='<%# Bind("FEE_AGE6") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                      
                    
                    
                                
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle CssClass="griditem_hilight" />
                            <HeaderStyle CssClass="gridheader_new" Height="25px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
                            </table>
                            <div id="footer">
                             
                                                    <asp:Literal ID="ltPnofee" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                                
          
       </div>
       <div style="page-break-after:always"></div>
       
       </td>
       </tr>
       <tr>
       <td  runat="server" id="t24">
        <div ID="header5" runat="server"></div> <br />
       
   
                    <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
                     
            <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 24px">
                    Attendance Details</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
                    <table id="table2" runat="server" class="BlueTable_simple" width="100%">
                        <tr>
                            <td class="matters" style="width: 320px">
                                <asp:Literal ID="ltTitleAcd" runat="server"></asp:Literal></td>
                            <td align="left" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" colspan="3">
                                <asp:Literal ID="ltAcdWorkingDays" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 320px">
                                <asp:Literal ID="ltTotTilldate" runat="server"></asp:Literal></td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" colspan="3">
                                <asp:Literal ID="ltTotWorkTilldate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 320px">
                                <asp:Literal ID="ltMrkTilldate" runat="server"></asp:Literal></td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" colspan="3">
                                <asp:Literal ID="ltAttMarkTilldate" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 320px">
                                Days Present</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" colspan="3">
                                <asp:Literal ID="ltDayPresent" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 320px">
                                Days Absent</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" colspan="3">
                                <asp:Literal ID="ltDayAbsent" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 320px">
                                Days on Leave</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" colspan="3">
                                <asp:Literal ID="ltDayLeave" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="matters" style="width: 320px">
                                Percentage of Attendance</td>
                            <td align="center" class="matters" style="width: 1px">
                                :</td>
                            <td class="matters" colspan="3">
                                <asp:Literal ID="ltPerAtt" runat="server"></asp:Literal></td>
                        </tr>
                    </table>
            </td>
            </tr>
           
        </table>
                    <table width="100%" align="center">
    <tr>
   
            <td align="left" colspan="2">
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
               <asp:Literal ID="FCLiteral" runat="server"></asp:Literal>&nbsp;</td>
        
    </tr>
        <tr>
            <td align="left" >
            <asp:Literal ID="FCAbsent" runat="server"></asp:Literal>&nbsp;
            </td>
            
            <td align="left" >
            <asp:Literal ID="FCLate" runat="server"></asp:Literal>&nbsp;
            </td>
        
        </tr>
        </table>
                    <div id="footer">
         <font color="#1b80b6" size="2px"><asp:Literal ID="ltPnoAtt" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                                 </font>
          
       </div>
       <div style="page-break-after:always"></div>
                      </td>
                      </tr>
                      
        <tr >
        <td runat="server" id="t25">
           <div ID="header6" runat="server"></div> <br />
           <table width="100%" cellpadding="0" cellspacing="0">
            <tr><td align="center" class="matters">
            <asp:Label ID="lblSession" Class="Error" runat="server" Text="">
            </asp:Label> 
             </td></tr></table> 
               <asp:Panel ID="Panel1" runat="server" Width="100%" >
                <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
                      
            <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="7" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 24px">
                    &nbsp;&nbsp;
               Subject List</td>
            </tr>
            <tr>
                <td class="matters" colspan="7" rowspan="4" valign="top">
                <table align="center"  bordercolor="#1b80b6" border="1" cellspacing="0"
                            style="border-top-color:#d0e0ff;border-style:solid; border-top-width:1px;border-bottom-width:1px; border-left-width:0px;border-right-width:0px;border-top-width:0px;" width="100%">
                            
                            
                             <tr>
                    <td align="left" class="matters" colspan="7" rowspan="4" valign="top">
                        <asp:GridView ID="gvSubjects" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                            Width="100%">
                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                            <Columns>
                                
                             <asp:TemplateField  >
                        <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                        <tr> <td align="center" style="width: 100px">
                        Subject</td></table></HeaderTemplate>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        <ItemTemplate><asp:Label ID="lblSubject"  runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>                                    
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField  >
                        <HeaderTemplate><table style="width: 100%; height: 100%">
                        <tr><td align="center" style="width: 100px">
                        Group</td></tr></table>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        <ItemTemplate>
                        <asp:Label ID="lblGroup"  runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>                                    
                        </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                        <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                         <tr><td align="center" style="width: 100px">Tutor</td>
                        </tr></table>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                        <ItemTemplate>
                        <asp:Label ID="lblTutor"  runat="server" Text='<%# Bind("TEACHERS") %>'></asp:Label>                                    
                        </ItemTemplate>
                        </asp:TemplateField>
                                
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle CssClass="griditem_hilight" />
                            <HeaderStyle CssClass="gridheader_new" Height="25px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
           </table>
           </table>
  
          <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
            <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 24px">
                    &nbsp;&nbsp;
              Progress Report</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
                <table align="center"  bordercolor="#1b80b6" border="1" cellspacing="0"
                            style="border-top-color:#d0e0ff;border-style:solid; border-top-width:1px;border-bottom-width:1px; border-left-width:0px;border-right-width:0px;border-top-width:0px;" width="100%">
                            
                            
                             <tr>
                    <td align="left" class="matters" colspan="6" rowspan="4" valign="top">
                        <asp:GridView ID="gvProgress" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                            Width="100%">
                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                            <Columns>
                                
                    <asp:TemplateField  >
                                <HeaderTemplate>
                                <table style="width: 100%; height: 100%">
                                <tr>
                                <td align="center" style="width: 100px">Report Type
                                </td>
                                </tr>
                                </table>
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                <asp:linkbutton ID="lnkPrinted" OnClick="lnkPrinted_Click"  runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:linkbutton>                                    
                                </ItemTemplate>
                                </asp:TemplateField>

                                
                                <asp:TemplateField Visible="FALSE" >
                                <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                <ItemTemplate>
                                <asp:Label ID="lblRpfId"  runat="server" Text='<%# Bind("RPF_ID") %>'></asp:Label>                                    
                                </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField Visible="FALSE" >
                                <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                <ItemTemplate>
                                <asp:Label ID="lblRSMId"  runat="server" Text='<%# Bind("RPF_RSM_ID") %>'></asp:Label>                                    
                                </ItemTemplate>
                                </asp:TemplateField>
                                
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle CssClass="griditem_hilight" />
                            <HeaderStyle CssClass="gridheader_new" Height="25px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
                            </table>
                               </table>
            </asp:Panel>                



            <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <div id="footer">
         <font color="#1b80b6" size="2px">
       
                                                    <asp:Literal ID="ltPnoCurr" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                                </font>
          
       </div>
       <div style="page-break-after:always"></div>
        </td>
        </tr>
    
             
       <tr >
       <td runat="server" id="t27">
     <div ID="header7" runat="server"></div> <br />
                           <table align="center"  bordercolor="#1b80b6" border="1" cellpadding="5" cellspacing="0"
                            style="border-top-color:#d0e0ff;border-style:solid; border-top-width:1px;border-bottom-width:1px; border-left-width:0px;border-right-width:0px;border-top-width:0px; height: 30px;" width="100%">
                                                   
                            <tr style="border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px;" >
                                <td align="left"  colspan="6" style="font-weight: bold; font-size: 8pt; background-image: url(../../Images/bgblue.gif);
                                    color: white; font-family: Verdana;border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px; height: 24px;">
                                    Good Behaviour Details</td>
                            </tr>
                            
                            
                             <tr>
                    <td align="center" class="matters" colspan="6" rowspan="4" valign="top">
                        &nbsp;No Details...</td>
                </tr>
                            </table>
                           <table align="center"  bordercolor="#1b80b6" border="1" cellpadding="5" cellspacing="0"
                            style="border-top-color:#d0e0ff;border-style:solid; border-top-width:1px;border-bottom-width:1px; border-left-width:0px;border-right-width:0px;border-top-width:0px;" width="100%">
                            <tr style="border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px;" >
                                <td align="left"  colspan="6" style="font-weight: bold; font-size: 8pt; background-image: url(../../Images/bgblue.gif);
                                    color: white; font-family: Verdana;border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px; height: 24px;">
                                    Bad Behaviour Details</td>
                            </tr>
                            <tr><td align="center">
                                <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    BorderStyle="None"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                    PageSize="15" Width="616px" SkinID="GridViewNormal">
                                    <RowStyle  Height="25px" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="INCIDENTID" Visible="False">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblStudId" runat="server" Text='<%# Bind("BM_ID") %>'></asp:Label>
                                                <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("STU_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("BM_ENTRY_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Staff Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStudName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActionType">
                                            <ItemTemplate>
                                                <asp:Label ID="lblShift" runat="server" Text='<%# Bind("BM_INCIDENT_TYPE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View">
                                            <ItemTemplate>
                                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                    OnClientClick='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                    Width="35px">View</asp:LinkButton>&nbsp;
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle  Wrap="False" />
                                    <HeaderStyle  Height="30px" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle  Wrap="False" />
                                </asp:GridView>
                            </td></tr>
                            </table>
                           <table align="center"  bordercolor="#1b80b6" border="1" cellpadding="5" cellspacing="0"
                            style="border-top-color:#d0e0ff;border-style:solid; border-top-width:1px;border-bottom-width:1px; border-left-width:0px;border-right-width:0px;border-top-width:0px;" width="100%">
                            <tr style="border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px;" >
                                <td align="left"  colspan="6" style="font-weight: bold; font-size: 8pt; background-image: url(../../Images/bgblue.gif);
                                    color: white; font-family: Verdana;border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px; height: 24px;">
                                    Friends Details</td>
                            </tr>
                            <tr><td align="center" class="matters">No Details...</td></tr>
                            </table>
                           <div id="footer">
                              <font color="#1b80b6" size="2px">
 
                                                    <asp:Literal ID="ltPnoBeh" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                                  </font>
          
       </div>
       <div style="page-break-after:always"></div>
       
       </td>
       </tr>
       <tr >
       <td runat="server" id="t28">
     <div ID="header8" runat="server"></div> <br />
            <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%" class="BlueTable_simple">
            
                    
            <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 24px">
                  Health Details</td>
            </tr>
           <tr >
          <td class="matters" width="80px" >
             Health Card No / Medical Insurance No</td>
          <td  align="center" width="3px">
              :</td>
          <td class="matters" >
             <font color="black"> <asp:Literal ID="ltHth_No" runat="server"></asp:Literal>&nbsp;</font></td>
          <td class="matters" >
              Blood Group</td>
          <td align="center" width="3px">
              :</td>
          <td class="matters" >
             <font color="black"> <asp:Literal ID="ltB_grp" runat="server"></asp:Literal>&nbsp;</font></td>
      </tr>
    <tr>
          <td class="matters" colspan="6" style="font-weight:600; letter-spacing:0.3px; font-size:11pt; background-image: url(../../Images/bgblue.gif); color: white; font-family:Arial, Helvetica, sans-serif;">
         Health Restriction</td>
      </tr>
   <tr>
    <td class="matters" colspan="6">
       Does your child have any allergies? &nbsp;<font color="black"><asp:Literal ID="ltAlg_Detail" 
            runat="server" EnableViewState="False"></asp:Literal>
            </font><div id="divAlg" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div></td>
    </tr>
  <tr>
    <td class="matters" colspan="6">
     Does your child have any prescribed Special Medication? <font color="black"> <asp:Literal ID="ltSp_med" runat="server"></asp:Literal></font>
      <div id="divSp_med" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div></td>
    </tr>
    
   
     <tr >
    <td class=matters colspan="6">
       Is there any Physical Education Restrictions for your child?       <font color="black"> <asp:Literal ID="ltPhy_edu" runat="server"></asp:Literal>&nbsp;</font>
         <div id="divPhy_edu" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div></td>
    </tr>
  
      <tr >
    <td class=matters colspan="6">
       Any other information related to health issue of your child the school should be aware of?
        <font color="black"><asp:Literal ID="ltHth_Info" runat="server"></asp:Literal>&nbsp;</font>
        <div id="divHth_Info" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div></td>
    </tr>
   
   <tr>
    <td class="matters" colspan="6" style="font-weight:600; letter-spacing:0.3px; font-size:11pt; background-image: url(../../Images/bgblue.gif); color: white; font-family:Arial, Helvetica, sans-serif;">
   Applicant's disciplinary, social, physical or psychological detail</td></tr>
      
    <tr >
    <td class=matters colspan="6">
      Has the child received any sort of learning support or theraphy?
        <font color="black"><asp:Literal ID="ltLS_Ther" runat="server"></asp:Literal>&nbsp;</font>
        <div id="divLS_Ther" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div></td>
    </tr>
   
     <tr >
    <td class=matters colspan="6">
      Does the child require any special education needs?
        <font color="black"><asp:Literal ID="ltSEN" runat="server"></asp:Literal>&nbsp;</font>
         <div id="divSEN" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div></td>
    </tr>
   
     <tr>
    <td class=matters colspan="6">
       Does the student require English support as Additional Language program (EAL) ?
          <font color="black"><asp:Literal ID="ltEAL" runat="server"></asp:Literal>&nbsp;</font>
          <div id="divEAL" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div></td>
    </tr>
   
     <tr>
    <td class="matters" colspan="6">
       Has your child's behaviour been any cause for concern in previous schools ?<font color="black">
       <asp:Literal ID="ltPrev_sch" runat="server"></asp:Literal></font>
       <div id="divPrev_sch" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div></td>

    </tr>
            
            
            
        </table>
                             <div id="footer">
        <font color="#1b80b6" size="2px">
       
                                                    <asp:Literal ID="ltPnoSp" runat="server"></asp:Literal>
                                                 &nbsp;&nbsp;&nbsp;
          </font>
       </div>
       <div style="page-break-after:always"></div>
       </td>
       </tr>
       <tr >
       <td runat="server" id="t29">
        <div ID="header9" runat="server"></div> <br />
                     <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
                   
            
            <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 24px">
                    &nbsp;
                    Transport Details</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
        <table class="BlueTable_simple"  runat ="server" id="table5" width="100%" align="center">
          
           <tr>
                <td style="width: 100px" class="matters">
                </td>
                <td class="matters" style="width: 1px" align="center">
                </td>
                <td class="matters33" align="center">
                    <b>PickUp</b></td>
                <td class="matters33" align="center">
                    <b>DropOff</b></td>
             
            </tr>
          
          
          
            
            <tr>
                <td style="width: 200px" class="matters">
                    Location</td>
                <td class="matters" style="width: 1px" align="left">
                    :</td>
                <td class="matters">
                    <asp:Literal ID="Literal7" runat="server"></asp:Literal></td>
                    
                     <td class="matters">
                    <asp:Literal ID="Literal8" runat="server"></asp:Literal></td>
            
            </tr>
            
            
            <tr>
                <td style="width: 200px" class="matters">
                    SubLocation</td>
                <td class="matters" style="width: 1px" align="left">
                    :</td>
                <td class="matters">
                    <asp:Literal ID="Literal9" runat="server"></asp:Literal></td>
                    
                     <td class="matters">
                    <asp:Literal ID="Literal10" runat="server"></asp:Literal></td>
            
            </tr>
            
            <tr>
                <td style="width: 200px" class="matters">
                    Bus No</td>
                <td class="matters" style="width: 1px" align="left">
                    :</td>
                <td class="matters">
                    <asp:Literal ID="Literal11" runat="server"></asp:Literal></td>
                    
                     <td class="matters">
                    <asp:Literal ID="Literal12" runat="server"></asp:Literal></td>
            
            </tr>
            
            
            <tr>
                <td style="width: 200px" class="matters">
                    Trip</td>
                <td class="matters" style="width: 1px" align="left">
                    :</td>
                <td class="matters">
                    <asp:Literal ID="Literal13" runat="server"></asp:Literal></td>
                    
                     <td class="matters">
                    <asp:Literal ID="Literal14" runat="server"></asp:Literal></td>
            
            </tr>     
        </table>
                    &nbsp;&nbsp;
              <span style="display:none" ><asp:LinkButton ID="lnkinsertnew" runat="server" >View Transaction History</asp:LinkButton></span></td>
            </tr>
        </table>
                     <div id="footer">
          <font color="#1b80b6" size="2px">
      
                                                    <asp:Literal ID="ltPnoTran" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                               </font>
          
       </div>
       <div style="page-break-after:always"></div>
       </td>
       </tr>
       <tr >
       <td runat="server" id="t30">
       <div ID="header10" runat="server"></div> <br />
                      <table border="1" bordercolor="#1b80b6" cellpadding="0" cellspacing="0"
            style="border-right: 0px solid; border-top: #d0e0ff 0px solid; border-left: 0px solid; border-bottom: 1px solid" width="100%">
          
            
             <tr style="border-right: 0px; border-top: 0px; border-left: 0px">
                <td align="left" colspan="6" style="border-right: 0px; border-top: 0px; font-weight: bold;
                    font-size: 8pt; background-image: url(../../Images/bgblue.gif); border-left: 0px;
                    color: white; font-family: Verdana; height: 24px">
                    &nbsp;&nbsp;
             Library Transaction History</td>
            </tr>
            <tr>
                <td class="matters" colspan="6" rowspan="4" valign="top">
                <table align="center"  bordercolor="#1b80b6" border="1" cellspacing="0"
                            style="border-top-color:#d0e0ff;border-style:solid; border-top-width:1px;border-bottom-width:1px; border-left-width:0px;border-right-width:0px;border-top-width:0px;" width="100%">
                                   <tr>
                    <td align="left" class="matters" colspan="6" rowspan="4" valign="top">
                        <asp:GridView ID="gvLibDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                            Width="100%">
                            <EmptyDataRowStyle CssClass="gridheader_new" Wrap="True" />
                            <Columns>
                                
                                <asp:BoundField DataField="Sl_No" HeaderText="Sl No" ReadOnly="True" />
                                <asp:TemplateField SortExpression="LIB_TYPE" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Library</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 <ItemStyle HorizontalAlign="left"></ItemStyle>
                                 <ItemTemplate>
                        <asp:Label ID="lblLIB_TYPE"  runat="server" Text='<%# Bind("LIB_TYPE") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                                
                                 <asp:TemplateField SortExpression="LIB_CAT" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Category</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 
                                 <ItemTemplate>
                        <asp:Label ID="lblLIB_CAT"  runat="server" Text='<%# Bind("LIB_CAT") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                                
                                
                                
                                  <asp:TemplateField SortExpression="LIB_TITLE" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Title</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 
                                 <ItemTemplate>
                        <asp:Label ID="lblLIB_TITLE"  runat="server" Text='<%# Bind("LIB_TITLE") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                
                                
                                     
                                  <asp:TemplateField SortExpression="LIB_ISSUEDATE" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Issue Date</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 
                                 <ItemTemplate>
                        <asp:Label ID="lblLIB_ISSUEDATE"  runat="server" Text='<%# Bind("LIB_ISSUEDATE") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                    
                    <asp:TemplateField SortExpression="LIB_RETDATE" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Return Date</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 
                                 <ItemTemplate>
                        <asp:Label ID="lblLIB_RETDATE"  runat="server" Text='<%# Bind("LIB_RETDATE") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                    
                    
                       <asp:TemplateField SortExpression="LIB_ACTRETDATE" >
                                 <HeaderTemplate>
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td align="center" style="width: 100px">
                                    Actual Return Date</td>
                            </tr>
                            </table>
                            </HeaderTemplate>
                                 
                                 <ItemTemplate>
                        <asp:Label ID="lblLIB_ACTRETDATE"  runat="server" Text='<%# Bind("LIB_ACTRETDATE") %>'></asp:Label>                                    
                    </ItemTemplate>
                    </asp:TemplateField>
                                                          
                            </Columns>
                            <RowStyle CssClass="griditem" Height="25px" />
                            <SelectedRowStyle CssClass="griditem_hilight" />
                            <HeaderStyle CssClass="gridheader_new" Height="25px" />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
                            </table>
   </div>
   <div>
                <table align="center"  bordercolor="#1b80b6" border="1" cellpadding="5" cellspacing="0"
                            style="border-top-color:#d0e0ff;border-style:solid; border-top-width:1px;border-bottom-width:1px; border-left-width:0px;border-right-width:0px;border-top-width:0px;" width="100%">
                            <tr style="border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px;" >
                                <td align="left"  colspan="6" style="font-weight: bold; font-size: 8pt; background-image: url(../../Images/bgblue.gif);
                                    color: white; font-family: Verdana;border-top-style:none; border-top-width:0px;border-left-style:none; border-left-width:0px;border-right-style:none; border-right-width:0px; height: 24px;">
                                    Reservations</td>
                            </tr>
                            <tr><td align="left" class="matters3">No Reservations Made...</td></tr>
                            </table>
                            
                              
            </td>
            </tr>
        </table>
                      <div id="footer">
        <font color="#1b80b6" size="2px">
       
                                                    <asp:Literal ID="ltPnoLib" runat="server"></asp:Literal>
                                                 &nbsp;&nbsp;&nbsp;</font>
          
       </div>
       <div style="page-break-after:always"></div>
       </td>
       </tr>
       <tr >
       <td runat="server" id="t31">
    <div ID="header11" runat="server"></div> <br />
       
     <uc1:Dash_ManageSMS ID="ComManageSMS1" runat="server" />
     
      <div id="footer">
       <font color="#1b80b6" size="2px">
      
                                                    <asp:Literal ID="ltPnoSMS" runat="server"></asp:Literal>
                                                  &nbsp;&nbsp;&nbsp;</font>
          
       </div>
       
       </td>
       </tr>
       </table>
       
       <%-- </ContentTemplate>
        </asp:UpdatePanel>--%>
       
 </form>
</body>
</html>


