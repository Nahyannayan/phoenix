<%@ Page Language="VB" AutoEventWireup="true" CodeFile="STU_TAB_OTH.aspx.vb" Inherits="STU_TAB_OTH" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
<%--  <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
           <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    
    <script type="text/javascript" >

        //Javascript Error Handling

        function handleError() {

            return true;
        }
        window.onerror = handleError;

    
    
    </script>
    
    
</head>
<body>
    <form id="form1" runat="server">
    <div> 
                   <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite"> Comments</td>
                            </tr>
                            <tr><td align="center">
                              <%--  <asp:GridView ID="gvStudGrade_POS" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    BorderStyle="None"  EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                    PageSize="15" Width="616px" SkinID="GridViewNormal">
                                    <RowStyle  Height="25px" Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="INCIDENTID" Visible="False">
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblStudId" runat="server" Text='<%# Bind("BM_ID") %>'></asp:Label>
                                                <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("STU_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("BM_ENTRY_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Staff Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStudName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActionType">
                                            <ItemTemplate>
                                                <asp:Label ID="lblShift" runat="server" Text='<%# Bind("BM_INCIDENT_TYPE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View" Visible="False">
                                            <ItemTemplate>
                                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                    OnClientClick='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                    Width="35px">View</asp:LinkButton>&nbsp;
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle  Wrap="False" />
                                    <HeaderStyle  Height="30px" Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle  Wrap="False" />
                                </asp:GridView>--%>
                                
                                 <asp:GridView ID="gvStudGrade_POS" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                        EmptyDataText="Information not available." Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                  
                                         <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
                    </ajaxToolkit:ToolkitScriptManager>
                                       
                                                Comments
                                          
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Elblview1" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label>
                                    <asp:Panel ID="E4Panel11" runat="server" >
                                        <%#Eval("STM_ENR_COMMENTS")%>
                                    </asp:Panel>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                        AutoCollapse="False" AutoExpand="False" CollapseControlID="Elblview1" Collapsed="true"
                                        CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="Elblview1"
                                        ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="E4Panel11"
                                        TextLabelID="Elblview1">
                                    </ajaxToolkit:CollapsiblePanelExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                   
                                                Entry Date
                                           
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <center>
                                        <%# Eval("STM_ENTRY_DATE")%>
                                    </center>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="griditem"  />
                        <EmptyDataRowStyle />
                        <SelectedRowStyle  />
                        <HeaderStyle  />
                        <EditRowStyle />
                        <AlternatingRowStyle CssClass="griditem_alternative"  />
                    </asp:GridView>
                            </td></tr>
                            </table>
                            
                            
               
                            
                            

 </div>
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <asp:HiddenField ID="HF_stuid" runat="server" />
    </form>
</body>
</html>
