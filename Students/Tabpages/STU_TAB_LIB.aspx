<%@ Page Language="VB" AutoEventWireup="true" CodeFile="STU_TAB_LIB.aspx.vb" Inherits="STU_TAB_LIB" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <%--  <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Library Membership(s)</td>
                </tr>
                <tr>
                    <td>

                        <br />
                        <asp:GridView ID="GridMemberships"   CssClass="table table-bordered table-row" 
                            runat="server" Width="100%" EmptyDataText="Membership not assigned for this user." AutoGenerateColumns="false">
                            <Columns>

                                <asp:TemplateField HeaderText="Library Divisions">
                                    <HeaderTemplate>
                                       Library Divisions
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("LIBRARY_DIVISION_DES")%>
                                        </center>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Library Code">
                                    <HeaderTemplate>
                                        Library Membership
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("MEMBERSHIP_DES")%>
                                        </center>
                                    </ItemTemplate>

                                </asp:TemplateField>



                            </Columns>
                            <RowStyle CssClass="griditem"  />
                            <EmptyDataRowStyle  />
                            <SelectedRowStyle  />
                            <HeaderStyle  />
                            <EditRowStyle  />
                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                        </asp:GridView>


                        <br />
                    </td>
                </tr>
            </table>

            <br />

           <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite"> Library Usage</td>
                </tr>
                <tr>
                    <td  valign="top">
                        <table align="center" border="0" cellspacing="0" width="100%">


                            <tr>
                                <td align="left" colspan="6" rowspan="4" valign="top">
                                    <br />
                                    <asp:GridView ID="GrdUserTransaction" runat="server" AllowPaging="True" EmptyDataText="No transactions done yet." AutoGenerateColumns="false"
                                     CssClass="table table-bordered table-row"   Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Stock ID">
                                                <HeaderTemplate>
                                                    Accession No
                                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("ACCESSION_NO")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Item Image">
                                                <HeaderTemplate>
                                                    Item Image
                                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:Image ID="Image4" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' />
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Item Name">
                                                <HeaderTemplate>
                                                    Item Name
                                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>

                                                    <%#Eval("ITEM_TITLE")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Library">
                                                <HeaderTemplate>
                                                   Library
                                                          
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("LIBRARY_DIVISION_DES")%>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Issue Period">
                                                <HeaderTemplate>
                                                   Issue Date
                                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("ITEM_TAKEN_DATE")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Tentative Period">
                                                <HeaderTemplate>
                                                    Tentative Date
                                                            
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("ITEM_RETURN_DATE")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Return Date">
                                                <HeaderTemplate>
                                                   Return Date
                                                         
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center><%#Eval("RTN_DATE")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Fine">
                                                <HeaderTemplate>
                                                    Fine
                                                           
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <center>
                                                        <%#Eval("FINE_AMOUNT") %>
                                                        <%#Eval("CURRENCY_ID") %>
                                                    </center>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>
                                        <RowStyle CssClass="griditem"  />
                                        <EmptyDataRowStyle  />
                                        <SelectedRowStyle  />
                                        <HeaderStyle  />
                                        <EditRowStyle />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                    </asp:GridView>
                                    <br />
                                </td>
                            </tr>
                        </table>
                         </td>
                </tr>
            </table>
        </div>
        <div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Item(s) Due</td>
                </tr>
                <tr>
                    <td align="left" >

                        <br />

                        <asp:GridView ID="GridDue" runat="server" AllowPaging="True" EmptyDataText="No Items on Due." AutoGenerateColumns="false" CssClass="table table-bordered table-row" 
                            Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Stock ID">
                                    <HeaderTemplate>
                                       Accession No
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("ACCESSION_NO")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Image">
                                    <HeaderTemplate>
                                        Item Image
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:Image ID="Image4" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Name">
                                    <HeaderTemplate>
                                       Item Name
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <%#Eval("ITEM_TITLE")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Library">
                                    <HeaderTemplate>
                                        Library
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("LIBRARY_DIVISION_DES")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Issue Period">
                                    <HeaderTemplate>
                                       Issue Date
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("ITEM_TAKEN_DATE")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Tentative Period">
                                    <HeaderTemplate>
                                        Tentative Date
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("ITEM_RETURN_DATE")%> </center>
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                            <RowStyle CssClass="griditem"  />
                            <EmptyDataRowStyle  />
                            <SelectedRowStyle  />
                            <HeaderStyle  />
                            <EditRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                        </asp:GridView>

                        <br />

                    </td>
                </tr>
            </table>


             <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Reservations</td>
                </tr>
                <tr>
                    <td align="left" >

                        <br />

                        <asp:GridView ID="GridReservations" runat="server" AllowPaging="True" EmptyDataText="No Items Reserved." AutoGenerateColumns="false"
                          CssClass="table table-bordered table-row"  Width="100%">
                            <Columns>

                                <asp:TemplateField HeaderText="Item Image">
                                    <HeaderTemplate>
                                        Item Image
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <asp:Image ID="Image4" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' />
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Name">
                                    <HeaderTemplate>
                                       Item Name
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <%#Eval("ITEM_TITLE")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Library">
                                    <HeaderTemplate>
                                       Library
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%#Eval("LIBRARY_DIVISION_DES")%>
                                        </center>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Reserved Date">
                                    <HeaderTemplate>
                                        Reserved Date
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("ENTRY_DATE")%></center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Period">
                                    <HeaderTemplate>
                                        Period Reserved
                                               
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("RESERVE_START_DATE")%> - <%#Eval("RESERVE_END_DATE")%> </center>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status">
                                    <HeaderTemplate>
                                        Status
                                              
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center><%#Eval("CANCEL_RESERVATION")%> </center>
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle />
                            <SelectedRowStyle  />
                            <HeaderStyle />
                            <EditRowStyle  />
                            <AlternatingRowStyle CssClass="griditem_alternative"  />
                        </asp:GridView>

                        <br />

                    </td>
                </tr>
            </table>




          <%--  </td>
            </tr>
        </table>--%>
        </div>
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <asp:HiddenField ID="HF_stuid" runat="server" />
    </form>
</body>
</html>

