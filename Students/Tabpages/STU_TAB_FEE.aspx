<%@ Page Language="VB" AutoEventWireup="true" CodeFile="STU_TAB_FEE.aspx.vb" Inherits="STU_TAB_FEE" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
   <%-- <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />

    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
      <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite">Fee Details</td>
                </tr>
                <tr>
                    <td valign="top">
                        <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                          CssClass="table table-bordered table-row"  Width="100%">
                            <EmptyDataRowStyle />
                            <Columns>


                                <asp:TemplateField SortExpression="FEE_TYPE">
                                    <HeaderTemplate>
                                       Fee Type
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblFEE_TYPE" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField SortExpression="OPENING">
                                    <HeaderTemplate>
                                       Opening
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblOpening" runat="server" Text='<%# Bind("OPENING") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="Monthly_Amount">
                                    <HeaderTemplate>
                                       Monthly Charge
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblMC" runat="server" Text='<%# Bind("Monthly_Amount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField SortExpression="Conc_Amount">
                                    <HeaderTemplate>
                                       Concession
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCC" runat="server" Text='<%# Bind("Conc_Amount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField SortExpression="Paid_Amount">
                                    <HeaderTemplate>
                                       Paid
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblPA" runat="server" Text='<%# Bind("Paid_Amount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField SortExpression="Amount">
                                    <HeaderTemplate>
                                       Balance
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBal" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <SelectedRowStyle />
                            <HeaderStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td valign="top">&nbsp;</td>
                </tr>
            </table>
        </div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="title-bg-lite"> Payment History</td>
            </tr>
            <tr>
                <td  valign="top">

                    <asp:GridView ID="gvPaymentHist" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                       CssClass="table table-bordered table-row"  Width="100%">
                        <EmptyDataRowStyle  />
                        <Columns>

                            <asp:BoundField DataField="Sl_No" HeaderText="Sl No" ReadOnly="True" />
                            <asp:TemplateField SortExpression="FCL_DATE">
                                <HeaderTemplate>
                                    Rct. Date
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFCL_DATE" runat="server" Text='<%# Bind("FCL_DATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField SortExpression="FCL_RECNO">
                                <HeaderTemplate>
                                   Rct No
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFCL_RECNO" runat="server" Text='<%# Bind("FCL_RECNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="FCL_NARRATION">
                                <HeaderTemplate>
                                    Narration
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFCL_NARRATION" runat="server" Text='<%# Bind("FCL_NARRATION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="FCL_AMOUNT">
                                <HeaderTemplate>
                                   Amount
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFCL_AMOUNT" runat="server" Text='<%# Bind("FCL_AMOUNT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <RowStyle CssClass="griditem"  />
                        <SelectedRowStyle  />
                        <HeaderStyle  />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </td>
            </tr>
        </table>

        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <asp:HiddenField ID="HF_stuid" runat="server" />
    </form>
</body>
</html>

