Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Xml
Imports System.Data.SqlTypes
Partial Class STU_TAB_FEE
    Inherits System.Web.UI.Page
  
    Dim Encr_decrData As New Encryption64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                If Session("sbsuid") = "" Or Session("sbsuid") Is Nothing Then
                    Panel1.Visible = False
                    lblSession.Text = "Your session has expired!!!.Please login again"
                    Exit Sub
                End If

                HF_stuid.Value = Session("db_stu_id")
                hfACD_ID.Value = Session("Current_ACD_ID")
                checkCBSESchool()
                BindSubjectList()
                BindProgressReport()
                getStudentGrade()

                'If Session("sbsuid") = "125005" Then

                'Else
                '    tblPer.Visible = False
                'End If

                If hfCBSESchool.Value = "1" And hfGRD_ID.Value = "11" Or hfGRD_ID.Value = "12" Then
                    tblPer.Visible = False
                Else
                    BindReports()
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Sub checkCBSESchool()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM VW_ACADEMICYEAR_D WHERE ACD_CLM_ID=1 AND ACD_ID='" + Session("CURRENT_ACD_ID") + "'"

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count > 0 Then
            hfCBSESchool.Value = "1"
        Else
            hfCBSESchool.Value = "0"
        End If
    End Sub
    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim STR_QUERY As String

        If hfCBSESchool.Value = "1" Then
            STR_QUERY = "EXEC [RPT].[GETREPORTCARDBYYEAR_CBSE] " _
                          & "'" + Session("SBSUID") + "'," _
                          & "'" + hfGRD_ID.Value + "'," _
                          & Session("CLM").ToString
        Else
            STR_QUERY = "EXEC RPT.GETREPORTCARDBYYEAR " _
                           & "'" + Session("SBSUID") + "'," _
                           & "'" + hfGRD_ID.Value + "'," _
                           & Session("CLM").ToString
        End If

        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, STR_QUERY)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString
        str = str.Replace("ACY_ID", "ID")
        str = str.Replace("ACY_DESCR", "TEXT")
        str = str.Replace("RPF_ID", "ID")
        str = str.Replace("RPF_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvReport.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvReport.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvReport.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)


        tvReport.ExpandAll()

    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub

    'Function getReportCards() As String
    '    Dim str As String = ""
    '    Dim i As Integer

    '    For i = 0 To lstReportSchedule.Items.Count - 1
    '        If lstReportSchedule.Items(i).Selected = True Then
    '            If str <> "" Then
    '                str += "|"
    '            End If
    '            str += lstReportSchedule.Items(i).Text
    '        End If
    '    Next

    '    Return str
    'End Function
    Function getReportCards() As String
        Dim node As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode

        Dim strRPF As String = ""

        Dim bRSM As Boolean = False

        For Each node In tvReport.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        If strRPF <> "" Then
                            strRPF += "|"
                        End If
                        strRPF += cNode.Text + "_" + ccNode.Text
                    End If
                Next
            Next
        Next


        Return strRPF.Trim
    End Function
    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        


        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_DESCR", getReportCards())
        param.Add("@STM_ID", "1")
        param.Add("@STU_ID", HF_stuid.Value)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            If hfCBSESchool.Value = "1" Then
                param.Add("@bEVALUATION", "false")
                .reportPath = Server.MapPath("../../Curriculum/Reports/Rpt/rptStudentPerformanceAcrossYear_CBSE.rpt")
            Else
                param.Add("UserName", Session("sUsr_name"))
                param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
                param.Add("accYear", hfACD_ID.Value)
                param.Add("grade", hfGRD_ID.Value)
                param.Add("@bEVALUATION", "false")
                .reportPath = Server.MapPath("../../Curriculum/Reports/Rpt/rptINTLStudentPerformanceAcrossYear.rpt")
            End If
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer_princi.aspx")
        Dim rptDownload As New ReportDownload
        rptDownload.LoadReports(rptClass, rs)
        rptDownload = Nothing
       
    End Sub
    Sub getStudentGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_GRD_ID FROM STUDENT_M WHERE STU_ID='" + HF_stuid.Value + "'"
        hfGRD_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub


    Sub BindSubjectList()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim str_query As String = " SELECT DISTINCT CASE WHEN SBG_PARENTS='NA' THEN SBG_DESCR ELSE SBG_PARENTS+'-'+SBG_DESCR END SBG_DESCR,SGR_DESCR, TEACHERS=ISNULL((SELECT " _
                               & "  STUFF((SELECT ', '+ ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,' ')+' '+ " _
                               & " ISNULL(EMP_LNAME,'') FROM EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S " _
                               & " AS L ON K.EMP_ID=L.SGS_EMP_ID   WHERE SGS_SGR_ID = C.SGR_ID" _
                               & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,'')),'')" _
                               & " FROM  STUDENT_GROUPS_S AS A INNER JOIN" _
                               & " SUBJECTS_GRADE_S AS B ON A.SSD_SBG_ID=B.SBG_ID" _
                               & " LEFT OUTER JOIN GROUPS_M AS C ON A.SSD_SGR_ID=C.SGR_ID" _
                               & " WHERE SSD_STU_ID=" + HF_stuid.Value + " AND SSD_ACD_ID='" + Session("CURRENT_ACD_ID") + "' ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjects.DataSource = ds
        gvSubjects.DataBind()
    End Sub

    Sub BindProgressReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        'Dim str_query As String = "SELECT DISTINCT RPF_ID,RPF_DESCR,RPF_DISPLAYORDER,RPF_RSM_ID FROM RPT.REPORT_PRINTEDFOR_M AS A " _
        '                      & " INNER JOIN rpt.REPORT_STUDENT_S AS B ON A.RPF_ID=B.RST_RPF_ID " _
        '                      & " WHERE RST_STU_ID=" + HF_stuid.Value _
        '                      & " AND RST_ACD_ID=" + hfACD_ID.Value _
        '                      & " ORDER BY RPF_DISPLAYORDER"

        Dim str_query As String = "SELECT DISTINCT RPF_ID,ACY_DESCR+ '-' + RPF_DESCR AS RPF_DESCR,RPF_DISPLAYORDER,RPF_RSM_ID,ACY_DESCR,RSM_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                           & " INNER JOIN rpt.REPORT_STUDENT_S AS B ON A.RPF_ID=B.RST_RPF_ID " _
                           & " INNER JOIN RPT.REPORT_SETUP_M AS E ON A.RPF_RSM_ID=E.RSM_ID" _
                           & " INNER JOIN VW_ACADEMICYEAR_D AS C ON B.RST_ACD_ID=C.ACD_ID" _
                           & " INNER JOIN VW_ACADEMICYEAR_M AS D ON C.ACD_ACY_ID=D.ACY_ID" _
                           & " WHERE RPF_DESCR<>'BROWNBOOK' AND RST_STU_ID=" + HF_stuid.Value


        str_query += " UNION ALL "

        str_query += "SELECT DISTINCT RPF_ID,ACY_DESCR+ '-' + RPF_DESCR AS RPF_DESCR,RPF_DISPLAYORDER,RPF_RSM_ID,ACY_DESCR,RSM_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                           & " INNER JOIN rpt.FINALPROCESS_REPORT_S AS B ON A.RPF_ID=B.FPR_RPF_ID " _
                           & " INNER JOIN RPT.REPORT_SETUP_M AS E ON A.RPF_RSM_ID=E.RSM_ID" _
                           & " INNER JOIN VW_ACADEMICYEAR_D AS C ON B.FPR_ACD_ID=C.ACD_ID" _
                           & " INNER JOIN VW_ACADEMICYEAR_M AS D ON C.ACD_ACY_ID=D.ACY_ID" _
                           & " WHERE RPF_DESCR<>'BROWNBOOK' AND FPR_STU_ID=" + HF_stuid.Value

        str_query += " UNION ALL "

        str_query += "SELECT DISTINCT RPF_ID,ACY_DESCR+ '-' + RPF_DESCR AS RPF_DESCR,RPF_DISPLAYORDER,RPF_RSM_ID,ACY_DESCR,RSM_DISPLAYORDER S FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                              & " INNER JOIN rpt.REPORT_STUDENT_PREVYEARS AS B ON A.RPF_ID=B.RST_RPF_ID " _
                              & " INNER JOIN RPT.REPORT_SETUP_M AS E ON A.RPF_RSM_ID=E.RSM_ID" _
                              & " INNER JOIN VW_ACADEMICYEAR_D AS C ON B.RST_ACD_ID=C.ACD_ID" _
                              & " INNER JOIN VW_ACADEMICYEAR_M AS D ON C.ACD_ACY_ID=D.ACY_ID" _
                              & " WHERE RPF_DESCR<>'BROWNBOOK' AND  RST_STU_ID=" + HF_stuid.Value

        str_query += " UNION ALL "

        str_query += "SELECT DISTINCT RPF_ID,ACY_DESCR+ '-' + RPF_DESCR AS RPF_DESCR,RPF_DISPLAYORDER,RPF_RSM_ID,ACY_DESCR,RSM_DISPLAYORDER FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                                  & " INNER JOIN rpt.FINALPROCESS_REPORT_PREVYEARS AS B ON A.RPF_ID=B.FPR_RPF_ID " _
                                  & " INNER JOIN RPT.REPORT_SETUP_M AS E ON A.RPF_RSM_ID=E.RSM_ID" _
                                  & " INNER JOIN VW_ACADEMICYEAR_D AS C ON B.FPR_ACD_ID=C.ACD_ID" _
                                  & " INNER JOIN VW_ACADEMICYEAR_M AS D ON C.ACD_ACY_ID=D.ACY_ID" _
                                  & " WHERE RPF_DESCR<>'BROWNBOOK' AND FPR_STU_ID=" + HF_stuid.Value

        str_query = "SELECT DISTINCT RPF_ID,RPF_DESCR,RPF_DISPLAYORDER,RPF_RSM_ID,ACY_DESCR,RSM_DISPLAYORDER  FROM(" + str_query + ")P ORDER BY ACY_DESCR,RSM_DISPLAYORDER ,RPF_DISPLAYORDER"




        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvProgress.DataSource = ds
        gvProgress.DataBind()
    End Sub
    Protected Sub lnkPrinted_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If Session("sbsuid") = "" Or Session("sbsuid") Is Nothing Then
                Panel1.Visible = False
                lblSession.Text = "Your session has expired!!!.Please login again"
                Exit Sub
            End If

            Dim lblRpfId As New Label
            Dim lblRsmId As New Label
            Dim url As String
            Dim viewid As String
            lblRpfId = TryCast(sender.FindControl("lblRpfId"), Label)
            lblRsmId = TryCast(sender.FindControl("lblRsmId"), Label)


            '  ViewState("MainMnu_code") = "StudentProfile"
            ViewState("MainMnu_code") = "PdfReport"

            Dim str As String = "~/Curriculum/Reports/Aspx/rptMonthlyProgressReportStudWise.aspx?" _
                              & "MainMnu_code=" + Encr_decrData.Encrypt(ViewState("MainMnu_code")) _
                              & "&datamode=" + Encr_decrData.Encrypt("view") _
                              & "&rsmid=" + Encr_decrData.Encrypt(lblRsmId.Text) _
                              & "&rpfid=" + Encr_decrData.Encrypt(lblRpfId.Text) _
                              & "&acdid=" + Encr_decrData.Encrypt(hfACD_ID.Value) _
                              & "&stuid=" + Encr_decrData.Encrypt(HF_stuid.Value) _
                              & "&grdid=" + Encr_decrData.Encrypt(hfGRD_ID.Value)
            Response.Redirect(str)
        Catch ex As Exception
            lblerror.Text = "Request could not be processed "
        End Try
    End Sub
    'Private Sub GenerateMonthlyProgressReport()
    '    Dim param As New Hashtable
    '    param.Add("@ACD_ID", hfACD_ID.Value)
    '    param.Add("@BSU_ID", Session("SBSUID"))
    '    param.Add("@RSM_ID", ddlReportType.SelectedValue)
    '    param.Add("@RPF_ID", ddlReportPrintedFor.SelectedValue)
    '    param.Add("@STU_ID", HF_stuid.Value)
    '    'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
    '    'param.Add("UserName", Session("sUsr_name"))
    '    Dim rptClass As New rptClass
    '    With rptClass
    '        .crDatabase = "oasis_curriculum"
    '        .reportParameters = param
    '        .Photos = GetPhotoClass()
    '        Select Case GetReportType()
    '            Case "CIS_MONTHLY_REPORT" 'Monthly Progress Report
    '                .reportPath = Server.MapPath("../Rpt/rptMONTHLY_PROGRESS_REPORT_CIS.rpt")
    '            Case "CIS_FINAL_REPORT" ' Final Report
    '                'Select Case ddlGrade.SelectedValue
    '                'Case "KG1", "KG2"
    '                '    .reportPath = Server.MapPath("../Rpt/rptFINAL_PROGRESS_REPORT_CIS_A.rpt")
    '                'Case Else
    '                '    .reportPath = Server.MapPath("../Rpt/rptFINAL_PROGRESS_REPORT_CIS_B.rpt")
    '                'End Select
    '                '.reportPath = Server.MapPath("../Rpt/rptCIS_SUB_FINALREPORT.rpt")

    '                .reportPath = Server.MapPath("../Rpt/rptCIS_PROGRESS_REPORT.rpt")
    '        End Select
    '    End With
    '    Session("rptClass") = rptClass
    '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    'End Sub
    'Function GetPhotoClass() As OASISPhotos
    '    Dim vPhoto As New OASISPhotos
    '    vPhoto.BSU_ID = Session("sBSUID")
    '    vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO
    '    Dim arrList As ArrayList = New ArrayList(h_STU_ID.Value.Split("___"))
    '    For Each vVal As Object In arrList
    '        If vVal.ToString <> "" Then
    '            vPhoto.IDs.Add(vVal)
    '        End If
    '    Next
    '    Return vPhoto
    'End Function
    'Public Function GetReportType() As String
    '    Dim strGRD_ID As String = ddlGrade.SelectedValue
    '    Dim strACD_ID As String = ddlAca_Year.SelectedValue
    '    Dim strBSSU_ID As String = Session("sBSUID")
    '    Dim strRSM_ID As String = ddlReportType.SelectedValue
    '    Dim str_sql As String = " SELECT dbo.GetReportType('" & strBSSU_ID & "', " & strACD_ID & ", '" & strGRD_ID & "', " & strRSM_ID & " )"
    '    Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    'End Function

    'Private Function GETStud_photoPath(ByVal STU_ID As String) As String
    '    STU_ID = CStr(CInt(STU_ID))
    '    Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FROM STUDENT_M where  STU_ID='" & STU_ID & " '"
    '    Dim RESULT As String
    '    Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
    '        Dim command As SqlCommand = New SqlCommand(sqlString, connection)
    '        command.CommandType = Data.CommandType.Text
    '        RESULT = command.ExecuteScalar
    '        SqlConnection.ClearPool(connection)
    '    End Using
    '    Return RESULT

    'End Function


    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub
End Class
