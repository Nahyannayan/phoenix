Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class STU_TAB_FEE
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    'Protected Sub FeeSponsor_Update(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If e.ToString = "1" Then
    '        mpxFeeSponsor.Show()
    '    Else
    '        mpxFeeSponsor.Hide()
    '    End If
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        GridBindFeeDetails()

        GridBindPayHist()


    End Sub

    Private Sub GridBindFeeDetails()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(5) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
                pParms(0).Value = Format(Date.Parse(Now.Date), "dd/MMM/yyyy")
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = Session("sbsuid")
                pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
                pParms(2).Value = Session("db_stu_id")
                pParms(3) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
                pParms(3).Value = "S"
                pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
                pParms(4).Value = Session("Current_ACD_ID")



                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "FEES.F_GetFeeDetailsFOrCollection", pParms)
                gvFeeDetails.DataSource = ds
                gvFeeDetails.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub



    Private Sub GridBindPayHist()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
                pParms(0).Value = Session("db_stu_id")

                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "FEES.Dashboard_PaymentHistory", pParms)
                gvPaymentHist.DataSource = ds
                gvPaymentHist.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

End Class
