<%@ Page Language="VB" AutoEventWireup="false" CodeFile="STU_Maindetails.aspx.vb" Inherits="Students_Tabpages_STU_Maindetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
 <%--    <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>

    
    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body>
    <form id="form1" runat="server">
    <div>
       <table align="center"  border="0" cellspacing="0"
                            style="border-top-color:#d0e0ff;border-style:solid; border-top-width:1px;border-bottom-width:1px; border-left-width:0px;border-right-width:0px;border-top-width:0px; height: 30px;" width="100%">
                            <tr >
                                <td align="left"  colspan="4" class="title-bg-lite">
                                    Student Personal Details</td>
                            </tr>
           <tr>
               <td  colspan="4" rowspan="4" valign="top">
                   <table width="100%" class="table table-bordered">
                       <tr>
                           <td align="left"  >
                                  <span  class="field-label">  Date of Birth</span></td>
                          
                           <td  align="left" >
                               <asp:Literal ID="txtDob" runat="server"></asp:Literal></td>
                           <td  align="left" >
                             <span  class="field-label">  Place of Birth</span></td>
                          
                           <td  align="left" >
                               <asp:Literal ID="LTpob" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                                  <span  class="field-label">  Country of Birth</span></td>
                  
                           <td  align="left" >
                               <asp:Literal ID="LTcunbirth" runat="server"></asp:Literal></td>
                           <td  align="left" >
                                  <span  class="field-label">  Nationality</span></td>
                    
                           <td  align="left" >
                               <asp:Literal ID="LTnationality" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                                   <span  class="field-label"> Gender</span></td>
                        
                           <td  align="left" >
                               <asp:Literal ID="LTgender" runat="server"></asp:Literal></td>
                           <td  align="left" >
                                   <span  class="field-label"> Religion</span></td>
                      
                           <td  align="left" >
                               <asp:Literal ID="LTreligion" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="title-bg-lite" colspan="4" >
                               Student Join Details</td>
                       </tr>
                       <tr>
                           <td  align="left" >
                              <span  class="field-label">  Fee ID</span></td>
                         
                           <td  align="left" >
                                <asp:Literal ID="txtFee_ID" runat="server"></asp:Literal></td>
                           <td  align="left" >
                                  <span  class="field-label">  MOE Reg#</span></td>
                          
                           <td  align="left" >
                               <asp:Literal ID="txtMOE_No" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                              <span  class="field-label"> Date of Join</span></td>
                    
                           <td  align="left" >
                                <asp:Literal ID="txtDOJ" runat="server"></asp:Literal></td>
                           <td  align="left" >
                                   <span  class="field-label"> Date of Join(Ministry)</span></td>
                     
                           <td  align="left" >
                                <asp:Literal ID="txtMINDOJ" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                                  <span  class="field-label">  Tranfer Type</span></td>
                        
                           <td  align="left" >
                                <asp:Literal ID="TLtftype" runat="server"></asp:Literal></td>
                           <td  align="left" >
                                   <span  class="field-label"> Join Academic Year</span></td>
                        
                           <td  align="left" >
                                <asp:Literal ID="txtACD_ID_Join" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                                   <span  class="field-label"> Join Grade</span></td>
                        
                           <td  align="left" >
                               <asp:Literal ID="txtGRD_ID_Join" runat="server"></asp:Literal></td>
                           <td  align="left" >
                                  <span  class="field-label">  Join Section</span></td>
                      
                           <td  align="left" >
                               <asp:Literal ID="txtSCT_ID_JOIN" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                                   <span  class="field-label"> Join Shift</span></td>
                         
                           <td  align="left" >
                               
                                <asp:Literal ID="txtJoin_Shift" runat="server"></asp:Literal></td>
                           <td  align="left" >
                                  <span  class="field-label">  Join Stream</span></td>
                       
                           <td  align="left" >
                               
                                <asp:Literal ID="txtJoin_Stream" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                              <span  class="field-label"> Ministry List</span></td>
                        
                           <td  align="left" >
                                <asp:Literal ID="LTminlist" runat="server"></asp:Literal></td>
                           <td  align="left" >
                              <span  class="field-label"> Ministry List Type</span></td>
                       
                           <td  align="left" >
                                <asp:Literal ID="LTminlistType" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                                  <span  class="field-label">  Emergency Contact</span></td>
                       
                           <td  align="left" >
                               <asp:Literal ID="LTemgcon" runat="server"></asp:Literal></td>
                           <td  align="left" >
                                   <span  class="field-label"> Fee Sponsor</span></td>
                      
                           <td  align="left" >
                                  <asp:Literal ID="txtFee_Spon" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td class="title-bg-lite" colspan="4" >
                    Student Passport/Visa Details</td>
                       </tr>
                       <tr>
                           <td  align="left" >
                             <span  class="field-label">  Passport No</span></td>
                       
                           <td  align="left" >
                               <asp:Literal ID="txtPNo" runat="server"></asp:Literal></td>
                           <td  align="left" >
                              <span  class="field-label"> Passport Issue Place</span></td>
                        
                           <td  align="left" >
                                   <asp:Literal ID="txtPIssPlace" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                              <span  class="field-label"> Passport Issue Date</span></td>
                    
                           <td  align="left" >
                                 <asp:Literal ID="txtPIssDate" runat="server"></asp:Literal></td>
                           <td  align="left" >
                             <span  class="field-label">  Passport Expiry Date</span></td>
                        
                           <td  align="left" >
                                
                                <asp:Literal ID="txtPExpDate" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                             <span  class="field-label">  Visa No</span></td>
                        
                           <td  align="left" >
                               <asp:Literal ID="txtVNo" runat="server"></asp:Literal></td>
                           <td  align="left" >
                              <span  class="field-label"> Visa Issue Place</span></td>
                        
                           <td  align="left" >
                                <asp:Literal ID="txtVIssPlace" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                              <span  class="field-label"> Visa Issue Date</span></td>
                     
                           <td  align="left" >
                              
                                <asp:Literal ID="txtVIssDate" runat="server"></asp:Literal></td>
                           <td  align="left" >
                              <span  class="field-label"> Visa Expiry Date</span></td>
                       
                           <td  align="left" >
                             
                                <asp:Literal ID="txtVExpDate" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left" >
                              <span  class="field-label">  Issuing Authority</span></td>
                      
                           <td class="matters" colspan="3">
                              
                                <asp:Literal ID="txtVIssAuth" runat="server"></asp:Literal></td>
                       </tr>
                       <tr>
                           <td  align="left"  >
                              <span  class="field-label">  Emirates ID</span></td>
                    
                           <td  align="left"  >
                              
                                <asp:Literal ID="ltEmiratesID" runat="server"></asp:Literal></td>
                           <td  align="left" >
                              <span  class="field-label"> Premises Id</span></td>
                          
                           <td  align="left"  >
                             
                                <asp:Literal ID="ltPremisesId" runat="server"></asp:Literal></td>
                       </tr>
                          <tr>
    <td colspan="4" class="title-bg-lite">
    Language Details </td></tr>
      
        <tr >
          <td  align="left" >
            <span  class="field-label">  First Language</span></td>
      
          <td  align="left" >
             <asp:Literal ID="ltFirstLang" runat="server"></asp:Literal></td>
          <td  align="left" >
             <span  class="field-label"> Other Languages</span></td>
   
          <td  align="left" >
              <asp:Literal ID="ltOthLang" runat="server"></asp:Literal></td>
      </tr>
      
       <tr >
          <td  align="left" >
             <span  class="field-label"> Proficiency in English</span></td>
     
          <td   colspan="3">
          
          <div style="width:100%">
          <div>
          <font style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #1B80B6;">
           Reading&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</font><font color="black"><asp:Literal ID="ltProEng_R" runat="server"></asp:Literal></font>&nbsp;&nbsp;&nbsp;
          <font style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #1B80B6;">
            Writing&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</font><font color="black"><asp:Literal ID="ltProEng_W" runat="server"></asp:Literal></font>&nbsp;&nbsp;&nbsp;
            <font style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #1B80B6;">
           Speaking&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;</font><font color="black"><asp:Literal ID="ltProEng_S" runat="server"></asp:Literal></font>&nbsp;&nbsp;&nbsp;
          </div>
          
         
          
          </div>
          
          
         </td>
      </tr>
                   </table>
               </td>
           </tr>
       </table>
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><asp:HiddenField
            ID="HF_stuid" runat="server" />
        <br />
    </div>
    </form>
</body>
</html>
