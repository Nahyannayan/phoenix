<%@ Page Language="VB" AutoEventWireup="true" CodeFile="STU_TAB_CURR.aspx.vb" Inherits="STU_TAB_FEE" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<script language="javascript" type="text/javascript">


    function OnTreeClick(evt) {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if (isChkBoxClick) {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            //check if nxt sibling is not null & is an element node
            if (nxtSibling && nxtSibling.nodeType == 1) {
                if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);

        }
    }


    function OnTreeClick1(evt) {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if (isChkBoxClick) {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            //check if nxt sibling is not null & is an element node
            if (nxtSibling && nxtSibling.nodeType == 1) {
                if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
    }


    function CheckUncheckChildren(childContainer, check) {
        var childChkBoxes = childContainer.getElementsByTagName("input");
        var childChkBoxCount = childChkBoxes.length;
        for (var i = 0; i < childChkBoxCount; i++) {
            childChkBoxes[i].checked = check;
        }
    }


    function CheckUncheckParents(srcChild, check) {
        var parentDiv = GetParentByTagName("div", srcChild);
        var parentNodeTable = parentDiv.previousSibling;
        if (parentNodeTable) {
            var checkUncheckSwitch;
            if (check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if (isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else
                    return; //do not need to check parent if any(one or more) child not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }
            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if (inpElemsInParentTable.length > 0) {
                var parentNodeChkBox = inpElemsInParentTable[0];
                parentNodeChkBox.checked = checkUncheckSwitch;
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
    }

    function AreAllSiblingsChecked(chkBox) {
        var parentDiv = GetParentByTagName("div", chkBox);
        var childCount = parentDiv.childNodes.length;
        for (var i = 0; i < childCount; i++) {
            if (parentDiv.childNodes[i].nodeType == 1) {
                //check if the child node is an element node
                if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                    var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                    //if any of sibling nodes are not checked, return false
                    if (!prevChkBox.checked) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //utility function to get the container of an element by tagname
    function GetParentByTagName(parentTagName, childElementObj) {
        var parent = childElementObj.parentNode;
        while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
            parent = parent.parentNode;
        }
        return parent;
    }

    function UnCheckAll() {
        var oTree = document.getElementById("<%=tvReport.ClientId %>");
    childChkBoxes = oTree.getElementsByTagName("input");
    var childChkBoxCount = childChkBoxes.length;
    for (var i = 0; i < childChkBoxCount; i++) {
        childChkBoxes[i].checked = false;
    }

    return true;
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <%--<link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />

    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
             <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">

        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" class="matters">
                        <asp:Label ID="lblSession" Class="Error" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" runat="server" Width="100%">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="title-bg-lite">SUBJECT LIST</td>
                    </tr>
                    <tr>
                        <td colspan="6" rowspan="4">
                            <table align="center" border="0" cellspacing="0" width="100%">


                                <tr>
                                    <td align="left" colspan="6" rowspan="4">
                                        <asp:GridView ID="gvSubjects" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                                            Width="100%" CssClass="table table-bordered table-row">

                                            <Columns>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        Subject
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        Group
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGroup" runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        Tutor
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="LEFT"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTutor" runat="server" Text='<%# Bind("TEACHERS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle  />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="title-bg-lite"> PROGRESS REPORTS</td>
                    </tr>
                    <tr>
                        <td  colspan="6" rowspan="4" >
                            <table align="center"  border="0" cellspacing="0" width="100%">


                                <tr>
                                    <td align="left"  colspan="6" rowspan="4">
                                        <asp:GridView ID="gvProgress" runat="server" AutoGenerateColumns="False" EmptyDataText="No Transaction details added yet."
                                            Width="100%" CssClass="table table-bordered table-row">
                                            <HeaderStyle />
                                            <EmptyDataRowStyle />
                                            <Columns>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                       Report Type
                                                               
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkPrinted"  OnClick="lnkPrinted_Click" runat="server" Text='<%# Bind("RPF_DESCR") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField Visible="FALSE">
                                                    <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRpfId" runat="server" Text='<%# Bind("RPF_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField Visible="FALSE">
                                                    <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRSMId" runat="server" Text='<%# Bind("RPF_RSM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="griditem"  />
                                            <SelectedRowStyle />
                                            <HeaderStyle />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table id="tblPer" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                    <td align="left" class="title-bg-lite"> STUDENT PERFORMANCE TRACKING</td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="6" rowspan="4" valign="top">
                            <table align="center"  border="0" cellspacing="0"width="100%">


                                <tr>
                                    <td align="center" colspan="6" valign="top" style="height: 212px">&nbsp;
                        <asp:TreeView ID="tvReport" runat="server" BorderStyle="Solid" BorderWidth="1px"
                            ExpandDepth="1" Height="206px" MaxDataBindDepth="3" NodeIndent="10" onclick="OnTreeClick(event);"
                            PopulateNodesFromClient="False" ShowCheckBoxes="All" Style="overflow: auto; text-align: left"
                            Width="293px">
                            <ParentNodeStyle Font-Bold="False" />
                            <HoverNodeStyle Font-Underline="True" />
                            <SelectedNodeStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px" Font-Underline="False"
                                HorizontalPadding="3px" VerticalPadding="1px" />
                            <NodeStyle  HorizontalPadding="5px"
                                NodeSpacing="1px" VerticalPadding="2px" />
                        </asp:TreeView>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="center" colspan="6">
                                        <asp:Button ID="btnGenerateReport" runat="server" CssClass="button"
                                            Text="Generate Report" ValidationGroup="groupM1" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


            </asp:Panel>

        </div>

        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <asp:HiddenField ID="HF_stuid" runat="server" />
        <asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfCBSESchool" runat="server" />
        <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
        </CR:CrystalReportSource>


    </form>
</body>
</html>

