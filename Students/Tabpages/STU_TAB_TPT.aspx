<%@ Page Language="VB" AutoEventWireup="false" CodeFile="STU_TAB_TPT.aspx.vb" Inherits="STU_TAB_TPT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <%--  <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table table-bordered">

                <tr>
                    <td valign="top">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">

                            <tr>
                                <td align="left" class="title-bg-lite" colspan="3">Transport Details</td>
                            </tr>

                            <tr>
                                <td ></td>
                             
                                <td  align="left">
                                    <b>PickUp</b></td>
                                <td align="left">
                                    <b>DropOff</b></td>

                            </tr>




                            <tr>
                                <td align="left"> <span  class="field-label">Location</span></td>
                   
                                <td >
                                    <asp:Literal ID="ltPickUp_Loc" runat="server"></asp:Literal></td>

                                <td >
                                    <asp:Literal ID="ltDropOff_Loc" runat="server"></asp:Literal></td>

                            </tr>


                            <tr>
                                <td  align="left"> <span  class="field-label">SubLocation</span></td>
                         
                                <td >
                                    <asp:Literal ID="ltPickUp_SubLoc" runat="server"></asp:Literal></td>

                                <td >
                                    <asp:Literal ID="ltDropOff_SubLoc" runat="server"></asp:Literal></td>

                            </tr>

                            <tr>
                                <td  align="left"> <span  class="field-label">Bus No</span></td>
     
                                <td >
                                    <asp:Literal ID="ltPickUp_BusNo" runat="server"></asp:Literal></td>

                                <td >
                                    <asp:Literal ID="ltDropOff_BusNo" runat="server"></asp:Literal></td>

                            </tr>


                            <tr>
                                <td  align="left"> <span  class="field-label">Trip</span></td>

                                <td >
                                    <asp:Literal ID="ltPickUp_Trip" runat="server"></asp:Literal></td>

                                <td >
                                    <asp:Literal ID="ltDropOff_Trip" runat="server"></asp:Literal></td>

                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:LinkButton ID="lnkinsertnew" runat="server">View Transaction History</asp:LinkButton></td>
                            </tr>
                        </table>
                        <table align="center" border="0" cellpadding="0" cellspacing="0"  width="100%">

                            <tr>
                                <td align="left" class="title-bg-lite">Extra School Provision</td>
                            </tr>

                            <tr>
                                <td align="left" >
                                    <asp:GridView ID="gvServices" runat="server" AllowPaging="True"
                                        AutoGenerateColumns="False" BorderStyle="None" CssClass="table table-bordered table-row"
                                        Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsrno" runat="server" Text='<%# Bind("srno") %>'></asp:Label>

                                                </ItemTemplate>

                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblService" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>

                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />

                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="From Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFromDT" runat="server" Text='<%# Bind("FROMDATE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Discontinue Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDiscontinueDate" runat="server" Text='<%# Bind("DiscontinueDate") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Availing">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAVAILING" runat="server" Text='<%# Bind("AVAILING") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle  />
                                        <RowStyle CssClass="griditem"  />
                                        <SelectedRowStyle  />
                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                        <EmptyDataRowStyle  />
                                        <EditRowStyle  />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>

        </div>
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <asp:HiddenField ID="HF_stuid" runat="server" />
    </form>
</body>
</html>
