<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StuSiblingDetails.aspx.vb" Inherits="Students_StuSiblingDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
<%--  <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
        <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

</head>
<body>
    <form id="form1" runat="server">
    <div><table border="0"  cellpadding="0" cellspacing="0" width="100%">
             <tr >
                <td align="left" class="title-bg-lite">
                     Sibling Details</td>
            </tr>
            <tr>
                <td  colspan="6" rowspan="4" valign="top">
        <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
             EmptyDataText="No Records Found" 
            CssClass="table table-bordered table-row" Width="100%">
            <RowStyle  CssClass="griditem" />
            <Columns>
                <asp:TemplateField HeaderText="Business Unit">
                    <ItemStyle  />
                    <ItemTemplate>
                        <asp:Label ID="lblBsuName" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Student No">
                    <ItemTemplate>
                        <asp:Label ID="lblStuNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                        <asp:Label ID="stuId" runat="server" Text='<%# bind("Stu_Id") %>' Visible="False"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Applicant Name">
                    <HeaderTemplate>
                      
                                    <asp:Label ID="lblApplName" runat="server"  Text="Student Name"></asp:Label>
                            
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkApplName1" runat="server" CommandName="Select" Text='<%# Bind("STU_FIRSTNAME") %>'
                            Visible="False"></asp:LinkButton>
                        <asp:Label ID="lnkApplName" runat="server" Text='<%# Bind("STU_FIRSTNAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Grade">
                    <HeaderTemplate>
                       
                                    <asp:Label ID="lblH12" runat="server"  Text="Grade"></asp:Label>
                    </HeaderTemplate>
                    <ItemStyle  />
                    <ItemTemplate>
                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRADE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Shift">
                    <HeaderTemplate>
                        Shift
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("SHIFT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Stream">
                    <HeaderTemplate>
                        Stream
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("STREAM") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle   />
             <AlternatingRowStyle CssClass="griditem_alternative" />
        </asp:GridView>
                    </td>
            </tr>
                </table>
        <asp:HiddenField ID="HF_stuid" runat="server" />
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label></div>
    </form>
</body>
</html>
