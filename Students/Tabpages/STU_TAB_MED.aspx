<%@ Page Language="VB" AutoEventWireup="false" CodeFile="STU_TAB_MED.aspx.vb" Inherits="STU_TAB_MED" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <%--  <link href="../../cssfiles/Textboxwatermark.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/Accordian.css" rel="stylesheet" type="text/css" />
    
    <link href="../../cssfiles/title.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../cssfiles/example.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%"  class="table table-bordered">
                <tr>
                    <td align="left" colspan="4" class="title-bg-lite">Health Details</td>
                </tr>

                <tr>
                    <td><span class="field-label">Health Card No / Medical Insurance No</span></td>

                    <td>
                        <asp:Literal ID="ltHth_No" runat="server"></asp:Literal></td>
                    <td><span class="field-label">Blood Group</span></td>

                    <td>
                        <asp:Literal ID="ltB_grp" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                    <td colspan="4" class="title-bg-lite">Health Restriction</td>
                </tr>
                <tr>
                    <td colspan="4">Does your child have any allergies? <span class="font-weight-bold">
                        <asp:Literal ID="ltAlg_Detail"
                            runat="server" EnableViewState="False"></asp:Literal></span>

                        <div id="divAlg" runat="server"></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">Does your child have any prescribed Special Medication? <span class="font-weight-bold">
                        <asp:Literal ID="ltSp_med" runat="server"></asp:Literal></span>
                        <div id="divSp_med" runat="server"></div>
                    </td>
                </tr>


                <tr>
                    <td colspan="4">Is there any Physical Education Restrictions for your child? <span class="font-weight-bold">
                        <asp:Literal ID="ltPhy_edu" runat="server"></asp:Literal></span>
                        <div id="divPhy_edu" runat="server"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">Any other information related to health issue of your child the school should be aware of? <span class="font-weight-bold">
                        <asp:Literal ID="ltHth_Info" runat="server"></asp:Literal></span>
                        <div id="divHth_Info" runat="server"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4" class="title-bg-lite">Applicant's disciplinary, social, physical or psychological detail</td>
                </tr>

                <tr>
                    <td colspan="4">Has the child received any sort of learning support or theraphy? <span class="font-weight-bold">
                        <asp:Literal ID="ltLS_Ther" runat="server"></asp:Literal></span>
                        <div id="divLS_Ther" runat="server"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">Does the child require any special education needs? <span class="font-weight-bold">
                        <asp:Literal ID="ltSEN" runat="server"></asp:Literal></span>
                        <div id="divSEN" runat="server"></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">Does the student require English support as Additional Language program (EAL) ? <span class="font-weight-bold">
                        <asp:Literal ID="ltEAL" runat="server"></asp:Literal></span>
                        <div id="divEAL" runat="server"></div>
                    </td>
                </tr>

                <tr>
                    <td class="matters" colspan="4">Has your child's behaviour been any cause for concern in previous schools ? <span class="font-weight-bold">
                        <asp:Literal ID="ltPrev_sch" runat="server"></asp:Literal></span>
                        <div id="divPrev_sch" runat="server"></div>
                    </td>

                </tr>

                <%--<tr>
    <td class="matters" colspan="6" style="font-weight:600; letter-spacing:0.3px; font-size:11pt; background-image: url(../../Images/bgblue.gif); color: white; font-family:Arial, Helvetica, sans-serif;">
  Gifted and talented</td></tr>
       
   
      <tr>
    <td class="matters"  colspan="6">
       Has your child ever been selected for specific enrichment activities? <font color="black"><asp:Literal ID="ltSEA" 
       runat="server"></asp:Literal></font>
       <div id="divSEA" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div></td>
    </tr>
     
      <tr>
    <td class="matters" colspan="6">
       Is your child musically proficient? <font color="black"><asp:Literal 
            ID="ltMus_Prof" runat="server"></asp:Literal>&nbsp;</font>
        <div id="divMus_Prof" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div>
            </td>
      
    </tr>
  
     <tr>
    <td class="matters" colspan="6" >
       Has your child represented a school or country in sport? <font color="black"> <asp:Literal 
            ID="ltSport" runat="server"></asp:Literal>&nbsp;</font>
            <div id="divSport" runat="server" style="color:Black;padding-top:2px;padding-bottom:2px;"></div>
            </td>
       </tr>
    
                --%>
            </table>
        </div>
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <asp:HiddenField ID="HF_stuid" runat="server" />
    </form>
</body>
</html>
