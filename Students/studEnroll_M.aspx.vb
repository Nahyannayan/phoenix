Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_studEnroll_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnectionString

                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100050") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                    If Session("BSU_BFlexiblePlan") = "False" Then
                        TR1.Cells(2).Visible = False
                        TR1.Cells(3).Visible = False
                        ' TR1.Cells(5).Visible = False
                    Else
                        TR1.Cells(2).Visible = True
                        TR1.Cells(3).Visible = True
                        ' TR1.Cells(5).Visible = True
                    End If
                    GridBind()
                    GetPayPlan()

                    'Dim cb As New CheckBox
                    'For Each gvr As GridViewRow In gvStudEnquiry.Rows
                    '    cb = gvr.FindControl("chkSelect")
                    '    ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
                    'Next
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    txtDoj.Attributes.Add("OnTextChanged", "MoveText();")
                    GetTransfer()
                End If
            Catch ex As Exception
                lblError.Text = "Request could not be processed"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try



        Else

            highlight_grid()

        End If

    End Sub
    Protected Sub lnkApplName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("action") = "select"
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnEnqid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnAcc_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnEnq_Date_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnAppl_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
            GetPayPlan()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
            GetPayPlan()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub gvStudEnquiry_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvStudEnquiry.PageIndexChanged
        'Dim hash As New Hashtable
        'If Not Session("hashCheck") Is Nothing Then
        '    hash = Session("hashCheck")
        'End If


        'Dim chk As CheckBox
        'Dim ENQID As String = String.Empty
        'For Each rowItem As GridViewRow In gvStudEnquiry.Rows
        '    chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
        '    ENQID = "<ID><EQS_ID>" + DirectCast(rowItem.FindControl("lblEqsId"), Label).Text + "</EQS_ID><ENQ_ID>" + DirectCast(rowItem.FindControl("lblEnqId"), Label).Text + "</ENQ_ID></ID>"
        '    If hash.ContainsValue(ENQID) = True Then
        '        chk.Checked = True
        '    Else
        '        chk.Checked = False
        '    End If
        'Next

    End Sub
    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudEnquiry.PageIndexChanging
        Try
            gvStudEnquiry.PageIndex = e.NewPageIndex
            GridBind()
            GetPayPlan()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
        'Try
        '    gvStudEnquiry.PageIndex = e.NewPageIndex
        '    Dim hash As New Hashtable
        '    Dim lblregdate As Label
        '    If Not Session("hashCheck") Is Nothing Then
        '        hash = Session("hashCheck")
        '    End If


        '    Dim chk As CheckBox
        '    Dim ENQID As String = String.Empty
        '    For Each rowItem As GridViewRow In gvStudEnquiry.Rows
        '        ' chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
        '        chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
        '        lblRegDate = rowItem.FindControl("lblRegDate")
        '        'DirectCast(row.FindControl("lblEnqId"), Label).Text)
        '        ENQID = "<ID><EQS_ID>" + DirectCast(rowItem.FindControl("lblEqsId"), Label).Text + "</EQS_ID><ENQ_ID>" + DirectCast(rowItem.FindControl("lblEnqId"), Label).Text + "</ENQ_ID></ID>"
        '        If chk.Checked = True Then
        '            If Date.Parse(lblregdate.Text) <= Date.Parse(txtDoj.Value) Then
        '                If hash.Contains(ENQID) = False Then
        '                    hash.Add(ENQID, "<ID><EQS_ID>" + DirectCast(rowItem.FindControl("lblEqsId"), Label).Text + "</EQS_ID><ENQ_ID>" + DirectCast(rowItem.FindControl("lblEnqId"), Label).Text + "</ENQ_ID></ID>")
        '                End If
        '            End If
        '        Else
        '            If hash.Contains(ENQID) = True Then
        '                hash.Remove(ENQID)
        '            End If
        '        End If
        '    Next

        '    Session("hashCheck") = hash
        '    GridBind()

        'Catch ex As Exception
        '    lblError.Text = "Request could not be processed"
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'End Try
    End Sub
    Protected Sub gvStudEnquiry_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudEnquiry.RowCommand
    End Sub
    Protected Sub btnEnroll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnroll.Click
        Try
            If Page.IsValid Then


                btnEnroll.Enabled = False
                lblError.Text = ""
                If CheckDOJ() = False Then
                    btnEnroll.Enabled = True
                    Exit Sub
                End If

                If ddlTranType.SelectedItem.Value = "" Then
                    lblError.Text = "Please Select Transfer Type"
                    btnEnroll.Enabled = True
                    Exit Sub
                End If

                If studClass.checkFeeClosingDate(Session("sbsuid"), ddlAcademicYear.SelectedValue, Date.Parse(txtDoj.Text)) = False Then
                    lblError.Text = "Date of join has to be a date higher than the fee closing date"
                    Exit Sub
                End If




                'Dim ChkState As Boolean = False

                'Dim chk As CheckBox
                'Dim ENQID As String = String.Empty
                'Dim hash As New Hashtable
                'If Not Session("hashCheck") Is Nothing Then
                '    hash = Session("hashCheck")
                'End If

                'Dim lblregdate As Label

                'For Each rowItem As GridViewRow In gvStudEnquiry.Rows
                '    ' chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
                '    chk = DirectCast(rowItem.FindControl("chkSelect"), CheckBox)
                '    lblRegDate = rowItem.FindControl("lblRegDate")
                '    'DirectCast(row.FindControl("lblEnqId"), Label).Text)
                '    ENQID = "<ID><EQS_ID>" + DirectCast(rowItem.FindControl("lblEqsId"), Label).Text + "</EQS_ID><ENQ_ID>" + DirectCast(rowItem.FindControl("lblEnqId"), Label).Text + "</ENQ_ID></ID>"
                '    If chk.Checked = True Then
                '        If Date.Parse(lblregdate.Text) <= Date.Parse(txtDoj.Value) Then
                '            If hash.Contains(ENQID) = False Then
                '                hash.Add(ENQID, "<ID><EQS_ID>" + DirectCast(rowItem.FindControl("lblEqsId"), Label).Text + "</EQS_ID><ENQ_ID>" + DirectCast(rowItem.FindControl("lblEnqId"), Label).Text + "</ENQ_ID></ID>")
                '            End If
                '        End If
                '    Else
                '        If hash.Contains(ENQID) = True Then
                '            hash.Remove(ENQID)
                '        End If
                '    End If
                'Next

                'Session("hashCheck") = hash
                'If Not Session("hashCheck") Is Nothing Then

                If SaveData() = True Then
                    If lblError.Text = "success" Then
                        ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
                        ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
                        Dim url As String = String.Format("~\Students\studPrintEnrollmentSlip2.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
                        Response.Redirect(url)
                    End If
                End If

                '  GridBind()
                '  btnEnroll.Enabled = True



                ''Else
                '  lblError.Text = "No Enquiry selected for print"
                ' End If

            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub



#Region "Private Methods"
    Function CheckDOJ() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND '" + txtDoj.Text + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The date of join should be within the academic year"
            Return False
        Else
            Return True
        End If

    End Function

    Function GetEQSIDS() As String
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblEqsId As Label
        Dim lblEnqId As Label
        Dim lblRegDate As Label
        Dim lblEnqNo As Label
        Dim str As String = ""
        Dim str1 As String = ""
        Dim enqIds As String = ""
        With gvStudEnquiry
            For i = 0 To .Rows.Count - 1
                chkSelect = .Rows(i).FindControl("chkSelect")
                If chkSelect.Checked = True Then
                    lblEqsId = .Rows(i).FindControl("lblEqsid")
                    lblEnqId = .Rows(i).FindControl("lblEnqId")
                    lblRegDate = .Rows(i).FindControl("lblRegDate")
                    If Date.Parse(lblRegDate.Text) <= Date.Parse(txtDoj.Text) Then
                        str += "<ID><EQS_ID>" + lblEqsId.Text + "</EQS_ID><ENQ_ID>" + lblEnqId.Text + "</ENQ_ID></ID>"
                        If enqIds <> "" Then
                            enqIds += ","
                        End If
                        enqIds += lblEnqId.Text
                    Else

                        ' if the date of join is less than the registration date then it has to be validated
                        If str1 <> "" Then
                            str1 += ","
                        End If
                        lblEnqNo = .Rows(i).FindControl("lblEnqNo")
                        str1 += lblEnqNo.Text
                    End If
                End If
            Next
        End With

        Session("ENQIDS") = enqIds
        If str <> "" Then
            Return "<IDS>" + str + "</IDS>" + "|" + str1
        Else
            Return "|" + str1
        End If
    End Function

    Function AuditData() As Boolean
        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblEqsId As Label
        Dim lblEnqId As Label
        Dim str As String = ""

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Try
                transaction = conn.BeginTransaction("SampleTransaction")
                With gvStudEnquiry
                    For i = 0 To .Rows.Count - 1
                        chkSelect = .Rows(i).FindControl("chkSelect")
                        If chkSelect.Checked = True Then
                            lblEqsId = .Rows(i).FindControl("lblEqsid")
                            UtilityObj.InsertAuditdetails(transaction, "edit", "ENQUIRY_SCHOOLPRIO_S", "EQS_ID", "EQS_EQM_ENQID", "EQS_ID=" + lblEqsId.Text)
                        End If
                    Next
                End With
                transaction.Commit()
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function

    Function SaveData() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim ids As String()
        ids = GetEQSIDS.Split("|")
        Dim XMlString As String
        Dim applNos As String

        XMlString = ids(0)
        applNos = ids(1)

        Dim stuIds As String = ""
        Dim eqsIds As String = ""

        If XMlString = "" And applNos = "" Then
            lblError.Text = "No records selected"
            Return False
        ElseIf XMlString <> "" Then
            If AuditData() = True Then
                str_query = "exec studENROLL '" + XMlString + "','" + Format(Date.Parse(txtDoj.Text), "yyyy-MM-dd") + "','" + Format(Date.Parse(txtMDoj.Text), "yyyy-MM-dd") + "','" + ddlTranType.SelectedValue + "','" + Session("sbsuid") + "','" + Session("sUsr_name") + "','" + ddlPayPlan.SelectedValue + "','" + ddlJoinResult.SelectedItem.Value + "'"
                ' SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
                While reader.Read
                    Try
                        ' stuIds = reader.GetString(0)
                        ' eqsIds = reader.GetString(1)
                    Catch ex As Exception
                    End Try
                End While
                reader.Close()


            Else
                lblError.Text = "Record coud not be saved"
                Return False
            End If
        End If

        If applNos <> "" Then
            Session("enrError") = "The following records  " + applNos + " were not updated,Since the date of join has to be higher or equal that the registration date "
            lblError.Text = "The following records  " + applNos + " were not updated,Since the date of join has to be higher or equal that the registration date "
        Else
            lblError.Text = "success"
        End If
        Return True
        'stuIds = IDS(0)

        'eqsIds = IDS(1)

        '    Dim flagAudit As Integer
        '    If stuIds <> "" Then
        '        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "STU_ID(" + stuIds + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
        '    End If
        '    If eqsIds <> "" Then
        '        flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "EQS_ID(" + stuIds + ")", "failed to insert", Page.User.Identity.Name.ToString, Me.Page)
        '    End If

    End Function

    Sub GetTransfer()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT Top 1 '' as TFR_CODE,'' as TFR_DESCR From TFRTYPE_M WHERE 1=1 UNION ALL SELECT TFR_CODE, TFR_DESCR FROM TFRTYPE_M order by TFR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlTranType.DataSource = ds
        ddlTranType.DataValueField = "TFR_CODE"
        ddlTranType.DataTextField = "TFR_DESCR"
        ddlTranType.DataBind()
    End Sub

    Protected Function GetNavigateUrl(ByVal eqsid As String) As String
        Dim str As String = "javascript:var popup = window.showModalDialog('studJoinDocuments.aspx?eqsid=" + eqsid + "', '','dialogHeight:400px;dialogWidth:705px;scroll:yes;resizable:no;');"
        Return str
    End Function
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub GetPayPlan()
        Dim li As New ListItem
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT     FEES.SCHEDULE_M.SCH_ID, FEES.SCHEDULE_M.SCH_DESCR FROM         FEES.SCHEDULE_M INNER JOIN                       FEES.FEESETUP_M ON FEES.SCHEDULE_M.SCH_ID = FEES.FEESETUP_M.FSM_Collection_SCH_ID                       where FsM_FEE_ID=5 and FsM_ACD_ID= '" & ddlAcademicYear.SelectedItem.Value & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlPayPlan.DataSource = ds
        ddlPayPlan.DataTextField = "SCH_DESCR"
        ddlPayPlan.DataValueField = "SCH_ID"
        ddlPayPlan.DataBind()


        '---- Get the default payment plan
        Using PayPlanReader As SqlDataReader = GetDefaultPayPlan(ddlAcademicYear.SelectedItem.Value)
            While PayPlanReader.Read
                If Convert.ToString(PayPlanReader("DefaultPlan")) = 1 Then
                    li.Text = Convert.ToString(PayPlanReader("SCH_DESCR"))
                    li.Value = Convert.ToString(PayPlanReader("SCH_ID"))
                    ddlPayPlan.Items(ddlPayPlan.Items.IndexOf(li)).Selected = True
                End If
            End While
        End Using

    End Sub
    Function GetDefaultPayPlan(ByVal ACD_ID As String) As SqlDataReader
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "OASIS_FEES.FEES.PaymentPlan", pParms)
        Return reader
    End Function


    Private Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            'if acd_dlinktpt is set to true then no need to check for enquiry transport allocation


            Dim str_query As String = "SELECT  EQS_ID,EQS_APPLNO,EQM_ENQID,EQM_ENQDATE, GRM_DISPLAY, SHF_DESCR,STM_DESCR, " _
                                    & "APPL_NAME=isnull(EQM_APPLFIRSTNAME,'') + ' ' + ISNULL(EQM_APPLMIDNAME, '') + ' ' + ISNULL(EQM_APPLLASTNAME, '') ," _
                                    & " ISNULL(EQS_REGNDATE,'01/Jan/1900') as EQS_REGNDATE,EQS_ACCNO " _
                                    & "FROM   ENQUIRY_M AS A WITH (NOLOCK) INNER JOIN " _
                                    & "ENQUIRY_SCHOOLPRIO_S AS B WITH(NOLOCK) ON A.EQM_ENQID = B.EQS_EQM_ENQID INNER JOIN " _
                                    & "GRADE_BSU_M AS C WITH(NOLOCK) ON B.EQS_GRM_ID = C.GRM_ID INNER JOIN " _
                                    & "SHIFTS_M AS D WITH(NOLOCK) ON B.EQS_SHF_ID = D.SHF_ID INNER JOIN " _
                                    & " STREAM_M AS E WITH(NOLOCK) ON B.EQS_STM_ID = E.STM_ID " _
                                    & " INNER JOIN dbo.vw_OSO_STUDAPPLSTAGES F ON B.EQS_ID=F.PRA_EQS_ID " _
                                    & " INNER JOIN ACADEMICYEAR_D AS G WITH(NOLOCK) ON B.EQS_ACD_ID=G.ACD_ID" _
                                    & " WHERE B.EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND EQS_BSU_ID='" + Session("sbsuid") + "' " _
                                    & " AND EQS_STATUS<>'ENR' AND" _
                                    & " F.STG1COMP = 1 And F.STG2COMP = 1 And F.STG3COMP = 1" _
                                    & " AND F.STG4COMP=1 AND F.STG5COMP=1 AND F.STG6COMP=1  " _
                                    & " AND (EQS_bTPTREQD ='FALSE' OR EQS_TPTSTATUS='ACCEPT' OR " _
                                    & " EQS_TPTSTATUS='WAIT' OR EQS_TPTSTATUS='REJECT' OR ACD_bDLINKTPT='TRUE')" _
                                    & " AND EQS_STATUS<>'DEL' AND EQS_STATUS<>'CO' AND EQS_STATUS<>'CR'"








            Dim strFilter As String = ""

            Dim strSidsearch As String()
            Dim strSearch As String

            Dim enqSearch As String = ""
            Dim accsearch As String = ""
            Dim nameSearch As String = ""
            Dim dateSearch As String = ""

            Dim ddlgvGrade As New DropDownList
            Dim ddlgvShift As New DropDownList
            Dim ddlgvStream As New DropDownList

            Dim selectedGrade As String = ""
            Dim selectedShift As String = ""
            Dim selectedStream As String = ""
            Dim txtSearch As New TextBox
            If gvStudEnquiry.Rows.Count > 0 Then



                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = GetSearchString("EQS_APPLNO", txtSearch.Text, strSearch)
                enqSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtAccSearch")
                strSidsearch = h_Selected_menu_6.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("EQS_ACCNO", txtSearch.Text, strSearch)
                accsearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("convert(varchar,eqm_enqdate,106)", txtSearch.Text.Replace("/", " "), strSearch)
                dateSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("grm_display", txtSearch.Text, strSearch)
                selectedGrade = txtSearch.Text

                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                If ddlgvShift.Text <> "ALL" Then
                    strFilter += " and shf_descr='" + ddlgvShift.Text + "'"
                    selectedShift = ddlgvShift.Text
                End If


                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")
                If ddlgvStream.Text <> "ALL" Then
                    strFilter += " and stm_descr='" + ddlgvStream.Text + "'"
                    selectedStream = ddlgvStream.Text
                End If


                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
                strSidsearch = h_Selected_menu_3.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("isnull(eqm_applfirstname,' ')+' '+isnull(eqm_applmidname,' ')+' '+isnull(eqm_appllastname,' ')", txtSearch.Text, strSearch)
                nameSearch = txtSearch.Text

                If strFilter.Trim <> "" Then
                    str_query = str_query + strFilter
                End If


            End If
            If Session("sbsuid") = "125018" Then
                str_query += " ORDER BY GRM_DISPLAY,EQM_APPLFIRSTNAME"
            Else
                str_query += " ORDER BY EQM_ENQDATE DESC "
            End If

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudEnquiry.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudEnquiry.DataBind()
                Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
                gvStudEnquiry.Rows(0).Cells.Clear()
                gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
                gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudEnquiry.DataBind()
            End If


            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
            txtSearch.Text = enqSearch


            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtAccSearch")
            txtSearch.Text = accsearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
            txtSearch.Text = dateSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
            txtSearch.Text = nameSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = selectedGrade

            Dim dt As DataTable = ds.Tables(0)

            If gvStudEnquiry.Rows.Count > 0 Then



                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")

                Dim dr As DataRow

                ddlgvShift.Items.Clear()
                ddlgvShift.Items.Add("ALL")


                ddlgvStream.Items.Clear()
                ddlgvStream.Items.Add("ALL")

                For Each dr In dt.Rows

                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If

                    With dr

                        ''check for duplicate values and add to dropdownlist 
                        If ddlgvShift.Items.FindByText(.Item(5)) Is Nothing Then
                            ddlgvShift.Items.Add(.Item(5))
                        End If
                        If ddlgvStream.Items.FindByText(.Item(6)) Is Nothing Then
                            ddlgvStream.Items.Add(.Item(6))
                        End If
                    End With
                Next

                If selectedGrade <> "" Then
                    ddlgvGrade.Text = selectedGrade
                End If


                If selectedShift <> "" Then
                    ddlgvShift.Text = selectedShift
                End If

                If selectedStream <> "" Then
                    ddlgvStream.Text = selectedStream
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getmenuid(Optional ByVal p_imgsrc As String = "", Optional ByVal imagename As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudEnquiry.HeaderRow.FindControl(imagename)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub highlight_grid()
        For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1
            Dim row As GridViewRow = gvStudEnquiry.Rows(i)
            Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
            If isSelect Then
                row.BackColor = Drawing.Color.FromName("#f6deb2")
            Else
                row.BackColor = Drawing.Color.Transparent
            End If
        Next
    End Sub

#End Region


    'Protected Sub txtDoj_TextChanged(sender As Object, e As EventArgs)
    '    ScriptManager.RegisterStartupScript(Me.Page, Page.GetType(), "text", "MoveText();return false;", True)
    '    'Response.Write("<script language='javascript'>")
    '    'Response.Write("MoveText();return false;")
    '    'Response.Write("</script>")
    'End Sub


End Class
