﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="EnquiryDashboard.aspx.vb" Inherits="Students_NewASPX_EnquiryDashboard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpageNew" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <link href="<%= ResolveUrl("~/cssfiles/EnquiryStyle.css")%>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/cssfiles/build.css")%>" rel="stylesheet" />
    <script type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudEnquiry.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }



        function change_chk_state(chkThis) {

            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click(); //fire the click event of the child element
                    }
                    if (chkstate = true) {

                    }
                }
            }
        }







    </script>
    <style>
        tbody > tr > td > table > tbody > tr:nth-child(1) > td > span {
            background-color: #016b05;
            color: #ffffff;
            font-weight: bold;
            padding: 5px;
        }
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <div class="row">


                <div class="col-lg-6 col-md-6 col-sm-12 ">
                    <i class="fa fa-book mr-3"></i>
                    Enquiry Managment
      
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  text-right">
                    <a class="btn btn-primary btn-sm"
                        href="<%= ResolveUrl("~/Students/studEnqForm_Reg_New.aspx?MainMnu_code=se4IhNkPp7M=&datamode=Zo4HhpVNpXc=")%>">Enquiry in School</a>

                

                </div>

            </div>
        </div>
        <section style="display: inline-block; width: 100%; margin-bottom: 0px;">
            <div class="header" onclick="showHide();" style="padding: 5px 15px;background: #f5f5f5;">

             <%--   <span id="ColExpCss" class="colClass"></span>--%>
                <i class="fa fa-filter" aria-hidden="true"></i>
                <span style="font-size: 14px; padding-left: 6px; color: #333333;">Filter By</span>
            </div>
            <div class="panel-cover FindJob" runat="server" id="FindJob" style="display: none;">
                <div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Applicant Name</label>

                                <asp:TextBox runat="server" ID="txt_ApplicantName" class="form-control" />

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Enquiry Number </label>
                                <asp:TextBox runat="server" ID="txt_Enquiry_Number" class="form-control" />

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Acount Number </label>
                                <asp:TextBox runat="server" ID="txt_AcountNumber" class="form-control" />

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Academic Year </label>

                                <asp:DropDownList ID="ddlAcademicYear" CssClass="form-control" runat="server">
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Contact Me </label>
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContactMe">
                                    <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                    <asp:ListItem Value="YES">YES</asp:ListItem>
                                    <asp:ListItem Value="NO">NO</asp:ListItem>
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Status</label>
                                <asp:DropDownList ID="ddlEnqStatus" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="0">ALL</asp:ListItem>
                                    <asp:ListItem Value="2">NEW ENQUIRY</asp:ListItem>
                                    <asp:ListItem Value="2">ENQUIRY</asp:ListItem>
                                    <asp:ListItem Value="3">REGISTER</asp:ListItem>
                                    <asp:ListItem Value="4">SCREENING</asp:ListItem>
                                    <asp:ListItem Value="5">APPROVAL</asp:ListItem>
                                    <asp:ListItem Value="6">OFFER LETTER</asp:ListItem>
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Follow Up Status</label>
                                <asp:DropDownList ID="ddlFollowupStatus" CssClass="form-control" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Enquiry Condition </label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddl_EnquiryCondition">
                                    <asp:ListItem Text="ALL" Value="ALL" />
                                    <asp:ListItem Text="Open" Value="Open" />
                                    <asp:ListItem Text="Cancelled" Value="Cancelled" />
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Paid </label>
                                <asp:DropDownList ID="ddlPaid" CssClass="form-control" runat="server" AutoPostBack="true">
                                    <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                    <asp:ListItem Value="YES">YES</asp:ListItem>
                                    <asp:ListItem Value="NO">NO</asp:ListItem>
                                    <asp:ListItem Value="AtSchool">At School</asp:ListItem>
                                    <asp:ListItem Value="Online">Online</asp:ListItem>
                                    <asp:ListItem Value="NotPaidReg">Not Paid-Registered</asp:ListItem>
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Prev School GEMS  </label>
                                <asp:DropDownList ID="ddlPrevSchool_BSU" CssClass="form-control" runat="server" AutoPostBack="true">
                                </asp:DropDownList>

                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">SEN</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddl_sen">
                                    <asp:ListItem Text="ALL" />
                                    <asp:ListItem Text="YES" Value="YES" />
                                    <asp:ListItem Text="NO" Value="NO" />
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">GEMS Staff</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddl_GEMS_Staff">
                                    <asp:ListItem Text="ALL" />
                                    <asp:ListItem Text="YES" />
                                    <asp:ListItem Text="NO" />
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Ex Student</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddl_Ex_Student">
                                    <asp:ListItem Text="ALL" />
                                    <asp:ListItem Text="YES" />
                                    <asp:ListItem Text="NO" />
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">Sibling</label>
                                <asp:DropDownList runat="server" class="form-control" ID="ddl_Siblingt">
                                    <asp:ListItem Text="ALL" />
                                    <asp:ListItem Text="YES" />
                                    <asp:ListItem Text="NO" />
                                </asp:DropDownList>

                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="Ftitle">--- </label>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" class=" btn btn-primary btn-sm" OnClick="btnSearch_Click" />
                                <asp:Button ID="btnClear" runat="server" Text="Reset" class="btn btn-primary btn-sm" />


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <div class="card-body">


            <div class="row">
                <div class="col-md-12">
                    <asp:Label ID="lblError" runat="server"></asp:Label>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <asp:Label ID="lblSTAT" runat="server" CssClass="field-value"></asp:Label>
                </div>
            </div>
            <div class="table-responsive">

                <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False" Style="font-size: 16px!important"
                    CssClass="table table-striped table-bordered  m-0 table-row" EmptyDataText="No Records Found" HeaderStyle-Height="30"
                    PageSize="20" DataKeyNames="Eqs_id" OnPageIndexChanged="gvStudEnquiry_PageIndexChanged">
                    <RowStyle CssClass="griditem" />
                    <Columns>
                        <asp:TemplateField HeaderText="Available" >
                            <EditItemTemplate>
                            </EditItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this);" runat="server"
                                    ToolTip="Click here to select/deselect all rows" Text=""></asp:CheckBox>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                            <HeaderStyle Wrap="False" Width="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="RowNumber" HeaderText="SI.NO" />
                        <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("Eqs_id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="APL_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAPL_ID" runat="server" Text='<%# Bind("APL_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EnqApplNo" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblEnqId" runat="server" Text='<%# Bind("eqm_enqid") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Enq No.">
                            <HeaderTemplate>
                                <asp:Label ID="lblEnqHeader" runat="server" Text="Enq No" CssClass="gridheader_text"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEnqApplNo" runat="server" Text='<%# Bind("Eqs_ApplNo") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Account No.">
                            <HeaderTemplate>
                                <asp:Label ID="lblAccHeader" runat="server" Text="Acc No." CssClass="gridheader_text"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAccNo" runat="server" Text='<%# Bind("Eqs_AccNo") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Enq Date">
                            <HeaderTemplate>
                                <asp:Label ID="lblEnqDate" runat="server" CssClass="gridheader_text" Text="Enq.Date"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("eqm_enqdate", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Grade">
                            <HeaderTemplate>
                                <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                            </HeaderTemplate>
                            <HeaderStyle />
                            <ItemTemplate>
                                <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stream">
                            <HeaderTemplate>
                                Stream
                            </HeaderTemplate>
                            <HeaderStyle Width="5%" />
                            <ItemTemplate>
                                <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Applicant Name">
                            <HeaderTemplate>
                                <asp:Label ID="lblApplName" runat="server" Text="Applicant Name" CssClass="gridheader_text"></asp:Label>

                            </HeaderTemplate>
                            <ItemTemplate>
<%--                                <asp:LinkButton ID="lnkApplName"  runat="server" 
                                    Text='<%# Bind("appl_name") %>' CommandName="Select" ToolTip="Clicking on this link will show the list of documents which has to be submitted for  each stage"></asp:LinkButton>--%>
                            <asp:Label ID="lnkApplNamelbl" runat="server" Text='<%# Bind("appl_name") %>'></asp:Label>
                                </ItemTemplate>
                            <HeaderStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Gender" HeaderStyle-Wrap="true">
                            <HeaderTemplate>
                                <asp:Label ID="lblH122" runat="server" CssClass="gridheader_text" Text="Gender"></asp:Label>
                            </HeaderTemplate>
                            <HeaderStyle />
                            <ItemTemplate>
                                <asp:Label ID="lblGender" runat="server" Text='<%# Bind("Gender") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" HeaderStyle-Wrap="true">
                            <HeaderTemplate>
                                <asp:Label ID="lblStatusHead" runat="server" CssClass="gridheader_text" Text="Status"></asp:Label>
                            </HeaderTemplate>
                            <HeaderStyle />
                            <ItemTemplate>

                                <asp:Label ID="lblStatus" runat="server" Text='<%# If(IsDBNull( Eval("EQS_CURRSTATUS")) = False, GetSomeStringValue(Eval("EQS_CURRSTATUS")), "") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Age" Visible="True">
                            <ItemTemplate>
                                <asp:Label ID="lblAPPL_DATE" runat="server" Text='<%# bind("Appl_Age") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle />
                            <ItemStyle />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="STG3COMP" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblSTG3COMP" runat="server" Text='<%# Bind("STG3COMP") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Shift" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField CommandName="Details" Text="Manage" HeaderText="Manage">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:ButtonField>
                        <asp:ButtonField CommandName="EditRow" Text="Edit" HeaderText="Edit">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                        </asp:ButtonField>

                    </Columns>
                </asp:GridView>
                <div class="row" id="btnHolder" runat="server" visible="false">
                    <div class="col-md-12 text-left mt-2 mb-2">
                        <asp:Button ID="btnPrintOffer" runat="server" CssClass="btn btn-primary btn-sm" OnClick="btnPrintOffer_Click"
                            Text="Print Offer Letter" Visible="false" />
                        <asp:LinkButton ID="btnExport" runat="server" CssClass="btn btn-primary btn-sm" OnClick="btnExport_Click"
                            Text="Export to Excel" />
                        <asp:LinkButton ID="btnPrint" runat="server" CssClass="btn btn-primary btn-sm" OnClick="btnPrint_Click" Visible="false"
                            Text="Print Screeening Test" />
                        <asp:Button ID="btnReject" runat="server" Text="Delete" CssClass="button" OnClientClick="return confirm_cancel();" Visible="false" />
                    </div>
                </div>
            </div>
        </div>



    </div>
    <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_12" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
    <input id="h_Selected_menu_13" runat="server" type="hidden" value="=" />
    <asp:HiddenField ID="hiddenENQIDs" runat="server"></asp:HiddenField>

    <script>
        function showHide() {
            $header = $(".header");
            $content = $(".FindJob");
            //$ColExpId = $("#ColExpCss");
            $content.slideToggle(500, function () {
                if ($content.is(":visible") == true) {
                    //$ColExpId.removeClass("colClass").addClass("ExpClass");
                }
                else {
                    //$ColExpId.removeClass("ExpClass").addClass("colClass");
                }
            });
        }
        //showHide();
    </script>
</asp:Content>


