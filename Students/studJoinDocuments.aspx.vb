Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studJoinDocuments
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64

    'Protected Sub FeeSponsor_Update(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If e.ToString = "1" Then
    '        mpxFeeSponsor.Show()
    '    Else
    '        mpxFeeSponsor.Hi de()
    '    End If
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        ''str_query = "SELECT BM_ID,BM_ENTRY_DATE,EMP_FNAME,EMP_LNAME " & _
        ''                "FROM BM.BM_MASTER A INNER JOIN " & _
        ''                "EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID ORDER BY BM_ENTRY_DATE DESC"

        'Dim str_query = "SELECT DISTINCT A.BM_ID, A.BM_INCIDENT_DATE,A.BM_ENTRY_DATE,D.EMP_FNAME,D.EMP_LNAME FROM BM.BM_MASTER A " & _
        '         "INNER JOIN BM.BM_ACTION_MASTER B ON A.BM_ID= B.BM_ID " & _
        '         "INNER JOIN STUDENT_M C ON C.STU_ID= B.BM_STU_ID " & _
        '         "INNER JOIN EMPLOYEE_M D ON D.EMP_ID= A.BM_REPORTING_STAFF_ID and C.stu_id='20131750'"

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'gvBadDetails.DataSource = ds
        'gvBadDetails.DataBind()
        Try
            If Page.IsPostBack = False Then
                'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                ''HF_stuid.Value = Request.QueryString("eqsid")
                ''Session("DB_Stu_ID") = HF_stuid.Value
                ''ViewState("stu_id") = Session("DB_Stu_ID").ToString
                ''HF_stuid.Value = ViewState("stu_id")

                getEQM_ENQID(Request.QueryString("eqsid"))
                BindGrid() ' Negative
                BindGrid_POS() ' Psoitive
                pnl_UploadDoc.Visible = False
            End If
        Catch ex As Exception

        End Try


    End Sub

    Private Sub getEQM_ENQID(ByVal eqsid As String)
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@eqs_id", eqsid)
        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "SV.GETEQM_ENQID", param)
            While datareader.Read
                HF_stuid.Value = Convert.ToString(datareader("EQM_ENQID"))
            End While
        End Using

    End Sub
    Protected Function GetNavigateUrl1(ByVal pId As String) As String
        Return String.Format("javascript:var popup = window.showModalDialog('https://school.gemsoasis.com/oasisfiles/hr_files/enquiry/1.gif', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ")
    End Function
    Private Sub BindGrid()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@EQM_ENQID", HF_stuid.Value)
            PARAM(1) = New SqlParameter("@INFO_TYPE", "PEND_DOC")

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SV_ENQ.GETENQ_DOC_LIST", PARAM)

            GridView1.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                GridView1.DataBind()
                Dim columnCount As Integer = GridView1.Rows(0).Cells.Count
                GridView1.Rows(0).Cells.Clear()
                GridView1.Rows(0).Cells.Add(New TableCell)
                GridView1.Rows(0).Cells(0).ColumnSpan = columnCount
                GridView1.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridView1.Rows(0).Cells(0).Text = "No Details...."

            Else
                GridView1.DataBind()

            End If

        Catch ex As Exception

        End Try


    End Sub


    Private Sub BindGrid_POS()
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
          

            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@EQM_ENQID", HF_stuid.Value)
            PARAM(1) = New SqlParameter("@INFO_TYPE", "APP_DOC")

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SV_ENQ.GETENQ_DOC_LIST", PARAM)
            gvStudGrade.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade.DataBind()
                Dim columnCount As Integer = gvStudGrade.Rows(0).Cells.Count
                gvStudGrade.Rows(0).Cells.Clear()
                gvStudGrade.Rows(0).Cells.Add(New TableCell)
                gvStudGrade.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade.Rows(0).Cells(0).Text = "No Details...."

            Else
                gvStudGrade.DataBind()

            End If

        Catch ex As Exception

        End Try


    End Sub

    Protected Sub lnkchangeDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hf_doc_id As HiddenField = sender.parent.FindControl("hf_doc_id")
        Dim hf_DOC_DESCR As HiddenField = sender.parent.FindControl("hf_DOC_DESCR")
        lblDoc.Text = hf_DOC_DESCR.Value
        ViewState("doc_id") = hf_doc_id.Value
        pnl_UploadDoc.Visible = True
        btnuploadDoc.Visible = False
        btnuploadChangeDoc.Visible = True
    End Sub

    Protected Sub imgDocCollected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim HF_SDU_ID As HiddenField = sender.parent.FindControl("HF_SDU_ID")
            Dim hf_doc_id As HiddenField = sender.parent.FindControl("hf_doc_id")
            Dim HF_SDU_BCOLLECTED As HiddenField = sender.parent.FindControl("HF_SDU_BCOLLECTED")

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim pParms(7) As SqlClient.SqlParameter


            pParms(0) = New SqlClient.SqlParameter("@EDM_ID", HF_SDU_ID.Value)
            pParms(1) = New SqlClient.SqlParameter("@EDM_EQM_ENQID", HF_stuid.Value)
            pParms(2) = New SqlClient.SqlParameter("@EDM_DOC_ID", hf_doc_id.Value)
            pParms(3) = New SqlClient.SqlParameter("@EDM_bCOLLECTED", IIf(HF_SDU_BCOLLECTED.Value = "False", "True", "False"))
            pParms(4) = New SqlClient.SqlParameter("@EDM_COL_USR", Session("sUsr_name"))

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "[SV].[SAVE_ENQ_DOCCOLL_M]", pParms)
            BindGrid()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub lnkUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim hf_doc_id As HiddenField = sender.parent.FindControl("hf_doc_id")
        Dim hf_DOC_DESCR As HiddenField = sender.parent.FindControl("hf_DOC_DESCR")
        lblDoc.Text = hf_DOC_DESCR.Value
        ViewState("doc_id") = hf_doc_id.Value
        pnl_UploadDoc.Visible = True
        btnuploadDoc.Visible = True
        btnuploadChangeDoc.Visible = False
    End Sub
    Protected Sub lbtnUploadDocClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnUploadDocClose.Click
        pnl_UploadDoc.Visible = False
    End Sub
    Protected Sub btnuploadDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnuploadDoc.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        lblerror.Text = ""
        Dim con As SqlConnection = New SqlConnection(str_conn)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim Stu_Id As String = HF_stuid.Value



        If FileUpload_stud.HasFile Then

            Dim serverpath As String = WebConfigurationManager.AppSettings("EnqAttachments").ToString
            Dim filen As String()
            Dim FileName As String = FileUpload_stud.PostedFile.FileName
            filen = FileName.Split("\")
            FileName = filen(filen.Length - 1)

            Dim extention = GetExtension(FileName)

            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@FILE_NAME", FileName)
            pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)
            pParms(2) = New SqlClient.SqlParameter("@EQM_ENQID", Stu_Id)
            pParms(3) = New SqlClient.SqlParameter("@EQM_DOC_ID", ViewState("doc_id"))
            pParms(4) = New SqlClient.SqlParameter("@Extension", extention)
            Dim uploadid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_DELETE_FILE_UPLOAD_MASTER", pParms)

            If HiddenUploadid.Value.Trim() <> "" Then
                HiddenUploadid.Value = HiddenUploadid.Value + "," + uploadid
            Else
                HiddenUploadid.Value = uploadid
            End If

            FileUpload_stud.SaveAs(serverpath + uploadid + "." + extention)
            BindGrid()
            BindGrid_POS()
        End If



        pnl_UploadDoc.Visible = False
    End Sub
    Protected Sub btnuploadChangeDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnuploadChangeDoc.Click
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        lblerror.Text = ""
        Dim con As SqlConnection = New SqlConnection(str_conn)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim Stu_Id As String = HF_stuid.Value

        If FileUpload_stud.HasFile Then

            Dim serverpath As String = WebConfigurationManager.AppSettings("EnqAttachments").ToString
            Dim filen As String()
            Dim FileName As String = FileUpload_stud.PostedFile.FileName
            filen = FileName.Split("\")
            FileName = filen(filen.Length - 1)

            Dim extention = GetExtension(FileName)

            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@UPLOAD_ID", ViewState("doc_id"))
            pParms(1) = New SqlClient.SqlParameter("@FILE_NAME", FileName)
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 3)
            pParms(3) = New SqlClient.SqlParameter("@Extension", extention)

            Dim uploadid As String = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "INSERT_DELETE_FILE_UPLOAD_MASTER", pParms)

            uploadid = ViewState("doc_id")
            If HiddenUploadid.Value.Trim() <> "" Then
                HiddenUploadid.Value = HiddenUploadid.Value + "," + uploadid
            Else
                HiddenUploadid.Value = uploadid
            End If

            FileUpload_stud.SaveAs(serverpath + uploadid + "." + extention)
            BindGrid()
            BindGrid_POS()
        End If
       
        pnl_UploadDoc.Visible = False
    End Sub
    Protected Sub btnCancelResetPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelupload.Click
        pnl_UploadDoc.Visible = False
    End Sub
    Private Function GetExtension(ByVal FileName As String) As String

        Dim Extension As String

        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)

        Return Extension


    End Function
End Class
