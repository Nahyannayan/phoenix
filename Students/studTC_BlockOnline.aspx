﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTC_BlockOnline.aspx.vb" Inherits="Students_studTC_BlockOnline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>TC Block Online
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
                    cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table id="tblClm" runat="server" align="center" cellpadding="4" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="rlSegment" CssClass="field-label" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true">
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>



                                <tr id="trGrid" runat="server">
                                    <td align="center">
                                        <asp:GridView ID="gvBsu" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="HideID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBsuId" runat="server" Text='<%# Bind("BSU_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBsuShort" runat="server" Text='<%# Bind("BSU_SHORTNAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("BSU_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Open/Close">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnBlock" Style="cursor: pointer" runat="server" CssClass="button" OnClick="btnBlock_Click" BackColor='<%# System.Drawing.Color.FromName(DataBinder.Eval(Container.DataItem,"BSU_COLOR").ToString())%>' Text='<%# Bind("BSU_BLOCK") %>'></asp:Button>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>

                            </table>

                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_7" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>


            </div>
        </div>
    </div>

</asp:Content>

