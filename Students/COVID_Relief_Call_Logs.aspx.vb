﻿
'Partial Class Students_COVID_Relief_StudentDetail
'    Inherits System.Web.UI.Page

'End Class

Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net

Partial Class Students_COVID_Relief_Call_Logs
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                ViewState("RRH_ID") = Encr_decrData.Decrypt(Request.QueryString("RRH_ID").Replace(" ", "+"))
                BindCommunication()
                gridbind(ViewState("RRH_ID"))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
    End Sub

    Public Sub BindCommunication()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_COV_RELIEF_COMMUNICATION_MODE")
            If ds.Tables(0).Rows.Count > 0 Then
                ddlComunicationMode.DataSource = ds.Tables(0)
                ddlComunicationMode.DataTextField = "RCM_DESC"
                ddlComunicationMode.DataValueField = "RCM_ID"
                ddlComunicationMode.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Public Sub gridbind(ByRef RRH_ID As Integer)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@RRC_RRH_ID", RRH_ID)

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "COV.GET_COV_RELIEF_REQUEST_CALL_LOG", param)
            If ds.Tables(0).Rows.Count > 0 Then

                gvAttachment.DataSource = ds.Tables(0)
                gvAttachment.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                gvAttachment.DataSource = ds.Tables(0)
                Try
                    gvAttachment.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvAttachment.Rows(0).Cells.Count
                gvAttachment.Rows(0).Cells.Clear()
                gvAttachment.Rows(0).Cells.Add(New TableCell)
                gvAttachment.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAttachment.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAttachment.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Protected Sub lnkDownload_Click(sender As Object, e As EventArgs)
        Dim FilePath As String = TryCast(sender.FindControl("hf_PhotoPath"), HiddenField).Value
        If FilePath = "" Then
            Exit Sub
        End If
        DownloadFile(FilePath)
    End Sub
    Sub DownloadFile(filename As String)
        Dim tempDir As String = Convert.ToString(ConfigurationManager.AppSettings("covidfiles"))
        Dim tempFileName As String = filename
        Dim tempFileNameUsed As String = tempDir + tempFileName
        Dim myWebClient As New WebClient
        ''myWebClient.DownloadFile(tempFileNameUsed, filename)

        Dim data As Byte() = myWebClient.DownloadData(tempFileNameUsed)
        Response.ContentType = "application/octect-stream"
        Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        Response.BinaryWrite(data)
        Response.Flush()
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        If (txtComment.Text <> "") Then
            errLbl.CssClass = "text-danger"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim objConn As New SqlConnection(str_conn) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retVal As Int64
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@RRC_ID", SqlDbType.Int)
                pParms(0).Value = 0
                pParms(0).Direction = ParameterDirection.InputOutput
                pParms(1) = New SqlClient.SqlParameter("@RRC_RRH_ID", SqlDbType.Int)
                pParms(1).Value = ViewState("RRH_ID")
                pParms(2) = New SqlClient.SqlParameter("@RRC_LOG_DATE", SqlDbType.DateTime)
                pParms(2).Value = Date.Now
                pParms(3) = New SqlClient.SqlParameter("@RRC_LOG_USR_ID", SqlDbType.VarChar, 20)
                pParms(3).Value = Session("sUsr_name")

                pParms(4) = New SqlClient.SqlParameter("@RRC_COM_MODE", SqlDbType.VarChar, 20)
                pParms(4).Value = ddlComunicationMode.SelectedItem.Text
                pParms(5) = New SqlClient.SqlParameter("@RRC_COMMENTS", SqlDbType.NVarChar, 200)
                pParms(5).Value = txtComment.Text.Trim
                pParms(6) = New SqlClient.SqlParameter("@RRC_FILETYPE", SqlDbType.VarChar, 100)
                pParms(6).Value = ""
                pParms(7) = New SqlClient.SqlParameter("@RRC_FILENAME", SqlDbType.VarChar, 200)
                pParms(7).Value = ""
                pParms(8) = New SqlClient.SqlParameter("@RRC_FILEPATH", SqlDbType.VarChar, 2000)
                pParms(8).Value = ""
                pParms(9) = New SqlClient.SqlParameter("@RRC_RRH_STATUS", SqlDbType.VarChar, 20)
                pParms(9).Value = ""


                pParms(10) = New SqlClient.SqlParameter("@RetVal", SqlDbType.BigInt)
                pParms(10).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "COV.SAVE_COV_RELIEF_REQUEST_CALL_LOG", pParms)
                retVal = pParms(10).Value
                Dim ID As Int64 = pParms(0).Value
                If (retVal = 0) Then

                    If fileUpload1.HasFile Then
                        Dim fileName As String = fileUpload1.PostedFile.FileName
                        Dim FileType As String = fileUpload1.PostedFile.ContentType
                        Dim serverpath As String = WebConfigurationManager.AppSettings("covidreleif").ToString

                        Directory.CreateDirectory(serverpath + "/call_logs/" + ID.ToString)
                        Dim filen As String()
                        fileName = fileUpload1.PostedFile.FileName
                        filen = fileName.Split("\")
                        fileName = filen(filen.Length - 1)
                        fileUpload1.SaveAs(serverpath + "/call_logs/" + ID.ToString + "/" + fileName)
                        Dim pParms1(5) As SqlClient.SqlParameter
                        pParms1(0) = New SqlClient.SqlParameter("@RRC_ID", ID.ToString)
                        pParms1(1) = New SqlClient.SqlParameter("@RRC_FILETYPE", FileType)
                        pParms1(2) = New SqlClient.SqlParameter("@RRC_FILENAME", fileName)
                        pParms1(3) = New SqlClient.SqlParameter("@RRC_FILEPATH", "\call_logs\" + ID.ToString + "\" + fileName)

                        pParms1(4) = New SqlClient.SqlParameter("@RetVal", SqlDbType.BigInt)
                        pParms1(4).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "COV.UPDATE_COV_RELIEF_REQUEST_CALL_LOG", pParms1)
                        retVal = pParms1(4).Value
                    End If
                    If retVal <> "0" Then
                        stTrans.Rollback()
                        errLbl.Text = "Unable to save the data"
                    Else
                        stTrans.Commit()
                        errLbl.Text = "Data Saved Sucessfully"
                        errLbl.CssClass = "text-success"
                        gridbind(ViewState("RRH_ID"))
                        txtComment.Text = ""
                    End If
                Else
                    stTrans.Rollback()
                    errLbl.Text = "Unable to save the data"
                End If
            Catch ex As Exception

                errLbl.Text = "Unable to save the data"
                stTrans.Rollback()
                UtilityObj.Errorlog(ex.Message)
            Finally
                objConn.Close()
            End Try
        Else
            errLbl.Text = "Please enter the comments"

        End If
    End Sub
End Class

