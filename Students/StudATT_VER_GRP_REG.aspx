<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="true"
    CodeFile="StudATT_VER_GRP_REG.aspx.vb" Inherits="Students_StudATT_VER_GRP_REG" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">



        function checkDate(sender, args) {

            var currenttime = document.getElementById("<%=hfDate.ClientID %>").value;

            if (sender._selectedDate > new Date(currenttime)) {
                alert("You cannot select a day greater than today!");
                sender._selectedDate = new Date(currenttime);
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }


        function hideButton() {
            document.forms[0].submit();
            window.setTimeout("hideButton('" + window.event.srcElement.id + "')", 0);
        }

        function hideButton(buttonID) {
            document.getElementById(buttonID).style.display = 'none';
            document.getElementById('<%=btnSave1.ClientID %>').style.display = 'none';
          document.getElementById('<%=btnSave2.ClientID %>').style.display = 'none';
      }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Vertical Group Attendance"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                          
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" DisplayMode="List"
                                        EnableViewState="False"  ValidationGroup="AttGroup"></asp:ValidationSummary>
                                    
                                </div>
                          
                        </td>
                    </tr>
                    <tr style="font-size: 8pt; color: #800000" valign="bottom">
                        <%--<td align="center" class="matters" valign="middle">&nbsp;Fields Marked with (<span style="font-size: 8pt; color: #800000">*</span>)
                are mandatory</td>--%>
                        <td align="center" class="text-danger font-small" valign="middle">Fields Marked with ( * ) are mandatory
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%-- <tr>
                                    <td class="subheader_img" colspan="6">
                                        <span style="font-size: 10pt; color: #ffffff">
                                            <asp:Literal ID="ltLabel" runat="server" Text="Vertical Group Attendance"></asp:Literal></span></td>
                                </tr>--%>
                                <tr>
                                    <td align="left" ><span class="field-label">Academic Year</span><span class="text-danger font-small">*</span></td>
                                 
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>

                                    <td></td>
                                     <td></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Group</span><span class="text-danger font-small">*</span></td>
                                 
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Attendance Period</span><span class="text-danger font-small">*</span></td>
                                
                                    <td align="left" >
                                        <asp:DropDownList ID="ddlPERIOD" runat="server" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                    <td align="left" ><span class="field-label">Attendance Date</span><span class="text-danger font-small">*</span></td>
                                   
                                    <td align="left" >
                                        <asp:TextBox ID="txtDate" runat="server" ></asp:TextBox>&nbsp;<asp:ImageButton
                                            ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                                CssClass="error" Display="Dynamic" ErrorMessage="Attendance Date required" ForeColor=""
                                                ValidationGroup="AttGroup">*</asp:RequiredFieldValidator></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td  align="center">
                            <br />
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button" Text="Add"
                                ValidationGroup="AttGroup" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False"
                                    CssClass="button" Text="Edit" ValidationGroup="AttGroup" OnClick="btnEdit_Click"  /></td>
                    </tr>
                    <tr>
                        <td  valign="bottom"></td>
                    </tr>
                    <tr>
                        <td  valign="bottom" align="center">
                            <asp:Button ID="btnSave2"
                                runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button
                                    ID="btnCancel2" runat="server" CausesValidation="False" CssClass="button"
                                    Text="Cancel"  /></td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <div id="divAttparam" runat="server" style="border: 1px solid #1b80b6; width: 90%; text-align: left; font-weight: normal; font-size: 10px; padding: 3px;"></div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:GridView ID="gvInfo" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" Width="100%" DataKeyNames="SRNO"
                                OnRowCreated="gvInfo_RowCreated" EnableModelValidation="True">
                                <RowStyle CssClass="griditem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsNo" runat="server" Text='<%# Bind("SRNO") %>'></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="StudentID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStuNo" runat="server" Text='<%# bind("STU_NO") %>'></asp:Label>
                                            <asp:Label ID="lblStud_ID" runat="server" Text='<%# Bind("STU_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblRAL_ID" runat="server" Text='<%# bind("RAL_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblAPPLEAVE" runat="server" Text='<%# Bind("APPLEAVE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblAPD_ID" runat="server" Text='<%# Bind("APD_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStudName" runat="server" Text='<%# bind("STUDNAME") %>'></asp:Label>
                                            <%-- <asp:Literal id="ltStar" runat="server" Text="*" __designer:wfdid="w7"></asp:Literal>--%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 1">
                                        <ItemTemplate>
                                            <div id="divp1" runat="server"><%# Eval("P1")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 2">
                                        <ItemTemplate>
                                            <div id="divp2" runat="server"><%# Eval("P2")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 3">
                                        <ItemTemplate>
                                            <div id="divp3" runat="server"><%# Eval("P3")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 4">
                                        <ItemTemplate>
                                            <div id="divp4" runat="server"><%# Eval("P4")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 5">
                                        <ItemTemplate>
                                            <div id="divp5" runat="server"><%# Eval("P5")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 6">
                                        <ItemTemplate>
                                            <div id="divp6" runat="server"><%# Eval("P6")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 7">
                                        <ItemTemplate>
                                            <div id="divp7" runat="server"><%# Eval("P7")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 8">
                                        <ItemTemplate>
                                            <div id="divp8" runat="server"><%# Eval("P8")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 9">
                                        <ItemTemplate>
                                            <div id="divp9" runat="server"><%# Eval("P9")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="PERIOD 10">
                                        <ItemTemplate>
                                            <div id="divp10" runat="server"><%# Eval("P10")%></div>
                                        </ItemTemplate>
                                        <HeaderStyle Wrap="False" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtRemarks" runat="server"
                                                Text='<%# bind("REMARKS") %>' MaxLength="255"></asp:TextBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="ftbeRemark" runat="server" TargetControlID="txtRemarks"
                                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars=". ">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DAY1" HeaderText="DAY1">
                                        <HeaderStyle Wrap="True"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DAY2" HeaderText="DAY2">
                                        <HeaderStyle Wrap="True"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DAY3" HeaderText="DAY3">
                                        <HeaderStyle Wrap="True"></HeaderStyle>
                                    </asp:BoundField>
                                </Columns>
                                <SelectedRowStyle />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnSave1"
                                runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button
                                ID="btnCancel1" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>

                </table>
                <asp:HiddenField ID="hfDay1" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay2" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay3" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay4" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfDay5" runat="server"></asp:HiddenField>
                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgCalendar" TargetControlID="txtDate" OnClientDateSelectionChanged="checkDate">
                </ajaxToolkit:CalendarExtender>
                <asp:HiddenField ID="hfDate" runat="server"></asp:HiddenField>

            </div>
        </div>
    </div>

</asp:Content>

