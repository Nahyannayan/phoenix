<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudVATT_M_GROUP_EDIT.aspx.vb" Inherits="Curriculum_clmGroup_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">





        function confirm_delete() {

            if (confirm("You are about to remove the selected students from this group.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }



        var color = '';
        function highlightView(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvView.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}

function highlightAdd(obj) {
    var rowObject = getParentRow(obj);
    var parentTable = document.getElementById("<%=gvAdd.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}

function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Vertical Attendance Group Setting(s)"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">

                    <tr>
                        <td align="left" valign="bottom"  >&nbsp;<asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                            ></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" ForeColor=""
                                ValidationGroup="TeachAdd"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"   valign="bottom">
                            <asp:LinkButton ID="lbAddNew" runat="server" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td   >
                            <table width="100%">

                                <tr>
                                    <td align="left" width="20%"  >
                                    <span class="field-label">   <asp:Label ID="lblAccText" runat="server" Text="Select Academic Year "  ></asp:Label></span> </td>
                                    <td align="left" width="30%"  >
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"  ><span class="field-label">Group Name</span></td>
                                    <td align="left" width="30%"  >

                                        <asp:TextBox ID="txtDescr" runat="server" TabIndex="2" MaxLength="100"  ></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescr"
                                            CssClass="text-danger" Display="Dynamic" ErrorMessage="Group Name Required " ForeColor="" ValidationGroup="TeachAdd"></asp:RequiredFieldValidator></td>

                                </tr>
                                <tr>
                                    <td align="left"   >
                                    <span class="field-label">    <asp:Label ID="lbl15" runat="server" Text="Teacher"  ></asp:Label></span></td>
                                    <td align="left"  >
                                        <asp:DropDownList ID="ddlTeacher" runat="server"  >
                                        </asp:DropDownList><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlTeacher"
                                            CssClass="text-danger" Display="Dynamic" ErrorMessage="Select teacher" ForeColor=""
                                            ValidationGroup="TeachAdd"></asp:RequiredFieldValidator></td>
                                    <td align="left"   ><span class="field-label">From Date </span>
                                    </td>
                                    <td align="left"  >
                                        <asp:TextBox ID="txtFrom" runat="server"  ></asp:TextBox>
                                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/calendar.gif" TabIndex="4" /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFrom"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="From Date Required" ForeColor="" ValidationGroup="TeachAdd"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="6">
                                        <table id="Table2" border="0"  width="100%">
                                            <tr>
                                                <td align="center">

                                                    <asp:Button ID="btnAddNew" runat="server" Text="Add Teacher"
                                                        CssClass="button"  ValidationGroup="TeachAdd"></asp:Button>
                                                    <asp:Button ID="btnUpdate" runat="server" CssClass="button" OnClick="btnUpdate_Click"
                                                        Text="Update" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gvTeacher" runat="server"  
                                                        CssClass="table table-bordered table-row"  DataKeyNames="SRNO" EmptyDataText="No Teachers for this group" AutoGenerateColumns="False" PageSize="2"
                                                         OnRowEditing="gvTeacher_RowEditing" Width="100%">
                                                        <RowStyle CssClass="griditem"  ></RowStyle>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SRNO" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSRNO" runat="server" Text='<%# Bind("SRNO") %>' __designer:wfdid="w15"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Teacher">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAVT_ENAME" runat="server" Text='<%# Bind("AVT_ENAME") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="From Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAVT_FROMDT" runat="server" Text='<%# Bind("AVT_FROMDT") %>' ></asp:Label>
                                                                </ItemTemplate>

                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AVT_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAVT_ID" runat="server" Text='<%# Bind("AVT_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AVT_AVG_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAVT_AVG_ID" runat="server" Text='<%# Bind("AVT_AVG_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AVT_EMP_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAVT_EMP_ID" runat="server" Text='<%# Bind("AVT_EMP_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="STATUS" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSTATUS" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="delete" Text="delete" HeaderText="delete">
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="Edit" Text="Edit" HeaderText="Edit">
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:ButtonField>
                                                        </Columns>

                                                        <SelectedRowStyle CssClass="griditem_hilight"></SelectedRowStyle>

                                                        <HeaderStyle CssClass="gridheader_pop"  ></HeaderStyle>

                                                        <AlternatingRowStyle CssClass="griditem_alternative"></AlternatingRowStyle>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Button ID="btnSave_T" runat="server" Text="Save" CssClass="button" ></asp:Button><asp:Button ID="btnCancel_T" runat="server" Text="Cancel" CssClass="button"  OnClick="btnCancel_T_Click"></asp:Button><asp:Button ID="btnEdit_T" runat="server" Text="Edit" CssClass="button"  OnClick="btnEdit_T_Click"></asp:Button></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td   >
                            <table border="0"  width="100%">
                                <tr>
                                    <td >
                                        <asp:LinkButton ID="lbView" runat="server" >ViewStudents</asp:LinkButton>
                                    </td>
                                    <td >
                                        <asp:LinkButton ID="lbAdd" runat="server" >Allocate Students</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td   ></td>
                    </tr>
                    <tr>
                        <td align="center">

                            <table  border="0"  cellpadding="0" cellspacing="0" align="center" id="tbSearch" runat="server"  width="100%">
                                <tr class="title-bg">
                                    <td align="left"  colspan="6"  >
                                         SEARCH</td>
                                </tr>
                                <tr>
                                    <td  align="left" > <span class="field-label"> Grade</span></td>
                                    <td   align="left">
                                        <asp:DropDownList ID="ddlGrade" runat="server"   AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td  align="left" > <span class="field-label"> Section</span></td>
                                    <td   align="left">
                                        <asp:DropDownList ID="ddlSect" runat="server" >
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td  align="left"> <span class="field-label"> Student ID</span></td>
                                    <td  >
                                        <asp:TextBox ID="txtStuNo" runat="server"></asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSTU_NO" runat="server" BehaviorID="AutoCompleteEx1"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNo" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtStuNo">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>
                                    <td > <span class="field-label"> Student Name</span></td>
                                    <td  >
                                        <asp:TextBox ID="txtNAME" runat="server"></asp:TextBox>
                                        <ajaxToolkit:AutoCompleteExtender ID="acSAME" runat="server" BehaviorID="AutoCompleteEx2"
                                            CompletionInterval="1000" CompletionListCssClass="autocomplete_completionListElement_S1"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem_S1" CompletionListItemCssClass="autocomplete_listItem_S1"
                                            CompletionSetCount="5" DelimiterCharacters="" EnableCaching="false" MinimumPrefixLength="1"
                                            ServiceMethod="StudentNAME" ServicePath="~/Students/WebServices/StudentService.asmx"
                                            TargetControlID="txtNAME">
                                            <Animations>
                    <OnShow>
                        <Sequence>
                            
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide></Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="center" >
                                        <asp:Button ID="btnList" runat="server" CausesValidation="False" CssClass="button"
                                            Text="List"   OnClick="btnList_Click" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblErrorStu" runat="server" CssClass="text-danger" EnableViewState="False"
                                SkinID="error"  Style="text-align: center"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"  colspan="7"  >&nbsp;
                        </td>
                    </tr>


                    <tr>
                        <td colspan="7">
                            <asp:MultiView ID="mvMaster" ActiveViewIndex="0" runat="server">


                                <asp:View ID="View1" runat="server">


                                    <table id="tb1" border="0"  width="100%">
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="btnRemove2" runat="server" CssClass="button" Text="Remove from Group" OnClientClick="javascript:return confirm_delete();" ValidationGroup="groupM1" TabIndex="7"  /></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Panel ID="Panel2" runat="server" ScrollBars="Vertical" >
                                                    <asp:GridView ID="gvView" runat="server" AutoGenerateColumns="False"
                                                          EmptyDataText="No Records Found"  CssClass="table table-bordered table-row"
                                                       PageSize="40" Width="100%">
                                                        <RowStyle CssClass="griditem"   />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Available">
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" />

                                                                </EditItemTemplate>
                                                                <HeaderTemplate>
                                                                   Select <br />
                                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                                                    ToolTip="Click here to select/deselect all rows" />

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" onclick="javascript:highlightView(this);" runat="server" __designer:wfdid="w38"></asp:CheckBox>
                                                                </ItemTemplate>

                                                                <HeaderStyle ></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuId" runat="server"  Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SL.No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSlNo" runat="server"  Text='<%# bind("srno") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Stud No.">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFeeId" runat="server"  Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Student Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuName" runat="server"  Text='<%# Bind("SNAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Grade">
                                                                <EditItemTemplate>
                                                                    &nbsp;
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Section">
                                                                <EditItemTemplate>
                                                                    &nbsp;
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("SCT_DESCR") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle  />
                                                        <HeaderStyle    />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="btnRemove" runat="server" CssClass="button" Text="Remove from Group" OnClientClick="javascript:return confirm_delete();" ValidationGroup="groupM1" TabIndex="7"  /></td>
                                        </tr>
                                    </table>


                                </asp:View>


                                <asp:View ID="View2" runat="server">


                                    <table id="Table1" border="0"  width="100%">
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="btnAllocate2" runat="server" CssClass="button" Text="Add to Group" ValidationGroup="groupM1" TabIndex="7"  /></td>
                                        </tr>
                                        <tr>
                                            <td align="center"  >
                                                <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical">
                                                    <asp:GridView ID="gvAdd" runat="server" AutoGenerateColumns="False"
                                                         EmptyDataText="No Records Found"  CssClass="table table-bordered table-row"
                                                        PageSize="40" Width="100%">
                                                        <RowStyle CssClass="griditem" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Available">
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" __designer:wfdid="w35"></asp:CheckBox>
                                                                </EditItemTemplate>
                                                                <HeaderTemplate>
                                                                    Select <br />
                                                                                    <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this);" runat="server"  ToolTip="Click here to select/deselect all rows"></asp:CheckBox>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" onclick="javascript:highlightAdd(this);" runat="server" ></asp:CheckBox>
                                                                </ItemTemplate>

                                                                <HeaderStyle ></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("STU_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SL.No">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSlNo" runat="server" Text='<%# BIND("SRNO") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Stud No.">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Student Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Grade">
                                                                <EditItemTemplate>
                                                                    &nbsp;
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("GRM_DISPLAY") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Section">
                                                                <EditItemTemplate>
                                                                    &nbsp;
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("SCT_DESCR") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle  />
                                                        <HeaderStyle   />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="btnAllocate" runat="server" CssClass="button" Text="Add to Group" ValidationGroup="groupM1" TabIndex="7"   />
                                            </td>
                                        </tr>
                                    </table>




                                </asp:View>
                            </asp:MultiView></td>
                    </tr>
                    <tr>
                        <td valign="bottom"  >&nbsp;<asp:HiddenField ID="hfAVG_ID" runat="server" />
                            <asp:HiddenField ID="hfAddNew" runat="server" />
                            <asp:HiddenField ID="hfDescr" runat="server" />
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp;
            &nbsp; &nbsp;
        &nbsp;
        &nbsp;
        &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
            PopupButtonID="txtFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
        </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server"
                                PopupButtonID="imgFrom" TargetControlID="txtFrom" Format="dd/MMM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                        </td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>


