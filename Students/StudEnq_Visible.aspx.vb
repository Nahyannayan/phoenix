﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_StudEnq_Visible
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If


            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("datamode") = "view"

            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'if query string returns Eid  if datamode is view state

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100161") Then

                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'calling page right class to get the access rights
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'disable the control based on the rights

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                gridbind()
            End If

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub gridbind()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As New DataSet
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[ONLINE_ENQ].[GET_ENQUIRY_VISIBLE]", PARAM)

        If ds.Tables(0).Rows.Count > 0 Then
            gvVisible.DataSource = ds.Tables(0)
            gvVisible.DataBind()

        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvVisible.DataSource = ds.Tables(0)
            Try
                gvVisible.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = gvVisible.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            gvVisible.Rows(0).Cells.Clear()
            gvVisible.Rows(0).Cells.Add(New TableCell)
            gvVisible.Rows(0).Cells(0).ColumnSpan = columnCount
            gvVisible.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvVisible.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        End If
    End Sub

    Protected Sub btnSav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSav.Click
        Dim lbl As Label
        Dim chk As CheckBox, chkValidate As CheckBox
        For Each rowItem As GridViewRow In gvVisible.Rows
            lbl = DirectCast(rowItem.FindControl("lblID"), Label)
            chk = DirectCast(rowItem.FindControl("cbVisible"), CheckBox)
            chkValidate = DirectCast(rowItem.FindControl("cbValid"), CheckBox)
            Dim Status As Integer
            Dim param(14) As SqlClient.SqlParameter
            Dim transaction As SqlTransaction
            
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try

                    param(0) = New SqlParameter("@EQV_ID", lbl.Text)
                    param(1) = New SqlParameter("@BSU_ID", Session("sBsuid"))
                    param(2) = New SqlClient.SqlParameter("@bVISIBLE", chk.Checked)
                    param(3) = New SqlClient.SqlParameter("@bVALIDATE", chkValidate.Checked)
                    param(4) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                    param(4).Direction = ParameterDirection.ReturnValue

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[ONLINE_ENQ].[SAVE_ENQUIRY_VISIBLE_D]", param)
                    Status = param(4).Value





                Catch ex As Exception

                    lblError.Text = "Record could not be updated"
                Finally
                    If Status <> 0 Then
                        UtilityObj.Errorlog(lblError.Text)
                        transaction.Rollback()
                    Else
                        lblError.Text = "Updated..."
                        transaction.Commit()
                    End If
                End Try

            End Using

        Next
        gridbind()
    End Sub
    Protected Sub CbV1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim chk As CheckBox = DirectCast(sender, CheckBox)
        Dim gr As GridViewRow = DirectCast(chk.Parent.Parent, GridViewRow)

        Dim chkvisi As New CheckBox
        chkvisi = gr.FindControl("cbVisible")
        If chk.Checked Then
            chkvisi.Checked = True
        
        End If


    End Sub
    Protected Sub CbV2_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim chk As CheckBox = DirectCast(sender, CheckBox)
        Dim gr As GridViewRow = DirectCast(chk.Parent.Parent, GridViewRow)

        Dim chkvalid As New CheckBox
        chkvalid = gr.FindControl("cbValid")
        If chk.Checked = False Then
            chkvalid.Checked = False


        End If


    End Sub
End Class
