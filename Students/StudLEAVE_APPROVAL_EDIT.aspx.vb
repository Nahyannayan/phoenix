Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Partial Class Students_studLeave_Entry_Edit
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059027") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    If ViewState("datamode") = "view" Then
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))

                        Call Student_Approval_edit()
                        bindAPD_ID()
                        getStart_EndDate()
                        GetEmpID()
                        disable_control()

                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Sub bindAPD_ID()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim sqlstr As String
        sqlstr = "select APD_ID,APD_PARAM_DESCR from ATTENDANCE_PARAM_D where APD_ACD_ID = '" & ViewState("ACD_ID") & "' " & _
" and APD_bSHOW=1 AND APD_PARAM_DESCR NOT IN ('PRESENT','ABSENT')" & _
" AND APD_PARAM_DESCR not like '%late%'" & _
" ORDER BY ISNULL(APD_DISPLAYORDER,5) "

        Dim DS As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlstr)
        ddlAPD_ID.Items.Clear()
        ddlAPD_ID.DataSource = DS.Tables(0)
        ddlAPD_ID.DataTextField = "APD_PARAM_DESCR"
        ddlAPD_ID.DataValueField = "APD_ID"
        ddlAPD_ID.DataBind()
        ddlAPD_ID.Items.Add(New ListItem("-- SELECT --", "0"))
        ddlAPD_ID.ClearSelection()
        If Not ddlAPD_ID.Items.FindByValue(ViewState("SLA_APD_ID")) Is Nothing Then
            ddlAPD_ID.Items.FindByValue(ViewState("SLA_APD_ID")).Selected = True
        Else
            ddlAPD_ID.Items.FindByValue("0").Selected = True
        End If




    End Sub
    Sub Student_Approval_edit()
        Using readerSTUDLeavesApproval_edit As SqlDataReader = AccessStudentClass.GetStudLeavesApproval_edit(ViewState("viewid"))
            While readerSTUDLeavesApproval_edit.Read
                ViewState("ACD_ID") = Convert.ToString(readerSTUDLeavesApproval_edit("ACD_ID"))
                lblAcdYear.Text = Convert.ToString(readerSTUDLeavesApproval_edit("ACY_DESCR"))
                lblSName.Text = Convert.ToString(readerSTUDLeavesApproval_edit("SNAME"))
                lblGrade.Text = Convert.ToString(readerSTUDLeavesApproval_edit("GRM_DIS"))
                lblSection.Text = Convert.ToString(readerSTUDLeavesApproval_edit("SCT_DESCR"))
                ViewState("SLA_APD_ID") = Convert.ToString(readerSTUDLeavesApproval_edit("SLA_APD_ID"))
                lblAcdYear.Enabled = False
                lblGrade.Enabled = False
                lblSName.Enabled = False
                lblSection.Enabled = False
                If IsDate(readerSTUDLeavesApproval_edit("FROMDT")) = True Then
                    txtFrom.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerSTUDLeavesApproval_edit("FROMDT"))))
                End If
                If IsDate(readerSTUDLeavesApproval_edit("TODT")) = True Then
                    txtTo.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerSTUDLeavesApproval_edit("TODT"))))
                End If
                txtRemarks.Text = Convert.ToString(readerSTUDLeavesApproval_edit("REMARKS"))
                Dim tempStatus As String = Convert.ToString(readerSTUDLeavesApproval_edit("STATUS"))
                If UCase(Trim(tempStatus)) = "APPROVED" Then
                    rbApproved.Checked = True
                ElseIf UCase(Trim(tempStatus)) = "NOT APPROVED" Then
                    rbNotApproved.Checked = True
                End If
            End While
        End Using
    End Sub
    Sub disable_control()
        txtTo.Enabled = False
        txtFrom.Enabled = False
        imgCalendar.Enabled = False
        ImageButton1.Enabled = False
        rbApproved.Enabled = False
        rbNotApproved.Enabled = False
        txtRemarks.Enabled = False
        ddlAPD_ID.Enabled = False
    End Sub
    Sub enable_control()
        txtTo.Enabled = True
        txtFrom.Enabled = True
        imgCalendar.Enabled = True
        ImageButton1.Enabled = True
        rbApproved.Enabled = True
        rbNotApproved.Enabled = True
        txtRemarks.Enabled = True
        ddlAPD_ID.Enabled = True
    End Sub
    Function serverDateValidate() As String
        Dim CommStr As String = String.Empty
        If txtFrom.Text.Trim <> "" Then
            Dim strfDate As String = txtFrom.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                CommStr = CommStr & "<div>From Date from is Invalid</div>"
            Else
                txtFrom.Text = strfDate
                Dim dateTime1 As String
                dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                If Not IsDate(dateTime1) Then
                    CommStr = CommStr & "<div>From Date from is Invalid</div>"
                End If
            End If
        End If
        If txtTo.Text.Trim <> "" Then
            Dim strfDate As String = txtTo.Text.Trim
            Dim str_err As String = DateFunctions.checkdate(strfDate)
            If str_err <> "" Then
                CommStr = CommStr & "<div>To Date format is Invalid</div>"
            Else
                txtTo.Text = strfDate
                Dim DateTime2 As Date
                Dim dateTime1 As Date
                Dim strfDate1 As String = txtFrom.Text.Trim
                Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                If str_err1 <> "" Then
                Else
                    DateTime2 = Date.ParseExact(txtTo.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    dateTime1 = Date.ParseExact(txtFrom.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If IsDate(DateTime2) Then
                        If DateTime2 < dateTime1 Then

                            CommStr = CommStr & "<div>To date must be greater than or equal to From Date</div>"
                        End If
                    Else

                        CommStr = CommStr & "<div>Invalid To date</div>"
                    End If
                End If
            End If
        End If

        If CommStr <> "" Then
            lblError.Text = CommStr
            Return "-1"
        Else
            Return "0"
        End If
    End Function
    Sub getStart_EndDate()
        Dim ACD_ID As String = ViewState("ACD_ID")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim sql_query = "select ACD_StartDt,ACD_ENDDT  from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sql_query)
        ViewState("ACD_STARTDT") = ds.Tables(0).Rows(0).Item("ACD_StartDt")
        ViewState("ACD_ENDDT") = ds.Tables(0).Rows(0).Item("ACD_ENDDT")
    End Sub
    Sub GetEmpID()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    '   Sub GetTCISSUE_DATE()
    '       Dim sqlString As String = " SELECT    replace(convert(varchar(11), STU_TCISSUEDATE, 106), ' ', '/') FROM    STUDENT_M   where " & _
    '" STU_ID='" & ViewState("STU_ID") & "' and  STU_ACD_ID='" & ViewState("ACD_ID") & "' and  STU_GRD_ID='" & ViewState("GRD_ID") & "' and " & _
    '" STU_SCT_ID='" & ViewState("SCT_ID") & "' and  STU_STM_ID='" & ViewState("STM_ID") & "' and STU_SHF_ID='" & ViewState("SHF_ID") & "'"
    '       Dim sdate As Object
    '       Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '       sdate = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, sqlString)
    '       If IsDBNull(sdate) = False Then
    '           If IsDate(CStr(sdate)) = True Then
    '               ViewState("TC_ISSUEDT") = CStr(sdate)
    '           Else
    '               ViewState("TC_ISSUEDT") = ""
    '           End If
    '       Else
    '           ViewState("TC_ISSUEDT") = ""
    '       End If
    '   End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim Acd_SDate As Date = ViewState("ACD_STARTDT")
        Dim Acd_EDate As Date = ViewState("ACD_ENDDT")
        Dim From_date As Date = txtFrom.Text
        Dim To_Date As Date = txtTo.Text
        If serverDateValidate() = "0" Then
            If (From_date >= Acd_SDate And From_date <= Acd_EDate) And (To_Date >= Acd_SDate And To_Date <= Acd_EDate) Then
                str_err = CallTransaction(errorMessage)
                If str_err = "0" Then
                    disable_control()
                    lblError.Text = "Record updated successfully"
                Else
                    lblError.Text = errorMessage
                End If
            Else
                lblError.Text = "From Date to To Date must be with in the academic year"
            End If

        End If
    End Sub
    Private Function CallTransaction(ByRef errorMessage As String) As String
        Dim Status As Integer
        Dim RecordStatus As String = String.Empty
        Dim transaction As SqlTransaction
        Dim SLA_ID As String = ViewState("viewid")
        Dim SLA_EMP_ID_APPR As String = ViewState("EMP_ID")
        Dim SLA_FROMDT As String = txtFrom.Text
        Dim SLA_TODT As String = txtTo.Text
        Dim SLA_REMARKS As String = txtRemarks.Text
        Dim SLA_APPRLEAVE As String = String.Empty
        Dim SLA_APD_ID As String = ddlAPD_ID.SelectedValue

        If rbApproved.Checked = True Then
            SLA_APPRLEAVE = "APPROVED"
        Else
            SLA_APPRLEAVE = "NOT APPROVED"
        End If


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Status = AccessStudentClass.SAVESTUD_LEAVE_APPROVAL_EDIT(SLA_ID, SLA_EMP_ID_APPR, _
                   SLA_FROMDT, SLA_TODT, SLA_REMARKS, SLA_APPRLEAVE, SLA_APD_ID, transaction)
                If Status = -111 Then
                    CallTransaction = "1"
                    errorMessage = "Date entered is already set for leave approval!!!"
                    Return "1"
                ElseIf Status <> 0 Then
                    CallTransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(Status)
                    Return "1"
                Else

                    Status = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), SLA_EMP_ID_APPR, "edit", Page.User.Identity.Name.ToString, Me.Page)
                    If Status <> 0 Then
                        CallTransaction = "1"
                        errorMessage = "Could not process audit request"
                        Return "1"
                    End If
                End If
                      
                ViewState("viewid") = "0"
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                CallTransaction = "0"
                
            Catch ex As Exception
                errorMessage = "Record could not be Updated"
            Finally
                If CallTransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try
        End Using

    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("datamode") = "edit"
        enable_control()
        ' btnFill.Visible = True
        Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ViewState("datamode") = "edit" Then
            ViewState("datamode") = "none"
            disable_control()
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

End Class
