Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_ShowAcademicInfo
    Inherits System.Web.UI.Page

    Dim SearchMode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                SearchMode = Request.QueryString("id")
                If SearchMode = "AY" Then
                    Page.Title = "Academic Year Info"

                ElseIf SearchMode = "CU" Then
                    Page.Title = "Curriculum Info"
                ElseIf SearchMode = "BY" Then
                    Page.Title = "Academic Year"

                ElseIf SearchMode = "GY" Then
                    Page.Title = "Academic Year"
                ElseIf SearchMode = "GB" Then
                    Page.Title = "Academic Year"
                ElseIf SearchMode = "R" Then
                    Page.Title = "Religion"
                End If


                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                gridbind()
            Catch ex As Exception

            End Try
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If

        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        'str_img = h_selected_menu_1.Value()
        str_Sid_img = h_selected_menu_1.Value.Split("__")
        getid(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid1(str_Sid_img(2))

    End Sub

    Public Function getid(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvEmpInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                'Return gvEmpInfo.HeaderRow.Controls("mnu_2_img").ClientID
                s = gvEmpInfo.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_code As String
            Dim str_filter_name As String
            Dim CurBsUnit As String = Session("sBsuid").ToString & ""
            SearchMode = Request.QueryString("id")
            If SearchMode = "AY" Then 'get all the academic year
                str_Sql = "Select ID,E_Name from(SELECT ACY_ID as ID,ACY_DESCR as E_Name  FROM [ACADEMICYEAR_M])a where a.id<>''"

            ElseIf SearchMode = "BY" Then 'get the business unit academic year
                str_Sql = "Select ID,E_Name from(SELECT distinct ACADEMICYEAR_M.ACY_DESCR as E_Name, ACADEMICYEAR_D.ACD_ID as ID, ACADEMICYEAR_D.ACD_BSU_ID as BSU_ID, ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID  " & _
                        " FROM ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.CLM_ID = '" & Session("CLM") & "' and   a.BSU_ID='" & CurBsUnit & "' and a.id<>''"
            ElseIf SearchMode = "CU" Then 'get all the curriculum details
                str_Sql = "Select ID,E_Name from(SELECT distinct CLM_ID as ID,CLM_DESCR as E_Name  FROM [CURRICULUM_M])a where  a.id<>''"

            ElseIf SearchMode = "GY" Then 'get all the academic year which is not in the  Grade_BSU_M

                str_Sql = "Select ID,E_Name from(SELECT distinct ACADEMICYEAR_M.ACY_DESCR AS E_Name, ACADEMICYEAR_D.ACD_ID AS ID,ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID " & _
                        " FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID WHERE ACADEMICYEAR_D.ACD_CLM_ID='" & Session("CLM") & "' and  ACADEMICYEAR_D.ACD_BSU_ID ='" & CurBsUnit & "' AND ACADEMICYEAR_D.ACD_ID NOT IN (SELECT DISTINCT GRM_ACD_ID FROM Grade_BSU_M WHERE  GRM_BSU_ID='" & CurBsUnit & "'))a where a.id<>''"

            ElseIf SearchMode = "GB" Then 'to populate only those academic year for which grades are alloacted
                str_Sql = "Select ID,E_Name from(SELECT  distinct GRADE_BSU_M.GRM_ACD_ID as ID, ACADEMICYEAR_M.ACY_DESCR as E_Name, ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID " & _
                       " FROM GRADE_BSU_M INNER JOIN ACADEMICYEAR_D ON GRADE_BSU_M.GRM_ACD_ID = ACADEMICYEAR_D.ACD_ID INNER JOIN " & _
                      " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID where  ACADEMICYEAR_D.ACD_CLM_ID ='" & Session("CLM") & "' and  GRADE_BSU_M.GRM_BSU_ID='" & CurBsUnit & "')a where a.id<>''"

            ElseIf SearchMode = "DT" Then 'Get the Doc Type
                str_Sql = "Select ID,E_Name from(SELECT [DOCTYPE_ID]as ID,[DOCTYPE_DESCR]as E_Name FROM DOCREQD_TYPE_M)a where a.id<>''"

            ElseIf SearchMode = "R" Then
                str_Sql = "Select ID,E_Name from(SELECT  RLG_ID as ID, RLG_DESCR as E_Name FROM  RELIGION_M)a where a.id<>''"
            End If


            Dim ds As New DataSet

            Dim lblID As New Label
            Dim lblName As New Label
            Dim txtSearch As New TextBox
            Dim str_txtCode, str_txtName As String
            Dim str_search As String
            str_txtCode = ""
            str_txtName = ""

            str_filter_code = ""
            str_filter_name = ""

            If gvEmpInfo.Rows.Count > 0 Then

                Dim str_Sid_search() As String




                'str_img = h_selected_menu_1.Value()
                str_Sid_search = h_selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
                str_txtCode = txtSearch.Text
                ''code
                If str_search = "LI" Then
                    str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_code = " AND a.ID LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_code = " AND a.ID NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_code = " AND a.ID LIKE '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_code = " AND a.ID NOT LIKE '%" & txtSearch.Text & "'"
                End If

                ''name
                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
                str_txtName = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_name = " AND a.E_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_name = "  AND  NOT a.E_Name LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_name = " AND a.E_Name  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_name = " AND a.E_Name  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_name = " AND a.E_Name LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_name = " AND a.E_Name NOT LIKE '%" & txtSearch.Text & "'"
                End If

            End If


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_code & str_filter_name & "order by a.E_Name")



            gvEmpInfo.DataSource = ds.Tables(0)
            ' gvEmpInfo.TemplateControl.FindControl("label1"). = ds.Tables(0).Columns("emp_ID")



            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvEmpInfo.DataBind()
                Dim columnCount As Integer = gvEmpInfo.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.


                gvEmpInfo.Rows(0).Cells.Clear()
                gvEmpInfo.Rows(0).Cells.Add(New TableCell)
                gvEmpInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvEmpInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvEmpInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

                'gvEmpInfo.HeaderRow.Visible = True
            Else
                gvEmpInfo.DataBind()


            End If

            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtcode")
            txtSearch.Text = str_txtCode
            txtSearch = gvEmpInfo.HeaderRow.FindControl("txtName")
            txtSearch.Text = str_txtName
            set_Menu_Img()

            'Page.Title = "Employee Info"

        Catch ex As Exception
            ' Errorlog(ex.Message)
        End Try





    End Sub


    Protected Sub btnSearchEmpId_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchEmpName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblcode As New LinkButton

        Dim lbClose As New LinkButton


        lbClose = sender

        lblcode = sender.Parent.FindControl("LinkButton2")

        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "___" & lblcode.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")


            'Response.Write("window.close();")
            'Response.Write("} </script>")
            Dim javaSr As String

            javaSr = "<script language='javascript'> function listen_window(){"
            javaSr += " var oArg = new Object();"
            javaSr += "oArg.Acad ='" & lbClose.Text & "||" & lblcode.Text.Replace("'", "\'") & "' ; "
            javaSr += "var oWnd = GetRadWindow('" & lbClose.Text & "||" & lblcode.Text.Replace("'", "\'") & "');"
            javaSr += "oWnd.close(oArg);"
            javaSr += "} </script>"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)

            h_SelectedId.Value = "Close"
        End If
    End Sub


    Protected Sub gvEmpInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmpInfo.PageIndexChanging
        gvEmpInfo.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lblcode As New LinkButton

        Dim lbClose As New LinkButton


        lbClose = sender.Parent.FindControl("LinkButton1")


        lblcode = sender
        ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
        Dim l_Str_Msg As String = lbClose.Text & "___" & lblcode.Text
        l_Str_Msg = l_Str_Msg.Replace("'", "\'")

        If (Not lblcode Is Nothing) Then
            '   Response.Write(lblcode.Text)
            'Response.Write("<script language='javascript'> function listen_window(){")
            'Response.Write("window.returnValue = '" & l_Str_Msg & "';")


            'Response.Write("window.close();")
            'Response.Write("} </script>")
            Dim javaSr As String

            javaSr = "<script language='javascript'> function listen_window(){"
            javaSr += " var oArg = new Object();"
            javaSr += "oArg.Acad ='" & lbClose.Text & "||" & lblcode.Text.Replace("'", "\'") & "' ; "
            javaSr += "var oWnd = GetRadWindow('" & lbClose.Text & "||" & lblcode.Text.Replace("'", "\'") & "');"
            javaSr += "oWnd.close(oArg);"
            javaSr += "} </script>"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "winId", javaSr)


            h_SelectedId.Value = "Close"
        End If
    End Sub
End Class
