Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studTERM_Master
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub setbutton()
        btnAcademic.Visible = False

    End Sub
    Sub resetbutton()
        btnAcademic.Visible = True

    End Sub
    Sub clearRecords()
        txtAcadYear.Text = ""
        txtTermDesc.Text = ""
        txtTo_date.Text = ""
        txtFrom_date.Text = ""
        ViewState("viewid") = ""
        txtAcadYear.Attributes.Add("readonly", "readonly")
        Session("TermDuration") = Nothing
        Session("termWeeks") = Nothing

        ''gvWeek.DataSource = Session("termWeeks")
        ''gvWeek.DataBind()

        gvDuration.DataSource = Session("TermDuration")
        gvDuration.DataBind()
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ViewState("datamode") = "add"
            Call clearRecords()
            resetbutton()
            Call callActive_year()
            txtTermDesc.Attributes.Remove("readonly")
            txtFrom_date.Attributes.Remove("readonly")
            txtTo_date.Attributes.Remove("readonly")
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog("Term Master", ex.Message)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim retVal As Integer = CheckEditingAllowed(ViewState("viewid"))
        If retVal = 1000 Then
            lblError.Text = "Editing not Allowed"
            Return
        Else
            ViewState("EditOnlyWeeks") = False
            txtTermDesc.Attributes.Remove("readonly")
            txtFrom_date.Attributes.Remove("readonly")
            txtTo_date.Attributes.Remove("readonly")
        End If
        ViewState("datamode") = "edit"
        UtilityObj.beforeLoopingControls(Me.Page)
        resetbutton()

        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    Private Sub DissableControlsForWeekedit(ByVal bDissable As Boolean)
        txtTermDesc.Attributes.Add("ReadOnly", "ReadOnly")
        txtFrom_date.Attributes.Add("ReadOnly", "ReadOnly")
        txtTo_date.Attributes.Add("ReadOnly", "ReadOnly")
        gvDuration.Enabled = Not bDissable
        btnAcademic.Visible = Not bDissable
        txtAcadYear.ReadOnly = bDissable
        CETFromDate.Enabled = Not bDissable
        CETToDate.Enabled = Not bDissable
        CBTEFromDate.Enabled = Not bDissable
        CBETToDate.Enabled = Not bDissable
    End Sub

    ''' <summary>
    ''' Fnction Updates the Session Datatable before it gets Saved
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateDataTable()
        Dim txtFromDate As TextBox
        Dim txtToDate As TextBox
        Dim lblUniqueID As Label
        Dim dtTerm As DataTable = Session("TermDuration")
        For Each grdRow As GridViewRow In gvDuration.Rows
            If Not grdRow.FindControl("txtFromDate") Is Nothing Then
                txtFromDate = grdRow.FindControl("txtFromDate")
                If Not grdRow.FindControl("txtToDate") Is Nothing Then
                    txtToDate = grdRow.FindControl("txtToDate")
                    If Not grdRow.FindControl("lblUniqueID") Is Nothing Then
                        lblUniqueID = grdRow.FindControl("lblUniqueID")
                        UpdateTable(dtTerm, lblUniqueID.Text, txtFromDate.Text, txtToDate.Text)
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub UpdateTable(ByRef dtTerm As DataTable, ByVal UniqueID As String, ByVal frmDT As DateTime, ByVal toDT As DateTime)
        For Each dr As DataRow In dtTerm.Rows
            If dr("UniqueID") = UniqueID Then
                dr("FromDate") = frmDT
                dr("ToDate") = toDT
            End If
        Next
    End Sub

    ''' <summary>
    ''' Fnction Updates the Session Datatable of the Week before it gets Saved
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateWeekDataTable()
        Dim txtFromDate As TextBox
        Dim txtToDate As TextBox
        Dim lblUniqueID As Label
        Dim dtTerm As DataTable = Session("termWeeks")

        ''For Each grdRow As GridViewRow In gvWeek.Rows
        ''    If Not grdRow.FindControl("txtFromDate") Is Nothing Then
        ''        txtFromDate = grdRow.FindControl("txtFromDate")
        ''        If Not grdRow.FindControl("txtToDate") Is Nothing Then
        ''            txtToDate = grdRow.FindControl("txtToDate")
        ''            If Not grdRow.FindControl("lblUniqueID") Is Nothing Then
        ''                lblUniqueID = grdRow.FindControl("lblUniqueID")
        ''                UpdateTable(dtTerm, lblUniqueID.Text, txtFromDate.Text, txtToDate.Text)
        ''            End If
        ''        End If
        ''    End If
        ''Next


    End Sub

    Private Sub UpdateWeekTable(ByRef dtTerm As DataTable, ByVal UniqueID As String, ByVal frmDT As DateTime, ByVal toDT As DateTime)
        For Each dr As DataRow In dtTerm.Rows
            If dr("UniqueID") = UniqueID Then
                dr("FromDate") = frmDT
                dr("ToDate") = toDT
            End If
        Next
    End Sub

    Private Function GetDatesforPeriod(ByVal fromDT As Date, ByVal TODT As Date) As Date()
        Dim count As Integer = DateDiff(DateInterval.Day, fromDT, TODT)
        Dim dtDates(count) As Date
        Dim i As Integer = 0
        While (fromDT <= TODT)
            dtDates(i) = fromDT
            fromDT = fromDT.AddDays(1)
            i += 1
        End While
        Return dtDates
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then
            UpdateDataTable()
            'UpdateWeekDataTable()
           
            Dim TRM_ID As Integer = 0
            Dim TRM_ACD_ID As Integer = hfACD_ID.Value
            Dim TRM_BSU_ID As String = Session("sBsuid")
            Dim TRM_DESCRIPTION As String = txtTermDesc.Text
            Dim TRM_STARTDATE As Date = txtFrom_date.Text
            Dim TRM_ENDDATE As Date = txtTo_date.Text

            Dim vWeeksCount As Integer  ''= IIf(gvWeek.Rows Is Nothing, 0, gvWeek.Rows.Count)

            Dim bEdit As Boolean

            Dim status As Integer
            Dim transaction As SqlTransaction

            If ViewState("datamode") = "add" Then
                bEdit = False

                'get the max Id to be inserted in audittrial report
                Dim sqlTRM_ID As String = "Select isnull(max(TRM_ID),0)+1  from TERM_MASTER"
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlTRM_ID, connection)
                Dim temp As String
                command.CommandType = CommandType.Text
                temp = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()

                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        'call the class to insert the record into TERM_MASTER
                        Dim NEW_TRM_ID As Integer
                        status = SaveTRM_M(TRM_ID, TRM_ACD_ID, TRM_BSU_ID, TRM_DESCRIPTION, TRM_STARTDATE, TRM_ENDDATE, vWeeksCount, bEdit, NEW_TRM_ID, transaction)

                        If status = 708 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        ElseIf status = 709 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        ElseIf status = 782 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        ElseIf status <> 0 Then
                            Throw New ArgumentException("Record could not be Inserted")
                        End If

                        transaction.Commit()
                        lblError.Text = "Record Inserted Successfully"
                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        Call clearRecords()

                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Inserted"

                    End Try
                End Using

            ElseIf ViewState("datamode") = "edit" Then
                bEdit = True
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        'call the class to insert the record into TERM_MASTER
                        TRM_ID = ViewState("viewid")
                        Dim NEW_TRM_ID As Integer = 0

                        Dim bEditOnlyWeeks As Boolean = ViewState("EditOnlyWeeks")
                        If Not bEditOnlyWeeks Then
                            status = SaveTRM_M(TRM_ID, TRM_ACD_ID, TRM_BSU_ID, TRM_DESCRIPTION, TRM_STARTDATE, TRM_ENDDATE, vWeeksCount, bEdit, NEW_TRM_ID, transaction)
                        End If
                        If status <> 0 Then
                            Select Case status
                                Case 708, 709, 782
                                    Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                                Case Else
                                    Throw New ArgumentException("Record could not be Inserted")
                            End Select
                        End If

                        transaction.Commit()
                        lblError.Text = "Record Updated Successfully"
                        ViewState("datamode") = "none"
                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                        Call clearRecords()

                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Updated"

                    End Try
                End Using

            End If

        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearRecords()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"
            setbutton()
            txtFrom_date.Attributes.Add("readonly", "readonly")
            txtTo_date.Attributes.Add("readonly", "readonly")
            txtTermDesc.Attributes.Add("readonly", "readonly")

            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim transaction As SqlTransaction
        Dim Status As Integer
        'Delete  the  user

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")

            Try

                'delete needs to be modified based on the trigger
                'Status = AccessStudentClass.DeleteACADEMICYEAR_D(ViewState("viewid"), transaction)
                If Status = -1 Then
                    Throw New ArgumentException("Record does not exist for deleting")
                ElseIf Status <> 0 Then
                    Throw New ArgumentException("Record could not be Deleted")
                Else
                    ViewState("datamode") = "none"
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    transaction.Commit()
                    Call clearRecords()
                    lblError.Text = "Record Deleted Successfully"
                End If

            Catch myex As ArgumentException
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message)
                transaction.Rollback()

            Catch ex As Exception
                lblError.Text = "Record could not be Deleted"
                UtilityObj.Errorlog(ex.Message)
                transaction.Rollback()
            End Try
        End Using
    End Sub
    Protected Sub txtFrom_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrom_date.TextChanged
        Dim strfDate As String = txtFrom_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtFrom_date.Text = strfDate
        End If
    End Sub

    Protected Sub txtTo_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo_date.TextChanged
        Dim strfDate As String = txtTo_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtTo_date.Text = strfDate
        End If
    End Sub
    Protected Sub cvTodate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTodate.ServerValidate
        Dim sflag As Boolean = False

        Dim DateTime2 As Date
        Dim dateTime1 As Date
        Try
            DateTime2 = Date.ParseExact(txtTo_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            dateTime1 = Date.ParseExact(txtFrom_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date

            If IsDate(DateTime2) Then

                If DateTime2 < dateTime1 Then
                    sflag = False
                Else
                    sflag = True
                End If
            End If
            If sflag Then
                args.IsValid = True
            Else
                'CustomValidator1.Text = ""
                args.IsValid = False
            End If
        Catch
            args.IsValid = False
        End Try
    End Sub

    Protected Sub cvDateFrom_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvDateFrom.ServerValidate
        Dim sflag As Boolean = False
        Dim dateTime1 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            dateTime1 = Date.ParseExact(txtFrom_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(dateTime1) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ViewState("WEEK_COUNT") = 1
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050041") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    If ViewState("datamode") = "view" Then
                        txtAcadYear.Attributes.Add("readonly", "readonly")
                        txtFrom_date.Attributes.Add("readonly", "readonly")
                        txtTo_date.Attributes.Add("readonly", "readonly")
                        txtTermDesc.Attributes.Add("readonly", "readonly")
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        Using readerTerm_M As SqlDataReader = GetDetailsTERM_M(ViewState("viewid"))
                            While readerTerm_M.Read
                                'handle the null value returned from the reader incase  convert.tostring
                                hfTRM_ID.Value = Convert.ToString(readerTerm_M("TRM_ID"))
                                hfACD_ID.Value = Convert.ToString(readerTerm_M("ACD_ID"))
                                txtTermDesc.Text = Convert.ToString(readerTerm_M("TRM_DESCR"))
                                txtAcadYear.Text = Convert.ToString(readerTerm_M("Y_DESCR"))
                                txtFrom_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerTerm_M("STARTDT"))))
                                txtTo_date.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerTerm_M("ENDDT"))))
                            End While
                            setbutton()
                            gvDuration.DataSource = GetDataSetforGrid(Fill_TRM_DETAILS(ViewState("viewid")))
                            gvDuration.DataBind()

                            ''gvWeek.DataSource = GetDataSetWeekforGrid(Fill_TRM_WEEKS(ViewState("viewid")))
                            ''gvWeek.DataBind()

                        End Using
                    ElseIf ViewState("datamode") = "add" Then
                        callActive_year()
                        txtFrom_date.Attributes.Remove("readonly")
                        txtTo_date.Attributes.Remove("readonly")
                        txtTermDesc.Attributes.Remove("readonly")
                    End If
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Sub callActive_year()
        Using readerActive_year As SqlDataReader = GetActive_Year(Session("sBsuid"), Session("CLM"))
            While readerActive_year.Read

                txtAcadYear.Text = Convert.ToString(readerActive_year("Y_DESCR"))
                hfACD_ID.Value = Convert.ToString(readerActive_year("ACD_ID"))
            End While

        End Using
    End Sub

    Protected Function GetDataSetWeekforGrid(ByVal ds As DataSet) As DataView
        'Session("termWeeks") = CreateTermWeekTable()
        'Dim dtTerm As DataTable = Session("termWeeks")
        'If ds Is Nothing OrElse ds.Tables(0).Rows.Count <= 0 Then
        '    Return Nothing
        'End If
        'Dim ldrTempNew As DataRow
        'Dim i As Integer = 1
        'For Each dr As DataRow In ds.Tables(0).Rows
        '    ldrTempNew = dtTerm.NewRow
        '    ldrTempNew("UniqueID") = dr("AWS_ID")
        '    ldrTempNew("WEEK_ID") = i 'dr("AWS_ID")
        '    ldrTempNew("WEEK_DESCR") = i
        '    ldrTempNew("FromDate") = dr("AWS_DTFROM")
        '    ldrTempNew("ToDate") = dr("AWS_DTTO")
        '    ldrTempNew("bDelete") = False
        '    dtTerm.Rows.Add(ldrTempNew)
        '    i += 1
        'Next

        'Dim dv As DataView = New DataView(dtTerm)
        'dv.Sort = "FromDate"
        'Return dv
    End Function

    Protected Function GetDataSetforGrid(ByVal ds As DataSet) As DataView
        Session("TermDuration") = CreateTermDurationTable()
        Dim dtTerm As DataTable = Session("TermDuration")
        If ds Is Nothing OrElse ds.Tables(0).Rows.Count <= 0 Then
            Return Nothing
        End If
        Dim ldrTempNew As DataRow
        For Each dr As DataRow In ds.Tables(0).Rows
            ldrTempNew = dtTerm.NewRow
            ldrTempNew("UniqueID") = dr("AMS_ID")
            ldrTempNew("ORD_ID") = dr("AMS_ORDER")
            ldrTempNew("Month_ID") = dr("AMS_MONTH")
            ldrTempNew("MONTH_DESCR") = GetMonth(dr("AMS_MONTH"))
            ldrTempNew("FromDate") = dr("AMS_DTFROM")
            ldrTempNew("ToDate") = dr("AMS_DTTO")
            dtTerm.Rows.Add(ldrTempNew)
        Next

        Dim dv As DataView = New DataView(dtTerm)
        dv.Sort = "FromDate"
        Return dv
    End Function

    Protected Sub lnkAddTerm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTerm.Click
        FillTermMonths()
        ViewState("bDeleteAllTerms_Months") = True
    End Sub

    Private Sub FillTermMonths()
        Session("TermDuration") = CreateTermDurationTable()
        Dim dtTerm As DataTable = Session("TermDuration")
        Dim frmdt As DateTime = txtFrom_date.Text
        Dim todt As DateTime = txtTo_date.Text
        Dim tempdt As DateTime = txtTo_date.Text
        Dim ldrTempNew As DataRow
        While (frmdt < todt)
            ldrTempNew = dtTerm.NewRow
            ldrTempNew("UniqueID") = dtTerm.Rows.Count + 1
            ldrTempNew("ORD_ID") = dtTerm.Rows.Count + 1
            ldrTempNew("Month_ID") = frmdt.Month
            ldrTempNew("MONTH_DESCR") = GetMonth(frmdt.Month)
            ldrTempNew("FromDate") = frmdt
            ldrTempNew("ToDate") = frmdt.AddDays(DateTime.DaysInMonth(frmdt.Year, frmdt.Month) - frmdt.Day)
            ldrTempNew("bDelete") = False
            ldrTempNew("bEdit") = False
            If frmdt.AddMonths(1) > todt Then
                ldrTempNew("ToDate") = todt
            End If
            frmdt = ldrTempNew("ToDate").AddDays(1)
            dtTerm.Rows.Add(ldrTempNew)
        End While

        Dim dv As DataView = New DataView(dtTerm)
        dv.Sort = "FromDate"
        gvDuration.DataSource = dv
        gvDuration.DataBind()
    End Sub

    Private Function GetMonth(ByVal month_id As Integer) As String
        Select Case month_id
            Case 1
                Return "January"
            Case 2
                Return "February"
            Case 3
                Return "March"
            Case 4
                Return "April"
            Case 5
                Return "May"
            Case 6
                Return "June"
            Case 7
                Return "July"
            Case 8
                Return "August"
            Case 9
                Return "September"
            Case 10
                Return "October"
            Case 11
                Return "November"
            Case 12
                Return "December"
        End Select
        Return ""
    End Function

    Private Function CheckForDuplicatePeriod(ByVal FromDate As DateTime, ByVal ToDate As DateTime, ByVal dttab As DataTable) As Boolean
        Dim dtFromDate As DateTime = FromDate
        Dim dtToDate As DateTime = ToDate
        Dim dtFromDateChk As DateTime
        Dim dtToDateChk As DateTime

        For Each dr As DataRow In dttab.Rows
            dtFromDateChk = dr("FromDate")
            dtToDateChk = dr("ToDate")
            If dtFromDate > dtFromDateChk AndAlso dtFromDate <= dtToDateChk Then
                Return False
            ElseIf dtFromDate < dtFromDateChk AndAlso dtFromDate > dtToDateChk Then
                Return False
                'ElseIf dtToDate <= dtToDateChk Then
                '    Return False
            End If
        Next
        Return True
    End Function

    Private Function CreateTermDurationTable() As DataTable
        Dim dt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cOrderID As New DataColumn("ORD_ID", System.Type.GetType("System.Int32"))
            Dim cMonthID As New DataColumn("MONTH_ID", System.Type.GetType("System.Int32"))
            Dim cMonthDesc As New DataColumn("MONTH_DESCR", System.Type.GetType("System.String"))
            Dim cFromDate As New DataColumn("FromDate", System.Type.GetType("System.DateTime"))
            Dim cToDate As New DataColumn("ToDate", System.Type.GetType("System.DateTime"))
            Dim cbEdit As New DataColumn("bEdit", System.Type.GetType("System.Boolean"))
            Dim cbDelete As New DataColumn("bDelete", System.Type.GetType("System.Boolean"))
            cUniqueID.Unique = True

            dt.Columns.Add(cUniqueID)
            dt.Columns.Add(cOrderID)
            dt.Columns.Add(cMonthID)
            dt.Columns.Add(cMonthDesc)
            dt.Columns.Add(cFromDate)
            dt.Columns.Add(cToDate)
            dt.Columns.Add(cbEdit)
            dt.Columns.Add(cbDelete)

            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
        Return Nothing
    End Function

    Private Function CreateTermWeekTable() As DataTable
        Dim dt As New DataTable
        Try
            Dim cUniqueID As New DataColumn("UniqueID", System.Type.GetType("System.String"))
            Dim cWEEKID As New DataColumn("WEEK_ID", System.Type.GetType("System.Int32"))
            Dim cWEEKDesc As New DataColumn("WEEK_DESCR", System.Type.GetType("System.String"))
            Dim cFromDate As New DataColumn("FromDate", System.Type.GetType("System.DateTime"))
            Dim cToDate As New DataColumn("ToDate", System.Type.GetType("System.DateTime"))
            Dim cbEdit As New DataColumn("bEdit", System.Type.GetType("System.Boolean"))
            Dim cbDelete As New DataColumn("bDelete", System.Type.GetType("System.Boolean"))
            cUniqueID.Unique = True
            dt.Columns.Add(cUniqueID)
            dt.Columns.Add(cWEEKID)
            dt.Columns.Add(cWEEKDesc)
            dt.Columns.Add(cFromDate)
            dt.Columns.Add(cToDate)
            dt.Columns.Add(cbEdit)
            dt.Columns.Add(cbDelete)

            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
        Return Nothing
    End Function

    Protected Sub lnkbtnDurationDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblUniqueID As New Label
        lblUniqueID = TryCast(sender.FindControl("lblUniqueID"), Label)
        If Not lblUniqueID Is Nothing Then
            If Not Session("TermDuration") Is Nothing Then
                Dim dtTermDuration As DataTable = Session("TermDuration")
                Dim i As Integer
                For i = 0 To dtTermDuration.Rows.Count - 1
                    If dtTermDuration.Rows(i)(0) = lblUniqueID.Text Then
                        dtTermDuration.Rows.RemoveAt(i)
                        Exit For
                    End If
                Next
                Session("TermDuration") = dtTermDuration
                Dim dv As DataView = New DataView(dtTermDuration)
                dv.Sort = "FromDate"
                dv.Sort = " bDelete = False "
                gvDuration.DataSource = dv
                gvDuration.DataBind()
            End If
        End If
    End Sub

    Protected Sub gvDuration_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
      
    End Sub

    Protected Sub lnkAddWeek_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AddDeleteWeeks(sender, True)
    End Sub

    Private Sub AddDeleteWeeks(ByVal sender As Object, ByVal bAdd As Boolean)
        UpdateWeekDataTable()
        Dim dttab As DataTable = Session("TermWeeks")
        Dim grRow As GridViewRow = sender.parent.parent
        Dim lblUniqueID As Label
        Dim rowStartID As Integer
        If Not grRow Is Nothing Then
            If Not grRow.FindControl("lbluniqueID") Is Nothing Then
                lblUniqueID = grRow.FindControl("lbluniqueID")
                rowStartID = 0
                Dim dr As DataRow
                For Each drow As DataRow In dttab.Rows
                    If drow("UniqueID") = lblUniqueID.Text Then
                        If IIf(drow("bDelete") Is Nothing, False, drow("bDelete")) = False Then
                            dr = drow
                            Exit For
                        End If
                    End If
                    rowStartID += 1
                Next
                If dr Is Nothing Then Return
                ' Dim incrementer As Integer = 1
                If bAdd Then
                    Dim newRow As DataRow = dttab.NewRow()
                    newRow("UniqueID") = GetNextNo(dttab)
                    newRow("WEEK_ID") = dr("WEEK_ID") + 1
                    newRow("WEEK_DESCR") = dr("WEEK_ID") + 1
                    newRow("FromDate") = Format(dr("FromDate"), OASISConstants.DateFormat)
                    newRow("ToDate") = Format(dr("ToDate"), OASISConstants.DateFormat)
                    newRow("bDelete") = False
                    dttab.Rows.InsertAt(newRow, rowStartID)
                    'incrementer = 1
                Else
                    dr("bDelete") = True
                    'dttab.Rows.RemoveAt(rowStartID - 1)
                    'incrementer = -1
                    'rowStartID -= 2
                End If
            End If
            Session("TermWeeks") = dttab
            Dim dv As DataView = New DataView(dttab)
            dv.Sort = "FromDate"
            dv.RowFilter = " bDelete = False "
        End If
    End Sub

    Private Function GetNextNo(ByVal dttab As DataTable) As Integer
        Dim dv As DataView = New DataView(dttab)
        dv.Sort = "UniqueID"
        Dim maxID As Integer = 0
        For Each dr As DataRow In dttab.Rows
            If dr("UniqueID") > maxID Then
                maxID = dr("UniqueID")
            End If
        Next
        Return maxID + 1
    End Function

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AddDeleteWeeks(sender, False)
    End Sub

    Public Shared Function GetActive_Year(ByVal Bsu_id As String, ByVal CLM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --21/JAN/2008
        'Purpose--Get active year from  ACADEMICYEAR_D
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetActive_Year As String = ""

        sqlGetActive_Year = "SELECT   ACADEMICYEAR_M.ACY_ID as ACY_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR , ACADEMICYEAR_D.ACD_ID as ACD_ID " & _
" FROM  ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
 " where ACADEMICYEAR_D.ACD_BSU_ID='" & Bsu_id & "'  and ACADEMICYEAR_D.ACD_CLM_ID='" & CLM_ID & "' and ACADEMICYEAR_D.ACD_CURRENT=1 "


        '" Select top 1  ACY_ID,Y_DESCR from(SELECT ACADEMICYEAR_D.ACD_ACY_ID as ACY_ID, ACADEMICYEAR_D.ACD_BSU_ID as BSU_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR,ACADEMICYEAR_D.ACD_Current as ACD_Current, " & _
        '                            " ACADEMICYEAR_D.ACD_CLM_ID as CLM_ID FROM ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID)a where a.BSU_ID='" & Bsu_id & "'  and a.CLM_ID=1 and a.ACD_Current=1 order by a.Y_DESCR desc"



        Dim command As SqlCommand = New SqlCommand(sqlGetActive_Year, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function SaveTRM_M(ByVal TRM_ID As Integer, ByVal TRM_ACD_ID As Integer, ByVal TRM_BSU_ID As String, ByVal TRM_DESCRIPTION As String, _
             ByVal TRM_STARTDATE As Date, ByVal TRM_ENDDATE As Date, ByVal weeksCount As Integer, ByVal bEdit As Boolean, ByRef NEW_TRM_ID As Integer, ByVal trans As SqlTransaction) As Integer

        'Author(--Lijo)
        'Date   --07/JAN/2008
        'Purpose--To save TERM_MASTER data based on the datamode (studTERM_MASTER.aspx)
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TRM_ID", TRM_ID)
            pParms(1) = New SqlClient.SqlParameter("@TRM_ACD_ID", TRM_ACD_ID)
            pParms(2) = New SqlClient.SqlParameter("@TRM_BSU_ID", TRM_BSU_ID)
            pParms(3) = New SqlClient.SqlParameter("@TRM_DESCRIPTION", TRM_DESCRIPTION)
            pParms(4) = New SqlClient.SqlParameter("@TRM_STARTDATE", TRM_STARTDATE)
            pParms(5) = New SqlClient.SqlParameter("@TRM_ENDDATE", TRM_ENDDATE)
            pParms(6) = New SqlClient.SqlParameter("@bEdit", bEdit)
            pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            pParms(8) = New SqlClient.SqlParameter("@NewTRM_ID", NEW_TRM_ID)
            pParms(8).Direction = ParameterDirection.Output
            pParms(9) = New SqlClient.SqlParameter("@TRM_WEEKS", weeksCount)
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveSTUDTERM_MASTER", pParms)
            Dim ReturnFlag As Integer = pParms(7).Value
            If Not pParms(8).Value Is DBNull.Value Then
                NEW_TRM_ID = pParms(8).Value
            End If
            If NEW_TRM_ID = 0 Then NEW_TRM_ID = TRM_ID
            Return ReturnFlag
        End Using

    End Function
    Public Shared Function GetTerm_CheckDate(ByVal ACD_ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String, ByVal TRM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --08/jun/2008--modified
        'Purpose--Get the terms for that BSU which is currently open based on ACD_ID
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTerm_CheckDate As String = ""

        sqlGetTerm_CheckDate = " select Trm_startDate,trm_endDate FROM TERM_MASTER " & _
 " WHERE TRM_ID='" & TRM_ID & "' and TRM_ACD_ID=(SELECT  ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_BSU_ID = '" & BSU_ID & "') AND " & _
" (ACD_ACY_ID = '" & ACD_ACY_ID & "') AND (ACD_CLM_ID = '" & CLM_ID & "'))"

        Dim command As SqlCommand = New SqlCommand(sqlGetTerm_CheckDate, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function GetTerm_4ACD(ByVal ACD_ACY_ID As String, ByVal CLM_ID As String, ByVal BSU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --08/jun/2008--modified
        'Purpose--Get the terms for that BSU which is currently open based on ACD_ID
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTerm_4ACD As String = ""

        sqlGetTerm_4ACD = " select TRM_ID,TRM_DESCRIPTION+'('+replace(convert(varchar(11), Trm_startDate, 106), ' ', '/')+' To '+replace(convert(varchar(11), trm_endDate, 106), ' ', '/')+')' as TDescr,Trm_startDate,trm_endDate FROM TERM_MASTER " & _
 " WHERE TRM_ACD_ID=(SELECT  ACD_ID FROM ACADEMICYEAR_D WHERE (ACD_BSU_ID = '" & BSU_ID & "') AND " & _
" (ACD_ACY_ID = '" & ACD_ACY_ID & "') AND (ACD_CLM_ID = '" & CLM_ID & "'))"

        '       "sqlGetTerm_4ACD = "SELECT distinct TERM_MASTER.TRM_DESCRIPTION as TDescr, TERM_MASTER.TRM_ID as TRM_ID FROM ACADEMICYEAR_D INNER JOIN   GRADE_BSU_M ON ACADEMICYEAR_D.ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN " & _
        '" TERM_MASTER ON ACADEMICYEAR_D.ACD_ID = TERM_MASTER.TRM_ACD_ID WHERE (ACADEMICYEAR_D.ACD_CURRENT=1) and (GRADE_BSU_M.GRM_OPENONLINE = 1) AND   (ACADEMICYEAR_D.ACD_OPENONLINE= 1) AND (GRADE_BSU_M.GRM_GRD_ID = '" & Grades & "') AND  (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "')"


        ' "SELECT TRM_ID , TRM_DESCRIPTION as TDescr FROM TERM_MASTER where TRM_ACD_ID='" & ACD_ID & "' and  TRM_BSU_ID='" & BSU_ID & "' order by TDescr"
        Dim command As SqlCommand = New SqlCommand(sqlGetTerm_4ACD, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return reader
    End Function

    Public Shared Function GetDetailsTERM_M(ByVal TRM_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --19/JAN/2008
        'Purpose--Get Grade data from GRADE_BSU_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetTERM_M As String = ""

        sqlGetTERM_M = " Select TRM_ID,ACD_ID,Y_DESCR,TRM_DESCR,STARTDT,ENDDT from(SELECT TERM_MASTER.TRM_DESCRIPTION AS TRM_DESCR, TERM_MASTER.TRM_STARTDATE AS STARTDT, TERM_MASTER.TRM_ENDDATE AS ENDDT, " & _
                      " TERM_MASTER.TRM_BSU_ID AS BSU_ID, ACADEMICYEAR_M.ACY_DESCR AS Y_DESCR, TERM_MASTER.TRM_ACD_ID AS ACD_ID, TERM_MASTER.TRM_ID as TRM_ID " & _
                    " FROM  ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
                     " TERM_MASTER ON ACADEMICYEAR_D.ACD_ID = TERM_MASTER.TRM_ACD_ID)a where a.TRM_ID='" & TRM_ID & "'"


        Dim command As SqlCommand = New SqlCommand(sqlGetTERM_M, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function CheckEditingAllowed(ByVal TRM_ID As String) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@TRMID", TRM_ID)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "CanDeleteTErm", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            If ReturnFlag = 800 Then
                Return 1
            ElseIf ReturnFlag <> 0 Then
                Return 1000
            Else
                Return 0
            End If
        End Using
    End Function

    Public Shared Function Fill_TRM_DETAILS(ByVal TRM_ID As Integer) As DataSet
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sqlTRM_DETAILS As String = ""

            sqlTRM_DETAILS = "SELECT AMS_ID , AMS_ACD_ID, AMS_MONTH, " & _
            "AMS_TRM_ID, AMS_ORDER, AMS_DTFROM, AMS_DTTO " & _
            "FROM ACADEMIC_MonthS_S WHERE AMS_TRM_ID=" & TRM_ID & _
            " ORDER BY AMS_ORDER"
            Dim command As SqlCommand = New SqlCommand(sqlTRM_DETAILS, connection)
            command.CommandType = CommandType.Text
            Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.Text, sqlTRM_DETAILS)
            Return ds
        End Using
    End Function

End Class


