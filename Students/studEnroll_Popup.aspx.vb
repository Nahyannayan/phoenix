﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studEnroll_Popup
    Inherits System.Web.UI.Page
    Sub GetTransfer()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT Top 1 '' as TFR_CODE,'' as TFR_DESCR From TFRTYPE_M WHERE 1=1 UNION ALL SELECT TFR_CODE, TFR_DESCR FROM TFRTYPE_M order by TFR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlTranType.DataSource = ds
        ddlTranType.DataValueField = "TFR_CODE"
        ddlTranType.DataTextField = "TFR_DESCR"
        ddlTranType.DataBind()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then
            Dim EQS_Id As String = Request.QueryString("id")
            HF_EQS_ID.Value = EQS_Id
            BindDetails()
            GetTransfer()
            bindSiblings()
            bindTWINS()
        End If
    End Sub

    Sub BindDetails()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            If HF_EQS_ID.Value <> "" Then
                Dim param(10) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", HF_EQS_ID.Value)
                Using ENQ_Reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ENQ_GETDETAILS", param)
                    If ENQ_Reader.HasRows = True Then
                        While ENQ_Reader.Read()
                            txtComments.Text = Convert.ToString(ENQ_Reader("EQS_COMMENTS"))
                            ddlTranType.SelectedValue = Convert.ToString(ENQ_Reader("EQS_TFRTYPE"))
                            rbSEN.SelectedValue = Convert.ToString(ENQ_Reader("EQS_bSEN"))

                        End While
                    End If
                End Using
            End If
        Catch ex As Exception
            lblError.Text = "Error Occured...!!!!"
        End Try
    End Sub
    Protected Sub btnSaveComments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveComments.Click
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            If HF_EQS_ID.Value <> "" Then
                Dim param(10) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", HF_EQS_ID.Value)
                param(1) = New SqlClient.SqlParameter("@EQS_COMMENTS", txtComments.Text)
                param(2) = New SqlClient.SqlParameter("@EQS_TFRTYPE", DBNull.Value) ''ddlTranType.SelectedValue
                param(3) = New SqlClient.SqlParameter("@EQS_bSEN", rbSEN.SelectedValue)
                param(4) = New SqlClient.SqlParameter("@Return_msg", SqlDbType.VarChar, 400)
                param(4).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "ENQ_SAVECOMMENTS", param)
                lblError.Text = "Comments Saved sucessfully...!!!!"

            End If
        Catch ex As Exception
            lblError.Text = "Insertion Failed...!!!!"
        End Try
    End Sub

    Protected Sub btnClearcomments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearcomments.Click

    End Sub

    Protected Sub btnSaveSib_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSib.Click
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            If HF_SIBL_ID.Value <> "" And HF_EQS_ID.Value <> "" Then
                Dim EQS_SIB_TYPE As String
                If RBSiblingEnq.Checked = True Then
                    EQS_SIB_TYPE = RBSiblingEnq.Text 'HF_SIBLING_TYPE.Value
                Else
                    EQS_SIB_TYPE = RBSiblingStu.Text
                End If
                Dim param(10) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", HF_EQS_ID.Value)
                param(1) = New SqlClient.SqlParameter("@SIBLING_TYPE", EQS_SIB_TYPE)
                param(2) = New SqlClient.SqlParameter("@SIBLING_ID", HF_SIBL_ID.Value)
                param(3) = New SqlClient.SqlParameter("@Return_msg", SqlDbType.VarChar, 400)
                param(3).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "ENQ_SAVE_SIBLING", param)
                lblErrorsib.Text = "Sibling  Saved sucessfully...!"
                bindSiblings()
            Else
                lblErrorsib.Text = "Please pick a student for adding sibling....!"
            End If
        Catch ex As Exception
            lblErrorsib.Text = "Insertion Failed...!!!!"
        End Try
    End Sub

    Protected Sub btnClearSib_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSib.Click
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim EQS_SIB_TYPE As String
            If RBSiblingEnq.Checked = True Then
                EQS_SIB_TYPE = RBSiblingEnq.Text 'HF_SIBLING_TYPE.Value
            Else
                EQS_SIB_TYPE = RBSiblingStu.Text
            End If


            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", HF_EQS_ID.Value)
            param(1) = New SqlClient.SqlParameter("@SIBLING_TYPE", EQS_SIB_TYPE)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "ENQ_REMOVE_SIBLING", param)
            lblErrorsib.Text = "Sibling  Removed Successfully...!"
            bindSiblings()
        Catch ex As Exception
            lblErrorsib.Text = "Insertion Failed...!!!!"
        End Try
    End Sub
    Sub bindSiblings()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As DataSet
        Dim strSQL As String = ""
        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", HF_EQS_ID.Value)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENQUIRY_SHOWSIBLING", param)
        gvSibling.DataSource = ds.Tables(0)
        gvSibling.DataBind()

    End Sub
    Sub bindTWINS()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim ds As DataSet
        Dim strSQL As String = ""
        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", HF_EQS_ID.Value)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "ENQUIRY_SHOWTWINS", param)
        gvTwins.DataSource = ds.Tables(0)
        gvTwins.DataBind()

    End Sub
    Protected Sub btnSaveTwin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveTwin.Click
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            If HF_Twins_ID.Value <> "" And HF_EQS_ID.Value <> "" Then
                Dim EQS_TWINS_TYPE As String
                If RBTwinsEnq.Checked = True Then
                    EQS_TWINS_TYPE = RBTwinsEnq.Text
                Else
                    EQS_TWINS_TYPE = RBTwinsStu.Text
                End If
                Dim param(10) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", HF_EQS_ID.Value)
                param(1) = New SqlClient.SqlParameter("@TWINS_TYPE", EQS_TWINS_TYPE)
                param(2) = New SqlClient.SqlParameter("@TWINS_ID", HF_Twins_ID.Value)

                param(3) = New SqlClient.SqlParameter("@Return_msg", SqlDbType.VarChar, 400)
                param(3).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "ENQ_SAVE_TWINS", param)
                lblErrorTwin.Text = "TWINS  Saved sucessfully...!"
                bindTWINS()
            Else
                lblErrorsib.Text = "Please pick a student for add as TWINS....!"
            End If
        Catch ex As Exception
            lblErrorTwin.Text = "Insertion Failed...!!!!"
        End Try
    End Sub

    Protected Sub RBSiblingStu_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBSiblingStu.CheckedChanged
        HF_SIBL_ID.Value = ""
        txtSibno.Text = ""
    End Sub

    Protected Sub RBSiblingEnq_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBSiblingEnq.CheckedChanged
        HF_SIBL_ID.Value = ""
        txtSibno.Text = ""
    End Sub

    Protected Sub btnClearTwin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearTwin.Click
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim EQS_SIB_TYPE As String
            If RBSiblingEnq.Checked = True Then
                EQS_SIB_TYPE = RBSiblingEnq.Text 'HF_SIBLING_TYPE.Value
            Else
                EQS_SIB_TYPE = RBSiblingStu.Text
            End If


            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EQS_EQM_ENQID", HF_EQS_ID.Value)
            param(1) = New SqlClient.SqlParameter("@SIBLING_TYPE", EQS_SIB_TYPE)
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "ENQ_REMOVE_TWINS", param)
            lblErrorTwin.Text = "Twins  Removed Successfully...!"
            bindTWINS()
        Catch ex As Exception
            lblErrorsib.Text = "Insertion Failed...!!!!"
        End Try
    End Sub
End Class
