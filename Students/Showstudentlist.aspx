<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Showstudentlist.aspx.vb" Inherits="Showstudentlist" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Student Info</title>
    <base target="_self" />
    <%--  <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>

    <script language="javascript" type="text/javascript">

        function test(val) {
            // alert(val);
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid0()%>").src = path;
            document.getElementById("<%=h_selected_menu_0.ClientID %>").value = val + '__' + path;
        }

        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
            document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
        }

        function test2(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid2()%>").src = path;
            document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
        }
        function test3(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid3()%>").src = path;
                      document.getElementById("<%=h_selected_menu_3.ClientID %>").value = val + '__' + path;
                  }
                  function SetValuetoParent(stuid) {
                      //alert(stuid);
                      //alert(window.parent.document.p);
                      parent.setStudValue(stuid);
                      return false;
                  }
    </script>
</head>
<body onload="listen_window();">
    <form id="form1" runat="server">

        <div>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>

                    <td align="center">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="title-bg">Student Details</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center">
                                                <asp:GridView ID="gvSiblingInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                    CssClass="table table-bordered table-row" Width="100%" CaptionAlign="Top" PageSize="15">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Grade">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblgrade" runat="server" Text="Grade"></asp:Label><br />
                                                                <asp:DropDownList ID="ddlgrade" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlgrade_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblgrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Section">
                                                            <HeaderTemplate>

                                                                <asp:Label ID="lblSection" runat="server" Text="Section"></asp:Label><br />
                                                                <asp:DropDownList ID="ddlsection" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlsection_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                &nbsp;<asp:Label ID="lblsection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Student ID" Visible="False">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Stud_ID") %>' Width="21px"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStudID" runat="server" Text='<%# Bind("SIBLING_ID") %>'></asp:Label>
                                                                <asp:Label ID="lblSibling" runat="server" Text='<%# Bind("SIBLING_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Fee ID">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Width="129px"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <HeaderTemplate>

                                                                <asp:Label ID="lblFeeIDH" runat="server" EnableViewState="False"
                                                                    Text="Fee ID"></asp:Label><br />
                                                                <asp:TextBox ID="txtFeeId" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnSearchFeeID" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchFeeID_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFeeID" runat="server" Text='<%# Bind("FeeID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Student No" Visible="False">
                                                            <EditItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("FeeID") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                &nbsp;
                                                                <asp:Label ID="lblStudNo" runat="server" Text='<%# Bind("StudNo") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Student Name" ShowHeader="False">
                                                            <HeaderTemplate>

                                                                <asp:Label ID="lblStudNameH" runat="server" Text="Student Name"></asp:Label><br />
                                                                <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnSearchStudName" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchStudName_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lblStudName" runat="server" CausesValidation="False" CommandName="Selected"
                                                                    Text='<%# Eval("StudName") %>'></asp:LinkButton>  <%--OnClick="lblStudName_Click"--%>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="School Name" Visible="False">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <HeaderTemplate>

                                                                <asp:Label ID="lblSchoolNameH" runat="server" Text="School Name"></asp:Label><br />
                                                                <asp:TextBox ID="txtSchoolName" runat="server"></asp:TextBox>
                                                                <asp:ImageButton ID="btnSearchSchoolName" runat="server" ImageAlign="Middle" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchSchoolName_Click" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSchoolName" runat="server" Text='<%# Bind("SchoolName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle  />
                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    <RowStyle CssClass="griditem"  />
                                                </asp:GridView>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" ></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" >
                        <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                        <input id="h_Selected_menu_0" runat="server" type="hidden" value="=" />
                        <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" /></td>
                </tr>
            </table>


        </div>

    </form>
</body>
</html>
