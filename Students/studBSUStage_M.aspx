<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studBSUStage_M.aspx.vb" Inherits="Students_studBSUStage_M" EnableEventValidation="false" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">

function moveOptionUp() 
{
var obj=document.getElementById("<%=lstRequired.ClientId%>")
	if (!hasOptions(obj)) { return; }
	for (i=0; i<obj.options.length; i++) {
		if (obj.options[i].selected) {
			if (i != 0 && !obj.options[i-1].selected) {
				swapOptions(obj,i,i-1);
				obj.options[i-1].selected = true;
				}
			}
		}
persistReqOptionsList();	
}
function hasOptions(obj) {
	if (obj!=null && obj.options!=null) { return true; }
	return false;
	}


function moveOptionDown() {
var obj=document.getElementById("<%=lstRequired.ClientId%>")
	if (!hasOptions(obj)) { return; }
	for (i=obj.options.length-1; i>=0; i--) {
		if (obj.options[i].selected) {
			if (i != (obj.options.length-1) && ! obj.options[i+1].selected) {
				swapOptions(obj,i,i+1);
				obj.options[i+1].selected = true;
				}
			}
		}
	persistReqOptionsList();
}


function removeSelectedOptions()
 { 
var from=document.getElementById("<%=lstRequired.ClientId%>")

	if (!hasOptions(from)) { return; }
	if (from.type=="select-one") {
		from.options[from.selectedIndex] = null;
		}
	else {
		for (var i=(from.options.length-1); i>=0; i--) { 
			var o=from.options[i]; 
			if (o.selected) { 
				from.options[i] = null; 
				} 
			}
		}
	from.selectedIndex = -1; 
	persistReqOptionsList();
	} 


function RemoveOption()
{

}
function swapOptions(obj,i,j) {
	var o = obj.options;
	var i_selected = o[i].selected;
	var j_selected = o[j].selected;
	var temp = new Option(o[i].text, o[i].value, o[i].defaultSelected, o[i].selected);
	var temp2= new Option(o[j].text, o[j].value, o[j].defaultSelected, o[j].selected);
	o[i] = temp2;
	o[j] = temp;
	o[i].selected = j_selected;
	o[j].selected = i_selected;
		}
		
		
	//to retain values in the trips listbox after postback the list items are stored in an input box	
function persistReqOptionsList()
{
 var listBox1 = document.getElementById("<%=lstRequired.ClientId%>");
 var optionsList = '';
 if (listBox1.options.length>0)
 {
 for (var i=0; i<listBox1.options.length; i++)
 {
  var optionText = listBox1.options[i].text;
  var optionValue = listBox1.options[i].value;
 
  if ( optionsList.length > 0 )
   optionsList += '|';
   optionsList += optionText+"$"+optionValue;
 }
}
 document.getElementById("<%=lstValuesReq.ClientId%>").value = optionsList;
 return false;
 }
 
 
 function persistNotReqOptionsList()
{
 var listBox1 = document.getElementById("<%=lstNotRequired.ClientId%>");
 var optionsList = '';
 if (listBox1.options.length>0)
 {
 for (var i=0; i<listBox1.options.length; i++)
 {
  var optionText = listBox1.options[i].text;
  var optionValue = listBox1.options[i].value;
 
  if ( optionsList.length > 0 )
   optionsList += '|';
   optionsList += optionText+"$"+optionValue;
 }
}
 document.getElementById("<%=lstValuesNotReq.ClientId%>").value = optionsList;
 return false;
 }
 
 function moveSelectedLeftToRight()
 {
 var from=document.getElementById("<%=lstNotRequired.ClientId%>");
 var to=document.getElementById("<%=lstRequired.ClientId%>");
 if (!hasOptions(from)) { return; }
	for (var i=0; i<from.options.length; i++) {
		var o = from.options[i];
		if (o.selected) {
			if (!hasOptions(to)) { var index = 0; } else { var index=to.options.length; }
			to.options[index] = new Option( o.text, o.value, false, false);
			}
		}
			// Delete them from original
	for (var i=(from.options.length-1); i>=0; i--) {
		var o = from.options[i];
		if (o.selected) {
			from.options[i] = null;
			}
		}
	persistReqOptionsList();
	persistNotReqOptionsList();
 }
 function moveSelectedRighttoLeft()
 {
 var to=document.getElementById("<%=lstNotRequired.ClientId%>");
 var from=document.getElementById("<%=lstRequired.ClientId%>");
 if (!hasOptions(from)) { return; }
	for (var i=0; i<from.options.length; i++) {
		var o = from.options[i];
		if (o.selected) {
			if (!hasOptions(to)) { var index = 0; } else { var index=to.options.length; }
			to.options[index] = new Option( o.text, o.value, false, false);
			}
		}
			// Delete them from original
	for (var i=(from.options.length-1); i>=0; i--) {
		var o = from.options[i];
		if (o.selected) {
			from.options[i] = null;
			}
		}
	persistReqOptionsList();
	persistNotReqOptionsList();
 }
 

</script>


<style>
    table td select {
        width:100% !important;
    }
</style>

    &nbsp; &nbsp;

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Business Unit Stage Setup
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


                <table id="tbl_ShowScreen" runat="server" align="center" border="0" bordercolor="#1b80b6"
                    cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" >
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <table id="Table1" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0" >
                              
                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblAccText" runat="server" CssClass ="field-label"  Text="Select Academic Year" ></asp:Label></td>
                                
                                    <td align="left"   width="20%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr >
                                    <td align="left"  valign="middle" colspan="4" class="title-bg">
                                        Details</td>
                                </tr>
                                <tr>
                                    <td align="center"  colspan="4">

                                        <table id="Table4" runat="server" align="center" border="0" width="100%"
                                            cellpadding="12" cellspacing="0" >

                                            <tr>
                                                <td align="center" width="40%">
                                                    <table id="Table3" runat="server" width="100%" >
                                                        <tr>
                                                            <td align="center" ><span class="field-label">Stages Not Required</span> </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:ListBox ID="lstNotRequired" runat="server"  SelectionMode="Multiple" Style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid" Font-Size="X-Small"></asp:ListBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="10%" align="center">
                                                    <table id="Table5" runat="server" width="100%" >
                                                        <tr>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding-top: 30%;">
                                                                <input id="btnRight" type="button" value=">>" onclick="moveSelectedLeftToRight()" class="button"  /><br />
                                                                
                                                                <input id="btnLeft" type="button" value="<<" onclick="moveSelectedRighttoLeft()" class="button" /><br />
                                                            </td>
                                                        </tr>
                                                        </table>                                                    
                                                </td>
                                                <td>
                                                    <table id="Table2" runat="server" width="100%" >
                                                        <tr >
                                                            <td align="center" ><span class="field-label">Stages Required</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:ListBox ID="lstRequired" runat="server"  SelectionMode="Multiple" Style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid" Font-Size="X-Small"></asp:ListBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left" width="10%" >

                                                    <table id="Table6" runat="server" width="100%" >
                                                        <tr >
                                                           <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding-top: 30%;">
                                                                <input id="btnUp" type="button" value="Up" onclick="moveOptionUp()" class="button" /><br />
                                                              
                                                                <input id="btnDown" type="button" value="Down" onclick="moveOptionDown()" class="button" /><br />                                                               
                                                            </td> 
                                                                
                                                        </tr>
                                                    </table>
                                                   
                                                    &nbsp;

                                                </td>

                                            </tr>

                                        </table>

                                    </td>
                                </tr>

                                <tr>
                                    <td  valign="bottom" align="center" colspan="4">
                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" CausesValidation="False" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                            Text="Cancel" UseSubmitBehavior="False" Visible="False" />
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;
               <input id="lstValuesReq" name="lstValues" runat="server" type="hidden" style="left: 274px; width: 74px; position: absolute; top: 161px; height: 10px" />
                            <input id="lstValuesNotReq" name="lstValuesNotReq" runat="server" type="hidden" style="left: 274px; width: 74px; position: absolute; top: 161px; height: 10px" />
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
           
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div> 
</asp:Content>

