<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudUploadPhoto.aspx.vb" Inherits="Students_StudUploadPhoto" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../cssfiles/chromejs/chrome.js"></script>

    <script language="javascript" type="text/javascript">
        function UploadPhoto() {
            //     var photopath= document.getElementById('<%=FUUploadStuPhoto.ClientID %>').value;
            //     alert(photopath);
            //     document.getElementById('<%=imgstuImage.ClientID %>').src=photopath;    
            //      document.getElementById('<%=h_EmpImagePath.ClientID %>').value = photopath;
            //            document.forms[0].submit(); 
        }
        function test(val) {
            // alert(val);
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid0()%>").src = path;
            document.getElementById("<%=h_selected_menu_0.ClientID %>").value = val + '__' + path;
        }

        function test1(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid1()%>").src = path;
            document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
        }

        function test2(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid2()%>").src = path;
            document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
        }
        function test3(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid3()%>").src = path;
                      document.getElementById("<%=h_selected_menu_3.ClientID %>").value = val + '__' + path;
                  }
                  function test4(val) {
                      var path;
                      if (val == 'LI') {
                          path = '../Images/operations/like.gif';
                      } else if (val == 'NLI') {
                          path = '../Images/operations/notlike.gif';
                      } else if (val == 'SW') {
                          path = '../Images/operations/startswith.gif';
                      } else if (val == 'NSW') {
                          path = '../Images/operations/notstartwith.gif';
                      } else if (val == 'EW') {
                          path = '../Images/operations/endswith.gif';
                      } else if (val == 'NEW') {
                          path = '../Images/operations/notendswith.gif';
                      }
                      document.getElementById("<%=getid4()%>").src = path;
         document.getElementById("<%=h_selected_menu_2.ClientID %>").value = val + '__' + path;
     }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server">Upload Student photo</asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div>
                    <table width="100%">
                        <tr class="title">
                            <td align="left" colspan="4">
                                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                        </tr>
                        <tr class="matters">
                            <td align="left" width="20%"><span class="field-label">Select Academic year</span> </td>
                            <td width="30%">
                                <asp:DropDownList ID="ddlAca_Year" runat="server" AutoPostBack="True" CssClass="listbox">
                                </asp:DropDownList></td>
                            <td width="20%"></td>
                            <td width="30%"></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table width="100%">
                                    <tr>
                                        <td align="center" class="matters" colspan="4" valign="top">
                                            <asp:GridView ID="gvSiblingInfo" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="tsble table-bordered table-row"
                                                Width="100%" CaptionAlign="Top" PageSize="20">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Fee ID">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblFeeIDH" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                Text="Fee ID"></asp:Label><br />
                                                            <asp:TextBox ID="txtFeeId" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchFeeID" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchFeeID_Click" />

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFeeID" runat="server" Text='<%# Bind("FeeID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Student No">
                                                        <EditItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("FeeID") %>'></asp:Label>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            &nbsp;
                                                                <asp:Label ID="lblStudNo" runat="server" Text='<%# Bind("StudNo") %>'></asp:Label>
                                                            <asp:HiddenField ID="HF_STU_ID" runat="server" Value='<%# BIND("STU_ID") %>' />
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblStudNoH" runat="server" CssClass="gridheader_text" EnableViewState="False"
                                                                Text="Student No"></asp:Label><br />
                                                            <asp:TextBox ID="txtStudNo" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchStudNo" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchStudNo_Click" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Student Name" ShowHeader="False">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblStudNameH" runat="server" CssClass="gridheader_text" Text="Student Name"></asp:Label><br />
                                                            <asp:TextBox ID="txtStudName" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchStudName" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchStudName_Click" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lblStudName" runat="server" CausesValidation="False" CommandName="Selected"
                                                                OnClick="lblStudName_Click" Text='<%# Eval("StudName") %>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Grade">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Stud_ID") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("GRADE") %>'></asp:Label>&nbsp;
 
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblGrade" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label><br />
                                                            <asp:TextBox ID="txtGrd_search" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchGrade" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchGrade_Click" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Section">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblSectionH" runat="server" CssClass="gridheader_text" Text="Section"></asp:Label><br />
                                                            <asp:TextBox ID="txtSectionName" runat="server"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchSection" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnSearchSection_Click" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Photo Available">
                                                        <ItemTemplate>
                                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Bind("PHOTOAvailable") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Upload Photo">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkbtnUpload" runat="server" OnClick="lnkbtnUpload_Click">Upload</asp:LinkButton>
                                                            <asp:HiddenField ID="HF_Photopath" runat="server" Value='<%# Bind("STU_PHOTOPATH") %>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader_pop" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                                <RowStyle CssClass="griditem" />
                                            </asp:GridView>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="matters" colspan="4"
                                valign="middle">
                                <input id="h_SelectedId" runat="server" type="hidden" value="0" />
                                <input id="h_Selected_menu_0" runat="server" type="hidden" value="=" />
                                <input id="h_selected_menu_1" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                                <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                                <ajaxToolkit:ModalPopupExtender ID="ModalpopupUpload" runat="server" BackgroundCssClass="modalBackground" DropShadow="True" PopupControlID="PanelTitleAdd" TargetControlID="lblmodalpopup">
                                </ajaxToolkit:ModalPopupExtender>
                                <asp:Label ID="lblmodalpopup" runat="server" ForeColor="White"></asp:Label>
                                <asp:Panel
                                    ID="PanelTitleAdd" runat="server" CssClass="panel-cover"
                                    Style="display: none">
                                    <table width="100%">
                                        <tr class="matters">
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <asp:Image ID="imgstuImage" runat="server" Height="150px" Width="150px" /></td>
                                        </tr>
                                        <tr class="matters">
                                            <td>Upload Photo</td>
                                            <td>
                                                <asp:FileUpload ID="FUUploadStuPhoto" runat="server" />&nbsp;

                                               


                                            </td>
                                        </tr>
                                        <tr class="matters">
                                            <td></td>
                                            <td></td>
                                            <td align="left">
                                                <asp:Button ID="btnSave" runat="server" Text="Upload" CssClass="button" /><asp:Button
                                                    ID="btnClose" runat="server" CssClass="button" Text="Close" /></td>
                                        </tr>
                                    </table>
                                    <asp:HiddenField ID="HF_stu_id" runat="server" />
                                    <asp:HiddenField ID="h_EmpImagePath" runat="server" />
                                </asp:Panel>

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

