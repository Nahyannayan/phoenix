<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnquiryTransferReq_View.aspx.vb" Inherits="Students_studEnquiryTransferReq_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        input {
           vertical-align :middle !important; 
        }
    </style>
    <script language="javascript" type="text/javascript">


        f
        function confirm_reject() {

            if (confirm("You are about to offer reject to this enquiry.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>


     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Transfer Request Enquiry</div>

 <div class="card-body">
            <div class="table-responsive m-auto">
    <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" width="100%">

       <%-- <tr >

            <td width="50%" align="left" class="title" style="height: 50px">&nbsp;&nbsp;
                    </td>
        </tr>--%>

        <tr>
            <td align="left"   valign="top">
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                <table align="center" border="0"  cellpadding="0" cellspacing="0" width="100%">

                  <%--  <tr>
                        <td align="left"></td>
                        <td 43% style="height: 40px; width: 91px;"></td>
                        <td align="left" 43% style="height: 40px; width: 795px;">&nbsp;&nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="100%" valign="top">
                            <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                HeaderStyle-Height="30" PageSize="20" >
                                <Columns>
                                    <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("Eqs_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="EQS_GRD_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblgrdId" runat="server" Text='<%# Bind("Eqs_grd_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="-" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEqsAcyId" runat="server" Text='<%# Bind("Eqs_ACY_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Enq No.">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblEnqHeader" runat="server" CssClass="gridheader_text" Text="Enq No."></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqSearch" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnqid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnqid_Search_Click" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEnqId"  runat="server" Text='<%# Bind("eqs_applno") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Applicant Name">

                                        <HeaderTemplate>
                                             <asp:Label ID="lblApplName" runat="server" CssClass="gridheader_text" Text="Applicant Name"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtApplName" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnAppl_Name" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnAppl_Name_Click" />
                                        </HeaderTemplate>

                                        <ItemTemplate>
                                            <asp:Label ID="lblApplName" runat="server" Text='<%# Bind("appl_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Business Unit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbsuname" runat="server"  Text='<%# Bind("bsu_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:ButtonField CommandName="view" HeaderText="Accept" Text="Accept">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:ButtonField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>Reject</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton Text="Reject" ID="lbReject" runat="server" OnClientClick="return confirm_reject();" CommandName="edit" CommandArgument='<%# Container.DataItemIndex %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <HeaderStyle  CssClass="gridheader_pop" />
                                <RowStyle CssClass="griditem"  />
                                <SelectedRowStyle CssClass="Green" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                    runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                        type="hidden" value="=" /></td>
        </tr>


    </table>
</div></div></div>

</asp:Content>

