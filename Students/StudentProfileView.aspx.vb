﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Imports InfosoftGlobal
Imports System.Collections.Generic
Imports System.Collections
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports UtilityObj
Partial Class Students_StudentProfileView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = ""
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Request.QueryString("ID") Is Nothing Then
            '' Dim STU_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
            ViewState("STUID") = Request.QueryString("Id").Replace(" ", "+")
        End If
        If Not Request.QueryString("stutype") Is Nothing Then
            '' Dim STU_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
            ViewState("STUTYPE") = Request.QueryString("stutype").Replace(" ", "+")
        End If
        If Not Request.QueryString("ID") Is Nothing Then
            '' Dim STU_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
            ViewState("mode") = Request.QueryString("mode").Replace(" ", "+")
        End If
        If Page.IsPostBack = False Then
            If Not ViewState("STUID") Is Nothing AndAlso Not ViewState("mode") Is Nothing Then
                getStudentGradeBsu()
                If (ViewState("mode") = "STUDENTPROFILE") Then
                    getColor_House()
                    bindstudentdetails(ViewState("STUID"))
                    trStuProfile.Visible = True
                    trParentDetails.Visible = False
                    tblAttendance.Visible = False
                    tblAttendanceChart.Visible = False
                    tblFeeDetails.Visible = False
                    tblCurriculum.Visible = False
                    tblBehaviour.Visible = False
                    tblCurriculum2.Visible = False
                    tblMed.Visible = False
                    tbllibrary1.Visible = False
                    tbllibrary2.Visible = False
                    tbllibrary3.Visible = False
                    tbllibrary4.Visible = False
                    tblcommunication1.Visible = False
                    tblcommunication2.Visible = False
                    tblPer.Visible = False
                End If
                If (ViewState("mode") = "PARENTPROFILE") Then
                    binddetails_parent(ViewState("STUID"))

                    trStuProfile.Visible = False
                    trParentDetails.Visible = True
                    tblAttendance.Visible = False
                    tblAttendanceChart.Visible = False
                    tblFeeDetails.Visible = False
                    tblCurriculum.Visible = False
                    tblCurriculum2.Visible = False
                    tblBehaviour.Visible = False
                    tblMed.Visible = False
                    tbllibrary1.Visible = False
                    tbllibrary2.Visible = False
                    tbllibrary3.Visible = False
                    tbllibrary4.Visible = False
                    tblcommunication1.Visible = False
                    tblcommunication2.Visible = False
                    tblPer.Visible = False
                End If
                If (ViewState("mode") = "STUDENTATTENDANCE") Then
                    GetStu_AcdYear()
                    bindAttendance_details(ViewState("STUID"))
                    bindAttChart(ViewState("STUID"))
                    bindABSChart(ViewState("STUID"))
                    bindLATEChart(ViewState("STUID"))

                    trStuProfile.Visible = False
                    trParentDetails.Visible = False
                    tblAttendance.Visible = True
                    tblAttendanceChart.Visible = True
                    tblFeeDetails.Visible = False
                    tblCurriculum.Visible = False
                    tblCurriculum2.Visible = False
                    tblBehaviour.Visible = False
                    tblMed.Visible = False
                    tbllibrary1.Visible = False
                    tbllibrary2.Visible = False
                    tbllibrary3.Visible = False
                    tbllibrary4.Visible = False
                    tblcommunication1.Visible = False
                    tblcommunication2.Visible = False
                    tblPer.Visible = False
                End If
                If (ViewState("mode") = "STUDENTFEES") Then

                    GridBindPayHist()

                    trStuProfile.Visible = False
                    trParentDetails.Visible = False
                    tblAttendance.Visible = False
                    tblAttendanceChart.Visible = False
                    tblFeeDetails.Visible = True
                    tblCurriculum.Visible = False
                    tblCurriculum2.Visible = False
                    tblBehaviour.Visible = False
                    tblMed.Visible = False
                    tbllibrary1.Visible = False
                    tbllibrary2.Visible = False
                    tbllibrary3.Visible = False
                    tbllibrary4.Visible = False
                    tblcommunication1.Visible = False
                    tblcommunication2.Visible = False
                    tblPer.Visible = False
                End If
                If (ViewState("mode") = "CURRICULUM") Then

                    BindSubjectList()
                    BindProgressReport()
                    HF_stuid.Value = ViewState("STUID")
                    hfACD_ID.Value = Session("Current_ACD_ID")
                    checkCBSESchool()
                    getStudentGrade()

                    If hfCBSESchool.Value = "1" And hfGRD_ID.Value = "11" Or hfGRD_ID.Value = "12" Then
                        tblPer.Visible = False
                    Else
                        BindReports()
                    End If
                    trStuProfile.Visible = False
                    trParentDetails.Visible = False
                    tblAttendance.Visible = False
                    tblAttendanceChart.Visible = False
                    tblFeeDetails.Visible = False
                    tblBehaviour.Visible = False
                    tblCurriculum.Visible = True
                    tblCurriculum2.Visible = True
                    tblPer.Visible = True
                    tblMed.Visible = False
                    tbllibrary1.Visible = False
                    tbllibrary2.Visible = False
                    tbllibrary3.Visible = False
                    tbllibrary4.Visible = False
                    tblcommunication1.Visible = False
                    tblcommunication2.Visible = False
                End If
                If (ViewState("mode") = "BEHAVIOUR") Then
                    'BindGrid_POS(ViewState("STUID"))
                    'BindGrid_neg(ViewState("STUID"))
                    If Not Page.IsPostBack Then                       
                        gridbind_Achievement(ViewState("STUID"))
                        gridbind_Merit(ViewState("STUID"))
                        bindcategory()
                        gridbindBehavior(ViewState("STUID"))
                        ShowMessage("", False)
                    End If
                    trStuProfile.Visible = False
                    trParentDetails.Visible = False
                    tblAttendance.Visible = False
                    tblAttendanceChart.Visible = False
                    tblFeeDetails.Visible = False
                    tblCurriculum.Visible = False
                    tblCurriculum2.Visible = False
                    tblBehaviour.Visible = True
                    tblMed.Visible = False
                    tbllibrary1.Visible = False
                    tbllibrary2.Visible = False
                    tbllibrary3.Visible = False
                    tbllibrary4.Visible = False
                    tblcommunication1.Visible = False
                    tblcommunication2.Visible = False
                    tblPer.Visible = False
                End If
                If (ViewState("mode") = "MEDICAL") Then

                    binddetails_med(ViewState("STUID"))
                    trStuProfile.Visible = False
                    trParentDetails.Visible = False
                    tblAttendance.Visible = False
                    tblAttendanceChart.Visible = False
                    tblFeeDetails.Visible = False
                    tblCurriculum.Visible = False
                    tblCurriculum2.Visible = False
                    tblMed.Visible = True
                    tbllibrary1.Visible = False
                    tbllibrary2.Visible = False
                    tbllibrary3.Visible = False
                    tbllibrary4.Visible = False
                    tblcommunication1.Visible = False
                    tblcommunication2.Visible = False
                    tblPer.Visible = False
                End If
                If (ViewState("mode") = "LIBRARY") Then

                    GridBindLibrary1()
                    GridBindLibrary2()
                    GridBindLibrary3()
                    GridBindLibrary4()
                    trStuProfile.Visible = False
                    trParentDetails.Visible = False
                    tblAttendance.Visible = False
                    tblAttendanceChart.Visible = False
                    tblFeeDetails.Visible = False
                    tblCurriculum.Visible = False
                    tblCurriculum2.Visible = False
                    tblMed.Visible = False
                    tbllibrary1.Visible = True
                    tbllibrary2.Visible = True
                    tbllibrary3.Visible = True
                    tbllibrary4.Visible = True
                    tblcommunication1.Visible = False
                    tblcommunication2.Visible = False
                    tblPer.Visible = False
                End If

                If (ViewState("mode") = "COMMUNICATION") Then

                    BindGridSMS()
                    BindGridEmail()
                    trStuProfile.Visible = False
                    trParentDetails.Visible = False
                    tblAttendance.Visible = False
                    tblAttendanceChart.Visible = False
                    tblFeeDetails.Visible = False
                    tblCurriculum.Visible = False
                    tblCurriculum2.Visible = False
                    tblMed.Visible = False
                    tbllibrary1.Visible = False
                    tbllibrary2.Visible = False
                    tbllibrary3.Visible = False
                    tbllibrary4.Visible = False
                    tblcommunication1.Visible = True
                    tblcommunication2.Visible = True
                    tblPer.Visible = False
                End If
            End If

        End If
    End Sub

    Protected Sub lnkPrinted_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            'If Session("sbsuid") = "" Or Session("sbsuid") Is Nothing Then
            '    Panel1.Visible = False
            '    lblSession.Text = "Your session has expired!!!.Please login again"
            '    Exit Sub
            'End If

            Dim lblRpfId As New Label
            Dim lblRsmId As New Label
            Dim url As String
            Dim viewid As String
            lblRpfId = TryCast(sender.FindControl("lblRpfId"), Label)
            lblRsmId = TryCast(sender.FindControl("lblRsmId"), Label)

            ''commented and added by nahyan on 21nov2018
            '' ViewState("MainMnu_code") = "StudentProfile"
            ViewState("MainMnu_code") = "PdfReport"
            Dim str As String = "~/Curriculum/Reports/Aspx/rptMonthlyProgressReportStudWise.aspx?" _
                              & "MainMnu_code=" + Encr_decrData.Encrypt(ViewState("MainMnu_code")) _
                              & "&datamode=" + Encr_decrData.Encrypt("view") _
                              & "&rsmid=" + Encr_decrData.Encrypt(lblRsmId.Text) _
                              & "&rpfid=" + Encr_decrData.Encrypt(lblRpfId.Text) _
                              & "&acdid=" + Encr_decrData.Encrypt(ViewState("acd_id")) _
                              & "&stuid=" + Encr_decrData.Encrypt(ViewState("STUID")) _
                              & "&grdid=" + Encr_decrData.Encrypt(ViewState("GRD_ID"))
            Response.Redirect(str)
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
        End Try
    End Sub

    Sub bindstudentdetails(ByVal stu_id As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "student_Details", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        'handle the null value returned from the reader incase  convert.tostring

                        LTpob.Text = Convert.ToString(readerStudent_Detail("STU_POB"))
                        LTcunbirth.Text = Convert.ToString(readerStudent_Detail("COB"))
                        txtJoin_Shift.Text = Convert.ToString(readerStudent_Detail("SHF_DESCR_JOIN"))
                        txtJoin_Stream.Text = Convert.ToString(readerStudent_Detail("STM_DESCR_JOIN"))
                        txtACD_ID_Join.Text = Convert.ToString(readerStudent_Detail("ACD_ID_JOIN_Y"))
                        txtGRD_ID_Join.Text = Convert.ToString(readerStudent_Detail("GRD_ID_JOIN"))
                        txtSCT_ID_JOIN.Text = Convert.ToString(readerStudent_Detail("SCT_DESCR_JOIN"))
                        txtFee_ID.Text = Convert.ToString(readerStudent_Detail("FEE_ID"))
                        txtGEMS_USI.Text = Convert.ToString(readerStudent_Detail("GEMS_USI")) 'Added by Nikunj (08-March-2020)
                        txtMOE_No.Text = Convert.ToString(readerStudent_Detail("BLUEID"))
                        txtFee_Spon.Text = Convert.ToString(readerStudent_Detail("SFEESPONSOR"))
                        txtPNo.Text = Convert.ToString(readerStudent_Detail("SPASPRTNO"))
                        txtPIssPlace.Text = Convert.ToString(readerStudent_Detail("SPASPRTISSPLACE"))
                        txtVNo.Text = Convert.ToString(readerStudent_Detail("STU_VISANO"))
                        txtVIssPlace.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSPLACE"))
                        txtVIssAuth.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSAUTH"))
                        TLtftype.Text = Convert.ToString(readerStudent_Detail("TFR_DESCR"))
                        LTreligion.Text = Convert.ToString(readerStudent_Detail("RLG_ID"))
                        LTnationality.Text = Convert.ToString(readerStudent_Detail("SNATIONALITY"))
                        ltminlist.Text = Convert.ToString(readerStudent_Detail("MINLIST"))
                        LTminlistType.Text = Convert.ToString(readerStudent_Detail("SMINLISTTYPE"))
                        txtFee_Spon.Text = Convert.ToString(readerStudent_Detail("SFEESPONSOR"))
                        LTemgcon.Text = Convert.ToString(readerStudent_Detail("SEMGCONTACT"))
                        lit_Other_Info.Text = Convert.ToString(readerStudent_Detail("FAMILY_NOTE"))

                        ViewState("temp_COB") = Convert.ToString(readerStudent_Detail("COB"))
                        ViewState("temp_HOUSE") = Convert.ToString(readerStudent_Detail("HOUSE"))
                        ViewState("temp_PrefContact") = Convert.ToString(readerStudent_Detail("STU_PREFCONTACT"))
                        ViewState("temp_Blood") = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        ViewState("ACD_ID") = Convert.ToString(readerStudent_Detail("ACD_ID"))
                        'Setting date
                        If IsDate(readerStudent_Detail("SDOJ")) = True Then
                            txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("SDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("MINDOJ")) = True Then
                            txtMINDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("MINDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("DOB")) = True Then
                            txtDob.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("DOB"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_PASPRTISSDATE")) = True Then
                            txtPIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTISSDATE"))))
                        End If
                        If IsDate(readerStudent_Detail("STU_PASPRTEXPDATE")) = True Then
                            txtPExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTEXPDATE"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_VISAISSDATE")) = True Then
                            txtVIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAISSDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_VISAEXPDATE")) = True Then
                            txtVExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAEXPDATE")))).Replace("01/Jan/1900", "")
                        End If

                        ltEmiratesID.Text = Convert.ToString(readerStudent_Detail("STU_EMIRATES_ID"))
                        ltPremisesId.Text = Convert.ToString(readerStudent_Detail("stu_PremisesID"))


                        Dim temp_Gender As String
                        temp_Gender = Convert.ToString(readerStudent_Detail("GENDER"))
                        If UCase(temp_Gender) = "F" Then
                            LTgender.Text = "Female"
                        ElseIf UCase(temp_Gender) = "M" Then
                            LTgender.Text = "Male"
                        End If
                    End While
                Else
                End If
            End Using
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim STR As String = "exec [dbo].[GETSTUDENT_M_DETAILS] " + stu_id + "," + Session("sBsuid") + ""



            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.Text, STR)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        Dim temp_Mail As Boolean = Convert.ToBoolean(readerStudent_Detail("STU_bRCVMAIL"))
                        If temp_Mail Then
                            ltRecevEmail.Text = "Yes"
                        Else
                            ltRecevEmail.Text = "No"
                        End If
                        Dim temp_SMS As Boolean = Convert.ToBoolean(readerStudent_Detail("STU_bRCVSMS"))
                        If temp_SMS Then
                            ltRecevSMS.Text = "Yes"
                        Else
                            ltRecevSMS.Text = "No"
                        End If
                        If Convert.ToBoolean(readerStudent_Detail("STU_bRCVPUBL")) = True Then
                            ltPublicpromotion.Text = "Yes"
                        Else
                            ltPublicpromotion.Text = "No"
                        End If
                        ltcommentstud.Text = Convert.ToString(readerStudent_Detail("STU_COMMENT"))
                        If Not ddlHouse_BSU.Items.FindByValue(Convert.ToString(readerStudent_Detail("HOUSE"))) Is Nothing Then
                            ddlHouse_BSU.ClearSelection()
                            ddlHouse_BSU.Items.FindByValue(Convert.ToString(readerStudent_Detail("HOUSE"))).Selected = True
                        End If
                    End While
                End If
            End Using


        Catch ex As Exception

        End Try
    End Sub
    Sub getColor_House()

        Try
            Using AllHouse_reader As SqlDataReader = AccessStudentClass.GetBSU_House(Session("sBsuid"))
                Dim di_House As ListItem
                ddlHouse_BSU.Items.Clear()
                di_House = New ListItem("", "")
                ddlHouse_BSU.Items.Add(di_House)
                If AllHouse_reader.HasRows = True Then
                    While AllHouse_reader.Read()
                        di_House = New ListItem(AllHouse_reader("ID"), AllHouse_reader("HideID"))
                        ddlHouse_BSU.Items.Add(di_House)
                    End While

                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetHOUSEBSU_info")
        End Try

    End Sub
    Sub binddetails_parent(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim ds As DataSet
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "stu_contactDetails", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        Ltl_Fname.Text = Convert.ToString(readerStudent_Detail("STS_FNAME"))
                        Ltl_Fnationality.Text = Convert.ToString(readerStudent_Detail("FNATIONALITY"))
                        Ltl_Fpob.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPOBOX"))
                        Ltl_FEmirate.Text = Convert.ToString(readerStudent_Detail("STS_FEMIR"))
                        Ltl_FPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_FRESPHONE"))
                        Ltl_FOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_FOFFPHONE"))
                        Ltl_FMobile.Text = Convert.ToString(readerStudent_Detail("STS_FMOBILE"))
                        Ltl_FEmail.Text = Convert.ToString(readerStudent_Detail("STS_FEMAIL"))
                        Ltl_FFax.Text = Convert.ToString(readerStudent_Detail("STS_FFAX"))
                        Ltl_FOccupation.Text = Convert.ToString(readerStudent_Detail("STS_FOCC"))
                        Ltl_FCompany.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPANY"))

                        Ltl_Mname.Text = Convert.ToString(readerStudent_Detail("STS_MNAME"))
                        Ltl_Mnationality.Text = Convert.ToString(readerStudent_Detail("MNATIONALITY"))
                        Ltl_Mpob.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPOBOX"))
                        Ltl_MEmirate.Text = Convert.ToString(readerStudent_Detail("STS_MEMIR"))
                        Ltl_MPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_MRESPHONE"))
                        Ltl_MOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_MOFFPHONE"))
                        Ltl_MMobile.Text = Convert.ToString(readerStudent_Detail("STS_MMOBILE"))
                        Ltl_MEmail.Text = Convert.ToString(readerStudent_Detail("STS_MEMAIL"))
                        Ltl_MFax.Text = Convert.ToString(readerStudent_Detail("STS_MFAX"))
                        Ltl_MOccupation.Text = Convert.ToString(readerStudent_Detail("STS_MOCC"))
                        Ltl_MCompany.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPANY"))

                        Ltl_Gname.Text = Convert.ToString(readerStudent_Detail("STS_GNAME"))
                        Ltl_Gnationality.Text = Convert.ToString(readerStudent_Detail("GNATIONALITY"))
                        Ltl_Gpob.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPOBOX"))
                        Ltl_GEmirate.Text = Convert.ToString(readerStudent_Detail("STS_GEMIR"))
                        Ltl_GPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_GRESPHONE"))
                        Ltl_GOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_GOFFPHONE"))
                        Ltl_GMobile.Text = Convert.ToString(readerStudent_Detail("STS_GMOBILE"))
                        Ltl_GEmail.Text = Convert.ToString(readerStudent_Detail("STS_GEMAIL"))
                        Ltl_GFax.Text = Convert.ToString(readerStudent_Detail("STS_GFAX"))
                        Ltl_GOccupation.Text = Convert.ToString(readerStudent_Detail("STS_GOCC"))
                        Ltl_GCompany.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPANY"))
                        Dim col As Integer
                        If readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "F" Then
                            col = 2
                        ElseIf readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "M" Then
                            col = 3

                        ElseIf readerStudent_Detail("STU_PRIMARYCONTACT").ToString = "G" Then
                            col = 4
                        Else
                            col = 2
                        End If
                    

                    End While
                Else
                    ' lblError.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            '  lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub
    Private Sub GetStu_AcdYear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))
        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "ATT.GETSTU_ACDYEAR", param)

            If readerStudent_Detail.HasRows = True Then
                ddlAttAcd_id.DataSource = readerStudent_Detail
                ddlAttAcd_id.DataTextField = "ACY_DESCR"
                ddlAttAcd_id.DataValueField = "ACD_ID"
                ddlAttAcd_id.DataBind()

                If Not ddlAttAcd_id.Items.FindByValue(Session("Current_ACD_ID")) Is Nothing Then

                    ddlAttAcd_id.ClearSelection()
                    ddlAttAcd_id.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
                End If
            Else
                ddlAttAcd_id.Items.Clear()

            End If

            ' ddlAttAcd_id_SelectedIndexChanged(ddlAttAcd_id, Nothing)
        End Using
    End Sub
    Sub bindAttendance_details(ByVal stu_id As String)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            param(1) = New SqlClient.SqlParameter("@Tacd_id", ddlAttAcd_id.SelectedValue)

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStud_dashBoard_Att", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        ltAcdWorkingDays.Text = Convert.ToString(readerStudent_Detail("tot_acddays"))
                        ltTotWorkTilldate.Text = readerStudent_Detail("tot_wrkdays").ToString
                        ltAttMarkTilldate.Text = readerStudent_Detail("tot_marked").ToString
                        ltDayAbsent.Text = readerStudent_Detail("tot_abs").ToString
                        ltDayPresent.Text = readerStudent_Detail("tot_att").ToString
                        ltDayLeave.Text = readerStudent_Detail("tot_leave").ToString
                        ltTitleAcd.Text = "Total working days for the academic year " + readerStudent_Detail("acd_year").ToString
                        ltTotTilldate.Text = "Total working days till " + todayDT
                        ltMrkTilldate.Text = "Total Attendance marked till " + todayDT
                    End While
                Else
                    ' lblError.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            'lblError.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub

    Sub bindAttChart(ByVal stu_id As String)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            param(1) = New SqlClient.SqlParameter("@Tacd_id", ddlAttAcd_id.SelectedValue)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_Att_Patten", param)
            radAttendanceChart.DataSource = ds.Tables(0)
            radAttendanceChart.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Sub bindABSChart(ByVal stu_id As String)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            param(1) = New SqlClient.SqlParameter("@Tacd_id", ddlAttAcd_id.SelectedValue)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_ABS_Patten", param)
            radAttendanceABSChart.DataSource = ds.Tables(0)
            radAttendanceABSChart.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Sub bindLATEChart(ByVal stu_id As String)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            param(1) = New SqlClient.SqlParameter("@Tacd_id", ddlAttAcd_id.SelectedValue)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_LATE_Patten", param)
            radAttendanceLateChart.DataSource = ds.Tables(0)
            radAttendanceLateChart.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GridBindPayHist()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Dim prevdate = DateTime.Now.AddDays(-365).ToString("dd/MMM/yyyy")
            Dim currdate = DateTime.Now.ToString("dd/MMM/yyyy")

            Using objConn
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                pParms(1) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))
                pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", ViewState("STUTYPE"))
                pParms(3) = New SqlClient.SqlParameter("@FROMDT", prevdate)
                pParms(4) = New SqlClient.SqlParameter("@TODT", currdate)


                'ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "FEES.Dashboard_PaymentHistory", pParms)
                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[FEES].[FEES_GEN_RPT_FEE_STUDENT_LEDGER]", pParms)
                gvPaymentHist.DataSource = ds
                gvPaymentHist.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Protected Sub gvPaymentHist_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        gvPaymentHist.PageIndex = e.NewPageIndex
        GridBindPayHist()
    End Sub

    Sub BindSubjectList()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))


                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "Get_StudentSubjectListById ", pParms)
                gvSubjects.DataSource = ds
                gvSubjects.DataBind()

            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Sub BindProgressReport()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))


                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "Get_StudentProgressReportById ", pParms)
                gvProgress.DataSource = ds
                gvProgress.DataBind()

            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Sub getStudentGradeBsu()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_GRD_ID,stu_acd_id,Stu_sct_id,STU_BSU_ID FROM STUDENT_M WHERE STU_ID='" + ViewState("STUID") + "'"
        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read
                    ViewState("GRD_ID") = readerStudent_Detail("STU_GRD_ID").ToString
                    ViewState("acd_id") = readerStudent_Detail("stu_acd_id").ToString
                    ViewState("sct_id") = readerStudent_Detail("Stu_sct_id").ToString
                    ViewState("bsuId") = readerStudent_Detail("STU_BSU_ID").ToString

                End While
            End If

        End Using


    End Sub

    'Private Sub BindGrid_POS(ByVal stu_id As String)
    '    Try
    '        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '        Dim dsDetails As DataSet
    '        Dim PARAM(2) As SqlParameter
    '        PARAM(0) = New SqlParameter("@STU_ID", stu_id)
    '        PARAM(1) = New SqlParameter("@CATEGORY", 2)
    '        dsDetails = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_ACHIEVEMENT_BEHAVIOUR", PARAM)
    '        'Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
    '        '                " BM_INCIDENT_TYPE,BM_CATEGORY_SCORE as BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
    '        '                " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName FROM BM.BM_MASTER A " & _
    '        '                " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
    '        '                " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
    '        '                " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
    '        '                " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=A.BM_CATEGORYID " & _
    '        '                " WHERE isNULL(BM_CONFIDENTIAL,0) =0 AND S.STU_ID =" & stu_id & "  AND BM_CATEGORYHRID=2 ORDER BY BM_ENTRY_DATE DESC"


    '        'Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '        gvStudGrade_POS.DataSource = dsDetails

    '        If dsDetails.Tables(0).Rows.Count = 0 Then
    '            dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
    '            gvStudGrade_POS.DataBind()
    '            Dim columnCount As Integer = gvStudGrade_POS.Rows(0).Cells.Count
    '            gvStudGrade_POS.Rows(0).Cells.Clear()
    '            gvStudGrade_POS.Rows(0).Cells.Add(New TableCell)
    '            gvStudGrade_POS.Rows(0).Cells(0).ColumnSpan = columnCount
    '            gvStudGrade_POS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
    '            gvStudGrade_POS.Rows(0).Cells(0).Text = "No record available."

    '        Else
    '            gvStudGrade_POS.DataBind()
    '            'For Each GrdvRow As GridViewRow In gvStudGrade_POS.Rows
    '            '    If GrdvRow.RowType = DataControlRowType.DataRow Then
    '            '        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
    '            '        'If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
    '            '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
    '            '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
    '            '        '    GrdvRow.ForeColor = Drawing.Color.Blue
    '            '        'Else
    '            '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
    '            '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
    '            '        '    GrdvRow.ForeColor = Drawing.Color.Red
    '            '        'End If
    '            '    End If
    '            'Next
    '        End If

    '    Catch ex As Exception

    '    End Try

    'End Sub

    'Private Sub BindGrid_neg(ByVal stu_id As String)
    '    Try
    '        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    '        Dim dsDetails As DataSet
    '        Dim PARAM(2) As SqlParameter
    '        PARAM(0) = New SqlParameter("@STU_ID", stu_id)
    '        PARAM(1) = New SqlParameter("@CATEGORY", 3)
    '        dsDetails = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_ACHIEVEMENT_BEHAVIOUR", PARAM)


    '        'Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
    '        '                " BM_INCIDENT_TYPE,BM_CATEGORY_SCORE as BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
    '        '                " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName FROM BM.BM_MASTER A " & _
    '        '                " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
    '        '                " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
    '        '                " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
    '        '                " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=A.BM_CATEGORYID " & _
    '        '                " WHERE isNULL(BM_CONFIDENTIAL,0) =0 AND S.STU_ID =" & stu_id & "  AND BM_CATEGORYHRID=3 ORDER BY BM_ENTRY_DATE DESC"


    '        ' Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '        gvStudGrade.DataSource = dsDetails

    '        If dsDetails.Tables(0).Rows.Count = 0 Then
    '            dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
    '            gvStudGrade.DataBind()
    '            Dim columnCount As Integer = gvStudGrade.Rows(0).Cells.Count
    '            gvStudGrade.Rows(0).Cells.Clear()
    '            gvStudGrade.Rows(0).Cells.Add(New TableCell)
    '            gvStudGrade.Rows(0).Cells(0).ColumnSpan = columnCount
    '            gvStudGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
    '            gvStudGrade.Rows(0).Cells(0).Text = "No record available."

    '        Else
    '            gvStudGrade.DataBind()
    '            'For Each GrdvRow As GridViewRow In gvStudGrade.Rows
    '            '    If GrdvRow.RowType = DataControlRowType.DataRow Then
    '            '        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
    '            '        'If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
    '            '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
    '            '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
    '            '        '    GrdvRow.ForeColor = Drawing.Color.Blue
    '            '        'Else
    '            '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
    '            '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
    '            '        '    GrdvRow.ForeColor = Drawing.Color.Red
    '            '        'End If
    '            '    End If
    '            'Next
    '        End If

    '    Catch ex As Exception

    '    End Try

    'End Sub
    Sub binddetails_med(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)

        Dim temp_Health As Boolean
        'Dim temp_HOUSE, temp_Blood, temp_Type, temp_Rlg_ID, temp_Nationality, temp_COB, temp_MINLIST, temp_MINLISTTYPE, temp_PrefContact As String
        Dim arInfo As String() = New String(2) {}
        Dim Temp_Phone_Split As String = String.Empty
        Dim splitter As Char = "-"
        'arInfo = info.Split(splitter)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_HealthInfo_bystudentId", param)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    ltAlg_Detail.Text = ds.Tables(0).Rows(0)("AllergiesNotes").ToString

                End If
            End If
            Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("STUID"), ViewState("bsuId"))
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        'ltHth_No.Text = Convert.ToString(readerStudent_Detail("SHEALTH"))
                        ltHth_No.Text = Convert.ToString(readerStudent_Detail("STU_HCNO"))
                        'ltAlg_Detail.Text = Convert.ToString(readerStudent_Detail("SPHYSICAL"))
                        ltSp_med.Text = Convert.ToString(readerStudent_Detail("STU_SPMEDICATION"))
                        ltB_grp.Text = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        'ltHth_Info.Text = Convert.ToString(readerStudent_Detail("STU_HCNO"))
                        ltHth_Info.Text = Convert.ToString(readerStudent_Detail("SHEALTH"))
                        temp_Health = Convert.ToBoolean(readerStudent_Detail("SbRCVSPMEDICATION"))
                        ltPhy_edu.Text = Convert.ToString(readerStudent_Detail("SPHYSICAL"))
                        If temp_Health Then
                            ltPrev_sch.Text = "YES" + " " + Convert.ToString(readerStudent_Detail("STU_PREVSCHI"))
                        Else
                            ltPrev_sch.Text = "NO"
                        End If
                    End While

                Else
                End If

            End Using
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnPayFee_Click(sender As Object, e As EventArgs)
        Dim usrPAssword As String = String.Empty
        Dim stuId As String = String.Empty
        Dim mnuType As String = String.Empty
        Dim menuCode As String = String.Empty
        Dim usrname As String = String.Empty
        usrPAssword = GetUserInfoByUsername()

        stuId = Encr_decrData.Encrypt(ViewState("STUID"))
        menuCode = Encr_decrData.Encrypt("F300135")
        usrname = Encr_decrData.Encrypt(Session("sUsr_name"))


        Dim stuInfo As String = stuId & "|FEEPAYMENT|" & menuCode & "|" & usrPAssword & "|" & usrname
        Response.Write("<script language='javascript'> function listen_window(){")
        Response.Write(" var oArg = new Object();")
        Response.Write("oArg.Employee ='" & stuInfo & "' ; ")
        Response.Write("var oWnd = GetRadWindow('" & stuInfo & "');")
        Response.Write("oWnd.close(oArg);")
        Response.Write("} </script>")
        h_SelectedId.Value = "Close"
    End Sub


    Private Sub GridBindLibrary1()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
                pParms(1) = New SqlClient.SqlParameter("@USER_ID", ViewState("STUID"))


                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[DBO].[GET_LIBRARY_PROFILE]", pParms)
                GridMemberships.DataSource = ds
                GridMemberships.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Private Sub GridBindLibrary2()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTION", 2)
                pParms(1) = New SqlClient.SqlParameter("@USER_ID", ViewState("STUID"))


                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[DBO].[GET_LIBRARY_PROFILE]", pParms)
                GrdUserTransaction.DataSource = ds
                GrdUserTransaction.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Private Sub GridBindLibrary3()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTION", 3)
                pParms(1) = New SqlClient.SqlParameter("@USER_ID", ViewState("STUID"))


                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[DBO].[GET_LIBRARY_PROFILE]", pParms)
                GridDue.DataSource = ds
                GridDue.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub

    Private Sub GridBindLibrary4()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTION", 4)
                pParms(1) = New SqlClient.SqlParameter("@USER_ID", ViewState("STUID"))


                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[DBO].[GET_LIBRARY_PROFILE]", pParms)
                GridReservations.DataSource = ds
                GridReservations.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub


    Public Sub BindGridSMS()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTION", 1)
                pParms(1) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))


                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[DBO].[GET_COMMUNICATION_PROFILE]", pParms)
                GridMSMS.DataSource = ds
                GridMSMS.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try



    End Sub

    Public Sub BindGridEmail()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTION", 2)
                pParms(1) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))


                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "[DBO].[GET_COMMUNICATION_PROFILE]", pParms)
                GridEmails.DataSource = ds
                GridEmails.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try




    End Sub

    Protected Sub GridMSMS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridMSMS.PageIndex = e.NewPageIndex
        BindGridSMS()
    End Sub


    Protected Sub GridEmails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridEmails.PageIndexChanging
        GridEmails.PageIndex = e.NewPageIndex
        BindGridEmail()
    End Sub
    Protected Sub GrdUserTransaction_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdUserTransaction.PageIndexChanging
        GrdUserTransaction.PageIndex = e.NewPageIndex
        GridBindLibrary2()

    End Sub

    Protected Sub GridDue_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridDue.PageIndexChanging
        GridReservations.PageIndex = e.NewPageIndex
        GridBindLibrary3()
    End Sub

    Protected Sub GridReservations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridReservations.PageIndexChanging
        GridReservations.PageIndex = e.NewPageIndex
        GridBindLibrary4()
    End Sub
    Private Function GetUserInfoByUsername() As String
        Dim usrPassword As String = String.Empty
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@USRNAME", Session("sUsr_name"))

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_User_Password_ById", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        usrPassword = readerStudent_Detail("USR_PASSWORD").ToString
                        Return usrPassword

                    End While
                End If

            End Using
        Catch ex As Exception

        End Try

    End Function
    Sub checkCBSESchool()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM VW_ACADEMICYEAR_D WHERE ACD_CLM_ID=1 AND ACD_ID='" + Session("CURRENT_ACD_ID") + "'"

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count > 0 Then
            hfCBSESchool.Value = "1"
        Else
            hfCBSESchool.Value = "0"
        End If
    End Sub
    Sub BindReports()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


        Dim STR_QUERY As String

        If hfCBSESchool.Value = "1" Then
            STR_QUERY = "EXEC [RPT].[GETREPORTCARDBYYEAR_CBSE] " _
                          & "'" + Session("SBSUID") + "'," _
                          & "'" + hfGRD_ID.Value + "'," _
                          & Session("CLM").ToString
        Else
            STR_QUERY = "EXEC RPT.GETREPORTCARDBYYEAR " _
                           & "'" + Session("SBSUID") + "'," _
                           & "'" + hfGRD_ID.Value + "'," _
                           & Session("CLM").ToString
        End If

        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, STR_QUERY)
        Dim sq As New SqlString
        Dim xmlData As New XmlDocument
        Dim str As String
        While reader.Read
            str += reader.GetString(0)
        End While
        Dim xl As New SqlString
        str = str.Replace("ACY_ID", "ID")
        str = str.Replace("ACY_DESCR", "TEXT")
        str = str.Replace("RPF_ID", "ID")
        str = str.Replace("RPF_DESCR", "TEXT")

        xl = "<root>" + str + "</root>"
        Dim xmlReader As New XmlTextReader(New StringReader(xl))
        reader.Close()

        tvReport.Nodes.Clear()
        xmlData.Load(xmlReader)
        tvReport.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

        Dim tnode As TreeNode
        tnode = tvReport.Nodes(0)
        AddNode(xmlData.DocumentElement, tnode)


        tvReport.ExpandAll()

    End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub

    Function getReportCards() As String
        Dim node As TreeNode
        Dim cNode As TreeNode
        Dim ccNode As TreeNode

        Dim strRPF As String = ""

        Dim bRSM As Boolean = False

        For Each node In tvReport.Nodes
            For Each cNode In node.ChildNodes
                For Each ccNode In cNode.ChildNodes
                    If ccNode.Checked = True Then
                        If strRPF <> "" Then
                            strRPF += "|"
                        End If
                        strRPF += cNode.Text + "_" + ccNode.Text
                    End If
                Next
            Next
        Next


        Return strRPF.Trim
    End Function
    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")



        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_DESCR", getReportCards())
        param.Add("@STM_ID", "1")
        param.Add("@STU_ID", HF_stuid.Value)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            If hfCBSESchool.Value = "1" Then
                param.Add("@bEVALUATION", "false")
                .reportPath = Server.MapPath("/PHOENIXBETA/Curriculum/Reports/Rpt/rptStudentPerformanceAcrossYear_CBSE.rpt")
            Else
                param.Add("UserName", Session("sUsr_name"))
                param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
                param.Add("accYear", hfACD_ID.Value)
                param.Add("grade", hfGRD_ID.Value)
                param.Add("@bEVALUATION", "false")
                .reportPath = Server.MapPath("/PHOENIXBETA/Curriculum/Reports/Rpt/rptINTLStudentPerformanceAcrossYear.rpt")
            End If
            .reportParameters = param
        End With
        Session("rptClass") = rptClass

        Dim rptDownload As New ReportDownload
        rptDownload.LoadReports(rptClass, rs)
        rptDownload = Nothing

    End Sub
    Sub getStudentGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_GRD_ID FROM STUDENT_M WHERE STU_ID='" + HF_stuid.Value + "'"
        hfGRD_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub
    
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        CallReport()
    End Sub

    Protected Sub lnkAdd_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Sub ddlAttAcd_id_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAttAcd_id.SelectedIndexChanged
        bindAttendance_details(ViewState("STUID"))
        bindAttChart(ViewState("STUID"))
        bindABSChart(ViewState("STUID"))
        bindLATEChart(ViewState("STUID"))
    End Sub
#Region "FileUpload"
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        'METHODE TO SHOW ERROR OR MESSAGE
        If Message <> "" Then
            If bError Then
                lblIncMssg.CssClass = "error"
            Else
                lblIncMssg.CssClass = "error"
            End If
        Else
            lblIncMssg.CssClass = ""
        End If
        lblIncMssg.Text = Message
    End Sub
    Function ContainsSpecialChars(s As String) As Boolean
        Return s.IndexOfAny("[~`!@#$%^&*()-+=|{}':;,<>/?]".ToCharArray) <> -1
    End Function
    Private Function GetExtension(ByVal FileName As String) As String
        Dim Extension As String
        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)
        Return Extension
    End Function
    Public Function CurrentDate() As String
        'Dim lastDate As String = Day(DateTime.Now).ToString() & "/" & MonthName(Month(DateTime.Now), True) & "/" & Year(DateTime.Now).ToString()
        'lastDate = Convert.ToDateTime(lastDate).ToString("ddMMMyyyy")
        'Return lastDate
        Dim lastdate As String = DateTime.Now.Ticks.ToString
        Return lastdate
    End Function
    Function callDocSave(ByVal incident_type As String, ByRef trans As SqlTransaction, ByVal DocPath As String, ByVal type As Char, ByVal mrg_id As Integer) As Integer
        Dim ReturnFlag As Int16
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim params(8) As SqlClient.SqlParameter
        params(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
        params(1) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
        params(2) = New SqlClient.SqlParameter("@INC_TYPE", incident_type)
        params(3) = New SqlClient.SqlParameter("@INC_DATE", txtincidentdate.Text)
        params(4) = New SqlClient.SqlParameter("@TYPE", type)
        params(5) = New SqlClient.SqlParameter("@DOCName", DocPath)
        params(6) = New SqlClient.SqlParameter("@MRG_ID", mrg_id)
        params(7) = New SqlClient.SqlParameter("@NewMRg_ID", SqlDbType.BigInt)
        params(7).Direction = ParameterDirection.Output
        params(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        params(8).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "[BM].[SaveDocMerit_M]", params)
        If type = "I" Then
            Session("MRG_ID") = params(7).Value
        End If
        ReturnFlag = params(8).Value.ToString
        Return ReturnFlag
    End Function
    Function callSavefile(ByVal upload As FileUpload, ByVal bsuid As String, ByVal merit As String, ByVal hidGrpId As Integer, ByRef strans As SqlTransaction) As Integer
        callSavefile = 1000
        Try
            Dim status As Integer = 0
            Dim STU_BSU_ID As String = bsuid
            Dim PHOTO_PATH As String = String.Empty
            Dim ConFigPath As String = WebConfigurationManager.AppSettings("FileUploadPathFinal").ToString
            Dim path As String
            If ViewState("ACTIVITYFILEPATH") <> "" Then

                If Not Directory.Exists(ConFigPath & bsuid & "\" & hidGrpId & "\") Then
                    Directory.CreateDirectory(ConFigPath & bsuid & "\" & hidGrpId & "\")
                    path = ConFigPath & bsuid & "\" & hidGrpId & "\"
                Else
                    Dim dold As New DirectoryInfo(ConFigPath & bsuid & "\" & hidGrpId & "\")
                    path = ConFigPath & bsuid & "\" & hidGrpId & "\"
                    Dim fiold() As System.IO.FileInfo
                    fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fiold.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fiold
                            f.Delete()
                        Next
                    End If
                End If

                If ViewState("ACTIVITYFILEPATH") <> "" Then
                    Dim d As New DirectoryInfo(ViewState("ACTIVITYFILEPATH"))

                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length = 1 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fi
                            f.MoveTo(path & f.Name)
                            Dim Str_fullpath As String = bsuid & "\" & hidGrpId & "\" & "" & f.Name
                            If path <> "" Then
                                status = callDocSave(merit, strans, Str_fullpath, "U", hidGrpId)
                            Else
                                callSavefile = 1000
                                strans.Rollback()
                                Exit Function
                            End If
                        Next
                    End If
                    d.Delete()
                    ViewState("ACTIVITYFILEPATH") = ""
                End If
            End If
            callSavefile = status
        Catch ex As Exception
            UtilityObj.Errorlog("From callSavefile: " + ex.Message, "OASIS ACTIVITY SERVICES")
            strans.Rollback()
            callSavefile = 1000
        End Try
    End Function
#End Region
#Region "Merit/Demerit/Achievement Add "
    Protected Sub saveupload_Click(sender As Object, e As EventArgs)
        Try
            Dim str_error As String = ""
            Dim obj As Object = sender.parent
            Dim BSUID = Session("sbsuid")
            Dim errCount As Integer = 0
            Dim fileCount As Integer = 0
            Dim returnvalue As Boolean = False
            Dim ConFigPath As String = WebConfigurationManager.AppSettings("FileUploadPathTemp").ToString
            Dim Tempath As String = ConFigPath & BSUID & "\" & ddlMerit.SelectedItem.Text & "\"

            'to remove exising files in temp path
            Dim dold As New DirectoryInfo(Tempath)
            Dim fiold() As System.IO.FileInfo
            If dold.Exists Then
                fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fiold.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fiold
                        f.Delete()
                    Next
                End If
            End If
            Dim UploadfileName As String = ""
            ' end of deletion of exising files
            ' For Each rptr As RepeaterItem In repdoc.Items
            ' Dim lblfile As Label = CType(rptr.FindControl("lblfile"), Label)
            ' Dim Fileupload As FileUpload = CType(rptr.FindControl("Fileupload"), FileUpload)
            ' Dim hffile As HiddenField = CType(rptr.FindControl("hffile"), HiddenField)
            If upload.HasFile Then
                Try
                    fileCount = fileCount + 1
                    Dim strPostedFileName As String = upload.FileName 'get the file name
                    Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower
                    If strPostedFileName <> "" And ConFigPath <> "" Then
                        Dim intDocFileLength As Integer = upload.FileContent.Length ' get the file size
                        Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower
                        Dim ContainsSplChar As Boolean = ContainsSpecialChars(strPostedFileName)
                        Dim FileNameLength As Integer = strPostedFileName.Length
                        UploadfileName = ddlMerit.SelectedItem.Text & "_" & CurrentDate() & strExtn
                        Dim s As String = strPostedFileName
                        Dim result() As String
                        result = s.Split(".")
                        Dim fileext = GetExtension(strExtn)
                        'Checking file extension
                        If (strPostedFileName <> String.Empty) And fileext <> "" Then
                            If Not (fileext.ToUpper = "PDF") And Not (fileext.ToUpper = "PNG") And Not (fileext.ToUpper = "JPG") Then 'Or fileext.ToUpper = "PNG"

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Upload PDF Files Only!!"
                                Else
                                    str_error = str_error & "Upload PDF Files Only!!"
                                End If
                                returnvalue = False
                                '  Exit Sub
                                'Checking file extension length
                            ElseIf fileext.Length = 0 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "file with out extension not allowed...!!"
                                Else
                                    str_error = str_error & "file with out extension not allowed...!!"
                                End If
                                returnvalue = False
                                '   Exit Sub
                                'Checking Special Characters in file name

                            ElseIf ContainsSplChar = True Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                Else
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub
                                'Checking FileName length

                            ElseIf FileNameLength = 0 Or FileNameLength > 1500 Then '255
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Error with file Name Length..!!"
                                Else
                                    str_error = str_error & "Error with file Name Length..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub

                            ElseIf result.Length > 2 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Invalid file Name..!!"
                                Else
                                    str_error = str_error & "Invalid file Name..!!"
                                End If

                                returnvalue = False
                                'Exit Sub

                            ElseIf upload.FileContent.Length > 5000000 Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                Else
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                End If

                                returnvalue = False
                                '' Exit Sub

                            ElseIf Not (strExtn = ".pdf") And Not strExtn = ".jpg" And Not strExtn = ".png" Then 'exten type

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File type is different than allowed!"
                                Else
                                    str_error = str_error & "File type is different than allowed!"
                                End If

                                returnvalue = False
                                'Exit Sub

                            Else
                                str_error = ""

                                If Not Directory.Exists(Tempath) Then
                                    Directory.CreateDirectory(Tempath)
                                End If
                                If errCount = 0 Then
                                    upload.SaveAs(Tempath & UploadfileName)
                                End If
                                ViewState("ACTIVITYFILEPATH") = ConFigPath & BSUID & "\" & ddlMerit.SelectedItem.Text
                            End If
                        End If
                    End If

                Catch ex As Exception
                    ' returnvalue = False
                    '  Return returnvalue
                    ShowMessage(ex.Message, True)
                    Exit Sub
                End Try
            End If
            'Next

            If str_error <> "" Then
                ShowMessage(str_error, True)
                ' returnvalue = False
                ' Return returnvalue
                Exit Sub
            ElseIf fileCount <> 1 Then
                If str_error <> "" Then
                    str_error = str_error & "<br /><br />"
                    str_error = str_error & "Please select file!!!"
                Else
                    str_error = str_error & "Please select file!!!"
                End If
                '  returnvalue = False
                ' Return returnvalue
                ShowMessage(str_error, True)
                Exit Sub
            Else
                If Not (errCount = 0 And fileCount = 1) Then
                    ' Dim status As Integer = callSave(BSUID, ActivityID, strans)
                    ' If status = 1000 Then
                    If str_error <> "" Then
                        str_error = str_error & "<br /><br />"
                        str_error = str_error & "Please upload file!"
                    Else
                        str_error = str_error & "Please upload file!"
                    End If
                    ShowMessage(str_error, True)
                    'returnvalue = False
                Else
                    'If str_error <> "" Then
                    '    str_error = str_error & "<br /><br />"
                    '    str_error = str_error & "File saved successfully!"
                    'Else
                    '    str_error = str_error & "File saved successfully!"
                    '    ShowMessage(str_error, True)
                    'End If
                    '   returnvalue = True
                    updpnl.Visible = False
                    lnkShowFile.Visible = True
                    lnkShowFile.Text = UploadfileName
                    Dim uploadpath As String = ("Temp\" & BSUID & "\" & ddlMerit.SelectedItem.Text & "\" & UploadfileName)
                    uploadpath = uploadpath.Replace("\", "\\")
                    Dim strExtn As String = System.IO.Path.GetExtension(UploadfileName).ToLower
                    lnkShowFile.Attributes.Add("OnClick", "javascript: return showDocument('" & uploadpath & "','" & strExtn & "')")
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From saveupload_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Function callTrans_Save_Hdr(ByRef trans As SqlTransaction) As Integer
        Try
            Session("MRT_ID") = ""
            ' Do save document before save
            Dim ReturnFlag As Integer
            ReturnFlag = callDocSave("", trans, "", "I", 0)
            ' end of save document
            If ReturnFlag = 0 Then
                If Not Session("MRG_ID") Is Nothing Then
                    If Session("MRG_ID") > 0 Then
                        ReturnFlag = callSavefile(upload, Session("sbsuid"), ddlMerit.SelectedItem.Text, Session("MRG_ID"), trans)
                        Dim pParms(11) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sbsuid"))
                        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", Session("Current_ACD_ID"))
                        pParms(2) = New SqlClient.SqlParameter("@INCIDENT_DATE", Format(CDate(txtincidentdate.Text), "dd-MMM-yyyy"))
                        pParms(3) = New SqlClient.SqlParameter("@INCIDENT_TYPE", ddlMerit.SelectedItem.Text)

                        pParms(4) = New SqlClient.SqlParameter("@INCIDENT_DESC", txtRemarks.Text)

                        pParms(5) = New SqlClient.SqlParameter("@CAT_ID", ddlMerit.SelectedValue)
                        pParms(6) = New SqlClient.SqlParameter("@SUB_CAT_ID", ddlcat.SelectedItem.Value)
                        pParms(7) = New SqlClient.SqlParameter("@STU_ID", ViewState("STUID"))
                        If chk_bshow.Checked Then
                            pParms(8) = New SqlClient.SqlParameter("@bShow", 1)
                        Else
                            pParms(8) = New SqlClient.SqlParameter("@bShow", 0)
                        End If
                        pParms(9) = New SqlClient.SqlParameter("@MRG_ID", Session("MRG_ID"))
                        pParms(10) = New SqlClient.SqlParameter("@NewMRT_ID", SqlDbType.BigInt)
                        pParms(10).Direction = ParameterDirection.Output
                        pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(11).Direction = ParameterDirection.ReturnValue

                        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "BM.SaveMerit_M", pParms)
                        ReturnFlag = pParms(11).Value
                        If ReturnFlag = 0 Then
                            If Session("MRT_ID") Is Nothing Or Session("MRT_ID").ToString = "" Then
                                Session("MRT_ID") = (IIf(TypeOf (pParms(10).Value) Is DBNull, String.Empty, pParms(10).Value)).ToString
                            Else
                                Session("MRT_ID") = Session("MRT_ID") & "|" & (IIf(TypeOf (pParms(10).Value) Is DBNull, String.Empty, pParms(10).Value)).ToString
                            End If
                            ReturnFlag = callTrans_Save_Dtl(trans, ViewState("STUID"), pParms(10).Value)
                        End If
                        '  Return ReturnFlag
                        If (ReturnFlag <> 0) Then
                            Return ReturnFlag
                        End If

                    End If
                End If
            End If
            Return ReturnFlag
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "merit1hdr1111")
            Return 1000
        End Try
    End Function
    Function callTrans_Save_Dtl(ByRef trans As SqlTransaction, ByVal stuid As Int64, ByVal MRT_ID As Int64) As Integer
        Dim ReturnFlag As Integer

        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'For Each DrRow As DataRow In dt.Rows
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(1) = New SqlClient.SqlParameter("@INCIDENT_ID", MRT_ID)
            pParms(2) = New SqlClient.SqlParameter("@STU_ID", stuid)
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "BM.SaveMerit_D", pParms)
            ReturnFlag = pParms(4).Value
            ' Next
            Return ReturnFlag
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "merit1dtl")
            Return -1
        End Try
    End Function
    Private Function CahekDateWithTodate(ByVal CheckDate As Date) As Boolean
        Try
            If DateDiff(DateInterval.Day, CheckDate, Date.Now()) < 0 Then
                Return False
                Exit Function
            End If
            Return True
        Catch ex As Exception
        End Try
    End Function
    Function Final_validation() As Boolean
        Dim IS_ERROR As Boolean

        IS_ERROR = False

        If CahekDateWithTodate(CDate(txtincidentdate.Text)) = False Then
            IS_ERROR = True
            lblIncMssg.Text = " Incident date Can't be grater than Today's date."
        ElseIf txtRemarks.Text = "" Then
            IS_ERROR = True
            lblIncMssg.Text = " Please fill out comments."
        End If

        If IS_ERROR = True Then
            Final_validation = False
        Else
            Final_validation = True
        End If

    End Function
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim transaction As SqlTransaction
        Dim Status As Integer

        If Final_validation() = True Then


            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Status = callTrans_Save_Hdr(transaction)
                    If Status <> 0 Then
                        Throw New ArgumentException("Record could not be Inserted")
                    End If
                    'Status = callTrans_Save_Dtl(transaction)
                    ' If Status <> 0 Then

                    '  Throw New ArgumentException("Record could not be Inserted")
                    'End If

                    transaction.Commit()

                    Try
                        Dim MrtIds() As String = Session("MRT_ID").split("|")
                        For Each MrtId In MrtIds
                            Dim ReturnValue As Integer = NotificationEmail.SendEmailNotification(1, MrtId, Session("sbsuid"))
                        Next

                    Catch ex As Exception
                        'lblError.Text = ex.Message.ToString
                    Finally

                    End Try
                    'Dim IsSendemail As Boolean = False
                    'Dim dt6 As DataTable = NotificationEmail.GetStudentNotificationFlag(Session("sbsuid"))
                    'For Each drow As DataRow In dt6.Rows
                    '    If cmbCategory.SelectedValue = "Achieves" Then
                    '        IsSendemail = drow.Item("PNT_bEMAIL_ACHIEVES")
                    '    ElseIf cmbCategory.SelectedValue = "Merit" Then
                    '        IsSendemail = drow.Item("PNT_bEMAIL_MERIT")
                    '    ElseIf cmbCategory.SelectedValue = "De-Merit" Then
                    '        IsSendemail = drow.Item("PNT_bEMAIL_DEMERIT")
                    '    End If

                    'Next

                    'If IsSendemail = True Then
                    '    For Each DrRow As DataRow In dt.Rows
                    '        SendEmail(Session("sbsuid"), cmbCategory.SelectedValue, DrRow.Item("STU_ID"), txtReportonincident.Text)
                    '    Next

                    'End If

                    lblIncMssg.Text = "Record Saved Succesfully!!!"
                    txtincidentdate.Text = Today.Date.ToString("dd/MMM/yyyy")
                    txtRemarks.Text = ""
                    lnkShowFile.Visible = False
                    updpnl.Visible = True
                    ' BindGrid_POS(ViewState("STUID"))
                    ' BindGrid_neg(ViewState("STUID"))
                    gridbind_Achievement(ViewState("STUID"))
                    gridbind_Merit(ViewState("STUID"))

                Catch myex As ArgumentException
                    transaction.Rollback()
                    lblIncMssg.Text = myex.Message
                    UtilityObj.Errorlog(myex.Message, "merit1")
                Catch ex As Exception
                    transaction.Rollback()
                    lblIncMssg.Text = "Record could not be Updated"
                    UtilityObj.Errorlog(ex.Message, "merit11")
                End Try
            End Using
        End If
    End Sub
    Sub bindcategory()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "BM.GET_CATEGORY_STUDENTPROFILE"
        Dim params(1) As SqlParameter
        params(0) = Mainclass.CreateSqlParameter("@BSUID", Session("sbsuid"), SqlDbType.VarChar)
        params(1) = Mainclass.CreateSqlParameter("@CAT", ddlMerit.SelectedValue, SqlDbType.Int)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_query, params)
        ddlcat.DataSource = ds
        ddlcat.DataTextField = "BM_CATEGORYNAME"
        ddlcat.DataValueField = "BM_CATEGORYID"
        ddlcat.DataBind()
    End Sub
    Protected Sub ddlMerit_SelectedIndexChanged(sender As Object, e As EventArgs)
        collapseOne1.Attributes.Add("class", "collapse show")
        bindcategory()
    End Sub
#End Region

#Region "Achievement Edit/Update"
    Protected Sub btnEdit_Command(sender As Object, e As CommandEventArgs)
        Dim st_conn = ConnectionManger.GetOASISConnectionString
        ShowMessage("", False)
        Dim btnEdit As LinkButton = DirectCast(sender, LinkButton)
        Dim meritid As Integer = Convert.ToInt16(e.CommandArgument)
        Dim lblStu_Name As Label = DirectCast(sender.parent.findcontrol("lblStu_Name"), Label)
        Dim lblStu_No As Label = DirectCast(sender.parent.findcontrol("lblStu_No"), Label)
        Dim ddl_Categoryname As DropDownList = DirectCast(sender.parent.findcontrol("ddl_Categoryname"), DropDownList)
        Dim txtStu_REMARKS As TextBox = DirectCast(sender.parent.findcontrol("txtStu_REMARKS"), TextBox)
        Dim GroupId As Integer = (TryCast(sender.parent.FindControl("hidGrpId"), HiddenField)).Value
        Dim lnkAttbtn As ImageButton = DirectCast(sender.parent.FindControl("lnkAttbtn"), ImageButton)
        Dim divUpload As HtmlGenericControl = DirectCast(sender.parent.FindControl("divUpload"), HtmlGenericControl)
        Dim chk_bshow_grid As CheckBox = DirectCast(sender.parent.FindControl("chk_bshow"), CheckBox)
        If (btnEdit.Text = "Edit") Then

            lblStu_Name.Visible = False
            lblStu_No.Visible = False
            lnkAttbtn.Visible = False
            divUpload.Visible = True

            Dim PARAM(0) As SqlParameter
            PARAM(0) = Mainclass.CreateSqlParameter("@MRTID", meritid, SqlDbType.BigInt)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(st_conn, "[BM].[GET_ACHEIVMENT_CATEGORIES]", PARAM)
            If (ds.Tables.Count > 0) Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    ddl_Categoryname.DataSource = ds.Tables(0)
                    ddl_Categoryname.DataValueField = "ID"
                    ddl_Categoryname.DataTextField = "NAME"
                    ddl_Categoryname.DataBind()
                    ddl_Categoryname.Visible = True
                    If Not lblStu_No.Text Is Nothing And lblStu_No.Text <> "" Then
                        If Not (ddl_Categoryname.Items.FindByText(lblStu_No.Text).Text) Is Nothing Then
                            ddl_Categoryname.Items.FindByText(lblStu_No.Text).Selected = True
                            'ddl_Categoryname.Visible = True
                        End If
                    End If
                End If
            End If
            'ddl_Categoryname.Items.Insert(0, New ListItem("SELECT", 0))
            'ddl_Categoryname.Items.FindByText("SELECT").Selected = True
            '[BM].[GET_ACHEIVMENT_CATEGORIES]
            txtStu_REMARKS.Visible = True
            txtStu_REMARKS.Text = lblStu_Name.Text

            btnEdit.Text = "Update"
        ElseIf (btnEdit.Text = "Update") Then
            Dim strans As SqlTransaction = Nothing
            Try
                Dim objConn As New SqlConnection(st_conn)
                objConn.Open()
                strans = objConn.BeginTransaction
                '[BM].[UPDATE_ACHEIVMENT_DETAILS] 
               

                Dim params(4) As SqlParameter
                params(0) = Mainclass.CreateSqlParameter("@PAGE", "A", SqlDbType.Char)
                params(1) = Mainclass.CreateSqlParameter("@MERITID", meritid, SqlDbType.BigInt)
                params(2) = Mainclass.CreateSqlParameter("@REMARKS", txtStu_REMARKS.Text, SqlDbType.VarChar)
                params(3) = Mainclass.CreateSqlParameter("@BM_CATID", ddl_Categoryname.SelectedValue, SqlDbType.BigInt)
                If chk_bshow_grid.Checked Then
                    params(4) = New SqlClient.SqlParameter("@bShow", 1)
                Else
                    params(4) = New SqlClient.SqlParameter("@bShow", 0)
                End If
                SqlHelper.ExecuteNonQuery(strans, "[BM].[UPDATE_ACHEIVMENT_DETAILS]", params)

                callSavefile(upload, Session("sbsuid"), ddl_Categoryname.SelectedItem.Text, GroupId, strans)
                strans.Commit()
                gridbind_Achievement(ViewState("STUID"))
                ShowMessage("Record got updated", False)               

            Catch ex As Exception
                strans.Rollback()
            Finally

            End Try
        End If
    End Sub
    Protected Sub saveupload_Click1(sender As Object, e As EventArgs)
        Dim DdlMerit As DropDownList = DirectCast(sender.parent.findcontrol("ddl_Categoryname"), DropDownList)
        Dim updpnl As UpdatePanel = DirectCast(sender.parent.findcontrol("updpnl1"), UpdatePanel)
        Dim lnkShowFile As LinkButton = DirectCast(sender.parent.findcontrol("lnkShowFile1"), LinkButton)
        Dim upload As FileUpload = DirectCast(sender.parent.findcontrol("upload1"), FileUpload)

        Try
            Dim str_error As String = ""
            Dim obj As Object = sender.parent
            Dim BSUID = Session("sbsuid")
            Dim errCount As Integer = 0
            Dim fileCount As Integer = 0
            Dim returnvalue As Boolean = False
            Dim ConFigPath As String = WebConfigurationManager.AppSettings("FileUploadPathTemp").ToString
            Dim Tempath As String = ConFigPath & BSUID & "\Achievement\"

            'to remove exising files in temp path
            Dim dold As New DirectoryInfo(Tempath)
            Dim fiold() As System.IO.FileInfo
            If dold.Exists Then
                fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fiold.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fiold
                        f.Delete()
                    Next
                End If
            End If
            Dim UploadfileName As String = ""
            ' end of deletion of exising files
            ' For Each rptr As RepeaterItem In repdoc.Items
            ' Dim lblfile As Label = CType(rptr.FindControl("lblfile"), Label)
            ' Dim Fileupload As FileUpload = CType(rptr.FindControl("Fileupload"), FileUpload)
            ' Dim hffile As HiddenField = CType(rptr.FindControl("hffile"), HiddenField)
            If upload.HasFile Then
                Try
                    fileCount = fileCount + 1
                    Dim strPostedFileName As String = upload.FileName 'get the file name
                    Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower
                    If strPostedFileName <> "" And ConFigPath <> "" Then
                        Dim intDocFileLength As Integer = upload.FileContent.Length ' get the file size
                        Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower
                        Dim ContainsSplChar As Boolean = ContainsSpecialChars(strPostedFileName)
                        Dim FileNameLength As Integer = strPostedFileName.Length
                        UploadfileName = "Achievement" & "_" & CurrentDate() & strExtn
                        Dim s As String = strPostedFileName
                        Dim result() As String
                        result = s.Split(".")
                        Dim fileext = GetExtension(strExtn)
                        'Checking file extension
                        If (strPostedFileName <> String.Empty) And fileext <> "" Then
                            If Not (fileext.ToUpper = "PDF") And Not (fileext.ToUpper = "PNG") And Not (fileext.ToUpper = "JPG") Then 'Or fileext.ToUpper = "PNG"

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Upload PDF Files Only!!"
                                Else
                                    str_error = str_error & "Upload PDF Files Only!!"
                                End If
                                returnvalue = False
                                '  Exit Sub
                                'Checking file extension length
                            ElseIf fileext.Length = 0 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "file with out extension not allowed...!!"
                                Else
                                    str_error = str_error & "file with out extension not allowed...!!"
                                End If
                                returnvalue = False
                                '   Exit Sub
                                'Checking Special Characters in file name

                            ElseIf ContainsSplChar = True Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                Else
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub
                                'Checking FileName length

                            ElseIf FileNameLength = 0 Or FileNameLength > 1500 Then '255
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Error with file Name Length..!!"
                                Else
                                    str_error = str_error & "Error with file Name Length..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub

                            ElseIf result.Length > 2 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Invalid file Name..!!"
                                Else
                                    str_error = str_error & "Invalid file Name..!!"
                                End If

                                returnvalue = False
                                'Exit Sub

                            ElseIf upload.FileContent.Length > 5000000 Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                Else
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                End If

                                returnvalue = False
                                '' Exit Sub

                            ElseIf Not (strExtn = ".pdf") And Not strExtn = ".jpg" And Not strExtn = ".png" Then 'exten type

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File type is different than allowed!"
                                Else
                                    str_error = str_error & "File type is different than allowed!"
                                End If

                                returnvalue = False
                                'Exit Sub

                            Else
                                str_error = ""

                                If Not Directory.Exists(Tempath) Then
                                    Directory.CreateDirectory(Tempath)
                                End If
                                If errCount = 0 Then
                                    upload.SaveAs(Tempath & UploadfileName)
                                End If
                                ViewState("ACTIVITYFILEPATH") = ConFigPath & BSUID & "\Achievement"
                            End If
                        End If
                    End If

                Catch ex As Exception
                    ' returnvalue = False
                    '  Return returnvalue
                    ShowMessage(ex.Message, True)
                    Exit Sub
                End Try
            End If
            'Next

            If str_error <> "" Then
                ShowMessage(str_error, True)
                ' returnvalue = False
                ' Return returnvalue
                Exit Sub
            ElseIf fileCount <> 1 Then
                If str_error <> "" Then
                    str_error = str_error & "<br /><br />"
                    str_error = str_error & "Please select file!!!"
                Else
                    str_error = str_error & "Please select file!!!"
                End If
                '  returnvalue = False
                ' Return returnvalue
                ShowMessage(str_error, True)
                Exit Sub
            Else
                If Not (errCount = 0 And fileCount = 1) Then
                    ' Dim status As Integer = callSave(BSUID, ActivityID, strans)
                    ' If status = 1000 Then
                    If str_error <> "" Then
                        str_error = str_error & "<br /><br />"
                        str_error = str_error & "Please upload file!"
                    Else
                        str_error = str_error & "Please upload file!"
                    End If
                    ShowMessage(str_error, True)
                    'returnvalue = False
                Else
                    'If str_error <> "" Then
                    '    str_error = str_error & "<br /><br />"
                    '    str_error = str_error & "File saved successfully!"
                    'Else
                    '    str_error = str_error & "File saved successfully!"
                    '    ShowMessage(str_error, True)
                    'End If
                    '   returnvalue = True
                    updpnl.Visible = False
                    lnkShowFile.Visible = True
                    lnkShowFile.Text = UploadfileName
                    Dim uploadpath As String = ("Temp\" & BSUID & "\" & DdlMerit.SelectedItem.Text & "\" & UploadfileName)
                    uploadpath = uploadpath.Replace("\", "\\")
                    Dim strExtn As String = System.IO.Path.GetExtension(UploadfileName).ToLower
                    lnkShowFile.Attributes.Add("OnClick", "javascript: return showDocument('" & uploadpath & "','" & strExtn & "')")
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From saveupload_Click1: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub
    Sub gridbind_Achievement(ByVal STU_ID As STRING)
        Try
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7 As String


            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            str_Filter = ""
            If gvStudTPT.Rows.Count > 0 Then
                ' --- Initialize The Variables

                lstrOpr = "LI"
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO

                lstrOpr = "LI"
                txtSearch = gvStudTPT.HeaderRow.FindControl("txtLogDate")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_ENTRY_DATE", lstrCondn1)

                '   -- 2  txtDate

                lstrOpr = "LI"
                txtSearch = gvStudTPT.HeaderRow.FindControl("txtInciDate")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_INCDNT_DATE", lstrCondn2)

                '   -- 3  txtGrade

                lstrOpr = "LI"
                txtSearch = gvStudTPT.HeaderRow.FindControl("txtMerit")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_INCDNT_TYPE", lstrCondn3)

                '   -- 4   txtStuno

                lstrOpr = "LI"
                txtSearch = gvStudTPT.HeaderRow.FindControl("txtRemarks")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_INCDNT_REMARKS", lstrCondn4)

            End If

            str_Sql = "Select isnull(MRG_ID ,'') MRGID, isnull(MRT_bShow,0) bShow, * From BM.Merits_M A" _
                   & " INNER JOIN BM.MERITS_D B ON A.MRT_ID=B.MRS_MRT_ID left outer join BM.BM_CATEGORY C on C.BM_CATEGORYID=A.MRT_BM_SUB_CATEGORY_ID " _
                   & " LEFT join BM.MERITS_G  G ON G.MRG_ID = A .MRT_MRG_ID " _
                   & " WHERE MRT_INCDNT_TYPE='Achieves' AND MRS_STU_ID='" & STU_ID & "' AND ISNULL(MRT_DELETED,0) = 0 AND MRT_ACD_ID='" & Session("CURRENT_ACD_ID") & "' " & str_Filter


            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            gvStudTPT.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudTPT.DataBind()
                Dim columnCount As Integer = gvStudTPT.Rows(0).Cells.Count
                gvStudTPT.Rows(0).Cells.Clear()
                gvStudTPT.Rows(0).Cells.Add(New TableCell)
                gvStudTPT.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudTPT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudTPT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudTPT.DataBind()
            End If

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtLogDate")
            txtSearch.Text = lstrCondn1

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtInciDate")
            txtSearch.Text = lstrCondn2

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtMerit")
            txtSearch.Text = lstrCondn3

            txtSearch = gvStudTPT.HeaderRow.FindControl("txtRemarks")
            txtSearch.Text = lstrCondn4



        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub gridbindBehavior(ByVal STU_ID As String)
        Dim param(0) As SqlClient.SqlParameter
        param(0) = Mainclass.CreateSqlParameter("@STU_ID", STU_ID, SqlDbType.VarChar)
        Dim DS As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, "BM.GET_BEHAVIOUR_DETAILS", param)
        If DS.Tables.Count > 0 Then
            grdBehavior.DataSource = DS.Tables(0)
            grdBehavior.DataBind()
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind_Achievement(ViewState("STUID"))
    End Sub
    Protected Sub lnkdelete_Command(sender As Object, e As CommandEventArgs)
        ShowMessage("", False)
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            '[BM].[DELETE_MERIT_DETAILS] '
            Dim params(0) As SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@MRT_ID", e.CommandArgument, SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(strans, "[BM].[DELETE_MERIT_DETAILS]", params)
            strans.Commit()
            gridbind_Achievement(ViewState("STUID"))
            ShowMessage("Record got deleted", False)
        Catch EX As Exception
        End Try
    End Sub
    Protected Sub gvStudTPT_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Dim index As String = gvStudTPT_Merit.EditIndex
        Dim hidDocName As HiddenField = DirectCast(e.Row.FindControl("hidDocName"), HiddenField)
        'Dim hidid As HiddenField = DirectCast(e.Row.FindControl("hidid"), HiddenField)
        Dim lnkAttbtn As ImageButton = DirectCast(e.Row.FindControl("lnkAttbtn"), ImageButton)
        ' If (e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Edit) Then
        '    Dim saveupload As Button = DirectCast(e.Row.FindControl("saveupload"), Button)
        '    Dim smScriptManager As New ScriptManager
        '    smScriptManager = Me.Page.FindControl("ScriptManager1")
        '    smScriptManager.RegisterPostBackControl(saveupload)
        ' End If


        ' Dim lnkAttDwnldbtn As LinkButton = DirectCast(e.Row.FindControl("lnkAttDwnldbtn"), LinkButton)
        If Not hidDocName Is Nothing Then
            If (hidDocName.Value <> "") Then
                lnkAttbtn.Visible = True
                '  lnkAttDwnldbtn.Visible = True
                hidDocName.Value = "Final\" + hidDocName.Value
                hidDocName.Value = hidDocName.Value.Replace("\", "\\")
                Dim strExtn As String = System.IO.Path.GetExtension(hidDocName.Value).ToLower
                lnkAttbtn.Attributes.Add("OnClick", "javascript: return showDocument('" & hidDocName.Value & "','" & strExtn & "')")
            Else
                ' lnkAttDwnldbtn.Visible = False
                lnkAttbtn.Visible = False
            End If

        End If
    End Sub
#End Region
#Region "Merit/Demerit Edit/Update"
    Protected Sub OnRowCancelingEdit(sender As Object, e As EventArgs)
        gvStudTPT_Merit.EditIndex = -1
        gridbind_Merit(ViewState("STUID"))
    End Sub
    Protected Sub OnRowEditing(sender As Object, e As GridViewEditEventArgs)
        ShowMessage("", False)
        Dim row As GridViewRow = gvStudTPT_Merit.Rows(e.NewEditIndex)
        Dim remark As String = (TryCast(row.FindControl("lblStu_NameMerit"), Label)).Text
        h_remark_edit.Value = remark
        gvStudTPT_Merit.EditIndex = e.NewEditIndex
        gridbind_Merit(ViewState("STUID"))
        'gvStudTPT.DataBind()
        'Me.gridbind()
    End Sub
    Protected Sub gvStudTPT_Merit_RowCreated(sender As Object, e As GridViewRowEventArgs)

        Dim txtEditRemarks As TextBox = (TryCast(e.Row.FindControl("txtEditRemarks"), TextBox))
        If Not txtEditRemarks Is Nothing Then
            txtEditRemarks.Text = h_remark_edit.Value
            h_remark_edit.Value = ""
        Else

        End If
    End Sub
    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvStudTPT.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Protected Sub ImageButton1_ClickM(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind_Merit(ViewState("STUID"))
    End Sub
    Protected Sub OnRowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Dim row As GridViewRow = gvStudTPT_Merit.Rows(e.RowIndex)
        Dim selectedmerit As String = ""
        'Dim customerId As Integer = Convert.ToInt32(gvStudTPT.DataKeys(e.RowIndex).Values(0))
        'Dim ddlgvGrade As New DropDownList  = row.FindControl("DdlMerit")
        'selectedmerit = ddlgvGrade.Text

        Dim merit As String = (TryCast(row.FindControl("DdlMeritMerit"), DropDownList)).Text
        Dim remarks As String = (TryCast(row.FindControl("txtEditRemarks"), TextBox)).Text
        Dim meritid As String = (TryCast(row.FindControl("hidid"), HiddenField)).Value
        Dim GroupId As Integer = (TryCast(row.FindControl("hidGrpId2"), HiddenField)).Value
        Dim upload As FileUpload = (TryCast(row.FindControl("upload2"), FileUpload))
        Dim chk_bshow_grid As CheckBox = DirectCast(row.FindControl("chk_bshow_merit"), CheckBox)

        Dim merittye As Integer
        If (merit = "Merit") Then
            merittye = 0
        ElseIf (merit = "De-merit") Then
            merittye = 1
        End If
        'Dim entrydatenew As DateTime = Date.ParseExact((TryCast(row.FindControl("lblEntry_Date"), Label)).Text, "yyyy/MM/dd", Nothing).Date()
        'Dim query As String = "UPDATE BM.Merits_M SET MRT_INCDNT_REMARKS=@Remarks,MRT_INCDNT_TYPE=@Merit WHERE MRT_INCDNT_REMARKS='" & Session("Remark") & "' AND MRT_ACD_ID='" & Session("MERIT_ACD_ID") & "' "
        Dim constr As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(constr)
            objConn.Open()
            strans = objConn.BeginTransaction
            Dim params(4) As SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@PAGE", "M", SqlDbType.Char)
            params(1) = Mainclass.CreateSqlParameter("@MERITID", meritid, SqlDbType.BigInt)
            params(2) = Mainclass.CreateSqlParameter("@REMARKS", remarks, SqlDbType.VarChar)
            params(3) = Mainclass.CreateSqlParameter("@BM_CATID", merittye, SqlDbType.BigInt)
            If chk_bshow_grid.Checked Then
                params(4) = New SqlClient.SqlParameter("@bShow", 1)
            Else
                params(4) = New SqlClient.SqlParameter("@bShow", 0)
            End If
            SqlHelper.ExecuteNonQuery(strans, "[BM].[UPDATE_ACHEIVMENT_DETAILS]", params)

            'update file in final folder path if file is uploaded
            callSavefile(upload, Session("sbsuid"), merit, GroupId, strans)
            strans.Commit()
            gvStudTPT_Merit.EditIndex = -1
            gridbind_Merit(ViewState("STUID"))
            ShowMessage("Record got updated", False)
        Catch ex As Exception
            strans.Rollback()
        Finally
        End Try
    End Sub
    
  
    Sub gridbind_Merit(ByVal stu_id As String)
        Try

            Dim str_Sql As String
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5, lstrCondn6, lstrCondn7 As String


            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""
            lstrCondn6 = ""
            lstrCondn7 = ""
            str_Filter = ""
            If gvStudTPT_Merit.Rows.Count > 0 Then
                ' --- Initialize The Variables

                lstrOpr = "LI"
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtReceiptno FCL_RECNO

                lstrOpr = "LI"
                txtSearch = gvStudTPT_Merit.HeaderRow.FindControl("txtLogDateMerit")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_ENTRY_DATE", lstrCondn1)
                '   -- 2  txtDate

                lstrOpr = "LI"
                txtSearch = gvStudTPT_Merit.HeaderRow.FindControl("txtInciDateMerit")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_INCDNT_DATE", lstrCondn2)
                '   -- 3  txtGrade

                lstrOpr = "LI"
                txtSearch = gvStudTPT_Merit.HeaderRow.FindControl("txtMeritMerit")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_INCDNT_TYPE", lstrCondn3)
                '   -- 4   txtStuno

                lstrOpr = "LI"
                txtSearch = gvStudTPT_Merit.HeaderRow.FindControl("txtRemarksMerit")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "MRT_INCDNT_REMARKS", lstrCondn4)
            End If


            str_Sql = "Select isnull(MRG_ID ,'') MRGID,isnull(MRT_bShow,0) bShow, * From BM.Merits_M A" _
                   & " INNER JOIN BM.MERITS_D B ON A.MRT_ID=B.MRS_MRT_ID " _
                   & " LEFT join BM.MERITS_G  G ON G.MRG_ID = A .MRT_MRG_ID " _
                   & " WHERE MRT_INCDNT_TYPE IN ('Merit','De-Merit')  AND MRS_STU_ID='" & stu_id & "'  AND ISNULL(MRT_DELETED,0) = 0  AND MRT_ACD_ID='" & Session("current_ACD_ID") & "' " & str_Filter



            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            gvStudTPT_Merit.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudTPT_Merit.DataBind()
                Dim columnCount As Integer = gvStudTPT.Rows(0).Cells.Count
                gvStudTPT_Merit.Rows(0).Cells.Clear()
                gvStudTPT_Merit.Rows(0).Cells.Add(New TableCell)
                gvStudTPT_Merit.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudTPT_Merit.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudTPT_Merit.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudTPT_Merit.DataBind()
            End If

            txtSearch = gvStudTPT_Merit.HeaderRow.FindControl("txtLogDateMerit")
            txtSearch.Text = lstrCondn1

            txtSearch = gvStudTPT_Merit.HeaderRow.FindControl("txtInciDateMerit")
            txtSearch.Text = lstrCondn2

            txtSearch = gvStudTPT_Merit.HeaderRow.FindControl("txtMeritMerit")
            txtSearch.Text = lstrCondn3

            txtSearch = gvStudTPT_Merit.HeaderRow.FindControl("txtRemarksMerit")
            txtSearch.Text = lstrCondn4


        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub lnkdeleteMerit_Command(sender As Object, e As CommandEventArgs)
        ShowMessage("", False)
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            '[BM].[DELETE_MERIT_DETAILS] '
            Dim params(0) As SqlParameter
            params(0) = Mainclass.CreateSqlParameter("@MRT_ID", e.CommandArgument, SqlDbType.BigInt)
            SqlHelper.ExecuteNonQuery(strans, "[BM].[DELETE_MERIT_DETAILS]", params)
            strans.Commit()
            ShowMessage("Record got deleted", False)
            gridbind_Merit(ViewState("STUID"))
        Catch EX As Exception
        End Try
    End Sub
    Protected Sub gvStudTPT_Merit_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Dim index As String = gvStudTPT_Merit.EditIndex
        Dim hidDocName As HiddenField = DirectCast(e.Row.FindControl("hidDocNameMerit"), HiddenField)
        Dim lnkAttbtn As ImageButton = DirectCast(e.Row.FindControl("lnkAttbtnMerit"), ImageButton)

        If Not hidDocName Is Nothing Then
            If (hidDocName.Value <> "") Then
                lnkAttbtn.Visible = True
                hidDocName.Value = "Final\" + hidDocName.Value
                hidDocName.Value = hidDocName.Value.Replace("\", "\\")
                Dim strExtn As String = System.IO.Path.GetExtension(hidDocName.Value).ToLower
                lnkAttbtn.Attributes.Add("OnClick", "javascript: return showDocument('" & hidDocName.Value & "','" & strExtn & "')")
            Else
                lnkAttbtn.Visible = False
            End If
        End If
    End Sub

    Protected Sub saveupload_Click2(sender As Object, e As EventArgs)
        Dim DdlMerit As DropDownList = DirectCast(sender.parent.findcontrol("DdlMeritMerit"), DropDownList)
        Dim updpnl As UpdatePanel = DirectCast(sender.parent.findcontrol("updpnl2"), UpdatePanel)
        Dim lnkShowFile As LinkButton = DirectCast(sender.parent.findcontrol("lnkShowFile2"), LinkButton)
        Dim upload As FileUpload = DirectCast(sender.parent.findcontrol("upload2"), FileUpload)

        Try
            Dim str_error As String = ""
            Dim obj As Object = sender.parent
            Dim BSUID = Session("sbsuid")
            Dim errCount As Integer = 0
            Dim fileCount As Integer = 0
            Dim returnvalue As Boolean = False
            Dim ConFigPath As String = WebConfigurationManager.AppSettings("FileUploadPathTemp").ToString
            Dim Tempath As String = ConFigPath & BSUID & "\" & DdlMerit.SelectedItem.Text & "\"

            'to remove exising files in temp path
            Dim dold As New DirectoryInfo(Tempath)
            Dim fiold() As System.IO.FileInfo
            If dold.Exists Then
                fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                If fiold.Length > 0 Then '' If Having Attachments
                    For Each f As System.IO.FileInfo In fiold
                        f.Delete()
                    Next
                End If
            End If
            Dim UploadfileName As String = ""
            ' end of deletion of exising files
            ' For Each rptr As RepeaterItem In repdoc.Items
            ' Dim lblfile As Label = CType(rptr.FindControl("lblfile"), Label)
            ' Dim Fileupload As FileUpload = CType(rptr.FindControl("Fileupload"), FileUpload)
            ' Dim hffile As HiddenField = CType(rptr.FindControl("hffile"), HiddenField)
            If upload.HasFile Then
                Try
                    fileCount = fileCount + 1
                    Dim strPostedFileName As String = upload.FileName 'get the file name
                    Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower
                    If strPostedFileName <> "" And ConFigPath <> "" Then
                        Dim intDocFileLength As Integer = upload.FileContent.Length ' get the file size
                        Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower
                        Dim ContainsSplChar As Boolean = ContainsSpecialChars(strPostedFileName)
                        Dim FileNameLength As Integer = strPostedFileName.Length
                        UploadfileName = DdlMerit.SelectedItem.Text & "_" & CurrentDate() & strExtn
                        Dim s As String = strPostedFileName
                        Dim result() As String
                        result = s.Split(".")
                        Dim fileext = GetExtension(strExtn)
                        'Checking file extension
                        If (strPostedFileName <> String.Empty) And fileext <> "" Then
                            If Not (fileext.ToUpper = "PDF") And Not (fileext.ToUpper = "PNG") And Not (fileext.ToUpper = "JPG") Then 'Or fileext.ToUpper = "PNG"

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Upload PDF Files Only!!"
                                Else
                                    str_error = str_error & "Upload PDF Files Only!!"
                                End If
                                returnvalue = False
                                '  Exit Sub
                                'Checking file extension length
                            ElseIf fileext.Length = 0 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "file with out extension not allowed...!!"
                                Else
                                    str_error = str_error & "file with out extension not allowed...!!"
                                End If
                                returnvalue = False
                                '   Exit Sub
                                'Checking Special Characters in file name

                            ElseIf ContainsSplChar = True Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                Else
                                    str_error = str_error & "File Name with special characters are not allowed..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub
                                'Checking FileName length

                            ElseIf FileNameLength = 0 Or FileNameLength > 1500 Then '255
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Error with file Name Length..!!"
                                Else
                                    str_error = str_error & "Error with file Name Length..!!"
                                End If

                                returnvalue = False
                                ' Exit Sub

                            ElseIf result.Length > 2 Then
                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "Invalid file Name..!!"
                                Else
                                    str_error = str_error & "Invalid file Name..!!"
                                End If

                                returnvalue = False
                                'Exit Sub

                            ElseIf upload.FileContent.Length > 5000000 Then

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                Else
                                    str_error = str_error & "The Size of file is greater than 5MB"
                                End If

                                returnvalue = False
                                '' Exit Sub

                            ElseIf Not (strExtn = ".pdf") And Not strExtn = ".jpg" And Not strExtn = ".png" Then 'exten type

                                If str_error <> "" Then
                                    str_error = str_error & "<br /><br />"
                                    str_error = str_error & "File type is different than allowed!"
                                Else
                                    str_error = str_error & "File type is different than allowed!"
                                End If

                                returnvalue = False
                                'Exit Sub

                            Else
                                str_error = ""

                                If Not Directory.Exists(Tempath) Then
                                    Directory.CreateDirectory(Tempath)
                                End If
                                If errCount = 0 Then
                                    upload.SaveAs(Tempath & UploadfileName)
                                End If
                                ViewState("ACTIVITYFILEPATH") = ConFigPath & BSUID & "\" & DdlMerit.SelectedItem.Text
                            End If
                        End If
                    End If

                Catch ex As Exception
                    ' returnvalue = False
                    '  Return returnvalue
                    ShowMessage(ex.Message, True)
                    Exit Sub
                End Try
            End If
            'Next

            If str_error <> "" Then
                ShowMessage(str_error, True)
                ' returnvalue = False
                ' Return returnvalue
                Exit Sub
            ElseIf fileCount <> 1 Then
                If str_error <> "" Then
                    str_error = str_error & "<br /><br />"
                    str_error = str_error & "Please select file!!!"
                Else
                    str_error = str_error & "Please select file!!!"
                End If
                '  returnvalue = False
                ' Return returnvalue
                ShowMessage(str_error, True)
                Exit Sub
            Else
                If Not (errCount = 0 And fileCount = 1) Then
                    ' Dim status As Integer = callSave(BSUID, ActivityID, strans)
                    ' If status = 1000 Then
                    If str_error <> "" Then
                        str_error = str_error & "<br /><br />"
                        str_error = str_error & "Please upload file!"
                    Else
                        str_error = str_error & "Please upload file!"
                    End If
                    ShowMessage(str_error, True)
                    'returnvalue = False
                Else
                    'If str_error <> "" Then
                    '    str_error = str_error & "<br /><br />"
                    '    str_error = str_error & "File saved successfully!"
                    'Else
                    '    str_error = str_error & "File saved successfully!"
                    '    ShowMessage(str_error, True)
                    'End If
                    '   returnvalue = True
                    updpnl.Visible = False
                    lnkShowFile.Visible = True
                    lnkShowFile.Text = UploadfileName
                    Dim uploadpath As String = ("Temp\" & BSUID & "\" & DdlMerit.SelectedItem.Text & "\" & UploadfileName)
                    uploadpath = uploadpath.Replace("\", "\\")
                    Dim strExtn As String = System.IO.Path.GetExtension(UploadfileName).ToLower
                    lnkShowFile.Attributes.Add("OnClick", "javascript: return showDocument('" & uploadpath & "','" & strExtn & "')")
                End If
            End If
        Catch ex As Exception
            Message = getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From saveupload_Click: " + ex.Message, "OASIS ACTIVITY SERVICES")
        End Try
    End Sub

#End Region
    
   




End Class
