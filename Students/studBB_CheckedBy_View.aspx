<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studBB_CheckedBy_View.aspx.vb" Inherits="Students_studBB_CheckedBy_View" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script type="text/javascript" language="javascript">
    Sys.Application.add_load( 
                function CheckForPrint()  
                     {
                       if (document.getElementById('<%= h_print.ClientID %>' ).value!='')
                       { 
                       document.getElementById('<%= h_print.ClientID %>' ).value=''; 
                      showModelessDialog ('../Reports/ASPX Report/rptReportViewerModel.aspx?paging=1', '',"dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;"); 
                       } 
                     } 
                    ); 
                    
    
                
</script>
     <style>
        input{
            vertical-align:middle !important;
        }
    </style>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Blue Book - Checked By Setup
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_ShowScreen" runat="server" align="center" 
                    cellpadding="5" cellspacing="0" width="100%" >

                    <tr >
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>

                    <tr>
                        <td align="center" colspan="8">
                            <table id="Table1" runat="server" align="center" 
                                cellpadding="5"
                                cellspacing="0" width="100%" >
                              

                                <tr>
                                    <td align="left" width="20%">
                                        <asp:Label ID="lblAccText" runat="server" CssClass="field-label" Text="Select Academic Year " ></asp:Label></td>
                                  
                                    <td align="left" width="20%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server"  AutoPostBack="True">
                                        </asp:DropDownList></td>
                                    <td>

                                    </td>
                                     <td>

                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="center" >
                                        <table id="Table2" runat="server" align="center" border="0" 
                                            cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" >
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvStudTutor" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                       CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                        HeaderStyle-Height="30" PageSize="20" BorderStyle="None" OnPageIndexChanging="gvStudTutor_PageIndexChanging" OnSelectedIndexChanging="gvStudTutor_SelectedIndexChanging">
                                                        <RowStyle CssClass="griditem" Wrap="False" />
                                                        <EmptyDataRowStyle Wrap="False" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SEC_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSecid" runat="server" Text='<%# Bind("sec_id") %>' __designer:wfdid="w40"></asp:Label>
                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSctid" runat="server" Text='<%# Bind("sec_sct_id") %>' __designer:wfdid="w41"></asp:Label>
                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrmId" runat="server" Text='<%# Bind("sec_grm_id") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Grade">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Shift">
                                                                <HeaderTemplate>
                                                                   
                                                                                    Shift <br />
                                                                                    <asp:DropDownList ID="ddlgvShift" runat="server" CssClass="listbox"  AutoPostBack="True" OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged" __designer:wfdid="w37">
                                                                                    </asp:DropDownList></td>
                                                                           
                                                                    
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>' __designer:wfdid="w36"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Stream">
                                                                <HeaderTemplate>
                                                                   Stream <br /> <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                                    OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged">
                                                                                </asp:DropDownList>

                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Section">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblH123" runat="server" Text="Section" CssClass="gridheader_text" __designer:wfdid="w33"></asp:Label>
                                                                    <br />
                                                                    <asp:TextBox ID="txtSection" runat="server" __designer:wfdid="w34"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnSection_Search" OnClick="btnSection_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w35"></asp:ImageButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>' __designer:wfdid="w32"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Class Teacher">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblCt" runat="server" Text="Class Teacher" CssClass="gridheader_text" __designer:wfdid="w29"></asp:Label><br />
                                                                    <asp:TextBox ID="txtClassTeacher" runat="server"  __designer:wfdid="w30"></asp:TextBox>
                                                                    <asp:ImageButton ID="btnClassTeacher_Search" OnClick="btnClassTeacher_Search_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w31"></asp:ImageButton>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClassTeacher" runat="server" Text='<%# Bind("emp_name") %>' __designer:wfdid="w28"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TeacherId" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTeacherId" runat="server" Visible="False" Text='<%# Bind("sec_emp_id") %>' __designer:wfdid="w27"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField CommandName="Select" Text="View" HeaderText="View">
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <SelectedRowStyle CssClass="Green" Wrap="False" />
                                                        <HeaderStyle  CssClass="gridheader_pop" Wrap="False" />
                                                        <EditRowStyle Wrap="False" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--<tr>
                      <td align="center"  colspan="3">
                          <asp:Button id="btnprint" runat="server" CssClass="button" OnClick="btnprint_Click"
                              Text="Print" Width="81px" /></td>
                  </tr>--%>
                            </table>
                            <asp:HiddenField ID="h_print" runat="server"></asp:HiddenField>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_8" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div> 

     
         
     
</asp:Content>

