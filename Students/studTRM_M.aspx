<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTRM_M.aspx.vb" Inherits="Students_studTRM_M" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControls/usrMessageBar.ascx" TagPrefix="uc2" TagName="usrMessageBar" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

<%--    <script language="javascript" type="text/javascript">


        function getAcademic(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 445px; ";
            sFeatures += "dialogHeight: 310px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var url;
            url = '../Students/ShowAcademicInfo.aspx?id=' + mode;

            if (mode == 'BY') {
                result = window.showModalDialog(url, "", sFeatures);

                if (result == '' || result == undefined) {
                    return false;
                }
                NameandCode = result.split('___');
                document.getElementById("<%=txtAcadYear.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfACD_ID.ClientID %>").value = NameandCode[1];
            }
        }
    </script>--%>
    <script>
         function getAcademic(mode) {
            var url;
            url = '../Students/ShowAcademicInfo.aspx?id=' + mode;

            if (mode == 'BY') {
                var oWnd = radopen(url, "pop_student");
            }
                             
            
        }

 function OnClientClose1(oWnd, args) {

     //get the transferred arguments
     var arg = args.get_argument();
     if (arg) {

         NameandCode = arg.Acad.split('||');
                document.getElementById("<%=txtAcadYear.ClientID %>").value = NameandCode[0];
                document.getElementById("<%=hfACD_ID.ClientID %>").value = NameandCode[1];
            }
        }

        function autoSizeWithCalendar(oWindow) {
            var iframe = oWindow.get_contentFrame();
            var body = iframe.contentWindow.document.body;

            var height = body.scrollHeight;
            var width = body.scrollWidth;

            var iframeBounds = $telerik.getBounds(iframe);
            var heightDelta = height - iframeBounds.height;
            var widthDelta = width - iframeBounds.width;

            if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
            if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
            oWindow.center();
        }


    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false"
        ReloadOnShow="true" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="pop_student" runat="server" Behaviors="Close,Move" OnClientClose="OnClientClose1"
                OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-money mr-3"></i>
            Term Master
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1" />
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top">
                            <table width="100%">
                                <tr>
                                    <td align="left" class="matters" width="20%"><span class="field-label"> Academic Year</span></td>
                                    <td align="left" class="matters" width="30%">
                                        <asp:TextBox ID="txtAcadYear" runat="server"  ></asp:TextBox>
                                       <%-- <asp:Button ID="btnAcademic" runat="server" CssClass="button" OnClientClick="getAcademic('BY');return false;"
                                            Text="..." />--%>
                                        <asp:ImageButton ID="btnAcademic" runat="server" ImageUrl="~/Images/forum_search.gif" OnClientClick="getAcademic('BY');return false;" />
                                        <asp:RequiredFieldValidator ID="rfvAcadYear" runat="server" ControlToValidate="txtAcadYear"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Academic Year is mandatory"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left" class="matters" width="20%"><span class="field-label">Term Description</span></td>
                                    <td align="left" class="matters"  width="30%">
                                        <asp:TextBox ID="txtTermDesc" runat="server" ></asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTermDesc" CssClass="error"
                                            Display="Dynamic" ErrorMessage="Term Description required" ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" class="title-bg" colspan="4">Term Durations</td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Start Date</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtFrom_date" runat="server" AutoPostBack="True" ></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnFrom_date" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFrom_date"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Start  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvDateFrom" runat="server" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Start Date entered is not a valid date" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvFrom_Date" runat="server" ControlToValidate="txtFrom_date"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Start Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                    <td align="left" class="matters"><span class="field-label">End Date</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtTo_date" runat="server" AutoPostBack="True" ></asp:TextBox>
                                        <asp:ImageButton ID="imgbtnTo_date" runat="server" ImageUrl="~/Images/calendar.gif" /><span style="color: #c00000"> </span>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtTo_date"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter End Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvTodate" runat="server" CssClass="error" Display="Dynamic"
                                            EnableViewState="False" ErrorMessage="End Date entered is not a valid date and must be greater than or equal to Start date"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTo_date"
                                            CssClass="error" Display="Dynamic" ErrorMessage="End Date can not be left empty"
                                            ForeColor="" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                        <asp:LinkButton ID="lnkAddTerm" runat="server" OnClick="lnkAddTerm_Click" ValidationGroup="groupM1">Add</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters"><span class="field-label">Actual Start Date</span></td>
                                    <td align="left" class="matters">
                                        <asp:TextBox ID="txtActualStartDate" runat="server" ></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnActualStart_date" runat="server" ImageUrl="~/Images/calendar.gif" /></td>
                                    <td align="left" class="matters"></td>
                                    <td class="matters" ></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <ajaxToolkit:TabContainer ID="tabSplitups" runat="server" ActiveTabIndex="0">
                                            <ajaxToolkit:TabPanel ID="TabMonths" runat="server" HeaderText="Months">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvDuration" runat="server" CssClass="table table-row table-bordered" AutoGenerateColumns="False" 
                                                        PageSize="5" OnRowDataBound="gvDuration_RowDataBound" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField Visible="False" HeaderText="MonthID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUniqueID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Month">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# bind("Month_Descr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="FROM DATE">
                                                                <HeaderTemplate>
                                                                    Duration
                                                                </HeaderTemplate>

                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtFromDate" runat="server" Text='<%# bind("FROMDATE","{0:dd/MMM/yyyy}") %>'  ></asp:TextBox><asp:ImageButton ID="imgBtnFrom_date" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RegularExpressionValidator ID="REFROMDT" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="Enter From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007" Display="Dynamic" ControlToValidate="txtFromdate" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$">*</asp:RegularExpressionValidator><asp:CustomValidator ID="cvDateFromDT" runat="server" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="From Date entered is not a valid date" Display="Dynamic" ControlToValidate="txtFromDate">*</asp:CustomValidator><asp:RequiredFieldValidator ID="rfvFromDT" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" ErrorMessage="Start Date can not be left empty" Display="Dynamic" ControlToValidate="txtFromdate">*</asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td>-</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtToDate" runat="server" Text='<%# bind("TODATE","{0:dd/MMM/yyyy}") %>'  ></asp:TextBox><asp:ImageButton ID="imgBtnTodate" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><asp:RegularExpressionValidator ID="RETODT" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="Enter To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007" Display="Dynamic" ControlToValidate="txtToDate" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$">*</asp:RegularExpressionValidator><asp:CustomValidator ID="cvTODT" runat="server" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="To Date entered is not a valid date" Display="Dynamic" ControlToValidate="txtToDate">*</asp:CustomValidator><asp:RequiredFieldValidator ID="rfvTODT" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" ErrorMessage="To Date can not be left empty" Display="Dynamic" ControlToValidate="txtToDate">*</asp:RequiredFieldValidator><asp:CompareValidator ID="CVToDate" runat="server" ValidationGroup="groupM1" ErrorMessage="To Date should be greater than From Date" Display="Dynamic" ControlToValidate="txtFromDate" ControlToCompare="txtToDate" Type="Date" Operator="GreaterThan" Enabled="False">*</asp:CompareValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <ajaxToolkit:CalendarExtender ID="CEFromDate" runat="server" CssClass="MyCalendar" TargetControlID="txtFromDate" PopupButtonID="txtFromDate" Format="dd/MMM/yyyy"></ajaxToolkit:CalendarExtender>
                                                                    <ajaxToolkit:CalendarExtender ID="CBEFromDate" runat="server" CssClass="MyCalendar" TargetControlID="txtFromDate" PopupButtonID="imgBtnFrom_date" Format="dd/MMM/yyyy"></ajaxToolkit:CalendarExtender>
                                                                    <ajaxToolkit:CalendarExtender ID="CEToDate" runat="server" CssClass="MyCalendar" TargetControlID="txtToDate" PopupButtonID="txtToDate" Format="dd/MMM/yyyy"></ajaxToolkit:CalendarExtender>
                                                                    <ajaxToolkit:CalendarExtender ID="CBEToDate" runat="server" CssClass="MyCalendar" TargetControlID="txtToDate" PopupButtonID="imgBtnTodate" Format="dd/MMM/yyyy"></ajaxToolkit:CalendarExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem" Height="25px" />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />

                                                    </asp:GridView>
                                                </ContentTemplate>
                                                <HeaderTemplate>
                                                    Months
                                                </HeaderTemplate>
                                            </ajaxToolkit:TabPanel>
                                            <ajaxToolkit:TabPanel ID="Weeks" runat="server" HeaderText="Weeks">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvWeek" runat="server" CssClass="table table-row table-bordered" AutoGenerateColumns="False" PageSize="5" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField Visible="False" HeaderText="MonthID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUniqueID" runat="server" Text='<%# bind("UniqueID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Week Count" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWeekID" runat="server" Text='<%# bind("WEEK_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Week Descr">
                                                                <HeaderTemplate>
                                                                    Week ID
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWeekDescr" runat="server" Text='<%# bind("WEEK_DESCR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="FROM DATE">
                                                                <HeaderTemplate>
                                                                    Duration
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtFromDate" runat="server" Text='<%# bind("FROMDATE","{0:dd/MMM/yyyy}") %>'  ></asp:TextBox><asp:ImageButton ID="imgBtnFrom_date" runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton><asp:RegularExpressionValidator ID="REFROMDT" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="Enter From Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007" Display="Dynamic" ControlToValidate="txtFromdate" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$">*</asp:RegularExpressionValidator><asp:CustomValidator ID="cvDateFromDT" runat="server" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="From Date entered is not a valid date" Display="Dynamic" ControlToValidate="txtFromDate">*</asp:CustomValidator><asp:RequiredFieldValidator ID="rfvFromDT" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" ErrorMessage="Start Date can not be left empty" Display="Dynamic" ControlToValidate="txtFromdate">*</asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td>-</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtToDate" runat="server" Text='<%# bind("TODATE","{0:dd/MMM/yyyy}") %>' ></asp:TextBox><asp:ImageButton ID="imgBtnTodate" runat="server" ImageUrl="~/Images/calendar.gif" OnClientClick="return false;"></asp:ImageButton><asp:RegularExpressionValidator ID="RETODT" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="Enter To Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007" Display="Dynamic" ControlToValidate="txtToDate" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$">*</asp:RegularExpressionValidator><asp:CustomValidator ID="cvTODT" runat="server" ValidationGroup="groupM1" EnableViewState="False" ErrorMessage="To Date entered is not a valid date" Display="Dynamic" ControlToValidate="txtToDate">*</asp:CustomValidator><asp:RequiredFieldValidator ID="rfvTODT" runat="server" ForeColor="" CssClass="error" ValidationGroup="groupM1" ErrorMessage="To Date can not be left empty" Display="Dynamic" ControlToValidate="txtToDate">*</asp:RequiredFieldValidator><asp:CompareValidator ID="CVToDate" runat="server" ValidationGroup="groupM1" ErrorMessage="To Date should be greater than From Date" Display="Dynamic" ControlToValidate="txtFromDate" ControlToCompare="txtToDate" Type="Date" Operator="GreaterThan" Enabled="False">*</asp:CompareValidator>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkAddWeek" runat="server" OnClick="lnkAddWeek_Click">Add</asp:LinkButton></td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click">Delete</asp:LinkButton></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <ajaxToolkit:CalendarExtender ID="CEFromDate" runat="server" CssClass="MyCalendar" TargetControlID="txtFromDate" PopupButtonID="txtFromDate" Format="dd/MMM/yyyy"></ajaxToolkit:CalendarExtender>
                                                                    <ajaxToolkit:CalendarExtender ID="CBEFromDate" runat="server" CssClass="MyCalendar" TargetControlID="txtFromDate" PopupButtonID="imgBtnFrom_date" Format="dd/MMM/yyyy"></ajaxToolkit:CalendarExtender>
                                                                    <ajaxToolkit:CalendarExtender ID="CEToDate" runat="server" CssClass="MyCalendar" TargetControlID="txtToDate" PopupButtonID="txtToDate" Format="dd/MMM/yyyy"></ajaxToolkit:CalendarExtender>
                                                                    <ajaxToolkit:CalendarExtender ID="CBEToDate" runat="server" CssClass="MyCalendar" TargetControlID="txtToDate" PopupButtonID="imgBtnTodate" Format="dd/MMM/yyyy"></ajaxToolkit:CalendarExtender>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="griditem" Height="25px" />
                                                        <SelectedRowStyle CssClass="griditem_hilight" />
                                                        <HeaderStyle CssClass="gridheader_new" Height="25px" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                                <HeaderTemplate>
                                                    Weeks
                                                </HeaderTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" align="center" >
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Cancel" UseSubmitBehavior="False" />
                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                                Text="Delete" /></td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <asp:HiddenField ID="hfACD_ID" runat="server" />
                            <asp:HiddenField ID="hfTRM_ID" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CETFromDate"
                                runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="txtFrom_date"
                                TargetControlID="txtFrom_date">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CBTEFromDate" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgBtnFrom_date" TargetControlID="txtFrom_date">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CETToDate" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="txtTo_date" TargetControlID="txtTo_date">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CBETToDate" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgbtnTo_date" TargetControlID="txtTo_date">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CBERActualStartDate" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgBtnActualStart_date" TargetControlID="txtActualStartDate">
                            </ajaxToolkit:CalendarExtender>



                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <uc2:usrMessageBar runat="server" ID="usrMessageBar" />
    </div>

</asp:Content>

