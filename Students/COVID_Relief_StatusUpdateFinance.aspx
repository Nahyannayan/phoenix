﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="COVID_Relief_StatusUpdateFinance.aspx.vb" Inherits="Students_COVID_Relief_StatusUpdateFinance" %>

<html>

<body>
    <form id="frm1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </ajaxToolkit:ToolkitScriptManager>
                <link href='<%= ResolveUrl("~/vendor/bootstrap/css/bootstrap.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/cssfiles/sb-admin.css")%>' rel="stylesheet" />
        <link href='<%= ResolveUrl("~/vendor/datatables/dataTables.bootstrap4.css")%>' rel="stylesheet" />
        <script src='<%= ResolveUrl("~/vendor/jquery/jquery.min.js")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.pack.js?1=2")%>'></script>
        <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.js?1=2")%>'></script>
        <link type="text/css" href='<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.css?1=2")%>' rel="stylesheet" />
        <div class="title-bg">Status Update
          </div>
        <div>
            <asp:Label ID="errLbl" runat="server" CssClass="text-danger" ></asp:Label>
        </div>
        <div class="col-12 row">
            <%--<div class="col-4 col-sm-4">
              Log Date:
                  <div>
                      <asp:TextBox runat="server" CssClass="form-control" ID="txtLogDate"></asp:TextBox></div>
            </div>--%>
            <div class="col-8 col-sm-8">

                <table style="width: 100%;">

                        <tr>
                    <td align="left" style="width: 30%;"><span class="field-label">Current Status</span>
                    </td>

                    <td align="left" width="70%">

                        <asp:Label ID="CurrStatusLbl" runat="server"  ></asp:Label>
                     </td>                                           
                                                               
                    </tr>

                     

                <tr>
                    <td align="left" style="width: 30%;"><span class="field-label">Fee Structure : </span>
                    </td>

                    <td align="left" width="70%">

                          <asp:Label ID="FeeLbl" runat="server"  ></asp:Label>

                     </td>                                           
                                                               
                   
                </tr>

                           <tr>
                    <td align="left" style="width: 30%;"><span class="field-label">Concession Type:</span>
                    </td>

                    <td align="left" width="70%">

                        
                          <asp:Label ID="ConTlbl" runat="server"  ></asp:Label>
                     </td>                                           
                                                               
                   
                </tr>

                   

                           <tr>
                    <td align="left" style="width: 30%;"><span class="field-label">Concession Amount:</span>
                    </td>

                    <td align="left" width="70%">

                        
                          <asp:Label ID="ConAmtlbl" runat="server"  ></asp:Label>
                     </td>                                           
                                                               
                   
                </tr>

                
                    

                           <tr>
                    <td align="left" style="width: 30%;"><span class="field-label">PRE Comments:</span>
                    </td>

                    <td align="left" width="150%">
                        
                        <asp:TextBox ID="PreComments" runat="server" CssClass="form-control" Enabled="false" TextMode="MultiLine" Width="100%" Rows="3"> 
                  </asp:TextBox>
                     </td>                                           
                                                               
                   
                </tr>
                    
                     <tr>
                    <td align="left" style="width: 30%;"><span class="field-label">Qualify for discount:</span>
                    </td>

                    <td align="left" width="100%">
                        <asp:RadioButtonList ID ="radioBtnList" runat="server" AutoPostBack="true" RepeatColumns="2" OnSelectedIndexChanged ="radioBtnList_SelectedIndexChanged" >
                            <asp:ListItem Text="Yes" Value="Yes" />
                              <asp:ListItem Text="No" Selected="True" Value="No"  />
                        </asp:RadioButtonList>
                         <%--<asp:RadioButton ID="YesRadioBtn"  runat="server" Text="Yes" GroupName="Discount" />  
                         <asp:RadioButton ID="NoRadioBtn" Checked="true"  runat="server" Text="No" GroupName="Discount" />  --%>
                     </td>                                           
                                                               
                   
                </tr>

                    <tr id ="trRelQalNo" runat="server" visible ="true">
                    <td align="left" style="width: 30%;"><span class="field-label">Rejection Reason</span>
                    </td>

                    <td align="left" width="70%">

                        <asp:DropDownList ID="ddlStatusRelQualNo" CssClass="form-control" runat="server" class="form-control" />
                     </td>                                           
                                                               
                    </tr>

                      <tr id ="trRelQalYes" runat="server" visible ="false">
                    <td align="left" style="width: 30%;"><span class="field-label">Relief Qualified Under</span>
                    </td>

                    <td align="left" width="70%">

                        <asp:DropDownList ID="ddlStatusRelQualYes" CssClass="form-control" AutoPostBack="true" runat="server" class="form-control"  OnSelectedIndexChanged="ddlStatusRelQualYes_SelectedIndexChanged" />
                     </td>                                           
                                                               
                    </tr>

                    <tr id="trDis" runat="server" visible="false">
                        <td align="left" style="width: 30%;"><span class="field-label">Discount(%)</span>
                        </td>

                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDiscount" runat="server" CssClass="form-control" Width="40%" Enabled="false" />
                        </td>
                        <td align="left" width="60%">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                ControlToValidate="txtDiscount"
                                ErrorMessage="Only numeric allowed." ForeColor="Red"
                                ValidationExpression="^[0-9]*$" ValidationGroup="NumericValidate">*
                            </asp:RegularExpressionValidator>
                        </td>

                    </tr>

                    <tr id="trDiscountAmt" runat="server" visible="false">
                        <td align="left" style="width: 30%;"><span class="field-label">Discount Amount</span>
                        </td>
                        <td align="left" width="30%">
                            <asp:TextBox ID="txtDiscountAmount" runat="server" CssClass="form-control" Width="40%" Enabled="false"/>
                        </td>
                        <td align="left" width="60%"></td>
                    </tr>

                    <tr>
                    <td align="left" style="width: 30%;"><span class="field-label">Status</span>
                    </td>

                    <td align="left" width="70%">

                        <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server" class="form-control" />
                     </td>                                           
                                                               
                    </tr>
                      </table>
               
            </div>
            
           
          
          
            
            <div class="col-4 col-sm-4"> 
            </div>
        </div>
        <div class="col-12 mt-3">
           Comment <span style="color:red">*</span>:
             <div><asp:TextBox ID="txtComments" runat="server" CssClass="form-control" TextMode="MultiLine" Width="100%" Rows="4"> 
                  </asp:TextBox></div>
        </div>

      
        <div class="col-12">
           <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Update" OnClick="btnSave_Click" OnClientClick="return validateSave();" />
        </div> 

    



        <script>
            function validateSave() {
                if ($("#txtComments").val() == "") {
                    alert("Please enter the comment");
                    return false;
                }
                return true;
            }

            function GetDiscount(obj) {

                if (parseFloat($(obj).val()) > 0) {

                    var amt1 = parseFloat($("#FeeLbl").html()) - parseFloat($("#ConAmtlbl").html());

                    $("#txtDiscountAmount").val(((amt1 * parseFloat($(obj).val())) / 100));
                } else {
                     $("#txtDiscountAmount").val(0);
                }
            }
        </script>
    </form>
</body>
</html>
