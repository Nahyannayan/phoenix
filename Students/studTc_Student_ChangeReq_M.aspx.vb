Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studTc_Student_ChangeReq_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))



            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100256") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))




                hfTCM_ID.Value = 0
                hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                txtGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                txtSection.Text = Encr_decrData.Decrypt(Request.QueryString("section").Replace(" ", "+"))
                txtName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                txtStudID_Fee.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                txtDoJ.Text = Format(Date.Parse(Encr_decrData.Decrypt(Request.QueryString("doj").Replace(" ", "+"))), "dd/MMM/yyyy")
                BindData()
                BindTCType()
                txtReqDate.Text = Format(Now.Date, "dd/MMM/yyyy")
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub
#Region "Private Methods"

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCM_REFNO,TCM_APPLYDATE,TCM_LEAVEDATE," _
                                 & " TCM_LASTATTDATE,TCM_ID,TCT_CODE,TCT_DESCR,isnull(TCM_REQTYPE,'') as TCM_REQTYPE FROM " _
                                 & " TCM_M AS A INNER JOIN TC_TFRTYPE_M AS B " _
                                 & " ON A.TCM_TCTYPE=B.TCT_CODE" _
                                 & " WHERE TCM_STU_ID=" + hfSTU_ID.Value _
                                 & " AND TCM_CANCELDATE IS NULL AND TCM_TCSO='TC'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            With reader
                txtRefNo.Text = .GetString(0)
                txtApplyDate.Text = Format(.GetDateTime(1), "dd/MMM/yyyy")
                txtLeave_Date.Text = Format(.GetDateTime(2), "dd/MMM/yyyy")
                txtLast_Attend.Text = Format(.GetDateTime(3), "dd/MMM/yyyy")
                hfTCM_ID.Value = .GetValue(4)
                hfTCT_CODE.Value = reader.GetValue(5)
                txtTCtype.Text = reader.GetString(6)
                If Convert.ToString(reader("TCM_REQTYPE")) = "PR" Then
                    rbParentRequest.Checked = True
                    rbDataEntry.Checked = False
                Else
                    rbDataEntry.Checked = True
                    rbParentRequest.Checked = False
                End If
            End With
        End While
        reader.Close()
    End Sub
    'Sub BindTCType()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String
    '    str_query = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [OASIS].[dbo].[TC_TFRTYPE_M] WHERE TCT_CODE<>" + hfTCT_CODE.Value + " order by [TCT_DESCR]"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlTransferType.DataSource = ds
    '    ddlTransferType.DataTextField = "TCT_DESCR"
    '    ddlTransferType.DataValueField = "TCT_CODE"
    '    ddlTransferType.DataBind()
    'End Sub

    Sub BindTCType()
        'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String
        'If Session("SBSUID") <> "800017" Then
        '    str_query = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [OASIS].[dbo].[TC_TFRTYPE_M] order by [TCT_DESCR]"
        'Else
        '    str_query = "SELECT [TCT_CODE],[TCT_DESCCR_AQABA]  as TCT_DESCR FROM [OASIS].[dbo].[TC_TFRTYPE_M] WHERE TCT_DESCCR_AQABA IS NOT NULL order by [TCT_DESCR]"
        'End If

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim conn_str As String = ConnectionManger.GetOASISConnectionString

        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sUsr_name")

        pParms(1) = New SqlClient.SqlParameter("@SBSUID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("SBSUID")

        pParms(2) = New SqlClient.SqlParameter("@TCTCODE", SqlDbType.VarChar, 20)
        pParms(2).Value = hfTCT_CODE.Value

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn_str, CommandType.StoredProcedure, "Get_TC_FORMATS_CHANGE", pParms)




        ddlTransferType.DataSource = ds
        ddlTransferType.DataTextField = "TCT_DESCR"
        ddlTransferType.DataValueField = "TCT_CODE"
        ddlTransferType.DataBind()
    End Sub
    Function SaveData() As Boolean
        Dim REQTYPE As String = String.Empty
        If rbDataEntry.CHECKED = True Then
            REQTYPE = "DEM"
        ElseIf rbParentRequest.CHECKED = True Then
            REQTYPE = "PR"
        End If


        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " saveSTUDENTTCCHANGEREQUEST " _
                                 & hfSTU_ID.Value + "," _
                                 & hfTCM_ID.Value + "," _
                                 & "'" + ddlTransferType.SelectedValue.ToString + "'," _
                                 & "'" + Format(Date.Parse(txtReqDate.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + txtReason.Text + "'," _
                                 & "'" + REQTYPE + "'"
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                UtilityObj.InsertAuditdetails(transaction, "edit", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + hfTCM_ID.Value.ToString, "TCM_M_CHANGE_TCTYPE_REQ")

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using


    End Function

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub
End Class
