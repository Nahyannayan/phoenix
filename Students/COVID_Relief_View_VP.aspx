<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="COVID_Relief_View_VP.aspx.vb" Inherits="COVID_Relief_View_VP" Title="Untitled Page" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .page-item.active .page-link {
            background-color: #8dc24c !important;
            border-color: gray !important;
        }
    </style>
    <style type="text/css">
        .gridfont {
            font-size: 7pt !important;
        }

        table th, table td {
            padding: 0.1em !important;
        }

        tr.gridheader_pop {
            font-size: 125%;
            background-color: rgba(0, 0, 0, 0.075);
        }

        .griditem, .griditem_alternative {
            font-size: 120%;
        }

        .clsTd {
            /*font-weight:bold;*/
            color: darkblue;
        }

        .modalWait {
            display: none;
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) 50% 50% no-repeat;
        }

        .clsTDBorder {
            border: hidden !important;
        }
    </style>
    <script lang="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript"></script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Covid Relief Application Approval"></asp:Literal>
        </div>
        <div class="card-body">
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            </div>
            <div class="mb-3">
                <table style="width: 100%;">
                    <tr>
                        <td align="left" style="width: 10%;"><span class="field-label">Status</span>
                        </td>

                        <td align="left" width="20%">
                            <asp:DropDownList ID="ddlStatus" runat="server" class="form-control" OnSelectedIndexChanged="statusOnSelectedIndexChanged" AutoPostBack="true" />
                        </td>

                        <td align="left" style="width: 10%;"><span class="field-label ml-4">School</span>
                        </td>

                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlSchool" runat="server" Width="80%" class="form-control" OnSelectedIndexChanged="schoolOnSelectedIndexChanged" AutoPostBack="true" />
                        </td>
                        <td align="right" width="10%">
                            <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary d-none"
                                OnClientClick="return OpenModal();" Text="Approve/Reject" />
                        </td>

                    </tr>
                </table>

            </div>
            <div>
                <div>
                    <table id="dataTableNew" class="table table-hover table-bordered  table-responsive" style="width: 100%">
                        <thead>
                            <tr style="background-color:lightgray;">
                                <th align="center" style="vertical-align:middle;">
                                    <input type="checkbox" id="checkAll" onclick="checkAllChk(this)" class="checkAll" runat="server" /></th>
                                <th align="center" style="vertical-align:middle;">Action</th>
                                <th align="center" style="vertical-align:middle;">School</th>
                                <th align="center" style="vertical-align:middle;">Father Details</th>
                                <th align="center" style="vertical-align:middle;">Mother Details</th>

                                <th align="center" style="vertical-align:middle;">Note By Parent</th>
                                <th align="center" style="vertical-align:middle;">Fees/Discount</th>
                                <th align="center" style="vertical-align:middle;">Reason for Approval / Rejection / FBP Comments</th>
                                <th align="center" style="vertical-align:middle;"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>

            </div>

        </div>
    </div>
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Approve/Reject</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="cl-12">
                        Status:
           <div>
               <asp:DropDownList ID="ddlStatusPopup" runat="server" class="form-control" />
           </div>
                    </div>
                    <div class="cl-12">
                        Comment:
           <div>
               <asp:TextBox ID="textCommentPopup" runat="server" class="form-control" Rows="4" TextMode="MultiLine" />
           </div>
                    </div>
                    <div class="mt-2">
                        <asp:LinkButton ID="btnSavePopup" runat="server" CssClass="btn btn-primary"
                          OnClientClick="return SetRRH_ID();" >Save</asp:LinkButton>

                    </div>
                </div>


            </div>
        </div>
    </div>
    <div>
        <asp:HiddenField ID="hdnRRH_ID" runat="server" Value="0" />
    </div>
    <script>
        function SetRRH_ID() {
           // alert($("#ctl00_cphMasterpage_hdnRRH_ID").val());
            if ($("#ctl00_cphMasterpage_hdnRRH_ID").val() == "0") { 
                var rrh_id_all = '';
                $(".chkBox").each(function () {
                    if ($($(this)[0]).prop("checked")) {
                        rrh_id_all = rrh_id_all + ',' + $($(this)[0]).attr("data-val");
                    }
                });
                $("#ctl00_cphMasterpage_hdnRRH_ID").val(rrh_id_all.substring(1));
            }

            SaveDataRRH()

            return false;
        }

        function SaveDataRRH() {
             
            $.ajax({
                type: "POST",
                url: "<%= Page.ResolveUrl("~/students/COVID_Relief_View_VP.aspx/SaveDataRRH")%>",
                data: JSON.stringify({
                    "hdnRRH_ID": $("#ctl00_cphMasterpage_hdnRRH_ID").val(),
                    "ddlStatusPopup": $("#ctl00_cphMasterpage_ddlStatusPopup").val(),
                    "textCommentPopup":$("#ctl00_cphMasterpage_textCommentPopup").val()
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {                     
                    ClosedAll();
                },
                error: function (xh) {
                    alert("Error: " + xh);
                }
            });
            return false;
        }
         function ClosedAll() {
           
            alert('Data Saved Sucessfully');
            //$('#myModal').modal('hide');
            $("#myModal .close").click()
            $("#ctl00_cphMasterpage_btnSave").addClass("d-none");
            $("#ctl00_cphMasterpage_hdnRRH_ID").val("0");
            $("#ctl00_cphMasterpage_textCommentPopup").val(""); 
            SetDataTable_New();
        }
        function OpenPopupInd(rrh_id) {
            $("#ctl00_cphMasterpage_hdnRRH_ID").val(rrh_id);
            $("#myModal").modal("show");
            return false;
        }
        function OpenModal() {
            $("#ctl00_cphMasterpage_hdnRRH_ID").val("0");
            $("#myModal").modal("show");
            return false;
        }

        function checkAllChk(obj) {
            //alert($(obj).prop("checked"));

            $(".chkBox").each(function () {
                if ($(obj).prop("checked")) {
                    $($(this)[0]).prop("checked", true)
                    $($(this).closest("tr").find('.btn-primary')[0]).addClass("d-none")
                    $("#ctl00_cphMasterpage_btnSave").removeClass("d-none");
                } else {
                    $($(this)[0]).prop("checked", false)
                    $($(this).closest("tr").find('.btn-primary')[0]).removeClass("d-none")
                    $("#ctl00_cphMasterpage_btnSave").addClass("d-none");
                }
            });
        };

        function CheckButton(obj, rrh_id) {
            var isCk = 0;
            $(".chkBox").each(function () {
                if ($($(this)[0]).prop("checked")) {
                    isCk = 1;
                    $($(this).closest("tr").find('.btn-primary')[0]).addClass("d-none")

                } else {
                    $($(this).closest("tr").find('.btn-primary')[0]).removeClass("d-none")
                }
            });

            if (isCk == 1) {
                $("#ctl00_cphMasterpage_btnSave").removeClass("d-none");
            } else {
                $("#ctl00_cphMasterpage_btnSave").addClass("d-none");
            }

            return false;
        };


    </script>

    <script>
        $(document).ready(function () {
            //setFacnyBox();
            SetDataTable_New();

        });

        function SetDataTable_New() {
            //setFacnyBox();
            $(".checkAll").prop("checked", false);

            $("#dataTableNew").DataTable(
                {
                    "createdRow": function (row, data, dataIndex) {
                                    $(row).css("background-color", data["BG_COLOR"]);
                                    $(row).attr("title", data["COLOR_DESC"]);
                                  },
                    "responsive": true,
                    "destroy": true,
                    "stateSave": true,
                     "order": [[3, "asc" ]],
                    "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
                    "columnDefs": [
                        {
                            "targets": 0,
                            "orderable": false
                        },
                        {
                            "targets": 1,
                            "orderable": false
                        },
                        {
                            "targets": 5,
                            "width": "10%"
                        },
                        {
                            "targets": 6,
                            "width": "20%"
                        },
                        {
                            "targets": 7,
                            "width": "10%"
                        },
                        {
                            "targets": 8,
                            "orderable": false
                        }
                    ],
                    "language":
                    {
                        "processing": "Please wait...."
                    },
                    "processing": true,
                    "serverSide": true,
                    "ajax":
                    {
                        "url":'<%= Page.ResolveUrl("~/Students/COVID_Relief_View.aspx/GetData")%>',
                        "contentType": "application/json",
                        "type": "GET",
                        "dataType": "JSON",
                        "data": function (d) {
                            return d;
                        },
                        "dataSrc": function (json) {
                            json.draw = json.d.draw;
                            json.recordsTotal = json.d.recordsTotal;
                            json.recordsFiltered = json.d.recordsFiltered;
                            json.data = json.d.data;
                            var return_data = json;
                            return return_data.data;
                        },
                        "error": function (xhr, error, code) {
                            console.log(xhr);
                            console.log(code);
                        }
                    },
                    "columns": [
                        {
                            "render": function (data, type, full, meta) {
                                var htm1 = ''
                                if (full.RRH_STATUS == '10' || full.RRH_STATUS == '11'||full.RRH_STATUS == '14' || full.RRH_STATUS == '15') {
                                    htm1 = "<input type='checkbox' onclick='CheckButton(this," + full.RRH_ID + ")' class='chkBox' data-val='"+full.RRH_ID+"'/>";
                                }
                                return htm1;
                            }
                        },
                        {
                            "render": function (data, type, full, meta) {
                                var htm1 = "<div>";
                                htm1 = htm1 + "<a href='#' datahRef='COVID_Relief_StudentDetail.aspx?MainMnu_code=S050002&RRH_ID=" + full.RRH_ID_ENCR + "' onclick='return OpenPopup(this)'>Details</a>";
                                htm1 = htm1 + "<br/><a  href='#' datahRef='COVID_Relief_Attachments.aspx?MainMnu_code=S050002&RRH_ID=" + full.RRH_ID_ENCR + "' onclick='return OpenPopup(this)'>Attachment</a>";
                                htm1 = htm1 + "<br/><a  href='#' datahRef='Covid_Relief_Parentportal.aspx?MainMnu_code=S050002&RRHID=" + full.RRH_ID_ENCR + "' onclick='return OpenPopup(this)'>View Form </a>";
                                htm1 = htm1 + "</div>";
                                return htm1;
                            }
                        },
                        { "data": "BSU" },
                        {
                            "render": function (data, type, full, meta) {
                                var htm1 = "<table width='100%' class='clsTDBorder'>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Name:</td><td class='clsTDBorder'>" + full.FatherName + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Type/Relief Reason:</td><td class='clsTDBorder'>" + full.FatherEmploymentType + "/" + full.FatherReliefReason + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Company:</td><td class='clsTDBorder'>" + full.FatherCompanyName + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Salary:</td><td class='clsTDBorder'>" + full.F_TOT_SALARY + "</td></tr>";
                                htm1 = htm1 + "</table>";
                                return htm1;
                            }
                        },
                        {
                            "render": function (data, type, full, meta) {
                                var htm1 = "<table width='100%'>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Name:</td><td class='clsTDBorder'>" + full.MotherName + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Type/Relief Reason:</td><td class='clsTDBorder'>" + full.MotherEmploymentType + "/" + full.MotherReliefReason + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Company:</td><td class='clsTDBorder'>" + full.MotherCompanyName + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Salary:</td><td class='clsTDBorder'>" + full.M_TOT_SALARY + "</td></tr>";
                                htm1 = htm1 + "</table>";
                                return htm1;
                            }
                        },
                        {
                            // "data": "RRH_REMARKS"
                            "render": function (data, type, full, meta) {
                                var htm1 = "";
                                if (full.RRH_REMARKS.length > 120) {
                                    htm1 = htm1 + "<p title='" + full.RRH_REMARKS + "'>" + full.RRH_REMARKS.substring(0, 119) + "..</p>";
                                } else {
                                    htm1 = htm1 + "<p title='" + full.RRH_REMARKS + "'>" + full.RRH_REMARKS + "</p>";
                                }
                                return htm1;
                            }
                        },
                        {
                            "render": function (data, type, full, meta) {
                                var htm1 = "<table width='100%'>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Due Previous Term :</td><td class='clsTDBorder'>" + full.Due_PreviousTerm + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Due Apr-Jun,Fee:</td><td class='clsTDBorder'>" + full.Due_AprJun + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Amount:</td><td class='clsTDBorder'>" + full.TermlyFee + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Concession Amount:</td><td class='clsTDBorder'>" + full.Current_Term_Concession + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Disc %:</td><td class='clsTDBorder'>" + full.DiscountPerc + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>Disc Amt:</td><td class='clsTDBorder'>" + full.DiscountAmount + "</td></tr>";
                                htm1 = htm1 + "</table>";
                                return htm1;  //,,, 
                            }
                        },
                        {
                            "render": function (data, type, full, meta) {
                                var htm1 = "<table width='100%'>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder'>" + full.APPROVE_OR_REJECT_REASON + "</td></tr>";
                                htm1 = htm1 + "<tr class='clsTDBorder'><td class='clsTDBorder clsTd'>" + full.FBP_COMMENTS + "</td></tr>";
                                htm1 = htm1 + "</table>";
                                return htm1;  //,,, 
                            }
                        },
                        {
                            "render": function (data, type, full, meta) {
                                var htm1 = ''
                                if (full.RRH_STATUS == '10' || full.RRH_STATUS == '11'||full.RRH_STATUS == '14' || full.RRH_STATUS == '15') {
                                    htm1 = "<button class='btn btn-primary' onclick='return OpenPopupInd(" + full.RRH_ID + ");'>Approve/Reject</button>";
                                }
                                return htm1;
                            }
                        }
                    ]
                });
        }

        function OpenPopup(obj) {
            var url1 = $(obj).attr("datahRef");
            $.fancybox({
                type: 'iframe',
                href: url1,
                fitToView: true,
                autoSize: true,
                width: "70%",
                height: "70%",
                //closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                'afterClose': function () {
                    //alert("sdkasldj");
                    SetDataTable_New();
                }
            });
            return false;
        }

       
    </script>
</asp:Content>

