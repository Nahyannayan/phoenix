Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data

Partial Class Students_stud_offer_create
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function IsPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Page.MaintainScrollPositionOnPostBack = True
        tr_Old.Visible = False
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave2)
        If Page.IsPostBack = False Then

            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "edit"
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050177") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    
                    rbOffer.Checked = True


                    Call Offer_letter()




                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If


    End Sub
    Private Function GetOffer_LetterDATA() As SqlDataReader
        'Author(--Lijo)
        'Date   --19/nov/2008
        'Purpose--Get PATTERN data from BSU_APPL_LETTER

        Dim sqlOffer_LetterDATA As String = String.Empty
        If rbOffer.Checked = True Then
            sqlOffer_LetterDATA = " Select BSU_STUD_OFFRLETTER as TEXT_CONTENT  From BusinessUnit_Sub  " & _
           "  where BUS_BSU_ID='" & Session("sBsuid") & "'"
        ElseIf rbTerms.Checked = True Then
            sqlOffer_LetterDATA = " Select BSU_OFFER_TNC as TEXT_CONTENT  From BusinessUnit_Sub  " & _
"  where BUS_BSU_ID='" & Session("sBsuid") & "'"
        End If



        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlOffer_LetterDATA, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Sub Offer_letter()
        Try
            Using readerOFFER_LETTER As SqlDataReader = GetOffer_LetterDATA()

                If readerOFFER_LETTER.HasRows = True Then
                    While readerOFFER_LETTER.Read
                        txtOfferText.Content = Convert.ToString(readerOFFER_LETTER("TEXT_CONTENT"))
                        '  txtOfferText.Text = Convert.ToString(readerOFFER_LETTER("BSU_STUD_OFFRLETTER"))

                       

                    End While
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try



    End Sub
    Protected Sub rbTerms_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTerms.CheckedChanged
        Call Offer_letter()
    End Sub

    Protected Sub rbOffer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbOffer.CheckedChanged
        Call Offer_letter()
    End Sub
    Sub loadMatter_Dropdown()
        Using MatterPattern_reader As SqlDataReader = GetMatter_Pattern()

            Dim MatterPattern As ListItem
            ddlMatter.Items.Clear()

            If MatterPattern_reader.HasRows = True Then
                While MatterPattern_reader.Read
                    MatterPattern = New ListItem(MatterPattern_reader("ILP_DESC"), MatterPattern_reader("ILP_PATTERN"))
                    ddlMatter.Items.Add(MatterPattern)

                End While

            End If
        End Using
    End Sub
    Sub loadPattern_Dropdown()
        Using Pattern_reader As SqlDataReader = GetLetter_Pattern()

            ddlRemark.Items.Clear()
            ddlSign.Items.Clear()
            ddlParent.Items.Clear()
            If Pattern_reader.HasRows = True Then
                While Pattern_reader.Read
                    'Pattern = New ListItem(Pattern_reader("BLP_DESC"), Pattern_reader("BLP_PATTERN"))
                    ddlRemark.Items.Add(New ListItem(Pattern_reader("ILP_DESC"), Pattern_reader("ILP_PATTERN")))
                    ddlSign.Items.Add(New ListItem(Pattern_reader("ILP_DESC"), Pattern_reader("ILP_PATTERN")))
                    ddlParent.Items.Add(New ListItem(Pattern_reader("ILP_DESC"), Pattern_reader("ILP_PATTERN")))
                End While
                'ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using
    End Sub
    Sub LoadPattern_Radio()
        Using Pattern_Radio_reader As SqlDataReader = GetBody_Tag()

            '   Dim Pattern_Radio As ListItem
            rbMatter.Items.Clear()
            rbParent.Items.Clear()
            rbRemark.Items.Clear()
            If Pattern_Radio_reader.HasRows = True Then
                While Pattern_Radio_reader.Read

                    rbMatter.Items.Add(New ListItem(Pattern_Radio_reader("ILP_DESC"), Pattern_Radio_reader("ILP_ID")))
                    rbParent.Items.Add(New ListItem(Pattern_Radio_reader("ILP_DESC"), Pattern_Radio_reader("ILP_ID")))
                    rbRemark.Items.Add(New ListItem(Pattern_Radio_reader("ILP_DESC"), Pattern_Radio_reader("ILP_ID")))
                End While
                'ddlCurri_SelectedIndexChanged(ddlCurri, Nothing)
            End If
        End Using
    End Sub
    Private Function GetMatter_Pattern() As SqlDataReader

        Dim sqlMatter_Pattern As String = "SELECT ILP_DESC ,ILP_PATTERN FROM STU.INDEMNITY_LETTER_PATTERN  where ILP_bBODY_TAG=0  order by ILP_DISP_ORDER"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlMatter_Pattern, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Private Function GetLetter_Pattern() As SqlDataReader

        Dim sqlLetter_Pattern As String = "SELECT ILP_DESC ,ILP_PATTERN FROM STU.INDEMNITY_LETTER_PATTERN  where ILP_bFORMAT=1 and  ILP_bBODY_TAG=0  order by ILP_DISP_ORDER"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlLetter_Pattern, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Private Function GetBody_Tag() As SqlDataReader
        Dim sqlLetter_Pattern As String = "SELECT ILP_DESC ,ILP_ID FROM STU.INDEMNITY_LETTER_PATTERN  where ILP_bFORMAT=1 and  ILP_bBODY_TAG=1  order by ILP_DISP_ORDER"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlLetter_Pattern, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function

    Protected Sub btnSave2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave2.Click
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            Call Offer_letter()

            lblError.Text = "Record Saved Successfully"


        Else
            lblError.Text = errorMessage
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer

        Dim IL_BSU_ID As String = Session("sBsuid")
        Dim IL_MAT As String = txtOfferText.Content
      
        

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim status As Integer

                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@IL_BSU_ID", IL_BSU_ID)
                pParms(1) = New SqlClient.SqlParameter("@IL_MAT", txtOfferText.Content)
                pParms(2) = New SqlClient.SqlParameter("@bOffer", rbOffer.Checked)
                pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.SaveBSU_OFFER_LETTER", pParms)

                status = pParms(3).Value




                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If


                ViewState("datamode") = "view"

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                calltransaction = "0"

                'disable_controlview

            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function


   
    
    
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
    End Sub

    
End Class
