﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAtt_Rediker.aspx.vb" Inherits="Students_studAtt_Rediker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
     <style>
        .ajax__tab_xp .ajax__tab_header {
            font-family: inherit !important;
            font-size: inherit !important;
        }

            .ajax__tab_xp .ajax__tab_header .ajax__tab_tab {
                height: auto !important;
                padding: inherit !important;
                background-image: none !important;
            }

        .ajax__tab_default .ajax__tab_tab {
            overflow: inherit !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_outer {
            padding-right: inherit !important;
            background-image: none !important;
            height: inherit !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_outer {
            background-image: none !important;
            margin-right: 2px;
            background-color: #ffffff !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_inner {
            background-image: none !important;
            background-color: #ffffff !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_inner {
            background-image: none !important;
        }

        .ajax__tab_inner {
            border: 1px solid #808080;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
            padding: 6px 4px !important;
            background-color: #efefef !important;
        }

            .ajax__tab_inner:hover {
                background-color: #cecece !important;
            }

        .ajax__tab_active {
            background-color: #ffffff !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_active .ajax__tab_tab {
            background-image: none !important;
        }

        .ajax__tab_xp .ajax__tab_header .ajax__tab_hover .ajax__tab_tab {
            background-image: none !important;
        }
    </style>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="Label1" runat="server" Text="REDIKER Import/Export"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

    <ajaxToolkit:TabContainer ID="tabPopup" runat="server" ActiveTabIndex="0" 
        Width="100%">
        <ajaxToolkit:TabPanel ID="HT1" runat="server" HeaderText="Export">
            <ContentTemplate>
                
                    <table width="100%" id="Table45" border="0"  cellpadding="0" cellspacing="0" >
                        <tr id="Tr1" runat="server">
                            <td id="Td1" runat="server" colspan="4" align="left">
                                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                    ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                        <td class="title-bg-lite" colspan="4" align="left">
                           
                                <asp:Literal id="ltLabel" runat="server" Text="REDIKER Export"></asp:Literal></td>
                    </tr>
                        <tr>
                            <td align="left" >
                           <span class="field-label">  Academic Year</span></td>
                    
                        <td align="left"  >
                            <asp:DropDownList id="ddlAcdYear" runat="server" AutoPostBack="True" >
                            </asp:DropDownList></td>
                                <td align="left" >
                            </td>
                                <td align="left" >
                            </td>
                        </tr>
                        <tr>
                             <td align="left" >
                            <span class="field-label"> Date From</span>
                        </td>
                     
                        <td align="left" >
                        <table><tr><td>
                            <asp:TextBox ID="txtCDate" runat="server" ></asp:TextBox><asp:ImageButton
                                ID="imgCDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgCDate" TargetControlID="txtCDate">
                            </ajaxToolkit:CalendarExtender></td></tr></table>
                        </td>
                                  <td align="left" >
                            <span class="field-label"> Date To</span>
                        </td>
                      
                        <td align="left" class="matters">
                            <table><tr><td><asp:TextBox ID="txtSDate" runat="server" ></asp:TextBox><asp:ImageButton
                                ID="imgSDate" runat="server" ImageUrl="~/Images/calendar.gif" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgSDate" TargetControlID="txtSDate">
                            </ajaxToolkit:CalendarExtender></td></tr></table>
                        </td>
                            </tr>
                  
                        <tr>
                           <td class="matters" colspan="4" align="center">
                               <asp:Button id="btnExport" runat="server" CssClass="button"   Text="Export"/>
                           </td>
                        </tr>
                        </table>
                   
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
         <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Import">
            <ContentTemplate>
                <table align="center" border="0" cellpadding="0" cellspacing="0"  width="100%">
                    <tr>
                        <td>
                            <table width="100%" id="Table3" border="0" cellpadding="0" cellspacing="0" >
                                <tr>
                                   <td align="left"  colspan="4">
                                        <asp:Label ID="lblerror_imp" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr >
                                    <td class="title-bg-lite" colspan="4" align="left" >
                                        Import
                                    </td>
                                    <tr>
                                        <td align="left" >
                                           <span class="field-label">  Select File</span>
                                        </td>
                                       
                                        <td align="left" >
                                            <asp:FileUpload ID="flUpExcel" runat="server"  EnableTheming="True"
                                                 />
                                        </td>
                                         <td align="left" ></td>
                                          <td align="left" ></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Button ID="btnUpload" runat="server" CssClass="button" Text="Import" 
                                                 CausesValidation="False" />
                                        </td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>

                
            </div>
        </div>
    </div>
</asp:Content>

