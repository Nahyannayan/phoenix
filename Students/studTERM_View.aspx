<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTERM_View.aspx.vb" Inherits="Students_studTERM_View" Title="Term Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <script language="javascript" type="text/javascript">





        function test3(val) {
            var path;
            if (val == 'LI') {
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                path = '../Images/operations/notendswith.gif';
            }
            document.getElementById("<%=getid3()%>").src = path;
                document.getElementById("<%=h_selected_menu_3.ClientID %>").value = val + '__' + path;
                }
                function test4(val) {
                    var path;
                    if (val == 'LI') {
                        path = '../Images/operations/like.gif';
                    } else if (val == 'NLI') {
                        path = '../Images/operations/notlike.gif';
                    } else if (val == 'SW') {
                        path = '../Images/operations/startswith.gif';
                    } else if (val == 'NSW') {
                        path = '../Images/operations/notstartwith.gif';
                    } else if (val == 'EW') {
                        path = '../Images/operations/endswith.gif';
                    } else if (val == 'NEW') {
                        path = '../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid4()%>").src = path;
                document.getElementById("<%=h_selected_menu_4.ClientID %>").value = val + '__' + path;
                }



                function test5(val) {
                    //alert(val);
                    var path;
                    if (val == 'LI') {
                        path = '../Images/operations/like.gif';
                    } else if (val == 'NLI') {
                        path = '../Images/operations/notlike.gif';
                    } else if (val == 'SW') {
                        path = '../Images/operations/startswith.gif';
                    } else if (val == 'NSW') {
                        path = '../Images/operations/notstartwith.gif';
                    } else if (val == 'EW') {
                        path = '../Images/operations/endswith.gif';
                    } else if (val == 'NEW') {
                        path = '../Images/operations/notendswith.gif';
                    }
                    document.getElementById("<%=getid5()%>").src = path;
                document.getElementById("<%=h_selected_menu_5.ClientID %>").value = val + '__' + path;
                }

    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>Term Master
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" colspan="4"  valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr >
                        <td align="left" colspan="3" valign="middle">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True">Add New</asp:LinkButton></td>
                        <td align="right" colspan="1" style="color: #000000; height: 21px; text-decoration: underline"
                            valign="middle"></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <table id="tbl_test" runat="server" align="center" width="100%"
                                cellpadding="5" cellspacing="0">
                                
                                <tr style="font-family: Verdana">
                                    <td align="left"  colspan="9" valign="top">
                                        <asp:GridView ID="gvAcademic" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                           CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="TRM_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("ACD_ID") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTRM_ID" runat="server" Text='<%# Bind("TRM_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Y_DESCR">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Y_DESCR") %>' Width="101px"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                       Acad. Year <br />
                                                                    <asp:DropDownList ID="ddlY_DESCRH" runat="server" AutoPostBack="True" CssClass="listbox"
                                                                        OnSelectedIndexChanged="ddlACY_DESCRH_SelectedIndexChanged" Width="101px">
                                                                    </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblY_DESCR" runat="server" Text='<%# Bind("Y_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TRM_DESCR">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("C_DESCR") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                       
                                                                    <asp:Label ID="lblC_DESCRH" runat="server" CssClass="gridheader_text" Text="Term Description"></asp:Label> <br />
                                                                                <asp:TextBox ID="txtTRM_DESCR" runat="server" ></asp:TextBox>
                                                                           
                                                                                <asp:ImageButton ID="btnSearchTRM_DESCR" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                    OnClick="btnSearchTRM_DESCR_Click" />&nbsp;
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTRM_DESCR" runat="server" Text='<%# Bind("TRM_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="STARTDT">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("STARTDT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblSTARTDTH" runat="server" CssClass="gridheader_text" Text="Start Date"></asp:Label><br />
                                                           
                                                                                <asp:TextBox ID="txtSTARTDT" runat="server" Width="65px"></asp:TextBox>
                                                                                <asp:ImageButton ID="btnSearchSTARTDT" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                    OnClick="btnSearchSTARTDT_Click" />&nbsp;
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTARTDT" runat="server" Text='<%# Bind("STARTDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ENDDT">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("ENDDT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        
                                                                    <asp:Label ID="lblENDDTH" runat="server" CssClass="gridheader_text" Text="End Date"></asp:Label><br />
                                                            
                                                                                <asp:TextBox ID="txtENDDT" runat="server" Width="65px"></asp:TextBox>
                                                                                <asp:ImageButton ID="btnSearchENDDT" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif"
                                                                                    OnClick="btnSearchENDDT_Click" />&nbsp;
                                                    </HeaderTemplate>
                                                    <ControlStyle Width="45%" />
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblENDDT" runat="server" Text='<%# Bind("ENDDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lbView" runat="server" OnClick="lbView_Click">View</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="griditem" Height="20px" />
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">


        cssdropdown.startchrome("Div3")
        cssdropdown.startchrome("Div4")
        cssdropdown.startchrome("Div5")

    </script>
</asp:Content>

