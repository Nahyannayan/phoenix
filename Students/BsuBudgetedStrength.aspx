<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="BsuBudgetedStrength.aspx.vb" Inherits="Students_BsuBudgetedStrength" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
   
     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book"></i>  School Strength
        </div>
        <div class="card-body">
            <div class="table-responsive">
    
     <table id="tbl_AddGroup" runat="server" align="center" width="100%" cellpadding="0" cellspacing="0">
        
        <tr>
            <td align="left">
                <asp:Label id="lblError" runat="server" CssClass="error" EnableViewState="False" ></asp:Label>
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table id="tabmain" align="center" width="100%" cellpadding="5" cellspacing="0">
                    
                    <tr id="row2">
                        <td align="left" width="20%">
                            <span class="field-label">Academic Year</span></td>
                        <td align="left" width="25%">
                            <asp:DropDownList id="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReport_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="left"  colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td align="left"  colspan="4">
                            <asp:GridView id="gvUNITS" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                OnPageIndexChanged="gvUNITS_PageIndexChanged" PageSize="30"
                                Width="100%">
                                <rowstyle cssclass="griditem" />
                                <columns>
<asp:BoundField DataField="BSU_ID" HeaderText="Business Unit">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="BSU_NAME" HeaderText="Unit Name"></asp:BoundField>
<asp:TemplateField HeaderText="Strength"><ItemTemplate>
<asp:TextBox id="TextBox1" runat="server"></asp:TextBox>
</ItemTemplate>
    <ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</columns>
                                <selectedrowstyle cssclass="Green" />
                                <headerstyle cssclass="gridheader_pop" />
                                <alternatingrowstyle cssclass="griditem_alternative" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
        
                <asp:Button id="btnSave" runat="server" CssClass="button" onclick="btnSave_Click"
                    tabIndex="7" Text="Save" ValidationGroup="groupM1"/>
                <asp:Button id="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    tabIndex="8" Text="Cancel" UseSubmitBehavior="False"/>
            </td>
        </tr>
    </table>

                </div>
            </div>
         </div>

</asp:Content>

