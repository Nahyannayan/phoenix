﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_studEmailText
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      


        If Page.IsPostBack = False Then

           
            Try


                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                ddlAcyear = studClass.PopulateAcademicYear(ddlAcyear, Session("clm").ToString, Session("sbsuid").ToString)


                bind_subject()

                If Not Request.QueryString("acdid") Is Nothing Then
                    ddlAcademicYear.SelectedValue = Request.QueryString("acdid")
                    ddlAcyear.SelectedValue = Request.QueryString("acdid")
                End If
                bind_grade()
                bind_grade_Copy()

                If Not Request.QueryString("grdid") Is Nothing Then
                    ddlGrade.SelectedValue = Request.QueryString("grdid")
                End If
                bind_stream()
                bind_stream_copy()
                If Not Request.QueryString("stmid") Is Nothing Then
                    ddlStream.SelectedValue = Request.QueryString("stmid")
                End If

                If Not Request.QueryString("subid") Is Nothing Then
                    ddlSubject.SelectedValue = Request.QueryString("subid")
                End If
                If Not Request.QueryString("emtid") Is Nothing Then
                    ViewState("EMTID") = Request.QueryString("emtid")
                End If

                If ViewState("EMTID") <> "0" Then
                    btnAdd.Text = "Update"
                Else
                    btnAdd.Text = "Add"
                End If
                enable_ctrls()
                Displayitems()


            Catch ex As Exception

                lblerror.Text = "Request could not be processed "
            End Try

        End If
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnAdd)
    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub bind_grade()
        ddlGrade.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                               & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                           & "  grm_acd_id=" + ddlAcademicYear.SelectedValue + " order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "grm_display"
        ddlGrade.DataValueField = "grm_grd_id"
        ddlGrade.DataBind()

        ddlGradeEmail.Items.Clear()
        ddlGradeEmail.DataSource = ds
        ddlGradeEmail.DataTextField = "grm_display"
        ddlGradeEmail.DataValueField = "grm_grd_id"
        ddlGradeEmail.DataBind()
    End Sub
    Private Sub bind_grade_Copy()
        ddlGradeEmail.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                               & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                           & "  grm_acd_id=" + ddlAcyear.SelectedValue + " order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)




        ddlGradeEmail.DataSource = ds
        ddlGradeEmail.DataTextField = "grm_display"
        ddlGradeEmail.DataValueField = "grm_grd_id"
        ddlGradeEmail.DataBind()
    End Sub
    Private Sub bind_stream()
        ddlStream.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
                                  & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & " grm_acd_id=" & ddlAcademicYear.SelectedValue & " and grm_grd_id='" & ddlGrade.SelectedValue & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStream.DataSource = ds
        ddlStream.DataTextField = "stm_descr"
        ddlStream.DataValueField = "stm_id"
        ddlStream.DataBind()
        If Not ddlStream.Items.FindByValue("1") Is Nothing Then
            ddlStream.Items.FindByValue("1").Selected = True
        End If

       

    End Sub
    Private Sub bind_stream_copy()
        ddlStreamEmail.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT stm_descr,stm_id FROM grade_bsu_m,stream_m WHERE" _
                                  & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                              & " grm_acd_id=" & ddlAcyear.SelectedValue & " and grm_grd_id='" & ddlGradeEmail.SelectedValue & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlStreamEmail.DataSource = ds
        ddlStreamEmail.DataTextField = "stm_descr"
        ddlStreamEmail.DataValueField = "stm_id"
        ddlStreamEmail.DataBind()
        If Not ddlStreamEmail.Items.FindByValue("1") Is Nothing Then
            ddlStreamEmail.Items.FindByValue("1").Selected = True
        End If

        

    End Sub
    Private Sub bind_subject()
        ddlSubject.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "  SELECT ESM_ID,ESM_SUBJECT  FROM ONLINE_ENQ.EMAIL_SUBJ_M "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "ESM_SUBJECT"
        ddlSubject.DataValueField = "ESM_ID"
        ddlSubject.DataBind()

        ddlSubjectEmail.Items.Clear()
        ddlSubjectEmail.DataSource = ds
        ddlSubjectEmail.DataTextField = "ESM_SUBJECT"
        ddlSubjectEmail.DataValueField = "ESM_ID"
        ddlSubjectEmail.DataBind()
    End Sub


    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        bind_grade()
        bind_stream()

    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        bind_stream()

    End Sub



    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        SAVE_DETAILS(ddlAcademicYear.SelectedValue, ddlGrade.SelectedValue, ddlStream.SelectedValue, ddlSubject.SelectedValue, "ADD")

    End Sub


    Private Sub SAVE_DETAILS(ByVal acyear As Integer, ByVal grd As String, ByVal stream As Integer, ByVal subj As Integer, ByVal TYPE As String)

        Dim Status As Integer
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction


        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                param(0) = New SqlParameter("@EMT_BSU_ID", Session("sbsuid"))
                param(1) = New SqlParameter("@EMT_ACD_ID", acyear)
                param(2) = New SqlParameter("@EMT_GRD_ID", grd)
                param(3) = New SqlParameter("@EMT_STM_ID", stream)
                param(4) = New SqlParameter("@EMT_ESM_ID", subj)
                param(5) = New SqlParameter("@EMT_TEXT", txtText.Content)
                param(6) = New SqlParameter("@EMT_ID", ViewState("EMTID"))
                param(7) = New SqlParameter("@TYPE", TYPE)

                param(8) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(8).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ONLINE_ENQ.SAVE_EMAIL_TEXTS", param)
                Status = param(8).Value





            Catch ex As Exception

                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If Status <> 0 Then
                    ' UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                    transaction.Rollback()
                    lblerror.Text = "Error "
                Else
                    lblerror.Text = "Saved Successfully."
                    transaction.Commit()

                End If
            End Try

        End Using
    End Sub


    Private Sub Displayitems()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""
            Dim subj As String = ""

            str_query = "SELECT EMT_ID,EMT_GRD_ID,EMT_ACD_ID,EMT_ESM_ID ,EMT_STM_ID,EMT_TEXT FROM ONLINE_ENQ.EMAIL_TEXTS " _
                        & " WHERE EMT_ID=" + ViewState("EMTID")

            If str_query <> "" Then
                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
                If ds.Tables(0).Rows.Count >= 1 Then

                    ddlAcademicYear.SelectedValue = ds.Tables(0).Rows(0).Item("EMT_ACD_ID")
                    ddlGrade.SelectedValue = ds.Tables(0).Rows(0).Item("EMT_GRD_ID")
                    ddlStream.SelectedValue = ds.Tables(0).Rows(0).Item("EMT_STM_ID")
                    ddlSubject.SelectedValue = ds.Tables(0).Rows(0).Item("EMT_ESM_ID")

                    txtText.Content = ds.Tables(0).Rows(0).Item("EMT_TEXT")


                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblerror.Text = "Request could not be processed"
        End Try
    End Sub

    Private Sub enable_ctrls()

        ddlAcademicYear.Enabled = True
        ddlGrade.Enabled = True
        ddlStream.Enabled = True
        ddlSubject.Enabled = True

    End Sub

    Protected Sub btClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btClose.Click
        divEmail.Visible = False
    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        divEmail.Visible = False
    End Sub

    Protected Sub lnkCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopy.Click
        divEmail.Visible = True

    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        SAVE_DETAILS(ddlAcyear.SelectedValue, ddlGradeEmail.SelectedValue, ddlStreamEmail.SelectedValue, ddlSubjectEmail.SelectedValue, "COPY")
        lblUerror.Text = "Copied...."
    End Sub

    Protected Sub ddlAcyear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAcyear.SelectedIndexChanged
        bind_grade_Copy()
        bind_stream_copy()
    End Sub

    Protected Sub ddlGradeEmail_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGradeEmail.SelectedIndexChanged
        bind_stream_copy()
    End Sub
End Class
