Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studCommDetail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100195") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    ' Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    GetDef_EMR()
                    ViewState("GRD_ACCESS") = isUSR_GRD_SCT_ACCESS(Session("sUsr_id"))
                    bindAcademic_Grade()
                    bindAcademic_Section()
                    rbFather.Checked = True
                    rbStartWith.Checked = True
                    Session("dt_CommDetail") = CreateDataTable()
                    Call BindComm_Detail()


                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub

   

    Public Function callEMR_DESCR() As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        Dim str_Sql As String = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M order by EMR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

        Return ds

    End Function
    Private Function isUSR_GRD_SCT_ACCESS(ByVal usrId As String) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "select count(GSA_ID) from GRADE_SECTION_ACCESS  where GSA_USR_ID='" & usrId & "' and  GSA_ACD_ID='" & Session("Current_ACD_ID") & "'"


        Dim AccessGrd As Object = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
        If Not AccessGrd Is DBNull.Value Then
            Return AccessGrd
        Else
            Return 0
        End If

    End Function
    Sub bindAcademic_Grade()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ACD_ID As String = Session("Current_ACD_ID")
            Dim ds As New DataSet
            ''included acd id by nahayn on 15may2019 #350254
            If ViewState("GRD_ACCESS") > 0 Then
                str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
                 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' AND GRADE_BSU_M.GRM_GRD_ID IN(SELECT DISTINCT GRADE_BSU_M.GRM_GRD_ID FROM  SECTION_M INNER JOIN GRADE_BSU_M ON SECTION_M.SCT_GRD_ID " & _
 " = GRADE_BSU_M.GRM_GRD_ID AND SECTION_M.SCT_GRM_ID = GRADE_BSU_M.GRM_ID WHERE (SECTION_M.SCT_ID IN (SELECT  ID  FROM  dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  GRADE_SECTION_ACCESS   WHERE GSA_ACD_ID='" & ACD_ID & "' AND (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|') AS fnSplitMe_1)))" & _
" order by GRADE_M.GRD_DISPLAYORDER "
            Else
                str_Sql = "SELECT  distinct  GRADE_BSU_M.GRM_GRD_ID AS GRD_ID,GRADE_BSU_M.GRM_DISPLAY  ,GRADE_M.GRD_DISPLAYORDER  FROM GRADE_BSU_M INNER JOIN " & _
                 " GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' order by GRADE_M.GRD_DISPLAYORDER "

            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlGrade.Items.Clear()
            ddlGrade.DataSource = ds.Tables(0)
            ddlGrade.DataTextField = "GRM_DISPLAY"
            ddlGrade.DataValueField = "GRD_ID"
            ddlGrade.DataBind()
           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub bindAcademic_Section()
        Try

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ds As New DataSet
            Dim BSU_ID As String = Session("sBsuid")
            Dim ACD_ID As String = Session("Current_ACD_ID")
            Dim GRD_ID As String
            Dim str_Sql As String
            If ddlGrade.SelectedIndex <> -1 Then
                GRD_ID = ddlGrade.SelectedItem.Value
            Else
                GRD_ID = ""
            End If

            If ViewState("GRD_ACCESS") > 0 Then

                str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
     " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID " & _
    " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND " & _
     " (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "') AND SECTION_M.SCT_ID IN(SELECT  ID  FROM  dbo.fnSplitMe ((SELECT  GSA_SCT_ID  FROM  GRADE_SECTION_ACCESS  " & _
 " WHERE GSA_ACD_ID='" & ACD_ID & "' AND (GSA_USR_ID = '" & Session("sUsr_id") & "')), '|'))  order by SECTION_M.SCT_DESCR "
            Else

                str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
     " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID " & _
    " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & BSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & ACD_ID & "') AND " & _
     " (GRADE_BSU_M.GRM_GRD_ID = '" & GRD_ID & "')  order by SECTION_M.SCT_DESCR "

            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlSection.Items.Clear()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSection.DataSource = ds.Tables(0)
                ddlSection.DataTextField = "SCT_DESCR"
                ddlSection.DataValueField = "SCT_ID"
                ddlSection.DataBind()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub BindComm_Detail()
        Try
            Dim ACD_ID As String = Session("Current_ACD_ID")
            Dim GRD_ID As String = ddlGrade.SelectedItem.Value
            Dim SCT_ID As String = ddlSection.SelectedItem.Value
            Dim ddlEmirate As New DropDownList
            Dim str_query As String = String.Empty
            Dim SPARENT As String = String.Empty
            Dim FEE_ID As String = String.Empty
            Dim SNAME As String = String.Empty
            Dim arInfo As String() = New String(2) {}
            Dim splitter As Char = "-"
            Dim Current_DT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", Now.Date)
            If Trim(txtSname.Text) <> "" Then
                If rbStartWith.Checked Then
                    SNAME = " AND SNAME LIKE '" & Trim(txtSname.Text) & "%'"
                Else
                    SNAME = " AND  SNAME LIKE '%" & Trim(txtSname.Text) & "%'"
                End If
            Else
                SNAME = " AND SNAME LIKE '%%'"
            End If
         
            If Trim(txtFeeID.Text) <> "" Then
                FEE_ID = " AND FEE_ID ='" & Trim(txtFeeID.Text) & "'"
            Else
                FEE_ID = " AND FEE_ID <> ''"
            End If
            If rbFather.Checked Then
                SPARENT = "F"
            ElseIf rbMother.Checked Then
                SPARENT = "M"
            ElseIf rbGuardian.Checked Then
                SPARENT = "G"
            End If

            Session("dt_CommDetail").Rows.Clear()

            Using readerComm_Detail As SqlDataReader = AccessStudentClass.GetComm_Detail(ACD_ID, GRD_ID, SCT_ID, _
      SPARENT, FEE_ID, SNAME, Current_DT)
                If readerComm_Detail.HasRows = True Then
                    While readerComm_Detail.Read
                        Dim rDt As DataRow
                        rDt = Session("dt_CommDetail").NewRow

                        rDt("STU_ID") = Convert.ToString(readerComm_Detail("STU_ID"))
                        rDt("FEE_ID") = Convert.ToString(readerComm_Detail("FEE_ID"))
                        rDt("SNAME") = Convert.ToString(readerComm_Detail("SNAME"))
                        rDt("POB") = Convert.ToString(readerComm_Detail("POB"))
                        rDt("EMIRATE") = Convert.ToString(readerComm_Detail("EMIRATE"))
                        Dim RCountry As String = Convert.ToString(readerComm_Detail("RESPHONE"))

                        arInfo = RCountry.Split(splitter)
                        If arInfo.Length > 2 Then
                            rDt("RCOUNTRY_CODE") = arInfo(0)
                            rDt("RAREA_CODE") = arInfo(1)
                            rDt("RCONTACT_NO") = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            rDt("RCOUNTRY_CODE") = ""
                            rDt("RAREA_CODE") = arInfo(0)
                            rDt("RCONTACT_NO") = arInfo(1)
                        Else

                            rDt("RCOUNTRY_CODE") = ""
                            rDt("RAREA_CODE") = ""
                            rDt("RCONTACT_NO") = RCountry

                        End If

                        Dim MOBILE As String = Convert.ToString(readerComm_Detail("MOBILE"))
                        arInfo = MOBILE.Split(splitter)
                        If arInfo.Length > 2 Then
                            rDt("MCOUNTRY_CODE") = arInfo(0)
                            rDt("MAREA_CODE") = arInfo(1)
                            rDt("MCONTACT_NO") = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            rDt("MCOUNTRY_CODE") = ""
                            rDt("MAREA_CODE") = arInfo(0)
                            rDt("MCONTACT_NO") = arInfo(1)
                        Else
                            rDt("MCOUNTRY_CODE") = ""
                            rDt("MAREA_CODE") = ""
                            rDt("MCONTACT_NO") = MOBILE


                        End If

                        rDt("EMAIL") = Convert.ToString(readerComm_Detail("EMAIL"))
                        Session("dt_CommDetail").Rows.Add(rDt)

                    End While

                    'str_query = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M order by EMR_DESCR"
                    'ddlEmirate = gvInfo.ro.FindControl("ddlEmirate")
                    'ddlEmirate.Items.Clear()
                    'Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                    'Dim reader As SqlDataReader
                    'reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
                    'While reader.Read
                    '    ddlEmirate.Items.Add(New ListItem(reader("EMR_DESCR"), reader("EMR_CODE")))
                    'End While
                    'reader.Close()



                    btnSave.Visible = True
                Else
                    btnSave.Visible = False
                End If
                

            End Using
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    'Private Function FillEmirateDetails(ByVal ddlList As DropDownList) As DropDownList
    '    If Session("sEmiratesList") Is Nothing Then
    '        Session("sEmiratesList") = callEMR_DESCR()
    '    End If
    '    ddlList.DataSource = Session("sEmiratesList")
    '    ddlList.DataTextField = "EMR_DESCR"
    '    ddlList.DataValueField = "EMR_CODE"
    '    ddlList.DataBind()
    '    Return ddlList
    'End Function

    Protected Sub gvInfo_rowdatabound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Dim mytable As New DataTable()
        Dim EMR_CODEcolumn As New DataColumn("EMR_CODE")
        Dim EMR_DESCRcolumn As New DataColumn("EMR_DESCR")

        mytable.Columns.Add(EMR_CODEcolumn)
        mytable.Columns.Add(EMR_DESCRcolumn)

        Dim ds As New DataSet()
        ds = callEMR_DESCR()


        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim ddl As DropDownList = DirectCast(e.Row.FindControl("ddlEmirate"), DropDownList)
            ' ddl = FillEmirateDetails(ddl)
            Dim rows As DataRow() = ds.Tables(0).Select

            For Each row As DataRow In rows
                Dim newrow As DataRow = mytable.NewRow()
                newrow("EMR_CODE") = row("EMR_CODE")
                newrow("EMR_DESCR") = row("EMR_DESCR")

                mytable.Rows.Add(newrow)
            Next

            ddl.DataSource = mytable
            ddl.DataTextField = "EMR_DESCR"
            ddl.DataValueField = "EMR_CODE"
            ddl.DataBind()
        End If
    End Sub
    Sub gridbind()
        Try
            gvInfo.DataSource = Session("dt_CommDetail")
            gvInfo.DataBind()
            Call EMIRATE_SELECT()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetDef_EMR()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_Sql As String = "SELECT isnull(BSU_CITY,'') as BSU_CITY  FROM BUSINESSUNIT_M where BSU_ID='" & Session("sBsuid") & "'"

        Dim bs_name = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)



        ViewState("BSU_EMR") = CStr(bs_name)

    End Sub

    Sub EMIRATE_SELECT()



        For j As Integer = 0 To Session("dt_CommDetail").Rows.Count - 1

            Dim STU_ID As String = Session("dt_CommDetail").Rows(j)("STU_ID")

            Dim CITY As String = Session("dt_CommDetail").Rows(j)("EMIRATE")



            For i As Integer = 0 To gvInfo.Rows.Count - 1

                Dim row As GridViewRow = gvInfo.Rows(i)

                Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStu_ID"), Label).Text

                Dim ddlCity As DropDownList = DirectCast(row.FindControl("ddlEmirate"), DropDownList)

                If Temp_stud_ID = STU_ID Then



                    If Not ddlCity.Items.FindByValue(CITY) Is Nothing Then

                        ddlCity.Items.FindByValue(CITY).Selected = True

                    ElseIf Not ddlCity.Items.FindByValue(ViewState("BSU_EMR")) Is Nothing Then

                        ddlCity.Items.FindByValue(ViewState("BSU_EMR")).Selected = True



                    End If



                End If





            Next



        Next



    End Sub



    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim FEE_ID As New DataColumn("FEE_ID", System.Type.GetType("System.String"))
            Dim SNAME As New DataColumn("SNAME", System.Type.GetType("System.String"))
            Dim POB As New DataColumn("POB", System.Type.GetType("System.String"))
            Dim EMIRATE As New DataColumn("EMIRATE", System.Type.GetType("System.String"))
            Dim RCOUNTRY_CODE As New DataColumn("RCOUNTRY_CODE", System.Type.GetType("System.String"))
            Dim RAREA_CODE As New DataColumn("RAREA_CODE", System.Type.GetType("System.String"))
            Dim RCONTACT_NO As New DataColumn("RCONTACT_NO", System.Type.GetType("System.String"))
            Dim MCOUNTRY_CODE As New DataColumn("MCOUNTRY_CODE", System.Type.GetType("System.String"))
            Dim MAREA_CODE As New DataColumn("MAREA_CODE", System.Type.GetType("System.String"))
            Dim MCONTACT_NO As New DataColumn("MCONTACT_NO", System.Type.GetType("System.String"))
            Dim EMAIL As New DataColumn("EMAIL", System.Type.GetType("System.String"))

            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(FEE_ID)
            dtDt.Columns.Add(SNAME)
            dtDt.Columns.Add(POB)
            dtDt.Columns.Add(EMIRATE)
            dtDt.Columns.Add(RCOUNTRY_CODE)
            dtDt.Columns.Add(RAREA_CODE)
            dtDt.Columns.Add(RCONTACT_NO)
            dtDt.Columns.Add(MCOUNTRY_CODE)
            dtDt.Columns.Add(MAREA_CODE)
            dtDt.Columns.Add(MCONTACT_NO)
            dtDt.Columns.Add(EMAIL)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bindAcademic_Section()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        If Page.IsValid = True Then

            str_err = calltransaction(errorMessage)
            If str_err = "0" Then
                lblError.Text = "Record Saved Successfully"
            Else
                lblError.Text = errorMessage
            End If

        End If

    End Sub

    Function calltransaction(ByRef errorMessage As String) As Integer

        'Dim lblStu_ID As New Label
        'Dim txtPob As New TextBox
        'Dim ddlEmirate As New DropDownList
        'Dim txtResC_Code As New TextBox
        'Dim txtResA_code As New TextBox
        'Dim txtRContact_No As New TextBox
        'Dim txtMCountry_Code As New TextBox
        'Dim txtMArea_Code As New TextBox
        'Dim txtMCont_No As New TextBox
        'Dim txtEmail As New TextBox
        Dim status As Integer
        Dim Stu_ID As String = String.Empty
        Dim Pob As String = String.Empty
        Dim Emirate As String = String.Empty
        Dim ResC_Code As String = String.Empty
        Dim ResA_code As String = String.Empty
        Dim RContact_No As String = String.Empty
        Dim MCountry_Code As String = String.Empty
        Dim MArea_Code As String = String.Empty
        Dim MCont_No As String = String.Empty
        Dim Email As String = String.Empty
        Dim SPARENT As String = String.Empty
        Dim Res_No As String = String.Empty
        Dim Mob_No As String = String.Empty
        Dim STR As String = String.Empty
        If rbFather.Checked Then
            SPARENT = "F"
        ElseIf rbMother.Checked Then
            SPARENT = "M"
        ElseIf rbGuardian.Checked Then
            SPARENT = "G"
        End If
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                For i As Integer = 0 To gvInfo.Rows.Count - 1

                    Dim row As GridViewRow = gvInfo.Rows(i)

                    Stu_ID = DirectCast(row.FindControl("lblStu_ID"), Label).Text
                    Pob = DirectCast(row.FindControl("txtPob"), TextBox).Text
                    Emirate = DirectCast(row.FindControl("ddlEmirate"), DropDownList).SelectedItem.Value
                    ResC_Code = DirectCast(row.FindControl("txtResC_Code"), TextBox).Text
                    ResA_code = DirectCast(row.FindControl("txtResA_code"), TextBox).Text
                    RContact_No = DirectCast(row.FindControl("txtRContact_No"), TextBox).Text
                    MCountry_Code = DirectCast(row.FindControl("txtMCountry_Code"), TextBox).Text
                    MArea_Code = DirectCast(row.FindControl("txtMArea_Code"), TextBox).Text
                    MCont_No = DirectCast(row.FindControl("txtMCont_No"), TextBox).Text
                    Email = DirectCast(row.FindControl("txtEmail"), TextBox).Text
                   
                    'If ResC_Code <> "" And ResA_code <> "" And RContact_No <> "" Then
                    Res_No = ResC_Code & "-" & ResA_code & "-" & RContact_No
                    'Else
                    ' Res_No = ""
                    'End If
                    'If MCountry_Code <> "" And MArea_Code <> "" And MCont_No <> "" Then
                    Mob_No = MCountry_Code & "-" & MArea_Code & "-" & MCont_No
                    ' Else
                    ' Mob_No = ""
                    ' End If
                    STR += String.Format("<STUDENT STU_ID='{0}'><DETAILS Pob='{1}' Emirate='{2}' Res_No='{3}' Mob_No='{4}' Email='{5}'/></STUDENT>", Stu_ID, Pob, Emirate, Res_No, Mob_No, Email)
                Next
                If STR <> "" Then
                    STR = "<STUDENTS>" + STR + "</STUDENTS>"
                Else
                    STR = ""
                End If






                status = Local_SAVEComm_Detail_XML(STR, SPARENT, transaction)

                If Status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(Status)  '"Error in inserting new record"
                    Return "1"
                End If

                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                calltransaction = "0"


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using


    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call BindComm_Detail()
    End Sub
    Public Function Local_SAVEComm_Detail_XML(ByVal STR_XML As String, ByVal SPARENT As String, ByVal transaction As SqlTransaction) As Integer
        'Author(--Lijo)
        'Date   --06/MAY/2009
        'Purpose--To save Parent Communication Details data based 
        Try


            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STR_XML", STR_XML)
                pParms(1) = New SqlClient.SqlParameter("@SPARENT", SPARENT)
                pParms(3) = New SqlClient.SqlParameter("@SESSION_USER", Session("sUsr_name"))
                pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(2).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVEComm_Detail_XML", pParms)
                Dim ReturnFlag As Integer = pParms(2).Value
                Return ReturnFlag

            End Using

        Catch ex As Exception
            Return 1
        End Try


    End Function
   
End Class
