<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudComments.aspx.vb" Debug="true"
    Inherits="Students_StudComments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Comments</title>
    <base target="_self" />
    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">

        //Javascript Error Handling

        function handleError() {

            return true;
        }
        window.onerror = handleError;



    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
                        </ajaxToolkit:ToolkitScriptManager>
                        <table width="100%" id="Table3" border="0">
                            <tr>
                                <td align="left" class="title-bg">Comments
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblMessage" runat="server" CssClass="error" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%-- <tr>
                <td align="center" style="width: 750px">
                </td>
            </tr>--%>
                <tr>
                    <td align="left">
                        <asp:TextBox ID="txtComments" runat="server" TabIndex="22" TextMode="MultiLine"
                            Width="475" Rows="4"></asp:TextBox>


                    </td>
                </tr>

                <tr>
                    <td align="left">
                        <asp:Button ID="btnsave" runat="server" CssClass="button" Text="Save" />
                    </td>
                </tr>
            </table>
            <br />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <%--<tr>
                <td class="subheader_img">
                    Comments
                </td>
            </tr>--%>
                <tr>
                    <td align="left">
                        <asp:GridView ID="GridInfo" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                            EmptyDataText="Information not available." Width="100%" CssClass="table table-bordered table-row">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Comments
                                           
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Elblview1" runat="server" Text='<%#Eval("tempview1")%>'></asp:Label>
                                        <asp:Panel ID="E4Panel11" runat="server" Height="50px">
                                            <%#Eval("STM_ENR_COMMENTS")%>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                            AutoCollapse="False" AutoExpand="False" CollapseControlID="Elblview1" Collapsed="true"
                                            CollapsedSize="0" CollapsedText='<%#Eval("tempview1")%>' ExpandControlID="Elblview1"
                                            ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="E4Panel11"
                                            TextLabelID="Elblview1">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Entry Date
                                           
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <center>
                                            <%# Eval("STM_ENTRY_DATE")%>
                                        </center>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="griditem" />
                            <EmptyDataRowStyle />
                            <SelectedRowStyle />
                            <HeaderStyle />
                            <EditRowStyle />
                            <AlternatingRowStyle CssClass="griditem_alternative" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
