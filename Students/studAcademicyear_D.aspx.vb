Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Student_studAcademicyear_D
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub txtFrom_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrom_date.TextChanged
        Dim strfDate As String = txtFrom_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtFrom_date.Text = strfDate
        End If
    End Sub

    Protected Sub txtTo_date_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo_date.TextChanged
        Dim strfDate As String = txtTo_date.Text.Trim
        Dim str_err As String = DateFunctions.checkdate(strfDate)
        If str_err <> "" Then
            lblError.Text = str_err
            Exit Sub
        Else
            txtTo_date.Text = strfDate
        End If
    End Sub
    Protected Sub cvTodate_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvTodate.ServerValidate
        Dim sflag As Boolean = False

        Dim DateTime2 As Date
        Dim dateTime1 As Date
        Try
            DateTime2 = Date.ParseExact(txtTo_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            dateTime1 = Date.ParseExact(txtFrom_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date

            If IsDate(DateTime2) Then

                If DateTime2 < dateTime1 Then
                    sflag = False
                Else
                    sflag = True
                End If
            End If
            If sflag Then
                args.IsValid = True
            Else
                'CustomValidator1.Text = ""
                args.IsValid = False
            End If
        Catch
            args.IsValid = False
        End Try
    End Sub

    Protected Sub cvDateFrom_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvDateFrom.ServerValidate
        Dim sflag As Boolean = False
        Dim dateTime1 As Date
        Try
            'convert the date into the required format so that it can be validate by Isdate function
            dateTime1 = Date.ParseExact(txtFrom_date.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
            'check for the leap year date
            If IsDate(dateTime1) Then
                sflag = True
            End If

            If sflag Then
                args.IsValid = True
            Else
                args.IsValid = False
            End If
        Catch
            'catch when format string through an error
            args.IsValid = False
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050005") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    'calling pageright class to get the access rights

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    txtAcadYear.Attributes.Add("readonly", "readonly")
                    txtCurr.Attributes.Add("readonly", "readonly")
                   


                    If ViewState("datamode") = "view" Then


                        Call setbutton()
                        ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
                        
                        DATABIND()

                    End If

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try



        End If

    End Sub

    Private Sub DATABIND()
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            Dim PARAM(1) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ViewState("viewid"))

            Using readerACADEMICYEAR_D As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "DBO.GETSCHACADEMIC_DETAILS", PARAM)
                While readerACADEMICYEAR_D.Read

                    'handle the null value returned from the reader incase  convert.tostring

                    hfACY_ID.Value = Convert.ToString(readerACADEMICYEAR_D("ACY_ID"))

                    txtAcadYear.Text = Convert.ToString(readerACADEMICYEAR_D("ACY_DESCR"))

                    txtFrom_date.Text = Convert.ToString(readerACADEMICYEAR_D("STARTDT"))
                    txtTo_date.Text = Convert.ToString(readerACADEMICYEAR_D("ENDDT"))
                    txtAttDate.Text = Convert.ToString(readerACADEMICYEAR_D("ACD_ATT_CLOSINGDATE"))
                    txtBrownBookDate.Text = Convert.ToString(readerACADEMICYEAR_D("ACD_BROWNBOOKDATE"))

                    hfCLM_ID.Value = Convert.ToString(readerACADEMICYEAR_D("CLM_ID"))

                    txtCurr.Text = Convert.ToString(readerACADEMICYEAR_D("CLM_DESCR"))

                    chkCurrent.Checked = Convert.ToBoolean(readerACADEMICYEAR_D("ACD_CURRENT"))
                    chkOpenOnline.Checked = Convert.ToBoolean(readerACADEMICYEAR_D("OPENONLINE"))
                    txtAffNo.Text = Convert.ToString(readerACADEMICYEAR_D("AFFLNO"))
                    txtMoeAffNo.Text = Convert.ToString(readerACADEMICYEAR_D("MOEAFFLNO"))
                    txtLicense.Text = Convert.ToString(readerACADEMICYEAR_D("LICENSE"))



                End While

            End Using
        Catch ex As Exception

        End Try
    End Sub
    Sub setbutton()
        txtAcadYear.Enabled = False
        txtCurr.Enabled = False
        txtFrom_date.Enabled = False
        txtTo_date.Enabled = False
        imgBtnFrom_date.Enabled = False
        imgbtnTo_date.Enabled = False
        chkCurrent.Enabled = False
        chkOpenOnline.Enabled = False
        txtAffNo.Enabled = False
        txtMoeAffNo.Enabled = False
        txtLicense.Enabled = False
        txtAttDate.Enabled = False
        txtBrownBookDate.Enabled = False
        btnAcademic.Visible = False
        btnCurr.Visible = False
    End Sub
    Sub resetbutton()
        txtAcadYear.Enabled = True
        txtCurr.Enabled = True
        txtFrom_date.Enabled = True
        txtTo_date.Enabled = True
        imgBtnFrom_date.Enabled = True
        imgbtnTo_date.Enabled = True
        chkCurrent.Enabled = True
        chkOpenOnline.Enabled = True
        txtAffNo.Enabled = True
        txtMoeAffNo.Enabled = True
        txtLicense.Enabled = True
        btnAcademic.Visible = True
        btnCurr.Visible = True
        txtAttDate.Enabled = True
        txtBrownBookDate.Enabled = True
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ViewState("datamode") = "add"
            Call clearRecords()

            resetbutton()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

        Catch ex As Exception
            UtilityObj.Errorlog("studAcademicyear_D", ex.Message)
        End Try

    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ViewState("datamode") = "edit"
        UtilityObj.beforeLoopingControls(Me.Page)
        resetbutton()
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub

    

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

            Call clearRecords()
            'clear the textbox and set the default settings

            ViewState("datamode") = "none"
            setbutton()
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Else
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub

    Sub clearRecords()
        txtAcadYear.Text = ""
        txtCurr.Text = ""
        txtTo_date.Text = ""
        txtFrom_date.Text = ""
        chkCurrent.Checked = False
        chkOpenOnline.Checked = False
        txtAffNo.Text = ""
        txtMoeAffNo.Text = ""
        txtLicense.Text = ""
        ViewState("viewid") = 0
        txtAttDate.Text = ""
        txtBrownBookDate.Text = ""
    End Sub

    
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = True Then

            Dim ACD_ID As Integer = 0
            Dim ACD_ACY_ID As Integer = hfACY_ID.Value
            Dim ACD_BSU_ID As String = Session("sBsuid")
            Dim ACD_CLM_ID As Integer = hfCLM_ID.Value
            Dim ACD_STARTDT As Date = txtFrom_date.Text
            Dim ACD_ENDDT As Date = txtTo_date.Text
            Dim ACD_CURRENT As Boolean = chkCurrent.Checked
            Dim ACD_OPENONLINE As Boolean = chkOpenOnline.Checked

            Dim ACD_AFFLNO As String = txtAffNo.Text
            Dim ACD_MOEAFFLNO As String = txtMoeAffNo.Text
            Dim ACD_LICENSE As String = txtLicense.Text

            Dim ACD_ATT_CLOSINGDATE As Date = Date.Now
            Dim IS_ACD_ATT_CLOSINGDATE As Boolean = 0
            If Not txtAttDate.Text = "" Then
                ACD_ATT_CLOSINGDATE = txtAttDate.Text
                IS_ACD_ATT_CLOSINGDATE = 1
            End If

            Dim ACD_BROWNBOOKDATE As Date = Date.Now
            Dim IS_ACD_BROWNBOOKDATE As Boolean = 0
            If Not txtBrownBookDate.Text = "" Then
                ACD_BROWNBOOKDATE = txtBrownBookDate.Text
                IS_ACD_BROWNBOOKDATE = 1
            End If

            Dim bEdit As Boolean

            Dim status As Integer
            Dim transaction As SqlTransaction

            If ViewState("datamode") = "add" Then
                bEdit = False

                'get the max Id to be inserted in audittrial report
                Dim sqlACD_ID As String = "Select isnull(max(ACD_ID),0)+1  from ACADEMICYEAR_D"
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlACD_ID, connection)
                Dim temp As String
                command.CommandType = CommandType.Text
                temp = command.ExecuteScalar()
                connection.Close()
                connection.Dispose()


                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try

                        'call the class to insert the record into ACADEMICYEAR_D
                        status = AccessStudentClass.SaveACADEMICYEAR_D(ACD_ID, ACD_ACY_ID, ACD_BSU_ID, ACD_CLM_ID, _
            ACD_STARTDT, ACD_ENDDT, ACD_CURRENT, ACD_OPENONLINE, ACD_AFFLNO, ACD_MOEAFFLNO, ACD_LICENSE, IIf(IS_ACD_ATT_CLOSINGDATE = True, ACD_ATT_CLOSINGDATE, Nothing), IIf(IS_ACD_BROWNBOOKDATE = True, ACD_BROWNBOOKDATE, Nothing), bEdit, transaction)
                        'throw error if insert to the ACADEMICYEAR_D is not Successfully
                        If status = 706 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        ElseIf status = 707 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        ElseIf status <> 0 Then
                            Throw New ArgumentException("Record could not be Inserted")
                        End If


                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), temp, "Insert", Page.User.Identity.Name.ToString)

                        If flagAudit <> 0 Then
                            Throw New ArgumentException("Could not process your request")
                        End If

                        transaction.Commit()

                        lblError.Text = "Record Inserted Successfully"

                        ViewState("datamode") = "none"

                        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                        Call clearRecords()
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblError.Text = myex.Message
                    Catch ex As Exception
                        transaction.Rollback()
                        lblError.Text = "Record could not be Inserted"

                    End Try
                End Using

            ElseIf ViewState("datamode") = "edit" Then
                Try

                    bEdit = True
                    ACD_ID = ViewState("viewid")
                    If ACD_ID <> 0 Then

                        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                            transaction = conn.BeginTransaction("SampleTransaction")
                            Try

                                'call the class to insert the record into ACADEMICYEAR_D

                                status = AccessStudentClass.SaveACADEMICYEAR_D(ACD_ID, ACD_ACY_ID, ACD_BSU_ID, ACD_CLM_ID, _
                    ACD_STARTDT, ACD_ENDDT, ACD_CURRENT, ACD_OPENONLINE, ACD_AFFLNO, ACD_MOEAFFLNO, ACD_LICENSE, ACD_ATT_CLOSINGDATE, ACD_BROWNBOOKDATE, bEdit, transaction)
                                'throw error if insert to the ACADEMICYEAR_D is not Successfully

                                If status = 706 Then
                                    Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                                ElseIf status = 707 Then
                                    Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                                ElseIf status <> 0 Then
                                    Throw New ArgumentException("Record could not be Inserted")
                                End If

                                'insert record for the last month
                                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ACD_ID, "edit", Page.User.Identity.Name.ToString, Me.Page)

                                If flagAudit <> 0 Then
                                    Throw New ArgumentException("Could not process your request")
                                End If

                                transaction.Commit()

                                lblError.Text = "Record Updated Successfully"

                                ViewState("datamode") = "none"
                                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                                Call clearRecords()
                            Catch myex As ArgumentException
                                transaction.Rollback()
                                lblError.Text = myex.Message
                            Catch ex As Exception
                                transaction.Rollback()
                                lblError.Text = "Record could not be Updated"

                            End Try
                        End Using
                    Else
                        lblError.Text = "Select the record to be edited."
                    End If
                Catch ex As Exception
                    lblError.Text = "Select the record to be edited."
                End Try

            End If





        End If
    End Sub
End Class
