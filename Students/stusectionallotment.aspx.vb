Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Partial Class Students_stusectionallotment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'code added by dhanya 16-11-08
        ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnYes)
        ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnNo)


        If Not IsPostBack Then
            Try
                CheckMenuandUser()
                BindYear()
               
                Session("stu_id_allot") = Nothing
                
            Catch ex As Exception

            End Try

        End If
        Try
            BindSectionHistory()
        Catch ex As Exception

        End Try

    End Sub
    
    
    Public Sub CheckMenuandUser()
        Dim Encr_decrData As New Encryption64
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim USR_NAME As String = Session("sUsr_name")

        If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100053") Then
            If Not Request.UrlReferrer Is Nothing Then
                Response.Redirect(Request.UrlReferrer.ToString())
            Else

                Response.Redirect("~\noAccess.aspx")
            End If
        Else
            ViewState("datamode") = "edit"
          
            ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, Session("sBsuid"), ViewState("MainMnu_code"))

            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        End If

    End Sub
    Public Sub BindYear()
        Dim acc As New studClass
        Dim clm As String = Session("clm").ToString ''"14"
        Dim bsu_id As String = Session("sBsuid").ToString ''"125016"
        ddAccYear = acc.PopulateAcademicYear(ddAccYear, clm, bsu_id)
        ddAccYear_SelectedIndexChanged(ddAccYear, Nothing)
    End Sub
    Public Sub BindGrade()
        Dim acc As New studClass
        Dim accid = ddAccYear.SelectedValue
        Dim SHF_ID As String = String.empty
        Dim STM_ID As String = String.Empty
        'If ddShift.SelectedIndex <> -1 Then
        '    SHF_ID = ddShift.SelectedValue
        'Else
        '    SHF_ID = ""
        'End If
        If ddStream.SelectedIndex <> -1 Then
            STM_ID = ddStream.SelectedValue
        Else
            STM_ID = ""
        End If
        ddGrade = acc.PopulateGrade(ddGrade, accid, "", STM_ID)
    End Sub
    Public Sub BindSection()
        Dim bsu_id As String = Session("sBsuid").ToString '' "125016"
        Dim acd_id As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        Dim SHF_ID As String = String.Empty

        If ddAccYear.SelectedIndex <> -1 Then
            acd_id = ddAccYear.SelectedValue
        Else
            acd_id = "0"
        End If

        If ddGrade.SelectedIndex <> -1 Then
            GRD_ID = ddGrade.SelectedValue
        Else
            GRD_ID = ""
        End If
        If ddShift.SelectedIndex <> -1 Then
            SHF_ID = ddShift.SelectedValue
        Else
            SHF_ID = "0"
        End If
        If ddStream.SelectedIndex <> -1 Then
            STM_ID = ddStream.SelectedValue
        Else
            STM_ID = "0"
        End If

        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query = "SELECT [SCT_ID],[SCT_DESCR] FROM [OASIS].[dbo].[SECTION_M] where [SCT_GRM_ID] in (SELECT [GRM_ID]  FROM [OASIS].[dbo].[GRADE_BSU_M] where [GRM_BSU_ID]='" & bsu_id & "' and [GRM_ACD_ID]='" & acd_id & "' and GRM_GRD_ID='" & GRD_ID & "' and GRM_SHF_ID='" & SHF_ID & "' and GRM_STM_ID='" & STM_ID & "') order by SCT_DESCR"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        ddSection.DataSource = ds
        ddSection.DataTextField = "SCT_DESCR"
        ddSection.DataValueField = "SCT_ID"
        ddSection.DataBind()

        If ds.Tables(0).Rows.Count = 0 Then
            lblmessage.Text = "No Section Under Selected Grade"
        Else
            lblmessage.Text = ""
        End If

    End Sub
   
    Public Sub BindNull()
        Dim dt As New DataTable
        dt.Columns.Add("STU_ID")
        dt.Columns.Add("STU_NO")
        dt.Columns.Add("STU_FIRSTNAME")
        dt.Columns.Add("STU_LASTNAME")
        dt.Columns.Add("SHF_DESCR")
        dt.Columns.Add("STM_DESCR")
        Dim drow As DataRow = dt.NewRow()
        drow.Item("STU_ID") = ""
        drow.Item("STU_NO") = ""
        drow.Item("STU_FIRSTNAME") = ""
        drow.Item("STU_LASTNAME") = ""
        drow.Item("SHF_DESCR") = ""
        drow.Item("STM_DESCR") = ""
        dt.Rows.Add(drow)
        GridAllot.DataSource = dt
        GridAllot.DataBind()
        DirectCast(GridAllot.Rows(0).FindControl("ch1"), CheckBox).Visible = False
        DirectCast(GridAllot.Rows(0).FindControl("ch1"), CheckBox).Visible = False

    End Sub
    Public Sub BindGrid()


        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim acd_id As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        Dim SHF_ID As String = String.Empty

        If ddAccYear.SelectedIndex <> -1 Then
            acd_id = ddAccYear.SelectedValue
        Else
            acd_id = "0"
        End If

        If ddGrade.SelectedIndex <> -1 Then
            GRD_ID = ddGrade.SelectedValue
        Else
            GRD_ID = ""
        End If
        If ddShift.SelectedIndex <> -1 Then
            SHF_ID = ddShift.SelectedValue
        Else
            SHF_ID = "0"
        End If
        If ddStream.SelectedIndex <> -1 Then
            STM_ID = ddStream.SelectedValue
        Else
            STM_ID = "0"
        End If

        Dim sql_query = " SELECT STU_ID,STU_NO, STU_SHF_ID,STU_STM_ID,stu_sct_id,stu_grd_id,stu_acd_id,STM_DESCR,SHF_DESCR,SNAME,STU_DOJ,STU_GENDER FROM( " & _
" select STU_ID,STU_NO, STU_SHF_ID,STU_STM_ID,stu_sct_id,stu_grd_id,stu_acd_id,STM_DESCR,SHF_DESCR, " & _
 " STU_FIRSTNAME+' '+ISNULL(STU_MIDNAME,'')+' '+ ISNULL(STU_LASTNAME,'') AS SNAME,STU_DOJ,STU_GENDER from  student_m," & _
" stream_m,shifts_m where student_m.stu_stm_id = stream_m.stm_id  and student_m.stu_shf_id= shifts_m.shf_id and  " & _
" (student_m.stu_sct_id is null or student_m.stu_sct_id=(select sct_id from section_m where sct_grm_id=student_m.stu_grm_id and sct_descr='TEMP'))" & _
" and student_m.stu_grd_id='" & GRD_ID & "' and student_m.stu_acd_id='" & acd_id & "' " & _
" and student_m.stu_STM_id='" & STM_ID & "' and student_m.stu_SHF_id='" & SHF_ID & "' " & _
"  AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE())  " & _
"  AND STU_CURRSTATUS NOT IN ('CN','TF') ) A WHERE STU_ID<>'' "


        If txtStuNo.Text <> "" Then
            sql_query += " AND STU_NO LIKE '%" + txtStuNo.Text.Trim + "%'"
        End If

        If txtName.Text <> "" Then
            sql_query += " AND SNAME LIKE '%" + txtName.Text.Trim + "%'"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query & "  ORDER BY SNAME,STU_NO ")
        If ds.Tables(0).Rows.Count > 0 Then
            GridAllot.DataSource = ds
            GridAllot.DataBind()
            btnsave.Visible = True
            btnCancel.Visible = True
        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            'start the count from 1 no matter gridcolumn is visible or not
            ' ds.Tables(0).Rows(0)(6) = True
            GridAllot.DataSource = ds.Tables(0)
            Try
                GridAllot.DataBind()
            Catch ex As Exception
            End Try

            Dim columnCount As Integer = GridAllot.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

            GridAllot.Rows(0).Cells.Clear()
            GridAllot.Rows(0).Cells.Add(New TableCell)
            GridAllot.Rows(0).Cells(0).ColumnSpan = columnCount
            GridAllot.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            GridAllot.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            'BindNull()
            btnsave.Visible = False
            btnCancel.Visible = False
        End If

    End Sub
  
    
   
    Public Sub bindShift()
        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = String.Empty
        Dim ds As DataSet
        Dim Acd_id As String = String.Empty
        If ddAccYear.SelectedIndex <> -1 Then
            Acd_id = ddAccYear.SelectedValue
        Else
            Acd_id = 0
        End If
        Dim GRD_ID As String = String.Empty
        If ddGrade.SelectedIndex <> -1 Then
            GRD_ID = ddGrade.SelectedValue
        Else
            GRD_ID = ""
        End If

        sql_query = "select SHF_ID,SHF_DESCR from SHIFTS_M WHERE SHF_ID IN(select distinct " _
        & " grm_shf_id from grade_bsu_m where GRM_GRD_ID='" & GRD_ID & "'  AND grm_acd_id='" & Acd_id & "')"
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        ddShift.DataSource = ds
        ddShift.DataTextField = "SHF_DESCR"
        ddShift.DataValueField = "SHF_ID"
        ddShift.DataBind()
    End Sub

    Public Sub bindStream()
        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query As String = String.Empty
        Dim ds As DataSet
        Dim Acd_id As String = String.Empty
        If ddAccYear.SelectedIndex <> -1 Then
            Acd_id = ddAccYear.SelectedValue
        Else
            Acd_id = 0
        End If
        Dim GRD_ID As String = String.Empty
        If ddGrade.SelectedIndex <> -1 Then
            GRD_ID = ddGrade.SelectedValue
        Else
            GRD_ID = ""
        End If
        sql_query = "SELECT STM_ID,STM_DESCR  FROM STREAM_M WHERE STM_ID IN( " _
        & " select distinct grm_STM_id from grade_bsu_m where GRM_BSU_ID='" & Session("sBsuid").ToString & "'  AND grm_acd_id='" & Acd_id & "')"
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        ddStream.DataSource = ds
        ddStream.DataTextField = "STM_DESCR"
        ddStream.DataValueField = "STM_ID"
        ddStream.DataBind()
        If ddStream.Items.Count > 0 Then
            ddStream.SelectedValue = "1"
        End If
    End Sub

    Protected Sub GridAllot_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridAllot.PageIndexChanged
        Dim hash As New Hashtable
        If Not Session("stu_id_allot") Is Nothing Then
            hash = Session("stu_id_allot")
        End If


        Dim chk As CheckBox
        Dim STU_ID As String = String.Empty
        For Each rowItem As GridViewRow In GridAllot.Rows
            chk = DirectCast(rowItem.FindControl("ch1"), CheckBox)

            STU_ID = DirectCast(rowItem.FindControl("HiddenStudid"), HiddenField).Value
            If hash.ContainsValue(STU_ID) = True Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
        Next

    End Sub

    Protected Sub GridAllot_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            GridAllot.PageIndex = e.NewPageIndex
            Dim hash As New Hashtable
            If Not Session("stu_id_allot") Is Nothing Then
                hash = Session("stu_id_allot")
            End If


            Dim chk As CheckBox
            Dim STU_ID As String = String.Empty
            For Each rowItem As GridViewRow In GridAllot.Rows
                ' chk = DirectCast((rowItem.Cells(0).FindControl("chkList")), CheckBox)
                chk = DirectCast(rowItem.FindControl("ch1"), CheckBox)
                'DirectCast(row.FindControl("lblEnqId"), Label).Text)
                STU_ID = DirectCast(rowItem.FindControl("HiddenStudid"), HiddenField).Value
                If chk.Checked = True Then
                    If hash.Contains(STU_ID) = False Then
                        hash.Add(STU_ID, DirectCast(rowItem.FindControl("HiddenStudid"), HiddenField).Value)
                    End If
                Else
                    If hash.Contains(STU_ID) = True Then
                        hash.Remove(STU_ID)
                    End If
                End If
            Next
            Session("stu_id_allot") = hash
            BindGrid()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Public Sub savedata()
        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim sql_query = ""
        Dim sectionid = ddSection.SelectedValue.Trim()
        Dim flag = 0
        Dim stuIds As String = ""

        Dim chkSelect As New CheckBox

        Dim chk As CheckBox
        Dim STU_ID As String = String.Empty
        Dim hash As New Hashtable
        If Not Session("stu_id_allot") Is Nothing Then
            hash = Session("stu_id_allot")
        End If
        For Each rowItem As GridViewRow In GridAllot.Rows

            chk = DirectCast(rowItem.FindControl("ch1"), CheckBox)

            STU_ID = DirectCast(rowItem.FindControl("HiddenStudid"), HiddenField).Value
            If chk.Checked = True Then
                If hash.Contains(STU_ID) = False Then
                    hash.Add(STU_ID, DirectCast(rowItem.FindControl("HiddenStudid"), HiddenField).Value)
                End If
            Else
                If hash.Contains(STU_ID) = True Then
                    hash.Remove(STU_ID)
                End If
            End If



        Next

        Session("stu_id_allot") = hash


        If Not Session("stu_id_allot") Is Nothing Then
            hash = Session("stu_id_allot")
            Dim hashloop As DictionaryEntry

            For Each hashloop In hash
                sql_query = "update student_m set stu_sct_id='" & sectionid & "' , stu_sct_id_join='" & sectionid & "' where stu_id='" & hashloop.Value.ToString() & "'"
                SqlHelper.ExecuteNonQuery(sql_conn, CommandType.Text, sql_query)
                sql_query = "update STUDENT_PROMO_S set stp_sct_id='" & sectionid & "' where stp_stu_id='" & hashloop.Value.ToString() & "'"
                SqlHelper.ExecuteNonQuery(sql_conn, CommandType.Text, sql_query)
                sql_query = "SELECT SCT_GRM_ID FROM SECTION_M where sct_bsu_id='" & Session("sBsuid").ToString & "' and sct_acd_id='" & ddAccYear.SelectedValue & "' and sct_id='" & ddSection.SelectedValue & "' "
                Dim GRM_ID = SqlHelper.ExecuteScalar(sql_conn, CommandType.Text, sql_query)
                sql_query = "update student_m set STU_GRM_ID='" & GRM_ID.ToString() & "' where stu_id='" & hashloop.Value.ToString() & "'"
                SqlHelper.ExecuteNonQuery(sql_conn, CommandType.Text, sql_query)

                sql_query = "SELECT SCT_ID FROM SECTION_M where SCT_GRM_ID='" & GRM_ID.ToString() & "' and sct_acd_id='" & ddAccYear.SelectedValue & "' and SCT_DESCR='TEMP' "
                Dim SCT_ID_TEMP = SqlHelper.ExecuteScalar(sql_conn, CommandType.Text, sql_query)
                UtilityObj.InsertAuditdetails(sql_conn, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hashloop.Value.ToString, "SECTION_ALLOT")


                '-- Call Fee Function during Section Allotment
                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", hashloop.Value.ToString())
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid").ToString)
                pParms(2) = New SqlClient.SqlParameter("@OLD_GRD_ID", ddGrade.SelectedValue)
                pParms(3) = New SqlClient.SqlParameter("@OLD_SCT_ID", SCT_ID_TEMP)
                pParms(4) = New SqlClient.SqlParameter("@OLD_GRM_ID", GRM_ID.ToString())
                pParms(5) = New SqlClient.SqlParameter("@NEW_GRD_ID", ddGrade.SelectedValue)
                pParms(6) = New SqlClient.SqlParameter("@NEW_SCT_ID", sectionid)
                pParms(7) = New SqlClient.SqlParameter("@NEW_GRM_ID", GRM_ID.ToString())
                pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(sql_conn, CommandType.StoredProcedure, "UpdateFeeTablesOnSectionAllotment", pParms)


                ''CODE ADDED BY DHANYA 03-08-08
                'Try
                '    sql_conn = ConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
                '    sql_query = "update student_m set STU_GRM_ID='" & GRM_ID.ToString() & "',stu_sct_id='" & sectionid & "' , stu_sct_id_join='" & sectionid & "' where stu_id='" & hid.Value.ToString() & "'"
                '    SqlHelper.ExecuteNonQuery(sql_conn, CommandType.Text, sql_query)
                'Catch ex As Exception
                'End Try
                flag = 1


                'code added by dhanya 16-11-08
                stuIds += "<ID><STU_ID>" + hashloop.Value + "</STU_ID></ID>"
            Next
            Session("stu_id_allot") = Nothing

        End If


        If flag = 1 Then
            BindYear()
            ddAccYear_SelectedIndexChanged(ddAccYear, Nothing)

            ddGrade_SelectedIndexChanged(ddGrade, Nothing)
            lblmessage.Text = "Section allotment done successfully for selected students"
            'GridAllot.Visible = False
            'CODE ADDED BY DHANYA 16-11-08
            If stuIds <> "" Then
                ' hfStuIds.Value = "<IDS>" + stuIds + "</IDS>"
                ViewState("stuIds") = "<IDS>" + stuIds + "</IDS>"
                If ChkPrint.Checked = True Then
                    PrintAdmitSlip()
                End If
            Else
                'hfStuIds.Value = "<IDS><ID><STU_ID>NULL</STU_ID></ID></IDS>"
                ViewState("stuIds") = "<IDS><ID><STU_ID>NULL</STU_ID></ID></IDS>"

            End If
        Else
            'hfStuIds.Value = "error"
            ViewState("stuIds") = "error"
            lblmessage.Text = "Record could not be saved !!"
        End If

        

    End Sub

    Sub PrintAdmitSlip()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("principal", GetEmpName("PRINCIPAL"))
        param.Add("registrar", GetEmpName("REGISTRAR"))
        param.Add("@STU_XML", ViewState("stuIds"))
        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportPath = Server.MapPath("../Students/Reports/RPT/rptAdmitSlip.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub

    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
        '                         & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
        '                         & " AND DES_DESCR='" + designation + "'"
        'Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'If emp = Nothing Then
        '    emp = ""
        'End If
        'Return emp

        Dim emp As String
        Dim pParms(3) As SqlClient.SqlParameter
        emp = ""
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@DES_DESCR", designation)
        Using reader_EQS As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_EMPNAME_DESIG", pParms)
            While reader_EQS.Read
                emp = Convert.ToString(reader_EQS("EMP_NAME"))
            End While
        End Using
        Return emp
    End Function
    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If ddSection.Items.Count > 0 Then
                If Stream_Shift_Check() Then
                    savedata()
                    
                Else
                    '' confirm 
                    lblmessage.Text = "Shift or Stream is different for the selected students?"

                    GridAllot.Enabled = False

                    Exit Sub
                End If
               

            Else

                lblmessage.Text = "No Section Under Selected Grade"
            End If
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddAccYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddAccYear.SelectedIndexChanged
        Try
            bindStream()
            BindGrade()
            ddGrade_SelectedIndexChanged(ddGrade, Nothing)
            GridAllot.Visible = False
            Session("stu_id_allot") = Nothing
            btnsave.Visible = False
            btnCancel.Visible = False
            Session("WEB_SER_VAR") = ddAccYear.SelectedValue.ToString & "|" & ddGrade.SelectedValue.ToString
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Protected Sub ddGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddGrade.SelectedIndexChanged
        Try
            bindShift()
            'bindStream()
            GridAllot.Visible = False
            Session("stu_id_allot") = Nothing
            BindSection()
            BindSectionHistory()
            Session("WEB_SER_VAR") = ddAccYear.SelectedValue.ToString & "|" & ddGrade.SelectedValue.ToString
            btnsave.Visible = False
            btnCancel.Visible = False
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Public Function Stream_Shift_Check() As Boolean
        Dim validate As Boolean = True
        Dim sec = ddSection.SelectedItem.Text.Trim()
        Dim shift = ""
        Dim stream = ""
        For Each row As GridViewRow In grdsectionhistory.Rows
            If DirectCast(row.FindControl("lblsct"), Label).Text.Trim() = sec Then
                stream = DirectCast(row.FindControl("lblstream"), Label).Text.Trim()
                shift = DirectCast(row.FindControl("lblshift"), Label).Text.Trim()
            End If
        Next
        For Each rows As GridViewRow In GridAllot.Rows
            Dim shifts = DirectCast(rows.FindControl("lblShifts"), Label).Text.Trim()
            Dim streams = DirectCast(rows.FindControl("lblStreams"), Label).Text.Trim()
            Dim check As CheckBox = DirectCast(rows.FindControl("ch1"), CheckBox)
            If check.Checked Then
                If shifts <> shift Or streams <> stream Then
                    validate = False
                End If
            End If

        Next
        Return validate
    End Function

    Public Sub BindSectionHistory()
        Dim sql_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim bsu_id As String = Session("sBsuid").ToString
        Dim acd_id As String = String.Empty
        Dim GRD_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        If ddAccYear.SelectedIndex <> -1 Then
            acd_id = ddAccYear.SelectedValue
        Else
            acd_id = "0"
        End If

        If ddGrade.SelectedIndex <> -1 Then
            GRD_ID = ddGrade.SelectedValue
        Else
            GRD_ID = ""
        End If
        If ddStream.SelectedIndex <> -1 Then
            STM_ID = ddStream.SelectedValue
        Else
            STM_ID = ""
        End If
        Dim sql_query = "SELECT SCT_ID,SCT_DESCR,SCT_MAXSTRENGTH, " &
                        " alloted=(SELECT COUNT(STU_ID) FROM vw_STUDENTS_TC WHERE STU_SCT_ID=A.SCT_ID AND TCM_LEAVEDATE IS NULL AND STU_CURRSTATUS NOT IN ('CN','TF'))," &
                        " alloted_Male=(SELECT COUNT(STU_ID) FROM vw_STUDENTS_TC WHERE Upper(Left(STU_GENDER,1))='M' AND STU_SCT_ID=A.SCT_ID AND TCM_LEAVEDATE IS NULL AND STU_CURRSTATUS NOT IN ('CN','TF'))," &
                        " alloted_FeMale=(SELECT COUNT(STU_ID) FROM vw_STUDENTS_TC WHERE Upper(Left(STU_GENDER,1))='F' AND STU_SCT_ID=A.SCT_ID AND TCM_LEAVEDATE IS NULL AND STU_CURRSTATUS NOT IN ('CN','TF'))," &
                        " (select SHF_DESCR from shifts_m where shf_id=  b.GRM_SHF_ID) as shift," &
                        " (select STM_DESCR from stream_m where STM_ID=  c.STM_ID)as stream" &
                        " FROM SECTION_M as A" &
                        " inner join (select GRM_ID,GRM_SHF_ID,GRM_STM_ID from GRADE_BSU_M ) as b on b.GRM_ID =SCT_GRM_ID" &
                        " inner join (select STM_ID,STM_DESCR from stream_m ) as c on b.GRM_STM_ID = c.STM_ID" &
                        " where [SCT_GRM_ID] in " &
                        " (SELECT [GRM_ID]  FROM [OASIS].[dbo].[GRADE_BSU_M] where [GRM_BSU_ID]='" & bsu_id & "' and [GRM_ACD_ID]='" & acd_id & "' and GRM_GRD_ID='" & GRD_ID & "' and GRM_STM_ID='" & STM_ID & "')" &
                        " order by SCT_DESCR "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(sql_conn, CommandType.Text, sql_query)
        grdsectionhistory.DataSource = ds
        grdsectionhistory.DataBind()
    End Sub

    

    Protected Sub checkall_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim c As CheckBox = DirectCast(GridAllot.HeaderRow.FindControl("checkall"), CheckBox)

            For Each row As GridViewRow In GridAllot.Rows
                If c.Checked Then
                    DirectCast(row.FindControl("ch1"), CheckBox).Checked = True
                Else
                    DirectCast(row.FindControl("ch1"), CheckBox).Checked = False
                End If
            Next
        Catch ex As Exception
            'lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridAllot.Visible = True
            Session("stu_id_allot") = Nothing
            BindGrid()
          
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    Protected Sub ddShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)



        Dim accid = ddAccYear.SelectedValue
        Dim SHF_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        If ddShift.SelectedIndex <> -1 Then
            SHF_ID = ddShift.SelectedValue
        Else
            SHF_ID = ""
        End If
        If ddStream.SelectedIndex <> -1 Then
            STM_ID = ddStream.SelectedValue
        Else
            STM_ID = ""
        End If




        BindSection()
        Session("WEB_SER_VAR") = ddAccYear.SelectedValue.ToString & "|" & ddGrade.SelectedValue.ToString

        GridAllot.Visible = False
        Session("stu_id_allot") = Nothing
        btnsave.Visible = False
        btnCancel.Visible = False
    End Sub

    Protected Sub ddStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim accid = ddAccYear.SelectedValue
        Dim SHF_ID As String = String.Empty
        Dim STM_ID As String = String.Empty
        If ddShift.SelectedIndex <> -1 Then
            SHF_ID = ddShift.SelectedValue
        Else
            SHF_ID = ""
        End If
        If ddStream.SelectedIndex <> -1 Then
            STM_ID = ddStream.SelectedValue
        Else
            STM_ID = ""
        End If

        BindGrade()
        ddGrade_SelectedIndexChanged(ddGrade, Nothing)
        Session("WEB_SER_VAR") = ddAccYear.SelectedValue.ToString & "|" & ddGrade.SelectedValue.ToString


        GridAllot.Visible = False
        Session("stu_id_allot") = Nothing
        btnsave.Visible = False
        btnCancel.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then

                GridAllot.Enabled = True
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Response.Redirect(ViewState("ReferrerUrl"))
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
End Class
