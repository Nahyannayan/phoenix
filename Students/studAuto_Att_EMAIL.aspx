<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="studAuto_Att_EMAIL.aspx.vb" Inherits="studAutoAttEMAIL"
    Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        .reToolZone {
            display: none;
        }

        .reContentCell iframe {
            height: 250px !important;
        }

        .Default.RadEditor, .Default.RadEditor .reModule, .Default.RadEditor .reEditorModes, .Default.RadEditor .reWrapper {
            background-color: #eee;
            height: 400px !important;
            min-height: 400px !important;
        }

        .darkPanlAlumini {
            width: 100%;
            height: 100%;
            position: fixed;
            left: 0%;
            top: 0%;
            background: rgba(0,0,0,0.2) !important;
            /*display: none;*/
            display: block;
        }

        .inner_darkPanlAlumini {
            left: 20%;
            top: 10%;
            position: fixed;
            width: 70%;
        }
    </style>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Schedule Attendance Email"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <span style="float: left; display: block;">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" />
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                        ValidationGroup="AttGroup" DisplayMode="List" ForeColor="" />
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="top">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date <font color="red">*</font></span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal1" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup"
                                            CssClass="error" ForeColor="">*</asp:RequiredFieldValidator>
                                        <ajaxToolkit:CalendarExtender ID="cal1" runat="server" TargetControlID="txtfrom"
                                            Format="dd/MMM/yyyy" PopupButtonID="imgcal1">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">To Date <font color="red">*</font></span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal2" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvTo" runat="server" ControlToValidate="txtTo" Display="Dynamic"
                                            ErrorMessage="To Date required" ValidationGroup="AttGroup" CssClass="error"
                                            ForeColor="">*</asp:RequiredFieldValidator>

                                        <ajaxToolkit:CalendarExtender ID="Cal2" runat="server" TargetControlID="txtTo" Format="dd/MMM/yyyy" PopupButtonID="imgcal2">
                                        </ajaxToolkit:CalendarExtender>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Schedule(Hrs:) <font color="red">*</font></span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlHrs" runat="server">
                                        </asp:DropDownList></td>
                                    <td align="left"><span class="field-label">Schedule(Mins:) <font color="red">*</font></span></td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlMin" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Applicable Grades <font color="red">*</font></span></td>
                                    <td align="left">
                                        <%--               <asp:Panel ID="plGrd"
                                            runat="server" ScrollBars="Vertical"  Width="100%">--%>
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="chkGrd" runat="server" RepeatColumns="5"
                                                RepeatDirection="Vertical" RepeatLayout="Flow">
                                            </asp:CheckBoxList>
                                        </div>
                                        <%--</asp:Panel>--%>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Parameters <font color="red">*</font></span></td>
                                    <td align="left">
                                        <%--                                        <asp:Panel ID="plparam"
                                            runat="server" ScrollBars="Vertical"   Width="100%">--%>
                                        <div class="checkbox-list">
                                            <asp:CheckBoxList ID="chkparam" runat="server"
                                                RepeatDirection="Vertical" RepeatLayout="Flow">
                                            </asp:CheckBoxList>
                                        </div>
                                        <%--</asp:Panel>--%>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Activate</span></td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkActive" runat="server" />
                                    </td>
                                    <td align="left"><span class="field-label">Email Content</span></td>
                                    <td align="left">
                                        <asp:LinkButton ID="lbtnEdit" CssClass="button" runat="server" Text="Edit"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="center">
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                ValidationGroup="AttGroup" /><asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                    CssClass="button" Text="Cancel" />&nbsp;
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hfDate" runat="server"></asp:HiddenField>

                <asp:Panel ID="plEmailContent" runat="server" CssClass="darkPanlAlumini" Visible="false">
                    <div class="panel-cover inner_darkPanlAlumini">
                        <div style="float: right; vertical-align: top; padding-bottom: 12px;">
                            <asp:ImageButton ID="imgEmailContentClose" runat="server" ToolTip="click here to close"
                                ImageUrl="~/IMAGES/closeme.png" OnClientClick="javascript:__doPostBack(this.name,'');return false;" />
                        </div>
                        <div style="text-align: left;" class="title-bg">
                            &nbsp;Edit Email Content
                        </div>
                        <div>
                            <table align="left" width="100%" border="0" cellpadding="4" cellspacing="1">
                                <tr>
                                    <td align="left">
                                        <telerik:RadEditor ID="txtEmailContent" runat="server" EditModes="All" Width="100%" Style="height: 400px; width: 100%; min-height: 200px !important; min-width: 1084px;">
                                        </telerik:RadEditor>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btnEmailContentCancel" runat="server" CausesValidation="False"
                                            CssClass="button" Text="Cancel" /></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>
</asp:Content>

