Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studTCSO_View
    Inherits System.Web.UI.Page

    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or ViewState("MainMnu_code") <> "S050175" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    BindApplicationDecision()

                    Call gridbind()

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If

    End Sub
   
    Public Sub gridbind()
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_App As String = String.Empty

            If ddlAppDes.SelectedIndex <> -1 Then
                If ddlAppDes.SelectedItem.Text = "ALL" Then
                    str_filter_App = " and BAL_APL_ID<>''"
                Else
                    str_filter_App = " and BAL_APL_ID='" & ddlAppDes.SelectedItem.Value & "'"
                End If
            Else

            End If



            Dim ds As New DataSet

            str_Sql = "SELECT     BAL_ID, BAL_SUB as Subject, substring(BAL_MAT,1,80)+ '....' as Matter, substring(BAL_REM,1,50)+ '....' as Remarks,substring( BAL_SIGN,1,50) AS SignApp, substring(BAL_ACK,1,80) + '....' AS ACK " & _
" FROM  BSU_APPL_LETTER where BAL_BSU_ID='" & Session("sBsuid") & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_App & " ORDER BY BAL_SUB")


            If ds.Tables(0).Rows.Count > 0 Then

                gvoffLetter_View.DataSource = ds.Tables(0)
                gvoffLetter_View.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                'ds.Tables(0).Rows(0)(6) = True

                gvoffLetter_View.DataSource = ds.Tables(0)
                Try
                    gvoffLetter_View.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvoffLetter_View.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvoffLetter_View.Rows(0).Cells.Clear()
                gvoffLetter_View.Rows(0).Cells.Add(New TableCell)
                gvoffLetter_View.Rows(0).Cells(0).ColumnSpan = columnCount
                gvoffLetter_View.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvoffLetter_View.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub



    Protected Sub gvoffLetter_View_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvoffLetter_View.RowCommand
        If e.CommandName = "View" Then

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvoffLetter_View.Rows(index), GridViewRow)
            Dim UserIDLabel As Label

            UserIDLabel = DirectCast(selectedRow.Cells(0).Controls(1), Label)


            Dim Eid As String = UserIDLabel.Text

            Dim url As String

            Eid = Encr_decrData.Encrypt(Eid)
            ViewState("datamode") = "view"
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))

            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\STUBSU_APPLIC_EDIT.aspx?MainMnu_code={0}&datamode={1}&Eid={2}", ViewState("MainMnu_code"), ViewState("datamode"), Eid)
            Response.Redirect(url)

            'SelectedUserID = UserIDLabel.Text
            ' Server.Transfer("~\updateUser.aspx", True)
        End If
    End Sub

    Sub BindApplicationDecision()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT APL_ID,APL_DESCR FROM APPLICATIONDECISION_M WHERE APL_BSU_ID='" & Session("sBsuid") & "' ORDER BY APL_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAppDes.DataSource = ds
        ddlAppDes.DataTextField = "APL_DESCR"
        ddlAppDes.DataValueField = "APL_ID"

        ddlAppDes.DataBind()
        ddlAppDes.Items.Add(New ListItem("ALL", ""))
        ddlAppDes.ClearSelection()
        ddlAppDes.Items.FindByText("ALL").Selected = True

    End Sub

    Protected Sub ddlAppDes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\STUBSU_APPLIC_EDIT.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
End Class
