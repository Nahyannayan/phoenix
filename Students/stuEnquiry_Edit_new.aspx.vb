﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System
Imports System.Text.RegularExpressions
Imports UserControls_usrMessageBar
Partial Class Students_stuEnquiry_Edit_new
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Public Popup_Message As String = ""
    Public Popup_Message_Status As WarningType

    Protected Sub enquiry_show(ByVal an As Integer)
        If an = 1 Then
            mpxFeeSponsor.Show()
        Else
            mpxFeeSponsor.Hide()
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.MaintainScrollPositionOnPostBack = True

        Dim CurBsUnit As String = Session("sBsuid")

        AddHandler FeeSponsor1.enquiry_showpop, AddressOf enquiry_show


        If Page.IsPostBack = False Then

            Try

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                txtPIssDate.Attributes.Add("onblur", "fillPassport()")

                txtVIssDate.Attributes.Add("onblur", "fillVisa()")



                '    mnuMaster.Items(0).ImageUrl = "~/Images/tabmenu/btnMain2.jpg"
                BindReligion()

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                ViewState("menu_rights") = AccessRight.PageRightsID(Session("sUsr_name"), CurBsUnit, ViewState("MainMnu_code"))
                hfENQ_ID.Value = Encr_decrData.Decrypt(Request.QueryString("enqid").Replace(" ", "+"))
                hfEQS_ID.Value = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
                hfENQNo.Value = Encr_decrData.Decrypt(Request.QueryString("enqno").Replace(" ", "+"))
                hfSTGREQ.Value = Encr_decrData.Decrypt(Request.QueryString("stgcomp").Replace(" ", "+"))
                Session("Fee_ENQ_ID") = hfENQ_ID.Value

                hfActual_GRD.Value = Getgrade()

                Dim editstring As String() = Encr_decrData.Decrypt(Request.QueryString("editstring").Replace(" ", "+")).Split("|")

                txtEnqNo.Text = hfENQNo.Value
                txtAcadYear.Text = editstring(0)
                hfACD_ID.Value = editstring(1)
                txtGrade.Text = editstring(2)
                txtShift.Text = editstring(3)
                txtStream.Text = editstring(4)
                AboutUs()
                Call bindLanguage_dropDown()
                BindEmirate_info()
                BindCountry(ddlCOB)
                BindCountry(ddlPCountry)


                BindCountry(ddlFContPermCountry)

                BindCountry(ddlMContPermCountry)

                BindCountry(ddlGContPermCountry)

                BindCountry(ddlFContCurrCountry)
                BindCountry(ddlMContCurrCountry)
                BindCountry(ddlGContCurrCountry)




                BindCurriculum()
                BindNationality(ddlNationality)
                BindNationality(ddlFNationality1)
                BindNationality(ddlFNationality2)
                BindNationality(ddlMNationality1)
                BindNationality(ddlMNationality2)
                BindNationality(ddlGNationality1)
                BindNationality(ddlGNationality2)


                BindBusinessUnit_prev()
                BindBusinessUnit(ddlSUnit)
                BindBusinessUnit(ddlStaffUnit)
                BindBusinessUnit(ddlEUnit)
                BindCompany(ddlFCompany)
                BindCompany(ddlMCompany)
                BindCompany(ddlGCompany)

                BindNurSery()
                BindGrade()
                btnGridUpdate.Visible = False
                PopulateEnquiryData()



                If hfActual_GRD.Value <> "KG1" Or hfActual_GRD.Value <> "PK" Then

                    ltSchoolType.Text = "Previous School"
                    Session("TABLE_School_Pre") = CreateDataTable()
                    Session("TABLE_School_Pre").Rows.Clear()
                    ViewState("id") = 1
                    gridbind()
                End If
                populatePrev_data(editstring(2))

                chkOLang.Attributes.Add("onclick", "GetSelectedItem()")

                ddlFLang.Attributes.Add("onChange", "chkFirst_lang()")

                '...............code added by lijo on 26/08/2008

                ltAboutUs.Text = StrConv(GetBSU_NAME(), VbStrConv.ProperCase) & "? [please tick]"
                '.....................

                If hfActual_GRD.Value <> "KG1" Then
                    ddlNUnit.Enabled = False
                    txtNRegNo.Enabled = False
                    chkNursery.Enabled = False
                End If


                If Session("sRegister") = "REG" Then
                    TR1.Visible = True

                    If studClass.GetBsuShowDocs(Session("sbsuid")) = True Then

                        studClass.BindDocumentsList(lstNotSubmitted, "3", "false", hfEQS_ID.Value)
                        studClass.BindDocumentsList(lstSubmitted, "3", "true", hfEQS_ID.Value)
                        If lstNotSubmitted.Items.Count <> 0 Or lstSubmitted.Items.Count <> 0 Then
                            tr_documents.Visible = True
                        Else
                            tr_documents.Visible = False
                        End If
                        lnkDocs.Visible = False

                    Else
                        hfURL.Value = "studJoinDocuments.aspx?eqsid=" + hfEQS_ID.Value
                        lnkDocs.Visible = True
                        tr_documents.Visible = False
                    End If

                    If studClass.isFeeIdAuto(hfACD_ID.Value) = True Then
                        TR1_C3.Visible = False
                        TR1_C4.Visible = False
                    End If

                    ' txtregdate.Text = Format(Session("EntryDate"), "dd/MMM/yyyy")
                Else
                    TR1.Visible = False
                    tr_documents.Visible = False
                    txtregdate.Text = ""
                End If
                BSU_Enq_validation()
            Catch ex As Exception
                lblError.Text = "Request could not be processed "
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End If



        If (Request.QueryString("datamode") Is Nothing) Or (Request.QueryString("MainMnu_code") Is Nothing) Then
            If Not Request.UrlReferrer Is Nothing Then
                Response.Redirect(Request.UrlReferrer.ToString())
            Else
                Response.Redirect("~\noAccess.aspx")
            End If
        Else
            'get the menucode to confirm the user is accessing the valid page
            'if query string returns Eid  if datamode is view state
            Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            If ViewState("datamode") = "view" Then
                ' ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            Else
                'mnuMaster.Items(7).Selectable = True
            End If
        End If

    End Sub

    Private Sub bind_FCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlFContCurrCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFCity.Items.Clear()
                ddlFCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlFCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlFCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FState")
        End Try
    End Sub
    Private Sub bind_MCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlMContCurrCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlMCity.Items.Clear()
                ddlMCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlMCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlMCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_MState")
        End Try
    End Sub
    Private Sub bind_GCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlGContCurrCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlGCity.Items.Clear()
                ddlGCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlGCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlGCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_GState")
        End Try
    End Sub
    Private Sub bind_FArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlFCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFArea.Items.Clear()
                ddlFArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlFArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlFArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Private Sub bind_MArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlMCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlMArea.Items.Clear()
                ddlMArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlMArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlMArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Private Sub bind_GArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlGCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlGArea.Items.Clear()
                ddlGArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlGArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlGArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Protected Sub ddlFContCurrCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFContCurrCountry.SelectedIndexChanged
        bind_FCITY()
        ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

    End Sub

    Protected Sub ddlGContCurrCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGContCurrCountry.SelectedIndexChanged
        bind_GCITY()
        ddlGCity_SelectedIndexChanged(ddlGCity, Nothing)

    End Sub

    Protected Sub ddlMContCurrCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMContCurrCountry.SelectedIndexChanged
        bind_MCITY()
        ddlMCity_SelectedIndexChanged(ddlMCity, Nothing)
    End Sub
    Protected Sub ddlFCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFCity.SelectedIndexChanged
        bind_FArea()
    End Sub

    Protected Sub ddlMCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMCity.SelectedIndexChanged
        bind_MArea()
    End Sub
    Protected Sub ddlGCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGCity.SelectedIndexChanged
        bind_GArea()
    End Sub
    Private Function Getgrade() As String
        Dim conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "select  eqs_grd_id from enquiry_schoolprio_s where  eqs_id='" & hfEQS_ID.Value & "' and eqs_eqm_enqid='" & hfENQ_ID.Value & "'"

        Dim grd_id As String = CStr(SqlHelper.ExecuteScalar(conn, CommandType.Text, str_sql))
        Return grd_id

    End Function
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim SCH_ID As New DataColumn("SCH_ID", System.Type.GetType("System.String"))
            Dim EPS_Id As New DataColumn("EPS_Id", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL As New DataColumn("EQM_PREVSCHOOL", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_HEAD_NAME As New DataColumn("EQM_PREVSCHOOL_HEAD_NAME", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_REG_ID As New DataColumn("EQM_PREVSCHOOL_REG_ID", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_GRD_ID As New DataColumn("EQM_PREVSCHOOL_GRD_ID", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_MEDIUM As New DataColumn("EQM_PREVSCHOOL_MEDIUM", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_ADDRESS As New DataColumn("EQM_PREVSCHOOL_ADDRESS", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_CLM_ID As New DataColumn("EQM_PREVSCHOOL_CLM_ID", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_CITY As New DataColumn("EQM_PREVSCHOOL_CITY", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_CTY_ID As New DataColumn("EQM_PREVSCHOOL_CTY_ID", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_PHONE As New DataColumn("EQM_PREVSCHOOL_PHONE", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_FAX As New DataColumn("EQM_PREVSCHOOL_FAX", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_FROMDT As New DataColumn("EQM_PREVSCHOOL_FROMDT", System.Type.GetType("System.String"))
            Dim EQM_PREVSCHOOL_LASTATTDATE As New DataColumn("EQM_PREVSCHOOL_LASTATTDATE", System.Type.GetType("System.String"))
            Dim EPS_DETAIL_TYPE As New DataColumn("EPS_DETAIL_TYPE", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))

            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(SCH_ID)
            dtDt.Columns.Add(EPS_Id)
            dtDt.Columns.Add(EQM_PREVSCHOOL)
            dtDt.Columns.Add(EQM_PREVSCHOOL_HEAD_NAME)
            dtDt.Columns.Add(EQM_PREVSCHOOL_REG_ID)
            dtDt.Columns.Add(EQM_PREVSCHOOL_GRD_ID)
            dtDt.Columns.Add(EQM_PREVSCHOOL_MEDIUM)
            dtDt.Columns.Add(EQM_PREVSCHOOL_ADDRESS)
            dtDt.Columns.Add(EQM_PREVSCHOOL_CLM_ID)
            dtDt.Columns.Add(EQM_PREVSCHOOL_CITY)
            dtDt.Columns.Add(EQM_PREVSCHOOL_CTY_ID)
            dtDt.Columns.Add(EQM_PREVSCHOOL_PHONE)
            dtDt.Columns.Add(EQM_PREVSCHOOL_FAX)
            dtDt.Columns.Add(EQM_PREVSCHOOL_FROMDT)
            dtDt.Columns.Add(EQM_PREVSCHOOL_LASTATTDATE)
            dtDt.Columns.Add(EPS_DETAIL_TYPE)
            dtDt.Columns.Add(STATUS)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Create table")

            Return dtDt
        End Try
    End Function
    Sub gridbind()
        Try


            gvSchool.DataSource = Session("TABLE_School_Pre")
            gvSchool.DataBind()
            If Not Session("TABLE_School_Pre") Is Nothing Then
                If Session("TABLE_School_Pre").Rows.Count > 0 Then
                    ltSchoolType.Text = "Previous School"
                Else
                    ltSchoolType.Text = "Current School"

                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GridBind")
        End Try
    End Sub
    Private Sub bindLanguage_dropDown()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT LNG_ID,LNG_DESCR  FROM LANGUAGE_M order by LNG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlFLang.DataSource = ds
        ddlFLang.DataTextField = "LNG_DESCR"
        ddlFLang.DataValueField = "LNG_ID"
        ddlFLang.DataBind()
        chkOLang.DataSource = ds
        chkOLang.DataTextField = "LNG_DESCR"
        chkOLang.DataValueField = "LNG_ID"
        chkOLang.DataBind()
        ddlFLang.Items.Add(New ListItem("", ""))
        ddlFLang.ClearSelection()
        ddlFLang.Items.FindByText("").Selected = True



    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If Final_validation() = False Then
                If SaveData() = True Then
                    If Session("sRegister") = "REG" Then
                        If studClass.isFeeIdAuto(hfACD_ID.Value) = True Then
                            ' PrintRegSlip()
                        Else
                            Response.Redirect(ViewState("ReferrerUrl"))
                        End If

                    End If
                    'If SaveData() = True Then
                    '    lblError.Text = "Record saved successfully"
                    Popup_Message = "Record saved successfully"
                    Popup_Message_Status = WarningType.Success
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
                Else
                    lblError.Text = "Error while saving data"
                    Popup_Message = "Error while saving data"
                    Popup_Message_Status = WarningType.Danger
                    usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)

                End If
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            Popup_Message = "Request could not be processed"
            Popup_Message_Status = WarningType.Danger
            usrMessageBar.ShowNotification(Popup_Message, Popup_Message_Status)
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "scrollTop", "jQuery('html,body').animate({ scrollTop: 0 }, 1000);", True)
        End Try

    End Sub
    Sub PrintRegSlip()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("principal", GetEmpName("PRINCIPAL"))
        param.Add("registrar", GetEmpName("REGISTRAR"))
        'param.Add("@STU_XML", ViewState("stuIds"))

        Dim i As Integer
        Dim chkSelect As CheckBox
        Dim lblStuId As Label
        Dim stuIds As String = ""

        stuIds = hfEQS_ID.Value.ToString
        param.Add("@EQS_ID", stuIds)

        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
            .reportPath = Server.MapPath("../Students/Reports/RPT/rptRegSlip.rpt")
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
    End Sub
    Function GetEmpName(ByVal designation As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        'Dim str_query As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') FROM " _
        '                         & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID WHERE EMP_BSU_ID='" + Session("SBSUID") + "'" _
        '                         & " AND DES_DESCR='" + designation + "'"
        'Dim emp As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'If emp = Nothing Then
        '    emp = ""
        'End If
        'Return emp

        Dim emp As String
        Dim pParms(3) As SqlClient.SqlParameter
        emp = ""
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("SBSUID"))
        pParms(1) = New SqlClient.SqlParameter("@DES_DESCR", designation)
        Using reader_EQS As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "Get_EMPNAME_DESIG", pParms)
            While reader_EQS.Read
                emp = Convert.ToString(reader_EQS("EMP_NAME"))
            End While
        End Using
        Return emp
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(ResolveUrl("~/Students/EnquiryDashboard.aspx?MainMnu_code=LjcF1BVVHbk%3d&datamode=Zo4HhpVNpXc%3d"))
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub rdTYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdTYes.CheckedChanged
        Try
            If rdTYes.Checked = True Then
                ddlLocation.Enabled = True
                ddlSubLocation.Enabled = True
                ddlPickUp.Enabled = True
                BindLocation()
                BindSubLocation()
                BindPickUp()
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlSubLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubLocation.SelectedIndexChanged
        Try
            BindPickUp()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            BindSubLocation()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub rdTNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdTNo.CheckedChanged
        Try
            If rdTNo.Checked = True Then
                ddlLocation.Enabled = False
                ddlSubLocation.Enabled = False
                ddlPickUp.Enabled = False
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub gvServices_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvServices.PageIndexChanging
        Try
            gvServices.PageIndex = e.NewPageIndex
            BindServices()
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRight.Click
        Try
            studClass.UpdateDocListItems(lstNotSubmitted, lstSubmitted, "true", hfEQS_ID.Value)
            studClass.UpdateEnquiryStage("Complete", hfEQS_ID.Value)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub btnLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeft.Click
        Try
            studClass.UpdateDocListItems(lstSubmitted, lstNotSubmitted, "false", hfEQS_ID.Value)
            studClass.UpdateEnquiryStage("Complete", hfEQS_ID.Value)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#Region "Private Methods"
    'code added by lijo on 26/08/2008
    Sub AboutUs()
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim AboutUSString As String = "select MODE_ID,MODE_DESCR from ENQUIRY_MODE_M order by MODE_DESCR"

        Dim ds1 As New DataSet

        ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, AboutUSString)

        conn.Close()
        Dim row As DataRow
        For Each row In ds1.Tables(0).Rows
            chkAboutUs.Items.Add(New ListItem(row("MODE_DESCR"), row("MODE_ID").ToString.Trim))
        Next
    End Sub
    'code added by lijo on 26/08/2008
    Private Sub BindEmirate_info()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String '= "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M order by EMR_DESCR"

        If Session("sBSUID") <> "800017" Then
            str_Sql = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M WHERE EMR_ENQ_GROUP='0' OR EMR_ENQ_GROUP='1' order by EMR_DESCR"
        Else
            str_Sql = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M WHERE EMR_ENQ_GROUP='0' OR EMR_ENQ_GROUP='3' order by EMR_DESCR"
        End If



        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlFContCurrPOBOX_EMIR.DataSource = ds
        ddlFContCurrPOBOX_EMIR.DataTextField = "EMR_DESCR"
        ddlFContCurrPOBOX_EMIR.DataValueField = "EMR_CODE"
        ddlFContCurrPOBOX_EMIR.DataBind()

        ddlMContCurrPOBOX_EMIR.DataSource = ds
        ddlMContCurrPOBOX_EMIR.DataTextField = "EMR_DESCR"
        ddlMContCurrPOBOX_EMIR.DataValueField = "EMR_CODE"
        ddlMContCurrPOBOX_EMIR.DataBind()

        ddlGContCurrPOBOX_EMIR.DataSource = ds
        ddlGContCurrPOBOX_EMIR.DataTextField = "EMR_DESCR"
        ddlGContCurrPOBOX_EMIR.DataValueField = "EMR_CODE"
        ddlGContCurrPOBOX_EMIR.DataBind()

    End Sub
    Private Function GetBSU_NAME() As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString

        Dim str_sql As String = " Select BSU_NAME FROM   BUSINESSUNIT_M WHERE    BSU_ID = '" & Session("sbsuid") & "'"
        Return SqlHelper.ExecuteScalar(str_conn, Data.CommandType.Text, str_sql)
    End Function

    Private Function SaveData() As Boolean
        'Try
        Dim saveStatus As Boolean = False
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim primarycontact As String
        If rdFather.Checked = True Then
            primarycontact = "F"
        ElseIf rdMother.Checked = True Then
            primarycontact = "M"
        Else
            primarycontact = "G"
        End If

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                If ViewState("datamode") = "edit" Then
                    UtilityObj.InsertAuditdetails(transaction, "edit", "ENQUIRY_M", "EQM_ENQID", "EQM_ENQID", "EQM_ENQID=" + hfENQ_ID.Value.ToString)
                    UtilityObj.InsertAuditdetails(transaction, "edit", "ENQUIRY_PARENT_M", "EQP_ID", "EQP_EQM_ENQID", "EQP_EQM_ENQID=" + hfENQ_ID.Value.ToString)
                    UtilityObj.InsertAuditdetails(transaction, "edit", "ENQUIRY_SCHOOLPRIO_S", "EQS_ID", "EQS_EQM_ENQID", "EQS_ID=" + hfEQS_ID.Value.ToString)
                    UtilityObj.InsertAuditdetails(transaction, "edit", "ENQUIRY_SERVICES_D", "ESV_ID", "ESV_EQS_ID", "ESV_EQS_ID=" + hfEQS_ID.Value.ToString)
                End If

                Dim retFlag As Integer = 0
                retFlag = UpdateSTUDENQUIRY_M(transaction, primarycontact)
                If retFlag <> 0 Then
                    Throw New ArgumentException("Record could not be Saved.")
                End If

                retFlag = UpdateSTUDPARENTENQUIRY_M(transaction, primarycontact)
                If retFlag <> 0 Then
                    Throw New ArgumentException("Record could not be Saved.")
                End If

                retFlag = updateStudEnquirySchoolPrio_S(transaction)
                If retFlag <> 0 Then
                    Throw New ArgumentException("Record could not be Saved.")
                End If
                retFlag = SaveENQUIRY_PREVIOUS_SCHOOL_EDIT(transaction)
                If retFlag <> 0 Then
                    Throw New ArgumentException("Record could not be Saved.")
                End If

                SaveServices(transaction)

                Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "EQM_ENQID(" + hfENQ_ID.Value.ToString + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                If flagAudit <> 0 Then
                    Throw New ArgumentException("Could not process your request")
                End If

                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                ' SaveData = True
                'Catch ex As Exception
                '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                'End Try
                'Return SaveData()

                Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try
        End Using
    End Function

    Private Function UpdateSTUDENQUIRY_M(ByVal transaction As SqlTransaction, ByVal primarycontact As String) As Integer
        Try

            Dim str_query As String = String.Empty
            Dim passportname As String = txtApplFirstName.Text.Replace("'", "''").ToUpper.Trim + " " + txtApplMidName.Text.Replace("'", "''").ToUpper.Trim + " " + txtApplLastName.Text.Replace("'", "''").ToUpper.Trim


            Dim EQM_OTHLANG As String = String.Empty

            For Each item As ListItem In chkOLang.Items
                If (item.Selected) Then

                    EQM_OTHLANG = EQM_OTHLANG + item.Value + "|"
                End If
            Next

            Dim EQM_bPREVSCHOOLGEMS As Boolean
            Dim EQM_PREVSCHOOL_BSU_ID As String = String.Empty
            Dim EQM_PREVSCHOOL_NURSERY As String = String.Empty
            Dim EQM_PREVSCHOOL_REG_ID As String = String.Empty
            Dim EQM_PREVSCHOOL As String = String.Empty
            Dim EQM_PREVSCHOOL_CITY As String = String.Empty
            Dim EQM_PREVSCHOOL_CTY_ID As String = String.Empty
            Dim EQM_PREVSCHOOL_GRD_ID As String = String.Empty
            Dim EQM_PREVSCHOOL_CLM_ID As String = String.Empty
            Dim EQM_PREVSCHOOL_MEDIUM As String = String.Empty
            Dim EQM_PREVSCHOOL_LASTATTDATE As String = String.Empty
            Dim EQM_PREVSCHOOL_FROMDT As String = String.Empty
            Dim EQM_PREVSCHOOL_HEAD_NAME As String = String.Empty
            Dim EQM_PREVSCHOOL_ADDRESS As String = String.Empty
            Dim EQM_PREVSCHOOL_PHONE As String = String.Empty
            Dim EQM_PREVSCHOOL_FAX As String = String.Empty


            Dim EQM_APPLPASPRTISSDATE As String = String.Empty
            Dim EQM_APPLPASPRTEXPDATE As String = String.Empty
            Dim EQM_APPLVISAISSDATE As String = String.Empty
            Dim EQM_APPLVISAEXPDATE As String = String.Empty

            If IsDate(txtPIssDate.Text) = True Then
                EQM_APPLPASPRTISSDATE = txtPIssDate.Text
            End If
            If IsDate(txtPExpDate.Text) = True Then
                EQM_APPLPASPRTEXPDATE = txtPExpDate.Text
            End If
            If IsDate(txtVIssDate.Text) = True Then
                EQM_APPLVISAISSDATE = txtVIssDate.Text
            End If
            If IsDate(txtVExpDate.Text) = True Then
                EQM_APPLVISAEXPDATE = txtVExpDate.Text
            End If

            Dim SEMIRATES_ID_ENG As String = txtSEmiratesId_ENG.Text.Replace("'", "")
            Dim SEMIRATES_ID_ARB As String = txtSEmiratesId_ARB.Text.Replace("'", "")


            If hfActual_GRD.Value = "PK" Or hfActual_GRD.Value = "KG1" Then
                EQM_bPREVSCHOOLGEMS = False
                EQM_PREVSCHOOL_REG_ID = txtRegNo.Text
                EQM_PREVSCHOOL_NURSERY = ddlPreSchool_Nursery.SelectedValue
                If ddlPreSchool_Nursery.SelectedItem.Text.ToUpper() = "OTHER" Then
                    EQM_PREVSCHOOL = txtPreSchool_Nursery.Text
                End If

            Else
                If (Not Session("TABLE_School_Pre") Is Nothing) And (Session("TABLE_School_Pre").Rows.Count > 0) Then
                    For i As Integer = 0 To Session("TABLE_School_Pre").Rows.Count - 1
                        If Session("TABLE_School_Pre").Rows(i)("EPS_DETAIL_TYPE") = "Current School" Then
                            EQM_bPREVSCHOOLGEMS = IIf(Session("TABLE_School_Pre").Rows(i)("sch_id") = "", False, True)
                            EQM_PREVSCHOOL_BSU_ID = Session("TABLE_School_Pre").Rows(i)("sch_id")
                            EQM_PREVSCHOOL = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL"))
                            EQM_PREVSCHOOL_HEAD_NAME = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_HEAD_NAME"))
                            EQM_PREVSCHOOL_REG_ID = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_REG_ID"))
                            EQM_PREVSCHOOL_GRD_ID = Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_GRD_ID")
                            EQM_PREVSCHOOL_MEDIUM = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_MEDIUM"))
                            EQM_PREVSCHOOL_ADDRESS = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_ADDRESS"))
                            EQM_PREVSCHOOL_CLM_ID = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_CLM_ID"))
                            EQM_PREVSCHOOL_CITY = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_CITY"))
                            EQM_PREVSCHOOL_CTY_ID = Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_CTY_ID")
                            EQM_PREVSCHOOL_PHONE = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_PHONE"))
                            EQM_PREVSCHOOL_FAX = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_FAX"))
                            EQM_PREVSCHOOL_FROMDT = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_FROMDT"))
                            EQM_PREVSCHOOL_LASTATTDATE = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_LASTATTDATE"))
                        End If
                    Next

                End If
            End If

            Dim paraEnq_M(85) As SqlClient.SqlParameter
            paraEnq_M(0) = New SqlClient.SqlParameter("@EQM_ENQID", hfENQ_ID.Value)
            paraEnq_M(1) = New SqlClient.SqlParameter("@EQM_ENQDATE", DateFormat(txtEnqDate.Text))
            paraEnq_M(2) = New SqlClient.SqlParameter("@EQM_APPLFIRSTNAME", txtApplFirstName.Text.Replace("'", "''").ToUpper)
            paraEnq_M(3) = New SqlClient.SqlParameter("@EQM_APPLMIDNAME", txtApplMidName.Text.Replace("'", "''").ToUpper)
            paraEnq_M(4) = New SqlClient.SqlParameter("@EQM_APPLLASTNAME", txtApplLastName.Text.Replace("'", "''").ToUpper)
            paraEnq_M(5) = New SqlClient.SqlParameter("@ENQ_APPLPASSPORTNAME", passportname)
            paraEnq_M(6) = New SqlClient.SqlParameter("@EQM_APPLDOB", DateFormat(txtDOB.Text))
            paraEnq_M(7) = New SqlClient.SqlParameter("@EQM_APPLGENDER", IIf(rdMale.Checked, "M", "F"))
            paraEnq_M(8) = New SqlClient.SqlParameter("@EQM_REL_ID", ddlReligion.SelectedValue)
            paraEnq_M(9) = New SqlClient.SqlParameter("@EQM_APPLNATIONALITY", ddlNationality.SelectedValue)
            paraEnq_M(10) = New SqlClient.SqlParameter("@EQM_APPLPOB", txtPOB.Text)
            paraEnq_M(11) = New SqlClient.SqlParameter("@EQM_APPLCOB", ddlCOB.SelectedValue)
            paraEnq_M(12) = New SqlClient.SqlParameter("@EQM_APPLPASPRTNO", txtPNo.Text)
            paraEnq_M(13) = New SqlClient.SqlParameter("@EQM_APPLPASPRTISSDATE", EQM_APPLPASPRTISSDATE)
            paraEnq_M(14) = New SqlClient.SqlParameter("@EQM_APPLPASPRTEXPDATE", EQM_APPLPASPRTEXPDATE)
            paraEnq_M(15) = New SqlClient.SqlParameter("@EQM_APPLPASPRTISSPLACE", txtPIssPlace.Text.Replace("'", "''"))
            paraEnq_M(16) = New SqlClient.SqlParameter("@EQM_APPLVISANO", txtVNo.Text.Replace("'", "''"))
            paraEnq_M(17) = New SqlClient.SqlParameter("@EQM_APPLVISAISSDATE", EQM_APPLVISAISSDATE)
            paraEnq_M(18) = New SqlClient.SqlParameter("@EQM_APPLVISAEXPDATE", EQM_APPLVISAEXPDATE)
            paraEnq_M(19) = New SqlClient.SqlParameter("@EQM_APPLVISAISSPLACE", txtVIssPlace.Text.Replace("'", "''"))
            paraEnq_M(20) = New SqlClient.SqlParameter("@EQM_APPLVISAISSAUTH", txtVIssAuth.Text.Replace("'", "''"))

            paraEnq_M(21) = New SqlClient.SqlParameter("@EQM_bPREVSCHOOLGEMS", EQM_bPREVSCHOOLGEMS)
            paraEnq_M(22) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_BSU_ID", EQM_PREVSCHOOL_BSU_ID)
            paraEnq_M(23) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_NURSERY", EQM_PREVSCHOOL_NURSERY)
            paraEnq_M(24) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_REG_ID", EQM_PREVSCHOOL_REG_ID)
            paraEnq_M(25) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL", EQM_PREVSCHOOL)
            paraEnq_M(26) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_CITY", EQM_PREVSCHOOL_CITY)
            paraEnq_M(27) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_CTY_ID", EQM_PREVSCHOOL_CTY_ID)
            paraEnq_M(28) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_GRD_ID", EQM_PREVSCHOOL_GRD_ID)
            paraEnq_M(29) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_CLM_ID", EQM_PREVSCHOOL_CLM_ID)
            paraEnq_M(30) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_MEDIUM", EQM_PREVSCHOOL_MEDIUM)
            paraEnq_M(31) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_LASTATTDATE", IIf(IsDate(DateFormat(EQM_PREVSCHOOL_LASTATTDATE)) = True, DateFormat(EQM_PREVSCHOOL_LASTATTDATE), ""))
            paraEnq_M(32) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_FROMDT", IIf(IsDate(DateFormat(EQM_PREVSCHOOL_FROMDT)) = True, DateFormat(EQM_PREVSCHOOL_FROMDT), ""))
            paraEnq_M(33) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_HEAD_NAME", EQM_PREVSCHOOL_HEAD_NAME)
            paraEnq_M(34) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_ADDRESS", EQM_PREVSCHOOL_ADDRESS)
            paraEnq_M(35) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_PHONE", EQM_PREVSCHOOL_PHONE)
            paraEnq_M(36) = New SqlClient.SqlParameter("@EQM_PREVSCHOOL_FAX", EQM_PREVSCHOOL_FAX)

            paraEnq_M(37) = New SqlClient.SqlParameter("@EQM_bAPPLSIBLING", chkSibling.Checked)
            paraEnq_M(38) = New SqlClient.SqlParameter("@EQM_SIBLINGFEEID", txtSRegNo.Text.Replace("'", "''"))
            paraEnq_M(39) = New SqlClient.SqlParameter("@EQM_SIBLINGSCHOOL", ddlSUnit.SelectedValue)
            paraEnq_M(40) = New SqlClient.SqlParameter("@EQM_bEXSTUDENT", chkExStudent.Checked)
            paraEnq_M(41) = New SqlClient.SqlParameter("@EQM_EXUNIT", ddlEUnit.SelectedValue)
            paraEnq_M(42) = New SqlClient.SqlParameter("@EQM_EX_STU_ID", txtERegNo.Text)
            paraEnq_M(43) = New SqlClient.SqlParameter("@EQM_bSTAFFGEMS", chkStaffGEMS.Checked)
            paraEnq_M(44) = New SqlClient.SqlParameter("@EQM_STAFFUNIT", ddlStaffUnit.SelectedValue)
            paraEnq_M(45) = New SqlClient.SqlParameter("@EQM_STAFF_EMP_ID", HFEMP_ID.Value)
            paraEnq_M(46) = New SqlClient.SqlParameter("@EQM_PRIMARYCONTACT", primarycontact)
            paraEnq_M(47) = New SqlClient.SqlParameter("@EQM_PREFCONTACT", ddlPrefContact.SelectedValue)
            paraEnq_M(48) = New SqlClient.SqlParameter("@EQM_BLOODGROUP", ddlBgroup.Text)
            paraEnq_M(49) = New SqlClient.SqlParameter("@EQM_HEALTHCARDNO", txtHealtCard.Text.Replace("'", "''"))
            paraEnq_M(50) = New SqlClient.SqlParameter("@EQM_FIRSTLANG", ddlFLang.SelectedValue)
            paraEnq_M(51) = New SqlClient.SqlParameter("@EQM_OTHLANG", EQM_OTHLANG)
            paraEnq_M(52) = New SqlClient.SqlParameter("@EQM_bRCVSMS", rdSmsYes.Checked)
            paraEnq_M(53) = New SqlClient.SqlParameter("@EQM_bRCVMAIL", rdEmailYes.Checked)
            paraEnq_M(54) = New SqlClient.SqlParameter("@EQM_bRCVPUBL", rbPubYES.Checked)
            paraEnq_M(55) = New SqlClient.SqlParameter("@EQM_FEE_SPONSOR", ddlFeeSponsor.SelectedValue)


            paraEnq_M(56) = New SqlClient.SqlParameter("@EQM_APPLEMIRATES_ID", txtEmiratesId.Text.Trim)
            paraEnq_M(57) = New SqlClient.SqlParameter("@EQM_PremisesID", txtPremisesId.Text.Trim)


            paraEnq_M(70) = New SqlClient.SqlParameter("@STU_EMIRATES_ID_ENG", SEMIRATES_ID_ENG)
            paraEnq_M(71) = New SqlClient.SqlParameter("@STU_EMIRATES_ID_ARB", SEMIRATES_ID_ARB)


            paraEnq_M(58) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            paraEnq_M(58).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "UpdateSTUDENQUIRY", paraEnq_M)
            Dim ReturnFlag As Integer = paraEnq_M(58).Value
            Return ReturnFlag

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "UpdateSTUDENQUIRY")
            Return -1
        End Try





    End Function

    Private Function UpdateSTUDPARENTENQUIRY_M(ByVal transaction As SqlTransaction, ByVal primarycontact As String) As Integer
        Try
            Dim str_query As String = String.Empty
            'code modified by lijo 26may08
            Dim EQP_FCOMP_ID As String
            Dim EQP_FCOMPANY As String
            If ddlFCompany.SelectedItem.Text = "Other" Then
                EQP_FCOMP_ID = "0"
                EQP_FCOMPANY = txtFCompany.Text 'txtCompany.Text.Replace("'", "''")
            Else
                EQP_FCOMP_ID = ddlFCompany.SelectedItem.Value
                EQP_FCOMPANY = txtFCompany.Text
            End If
            Dim EQP_MCOMP_ID As String
            Dim EQP_MCOMPANY As String
            If ddlMCompany.SelectedItem.Text = "Other" Then
                EQP_MCOMP_ID = "0"
                EQP_MCOMPANY = txtMCompany.Text 'txtCompany.Text.Replace("'", "''")
            Else
                EQP_MCOMP_ID = ddlMCompany.SelectedItem.Value
                EQP_MCOMPANY = txtMCompany.Text
            End If
            Dim EQP_GCOMP_ID As String
            Dim EQP_GCOMPANY As String
            If ddlGCompany.SelectedItem.Text = "Other" Then
                EQP_GCOMP_ID = "0"
                EQP_GCOMPANY = txtGCompany.Text 'txtCompany.Text.Replace("'", "''")
            Else
                EQP_GCOMP_ID = ddlGCompany.SelectedItem.Value
                EQP_GCOMPANY = txtGCompany.Text
            End If

            'code added by lijo 26/08/2008
            Dim EQP_FMODE_ID As String = String.Empty
            For Each item As ListItem In chkAboutUs.Items
                If (item.Selected) Then
                    EQP_FMODE_ID = EQP_FMODE_ID + item.Value
                    EQP_FMODE_ID = EQP_FMODE_ID + "|"
                End If
            Next

            Dim EQP_FCOMAREA_ID As String = ddlFArea.SelectedValue ' IIf(ddlFArea.SelectedValue = 0, "-1", ddlFArea.SelectedValue)
            Dim EQP_FCOMSTATE_ID As String = ddlFCity.SelectedValue ' IIf(ddlFCity.SelectedValue = 0, "-1", ddlFCity.SelectedValue)

            Dim EQP_MCOMAREA_ID As String = ddlMArea.SelectedValue ' IIf(ddlMArea.SelectedValue = 0, "-1", ddlMArea.SelectedValue)
            Dim EQP_MCOMSTATE_ID As String = ddlMCity.SelectedValue ' IIf(ddlMCity.SelectedValue = 0, "-1", ddlMCity.SelectedValue)

            Dim EQP_GCOMAREA_ID As String = ddlGArea.SelectedValue ' IIf(ddlGArea.SelectedValue = 0, "-1", ddlGArea.SelectedValue)
            Dim EQP_GCOMSTATE_ID As String = ddlGCity.SelectedValue 'IIf(ddlGCity.SelectedValue = 0, "-1", ddlGCity.SelectedValue)

            Dim EQP_FCOMAREA As String = String.Empty
            Dim EQP_FCOMCITY As String = String.Empty
            Dim EQP_FCOMSTATE As String = String.Empty

            If ddlFArea.SelectedValue = "0" Then
                EQP_FCOMAREA = txtFContCurArea.Text
                EQP_FCOMCITY = txtFContCurrCity.Text
                EQP_FCOMSTATE = txtFContCurrCity.Text

            ElseIf ddlFArea.SelectedValue = "-1" Then
                EQP_FCOMAREA = txtFContCurArea.Text
                EQP_FCOMCITY = txtFContCurrCity.Text
                EQP_FCOMSTATE = txtFContCurrCity.Text

            Else
                EQP_FCOMAREA = ddlFArea.SelectedItem.Text
                EQP_FCOMCITY = ddlFCity.SelectedItem.Text
                EQP_FCOMSTATE = ddlFCity.SelectedItem.Text
            End If

            Dim EQP_MCOMAREA As String = String.Empty
            Dim EQP_MCOMCITY As String = String.Empty
            Dim EQP_MCOMSTATE As String = String.Empty

            If ddlMArea.SelectedValue = "0" Then
                EQP_MCOMAREA = txtMContCurArea.Text
                EQP_MCOMCITY = txtMContCurrCity.Text
                EQP_MCOMSTATE = txtMContCurrCity.Text

            ElseIf ddlMArea.SelectedValue = "-1" Then
                EQP_MCOMAREA = txtMContCurArea.Text
                EQP_MCOMCITY = txtMContCurrCity.Text
                EQP_MCOMSTATE = txtMContCurrCity.Text


            Else
                EQP_MCOMAREA = ddlMArea.SelectedItem.Text
                EQP_MCOMCITY = ddlMCity.SelectedItem.Text
                EQP_MCOMSTATE = ddlMCity.SelectedItem.Text
            End If

            'EQP_MCOMAREA = txtMContCurArea.Text
            'EQP_MCOMCITY = txtMContCurrCity.Text
            'EQP_MCOMSTATE = txtMContCurrCity.Text


            Dim EQP_GCOMAREA As String = String.Empty
            Dim EQP_GCOMCITY As String = String.Empty
            Dim EQP_GCOMSTATE As String = String.Empty


            If ddlGArea.SelectedValue = "0" Then
                EQP_GCOMAREA = txtGContCurArea.Text
                EQP_GCOMCITY = txtGContCurrCity.Text
                EQP_GCOMSTATE = txtGContCurrCity.Text

            ElseIf ddlGArea.SelectedValue = "-1" Then
                EQP_GCOMAREA = txtGContCurArea.Text
                EQP_GCOMCITY = txtGContCurrCity.Text
                EQP_GCOMSTATE = txtGContCurrCity.Text



            Else
                EQP_GCOMAREA = ddlGArea.SelectedItem.Text
                EQP_GCOMCITY = ddlGCity.SelectedItem.Text
                EQP_GCOMSTATE = ddlGCity.SelectedItem.Text
            End If



            Dim STS_FEMIRATES_ID As String = txtFEMIRATES_ID.Text
            Dim STS_MEMIRATES_ID As String = txtMEMIRATES_ID.Text

            Dim STS_FEMIRATES_ID_EXPDATE As String = txtFEMIRATES_ID_EXPDATE.Text
            Dim STS_MEMIRATES_ID_EXPDATE As String = txtMEMIRATES_ID_EXPDATE.Text


            Dim FEMIRATES_ID_ENG As String = txtFEmiratesId_ENG.Text.Replace("'", "")
            Dim FEMIRATES_ID_ARB As String = txtFEmiratesId_ARB.Text.Replace("'", "")

            Dim MEMIRATES_ID_ENG As String = txtMEmiratesId_ENG.Text.Replace("'", "")
            Dim MEMIRATES_ID_ARB As String = txtMEmiratesId_ARB.Text.Replace("'", "")





            Dim EQP_FAMILY_NOTE As String = txtFamily_NOTE.Text.Trim
            Dim param(130) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EQP_EQM_ENQID", CType(hfENQ_ID.Value, String))
            param(1) = New SqlClient.SqlParameter("@EQP_FFIRSTNAME", txtFFirstName.Text)
            param(2) = New SqlClient.SqlParameter("@EQP_FMIDNAME", txtFMidName.Text)
            param(3) = New SqlClient.SqlParameter("@EQP_FLASTNAME", txtFLastName.Text)
            param(4) = New SqlClient.SqlParameter("@EQP_FNATIONALITY", ddlFNationality1.SelectedValue)
            param(5) = New SqlClient.SqlParameter("@EQP_FNATIONALITY2", ddlFNationality2.SelectedValue)
            param(6) = New SqlClient.SqlParameter("@EQP_FCOMSTREET", txtFContCurStreet.Text)
            param(7) = New SqlClient.SqlParameter("@EQP_FCOMAREA", EQP_FCOMAREA)
            param(8) = New SqlClient.SqlParameter("@EQP_FCOMBLDG", txtFContCurrBldg.Text)
            param(9) = New SqlClient.SqlParameter("@EQP_FCOMAPARTNO", txtFContCurrApartNo.Text)
            param(10) = New SqlClient.SqlParameter("@EQP_FCOMPOBOX", txtFContCurrPOBox.Text)
            param(11) = New SqlClient.SqlParameter("@EQP_FCOMCITY", EQP_FCOMCITY)
            param(12) = New SqlClient.SqlParameter("@EQP_FCOMCOUNTRY", ddlFContCurrCountry.SelectedValue)
            param(13) = New SqlClient.SqlParameter("@EQP_FOFFPHONE", txtFOfficePhone.Text)
            param(14) = New SqlClient.SqlParameter("@EQP_FRESPHONE", txtFContCurrPhoneNo.Text)
            param(15) = New SqlClient.SqlParameter("@EQP_FFAX", txtFFax.Text)
            param(16) = New SqlClient.SqlParameter("@EQP_FMOBILE", txtFContCurrMobNo.Text)
            param(17) = New SqlClient.SqlParameter("@EQP_FPRMADDR1", txtFContPermAddress1.Text)
            param(18) = New SqlClient.SqlParameter("@EQP_FPRMADDR2", txtFContPermAddress2.Text)
            param(19) = New SqlClient.SqlParameter("@EQP_FPRMPOBOX", txtFContPermPOBOX.Text)
            param(20) = New SqlClient.SqlParameter("@EQP_FPRMCITY", txtFContPermCity.Text)
            param(21) = New SqlClient.SqlParameter("@EQP_FPRMCOUNTRY", ddlFContPermCountry.SelectedValue)
            param(22) = New SqlClient.SqlParameter("@EQP_FPRMPHONE", txtFContPermPhoneNo.Text)
            param(23) = New SqlClient.SqlParameter("@EQP_FOCC", txtFOcc.Text)
            param(24) = New SqlClient.SqlParameter("@EQP_FCOMP_ID", EQP_FCOMP_ID)
            param(25) = New SqlClient.SqlParameter("@EQP_FCOMPANY", txtFCompany.Text)
            param(26) = New SqlClient.SqlParameter("@EQP_FEMAIL", txtFContEmail.Text)
            param(27) = New SqlClient.SqlParameter("@EQP_MFIRSTNAME", txtMFirstName.Text)
            param(28) = New SqlClient.SqlParameter("@EQP_MMIDNAME", txtMMidName.Text)
            param(29) = New SqlClient.SqlParameter("@EQP_MLASTNAME", txtMLastName.Text)
            param(30) = New SqlClient.SqlParameter("@EQP_MNATIONALITY", ddlMNationality1.SelectedValue)
            param(31) = New SqlClient.SqlParameter("@EQP_MNATIONALITY2", ddlMNationality2.SelectedValue)
            param(32) = New SqlClient.SqlParameter("@EQP_MCOMSTREET", txtMContCurStreet.Text)
            param(33) = New SqlClient.SqlParameter("@EQP_MCOMAREA", EQP_MCOMAREA)
            param(34) = New SqlClient.SqlParameter("@EQP_MCOMBLDG", txtMContCurrBldg.Text)
            param(35) = New SqlClient.SqlParameter("@EQP_MCOMAPARTNO", txtMContCurrApartNo.Text)
            param(36) = New SqlClient.SqlParameter("@EQP_MCOMPOBOX", txtMContCurrPOBox.Text)
            param(37) = New SqlClient.SqlParameter("@EQP_MCOMCITY", EQP_MCOMCITY)
            param(38) = New SqlClient.SqlParameter("@EQP_MCOMCOUNTRY", ddlMContCurrCountry.SelectedValue)
            param(39) = New SqlClient.SqlParameter("@EQP_MOFFPHONE", txtMOfficePhone.Text)
            param(40) = New SqlClient.SqlParameter("@EQP_MRESPHONE", txtMContCurrPhoneNo.Text)
            param(41) = New SqlClient.SqlParameter("@EQP_MFAX", txtMFax.Text)
            param(42) = New SqlClient.SqlParameter("@EQP_MMOBILE", txtMContCurrMobNo.Text)
            param(43) = New SqlClient.SqlParameter("@EQP_MPRMADDR1", txtMContPermAddress1.Text)
            param(44) = New SqlClient.SqlParameter("@EQP_MPRMADDR2", txtMContPermAddress2.Text)
            param(45) = New SqlClient.SqlParameter("@EQP_MPRMPOBOX", txtMContPermPOBOX.Text)
            param(46) = New SqlClient.SqlParameter("@EQP_MPRMCITY", txtMContPermCity.Text)
            param(47) = New SqlClient.SqlParameter("@EQP_MPRMCOUNTRY", ddlMContPermCountry.SelectedValue)
            param(48) = New SqlClient.SqlParameter("@EQP_MPRMPHONE", txtMContPermPhoneNo.Text)
            param(49) = New SqlClient.SqlParameter("@EQP_MOCC", txtMOcc.Text)
            param(50) = New SqlClient.SqlParameter("@EQP_MCOMP_ID", EQP_MCOMP_ID)
            param(51) = New SqlClient.SqlParameter("@EQP_MCOMPANY", txtMCompany.Text)
            param(52) = New SqlClient.SqlParameter("@EQP_MEMAIL", txtMContEmail.Text)
            param(53) = New SqlClient.SqlParameter("@EQP_GFIRSTNAME", txtGFirstName.Text)
            param(54) = New SqlClient.SqlParameter("@EQP_GMIDNAME", txtGMidName.Text)
            param(55) = New SqlClient.SqlParameter("@EQP_GLASTNAME", txtGLastName.Text)
            param(56) = New SqlClient.SqlParameter("@EQP_GNATIONALITY", ddlGNationality1.SelectedValue)
            param(57) = New SqlClient.SqlParameter("@EQP_GNATIONALITY2", ddlGNationality2.SelectedValue)
            param(58) = New SqlClient.SqlParameter("@EQP_GCOMSTREET", txtGContCurStreet.Text)
            param(59) = New SqlClient.SqlParameter("@EQP_GCOMAREA", EQP_GCOMAREA)
            param(60) = New SqlClient.SqlParameter("@EQP_GCOMBLDG", txtGContCurrBldg.Text)
            param(61) = New SqlClient.SqlParameter("@EQP_GCOMAPARTNO", txtGContCurrApartNo.Text)
            param(62) = New SqlClient.SqlParameter("@EQP_GCOMPOBOX", txtGContCurrPOBox.Text)
            param(63) = New SqlClient.SqlParameter("@EQP_GCOMCITY", EQP_GCOMCITY)
            param(64) = New SqlClient.SqlParameter("@EQP_GCOMCOUNTRY", ddlGContCurrCountry.SelectedValue)
            param(65) = New SqlClient.SqlParameter("@EQP_GOFFPHONE", txtGOfficePhone.Text)
            param(66) = New SqlClient.SqlParameter("@EQP_GRESPHONE", txtGContCurrPhoneNo.Text)
            param(67) = New SqlClient.SqlParameter("@EQP_GFAX", txtGFax.Text)
            param(68) = New SqlClient.SqlParameter("@EQP_GMOBILE", txtGContCurrMobNo.Text)
            param(69) = New SqlClient.SqlParameter("@EQP_GPRMADDR1", txtGContPermAddress1.Text)
            param(70) = New SqlClient.SqlParameter("@EQP_GPRMADDR2", txtGContPermAddress2.Text)
            param(71) = New SqlClient.SqlParameter("@EQP_GPRMPOBOX", txtGContPermPOBOX.Text)
            param(72) = New SqlClient.SqlParameter("@EQP_GPRMCITY", txtGContPermCity.Text)
            param(73) = New SqlClient.SqlParameter("@EQP_GPRMCOUNTRY", ddlGContPermCountry.SelectedValue)
            param(74) = New SqlClient.SqlParameter("@EQP_GPRMPHONE", txtGContPermPhoneNo.Text)
            param(75) = New SqlClient.SqlParameter("@EQP_GOCC", txtGOcc.Text)
            param(76) = New SqlClient.SqlParameter("@EQP_GCOMP_ID", EQP_GCOMP_ID)
            param(77) = New SqlClient.SqlParameter("@EQP_GCOMPANY", txtGCompany.Text)
            param(78) = New SqlClient.SqlParameter("@EQP_GEMAIL", txtGContEmail.Text)
            param(79) = New SqlClient.SqlParameter("@EQP_FCOMPOBOX_EMIR", ddlFContCurrPOBOX_EMIR.SelectedValue)
            param(80) = New SqlClient.SqlParameter("@EQP_MCOMPOBOX_EMIR", ddlMContCurrPOBOX_EMIR.SelectedValue)
            param(81) = New SqlClient.SqlParameter("@EQP_GCOMPOBOX_EMIR", ddlGContCurrPOBOX_EMIR.SelectedValue)
            param(82) = New SqlClient.SqlParameter("@EQM_PRIMARYCONTACT", primarycontact)
            param(83) = New SqlClient.SqlParameter("@EQP_FMODE_ID", EQP_FMODE_ID)
            param(84) = New SqlClient.SqlParameter("@EQP_FAMILY_NOTE", EQP_FAMILY_NOTE)


            param(85) = New SqlClient.SqlParameter("@EQP_FCOMAREA_ID", EQP_FCOMAREA_ID)
            param(86) = New SqlClient.SqlParameter("@EQP_FCOMSTATE_ID", EQP_FCOMSTATE_ID)

            param(87) = New SqlClient.SqlParameter("@EQP_MCOMAREA_ID", EQP_MCOMAREA_ID)
            param(88) = New SqlClient.SqlParameter("@EQP_MCOMSTATE_ID", EQP_MCOMSTATE_ID)

            param(89) = New SqlClient.SqlParameter("@EQP_GCOMAREA_ID", EQP_GCOMAREA_ID)
            param(90) = New SqlClient.SqlParameter("@EQP_GCOMSTATE_ID", EQP_GCOMSTATE_ID)



            param(102) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID", STS_FEMIRATES_ID)
            param(103) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID", STS_MEMIRATES_ID)


            param(105) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID_EXPDATE", IIf(STS_FEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_FEMIRATES_ID_EXPDATE))
            param(106) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID_EXPDATE", IIf(STS_MEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_MEMIRATES_ID_EXPDATE))




            param(122) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID_ENG", FEMIRATES_ID_ENG)
            param(123) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID_ENG", MEMIRATES_ID_ENG)


            param(124) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID_ARB", FEMIRATES_ID_ARB)
            param(125) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID_ARB", MEMIRATES_ID_ARB)



            param(91) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(91).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "UpdateSTUDPARENTENQUIRY", param)
            Dim ReturnFlag As Integer = param(91).Value
            Return ReturnFlag
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "UpdateSTUDPARENTENQUIRY_M")
            Return -1
        End Try
    End Function
    Private Function updateStudEnquirySchoolPrio_S(ByVal transaction As SqlTransaction) As Integer
        Try
            Dim lstrReg As String
            lstrReg = ""
            Dim strReg As String
            'registration date can be updated once only as it may affect the reports if the  date is canged
            If Session("sRegister") = "REG" Then
                If hfSTGREQ.Value = "0" Then
                    lstrReg = "REG"
                    strReg = "'" + txtregdate.Text.ToString + "'"
                Else
                    lstrReg = ""
                    strReg = "NULL"
                End If
            Else
                strReg = "NULL"
            End If
            Dim EQS_ENG_READING As String = String.Empty
            Dim EQS_ENG_WRITING As String = String.Empty
            Dim EQS_ENG_SPEAKING As String = String.Empty

            If rbWExc.Checked = True Then
                EQS_ENG_WRITING = 1
            ElseIf rbWGood.Checked = True Then
                EQS_ENG_WRITING = 2
            ElseIf rbWFair.Checked = True Then
                EQS_ENG_WRITING = 3
            ElseIf rbWPoor.Checked = True Then
                EQS_ENG_WRITING = 4
            Else
                EQS_ENG_WRITING = 0
            End If


            If rbSExc.Checked = True Then
                EQS_ENG_SPEAKING = 1
            ElseIf rbSGood.Checked = True Then
                EQS_ENG_SPEAKING = 2
            ElseIf rbSFair.Checked = True Then
                EQS_ENG_SPEAKING = 3
            ElseIf rbSPoor.Checked = True Then
                EQS_ENG_SPEAKING = 4
            Else
                EQS_ENG_SPEAKING = 0
            End If

            If rbRExc.Checked = True Then
                EQS_ENG_READING = 1
            ElseIf rbRGood.Checked = True Then
                EQS_ENG_READING = 2
            ElseIf rbRFair.Checked = True Then
                EQS_ENG_READING = 3
            ElseIf rbRPoor.Checked = True Then
                EQS_ENG_READING = 4
            Else
                EQS_ENG_READING = 0
            End If




            Dim param(70) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@EQS_ID", hfEQS_ID.Value.ToString)
            param(1) = New SqlClient.SqlParameter("@EQS_bTPTREQD", rdTYes.Checked)
            param(2) = New SqlClient.SqlParameter("@EQS_LOC_ID", IIf(rdTYes.Checked = True, ddlLocation.SelectedValue.ToString, System.DBNull.Value))
            param(3) = New SqlClient.SqlParameter("@EQS_SBL_ID", IIf(rdTYes.Checked = True, ddlSubLocation.SelectedValue.ToString, System.DBNull.Value))
            param(4) = New SqlClient.SqlParameter("@EQS_PNT_ID", IIf(rdTYes.Checked = True, ddlPickUp.SelectedValue.ToString, System.DBNull.Value))
            param(5) = New SqlClient.SqlParameter("@EQS_TPTREMARKS", txtTptRemarks.Text.Replace("'", "''"))
            param(6) = New SqlClient.SqlParameter("@EQS_REG", lstrReg)
            param(7) = New SqlClient.SqlParameter("@EQS_REGNDATE", IIf(strReg = "NULL", System.DBNull.Value, txtregdate.Text.ToString))
            param(8) = New SqlClient.SqlParameter("@EQS_DOJ", IIf(IsDate(DateFormat(txtTenDate.Text)) = True, DateFormat(txtTenDate.Text), ""))
            param(9) = New SqlClient.SqlParameter("@EQS_ACCNO", txtACCNO.Text.Trim)
            param(10) = New SqlClient.SqlParameter("@EQS_bALLERGIES", rbHthAll_Yes.Checked)
            param(11) = New SqlClient.SqlParameter("@EQS_ALLERGIES", txtHthAll_Note.Text)

            param(12) = New SqlClient.SqlParameter("@EQS_bRCVSPMEDICATION", rbHthSM_Yes.Checked)
            param(13) = New SqlClient.SqlParameter("@EQS_SPMEDICN", txtHthSM_Note.Text)

            param(14) = New SqlClient.SqlParameter("@EQS_bPRESTRICTIONS", rbHthPER_Yes.Checked)
            param(15) = New SqlClient.SqlParameter("@EQS_PRESTRICTIONS", txtHthPER_Note.Text)

            param(16) = New SqlClient.SqlParameter("@EQS_bHRESTRICTIONS", rbHthOther_yes.Checked)
            param(17) = New SqlClient.SqlParameter("@EQS_HRESTRICTIONS", txtHthOth_Note.Text)


            param(18) = New SqlClient.SqlParameter("@EQS_bTHERAPHY", rbHthLS_Yes.Checked)
            param(19) = New SqlClient.SqlParameter("@EQS_THERAPHY", txtHthLS_Note.Text)

            param(20) = New SqlClient.SqlParameter("@EQS_bSEN", rbHthSE_Yes.Checked)
            param(21) = New SqlClient.SqlParameter("@EQS_SEN", txtHthSE_Note.Text)

            param(22) = New SqlClient.SqlParameter("@EQS_bEAL", rbHthEAL_Yes.Checked)
            param(23) = New SqlClient.SqlParameter("@EQS_EAL", txtHthEAL_Note.Text)

            param(24) = New SqlClient.SqlParameter("@EQS_bMUSICAL", rbHthMus_Yes.Checked)
            param(25) = New SqlClient.SqlParameter("@EQS_MUSICAL", txtHthMus_Note.Text)

            param(26) = New SqlClient.SqlParameter("@EQS_bENRICH", rbHthEnr_Yes.Checked)
            param(27) = New SqlClient.SqlParameter("@EQS_ENRICH", txtHthEnr_note.Text)

            param(28) = New SqlClient.SqlParameter("@EQS_bBEHAVIOUR", rbHthBehv_Yes.Checked)
            param(29) = New SqlClient.SqlParameter("@EQS_BEHAVIOUR", txtHthBehv_Note.Text)
            param(30) = New SqlClient.SqlParameter("@EQS_bSPORTS", rbHthSport_Yes.Checked)
            param(31) = New SqlClient.SqlParameter("@EQS_SPORTS", txtHthSport_note.Text)

            param(32) = New SqlClient.SqlParameter("@EQS_ENG_READING", EQS_ENG_READING)
            param(33) = New SqlClient.SqlParameter("@EQS_ENG_WRITING", EQS_ENG_WRITING)
            param(34) = New SqlClient.SqlParameter("@EQS_ENG_SPEAKING", EQS_ENG_SPEAKING)



            param(35) = New SqlClient.SqlParameter("@EQS_bCommInt", rbHthComm_Yes.Checked)
            param(36) = New SqlClient.SqlParameter("@EQS_CommInt", txtHthCommInt_Note.Text.Replace("'", "''"))
            param(37) = New SqlClient.SqlParameter("@EQS_bDisabled", rbHthDisabled_YES.Checked)
            param(38) = New SqlClient.SqlParameter("@EQS_Disabled", txtHthDisabled_Note.Text.Replace("'", "''"))

            param(39) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_name"))


            param(40) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(40).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "updateStudEnquirySchoolPrio", param)
            Dim ReturnFlag As Integer = param(40).Value
            Return ReturnFlag

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "updateStudEnquirySchoolPrio_S")
            Return -1
        End Try

    End Function
    Private Function SaveENQUIRY_PREVIOUS_SCHOOL_EDIT(ByVal transaction As SqlTransaction) As Integer

        Try


            Dim str As String = String.Empty
            Dim EQM_bPREVSCHOOLGEMS As Boolean
            Dim EQM_PREVSCHOOL_BSU_ID As String = String.Empty
            Dim EQM_PREVSCHOOL_NURSERY As String = String.Empty
            Dim EQM_PREVSCHOOL_REG_ID As String = String.Empty
            Dim EQM_PREVSCHOOL As String = String.Empty
            Dim EQM_PREVSCHOOL_CITY As String = String.Empty
            Dim EQM_PREVSCHOOL_CTY_ID As String = String.Empty
            Dim EQM_PREVSCHOOL_GRD_ID As String = String.Empty
            Dim EQM_PREVSCHOOL_CLM_ID As String = String.Empty
            Dim EQM_PREVSCHOOL_MEDIUM As String = String.Empty
            Dim EQM_PREVSCHOOL_LASTATTDATE As String = String.Empty
            Dim EQM_PREVSCHOOL_FROMDT As String = String.Empty
            Dim EQM_PREVSCHOOL_HEAD_NAME As String = String.Empty
            Dim EQM_PREVSCHOOL_ADDRESS As String = String.Empty
            Dim EQM_PREVSCHOOL_PHONE As String = String.Empty
            Dim EQM_PREVSCHOOL_FAX As String = String.Empty
            Dim EPS_Id As String = String.Empty
            If (Not Session("TABLE_School_Pre") Is Nothing) And (Session("TABLE_School_Pre").Rows.Count > 0) Then
                For i As Integer = 0 To Session("TABLE_School_Pre").Rows.Count - 1
                    If Session("TABLE_School_Pre").Rows(i)("EPS_DETAIL_TYPE") <> "Current School" Then
                        EPS_Id = Session("TABLE_School_Pre").Rows(i)("EPS_Id")
                        EQM_bPREVSCHOOLGEMS = IIf(Session("TABLE_School_Pre").Rows(i)("sch_id") = "", False, True)
                        EQM_PREVSCHOOL_BSU_ID = Session("TABLE_School_Pre").Rows(i)("sch_id")
                        EQM_PREVSCHOOL = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL"))
                        EQM_PREVSCHOOL_HEAD_NAME = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_HEAD_NAME"))
                        EQM_PREVSCHOOL_REG_ID = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_REG_ID"))
                        EQM_PREVSCHOOL_GRD_ID = Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_GRD_ID")
                        EQM_PREVSCHOOL_MEDIUM = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_MEDIUM"))
                        EQM_PREVSCHOOL_ADDRESS = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_ADDRESS"))
                        EQM_PREVSCHOOL_CLM_ID = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_CLM_ID"))
                        EQM_PREVSCHOOL_CITY = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_CITY"))
                        EQM_PREVSCHOOL_CTY_ID = Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_CTY_ID")
                        EQM_PREVSCHOOL_PHONE = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_PHONE"))
                        EQM_PREVSCHOOL_FAX = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_FAX"))
                        EQM_PREVSCHOOL_FROMDT = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_FROMDT"))
                        EQM_PREVSCHOOL_LASTATTDATE = Replace_InvalidXML(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_LASTATTDATE"))
                    End If

                    str += String.Format("<School sch_id='{0}' SCH_NAME='{1}' SCH_HEAD='{2}' SCH_FEE_ID='{3}' SCH_GRADE='{4}' SCH_LEARN_INS='{5}' SCH_ADDR='{6}' SCH_CURR='{7}' " &
                                                            " SCH_CITY='{8}' SCH_COUNTRY='{9}' SCH_PHONE='{10}' SCH_FAX='{11}'  SCH_FROMDT='{12}' SCH_TODT='{13}'  SCH_TYPE='{14}' EPS_ID='{15}' />",
                                         EQM_PREVSCHOOL_BSU_ID, EQM_PREVSCHOOL, EQM_PREVSCHOOL_HEAD_NAME,
                                         EQM_PREVSCHOOL_REG_ID, EQM_PREVSCHOOL_GRD_ID, EQM_PREVSCHOOL_MEDIUM,
                                         EQM_PREVSCHOOL_ADDRESS, EQM_PREVSCHOOL_CLM_ID, EQM_PREVSCHOOL_CITY, EQM_PREVSCHOOL_CTY_ID,
                           EQM_PREVSCHOOL_PHONE, EQM_PREVSCHOOL_FAX,
                 IIf(IsDate(DateFormat(EQM_PREVSCHOOL_FROMDT)) = True, DateFormat(EQM_PREVSCHOOL_FROMDT), ""), IIf(IsDate(DateFormat(EQM_PREVSCHOOL_LASTATTDATE)) = True, DateFormat(EQM_PREVSCHOOL_LASTATTDATE), ""), Session("TABLE_School_Pre").Rows(i)("EPS_DETAIL_TYPE"), EPS_Id)



                Next

            End If



            If str <> "" Then
                str = "<Schools>" + str + "</Schools>"

            End If



            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STR", str)
            pParms(1) = New SqlClient.SqlParameter("@EPS_EQM_ENQID", hfENQ_ID.Value)
            pParms(2) = New SqlClient.SqlParameter("@EPS_BSU_ID", Session("sBsuid"))
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SaveENQUIRY_PREVIOUS_SCHOOL_EDIT", pParms)
            Dim returnvalue = pParms(3).Value
            Return returnvalue
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "SaveENQUIRY_PREVIOUS_SCHOOL_EDIT")
            Return -1
        End Try

    End Function


    Sub SaveServices(ByVal transaction As SqlTransaction)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim lblSvcId As Label
        Dim rdY As RadioButton
        Dim status As Integer
        If ViewState("transportservice") = 1 Then
            'save transport
            If rdTYes.Checked = True Then
                str_query = "exec studSAVEENQSERVICES '" + Session("sbsuid").ToString + "'," + hfACD_ID.Value + "," + hfEQS_ID.Value.ToString + ",1," + ddlLocation.SelectedValue.ToString + "," + ddlSubLocation.SelectedValue.ToString + "," + ddlPickUp.SelectedValue.ToString + "," + rdTYes.Checked.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
            End If
        End If
        With gvServices
            For i = 0 To .Rows.Count - 1
                lblSvcId = .Rows(i).FindControl("lblSvcId")
                rdY = .Rows(i).FindControl("rdYes")
                If rdY.Checked = True Then
                    status = 1
                Else
                    status = 0
                End If
                str_query = "exec studSAVEENQSERVICES '" + Session("sbsuid").ToString + "'," + hfACD_ID.Value + "," + hfEQS_ID.Value.ToString + "," + lblSvcId.Text.ToString + ",0,0,0," + rdY.Checked.ToString
                SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
            Next
        End With
    End Sub
    Function Replace_InvalidXML(ByVal str As String) As String
        str = str.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("""", "&quot;").Replace("'", "&apos;")
        Return str
    End Function
    Private Sub PopulateEnquiryData()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            ' Dim str_query As String = "exec [dbo].[GetENQUIRY_EDITINFO] " + hfENQ_ID.Value + ",'" + Session("sbsuid").ToString + "'"

            Dim primarycontact As String = ""
            Dim li As New ListItem
            Dim grdId As String = String.Empty
            Dim arr As String() = New String(2) {}

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@ENQ_ID", hfENQ_ID.Value)
            param(1) = New SqlParameter("@BSU_ID", Session("sbsuid").ToString)
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[dbo].[GetENQUIRY_EDITINFO]", param)

                While reader.Read

                    '''''Main
                    If reader.HasRows = True Then
                        txtSEmiratesId_ENG.Text = Convert.ToString(reader("EQM_EMIRATESID_NAME"))
                        txtSEmiratesId_ARB.Text = Convert.ToString(reader("EQM_EMIRATESID_ARABIC_NAME"))
                        txtFEmiratesId_ENG.Text = Convert.ToString(reader("EQP_FEMIRATESID_NAME"))
                        txtFEmiratesId_ARB.Text = Convert.ToString(reader("EQP_FEMIRATESID_ARABIC_NAME"))
                        txtMEmiratesId_ENG.Text = Convert.ToString(reader("EQP_MEMIRATESID_NAME"))
                        txtMEmiratesId_ARB.Text = Convert.ToString(reader("EQP_MEMIRATESID_ARABIC_NAME"))
                        txtFEMIRATES_ID.Text = Convert.ToString(reader("EQP_FEMIRATES_ID"))
                        txtMEMIRATES_ID.Text = Convert.ToString(reader("EQP_MEMIRATES_ID"))
                        txtFEMIRATES_ID_EXPDATE.Text = Convert.ToString(reader("EQP_FEMIRATES_ID_EXPDATE"))
                        txtMEMIRATES_ID_EXPDATE.Text = Convert.ToString(reader("EQP_mEMIRATES_ID_EXPDATE"))



                        txtEnqDate.Text = Convert.ToString(reader("EQM_ENQDATE"))
                        txtApplFirstName.Text = Convert.ToString(reader("EQM_APPLFIRSTNAME")).Trim.Replace("''", "'")
                        txtApplMidName.Text = Convert.ToString(reader("EQM_APPLMIDNAME")).Trim.Replace("''", "'")
                        txtApplLastName.Text = Convert.ToString(reader("EQM_APPLLASTNAME")).Trim.Replace("''", "'")
                        txtDOB.Text = Convert.ToString(reader("EQM_APPLDOB"))
                        If Convert.ToString(reader("EQM_APPLGENDER")) = "M" Then
                            rdMale.Checked = True
                        Else
                            rdFemale.Checked = True
                        End If

                        'li.Value = Convert.ToString(reader(7))
                        'li.Text = Convert.ToString(reader(8))
                        'If li.Text <> "" And li.Text <> "--" Then
                        '    ddlReligion.Items(ddlReligion.Items.IndexOf(li)).Selected = True
                        'End If



                        If Not ddlReligion.Items.FindByValue(Convert.ToString(reader("EQM_REL_ID"))) Is Nothing Then
                            ddlReligion.ClearSelection()
                            ddlReligion.Items.FindByValue(Convert.ToString(reader("EQM_REL_ID"))).Selected = True
                        End If

                        If Not ddlNationality.Items.FindByValue(Convert.ToString(reader("EQM_APPLNATIONALITY"))) Is Nothing Then
                            ddlNationality.ClearSelection()
                            ddlNationality.Items.FindByValue(Convert.ToString(reader("EQM_APPLNATIONALITY"))).Selected = True
                        End If

                        If Not ddlFeeSponsor.Items.FindByValue(Convert.ToString(reader("EQM_FEE_SPONSOR"))) Is Nothing Then
                            ddlFeeSponsor.ClearSelection()
                            ddlFeeSponsor.Items.FindByValue(Convert.ToString(reader("EQM_FEE_SPONSOR"))).Selected = True
                        Else
                            ddlFeeSponsor.ClearSelection()
                            ddlFeeSponsor.SelectedIndex = 0

                        End If



                        'li = New ListItem

                        'li.Value = Convert.ToString(reader(9)).Trim
                        'li.Text = Convert.ToString(reader(10))
                        'If Not li.Text = "" Then
                        '    ddlNationality.Items(ddlNationality.Items.IndexOf(li)).Selected = True
                        'End If

                        txtPOB.Text = Convert.ToString(reader("EQM_APPLPOB")).Trim

                        'li = New ListItem
                        'li.Value = Convert.ToString(reader(12)).Trim
                        'li.Text = Convert.ToString(reader(13))
                        'If li.Text <> "" And li.Text <> "--" Then
                        '    ddlCOB.Items(ddlCOB.Items.IndexOf(li)).Selected = True
                        'End If

                        If Not ddlCOB.Items.FindByValue(Convert.ToString(reader("EQM_APPLCOB"))) Is Nothing Then
                            ddlCOB.ClearSelection()
                            ddlCOB.Items.FindByValue(Convert.ToString(reader("EQM_APPLCOB"))).Selected = True
                        End If




                        '''''Passport/Visa
                        txtPNo.Text = Convert.ToString(reader("EQM_APPLPASPRTNO")).Trim

                        If Convert.ToString(reader("EQM_APPLPASPRTISSDATE")) <> "01/Jan/1900" Then
                            txtPIssDate.Text = Convert.ToString(reader("EQM_APPLPASPRTISSDATE"))
                        End If
                        If Convert.ToString(reader("EQM_APPLPASPRTEXPDATE")) <> "01/Jan/1900" Then
                            txtPExpDate.Text = Convert.ToString(reader("EQM_APPLPASPRTEXPDATE"))
                        End If


                        txtPIssPlace.Text = Convert.ToString(reader("EQM_APPLPASPRTISSPLACE")).Trim.Replace("''", "'")

                        txtVNo.Text = Convert.ToString(reader("EQM_APPLVISANO")).Trim

                        If Convert.ToString(reader("EQM_APPLVISAISSDATE")) <> "01/Jan/1900" Then
                            txtVIssDate.Text = Convert.ToString(reader("EQM_APPLVISAISSDATE"))
                        End If
                        If Convert.ToString(reader("EQM_APPLVISAEXPDATE")) <> "01/Jan/1900" Then
                            txtVExpDate.Text = Convert.ToString(reader("EQM_APPLVISAEXPDATE"))
                        End If



                        txtVIssPlace.Text = Convert.ToString(reader("EQM_APPLVISAISSPLACE")).Trim.Replace("''", "'")
                        txtVIssAuth.Text = Convert.ToString(reader("EQM_APPLVISAISSAUTH")).Trim

                        txtEmiratesId.Text = Convert.ToString(reader("EQM_APPLEMIRATES_ID")).Trim
                        txtPremisesId.Text = Convert.ToString(reader("EQM_PremisesID")).Trim

                        ''''''''''Previous School
                        '' ''If Convert.ToBoolean(reader("EQM_bPREVSCHOOLGEMS")) = True Then
                        '' ''    rdPYes.Checked = True
                        '' ''    ddlPSchool.Visible = True
                        '' ''    txtPSchool.Visible = False
                        '' ''    'li = New ListItem
                        '' ''    'li.Value = reader.GetValue(24)
                        '' ''    'li.Text = Convert.ToString(reader(25)).Trim
                        '' ''    'ddlPSchool.Items(ddlPSchool.Items.IndexOf(li)).Selected = True

                        '' ''    If Not ddlPSchool.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_BSU_ID"))) Is Nothing Then
                        '' ''        ddlPSchool.ClearSelection()
                        '' ''        ddlPSchool.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_BSU_ID"))).Selected = True
                        '' ''    End If

                        '' ''Else
                        '' ''    rdPNo.Checked = True
                        '' ''    txtPSchool.Visible = True
                        '' ''    ddlPSchool.Visible = False
                        '' ''End If

                        '' ''txtPSchool.Text = Convert.ToString(reader("EQM_PREVSCHOOL"))
                        '' ''txtPSchCity.Text = Convert.ToString(reader("EQM_PREVSCHOOL_CITY"))

                        'li = New ListItem
                        'li.Value = Convert.ToString(reader(28)).Trim
                        'li.Text = Convert.ToString(reader(29))
                        'If li.Text <> "" And li.Text <> "--" Then
                        '    ddlPCountry.Items(ddlPCountry.Items.IndexOf(li)).Selected = True
                        'End If
                        '' ''If Not ddlPSchool.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_BSU_ID"))) Is Nothing Then
                        '' ''    ddlPSchool.ClearSelection()
                        '' ''    ddlPSchool.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_BSU_ID"))).Selected = True
                        '' ''End If

                        '' ''If Not ddlPGrade.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_GRD_ID"))) Is Nothing Then
                        '' ''    ddlPGrade.ClearSelection()
                        '' ''    ddlPGrade.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_GRD_ID"))).Selected = True
                        '' ''End If

                        'If Convert.ToString(reader(30)) <> "" Then
                        '    ddlPGrade.Text = Convert.ToString(reader(30))
                        'Else
                        '    ddlPGrade.Text = ""
                        'End If

                        '' ''If Not ddlPCurriculum.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_CLM_ID"))) Is Nothing Then
                        '' ''    ddlPCurriculum.ClearSelection()
                        '' ''    ddlPCurriculum.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_CLM_ID"))).Selected = True
                        '' ''End If



                        'li = New ListItem
                        'li.Value = reader.GetValue(31)
                        'li.Text = Convert.ToString(reader(32))
                        'If li.Text <> "" And li.Text <> "--" Then
                        '    ddlPCurriculum.Items(ddlPCurriculum.Items.IndexOf(li)).Selected = True
                        'End If


                        '''' txtLastAttDate.Text = DateFormat(Convert.ToDateTime(reader("EQM_PREVSCHOOL_LASTATTDATE")))

                        '' ''If Not ddlNUnit.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_NURSERY"))) Is Nothing Then
                        '' ''    ddlNUnit.ClearSelection()
                        '' ''    ddlNUnit.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL_NURSERY"))).Selected = True
                        '' ''End If

                        'li = New ListItem
                        'li.Value = reader.GetValue(34)
                        'li.Text = Convert.ToString(reader(35))
                        'If li.Text <> "" And li.Text <> "--" Then
                        '    ddlNUnit.Items(ddlNUnit.Items.IndexOf(li)).Selected = True
                        'End If




                        If (hfGRD_ID.Value = "KG1" Or hfGRD_ID.Value = "PK") And ddlNUnit.SelectedItem.Value <> 4 Then
                            chkNursery.Checked = True
                            txtNRegNo.Text = Convert.ToString(reader("EQM_PREVSCHOOL_REG_ID")).Trim

                        End If

                        If ddlPreSchool_Nursery.SelectedItem.Text.ToUpper() = "OTHER" Then
                            txtPreSchool_Nursery.Text = Convert.ToString(reader("EQM_PREVSCHOOL")).Trim
                        Else
                            txtPreSchool_Nursery.Text = ""
                        End If
                        chkSibling.Checked = Convert.ToBoolean(reader("EQM_bAPPLSIBLING"))

                        txtSRegNo.Text = Convert.ToString(reader("EQM_SIBLINGFEEID")).Trim



                        'li = New ListItem
                        'li.Value = Convert.ToString(reader(39))
                        'li.Text = Convert.ToString(reader(40))
                        'If li.Text <> "" And li.Text <> "--" Then
                        '    ddlSUnit.Items(ddlSUnit.Items.IndexOf(li)).Selected = True
                        'End If

                        If Not ddlSUnit.Items.FindByValue(Convert.ToString(reader("EQM_SIBLINGSCHOOL"))) Is Nothing Then
                            ddlSUnit.ClearSelection()
                            ddlSUnit.Items.FindByValue(Convert.ToString(reader("EQM_SIBLINGSCHOOL"))).Selected = True
                        End If

                        chkExStudent.Checked = Convert.ToBoolean(reader("EQM_bEXSTUDENT"))

                        'li = New ListItem
                        'li.Value = Convert.ToString(reader(42))
                        'li.Text = Convert.ToString(reader(43))
                        'If li.Text <> "" And li.Text <> "--" Then
                        '    ddlEUnit.Items(ddlEUnit.Items.IndexOf(li)).Selected = True
                        'End If

                        If Not ddlEUnit.Items.FindByValue(Convert.ToString(reader("EQM_EXUNIT"))) Is Nothing Then
                            ddlEUnit.ClearSelection()
                            ddlEUnit.Items.FindByValue(Convert.ToString(reader("EQM_EXUNIT"))).Selected = True
                        End If

                        txtERegNo.Text = Convert.ToString(reader("EQM_EX_STU_ID"))
                        chkStaffGEMS.Checked = Convert.ToBoolean(reader("EQM_bSTAFFGEMS"))

                        If Not ddlStaffUnit.Items.FindByValue(Convert.ToString(reader("EQM_STAFFUNIT"))) Is Nothing Then
                            ddlStaffUnit.ClearSelection()
                            ddlStaffUnit.Items.FindByValue(Convert.ToString(reader("EQM_STAFFUNIT"))).Selected = True
                        End If

                        'li = New ListItem
                        'li.Value = Convert.ToString(reader(46))
                        'li.Text = Convert.ToString(reader(47))
                        'If li.Text <> "" Or li.Text <> "--" Then
                        '    ddlStaffUnit.Items(ddlStaffUnit.Items.IndexOf(li)).Selected = True
                        'End If

                        txtSPayRollId.Text = Convert.ToString(reader("staff_EMP_NO"))

                        ' special medication


                        If Convert.ToBoolean(reader("EQM_bRCVSMS")) = True Then
                            rdSmsYes.Checked = True
                            rdSmsNo.Checked = False
                        Else
                            rdSmsNo.Checked = True
                            rdSmsYes.Checked = False
                        End If

                        If Convert.ToBoolean(reader("EQM_bRCVMAIL")) = True Then
                            rdEmailYes.Checked = True
                        Else
                            rdEmailNo.Checked = True
                        End If
                        'txtNotes.Text = convert.tostring(reader(51)
                        primarycontact = Convert.ToString(reader("EQM_PRIMARYCONTACT"))

                        'li = New ListItem
                        'li.Text = Convert.ToString(reader(53))
                        'li.Value = Convert.ToString(reader(53))
                        'If li.Text <> "" Or li.Text <> "" Then
                        '    ddlBgroup.Items(ddlBgroup.Items.IndexOf(li)).Selected = True
                        'End If

                        If Not ddlBgroup.Items.FindByValue(Convert.ToString(reader("EQM_BLOODGROUP"))) Is Nothing Then
                            ddlBgroup.ClearSelection()
                            ddlBgroup.Items.FindByValue(Convert.ToString(reader("EQM_BLOODGROUP"))).Selected = True
                        End If

                        txtHealtCard.Text = Convert.ToString(reader("EQM_HEALTHCARDNO"))

                        ' hfEQP_ID.Value = reader.GetValue(52)

                        If primarycontact = "F" Then
                            rdFather.Checked = True
                        ElseIf primarycontact = "M" Then
                            rdMother.Checked = True
                        Else
                            rdGuardian.Checked = True
                        End If

                        If Not ddlPrefContact.Items.FindByValue(Convert.ToString(reader("EQM_PREFCONTACT"))) Is Nothing Then
                            ddlPrefContact.ClearSelection()
                            ddlPrefContact.Items.FindByValue(Convert.ToString(reader("EQM_PREFCONTACT"))).Selected = True
                        End If


                        If Not ddlFLang.Items.FindByValue(Convert.ToString(reader("EQM_FIRSTLANG"))) Is Nothing Then
                            ddlFLang.ClearSelection()
                            ddlFLang.Items.FindByValue(Convert.ToString(reader("EQM_FIRSTLANG"))).Selected = True
                        End If
                        If Convert.ToBoolean(reader("EQM_bRCVPUBL")) = True Then
                            rbPubYES.Checked = True
                        Else
                            rbPubNo.Checked = True

                        End If

                        Dim arInfoLang() As String
                        Dim splitterLang As Char = "|"
                        Dim new1 As String = String.Empty
                        arInfoLang = Convert.ToString(reader("EQM_OTHLANG")).Trim.Replace("''", "'").Split(splitterLang)
                        If arInfoLang.Length > 0 Then
                            For i As Integer = 0 To arInfoLang.Length - 1
                                If arInfoLang(i).Trim <> "" Then
                                    If Not chkOLang.Items.FindByValue(arInfoLang(i)) Is Nothing Then
                                        chkOLang.Items.FindByValue(arInfoLang(i)).Selected = True
                                    End If
                                End If
                            Next
                        End If

                        '-----Getting  fathers details-------------
                        txtFFirstName.Text = Convert.ToString(reader("EQP_FFIRSTNAME")).Trim
                        txtFMidName.Text = Convert.ToString(reader("EQP_FMIDNAME")).Trim
                        txtFLastName.Text = Convert.ToString(reader("EQP_FLASTNAME")).Trim
                        If Not ddlFNationality1.Items.FindByValue(Convert.ToString(reader("EQP_FNATIONALITY"))) Is Nothing Then
                            ddlFNationality1.ClearSelection()
                            ddlFNationality1.Items.FindByValue(Convert.ToString(reader("EQP_FNATIONALITY"))).Selected = True
                        End If
                        If Not ddlFNationality2.Items.FindByValue(Convert.ToString(reader("EQP_FNATIONALITY2"))) Is Nothing Then
                            ddlFNationality2.ClearSelection()
                            ddlFNationality2.Items.FindByValue(Convert.ToString(reader("EQP_FNATIONALITY2"))).Selected = True
                        End If
                        txtFContCurStreet.Text = Convert.ToString(reader("EQP_FCOMSTREET"))

                        txtFContCurrBldg.Text = Convert.ToString(reader("EQP_FCOMBLDG"))
                        txtFContCurrApartNo.Text = Convert.ToString(reader("EQP_FCOMAPARTNO"))
                        txtFContCurrPOBox.Text = Convert.ToString(reader("EQP_FCOMPOBOX"))




                        If Convert.ToString(reader("EQP_FOFFPHONE")).Replace("-", "").Trim.Length > 6 Then
                            txtFOfficePhone.Text = Convert.ToString(reader("EQP_FOFFPHONE"))
                        End If

                        If Convert.ToString(reader("EQP_FRESPHONE")).Replace("-", "").Trim.Length > 6 Then
                            txtFContCurrPhoneNo.Text = Convert.ToString(reader("EQP_FRESPHONE"))
                        End If
                        If Convert.ToString(reader("EQP_FFAX")).Replace("-", "").Trim.Length > 6 Then
                            txtFFax.Text = Convert.ToString(reader("EQP_FFAX"))
                        End If
                        If Convert.ToString(reader("EQP_FMOBILE")).Replace("-", "").Trim.Length > 6 Then
                            txtFContCurrMobNo.Text = Convert.ToString(reader("EQP_FMOBILE"))
                        End If






                        txtFContPermAddress1.Text = Convert.ToString(reader("EQP_FPRMADDR1"))
                        txtFContPermAddress2.Text = Convert.ToString(reader("EQP_FPRMADDR2"))
                        '  txtContPermPOBox.Text = convert.tostring(reader(20).Trim.Replace("''", "'")
                        txtFContPermCity.Text = Convert.ToString(reader("EQP_FPRMCITY"))
                        If Not ddlFContPermCountry.Items.FindByValue(Convert.ToString(reader("EQP_FPRMCOUNTRY"))) Is Nothing Then
                            ddlFContPermCountry.ClearSelection()
                            ddlFContPermCountry.Items.FindByValue(Convert.ToString(reader("EQP_FPRMCOUNTRY"))).Selected = True
                        End If

                        If Convert.ToString(reader("EQP_FPRMPHONE")).Replace("-", "").Trim.Length > 6 Then
                            txtFContPermPhoneNo.Text = Convert.ToString(reader("EQP_FPRMPHONE"))
                        End If


                        txtFOcc.Text = Convert.ToString(reader("EQP_FOCC"))
                        txtFCompany.Text = Convert.ToString(reader("EQP_FCOMPANY"))

                        txtFContEmail.Text = Convert.ToString(reader("EQP_FEMAIL"))

                        If Not ddlFCompany.Items.FindByValue(Convert.ToString(reader("EQP_FCOMP_ID"))) Is Nothing Then
                            ddlFCompany.ClearSelection()
                            ddlFCompany.Items.FindByValue(Convert.ToString(reader("EQP_FCOMP_ID"))).Selected = True
                        End If

                        txtFContPermPOBOX.Text = Convert.ToString(reader("EQP_FPRMPOBOX"))

                        If Not ddlFContCurrPOBOX_EMIR.Items.FindByValue(Convert.ToString(reader("EQP_FCOMPOBOX_EMIR"))) Is Nothing Then
                            ddlFContCurrPOBOX_EMIR.ClearSelection()
                            ddlFContCurrPOBOX_EMIR.Items.FindByValue(Convert.ToString(reader("EQP_FCOMPOBOX_EMIR"))).Selected = True
                        End If
                        '-----Getting  Mothers details-------------
                        txtMFirstName.Text = Convert.ToString(reader("EQP_MFIRSTNAME")).Trim
                        txtMMidName.Text = Convert.ToString(reader("EQP_MMIDNAME")).Trim
                        txtMLastName.Text = Convert.ToString(reader("EQP_MLASTNAME")).Trim
                        If Not ddlMNationality1.Items.FindByValue(Convert.ToString(reader("EQP_MNATIONALITY"))) Is Nothing Then
                            ddlMNationality1.ClearSelection()
                            ddlMNationality1.Items.FindByValue(Convert.ToString(reader("EQP_MNATIONALITY"))).Selected = True
                        End If
                        If Not ddlMNationality2.Items.FindByValue(Convert.ToString(reader("EQP_MNATIONALITY2"))) Is Nothing Then
                            ddlMNationality2.ClearSelection()
                            ddlMNationality2.Items.FindByValue(Convert.ToString(reader("EQP_MNATIONALITY2"))).Selected = True
                        End If
                        txtMContCurStreet.Text = Convert.ToString(reader("EQP_MCOMSTREET"))

                        txtMContCurrBldg.Text = Convert.ToString(reader("EQP_MCOMBLDG"))
                        txtMContCurrApartNo.Text = Convert.ToString(reader("EQP_MCOMAPARTNO"))
                        txtMContCurrPOBox.Text = Convert.ToString(reader("EQP_MCOMPOBOX"))




                        If Convert.ToString(reader("EQP_MOFFPHONE")).Replace("-", "").Trim.Length > 6 Then
                            txtMOfficePhone.Text = Convert.ToString(reader("EQP_MOFFPHONE"))
                        End If

                        If Convert.ToString(reader("EQP_MRESPHONE")).Replace("-", "").Trim.Length > 6 Then
                            txtMContCurrPhoneNo.Text = Convert.ToString(reader("EQP_MRESPHONE"))
                        End If
                        If Convert.ToString(reader("EQP_MFAX")).Replace("-", "").Trim.Length > 6 Then
                            txtMFax.Text = Convert.ToString(reader("EQP_MFAX"))
                        End If
                        If Convert.ToString(reader("EQP_MMOBILE")).Replace("-", "").Trim.Length > 6 Then
                            txtMContCurrMobNo.Text = Convert.ToString(reader("EQP_MMOBILE"))
                        End If











                        txtMContPermAddress1.Text = Convert.ToString(reader("EQP_MPRMADDR1"))
                        txtMContPermAddress2.Text = Convert.ToString(reader("EQP_MPRMADDR2"))
                        '  txtContPermPOBox.Text = convert.tostring(reader(20).Trim.Replace("''", "'")
                        txtMContPermCity.Text = Convert.ToString(reader("EQP_MPRMCITY"))
                        If Not ddlMContPermCountry.Items.FindByValue(Convert.ToString(reader("EQP_MPRMCOUNTRY"))) Is Nothing Then
                            ddlMContPermCountry.ClearSelection()
                            ddlMContPermCountry.Items.FindByValue(Convert.ToString(reader("EQP_MPRMCOUNTRY"))).Selected = True
                        End If

                        If Convert.ToString(reader("EQP_MPRMPHONE")).Replace("-", "").Trim.Length > 6 Then
                            txtMContPermPhoneNo.Text = Convert.ToString(reader("EQP_MPRMPHONE"))
                        End If


                        txtMOcc.Text = Convert.ToString(reader("EQP_MOCC"))
                        txtMCompany.Text = Convert.ToString(reader("EQP_MCOMPANY"))

                        txtMContEmail.Text = Convert.ToString(reader("EQP_MEMAIL"))

                        If Not ddlMCompany.Items.FindByValue(Convert.ToString(reader("EQP_MCOMP_ID"))) Is Nothing Then
                            ddlMCompany.ClearSelection()
                            ddlMCompany.Items.FindByValue(Convert.ToString(reader("EQP_MCOMP_ID"))).Selected = True
                        End If

                        txtMContPermPOBOX.Text = Convert.ToString(reader("EQP_MPRMPOBOX"))

                        If Not ddlMContCurrPOBOX_EMIR.Items.FindByValue(Convert.ToString(reader("EQP_MCOMPOBOX_EMIR"))) Is Nothing Then
                            ddlMContCurrPOBOX_EMIR.ClearSelection()
                            ddlMContCurrPOBOX_EMIR.Items.FindByValue(Convert.ToString(reader("EQP_MCOMPOBOX_EMIR"))).Selected = True
                        End If
                        '-----Getting  Guardians details-------------
                        txtGFirstName.Text = Convert.ToString(reader("EQP_GFIRSTNAME")).Trim
                        txtGMidName.Text = Convert.ToString(reader("EQP_GMIDNAME")).Trim
                        txtGLastName.Text = Convert.ToString(reader("EQP_GLASTNAME")).Trim
                        If Not ddlGNationality1.Items.FindByValue(Convert.ToString(reader("EQP_GNATIONALITY"))) Is Nothing Then
                            ddlGNationality1.ClearSelection()
                            ddlGNationality1.Items.FindByValue(Convert.ToString(reader("EQP_GNATIONALITY"))).Selected = True
                        End If
                        If Not ddlGNationality2.Items.FindByValue(Convert.ToString(reader("EQP_GNATIONALITY2"))) Is Nothing Then
                            ddlGNationality2.ClearSelection()
                            ddlGNationality2.Items.FindByValue(Convert.ToString(reader("EQP_GNATIONALITY2"))).Selected = True
                        End If
                        txtGContCurStreet.Text = Convert.ToString(reader("EQP_GCOMSTREET"))

                        txtGContCurrBldg.Text = Convert.ToString(reader("EQP_GCOMBLDG"))
                        txtGContCurrApartNo.Text = Convert.ToString(reader("EQP_GCOMAPARTNO"))
                        txtGContCurrPOBox.Text = Convert.ToString(reader("EQP_GCOMPOBOX"))




                        If Convert.ToString(reader("EQP_MOFFPHONE")).Replace("-", "").Trim.Length > 6 Then
                            txtGOfficePhone.Text = Convert.ToString(reader("EQP_GOFFPHONE"))
                        End If

                        If Convert.ToString(reader("EQP_GRESPHONE")).Replace("-", "").Trim.Length > 6 Then
                            txtGContCurrPhoneNo.Text = Convert.ToString(reader("EQP_GRESPHONE"))
                        End If
                        If Convert.ToString(reader("EQP_GFAX")).Replace("-", "").Trim.Length > 6 Then
                            txtGFax.Text = Convert.ToString(reader("EQP_GFAX"))
                        End If
                        If Convert.ToString(reader("EQP_GMOBILE")).Replace("-", "").Trim.Length > 6 Then
                            txtGContCurrMobNo.Text = Convert.ToString(reader("EQP_GMOBILE"))
                        End If







                        txtGContPermAddress1.Text = Convert.ToString(reader("EQP_GPRMADDR1"))
                        txtGContPermAddress2.Text = Convert.ToString(reader("EQP_GPRMADDR2"))
                        '  txtContPermPOBox.Text = convert.tostring(reader(20).Trim.Replace("''", "'")
                        txtGContPermCity.Text = Convert.ToString(reader("EQP_GPRMCITY"))
                        If Not ddlGContPermCountry.Items.FindByValue(Convert.ToString(reader("EQP_GPRMCOUNTRY"))) Is Nothing Then
                            ddlGContPermCountry.ClearSelection()
                            ddlGContPermCountry.Items.FindByValue(Convert.ToString(reader("EQP_GPRMCOUNTRY"))).Selected = True
                        End If

                        If Convert.ToString(reader("EQP_GPRMPHONE")).Replace("-", "").Trim.Length > 6 Then
                            txtGContPermPhoneNo.Text = Convert.ToString(reader("EQP_GPRMPHONE"))
                        End If


                        txtGOcc.Text = Convert.ToString(reader("EQP_GOCC"))
                        txtGCompany.Text = Convert.ToString(reader("EQP_GCOMPANY"))

                        txtGContEmail.Text = Convert.ToString(reader("EQP_GEMAIL"))

                        If Not ddlGCompany.Items.FindByValue(Convert.ToString(reader("EQP_GCOMP_ID"))) Is Nothing Then
                            ddlGCompany.ClearSelection()
                            ddlGCompany.Items.FindByValue(Convert.ToString(reader("EQP_GCOMP_ID"))).Selected = True
                        End If

                        txtGContPermPOBOX.Text = Convert.ToString(reader("EQP_GPRMPOBOX"))

                        If Not ddlGContCurrPOBOX_EMIR.Items.FindByValue(Convert.ToString(reader("EQP_GCOMPOBOX_EMIR"))) Is Nothing Then
                            ddlGContCurrPOBOX_EMIR.ClearSelection()
                            ddlGContCurrPOBOX_EMIR.Items.FindByValue(Convert.ToString(reader("EQP_GCOMPOBOX_EMIR"))).Selected = True
                        End If




                        txtFamily_NOTE.Text = Convert.ToString(reader("EQP_FAMILY_NOTE"))

                        'ddlContCurrPOBOX_EMIR.SelectedValue
                        'EQP_FMODE_ID=convert.tostring(reader(34).Trim.Replace("''", "'")
                        Dim arInfo() As String
                        Dim splitter As Char = "|"
                        Dim new2 As String = String.Empty
                        arInfo = Convert.ToString(reader("EQP_MODE_ID")).Trim.Replace("''", "'").Split(splitter)
                        If arInfo.Length > 0 Then
                            For i As Integer = 0 To arInfo.Length - 1
                                If arInfo(i).Trim <> "" Then
                                    chkAboutUs.Items.FindByValue(arInfo(i).Trim).Selected = True
                                End If
                            Next
                        End If

                        txtFContCurArea.Text = Convert.ToString(reader("EQP_FCOMAREA"))
                        txtFContCurrCity.Text = Convert.ToString(reader("EQP_FCOMCITY"))
                        txtMContCurArea.Text = Convert.ToString(reader("EQP_MCOMAREA"))
                        txtMContCurrCity.Text = Convert.ToString(reader("EQP_MCOMCITY"))
                        txtGContCurArea.Text = Convert.ToString(reader("EQP_GCOMAREA"))
                        txtGContCurrCity.Text = Convert.ToString(reader("EQP_GCOMCITY"))


                        If Not ddlFContCurrCountry.Items.FindByValue(Convert.ToString(reader("EQP_FCOMCOUNTRY"))) Is Nothing Then
                            ddlFContCurrCountry.ClearSelection()
                            ddlFContCurrCountry.Items.FindByValue(Convert.ToString(reader("EQP_FCOMCOUNTRY"))).Selected = True
                            ddlFContCurrCountry_SelectedIndexChanged(ddlFContCurrCountry, Nothing)

                        End If

                        If Not ddlFCity.Items.FindByValue(Convert.ToString(reader("EQP_FCOMSTATE_ID"))) Is Nothing Then
                            ddlFCity.ClearSelection()
                            ddlFCity.Items.FindByValue(Convert.ToString(reader("EQP_FCOMSTATE_ID"))).Selected = True
                            ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

                        End If


                        If Not ddlFArea.Items.FindByValue(Convert.ToString(reader("EQP_FCOMAREA_ID"))) Is Nothing Then
                            ddlFArea.ClearSelection()
                            ddlFArea.Items.FindByValue(Convert.ToString(reader("EQP_FCOMAREA_ID"))).Selected = True
                        End If


                        If Not ddlMContCurrCountry.Items.FindByValue(Convert.ToString(reader("EQP_MCOMCOUNTRY"))) Is Nothing Then
                            ddlMContCurrCountry.ClearSelection()
                            ddlMContCurrCountry.Items.FindByValue(Convert.ToString(reader("EQP_MCOMCOUNTRY"))).Selected = True
                            ddlMContCurrCountry_SelectedIndexChanged(ddlMContCurrCountry, Nothing)
                        End If
                        If Not ddlMCity.Items.FindByValue(Convert.ToString(reader("EQP_MCOMSTATE_ID"))) Is Nothing Then
                            ddlMCity.ClearSelection()
                            ddlMCity.Items.FindByValue(Convert.ToString(reader("EQP_MCOMSTATE_ID"))).Selected = True
                            ddlMCity_SelectedIndexChanged(ddlMCity, Nothing)
                        End If
                        If Not ddlMArea.Items.FindByValue(Convert.ToString(reader("EQP_MCOMAREA_ID"))) Is Nothing Then
                            ddlMArea.ClearSelection()
                            ddlMArea.Items.FindByValue(Convert.ToString(reader("EQP_MCOMAREA_ID"))).Selected = True
                        End If



                        If Not ddlGContCurrCountry.Items.FindByValue(Convert.ToString(reader("EQP_GCOMCOUNTRY"))) Is Nothing Then
                            ddlGContCurrCountry.ClearSelection()
                            ddlGContCurrCountry.Items.FindByValue(Convert.ToString(reader("EQP_GCOMCOUNTRY"))).Selected = True
                            ddlGContCurrCountry_SelectedIndexChanged(ddlGContCurrCountry, Nothing)
                        End If

                        If Not ddlGCity.Items.FindByValue(Convert.ToString(reader("EQP_GCOMSTATE_ID"))) Is Nothing Then
                            ddlGCity.ClearSelection()
                            ddlGCity.Items.FindByValue(Convert.ToString(reader("EQP_GCOMSTATE_ID"))).Selected = True
                            ddlGCity_SelectedIndexChanged(ddlGCity, Nothing)
                        End If


                        If Not ddlGArea.Items.FindByValue(Convert.ToString(reader("EQP_GCOMAREA_ID"))) Is Nothing Then
                            ddlGArea.ClearSelection()
                            ddlGArea.Items.FindByValue(Convert.ToString(reader("EQP_GCOMAREA_ID"))).Selected = True
                        End If





                        '--------------------PARENT DETAIL ENDS OVER HERE------------------------------



                        txtStatus.Text = Convert.ToString(reader("EQS_STATUS"))
                        hfGRD_ID.Value = Convert.ToString(reader("EQS_GRD_ID"))
                        txtEnqNo.Text = Convert.ToString(reader("EQS_APPLNO"))
                        If Convert.ToBoolean(reader("EQS_bTPTREQD")) = False Then
                            rdTNo.Checked = True
                            ddlLocation.Enabled = False
                            ddlSubLocation.Enabled = False
                            ddlPickUp.Enabled = False
                        End If



                        If Convert.ToBoolean(reader("EQS_bRCVSPMEDICATION")) = True Then
                            rbHthSM_Yes.Checked = True
                        Else
                            rbHthSM_No.Checked = True
                        End If
                        txtHthSM_Note.Text = Convert.ToString(reader("EQS_SPMEDICN"))

                        'txtNotes.Text = Convert.ToString(reader("EQS_REMARKS"))

                        If Convert.ToBoolean(reader("EQS_bHRESTRICTIONS")) = True Then
                            rbHthOther_yes.Checked = True
                        Else
                            rbHthOther_No.Checked = True
                        End If
                        txtHthOth_Note.Text = Convert.ToString(reader("EQS_HRESTRICTIONS"))

                        If Convert.ToBoolean(reader("EQS_bPRESTRICTIONS")) = True Then
                            rbHthPER_Yes.Checked = True
                        Else
                            rbHthPER_No.Checked = True
                        End If
                        txtHthPER_Note.Text = Convert.ToString(reader("EQS_PRESTRICTIONS"))


                        If Convert.ToBoolean(reader("EQS_bALLERGIES")) = True Then
                            rbHthAll_Yes.Checked = True
                        Else
                            rbHthAll_No.Checked = True
                        End If
                        txtHthAll_Note.Text = Convert.ToString(reader("EQS_ALLERGIES"))

                        If Convert.ToBoolean(reader("EQS_bTHERAPHY")) = True Then
                            rbHthLS_Yes.Checked = True
                        Else
                            rbHthLS_No.Checked = True
                        End If
                        txtHthLS_Note.Text = Convert.ToString(reader("EQS_THERAPHY"))

                        If Convert.ToBoolean(reader("EQS_bSEN")) = True Then
                            rbHthSE_Yes.Checked = True
                        Else
                            rbHthSE_No.Checked = True
                        End If
                        txtHthSE_Note.Text = Convert.ToString(reader("EQS_SEN"))

                        If Convert.ToBoolean(reader("EQS_bEAL")) = True Then
                            rbHthEAL_Yes.Checked = True
                        Else
                            rbHthEAL_No.Checked = True
                        End If
                        txtHthEAL_Note.Text = Convert.ToString(reader("EQS_EAL"))

                        If Convert.ToBoolean(reader("EQS_bBEHAVIOUR")) = True Then
                            rbHthBehv_Yes.Checked = True
                        Else
                            rbHthBehv_No.Checked = True
                        End If
                        txtHthBehv_Note.Text = Convert.ToString(reader("EQS_BEHAVIOUR"))

                        If Convert.ToBoolean(reader("EQS_bENRICH")) = True Then
                            rbHthEnr_Yes.Checked = True
                        Else
                            rbHthEnr_No.Checked = True
                        End If
                        txtHthEnr_note.Text = Convert.ToString(reader("EQS_ENRICH"))

                        If Convert.ToBoolean(reader("EQS_bMUSICAL")) = True Then
                            rbHthMus_Yes.Checked = True
                        Else
                            rbHthMus_No.Checked = True
                        End If
                        txtHthMus_Note.Text = Convert.ToString(reader("EQS_MUSICAL"))


                        If Convert.ToBoolean(reader("EQS_bSPORTS")) = True Then
                            rbHthSport_Yes.Checked = True
                        Else
                            rbHthSport_No.Checked = True
                        End If
                        txtHthSport_note.Text = Convert.ToString(reader("EQS_SPORTS"))

                        If Convert.ToBoolean(reader("EQS_bCommInt")) = True Then
                            rbHthComm_Yes.Checked = True
                        Else
                            rbHthComm_No.Checked = True
                        End If
                        txtHthCommInt_Note.Text = Convert.ToString(reader("EQS_CommInt"))

                        If Convert.ToBoolean(reader("EQS_bDisabled")) = True Then
                            rbHthDisabled_YES.Checked = True
                        Else
                            rbHthDisabled_No.Checked = True
                        End If
                        txtHthDisabled_Note.Text = Convert.ToString(reader("EQS_Disabled"))



                        txtTptRemarks.Text = Convert.ToString(reader("EQS_TPTREMARKS"))
                        grdId = Convert.ToString(reader("EQS_GRD_ID"))
                        txtregdate.Text = Convert.ToString(reader("EQS_REGNDATE"))
                        txtTenDate.Text = Convert.ToString(reader("EQS_DOJ"))

                        If Convert.ToBoolean(reader("EQS_bTPTREQD")) = True Then
                            If Convert.ToString(reader("EQS_TPTSTATUS")) = "ACCEPT" Then
                                lblTptStatus.Text = "Accepted"
                            ElseIf Convert.ToString(reader("EQS_TPTSTATUS")) = "REJECT" Then
                                lblTptStatus.Text = "Rejected"
                            ElseIf Convert.ToString(reader("EQS_TPTSTATUS")) = "WAITLIST" Then
                                lblTptStatus.Text = "In Waiting List"
                            ElseIf Convert.ToString(reader("EQS_TPTSTATUS")) = "NA" Then
                                lblTptStatus.Text = "Not updated"
                            End If
                        Else
                            lblTptStatus.Text = "NA"
                        End If
                        txtACCNO.Text = Convert.ToString(reader("EQS_ACCNO"))
                        hfACCNO.Value = Convert.ToString(reader("EQS_ACCNO"))
                    End If

                    Dim EQS_ENG_READING As String = Convert.ToString(reader("EQS_ENG_READING"))
                    Dim EQS_ENG_WRITING As String = Convert.ToString(reader("EQS_ENG_WRITING"))
                    Dim EQS_ENG_SPEAKING As String = Convert.ToString(reader("EQS_ENG_SPEAKING"))

                    If EQS_ENG_WRITING = "1" Then
                        rbWExc.Checked = True
                    ElseIf EQS_ENG_WRITING = "2" Then
                        rbWGood.Checked = True
                    ElseIf EQS_ENG_WRITING = "3" Then
                        rbWFair.Checked = True
                    ElseIf EQS_ENG_WRITING = "4" Then
                        rbWPoor.Checked = True

                    End If


                    If EQS_ENG_SPEAKING = "1" Then
                        rbSExc.Checked = True
                    ElseIf EQS_ENG_SPEAKING = "2" Then
                        rbSGood.Checked = True
                    ElseIf EQS_ENG_SPEAKING = "3" Then
                        rbSFair.Checked = True
                    ElseIf EQS_ENG_SPEAKING = "4" Then
                        rbSPoor.Checked = True
                    End If

                    If EQS_ENG_READING = "1" Then
                        rbRExc.Checked = True
                    ElseIf EQS_ENG_READING = "2" Then
                        rbRGood.Checked = True
                    ElseIf EQS_ENG_READING = "3" Then
                        rbRFair.Checked = True
                    ElseIf EQS_ENG_READING = "4" Then
                        rbRPoor.Checked = True
                    End If





                End While


            End Using


            BindServices()




            'If grdId = "KG1" Then
            '    mnuMaster.Items(2).Enabled = False
            'Else
            '    mnuMaster.Items(2).Enabled = True
            'End If

            If ViewState("transportservice") = 1 Then

                'If ViewState("enqservice") = 1 Then
                '    str_query = "SELECT BTPTREQD=(select CAST(1 AS BIT)),LOC_DESCRIPTION, LOC_ID,SBL_DESCRIPTION,SBL_ID,PNT_DESCRIPTION,PNT_ID" _
                '                & " FROM ENQUIRY_SERVICES_D AS A INNER JOIN" _
                '                & " TPTPICKUPPOINTS_M AS B ON A.ESV_PNT_ID = B.PNT_ID INNER JOIN" _
                '                & " TPTSUBLOCATION_M AS C ON A.ESV_SBL_ID = C.SBL_ID INNER JOIN" _
                '                & " TPTLOCATION_M AS D ON A.ESV_LOC_ID = D.LOC_ID" _
                '                & " AND ESV_EQS_ID=" + hfEQS_ID.Value
                'Else
                '    str_query = "SELECT EQS_BTPTREQD,LOC_DESCRIPTION, LOC_ID,SBL_DESCRIPTION,SBL_ID,PNT_DESCRIPTION,PNT_ID" _
                '                & " FROM ENQUIRY_SCHOOLPRIO_S AS A INNER JOIN" _
                '                & " TPTPICKUPPOINTS_M AS B ON A.EQS_PNT_ID = B.PNT_ID INNER JOIN" _
                '                & " TPTSUBLOCATION_M AS C ON A.EQS_SBL_ID = C.SBL_ID INNER JOIN" _
                '                & " TPTLOCATION_M AS D ON A.EQS_LOC_ID = D.LOC_ID" _
                '                & " AND EQS_ID=" + hfEQS_ID.Value
                'End If
                Dim str_query As String = String.Empty
                If ViewState("enqservice") = 1 Then
                    str_query = "SELECT BTPTREQD=(select CAST(1 AS BIT)),isnull(ESV_LOC_ID,0),isnull(ESV_SBL_ID,0),isnull(ESV_PNT_ID,0)" _
                                & " FROM ENQUIRY_SERVICES_D WHERE ESV_SVC_ID=1 AND ESV_EQS_ID = " + hfEQS_ID.Value
                Else
                    str_query = "SELECT EQS_BTPTREQD,isnull(EQS_LOC_ID,0),isnull(EQS_SBL_ID,0),isnull(EQS_PNT_ID,0)" _
                                & " FROM ENQUIRY_SCHOOLPRIO_S WHERE EQS_ID=" + hfEQS_ID.Value
                End If


                Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
                    While reader.Read

                        ' chkAppr.Checked = reader.GetBoolean(0)
                        ' chkSuitAdm.Checked = reader.GetBoolean(1)
                        '    txtStatus.Text = convert.tostring(reader(0)
                        If reader.GetBoolean(0) = True Then
                            rdTYes.Checked = True

                            ddlLocation.Enabled = True
                            ddlSubLocation.Enabled = True
                            ddlPickUp.Enabled = True

                            BindLocation()
                            If reader.GetValue(1) <> 0 Then
                                ddlLocation.Items.FindByValue(reader.GetValue(1)).Selected = True
                            End If


                            BindSubLocation()
                            If reader.GetValue(2) <> 0 Then
                                ddlSubLocation.Items.FindByValue(reader.GetValue(2)).Selected = True
                            End If

                            BindPickUp()
                            If reader.GetValue(3) <> 0 Then
                                ddlPickUp.Items.FindByValue(reader.GetValue(3)).Selected = True
                            End If
                        End If
                    End While
                End Using
            Else
                'Panel1.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub BindServices()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SVC_ID,SVC_DESCRIPTION," _
                                  & " STATUS=(SELECT CASE  WHEN B.SVC_ID IN (SELECT ISNULL(ESV_SVC_ID,0)" _
                                  & " FROM ENQUIRY_SERVICES_D WHERE ESV_EQS_ID=" + hfEQS_ID.Value + ") THEN 1 " _
                                  & " ELSE 0 END)FROM  SERVICES_BSU_M AS A INNER JOIN" _
                                  & " SERVICES_SYS_M AS B ON A.SVB_SVC_ID = B.SVC_ID" _
                                  & " WHERE SVB_ACD_ID=" + hfACD_ID.Value + " AND SVB_BSU_ID='" + Session("sbsuid") + "' AND SVB_BAVAILABLE=1"
        Dim dt As New DataTable
        dt.Columns.Add("SVC_ID", GetType(Integer))
        dt.Columns.Add("SVC_DESCRIPTION", GetType(String))
        dt.Columns.Add("YES", GetType(Boolean))
        dt.Columns.Add("NO", GetType(Boolean))
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim flag As Boolean = False
        Dim dr As DataRow
        While reader.Read
            dr = dt.NewRow
            With dr
                If reader.GetValue(0) <> 1 Then  '''' if transport service is not stored in the grid
                    .Item(0) = reader.GetValue(0)
                    .Item(1) = reader.GetValue(1)
                    If reader.GetValue(2) = 1 Then
                        .Item(2) = True
                        .Item(3) = False
                    Else
                        .Item(2) = False
                        .Item(3) = True
                    End If
                    dt.Rows.Add(dr)
                Else
                    ViewState("transportservice") = 1 '' to check if transprt service is available for that bsu
                    If reader.GetValue(2) = 1 Then
                        ViewState("enqservice") = 1
                    End If
                End If
            End With
        End While
        reader.Close()


        gvServices.DataSource = dt
        gvServices.DataBind()
    End Sub
    Function DateFormat(ByVal fDate As DateTime) As String
        Return Format(fDate, "dd/MMM/yyyy").Replace("01/Jan/1900", "")
    End Function
    Function DateFormat(ByVal fDate As String) As String
        Dim newDate As String
        If fDate <> "" Then
            newDate = Format(CType(fDate, DateTime), "MM/dd/yyyy")
            Return newDate
        Else
            Return "NULL"
        End If
    End Function
    Sub BSU_Enq_validation()

        Try

            Dim Temp_Rfv As New RequiredFieldValidator
            Dim Enq_hash As New Hashtable



            Using Enq_validation_reader As SqlDataReader = AccessStudentClass.GetEnquiry_Validation(Session("sbsuid"), "3")
                While Enq_validation_reader.Read
                    If Enq_validation_reader.HasRows Then
                        Enq_hash.Add(Enq_validation_reader("EQV_CODE"), Enq_validation_reader("CONTROL_ID"))

                        If Convert.ToString(Enq_validation_reader("CONTROL_ID")) <> "" Then
                            If Not Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("CONTROL_ID")) Is Nothing Then
                                Temp_Rfv = Page.Master.FindControl("cphMasterpage").FindControl(Enq_validation_reader("CONTROL_ID"))
                                Temp_Rfv.EnableClientScript = False
                                Temp_Rfv.Enabled = False
                                Temp_Rfv.Visible = False

                            End If
                        End If
                    End If


                End While

            End Using

            Session("BSU_Enq_Valid") = Enq_hash

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try



    End Sub
    Function Final_validation() As Boolean
        'Try
        Dim Final_error As Boolean
        Dim DateValid_DOB As Date
        Dim DateValid_PIss_D As Date = DateTime.MinValue
        Dim DateValid_PExp_D As Date = DateTime.MinValue
        Dim DateValid_VIss_D As Date = DateTime.MinValue
        Dim DateValid_VExp_D As Date = DateTime.MinValue
        Dim DateValid_Last_D As Date = DateTime.MinValue
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        Dim commString As String = "The following fields needs to be validated: <UL>"



        'validation of Application Info
        If Session("sRegister") = "REG" And hfSTGREQ.Value = "0" Then
            If txtregdate.Text.Trim = "" Or txtregdate.Text.Trim = "" Then
                commString = commString & "<LI>Registration Date cannot be left empty"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            End If
        End If

        Dim Enq_hash As New Hashtable
        If Not Session("BSU_Enq_Valid") Is Nothing Then
            Enq_hash = Session("BSU_Enq_Valid")
        End If


        If Enq_hash.ContainsKey("SFN") = False Then
            If txtApplFirstName.Text.Trim = "" Then
                commString = commString & "<LI>Student name cannot be left empty"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            End If
        End If

        If Trim(txtApplFirstName.Text) <> "" Then

            If Not Regex.Match(txtApplFirstName.Text, "^[a-zA-Z']{1,100}$").Success Then

                commString += "<LI>Student first name can contain only alphabetic characters ,single quotation mark and  dot character"

                ViewState("Valid_Appl") = "-1"

            End If

        End If




        If Enq_hash.ContainsKey("SRE") = False Then
            If ddlReligion.SelectedItem.Text = "" Then
                commString = commString & "<LI>Please Select Religion"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            End If
        End If





        If Enq_hash.ContainsKey("COB") = False Then
            If ddlCOB.SelectedItem.Text = "" Then
                commString = commString & "<LI>Please select Country of Birth"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            End If
        End If

        If Enq_hash.ContainsKey("NAT") = False Then

            If ddlNationality.SelectedItem.Text = "" Then
                commString = commString & "<LI>Please select Student Nationality"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            End If
        End If

        If Enq_hash.ContainsKey("DOB") = False Then
            If txtDOB.Text.Trim = "" Then
                commString = commString & "<LI>Student date of birth required"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            ElseIf IsDate(txtDOB.Text.Trim) = False Then
                commString = commString & "<LI>Date of birth format is invalid"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            Else
                ' DateValid_DOB = Date.ParseExact(txtDOB.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), Globalization.DateTimeStyles.None)
                DateValid_DOB = DateTime.Parse(txtDOB.Text)
            End If
        End If

        If Enq_hash.ContainsKey("POB") = False Then
            If txtPOB.Text.Trim = "" Then
                commString = commString & "<LI>Student place of birth required"
                ViewState("Valid_Appl") = "-1"
                Final_error = True

            End If
        End If

        If chkSibling.Checked Then
            If Trim(txtSRegNo.Text) = "" Then
                commString = commString & "<LI>Sibling fee ID required"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            Else
                'check for sibling ID
                Using ValidSibl_reader As SqlDataReader = AccessStudentClass.GetValid_Sibl(txtSRegNo.Text, ddlSUnit.SelectedItem.Value)
                    If ValidSibl_reader.HasRows = True Then
                        ViewState("Valid_Appl") = "1"
                    Else
                        commString = commString & "<LI>Sibling Fee ID is not valid"
                        ViewState("Valid_Appl") = "-1"
                        Final_error = True
                    End If

                End Using
            End If
        End If

        ' End of select validation of Application Info


        'Validation  for passport/visa  Details
        If Enq_hash.ContainsKey("PPN") = False Then
            If txtPNo.Text.Trim = "" Then
                commString = commString & "<LI>Passport No. required"
                'ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            End If
        End If

        If Enq_hash.ContainsKey("PPIP") = False Then
            If txtPIssPlace.Text.Trim = "" Then
                commString = commString & "<LI>Passport issue place required"
                'ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            End If
        End If

        If Enq_hash.ContainsKey("PPID") = False Then
            If txtPIssDate.Text.Trim = "" Then
                commString = commString & "<LI>Passport issue date required"
                ' ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            ElseIf IsDate(txtPIssDate.Text) = False Then
                commString = commString & "<LI>Passport Issue Date is not a valid date"
                ' ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            ElseIf IsDate(txtPIssDate.Text) = True Then
                ' DateValid_PIss_D = Date.ParseExact(txtPIssDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), Globalization.DateTimeStyles.None)
                DateValid_PIss_D = DateTime.Parse(txtPIssDate.Text)
                If IsDate(txtDOB.Text) = True And DateValid_PIss_D < DateValid_DOB Then
                    commString = commString & "<LI>Passport Issue Date entered is not a valid date and must be greater than Date of Birth"
                    'ViewState("Valid_Pass") = "-1"
                    ViewState("Valid_Appl") = "-1"
                    Final_error = True
                End If
            End If
        End If
        If Enq_hash.ContainsKey("PPED") = False Then
            ' DateValid_PExp_D = Date.ParseExact(txtPExpDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), Globalization.DateTimeStyles.None)
            If txtPExpDate.Text.Trim = "" Then
                commString = commString & "<LI>Passport expiry date required"
                'ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            ElseIf IsDate(txtPExpDate.Text.Trim) = False Then
                commString = commString & "<LI>Passport Expiry Date is not a valid date"
                ' ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = True

            Else
                '   DateValid_PExp_D = Date.ParseExact(txtPExpDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), Globalization.DateTimeStyles.None)
                DateValid_PExp_D = DateTime.Parse(txtPExpDate.Text)
                If IsDate(DateValid_PIss_D) = True And DateValid_PIss_D > DateValid_PExp_D Then
                    commString = commString & "<LI>Passport Expiry  Date entered is not a valid date and must be greater than Passport Issue Date"
                    'ViewState("Valid_Pass") = "-1"
                    ViewState("Valid_Appl") = "-1"
                    Final_error = True
                End If
            End If

        End If

        If txtVIssDate.Text.Trim <> "" Then
            If IsDate(txtVIssDate.Text.Trim) = False Then
                commString = commString & "<LI>Visa Issue Date is not a valid date"
                ' ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = True
            Else
                'DateValid_VIss_D = Date.ParseExact(txtVIssDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), Globalization.DateTimeStyles.None)
                DateValid_VIss_D = DateTime.Parse(txtVIssDate.Text)
            End If
        End If

        If txtVExpDate.Text.Trim <> "" Then
            If IsDate(txtVExpDate.Text.Trim) = False Then
                commString = commString & "<LI>Visa Expiry Date is not a valid date"
                'ViewState("Valid_Pass") = "-1"
                ViewState("Valid_Appl") = "-1"
                Final_error = True

            Else
                'DateValid_VExp_D = Date.ParseExact(txtVExpDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), Globalization.DateTimeStyles.None)
                DateValid_VExp_D = DateTime.Parse(txtVExpDate.Text)
                If IsDate(DateValid_VIss_D) = True And DateValid_VIss_D > DateValid_VExp_D Then
                    commString = commString & "<LI>Visa Expiry  Date entered is not a valid date and must be greater than Visa Issue Date"
                    ' ViewState("Valid_Pass") = "-1"
                    ViewState("Valid_Appl") = "-1"
                    Final_error = True
                End If
            End If
        End If

        ' End for passport/visa  Details
        'Validation  for primary School Details
        If Enq_hash.ContainsKey("PCFN") = False Then

            If rdFather.Checked Then
                If txtFFirstName.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact name cannot be left empty"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = True
                End If
            ElseIf rdMother.Checked Then
                If txtMFirstName.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact name cannot be left empty"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = True
                End If

            ElseIf rdGuardian.Checked Then
                If txtGFirstName.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact name cannot be left empty"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = True
                End If
            End If


        End If

        If Enq_hash.ContainsKey("PCN") = False Then


            If rdFather.Checked Then
                If ddlFNationality1.SelectedItem.Text = "" And ddlFNationality1.SelectedItem.Text = "-" Then
                    commString = commString & "<LI>Primary contact Nationality required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf rdMother.Checked Then
                If ddlMNationality1.SelectedItem.Text = "" And ddlMNationality1.SelectedItem.Text = "-" Then
                    commString = commString & "<LI>Primary contact Nationality required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            ElseIf rdGuardian.Checked Then
                If ddlGNationality1.SelectedItem.Text = "" And ddlGNationality1.SelectedItem.Text = "-" Then
                    commString = commString & "<LI>Primary contact Nationality required "
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = False
                End If

            End If




        End If

        If Enq_hash.ContainsKey("PCEID") = False Then
            If rdFather.Checked Then
                If txtFContEmail.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact E-mail address required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = True

                ElseIf isEmail(txtFContEmail.Text) = False Then
                    commString = commString & "<LI>Primary contact E-mail address is invalid"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = True
                End If
            ElseIf rdMother.Checked Then
                If txtMContEmail.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact E-mail address required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = True

                ElseIf isEmail(txtMContEmail.Text) = False Then
                    commString = commString & "<LI>Primary contact E-mail address is invalid"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = True
                End If

            ElseIf rdGuardian.Checked Then
                If txtGContEmail.Text.Trim = "" Then
                    commString = commString & "<LI>Primary contact E-mail address required"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = True

                ElseIf isEmail(txtGContEmail.Text) = False Then
                    commString = commString & "<LIPrimary contact E-mail address is invalid"
                    ViewState("Valid_Prim_cont") = "-1"
                    Final_error = True
                End If

            End If


        End If
        'validation for previous school Details
        If Session("sBsuid") <> "125017" Then
            If hfGRD_ID.Value <> "KG1" Then
                If ddlPGrade.SelectedItem.Value = "KG1" Then
                    If ddlNUnit.SelectedItem.Text = "--" And chkSibling.Checked = True Then
                        If Enq_hash.ContainsKey("CSN") = False Then
                            commString = commString & "<LI>Kindly select previous school name"
                            ViewState("Valid_Prev_school") = "-1"
                            Final_error = True
                        End If
                    End If
                End If
            End If
            If Enq_hash.ContainsKey("CSC") = False Then
                If txtPSchCity.Text.Trim = "" Then
                    commString = commString & "<LI>Previous school details cannot be left empty"
                    ViewState("Valid_Prev_school") = "-1"
                    Final_error = True
                End If
            End If
            If Enq_hash.ContainsKey("CURRN") = False Then
                If ddlPCurriculum.SelectedItem.Text = "--" Then
                    commString = commString & "<LI>Kindly select the curriculum"
                    ViewState("Valid_Prev_school") = "-1"
                End If
            End If
            'If txtLastAttDate.Text.Trim = "" Then
            '    commString = commString & "<LI>Last Attendance Date required"
            '    ViewState("Valid_Prev_school") = "-1"
            '    Final_error = True
            'End If
        End If


        'If txtLastAttDate.Text.Trim <> "" And IsDate(txtLastAttDate.Text.Trim) = False Then
        '    commString = commString & "<LI>Last Attendance Date is not a vaild date"
        '    ViewState("Valid_Prev_school") = "-1"
        '    Final_error = True
        'End If
        ' DateValid_Last_D = Date.ParseExact(txtLastAttDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), Globalization.DateTimeStyles.None)

        'check if payroll id is valid
        If txtSPayRollId.Text <> "" Then
            str_query = "SELECT ISNULL(EMP_ID,0) FROM EMPLOYEE_M WHERE EMPNO='" + txtSPayRollId.Text + "' AND EMP_BSU_ID='" + ddlStaffUnit.SelectedValue + "'"
            Dim EMPID As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
            If EMPID = 0 Then
                commString = commString & "<LI>StaffId is not a valid ID"
                Final_error = True
            End If
            HFEMP_ID.Value = EMPID
        Else
            HFEMP_ID.Value = 0
        End If


        If txtACCNO.Text = "" And txtACCNO.Visible = True Then
            commString = commString & "<LI>Please enter data in the field Account No."
            Final_error = True
        End If

        If txtACCNO.Visible = True Then
            If txtACCNO.Text <> hfACCNO.Value Then
                If isAccNoExist() = True Then
                    commString = commString & "<LI>Account No already exists."
                    Final_error = True
                End If
            End If
        End If
        If Final_error Then
            lblError.Text = commString & "</UL>"
        End If



        ' End of primary school details Application Info

        Return Final_error
        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'End Try
    End Function
    Function isAccNoExist() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT COUNT(EQS_ACCNO) FROM ENQUIRY_SCHOOLPRIO_S WHERE EQS_BSU_ID='" + Session("SBSUID") + "' AND EQS_ACCNO='" + CType(txtACCNO.Text.Trim, Integer).ToString + "'"

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)

        If count = 0 Then
            str_Sql = "SELECT COUNT(STU_ID) FROM STUDENT_M WHERE STU_FEE_ID=" + CType(txtACCNO.Text.Trim, Integer).ToString + " AND STU_BSU_ID='" + Session("SBSUID") + "'"
            Dim count1 As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)
            If count1 = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function
    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        Try
            If inputEmail.Length < 1 Then
                Return False
            Else
                Dim strRegex As String = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" '"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                Dim re As New Regex(strRegex)
                If re.IsMatch(inputEmail) Then
                    Return (True)
                Else
                    Return (False)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Function
    Private Sub BindReligion()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = "select RLG_ID,RLG_DESCR from RELIGION_M ORDER BY RLG_DESCR"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlReligion.DataSource = ds
            ddlReligion.DataTextField = "RLG_DESCR"
            ddlReligion.DataValueField = "RLG_ID"
            ddlReligion.DataBind()
            Dim li As New ListItem
            li.Text = "--"
            li.Value = "--"
            ddlReligion.Items.Add(li)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Function BindCountry(ByVal ddlCountry As DropDownList) As DropDownList

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select CTY_ID,CTY_DESCR from COUNTRY_M ORDER BY CTY_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlCountry.DataSource = ds
        ddlCountry.DataTextField = "CTY_DESCR"
        ddlCountry.DataValueField = "CTY_ID"
        ddlCountry.DataBind()
        Dim li As New ListItem
        li.Text = ""
        li.Value = ""
        ddlCountry.Items.Insert(0, li)
        Return ddlCountry

    End Function
    'Code added by Lijo 25-05-2008
    Private Function BindCompany(ByVal ddlComp) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select comp_ID,comp_Name from comp_Listed_M order by comp_Name"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlComp.DataSource = dr
        ddlComp.DataTextField = "comp_Name"
        ddlComp.DataValueField = "comp_ID"
        ddlComp.DataBind()
        Dim li As New ListItem
        li.Text = "Other"
        li.Value = "0"
        ddlComp.Items.Add(li)

        'ddlComp.Items(ddlComp.Items.IndexOf(li)).Selected = True
        ' ddlCompany.SelectedIndex = 0
        Return ddlComp
    End Function
    Private Function BindNationality(ByVal ddlNat) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select CTY_ID,CTY_NATIONALITY from COUNTRY_M ORDER BY CTY_NATIONALITY"
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        ddlNat.DataSource = dr
        ddlNat.DataTextField = "CTY_NATIONALITY"
        ddlNat.DataValueField = "CTY_ID"
        ddlNat.DataBind()
        Dim li As New ListItem
        li.Text = ""
        li.Value = ""
        ddlNat.Items.Insert(0, li)
        Return ddlNat
    End Function
    Public Sub BindCurriculum()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select CLM_ID,CLM_DESCR from CURRICULUM_M  ORDER BY CLM_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlPCurriculum.DataSource = ds
        ddlPCurriculum.DataTextField = "CLM_DESCR"
        ddlPCurriculum.DataValueField = "CLM_ID"
        ddlPCurriculum.DataBind()
        Dim li As New ListItem
        li.Text = ""
        li.Value = "0"
        ddlPCurriculum.Items.Insert(0, li)
    End Sub
    Public Function BindBusinessUnit(ByVal ddlbsu As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_sql As String = "select BSU_ID,BSU_NAME from businessunit_M  order by BSU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        ddlbsu.DataSource = ds
        ddlbsu.DataTextField = "BSU_NAME"
        ddlbsu.DataValueField = "BSU_ID"
        ddlbsu.DataBind()

        Dim li As New ListItem
        li.Text = ""
        li.Value = ""
        ddlbsu.Items.Insert(0, li)
    End Function
    Public Function BindBusinessUnit_prev()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list")
        ddlPSchool.DataSource = ds
        ddlPSchool.DataTextField = "BSU_NAME"
        ddlPSchool.DataValueField = "BSU_ID"
        ddlPSchool.DataBind()
        ddlPSchool.Items.Add(New ListItem("OTHER", ""))
        ddlPSchool.ClearSelection()
        ddlPSchool.Items.FindByText("OTHER").Selected = True

    End Function
    Sub BindGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select GRD_ID from GRADE_M"
        ddlPGrade.Items.Clear()
        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
        While reader.Read
            ddlPGrade.Items.Add(Convert.ToString(reader("GRD_ID")))
        End While
        reader.Close()
        Dim li As New ListItem
        li.Text = ""
        li.Value = ""
        ddlPGrade.Items.Insert(0, li)
    End Sub
    Sub BindNurSery()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select SCH_CODE,SCH_DESCR from PRENURSERY_M order by SCH_CODE"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlNUnit.DataSource = ds
        ddlNUnit.DataTextField = "SCH_DESCR"
        ddlNUnit.DataValueField = "SCH_CODE"
        ddlNUnit.DataBind()

        ddlPreSchool_Nursery.DataSource = ds
        ddlPreSchool_Nursery.DataTextField = "SCH_DESCR"
        ddlPreSchool_Nursery.DataValueField = "SCH_CODE"
        ddlPreSchool_Nursery.DataBind()

        Dim li As New ListItem
        li.Text = ""
        li.Value = "0"
        ddlPGrade.Items.Insert(0, li)
    End Sub
    Sub BindLocation()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT loc_description,loc_id FROM transport.vv_location_m WHERE loc_bsu_id='" + Session("sbsuid") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlLocation.DataSource = ds
        ddlLocation.DataTextField = "loc_description"
        ddlLocation.DataValueField = "loc_id"
        ddlLocation.DataBind()

    End Sub
    Sub BindSubLocation()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT sbl_description,sbl_id FROM transport.vv_sublocation_m WHERE sbl_bsu_id='" + Session("sbsuid") + "' and sbl_loc_id=" + ddlLocation.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlSubLocation.DataSource = ds
        ddlSubLocation.DataTextField = "sbl_description"
        ddlSubLocation.DataValueField = "sbl_id"
        ddlSubLocation.DataBind()
    End Sub
    Sub BindPickUp()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "SELECT pnt_description,pnt_id FROM transport.vv_pickupoints_m WHERE pnt_bsu_id='" + Session("sbsuid") + "' and pnt_sbl_id=" + ddlSubLocation.SelectedValue.ToString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlPickUp.DataSource = ds
        ddlPickUp.DataTextField = "pnt_description"
        ddlPickUp.DataValueField = "pnt_id"
        ddlPickUp.DataBind()
    End Sub

#End Region
    Sub populatePrev_data(ByVal Grd_id As String)
        Try


            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "exec [dbo].[GetENQUIRY_SCHPREV_EDITINFO] '" + hfENQ_ID.Value + "'"


            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

                While reader.Read

                    If reader.HasRows = True Then
                        If hfActual_GRD.Value = "PK" Or hfActual_GRD.Value = "KG1" Then
                            If Not ddlPreSchool_Nursery.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL"))) Is Nothing Then
                                ddlPreSchool_Nursery.ClearSelection()
                                ddlPreSchool_Nursery.Items.FindByValue(Convert.ToString(reader("EQM_PREVSCHOOL"))).Selected = True
                            End If
                            txtRegNo.Text = Convert.ToString(reader("EQM_PREVSCHOOL_REG_ID"))
                        Else
                            Dim rDt As DataRow
                            rDt = Session("TABLE_School_Pre").NewRow
                            rDt("ID") = ViewState("id")
                            rDt("SCH_ID") = Convert.ToString(reader("SCH_ID"))
                            rDt("EPS_Id") = Convert.ToString(reader("EPS_Id"))
                            rDt("EQM_PREVSCHOOL") = Convert.ToString(reader("EQM_PREVSCHOOL"))
                            rDt("EQM_PREVSCHOOL_HEAD_NAME") = Convert.ToString(reader("EQM_PREVSCHOOL_HEAD_NAME"))
                            rDt("EQM_PREVSCHOOL_REG_ID") = Convert.ToString(reader("EQM_PREVSCHOOL_REG_ID"))
                            rDt("EQM_PREVSCHOOL_GRD_ID") = Convert.ToString(reader("EQM_PREVSCHOOL_GRD_ID"))
                            rDt("EQM_PREVSCHOOL_MEDIUM") = Convert.ToString(reader("EQM_PREVSCHOOL_MEDIUM"))
                            rDt("EQM_PREVSCHOOL_ADDRESS") = Convert.ToString(reader("EQM_PREVSCHOOL_ADDRESS"))
                            rDt("EQM_PREVSCHOOL_CLM_ID") = Convert.ToString(reader("EQM_PREVSCHOOL_CLM_ID"))
                            rDt("EQM_PREVSCHOOL_CITY") = Convert.ToString(reader("EQM_PREVSCHOOL_CITY"))
                            rDt("EQM_PREVSCHOOL_CTY_ID") = Convert.ToString(reader("EQM_PREVSCHOOL_CTY_ID"))
                            rDt("EQM_PREVSCHOOL_PHONE") = Convert.ToString(reader("EQM_PREVSCHOOL_PHONE"))
                            rDt("EQM_PREVSCHOOL_FAX") = Convert.ToString(reader("EQM_PREVSCHOOL_FAX"))
                            rDt("EQM_PREVSCHOOL_FROMDT") = Convert.ToString(reader("EQM_PREVSCHOOL_FROMDT"))
                            rDt("EQM_PREVSCHOOL_LASTATTDATE") = Convert.ToString(reader("EQM_PREVSCHOOL_LASTATTDATE"))
                            rDt("EPS_DETAIL_TYPE") = Convert.ToString(reader("EPS_DETAIL_TYPE"))
                            rDt("STATUS") = "old"
                            ViewState("id") = ViewState("id") + 1
                            Session("TABLE_School_Pre").Rows.Add(rDt)

                        End If
                    End If
                End While
            End Using

            If hfActual_GRD.Value = "PK" Or hfActual_GRD.Value = "KG1" Then

                hideRow(True)
            Else
                hideRow(False)
                gridbind()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "populatePrev_data")
        End Try
    End Sub
    Sub hideRow(ByVal flag As Boolean)
        trSch0.Visible = flag
        trSch1.Visible = Not flag
        trSch2.Visible = Not flag
        trSch3.Visible = Not flag
        trSch4.Visible = Not flag
        trSch5.Visible = Not flag
        trSch5.Visible = Not flag
        trSch6.Visible = Not flag
        trSch7.Visible = Not flag
        trSch8.Visible = Not flag
        trSch9.Visible = Not flag
        trSch10.Visible = Not flag
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGridAdd.Click
        Try
            Dim Ibsu As Integer = 0
            Dim EQM_PREVSCHOOL As String = String.Empty
            Dim SCH_ID As String = String.Empty
            If ddlPSchool.SelectedItem.Text = "OTHER" Then
                EQM_PREVSCHOOL = txtPSchool.Text
                SCH_ID = ""
            Else
                EQM_PREVSCHOOL = ddlPSchool.SelectedItem.Text
                SCH_ID = ddlPSchool.SelectedValue
            End If

            If Session("TABLE_School_Pre").Rows.Count > 4 Then
                lblError.Text = "Add school details not more than four !!!"
                Exit Sub
            End If

            For i As Integer = 0 To Session("TABLE_School_Pre").Rows.Count - 1
                If ((UCase(Trim(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL"))).Trim = UCase(Trim(EQM_PREVSCHOOL.Trim)))) _
                 And ((UCase(Trim(Session("TABLE_School_Pre").Rows(i)("EQM_PREVSCHOOL_GRD_ID"))).Trim = UCase(Trim(ddlPGrade.SelectedValue.Trim)))) Then
                    lblError.Text = "Duplicate entry not allowed !!!"
                    Exit Sub
                    'ElseIf Session("TABLE_School_Pre").Rows(i)("EPS_DETAIL_TYPE").Trim = "Current School" Then
                    '    lblError.Text = "Duplicate current school type entry not allowed !!!"
                    '    Exit Sub
                End If
            Next


            Dim rDt As DataRow

            rDt = Session("TABLE_School_Pre").NewRow
            rDt("ID") = ViewState("id")
            rDt("SCH_ID") = SCH_ID
            rDt("EPS_Id") = "0"
            rDt("EQM_PREVSCHOOL") = EQM_PREVSCHOOL
            rDt("EQM_PREVSCHOOL_HEAD_NAME") = txtSchool_head.Text.Trim
            rDt("EQM_PREVSCHOOL_REG_ID") = txtFeeID_GEMS.Text.Trim
            rDt("EQM_PREVSCHOOL_GRD_ID") = ddlPGrade.SelectedValue.Trim
            rDt("EQM_PREVSCHOOL_MEDIUM") = txtLang_Instr.Text
            rDt("EQM_PREVSCHOOL_ADDRESS") = txtSchAddr.Text.Trim
            rDt("EQM_PREVSCHOOL_CLM_ID") = ddlPCurriculum.SelectedValue
            rDt("EQM_PREVSCHOOL_CITY") = txtPSchCity.Text.Trim
            rDt("EQM_PREVSCHOOL_CTY_ID") = ddlPCountry.SelectedValue
            rDt("EQM_PREVSCHOOL_PHONE") = txtSCHPhone_Country.Text + "-" + txtSCHPhone_Area.Text + "-" + txtSCHPhone_No.Text
            rDt("EQM_PREVSCHOOL_FAX") = txtSCHFax_Country.Text + "-" + txtSCHFax_Area.Text + "-" + txtSCHFax_No.Text
            rDt("EQM_PREVSCHOOL_FROMDT") = txtSchFrom_dt.Text
            rDt("EQM_PREVSCHOOL_LASTATTDATE") = txtSchTo_dt.Text
            rDt("EPS_DETAIL_TYPE") = ltSchoolType.Text
            rDt("STATUS") = "add"
            ViewState("id") = ViewState("id") + 1
            Session("TABLE_School_Pre").Rows.Add(rDt)
            gridbind()
            clearall()


        Catch ex As Exception
            lblError.Text = "Error in adding new record"
        End Try
    End Sub
    Protected Sub gvSchool_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvSchool.RowEditing


        gvSchool.SelectedIndex = e.NewEditIndex


        Dim row As GridViewRow = gvSchool.Rows(e.NewEditIndex)
        Dim lblID As New Label
        lblID = TryCast(row.FindControl("lblId"), Label)
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        iIndex = CInt(lblID.Text)

        Dim arInfoLang(3) As String
        Dim splitterLang As Char = "-"


        'loop through the data table row  for the selected rowindex item in the grid view
        For iEdit = 0 To Session("TABLE_School_Pre").Rows.Count - 1
            If iIndex = Session("TABLE_School_Pre").Rows(iEdit)("ID") Then

                If Not ddlPSchool.Items.FindByValue(Session("TABLE_School_Pre").Rows(iEdit)("SCH_ID")) Is Nothing Then
                    ddlPSchool.ClearSelection()
                    ddlPSchool.Items.FindByValue(Session("TABLE_School_Pre").Rows(iEdit)("SCH_ID")).Selected = True
                End If

                If Session("TABLE_School_Pre").Rows(iEdit)("SCH_ID") = "" Then
                    txtPSchool.Text = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL")
                    txtPreSchool_Nursery.Text = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL")
                Else
                    txtPSchool.Text = ""
                    txtPreSchool_Nursery.Text = ""
                End If
                txtLang_Instr.Text = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_MEDIUM")

                txtSchool_head.Text = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_HEAD_NAME")
                txtFeeID_GEMS.Text = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_REG_ID")

                If Not ddlPGrade.Items.FindByValue(Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_GRD_ID")) Is Nothing Then
                    ddlPGrade.ClearSelection()
                    ddlPGrade.Items.FindByValue(Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_GRD_ID")).Selected = True
                End If
                txtPSchCity.Text = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_CITY")
                If Not ddlPCountry.Items.FindByValue(Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_CTY_ID")) Is Nothing Then
                    ddlPCountry.ClearSelection()
                    ddlPCountry.Items.FindByValue(Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_CTY_ID")).Selected = True
                End If

                If Not ddlPCurriculum.Items.FindByValue(Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_CLM_ID")) Is Nothing Then
                    ddlPCurriculum.ClearSelection()
                    ddlPCurriculum.Items.FindByValue(Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_CLM_ID")).Selected = True
                End If


                txtSchAddr.Text = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_ADDRESS")
                arInfoLang = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_PHONE").Trim.Split(splitterLang)

                If arInfoLang.Length > 2 Then
                    txtSCHPhone_Country.Text = arInfoLang(0)
                    txtSCHPhone_Area.Text = arInfoLang(1)
                    txtSCHPhone_No.Text = arInfoLang(2)
                ElseIf arInfoLang.Length = 2 Then

                    txtSCHPhone_Area.Text = arInfoLang(0)
                    txtSCHPhone_No.Text = arInfoLang(1)
                Else
                    txtSCHPhone_No.Text = arInfoLang(0)
                End If

                arInfoLang = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_FAX").Trim.Split(splitterLang)

                If arInfoLang.Length > 2 Then
                    txtSCHFax_Country.Text = arInfoLang(0)
                    txtSCHFax_Area.Text = arInfoLang(1)
                    txtSCHFax_No.Text = arInfoLang(2)

                ElseIf arInfoLang.Length = 2 Then
                    txtSCHFax_Area.Text = arInfoLang(0)
                    txtSCHFax_No.Text = arInfoLang(1)
                Else
                    txtSCHFax_No.Text = arInfoLang(0)
                End If


                txtSchFrom_dt.Text = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_FROMDT")
                txtSchTo_dt.Text = Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_LASTATTDATE")
                ltSchoolType.Text = Session("TABLE_School_Pre").Rows(iEdit)("EPS_DETAIL_TYPE")

                Exit For
            End If
        Next
        gvSchool.Columns(6).Visible = False
        btnGridAdd.Visible = False
        btnGridUpdate.Visible = True
        'gridbind()
    End Sub
    Protected Sub btnGridUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGridUpdate.Click
        Dim iEdit As Integer = 0
        Dim iIndex As Integer = 0
        Dim row As GridViewRow = gvSchool.Rows(gvSchool.SelectedIndex)
        Dim idRow As New Label
        idRow = TryCast(row.FindControl("lblID"), Label)
        iIndex = CInt(idRow.Text)

        Dim EQM_PREVSCHOOL As String = String.Empty
        Dim SCH_ID As String = String.Empty
        If ddlPSchool.SelectedItem.Text = "OTHER" Then
            EQM_PREVSCHOOL = txtPSchool.Text
            SCH_ID = ""
        Else
            EQM_PREVSCHOOL = ddlPSchool.SelectedItem.Text
            SCH_ID = ddlPSchool.SelectedValue
        End If


        For iEdit = 0 To Session("TABLE_School_Pre").Rows.Count - 1
            If iIndex = Session("TABLE_School_Pre").Rows(iEdit)("ID") Then
                Session("TABLE_School_Pre").Rows(iEdit)("SCH_ID") = SCH_ID
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL") = EQM_PREVSCHOOL
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_HEAD_NAME") = txtSchool_head.Text.Trim
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_REG_ID") = txtFeeID_GEMS.Text.Trim
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_GRD_ID") = ddlPGrade.SelectedValue.Trim
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_MEDIUM") = txtLang_Instr.Text
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_ADDRESS") = txtSchAddr.Text
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_CLM_ID") = ddlPCurriculum.SelectedValue
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_CITY") = txtPSchCity.Text.Trim
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_CTY_ID") = ddlPCountry.SelectedValue
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_PHONE") = txtSCHPhone_Country.Text + "-" + txtSCHPhone_Area.Text + "-" + txtSCHPhone_No.Text
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_FAX") = txtSCHFax_Country.Text + "-" + txtSCHFax_Area.Text + "-" + txtSCHFax_No.Text
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_FROMDT") = txtSchFrom_dt.Text
                Session("TABLE_School_Pre").Rows(iEdit)("EQM_PREVSCHOOL_LASTATTDATE") = txtSchTo_dt.Text
                Session("TABLE_School_Pre").Rows(iEdit)("EPS_DETAIL_TYPE") = ltSchoolType.Text

                If Session("TABLE_School_Pre").Rows(iEdit)("STATUS") = "old" Then
                    Session("TABLE_School_Pre").Rows(iEdit)("STATUS") = "update"
                End If


                Exit For
            End If
        Next

        btnGridAdd.Visible = True

        btnGridUpdate.Visible = False

        gvSchool.SelectedIndex = -1
        gridbind()
        gvSchool.Columns(6).Visible = True
        clearall()

    End Sub
    Sub clearall()
        txtPSchool.Text = ""
        txtSchool_head.Text = ""
        txtFeeID_GEMS.Text = ""
        If Not ddlPSchool.Items.FindByText("OTHER") Is Nothing Then
            ddlPSchool.ClearSelection()
            ddlPSchool.Items.FindByText("OTHER").Selected = True
        End If

        ddlPCountry.ClearSelection()
        ddlPCountry.Items.FindByValue("172").Selected = True
        If Not ddlPCurriculum.Items.FindByText("--") Is Nothing Then
            ddlPCurriculum.ClearSelection()
            ddlPCurriculum.Items.FindByText("--").Selected = True
        End If

        txtLang_Instr.Text = ""
        txtPSchCity.Text = ""
        txtSCHPhone_Country.Text = ""
        txtSCHPhone_Area.Text = ""
        txtSCHPhone_No.Text = ""
        txtSCHFax_Country.Text = ""
        txtSCHFax_Area.Text = ""
        txtSCHFax_No.Text = ""
        txtSchFrom_dt.Text = ""
        txtSchTo_dt.Text = ""
        txtSchAddr.Text = ""
        ltSchoolType.Text = "Previous School"
    End Sub
End Class
