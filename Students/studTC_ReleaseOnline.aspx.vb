﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System

Partial Class Students_studTC_ReleaseOnline
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100283") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    BindCurriculum()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, ddlClm.SelectedValue.ToString, Session("sbsuid"))
                    If Session("sbsuid") <> "999998" And ddlClm.Items.Count = 1 Then
                        trBsu.Visible = False
                    Else

                    End If
                    BindGrade()
                    BindSection()
                    trGrid.Visible = False
                    trRelease.Visible = False
                End If
                gvStud.Attributes.Add("bordercolor", "#1b80b6")
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindCurriculum()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT DISTINCT CLM_DESCR,CLM_ID FROM CURRICULUM_M INNER JOIN ACADEMICYEAR_D ON " _
                                & " ACD_CLM_ID=CLM_ID AND ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("sbsuid") + "' ORDER BY CLM_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClm.DataSource = ds
        ddlClm.DataTextField = "CLM_DESCR"
        ddlClm.DataValueField = "CLM_ID"
        ddlClm.DataBind()
    End Sub

    Sub BindGrade()
        ddlGrade.Items.Clear()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT GRM_DISPLAY,GRD_ID,GRD_DISPLAYORDER FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRM_GRD_ID=GRD_ID" _
                              & " WHERE GRM_ACD_ID= " + ddlAcademicYear.SelectedValue.ToString _
                              & " ORDER BY GRD_DISPLAYORDER"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlGrade.DataSource = ds
        ddlGrade.DataTextField = "GRM_DISPLAY"
        ddlGrade.DataValueField = "GRD_ID"
        ddlGrade.DataBind()
        ddlGrade.Items.Insert(0, li)
    End Sub
    Sub BindSection()
        ddlSection.Items.Clear()
        Dim li As New ListItem
        li.Text = "ALL"
        li.Value = "0"
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT SCT_ID,SCT_DESCR FROM SECTION_M WHERE SCT_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString _
                                    & " AND SCT_GRD_ID='" + ddlGrade.SelectedValue.ToString + "' ORDER BY SCT_DESCR"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSection.DataSource = ds
            ddlSection.DataTextField = "SCT_DESCR"
            ddlSection.DataValueField = "SCT_ID"
            ddlSection.DataBind()
        ddlSection.Items.Insert(0, li)

    End Sub

    Sub GridBind()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') AS STU_NAME," _
                                  & " GRM_DISPLAY,SCT_DESCR,ISNULL(TSR_bRELEASE,'false') AS TSR_bRELEASE,STU_GRD_ID FROM STUDENT_M INNER JOIN " _
                                  & " GRADE_BSU_M ON STU_GRM_ID=GRM_ID INNER JOIN SECTION_M ON STU_SCT_ID=SCT_ID " _
                                  & " LEFT OUTER JOIN TC_ONLINE_RELASESTUDENTS ON TSR_STU_ID=STU_ID AND TSR_aCD_ID=STU_ACD_ID" _
                                  & " WHERE STU_ACD_ID=" + hfACD_ID.Value _
                                  & " AND STU_CURRSTATUS='EN'"

        If ddlType.SelectedIndex = 0 Then
            str_query += " AND STU_ID IN(SELECT TPS_STU_ID FROM TC_PRINCIPALINTERVIEW_STUDENT WHERE TPS_ACd_ID=" + hfACD_ID.Value + " AND ISNULL(TPS_bRELEASETC,0)=1)"
        End If
        If ddlGrade.SelectedValue.ToString <> "0" Then
            str_query += " AND STU_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'"
        End If

        If ddlSection.SelectedValue.ToString <> "0" Then
            str_query += " AND STU_SCT_ID=" + ddlSection.SelectedValue.ToString
        End If

       

        If txtStudentID.Text <> "" Then
            str_query += " AND STU_NO LIKE '%" + txtStudentID.Text + "%'"
        End If

        If txtName.Text <> "" Then
            str_query += " AND ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        End If

        str_query += "ORDER BY GRM_DISPLAY,SCT_DESCR,STU_FIRSTNAME,STU_MIDNAME,STU_LASTNAME "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStud.DataSource = ds
        gvStud.DataBind()

     
    End Sub

    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim lblStuId As Label
        Dim chkRelease As CheckBox
        Dim i As Integer

        For i = 0 To gvStud.Rows.Count - 1
            lblStuId = gvStud.Rows(i).FindControl("lblStuId")
            chkRelease = gvStud.Rows(i).FindControl("chkRelease")

            str_query = "EXEC saveTC_ONLINE_RELASESTUDENTS " _
                       & " @TSR_STU_ID=" + lblStuId.Text + "," _
                       & " @TSR_ACD_ID=" + hfACD_ID.Value + "," _
                       & " @TSR_GRD_ID='" + ddlGrade.SelectedValue.ToString + "'," _
                       & " @TSR_bRELEASE='" + chkRelease.Checked.ToString + "'," _
                       & " @TSR_USER='" + Session("sUsr_name") + "'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Next
    End Sub



#End Region

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        hfACD_ID.Value = ddlAcademicYear.SelectedValue.ToString
        GridBind()
        trGrid.Visible = True
        trRelease.Visible = True
    End Sub

    Protected Sub btnRelease_Click(sender As Object, e As EventArgs) Handles btnRelease.Click
        SaveData()
        lblError.Text = "Record saved successfully"
    End Sub

    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGrade.SelectedIndexChanged
        BindSection()
    End Sub
End Class
