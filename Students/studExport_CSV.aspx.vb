Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Xml
Imports GemBox.Spreadsheet
Imports ResponseHelper
Imports system
Imports System.Data.SqlTypes
Partial Class studExport_CSV
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        tr_TCSO.Visible = False

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If GetBSU_ShowExpButton() = "0" Then
                    btnExport.Visible = "FALSE"
                Else
                    btnExport.Visible = "TRUE"
                End If


                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If


                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


                If USR_NAME = "" Or ((ViewState("MainMnu_code") <> "S100296") And (ViewState("MainMnu_code") <> "S100199") And (ViewState("MainMnu_code") <> "S200558")) Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                    'Session("Current_ACD_ID") = "-1"
                    'Session("Current_ACY_ID") = "-1"
                    'Session("Cutoff_Age") = ""
                    'Call Academic_Cutoff()
                    callYEAR_DESCRBind()
                    BindGrades()

                    Call GetTransfer()
                    ' Call Shift_Bind()
                    ' Call Prev_Next()
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub
    Sub BindGrades()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
        Dim ds As New DataSet

        str_Sql = " SELECT DISTINCT  GRADE_BSU_M.GRM_DISPLAY, GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_M.GRD_DISPLAYORDER " & _
              " FROM  GRADE_M INNER JOIN GRADE_BSU_M ON GRADE_M.GRD_ID = GRADE_BSU_M.GRM_GRD_ID WHERE GRM_BSU_ID=" & Session("SBsuID") & " and GRM_ACD_ID=" & ACD_ID & " ORDER BY GRD_DISPLAYORDER"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlGrdaes.Items.Clear()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlGrdaes.DataSource = ds.Tables(0)
            ddlGrdaes.DataTextField = "GRM_DISPLAY"
            ddlGrdaes.DataValueField = "GRD_ID"
            ddlGrdaes.DataBind()
        End If

    End Sub

    Sub GetTransfer()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " SELECT FRM_CODE, FRM_DESCR FROM EXPORT_FORMATS WHERE FRM_BSU_ID='" & Session("sBsuid") & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlTransferType.DataSource = ds
        ddlTransferType.DataValueField = "FRM_CODE"
        ddlTransferType.DataTextField = "FRM_DESCR"
        ddlTransferType.DataBind()
    End Sub
    Public Sub callYEAR_DESCRBind()
        Try
            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                    End While
                End If
            End Using
            For ItemTypeCounter As Integer = 0 To ddlAcademicYear.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not Session("Current_ACD_ID") Is Nothing Then
                    If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
                        ddlAcademicYear.SelectedIndex = ItemTypeCounter
                    End If
                End If
            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrades()
    End Sub



    

    
    Public Function GetBSU_bbsct_status() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select BSU_bMOEGRADE FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBSUID") & "'"
        Return IIf(IsDBNull(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)) = False, SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql), "False")

    End Function
    Public Function GetBSU_ShowExpButton() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select BSU_SHOW_BB_BUTTON FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBSUID") & "'"
        Return IIf(IsDBNull(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)) = False, SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql), "0")

    End Function


    


    'Sub Shift_Bind()
    '    Using readerGrade_Section As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Session("sBsuid"), Session("Current_ACD_ID"))
    '        Dim di_Shift As ListItem
    '        ddlShift.Items.Clear()
    '        If readerGrade_Section.HasRows = True Then
    '            While readerGrade_Section.Read
    '                di_Shift = New ListItem(readerGrade_Section("SHF_DESCR"), readerGrade_Section("SHF_ID"))
    '                ddlShift.Items.Add(di_Shift)
    '            End While
    '            For ItemTypeCounter As Integer = 0 To ddlShift.Items.Count - 1
    '                'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
    '                If ddlShift.Items(ItemTypeCounter).Text = "NORMAL" Then
    '                    ddlShift.SelectedIndex = ItemTypeCounter
    '                End If
    '            Next
    '        Else
    '            di_Shift = New ListItem("Not Selected", "-1")
    '            ddlShift.Items.Add(di_Shift)
    '        End If

    '    End Using

    'End Sub

    'Protected Sub ddlShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShift.SelectedIndexChanged

    'End Sub

    'Protected Sub ddlSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSection.SelectedIndexChanged

    'End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        

        ExportCSV_MISC()

    End Sub


    Protected Sub ExportCSV_MISC()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim lstrTC As String
        Dim lstrSO As String

        Dim strGrades As String = String.Empty

        For Each LstItem As ListItem In ddlGrdaes.Items
            If LstItem.Selected = True Then
                strGrades += LstItem.Value.ToString + "|"
            End If
        Next



        If consider_tc.Checked = True Then
            lstrTC = "1"
        Else
            lstrTC = "0"
        End If

        If consider_tc.Checked = True Then
            lstrSO = "1"
        Else
            lstrSO = "0"
        End If

        Dim param(30) As SqlClient.SqlParameter

        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBSUID"))
        param(2) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedItem.Value)
        param(3) = New SqlClient.SqlParameter("@GRD_ID", strGrades)
        param(4) = New SqlClient.SqlParameter("@SCT_ID", "")
        param(5) = New SqlClient.SqlParameter("@MINLIST", "ALL")
        param(6) = New SqlClient.SqlParameter("@DOJ_FROM", txtAcdDate_From.Text)
        param(7) = New SqlClient.SqlParameter("@DOJ_TO", txtAcdDate_To.Text)
        param(8) = New SqlClient.SqlParameter("@TCSO_CutOff", txtTC_SO_Cutoff.Text)
        param(9) = New SqlClient.SqlParameter("@Consider_tc", lstrTC)
        param(10) = New SqlClient.SqlParameter("@Consider_so", lstrSO)

        param(11) = New SqlClient.SqlParameter("@STU_TFRTYPE", IIf(ddlTransferType.SelectedValue = "All", System.DBNull.Value, ddlTransferType.SelectedValue))
        param(12) = New SqlClient.SqlParameter("@STU_EXPORT_TYPE", ddlExportType.SelectedValue)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Export_Data_CSV_MISC", param)
        Dim dtCSV As New DataTable
        dtCSV = ds.Tables(0)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", _
                "attachment;filename=DataTable.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"

        Dim sb As New StringBuilder()
        For k As Integer = 0 To dtCSV.Columns.Count - 1
            'add separator
            sb.Append(dtCSV.Columns(k).ColumnName + ","c)
        Next
        If sb.Length > 0 Then
            sb.Remove(sb.ToString.LastIndexOf(","), 1)
        End If

        'append new line
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To dtCSV.Rows.Count - 1
            For k As Integer = 0 To dtCSV.Columns.Count - 1
                'add separator
                sb.Append(dtCSV.Rows(i)(k).ToString().Replace(",", "--") + ","c)
            Next
            If sb.Length > 0 Then
                sb.Remove(sb.ToString.LastIndexOf(","), 1)
            End If
            'append new line
            sb.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
End Class
