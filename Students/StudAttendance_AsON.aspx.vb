Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_StudStaffAuthorized_Grade_View
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0

    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059075") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    txtAsOnDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    h_selected_menu_6.Value = "LI__../Images/operations/like.gif"

                    Call GetAcademicWeekend()
                    Call GetAcademicSTARTDT_ENDDT()

                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Sub GetAcademicWeekend()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetAcademicSTARTDT_ENDDT()
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = Session("Current_ACD_ID")


            str_Sql = " select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = ds.Tables(0).Rows(0)(0).ToString
                ViewState("ACD_ENDDT") = ds.Tables(0).Rows(0)(1).ToString
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
        str_Sid_img = h_selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
      

    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
   
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvAuthorizedRecord.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAuthorizedRecord.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Function ValidateDate() As String
        Try


            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtAsOnDate.Text.Trim <> "" Then
                Dim strfDate As String = txtAsOnDate.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "As on Date format is Invalid"
                Else
                    txtAsOnDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtAsOnDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "As on Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "As on Date required"

            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN AS ON DATE SELECT", "School Strength Detail")
            Return "-1"
        End Try

    End Function
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        If ValidateDate() <> "-1 " Then
            Try

                Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
                Dim str_Sql As String = ""
                Dim str_filter_GRADE As String = String.Empty
                Dim str_filter_SECTION As String = String.Empty
                Dim str_filter_SNAME As String = String.Empty
                Dim str_filter_STU_NO As String = String.Empty
                Dim str_filter_STATUS As String = String.Empty
                Dim str_filter_REMARKS As String = String.Empty

                Dim STDT As String = ViewState("ACD_STARTDT")
                Dim ETDT As String = ViewState("ACD_ENDDT")
                Dim WE1 As String = ViewState("BSU_WEEKEND1")
                Dim WE2 As String = ViewState("BSU_WEEKEND2")
                Dim AS_ON_DATE As String = Format(Date.Parse(txtAsOnDate.Text), "dd/MMM/yyyy")
                Dim ACD_ID As String = Session("Current_ACD_ID")
                Dim ds As New DataSet

                str_Sql = " SELECT STU_ID,STU_NO,SNAME,STATUS,REMARKS,SCT_DESCR,GRM_DISPLAY FROM [ATT].[fn_DAY_ATT_SCHOOL]('" & ACD_ID & "','" & AS_ON_DATE & "','" & STDT & "','" & ETDT & "','" & WE1 & "','" & WE2 & "') WHERE STU_ID<>'' "




                Dim lblID As New Label

                Dim txtSearch As New TextBox
                Dim str_search As String
                Dim str_GRADE As String = String.Empty
                Dim str_SECTION As String = String.Empty
                Dim str_SNAME As String = String.Empty
                Dim str_STU_NO As String = String.Empty
                Dim str_STATUS As String = String.Empty
                Dim str_REMARKS As String = String.Empty

                If gvAuthorizedRecord.Rows.Count > 0 Then

                    Dim str_Sid_search() As String


                    str_Sid_search = h_Selected_menu_1.Value.Split("__")
                    str_search = str_Sid_search(0)
                    txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTU_NO")
                    str_STU_NO = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_STU_NO = " AND STU_NO  LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_STU_NO = "  AND  NOT STU_NO   LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_STU_NO = " AND STU_NO    LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_STU_NO = " AND STU_NO   NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_STU_NO = " AND STU_NO  LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_STU_NO = " AND STU_NO   NOT LIKE '%" & txtSearch.Text & "'"
                    End If


                    str_Sid_search = h_Selected_menu_2.Value.Split("__")
                    str_search = str_Sid_search(0)

                    txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSNAME")
                    str_SNAME = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_SNAME = " AND SNAME LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_SNAME = "  AND  NOT SNAME LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_SNAME = " AND SNAME  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_SNAME = " AND SNAME NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_SNAME = " AND SNAME LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_SNAME = " AND SNAME NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                   
                    str_Sid_search = h_Selected_menu_3.Value.Split("__")
                    str_search = str_Sid_search(0)

                    txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtGRM_DISPLAY")
                    str_GRADE = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_GRADE = " AND GRM_DISPLAY LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_GRADE = "  AND  NOT GRM_DISPLAY LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_GRADE = " AND GRM_DISPLAY  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_GRADE = " AND GRM_DISPLAY NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_GRADE = " AND GRM_DISPLAY LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_GRADE = " AND GRM_DISPLAY NOT LIKE '%" & txtSearch.Text & "'"
                    End If



                    str_Sid_search = h_Selected_menu_4.Value.Split("__")
                    str_search = str_Sid_search(0)

                    txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSCT_DESCR")
                    str_SECTION = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_SECTION = " AND SCT_DESCR LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_SECTION = "  AND  NOT SCT_DESCR LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_SECTION = " AND SCT_DESCR  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_SECTION = " AND SCT_DESCR NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_SECTION = " AND SCT_DESCR LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_SECTION = " AND SCT_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                    str_Sid_search = h_Selected_menu_5.Value.Split("__")
                    str_search = str_Sid_search(0)

                    txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTATUS")
                    str_STATUS = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_STATUS = " AND STATUS LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_STATUS = "  AND  NOT STATUS LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_STATUS = " AND STATUS  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_STATUS = " AND STATUS NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_STATUS = " AND STATUS LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_STATUS = " AND STATUS NOT LIKE '%" & txtSearch.Text & "'"
                    End If

                    str_Sid_search = h_selected_menu_6.Value.Split("__")
                    str_search = str_Sid_search(0)

                    txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtREMARKS")
                    str_REMARKS = txtSearch.Text

                    If str_search = "LI" Then
                        str_filter_REMARKS = " AND REMARKS LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "NLI" Then
                        str_filter_REMARKS = "  AND  NOT REMARKS LIKE '%" & txtSearch.Text & "%'"
                    ElseIf str_search = "SW" Then
                        str_filter_REMARKS = " AND REMARKS  LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "NSW" Then
                        str_filter_REMARKS = " AND REMARKS NOT LIKE '" & txtSearch.Text & "%'"
                    ElseIf str_search = "EW" Then
                        str_filter_REMARKS = " AND REMARKS LIKE  '%" & txtSearch.Text & "'"
                    ElseIf str_search = "NEW" Then
                        str_filter_REMARKS = " AND REMARKS NOT LIKE '%" & txtSearch.Text & "'"
                    End If


                End If

                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_SNAME & str_filter_GRADE & str_filter_SECTION & str_filter_STATUS & str_filter_REMARKS & str_filter_STU_NO & " ORDER BY GRD_DISPLAYORDER ,SCT_DESCR, SNAME")
              
                If ds.Tables(0).Rows.Count > 0 Then

                    gvAuthorizedRecord.DataSource = ds.Tables(0)
                    gvAuthorizedRecord.DataBind()

                Else
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                    'start the count from 1 no matter gridcolumn is visible or not
                    ds.Tables(0).Rows(0)(6) = True

                    gvAuthorizedRecord.DataSource = ds.Tables(0)
                    Try
                        gvAuthorizedRecord.DataBind()
                    Catch ex As Exception
                    End Try

                    Dim columnCount As Integer = gvAuthorizedRecord.Rows(0).Cells.Count
                    'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                    gvAuthorizedRecord.Rows(0).Cells.Clear()
                    gvAuthorizedRecord.Rows(0).Cells.Add(New TableCell)
                    gvAuthorizedRecord.Rows(0).Cells(0).ColumnSpan = columnCount
                    gvAuthorizedRecord.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                    gvAuthorizedRecord.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
                End If

                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtGRM_DISPLAY")
                txtSearch.Text = str_GRADE
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSCT_DESCR")
                txtSearch.Text = str_SECTION
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTU_NO")
                txtSearch.Text = str_STU_NO
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSNAME")
                txtSearch.Text = str_SNAME
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtSTATUS")
                txtSearch.Text = str_STATUS
                txtSearch = gvAuthorizedRecord.HeaderRow.FindControl("txtREMARKS")
                txtSearch.Text = str_REMARKS
             
                'Call callYEAR_DESCRBind()

                'Call ddlOpenOnLine_state(ddlOPENONLINEH.SelectedItem.Text)

                set_Menu_Img()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
 

    Protected Sub gvStudentRecord_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAuthorizedRecord.PageIndexChanging
        gvAuthorizedRecord.PageIndex = e.NewPageIndex
        gridbind()
    End Sub


    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub


    Protected Sub btnSearchSTU_NO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSNAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchGRM_DISPLAY_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSCT_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchStatus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchREMARKS_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub gvAuthorizedRecord_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAuthorizedRecord.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim lbl As Label = DirectCast(e.Row.FindControl("lblSTATUS"), Label)
            If lbl.Text = "ABSENT" Then
                lbl.ForeColor = Drawing.Color.Red
            ElseIf lbl.Text = "HOLIDAY" Then
                lbl.ForeColor = System.Drawing.Color.Green 'FromArgb(27, 128, 182)

            ElseIf lbl.Text = "WEEKEND" Then
                lbl.ForeColor = System.Drawing.Color.Brown
            ElseIf lbl.Text = "" Then
                lbl.Text = "NOT MARKED"
                lbl.ForeColor = System.Drawing.Color.Chocolate


            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gridbind()
    End Sub
End Class
