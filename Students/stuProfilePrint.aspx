﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="stuProfilePrint.aspx.vb"
    Inherits="Students_stuProfilePrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD Xhtml 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0038)http://live.gemsoasis.com/studprof.php -->
<html>
<head>
    <title>:::: GEMS :::: Student Profile ::::</title>
    <%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta content="MShtml 6.00.2900.3268" name="GENERATOR">
</head>
<body text="#000000" bgcolor="#ffffff" leftmargin="0" topmargin="0" onload="window.print()">
    <form id="Form1" runat="server">
        <table id="AutoNumber1"
            cellspacing="0" cellpadding="0" width="90%" border="0">
            <tbody>
                <tr>
                    <td align="center" width="50%">
                        <asp:Image ID="Image2" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
        <%--    <table id="table1" style="border-collapse: collapse" bordercolor="#111111" height="1"
        cellspacing="0" cellpadding="0" width="100%" border="0">
        <tbody>
            <tr>
                <td width="25%" bgcolor="rgb(27,128,182)" height="18">
                </td>
                <td width="25%" bgcolor="rgb(237,177,19)" height="18">
                </td>
                <td width="25%" bgcolor="rgb(83,176,70)" height="18">
                </td>
                <td align="right" width="25%" bgcolor="rgb(236,50,37)" height="18">
                    <b><font face="Arial" color="#c7c9b5" size="2">&nbsp;</font></b>
                </td>
            </tr>
        </tbody>
    </table>--%>
        <table cellspacing="0" cellpadding="0" width="90%" align="center"
            border="0">
            <tbody>
                <tr>
                    <td align="left" width="50%">&nbsp;
                    </td>
                    <td align="right" width="50%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="title-bg" valign="middle" align="left" colspan="6">Student Profile
                    </td>
                </tr>
            </tbody>
        </table>
        <table cellspacing="0" cellpadding="0" width="90%" align="center"
            border="0" class="table table-bordered table-row">
            <tbody>
                <tr>
                    <td valign="middle" align="right" colspan="6">&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <div style="width: 90%; margin: auto;">
            <table id="table2" height="730" cellspacing="0" cellpadding="0" width="90%" border="0" class="table table-bordered table-row">
                <tbody>
                    <tr>
                        <td class="matters" valign="top">
                            <table id="table3" cellspacing="0" cellpadding="0" width="100%"
                                border="0" class="table table-bordered table-row">
                                <tbody>
                                    <tr class="title-bg-lite">
                                        <td colspan="8">
                                            <p style="margin-top: 0px; margin-bottom: 0px" align="center">

                                                <asp:Label ID="lblstuname" runat="server"></asp:Label>

                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="6%">SEN</td>
                                        <td width="23%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblsen" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td width="6%">Year</td>
                                        <td width="8%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblyear" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td width="6%">div</td>
                                        <td width="6%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lbldiv" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td width="12%">School</td>
                                        <td class="matters3" width="31%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblschool" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: 0px; margin-bottom: 0px">
                                &nbsp;
                            </p>
                            <table id="table4" cellspacing="0" cellpadding="0" width="100%"
                                border="0" class="table table-bordered table-row">
                                <tbody align="left">
                                    <tr>
                                        <td width="16%">Date of Birth</td>
                                        <td width="23%" colspan="2">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lbldateofbirth" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td width="15%">Religion</td>
                                        <td width="19%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblreligion" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td align="center" width="22%" rowspan="9">
                                            <asp:Image ID="Image1" Height="137" Width="106" runat="server" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="16%">Place of Birth</td>
                                        <td width="23%" colspan="2">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblplaceofbirth" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td width="15%">Nationality</td>
                                        <td width="19%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblnationality" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="16%">Date of Joining</td>
                                        <td width="23%"
                                            colspan="2">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lbldateofjoin" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td width="15%">Emergency Contact</td>
                                        <td width="19%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblemergencycontact" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="title-bg-lite">
                                        <td valign="middle" align="center" colspan="5">
                                            <div align="left">
                                                Passport/Visa Details 
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">Full Name as in Passport: &nbsp;&nbsp;
                                        <asp:Label ID="lblpassportname"
                                            runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Passport No</td>
                                        <td width="23%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblpassportnumber" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td>Issue Place</td>
                                        <td width="19%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblpassportissueplace" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Issue Date</td>
                                        <td width="23%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblpassportissuedate" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td>Expiry Date</td>
                                        <td width="19%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblpassportexpirydate" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Visa No</td>
                                        <td width="23%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblvisanumber" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td>Issue Place</td>
                                        <td width="19%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblvisaissueplace" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Issue Date</td>
                                        <td width="23%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblvisaissuedate" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                        <td>Expiry Date</td>
                                        <td width="19%">
                                            <p style="margin-top: 0px; margin-bottom: 0px">
                                                <asp:Label ID="lblvisaexpirydate" runat="server"></asp:Label>&nbsp;
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: 0px; margin-bottom: 0px">
                                &nbsp;
                            </p>
                            <table id="table5" cellspacing="0" cellpadding="0" width="100%"
                                border="0" class="table table-bordered table-row">
                                <tbody align="left">
                                    <tr class="title-bg-lite">
                                        <td valign="middle" align="center" colspan="5">
                                            <div align="left">
                                                Parent Details 
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="50%" colspan="2">
                                            <b>Father's Details</b></td>
                                        <td
                                            colspan="2">
                                            <b>Mother's/Guardian's Details</b></td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Name</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfathername" runat="server"></asp:Label>&nbsp;
                                        </td>
                                        <td
                                            width="13%">Name</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgname" runat="server"></asp:Label>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Nationality</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfathernationality" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="13%">Nationality</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgnationality" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Comm. PoB</td>
                                        <td width="36%">
                                            <asp:Label ID="lblfathercomppob" Text=" " runat="server"></asp:Label><asp:Label ID="lblcompanypob"
                                                runat="server"></asp:Label></td>
                                        <td
                                            width="13%">Comm. PoB</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgcommpob" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">City/Emirate</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfatheremirate" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="13%">City/Emirate</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgemirates" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Phone Res.</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfatherphoneres" runat="server"></asp:Label>&nbsp;&nbsp;
                                        </td>
                                        <td
                                            width="13%">Phone Res.</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgphoneres" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Office Phone.</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfatherofficephone" runat="server"></asp:Label>&nbsp;
                                        </td>
                                        <td
                                            width="13%">Office Phone.</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgofficephone" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Mobile</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfathermobile" runat="server"></asp:Label>&nbsp;&nbsp;
                                        </td>
                                        <td
                                            width="13%">Mobile</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgmobile" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Email</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfatheremail" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="13%">Email</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgemail" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Fax</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfatherfax" runat="server"></asp:Label>&nbsp;&nbsp;
                                        </td>
                                        <td
                                            width="13%">Fax</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgfax" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Occupation</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfatheroccupation" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="13%">Occupation</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgoccupation" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Company</td>
                                        <td
                                            width="36%">
                                            <asp:Label ID="lblfathercompany" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="13%">Company</td>
                                        <td
                                            width="32%">
                                            <asp:Label ID="lblmgcompany" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: 1px; margin-bottom: 0px">
                                &nbsp;
                            </p>
                            <table id="table10" cellspacing="0" cellpadding="0" width="100%"
                                border="0" class="table table-bordered table-row">
                                <tbody align="left">
                                    <tr>
                                        <td class="matters" colspan="2">
                                            <b>Primary Contact:</b></td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="88%" colspan="2">
                                            <b>Permanent Address of Primary Contact</b></td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="20%">Address 1</td>
                                        <td
                                            width="75%">
                                            <asp:Label ID="lblprimaryaddress" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="20%">City, State</td>
                                        <td
                                            width="75%">
                                            <asp:Label ID="lblprimarycitystate" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="20%">Country</td>
                                        <td
                                            width="75%">
                                            <asp:Label ID="lblprimarycountry" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: 1px; margin-bottom: 0px">
                                &nbsp;
                            </p>
                            <table id="table6" cellspacing="0" cellpadding="0" width="100%"
                                border="0" class="table table-bordered table-row">
                                <tbody align="left">
                                    <tr class="title-bg-lite">
                                        <td valign="middle" align="center" colspan="5">
                                            <div align="left">
                                                Other Details 
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <b>Sibling</b><b> Details</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <asp:GridView ID="GridSiblings" Width="100%" AutoGenerateColumns="false" runat="server" CssClass="table table-bordered table-row">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Name">
                                                        <ItemTemplate>

                                                            <%# Eval("STU_FIRSTNAME") %> &nbsp; <%# Eval("STU_LASTNAME") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Relation">
                                                        <ItemTemplate>

                                                            <%# Eval("relation") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="School">
                                                        <ItemTemplate>

                                                            <%# Eval("school") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="GEMS School SEN">
                                                        <ItemTemplate>

                                                            <%# Eval("STU_NO") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <%--<tr>
                                    <td class="matters" style="padding-right: 4px; padding-left: 4px" bordercolor="#c0c0c0"
                                        width="34%">
                                        Name</td>
                                    <td class="matters" style="padding-right: 4px; padding-left: 4px" bordercolor="#c0c0c0"
                                        width="15%">
                                        Relation</td>
                                    <td class="matters" style="padding-right: 4px; padding-left: 4px" bordercolor="#c0c0c0"
                                        width="27%">
                                        School</td>
                                    <td class="matters" style="padding-right: 4px; padding-left: 4px" bordercolor="#c0c0c0"
                                        width="19%">
                                        GEMS School SEN</td>
                                </tr>
                                <tr>
                                    <td class="matters" style="padding-right: 4px; padding-left: 4px" bordercolor="#c0c0c0"
                                        width="34%">
                                        &nbsp;</td>
                                    <td class="matters" style="padding-right: 4px; padding-left: 4px" bordercolor="#c0c0c0"
                                        width="15%">
                                        &nbsp;</td>
                                    <td class="matters" style="padding-right: 4px; padding-left: 4px" bordercolor="#c0c0c0"
                                        width="27%">
                                        &nbsp;</td>
                                    <td class="matters" style="padding-right: 4px; padding-left: 4px" bordercolor="#c0c0c0"
                                        width="19%">
                                        &nbsp;</td>
                                </tr>--%>
                                </tbody>
                            </table>
                            <p style="margin-top: 1px; margin-bottom: 0px">
                                &nbsp;
                            </p>
                            <table id="table7" cellspacing="0" cellpadding="0" width="100%"
                                border="0" class="table table-bordered table-row">
                                <tbody align="left">
                                    <tr>
                                        <td colspan="5">
                                            <b>Previous School</b><b> Details</b></td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="24%">Previous School Name</td>
                                        <td
                                            width="5%">Class</td>
                                        <td
                                            width="8%">Program</td>
                                        <td
                                            width="28%">City/Country</td>
                                        <td
                                            width="14%">To Date</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="24%">
                                            <asp:Label ID="lblpevsname1" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="5%">
                                            <asp:Label ID="lblpevsclass1" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="8%">
                                            <asp:Label ID="lblpevsprogram1" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="28%">
                                            <asp:Label ID="lblpevscitycountry1" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="14%">
                                            <asp:Label ID="lblpevstodate1" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="24%">
                                            <asp:Label ID="lblpevsname2" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="5%">
                                            <asp:Label ID="lblpevsclass2" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="8%">
                                            <asp:Label ID="lblpevsprogram2" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="28%">
                                            <asp:Label ID="lblpevscitycountry2" runat="server"></asp:Label>&nbsp;&nbsp;</td>
                                        <td
                                            width="14%">
                                            <asp:Label ID="lblpevstodate2" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="24%">
                                            <asp:Label ID="lblpevsname3" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="5%">
                                            <asp:Label ID="lblpevsclass3" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="8%">
                                            <asp:Label ID="lblpevsprogram3" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="28%">
                                            <asp:Label ID="lblpevscitycountry3" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="14%">
                                            <asp:Label ID="lblpevstodate3" runat="server"></asp:Label>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table id="table8" cellspacing="0" cellpadding="0" width="100%"
                                border="0" class="table table-bordered table-row">
                                <tbody align="left">
                                    <tr>
                                        <td colspan="6">
                                            <b>Transport Details</b></td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="7%">Bus No</td>
                                        <td
                                            width="18%">
                                            <asp:Label ID="lblbusno" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="6%">Route</td>
                                        <td
                                            width="26%">
                                            <asp:Label ID="lblroute" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="7%">Stage</td>
                                        <td
                                            width="29%">
                                            <asp:Label ID="lblstage" runat="server"></asp:Label>&nbsp;&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: 1px; margin-bottom: 0px">
                                &nbsp;
                            </p>
                            <table id="table9" cellspacing="0" cellpadding="0" width="100%"
                                border="0" class="table table-bordered table-row">
                                <tbody align="left">
                                    <tr>
                                        <td colspan="5">
                                            <b>Medical History and Details</b></td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Health Card No</td>
                                        <td
                                            width="17%">Special Medication</td>
                                        <td
                                            width="10%">Blood Grp</td>
                                        <td
                                            width="21%">Sports Restriction</td>
                                        <td
                                            width="31%">Health Problems</td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">
                                            <asp:Label ID="lblhealthcardno" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="17%">
                                            <asp:Label ID="lblspecialmedication" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="10%">
                                            <asp:Label ID="lblbloodgroup" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="21%">
                                            <asp:Label ID="lblsportsrestriction" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="31%">
                                            <asp:Label ID="lblhealthproblems" runat="server"></asp:Label>&nbsp;&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p style="margin-top: 1px; margin-bottom: 0px">
                                &nbsp;
                            </p>
                            <table id="table11" cellspacing="0" cellpadding="0" width="100%"
                                border="0" class="table table-bordered table-row">
                                <tbody align="left">
                                    <tr>
                                        <td colspan="5">
                                            <b>Current Academic Details</b></td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">School House</td>
                                        <td
                                            width="27%">
                                            <asp:Label ID="lblhouse" runat="server"></asp:Label></td>
                                        <td
                                            width="20%" colspan="2">Name in Arabic</td>
                                        <td
                                            width="33%">
                                            <asp:Label ID="lblarabicname" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td
                                            width="14%">Form Tutor</td>
                                        <td
                                            width="27%">
                                            <asp:Label ID="lblformtutor" runat="server"></asp:Label>&nbsp;</td>
                                        <td
                                            width="20%" colspan="2">MOE Reg No</td>
                                        <td
                                            width="33%">
                                            <asp:Label ID="lblmoeregno" runat="server"></asp:Label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br>
        <asp:HiddenField ID="HiddenStudentId" runat="server" />
        <asp:HiddenField ID="HiddenBsuId" runat="server" />
    </form>
</body>
</html>
