Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studAcademicyear_D_View
    Inherits System.Web.UI.Page
    Dim menu_rights As Integer = 0
    Dim MainMnu_code As String = String.Empty
    Dim Encr_decrData As New Encryption64
    Dim str_Sql As String
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try



                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                MainMnu_code = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or MainMnu_code <> "S050005" Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, MainMnu_code)

                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                   
                    rbAll.Checked = True
                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String

       

        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

        str_Sid_img = h_Selected_menu_5.Value.Split("__")
        getid5(str_Sid_img(2))
       

    End Sub
    
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAcademic.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAcademic.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvAcademic.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAcademic.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid5(Optional ByVal p_imgsrc As String = "") As String
        If gvAcademic.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAcademic.HeaderRow.FindControl("mnu_5_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try

            Dim CurrentDatedType As String = String.Empty


            If rbCurrent.Checked = True Then
                CurrentDatedType = " And a.D_CURRENT=1"
            ElseIf rbDated.Checked = True Then
                CurrentDatedType = " And a.D_CURRENT=0"
            End If


            Dim ddlOPENONLINEH As New DropDownList

            Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
            Dim str_Sql As String = ""


            Dim str_filter_C_DESCR As String = String.Empty
           
            Dim str_filter_STARTDT As String = String.Empty
            Dim str_filter_ENDDT As String = String.Empty
            Dim str_filter_OPENONLINE As String = String.Empty

            Dim ds As New DataSet

            str_Sql = " SELECT  ACD_ID,Y_DESCR,C_DESCR,STARTDT,ENDDT,D_CURRENT,OPENONLINE,ACY_ID FROM(SELECT  ACADEMICYEAR_D.ACD_ID AS ACD_ID,ACADEMICYEAR_D.ACD_ACY_ID as ACY_ID, ACADEMICYEAR_M.ACY_DESCR as Y_DESCR,CURRICULUM_M.CLM_ID as CLM_ID, CURRICULUM_M.CLM_DESCR as C_DESCR, ACADEMICYEAR_D.ACD_BSU_ID AS BSU_ID, " & _
              "  ACADEMICYEAR_D.ACD_STARTDT AS STARTDT, ACADEMICYEAR_D.ACD_ENDDT AS ENDDT,ACADEMICYEAR_D.ACD_CURRENT aS D_CURRENT, ACADEMICYEAR_D.ACD_OPENONLINE AS OPENONLINE " & _
                    " FROM  ACADEMICYEAR_D INNER JOIN ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID INNER JOIN " & _
                     " CURRICULUM_M ON ACADEMICYEAR_D.ACD_CLM_ID = CURRICULUM_M.CLM_ID)A WHERE   A.BSU_ID='" & Session("sBsuid") & "' AND A.ACD_ID<>''"


            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String

            Dim str_C_DESCR As String = String.Empty
            Dim str_STARTDT As String = String.Empty
            Dim str_ENDDT As String = String.Empty
          

            If gvAcademic.Rows.Count > 0 Then

                Dim str_Sid_search() As String






                ddlOPENONLINEH = gvAcademic.HeaderRow.FindControl("ddlOPENONLINEH")

                If ddlOPENONLINEH.SelectedItem.Text = "No" Then
                    str_filter_OPENONLINE = " AND a.OPENONLINE=0"
                ElseIf ddlOPENONLINEH.SelectedItem.Text = "Yes" Then
                    str_filter_OPENONLINE = " AND a.OPENONLINE=1"

                End If



                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAcademic.HeaderRow.FindControl("txtC_DESCR")
                str_C_DESCR = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_C_DESCR = " AND a.C_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_C_DESCR = "  AND  NOT a.C_DESCR LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_C_DESCR = " AND a.C_DESCR  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_C_DESCR = " AND a.C_DESCR  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_C_DESCR = " AND a.C_DESCR LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_C_DESCR = " AND a.C_DESCR NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_4.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAcademic.HeaderRow.FindControl("txtSTARTDT")

                str_STARTDT = txtSearch.Text

                Dim StartDT As String = "  DATENAME(day,a.STARTDT)+'/'+left(DATENAME(month,a.STARTDT),3)+'/'+DATENAME(year,a.STARTDT)"


                If str_search = "LI" Then
                    str_filter_STARTDT = " AND " & StartDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_STARTDT = "  AND  NOT " & StartDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_STARTDT = " AND " & StartDT & " LIKE  '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_STARTDT = " AND " & StartDT & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_STARTDT = " AND " & StartDT & " LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_STARTDT = " AND " & StartDT & " NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_5.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAcademic.HeaderRow.FindControl("txtENDDT")

                str_ENDDT = txtSearch.Text

                Dim ENDDT As String = "  DATENAME(day,a.ENDDT)+'/'+left(DATENAME(month,a.ENDDT),3)+'/'+DATENAME(year,a.ENDDT)"

                If str_search = "LI" Then
                    str_filter_ENDDT = " AND  " & ENDDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_ENDDT = "  AND  NOT " & ENDDT & " LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_ENDDT = " AND " & ENDDT & "  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_ENDDT = " AND " & ENDDT & " NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_ENDDT = " AND " & ENDDT & "  LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_ENDDT = " AND " & ENDDT & " NOT LIKE '%" & txtSearch.Text & "'"
                End If

            End If



            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & ViewState("str_filter_Year") & str_filter_C_DESCR & str_filter_STARTDT & str_filter_ENDDT & CurrentDatedType & str_filter_OPENONLINE & "  order by  a.ACY_ID desc")

            If ds.Tables(0).Rows.Count > 0 Then

                gvAcademic.DataSource = ds.Tables(0)
                gvAcademic.DataBind()
                
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True

                gvAcademic.DataSource = ds.Tables(0)
                Try
                    gvAcademic.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAcademic.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAcademic.Rows(0).Cells.Clear()
                gvAcademic.Rows(0).Cells.Add(New TableCell)
                gvAcademic.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAcademic.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAcademic.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            txtSearch = gvAcademic.HeaderRow.FindControl("txtC_DESCR")
            txtSearch.Text = str_C_DESCR
            txtSearch = gvAcademic.HeaderRow.FindControl("txtSTARTDT")
            txtSearch.Text = str_STARTDT
            txtSearch = gvAcademic.HeaderRow.FindControl("txtENDDT")
            txtSearch.Text = str_ENDDT


            Call callYEAR_DESCRBind()

            Call ddlOpenOnLine_state(ddlOPENONLINEH.SelectedItem.Text)

            set_Menu_Img()

            


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub


    Sub ddlOpenOnLine_state(ByVal selText As String)
        Try

        
            Dim ItemTypeCounter As Integer
            Dim ddlOpenonLineH As New DropDownList
            ddlOpenonLineH = gvAcademic.HeaderRow.FindControl("ddlOPENONLINEH")
            For ItemTypeCounter = 0 To ddlOpenonLineH.Items.Count - 1
                'keep loop until you get the counter for default OPENONLINE into  the SelectedIndex

                If ddlOpenonLineH.Items(ItemTypeCounter).Text = selText Then
                    ddlOpenonLineH.SelectedIndex = ItemTypeCounter
                End If
            Next
            '  ddlOpenonLineH.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlOPENONLINEH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'drop down list for Reg OPENONLINE
        ' Call ddlOpenOnLine_state(sender.selectedItem.Text)
        gridbind()

    End Sub
    Protected Sub ddlACY_DESCRH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ViewState("Y_DESCR") = sender.SelectedItem.Text
        callYEAR_DESCRBind(ViewState("Y_DESCR"))
        gridbind()
    End Sub
    Public Sub callYEAR_DESCRBind(Optional ByVal p_selected As String = "All")

        Try

            Dim ddlY_DESCRH As New DropDownList
            ddlY_DESCRH = gvAcademic.HeaderRow.FindControl("ddlY_DESCRH")

            Dim di As ListItem
            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(Session("sBsuid"), Session("CLM"))

                ddlY_DESCRH.Items.Clear()
                di = New ListItem("All", "All")
                ddlY_DESCRH.Items.Add(di)
                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACY_ID"))
                        ddlY_DESCRH.Items.Add(di)

                    End While
                End If
            End Using

            If p_selected <> "All" Then
                ViewState("str_filter_Year") = " AND a.Y_DESCR = '" & p_selected & "'"
            Else
                ViewState("str_filter_Year") = " AND a.Y_DESCR <>''"
            End If
            Dim ItemTypeCounter As Integer = 0
            For ItemTypeCounter = 0 To ddlY_DESCRH.Items.Count - 1
                'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                If Not ViewState("Y_DESCR") Is Nothing Then
                    If ddlY_DESCRH.Items(ItemTypeCounter).Text = ViewState("Y_DESCR") Then
                        ddlY_DESCRH.SelectedIndex = ItemTypeCounter
                    End If
                End If

            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Protected Sub lbView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lblACD_ID As New Label
            Dim url As String
            Dim viewid As String

            lblACD_ID = TryCast(sender.FindControl("lblACD_ID"), Label)
            viewid = lblACD_ID.Text


            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String

            MainMnu_code = Request.QueryString("MainMnu_code")

            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studAcademicyear_D.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", MainMnu_code, ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try

    End Sub

    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddNew.Click
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            MainMnu_code = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studAcademicyear_D.aspx?MainMnu_code={0}&datamode={1}", MainMnu_code, ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub

   
    
    Protected Sub rbCurrent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCurrent.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbDated_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbDated.CheckedChanged
        gridbind()
    End Sub

    Protected Sub rbAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbAll.CheckedChanged
        gridbind()
    End Sub

    Protected Sub btnSearchC_DESCR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchSTARTDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchENDDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

  
    Protected Sub gvAcademic_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAcademic.PageIndexChanging
        gvAcademic.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
End Class
