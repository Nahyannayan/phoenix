Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studStrikeOffCancelRequest_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100253") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"
                    hfSTU_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stuid").Replace(" ", "+"))
                    hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acdid").Replace(" ", "+"))
                    txtName.Text = Encr_decrData.Decrypt(Request.QueryString("stuname").Replace(" ", "+"))
                    txtSEN.Text = Encr_decrData.Decrypt(Request.QueryString("stuno").Replace(" ", "+"))
                    GetData()

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try


        Else


        End If
    End Sub
#Region "Private Methods"
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCM_ID,STK_LASTATTDATE,isNULL(STK_RECREMARKS,'') as STK_RECREMARKS,STK_RECDATE," _
                                 & " isnull(STK_APRREMARKS,''),STK_APRDATE,ISNULL(TCM_CANCELREQREMARKS,''),ISNULL(TCM_CANCELREQDATE,'01/jan/1900'),  " _
                                 & " isNULL(TCM_REMARKS,'') as TCM_REMARKS,TCM_LEAVEDATE,STK_ID FROM" _
                                 & " STRIKEOFF_RECOMMEND_M AS A INNER JOIN TCM_M AS B ON A.STK_STU_ID=B.TCM_STU_ID" _
                                 & " WHERE TCM_TCSO='SO' AND  STK_STU_ID = " + hfSTU_ID.Value _
                                 & " AND STK_bAPPROVED='TRUE' AND STK_CANCELDATE IS NULL AND TCM_CANCELDATE IS NULL"

        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            hfTCM_ID.Value = reader.GetValue(0)
            txtLast.Text = Format(reader.GetDateTime(1), "dd/MMM/yyyy")
            txtRecRemarks.Text = reader.GetString(2).Replace("''", "'")
            txtRec.Text = Format(reader.GetDateTime(3), "dd/MMM/yyyy")
            txtAprRemarks.Text = reader.GetString(4).Replace("''", "'")
            txtApr.Text = Format(reader.GetDateTime(5), "dd/MMM/yyyy")
            txtRemarks.Text = reader.GetString(6).Replace("''", "'")
            txtDate.Text = Format(reader.GetDateTime(7), "dd/MMM/yyyy").Replace("01/Jan/1900", "")
            txtStrikeRemarks.Text = reader.GetString(8).Replace("''", "'")
            txtStrike.Text = Format(reader.GetDateTime(9), "dd/MMM/yyyy")
            HF_STK_ID.Value = reader.GetValue(10)
        End While
        reader.Close()
    End Sub

    Function CheckStrikeOffDate() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM ACADEMICYEAR_D WHERE ACD_ID=" + hfACD_ID.Value + " AND '" + txtStrike.Text + "' BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            lblError.Text = "The strikeoff date should be within the academic year"
            Return False
        Else

            If DateDiff("d", Date.Parse(txtStrike.Text), Date.Parse(txtLast.Text)) > 0 Then
                lblError.Text = "Strike of date has to be a date higher than the last attendance date"
                Return False
            End If
            Return True
        End If

    End Function
    'Sub GetDataFROMTCM()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim str_query As String = "SELECT TCM_ID FROM TCM_M WHERE TCM_STU_ID=" & hfSTU_ID.Value & " AND TCM_TCSO='SO'AND TCM_bAPPROVED=0  AND TCM_CANCELDATE IS NULL"

    '    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
    '    If reader.HasRows Then
    '        While reader.Read
    '            hfTCM_ID.Value = reader.GetValue(0)
    '            'txtLast.Text = Format(reader.GetDateTime(1), "dd/MMM/yyyy")
    '            'txtRemarks.Text = reader.GetString(2).Replace("''", "'")
    '            'txtRec.Text = Format(reader.GetDateTime(3), "dd/MMM/yyyy")
    '            'txtRecRemarks.Text = reader.GetString(4).Replace("''", "'")
    '        End While
    '    Else
    '        HF_TCM_ID.Value = 0

    '    End If

    '    reader.Close()
    'End Sub


    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = " EXEC saveSTRIKEOFFCANCELREQUEST " _
                                 & hfTCM_ID.Value + "," _
                                 & "'" + Format(Date.Parse(txtDate.Text), "yyyy-MM-dd") + "'," _
                                 & "'" + txtRemarks.Text + "'"
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hfSTU_ID.Value.ToString)
                If hfTCM_ID.Value > 0 Then
                    If HF_STK_ID.Value > 0 Then
                        UtilityObj.InsertAuditdetails(transaction, "edit", "STRIKEOFF_RECOMMEND_M", "STK_ID", "STK_STU_ID", "STK_ID=" + HF_STK_ID.Value.ToString, "SO_CANCEL_REQUEST")
                    End If
                    UtilityObj.InsertAuditdetails(transaction, "edit", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + hfTCM_ID.Value.ToString)
                End If

                SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"
                Exit Sub
            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Exit Sub
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                Exit Sub
            End Try
        End Using


    End Sub


#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If CheckStrikeOffDate() = False Then
        '    Exit Sub
        'End If
        SaveData()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

End Class
