﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studChangeCritical.aspx.vb" Inherits="Students_studChangeCritical" title="Untitled Page" %>
    
       
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript" src="../../../chromejs/chrome.js"></script>
     <script language="javascript" type="text/javascript">
         
                

var color = ''; 
function highlight(obj)
{ 
var rowObject = getParentRow(obj); 
var parentTable = document.getElementById("<%=GrdView.ClientID %>"); 
if(color == '') 
{
color = getRowColor(); 
} 
if(obj.checked) 
{ 
rowObject.style.backgroundColor ='#f6deb2'; 
}
else 
{
rowObject.style.backgroundColor = '';  
color = ''; 
}
// private method

function getRowColor() 
{
if(rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor; 
else return rowObject.style.backgroundColor; 
}
}
// This method returns the parent row of the object
function getParentRow(obj) 
{  
do 
{
obj = obj.parentElement;
}
while(obj.tagName != "TR") 
return obj; 
}


    function change_chk_state(chkThis)
         {
        var chk_state= ! chkThis.checked ;
         for(i=0; i<document.forms[0].elements.length; i++)
               {
               var currentid =document.forms[0].elements[i].id; 
               if(document.forms[0].elements[i].type=="checkbox" && currentid.indexOf("chkSelect")!=-1)
             {
               //if (document.forms[0].elements[i].type=='checkbox' )
                  //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked=chk_state;
                     document.forms[0].elements[i].click();//fire the click event of the child element
                 }
              }
          }
        
        function correctingdate(len)
            {
           
                  if(len[1]==01)
                  {
                   document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Jan/'+len[2]     
                   return
                  }  
                  else if(len[1]==02)
                  {
                   document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Feb/'+len[2]     
                   return
                  }  
                  else if(len[1]==03)
                  {
                   document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Mar/'+len[2]     
                   return
                  }
                  else if(len[1]==04)
                  {
                   document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Apr/'+len[2]     
                   return
                  }
                  else if(len[1]==05)
                  {
                   document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/May/'+len[2]     
                   return
                  }
                   else if(len[1]==06)
                 {
                  document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Jun/'+len[2]     
                  return
                 }
                 else if(len[1]==07)
                 {
                  document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Jul/'+len[2]     
                  return
                 }
                 else if(len[1]==08)
                 {
                  document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Aug/'+len[2]     
                  return
                 }
                  else if(len[1]==09)
                 {
                  document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Sep/'+len[2]     
                  return
                 }
                 else if(len[1]==10)
                 {
                  document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Oct/'+len[2]     
                  return
                 }
                 else if(len[1]==11)
                 {
                  document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Nov/'+len[2]     
                  return
                 }
                 else if(len[1]==12)
                 {
                  document.getElementById('<%=txtTransDate.ClientID %>').value=len[0]+'/Dec/'+len[2]     
                  return
                 }                      
            }
          function valDate()
          {
            var date=document.getElementById('<%=txtTransDate.ClientID %>').value 
            if(date.search('/')==2)
            {
                var len=date.split('/')  
            }       
            else if(date.search('-')==2)
            {
                var len=date.split('-')  
            } 
            else
            {
                alert('Date format is like (eg 01/jan/2010)')
                return false
            }                     
            if(len[1]>12)
            {
                alert('Date format is like (eg 01/jan/2010)')
                return false
            }  
            correctingdate(len)
            return true   
          }      
      function validateOrFields(source, args)
      {
      var aa= valDate() 
//      alert(aa)
          if(aa==false)
          {    
              args.IsValid = false;         
          }
           else
           {
              args.IsValid = true;         
           }  
       return;  
     } 
  </script>

        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user"></i>  Edit Critical Data
        </div>
        <div class="card-body">
            <div class="table-responsive">

            
             <table width="100%" align="center">
                            
                     <tr>
            <td colspan="2" class="title-bg">
                Change From </td>
                         <td></td>
            <td colspan="2" class="title-bg">
                Change To</td>
        </tr>
                    
        <tr >
            <td align="left" width="20%">
                <span class="field-label">Academic Year</span></td>
            
            <td align="left" width="25%">
                <asp:DropDownList ID="ddlFAcademicYear" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td></td>
            <td align="left" width="20%">
                <span class="field-label">Academic Year</span></td>
            <td align="left" width="25%">
                <asp:DropDownList ID="ddlTAcademicYear" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr >
            <td align="left">
                <span class="field-label">Shift</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlFshift" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td></td>
            <td align="left">
                <span class="field-label">Shift</span></td>
            <td align="left">
                <asp:DropDownList ID="ddlTshift" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr >
            <td align="left">
                <span class="field-label">Stream</span></td>
            
            <td align="left">
                <asp:DropDownList ID="ddlFStream" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td></td>
            <td align="left">
                <span class="field-label">Stream</span></td>
            <td align="left">
                <asp:DropDownList ID="ddlTstream" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr >
            <td align="left">
                <span class="field-label">Grade</span></td>
           
            <td align="left">
                <asp:DropDownList ID="ddlFgrade" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td></td>
            <td align="left">
                <span class="field-label">Grade</span></td>
            <td align="left">
                <asp:DropDownList ID="ddlTgrade" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr >
            <td align="left">
                <span class="field-label">Section</span></td>
           
            <td align="left">
                <asp:DropDownList ID="ddlFsection" runat="server" >
                </asp:DropDownList>
            </td>
            <td></td>
            <td align="left">
                <span class="field-label">Section</span></td>
            <td align="left">
                <asp:DropDownList ID="ddlTsection" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr >
            <td></td>
            <td></td>
            <td></td>
            <td align="left">
                
                            <span class="field-label">Change DOJ</span> </td>
           
            <td align="left">
                <asp:TextBox ID="txtTransDate" runat="server"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="txtTransDate_CalendarExtender" runat="server" 
                    Enabled="True" PopupButtonID="imgCalendar" TargetControlID="txtTransDate" 
                    Format="dd/MMM/yyyy">
                </ajaxToolkit:CalendarExtender>
                <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif" />
                          
                      <asp:CustomValidator ID="CustomValidator1" runat="server" 
                    ErrorMessage="Not a valid Date" ClientValidationFunction="validateOrFields" 
                    ControlToValidate="txtTransDate" ValidationGroup="trans"></asp:CustomValidator>
            </td>
        </tr>
        
         <tr >
            <td colspan="5" align="center">
                <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search Student"
                    ValidationGroup="dayBook" /></td>
        
        </tr>
        
        
        
        <tr >
            <td align="left" colspan="5">
                <asp:Label ID="lblError" runat="server" CssClass="error" 
                    EnableViewState="False"></asp:Label>
                </td>
        </tr>
        <tr class="BlueTableView">
            <td align="center" colspan="5">
                <asp:Panel ID="Panel1" runat="server"  ScrollBars="Auto" 
                    Width="100%" CssClass="BlueTableView">
                    <asp:GridView ID="GrdView" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                       PageSize="20" Width="100%" CssClass="table table-bordered table-row" >
                        <Columns>
                            <asp:TemplateField HeaderText="Select">
                                <EditItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server"  />
                                </EditItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle Wrap="False" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect0" runat="server" 
                                        onclick="javascript:highlight(this);" />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Select<br />
                                    <asp:CheckBox ID="chkAll" runat="server"  onclick="javascript:change_chk_state(this);"
                     ToolTip="Click here to select/deselect all rows" />
                                            
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <%-- text='<%# getSerialNo() %>'--%>
                            <asp:TemplateField HeaderText="SL.No"  >
                                <ItemTemplate>
                                    <asp:Label ID="lblSlNo" runat="server" Text='<%# Bind("SINO") %>' ></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student No">
                                <HeaderTemplate>
                                    <asp:Label ID="lblFeeHeader" runat="server" CssClass="gridheader_text" 
                                    Text="Stud. No"></asp:Label>
                                    <br>
                                    
                                    <asp:TextBox ID="txtFeeSearch" runat="server" Width="75%"></asp:TextBox>
                                    <asp:ImageButton ID="btnFeeId_Search" runat="server" ImageAlign="middle" 
                                       ImageUrl="~/Images/forum_search.gif" OnClick="btnFeeId_Search_Click" />
                                                           
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblFeeId" runat="server" text='<%# Bind("STU_NO") %>'></asp:Label>
                                    <asp:HiddenField ID="HF_stu_id" runat="server" Value='<%# Bind("STU_ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student Name">
                                <HeaderTemplate>
                                  <asp:Label ID="lblName" runat="server" CssClass="gridheader_text" 
                                           Text="Student Name"></asp:Label>
                                  <br />
                                  <asp:TextBox ID="txtStudName" runat="server" Width="75%"></asp:TextBox>
                                  <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="middle" 
                                       ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                                            
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEnqDate" runat="server" text='<%# Bind("STU_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Grade">
                                <HeaderTemplate>
                                        Grade  
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblGrade" runat="server" text='<%# Bind("grm_display") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Section">
                                <HeaderTemplate>
                                     Section
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblSection" runat="server" text='<%# Bind("SCT_DESCR") %>'></asp:Label>
                                    <asp:HiddenField ID="HF_sct_id" runat="server" 
                                        Value='<%# Bind("STU_SCT_ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Stu_gender" HeaderText="Gender">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="gridheader_pop" />
                        <RowStyle CssClass="griditem" />
                        <SelectedRowStyle CssClass="Green" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </asp:Panel>
            </td>
       </tr>
       
          <tr>
                        <td align="left" >
                            <span class="field-label">Remarks</span></td>
                        <td align="left" colspan="3">
                      <asp:TextBox id="txtRemarks" runat="server" Width="238px" TextMode="MultiLine" Height="67px" SkinID="MultiText" MaxLength="100"></asp:TextBox>
                      </td>        
                  
                       </tr>
       
       
        <tr >
            <td colspan="5" align="center">
                <asp:Button ID="btnTransfer" runat="server" CssClass="button" Text="Request Change" 
                    ValidationGroup="trans" /> <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
            </td>
       </tr>
  
                 </table>
            <ajaxToolkit:ModalPopupExtender ID="Modalpopupconfirm" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="btn_no" DropShadow="True" PopupControlID="PanelTitleAdd" 
                    TargetControlID="lblmodalpopup">
                </ajaxToolkit:ModalPopupExtender>
                 <asp:Label id="lblmodalpopup" runat="server" ForeColor="White"></asp:Label>
                <asp:Panel id="PanelTitleAdd" runat="server" CssClass="panel-cover"
                    style="display: none;width: 350px ;height:142px;vertical-align:middle "><table width="100%">
                <tr>
                    <td  colspan="3">
                        <asp:Label ID="lbltransfer" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    
                    <td colspan="4" align="center">
                        <asp:Button ID="btnYES" runat="server" CausesValidation="False" 
                            CssClass="button" onclick="btnYES_Click" Text="Yes" />
                            <asp:Button ID="btn_no" runat="server" CssClass="button" Text="No" />
                    </td>
                    
                </tr>
            </table></asp:Panel>
                
    
                </div>
            </div>
        </div>

</asp:Content>

