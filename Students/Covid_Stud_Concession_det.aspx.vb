﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_Covid_Stud_Concession_det
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            ViewState("STU_ID") = 0
            If Request.QueryString("id") IsNot Nothing AndAlso Request.QueryString("id").Trim <> "" Then
                ViewState("STU_ID") = Request.QueryString("id").Replace(" ", "+")
            End If
            bind_covid_STATUS(Session("sBsuid"))
            gridbind()
            txtExpDate.Text = Format(CDate(Convert.ToString(Now.AddDays(1))), "dd/MMM/yyyy")
        End If

    End Sub
    Public Sub bind_covid_STATUS(ByVal id As String)
        Dim s As String = ""
        ddlStatus.Items.Clear()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", id)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "cov.admin_status", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlStatus.DataSource = ds
            ddlStatus.DataValueField = "ID"
            ddlStatus.DataTextField = "STATUS"
            ddlStatus.DataBind()
        End If
        ddlStatus.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Public Sub gridbind()
       
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", ViewState("STU_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.SP_GET_STATUS_CHANGE_ADMIN_LOG", param)

        If ds.Tables(0).Rows.Count > 0 Then
            gvAdmin.DataSource = ds
           
            gvAdmin.DataBind()
        End If

    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        lblStatus.Text = ""
        lblComment.Text = ""
        lblExpDate.Text = ""
        If ddlStatus.SelectedValue = 0 Then
            lblStatus.Text = "Please select status"
            Exit Sub
        End If
        If txtExpDate.Text = "" Then
            lblExpDate.Text = "Please enter expiry date"
            Exit Sub
        End If
        If txtComment.Text = "" Then
            lblComment.Text = "Please enter comment"
            Exit Sub
        End If
        

        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction

        sqltran = con.BeginTransaction("trans")
        Try
            Dim result As String = SAVE_Admin(sqltran)


            If result = "" Then
                sqltran.Commit()
                lblError.Text = "Successfully Saved"
                gridbind()
            Else
                sqltran.Rollback()
                lblError.Text = result
            End If


            lblError.Text = "Successfully Saved"


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "covid Admin")
            lblError.Text = "Request could not be processed"
            sqltran.Rollback()
        End Try

    End Sub
    Private Function SAVE_Admin(sqltran As SqlTransaction) As String
        Dim pParm(7) As SqlClient.SqlParameter

        pParm(0) = New SqlClient.SqlParameter("@SCA_BSU_ID", Session("sBsuid"))
        pParm(1) = New SqlClient.SqlParameter("@SCA_STU_ID", ViewState("STU_ID"))
        pParm(2) = New SqlClient.SqlParameter("@SCA_STATUS", ddlStatus.SelectedValue)
        pParm(3) = New SqlClient.SqlParameter("@SCA_VALIDTILL", txtExpDate.Text)
        pParm(4) = New SqlClient.SqlParameter("@SCA_REMARKS", txtComment.Text)
        pParm(5) = New SqlClient.SqlParameter("@SCA_LOGUSER", Session("sUsr_name"))
        pParm(6) = New SqlClient.SqlParameter("@RESULT", SqlDbType.VarChar, 500)
        pParm(6).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[COV].[SAVE_STATUS_CHANGE_ADMIN]", pParm)

        Dim RETVAL As String = Convert.ToString(pParm(6).Value)

        Return RETVAL
    End Function
End Class
