<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studRegOfferCancel_M.aspx.vb" Inherits="Students_studRegOfferCancel_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function getDate(val) {
            var sFeatures;
            sFeatures = "dialogWidth: 227px; ";
            sFeatures += "dialogHeight: 252px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var nofuture = "../Accounts/calendar.aspx?nofuture=yes";
            if (val == 0) {
                result = window.showModalDialog(nofuture, "", sFeatures)

            }
            else {
                result = window.showModalDialog("../Accounts/calendar.aspx", "", sFeatures)
            }
            if (result == '' || result == undefined) {
                return false;
            }


            if (val == 1) {
                document.getElementById('<%=txtCancelDate.ClientID %>').value = result;
            }


            return false;
        }

        function confirm_cancel() {
            var mode = document.getElementById('<%=hfMode.ClientID %>');
     if (mode.value == 0) {
         if (confirm("You are about to cancel this registration.Do you wish to proceed?") == true)
             return true;
         else
             return false;
     }
     else {
         if (confirm("You are about to cancel this Offer.Do you wish to proceed?") == true)
             return true;
         else
             return false;
     }
 }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i> <asp:Label ID="lblCancel" runat="server" Text="Label"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:"
                                ValidationGroup="groupM1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCancelDate"
                                Display="None" ErrorMessage="Please enter the date" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <%--<tr>
                                    <td align="left" colspan="4">

                                       
                                    </td>
                                </tr>--%>


                                <tr>

                                    <td align="left" width="20%">
                                        <asp:Label ID="lblShift" runat="server" Text="Date" CssClass="field-label"></asp:Label></td>


                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCancelDate" runat="server"></asp:TextBox>
                                      <%--  <asp:ImageButton ID="imgBtnEnqDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            OnClientClick="getDate(1);return false;" /><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCancelDate"
                                                Display="Dynamic" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtCancelDate"
                                                     Display="Dynamic" EnableViewState="False" ErrorMessage="Date entered is not a valid date"
                                                    ValidationGroup="groupM1">*</asp:CustomValidator>--%>
                                          <asp:ImageButton ID="imgBtnCancel_date" runat="server"
                                                            ImageUrl="~/Images/calendar.gif"></asp:ImageButton>

                                    </td>
                                    <td align="left" colspan="2" width="50%"></td>

                                </tr>
                                <tr>
                                    <td align="left" class="matters">
                                        <span class="field-label">Remarks</span></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtRemarks" runat="server" Width="238px" TextMode="MultiLine" Height="67px" SkinID="MultiText"></asp:TextBox>
                                    </td>
                                         <td align="left" colspan="2" width="50%"></td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>

                            <asp:Button ID="btnUpdate" OnClientClick="javascript:return confirm_cancel();" runat="server" CssClass="button" Text="Update" ValidationGroup="groupM1" CausesValidation="False" />&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks"
                                Display="None" ErrorMessage="Please enter data in the field remarks" ValidationGroup="groupM1"
                                Width="23px"></asp:RequiredFieldValidator>&nbsp;
                <asp:HiddenField ID="hfEQS_ID" runat="server" />
                            <asp:HiddenField ID="hfMode" runat="server" />
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>

                 <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                    PopupButtonID="imgBtnCancel_date" TargetControlID="txtCancelDate">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>

</asp:Content>

