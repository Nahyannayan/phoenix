﻿<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAtt_travelled_absent_edit.aspx.vb" Inherits="Students_studAtt_travelled_absent_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">

        function ShowHDetails(url) {
            var sFeatures;
            sFeatures = "dialogWidth: 370px; ";
            sFeatures += "dialogHeight: 300px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var result;


            result = window.showModalDialog(url, "", sFeatures)
            if (result == '' || result == undefined) {
                return false;
            }

            return false;
        }
        function onCancel() {
            // var postBack = new Sys.WebForms.PostBackAction(); 
            //postBack.set_target('CancelButton'); 
            //postBack.set_eventArgument(''); 
            // postBack.performAction();
        }

        function onOk() {
            var postBack = new Sys.WebForms.PostBackAction();
            postBack.set_target('okButton');
            postBack.set_eventArgument('');
            postBack.performAction();
        }



        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }



    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="Literal1" runat="server" Text="Mail For Travelled in Bus & Absent in School"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <%--                <table id="Table1" border="0" width="100%">
                    <tr style="font-size: 12pt">
                        <td align="left" class="title" style="width: 48%">MAIL FOR TRAVELLED IN BUS & ABSENT IN SCHOOL</td>
                    </tr>
                </table>--%>


                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <span style="float: left; display: block;">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False" />
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                        ValidationGroup="AttGroup" DisplayMode="List" />
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr class="title-bg-lite">
                                    <td align="left" colspan="4">Setting</td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">School</span></td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlBSU_ID" runat="server" AutoPostBack="True">
                                        </asp:DropDownList></td>

                                    <td align="left" width="20%"></td>
                                    <td align="left" width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span><font color="red" class="error">*</font></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal1" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                            Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator></td>
                                    <td align="left" width="20%"><span class="field-label">To Date</span><font color="red" class="error">*</font></td>

                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="imgCal2" runat="server"
                                            ImageUrl="~/Images/calendar.gif" />
                                        <asp:RequiredFieldValidator ID="rfvTo" runat="server" ControlToValidate="txtTo" Display="Dynamic"
                                            ErrorMessage="To Date required" ValidationGroup="AttGroup" CssClass="error" ForeColor="">*</asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Schedule</span><font color="red" class="error">*</font></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSCHEDULE" runat="server">
                                            <asp:ListItem Value="8">8 AM</asp:ListItem>
                                            <asp:ListItem Value="9">9 AM</asp:ListItem>
                                            <asp:ListItem Value="10" Selected="True">10 AM</asp:ListItem>
                                            <asp:ListItem Value="11">11 AM</asp:ListItem>
                                            <asp:ListItem Value="12">12 AM</asp:ListItem>
                                            <asp:ListItem Value="16">4 PM</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left"></td>
                                    <td align="left"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">Notify Pending Attendance To</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlEMP" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>

                                    </td>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnAddGrd" runat="server" CssClass="button" Text="Add" />
                                        <asp:Button ID="btnUpdateGrd" runat="server" CssClass="button" Text="Update" />
                                        <asp:Button ID="btnCancelGRD" runat="server" CssClass="button" Text="Cancel" />

                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" colspan="4">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <div>
                                <asp:Label ID="lblError_grid" runat="server" CssClass="error"
                                    EnableViewState="False"></asp:Label>
                                <asp:GridView ID="gvEMP_NAME" runat="server" AutoGenerateColumns="False"
                                    DataKeyNames="ID" CssClass="table table-bordered table-row"
                                    EmptyDataText="No items added yet"
                                    Width="100%">
                                    <RowStyle CssClass="griditem" />
                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNMPA_EMP_NAME" runat="server" Text='<%# Bind("NMTA_EMP_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="EditBtn" runat="server" CausesValidation="False" CommandArgument='<%# Eval("id") %>' CommandName="Edit">           
    Edit </asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="DeleteBtn" runat="server" CommandArgument='<%# Eval("id") %>' CommandName="Delete">
         Delete</asp:LinkButton>
                                            </ItemTemplate>

                                            <HeaderStyle />

                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NMPA_ID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNMPA_ID" runat="server" Text='<%# Bind("NMTA_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="STATUS" Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("STATUS") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSTATUS" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NMPA_MPA_ID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNMPA_MPA_ID" runat="server" Text='<%# Bind("NMTA_MTA_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NMPA_EMP_ID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNMPA_EMP_ID" runat="server" Text='<%# Bind("NMTA_EMP_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle BackColor="Khaki" />
                                    <HeaderStyle />
                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                Text="Add" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                                Text="Edit" OnClick="btnEdit_Click" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                                ValidationGroup="AttGroup" />
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />&nbsp;
                <asp:HiddenField ID="hfNMPA_ID" runat="server" />
                        </td>
                    </tr>
                </table>

                <ajaxToolkit:CalendarExtender ID="cal1" runat="server" TargetControlID="txtfrom" Format="dd/MMM/yyyy" PopupButtonID="imgcal1">
                </ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="Cal2" runat="server" TargetControlID="txtTo" Format="dd/MMM/yyyy" PopupButtonID="imgcal2">
                </ajaxToolkit:CalendarExtender>

            </div>
        </div>
    </div>


</asp:Content>

