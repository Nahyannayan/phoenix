<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudVATT_M_GROUP_VIEW.aspx.vb" Inherits="Students_StudStaffAuthorized_Grade_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Vertical Attendance Group"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table width="100%">
                    <tr>
                        <td align="left" colspan="4"   valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top" >
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <table id="tbl_test" runat="server" width="100%">
                                <tr class="matters">
                                    <td align="left"   valign="middle"  width="20%" ><span class="field-label">Select Academic year</span> </td>
                                    <td  width="30%"><asp:DropDownList ID="ddlAca_Year" runat="server"
                                        AutoPostBack="True"  OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                                    </asp:DropDownList></td>
                                    <td  width="20%">
                                    </td>
                                    <td  width="30%"></td>
                                </tr>
                                <tr  >
                                    <td align="left" class="matters" colspan="4" valign="top">
                                        <asp:GridView ID="gvAuthorizedRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Group">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                            <asp:Label ID="lblGROUPNAMEH" runat="server" Text="Group Name" CssClass="gridheader_text" __designer:wfdid="w3"></asp:Label> <br />
                            <asp:TextBox ID="txtGROUP_NAME" runat="server" ></asp:TextBox> 
                            <asp:ImageButton ID="btnSearchGROUP_NAME" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w5" OnClick="btnSearchGROUP_NAME_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGROUP_NAME" runat="server" Text='<%# Bind("GROUP_NAME") %>' __designer:wfdid="w2"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Authorized Staff">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                     <asp:Label ID="lblStud_NameH" runat="server" Text="Authorized Staff" CssClass="gridheader_text" __designer:wfdid="w7"></asp:Label> <br />
                                            <asp:TextBox ID="txtEMP_Name" runat="server" ></asp:TextBox> 
                                            <asp:ImageButton ID="btnSearchEMP_Name" OnClick="btnSearchEMP_Name_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w9"></asp:ImageButton>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEMP_Name" runat="server" Text='<%# Bind("EMP_Name") %>' __designer:wfdid="w6"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Stud. Strength">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_COUNT" runat="server" Text='<%# Bind("STU_COUNT") %>' __designer:wfdid="w10"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View" __designer:wfdid="w13"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server" __designer:wfdid="w11">View</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AVG_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAVG_ID" runat="server" Text='<%# Bind("AVG_ID") %>' __designer:wfdid="w14"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop"   HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

