﻿<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studChangeCritical_Approve.aspx.vb" Inherits="Students_studChangeCritical_Approve" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript" src="../../../chromejs/chrome.js"></script>
    <script language="javascript" type="text/javascript">

        
                   var color = '';
                   function highlight(obj) {
                       var rowObject = getParentRow(obj);
                       var parentTable = document.getElementById("<%=GrdView.ClientID %>");
if (color == '') {
    color = getRowColor();
}
if (obj.checked) {
    rowObject.style.backgroundColor = '#f6deb2';
}
else {
    rowObject.style.backgroundColor = '';
    color = '';
}
    // private method

function getRowColor() {
    if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
    else return rowObject.style.backgroundColor;
}
}
// This method returns the parent row of the object
function getParentRow(obj) {
    do {
        obj = obj.parentElement;
    }
    while (obj.tagName != "TR")
    return obj;
}


function change_chk_state(chkThis) {
    var chk_state = !chkThis.checked;
    for (i = 0; i < document.forms[0].elements.length; i++) {
        var currentid = document.forms[0].elements[i].id;
        if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
            //if (document.forms[0].elements[i].type=='checkbox' )
            //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
            document.forms[0].elements[i].checked = chk_state;
            document.forms[0].elements[i].click();//fire the click event of the child element
        }
    }
}


    </script>

    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-dollar"></i> Approve Edit Critical Details
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">


    <table class="BlueTable" style="width: 90%" align="center" border="0">

        <tr>
            <td align="left" width="50%">
                View :
                <asp:DropDownList ID="ddlTopFilter" runat="server" AutoPostBack="True" SkinID="DropDownListNormal"
                    OnSelectedIndexChanged="ddlCount_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Value="10">Last 10</asp:ListItem>
                    <asp:ListItem Value="50">Last 50</asp:ListItem>
                    <asp:ListItem Value="100">Last 100</asp:ListItem>
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="right" colspan="2">
                <asp:RadioButton ID="rbOpen" runat="server" AutoPostBack="True" GroupName="SEC" OnCheckedChanged="rbOpen_CheckedChanged"
                    Text="Pending"></asp:RadioButton>
                <asp:RadioButton ID="rbApproved" runat="server" AutoPostBack="True" GroupName="SEC" 
                    Text="Approved"></asp:RadioButton>
                <asp:RadioButton ID="rbCancel" runat="server" AutoPostBack="True" GroupName="SEC"
                    OnCheckedChanged="rbCancel_CheckedChanged" Text="Rejected"></asp:RadioButton>
                <asp:RadioButton ID="rbAll" runat="server" AutoPostBack="True" GroupName="SEC" OnCheckedChanged="rbAll_CheckedChanged"
                    Text="All"></asp:RadioButton>
            </td>
        </tr>
        

        <tr >
            <td align="left" colspan="3">
                <asp:Label ID="lblError" runat="server" CssClass="error"
                    EnableViewState="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Panel ID="Panel1" runat="server" Width="100%" CssClass="BlueTableView">
                    <asp:GridView ID="GrdView" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                        HeaderStyle-Height="30" Width="100%" SkinID="GridViewView"
                         CssClass="table table-striped table-bordered text-left my-orders-table">
                        <Columns>
                            <%-- text='<%# getSerialNo() %>'--%>
                            <asp:TemplateField HeaderText="Select">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle Wrap="False" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server"
                                        onclick="javascript:highlight(this);" />
                                </ItemTemplate>
                                <HeaderTemplate>
                                   
                                          Select
                                                <br />
                                                <asp:CheckBox ID="chkAll" runat="server"
                                                    onclick="javascript:change_chk_state(this);"
                                                    ToolTip="Click here to select/deselect all rows" />
                                          
                                       
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SL.No">
                                <ItemTemplate>
                                    <asp:Label ID="lblSlNo" runat="server" Text='<%# Bind("SINO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student No">
                                <HeaderTemplate>
                                 
                                    
                                                <asp:Label ID="lblFeeHeader" runat="server" CssClass="gridheader_text"
                                                    Text="Stud. No"></asp:Label>
                                                <br />
                                                <asp:TextBox ID="txtFeeSearch" runat="server" Width="75%"></asp:TextBox>
                                                <asp:ImageButton ID="btnFeeId_Search" runat="server" ImageAlign="Top"
                                                                    ImageUrl="~/Images/forum_search.gif" OnClick="btnFeeId_Search_Click" />
                                      
                                        
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblFeeId" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                    <asp:HiddenField ID="HF_stu_id" runat="server" Value='<%# Bind("STU_ID") %>' />
                                    <asp:HiddenField ID="HF_sts_id" runat="server" Value='<%# Bind("STS_ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Student Name">
                                <HeaderTemplate>
                                   
                                          
                                                <asp:Label ID="lblName" runat="server" CssClass="gridheader_text"
                                                    Text="Student Name"></asp:Label>
                                                <br />
                                                <asp:TextBox ID="txtStudName" runat="server" Width="75%"></asp:TextBox>
                                                <asp:ImageButton ID="btnStudName_Search" runat="server" ImageAlign="Top"
                                                                    ImageUrl="~/Images/forum_search.gif" OnClick="btnStudName_Search_Click" />
                                        
                                       
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From Academic year">
                                <ItemTemplate>
                                    <asp:Label ID="lblF_ACY_DESCR" runat="server" Text='<%# Bind("F_ACY_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From Grade">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("F_GRM_DISPLAY") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From Section">
                                <ItemTemplate>
                                    <asp:Label ID="lblSection" runat="server" Text='<%# Bind("F_SCT_DESCR") %>'></asp:Label>
                                    <asp:HiddenField ID="HF_sct_id" runat="server"
                                        Value='<%# Bind("F_STS_SCT_ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="From Stream">
                                <ItemTemplate>
                                    <asp:Label ID="lblStream" runat="server" Text='<%# Bind("F_STM_DESCR") %>'></asp:Label>
                                    <asp:HiddenField ID="HF_STM_id" runat="server"
                                        Value='<%# Bind("F_STS_STM_ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DOJ Change">
                                <ItemTemplate>
                                    <asp:Label ID="lblDOJ" runat="server" Text='<%# Bind("STU_DOJ") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>



                            <asp:TemplateField HeaderText="To  Academic year">
                                <ItemTemplate>
                                    <asp:Label ID="lblT_ACY_DESCR" runat="server" Text='<%# Bind("T_ACY_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Grade">
                                <ItemTemplate>
                                    <asp:Label ID="lblT_GRM_DISPLAY" runat="server" Text='<%# Bind("T_GRM_DISPLAY") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Section">
                                <ItemTemplate>

                                    <asp:Label ID="lblT_SCT_DESCR" runat="server" Text='<%# Bind("T_SCT_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="To Stream">
                                <ItemTemplate>

                                    <asp:Label ID="lblT_STM_DESCR" runat="server" Text='<%# Bind("T_STM_DESCR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>

                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("SEC_ENTRY_REMARKS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <HeaderStyle Height="30px" CssClass="gridheader_pop" />
                        <RowStyle CssClass="griditem" Height="25px" />
                        <SelectedRowStyle CssClass="Green" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>

        <tr runat="server" id="tr_Remarks">
            <td align="left" width="10%">Remarks</td>
            <td  style="width: 1px">:</td>
            <td align="left"  width="50%">
                <asp:TextBox ID="txtRemarks" runat="server"  TextMode="MultiLine"  SkinID="MultiText" MaxLength="100"></asp:TextBox>
            </td>

        </tr>



        <tr  runat="server" id="tr_button">
            <td colspan="3" align="center">
                <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve"
                    ValidationGroup="trans" />
                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Reject"
                    ValidationGroup="trans" />
            </td>
        </tr>
        <%--     <tr >
            <td>
                &nbsp;</td>
            <td  align ="center" width="1px" >
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
           
                &nbsp;</td>
       </tr>--%>
    </table>

  
                </div>
            </div>
        </div>
    
</asp:Content>

