Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studSet_Curr_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

            Try

            

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050003" And ViewState("MainMnu_code") <> "C100380" And ViewState("MainMnu_code") <> "T050003") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    Using readerCURRICULUM_M As SqlDataReader = AccessStudentClass.GetCURRICULUM_BSU(Session("sBsuid"))
                        'create a list item to bind records from reader to dropdownlist ddlCurr
                        Dim di As ListItem
                        ddlCurr.Items.Clear()
                        'check if it return rows or not
                        If readerCURRICULUM_M.HasRows = True Then
                            While readerCURRICULUM_M.Read
                                di = New ListItem(readerCURRICULUM_M("CLM_DESCR"), readerCURRICULUM_M("CLM_ID"))
                                'adding listitems into the dropdownlist
                                ddlCurr.Items.Add(di)
                                For ItemTypeCounter As Integer = 0 To ddlCurr.Items.Count - 1
                                    'keep loop until you get the counter for default BusinessUnit into  the SelectedIndex
                                    If ddlCurr.Items(ItemTypeCounter).Value = Session("CLM") Then
                                        ddlCurr.SelectedIndex = ItemTypeCounter
                                    End If
                                Next
                            End While
                        End If
                    End Using

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub


  

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ddlCurr.SelectedItem.Text = "--" Then
            lblError.Text = "Curriculum needs to be selected"

        Else
            Session("CLM") = ddlCurr.SelectedItem.Value

            Using readerAcademic_Cutoff As SqlDataReader = AccessStudentClass.GetActive_BsuAndCutOff(Session("sBsuid"), Session("CLM"))
                If readerAcademic_Cutoff.HasRows = True Then
                    While readerAcademic_Cutoff.Read
                        If IsDate(readerAcademic_Cutoff("ACD_AGE_CUTOFF")) = True Then
                            Session("Cutoff_Age") = Convert.ToDateTime(readerAcademic_Cutoff("ACD_AGE_CUTOFF"))
                        Else
                            Session("Cutoff_Age") = ""
                        End If

                        Session("Current_ACY_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ACY_ID"))
                        Session("Current_ACD_ID") = Convert.ToString(readerAcademic_Cutoff("ACD_ID"))
                    End While
                End If
            End Using

            Using readerPrev_Next As SqlDataReader = AccessStudentClass.getNEXT_CURR_PREV(Session("sBsuid"), Session("CLM"), Session("Current_ACY_ID"))
                If readerPrev_Next.HasRows = True Then
                    While readerPrev_Next.Read
                        Session("prev_ACD_ID") = Convert.ToString(readerPrev_Next("prev_ACD_ID"))
                        Session("next_ACD_ID") = Convert.ToString(readerPrev_Next("next_ACD_ID"))
                    End While
                End If
            End Using
            lblError.Text = "Curriculum is set to  " & ddlCurr.SelectedItem.Text

        End If
    End Sub

    
End Class
