Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studScreen_D
    Inherits System.Web.UI.Page

    'Dim academicyear As String
    'Dim grade As String
    Dim SubjectArray As New ArrayList
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Not Page.IsPostBack Then
            Try

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If



                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code"))

                Dim CurUsr_id As String = Session("sUsr_id")
                Dim CurRole_id As String = Session("sroleid")
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                EnableDisableControls(False)
                If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S050060") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If

                Session("SelectedRow") = -1
                lblacademicYear.Text = Encr_decrData.Decrypt(Request.QueryString("academicyear").Replace(" ", "+"))
                lblGrade.Text = Encr_decrData.Decrypt(Request.QueryString("grade").Replace(" ", "+"))
                Dim str As String = Request.QueryString("acd_id").Replace(" ", "+")
                hfACD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("acd_id").Replace(" ", "+"))
                hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grdid").Replace(" ", "+"))
                hfSTM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("stmid").Replace(" ", "+"))
                lblStream.Text = Encr_decrData.Decrypt(Request.QueryString("stream").Replace(" ", "+"))
                '   hfACD_ID.Value = Request.QueryString("acd_id")
                PopulateSubjects()
                Session("dtScreen") = SetDataTable()
                GetRows()
                GridBind()

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, "Page_Load")
                lblError.Text = "Request could not be processed"
            End Try
        End If
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Try
            lblError.Text = ""
            AddEditRows(Session("SelectedRow"))
            GridBind()
            Session("SelectedRow") = -1
            txtMinMarks.Text = ""
            txtMaxMarks.Text = ""
            btnAddNew.Text = "Add"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            Session("dtScreen") = SetDataTable()
            GetRows()
            GridBind()
            ViewState("datamode") = "none"
            EnableDisableControls(False)
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStudScreen_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudScreen.RowCommand
        Try
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStudScreen.Rows(index), GridViewRow)

            Dim i As Integer
            Dim lblSubject As Label = selectedRow.Cells(0).FindControl("lblSubject")

            For i = 0 To Session("dtScreen").rows.count - 1
                If CType(Session("dtScreen").rows(i)(0), String) = lblSubject.Text And CType(Session("dtScreen").rows(i)(3), String) <> "Delete" And CType(Session("dtScreen").rows(i)(0), String) <> "Remove" Then
                    Session("SelectedRow") = i
                    Exit For
                End If
            Next
            If e.CommandName = "Edit" Then
                Dim lblMin As Label = selectedRow.Cells(1).FindControl("lblMin")
                Dim lblMax As Label = selectedRow.Cells(1).FindControl("lblMax")
                txtMinMarks.Text = lblMin.Text
                txtMaxMarks.Text = lblMax.Text
                btnAddNew.Text = "Update"
            Else
                If Session("dtScreen").rows(Session("SelectedRow"))("datamode") = "Add" Then
                    Session("dtScreen").rows(Session("SelectedRow"))("datamode") = "Remove"
                Else
                    Session("dtScreen").rows(Session("SelectedRow"))("datamode") = "Delete"
                End If

                GridBind()
                Session("SelectedRow") = -1
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvStudScreen_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvStudScreen.RowDeleting

    End Sub

    Protected Sub gvStudScreen_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvStudScreen.RowEditing
        Try
            gvStudScreen.EditIndex = e.NewEditIndex
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'ViewState("datamode") = Encr_decrData.Encrypt("view")
        'Dim url As String = String.Format("~\Students\studScreen_M.aspx?MainMnu_code={0}&datamode={1}", Encr_decrData.Encrypt(ViewState("MainMnu_code")), ViewState("datamode"))
        'Response.Redirect(url)
        Try
            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                txtMinMarks.Text = ""
                txtMaxMarks.Text = ""
                'clear the textbox and set the default settings
                ViewState("datamode") = "none"
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Session("dtScreen") = SetDataTable()
                GetRows()
                GridBind()
            Else
                Response.Redirect(ViewState("ReferrerUrl"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "add"
            EnableDisableControls(True)
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            lblError.Text = ""
            ViewState("datamode") = "edit"
            EnableDisableControls(True)
            Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Page_Load")
            lblError.Text = "Request could not be processed"
        End Try
    End Sub


#Region "Private Methods"

    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub PopulateSubjects()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim str_query As String = "SELECT distinct sbm_descr FROM subject_m as a inner join " _
            '                         & " subjectS_grade_s as b on a.sbm_id=b.sbg_sbm_id " _
            '                         & " WHERE SBG_GRD_ID='" + hfGRD_ID.Value + "' AND SBG_STM_ID=" + hfSTM_ID.Value
            'Dim reader As SqlDataReader
            'reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            'While reader.Read
            '    ddlSubject.Items.Add(reader.GetString(0))
            'End While
            'reader.Close()

            Dim str_query As String = "SELECT SCB_ID,SCB_DESCR FROM SCREENING_SUBJECTS_S WHERE SCB_BSU_ID='" + Session("SBSUID") + "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlSubject.DataSource = ds
            ddlSubject.DataTextField = "SCB_DESCR"
            ddlSubject.DataValueField = "SCB_ID"
            ddlSubject.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "PopulateSubjects")
        End Try
    End Sub

    Private Shared Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
        Dim key() As DataColumn
        ReDim key(1)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "Subject"
        dt.Columns.Add(column)
        key(0) = column

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MinMarks"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "MaxMarks"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "DataMode"
        dt.Columns.Add(column)

        dt.PrimaryKey = key

        Return dt
    End Function

    Private Sub GetRows()

        Try
            Dim dr As DataRow
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            'Dim str_query As String = "SELECT sbm_descr,scd_minmarks,scd_maxmarks FROM subject_m,screening_d,screening_m " _
            '                         & "WHERE(subject_m.sbm_id = screening_d.scd_sbm_id And screening_m.scm_id = screening_d.scd_scm_id " _
            '                         & " AND scm_acd_id = " + hfACD_ID.Value + " And scm_grd_id = '" + hfGRD_ID.Value.ToString + "' AND scm_smt_id=" + hfSTM_ID.Value + ")"
            Dim str_query As String = "SELECT SCB_DESCR,SCD_MINMARKS,SCD_MAXMARKS FROM SCREENING_SUBJECTS_S AS A" _
                                     & " INNER JOIN SCREENING_D AS B ON A.SCB_ID=B.SCD_SCB_ID" _
                                     & " INNER JOIN SCREENING_M AS C ON B.SCD_SCM_ID=C.SCM_ID" _
                                     & " WHERE SCM_ACD_ID=" + hfACD_ID.Value + " AND SCM_GRD_ID='" + hfGRD_ID.Value + "'" _
                                     & " AND SCM_SMT_ID=" + hfSTM_ID.Value

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
            While reader.Read
                dr = Session("dtScreen").newrow
                dr.Item(0) = reader.GetString(0)
                dr.Item(1) = reader.GetValue(1)
                dr.Item(2) = reader.GetValue(2)
                dr.Item(3) = "Edit"
                Session("dtScreen").rows.add(dr)
            End While
            reader.Close()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub AddEditRows(Optional ByVal index As Integer = -1)
        Try
            Dim dr As DataRow
            Dim dt As DataTable

            dt = Session("dtScreen")
            If index = -1 Then
                Dim keys As Object

                keys = ddlSubject.Text.Trim
                Dim row As DataRow = dt.Rows.Find(keys)
                If Not row Is Nothing Then
                    lblError.Text = "Marks for the subject " + ddlSubject.SelectedItem.Text + " is already entered "
                    Exit Sub
                End If
                dr = dt.NewRow
                dr.Item(0) = ddlSubject.SelectedItem.Text
                dr.Item(1) = txtMinMarks.Text
                dr.Item(2) = txtMaxMarks.Text
                dr.Item(3) = "Add"
                dt.Rows.Add(dr)
            Else
                dt.Rows(index)("MinMarks") = txtMinMarks.Text
                dt.Rows(index)("MaxMarks") = txtMaxMarks.Text
                dt.Rows(index)("datamode") = "Update"
            End If

            Session("dtScreen") = dt
            GridBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub GridBind()
        Try
            Dim dtTemp As New DataTable
            dtTemp = SetDataTable()
            Dim drTemp As DataRow
            Dim i, j As Integer

            Dim s As String
            If Session("dtScreen").Rows.Count > 0 Then
                For i = 0 To Session("dtscreen").Rows.Count - 1
                    s = Session("dtScreen").Rows(i)("DataMode")
                    If (Session("dtScreen").Rows(i)("DataMode") <> "Delete") And (Session("dtScreen").Rows(i)("DataMode") <> "Remove") Then
                        drTemp = dtTemp.NewRow
                        For j = 0 To Session("dtScreen").Columns.Count - 1
                            drTemp.Item(j) = Session("dtScreen").Rows(i)(j)
                        Next
                        dtTemp.Rows.Add(drTemp)
                    End If
                Next
            End If
            gvStudScreen.DataSource = dtTemp
            gvStudScreen.DataBind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Sub EnableDisableControls(ByVal value As Boolean)
        btnAddNew.Enabled = value
        txtMinMarks.Enabled = value
        txtMaxMarks.Enabled = value
        ddlSubject.Enabled = value
        gvStudScreen.Enabled = value
    End Sub

    Private Sub SaveData()

        Dim dtTemp As DataTable = Session("dtScreen")
        Dim i As Integer
        Dim str_query As String
        Dim scmAddIds As String = ""
        Dim scmEditIds As String = ""
        Dim scmDeleteIds As String = ""
        Dim scmId As Integer

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try


                For i = 0 To dtTemp.Rows.Count - 1
                    If dtTemp.Rows(i)("datamode") <> "Remove" And dtTemp.Rows(i)("datamode") <> "Edit" Then
                        str_query = "exec savestudscreening " + CType(hfACD_ID.Value, String) + ",'" + CType(hfGRD_ID.Value, String) + "','" + CType(Session("sBsuid"), String) + "'," + hfSTM_ID.Value.ToString + ",'" + CType(dtTemp.Rows(i)(0), String).Trim + "'," + CType(dtTemp.Rows(i)(1), String) + "," + CType(dtTemp.Rows(i)(2), String) + ",'" + CType(dtTemp.Rows(i)(3), String) + "'"
                        scmId = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                        If dtTemp.Rows(i)("datamode").ToString.ToLower = "add" Then
                            scmAddIds = scmId.ToString
                        End If

                        If dtTemp.Rows(i)("datamode").ToString.ToLower = "update" Then
                            scmEditIds = scmId.ToString
                        End If

                        If dtTemp.Rows(i)("datamode").ToString.ToLower = "delete" Then
                            scmDeleteIds = scmId.ToString
                        End If
                    End If

                Next
                Dim flagAudit As Integer
                If scmAddIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SCM_ID(" + scmAddIds + ")", "Insert", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If scmEditIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SCM_ID(" + scmEditIds + ")", "edit", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If

                If scmDeleteIds <> "" Then
                    flagAudit = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), "SCM_ID(" + scmDeleteIds + ")", "delete", Page.User.Identity.Name.ToString, Me.Page)
                    If flagAudit <> 0 Then
                        Throw New ArgumentException("Could not process your request")
                    End If
                End If
                transaction.Commit()
                lblError.Text = "Record Saved Successfully"

            Catch myex As ArgumentException
                transaction.Rollback()
                lblError.Text = myex.Message
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            Catch ex As Exception
                transaction.Rollback()
                lblError.Text = "Record could not be Saved"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        End Using
    End Sub

#End Region
End Class
