Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports studClass
Imports Encryption64


Partial Class Students_studConfirmOffer
    Inherits System.Web.UI.Page
    Dim studClass As New studClass
    Dim encr As New Encryption64
    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click

        Dim Decr_decrData As New Encryption64()
        ''Get the Enquiry Ids.
        Dim eqsid As String = Request.QueryString("eqsid")
        Dim enqid As String = Request.QueryString("enqid")

        Dim Engchange As New studClass
        Dim dec As New Encryption64
        eqsid = dec.Decrypt(eqsid.Replace(" ", "+"))
        ''Change Enquirey Status
        Engchange.EnquiryChange(eqsid, "OFR", System.DateTime.Today, 6, 6, True)
        ''Redirecting to Print Offer Page.
        Response.Redirect("~/Students/studPrintOffer.aspx?eqsid=" + encr.Encrypt(eqsid) + "&enqid=" + encr.Encrypt(enqid))
       
    End Sub

    

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        ''Redirecting to Student Enquiry. 
        Response.Redirect("~/Students/studEnquiryNew.aspx?datamode=Zo4HhpVNpXc=&MainMnu_code=LjcF1BVVHbk=")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CheckOfferIssued()
            GET_STUDENTSHORTLIST_DUE()
            '***********Code added by dhanya 16-07-08

            If studClass.GetBsuShowDocs(Session("sbsuid")) = True Then
                studClass.BindDocumentsList(lstNotSubmitted, "6", "false", HiddenEQSID.Value)
                studClass.BindDocumentsList(lstSubmitted, "6", "true", HiddenEQSID.Value)
                If lstNotSubmitted.Items.Count <> 0 Or lstSubmitted.Items.Count <> 0 Then
                    trDoc.Visible = True
                Else
                    trDoc.Visible = False
                End If
                lnkDocs.Visible = False
            Else
                hfURL.Value = "studJoinDocuments.aspx?eqsid=" + HiddenEQSID.Value
                lnkDocs.Visible = True
                trDoc.Visible = False
            End If
        End If
    End Sub

    'code added by dhanya 16-07-08
    Protected Sub btnRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRight.Click
        studClass.UpdateDocListItems(lstNotSubmitted, lstSubmitted, "true", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub

    Protected Sub btnLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeft.Click
        studClass.UpdateDocListItems(lstSubmitted, lstNotSubmitted, "false", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub

    Public Sub CheckOfferIssued()
        Dim Decr_decrData As New Encryption64()
        ''Get the Enquiry Ids.
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString()
        Dim eqsid As String = Decr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
        HiddenEQSID.Value = eqsid
        Dim enqid As String = Request.QueryString("enqid")
        Dim str_query = "select PRA_bCOMPLETED from PROCESSFO_APPLICANT_S where pra_eqs_id='" & eqsid & "' and PRA_STG_ID='6'"
        Dim val As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString()
        If val = "True" Then
            Response.Redirect("~/Students/studPrintOffer.aspx?eqsid=" + Request.QueryString("eqsid") + "&enqid=" + enqid)
        End If

    End Sub


    Sub GET_STUDENTSHORTLIST_DUE()
        Try
            Dim str_Sql As String = "exec FEES.GET_STUDENTSHORTLIST_DUE  '" & HiddenEQSID.Value.ToString & "'  "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
            GridViewShowDetails.DataSource = ds

            GridViewShowDetails.EmptyDataText = "No data Found"
            GridViewShowDetails.DataBind()
        Catch ex As Exception

        End Try
    End Sub
End Class

