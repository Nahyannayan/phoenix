Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_studBlueBookAll_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim lstrMenuCode As String
    Dim lstrGrade As String
    Dim lstrSection As String

    Protected Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then

           
            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            ViewState("grd_id") = Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+"))
            ViewState("sctdesc") = Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+"))


            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100190" And ViewState("MainMnu_code") <> "S100450" And ViewState("MainMnu_code") <> "S100451") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

                ViewState("FirstPage") = 1

                ViewState("BlankSheet") = False
                ViewState("AutoPage_No") = False
                'if reprint bluebook all grades are selected then dont show main header and sho all records in one table'CODE ADDED BY LIJO MENUCODE CONDTION
                If Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")) <> "0" Then
                    GetBlueBook(Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+")), Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")))
                Else

                    ReprintBlueBookForAllGrades(Encr_decrData.Decrypt(Request.QueryString("Grade").Replace(" ", "+")), Encr_decrData.Decrypt(Request.QueryString("Section").Replace(" ", "+")))
                End If
            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try
        End If
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub GetBlueBook(ByVal grd_id As String, ByVal sct_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim sb As New StringBuilder
        '************Page Header************'
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: Blue Book ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<BODY>")

        If sct_id = "ALL" Then
            'str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
            '            & "' AND SCT_ACD_ID=" + Session("Current_ACD_ID")


            If GetBSU_bbsct_status() = True Then

                str_query = "select distinct STU_BB_SECTION_DES as SCT_ID,STU_BB_SECTION_DES as SCT_DESCR from student_m where [STU_GRM_ID] in (SELECT [GRM_ID]  FROM [GRADE_BSU_M] where [GRM_BSU_ID]='" & Session("sBsuid") & "' and [GRM_ACD_ID]='" & Session("Current_ACD_ID") & "' and GRM_GRD_ID='" & grd_id & "')   order by SCT_DESCR"
            Else
                str_query = "SELECT SCT_ID FROM SECTION_M WHERE SCT_GRD_ID='" + grd_id _
                                                        & "' AND SCT_ACD_ID=" + Session("Current_ACD_ID")
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            Dim i As Integer = 0
            With ds.Tables(0)
                For i = 0 To .Rows.Count - 1
                    sb.AppendLine(GetBlueBookForSection(grd_id, .Rows(i).Item(0).ToString))
                    'CONTROLLING THE BLANK SHEET--CODE MODIFIED BY LIJO
                    If ViewState("BlankSheet") = True Then
                        sb.AppendLine(GetBlank(grd_id, .Rows(i).Item(0).ToString))
                    End If

                Next
            End With
        Else
            sb.AppendLine(GetBlueBookForSection(grd_id, sct_id))
            'CONTROLLING THE BLANK SHEET--CODE MODIFIED BY LIJO
            'If (Session("sbsuid") = "125011") Then
            If ViewState("BlankSheet") = True Then

                sb.AppendLine(GetBlank(grd_id, sct_id))
            End If
            'End If
        End If
        sb.AppendLine("</BODY></HTML>")
        ltBlueBook.Text = sb.ToString

    End Sub
    Function GetHeadersAndFooter(ByVal sct_id As String) As String()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BB_HDR,BB_FTR,BB_TBL_HDR FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim strHeader As String
        Dim strFooter As String
        Dim strTblHeader As String
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            strHeader = reader.GetString(0)
            strFooter = reader.GetString(1)
            strTblHeader = reader.GetString(2)
        End While
        reader.Close()
        strHeader = strHeader.Replace("''", "'")
        strFooter = strFooter.Replace("''", "'")
        strTblHeader = strTblHeader.Replace("''", "'")

        If GetBSU_bbsct_status() = True Then
            str_query = "exec studGETBLUEBOOKHEADERANDFOOTER_new " & Session("Current_ACD_ID") & "," & Session("sBsuid") & ",'" & ViewState("grd_id") & "','" & sct_id & "'"

        Else
            str_query = "exec studGETBLUEBOOKHEADERANDFOOTER " + sct_id

        End If

        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            With reader
                strHeader = strHeader.Replace("%moelogo%", .GetString(0))
                strHeader = strHeader.Replace("%logo%", .GetString(1))
                strHeader = strHeader.Replace("%bsuname%", .GetString(2))
                strHeader = strHeader.Replace("%bsuarabic%", .GetString(3))
                strHeader = strHeader.Replace("%bsucity%", .GetString(4))
                strHeader = strHeader.Replace("%moelicence%", .GetString(5))
                strHeader = strHeader.Replace("%grade%", .GetString(6))

                If Session("sBsuid") <> "111001" Then
                    strHeader = strHeader.Replace("%grdheader%", .GetString(7))
                Else
                    If (ViewState("grd_id") <> "KG1") And (ViewState("grd_id") <> "KG2") And (ViewState("grd_id") <> "01") And (ViewState("grd_id") <> "02") Then
                        strHeader = strHeader.Replace("%grdheader%", "Grades 10 to 3")
                    Else
                        strHeader = strHeader.Replace("%grdheader%", "Grades 2 to KG 1")
                    End If
                End If

                strHeader = strHeader.Replace("%section%", .GetString(8))
                strHeader = strHeader.Replace("%accyear%", .GetString(9))
                strHeader = strHeader.Replace("%cteacher%", .GetString(10))
                strFooter = strFooter.Replace("%cteacher%", .GetString(10))
                strHeader = strHeader.Replace("%totstudents%", .GetValue(11).ToString)
                strHeader = strHeader.Replace("%musilims%", .GetValue(12).ToString)
                strHeader = strHeader.Replace("%nonmuslims%", .GetValue(13).ToString)
                strHeader = strHeader.Replace("%locals%", .GetValue(14).ToString)
                strHeader = strHeader.Replace("%expats%", .GetValue(15).ToString)
                strHeader = strHeader.Replace("%localmuslims%", .GetValue(16).ToString)
                strHeader = strHeader.Replace("%expatmuslims%", .GetValue(17).ToString)
                strHeader = strHeader.Replace("%localnonmuslims%", .GetValue(18).ToString)
                strHeader = strHeader.Replace("%expatnonmuslims%", .GetValue(19).ToString)
                strHeader = strHeader.Replace("%curriculum%", .GetValue(20).ToString)
                strHeader = strHeader.Replace("%qualification%", .GetValue(21).ToString)
                strFooter = strFooter.Replace("%registrar%", .GetString(22))
                strFooter = strFooter.Replace("%principal%", .GetString(23))
                strFooter = strFooter.Replace("%ARABIC_SECRETARY%", .GetString(24))
                strFooter = strFooter.Replace("%printdate%", Session("PrintDate"))
            End With
        End While

        Dim str(2) As String

        str(0) = strHeader
        str(1) = strFooter
        str(2) = strTblHeader
        Return str
    End Function
    Public Function GetBSU_bbsct_status() As Boolean

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String = "select BSU_bMOEGRADE FROM BUSINESSUNIT_M WHERE BSU_ID='" & Session("sBSUID") & "'"
        Return IIf(IsDBNull(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql)) = False, SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql), "False")


    End Function
    Function GetBlueBookForSection(ByVal grd_id As String, ByVal sct_id As String) As String

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim bHdrAllPages As Boolean = False
        Dim bFtrAllPages As Boolean = False
        Dim bNationality As Boolean = False
        Dim bReligion As Boolean = False
        Dim bRelDISAll As Boolean = False
        Dim bGender As Boolean = False
        Dim pageSize As Integer
        Dim bRedLine As Boolean = False
        Dim bBlank As Boolean = False
        Dim bIncludeTC As Boolean = False
        Dim bIncludeSO As Boolean = False
        Dim bShowParent1 As Boolean = False
        Dim bShowParent2 As Boolean = False
        Dim BB_bPOB_ABBRVN As Boolean = False


        Dim recordCount As Integer = 0
        Dim pages As Integer

        Dim strHeaders As String() = GetHeadersAndFooter(sct_id)

        Dim strHeader As String
        Dim strFooter As String = strHeaders(1)
        Dim strTblHeader As String = strHeaders(2)
        Dim sb As New StringBuilder

        Dim ageYear As Integer = 0
        Dim ageMonth As Integer = 0
        Dim cutOffDate As Date
        Dim dob As Date

        Dim prevSchInt As String = ""
        Dim prevSchOvr As String = ""
        Dim prevSchNew As String = ""

        Dim tdStyle As String
        Dim tdStyleSmall As String
        Dim tdStyleArabic As String

        Dim intHold As Integer
        Dim intHold2 As Decimal

        Dim stu_id As String
        Dim stuLink As String = ""
        Dim srNo As Integer = 0

        Dim stuName As String = ""
        Dim stuNames As String()
        Dim parentName As String = ""
        Dim lstrPOB As String
        Dim lstrTC_CutOff
        Dim lstrBB_EDIT_ROLES As String
        Dim lstrBB_CAN_EDIT As String

        Dim str_query As String = "SELECT BB_HDR_ALLPAGES,BB_FTR_ALLPAGES,BB_NATIONALITY," _
                                & " BB_RELIGION,BB_REL_DISPLAYALL,BB_GENDER,BB_PAGESIZE," _
                                & " BB_REDLINE,BB_BLANK_REPLACE,ISNULL(BB_INCLUDETC,'FALSE'),ISNULL(BB_INCLUDESO,'FALSE')," _
                                & " ISNULL(BB_INTPREVSCHOOL,'DATA'), " _
                                & " ISNULL(BB_OVRPREVSCHOOL,'BLANK'),ISNULL(BB_NEWPREVSCHOOL,'BLANK'),ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE') , " _
                                & " ISNULL(BB_AUTOPAGE_NO,'FALSE'),ISNULL(BB_BLANKSHEET,'FALSE'),ISNULL(BB_TCOUTSIDE_UAE_COL,'Green'),ISNULL(BB_TCWITHIN_UAE_COL,'Green'),ISNULL(BB_SO_COL,'Red'),ISNULL(BB_bPOB_ABBRVN,'FALSE'),isNULL(BB_LASTATTDATE,'') as TC_CUTOFF,BB_EDIT_ROLES FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"


        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            With reader
                bHdrAllPages = .GetBoolean(0)
                bFtrAllPages = .GetBoolean(1)
                bNationality = .GetBoolean(2)
                bReligion = .GetBoolean(3)
                bRelDISAll = .GetBoolean(4)
                bGender = .GetBoolean(5)
                pageSize = .GetValue(6)
                bRedLine = .GetBoolean(7)
                bBlank = .GetBoolean(8)
                bIncludeTC = .GetBoolean(9)
                bIncludeSO = .GetBoolean(10)
                prevSchInt = .GetString(11)
                prevSchOvr = .GetString(12)
                prevSchNew = .GetString(13)
                bShowParent1 = .GetBoolean(14)
                bShowParent2 = .GetBoolean(15)
                ViewState("AutoPage_No") = .GetBoolean(16)
                ViewState("BlankSheet") = .GetBoolean(17)
                ViewState("TCOUTSIDE_UAE_COL") = .GetString(18)
                ViewState("TCWITHIN_UAE_COL") = .GetString(19)
                ViewState("SO_COL") = .GetString(20)
                BB_bPOB_ABBRVN = .GetBoolean(21)
                lstrTC_CutOff = .GetString(22)
                lstrBB_EDIT_ROLES = .GetString(23)
            End With

        End While
        reader.Close()
        If lstrBB_EDIT_ROLES = "ALL" Then
            lstrBB_CAN_EDIT = "1"
        Else
            If lstrBB_EDIT_ROLES.Contains(Session("SRoleId")) Then
                lstrBB_CAN_EDIT = "1"
            Else
                lstrBB_CAN_EDIT = "0"
            End If
        End If
        str_query = "SELECT distinct ISNULL(STU_BLUEID,'&nbsp'),isNULL(STU_BB_PREVGRADE,ISNULL((SELECT isNULL(STP_BB_GRADE_DES,GRM_BB_DISPLAY)+' '+isNULL(STP_BB_SECTION_DES,SCT_DESCR) AS LastGrade " _
                   & " FROM  STUDENT_PROMO_S INNER JOIN  GRADE_BSU_M ON STUDENT_PROMO_S.STP_GRM_ID = GRADE_BSU_M.GRM_ID LEFT JOIN" _
                   & " SECTION_M ON STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID " _
                   & " WHERE STUDENT_PROMO_S.STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STUDENT_PROMO_S.STP_STU_ID =A.STU_ID),'NEW')) AS LastGrade," _
                   & " ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,'')," _
                   & " ISNULL(STU_GENDER,'&nbsp'),STU_GENDERDETAIL=CASE STU_GENDER WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END," _
                   & " ISNULL(C.CTY_SHORT,'&nbsp'),ISNULL(C.CTY_NATIONALITY,'&nbsp'),ISNULL(RLG_ID,0),ISNULL(RLG_DESCR,''),POB=CASE STU_COB WHEN 172 THEN ISNULL(STU_POB,'&nbsp')+'/UAE' ELSE ISNULL(J.CTY_DESCR,'&nbsp') END ,ISNULL(STU_DOB,'&nbsp')," _
                   & " STU_EMGCONTACT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FMOBILE,'&nbsp') WHEN 'M' THEN ISNULL(STS_MMOBILE,'&nbsp') ELSE ISNULL(STS_GMOBILE,'&nbsp') END," _
                   & " STU_POBOX=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMPOBOX,'&nbsp') WHEN 'M' THEN ISNULL(STS_MCOMPOBOX,'&nbsp') ELSE ISNULL(STS_GCOMPOBOX,'&nbsp') END," _
                   & " ISNULL(STU_TFRTYPE,'&nbsp'), ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
                   & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
                   & " (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'NEW')" _
                   & " ,ISNULL(STU_PREVSCHI,'&nbsp'),ISNULL(STU_PREVSCHI_COUNTRY,'&nbsp'),ISNULL(CLM_DESCR,'&nbsp'), " _
                   & " ISNULL(STU_PREVSCHI_LASTATTDATE,'01/01/1900'),ISNULL(STU_MINDOJ,''),ISNULL(GRM_BB_DISPLAY,STU_GRD_ID_JOIN),ISNULL(I.TCM_STU_ID,0),ISNULL(TCM_TCSO,''),ISNULL(STU_LEAVEDATE,'2100-01-01'),ISNULL(TCR_DESCR,'&nbsp'),STU_ID ," _
                   & " CASE WHEN STU_MINDOJ BETWEEN ACD_STARTDT AND ACD_ENDDT THEN 'Y' ELSE 'N' END, " _
                   & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                   & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                   & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END,isNULL(STU_COB,5) as ST_COB,isNULL(STU_POB,'') as STU_POB " _
                   & " ,CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMCOUNTRY,'') WHEN 'M' " _
                   & " THEN ISNULL(STS_MCOMCOUNTRY,'') WHEN 'G' THEN ISNULL(STS_MCOMCOUNTRY,'') END As pobox_Country " _
                   & " ,CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMCITY,'&nbsp') WHEN 'M' " _
                   & " THEN ISNULL(STS_MCOMCITY,'&nbsp') WHEN 'G' THEN ISNULL(STS_GCOMCITY,'&nbsp') END As pobox_emir, " _
                   & " ISNULL(H.GRM_BB_DISPLAY,STU_GRD_ID_JOIN) AS JOIN_GRD " _
                   & " ,(Select BSU_CITY from dbo.BUSINESSUNIT_M where bsu_id=a.stu_bsu_id) as School_Emir " _
                   & " ,ISNULL(I.TCM_TCTYPE,'0') as TC_TYPE,STU_PASPRTNAME,ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
                   & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
        & " (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'Regular') as STP_MINLIST,TCM_LASTATTDATE,isNULL(OIS_Year,'') as OIS_Year,isNULL(OIS_Month,'') as OIS_Month,isNULL(STU_BB_REMARKS,'') as BBRemarks ," _
                    & " isnull((select isnull(STP_BBSLNO,'') from STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID),'') as STP_BBSLNO,isNULL(STU_BB_TFRTYPE,'') as STU_BB_TFRTYPE FROM STUDENT_M AS A INNER JOIN " _
                   & " STUDENT_D AS B ON  A.STU_SIBLING_ID = B.STS_STU_ID" _
                   & " INNER JOIN COUNTRY_M AS C  ON C.CTY_ID = A.STU_NATIONALITY" _
                   & " INNER JOIN RELIGION_M AS F ON A.STU_RLG_ID = F.RLG_ID" _
                   & " LEFT OUTER JOIN CURRICULUM_M AS G ON A.STU_PREVSCHI_CLM=G.CLM_ID" _
                   & " LEFT OUTER JOIN GRADE_BSU_M AS H ON Replace(Replace(A.STU_GRD_ID_JOIN,'FS1','KG1'),'FS2','KG2')=H.GRM_GRD_ID AND A.STU_ACD_ID=H.GRM_ACD_ID AND  H.GRM_SHF_ID=A.STU_SHF_ID" _
                   & " LEFT OUTER JOIN TCM_M AS I ON A.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL" _
                   & " LEFT OUTER JOIN TC_REASONS_M AS K ON I.TCM_REASON=K.TCR_CODE" _
                & " LEFT OUTER JOIN OIS_BB_Temp AS OIS ON A.STU_Fee_Id=OIS.OIS_FeeId AND A.STU_BSU_ID=OIS.OIS_BSU_ID" _
                   & " LEFT OUTER JOIN COUNTRY_M AS J  ON J.CTY_ID = A.STU_COB" _
                   & " INNER JOIN ACADEMICYEAR_D AS P ON A.STU_ACD_ID=P.ACD_ID "
        If ViewState("MainMnu_code") <> "S100450" Then
            If (Session("sbsuid") <> "121013") Then
                If GetBSU_bbsct_status() = True Then
                    str_query += " WHERE STU_CURRSTATUS NOT IN ('CN','TF') AND STU_ACD_ID='" & Session("Current_ACD_ID") & "' AND (STU_BB_SECTION_DES = '" + sct_id + "') AND STU_MINLIST='Regular'" _
                           & "    AND STU_BB_GRM_GRD_ID='" + grd_id + "' and isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"
                Else
                    str_query += " WHERE   STU_CURRSTATUS NOT IN ('CN','TF') AND  (STU_SCT_ID = " + sct_id + ") AND STU_MINLIST='Regular'" _
                                      & "   AND  isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"
                End If
            Else
                If GetBSU_bbsct_status() = True Then
                    str_query += " WHERE STU_CURRSTATUS NOT IN ('CN','TF') AND STU_ACD_ID='" & Session("Current_ACD_ID") & "' AND (STU_BB_SECTION_DES = '" + sct_id + "') AND STU_MINLIST='Regular'" _
                           & "    AND STU_BB_GRM_GRD_ID='" + grd_id + "' "
                Else
                    'str_query += " WHERE   STU_CURRSTATUS<>'CN' AND  (STU_SCT_ID = " + sct_id + ") AND STU_MINLIST='Regular'" _
                    '                  & "   "

                    str_query += " WHERE   STU_CURRSTATUS NOT IN ('CN','TF') AND  (STU_SCT_ID = " + sct_id + ") AND STU_MINLIST='Regular'" _
                                    & "   AND  isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"
                End If
            End If



            If lstrTC_CutOff <> "" Then
                str_query += " AND isnull(I.TCM_LASTATTDATE,getdate())>='" & lstrTC_CutOff & "'"
            End If



            If bIncludeTC = False And bIncludeSO = False Then
                str_query += " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) "
            End If
        End If


        If ViewState("MainMnu_code") = "S100450" Then
            str_query += "AND STU_ID IN(" + Session("BlueBookStuIds") + ")"
        End If


        str_query += "     ORDER BY STU_PASPRTNAME"
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        Dim records As Integer
        If ViewState("MainMnu_code") = "S100450" Then
            records = CType(Session("BlueBookRecords"), Integer)
        Else
            If GetBSU_bbsct_status() = True Then
                str_query = " SELECT COUNT(STU_ID) FROM STUDENT_M A WHERE STU_BB_SECTION_DES='" + sct_id + "' AND STU_BB_GRM_GRD_ID='" + grd_id + "' AND STU_MINLIST='Regular' AND STU_CurrStatus NOT IN ('CN','TF')"
            Else
                str_query = " SELECT COUNT(STU_ID) FROM STUDENT_M A LEFT OUTER JOIN TCM_M AS I ON A.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL WHERE STU_CURRSTATUS NOT IN ('CN','TF') AND  (STU_SCT_ID = " + sct_id + ") AND STU_MINLIST='Regular'" _
                                  & "   AND  isnull(I.TCM_LASTATTDATE,getdate())>=(select ACD_STARTDT from academicyear_d where acd_id=" + Session("Current_ACD_ID") + ")"

                If lstrTC_CutOff <> "" Then
                    str_query += " AND isnull(I.TCM_LASTATTDATE,getdate())>='" & lstrTC_CutOff & "'"
                End If



                If bIncludeTC = False And bIncludeSO = False Then
                    str_query += " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) "
                End If

            End If

            records = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        End If

        Dim blank As String = "&nbsp"
        If bBlank = True Then
            blank = "-"
        End If


        Dim pageNo As Integer = 1
        If bHdrAllPages = False Then
            strHeader = strHeaders(0)
            'CODE MODIFIED BY LIJO TO CONTROL AUTO PAGE NO
            If ViewState("AutoPage_No") = True Then
                strHeader = strHeader.Replace("%pgno%", pageNo)
            Else
                strHeader = strHeader.Replace("%pgno%", "&nbsp")
            End If

            sb.AppendLine(strHeader)
        End If


        While reader.Read

            If recordCount = 0 And bHdrAllPages = True Then
                If ViewState("FirstPage") <> "1" Then
                    sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
                End If
                ViewState("FirstPage") = "0"
                strHeader = strHeaders(0)
                'CODE MODIFIED BY LIJO TO CONTROL AUTO PAGE NO
                If ViewState("AutoPage_No") = True Then
                    strHeader = strHeader.Replace("%pgno%", pageNo)
                Else
                    strHeader = strHeader.Replace("%pgno%", "&nbsp")
                End If

                sb.AppendLine(strHeader)
                pageNo += 1
            End If

            If recordCount = 0 Then
                sb.AppendLine(strTblHeader)
            End If

            srNo += 1

            tdStyle = """FONT: bold 7pt verdana,timesroman,garamond"""
            tdStyleSmall = """FONT: 6pt verdana,timesroman,garamond"""


            tdStyleArabic = """FONT: bold 11pt verdana,timesroman,garamond"""
            Dim tdStyle_red As String = String.Empty
            If bRedLine = True Then
                If srNo = records Then
                    tdStyle = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 7pt verdana,timesroman,garamond;padding-bottom:2px;"""
                    tdStyleArabic = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 9pt verdana,timesroman,garamond"""
                End If
            End If


            If bIncludeTC = False And bIncludeSO = False Then
                sb.AppendLine("<TR style=""height:30px"">")
            ElseIf bIncludeTC = True And reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date Then '24 LEAVE DATE
                If Convert.ToString(reader("TC_TYPE")) = "1" Or Convert.ToString(reader("TC_TYPE")) = "3" Then
                    sb.AppendLine("<TR style=""height:30px"" bgColor=" + ViewState("TCWITHIN_UAE_COL") + ">")
                ElseIf Convert.ToString(reader("TC_TYPE")) = "2" Or Convert.ToString(reader("TC_TYPE")) = "4" Then

                    sb.AppendLine("<TR style=""height:30px"" bgColor=" + ViewState("TCOUTSIDE_UAE_COL") + ">")
                Else
                    sb.AppendLine("<TR style=""height:30px"" bgColor=green>")
                End If
            ElseIf bIncludeSO = True And reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date Then

                sb.AppendLine("<TR style=""height:30px"" bgColor=" + ViewState("SO_COL") + ">")


            Else
                sb.AppendLine("<TR style=""height:30px"">")
            End If

            'serial no
            sb.AppendLine("<TD style=" + tdStyle + " border=""1"" align=middle>")
            sb.AppendLine(srNo.ToString + "</TD>")

            'bluebook id
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(reader.GetString(0).Replace("&nbsp", blank) + "</TD>")

            'last grade
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")

            If StrConv(Trim(reader("STP_MINLIST").ToString), VbStrConv.ProperCase) <> "Regular" Then
                sb.AppendLine("NEW" + "</TD>")
            Else
                'sb.AppendLine(reader.GetString(1).Replace("&nbsp", blank).ToUpper + "</TD>")
                If Trim(reader("STP_BBSLNO").ToString) <> "" Then
                    sb.AppendLine(reader.GetString(1).ToUpper + "/" + Trim(reader("STP_BBSLNO").ToString) + "</TD>")
                Else
                    sb.AppendLine(reader.GetString(1).ToUpper + "</TD>")
                End If
            End If


            'name in english
            '' '' If the name has only one or two names then add parentname based on bb_showparent1 or bb_showparent2
            '' '' If the name has only one or two names then add parentname based on bb_showparent1 or bb_showparent2
            stu_id = Encr_decrData.Encrypt(reader.GetValue(26))
            If ViewState("MainMnu_code") = "S100190" Then
                stuLink = "../Students/StudRecordEdit_new.aspx?MainMnu_code=s08/lw50oAY=&datamode=4xCdg/cr4Xw=&viewid=" + stu_id + "&SetMnu=" + lstrMenuCode + "&SetGrade=" + lstrGrade + "&SetSect=" + lstrSection
            Else
                stuLink = "../Students/StudRecordEdit_new.aspx?MainMnu_code=SbWsUfLyXSM=&datamode=4xCdg/cr4Xw=&viewid=" + stu_id + "&SetMnu=" + lstrMenuCode + "&SetGrade=" + lstrGrade + "&SetSect=" + lstrSection
            End If
            If lstrBB_CAN_EDIT = "1" Then
                sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")
            Else
                sb.AppendLine("<TD style=" + tdStyle + " align=left><TABLE Width=""98%""><TR><TD style=" + tdStyle + " align=left>")
            End If


            stuName = reader.GetString(2).Trim

            stuName = stuName.Replace("  ", " ")
            stuNames = stuName.Split(" ")
            parentName = reader.GetString(28)

            If (bShowParent1 = True And stuNames.Length = 1) Or (bShowParent2 = True And stuNames.Length = 2) Then
                stuName = stuName + " / " + parentName
            End If

            If lstrBB_CAN_EDIT = "1" Then
                sb.AppendLine(stuName.ToUpper + "</a></TD></TR></TABLE></TD>")
            Else
                sb.AppendLine(stuName.ToUpper + "</TD></TR></TABLE></TD>")
            End If



            ' sb.AppendLine(reader.GetString(2).ToUpper.Replace("&nbsp", blank) + "</a></TD>")

            'name in arabic
            sb.AppendLine("<TD style=" + tdStyleArabic + " align=right><TABLE Width=""100%""><TR><TD style=" + tdStyleArabic + " align=right>")
            sb.AppendLine(reader.GetString(3).Replace("&nbsp", "") + "</TD></TR></TABLE></TD>")


            'gender
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            If bGender = False Then
                sb.AppendLine(reader.GetString(4).ToUpper.Replace("&NBSP", blank) + "</TD>")
            Else
                sb.AppendLine(reader.GetString(5).ToUpper.Replace("&NBSP", blank) + "</TD>")
            End If


            'nationality
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            If bNationality = False Then
                sb.AppendLine(reader.GetString(6).Replace("&nbsp", blank).ToUpper + "</TD>")
            Else
                sb.AppendLine(reader.GetString(7).Replace("&nbsp", blank).ToUpper + "</TD>")
            End If

            'religion
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            If bReligion = False Then
                If bRelDISAll = True Then
                    sb.AppendLine(reader.GetString(8).Replace("&nbsp", blank).ToUpper + "</TD>")
                Else
                    If reader.GetString(8) = "ISL" Then
                        sb.AppendLine(reader.GetString(8).Replace("&nbsp", blank).ToUpper + "</TD>")
                    Else
                        sb.AppendLine("OTH</TD>")
                    End If
                End If
            Else
                If bRelDISAll = True Then
                    sb.AppendLine(reader.GetString(8).Replace("&nbsp", blank).ToUpper + "</TD>")
                Else
                    If reader.GetString(8) = "ISL" Then
                        sb.AppendLine(reader.GetString(9).ToUpper.Replace("&NBSP", blank) + "</TD>")
                    Else
                        sb.AppendLine("OTH</TD>")
                    End If
                End If
            End If
            ''nahyan modified on 3mar2019 
            Dim pob As String = String.Empty
            If reader.GetString(29) = "" Then
                pob = "0"
            Else
                pob = reader.GetString(29)
            End If
            'place of birth
            If (pob = 172) Then
                If BB_bPOB_ABBRVN = True Then

                    If InStr(reader.GetString(30), "BU", CompareMethod.Text) Then
                        lstrPOB = "AUH"

                    ElseIf InStr(reader.GetString(30), "MAN", CompareMethod.Text) Then
                        lstrPOB = "AJ "
                    ElseIf InStr(reader.GetString(30), "BAI", CompareMethod.Text) Then
                        lstrPOB = "DXB "
                    ElseIf InStr(reader.GetString(30), "UJ", CompareMethod.Text) Then
                        lstrPOB = "FUJ "
                    ElseIf InStr(reader.GetString(30), "AS", CompareMethod.Text) Then
                        lstrPOB = "RAK "
                    ElseIf InStr(reader.GetString(30), "HAR", CompareMethod.Text) Then
                        lstrPOB = "SHJ "
                    ElseIf InStr(reader.GetString(30), "SHJ", CompareMethod.Text) Then
                        lstrPOB = "SHJ "
                    ElseIf InStr(reader.GetString(30), "Q", CompareMethod.Text) Then
                        lstrPOB = "UMQ "
                    ElseIf InStr(reader.GetString(30), "AIN", CompareMethod.Text) Then
                        lstrPOB = "AIN "
                    ElseIf InStr(reader.GetString(30), "OR", CompareMethod.Text) Then
                        lstrPOB = "KORF "
                    Else
                        lstrPOB = "UAE"
                    End If


                Else

                    If InStr(reader.GetString(30), "BU", CompareMethod.Text) Then
                        lstrPOB = "ABU DHABI "

                    ElseIf InStr(reader.GetString(30), "MAN", CompareMethod.Text) Then
                        lstrPOB = "AJMAN "
                    ElseIf InStr(reader.GetString(30), "BAI", CompareMethod.Text) Then
                        lstrPOB = "DUBAI "
                    ElseIf InStr(reader.GetString(30), "UJ", CompareMethod.Text) Then
                        lstrPOB = "FUJAIRAH "
                    ElseIf InStr(reader.GetString(30), "AS", CompareMethod.Text) Then
                        lstrPOB = "RAS AL KHAIMAH "
                    ElseIf InStr(reader.GetString(30), "HAR", CompareMethod.Text) Then
                        lstrPOB = "SHARJAH "
                    ElseIf InStr(reader.GetString(30), "SHJ", CompareMethod.Text) Then
                        lstrPOB = "SHARJAH "
                    ElseIf InStr(reader.GetString(30), "Q", CompareMethod.Text) Then
                        lstrPOB = "UMM AL QUWAIN "
                    ElseIf InStr(reader.GetString(30), "AIN", CompareMethod.Text) Then
                        lstrPOB = "AL AIN "
                    ElseIf InStr(reader.GetString(30), "OR", CompareMethod.Text) Then
                        lstrPOB = "KHOR FAKKAN "
                    Else
                        lstrPOB = "UAE"
                    End If




                End If


            Else
                If reader.GetString(10) <> "&nbsp" Then
                    lstrPOB = reader.GetString(10)
                Else
                    lstrPOB = "--"
                End If

            End If


            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(lstrPOB.ToUpper + "</TD>")


            'date of birth
            sb.AppendLine("<TD style=" + tdStyle + " align=left>")
            sb.AppendLine(Format(reader.GetDateTime(11), "dd/MM/yyyy") + "</TD>")

            dob = reader.GetDateTime(11)
            cutOffDate = Session("Cutoff_Age")

            intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))

            intHold = Int(DateDiff(DateInterval.Day, dob, cutOffDate))
            ageYear = Int(intHold / 365.2425)
            ageMonth = Math.Floor((intHold Mod 365.2425) / 30.436875)
            If reader.GetString(39) <> "" Then
                ageYear = reader.GetString(39)
                ageMonth = reader.GetString(40)
            End If

            'ageMonth = Math.Round(((intHold Mod 365.2425) / 30.436875), 0, MidpointRounding.ToEven)
            'age years
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(ageYear.ToString + "</TD>")

            'age month
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(ageMonth.ToString + "</TD>")

            'telephone
            sb.AppendLine("<TD style=" + tdStyle + " align=left>&nbsp")
            If Not Session("sbsuid") Is Nothing Then
                'If ((Session("sbsuid") = "121014") Or (Session("sbsuid") = "123006") Or (Session("sbsuid") = "123004") Or (Session("sbsuid") = "121009")) Then
                sb.AppendLine(Right(Replace(reader.GetString(12), "-", ""), 9) + "</TD>")
                'Else
                '    sb.AppendLine("0" + Trim(reader.GetString(12)).Replace("-", "").Replace("&nbsp", blank) + "</TD>")
                'End If
            Else
                sb.AppendLine("0" + Trim(reader.GetString(12)).Replace("-", "").Replace("&nbsp", blank) + "</TD>")
            End If

            'Code Added by Lijo
            ''pobox
            Dim lstrPOBOX As String = String.Empty
            If (Convert.ToString(reader("pobox_Country")) = "172") Or (Convert.ToString(reader("pobox_Country")) = "UAE") Then
                If InStr(Convert.ToString(reader("pobox_emir")), "AUH", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AUH"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "AIN", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AIN"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "AJ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "DXB", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "DXB"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "FUJ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "FUJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "RAK", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "RAK"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "SHJ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "SHJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "UMQ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "UMQ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "KORF", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "KORF"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "BU", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AUH"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "AIN", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AIN"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "MAN", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "BAI", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "DXB"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "UJ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "FUJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "AS", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "RAK"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "HAR", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "SHJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "Q", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "UMQ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "OR", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "KORF"
                Else
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & Convert.ToString(reader("School_Emir"))

                End If

            Else
                lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & Convert.ToString(reader("School_Emir"))

            End If

            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(lstrPOBOX + "&nbsp</TD>")

            'I/O
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            If (Session("sbsuid") = "125011") Then
                If reader.GetString(1) = "NEW" Then
                    If reader.GetString(14).ToUpper.Trim = "I" Or reader.GetString(14).ToUpper.Trim = "O" Then

                        sb.AppendLine(reader.GetString(14).ToUpper + "</TD>")
                    Else
                        sb.AppendLine(blank + "</TD>")
                    End If
                Else
                    sb.AppendLine(blank + "</TD>")
                End If
            Else
                If reader.GetString(14).ToUpper.Trim = "I" Or reader.GetString(14).ToUpper.Trim = "O" Then

                    sb.AppendLine(reader.GetString(14).ToUpper + "</TD>")
                Else
                    sb.AppendLine(blank + "</TD>")
                End If
            End If



            '' ''All kg1 result is new
            '' '' for other grades if last grade is not 'NEW' then show result
            '' '' For Internal transfer if the date of join is within the academicyear then pass

            'new pass readmit
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            If Trim(reader("STU_BB_TFRTYPE").ToString) <> "" Then
                sb.AppendLine(Trim(reader("STU_BB_TFRTYPE").ToString) + "</TD>")
            ElseIf (reader.GetString(14) = "I") And reader.GetString(27) = "Y" Then
                If (Session("sbsuid") = "125011") And reader.GetString(1) = "NEW" Then
                    sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=""NEW"" /></TD>")
                    'ElseIf (Session("sbsuid") = "123004") And reader.GetString(1) = "NEW" Then
                ElseIf (Session("sbsuid") = "151001") And reader.GetString(1) = "NEW" Then
                    sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=""NEW"" /></TD>")
                ElseIf (Session("sbsuid") = "125010") And reader.GetString(1) = "NEW" Then
                    sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=""NEW"" /></TD>")

                ElseIf (Session("sbsuid") <> "123006") And reader.GetString(1) = "NEW" Then
                    sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=""PASS"" /></TD>")
                Else
                    sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
                End If

            ElseIf grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                If (Session("sbsuid") = "125011") And reader.GetString(1) = "NEW" Then
                    sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=""NEW"" /></TD>")
                Else
                    sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
                End If
            ElseIf (Session("sbsuid") = "151001") And grd_id = "KG1" And reader.GetString(1) <> "NEW" Then

                sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            ElseIf (Session("sbsuid") = "141001") And grd_id = "KG1" And reader.GetString(1) <> "NEW" Then

                sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            ElseIf (Session("sbsuid") = "121012") And grd_id = "KG1" And reader.GetString(1) <> "NEW" Then

                sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            ElseIf (Session("sbsuid") = "125010") And grd_id = "KG1" And reader.GetString(1) <> "NEW" Then

                sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")

            Else
                If (Session("sbsuid") = "125011") And reader.GetString(1) = "NEW" Then
                    sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=""NEW"" /></TD>")
                Else
                    sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=""NEW"" /></TD>")
                End If
                'sb.AppendLine("NEW</TD>")
            End If

            'prev school 

            '' ''For all overseas admission, FRESH
            '' ''For Internal Transfer  � Name of the previous school 


            'If (reader.GetString(14) = "I") Then
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
            '    sb.AppendLine(reader.GetString(16).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    ''prev school country
            '    'sb.AppendLine("<TD style=" + tdStyle + " align=left>&nbsp")
            '    'sb.AppendLine(reader.GetString(17).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    'prev school curriculum
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            '    sb.AppendLine(reader.GetString(18).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    'last attendance date
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            '    sb.AppendLine(Format(reader.GetDateTime(19), "dd/MM/yyyy").Replace("01/01/1900", blank) + "</TD>")
            'Else
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
            '    sb.AppendLine("FRESH</TD>")

            '    ''prev school country
            '    'sb.AppendLine("<TD style=" + tdStyle + " align=left>&nbsp")
            '    'sb.AppendLine(reader.GetString(17).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    'prev school curriculum
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            '    sb.AppendLine("</TD>")

            '    'last attendance date
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            '    sb.AppendLine("</TD>")
            'End If





            If (Session("sbsuid") = "125011") And reader.GetString(1) <> "NEW" Then
                sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                sb.AppendLine("</TD>")

                'prev school curriculum
                sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                sb.AppendLine("</TD>")

                'last attendance date
                sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                sb.AppendLine("</TD>")
                'ElseIf (Session("sbsuid") = "151001") And reader.GetString(1) = "NEW" Then
                '    sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                '    sb.AppendLine("NEW</TD>")

                '    'prev school curriculum
                '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                '    sb.AppendLine("</TD>")

                '    'last attendance date
                '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                '    sb.AppendLine("</TD>")
            ElseIf (Session("sbsuid") = "125011") And reader.GetString(1) = "NEW" And (grd_id = "KG1" Or grd_id = "KG2" Or grd_id = "01") Then
                sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                sb.AppendLine("</TD>")

                'prev school curriculum
                sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                sb.AppendLine("</TD>")

                'last attendance date
                sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                sb.AppendLine("</TD>")
            Else
                Select Case reader.GetString(14)
                    Case "I"
                        If prevSchInt = "DATA" Then
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">")
                            sb.AppendLine(reader.GetString(16).ToUpper.Replace("&NBSP", blank) + "</TD>")
                            'prev school curriculum
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine(reader.GetString(18).ToUpper.Replace("&NBSP", blank) + "</TD>")

                            'last attendance date
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
                            sb.AppendLine(Format(reader.GetDateTime(19), "dd/MM/yyyy").Replace("01/01/1900", blank) + "</TD>")
                        ElseIf prevSchInt = "FRESH" Then
                            sb.AppendLine("<TD style=" + tdStyleSmall + " align=middle colspan=""3"">&nbsp")
                            sb.AppendLine("FRESH</TD>")

                            'prev school curriculum
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")

                            'last attendance date
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")
                        Else
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                            sb.AppendLine(blank + "</TD>")

                            'prev school curriculum
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")

                            'last attendance date
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")
                        End If

                    Case "O"
                        If prevSchOvr = "DATA" Then
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">")
                            sb.AppendLine(reader.GetString(16).ToUpper.Replace("&NBSP", blank) + "</TD>")
                            'prev school curriculum
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine(reader.GetString(18).ToUpper.Replace("&NBSP", blank) + "</TD>")

                            'last attendance date
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
                            sb.AppendLine(Format(reader.GetDateTime(19), "dd/MM/yyyy").Replace("01/01/1900", blank) + "</TD>")
                        ElseIf prevSchInt = "FRESH" Then
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                            sb.AppendLine("FRESH</TD>")

                            'prev school curriculum
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")

                            'last attendance date
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")
                        Else
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                            sb.AppendLine(blank + "</TD>")

                            'prev school curriculum
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")

                            'last attendance date
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")
                        End If

                    Case Else
                        If prevSchNew = "DATA" Then
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">")
                            sb.AppendLine(reader.GetString(16).ToUpper.Replace("&NBSP", blank) + "</TD>")
                            'prev school curriculum
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle >&nbsp")
                            sb.AppendLine(reader.GetString(18).ToUpper.Replace("&NBSP", blank) + "</TD>")

                            'last attendance date
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
                            sb.AppendLine(Format(reader.GetDateTime(19), "dd/MM/yyyy").Replace("01/01/1900", blank) + "</TD>")
                        ElseIf prevSchInt = "FRESH" Then
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                            sb.AppendLine("FRESH</TD>")

                            'prev school curriculum
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")

                            'last attendance date
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")
                        Else
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                            sb.AppendLine(blank + "</TD>")

                            'prev school curriculum
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")

                            'last attendance date
                            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                            sb.AppendLine("</TD>")
                        End If


                End Select
            End If

            'date of join
            sb.AppendLine("<TD style=" + tdStyle + " align=left>")
            sb.AppendLine(Format(reader.GetDateTime(20), "dd/MM/yyyy") + "</TD>")

            'join grade
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(reader.GetString(21).ToUpper.Replace("&NBSP", blank) + "</TD>")


            If (bIncludeTC = True And reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date) _
            Or (bIncludeSO = True And reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date) Then
                'leaving date
                sb.AppendLine("<TD style=" + tdStyle + ">" + Format(reader.GetDateTime(38).Date, "dd/MMM/yyyy") + "</TD>")

                'leaving reason
                sb.AppendLine("<TD style=" + tdStyle + ">" + reader.GetString(25).ToUpper + "</TD>")

            Else
                'leaving date
                sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

                'leaving reason
                sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")
            End If

            'result annual
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'reexam
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'remarks
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp;" + reader.GetString(41).ToUpper + "</TD>")

            sb.AppendLine("</TR>")



            recordCount += 1

            If recordCount = pageSize And bHdrAllPages = True Then

                sb.AppendLine("</TABLE>")

                'sb.AppendLine("<BR>")

                sb.AppendLine(strFooter)

                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")




                recordCount = 0

            End If

            If recordCount = pageSize And bHdrAllPages = False Then
                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                recordCount = 0
            End If
            If recordCount <> pageSize And bHdrAllPages = False Then
                sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
            End If


        End While
        Dim ss As String = "<div style=""page-break-after:always"">&nbsp;</div>"



        If recordCount <> 0 Then
            Dim remainingLines As Integer = pageSize - recordCount
            Dim i As Integer
            For i = 0 To remainingLines - 1
                sb.AppendLine(EmptyLines)
            Next

            If bHdrAllPages = True Then
                sb.AppendLine("</TABLE>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine(strFooter)
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
            End If

        End If

        If bHdrAllPages = False Then
            sb.AppendLine("</TABLE>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")
            sb.AppendLine(strFooter)
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
        End If
        reader.Close()

        ' Response.Write(sb.ToString)


        Return sb.ToString
    End Function
    Function GetBlank(ByVal grd_id As String, ByVal sct_id As String) As String

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim bHdrAllPages As Boolean = False
        Dim bFtrAllPages As Boolean = False
        Dim bNationality As Boolean = False
        Dim bReligion As Boolean = False
        Dim bRelDISAll As Boolean = False
        Dim bGender As Boolean = False
        Dim pageSize As Integer
        Dim bRedLine As Boolean = False
        Dim bBlank As Boolean = False
        Dim bIncludeTC As Boolean = False
        Dim bIncludeSO As Boolean = False
        Dim bShowParent1 As Boolean = False
        Dim bShowParent2 As Boolean = False
        Dim bAutoPage_No As Boolean = False
        Dim bBlankSheet As Boolean = False

        Dim recordCount As Integer = 0
        Dim pages As Integer

        Dim strHeaders As String() = GetHeadersAndFooter(sct_id)

        Dim strHeader As String
        Dim strFooter As String = strHeaders(1)
        Dim strTblHeader As String = strHeaders(2)
        Dim sb As New StringBuilder

        Dim ageYear As Integer = 0
        Dim ageMonth As Integer = 0
        Dim cutOffDate As Date
        Dim dob As Date

        Dim prevSchInt As String = ""
        Dim prevSchOvr As String = ""
        Dim prevSchNew As String = ""

        Dim tdStyle As String
        Dim tdStyleArabic As String

        Dim intHold As Integer

        Dim stu_id As String
        Dim stuLink As String = ""
        Dim srNo As Integer = 0

        Dim stuName As String = ""
        Dim stuNames As String()
        Dim parentName As String = ""

        Dim str_query As String = "SELECT BB_HDR_ALLPAGES,BB_FTR_ALLPAGES,BB_NATIONALITY," _
                                & " BB_RELIGION,BB_REL_DISPLAYALL,BB_GENDER,BB_PAGESIZE," _
                                & " BB_REDLINE,BB_BLANK_REPLACE,ISNULL(BB_INCLUDETC,'FALSE'),ISNULL(BB_INCLUDESO,'FALSE')," _
                                & " ISNULL(BB_INTPREVSCHOOL,'DATA'), " _
                                & " ISNULL(BB_OVRPREVSCHOOL,'BLANK'),ISNULL(BB_NEWPREVSCHOOL,'BLANK'),ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE'),  " _
                                & " ISNULL(BB_AUTOPAGE_NO,'FALSE'),ISNULL(BB_BLANKSHEET,'FALSE') FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"


        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            With reader
                bHdrAllPages = .GetBoolean(0)
                bFtrAllPages = .GetBoolean(1)
                bNationality = .GetBoolean(2)
                bReligion = .GetBoolean(3)
                bRelDISAll = .GetBoolean(4)
                bGender = .GetBoolean(5)
                pageSize = .GetValue(6)
                bRedLine = .GetBoolean(7)
                bBlank = .GetBoolean(8)
                bIncludeTC = .GetBoolean(9)
                bIncludeSO = .GetBoolean(10)
                prevSchInt = .GetString(11)
                prevSchOvr = .GetString(12)
                prevSchNew = .GetString(13)
                bShowParent1 = .GetBoolean(14)
                bShowParent2 = .GetBoolean(15)
                ViewState("AutoPage_No") = .GetBoolean(16)
                ViewState("BlankSheet") = .GetBoolean(17)

            End With

        End While
        reader.Close()

        str_query = "SELECT Top 1 ISNULL(STU_BLUEID,'&nbsp'),ISNULL((SELECT GRM_BB_DISPLAY+' '+SCT_DESCR AS LastGrade " _
                   & " FROM  STUDENT_PROMO_S INNER JOIN  GRADE_BSU_M ON STUDENT_PROMO_S.STP_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN" _
                   & " SECTION_M ON STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID " _
                   & " WHERE STUDENT_PROMO_S.STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STUDENT_PROMO_S.STP_STU_ID =A.STU_ID),'NEW') AS LastGrade," _
                   & " ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,'')," _
                   & " ISNULL(STU_GENDER,'&nbsp'),STU_GENDERDETAIL=CASE STU_GENDER WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END," _
                   & " ISNULL(C.CTY_SHORT,'&nbsp'),ISNULL(C.CTY_NATIONALITY,'&nbsp'),ISNULL(RLG_ID,0),ISNULL(RLG_DESCR,''),POB=CASE STU_COB WHEN 172 THEN ISNULL(STU_POB,'&nbsp')+'/UAE' ELSE ISNULL(J.CTY_DESCR,'&nbsp') END ,ISNULL(STU_DOB,'&nbsp'),isnull(STU_EMGCONTACT,'&nbsp')," _
                   & " STU_POBOX=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMPOBOX,'&nbsp') WHEN 'M' THEN ISNULL(STS_MCOMPOBOX,'&nbsp') ELSE ISNULL(STS_GCOMPOBOX,'&nbsp') END," _
                   & " ISNULL(STU_TFRTYPE,'&nbsp'),  ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
                   & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
                   & " (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'NEW')" _
                   & " ,ISNULL(STU_PREVSCHI,'&nbsp'),ISNULL(STU_PREVSCHI_COUNTRY,'&nbsp'),ISNULL(CLM_DESCR,'&nbsp'), " _
                   & " ISNULL(STU_PREVSCHI_LASTATTDATE,'01/01/1900'),ISNULL(STU_MINDOJ,''),ISNULL(GRM_BB_DISPLAY,STU_GRD_ID_JOIN),ISNULL(I.TCM_STU_ID,0),ISNULL(TCM_TCSO,''),ISNULL(TCM_LASTATTDATE,'2100-01-01'),ISNULL(TCR_DESCR,'&nbsp'),STU_ID ," _
                   & " CASE WHEN STU_MINDOJ BETWEEN ACD_STARTDT AND ACD_ENDDT THEN 'Y' ELSE 'N' END, " _
                   & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                   & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                   & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END " _
                   & " FROM STUDENT_M AS A INNER JOIN" _
                   & " STUDENT_D AS B ON A.STU_ID = B.STS_STU_ID" _
                   & " INNER JOIN COUNTRY_M AS C  ON C.CTY_ID = A.STU_NATIONALITY" _
                   & " INNER JOIN RELIGION_M AS F ON A.STU_RLG_ID = F.RLG_ID" _
                   & " LEFT OUTER JOIN CURRICULUM_M AS G ON A.STU_PREVSCHI_CLM=G.CLM_ID" _
                   & " LEFT OUTER JOIN GRADE_BSU_M AS H ON A.STU_GRD_ID_JOIN=H.GRM_GRD_ID AND A.STU_ACD_ID=H.GRM_ACD_ID AND A.STU_STM_ID=H.GRM_STM_ID AND A.STU_SHF_ID=H.GRM_SHF_ID" _
                   & " LEFT OUTER JOIN TCM_M AS I ON A.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL" _
                   & " LEFT OUTER JOIN TC_REASONS_M AS K ON I.TCM_REASON=K.TCR_CODE" _
                   & " LEFT OUTER JOIN COUNTRY_M AS J  ON J.CTY_ID = A.STU_COB" _
                   & " INNER JOIN ACADEMICYEAR_D AS P ON A.STU_ACD_ID=P.ACD_ID " _
                   & " WHERE STU_MINLIST='Regular'" _
                   & "  AND STU_CurrStatus NOT IN ('CN','TF') "
        If GetBSU_bbsct_status() = True Then
            str_query = str_query & " AND (STU_BB_SECTION_DES = '" + sct_id + "') "
        Else
            str_query = str_query & " AND (STU_SCT_ID = " + sct_id + ") "
        End If
        If bIncludeTC = False And bIncludeSO = False Then
            str_query += " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) "
        End If

        If ViewState("MainMnu_code") = "S100450" Then
            'str_query += "AND STU_ID IN(" + Session("BlueBookStuIds") + ")"
            str_query += "AND STU_ID IN('')"
        End If


        str_query += " ORDER BY STU_PASPRTNAME"
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)


        Dim records As Integer
        If ViewState("MainMnu_code") = "S100450" Then
            'If Session("BlueBookRecords") <> "" Then
            '    records = CType(Session("BlueBookRecords"), Integer)
            'Else
            records = 0
            'End If
        Else
            str_query = " SELECT COUNT(STU_ID) FROM STUDENT_M WHERE STU_CURRSTATUS NOT IN ('CN','TF') AND  STU_MINLIST='Regular'"

            If GetBSU_bbsct_status() = True Then
                str_query = str_query & " AND (STU_BB_SECTION_DES = '" + sct_id + "') "
            Else
                str_query = str_query & " AND (STU_SCT_ID = " + sct_id + ") "
            End If


        records = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        End If

        Dim blank As String = "&nbsp"
        If bBlank = True Then
            blank = "-"
        End If


        Dim pageNo As Integer = 1
        If bHdrAllPages = False Then
            strHeader = strHeaders(0)

            strHeader = strHeader.Replace("%pgno%", "")
            sb.AppendLine(strHeader)
        End If


        While reader.Read

            If recordCount = 0 And bHdrAllPages = True Then
                If ViewState("FirstPage") <> "1" Then
                    sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
                End If
                ViewState("FirstPage") = "0"
                strHeader = strHeaders(0)

                If ViewState("AutoPage_No") = True Then
                    strHeader = strHeader.Replace("%pgno%", pageNo)
                Else
                    strHeader = strHeader.Replace("%pgno%", "&nbsp")
                End If

                sb.AppendLine(strHeader)
                pageNo += 1
            End If

            If recordCount = 0 Then
                sb.AppendLine(strTblHeader)
            End If

            srNo += 1

            tdStyle = """FONT: bold 7pt verdana,timesroman,garamond"""
            tdStyleArabic = """FONT: bold 10pt verdana,timesroman,garamond"""

            If bRedLine = True Then
                If srNo = records Then
                    tdStyle = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 7pt verdana,timesroman,garamond"""
                    tdStyleArabic = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 9pt verdana,timesroman,garamond"""
                End If
            End If


            If bIncludeTC = False And bIncludeSO = False Then
                sb.AppendLine("<TR style=""height:30px"">")
            ElseIf bIncludeTC = True And reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=green>")
            ElseIf bIncludeSO = True And reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date Then
                sb.AppendLine("<TR style=""height:30px"" bgColor=red>")
            Else
                sb.AppendLine("<TR style=""height:30px"">")
            End If

            'serial no
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'bluebook id
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")
            'last grade
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'name in english
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")
            ' sb.AppendLine(reader.GetString(2).ToUpper.Replace("&nbsp", blank) + "</a></TD>")

            'name in arabic
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")


            'gender
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")


            'nationality
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'religion
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'place of birth
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")


            'date of birth
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            dob = reader.GetDateTime(11)
            cutOffDate = Session("Cutoff_Age")

            intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))
            intHold = Int(DateDiff(DateInterval.Day, dob, cutOffDate))
            ageYear = Int(intHold / 365)
            ageMonth = Math.Floor((intHold Mod 365) / 30.436875)

            'age years
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'age month
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'telephone
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'pobox
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'I/O
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")


            '' ''All kg1 result is new
            '' '' for other grades if last grade is not 'NEW' then show result
            '' '' For Internal transfer if the date of join is within the academicyear then pass

            'new pass readmit
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'prev school 

            '' ''For all overseas admission, FRESH
            '' ''For Internal Transfer  � Name of the previous school 


            'If (reader.GetString(14) = "I") Then
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
            '    sb.AppendLine(reader.GetString(16).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    ''prev school country
            '    'sb.AppendLine("<TD style=" + tdStyle + " align=left>&nbsp")
            '    'sb.AppendLine(reader.GetString(17).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    'prev school curriculum
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            '    sb.AppendLine(reader.GetString(18).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    'last attendance date
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            '    sb.AppendLine(Format(reader.GetDateTime(19), "dd/MM/yyyy").Replace("01/01/1900", blank) + "</TD>")
            'Else
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
            '    sb.AppendLine("FRESH</TD>")

            '    ''prev school country
            '    'sb.AppendLine("<TD style=" + tdStyle + " align=left>&nbsp")
            '    'sb.AppendLine(reader.GetString(17).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    'prev school curriculum
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            '    sb.AppendLine("</TD>")

            '    'last attendance date
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            '    sb.AppendLine("</TD>")
            'End If







            sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
            sb.AppendLine(blank + "</TD>")

            'prev school curriculum
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            sb.AppendLine("</TD>")

            'last attendance date
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            sb.AppendLine("</TD>")



            'date of join
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'join grade
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")


            If (bIncludeTC = True And reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date) _
            Or (bIncludeSO = True And reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date) Then
                'leaving date
                sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

                'leaving reason
                sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            Else
                'leaving date
                sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

                'leaving reason
                sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")
            End If

            'result annual
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'reexam
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'remarks
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            sb.AppendLine("</TR>")



            recordCount += 1

            If recordCount = pageSize And bHdrAllPages = True Then

                sb.AppendLine("</TABLE>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")

                sb.AppendLine(strFooter)

                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")




                recordCount = 0

            End If

            If recordCount = pageSize And bHdrAllPages = False Then
                sb.AppendLine("</TABLE>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                recordCount = 0
            End If
            If recordCount <> pageSize And bHdrAllPages = False Then
                sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
            End If


        End While
        Dim ss As String = "<div style=""page-break-after:always"">&nbsp;</div>"



        If recordCount <> 0 Then
            Dim remainingLines As Integer = pageSize - recordCount
            Dim i As Integer
            For i = 0 To remainingLines - 1
                sb.AppendLine(EmptyLines)
            Next

            If bHdrAllPages = True Then
                sb.AppendLine("</TABLE>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine(strFooter)
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
            End If

        End If

        If bHdrAllPages = False Then
            sb.AppendLine("</TABLE>")
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")
            sb.AppendLine("<BR>")
            sb.AppendLine(strFooter)
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
        End If
        reader.Close()

        ' Response.Write(sb.ToString)


        Return sb.ToString
    End Function

    Function ReprintBlueBookForAllGrades(ByVal grd_id As String, ByVal sct_id As String) As String

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim bHdrAllPages As Boolean = False
        Dim bFtrAllPages As Boolean = False
        Dim bNationality As Boolean = False
        Dim bReligion As Boolean = False
        Dim bRelDISAll As Boolean = False
        Dim bGender As Boolean = False
        Dim pageSize As Integer
        Dim bRedLine As Boolean = False
        Dim bBlank As Boolean = False
        Dim bIncludeTC As Boolean = False
        Dim bIncludeSO As Boolean = False
        Dim bShowParent1 As Boolean = False
        Dim bShowParent2 As Boolean = False
        Dim bAutoPage_No As Boolean = False
        Dim bBlankSheet As Boolean = False
        Dim BB_bPOB_ABBRVN As Boolean = False

        Dim recordCount As Integer = 0
        Dim pages As Integer

        'Dim strHeaders As String() = GetHeadersAndFooter(sct_id)

        'Dim strHeader As String
        ' Dim strFooter As String = strHeaders(1)
        Dim strTblHeader As String

        Dim str_query As String = "SELECT BB_TBL_HDR FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"
        Dim strHeader As String
        Dim strFooter As String
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While reader.Read
            strTblHeader = reader.GetString(0)
        End While
        reader.Close()
        strTblHeader = strTblHeader.Replace("''", "'")



        Dim sb As New StringBuilder
        sb.AppendLine("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        sb.AppendLine("<HTML><HEAD><TITLE>:::: Blue Book ::::</TITLE>")
        sb.AppendLine("<META http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        sb.AppendLine("<META content=""MSHTML 6.00.2900.3268"" name=GENERATOR></HEAD>")
        sb.AppendLine("<BODY>")

        Dim ageYear As Integer = 0
        Dim ageMonth As Integer = 0
        Dim cutOffDate As Date
        Dim dob As Date

        Dim prevSchInt As String = ""
        Dim prevSchOvr As String = ""
        Dim prevSchNew As String = ""

        Dim tdStyle As String
        Dim tdStyleArabic As String

        Dim intHold As Integer

        Dim stu_id As String
        Dim stuLink As String = ""
        Dim srNo As Integer = 0

        Dim stuName As String = ""
        Dim stuNames As String()
        Dim parentName As String = ""

        str_query = "SELECT BB_HDR_ALLPAGES,BB_FTR_ALLPAGES,BB_NATIONALITY," _
                                & " BB_RELIGION,BB_REL_DISPLAYALL,BB_GENDER,BB_PAGESIZE," _
                                & " BB_REDLINE,BB_BLANK_REPLACE,ISNULL(BB_INCLUDETC,'FALSE'),ISNULL(BB_INCLUDESO,'FALSE')," _
                                & " ISNULL(BB_INTPREVSCHOOL,'DATA'), " _
                                & " ISNULL(BB_OVRPREVSCHOOL,'BLANK'),ISNULL(BB_NEWPREVSCHOOL,'BLANK'),ISNULL(BB_SHOWPARENT1,'FALSE'),ISNULL(BB_SHOWPARENT2,'FALSE'),  " _
                                & " ISNULL(BB_AUTOPAGE_NO,'FALSE'),ISNULL(BB_BLANKSHEET,'FALSE') ,ISNULL(BB_bPOB_ABBRVN,'FALSE') FROM BSU_BLUEBOOK WHERE BB_BSU_ID='" + Session("sbsuid") + "'"


        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        While reader.Read
            With reader
                bHdrAllPages = .GetBoolean(0)
                bFtrAllPages = .GetBoolean(1)
                bNationality = .GetBoolean(2)
                bReligion = .GetBoolean(3)
                bRelDISAll = .GetBoolean(4)
                bGender = .GetBoolean(5)
                pageSize = .GetValue(6)
                bRedLine = .GetBoolean(7)
                bBlank = .GetBoolean(8)
                bIncludeTC = .GetBoolean(9)
                bIncludeSO = .GetBoolean(10)
                prevSchInt = .GetString(11)
                prevSchOvr = .GetString(12)
                prevSchNew = .GetString(13)
                bShowParent1 = .GetBoolean(14)
                bShowParent2 = .GetBoolean(15)
                ViewState("AutoPage_No") = .GetBoolean(16)
                ViewState("BlankSheet") = .GetBoolean(17)
                BB_bPOB_ABBRVN = .GetBoolean(18)

            End With

        End While
        reader.Close()

        str_query = "SELECT distinct ISNULL(STU_BLUEID,'&nbsp'),ISNULL((SELECT GRM_BB_DISPLAY+' '+SCT_DESCR AS LastGrade " _
                   & " FROM  STUDENT_PROMO_S INNER JOIN  GRADE_BSU_M ON STUDENT_PROMO_S.STP_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN" _
                   & " SECTION_M ON STUDENT_PROMO_S.STP_SCT_ID = SECTION_M.SCT_ID " _
                   & " WHERE STUDENT_PROMO_S.STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STUDENT_PROMO_S.STP_STU_ID =A.STU_ID),'NEW') AS LastGrade," _
                   & " ISNULL(STU_PASPRTNAME,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'')),STU_ARABICNAME=ISNULL(STU_FIRSTNAMEARABIC,'')+'&nbsp'+ISNULL(STU_MIDNAMEARABIC,' ')+'&nbsp'+ISNULL(STU_LASTNAMEARABIC,'')," _
                   & " ISNULL(STU_GENDER,'&nbsp'),STU_GENDERDETAIL=CASE STU_GENDER WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' END," _
                   & " ISNULL(C.CTY_SHORT,'&nbsp'),ISNULL(C.CTY_NATIONALITY,'&nbsp'),ISNULL(RLG_ID,0),ISNULL(RLG_DESCR,''),POB=CASE STU_COB WHEN 172 THEN ISNULL(STU_POB,'&nbsp')+'/UAE' ELSE ISNULL(J.CTY_DESCR,'&nbsp') END ,ISNULL(STU_DOB,'&nbsp'),isnull(STU_EMGCONTACT,'&nbsp')," _
                   & " STU_POBOX=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMPOBOX,'&nbsp') WHEN 'M' THEN ISNULL(STS_MCOMPOBOX,'&nbsp') ELSE ISNULL(STS_GCOMPOBOX,'&nbsp') END," _
                   & " ISNULL(STU_TFRTYPE,'&nbsp'), ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
                   & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
                   & " (SELECT STP_RESULT FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'NEW')" _
                   & " ,ISNULL(STU_PREVSCHI,'&nbsp'),ISNULL(STU_PREVSCHI_COUNTRY,'&nbsp'),ISNULL(CLM_DESCR,'&nbsp'), " _
                   & " ISNULL(STU_PREVSCHI_LASTATTDATE,'01/01/1900'),ISNULL(STU_MINDOJ,''),ISNULL(GRM_BB_DISPLAY,STU_GRD_ID_JOIN),ISNULL(I.TCM_STU_ID,0),ISNULL(TCM_TCSO,''),ISNULL(STU_LEAVEDATE,'2100-01-01'),ISNULL(TCR_DESCR,'&nbsp'),STU_ID ," _
                   & " CASE WHEN STU_MINDOJ BETWEEN ACD_STARTDT AND ACD_ENDDT THEN 'Y' ELSE 'N' END, " _
                   & " STU_PARENT=CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') " _
                   & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') " _
                   & " ELSE ISNULL(STS_GFIRSTNAME,'')+' '+ISNULL(STS_GMIDNAME,'')+' '+ISNULL(STS_GLASTNAME,'') END " _
                   & " ,CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMCOUNTRY,'') WHEN 'M' " _
                   & " THEN ISNULL(STS_MCOMCOUNTRY,'') WHEN 'G' THEN ISNULL(STS_GCOMCOUNTRY,'') END As pobox_Country " _
                   & " ,CASE STU_PRIMARYCONTACT WHEN 'F' THEN ISNULL(STS_FCOMCITY,'&nbsp') WHEN 'M' " _
                   & " THEN ISNULL(STS_MCOMCITY,'&nbsp') WHEN 'G' THEN ISNULL(STS_GCOMCITY,'&nbsp') END As pobox_emir, " _
                   & " ISNULL(H.GRM_BB_DISPLAY,STU_GRD_ID_JOIN) AS JOIN_GRD " _
                   & " ,(Select BSU_CITY from dbo.BUSINESSUNIT_M where bsu_id=a.stu_bsu_id) as School_Emir " _
                   & " ,ISNULL(I.TCM_TCTYPE,'0') as TC_TYPE ,STU_PASPRTNAME,ISNULL((CASE WHEN ((SELECT COUNT(STP_ID) FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString _
                   & " AND STP_STU_ID =A.STU_ID)=0) THEN (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =A.STU_ACD_ID AND STP_STU_ID =A.STU_ID) ELSE " _
                   & " (SELECT isnull(STP_MINLIST,'Regular') FROM  STUDENT_PROMO_S WHERE STP_ACD_ID =" + Session("Prev_ACD_ID").ToString + " AND STP_STU_ID =A.STU_ID) END),'Regular') as STP_MINLIST" _
                   & " FROM STUDENT_M AS A INNER JOIN " _
                   & " STUDENT_D AS B ON A.STU_SIBLING_ID = B.STS_STU_ID" _
                   & " INNER JOIN COUNTRY_M AS C  ON C.CTY_ID = A.STU_NATIONALITY" _
                   & " INNER JOIN RELIGION_M AS F ON A.STU_RLG_ID = F.RLG_ID" _
                   & " LEFT OUTER JOIN CURRICULUM_M AS G ON A.STU_PREVSCHI_CLM=G.CLM_ID" _
                   & " LEFT OUTER JOIN GRADE_BSU_M AS H ON A.STU_GRD_ID_JOIN=H.GRM_GRD_ID AND A.STU_ACD_ID=H.GRM_ACD_ID AND A.STU_STM_ID=H.GRM_STM_ID AND A.STU_SHF_ID=H.GRM_SHF_ID" _
                   & " LEFT OUTER JOIN TCM_M AS I ON A.STU_ID=I.TCM_STU_ID AND TCM_CANCELDATE IS NULL AND TCM_TC_ID IS NULL" _
                   & " LEFT OUTER JOIN TC_REASONS_M AS K ON I.TCM_REASON=K.TCR_CODE" _
                   & " LEFT OUTER JOIN COUNTRY_M AS J  ON J.CTY_ID = A.STU_COB" _
                   & " INNER JOIN ACADEMICYEAR_D AS P ON A.STU_ACD_ID=P.ACD_ID " _
                   & " WHERE STU_MINLIST='Regular'" _
                   & "  AND STU_CurrStatus NOT IN ('CN','TF')"

        If bIncludeTC = False And bIncludeSO = False Then
            str_query += " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) "
        End If

        If ViewState("MainMnu_code") = "S100450" Then
            str_query += "AND STU_ID IN(" + Session("BlueBookStuIds") + ")"
        End If


        str_query += " ORDER BY STU_PASPRTNAME"
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)


        Dim records As Integer
        If ViewState("MainMnu_code") = "S100450" Then
            records = CType(Session("BlueBookRecords"), Integer)
        Else
            str_query = " SELECT COUNT(STU_ID) FROM STUDENT_M WHERE STU_CurrStatus NOT IN ('CN','TF') AND STU_SCT_ID=" + sct_id + " AND STU_MINLIST='Regular'"
            records = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        End If

        Dim blank As String = "&nbsp"
        If bBlank = True Then
            blank = "-"
        End If


        Dim pageNo As Integer = 1
        If bHdrAllPages = False Then
            '  strHeader = strHeaders(0)
            'strHeader = strHeader.Replace("%pgno%", pageNo)
            'sb.AppendLine(strHeader)
        End If


        While reader.Read

            If recordCount = 0 And bHdrAllPages = True Then
                If ViewState("FirstPage") <> "1" Then
                    sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
                End If
                ViewState("FirstPage") = "0"
                'strHeader = strHeaders(0)
                '  strHeader = strHeader.Replace("%pgno%", pageNo)
                '  sb.AppendLine(strHeader)
                ' pageNo += 1
            End If

            If recordCount = 0 Then
                sb.AppendLine(strTblHeader)
            End If

            srNo += 1

            tdStyle = """FONT: bold 7pt verdana,timesroman,garamond"""
            tdStyleArabic = """FONT: bold 12pt verdana,timesroman,garamond"""

            If bRedLine = True Then
                If srNo = records Then
                    tdStyle = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 7pt verdana,timesroman,garamond"""
                    tdStyleArabic = """BORDER-BOTTOM-WIDTH: 3px; BORDER-BOTTOM-COLOR: red; FONT: bold 9pt verdana,timesroman,garamond"""
                End If
            End If


            'If bIncludeTC = False And bIncludeSO = False Then
            '    sb.AppendLine("<TR style=""height:30px"">")
            'ElseIf bIncludeTC = True And reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date Then
            '    sb.AppendLine("<TR style=""height:30px"" bgColor=green>")
            'ElseIf bIncludeSO = True And reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date Then
            '    sb.AppendLine("<TR style=""height:30px"" bgColor=red>")
            'Else
            '    sb.AppendLine("<TR style=""height:30px"">")
            'End If


            If bIncludeTC = False And bIncludeSO = False Then
                sb.AppendLine("<TR style=""height:30px"">")
            ElseIf bIncludeTC = True And reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date Then '24 LEAVE DATE
                If Convert.ToString(reader("TC_TYPE")) = "1" Or Convert.ToString(reader("TC_TYPE")) = "3" Then
                    sb.AppendLine("<TR style=""height:30px"" bgColor=" + ViewState("TCWITHIN_UAE_COL") + ">")
                ElseIf Convert.ToString(reader("TC_TYPE")) = "2" Or Convert.ToString(reader("TC_TYPE")) = "4" Then

                    sb.AppendLine("<TR style=""height:30px"" bgColor=" + ViewState("TCOUTSIDE_UAE_COL") + ">")
                Else
                    sb.AppendLine("<TR style=""height:30px"" bgColor=green>")
                End If
            ElseIf bIncludeSO = True And reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date Then

                sb.AppendLine("<TR style=""height:30px"" bgColor=" + ViewState("SO_COL") + ">")


            Else
                sb.AppendLine("<TR style=""height:30px"">")
            End If








            'serial no
            sb.AppendLine("<TD style=" + tdStyle + " border=""1"" align=middle>")
            sb.AppendLine(srNo.ToString + "</TD>")

            'bluebook id
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(reader.GetString(0).Replace("&nbsp", blank) + "</TD>")

            'last grade
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")

            If StrConv(Trim(reader("STP_MINLIST").ToString), VbStrConv.ProperCase) <> "Regular" Then
                sb.AppendLine("NEW" + "</TD>")
            Else
                sb.AppendLine(StrConv(reader.GetString(1).Replace("&nbsp", blank), VbStrConv.Uppercase) + "</TD>")
            End If



            'name in english
            '' '' If the name has only one or two names then add parentname based on bb_showparent1 or bb_showparent2
            stu_id = Encr_decrData.Encrypt(reader.GetValue(26))
            If ViewState("MainMnu_code") = "S100190" Then
                stuLink = "../Students/StudRecordEdit.aspx?MainMnu_code=s08/lw50oAY=&datamode=4xCdg/cr4Xw=&viewid=" + stu_id
            Else
                stuLink = "../Students/StudRecordEdit.aspx?MainMnu_code=SbWsUfLyXSM=&datamode=4xCdg/cr4Xw=&viewid=" + stu_id
            End If

            sb.AppendLine("<TD style=" + tdStyle + " align=left><a STYLE=""text-decoration:none;color:black"" href=" + stuLink + ">")

            stuName = reader.GetString(2).Trim

            stuName = stuName.Replace("  ", " ")
            stuNames = stuName.Split(" ")
            parentName = reader.GetString(28)

            If (bShowParent1 = True And stuNames.Length = 1) Or (bShowParent2 = True And stuNames.Length = 2) Then
                stuName = stuName + " / " + parentName
            End If

            sb.AppendLine(StrConv(stuName, VbStrConv.Uppercase) + "</a></TD>")
            ' sb.AppendLine(reader.GetString(2).ToUpper.Replace("&nbsp", blank) + "</a></TD>")

            'name in arabic
            sb.AppendLine("<TD style=" + tdStyleArabic + " align=right>")
            sb.AppendLine(reader.GetString(3).Replace("&nbsp", "") + "</TD>")


            'gender
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            If bGender = False Then
                sb.AppendLine(reader.GetString(4).ToUpper.Replace("&NBSP", blank) + "</TD>")
            Else
                sb.AppendLine(reader.GetString(5).ToUpper.Replace("&NBSP", blank) + "</TD>")
            End If


            'nationality
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            If bNationality = False Then
                sb.AppendLine(reader.GetString(6).Replace("&nbsp", blank).ToUpper + "</TD>")
            Else
                sb.AppendLine(reader.GetString(7).Replace("&nbsp", blank).ToUpper + "</TD>")
            End If

            'religion
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            If bReligion = False Then
                If bRelDISAll = True Then
                    sb.AppendLine(reader.GetString(8).Replace("&nbsp", blank).ToUpper + "</TD>")
                Else
                    If reader.GetString(8) = "ISL" Then
                        sb.AppendLine(reader.GetString(8).Replace("&nbsp", blank).ToUpper + "</TD>")
                    Else
                        sb.AppendLine("OTH</TD>")
                    End If
                End If
            Else
                If bRelDISAll = True Then
                    sb.AppendLine(reader.GetString(8).Replace("&nbsp", blank) + "</TD>")
                Else
                    If reader.GetString(8) = "ISL" Then
                        sb.AppendLine(reader.GetString(9).ToUpper.Replace("&NBSP", blank).ToUpper + "</TD>")
                    Else
                        sb.AppendLine("OTH</TD>")
                    End If
                End If
            End If

            'place of birth
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(reader.GetString(10).ToUpper.Replace("&NBSP", blank) + "</TD>")


            'date of birth
            sb.AppendLine("<TD style=" + tdStyle + " align=left>")
            sb.AppendLine(Format(reader.GetDateTime(11), "dd/MM/yyyy") + "</TD>")

            dob = reader.GetDateTime(11)
            cutOffDate = Session("Cutoff_Age")

            intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate)) + (cutOffDate < DateSerial(Year(cutOffDate), Month(cutOffDate), Day(cutOffDate)))
            intHold = Int(DateDiff(DateInterval.Month, dob, cutOffDate))
            ageYear = Int(intHold / 12)
            ageMonth = intHold Mod 12

            'age years
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(ageYear.ToString + "</TD>")

            'age month
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(ageMonth.ToString + "</TD>")

            'telephone
            sb.AppendLine("<TD style=" + tdStyle + " align=left>&nbsp")
            If Not Session("sbsuid") Is Nothing Then
                If ((Session("sbsuid") = "121014") Or (Session("sbsuid") = "123006")) Then
                    sb.AppendLine(Trim(reader.GetString(12)).Replace("-", "").Replace("00971", "971").Replace("0971", "971").Replace("971", "").Replace("&nbsp", blank) + "</TD>")
                Else
                    sb.AppendLine("0" + Trim(reader.GetString(12)).Replace("-", "").Replace("&nbsp", blank) + "</TD>")
                End If
            Else
                sb.AppendLine("0" + Trim(reader.GetString(12)).Replace("-", "").Replace("&nbsp", blank) + "</TD>")
            End If

            'pobox
            Dim lstrPOBOX As String = String.Empty
            If (Convert.ToString(reader("pobox_Country")) = "172") Or (Convert.ToString(reader("pobox_Country")) = "UAE") Then
                If InStr(Convert.ToString(reader("pobox_emir")), "AUH", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AUH"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "AIN", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AIN"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "AJ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "DXB", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "DXB"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "FUJ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "FUJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "RAK", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "RAK"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "SHJ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "SHJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "UMQ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "UMQ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "KORF", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "KORF"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "BU", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AUH"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "AIN", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AIN"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "MAN", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "AJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "BAI", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "DXB"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "UJ", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "FUJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "AS", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "RAK"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "HAR", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "SHJ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "Q", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "UMQ"
                ElseIf InStr(Convert.ToString(reader("pobox_emir")), "OR", CompareMethod.Text) Then
                    lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & "KORF"
                End If

            Else
                lstrPOBOX = reader.GetString(13).Replace("&nbsp", blank) & " " & Convert.ToString(reader("School_Emir")).ToUpper

            End If

            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(lstrPOBOX + "</TD>")


            'I/O
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            If reader.GetString(14).ToUpper.Trim = "I" Or reader.GetString(14).ToUpper.Trim = "O" Then
                sb.AppendLine(reader.GetString(14).ToUpper + "</TD>")
            Else
                sb.AppendLine(blank + "</TD>")
            End If


            '' ''All kg1 result is new
            '' '' for other grades if last grade is not 'NEW' then show result
            '' '' For Internal transfer if the date of join is within the academicyear then pass

            'new pass readmit
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            If (reader.GetString(14) = "I") And reader.GetString(27) = "Y" Then
                sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=""PASS"" /></TD>")
            ElseIf grd_id <> "KG1" And reader.GetString(1) <> "NEW" Then
                sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=" + reader.GetString(15).ToUpper + " /></TD>")
            Else
                sb.AppendLine(" <input style=""FONT: bold 7pt verdana,timesroman,garamond ; border:0 ;width:30px""  id=""Text" + srNo.ToString + " type=""text"" value=""NEW"" /></TD>")
                'sb.AppendLine("NEW</TD>")
            End If

            'prev school 

            '' ''For all overseas admission, FRESH
            '' ''For Internal Transfer  � Name of the previous school 


            'If (reader.GetString(14) = "I") Then
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
            '    sb.AppendLine(reader.GetString(16).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    ''prev school country
            '    'sb.AppendLine("<TD style=" + tdStyle + " align=left>&nbsp")
            '    'sb.AppendLine(reader.GetString(17).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    'prev school curriculum
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            '    sb.AppendLine(reader.GetString(18).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    'last attendance date
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            '    sb.AppendLine(Format(reader.GetDateTime(19), "dd/MM/yyyy").Replace("01/01/1900", blank) + "</TD>")
            'Else
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
            '    sb.AppendLine("FRESH</TD>")

            '    ''prev school country
            '    'sb.AppendLine("<TD style=" + tdStyle + " align=left>&nbsp")
            '    'sb.AppendLine(reader.GetString(17).ToUpper.Replace("&NBSP", blank) + "</TD>")

            '    'prev school curriculum
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            '    sb.AppendLine("</TD>")

            '    'last attendance date
            '    sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
            '    sb.AppendLine("</TD>")
            'End If





            Select Case reader.GetString(14)
                Case "I"
                    If prevSchInt = "DATA" Then
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                        sb.AppendLine(reader.GetString(16).ToUpper.Replace("&NBSP", blank) + "</TD>")
                        'prev school curriculum
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine(reader.GetString(18).ToUpper.Replace("&NBSP", blank) + "</TD>")

                        'last attendance date
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
                        sb.AppendLine(Format(reader.GetDateTime(19), "dd/MM/yyyy").Replace("01/01/1900", blank) + "</TD>")
                    ElseIf prevSchInt = "FRESH" Then
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                        sb.AppendLine("FRESH</TD>")

                        'prev school curriculum
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")

                        'last attendance date
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")
                    Else
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                        sb.AppendLine(blank + "</TD>")

                        'prev school curriculum
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")

                        'last attendance date
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")
                    End If

                Case "O"
                    If prevSchOvr = "DATA" Then
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                        sb.AppendLine(reader.GetString(16).ToUpper.Replace("&NBSP", blank) + "</TD>")
                        'prev school curriculum
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine(reader.GetString(18).ToUpper.Replace("&NBSP", blank) + "</TD>")

                        'last attendance date
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
                        sb.AppendLine(Format(reader.GetDateTime(19), "dd/MM/yyyy").Replace("01/01/1900", blank) + "</TD>")
                    ElseIf prevSchInt = "FRESH" Then
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                        sb.AppendLine("FRESH</TD>")

                        'prev school curriculum
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")

                        'last attendance date
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")
                    Else
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                        sb.AppendLine(blank + "</TD>")

                        'prev school curriculum
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")

                        'last attendance date
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")
                    End If

                Case Else
                    If prevSchNew = "DATA" Then
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                        sb.AppendLine(reader.GetString(16).ToUpper.Replace("&NBSP", blank) + "</TD>")
                        'prev school curriculum
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine(reader.GetString(18).ToUpper.Replace("&NBSP", blank) + "</TD>")

                        'last attendance date
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
                        sb.AppendLine(Format(reader.GetDateTime(19), "dd/MM/yyyy").Replace("01/01/1900", blank) + "</TD>")
                    ElseIf prevSchInt = "FRESH" Then
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                        sb.AppendLine("FRESH</TD>")

                        'prev school curriculum
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")

                        'last attendance date
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")
                    Else
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle colspan=""3"">&nbsp")
                        sb.AppendLine(blank + "</TD>")

                        'prev school curriculum
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")

                        'last attendance date
                        sb.AppendLine("<TD style=" + tdStyle + " align=middle>&nbsp")
                        sb.AppendLine("</TD>")
                    End If


            End Select
            'date of join
            sb.AppendLine("<TD style=" + tdStyle + " align=left>")
            sb.AppendLine(Format(reader.GetDateTime(20), "dd/MM/yyyy") + "</TD>")

            'join grade
            sb.AppendLine("<TD style=" + tdStyle + " align=middle>")
            sb.AppendLine(reader.GetString(21).ToUpper.Replace("&NBSP", blank) + "</TD>")


            If (bIncludeTC = True And reader.GetString(23) = "TC" And reader.GetDateTime(24).Date < Now.Date) _
            Or (bIncludeSO = True And reader.GetString(23) = "SO" And reader.GetDateTime(24).Date < Now.Date) Then
                'leaving date
                sb.AppendLine("<TD style=" + tdStyle + ">" + Format(reader.GetDateTime(24).Date, "dd/MMM/yyyy") + "</TD>")

                'leaving reason
                sb.AppendLine("<TD style=" + tdStyle + ">" + reader.GetString(25).ToUpper + "</TD>")

            Else
                'leaving date
                sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

                'leaving reason
                sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")
            End If

            'result annual
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'reexam
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            'remarks
            sb.AppendLine("<TD style=" + tdStyle + ">&nbsp</TD>")

            sb.AppendLine("</TR>")



            recordCount += 1

            If recordCount = pageSize And bHdrAllPages = True Then

                sb.AppendLine("</TABLE>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")

                sb.AppendLine(strFooter)

                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")
                'sb.AppendLine("<BR>")




                recordCount = 0

            End If

            If recordCount = pageSize And bHdrAllPages = False Then
                sb.AppendLine("</TABLE>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                sb.AppendLine("<BR>")
                recordCount = 0
            End If
            If recordCount <> pageSize And bHdrAllPages = False Then
                sb.AppendLine("<div style=""page-break-after:always"">&nbsp;</div>")
            End If


        End While
        Dim ss As String = "<div style=""page-break-after:always"">&nbsp;</div>"



        'If recordCount <> 0 Then
        '    Dim remainingLines As Integer = pageSize - recordCount
        '    Dim i As Integer
        '    For i = 0 To remainingLines - 1
        '        sb.AppendLine(EmptyLines)
        '    Next

        '    If bHdrAllPages = True Then
        '        'sb.AppendLine("</TABLE>")
        '        ' sb.AppendLine("<BR>")
        '        ' sb.AppendLine("<BR>")
        '        ' sb.AppendLine("<BR>")
        '        ' sb.AppendLine(strFooter)

        '    End If

        'End If

        If bHdrAllPages = False Then
            'sb.AppendLine("</TABLE>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine("<BR>")
            'sb.AppendLine(strFooter)
        End If
        reader.Close()

        ' Response.Write(sb.ToString)


        sb.AppendLine("</BODY></HTML>")
        ltBlueBook.Text = sb.ToString
    End Function



    Function EmptyLines() As String
        Dim sb1 As New StringBuilder
        Dim i As Integer
        sb1.AppendLine("<TR style=""height:30px"">")
        For i = 0 To 25
            If i <> 16 Then
                sb1.AppendLine("<TD>&nbsp</TD>")
            Else
                sb1.AppendLine("<TD colspan=""3"">&nbsp</TD>")
            End If
        Next
        sb1.AppendLine("</TR>")

        Return sb1.ToString
    End Function


End Class
