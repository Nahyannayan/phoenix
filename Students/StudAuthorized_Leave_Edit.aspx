<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="StudAuthorized_Leave_Edit.aspx.vb" Inherits="Students_StudAuthorized_Leave_Edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function getDate(mode) {
            var sFeatures;
            sFeatures = "dialogWidth: 229px; ";
            sFeatures += "dialogHeight: 234px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            if (mode == 1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtFrom.ClientID %>').value, "", sFeatures)
            else if (mode == 2)
                result = window.showModalDialog("../accounts/calendar.aspx?dt=" + document.getElementById('<%=txtTo.ClientID %>').value, "", sFeatures)

        if (result == '' || result == undefined) {
            return false;
        }
        if (mode == 1)
            document.getElementById('<%=txtFrom.ClientID %>').value = result;
        else if (mode == 2)
            document.getElementById('<%=txtTo.ClientID %>').value = result;
       return true;
   }



   function change_chk_state(chkThis) {
       var chk_state = !chkThis.checked;
       for (i = 0; i < document.forms[0].elements.length; i++) {
           var currentid = document.forms[0].elements[i].id;
           if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkG_bAVAILABLE") != -1) {
               //if (document.forms[0].elements[i].type=='checkbox' )
               //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
               document.forms[0].elements[i].checked = chk_state;
               document.forms[0].elements[i].click();//fire the click event of the child element
           }
       }
   }
   var color = '';
   function highlight(obj) {
       var rowObject = getParentRow(obj);
       var parentTable = document.getElementById("<%=gvInfo.ClientID %>");
         if (color == '') {
             color = getRowColor();
         }
         if (obj.checked) {
             rowObject.style.backgroundColor = '#f6deb2';
         }
         else {
             rowObject.style.backgroundColor = '';
             color = '';
         }
         // private method

         function getRowColor() {
             if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
             else return rowObject.style.backgroundColor;
         }
     }
     // This method returns the parent row of the object
     function getParentRow(obj) {
         do {
             obj = obj.parentElement;
         }
         while (obj.tagName != "TR")
         return obj;
     }


    </script>

    <script type="text/javascript">

        function Gridvalidate() {
            var Checkbol = 0;
            var bool = 0;

            for (j = 0; j < grd_Cb.length; j++) {
                var obj = document.getElementById(grd_Cb[j]);
                if (obj.checked == true) {
                    Checkbol = 1;
                    bool = 1;
                }
            }

            if (bool == 0) {
                alert(" Please enter atleast one Grade");
                return false;
            }

            if (Checkbol == 1) {

                for (i = 0; i < grd_Cb.length; i++) {
                    var Obj1 = document.getElementById(grd_Cb[i]);
                    if (Obj1.checked == true) {
                        var objNDays = document.getElementById(grdNDays_Txt[i]);
                        if (objNDays.value == "") {
                            alert("Line No : " + [parseInt(i) + 1] + " No of Days can not be blank");
                            objNDays.focus();
                            return false;
                        }
                        else if (isNaN(objNDays.value) == true) {
                            alert("Line No : " + [parseInt(i) + 1] + " Please check - non numeric value!");
                            objNDays.focus();
                            return false;

                        }
                    }

                }

            }

            return true;
        }






    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltLabel" runat="server"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_AddGroup" runat="server"   width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                         ></asp:Label><span style="  color: #800000">&nbsp;</span>
                                </div>
                                <div align="left">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" DisplayMode="List"
                                        EnableViewState="False"  ForeColor="" ValidationGroup="AttGroup"></asp:ValidationSummary>
                                    <span style="  color: #800000">&nbsp;</span>
                                </div>
                            </span>
                        </td>
                    </tr>
                    <tr style="  color: #800000" valign="bottom">
                        <td align="center" class="matters" valign="middle">&nbsp;Fields Markedwith (<span style="  color: #800000">*</span>) are mandatory</td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom">
                            <table width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year <span style="color: #800000">*</span></span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr>
                                    <td align="left"><span class="field-label">From <span style="color: #800000">*</span></span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" runat="server">
                                        </asp:TextBox>&nbsp;<asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"
                                            ></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                                CssClass="text-danger" Display="Dynamic" ErrorMessage="From Date required" ForeColor=""
                                                ValidationGroup="AttGroup"></asp:RequiredFieldValidator></td>
                                    <td align="left"><span class="field-label">To <span style="color: #800000">*</span></span></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTo" runat="server">
                                        </asp:TextBox>&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                            ></asp:ImageButton>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTo"
                                            CssClass="text-danger" Display="Dynamic" ErrorMessage="To Date required" ForeColor=""
                                            ValidationGroup="AttGroup"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr class="title-bg-lite">
                                    <td align="left" class="matters" colspan="4">Set Authorization</td>
                                </tr>
                                <tr>
                                    <td colspan="4" valign="top">
                                        <asp:GridView ID="gvInfo" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Details Added" Height="100%" OnPreRender="gvInfo_PreRender"
                                            Width="100%">
                                            <RowStyle CssClass="griditem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select All">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this); " runat="server" ToolTip="Click here to select/deselect all rows"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkG_bAVAILABLE" onclick="javascript:highlight(this);" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SCT_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRD_ID" runat="server" Text='<%# Bind("GRD_ID") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRD_DESCR" runat="server" Text='<%# Bind("GRD_DESCR") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Authorized Staff">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:DropDownList ID="ddlEmpName" runat="server"   DataValueField="EMP_ID" DataTextField="EMP_NAME" DataSource="<%# callAuthorised_Emp() %>"></asp:DropDownList>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No. of Days">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:TextBox ID="txtNDays" runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle CssClass="Green" />
                                            <HeaderStyle CssClass="gridheader_pop" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        <asp:Panel ID="plEdit" runat="server" Width="100%">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" width="20%"><span class="field-label">Authorized Staff <span style="  color: #800000">*</span></span></td>
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="ddlAuthStaff" runat="server" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                                        </asp:DropDownList></td>
                                                    <td align="left" width="20%"><span class="field-label">Grade <span style="  color: #800000">*</span></span></td>
                                                    <td align="left" width="30%">
                                                        <asp:DropDownList ID="ddlAuthGrade" runat="server" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td align="left"><span class="field-label">No. of Days <span style="  color: #800000">*</span></span></td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtNo" runat="server">
                                                        </asp:TextBox></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="plDelete" runat="server" Width="100%">Record Deleted Successfully</asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" align="center">
                            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                                OnClick="btnAdd_Click" Text="Add" /><asp:Button ID="btnEdit" runat="server" CausesValidation="False"
                                    CssClass="button" OnClick="btnEdit_Click" Text="Edit" /><asp:Button ID="btnSave"
                                        runat="server" CssClass="button" OnClick="btnSave_Click" Text="Save" ValidationGroup="AttGroup" /><asp:Button
                                            ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" OnClick="btnCancel_Click"
                                            Text="Cancel" /><asp:Button ID="btnDelete" runat="server" CausesValidation="False"
                                                CssClass="button" OnClick="btnDelete_Click" Text="Delete" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
      <ajaxToolkit:CalendarExtender ID="calDate1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar"
        TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>
      <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="ImageButton1"
        TargetControlID="txtTo">
    </ajaxToolkit:CalendarExtender>
</asp:Content>

