﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Partial Class Students_studTransfer
    Inherits System.Web.UI.Page
    Dim splitquery() As String
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        Try

      
            If Page.IsPostBack = False Then
                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If
                'Try
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S200114") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights
                    bindbsu(ddlFschool)
                    bindbsu(ddlTschool)

                    callYEAR_DESCRBind(ddlFschool, ddlFAcademicYear)
                    callYEAR_DESCRBind(ddlTschool, ddlTAcademicYear)

                    callCurrent_BsuShift(ddlFschool, ddlFAcademicYear, ddlFshift)
                    callCurrent_BsuShift(ddlTschool, ddlTAcademicYear, ddlTshift)

                    Call bindAcademic_STREAM(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFStream)
                    Call bindAcademic_STREAM(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTstream)

                    bindAcademic_Grade(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFStream, ddlFgrade)
                    bindAcademic_Grade(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTstream, ddlTgrade)

                    callGrade_Section(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFgrade, ddlFsection)
                    callGrade_Section(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTgrade, ddlTsection)

                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"

                    set_Menu_Img()
                    GridBind()
                End If

            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If GrdView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = GrdView.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If GrdView.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = GrdView.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function


    Private Sub bindbsu(ByVal ddlbsu As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        str_Sql = "SELECT   BSU_ID,BSU_NAME  FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlbsu.DataSource = ds.Tables(0)
        ddlbsu.DataValueField = "BSU_ID"
        ddlbsu.DataTextField = "BSU_NAME"
        ddlbsu.DataBind()
        ddlbsu.SelectedValue = Session("sBsuid")
        'Dim lst As New ListItem
        'lst.Text = "--------Select--------"
        'lst.Value = 0
        'ddlFschool.Items.Insert(0, lst)
    End Sub
    Public Sub callYEAR_DESCRBind(ByVal ddlschool As DropDownList, ByVal ddlAcademicYear As DropDownList)
        Try
            Dim di As ListItem
            Dim lstrCLM As Integer

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", ddlschool.SelectedValue)

            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetBSU_CLM", pParms)
                While reader.Read
                    lstrCLM = Convert.ToString(reader("CLM"))
                End While
            End Using

            Using YEAR_DESCRreader As SqlDataReader = AccessStudentClass.GetYear_DESCR(ddlschool.SelectedValue, lstrCLM)
                ddlAcademicYear.Items.Clear()

                If YEAR_DESCRreader.HasRows = True Then
                    While YEAR_DESCRreader.Read
                        di = New ListItem(YEAR_DESCRreader("Y_DESCR"), YEAR_DESCRreader("ACD_ID"))
                        ddlAcademicYear.Items.Add(di)
                        Dim bactive As String = Convert.ToString(YEAR_DESCRreader("ACD_CURRENT"))
                        If bactive = True Then
                            di.Selected = True
                        End If
                    End While
                End If
            End Using
            'For ItemTypeCounter As Integer = 0 To ddlFAcademicYear.Items.Count - 1
            '    'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
            '    If Not Session("Current_ACD_ID") Is Nothing Then
            '        If ddlAcademicYear.Items(ItemTypeCounter).Value = Session("Current_ACD_ID") Then
            '            ddlAcademicYear.SelectedIndex = ItemTypeCounter
            '        End If
            '    End If
            'Next

            ddlFAcademicYear_SelectedIndexChanged(ddlFAcademicYear, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callCurrent_BsuShift(ByVal ddlschool As DropDownList, ByVal ddlAcademicYear As DropDownList, ByVal ddlshift As DropDownList)
        Try
            Dim ACD_ID As String = String.Empty
            Dim Bsu_id As String = ddlschool.SelectedValue
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Current_BsuShiftReader As SqlDataReader = AccessStudentClass.GetCurrent_BsuShift(Bsu_id, ACD_ID)
                ddlshift.Items.Clear()
                If Current_BsuShiftReader.HasRows = True Then
                    While Current_BsuShiftReader.Read
                        di = New ListItem(Current_BsuShiftReader("SHF_DESCR"), Current_BsuShiftReader("SHF_ID"))
                        ddlshift.Items.Add(di)
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlFshift.Items.Count - 1
                        'keep loop until you get the counter for default Acad_YEAR into  the SelectedIndex
                        If Not Session("Current_ACD_ID") Is Nothing Then
                            If UCase(ddlshift.Items(ItemTypeCounter).Text) = "NORMAL" Then
                                ddlshift.SelectedIndex = ItemTypeCounter
                            End If
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAcademic_STREAM(ByVal ddlSchool As DropDownList, ByVal ddlAcademicYear As DropDownList, ByVal ddlshift As DropDownList, ByVal ddlStream As DropDownList)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedValue
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim SHF_ID As String = ddlshift.SelectedValue
            Dim ds As New DataSet
            str_Sql = " SELECT DISTINCT STREAM_M.STM_DESCR, STREAM_M.STM_ID " & _
" FROM GRADE_BSU_M INNER JOIN  STREAM_M ON GRADE_BSU_M.GRM_STM_ID = STREAM_M.STM_ID where " & _
" GRADE_BSU_M.GRM_ACD_ID='" & ACD_ID & "' and GRADE_BSU_M.GRM_BSU_ID='" & BSU_ID & "' And GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "' order by STREAM_M.STM_ID"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlStream.Items.Clear()
            ddlStream.DataSource = ds.Tables(0)
            ddlStream.DataTextField = "STM_DESCR"
            ddlStream.DataValueField = "STM_ID"
            ddlStream.DataBind()

            'ddlStream.Items.Add(New ListItem("ALL", "0"))
            'ddlStream.ClearSelection()
            ''If Not ddlStream.Items.FindByText("ALL") Is Nothing Then
            ''    ddlStream.Items.FindByText("ALL").Selected = True
            ''End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAcademic_Grade(ByVal ddlSchool As DropDownList, ByVal ddlAcademicYear As DropDownList, ByVal ddlshift As DropDownList, ByVal ddlStream As DropDownList, ByVal ddlgrade As DropDownList)
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String
            Dim BSU_ID As String = ddlSchool.SelectedValue
            Dim ACD_ID As String = ddlAcademicYear.SelectedItem.Value
            Dim SHF_ID As String = ddlshift.SelectedValue
            Dim STM_ID As String = ddlStream.SelectedValue
            Dim ds As New DataSet
            str_Sql = " SELECT DISTINCT GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, GRADE_BSU_M.GRM_DISPLAY, GRADE_M.GRD_DISPLAYORDER,GRM_ID " & _
" FROM GRADE_BSU_M INNER JOIN GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID where GRADE_BSU_M.GRM_BSU_ID='" & BSU_ID & "' " & _
 " and GRADE_BSU_M.GRM_ACD_ID= '" & ACD_ID & "' And GRADE_BSU_M.GRM_SHF_ID='" & SHF_ID & "' AND GRM_STM_ID='" & STM_ID & "'order by GRADE_M.GRD_DISPLAYORDER "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            ddlgrade.Items.Clear()
            ddlgrade.DataSource = ds.Tables(0)
            ddlgrade.DataTextField = "GRD_ID"
            ddlgrade.DataValueField = "GRM_ID"
            ddlgrade.DataBind()

            'ddlgrade.Items.Add(New ListItem("ALL", "0"))
            'ddlgrade.ClearSelection()
            'If Not ddlgrade.Items.FindByText("ALL") Is Nothing Then
            '    ddlgrade.Items.FindByText("ALL").Selected = True
            'End If
            ' ddlFgrade_SelectedIndexChanged(ddlFgrade, Nothing)

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Sub callGrade_Section(ByVal ddlSchool As DropDownList, ByVal ddlAcademicYear As DropDownList, ByVal ddlshift As DropDownList, ByVal ddlgrade As DropDownList, ByVal ddlsection As DropDownList)
        Try

            Dim GRD_ID As String = String.Empty
            Dim Bsu_id As String = ddlSchool.SelectedValue
            If ddlgrade.SelectedIndex <> -1 Then

                If ddlgrade.SelectedItem.Value = "0" Then
                    GRD_ID = ""
                Else
                    GRD_ID = ddlgrade.SelectedItem.Text
                End If
            End If

            Dim SHF_ID As String = String.Empty
            If ddlshift.SelectedIndex = -1 Then
                SHF_ID = ""
            Else
                SHF_ID = ddlshift.SelectedItem.Value
            End If
            Dim di As ListItem
            Using Grade_SectionReader As SqlDataReader = AccessStudentClass.GetGrade_Section(Bsu_id, ddlAcademicYear.SelectedItem.Value, GRD_ID, SHF_ID)
                ddlsection.Items.Clear()
                'di = New ListItem("ALL", "0")
                'ddlsection.Items.Add(di)
                If Grade_SectionReader.HasRows = True Then
                    While Grade_SectionReader.Read
                        di = New ListItem(Grade_SectionReader("SCT_DESCR"), Grade_SectionReader("SCT_ID"))
                        ddlsection.Items.Add(di)
                    End While
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    'Public Sub callCurriculum()
    '    Try
    '        Using CurriculumReader As SqlDataReader = AccessStudentClass.GetCurriculum()
    '            ddlPrevBoard.Items.Clear()
    '            ddlPrevBoard.Items.Add(New ListItem("ALL", "0"))
    '            If CurriculumReader.HasRows = True Then
    '                While CurriculumReader.Read
    '                    ddlPrevBoard.Items.Add(New ListItem(CurriculumReader("CLM_DESCR"), CurriculumReader("CLM_ID")))
    '                End While
    '            End If
    '        End Using
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    'End Sub


    Protected Sub ddlFAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFAcademicYear.SelectedIndexChanged
        callCurrent_BsuShift(ddlFschool, ddlFAcademicYear, ddlFshift)
        Call bindAcademic_STREAM(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFStream)
        bindAcademic_Grade(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFStream, ddlFgrade)
        callGrade_Section(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFgrade, ddlFsection)
        GridBind()
    End Sub

    Protected Sub ddlFshift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFshift.SelectedIndexChanged
        Call bindAcademic_STREAM(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFStream)
        bindAcademic_Grade(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFStream, ddlFgrade)
        callGrade_Section(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFgrade, ddlFsection)
        GridBind()
    End Sub

    Protected Sub ddlFStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFStream.SelectedIndexChanged
        bindAcademic_Grade(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFStream, ddlFgrade)
        callGrade_Section(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFgrade, ddlFsection)
        GridBind()
    End Sub

    Protected Sub ddlFgrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFgrade.SelectedIndexChanged
        callGrade_Section(ddlFschool, ddlFAcademicYear, ddlFshift, ddlFgrade, ddlFsection)
        GridBind()
    End Sub

    Protected Sub ddlTschool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTschool.SelectedIndexChanged
        callYEAR_DESCRBind(ddlTschool, ddlTAcademicYear)
        callCurrent_BsuShift(ddlTschool, ddlTAcademicYear, ddlTshift)
        Call bindAcademic_STREAM(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTstream)
        bindAcademic_Grade(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTstream, ddlTgrade)
        callGrade_Section(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTgrade, ddlTsection)
    End Sub

    Protected Sub ddlTAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTAcademicYear.SelectedIndexChanged
        callCurrent_BsuShift(ddlTschool, ddlTAcademicYear, ddlTshift)
        Call bindAcademic_STREAM(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTstream)
        bindAcademic_Grade(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTstream, ddlTgrade)
        callGrade_Section(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTgrade, ddlTsection)
    End Sub
    Protected Sub ddlTshift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTshift.SelectedIndexChanged
        Call bindAcademic_STREAM(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTstream)
        bindAcademic_Grade(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTstream, ddlTgrade)
        callGrade_Section(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTgrade, ddlTsection)
    End Sub
    Protected Sub ddlTstream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTstream.SelectedIndexChanged
        bindAcademic_Grade(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTstream, ddlTgrade)
        callGrade_Section(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTgrade, ddlTsection)

    End Sub
    Protected Sub ddlTgrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTgrade.SelectedIndexChanged
        callGrade_Section(ddlTschool, ddlTAcademicYear, ddlTshift, ddlTgrade, ddlTsection)
    End Sub
    'Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
    '    GridBind()
    'End Sub
    Sub GridBind()
        ViewState("slno") = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT row_number() over (ORDER BY A.GRD_DISPLAYORDER,A.SCT_DESCR,A.STU_NAME)as SINO,STU_ID,STU_NO,STU_NAME,GRD_DISPLAYORDER,GRM_DISPLAY,STU_GRM_ID,STU_SCT_ID,SCT_DESCR,STU_ACD_ID,STU_LEAVEDATE,STU_GENDER FROM (SELECT STU_ID,STU_NO,ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ') AS STU_NAME,GRD_DISPLAYORDER, GRM_DISPLAY, STU_GRM_ID, STU_SCT_ID, SCT_DESCR, STU_ACD_ID, STU_LEAVEDATE, STU_GENDER FROM STUDENT_M  AS STU INNER JOIN GRADE_M ON STU.STU_GRD_ID=GRADE_M.GRD_ID INNER JOIN GRADE_BSU_M ON STU.STU_GRM_ID = GRADE_BSU_M.GRM_ID INNER JOIN  SECTION_M ON STU.STU_SCT_ID = SECTION_M.SCT_ID AND ISNULL(STU_LEAVEDATE,'')='' AND STU_CurrStatus<>'CN' AND STU_CurrStatus<>'TF' AND STU_ID NOT IN (SELECT STS_STU_ID FROM dbo.STUDENT_TRANSFER_S WHERE STS_bCANCEL_TRANS='FALSE' and  STS_TO_bApproved='False')AND STU_ID NOT IN(SELECT TCM_STU_ID FROM TCM_M WHERE TCM_CANCELDATE IS NULL))A"

        If ddlFAcademicYear.Items.Count > 0 Then
            str_query += " WHERE A.STU_ACD_ID='" + ddlFAcademicYear.SelectedValue.ToString + "'"
        End If
        If ddlFgrade.Items.Count > 0 Then
            str_query += " AND A.STU_GRM_ID='" + ddlFgrade.SelectedValue.ToString + "'"
        End If
        If ddlFsection.Items.Count > 0 Then
            str_query += " AND A.STU_SCT_ID=" + ddlFsection.SelectedValue.ToString + ""
        End If
        str_query += " AND A.STU_GENDER='" + rbGender.SelectedValue.ToString + "'"
        'Dim txtStuNo As TextBox
        'Dim txtName As TextBox
        'If txtStuNo.Text <> "" Then
        '    str_query += " AND STU_NO LIKE '%" + txtStuNo.Text + "%'"
        'End If

        'If txtName.Text <> "" Then
        '    str_query += " AND ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,'') LIKE '%" + txtName.Text + "%'"
        'End If


        ' AND C.SCT_GRM_ID=B.GRM_ID" 
        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedStatus As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim strFilter As String = ""

        Dim strName As String = ""
        Dim strFee As String = ""
        Dim txtSearch As New TextBox

        If GrdView.Rows.Count > 0 Then


            txtSearch = GrdView.HeaderRow.FindControl("txtStudName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then
                strFilter += " AND "
                strFilter += GetSearchString("STU_NAME", txtSearch.Text, strSearch)
            End If
            strName = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = GrdView.HeaderRow.FindControl("txtFeeSearch")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            If txtSearch.Text <> "" Then

                strFilter += " AND "

                strFilter += GetSearchString("STU_NO", txtSearch.Text.Replace("/", " "), strSearch)
            End If
            strFee = txtSearch.Text

        End If

        str_query += strFilter + "ORDER BY A.GRD_DISPLAYORDER,A.SCT_DESCR,A.STU_NAME"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'Dim dv As DataView = ds.Tables(0).DefaultView
        'If strFilter <> "" Then
        '    dv.RowFilter = strFilter
        'End If

        Dim dt As DataTable
        If ds.Tables(0).Rows.Count = 0 Then
            GrdView.DataSource = ds
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            GrdView.DataBind()
            Dim columnCount As Integer = GrdView.Rows(0).Cells.Count
            GrdView.Rows(0).Cells.Clear()
            GrdView.Rows(0).Cells.Add(New TableCell)
            GrdView.Rows(0).Cells(0).ColumnSpan = columnCount
            GrdView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            GrdView.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."

        Else
            GrdView.DataSource = ds
            GrdView.DataBind()

        End If

        set_Menu_Img()
    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value.Trim <> "" Then
            If strSearch = "LI" Then
                strFilter = field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
    Protected Sub btnStudName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnFeeId_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub btnTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransfer.Click

        If ddlFschool.SelectedValue <> ddlTschool.SelectedValue Then

            Dim trandate As Date = txtTransDate.Text
            If trandate >= Date.Now.Date Then

                lbltransfer.Text = "You have chosen to transfer students of " + ddlFschool.SelectedItem.Text + " Grade " + ddlFgrade.SelectedItem.Text + "(" + ddlFAcademicYear.SelectedItem.Text + ") to " + ddlTschool.SelectedItem.Text + " Grade " + ddlTgrade.SelectedItem.Text + " (" + ddlTAcademicYear.SelectedItem.Text + ").Please Confirm"

                Modalpopupconfirm.Show()
            Else
                lblError.Text = "Transfer Date should be greater than or equal todays date...!!!!"
            End If

        Else
            lblError.Text = "Current School and Transfer School Cannot be Same...!!!!"
        End If
    End Sub

    'Sub SaveData()
    '    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
    '    Dim i As Integer
    '    Dim chkSelect As CheckBox
    '    Dim lblStuId As Label
    '    Dim lblsctId As Label
    '    Dim con As SqlConnection = New SqlConnection(str_conn)
    '    con.Open()
    '    Dim trans As SqlTransaction
    '    Dim transstatus As Integer = 0

    '    trans = con.BeginTransaction("sampletrans")
    '    Try

    '        For i = 0 To gvStudPromote.Rows.Count - 1
    '            chkSelect = gvStudPromote.Rows(i).FindControl("chkSelect")
    '            If chkSelect.Checked = True Then
    '                lblStuId = gvStudPromote.Rows(i).FindControl("lblStuId")
    '                lblsctId = gvStudPromote.Rows(i).FindControl("lblsctId")

    '                Dim param(6) As SqlClient.SqlParameter
    '                param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
    '                param(1) = New SqlClient.SqlParameter("@CLM_ID", Session("CLM"))
    '                param(2) = New SqlClient.SqlParameter("@SCT_DEMOTEDID", ddlDemoteSection.SelectedValue)
    '                param(3) = New SqlClient.SqlParameter("@STU_ID", lblStuId.Text)
    '                param(4) = New SqlClient.SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)

    '                param(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '                param(5).Direction = ParameterDirection.ReturnValue
    '                Dim status As Integer = 0
    '                status = SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "STUD_DEPROMOTE", param)
    '                Dim returnflag As Integer = param(5).Value
    '                If returnflag <> 0 Then
    '                    transstatus = 1
    '                    Exit For
    '                End If
    '            End If
    '        Next

    '        If transstatus <> 0 Then
    '            trans.Rollback()
    '            lblError.Text = "Data not saved"

    '        Else
    '            trans.Commit()
    '            lblError.Text = "Record  saved Sucessfully"
    '        End If
    '    Catch ex As Exception
    '        trans.Rollback()
    '        lblError.Text = "Record could not be Saved"
    '    End Try
    'End Sub

    Protected Sub ddlFsection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFsection.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub btnYES_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim CurBsUnit As String = Session("sBsuid")
        Dim CurACDID As String = "" ' ddlAca_Year.SelectedValue
        'ANDACD_ACY_ID=20 ANDACD_ACY_ID=20
        Dim con As New SqlConnection(str_conn)
        Dim transaction As SqlTransaction
        con.Open()
        transaction = con.BeginTransaction("trans")
        Try
            For Each gr As GridViewRow In GrdView.Rows
                Dim btransfer As CheckBox = TryCast(gr.FindControl("chkSelect0"), CheckBox)
                Dim HF_sct_id As HiddenField = TryCast(gr.FindControl("HF_sct_id"), HiddenField)
                If btransfer.Checked = True Then
                    Dim stu_id As String = TryCast(gr.FindControl("HF_stu_id"), HiddenField).Value
                    Dim param(21) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@STS_FROMDATE", txtTransDate.Text)
                    param(1) = New SqlClient.SqlParameter("@STS_STU_ID", stu_id)
                    param(2) = New SqlClient.SqlParameter("@STS_BSU_ID", ddlFschool.SelectedValue)
                    param(3) = New SqlClient.SqlParameter("@STS_ACD_ID", ddlFAcademicYear.SelectedValue)
                    param(4) = New SqlClient.SqlParameter("@STS_GRM_ID", ddlFgrade.SelectedValue)
                    param(5) = New SqlClient.SqlParameter("@STS_GRD_ID", ddlFgrade.SelectedItem.Text)
                    param(6) = New SqlClient.SqlParameter("@STS_SCT_ID", ddlFsection.SelectedValue)
                    param(7) = New SqlClient.SqlParameter("@STS_SHF_ID", ddlFshift.SelectedValue)
                    param(8) = New SqlClient.SqlParameter("@STS_STM_ID", ddlFStream.SelectedValue)
                    param(9) = New SqlClient.SqlParameter("@STS_TO_BSU_ID", ddlTschool.SelectedValue)
                    param(10) = New SqlClient.SqlParameter("@STS_TO_ACD_ID", ddlTAcademicYear.SelectedValue)
                    param(11) = New SqlClient.SqlParameter("@STS_TO_GRM_ID", ddlTgrade.SelectedValue)
                    param(12) = New SqlClient.SqlParameter("@STS_TO_GRD_ID", ddlTgrade.SelectedItem.Text)
                    param(13) = New SqlClient.SqlParameter("@STS_TO_SCT_ID", ddlTsection.SelectedValue)
                    param(14) = New SqlClient.SqlParameter("@STS_TO_SHF_ID", ddlTshift.SelectedValue)
                    param(15) = New SqlClient.SqlParameter("@STS_TO_STM_ID", ddlTstream.SelectedValue)
                    param(17) = New SqlClient.SqlParameter("@STS_ENTRY_USR", Session("sUsr_name"))
                    param(16) = New SqlClient.SqlParameter("@Return_msg", SqlDbType.VarChar, 400)
                    param(16).Direction = ParameterDirection.Output

                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "STU.SAVE_STUDENT_TRANSFER_S", param)


                End If

            Next
            transaction.Commit()
            lblError.Text = "Records Saved sucessfully...!!!!"
            GridBind()
        Catch ex As Exception
            transaction.Rollback()
            lblError.Text = "Insertion Failed...!!!!"
        End Try
    End Sub

    Protected Sub rbGender_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbGender.SelectedIndexChanged
        GridBind()
    End Sub

    Protected Sub ddlFschool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFschool.SelectedIndexChanged

    End Sub
End Class
