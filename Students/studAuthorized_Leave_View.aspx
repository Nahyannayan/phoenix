<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAuthorized_Leave_View.aspx.vb" Inherits="Students_studAuthorized_Leave_View" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Leave Approval Permission "></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table width="100%">
                    <tr>
                        <td align="left" colspan="4"   valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top" >
                            <table id="tbl_test" runat="server" width="100%">
                                <tr class="matters">
                                    <td align="left" width="20%"><span class="field-label"> Select Academic year</span> </td>
                                    <td width="30%">
                                        <asp:DropDownList ID="ddlAca_Year" runat="server"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlAca_Year_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td width="20%"></td>
                                    <td width="30%"></td>
                                </tr>
                                <tr   style="text-align:center">
                                    <td align="left" class="matters" colspan="4" valign="top">
                                        <asp:GridView ID="gvAuthorizedRecord" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem"   />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Grade">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("GRD_DESCR") %>' __designer:wfdid="w16"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblGRD_DESCRH" runat="server" Text="Grade" CssClass="gridheader_text" __designer:wfdid="w17"></asp:Label><br />
                                                        <asp:TextBox ID="txtGRD_DESCR" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchGRD_DESCR" OnClick="btnSearchGRD_DESCR_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w19"></asp:ImageButton>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGRD_DESCR" runat="server" Text='<%# Bind("GRD_DESCR") %>' __designer:wfdid="w15"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Authorized Staff">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("EMP_Name") %>' __designer:wfdid="w21"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblStud_NameH" runat="server" Text="Authorized Staff" CssClass="gridheader_text" __designer:wfdid="w22"></asp:Label><br />
                                                        <asp:TextBox ID="txtEMP_Name" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchEMP_Name" OnClick="btnSearchEMP_Name_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w24"></asp:ImageButton>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEMP_Name" runat="server" Text='<%# Bind("EMP_Name") %>' __designer:wfdid="w20"></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("FromDT", "{0:dd/MMM/yyyy}") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblGradeH" runat="server" Text="From Date" CssClass="gridheader_text"></asp:Label><br />
                                                        <asp:TextBox ID="txtFromDT" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchFromDT" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" OnClick="btnSearchFromDT_Click"></asp:ImageButton>

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;
                                            <asp:Label ID="lblFromDTH" runat="server" Text='<%# Bind("FromDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblToDateH" runat="server" Text="To Date" CssClass="gridheader_text"></asp:Label><br />
                                                        <asp:TextBox ID="txtToDT" runat="server"  ></asp:TextBox>
                                                        <asp:ImageButton ID="btnSearchToDT" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" OnClick="btnSearchToDT_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToDTH" runat="server" Text='<%# Bind("ToDT", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                           <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Section">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("SCT_DESCR") %>' __designer:wfdid="w26"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblSTUD_IDH" runat="server" Text="No. of " CssClass="gridheader_text" __designer:wfdid="w27"></asp:Label><br />
                                                                        <asp:Label ID="Label1" runat="server" Text="Days" CssClass="gridheader_text" __designer:wfdid="w28"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSAL_NDAYS" runat="server" Text='<%# Bind("SAL_NDAYS") %>' __designer:wfdid="w8"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server">View</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Student ID" Visible="False">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("SAL_ID") %>' __designer:wfdid="w30"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSAL_ID" runat="server" Text='<%# Bind("SAL_ID") %>' __designer:wfdid="w29"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" /></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

</asp:Content>

