﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EnqConvTranDateStatistics.aspx.vb" Inherits="Students_EnqConvTranDateStatistics" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">
    <link href="../cssfiles/Dashboard.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../FusionCharts/fusioncharts.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css" media="screen" />

    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">

        function AddDetails(vid, regionID, stat) {

            $.fancybox({
                type: 'iframe',

                href: 'EnqConvTranDatestatisticsF.aspx?vid=' + vid + '&regionId=' + regionID + '&stat=' + stat,
                maxHeight: 400,
                fitToView: false,
                width: '950',
                height: '98%',
                autoSize: true,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                hideOnOverlayClick: false,
                hideOnContentClick: false,
                topRatio: 0
            });
        }
    </script>
    <style type="text/css">


        .chkTxtSearch {
            /*height: 20px !important;*/
            /*border: 1px solid #a9bed8 !important;
            border-collapse: collapse;*/
            /*font-family: Helvetica,Andale Mono,Sans-Serif,sans;*/
            /*font-size: 11px !important;*/
            margin: 1px 0px 1px 0px;
            padding-left: 2px;
            line-height: 20px;
            /*color: #555152;*/
            background: #fff url('../Images/arrDown.png') no-repeat 1px 3px !important;
            padding-left: 20px;
        }
    </style>

</head>
<body>

    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptManager1" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
<%--margin: -10px 16px 4px 10px; padding-right: 5px;--%><%-- z-index: 2; --%>
        <div width="100%" style=" text-align: left; border-collapse: collapse; position: relative;">
            <table border="0" cellpadding="3" cellspacing="0" style="width: 100%; text-align: left; ">
                <tr>
                    <td style="vertical-align: middle;"><span class="field-label">From</span></td>
                    <td><asp:TextBox ID="txtIncFromDT" runat="server" CssClass="txtDateIR"
                        ToolTip="Incident From Date"></asp:TextBox><asp:ImageButton
                            ID="imgBtnTXTIcDateFrm" runat="server" ImageUrl="~/Images/calendar.gif" ToolTip="Incident From Date" />
                         <ajaxToolkit:TextBoxWatermarkExtender ID="twmFrmDT" runat="server"
                            TargetControlID="txtIncFromdt" WatermarkText="From Date" WatermarkCssClass="watermarked">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                        <ajaxToolkit:CalendarExtender ID="cxIncDateFrm" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgBtnTXTIcDateFrm"
                            TargetControlID="txtIncFromDT">
                        </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="cxIncDateFrm2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtIncFromDT"
                            TargetControlID="txtIncFromDT">
                        </ajaxToolkit:CalendarExtender>
                                                                             </td>
                    <td>
                        <span class="field-label">To</span></td>
                    <td>
                        <asp:TextBox ID="txtIncDateTo" runat="server" CssClass="txtDateIR"
                            ToolTip="Incident To Date"></asp:TextBox><asp:ImageButton ID="imgBtnTXTIcDateTo" runat="server" ImageUrl="~/Images/calendar.gif"
                                ToolTip="Incident To Date" />
                             <ajaxToolkit:TextBoxWatermarkExtender ID="twmToDT" runat="server"
                            TargetControlID="txtIncDateTo" WatermarkText="To Date" WatermarkCssClass="watermarked">
                        </ajaxToolkit:TextBoxWatermarkExtender>
                        <ajaxToolkit:CalendarExtender ID="cxIncDateTo" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgBtnTXTIcDateTo"
                            TargetControlID="txtIncDateTo">
                        </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="cxIncDateToC" runat="server" Format="dd/MMM/yyyy" PopupButtonID="txtIncDateTo"
                            TargetControlID="txtIncDateTo">
                        </ajaxToolkit:CalendarExtender>
                       
                    </td>
                   <%-- <td style="vertical-align: middle; color: #556270; width: 60px;">
                       
                    </td>--%>
                    <%-- <td style="vertical-align:middle;width:180px;">
                 
                </td>--%>
                    </tr>
                <tr>
                    <td style="text-align: left; width: 20%;"><span class="field-label">Academic Year</span> </td>
                    <td>
                    <asp:TextBox ID="txtFilterYear" runat="server" ReadOnly="true" CssClass="chkTxtSearch" ></asp:TextBox>
                        <div id="plYear" runat="server" style="display: none; background-color: white;  text-align: left;  overflow-y: auto; overflow-x: hidden;">
                            <asp:CheckBoxList ID="chklACY_IDs" runat="server" RepeatColumns="1" RepeatDirection="Vertical"    AutoPostBack="true"></asp:CheckBoxList>
                        </div>

                        <asp:HiddenField ID="HDNaCYiD" runat="server" />
                        <ajaxToolkit:PopupControlExtender ID="PopEx" runat="server"
                            TargetControlID="txtFilterYear"
                            PopupControlID="plYear"
                            Position="Bottom" />
                    </td>
                    <td colspan="2"></td>
                    </tr>
                <tr>
                    <td style="text-align: center; " colspan="4">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button"   />&nbsp;
                    <asp:Button ID="btnResetDate" runat="server" Text="Reset" CssClass="button"   />

                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div class="title-bg">
            Enquiry Conversion         
        </div>
        <div class="dbcontainerMain" width="100%">
            <div class="dbULcontainer" width="100%">
                <asp:Repeater ID="rptTabDB" runat="server">
                    <HeaderTemplate>
                        <div style="list-style: none; text-align: left; padding: 0px; margin: 0px 8px 0px 0px;">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="dbinactiveCat" id="divTabHolder" runat="server">
                            <asp:HiddenField ID="hfWidthENQ" runat="server" Value='<%# Eval("PER_ENQ_WIDTH")%>' />
                            <asp:HiddenField ID="hfwidthREG" runat="server" Value='<%# Eval("PER_REG_WIDTH")%>' />
                            <asp:HiddenField ID="hfwidthTC" runat="server" Value='<%# Eval("PER_TCS_WIDTH")%>' />

                            <asp:HiddenField ID="hfZoneID" runat="server" Value='<%# Eval("BSU_ID")%>' />
                            <asp:LinkButton ID="lbtnZoneID" runat="server" CssClass="dblinkbtnCat" CommandName='<%# Eval("BSU_NAME")%>' CommandArgument='<%# Eval("BSU_ID")%>'>
                                <div class='<%# Eval("cssLine")%>'>
                                </div>
                                <div style="margin-top: 2px; margin-left: 4px;"><%# Eval("BSU_NAME")%></div>
                                <div class="dbCatSubContainer">
                                    <!--Enrolled  -->
                                    <div class='<%# Eval("StuTotCountCSS")%>'><%# Eval("TCS")%></div>
                                    <div class="dbCatSubProgContainer">
                                        <div class="dbprogIR">
                                            <span class="dbprogTitle" style="color: #40C0CB; font-weight: bold;">ENQ</span><span class="dbProgCount"><%# Eval(" ENQS")%></span><div class="barStudENQ"
                                                id="dbbarENQ" runat="server">
                                            </div>
                                        </div>
                                        <div class="dbprogIR">
                                            <span class="dbprogTitle" style="color: #E97F02; font-weight: bold;">REG</span><span class="dbProgCount"><%# Eval("REGS")%></span><div class="barStudREG"
                                                id="dbbarREG" runat="server">
                                            </div>
                                        </div>
                                        <div class="dbprogIR">
                                            <span class="dbprogTitle" style="color: #ED303C; font-weight: bold;">OFR</span><span class="dbProgCount"><%#Eval("STUDS")%></span><div class="barStudTC"
                                                id="dbbarTC" runat="server">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="arrRightInactive" id="divRightArrow" runat="server"></div>
                                </div>
                            </asp:LinkButton>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="dbStudDetailcontainer" width="100%">
                <div id="divTitle" runat="server" style=" text-align: left; color: #1B80B6;  font-weight: bold; letter-spacing: 1px; line-height: 24px; padding: 4px 0px 2px 1px; width: 100%;">
                </div>
                <div class="printImage"><a href="Javascript:window.print();">
                    <img src="../Images/print.gif" alt="Print" width="20px" height="20px" /></a></div>
                <br />
                <br />
                <div style=" border: 0px none; padding: 4px 0px 5px 0px; margin: 0px; overflow: hidden;">
                    <div style="border: 0px none;  overflow-x: hidden; overflow-y: auto; max-height: 497px; margin: 0px 0px 0px -6px;">
                        <br />
                        <div id="divChart" runat="server" style="padding: 6px 2px 2px 2px; width: 100%;">
                            <table style="width: 100%; margin: 0px; padding: 0px;" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Literal ID="ltPieEnq" runat="server"></asp:Literal></td>
                                    <td>
                                        <asp:Literal ID="ltPieReg" runat="server"></asp:Literal></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="ltPieStud" runat="server"></asp:Literal></td>
                                    <td>
                                        <asp:Literal ID="ltPieTC" runat="server"></asp:Literal></td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
                <br />
                <br />
            </div>
        </div>
    </form>
</body>
</html>
