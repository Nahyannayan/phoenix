Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports studClass
Partial Class Students_studministryapproval_Sharjah
    Inherits System.Web.UI.Page

    Shared section As New Dictionary(Of String, String)()
    Shared grade As New Dictionary(Of String, String)()
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            section.Clear()
            grade.Clear()
            CheckMenuandUser()
            Dim acc As New studClass
            ddlAcademicYear = acc.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
            BindGrid()
            BindHeader()
        End If
        'For Each row As GridViewRow In gridMiniApp.Rows
        '    Dim linkdownlaod As LinkButton = DirectCast(row.FindControl("lblPrint"), LinkButton)
        '    ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(linkdownlaod)

        'Next
        Print()

    End Sub
    Public Sub Print()
        For Each row As GridViewRow In gridMiniApp.Rows
            Dim Encr_decrData As New Encryption64
            'Dim stuid = Encr_decrData.Encrypt(DirectCast(row.FindControl("HiddenStudid"), HiddenField).Value)
            Dim stuid = DirectCast(row.FindControl("HiddenStudid"), HiddenField).Value
            Dim printlink As LinkButton = DirectCast(row.FindControl("lblPrint"), LinkButton)
            ' If Session("SBsuid") <> 131001 And Session("SBsuid") <> 131002 Then
            '    Dim val = "javascript:openw("
            '    val &= "'" & stuid & "'"
            '    val &= "); return false;"
            '    printlink.OnClientClick = val
            'Else
            If Session("SBsuid") = "165010" Then
                Dim val = "javascript:openrak("
                val &= stuid
                val &= "); return false;"
                printlink.OnClientClick = val
            Else
                Dim val = "javascript:opensha("
                val &= stuid
                val &= "); return false;"
                printlink.OnClientClick = val
            End If
            

            'End If


        Next
    End Sub
    Public Sub CheckMenuandUser()
        Dim Encr_decrData As New Encryption64
        ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
        Dim USR_NAME As String = Session("sUsr_name")

        If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100072") Then
            If Not Request.UrlReferrer Is Nothing Then
                Response.Redirect(Request.UrlReferrer.ToString())
            Else

                Response.Redirect("~\noAccess.aspx")
            End If
        Else
            If (Session("SBsuid") <> "131001") And (Session("SBsuid") <> "131002") And (Session("SBsuid") <> "133006") And (Session("SBsuid") <> "135010") And (Session("SBsuid") <> "165010") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
        End If

    End Sub
    Public Sub BindHeader()
        If gridMiniApp.Rows.Count > 0 Then

            Dim sql_con = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim ddstream As DropDownList = gridMiniApp.HeaderRow.FindControl("ddstream")
            Dim sql_query As String = ""
            Dim ds As DataSet
            sql_query = "select * from STREAM_M "
            ds = SqlHelper.ExecuteDataset(sql_con, CommandType.Text, sql_query)
            ddstream.DataSource = ds
            ddstream.DataValueField = "STM_ID"
            ddstream.DataTextField = "STM_DESCR"
            ddstream.DataBind()
            Dim list As New ListItem
            list.Text = "All"
            list.Value = "-1"
            ddstream.Items.Insert(0, list)

        End If
    End Sub

    Public Sub BindGrid()
        Dim iDate As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        Try
            Dim sql_con = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sql_query = "select *,(case isnull(STU_MOE_UPDATE,0) when 0 then '~/Images/cross.gif' else '~/Images/tick.gif' end) moeupdateimagepath  from STUDENT_M INNER JOIN SECTION_M ON  STU_ACD_ID='" & ddlAcademicYear.SelectedValue & "' AND SECTION_M.SCT_BSU_ID = STUDENT_M.STU_BSU_ID AND SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID" & _
                            " INNER JOIN Grade_M_MOE_SHJ  ON STUDENT_M.STU_GRD_ID = Grade_M_MOE_SHJ.GRD_ID" & _
                            " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_GRD_ID = Grade_M_MOE_SHJ.GRD_ID AND GRADE_BSU_M.GRM_BSU_ID= STUDENT_M.STU_BSU_ID AND GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID and STUDENT_M.stu_bsu_id='" & Session("sBsuid").ToString & "' WHERE 1=1 AND STU_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "'"
            If Session("BSU_bMOE_TC") = "True" Then ' -- SHOW TC

            Else
                sql_query = sql_query & " AND (STUDENT_M.STU_CURRSTATUS <> 'CN') AND (CONVERT(datetime, STUDENT_M.STU_LEAVEDATE) > CONVERT(datetime, '" & iDate & "') OR " & _
                     " CONVERT(datetime, STUDENT_M.STU_LEAVEDATE) IS NULL) "
            End If
            sql_query = sql_query & " ORDER BY GRD_DISPLAYORDER,SCT_DESCR,STU_PASPRTNAME"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(sql_con, CommandType.Text, sql_query)
            gridMiniApp.DataSource = ds
            gridMiniApp.DataBind()
            HeaderBind(ds)
        Catch ex As Exception
            BindNull()
        End Try



    End Sub

    Protected Sub gridMiniApp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridMiniApp.RowCommand

        Dim Encr_decrData As New Encryption64
        Dim stuid = Encr_decrData.Encrypt(e.CommandArgument)
        Dim MainMnu_code = Request.QueryString("MainMnu_code")
        Dim flag = Encr_decrData.Encrypt("1")

        If e.CommandName = "View" Then
            Dim mInfo As String = "&MainMnu_code=" & Request.QueryString("MainMnu_code").ToString() & "&datamode=" & Request.QueryString("datamode").ToString()
            Response.Redirect("~/Students/studlistview_new.aspx?stuid=" & stuid & "&flag='" & flag & "'" & mInfo)

        End If

        'If e.CommandName = "Print" Then
        '    Response.Write("<Script> window.open('studMOEPrint.aspx?stuid=" & stuid & "') </Script>")
        '    ''Response.Redirect("~/Students/studMOEPrint.aspx?stuid=" & stuid)
        'End If

    End Sub

    Public Sub BindSearchGrid()
        Try
            Dim sql_con = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim tno As TextBox = gridMiniApp.HeaderRow.FindControl("txtNo")
            Dim tname As TextBox = gridMiniApp.HeaderRow.FindControl("txtName")
            Dim ddgrade As DropDownList = gridMiniApp.HeaderRow.FindControl("ddgrade")
            Dim ddsection As DropDownList = gridMiniApp.HeaderRow.FindControl("ddsection")
            Dim ddstream As DropDownList = gridMiniApp.HeaderRow.FindControl("ddstream")

            Dim strSidsearch As String()
            Dim strSearch As String
            Dim searchfilter As String = ""
            Dim val1 = "-1", val2 = "-1", val3 = "-1"
            Dim textval1 = ""
            Dim textval2 = ""
            Dim iDate As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim sql_query = "select *,(case isnull(STU_MOE_UPDATE,0) when 0 then '~/Images/cross.gif' else '~/Images/tick.gif' end) moeupdateimagepath  from STUDENT_M INNER JOIN SECTION_M ON  STU_ACD_ID='" & ddlAcademicYear.SelectedValue & "' AND SECTION_M.SCT_BSU_ID = STUDENT_M.STU_BSU_ID AND SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID" & _
                            " INNER JOIN Grade_M_MOE_SHJ  ON STUDENT_M.STU_GRD_ID = Grade_M_MOE_SHJ.GRD_ID" & _
                            " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_GRD_ID = Grade_M_MOE_SHJ.GRD_ID AND GRADE_BSU_M.GRM_BSU_ID= STUDENT_M.STU_BSU_ID AND GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID and STUDENT_M.stu_bsu_id='" & Session("sBsuid").ToString & "' WHERE 1=1 AND STU_ACD_ID='" & ddlAcademicYear.SelectedItem.Value & "'"
            If Session("BSU_bMOE_TC") = "True" Then ' -- SHOW TC

            Else
                sql_query = sql_query & " AND (STUDENT_M.STU_CURRSTATUS <> 'CN') AND (CONVERT(datetime, STUDENT_M.STU_LEAVEDATE) > CONVERT(datetime, '" & iDate & "') OR " & _
                     " CONVERT(datetime, STUDENT_M.STU_LEAVEDATE) IS NULL) "
            End If

            If tno.Text.Trim() <> "" Then
                textval1 = tno.Text.Trim()
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                If searchfilter <> "" Then
                    searchfilter = searchfilter & GetSearchString("STUDENT_M.STU_NO", tno.Text.Trim(), strSearch)
                Else
                    searchfilter = searchfilter & GetSearchString1("STUDENT_M.STU_NO", tno.Text.Trim(), strSearch)
                End If

            End If
            If tname.Text.Trim() <> "" Then
                textval2 = tname.Text.Trim()
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                If searchfilter <> "" Then
                    searchfilter = searchfilter & GetSearchString("STUDENT_M.STU_PASPRTNAME", tname.Text.Trim(), strSearch)
                Else
                    searchfilter = searchfilter & GetSearchString1("STUDENT_M.STU_PASPRTNAME", tname.Text.Trim(), strSearch)
                End If
            End If

            If ddgrade.SelectedIndex > 0 Then
                strSearch = ddgrade.SelectedValue
                val1 = ddgrade.SelectedValue
                If searchfilter <> "" Then
                    searchfilter = searchfilter & GetSearchString("STUDENT_M.STU_GRD_ID", strSearch, "LI")
                Else
                    searchfilter = searchfilter & GetSearchString1("STUDENT_M.STU_GRD_ID", strSearch, "LI")
                End If

            End If
            If ddsection.SelectedIndex > 0 Then
                strSearch = ddsection.SelectedValue
                val2 = ddsection.SelectedValue
                If searchfilter <> "" Then
                    searchfilter = searchfilter & " AND STUDENT_M.STU_SCT_ID LIKE COALESCE(" & strSearch & ",STUDENT_M.STU_SCT_ID) "
                Else
                    searchfilter = searchfilter & "  STUDENT_M.STU_SCT_ID LIKE COALESCE(" & strSearch & ",STUDENT_M.STU_SCT_ID) "
                End If
            End If
            If ddstream.SelectedIndex > 0 Then
                strSearch = ddstream.SelectedValue
                val3 = ddstream.SelectedValue
                If searchfilter <> "" Then
                    searchfilter = searchfilter & " AND STUDENT_M.STU_STM_ID LIKE COALESCE(" & strSearch & ",STUDENT_M.STU_STM_ID) "

                Else
                    searchfilter = searchfilter & "  STUDENT_M.STU_STM_ID LIKE COALESCE(" & strSearch & ",STUDENT_M.STU_STM_ID) "

                End If
            End If


            If searchfilter <> "" Then
                sql_query = sql_query & " AND " & searchfilter
            End If


            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(sql_con, CommandType.Text, sql_query)
            If ds.Tables(0).Rows.Count > 0 Then
                Session("searchdata") = ds
                gridMiniApp.DataSource = ds
                gridMiniApp.DataBind()
            Else
                BindNull()
            End If
            HeaderBind(ds)
            BindHeader()
            Dim tno1 As TextBox = gridMiniApp.HeaderRow.FindControl("txtNo")
            Dim tname1 As TextBox = gridMiniApp.HeaderRow.FindControl("txtName")
            tno1.Text = textval1
            tname1.Text = textval2

            'Dim ddgrade1 As New DropDownList
            'ddgrade1 = gridMiniApp.HeaderRow.FindControl("ddgrade")
            'Dim ddsection1 As New DropDownList
            'ddsection1 = gridMiniApp.HeaderRow.FindControl("ddsection")
            'Dim ddstream1 As DropDownList = gridMiniApp.HeaderRow.FindControl("ddstream")



            'ddstream1.ClearSelection()
            'Dim li As New ListItem
            'li.Text = ddstream.SelectedItem.Text
            'li.Value = ddstream.SelectedValue
            'ddstream.ClearSelection()
            'ddstream1.Items(ddstream1.Items.IndexOf(li)).Selected = True


            'ddsection1.ClearSelection()
            'Dim li3 As New ListItem
            'li3.Text = ddsection.SelectedItem.Text
            'li3.Value = ddsection.SelectedValue
            'ddsection.ClearSelection()
            'ddsection1.Items(ddsection1.Items.IndexOf(li3)).Selected = True
            ''ddsection1.Text = ddsection.SelectedItem.Text

            ''ddgrade1.ClearSelection()
            ''Dim li2 As New ListItem
            ''li2.Text = ddgrade.SelectedItem.Text
            ''li2.Value = ddgrade.SelectedValue
            ''ddgrade.ClearSelection()
            ''ddgrade1.Items(ddgrade1.Items.IndexOf(li2)).Selected = True
            ' ''ddgrade1.Text = ddgrade.SelectedItem.Text

        Catch ex As Exception
            BindNull()
        End Try


    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  COALESCE('%" & value & "'," + field + ") "
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE COALESCE('%" & value & "'," + field + ") "
            End If
        End If
        Return strFilter
    End Function

    Public Function GetSearchString1(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = "  " + field + " LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "NLI" Then
                strFilter = "   " + field + " NOT LIKE COALESCE('%" & value & "%'," + field + ") "
            ElseIf strSearch = "SW" Then
                strFilter = "  " + field + "  LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "NSW" Then
                strFilter = "  " + field + "  NOT LIKE COALESCE('" & value & "%'," + field + ") "
            ElseIf strSearch = "EW" Then
                strFilter = "  " + field + " LIKE  COALESCE('%" & value & "'," + field + ") "
            ElseIf strSearch = "NEW" Then
                strFilter = "  " + field + " NOT LIKE COALESCE('%" & value & "'," + field + ") "
            End If
        End If
        Return strFilter
    End Function

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gridMiniApp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gridMiniApp.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gridMiniApp.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gridMiniApp.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Sub HeaderBind(ByVal ds As DataSet)
        Try


            Dim ddgrade As DropDownList = gridMiniApp.HeaderRow.FindControl("ddgrade")
            Dim ddsection As DropDownList = gridMiniApp.HeaderRow.FindControl("ddsection")
            If section.Values.Count = 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim i = 0
                    For i = 0 To (ds.Tables(0).Rows.Count - 1)
                        Dim secid As Integer = Convert.ToInt32(ds.Tables(0).Rows(i).Item("SCT_ID").ToString())
                        Dim sec_des = ds.Tables(0).Rows(i).Item("SCT_DESCR").ToString()
                        Dim gradeid As String = ds.Tables(0).Rows(i).Item("STU_GRD_ID").ToString()
                        Dim grade_des As String = ds.Tables(0).Rows(i).Item("GRM_DISPLAY").ToString()
                        If section.ContainsKey(secid) Then
                        Else
                            section.Add(secid, sec_des)
                        End If
                        If grade.ContainsKey(gradeid) Then
                        Else
                            grade.Add(gradeid, grade_des)
                        End If
                    Next
                    ddsection.DataSource = section
                    ddsection.DataTextField = "Value"
                    ddsection.DataValueField = "Key"
                    ddsection.DataBind()
                    ddgrade.DataSource = grade
                    ddgrade.DataTextField = "Value"
                    ddgrade.DataValueField = "Key"
                    ddgrade.DataBind()

                    Dim list As New ListItem
                    list.Text = "All"
                    list.Value = "-1"
                    ddgrade.Items.Insert(0, list)
                    ddsection.Items.Insert(0, list)
                End If
            Else
                ddsection.DataSource = section
                ddsection.DataTextField = "Value"
                ddsection.DataValueField = "Key"
                ddsection.DataBind()
                ddgrade.DataSource = grade
                ddgrade.DataTextField = "Value"
                ddgrade.DataValueField = "Key"
                ddgrade.DataBind()

                Dim list As New ListItem
                list.Text = "All"
                list.Value = "-1"

                ddgrade.Items.Insert(0, list)
                ddsection.Items.Insert(0, list)

            End If
            'For Each row As GridViewRow In gridMiniApp.Rows
            '    Dim linkdownlaod As LinkButton = DirectCast(row.FindControl("lblPrint"), LinkButton)
            '    ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(linkdownlaod)

            'Next
        Catch ex As Exception

        End Try
    End Sub


    Public Sub BindNull()
        Try
            Dim dt As New DataTable
            dt.Columns.Add("STU_ID")
            dt.Columns.Add("STU_NO")
            dt.Columns.Add("STU_PASPRTNAME")
            dt.Columns.Add("STU_LASTNAME")
            dt.Columns.Add("STU_GRD_ID")
            dt.Columns.Add("SCT_DESCR")
            dt.Columns.Add("STM_DESCR")
            dt.Columns.Add("GRM_DISPLAY")
            dt.Columns.Add("moeupdateimagepath")
            dt.Columns.Add("STU_MOE_UPDATE_DATE")
            dt.Columns.Add("STU_MOERETURNDATE")
            Dim drow As DataRow = dt.NewRow()
            drow.Item("STU_ID") = ""
            drow.Item("STU_NO") = ""
            drow.Item("STU_FIRSTNAME") = ""
            drow.Item("STU_LASTNAME") = ""
            drow.Item("STU_GRD_ID") = ""
            drow.Item("SCT_DESCR") = ""
            drow.Item("STM_DESCR") = ""
            drow.Item("GRM_DISPLAY") = ""
            drow.Item("moeupdateimagepath") = ""
            drow.Item("STU_MOE_UPDATE_DATE") = ""
            drow.Item("STU_MOERETURNDATE") = ""
            dt.Rows.Add(drow)
            gridMiniApp.DataSource = dt
            gridMiniApp.DataBind()
            Dim link1 As LinkButton = gridMiniApp.Rows(0).FindControl("lblView")
            Dim link2 As LinkButton = gridMiniApp.Rows(0).FindControl("lblPrint")
            link1.Visible = False
            link2.Visible = False
        Catch ex As Exception

        End Try


    End Sub
    Protected Sub ddstream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSearchGrid()
    End Sub

    Protected Sub ddsection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSearchGrid()
    End Sub

    Protected Sub ddgrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSearchGrid()
    End Sub

    Protected Sub btnNo_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSearchGrid()
    End Sub

    Protected Sub btnName_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSearchGrid()
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindGrid()
    End Sub

    Protected Sub gridMiniApp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gridMiniApp.PageIndex = e.NewPageIndex
        If Session("searchdata") Is Nothing Then
            BindGrid()
        Else
            Dim ds As DataSet = Session("searchdata")
            gridMiniApp.DataSource = ds
            gridMiniApp.DataBind()
            HeaderBind(ds)
        End If

    End Sub

    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub


End Class
