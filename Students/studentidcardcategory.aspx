﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="studentidcardcategory.aspx.vb" MasterPageFile="~/mainMasterPage.master" Inherits="Students_studentidcardcategory" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="C1" ContentPlaceHolderID="cphMasterpage" runat="server">
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Define ID Card Category"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


                <div class="matters">
                    <table width="100%">
                        <tr>
                            <td align="left">

                                <table width="100%">
                                    <tr>
                                        <td>ID Category Name</td>
                                        <td>
                                            <asp:TextBox ID="txtcatname" runat="server" ></asp:TextBox>
                                        </td>
                                        <td>ID Category Image</td>
                                        <td>
                                            <asp:DropDownList ID="ddcatimage" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>Color Code</td>
                                        <td>
                                            <telerik:RadColorPicker runat="server" ID="RadColorPicker5" PaletteModes="All" Overlay="true" KeepInScreenBounds="true"   />
                                        </td>
                                        <td colspan="2">
                                            <asp:Button ID="btnsave" runat="server"   CssClass="button" Text="Save" />
                                            <asp:Button ID="btnupdate" runat="server"   CssClass="button"
                                                Text="Update" Visible="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <table width="100%">
                        <tr>
                            <td class="title-bg-small">ID Card Categories List</td>
                        </tr>
                        <tr>
                            <td style="text-align:center">


                                <asp:GridView ID="GridDocuments" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-row"
                                    Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Color">
                                            <HeaderTemplate>
                                                Color
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                        <asp:TextBox ID="TextStatus" runat="server" 
                            BackColor='<%#System.Drawing.Color.FromName(DataBinder.Eval(Container, "DataItem.CT_COLOR_CODE") ) %>' 
                            BorderWidth="0" EnableTheming="false" ReadOnly="true"  ></asp:TextBox>
                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From">
                                            <HeaderTemplate>
                                                Category Name
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                      <%#Eval("CAT_DES")%>
                                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To">
                                            <HeaderTemplate>
                                                Category Image

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                     
                                        <%# Eval("IMAGE_DES")%>
                                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Edit">
                                            <HeaderTemplate>
                                                Edit

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                        <asp:LinkButton ID="LinkEdit" runat="server" 
                            CommandArgument='<%#Eval("ID_CT_ID")%>' CommandName="Editing">Edit</asp:LinkButton>                    
                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Delete">
                                            <HeaderTemplate>
                                                Delete

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                        <asp:LinkButton ID="LinkDelete" runat="server" 
                            CommandArgument='<%#Eval("ID_CT_ID")%>' CommandName="deleting">Delete</asp:LinkButton>
                        <ajaxToolkit:ConfirmButtonExtender ID="C1" runat="server" 
                            ConfirmText="Do you wish to continue?" TargetControlID="LinkDelete">
                        </ajaxToolkit:ConfirmButtonExtender>
                    </center>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="griditem"  Wrap="False" />
                                    <EmptyDataRowStyle Wrap="False" />
                                    <SelectedRowStyle CssClass="Green" Wrap="False" />
                                    <HeaderStyle CssClass="gridheader_pop"  Wrap="False" />
                                    <EditRowStyle Wrap="False" />
                                    <AlternatingRowStyle CssClass="griditem_alternative" Wrap="False" />
                                </asp:GridView>



                            </td>
                        </tr>
                    </table>

                    <asp:HiddenField ID="Hiddenrid" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
