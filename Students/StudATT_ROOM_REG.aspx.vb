Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections

Partial Class Students_StudATT_ROOM_REG
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64

    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function


    Sub GetAcademicSTARTDT_ENDDT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            str_Sql = " select ACD_STARTDT,ACD_ENDDT from ACADEMICYEAR_D where ACD_ID='" & ACD_ID & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ACD_STARTDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_StartDt"))
                ViewState("ACD_ENDDT") = String.Format("{0:" & OASISConstants.DateFormat & "}", ds.Tables(0).Rows(0).Item("ACD_ENDDT"))
            Else
                ViewState("ACD_STARTDT") = ""
                ViewState("ACD_ENDDT") = ""

            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave2)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnCancel)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnCancel2)

        If Page.IsPostBack = False Then

            Try

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'if query string returns Eid  if datamode is view state

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "C115005" And ViewState("MainMnu_code") <> "S059056") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else

                    hfDate.Value = DateTime.Now
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    btnSave.Attributes.Add("onclick", "hideButton()")
                    btnSave2.Attributes.Add("onclick", "hideButton()")
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call bindAcademic_Year()
                    Call bindMonthPRESENT()
                    Call GETPERIODS()
                    Call GetEmpID()
                    btnSave2.Visible = False
                    btnCancel.Visible = False
                    btnCancel2.Visible = False
                    Session("dt_ATT_GROUP") = CreateDataTable()
                    Session("dt_ATT_GROUP").Rows.Clear()

                    txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    Call check_transport_status_bsu()

                    If ViewState("IsvalidBsu") = 0 Then
                        gvInfo.Columns(4).Visible = False
                        trStatus.Visible = False
                    End If
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        End If

    End Sub

    Sub bindMonthPRESENT()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2,ISNULL(BSU_bAPP_LEAVE_ABSENT,'False') AS bAPP_LEAVE_ABS " _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
                ViewState("bAPP_LEAVE_ABS") = ds.Tables(0).Rows(0)(2)
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetEmpID()

        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " select usr_emp_id from users_m where usr_id='" & Session("sUsr_id") & "'"
            ViewState("EMP_ID") = CStr(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = " SELECT  ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, ACADEMICYEAR_D.ACD_ID as ACD_ID, ACADEMICYEAR_D.ACD_ACY_ID " & _
            " FROM  ACADEMICYEAR_D INNER JOIN  ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
            " where acd_bsu_id = '" & Session("sBSUID") & "'"
            ddlAcdYear.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlAcdYear.Items.Add(New ListItem(reader("ACY_DESCR"), reader("ACD_ID")))
            End While
            reader.Close()



            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcdYear_SelectedIndexChanged(ddlAcdYear, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindSubject()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String

            str_Sql = " SELECT DISTINCT SBG_ID ,CASE WHEN SBG_PARENTS_SHORT ='NA'THEN SBG_DESCR ELSE SBG_DESCR+ ' - '+ SBG_PARENTS_SHORT END AS DESCR2 " & _
" FROM SUBJECTS_GRADE_S  INNER JOIN GRADE_BSU_M ON SUBJECTS_GRADE_S.SBG_GRD_ID = GRADE_BSU_M.GRM_GRD_ID " & _
" AND SUBJECTS_GRADE_S.SBG_ACD_ID = GRADE_BSU_M.GRM_ACD_ID INNER JOIN STREAM_M ON SUBJECTS_GRADE_S.SBG_STM_ID = STREAM_M.STM_ID " & _
" WHERE SBG_BSU_ID = '" & Session("sBsuid") & "' AND SBG_GRD_ID= '" & ddlGrade.SelectedValue & "'  and SUBJECTS_GRADE_S.SBG_ACD_ID='" & ddlAcdYear.SelectedValue & "' order by DESCR2 "

            ddlSubject.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlSubject.Items.Add(New ListItem(reader("DESCR2"), reader("SBG_ID")))
            End While
            reader.Close()
            ddlSubject_SelectedIndexChanged(ddlSubject, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindSubjectGroup()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim SBG_ID As String = String.Empty

            If ddlSubject.SelectedIndex = -1 Then
                SBG_ID = ""
            Else
                SBG_ID = ddlSubject.SelectedValue
            End If

            str_Sql = " SELECT SGR_ID,SGR_DESCR  FROM dbo.GROUPS_M WHERE SGR_ACD_ID='" & ddlAcdYear.SelectedValue & "' AND SGR_GRD_ID='" & ddlGrade.SelectedValue & "'" & _
                    " AND SGR_SBG_ID ='" & SBG_ID & "' ORDER BY SGR_ID "

            ddlSubjectGroup.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlSubjectGroup.Items.Add(New ListItem(reader("SGR_DESCR"), reader("SGR_ID")))
            End While
            reader.Close()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub




    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Call GetAcademicSTARTDT_ENDDT()
        Call GetGrades()
        GETROOM_ATT_PARAM()
    End Sub
    Sub GETPERIODS()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT RAP_PERIOD_ID,RAP_PERIOD_DESCR FROM ATT.ROOM_ATTENDANCE_PERIOD"
            ddlPERIOD.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlPERIOD.Items.Add(New ListItem(reader("RAP_PERIOD_DESCR"), reader("RAP_PERIOD_ID")))
            End While
            reader.Close()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub GetGrades()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            str_Sql = "SELECT DISTINCT GRADE_BSU_M.GRM_GRD_ID, GRADE_BSU_M.GRM_DISPLAY, GRADE_M.GRD_DISPLAYORDER " & _
" FROM GRADE_BSU_M INNER JOIN  GRADE_M ON GRADE_BSU_M.GRM_GRD_ID = GRADE_M.GRD_ID " & _
" WHERE (GRADE_BSU_M.GRM_ACD_ID ='" & ddlAcdYear.SelectedValue & "')ORDER BY GRADE_M.GRD_DISPLAYORDER "

            ddlGrade.Items.Clear()

            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_Sql)
            While reader.Read
                ddlGrade.Items.Add(New ListItem(reader("GRM_DISPLAY"), reader("GRM_GRD_ID")))
            End While
            reader.Close()

            ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSubject()
    End Sub

    Protected Sub ddlSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindSubjectGroup()
    End Sub


    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim t1 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            If t1 > t2 Then
                lblError.Text = "You cannot select a day greater than today!"
                txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Else

                If ddlAcdYear.SelectedIndex = -1 Then
                    lblError.Text = "Academic Year not selected"

                ElseIf ddlSubjectGroup.SelectedIndex = -1 Then
                    lblError.Text = "Subject Group not selected"
                Else

                    If ValidateDate() = "0" Then
                        Call Add_clicked()
                        Call backGround()
                    End If
                End If
            End If
        Catch ex As Exception
            lblError.Text = "You cannot select a day greater than today and must be a valid date!"
            txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        End Try
    End Sub


    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtDate.Text <> "" Then
                Dim strfDate As String = txtDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"

            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
            Return "-1"
        End Try

    End Function
    Sub Add_clicked()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim totRow As Integer
        Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue

        Dim EMP_ID As String = ViewState("EMP_ID")


        Dim AttDate As String

        AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)


        Try
            Dim DupCount As Integer = RecordCount(ACD_ID, SGR_ID, AttDate)

            If DupCount = 0 Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_Sql As String

                str_Sql = "SELECT COUNT(SSD_ID) FROM STUDENT_GROUPS_S WHERE SSD_SGR_ID='" & SGR_ID & "' AND SSD_ACD_ID='" & ACD_ID & "' AND SSD_GRD_ID='" & GRD_ID & "'"

                totRow = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
                If totRow > 0 Then
                    ViewState("datamode") = "add"


                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call BindAdd_Attendance()
                    setControl()
                Else

                    lblError.Text = "Record currently not updated. Please Contact System Admin"
                End If
            Else

                lblError.Text = "Attendances already marked for the given date & Group!!!"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub

    Function RecordCount(ByVal ACD_ID As String, ByVal SGR_ID As String, ByVal AttDate As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_Sql As String
        Dim PRD_ID As String = ddlPERIOD.SelectedValue

        str_Sql = "select count(RAL_ID)  from ATT.ROOM_ATTENDANCE_LOG where  RAL_ACD_ID='" & ACD_ID & "' and RAL_SGR_ID='" & SGR_ID & "' AND RAL_ATTDT='" & AttDate & "' AND RAL_PERIOD_ID='" & PRD_ID & "'"


        RecordCount = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
    End Function
    Sub BindAdd_Attendance()
        Try
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
            Dim GRD_ID As String = ddlGrade.SelectedValue
            Dim PERIOD_ID As String = ddlPERIOD.SelectedValue
            Dim EMP_ID As String = ViewState("EMP_ID")
            Dim AttDate As String

            AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)


            Session("dt_ATT_GROUP").Rows.Clear()
            ViewState("idTr") = 1
            Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GETSTUDATT_REGIST_ROOMGROUP(ACD_ID, GRD_ID, SGR_ID, AttDate, PERIOD_ID, ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"))
                While readerSTUD_ATT_ADD.Read
                    Dim rDt As DataRow
                    Dim cABS1 As Boolean
                    Dim cABS2 As Boolean
                    Dim cABS3 As Boolean
                    rDt = Session("dt_ATT_GROUP").NewRow
                    rDt("SRNO") = ViewState("idTr")
                    rDt("RAL_ID") = "0"
                    rDt("STU_ID") = Convert.ToString(readerSTUD_ATT_ADD("STU_ID"))
                    rDt("STU_NO") = Convert.ToString(readerSTUD_ATT_ADD("STU_NO"))
                    rDt("STUDNAME") = Convert.ToString(readerSTUD_ATT_ADD("STUDNAME"))
                    rDt("STATUS") = ""
                    rDt("REMARKS") = ""
                    rDt("SGENDER") = Convert.ToString(readerSTUD_ATT_ADD("SGENDER"))
                    rDt("APPLEAVE") = ""
                    rDt("minList") = Convert.ToString(readerSTUD_ATT_ADD("MINLIST"))
                    rDt("TRANSSTATUS") = Convert.ToString(readerSTUD_ATT_ADD("cssclass"))
                    rDt("TRANSSTATUS_CODE") = Convert.ToString(readerSTUD_ATT_ADD("SCAN_STAT_CODE"))
                    If InStr(Convert.ToString(readerSTUD_ATT_ADD(13)), "|") > 0 Then
                        If Right(Convert.ToString(readerSTUD_ATT_ADD(13)), InStr(Convert.ToString(readerSTUD_ATT_ADD(13)), "|") + 1) = "2" Then
                            cABS1 = True
                        Else
                            cABS1 = False
                        End If

                        rDt("DAY1") = Left(Convert.ToString(readerSTUD_ATT_ADD(13)), InStr(Convert.ToString(readerSTUD_ATT_ADD(13)), "|") - 1)
                    Else
                        rDt("DAY1") = Convert.ToString(readerSTUD_ATT_ADD(13))
                    End If


                    If InStr(Convert.ToString(readerSTUD_ATT_ADD(14)), "|") > 0 Then

                        If Right(Convert.ToString(readerSTUD_ATT_ADD(14)), InStr(Convert.ToString(readerSTUD_ATT_ADD(14)), "|") + 1) = "2" Then
                            cABS2 = True
                        Else
                            cABS2 = False
                        End If

                        rDt("DAY2") = Left(Convert.ToString(readerSTUD_ATT_ADD(14)), InStr(Convert.ToString(readerSTUD_ATT_ADD(14)), "|") - 1)
                    Else
                        rDt("DAY2") = Convert.ToString(readerSTUD_ATT_ADD(14))
                    End If


                    If InStr(Convert.ToString(readerSTUD_ATT_ADD(15)), "|") > 0 Then
                        If Right(Convert.ToString(readerSTUD_ATT_ADD(15)), InStr(Convert.ToString(readerSTUD_ATT_ADD(15)), "|") + 1) = "2" Then
                            cABS3 = True
                        Else
                            cABS3 = False
                        End If

                        rDt("DAY3") = Left(Convert.ToString(readerSTUD_ATT_ADD(15)), InStr(Convert.ToString(readerSTUD_ATT_ADD(15)), "|") - 1)

                    Else
                        rDt("DAY3") = Convert.ToString(readerSTUD_ATT_ADD(15))
                    End If
                    If cABS1 = True And cABS2 = True And cABS3 = True Then
                        rDt("ContAbs") = "1"
                    Else
                        rDt("ContAbs") = "0"
                    End If


                    hfDay1.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(13)).Replace("|", " ")
                    hfDay2.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(14)).Replace("|", " ")
                    hfDay3.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(15)).Replace("|", " ")

                    Session("dt_ATT_GROUP").Rows.Add(rDt)
                    ViewState("idTr") = ViewState("idTr") + 1
                End While
            End Using

            Using readerSTUD_GRADE_SECTION_HOLIDAY As SqlDataReader = AccessStudentClass.GetSTUD_ROOM_HOLIDAY_GROUP(ACD_ID, SGR_ID, AttDate)
                If readerSTUD_GRADE_SECTION_HOLIDAY.HasRows Then
                    While readerSTUD_GRADE_SECTION_HOLIDAY.Read
                        ViewState("STYPE") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("STYPE"))
                        ViewState("WEEKEND1") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND1"))
                        ViewState("bWEEKEND1") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND1"))
                        ViewState("WEEKEND2") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND2"))
                        ViewState("bWEEKEND2") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND2"))
                    End While

                Else
                    ViewState("STYPE") = "Empty"
                    ViewState("WEEKEND1") = ""
                    ViewState("bWEEKEND1") = False
                    ViewState("WEEKEND2") = ""
                    ViewState("bWEEKEND2") = False

                End If

            End Using

            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub BindAdd_Attendance_APPROVED()
        Try
            Dim RAS_STU_ID As String = String.Empty
            Dim RAS_PAR_ID As String = String.Empty
            Dim RAS_REMARKS As String = String.Empty
            Dim ACD_ID As String = ddlAcdYear.SelectedValue
            Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
            Dim GRD_ID As String = ddlGrade.SelectedValue
            Dim EMP_ID As String = ViewState("EMP_ID")
            Dim lastAttDate As String

            lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Using readerGetALG_StudentID As SqlDataReader = AccessStudentClass.GetRAL_StudentID_ROOMADD(ACD_ID, SGR_ID, lastAttDate)
                While readerGetALG_StudentID.Read
                    RAS_STU_ID = Convert.ToString(readerGetALG_StudentID("ALS_STU_ID"))
                    RAS_PAR_ID = Convert.ToString(readerGetALG_StudentID("ALS_APD_ID"))
                    RAS_REMARKS = Convert.ToString(readerGetALG_StudentID("ALS_REMARKS"))
                    For j As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(j)
                        Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                        If Trim(RAS_STU_ID) = Trim(Temp_stud_ID) Then
                            ddlStatus.ClearSelection()
                            DirectCast(row.FindControl("txtRemarks"), TextBox).Text = RAS_REMARKS
                            ddlStatus.Items.FindByValue(RAS_PAR_ID).Selected = True
                        End If
                    Next
                End While
            End Using







            Dim Astud_ID As New ArrayList
            Using readerSTUD_Leave_Approval As SqlDataReader = AccessStudentClass.GetSTUD_Leave_Approval_ROOM(ACD_ID, SGR_ID, GRD_ID, lastAttDate)
                While readerSTUD_Leave_Approval.Read
                    Dim Stud_ID As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_STU_ID"))
                    Dim Remarks As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_REMARKS"))
                    Dim AppLeave As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_APPRLEAVE"))
                    Dim SLA_APD_ID As Integer = Convert.ToInt64(readerSTUD_Leave_Approval("SLA_APD_ID"))

                    ' Astud_ID.Add(Stud_ID)

                    For j As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(j)

                        Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)

                        If Trim(Stud_ID) = Trim(Temp_stud_ID) Then
                            ddlStatus.ClearSelection()
                            DirectCast(row.FindControl("txtRemarks"), TextBox).Text = Remarks
                            DirectCast(row.FindControl("lblAppLeave"), Label).Text = AppLeave
                            If AppLeave = "APPROVED" Then
                                Astud_ID.Add(Stud_ID)
                                If ViewState("bAPP_LEAVE_ABS") = "True" Then
                                    If SLA_APD_ID <> 0 Then
                                        If Not ddlStatus.Items.FindByValue(SLA_APD_ID) Is Nothing Then
                                            ddlStatus.Items.FindByValue(SLA_APD_ID).Selected = True
                                        Else
                                            ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                                        End If
                                    Else
                                        ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                                    End If
                                Else
                                    If SLA_APD_ID <> 0 Then
                                        If Not ddlStatus.Items.FindByValue(SLA_APD_ID) Is Nothing Then
                                            ddlStatus.Items.FindByValue(SLA_APD_ID).Selected = True
                                        Else
                                            ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                                        End If
                                    Else
                                        ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                                    End If
                                End If
                            ElseIf AppLeave = "NOT APPROVED" Then
                                ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                            End If
                        End If
                    Next
                End While
            End Using

            Session("Astud_ID_GROUP") = Astud_ID
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Sub gridbind()
        Try
            gvInfo.DataSource = Session("dt_ATT_GROUP")
            gvInfo.DataBind()
            Call SetSTYPE()
            Call CheckWeekend_Attendance()
            If ViewState("flag_weekend") = "0" Then
                If ViewState("datamode") = "add" Then
                    Call BindAdd_Attendance_APPROVED()
                ElseIf ViewState("datamode") = "edit" Then
                    Call BindEdit_AttendanceAPPROVED()
                End If
                Call DisableControls()
                gvInfo.Visible = True
                btnSave.Visible = True
                btnSave2.Visible = True
                btnCancel.Visible = True
                btnCancel2.Visible = True
                setControl()

            Else
                gvInfo.Visible = False
                btnSave.Visible = False
                btnSave2.Visible = False
                btnCancel.Visible = True
                btnCancel2.Visible = False
                setControl()
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    
    Sub CheckWeekend_Attendance()

        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value

       
        Dim TDATE As String
        Dim T_DATE As Date

        TDATE = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
       

        Dim Check_date As Integer

        Check_date = GETVALID_ATTDay(ACD_ID, ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"), TDATE)

        If Check_date > 0 Then
            T_DATE = TDATE
            If UCase(T_DATE.DayOfWeek.ToString) = ViewState("BSU_WEEKEND1").ToUpper.Trim Then
                If ViewState("WEEKEND1").Trim <> "" Then

                    ViewState("flag_weekend") = "0"
                    Call SetSTYPE()
                Else
                    ViewState("flag_weekend") = "1"
                End If
            ElseIf UCase(T_DATE.DayOfWeek.ToString) = ViewState("BSU_WEEKEND2").ToUpper.Trim Then
                If ViewState("WEEKEND2").Trim <> "" Then
                    ViewState("flag_weekend") = "0"

                    Call SetSTYPE()


                Else
                    ViewState("flag_weekend") = "1"
                End If
            ElseIf Not (UCase(T_DATE.DayOfWeek.ToString) = ViewState("BSU_WEEKEND1").ToUpper.Trim Or UCase(T_DATE.DayOfWeek.ToString) = ViewState("BSU_WEEKEND2").ToUpper.Trim) Then
                gvInfo.Visible = True
                btnSave.Visible = True
                btnSave2.Visible = True
                ViewState("flag_weekend") = "0"
                Call SetSTYPE()

            End If

        Else
            lblError.Text = "Date entered is not a working day"
            ViewState("flag_weekend") = "1"

        End If
    End Sub
    Private Function GETVALID_ATTDay(ByVal ACD_ID As String, ByVal SDT As String, ByVal EDT As String, ByVal W1 As String, ByVal W2 As String, ByVal Check_DT As String) As Integer

        Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
        Dim sqlString As String = "declare @temp_grd_id varchar(10),@temp_sct_id int;" & _
" SELECT @temp_grd_id=STU_GRD_ID,@temp_sct_id=STU_SCT_ID FROM STUDENT_M INNER JOIN STUDENT_GROUPS_S ON SSD_STU_ID=STU_ID  " & _
" INNER JOIN GROUPS_M ON SSD_SGR_ID=SGR_ID  LEFT OUTER JOIN TCM_M  ON TCM_STU_ID=STU_ID  AND TCM_CANCELDATE IS NULL  WHERE    STU_ACD_ID=  '" & ACD_ID & "'  AND SGR_ID= '" & SGR_ID & "'   AND STU_CURRSTATUS <> 'CN' AND " & _
" (convert(datetime,TCM_LASTATTDATE) > convert(datetime,'" & Check_DT & "') or convert(datetime,TCM_LASTATTDATE) is null ) " & _
" and  STU_DOJ <= convert(datetime,'" & Check_DT & "'); " & _
" select count(WDATE)  from fn_TWORKINGDAYS_XLOGBOOK( '" & ACD_ID & "'  ,@temp_grd_id,@temp_sct_id,'" & SDT & "','" & EDT & "','" & W1 & "','" & W2 & "') where WDATE='" & Check_DT & "'"

        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)
        End Using
        Return CInt(result)

    End Function
    Sub SetSTYPE()
        If StrConv(ViewState("STYPE").Trim, VbStrConv.ProperCase) = "None" Then
            gvInfo.Visible = False
            btnSave.Visible = False
            btnSave2.Visible = False
            lblError.Text = "Date entered is not a working day"
            ViewState("flag_weekend") = "1"
        ElseIf StrConv(ViewState("STYPE").Trim, VbStrConv.ProperCase) = "All" Then
            gvInfo.Visible = True
            btnSave.Visible = True
            btnSave2.Visible = True
            ViewState("flag_weekend") = "0"
        ElseIf StrConv(ViewState("STYPE").ToString.Trim, VbStrConv.ProperCase) = "Female" Then
            For j As Integer = 0 To gvInfo.Rows.Count - 1
                Dim row As GridViewRow = gvInfo.Rows(j)
                Dim SGENDER As String = DirectCast(row.FindControl("lblSGENDER"), Label).Text
                Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                'ddlStatus.ClearSelection()
                'ddlStatus.SelectedIndex = 0

                'Dim STR1 As String = ddlStatus.SelectedItem.Text   'UCase(Trim(ddlStatus.SelectedItem.Text))
                'ddlStatus.ClearSelection()
                ''STR1 = STR1.Substring(0, STR1.Length - 3)

                If (StrConv(SGENDER.Trim, VbStrConv.ProperCase) = "Male" Or StrConv(SGENDER.Trim, VbStrConv.Uppercase) = "M") Then
                    'All male will be checked by default
                    'DirectCast(row.FindControl("chkPresent"), CheckBox).Checked = True
                    'DirectCast(row.FindControl("chkLate"), CheckBox).Checked = False

                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                    ddlStatus.Enabled = False
                    'DirectCast(row.FindControl("ddlStatus"), DropDownList).SelectedIndex = 0
                    'DirectCast(row.FindControl("ddlStatus"), DropDownList).Enabled = False

                    'DirectCast(row.FindControl("chkPresent"), CheckBox).Enabled = False
                    'DirectCast(row.FindControl("chkLate"), CheckBox).Enabled = False
                    DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
                End If
            Next
            gvInfo.Visible = True
            btnSave.Visible = True
            btnSave2.Visible = True
            ViewState("flag_weekend") = "0"
        ElseIf StrConv(ViewState("STYPE").Trim, VbStrConv.ProperCase) = "Male" Then
            For j As Integer = 0 To gvInfo.Rows.Count - 1
                Dim row As GridViewRow = gvInfo.Rows(j)
                Dim SGENDER As String = DirectCast(row.FindControl("lblSGENDER"), Label).Text
                Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)

                If (StrConv(SGENDER.Trim, VbStrConv.ProperCase) = "Female" Or StrConv(SGENDER.Trim, VbStrConv.Uppercase) = "F") Then
                    'All male will be checked by default
                    'DirectCast(row.FindControl("chkPresent"), CheckBox).Checked = True
                    'DirectCast(row.FindControl("chkLate"), CheckBox).Checked = False


                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                    ddlStatus.Enabled = False
                    'DirectCast(row.FindControl("ddlStatus"), DropDownList).SelectedIndex = 0
                    'DirectCast(row.FindControl("ddlStatus"), DropDownList).Enabled = False

                    'DirectCast(row.FindControl("chkPresent"), CheckBox).Enabled = False
                    'DirectCast(row.FindControl("chkLate"), CheckBox).Enabled = False
                    DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
                End If
            Next
            gvInfo.Visible = True
            btnSave.Visible = True
            btnSave2.Visible = True
            ViewState("flag_weekend") = "0"
        End If
    End Sub
    Sub DisableControls()
        For j As Integer = 0 To gvInfo.Rows.Count - 1
            Dim row As GridViewRow = gvInfo.Rows(j)
            Dim G_Stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
            If Session("Astud_ID_GROUP").Contains(G_Stud_ID) Then

                DirectCast(row.FindControl("ddlStatus"), DropDownList).Enabled = False

                'DirectCast(row.FindControl("chkPresent"), CheckBox).Enabled = False
                'DirectCast(row.FindControl("chkLate"), CheckBox).Enabled = False

                DirectCast(row.FindControl("txtRemarks"), TextBox).Enabled = False
            End If
        Next
    End Sub
    Sub bindWeekEndstatus()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String
            Dim ds As New DataSet

            str_Sql = "SELECT BSU_WEEKEND1, BSU_WEEKEND2" _
                & " FROM BUSINESSUNIT_M" _
                & " WHERE BSU_ID='" & Session("sBSUID") & "'"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("BSU_WEEKEND1") = ds.Tables(0).Rows(0)(0).ToString.ToUpper
                ViewState("BSU_WEEKEND2") = ds.Tables(0).Rows(0)(1).ToString.ToUpper
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim RAL_ID As New DataColumn("RAL_ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STUDNAME As New DataColumn("STUDNAME", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))
            Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim SGENDER As New DataColumn("SGENDER", System.Type.GetType("System.String"))
            Dim APPLEAVE As New DataColumn("APPLEAVE", System.Type.GetType("System.String"))
            Dim MinList As New DataColumn("MinList", System.Type.GetType("System.String"))
            Dim DAY1 As New DataColumn("DAY1", System.Type.GetType("System.String"))
            Dim DAY2 As New DataColumn("DAY2", System.Type.GetType("System.String"))
            Dim DAY3 As New DataColumn("DAY3", System.Type.GetType("System.String"))
            Dim ContAbs As New DataColumn("ContAbs", System.Type.GetType("System.String"))
            Dim TRANSSTATUS As New DataColumn("TRANSSTATUS", System.Type.GetType("System.String"))
            Dim TRANSSTATUS_CODE As New DataColumn("TRANSSTATUS_CODE", System.Type.GetType("System.String"))

            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(RAL_ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NO)
            dtDt.Columns.Add(STUDNAME)
            dtDt.Columns.Add(STATUS)
            dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(SGENDER)
            dtDt.Columns.Add(APPLEAVE)
            dtDt.Columns.Add(MinList)
            dtDt.Columns.Add(DAY1)
            dtDt.Columns.Add(DAY2)
            dtDt.Columns.Add(DAY3)
            dtDt.Columns.Add(ContAbs)
            dtDt.Columns.Add(TRANSSTATUS)
            dtDt.Columns.Add(TRANSSTATUS_CODE)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If ddlAcdYear.SelectedIndex = -1 Then
            lblError.Text = "Academic Year not selected"

        ElseIf ddlSubjectGroup.SelectedIndex = -1 Then
            lblError.Text = "Subject Group not selected"
        Else

            If ValidateDate() = "0" Then
                Call Edit_clicked()
                Call backGround()
            End If
        End If
    End Sub
    Sub Edit_clicked()
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        'Dim totRow As Integer
        Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
        Dim GRD_ID As String = ddlGrade.SelectedValue
        Dim PERIOD_ID As String = ddlPERIOD.SelectedValue
        Dim EMP_ID As String = ViewState("EMP_ID")


        Dim AttDate As String

        AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)




        Try
            Using readerALS_ID As SqlDataReader = AccessStudentClass.GetRAL_ID(ACD_ID, SGR_ID, PERIOD_ID, AttDate)
                If readerALS_ID.HasRows Then
                    While readerALS_ID.Read

                        ViewState("RAL_ID") = Convert.ToString(readerALS_ID("RAL_ID"))

                    End While
                Else
                    ViewState("RAL_ID") = ""
                End If

            End Using


            If ViewState("RAL_ID") <> "" Then



                'Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                'Dim str_Sql As String

                'str_Sql = "select count(AVSG_STU_ID) from  ATT.ATTENDANCE_VSTUDENT_GROUP where AVSG_AVG_ID='" & AVG_ID & "' and AVSG_bREMOVE=0 "
                'totRow = CInt(SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_Sql))
                'If totRow > 0 Then
                ViewState("datamode") = "edit"



                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                Call BindEdit_Attendance()
                setControl()
                'Else

                '    lblError.Text = "Record currently not updated. Please Contact System Admin"
                'End If
            Else

            lblError.Text = "Attendance not marked for the given date & group!!!"
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Sub BindEdit_AttendanceAPPROVED()
        Try


            Dim RAS_STU_ID As String = String.Empty
            Dim RAS_PAR_ID As String = String.Empty
            Dim RAS_REMARKS As String = String.Empty

            Dim ACD_ID As String = ddlAcdYear.SelectedValue
            Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
            Dim GRD_ID As String = ddlGrade.SelectedValue
            Dim EMP_ID As String = ViewState("EMP_ID")
            Dim PERIOD_ID As String = ddlPERIOD.SelectedValue
            Dim lastAttDate As String

            lastAttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)

            Using readerGetALG_StudentID As SqlDataReader = AccessStudentClass.GetRAL_StudentID_ROOM(ViewState("RAL_ID"), ACD_ID, PERIOD_ID, SGR_ID, lastAttDate)
                While readerGetALG_StudentID.Read
                    RAS_STU_ID = Convert.ToString(readerGetALG_StudentID("RAS_STU_ID"))
                    RAS_PAR_ID = Convert.ToString(readerGetALG_StudentID("RAS_PAR_ID"))
                    RAS_REMARKS = Convert.ToString(readerGetALG_StudentID("RAS_REMARKS"))
                    For j As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(j)
                        Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                        If Trim(RAS_STU_ID) = Trim(Temp_stud_ID) Then
                            ddlStatus.ClearSelection()
                            DirectCast(row.FindControl("txtRemarks"), TextBox).Text = RAS_REMARKS
                            ddlStatus.Items.FindByValue(RAS_PAR_ID).Selected = True
                        End If
                    Next
                End While
            End Using

            Dim Astud_ID As New ArrayList
            Using readerSTUD_Leave_Approval As SqlDataReader = AccessStudentClass.GetSTUD_Leave_Approval_ROOM(ACD_ID, SGR_ID, GRD_ID, lastAttDate)
                While readerSTUD_Leave_Approval.Read
                    Dim Stud_ID As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_STU_ID"))
                    Dim Remarks As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_REMARKS"))
                    Dim AppLeave As String = Convert.ToString(readerSTUD_Leave_Approval("SLA_APPRLEAVE"))
                    Dim SLA_APD_ID As Integer = Convert.ToInt64(readerSTUD_Leave_Approval("SLA_APD_ID"))
                    'Astud_ID.Add(Stud_ID)

                    For j As Integer = 0 To gvInfo.Rows.Count - 1
                        Dim row As GridViewRow = gvInfo.Rows(j)

                        Dim Temp_stud_ID As String = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                        Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)

                        If Trim(Stud_ID) = Trim(Temp_stud_ID) Then
                            ddlStatus.ClearSelection()
                            DirectCast(row.FindControl("txtRemarks"), TextBox).Text = Remarks
                            DirectCast(row.FindControl("lblAppLeave"), Label).Text = AppLeave
                            If AppLeave = "APPROVED" Then
                                Astud_ID.Add(Stud_ID)
                                If ViewState("bAPP_LEAVE_ABS") = "True" Then
                                    If SLA_APD_ID <> 0 Then
                                        If Not ddlStatus.Items.FindByValue(SLA_APD_ID) Is Nothing Then
                                            ddlStatus.Items.FindByValue(SLA_APD_ID).Selected = True
                                        Else
                                            ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                                        End If
                                    Else
                                        ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                                    End If
                                Else
                                    If SLA_APD_ID <> 0 Then
                                        If Not ddlStatus.Items.FindByValue(SLA_APD_ID) Is Nothing Then
                                            ddlStatus.Items.FindByValue(SLA_APD_ID).Selected = True
                                        Else
                                            ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                                        End If
                                    Else
                                        ddlStatus.Items.FindByText(ddlStatus.Items(0).Text).Selected = True
                                    End If
                                End If
                            ElseIf AppLeave = "NOT APPROVED" Then
                                ddlStatus.Items.FindByText(ddlStatus.Items(1).Text).Selected = True
                            End If
                        End If
                    Next
                End While
            End Using
            ' gvInfo.DataSource =Session("dt_ATT_GROUP")
            ' gvInfo.DataBind()

            Session("Astud_ID_GROUP") = Astud_ID

        Catch ex As Exception

        End Try
    End Sub
    Sub BindEdit_Attendance()
        Try
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim SGR_ID As String = ddlSubjectGroup.SelectedValue
            Dim GRD_ID As String = ddlGrade.SelectedValue
            Dim PERIOD_ID As String = ddlPERIOD.SelectedValue
            Dim EMP_ID As String = ViewState("EMP_ID")
            Dim AttDate As String

            AttDate = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)

            Session("dt_ATT_GROUP").Rows.Clear()
            ViewState("idTr") = 1
            Using readerSTUD_ATT_ADD As SqlDataReader = AccessStudentClass.GETSTUDATT_REGIST_ROOMGROUP(ACD_ID, GRD_ID, SGR_ID, AttDate, PERIOD_ID, ViewState("ACD_STARTDT"), ViewState("ACD_ENDDT"), ViewState("BSU_WEEKEND1"), ViewState("BSU_WEEKEND2"))
                While readerSTUD_ATT_ADD.Read
                    Dim rDt As DataRow
                    Dim cABS1 As Boolean
                    Dim cABS2 As Boolean
                    Dim cABS3 As Boolean
                    rDt = Session("dt_ATT_GROUP").NewRow
                    rDt("SRNO") = ViewState("idTr")
                    rDt("RAL_ID") = "0"
                    rDt("STU_ID") = Convert.ToString(readerSTUD_ATT_ADD("STU_ID"))
                    rDt("STU_NO") = Convert.ToString(readerSTUD_ATT_ADD("STU_NO"))
                    rDt("STUDNAME") = Convert.ToString(readerSTUD_ATT_ADD("STUDNAME"))
                    rDt("STATUS") = ""
                    rDt("REMARKS") = ""
                    rDt("SGENDER") = Convert.ToString(readerSTUD_ATT_ADD("SGENDER"))
                    rDt("APPLEAVE") = ""
                    rDt("minList") = Convert.ToString(readerSTUD_ATT_ADD("MINLIST"))
                    rDt("TRANSSTATUS") = Convert.ToString(readerSTUD_ATT_ADD("cssclass"))
                    rDt("TRANSSTATUS_CODE") = Convert.ToString(readerSTUD_ATT_ADD("SCAN_STAT_CODE"))
                    If InStr(Convert.ToString(readerSTUD_ATT_ADD(13)), "|") > 0 Then
                        If Right(Convert.ToString(readerSTUD_ATT_ADD(13)), InStr(Convert.ToString(readerSTUD_ATT_ADD(13)), "|") + 1) = "2" Then
                            cABS1 = True
                        Else
                            cABS1 = False
                        End If

                        rDt("DAY1") = Left(Convert.ToString(readerSTUD_ATT_ADD(13)), InStr(Convert.ToString(readerSTUD_ATT_ADD(13)), "|") - 1)
                    Else
                        rDt("DAY1") = Convert.ToString(readerSTUD_ATT_ADD(13))
                    End If


                    If InStr(Convert.ToString(readerSTUD_ATT_ADD(14)), "|") > 0 Then

                        If Right(Convert.ToString(readerSTUD_ATT_ADD(14)), InStr(Convert.ToString(readerSTUD_ATT_ADD(14)), "|") + 1) = "2" Then
                            cABS2 = True
                        Else
                            cABS2 = False
                        End If

                        rDt("DAY2") = Left(Convert.ToString(readerSTUD_ATT_ADD(14)), InStr(Convert.ToString(readerSTUD_ATT_ADD(14)), "|") - 1)
                    Else
                        rDt("DAY2") = Convert.ToString(readerSTUD_ATT_ADD(14))
                    End If


                    If InStr(Convert.ToString(readerSTUD_ATT_ADD(15)), "|") > 0 Then
                        If Right(Convert.ToString(readerSTUD_ATT_ADD(15)), InStr(Convert.ToString(readerSTUD_ATT_ADD(15)), "|") + 1) = "2" Then
                            cABS3 = True
                        Else
                            cABS3 = False
                        End If

                        rDt("DAY3") = Left(Convert.ToString(readerSTUD_ATT_ADD(15)), InStr(Convert.ToString(readerSTUD_ATT_ADD(15)), "|") - 1)

                    Else
                        rDt("DAY3") = Convert.ToString(readerSTUD_ATT_ADD(15))
                    End If
                    If cABS1 = True And cABS2 = True And cABS3 = True Then
                        rDt("ContAbs") = "1"
                    Else
                        rDt("ContAbs") = "0"
                    End If


                    hfDay1.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(13)).Replace("|", " ")
                    hfDay2.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(14)).Replace("|", " ")
                    hfDay3.Value = Convert.ToString(readerSTUD_ATT_ADD.GetName(15)).Replace("|", " ")
                    Session("dt_ATT_GROUP").Rows.Add(rDt)
                    ViewState("idTr") = ViewState("idTr") + 1
                End While
            End Using


            Using readerSTUD_GRADE_SECTION_HOLIDAY As SqlDataReader = AccessStudentClass.GetSTUD_ROOM_HOLIDAY_GROUP(ACD_ID, SGR_ID, AttDate)

                If readerSTUD_GRADE_SECTION_HOLIDAY.HasRows Then
                    While readerSTUD_GRADE_SECTION_HOLIDAY.Read
                        ViewState("STYPE") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("STYPE"))
                        ViewState("WEEKEND1") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND1"))
                        ViewState("bWEEKEND1") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND1"))
                        ViewState("WEEKEND2") = Convert.ToString(readerSTUD_GRADE_SECTION_HOLIDAY("WEEKEND2"))
                        ViewState("bWEEKEND2") = Convert.ToBoolean(readerSTUD_GRADE_SECTION_HOLIDAY("bWEEKEND2"))
                    End While

                Else
                    ViewState("STYPE") = "Empty"
                    ViewState("WEEKEND1") = ""
                    ViewState("bWEEKEND1") = False
                    ViewState("WEEKEND2") = ""
                    ViewState("bWEEKEND2") = False

                End If
            End Using

            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSave2.Click
        System.Threading.Thread.Sleep(20)

        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty


        str_err = calltransaction(errorMessage)
        If str_err = "0" Then

            lblError.Text = "Record Saved Successfully"
            btnSave2.Visible = False
        Else
            lblError.Text = errorMessage
            Call backGround()
        End If

    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer

        Dim RAL_ID As String = String.Empty
        Dim RAL_CREATEDDT As String = String.Empty
        Dim RAL_CREATEDTM As String = String.Empty
        Dim RAL_MODIFIEDDT As String = String.Empty
        Dim RAL_MODIFIEDTM As String = String.Empty
        Dim STR_XML As String = String.Empty
        Dim RAL_ATTDT As String
        Dim EMP_ID As String = ViewState("EMP_ID")
        Dim RAL_BSU_ID As String = Session("sBsuid")
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim ALS_STU_ID As String = String.Empty
        Dim ALS_APD_ID As String = String.Empty
        Dim ALS_REMARKS As String = String.Empty
        Dim AppLeave As String = String.Empty
        Dim RAL_SGR_ID As String = ddlSubjectGroup.SelectedValue
        Dim RAL_PERIOD_ID As String = ddlPERIOD.SelectedValue

        RAL_ATTDT = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
      

        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer
                For i As Integer = 0 To gvInfo.Rows.Count - 1
                    Dim row As GridViewRow = gvInfo.Rows(i)

                    Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                    ALS_STU_ID = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                    ALS_REMARKS = DirectCast(row.FindControl("txtRemarks"), TextBox).Text
                    AppLeave = DirectCast(row.FindControl("lblAppLeave"), Label).Text
                    If (ddlStatus.SelectedIndex <> 0) Then
                        If (AppLeave <> "APPROVED") Then
                            ALS_APD_ID = ddlStatus.SelectedItem.Value
                            STR_XML += String.Format("<STUDENT STUID='{0}' ALS_APD_ID='{1}' ALS_REMARKS='{2}'> </STUDENT>", ALS_STU_ID, ALS_APD_ID, ALS_REMARKS)
                        End If
                    End If
                Next


                If STR_XML <> "" Then
                    STR_XML = "<STUDENTS>" + STR_XML + "</STUDENTS>"
                Else
                    STR_XML = ""
                End If




                If ViewState("datamode") = "add" Then
                    RAL_CREATEDDT = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    RAL_CREATEDTM = String.Format("{0:T}", DateTime.Now)

                    status = AccessStudentClass.SAVEATT_LOG_ROOM(STR_XML, ACD_ID, RAL_ATTDT, _
                                                  RAL_SGR_ID, RAL_ID, EMP_ID, RAL_BSU_ID, RAL_CREATEDDT, _
                                                    RAL_CREATEDTM, RAL_MODIFIEDDT, RAL_MODIFIEDTM, RAL_PERIOD_ID, _
                                                     ViewState("datamode"), transaction)


                ElseIf ViewState("datamode") = "edit" Then
                    RAL_MODIFIEDDT = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    RAL_MODIFIEDTM = String.Format("{0:T}", DateTime.Now)


                    status = AccessStudentClass.SAVEATT_LOG_ROOM(STR_XML, ACD_ID, RAL_ATTDT, _
                                                  RAL_SGR_ID, ViewState("RAL_ID"), EMP_ID, RAL_BSU_ID, RAL_CREATEDDT, _
                                                    RAL_CREATEDTM, RAL_MODIFIEDDT, RAL_MODIFIEDTM, RAL_PERIOD_ID, _
                                                     ViewState("datamode"), transaction)
                    

                End If



                ViewState("datamode") = "view"

                Session("dt_ATT_GROUP").Rows.Clear()

                gvInfo.DataSource = Session("dt_ATT_GROUP")
                gvInfo.DataBind()

                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                btnCancel.Visible = False
                btnCancel2.Visible = False
                btnSave2.Visible = False
                calltransaction = "0"
                ResetControl()


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnCancel2.Click
        Try

            If ViewState("datamode") = "add" Or ViewState("datamode") = "edit" Then
                'clear the textbox and set the default settings
                ViewState("datamode") = "view"
                Session("dt_ATT_GROUP").Rows.Clear()
                gvInfo.DataSource = Session("dt_ATT_GROUP")
                gvInfo.DataBind()

                ' gvInfo.Visible = False
                Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                btnSave2.Visible = False
                btnCancel.Visible = False
                btnCancel2.Visible = False
                ResetControl()
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Sub setControl()
        ddlAcdYear.Enabled = False
        ddlSubjectGroup.Enabled = False
        ddlGrade.Enabled = False
        ddlSubject.Enabled = False
        ddlPERIOD.Enabled = False
        imgCalendar.Visible = False
        txtDate.Enabled = False
    End Sub
    Sub ResetControl()
        ddlAcdYear.Enabled = True
        ddlSubjectGroup.Enabled = True
        ddlGrade.Enabled = True
        ddlSubject.Enabled = True
        ddlPERIOD.Enabled = True
        imgCalendar.Visible = True
        txtDate.Enabled = True

      

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Try


        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = True

        'smScriptManager.RegisterPostBackControl(gvInfo)
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub GETROOM_ATT_PARAM()
        Dim CONN As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As New DataSet
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim BSU_ID As String = Session("sBsuid")
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@ACD_ID", ACD_ID)
        PARAM(1) = New SqlParameter("@BSU_ID", BSU_ID)
        ds1 = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "ATT.ROOM_PARAMS", PARAM)
            Session("rOOM_ATT_SES") = ds1

    End Sub
    Protected Sub gvInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvInfo.RowDataBound
        Try


            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim BSU_ID As String = Session("sBsuid")


            Dim DS As DataSet = Session("rOOM_ATT_SES") 'AccessStudentClass.GetATTENDANCE_ROOM_PARAM_D(ACD_ID, BSU_ID)
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ddlSTATUS As DropDownList = DirectCast(e.Row.FindControl("ddlStatus"), DropDownList)
                Dim lblMinList As Label = DirectCast(e.Row.FindControl("lblMinList"), Label)
                Dim lblContAbs As Label = DirectCast(e.Row.FindControl("lblContAbs"), Label)
                Dim ltStar As Literal = DirectCast(e.Row.FindControl("ltstar"), Literal)
                If lblMinList.Text.ToUpper <> "REGULAR" Then
                    e.Row.BackColor = Drawing.Color.FromArgb(251, 204, 119)
                End If
                If lblContAbs.Text = "1" Then
                    ltStar.Text = "<font color='red' size='3px'>*</font>"
                Else
                    ltStar.Visible = False
                End If

                ddlSTATUS.Items.Clear()
                If DS.Tables(0).Rows.Count > 0 Then
                    ddlSTATUS.DataSource = DS.Tables(0)
                    ddlSTATUS.DataTextField = "APD_PARAM_DESCR"
                    ddlSTATUS.DataValueField = "APD_ID"
                   
                    ddlSTATUS.DataBind()

                    Dim lst As IEnumerator
                    Dim currentItem As ListItem

                    lst = ddlSTATUS.Items.GetEnumerator()
                    While (lst.MoveNext())
                        currentItem = CType(lst.Current, ListItem)
                        If Left(currentItem.Text, 7) = "PRESENT" Then
                            ddlSTATUS.ClearSelection()
                            ddlSTATUS.Items.FindByValue(currentItem.Value).Selected = True
                            Exit While
                        End If
                    End While

                    Call backGround()
                End If
            ElseIf e.Row.RowType = DataControlRowType.Header Then
                'Dim HeaderGrid As GridView = DirectCast(sender, GridView)
                'Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                'Dim HeaderCell As New TableCell()
                'HeaderCell.Text = ""
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)
                'HeaderCell = New TableCell()
                'HeaderCell.ForeColor = Drawing.Color.Black
                'HeaderCell.Height = 20
                'HeaderCell.Text = "Last Five Day's Attendance History"
                'HeaderCell.ColumnSpan = 5
                'HeaderGridRow.Cells.Add(HeaderCell)


                'gvInfo.Controls(0).Controls.AddAt(0, HeaderGridRow)

                e.Row.Cells(11).Text = hfDay1.Value
                e.Row.Cells(12).Text = hfDay2.Value
                e.Row.Cells(13).Text = hfDay3.Value
                'e.Row.Cells(15).Text = hfDay4.Value
                'e.Row.Cells(16).Text = hfDay5.Value
            End If

        Catch ex As Exception

        End Try


    End Sub
    Sub backGround()
        For i As Integer = 0 To gvInfo.Rows.Count - 1
            Dim row As GridViewRow = gvInfo.Rows(i)
            Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)

            ddlStatus.Items(0).Attributes.Add("style", "background-color:#fdf4a8") 'GREEN
            ddlStatus.Items(1).Attributes.Add("style", "background-color:#feb4a8") 'PINK
            ' ddlStatus.Font.Name = "Verdana"
            '  ddlStatus.Font.Size = "8"


        Next

    End Sub
    Protected Sub gvInfo_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then

            Dim HeaderGrid As GridView = DirectCast(sender, GridView)
            Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderCell As New TableCell()
            HeaderCell.Text = ""
            HeaderCell.ColumnSpan = 5
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
            HeaderCell.ForeColor = Drawing.Color.Black
            HeaderCell.Height = 20
            HeaderCell.Text = "Last Three Period's Attendance History"
            HeaderCell.ColumnSpan = 4
            HeaderGridRow.Cells.Add(HeaderCell)

            gvInfo.Controls(0).Controls.AddAt(0, HeaderGridRow)
        End If

    End Sub
    Sub check_transport_status_bsu()

        Dim constring As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Using con As New SqlConnection(constring)

            Using cmd As New SqlCommand("att.get_trStatus_acess_bsu", con)

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("@bsu_id", Session("sBsuid"))

                cmd.Parameters.Add("@valid", SqlDbType.Int)

                cmd.Parameters("@valid").Direction = ParameterDirection.Output

                con.Open()

                cmd.ExecuteNonQuery()

                con.Close()

                ViewState("IsvalidBsu") = cmd.Parameters("@valid").Value

            End Using

        End Using
    End Sub
End Class
