<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"    CodeFile="studAuto_Att_edit.aspx.vb" Inherits="Students_studSetHoliday" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
  
    <script language="javascript" type="text/javascript">
            
      function checkDate(sender,args)
{

  var currenttime =document.getElementById("<%=hfDate.ClientID %>").value;
   
if (sender._selectedDate <new Date(currenttime)) 
                  {
                        alert("You cannot select a day less than today!");
                        sender._selectedDate = new Date(currenttime); 
                        // set the date back to the current date
sender._textbox.set_Value(sender._selectedDate.format(sender._format))
                  }
}
   
 
    
    
   function ShowHDetails(url)
  
    {  
             var sFeatures;
            sFeatures="dialogWidth: 370px; ";
            sFeatures+="dialogHeight: 300px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
          
            var result;
           
             
            result = window.showModalDialog(url,"", sFeatures)
            if (result=='' || result==undefined)
            {
            return false;
            }
                
           return false;
    }
function onCancel()
      {
  // var postBack = new Sys.WebForms.PostBackAction(); 
//postBack.set_target('CancelButton'); 
//postBack.set_eventArgument(''); 
// postBack.performAction();
      }
    
    function onOk()
      {
   var postBack = new Sys.WebForms.PostBackAction(); 
postBack.set_target('okButton'); 
postBack.set_eventArgument(''); 
 postBack.performAction();
      }
    
  
    
    function client_OnTreeNodeChecked( )
        { 
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
            var treeNode = obj;
            checkedState = treeNode.checked;
            do {
                obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
            var parentTreeLevel = obj.rows[0].cells.length;
            var parentTreeNode = obj.rows[0].cells[0];
            var tables = obj.parentElement.getElementsByTagName("TABLE");
            var numTables = tables.length
            if (numTables >= 1)
            {
                for (i=0; i < numTables; i++)
                {
                    if (tables[i] == obj)
                    {
                        treeNodeFound = true;
                        i++;
                        if (i == numTables)
                            {
                            return;
                            }
                    }
                    if (treeNodeFound == true)
                    {
                    var childTreeLevel = tables[i].rows[0].cells.length;
                    if (childTreeLevel > parentTreeLevel)
                        {
                        var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                        var inputs = cell.getElementsByTagName("INPUT");
                        inputs[0].checked = checkedState;
                        }
                    else
                        {
                        return;
                        }
                    }
                }
            }
        }
        }
    
    
    function getDate(mode) 
   {     
            var sFeatures;
            sFeatures="dialogWidth: 229px; ";
            sFeatures+="dialogHeight: 234px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            if (mode==1)
                result = window.showModalDialog("../accounts/calendar.aspx?dt="+document.getElementById('<%=txtFrom.ClientID %>').value,"", sFeatures) 
            else if (mode==2)
                result = window.showModalDialog("../accounts/calendar.aspx?dt="+document.getElementById('<%=txtTo.ClientID %>').value,"", sFeatures)

            if (result=='' || result==undefined)
            {
//            document.getElementById("txtDate").value=''; 
            return false;
            }
           if (mode==1)
              document.getElementById('<%=txtFrom.ClientID %>').value=result; 
             else if (mode==2)
              document.getElementById('<%=txtTo.ClientID %>').value=result;
           return true;
    } 
    </script>
        <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Set Auto Attendance "></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">


    <table id="tbl_AddGroup" runat="server" width="100%">
        <tr>
            <td align="left" style="width: 778px;">
                            <span  style="float:left; display: block; "> <div align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"  /></div>
                                <div align="left"><asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                  
                    ValidationGroup="AttGroup" DisplayMode="List"   ForeColor="" /></div></span>
             </td>
        </tr>
        <tr>
            <td class="matters"  >
                <table width="100%">

                    <tr>
                        <td align="left" class="matters" width="20%">
                          <span class="field-label">  Academic Year</span></td>
                        <td align="left" class="matters" width="30%">
                            <asp:DropDownList ID="ddlAcdYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        <td align="left" class="matters" width="20%">
                          <span class="field-label">Description <font color="red"  >*</font><span class="field-label"></td>
                        <td align="left" class="matters" width="20%">
                            <asp:TextBox ID="txtDescr" runat="server" MaxLength="300"  ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="txtDescr" CssClass="error" Display="Dynamic" 
                                EnableViewState="False" ErrorMessage="Description required" ForeColor="" 
                                ValidationGroup="AttGroup">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" ><span class="field-label">
                            From Date <font color="red"  >*</font></span></td>
                        <td align="left" class="matters"  >
                            <asp:TextBox ID="txtFrom" runat="server"  ></asp:TextBox>
                            <asp:ImageButton ID="imgCal1" runat="server" 
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator id="rfvFrom" runat="server" ControlToValidate="txtFrom"
                                Display="Dynamic" ErrorMessage="From Date required" ValidationGroup="AttGroup"   ForeColor="Red">*</asp:RequiredFieldValidator></td>
                        <td align="left" class="matters"><span class="field-label">
                            To Date <font color="red"  >*</font></span></td>
                        <td align="left" class="matters">
                            <asp:TextBox ID="txtTo" runat="server"  ></asp:TextBox>
                            <asp:ImageButton ID="imgCal2" runat="server" 
                                ImageUrl="~/Images/calendar.gif" />
                            <asp:RequiredFieldValidator id="rfvTo" runat="server" ControlToValidate="txtTo" Display="Dynamic"
                                ErrorMessage="To Date required" ValidationGroup="AttGroup"  ForeColor="">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" ><span class="field-label">
                            Automate Schedule <font color="red"  >*</font></span></td>
                        <td align="left" class="matters"  >
                            <asp:DropDownList ID="ddlTime" runat="server">
                                <asp:ListItem Value="6" Selected="True">6 AM</asp:ListItem>
                                <asp:ListItem Value="7">7 AM</asp:ListItem>
                                <asp:ListItem Value="8">8 AM</asp:ListItem>
                                <asp:ListItem Value="4">4 AM</asp:ListItem>
                                <asp:ListItem Value="5">5 AM</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" class="matters"><span class="field-label">
                            Att Param <font color="red"  >*</font></span></td>
                        <td align="left" class="matters">
                            <asp:DropDownList ID="ddlAttParam" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" ><span class="field-label">
                            Select Grade &amp; Section <font color="red"  >*</font></span></td>
                        <td align="left" class="matters"  >
                             <div class="checkbox-list"
                                  <asp:TreeView id="tvGrade" runat="server"
                    ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked();" ExpandDepth="1"><databindings>
<asp:TreeNodeBinding DataMember="GradeItem" SelectAction="SelectExpand" TextField="Text" ValueField="ValueField">
</asp:TreeNodeBinding>
</databindings>
                </asp:TreeView>
                                 </div>

                  </td>
                        <td align="left" class="matters"   ><span class="field-label">
                            Activate</span></td>
                        
                        <td align="left" class="matters"    >
                            <asp:CheckBox ID="chkActive" runat="server" />
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="matters"   valign="bottom">
                <br />
            </td>
        </tr>
        <tr>
            <td class="matters " valign="bottom" align="center">
                <asp:Button id="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    Text="Add" OnClick="btnAdd_Click" /><asp:Button id="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    Text="Edit" OnClick="btnEdit_Click" /><asp:Button id="btnSave" runat="server" CssClass="button" Text="Save"
                        ValidationGroup="AttGroup" /><asp:Button id="btnCancel" runat="server" CausesValidation="False"
                            CssClass="button" Text="Cancel" OnClick="btnCancel_Click" />&nbsp;
            </td>
        </tr>
    </table>
 <asp:HiddenField id="hfDate" runat="server">
    </asp:HiddenField>
    <ajaxToolkit:CalendarExtender ID="cal1" runat="server" TargetControlID="txtfrom" Format="dd/MMM/yyyy"  PopupButtonID="imgcal1" OnClientDateSelectionChanged="checkDate">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="Cal2" runat="server" TargetControlID="txtTo" Format="dd/MMM/yyyy"  PopupButtonID="imgcal2">
    </ajaxToolkit:CalendarExtender>
   </div>
            </div>
            </div>
   
</asp:Content>

