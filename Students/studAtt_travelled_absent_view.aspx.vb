﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Students_studAtt_travelled_absent_view
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then
            Try

                Dim menu_rights As String

                If isPageExpired() Then
                    Response.Redirect("expired.htm")
                Else
                    Session("TimeStamp") = Now.ToString
                    ViewState("TimeStamp") = Now.ToString
                End If

                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


                Dim CurUsr_id As String = Session("sUsr_id")

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                If Not Request.UrlReferrer Is Nothing Then

                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'hardcode the menu code
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S050117") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else



                    menu_rights = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"


                    Call gridbind()


                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), menu_rights, ViewState("datamode"))
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

            End Try
        End If
        set_Menu_Img()
    End Sub
    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))




    End Sub

    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvAutoATT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAutoATT.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvAutoATT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAutoATT.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvAutoATT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvAutoATT.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID

            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function



    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""

            Dim str_filter_BSU_NAME As String = String.Empty
            Dim str_filter_FROMDT As String = String.Empty
            Dim str_filter_TODT As String = String.Empty
            Dim ds As New DataSet

            str_Sql = " SELECT MTA_BSU_ID,MTA_ID,FROMDT,TODT,BSU_NAME FROM(SELECT P.MTA_BSU_ID, P.MTA_ID, replace(convert(varchar(12),P.MTA_FROMDT,106),' ','/') as FROMDT,replace(convert(varchar(12),P.MTA_TODT,106),' ','/') as TODT, B.BSU_NAME FROM ATT.MAIL_TRAVEL_ABSENT_ATT AS P " & _
" INNER JOIN BUSINESSUNIT_M AS B ON P.MTA_BSU_ID = B.BSU_ID ) A WHERE MTA_ID <>''"


            Dim lblID As New Label

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_BSU_NAME As String = String.Empty

            Dim str_FROMDT As String = String.Empty
            Dim str_TODT As String = String.Empty

            If gvAutoATT.Rows.Count > 0 Then

                Dim str_Sid_search() As String

                str_Sid_search = h_Selected_menu_1.Value.Split("__")
                str_search = str_Sid_search(0)

                txtSearch = gvAutoATT.HeaderRow.FindControl("txtBSU_NAME")
                str_BSU_NAME = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_BSU_NAME = " AND a.BSU_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_BSU_NAME = "  AND  NOT a.BSU_NAME LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_BSU_NAME = " AND a.BSU_NAME  LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_BSU_NAME = " AND a.BSU_NAME NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_BSU_NAME = " AND a.BSU_NAME LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_BSU_NAME = " AND a.BSU_NAME NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_2.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAutoATT.HeaderRow.FindControl("txtFROMDT")
                str_FROMDT = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_FROMDT = " AND  FROMDT  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_FROMDT = "  AND  NOT  FROMDT  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_FROMDT = " AND  FROMDT   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_FROMDT = " AND  FROMDT  NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_FROMDT = " AND  FROMDT  LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_FROMDT = " AND  FROMDT  NOT LIKE '%" & txtSearch.Text & "'"
                End If


                str_Sid_search = h_Selected_menu_3.Value.Split("__")
                str_search = str_Sid_search(0)
                txtSearch = gvAutoATT.HeaderRow.FindControl("txtTODT")
                str_TODT = txtSearch.Text

                If str_search = "LI" Then
                    str_filter_TODT = " AND TODT  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "NLI" Then
                    str_filter_TODT = "  AND  NOT  TODT  LIKE '%" & txtSearch.Text & "%'"
                ElseIf str_search = "SW" Then
                    str_filter_TODT = " AND  TODT   LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "NSW" Then
                    str_filter_TODT = " AND  TODT NOT LIKE '" & txtSearch.Text & "%'"
                ElseIf str_search = "EW" Then
                    str_filter_TODT = " AND  TODT  LIKE  '%" & txtSearch.Text & "'"
                ElseIf str_search = "NEW" Then
                    str_filter_TODT = " AND  TODT  NOT LIKE '%" & txtSearch.Text & "'"
                End If




            End If

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_BSU_NAME & str_filter_FROMDT & str_filter_TODT & " ORDER BY BSU_NAME")

            If ds.Tables(0).Rows.Count > 0 Then

                gvAutoATT.DataSource = ds.Tables(0)
                gvAutoATT.DataBind()

            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(4) = True

                gvAutoATT.DataSource = ds.Tables(0)
                Try
                    gvAutoATT.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvAutoATT.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvAutoATT.Rows(0).Cells.Clear()
                gvAutoATT.Rows(0).Cells.Add(New TableCell)
                gvAutoATT.Rows(0).Cells(0).ColumnSpan = columnCount
                gvAutoATT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvAutoATT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If

            'txtSearch = gvOnlineEnq.HeaderRow.FindControl("txtACY_DESCR")
            'txtSearch.Text = str_ACY_DESCR

            txtSearch = gvAutoATT.HeaderRow.FindControl("txtBSU_NAME")
            txtSearch.Text = str_BSU_NAME
            txtSearch = gvAutoATT.HeaderRow.FindControl("txtFROMDT")
            txtSearch.Text = str_FROMDT
            txtSearch = gvAutoATT.HeaderRow.FindControl("txtTODT")
            txtSearch.Text = str_TODT

            '
            set_Menu_Img()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub btnSearchFromDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub

    Protected Sub btnSearchToDT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
    Protected Sub gvOnlineEnq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAutoATT.PageIndexChanging
        gvAutoATT.PageIndex = e.NewPageIndex
        gridbind()
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")

        smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub lbAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim url As String
            'define the datamode to Add if Add New is clicked
            ViewState("datamode") = "add"

            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))

            url = String.Format("~\Students\studAtt_travelled_absent_edit.aspx?MainMnu_code={0}&datamode={1}", ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub
    Protected Sub lblView_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lblMPA_BSU_ID As New Label
            Dim url As String
            Dim viewid As String
            lblMPA_BSU_ID = TryCast(sender.FindControl("lblMPA_BSU_ID"), Label)
            viewid = lblMPA_BSU_ID.Text
            'define the datamode to view if view is clicked
            ViewState("datamode") = "view"
            'Encrypt the data that needs to be send through Query String
            ViewState("MainMnu_code") = Request.QueryString("MainMnu_code")
            viewid = Encr_decrData.Encrypt(viewid)
            ViewState("datamode") = Encr_decrData.Encrypt(ViewState("datamode"))
            url = String.Format("~\Students\studAtt_travelled_absent_edit.aspx?MainMnu_code={0}&datamode={1}&viewid={2}", ViewState("MainMnu_code"), ViewState("datamode"), viewid)
            Response.Redirect(url)
        Catch ex As Exception
            lblError.Text = "Request could not be processed "
        End Try
    End Sub





    Protected Sub btnSearchBSU_NAME_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        gridbind()
    End Sub
End Class
