<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTCCancel_View.aspx.vb" Inherits="Students_studTCCancel_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>Cancel TC             
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table id="tbl_ShowScreen" runat="server" align="center" width="100%" cellpadding="0"
                    cellspacing="0">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                            <table align="center" width="100%">

                                <tr>
                                    <td align="left" width="25%"><span class="field-label">Select Academic Year</span> </td>

                                    <td align="left" width="25%">
                                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:GridView ID="gvStudTCCancel" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                PageSize="20" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStuId" runat="server" Text='<%# Bind("Stu_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="HideID" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbltcmId" runat="server" Text='<%# Bind("TCM_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Student No">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblStu_NoH" runat="server">Student No</asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtStuNo" runat="server" Width="75%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchStuNo" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuNo_Click" />

                                                        </HeaderTemplate>
                                                        <ItemStyle />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStu_No" runat="server" Text='<%# Bind("Stu_No") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Student Name" SortExpression="DESCR">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblStu_NameH" runat="server">Student Name</asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtStuName" runat="server" Width="75%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchStuName" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchStuName_Click" />

                                                        </HeaderTemplate>
                                                        <ItemStyle />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("Stu_Name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Grade">
                                                        <HeaderTemplate>
                                                            Grade
                                                            <br />
                                                            <asp:DropDownList ID="ddlgvGrade" runat="server" AutoPostBack="True"
                                                                OnSelectedIndexChanged="ddlgvGrade_SelectedIndexChanged" Width="75%">
                                                            </asp:DropDownList>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Section">
                                                        <HeaderTemplate>
                                                            Section
                                                            <br />
                                                            <asp:DropDownList ID="ddlgvSection" runat="server" AutoPostBack="True"
                                                                OnSelectedIndexChanged="ddlgvSection_SelectedIndexChanged" Width="100%">
                                                            </asp:DropDownList>

                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("sct_descr") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>




                                                    <asp:TemplateField HeaderText="Document No.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("TCM_DOCNO") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblDocH" runat="server">Document No</asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtDocNo" runat="server" Width="75%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnSearchDocNo" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnSearchDocNo_Click" />

                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="TC Apply Date">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblApplyH" runat="server">TC Apply Date</asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtApply" runat="server" Width="75%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnApply" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnApply_Click" />


                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblApply" runat="server" Text='<%# Bind("TCM_APPLYDATE","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="TC Issue Date">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblIssueH" runat="server">Tc Issue Date</asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtIssue" runat="server" Width="75%"></asp:TextBox>
                                                            <asp:ImageButton ID="btnIssue" runat="server" ImageAlign="middle" ImageUrl="../Images/forum_search.gif" OnClick="btnIssue_Click" />


                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblIssue" runat="server" Text='<%# Bind("TCM_ISSUEDATE","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>



                                                    <asp:ButtonField CommandName="view" HeaderText="Cancel" Text="Cancel">
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:ButtonField>


                                                </Columns>
                                                <HeaderStyle CssClass="gridheader_pop" />
                                                <RowStyle CssClass="griditem" />
                                                <SelectedRowStyle CssClass="Green" />
                                                <AlternatingRowStyle CssClass="griditem_alternative" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                    type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server"
                                type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server"
                                type="hidden" value="=" /></td>
                    </tr>


                </table>

            </div>
        </div>
    </div>

</asp:Content>

