<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stuFeeServiceSetup.aspx.vb" Inherits="stuFeeServiceSetup" title="Untitled Page" %>
<%@ MasterType  virtualPath="~/mainMasterPage.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
    
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book"></i>
            <asp:Label ID="lblHeader" runat="server" Text="Service Provider Settings... "></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                  
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" ValidationGroup="FEE_COUNTER" />
    <br />
    <table align="center" width="100%" cellpadding="5" cellspacing="0">
        
        <tr>
            <td align="left" width="20%">
                 <span class="field-label">Academic Year</span></td>
            
            <td align="left" width="30%">
                <asp:DropDownList id="ddlACDYear" runat="server">
                </asp:DropDownList></td>
            <td align="left" width="20%">
                 <span class="field-label">Service</span></td>
           
            <td align="left">
                <asp:DropDownList id="ddlService" runat="server">
                </asp:DropDownList></td>
        </tr>
        
        <tr>
            <td align="left" >
                <span class="field-label"> Provider</span></td>
            
            <td align="left">
                <asp:DropDownList id="ddlProvideBSU" runat="server">
                </asp:DropDownList></td>
            <td></td>
            <td></td>
        </tr>
        <tr runat="server" visible="false">
            <td align="left" >
                 <span class="field-label">Rate</span></td>
            
            <td align="left">
                <asp:TextBox ID="txtRate" runat="server">0</asp:TextBox></td>
             <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <asp:CheckBox id="chkServiceAvailable" runat="server" CssClass="field-label" Text="Service Available">
                </asp:CheckBox></td>
        </tr>
        <tr>
            <td align="center"  colspan="4">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnAdd_Click" Text="Add" />
                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnEdit_Click" Text="Edit" />
                <asp:Button ID="btnSave" runat="server" CssClass="button" OnClick="btnSave_Click"
                    Text="Save" ValidationGroup="FEE_COUNTER" />
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="button"
                    Text="Delete" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    OnClick="btnCancel_Click" Text="Cancel" /></td>
        </tr>
    </table>

                </div>
            </div>
        </div>

</asp:Content>

