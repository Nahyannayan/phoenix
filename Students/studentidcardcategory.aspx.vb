﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Data.SqlClient
Imports OasisYearly
Imports System.Drawing

Partial Class Students_studentidcardcategory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindCatImage()
            BindGrid()
        End If

    End Sub


    Public Sub BindCatImage()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()


        Dim Sql_Query = " SELECT * FROM STU_CARD_CATEGORY_IMAGE "

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        If ds.Tables(0).Rows.Count > 0 Then

            ddcatimage.DataSource = ds
            ddcatimage.DataTextField = "IMAGE_DES"
            ddcatimage.DataValueField = "CT_IMG_ID"
            ddcatimage.DataBind()

        End If

        Dim list As New ListItem
        list.Text = "--Optional--"
        list.Value = "-1"

        ddcatimage.Items.Insert(0, list)



    End Sub
    Public Sub BindGrid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
        Dim Sql_Query = " SELECT *  FROM STU_CARD_CATEGORY a " & _
                        " left join dbo.STU_CARD_CATEGORY_IMAGE b on a.CT_IMG_ID= b.CT_IMG_ID " & _
                        " where bsu_id='" & Session("sbsuid") & "' "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

        GridDocuments.DataSource = ds
        GridDocuments.DataBind()


    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        If txtcatname.Text <> "" Then

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblmessage.Text = ""
            Try

                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@CAT_DES", txtcatname.Text.Trim())
                Dim color As String = ColorTranslator.ToHtml(RadColorPicker5.SelectedColor)
                Dim RBG = RadColorPicker5.SelectedColor.R.ToString() & "," & RadColorPicker5.SelectedColor.G.ToString() & "," & RadColorPicker5.SelectedColor.B.ToString()

                pParms(1) = New SqlClient.SqlParameter("@CT_COLOR_CODE", color)
                If ddcatimage.SelectedValue <> "-1" Then
                    pParms(2) = New SqlClient.SqlParameter("@CT_IMG_ID", ddcatimage.SelectedValue)
                End If
                Dim bsuid As String = Session("sbsuid")
                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", bsuid)
                pParms(4) = New SqlClient.SqlParameter("@OPTION", 1)
                pParms(5) = New SqlClient.SqlParameter("@RBG", RBG)

                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "TRAN_STU_CARD_CATEGORY", pParms)
                txtcatname.Text = ""
                ddcatimage.SelectedValue = "-1"
                transaction.Commit()
                BindGrid()
            Catch ex As Exception
                transaction.Rollback()
                lblmessage.Text = "Error : " & ex.Message
            Finally
                connection.Close()

            End Try

        Else
            lblmessage.Text = "Please Enter Category Name."
        End If

    End Sub

    Protected Sub GridDocuments_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridDocuments.RowCommand

        If e.CommandName = "deleting" Then
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblmessage.Text = ""
            Try

                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ID_CT_ID", e.CommandArgument)
                pParms(1) = New SqlClient.SqlParameter("@OPTION", 2)
                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "TRAN_STU_CARD_CATEGORY", pParms)
             
                transaction.Commit()
                BindGrid()
            Catch ex As Exception
                transaction.Rollback()
                lblmessage.Text = "Error : " & ex.Message
            Finally
                connection.Close()

            End Try

        End If

        If e.CommandName = "Editing" Then
            Hiddenrid.Value = e.CommandArgument
            btnsave.Visible = False
            btnupdate.VISIBLE = True
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString()
            Dim Sql_Query = " SELECT *  FROM STU_CARD_CATEGORY a " & _
                            " left join dbo.STU_CARD_CATEGORY_IMAGE b on a.CT_IMG_ID= b.CT_IMG_ID " & _
                            " where ID_CT_ID='" & e.CommandArgument & "' "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, Sql_Query)

            txtcatname.Text = ds.Tables(0).Rows(0).Item("CAT_DES").ToString()

            Dim op As String = ds.Tables(0).Rows(0).Item("CT_IMG_ID").ToString()

            If op <> "" Then
                ddcatimage.SelectedValue = op
            Else
                ddcatimage.SelectedValue = "-1"
            End If

        End If



    End Sub

    Protected Sub btnupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdate.Click


        If txtcatname.Text <> "" Then

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblmessage.Text = ""
            Try

                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@CAT_DES", txtcatname.Text.Trim())
                Dim color As String = ColorTranslator.ToHtml(RadColorPicker5.SelectedColor)
                Dim RBG = RadColorPicker5.SelectedColor.R.ToString() & "," & RadColorPicker5.SelectedColor.G.ToString() & "," & RadColorPicker5.SelectedColor.B.ToString()

                pParms(1) = New SqlClient.SqlParameter("@CT_COLOR_CODE", color)
                If ddcatimage.SelectedValue <> "-1" Then
                    pParms(2) = New SqlClient.SqlParameter("@CT_IMG_ID", ddcatimage.SelectedValue)
                End If
                Dim bsuid As String = Session("sbsuid")
                pParms(3) = New SqlClient.SqlParameter("@ID_CT_ID", Hiddenrid.Value)
                pParms(4) = New SqlClient.SqlParameter("@OPTION", 3)
                pParms(5) = New SqlClient.SqlParameter("@RBG", RBG)

                lblmessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "TRAN_STU_CARD_CATEGORY", pParms)
                txtcatname.Text = ""
                ddcatimage.SelectedValue = "-1"
                transaction.Commit()
                BindGrid()
                btnsave.Visible = True
                btnupdate.Visible = False
            Catch ex As Exception
                transaction.Rollback()
                lblmessage.Text = "Error : " & ex.Message
            Finally
                connection.Close()

            End Try

        Else
            lblmessage.Text = "Please Enter Category Name."
        End If




    End Sub

End Class
