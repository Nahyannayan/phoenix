<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="stuServiceDisctnueView.aspx.vb" Inherits="stuServiceDisctnueView" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/mainMasterPage.master" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">


    <script language="javascript" type="text/javascript">
        function getDate(left, top, txtControl) {
            var sFeatures;
            sFeatures = "dialogWidth: 250px; ";
            sFeatures += "dialogHeight: 270px; ";
            sFeatures += "dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: yes; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/calendar.aspx", "", sFeatures);
            if (result != '' && result != undefined) {
                switch (txtControl) {
                    case 0:
                        document.getElementById('<%=txtfroDate.ClientID %>').value = result;
                        break;
                    case 1:
                        document.getElementById('<%=txtToDate.ClientID %>').value = result;
                        break;
                }
            }
            return false;
        }

    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-graduation-cap mr-3"></i>
            Student Service Discontinue Request
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <table align="center" width="100%">
                    <tr>
                        <th align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></th>
                    </tr>
                    <tr>
                        <td align="left">
                            <table align="center" width="100%">
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlACDYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlACDYear_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" width="20%"><span class="field-label">Services</span></td>
                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlService" runat="server" Width="135px" OnSelectedIndexChanged="ddlService_SelectedIndexChanged" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">From Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtFroDate" runat="server">
                                        </asp:TextBox>
                                        <%-- <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return getDate(550, 310, 0)"></asp:ImageButton>--%>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFroDate" Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                    <td align="left" width="20%"><span class="field-label">To Date</span></td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtToDate" runat="server">
                                        </asp:TextBox>
                                        <%-- <asp:ImageButton ID="imgFromDate" runat="server" CausesValidation="False"
                                            ImageUrl="~/Images/calendar.gif" OnClientClick="return getDate(550, 310, 1)"></asp:ImageButton>--%>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToDate" Format="dd/MMM/yyyy">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="button" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th align="left">
                            <asp:LinkButton ID="hlAddnew" runat="server">New Request</asp:LinkButton></th>
                    </tr>

                    <tr>
                        <td align="center" class="matters" valign="top">
                            <asp:GridView ID="gvJournal" runat="server" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-row table-bordered"
                                EmptyDataText="No Data Found" PageSize="30" Width="100%" OnRowCommand="gvJournal_RowCommand" OnRowDeleting="gvJournal_RowDeleting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Academic Year">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAcdYear" runat="server" Text='<%# Bind("ACY_DESCR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student No">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Service">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Date">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("SSD_REQUESTED_DT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Discontinue From">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("SSD_SERVICE_DIS_DT") %>'></asp:Label>
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="STATUSDIS" HeaderText="Status"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkView" runat="server" CommandName="View" CommandArgument='<%# Bind("SSD_ID") %>'>Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" OnClick="LinkButton1_Click" runat="server" CommandArgument='<%# Bind("SSD_ID") %>' CommandName="Delete">Delete</asp:LinkButton>
                                            <asp:HiddenField ID="H_ID" runat="server" Value='<%# Bind("SSD_ID") %>'></asp:HiddenField>
                                            <ajaxToolkit:ConfirmButtonExtender ID="delConfirm" runat="server" ConfirmText="Do you Wnat to delete the Request" TargetControlID="LinkButton1"></ajaxToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STU_ID" Visible="False">
                                        <EditItemTemplate>
                                            &nbsp; 
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblELA_ID" runat="server" Text='<%# Bind("SSD_STU_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

