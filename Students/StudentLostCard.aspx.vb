﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.IO
Imports Telerik.Web.UI
Imports System.Web.Configuration

Partial Class Students_StudentLostCard
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Page.Title = OASISConstants.Gemstitle
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            If Request.QueryString("datamode") <> "" Then
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            End If
            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or ViewState("MainMnu_code") <> "S200456" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
            End If
            If Request.QueryString("TYPE") <> "" Then
                ViewState("TYPE") = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
            End If

            If Request.QueryString("viewid") <> "" Then
                BindStudentRequestDetail(Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+")))
                UtilityObj.beforeLoopingControls(Me.Page)
            Else
                clearMe()
            End If
        End If
    End Sub
    'Protected Sub ddlStudents_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlStudents.SelectedIndexChanged
    '    If ddlStudents.SelectedItem IsNot Nothing Then
    '        BindStudentDetail(ddlStudents.SelectedItem.Value)
    '    End If
    'End Sub

    Public Sub BindStudentDetail(ByVal stuid As String)
        Try
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(0) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", stuid)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "GETSTUDENTDETAIL_LOSTCARD", param)
            lnkRequestNewCard.Visible = False
            DisableControl(False)
            clearMe()
            If ds.Tables(0).Rows.Count > 0 Then
                tr_StudentDet.Visible = True
                If ds.Tables(0).Rows.Count > 0 Then
                    lblStdNo.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_NO").ToString())
                    lblStudName.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_Name").ToString())
                    lblacademicyear.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("ACY_DESCR").ToString())
                    lblGrade.Text = Convert.ToString((ds.Tables(0).Rows(0)("STU_GRD_ID")).ToString)
                    lblsect.Text = Convert.ToString(ds.Tables(0).Rows(0)("SCT_DESCR"))
                    lblschoolname.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("BSU_NAME").ToString())
                    lblNewCardRate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("SVB_FEE_IDCARD").ToString()) & " " & Convert.ToString(ds.Tables(0).Rows(0).Item("BSU_CURRENCY").ToString())
                    chkRequest.Checked = ds.Tables(0).Rows(0).Item("SLC_bRequestNew")
                    txtRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("SLC_REMARKS").ToString())
                    If ds.Tables(0).Rows(0).Item("REPORTED") = True And ds.Tables(0).Rows(0).Item("SLC_bRequestNew") = True And ds.Tables(0).Rows(0).Item("SLC_bPaid") = True Then
                        lblReqStatus.Text = "You have already reported a lost card and requested for a new card."
                        DisableControl(True)
                        tr_Status.Visible = True
                        hdnSLCID.Value = ds.Tables(0).Rows(0).Item("SLC_ID")
                    ElseIf ds.Tables(0).Rows(0).Item("REPORTED") = True And ds.Tables(0).Rows(0).Item("SLC_bRequestNew") = False And ds.Tables(0).Rows(0).Item("SLC_bPaid") = False Then
                        lblReqStatus.Text = "You have already reported a lost card ."
                        lnkRequestNewCard.Visible = True
                        DisableControl(True)
                        tr_Status.Visible = True
                        hdnSLCID.Value = ds.Tables(0).Rows(0).Item("SLC_ID")
                    ElseIf ds.Tables(0).Rows(0).Item("REPORTED") = True And ds.Tables(0).Rows(0).Item("SLC_bRequestNew") = True And ds.Tables(0).Rows(0).Item("SLC_bPaid") = False Then
                        lblReqStatus.Text = "You have already reported a lost card and requested for a new card."
                        DisableControl(True)
                        tr_Status.Visible = True
                        hdnSLCID.Value = ds.Tables(0).Rows(0).Item("SLC_ID")
                    Else
                        hdnSLCID.Value = 0
                    End If
                    BindStudentRequests()
                End If
                If ViewState("datamode") = "add" Then
                    imgStudent.Enabled = True
                Else
                    imgStudent.Enabled = False
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub BindStudentRequests()
        Try
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
            param(1) = New SqlClient.SqlParameter("@NotPaid", 0)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[GETSTUDENT_LOSTCARD_REQUEST]", param)
            'gvReportLostCardDetails.DataSource = ds.Tables(0)
            ' gvReportLostCardDetails.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Private Function getStudentLostCardRequestDetail(ByVal SLC_ID As Int16) As DataTable
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@SLC_ID", SLC_ID)
        Dim dt As New DataTable
        dt = Mainclass.getDataTable("GETSTUDENT_LOSTCARD_REQUEST_DETAIL", param, ConnectionManger.GetOASISConnectionString)
        getStudentLostCardRequestDetail = dt
    End Function

    Public Sub BindStudentRequestDetail(ByVal SLC_ID As Int16)
        Try
            Dim dt As New DataTable
            dt = getStudentLostCardRequestDetail(SLC_ID)
            If dt.Rows.Count > 0 Then
                hdnSLCID.Value = dt.Rows(0).Item("SLC_ID").ToString
                txtRemarks.Text = dt.Rows(0).Item("SLC_REMARKS").ToString
                hdnStatus.Value = dt.Rows(0).Item("SLC_STATUS").ToString
                hdnRequestNew.Value = IIf(dt.Rows(0).Item("SLC_bRequestNew").ToString, 1, 0)
                BindStudentDetail(dt.Rows(0).Item("SLC_STU_ID").ToString)
                DisableControl(True)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub imgStudent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'If h_STU_IDs.Value <> "" Then
        '    BindStudentDetail(h_STU_IDs.Value)
        'End If

    End Sub
    Protected Sub lblbtn_Click(sender As Object, e As EventArgs) Handles lblbtn.Click
        If h_STU_IDs.Value <> "" Then
            BindStudentDetail(h_STU_IDs.Value)
        End If
    End Sub
    Private Function SaveData(ByVal AlsoRequestNew As Boolean) As String
        Try
            AlsoRequestNew = True
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim sqlParam(7) As SqlParameter
            sqlParam(0) = Mainclass.CreateSqlParameter("@SLC_ID", Val(hdnSLCID.Value), SqlDbType.Int, True)
            sqlParam(1) = Mainclass.CreateSqlParameter("@SLC_SOURCE", "COUNTER", SqlDbType.VarChar)
            sqlParam(2) = Mainclass.CreateSqlParameter("@SLC_STU_ID", h_STU_IDs.Value, SqlDbType.Int)
            sqlParam(3) = Mainclass.CreateSqlParameter("@SLC_USR_ID", Session("sUsr_id"), SqlDbType.VarChar)
            sqlParam(4) = Mainclass.CreateSqlParameter("@SLC_REMARKS", txtRemarks.Text, SqlDbType.VarChar)
            sqlParam(5) = Mainclass.CreateSqlParameter("@SLC_bRequestNew", AlsoRequestNew, SqlDbType.Bit)
            sqlParam(6) = Mainclass.CreateSqlParameter("@SLC_STATUS", 1, SqlDbType.Int)
            sqlParam(7) = Mainclass.CreateSqlParameter("@ReturnValue", "", SqlDbType.VarChar, True, 200)
            Dim Retval As String
            Retval = Mainclass.ExecuteParamQRY(str_conn, "[SaveStudentLostCardRequest]", sqlParam)
            If (Retval = "0" Or Retval = "") And sqlParam(7).Value = "" Then
                SaveData = ""
                hdnSLCID.Value = sqlParam(0).Value
                BindStudentRequests()
                BindStudentRequestDetail(hdnSLCID.Value)
                If AlsoRequestNew Then
                    lblerror.Text = "Requested new card"
                Else
                    lblerror.Text = "Lost card reported"
                End If
                SaveData = ""
                ViewState("datamode") = "view"
                btnAdd.Visible = True
            Else
                SaveData = IIf(Retval = "-1", "Unexpected Error !!!", sqlParam(7).Value)
            End If
        Catch ex As Exception
            SaveData = "Request Cannot be processed"
        End Try
    End Function

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        SaveData(chkRequest.Checked)
    End Sub
    Private Sub DisableControl(ByVal mDisable As Boolean)
        txtRemarks.Enabled = Not mDisable
        btnReport.Enabled = Not mDisable
        chkRequest.Enabled = Not mDisable
    End Sub
    Private Sub clearMe()
        txtRemarks.Text = ""
        chkRequest.Checked = False
        tr_Status.Visible = False
    End Sub
    Private Sub clearStudentDetails()
        lblStdNo.Text = ""
        lblStudName.Text = ""
        lblacademicyear.Text = ""
        lblGrade.Text = ""
        lblsect.Text = ""
        lblschoolname.Text = ""
        lblNewCardRate.Text = ""
        tr_StudentDet.visible = False
    End Sub
    Protected Sub lnkRequestNewCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRequestNewCard.Click
        SaveData(True)
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("datamode") = "add"
        imgStudent.Enabled = True
        clearMe()
        DisableControl(False)
        clearStudentDetails()
        BindStudentDetail(0)
        Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If ViewState("ReferrerUrl") IsNot Nothing Then
            Response.Redirect(ViewState("ReferrerUrl"))
        End If
    End Sub
End Class
