<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="STUBSU_APPLIC_VIEW.aspx.vb" Inherits="Students_studTCSO_View" Title="::::GEMS OASIS:::: Online Student Administration System::::"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <script src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>

   

    <!--1nd drop down menu -->
 
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="OFFER LETTER SETTING"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
              <%--  <table align="center" width="100%" cellpadding="0" cellspacing="0"
                  >
                    <tr>
                      
                        <td  colspan="2" style="font-weight: bold" valign="top">--%>
                          <%--  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; color: #0000ff">
                                <tr>
                                    <td>--%>
                                        <table align="center"  cellpadding="0" cellspacing="0"
                                            width="100%">
                                            <tr>
                                                <td align="left"   valign="top">
                                                    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  valign="top">
                                                    <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                                            </tr>
                                            <tr>
                                                <td align="left"  valign="top">
                                                    <table id="tbl_test" runat="server" align="center"
                                                        cellpadding="5" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left" valign="middle" width="20%" >
                                                                <asp:Label ID="lblAccText" runat="server" Text="Select Application decision " CssClass="field-label"></asp:Label>
                                                                </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlAppDes" runat="server" AutoPostBack="True"  OnSelectedIndexChanged="ddlAppDes_SelectedIndexChanged">
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr >
                                                            <td align="left"  colspan="2" valign="top">
                                                                <asp:GridView ID="gvoffLetter_View" runat="server" AutoGenerateColumns="False"
                                                                   CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                                                    HeaderStyle-Height="30" PageSize="30" Width="100%">
                                                                    <RowStyle CssClass="griditem"  />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="HideID" Visible="False">
                                                                            <EditItemTemplate>
                                                                                &nbsp; 
                                                                            </EditItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblHideID" runat="server" Text='<%# Bind("BAL_ID") %>' __designer:wfdid="w39"></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle ></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Subject">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("Subject") %>' __designer:wfdid="w40"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Matter">
                                                                            <EditItemTemplate>
                                                                                &nbsp; 
                                                                            </EditItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblMatter" runat="server" Text='<%# Bind("Matter") %>' __designer:wfdid="w44"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Remarks">
                                                                            <EditItemTemplate>
                                                                                &nbsp; 
                                                                            </EditItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>' __designer:wfdid="w48"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Signature">
                                                                            <EditItemTemplate>
                                                                            </EditItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSignApp" runat="server" Text='<%# Bind("SignApp") %>' __designer:wfdid="w52"></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle Wrap="True"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:ButtonField CommandName="View" Text="View" HeaderText="View">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" ></ItemStyle>
                                                                        </asp:ButtonField>
                                                                    </Columns>
                                                                    <SelectedRowStyle  />
                                                                    <HeaderStyle CssClass="gridheader_pop" />
                                                                    <AlternatingRowStyle CssClass="griditem_alternative" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                   <%-- </td>
                                </tr>
                            </table>--%>



                        <%--</td>
                    </tr>
                </table>--%>


            </div>
        </div>
    </div>
</asp:Content>

