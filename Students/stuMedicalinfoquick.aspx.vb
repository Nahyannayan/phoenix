﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class Students_stuMedicalinfoquick
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Page.IsPostBack = False Then
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'hardcode the menu code
                If USR_NAME = "" Then
                    Response.Redirect("~\noAccess.aspx")
                End If
                'Dim Cstu_id As String = Session("Cstu_id")
                'If Not Request.QueryString("Stu_id") And Not Request.QueryString("Sib_id") Is Nothing Then
                '    ''  ViewState("stu_id") = Encr_decrData.Decrypt(Request.QueryString("Stu_id").Replace(" ", "+"))
                '    ViewState("stu_id") = Request.QueryString("Stu_id").ToString
                '    ViewState("Sib_id") = Request.QueryString("Sib_id").ToString
                '    'Session("sUsr_name") = 0
                'Else
                '    ViewState("stu_id") = ""
                '    Response.Redirect("~\noAccess.aspx")
                'End If '

                '  ViewState("viewid") = Session("Trans_Stu_ID")
                'HF_SibId.Value = ViewState("Sib_id")
                'HF_stuid.Value = ViewState("stu_id")
                Dim charSplit As String = "|"
                Dim Links As String() = New String(2) {}

                If Not Session("Link_Ref") Is Nothing Then
                    Links = Session("Link_Ref").ToString.Split(charSplit)
                End If
                ViewState("stu_id") = Links(0)
                binddetails(ViewState("stu_id"))
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim con As New SqlConnection(str_conn)
        Dim transaction As SqlTransaction
        Dim STU_bMEDICAL As Integer

        'If rbMedical_Yes.Checked = True Then
        STU_bMEDICAL = 1
        ' Else
        STU_bMEDICAL = 0
        ' txtMedical_Note.Text = ""
        'End If

        con.Open()
        transaction = con.BeginTransaction("trans")
        Try
            If Page.IsValid = True Then
                Dim param(5) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@STU_ID", ViewState("stu_id"))
                param(1) = New SqlClient.SqlParameter("@STU_bMEDICAL", STU_bMEDICAL)
                param(2) = New SqlClient.SqlParameter("@STU_MEDICAL", txtMedical_Note.Text)
                param(3) = New SqlClient.SqlParameter("@STU_MEDICAL_action", txtMedical_Action.Text)
                param(4) = New SqlClient.SqlParameter("@USR", Session("sUsr_name"))

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVE_STU_QUICK_MED_DETAILS", param)
            End If
            transaction.Commit()
            lblError.Text = "Records Saved sucessfully...!!!!"
            txtMedical_Action.Text = ""
            txtMedical_Note.Text = ""
            binddetails(ViewState("stu_id"))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "SAVEMEDICALQUICK")
            transaction.Rollback()
            lblError.Text = "Insertion Failed...!!!!"
        End Try
    End Sub

    Sub binddetails(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim dt As New DataTable
        
            ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.Get_Stu_quick_Med_Details", param)
            If Not ds2 Is Nothing Then
                If Not ds2.Tables(0) Is Nothing Then


                    If ds2.Tables(0).Rows.Count > 0 Then
                        gvStudMEdInfo.DataSource = ds2.Tables(0)
                        gvStudMEdInfo.DataBind()
                    End If
                End If
            End If

            Dim param1(1) As SqlClient.SqlParameter
            param1(0) = New SqlClient.SqlParameter("@STUID", stu_id)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[STU].[GET_STUDENT_INFO_STUID]", param1)
            If Not ds Is Nothing Then
                If Not ds.Tables(0) Is Nothing Then


                    If ds.Tables(0).Rows.Count > 0 Then
                       

                        dt = ds.Tables(0)

                        ltStuname.Text = Convert.ToString(dt.Rows(0)("STU_PASPRTNAME")).ToString()
                        ltStuNo.Text = Convert.ToString(dt.Rows(0)("STU_NO")).ToString()
                        'ltCLM.Text = Convert.ToString(readerStudent_Detail("clm_descr"))
                        ltGradeSection.Text = Convert.ToString(dt.Rows(0)("GRM_DISPLAY")).ToString() & "-" & Convert.ToString(dt.Rows(0)("SCT_DESCR")).ToString()

                    End If
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "BINDMEDICALQUICK")
        End Try
    End Sub
End Class
