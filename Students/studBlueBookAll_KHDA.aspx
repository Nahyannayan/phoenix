<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studBlueBookAll_KHDA.aspx.vb" Inherits="Students_studBlueBook_KHDA" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
 <script language="javascript" type="text/javascript">
 function getDate(left,top,txtControl) 
           {
            var sFeatures;
            sFeatures="dialogWidth: 250px; ";
            sFeatures+="dialogHeight: 270px; ";
            sFeatures+="dialogTop: " + top + "px; dialogLeft: " + left + "px";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: yes; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";

            var NameandCode;
            var result;
            result = window.showModalDialog("../Accounts/calendar.aspx","", sFeatures);
            if(result != '' && result != undefined)
            {
                switch(txtControl)
                {
                  case 1:
                    document.getElementById('<%=txtAcdDate_From.ClientID %>').value=result;
                    break;
                   case 2:
                    document.getElementById('<%=txtAcdDate_To.ClientID %>').value=result;
                    break;
                     case 3:
                    document.getElementById('<%=txtTC_SO_Cutoff.ClientID %>').value=result;
                    break;
                   }
            }
            return false;    
           }
         
    </script>
   

    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0" style="width: 465px; position: relative">
        <tr>
            <td align="left">
                &nbsp;</td>
        </tr>
        <tr valign="bottom">
            <td align="left" valign="bottom">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error"></asp:Label></td>
        </tr>
        <tr>
            <td class="matters" style="font-weight: bold; height: 88px" valign="top">
                <table align="center" border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
                    style="width: 647px; height: 66px">
                    <tr class="subheader_img">
                        <td align="left" colspan="15" valign="middle">
                            <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="2"><span style="font-family: Verdana">
                                KHDA Export</span></font>
                        </td>
                    </tr>
                    
                      <tr>
                        <td align="left" class="matters" >
                            Academic Year</td>
                        <td class="matters" align="center">
                            :</td>
                        <td align="left" class="matters"  style="text-align: left">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList></td>
                            
                            
                              <td align="left" class="matters">
                            Regular/Observer</td>
                        <td class="matters" style="width: 7px; color: #1b80b6">
                            :</td>
                        <td align="left" class="matters" colspan="10">
                           <asp:DropDownList ID="ddlMinList" runat="server"  Width="108px">
                           
                             <asp:ListItem Value="REGULAR">REGULAR</asp:ListItem>     
                             <asp:ListItem Value="OBSERVER">OBSERVER</asp:ListItem>                                                   
                             <asp:ListItem Value="ALL">ALL</asp:ListItem>                             
                          </asp:DropDownList>       
                            
                            
                            </td>
                            
                            
                    </tr>
                    
                    
                    
                    <tr>
                        <td align="left" class="matters">
                            Grade</td>
                        <td class="matters" style="width: 1px">
                            :</td>
                        <td align="left" class="matters" style="width: 149px">
                            <asp:DropDownList ID="ddlGrade" runat="server" Style="position: relative" AutoPostBack="True">
                            </asp:DropDownList></td>
                        <td align="left" class="matters">
                            Section</td>
                        <td class="matters" style="width: 7px; color: #1b80b6">
                            :</td>
                        <td align="left" class="matters" colspan="10">
                            <asp:DropDownList ID="ddlSection" runat="server" Style="position: relative">
                            </asp:DropDownList></td>
                    </tr>
                    
                    
                    
                     <tr>
                        <td align="left" class="matters">
                            DOJ From</td>
                        <td class="matters" style="width: 1px">
                            :</td>
                        <td align="left" class="matters" style="width: 149px">
                         <asp:TextBox ID="txtAcdDate_From" runat="server" Width="123px"></asp:TextBox><span style="font-size: 7pt">
                            </span>
                         <%--   <asp:ImageButton ID="imgShowDate_From" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return getDate(550, 310, 1)" />--%>
                              <br />
                            <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                        </td>
                        <td align="left" class="matters">
                            DOJ To</td>
                        <td class="matters" style="width: 7px; color: #1b80b6">
                            :</td>
                        <td align="left" class="matters" colspan="10">
                         <asp:TextBox ID="txtAcdDate_To" runat="server" Width="123px"></asp:TextBox><span style="font-size: 7pt">
                            </span>
                            <%--<asp:ImageButton ID="imgShowDate_To" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return getDate(550, 310, 2)" />--%>
                           
                            <br />
                            <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                        
                        
                        </td>
                    </tr>
                    
                    
               <tr>
                        <td align="left" class="matters">
                            Include TC/SO</td>
                        <td class="matters" style="width: 1px">
                            :</td>
                        <td align="left" class="matters" style="width: 149px">
                         <asp:CheckBox ID="consider_tc" runat="server" 
                    Text="TC" />
                     <asp:CheckBox ID="consider_so" runat="server" 
                    Text="SO" />
                    
                        </td>
                        <td align="left" class="matters">
                            LDA Cutoff</td>
                        <td class="matters" style="width: 7px; color: #1b80b6">
                            :</td>
                        <td align="left" class="matters" colspan="10">
                            <asp:TextBox ID="txtTC_SO_Cutoff" runat="server" Width="123px"></asp:TextBox><span style="font-size: 7pt">
                            </span>
                            <%--<asp:ImageButton ID="imgShowDate_TC" runat="server" CausesValidation="False" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="return getDate(550, 310, 3)" />--%>
                           
                            <br />
                            <span style="font-size: 7pt">(dd/mmm/yyyy)</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters">
                            Transfer Type</td>
                        <td class="matters" style="width: 1px">
                            :</td>
                        <td align="left" class="matters" colspan="13">
                            <asp:DropDownList ID="ddlTransferType" runat="server">
                               <%-- <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem Value="I">Internal</asp:ListItem>
                                <asp:ListItem Value="N">New</asp:ListItem>
                                <asp:ListItem Value="O">Overseas</asp:ListItem>
                                <asp:ListItem Value="R">Re-Admit</asp:ListItem>--%>
                            </asp:DropDownList></td>
                    </tr>
                    
                    
                    
                </table>
            </td>
        </tr>
        
         
                    
                    
        <tr>
            <td class="matters" style="height: 1px" valign="bottom">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="matters" style="height: 19px" valign="bottom">
                &nbsp;
               
                  <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export Data To Excel"  />&nbsp;
            </td>
           
                
              
            
        </tr>
        <tr>
            <td class="matters" valign="bottom">
                &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            </td>
        </tr>
    </table>
        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtAcdDate_From">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtAcdDate_To">
    </ajaxToolkit:CalendarExtender>
     <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtTC_SO_Cutoff">
    </ajaxToolkit:CalendarExtender>
    &nbsp;
</asp:Content>

