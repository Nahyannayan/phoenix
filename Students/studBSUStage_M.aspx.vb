Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collection
Partial Class Students_studBSUStage_M
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then

            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If
            '   Try


            'Dim MainMnu_code As String = String.Empty
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))


            If Not Request.UrlReferrer Is Nothing Then

                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))


            Dim CurUsr_id As String = Session("sUsr_id")
            Dim CurRole_id As String = Session("sroleid")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            If USR_NAME = "" Or CurBsUnit = "" Or (ViewState("MainMnu_code") <> "S050110") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                'GetActive_ACD_4_Grade


                '  Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))

                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                BindList()
            End If
            'Catch ex As Exception

            '    lblError.Text = "Request could not be processed "
            'End Try

        End If


    End Sub
    Private Function isPageExpired() As Boolean
        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub BindList()
        lstRequired.Items.Clear()
        lstNotRequired.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim count As Integer
        Dim str_query As String = "SELECT COUNT(PRB_ID) FROM PROCESSFO_BSU_M WHERE PRB_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        'if there are no records in processfo_bsu_m then select from processfo_sys_m
        If count = 0 Then
            str_query = "SELECT PRO_ID,PRO_DESCRIPTION,PRB_bREQUIRED=convert(bit,0) FROM PROCESSFO_SYS_M WHERE PRO_ID  BETWEEN 3 AND 6 ORDER BY PRO_ORDER"
        Else
            str_query = "SELECT PRO_ID,PRO_DESCRIPTION, isNULL(PRB_bREQUIRED,0) as PRB_bREQUIRED" _
                        & " FROM PROCESSFO_SYS_M LEFT JOIN  PROCESSFO_BSU_M  ON " _
                        & " PROCESSFO_BSU_M.PRB_BSU_ID = '" + Session("SBSUID") + "'" _
                        & " AND PROCESSFO_BSU_M.PRB_STG_ID = PROCESSFO_SYS_M.PRO_ID  AND PRB_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " " _
                        & " WHERE  PRO_ID BETWEEN 3 AND 6   ORDER BY PRB_STG_ORDER"
        End If

        Dim chk As CheckBox
        Dim txt As TextBox


        Dim reader As SqlDataReader
        reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim i As Integer = 0
        Dim li As ListItem

        While reader.Read
            li = New ListItem
            li.Value = reader.GetValue(0)
            li.Text = reader.GetString(1)
            If reader.GetBoolean(2) = True Then
                lstRequired.Items.Add(li)
            Else
                lstNotRequired.Items.Add(li)
            End If

        End While
    End Sub

   
    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim i As Integer
        Dim lstCount As Integer

        Dim strReqs As String()
        Dim strNotReqs As String()
        Dim strReq As String()
        Dim strNotReq As String()
        If lstValuesReq.Value.Length <> 0 Then
            strReqs = lstValuesReq.Value.Split("|")
            For i = 0 To strReqs.Length - 1
                strReq = strReqs(i).Split("$")
                str_query = "exec saveBSUSTAGES '" + Session("SBSUID") + "'," _
                          & ddlAcademicYear.SelectedValue.ToString + "," _
                          & strReq(1) + "," _
                          & "'true'," _
                          & (3 + i).ToString
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next

        End If
        If lstValuesNotReq.Value.Length <> 0 Then
            strNotReqs = lstValuesNotReq.Value.Split("|")
            lstCount = strReqs.Length
            For i = 0 To strNotReqs.Length - 1
                strNotReq = strNotReqs(i).Split("$")
                str_query = "exec saveBSUSTAGES '" + Session("SBSUID") + "'," _
                        & ddlAcademicYear.SelectedValue.ToString + "," _
                        & strNotReq(1) + "," _
                        & "'false'," _
                        & (lstCount + 3 + i).ToString
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Next
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
            lblError.Text = "Records Saved Successfully"
        Catch ex As Exception
            lblError.Enabled = "Error while saving data"
        Finally
            BindList()
        End Try



    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl"))
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindList()
    End Sub
End Class
