<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studTCSOCancel_M.aspx.vb" Inherits="Students_studTCSOCancel_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">
<script language="javascript" type="text/javascript">
   function getDate(val) 
          {     
            var sFeatures;
            sFeatures="dialogWidth: 227px; ";   
            sFeatures+="dialogHeight: 252px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: no; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            var nofuture="../Accounts/calendar.aspx?nofuture=yes";
           if (val==0)
            { 
           result = window.showModalDialog(nofuture,"", sFeatures)
           
         } 
         else
         {
            result = window.showModalDialog("../Accounts/calendar.aspx","", sFeatures)}
            if (result=='' || result==undefined)
            {
            return false;
            }
            
            if (val==1)
            { 
           document.getElementById('<%=txtCancelDate.ClientID %>').value=result; 
         }  
      
          
      return false;
    }  
 
 function confirm_cancel()
{

  if (confirm("You are about to cancel this TC/SO.Do you wish to proceed?")==true)
    return true;
  else
    return false;
 
}
</script>

     <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Label ID="lblMode" runat="server" Text="Label"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive">

<table id="tbl_AddGroup" runat="server" align="center" width="100%">
        <tr>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    HeaderText="You must enter a value in the following fields:" 
                    ValidationGroup="groupM1" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCancelDate"
                    Display="None" ErrorMessage="Please enter the date" ValidationGroup="groupM1"
                    ></asp:RequiredFieldValidator>
          
            </td>
        </tr>
        <tr valign="bottom">
            <td align="left">
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                    SkinID="error"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <table align="center" width="100%">
                   
                    <tr>
           
                            <td align="left" width="25%">
               <asp:Label ID="lblShift" runat="server" CssClass="field-label" Text="Date"></asp:Label></td>
              
                 <td align="left" width="30%">
                         <asp:TextBox ID="txtCancelDate" runat="server"></asp:TextBox>
                       <%--  <asp:ImageButton ID="imgBtnEnqDate" runat="server" ImageUrl="~/Images/calendar.gif"
                          OnClientClick="getDate(1);return false;" /><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCancelDate"
                            Display="Dynamic" ErrorMessage="Enter the  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtCancelDate"
                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Date entered is not a valid date"
                            ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>--%></td>
                        
                   <td></td>
                   <td></td> 
            </tr>
            <tr id="trExit" runat="server">
            <td align="left"><span class="field-label">Cancel Reason</span></td>
            
            <td >
            <asp:RadioButton ID="rdCorrection" runat="server" CssClass="field-label" Text="Data Correction" /><br />
            <asp:RadioButton ID="rdParent" runat="server" CssClass="field-label" Text="Parent requested for cancellation" /><br />
            <asp:RadioButton ID="rdPrincipal" runat="server" CssClass="field-label" Text="Reversed After Exit Interview with Principal" /><br />
             <asp:RadioButton ID="rdCorporate" runat="server" CssClass="field-label" Text="Reversed After Exit Interview with Corporate" />
            <br />
            </td>
                <td></td>
                <td></td>
            </tr>
                    <tr>
                        <td align="left">
                          <asp:Label ID="lblReason" CssClass="field-label" Text="Cancel Reason" runat="server"></asp:Label></td>
                       <td align="left" >
                      <asp:TextBox id="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                      </td>        
                     <td></td>
                     <td></td>
                       </tr>  </table>
               </td>
        </tr>
       
        <tr>
            <td align="center">
                
                <asp:Button ID="btnUpdate" OnClientClick="javascript:return confirm_cancel();" runat="server" CssClass="button"
                 Text="Update" ValidationGroup="groupM1" CausesValidation="False" />
                </td>
        </tr>
        <tr>
            <td valign="bottom">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please enter data in the field remarks" ValidationGroup="groupM1"
                    Width="23px"></asp:RequiredFieldValidator>&nbsp;
                <asp:HiddenField ID="hfSTU_ID" runat="server" />
                <asp:HiddenField ID="hfTCM_ID" runat="server" /><asp:HiddenField ID="hfMode" runat="server" />
                &nbsp;&nbsp;
           </td>
        </tr>
    </table>
                </div>
            </div>
         </div>

</asp:Content>

