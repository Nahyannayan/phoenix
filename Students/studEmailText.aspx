<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEmailText.aspx.vb" Inherits="Students_studEmailText" title="Untitled Page" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-user mr-3"></i>
            Email Text
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
                <table width="100%">
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Label ID="lblerror" CssClass="error" runat="server" EnableViewState="False" ></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td width="20%"><span class="field-label">Academic Year</span>
                        </td>

                        <td width="30%">
                            <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td width="20%"><span class="field-label">Grade</span>
                        </td>

                        <td width="30%">
                            <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true"></asp:DropDownList>

                        </td>
                    </tr>
                    <tr align="left" id="tr_Status" runat="server">
                        <td width="20%"><span class="field-label">Stream</span>
                        </td>

                        <td>
                            <asp:DropDownList ID="ddlStream" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td width="20%"><span class="field-label">Subject</span>
                        </td>

                        <td>
                            <asp:DropDownList ID="ddlSubject" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="right">
                            <asp:LinkButton ID="lnkCopy" runat="server" Text="Copy To"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="Tr2" align="left" runat="server">
                        <td width="20%"><span class="field-label">Text</span>
                        </td>
                        <td colspan="3">
                            <telerik:RadEditor ID="txtText" runat="server" EditModes="All" >
                                <Tools>
                                    <telerik:EditorToolGroup>
                                        <telerik:EditorTool Name="Bold" />
                                        <telerik:EditorTool Name="Italic" />
                                        <telerik:EditorTool Name="InsertOrderedList" />
                                        <telerik:EditorTool Name="InsertUnorderedList" />
                                    </telerik:EditorToolGroup>
                                </Tools>
                            </telerik:RadEditor>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" />

                        </td>
                    </tr>
                </table>

                <div id="divEmail" runat="server" class="panel-cover" visible="false">
                    <div >
                        <asp:Button ID="btClose" type="button" runat="server"
                            Style="float: right; margin-top: -1px; margin-right: -1px; font-size: 14px; color: white; border: 1px solid red; border-radius: 10px 10px; background-color: red;"
                            ForeColor="White" Text="X"></asp:Button>
                        <div class="holderInner">
                            <div align="left">
                                <asp:Label ID="lblUerror" runat="server" CssClass="error" EnableViewState="False"
                                    ></asp:Label>
                            </div>
                           
                            <table  cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Academic Year</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlAcyear" runat="server"  AutoPostBack="true"></asp:DropDownList>
                                    </td>

                                    <td align="left" width="20%"><span class="field-label">Grade</span>
                                    </td>

                                    <td align="left" width="30%">
                                        <asp:DropDownList ID="ddlGradeEmail" runat="server"  AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                </tr>
                              
                                <tr>
                                    <td align="left" width="20%"><span class="field-label">Stream</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlStreamEmail" runat="server" ></asp:DropDownList>
                                    </td>

                                     <td align="left" width="20%"><span class="field-label">Subject</span>
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubjectEmail" runat="server" ></asp:DropDownList>
                                    </td>
                                </tr>
                               

                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnUpdate"  Text="Copy" runat="server"  CssClass="button"/>
                                        <asp:Button ID="btnUClose" Text="Close" runat="server"  CssClass="button"/>
                                    </td>
                                </tr>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div> 
</asp:Content>

