<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAuto_Att_View.aspx.vb" Inherits="Students_studAuto_Att_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Schedule Auto Attendance "></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">



                <table width="100%">
                    <tr>
                        <td align="left"   valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top"  >
                            <asp:LinkButton ID="lbAddNew" runat="server" Font-Bold="True" OnClick="lbAddNew_Click">Add New</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table id="tbl_test" runat="server" width="100%"
                                 >
                                <tr  >
                                    <td align="left" class="matters" colspan="9" valign="top">
                                        <asp:GridView ID="gvAutoATT" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="Your Search query does not match any records. Kindly try with some other keywords."
                                            PageSize="20" Width="100%">
                                            <RowStyle CssClass="griditem"    />
                                            <Columns>
                                                <asp:TemplateField HeaderText="AAM_DESCR">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblDescription_H" runat="server" Text="Description"
                                                                            CssClass="gridheader_text" __designer:wfdid="w51"></asp:Label><br />
                                                                                        <asp:TextBox ID="txtAAM_DESCR" runat="server"  
                                                                                            __designer:wfdid="w7"></asp:TextBox> 
                                                                                        <asp:ImageButton ID="btnSearchAAM_DESCR" runat="server"
                                                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"
                                                                                            __designer:wfdid="w8" OnClick="btnSearchAAM_DESCR_Click"></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAAM_DESCR" runat="server" Text='<%# Bind("AAM_DESCR") %>'
                                                            ></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="APD_PARAM_DESCR">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblAttParamH"
                                                                            runat="server" Text="Att. Parameter" CssClass="gridheader_text"
                                                                           ></asp:Label><br />
                                                                                        <asp:TextBox ID="txtAPD_PARAM_DESCR" runat="server"  
                                                                                            ></asp:TextBox> 
                                                                                        <asp:ImageButton ID="btnSearchAPD_PARAM_DESCR" runat="server"
                                                                                            ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"
                                                                                              OnClick="btnSearchAPD_PARAM_DESCR_Click"></asp:ImageButton> 
                                                      
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPD_PARAM_DESCR" runat="server"
                                                            Text='<%# Bind("APD_PARAM_DESCR") %>'  ></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle Wrap="False"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                     <asp:Label ID="lblGradeH" runat="server" Text="From Date" CssClass="gridheader_text"  ></asp:Label><br />
                                                    <asp:TextBox ID="txtFromDT" runat="server" __designer:wfdid="w43"></asp:TextBox> 
                                                    <asp:ImageButton ID="btnSearchFromDT" OnClick="btnSearchFromDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top"  ></asp:ImageButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                      
                                            <asp:Label ID="lblFromDTH" runat="server" Text='<%# Bind("FROMDT") %>' ></asp:Label>
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>

                                                    <ItemStyle HorizontalAlign="Left" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <EditItemTemplate>
                                                        &nbsp;
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblToDateH" runat="server" Text="To Date" CssClass="gridheader_text" __designer:wfdid="w47"></asp:Label><br />
                                                <asp:TextBox ID="txtToDT" runat="server"  ></asp:TextBox> 
                                                <asp:ImageButton ID="btnSearchToDT" OnClick="btnSearchToDT_Click" runat="server" ImageUrl="~/Images/forum_search.gif" ImageAlign="Top" __designer:wfdid="w49"></asp:ImageButton> 
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToDTH" runat="server" Text='<%# Bind("TODT") %>'
                                                           ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblViewH" runat="server" Text="View"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblView" OnClick="lblView_Click" runat="server">View</asp:LinkButton>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AAM_ID" Visible="False">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAAM_ID" runat="server" Text='<%# Bind("AAM_ID") %>'
                                                            __designer:wfdid="w54"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Wheat" />
                                            <HeaderStyle CssClass="gridheader_pop"  HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                        &nbsp;<br />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_4" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_5" runat="server" type="hidden" value="=" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>


</asp:Content>

