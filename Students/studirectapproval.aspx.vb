Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Partial Class Students_studirectapproval
    Inherits System.Web.UI.Page
    Dim studClass As New studClass
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~/Students/studEnquiryNew.aspx?MainMnu_code=LjcF1BVVHbk=&datamode=Zo4HhpVNpXc=")
    End Sub

    Protected Sub btnapprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnapprove.Click
        Dim Encr_decrData As New Encryption64()
        Dim str_query = ""
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString()
        Dim eqs_id = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
        'str_query = "UPDATE ENQUIRY_SCREENING_S SET ENS_bAPPROVED='true' , ENS_bRETEST='false', ENS_APPROVEDBY='" & Session("sUsr_name") & "',ENS_REMARKS='', ENS_APPROVEDDATE=" & Date.Today & " WHERE ENS_EQS_ID='" & eqs_id & "'"
        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        Dim apr_id As String() = ddlApplication.SelectedValue.Split("|")

        'str_query = "UPDATE PROCESSFO_APPLICANT_S SET PRA_bCOMPLETED='true' where PRA_STG_ID='5' AND PRA_EQS_ID='" & eqs_id & "'"
        'SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        'str_query = "UPDATE ENQUIRY_SCHOOLPRIO_S SET EQS_APL_ID=" + apl_id(0) + ",EQS_APR_ID=" + ddlReason.SelectedValue.ToString + ",EQS_APL_DATE=GETDATE()," _
        '           & " EQS_APL_DISABLE='" + IIf(apl_id(1) = "1", "true", "false") + "' WHERE EQS_ID=" + eqs_id

        Dim ChkState As String = String.Empty
        If chkPrint.Checked Then
            ChkState = "1"
        Else
            ChkState = "0"
        End If



        str_query = "exec studDirectApproval " _
                                          & "'" + Format(Now, "yyyy-MM-dd") + "'," _
                                          & "'" + apr_id(1) + "'," _
                                          & ddlReason.SelectedValue.ToString + "," _
                                          & apr_id(0) + "," _
                                          & HiddenEQSID.Value + "," _
                                          & chkPrint.Checked

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

        If chkPrint.Checked = True Then

            Dim param As New Hashtable
            Dim EQS_IDs As String
            EQS_IDs = HiddenEQSID.Value + "|"
            param.Add("@IMG_BSU_ID", Session("sBsuid"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("@EQS_IDs", EQS_IDs)
            param.Add("@BSU_ID", Session("sBsuid"))
            param.Add("@BAL_APL_ID", apr_id(0))
            param.Add("CurrentDate", Format(Date.Parse(Now.Date), "dd/MMM/yyyy"))
            param.Add("UserName", Session("sUsr_name"))

            Dim rptClass As New rptClass
            With rptClass
                .crDatabase = System.Configuration.ConfigurationManager.AppSettings("OasisdbName")
                .reportParameters = param

                .reportPath = Server.MapPath("~\Students\Reports\RPT\rptOffer_letter.rpt")


            End With
            Session("rptClass") = rptClass
            Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")




            lblmessage.Text = "Record Saved Successfully"

            'Else
            '    lblmessage.Text = "Error while generating Offer Letter"
        Else
            updateenquiry(Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+")), "APR", 5, 5, True)
        End If


        Response.Redirect("~/Students/studEnquiryNew.aspx?MainMnu_code=LjcF1BVVHbk=&datamode=Zo4HhpVNpXc=")
    End Sub
    Public Sub updateenquiry(ByVal eqsid As String, ByVal eqsstatus As String, ByVal currentstatus As Integer, ByVal stgid As Integer, ByVal pra_completed As Boolean)
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString()
        Dim pParms(7) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EQS_ID", eqsid)
        pParms(1) = New SqlClient.SqlParameter("@EQS_STATUS", eqsstatus)
        pParms(2) = New SqlClient.SqlParameter("@CURRENT_STATUS", currentstatus)
        pParms(3) = New SqlClient.SqlParameter("@PRA_STG_ID", stgid)
        pParms(4) = New SqlClient.SqlParameter("@PRA_bCOMPLETED", pra_completed)
        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "StudentEnqChange", pParms)
        Dim returnflag As Integer = pParms(5).Value

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim Encr_decrData As New Encryption64()
            HiddenEQSID.Value = Encr_decrData.Decrypt(Request.QueryString("eqsid").Replace(" ", "+"))
            If studClass.GetBsuShowDocs(Session("sbsuid")) = True Then
                studClass.BindDocumentsList(lstNotSubmitted, "5", "false", HiddenEQSID.Value)
                studClass.BindDocumentsList(lstSubmitted, "5", "true", HiddenEQSID.Value)
                If lstNotSubmitted.Items.Count <> 0 Or lstSubmitted.Items.Count <> 0 Then
                    trDoc.Visible = True
                Else
                    trDoc.Visible = False
                End If
                lnkDocs.Visible = False
            Else
                hfURL.Value = "studJoinDocuments.aspx?eqsid=" + HiddenEQSID.Value
                lnkDocs.Visible = True
                trDoc.Visible = False
            End If

            BindApplicationDecision()
            BindApplicationReason(ddlApplication.Items(0).Value)
            'CODE ADDED BY LIJO 23/NOV/2008
            enable_disble_ChkPrint(ddlApplication.Items(0).Value)
            GetInfo()
        End If
    End Sub

    Protected Sub btnRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRight.Click
        studClass.UpdateDocListItems(lstNotSubmitted, lstSubmitted, "true", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub

    Protected Sub btnLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeft.Click
        studClass.UpdateDocListItems(lstSubmitted, lstNotSubmitted, "false", HiddenEQSID.Value)
        studClass.UpdateEnquiryStage("Complete", HiddenEQSID.Value)
    End Sub
    Sub BindApplicationDecision()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT APL_ID=CONVERT(VARCHAR(100),APL_ID)+'|'+CONVERT(VARCHAR(10),APL_DISABLE),APL_DESCR FROM APPLICATIONDECISION_M WHERE APL_BSU_ID='" + Session("SBSUID") + "' and apl_type='APR'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlApplication.DataSource = ds
        ddlApplication.DataTextField = "APL_DESCR"
        ddlApplication.DataValueField = "APL_ID"
        ddlApplication.DataBind()
    End Sub

    Sub BindApplicationReason(ByVal aplid As String)
        Dim apl_id As String() = aplid.Split("|")
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT APR_ID,APR_DESCR FROM APPLDESC_REASON_M WHERE APR_APL_ID=" + apl_id(0)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReason.DataSource = ds
        ddlReason.DataTextField = "APR_DESCR"
        ddlReason.DataValueField = "APR_ID"
        ddlReason.DataBind()
    End Sub

    Sub GetInfo()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(EQS_APL_ID,0),ISNULL(EQS_APL_DISABLE,'false'),ISNULL(EQS_APR_ID,0) FROM ENQUIRY_SCHOOLPRIO_S " _
                                & " WHERE EQS_ID=" + HiddenEQSID.Value
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim apl_id As String
        Dim apr_id As Integer
        While reader.Read
            apl_id = reader.GetValue(0).ToString + "|" + IIf(reader.GetBoolean(1) = True, "1", "0")
            apr_id = reader.GetValue(2)
        End While
        reader.Close()
        ddlApplication.ClearSelection()
        If apl_id <> "0|0" And apl_id <> "0|1" Then
            ddlApplication.Items.FindByValue(apl_id).Selected = True
            BindApplicationReason(apl_id)
        End If
        ddlReason.ClearSelection()
        If apr_id <> 0 Then
            ddlReason.Items.FindByValue(apr_id).Selected = True
        End If
    End Sub

    Protected Sub ddlApplication_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApplication.SelectedIndexChanged
        BindApplicationReason(ddlApplication.SelectedValue)

        enable_disble_ChkPrint(ddlApplication.SelectedValue)
    End Sub
    Sub enable_disble_ChkPrint(ByVal APL_ID As String)
        Dim APL_FLAG As String() = APL_ID.Split("|")
        If APL_FLAG(1) = 1 Then
            chkPrint.Checked = False
            chkPrint.Enabled = False
        Else
            chkPrint.Enabled = True
        End If
    End Sub
End Class
