﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Partial Class Students_ShowEnquiryStudents
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Dim smScriptManager As New ScriptManager
        'smScriptManager = Master.FindControl("ScriptManager1")

        'smScriptManager.EnablePartialRendering = False
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        If Page.IsPostBack = False Then
            Try
                ViewState("SearchMode") = Request.QueryString("id")
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim str_sql As String = ""

                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm").ToString, Session("sbsuid").ToString)
                GridBind()
                h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_6.Value = "LI__../Images/operations/like.gif"
                set_Menu_Img()

            Catch ex As Exception
                lblError.Text = "Request could not be processed"
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
        Else
            highlight_grid()
        End If
        If h_SelectedId.Value <> "Close" Then
            Response.Write("<script language='javascript'>" & vbCrLf & "function listen_window(){;" & vbCrLf)

            Response.Write("} </script>" & vbCrLf)
        End If
    End Sub
    Protected Sub lnkApplName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ViewState("action") = "select"
            Dim lblEnqNo As New Label
            Dim lblENQID As New Label
            Dim lbClose As New LinkButton
            Dim HF_SIBLING_TYPE As New HiddenField
            lbClose = sender

            lblEnqNo = sender.Parent.FindControl("lblEnqNo")
            lblENQID = sender.parent.findcontrol("lblEnqId")
            HF_SIBLING_TYPE = sender.parent.findcontrol("HF_SIBLING_TYPE")
            ' lblcode = gvGroup.SelectedRow.FindControl("lblCode")
            Dim l_Str_Msg As String = lblEnqNo.Text & "___" & lblENQID.Text & "___" & HF_SIBLING_TYPE.Value
            l_Str_Msg = l_Str_Msg.Replace("'", "\'")

            If (Not lblEnqNo Is Nothing) Then
                '   Response.Write(lblcode.Text)
                Response.Write("<script language='javascript'> function listen_window(){")
                ' Response.Write("window.returnValue = '" & l_Str_Msg & "';")
                Response.Write("var oArg = new Object();")
                Response.Write("oArg.Result ='" & l_Str_Msg & "';")
                Response.Write("var oWnd = GetRadWindow('" & l_Str_Msg & "');")
                Response.Write("oWnd.close(oArg);")
                ' Response.Write("window.close();")
                Response.Write("} </script>")
                h_SelectedId.Value = "Close"
            End If
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnEnqid_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnAcc_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnEnq_Date_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnAppl_Name_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub btnGrade_Search_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Protected Sub ddlgvShift_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub ddlgvStream_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        Try
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
    Protected Sub gvStudEnquiry_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudEnquiry.PageIndexChanging
        Try
            gvStudEnquiry.PageIndex = e.NewPageIndex
            GridBind()
        Catch ex As Exception
            lblError.Text = "Request could not be processed"
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
    Private Sub GridBind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = ""
            'if acd_dlinktpt is set to true then no need to check for enquiry transport allocation
            If ViewState("SearchMode") = "SIB" Then
                str_query = "SELECT  EQS_ID,EQS_APPLNO,ISNULL(EQS_SIBLING_TYPE,'ENQUIRY')EQS_SIBLING_TYPE,ISNULL(EQS_SIBLING_ID,EQS_EQM_ENQID)AS EQM_ENQID,EQM_ENQDATE, GRM_DISPLAY, SHF_DESCR,STM_DESCR, " _
                                & "APPL_NAME=isnull(EQM_APPLFIRSTNAME,'') + ' ' + ISNULL(EQM_APPLMIDNAME, '') + ' ' + ISNULL(EQM_APPLLASTNAME, '') ," _
                                & " ISNULL(EQS_REGNDATE,'01/Jan/1900') as EQS_REGNDATE,EQS_ACCNO " _
                                & "FROM   ENQUIRY_M AS A WITH (NOLOCK) INNER JOIN " _
                                & "ENQUIRY_SCHOOLPRIO_S AS B ON A.EQM_ENQID = B.EQS_EQM_ENQID INNER JOIN " _
                                & "GRADE_BSU_M AS C ON B.EQS_GRM_ID = C.GRM_ID INNER JOIN " _
                                & "SHIFTS_M AS D ON B.EQS_SHF_ID = D.SHF_ID INNER JOIN " _
                                & " STREAM_M AS E ON B.EQS_STM_ID = E.STM_ID " _
                                & " INNER JOIN dbo.vw_OSO_STUDAPPLSTAGES F ON B.EQS_ID=F.PRA_EQS_ID " _
                                & " INNER JOIN ACADEMICYEAR_D AS G ON B.EQS_ACD_ID=G.ACD_ID" _
                                & " WHERE B.EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND EQS_BSU_ID='" + Session("sbsuid") + "' " _
                                & " AND EQS_ID NOT IN(SELECT EQS_ID FROM ENQUIRY_SCHOOLPRIO_S WHERE EQS_STATUS='ENR' AND" _
                                & " EQS_BSU_ID<>'" + Session("sbsuid") + "')AND EQS_STATUS<>'ENR' AND" _
                                & " F.STG1COMP = 1 And F.STG2COMP = 1 And F.STG3COMP = 1" _
                                & " AND F.STG4COMP=1 AND F.STG5COMP=1 AND F.STG6COMP=1  " _
                                & " AND (EQS_bTPTREQD ='FALSE' OR EQS_TPTSTATUS='ACCEPT' OR " _
                                & " EQS_TPTSTATUS='WAIT' OR EQS_TPTSTATUS='REJECT' OR ACD_bDLINKTPT='TRUE')" _
                                & " AND EQS_STATUS<>'DEL' AND EQS_STATUS<>'CO' AND EQS_STATUS<>'CR'"

            Else
                str_query = "SELECT  EQS_ID,EQS_APPLNO,ISNULL(EQS_TWINS_TYPE,'ENQUIRY')EQS_SIBLING_TYPE,ISNULL(EQS_TWINS_ID,EQS_EQM_ENQID)AS EQM_ENQID,EQM_ENQDATE, GRM_DISPLAY, SHF_DESCR,STM_DESCR, " _
                                & "APPL_NAME=isnull(EQM_APPLFIRSTNAME,'') + ' ' + ISNULL(EQM_APPLMIDNAME, '') + ' ' + ISNULL(EQM_APPLLASTNAME, '') ," _
                                & " ISNULL(EQS_REGNDATE,'01/Jan/1900') as EQS_REGNDATE,EQS_ACCNO " _
                                & "FROM   ENQUIRY_M AS A WITH (NOLOCK) INNER JOIN " _
                                & "ENQUIRY_SCHOOLPRIO_S AS B WITH (NOLOCK) ON A.EQM_ENQID = B.EQS_EQM_ENQID INNER JOIN " _
                                & "GRADE_BSU_M AS C WITH (NOLOCK) ON B.EQS_GRM_ID = C.GRM_ID INNER JOIN " _
                                & "SHIFTS_M AS D WITH (NOLOCK) ON B.EQS_SHF_ID = D.SHF_ID INNER JOIN " _
                                & " STREAM_M AS E WITH (NOLOCK) ON B.EQS_STM_ID = E.STM_ID " _
                                & " INNER JOIN dbo.vw_OSO_STUDAPPLSTAGES F ON B.EQS_ID=F.PRA_EQS_ID " _
                                & " INNER JOIN ACADEMICYEAR_D AS G WITH (NOLOCK) ON B.EQS_ACD_ID=G.ACD_ID" _
                                & " WHERE B.EQS_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString + " AND EQS_BSU_ID='" + Session("sbsuid") + "' " _
                                & " AND EQS_ID NOT IN(SELECT EQS_ID FROM ENQUIRY_SCHOOLPRIO_S WHERE EQS_STATUS='ENR' AND" _
                                & " EQS_BSU_ID<>'" + Session("sbsuid") + "')AND EQS_STATUS<>'ENR' AND" _
                                & " F.STG1COMP = 1 And F.STG2COMP = 1 And F.STG3COMP = 1" _
                                & " AND F.STG4COMP=1 AND F.STG5COMP=1 AND F.STG6COMP=1  " _
                                & " AND (EQS_bTPTREQD ='FALSE' OR EQS_TPTSTATUS='ACCEPT' OR " _
                                & " EQS_TPTSTATUS='WAIT' OR EQS_TPTSTATUS='REJECT' OR ACD_bDLINKTPT='TRUE')" _
                                & " AND EQS_STATUS<>'DEL' AND EQS_STATUS<>'CO' AND EQS_STATUS<>'CR'"


            End If

        







            Dim strFilter As String = ""

            Dim strSidsearch As String()
            Dim strSearch As String

            Dim enqSearch As String = ""
            Dim accsearch As String = ""
            Dim nameSearch As String = ""
            Dim dateSearch As String = ""

            Dim ddlgvGrade As New DropDownList
            Dim ddlgvShift As New DropDownList
            Dim ddlgvStream As New DropDownList

            Dim selectedGrade As String = ""
            Dim selectedShift As String = ""
            Dim selectedStream As String = ""
            Dim txtSearch As New TextBox
            If gvStudEnquiry.Rows.Count > 0 Then



                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter = GetSearchString("EQS_APPLNO", txtSearch.Text, strSearch)
                enqSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtAccSearch")
                strSidsearch = h_Selected_menu_6.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("EQS_ACCNO", txtSearch.Text, strSearch)
                accsearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
                strSidsearch = h_Selected_menu_2.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("convert(varchar,eqm_enqdate,106)", txtSearch.Text.Replace("/", " "), strSearch)
                dateSearch = txtSearch.Text

                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
                strSidsearch = h_Selected_menu_1.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("grm_display", txtSearch.Text, strSearch)
                selectedGrade = txtSearch.Text

                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                If ddlgvShift.Text <> "ALL" Then
                    strFilter += " and shf_descr='" + ddlgvShift.Text + "'"
                    selectedShift = ddlgvShift.Text
                End If


                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")
                If ddlgvStream.Text <> "ALL" Then
                    strFilter += " and stm_descr='" + ddlgvStream.Text + "'"
                    selectedStream = ddlgvStream.Text
                End If


                txtSearch = New TextBox
                txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
                strSidsearch = h_Selected_menu_3.Value.Split("__")
                strSearch = strSidsearch(0)
                strFilter += GetSearchString("isnull(eqm_applfirstname,' ')+' '+isnull(eqm_applmidname,' ')+' '+isnull(eqm_appllastname,' ')", txtSearch.Text, strSearch)
                nameSearch = txtSearch.Text

                If strFilter.Trim <> "" Then
                    str_query = str_query + strFilter
                End If


            End If

            str_query += " ORDER BY EQM_ENQDATE DESC "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudEnquiry.DataSource = ds
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudEnquiry.DataBind()
                Dim columnCount As Integer = gvStudEnquiry.Rows(0).Cells.Count
                gvStudEnquiry.Rows(0).Cells.Clear()
                gvStudEnquiry.Rows(0).Cells.Add(New TableCell)
                gvStudEnquiry.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudEnquiry.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudEnquiry.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudEnquiry.DataBind()
            End If


            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqSearch")
            txtSearch.Text = enqSearch


            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtAccSearch")
            txtSearch.Text = accSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtEnqDate")
            txtSearch.Text = dateSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtApplName")
            txtSearch.Text = nameSearch

            txtSearch = New TextBox
            txtSearch = gvStudEnquiry.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = selectedGrade

            Dim dt As DataTable = ds.Tables(0)

            If gvStudEnquiry.Rows.Count > 0 Then



                ddlgvShift = gvStudEnquiry.HeaderRow.FindControl("ddlgvShift")
                ddlgvStream = gvStudEnquiry.HeaderRow.FindControl("ddlgvStream")

                Dim dr As DataRow

                ddlgvShift.Items.Clear()
                ddlgvShift.Items.Add("ALL")


                ddlgvStream.Items.Clear()
                ddlgvStream.Items.Add("ALL")

                For Each dr In dt.Rows

                    If dr.Item(0) Is DBNull.Value Then
                        Exit For
                    End If

                    With dr

                        ''check for duplicate values and add to dropdownlist 
                        If ddlgvShift.Items.FindByText(.Item(5)) Is Nothing Then
                            ddlgvShift.Items.Add(.Item(5))
                        End If
                        If ddlgvStream.Items.FindByText(.Item(6)) Is Nothing Then
                            ddlgvStream.Items.Add(.Item(6))
                        End If
                    End With
                Next

                If selectedGrade <> "" Then
                    ddlgvGrade.Text = selectedGrade
                End If


                If selectedShift <> "" Then
                    ddlgvShift.Text = selectedShift
                End If

                If selectedStream <> "" Then
                    ddlgvStream.Text = selectedStream
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_6.Value.Split("__")
        getid6(str_Sid_img(2))
    End Sub
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid7(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_7_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid6(Optional ByVal p_imgsrc As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudEnquiry.HeaderRow.FindControl("mnu_6_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getmenuid(Optional ByVal p_imgsrc As String = "", Optional ByVal imagename As String = "") As String
        If gvStudEnquiry.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try

                s = gvStudEnquiry.HeaderRow.FindControl(imagename)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function

    Sub highlight_grid()
        'For i As Integer = 0 To gvStudEnquiry.Rows.Count - 1
        '    Dim row As GridViewRow = gvStudEnquiry.Rows(i)
        '    Dim isSelect As Boolean = DirectCast(row.FindControl("chkSelect"), CheckBox).Checked
        '    If isSelect Then
        '        row.BackColor = Drawing.Color.FromName("#f6deb2")
        '    Else
        '        row.BackColor = Drawing.Color.Transparent
        '    End If
        'Next
    End Sub

End Class
