Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Imports System.Web
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Imports GemBox.Spreadsheet
Imports ResponseHelper
Partial Class Students_CorpSchoolComm
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            'Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_sql As String = ""
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("sUsr_name")

            'collect the url of the file to be redirected in view state

            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            'get the data mode from the query string to check if in add or edit mode 
            ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
            'get the menucode to confirm the user is accessing the valid page
            ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

            'check for the usr_name and the menucode are valid otherwise redirect to login page

            If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S135053" And ViewState("MainMnu_code") <> "S135210") Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else

                    Response.Redirect("~\noAccess.aspx")
                End If

            Else
                'calling pageright class to get the access rights


                ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))



                'disable the control based on the rights
                'use content if the page is comming from master page else use me.Page

                'disable the control buttons based on the rights

               
                If Session("USR_MIS") <> "2" Then

                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                End If
                BindAcad()

                PopulateTree()






            End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            'End Try

        End If
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim i As Integer
        Dim str As String = ""

        If Page.IsValid Then
            Dim Str_ValidateDate As String = String.Empty

            If Str_ValidateDate = "" Then
                If tvBusinessunit.CheckedNodes.Count < 1 Then
                    lblError.Text = "At least one school needs to be selected"
                ElseIf tvBusinessunit.CheckedNodes.Count > 0 Then
                    For Each node As TreeNode In tvBusinessunit.CheckedNodes
                        If node.Value.Length < 2 Then
                            lblError.Text = "Atleast one school needs to be selected"
                        Else
                            lblError.Text = ""
                            CallReport()
                        End If
                    Next
                End If


                With lstACAD
                    For i = 0 To .Items.Count - 1
                        If .Items(i).Selected = True Then
                            If str <> "" Then
                                str += ","
                            End If
                            str += "'" + .Items(i).Value + "'"
                        End If
                    Next
                End With

                If str = "" Then
                    lblError.Text = "Atleast one academic year needs to be selected"
                End If

            End If



        End If

    End Sub
    Function GetACDFilter() As String
        Dim i As Integer
        Dim str As String = ""
        With lstACAD
            For i = 0 To .Items.Count - 1
                If .Items(i).Selected = True Then
                    str += "<ID><ACD_ID>" + .Items(i).Value + "</ACD_ID></ID>"
                End If
            Next
        End With
        Return "<IDS>" + str + "</IDS>"
    End Function
    Sub CallReport()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim FromTo_Date As String = ""
        Dim i As Integer = 0

        Dim str_bsu_ids As New StringBuilder
        Dim bsu_xml As String = ""
        Dim ACD_XML As String = ""
        For Each node As TreeNode In tvBusinessunit.CheckedNodes
            If node.Value.Length > 2 Then
                str_bsu_ids.Append(node.Value)
                str_bsu_ids.Append("|")
                bsu_xml += "<ID><BSU_ID>" + node.Value + "</BSU_ID></ID>"
            End If
        Next
        ACD_XML = GetACDFilter()

        Dim param As New Hashtable
        'If ViewState("MainMnu_code") = "S135210" Then
        '    If bsu_xml <> "" Then
        '        param.Add("@BSU_XML", "<IDS>" + bsu_xml + "</IDS>")
        '    Else
        '        param.Add("@BSU_XML", DBNull.Value)
        '    End If

        '    param.Add("@ACD_XML", ACD_XML)
        'End If



        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        ''commenetd and added new line by nahyan for new gembox dll on 18Apr2016
        '' GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EBTV-6EQR-50E3-G5CI")
        SpreadsheetInfo.SetLicense("E20E-EPOY-4MUB-N63J")
        Dim ef As ExcelFile = New ExcelFile


        Dim str_query As String
        Dim lstrExportType As Integer
        



        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_XML", ACD_XML)
        pParms(1) = New SqlClient.SqlParameter("@BSU_XML", "<IDS>" + bsu_xml + "</IDS>")
        pParms(2) = New SqlClient.SqlParameter("@EXPORT_TYPE", ddlQuery.SelectedItem.Value)
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "QRY.STUDENT_LIST_MESSAGING", pParms)

        Dim dtEXCEL As New DataTable
        dtEXCEL = ds1.Tables(0)

        If dtEXCEL.Rows.Count > 0 Then
            Dim title = String.Empty
            If ddlQuery.SelectedItem.Text.Length > 30 Then
                title = ddlQuery.SelectedItem.Text.Substring(0, 30)
            Else
                title = ddlQuery.SelectedItem.Text
            End If

            Dim ws As ExcelWorksheet = ef.Worksheets.Add(title)
            ws.InsertDataTable(dtEXCEL, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
            '  ws.HeadersFooters.AlignWithMargins = True

            If ddlQuery.SelectedItem.Value = 11 Then
                Dim dtEXCEL2 As New DataTable
                dtEXCEL2 = ds1.Tables(1)

                If dtEXCEL2.Rows.Count > 0 Then
                    Dim ws2 As ExcelWorksheet = ef.Worksheets.Add(title)
                    ws2.InsertDataTable(dtEXCEL2, New InsertDataTableOptions("A1") With {.ColumnHeaders = True})
                    '  ws2.HeadersFooters.AlignWithMargins = True
                End If
            End If

            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "OASIS_DATA_EXPORT.xlsx")
            Dim cvVirtualPath = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
            Dim pathSave As String
            pathSave = "123" + "_" + Today.Now().ToString().Replace("/", "-").Replace(":", "-") + ".xlsx"
            ef.Save(cvVirtualPath & "StaffExport\" + pathSave)
            Dim path = cvVirtualPath & "\StaffExport\" + pathSave

            Dim bytes() As Byte = File.ReadAllBytes(path)
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = "application/octect-stream"
            Response.AddHeader("content-disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()

            'HttpContext.Current.Response.ContentType = "application/octect-stream"
            'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(path))
            'HttpContext.Current.Response.Clear()
            'HttpContext.Current.Response.WriteFile(path)
            'HttpContext.Current.Response.End()
        Else
            lblError.Text = "No Records To display with this filter condition....!!!"
            lblError.Focus()
        End If


       

    End Sub
  

    Private Sub PopulateRootLevel()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String
        tvBusinessunit.Nodes.Clear()
        str_Sql = "SELECT  0 AS BSU_ID,'All' AS BSU_NAME,  COUNT (*)  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        PopulateNodes(ds.Tables(0), tvBusinessunit.Nodes)
    End Sub

    Private Sub PopulateNodes(ByVal dt As DataTable, _
        ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("BSU_NAME").ToString()
            tn.Value = dr("BSU_ID").ToString()
            tn.Target = "_self"
            tn.NavigateUrl = "javascript:void(0)"
            'If tn.Value = Session("sBsuid") Then
            '    tn.Checked = True
            'End If
            nodes.Add(tn)
            'If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = (CInt(dr("childnodecount")) > 0)
        Next
    End Sub

    Private Sub PopulateTree() 'Generate Tree
        PopulateRootLevel()
        tvBusinessunit.DataBind()
        tvBusinessunit.CollapseAll()
    End Sub

    Protected Sub tvBusinessunit_TreeNodePopulate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles tvBusinessunit.TreeNodePopulate
        Dim str As String = e.Node.Value
        PopulateSubLevel(str, e.Node)
    End Sub

    Private Sub PopulateSubLevel(ByVal parentid As String, _
        ByVal parentNode As TreeNode)

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String

        If parentid = "0" Then

            str_Sql = "SELECT   BSU_ID,BSU_NAME,  0  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"



        Else
            str_Sql = "SELECT   BSU_ID,BSU_NAME,  0  AS childnodecount   FROM BUSINESSUNIT_M BSU WHERE (BUS_BSG_ID<>'4') order by bsu_name"
        End If



        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_MESSAGING")
        PopulateNodes(ds.Tables(0), parentNode.ChildNodes)



    End Sub
    Private Sub BindAcad()
        Dim str_conn As String = ConnectionManger.GetOASISConnection.ConnectionString
        'Dim strQuery As String = "SELECT BSU_SHORT_RESULT,BSU_ID FROM BUSINESSUNIT_M" _
        '                         & " WHERE  BSU_bASIANSCHOOL=1 AND BSU_TYPE<>'O' AND BSU_ID IN (SELECT USA_BSU_ID FROM USERACCESS_S WHERE USA_USR_ID='" + Session("sUsr_id").ToString + "') and bsu_short_result is not null"
        'Dim ds As DataSet

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetSchool_list_ACD_MESSAGING")


        lstACAD.DataSource = ds
        lstACAD.DataTextField = "ACY_DESCR"
        lstACAD.DataValueField = "ACY_ID"
        lstACAD.DataBind()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
End Class
