<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studLC_Entry.aspx.vb" Inherits="Students_studTCEntry_M" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" Runat="Server">


<script language="javascript" type="text/javascript">
 function getDate(val) 
          {     
            var sFeatures;
            sFeatures="dialogWidth: 227px; ";
            sFeatures+="dialogHeight: 252px; ";
            sFeatures+="help: no; ";
            sFeatures+="resizable: no; ";
            sFeatures+="scroll: no; ";
            sFeatures+="status: no; ";
            sFeatures+="unadorned: no; ";
            var NameandCode;
            var result;
            //alert("hello")
            result = window.showModalDialog("../Payroll/Calendar.aspx","", sFeatures)
            if (result=='' || result==undefined)
            {
            return false;
            }
            
            if (val==1)
            { 
           document.getElementById('<%=txtDocDate.ClientID %>').value='';
           document.getElementById('<%=txtDocDate.ClientID %>').value=result; 
         }  
         
          else if (val==2)
                {
                document.getElementById('<%=txtApplyDate.ClientID %>').value='';
                document.getElementById('<%=txtApplyDate.ClientID %>').value=result; 
                }
                else if (val==3)
                {
                 document.getElementById('<%=txtLast_Attend.ClientID %>').value='';
                document.getElementById('<%=txtLast_Attend.ClientID %>').value=result; 
                }
         else if (val==4)
                {
                document.getElementById('<%=txtLeave_Date.ClientID %>').value='';
                document.getElementById('<%=txtLeave_Date.ClientID %>').value=result; 
                }
                
                    else if (val==5)
                {
                document.getElementById('<%=txtIssue.ClientID %>').value='';
                document.getElementById('<%=txtIssue.ClientID %>').value=result; 
                }
              
         }  
 
 function Disable_Text(ddlId)
        {
            var ControlName = document.getElementById(ddlId.id);
        
            
            
         //   var myVal = document.getElementById('myValidatorClientID');
  
             if(ControlName.value == '-1')  //it depends on which value Selection do u want to hide or show your textbox 
             {
               
               document.getElementById('<%=txtOthers.ClientID %>').value=''; 
                document.getElementById('<%=txtOthers.ClientID %>').readOnly="";
              
               
             }
             else
             {
               document.getElementById('<%=txtOthers.ClientID %>').value=''; 
                 document.getElementById('<%=txtOthers.ClientID %>').readOnly="readonly";
               
             
                
                
             }
        } 



 
 
 </script>

      <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            Transfer Certificate(Out of UAE)
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">
    
    <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
        cellspacing="0">
        <tr>
            <td align="left" >
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM1" /><asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="error" EnableViewState="False"
                    ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                    ValidationGroup="groupM2" />
                <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label></td>
        </tr>
               <tr>
            <td >
               
                <table ID="tbTCSO" runat="server" border="0" CellPadding="0" CellSpacing="0"   width="100%">
                     <tr><td colspan="4" >Fields Marked with (<span class="text-danger font-small" >*</span>)
                are mandatory      
        </td></tr>  
                    <tr Class="title-bg-lite">
                        <td   colspan="4"  >
                                Student Details</td>
                    </tr>
                    
                    <tr>
                        <td align="left">  <span class="field-label">Student Id(Fees)</span></td>                       
                        <td align="left">  <asp:TextBox ID="txtStudID_Fee" runat="server"></asp:TextBox></td>
                        <td align="left">  <span class="field-label">Date of Join</span></td>                        
                        <td align="left"><asp:TextBox ID="txtDoJ" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr >
                        <td align="left">  <span class="field-label">Grade</span></td>
                 
                        <td align="left"> <asp:TextBox ID="txtGrade" runat="server" ></asp:TextBox></td>
                        <td align="left">  <span class="field-label">Section</span></td>
             
                        <td align="left"><asp:TextBox ID="txtSection" runat="server" ></asp:TextBox></td>
                    </tr>
                    <tr>
                      <td  align="left">  <span class="field-label">Name</span></td>
             
                        <td  colspan="3" align="left">
                            <asp:TextBox ID="txtName" runat="server" ></asp:TextBox>
                        </td>
                    
                    
                    </tr>
                    <tr>
                        <td align="left" >
                           <span class="field-label">   Father's Name</span><span class="text-danger font-small">*</span></td>
                      
                        <td align="left" colspan="3" >
                            <asp:TextBox ID="txtFName" runat="server" >
                            </asp:TextBox></td>
                    </tr>
                    <tr>
                     <td align="left">
                         <span class="field-label">  Subjects</span></td>
                    
                <td colspan="3" align="left">
                 <asp:TextBox ID="txtSubject" runat="server" ></asp:TextBox>                                                                    
                </td>
                    
                    
                    </tr>
                    <tr Class="title-bg-lite">                                   
                    <td   colspan="4"> LC Details </td>         
                    
                    </tr>
                    <tr >
                    <td align="left">  <span class="field-label">Doc Date</span><span class="text-danger font-small">*</span></td>
             
                        <td colspan="3" align="left" >
                         <asp:TextBox ID="txtDocDate" runat="server" ></asp:TextBox>
                           &nbsp;<asp:ImageButton ID="imgBtnDocDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="getDate(1);return false;" style="position: relative" />
                               <asp:RegularExpressionValidator ID="revDocDate" runat="server" ControlToValidate="txtDocDate"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Doc Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ForeColor="" Style="position: relative" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator
                                    ID="rfvDocDate" runat="server" ControlToValidate="txtDocDate" CssClass="error"
                                    Display="Dynamic" ErrorMessage="Doc Date can not be left empty" ForeColor=""
                                 ValidationGroup="groupM1">*</asp:RequiredFieldValidator>                                                                    
                        </td>
                    </tr>
                    <tr>
                        <td  align="left"><span class="field-label">Total Working Days</span></td>
             
                       <td  align="left"> <asp:TextBox ID="txtTotWork_Day" runat="server" >0</asp:TextBox>
                            <asp:CompareValidator ID="cvTotalDays" runat="server" ControlToValidate="txtTotWork_Day"
                                ErrorMessage="Total working days must be a numerical entry" Operator="DataTypeCheck"
                                Style="position: relative" Type="Integer" ValidationGroup="groupM1" CssClass="error" ForeColor="">*</asp:CompareValidator></td>
                       <td align="left"><span class="field-label">Days Present</span></td>
            
                       <td align="left"><asp:TextBox ID="txtDay_Present" runat="server">0</asp:TextBox>
                            <asp:CompareValidator ID="cvDayPresent" runat="server" ControlToValidate="txtDay_Present"
                                ErrorMessage="Days present must be a numerical entry" Operator="DataTypeCheck"
                                Style="position: relative" Type="Integer" ValidationGroup="groupM1" CssClass="error" ForeColor="">*</asp:CompareValidator></td>
                    </tr>
                    <tr >
                   <td align="left"  >
                      <span class="field-label"> LC Ref No</span></td>
              
                   <td align="left"><asp:TextBox ID="txtRefNo" runat="server" ></asp:TextBox></td>
                   <td align="left" > <span class="field-label">LCApply Date</span><span class="text-danger font-small">*</span></td>
  
                   <td align="left" ><asp:TextBox ID="txtApplyDate" runat="server" ></asp:TextBox>
                           &nbsp;<asp:ImageButton ID="imgBtnDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="getDate(2);return false;" />
                            <asp:RegularExpressionValidator ID="revDate" runat="server" ControlToValidate="txtApplyDate"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ForeColor="" Style="position: relative" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtApplyDate"
                                CssClass="error" Display="Dynamic" ErrorMessage=" Date field can not be left empty"
                                 Style="position: relative" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr >
                    <td  align="left"><span class="field-label">Last Attendance Date</span><span class="text-danger font-small">*</span></td>
                   
                    <td align="left"> <asp:TextBox ID="txtLast_Attend" runat="server" ></asp:TextBox>
                            &nbsp;&nbsp;
                    </td>
                  <td align="left"><span class="field-label">Leaving Date</span><span class="text-danger font-small">*</span></td>
   
                 <td align="left"><asp:TextBox ID="txtLeave_Date" runat="server" ></asp:TextBox>
                            &nbsp;<asp:ImageButton ID="imgbtnLeave_date" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="getDate(4);return false;" /><asp:RegularExpressionValidator ID="RegularExpressionValidator3"
                                    runat="server" ControlToValidate="txtLeave_Date" CssClass="error" Display="Dynamic"
                                    EnableViewState="False" ErrorMessage="Enter Leaving Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                    ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                            ID="rfvLeavingDate" runat="server" ControlToValidate="txtLeave_Date" CssClass="error"
                                            Display="Dynamic" ErrorMessage="Leaving Date can not be left empty" ForeColor=""
                                            ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                    </tr>
                    
                    <tr >
                        <td  align="left">
                          <span class="field-label">  Remarks</span></td>
          
                     <td align="left" ><asp:TextBox ID="txtRemarks" runat="server" MaxLength="255" ></asp:TextBox></td>
                     <td  align="left"><span class="field-label">Result</span></td>

                     <td align="left" > &nbsp;<asp:DropDownList ID="ddlResult" runat="server"><asp:ListItem>NA</asp:ListItem>
                             <asp:ListItem>Pass</asp:ListItem>
                             <asp:ListItem>Fail</asp:ListItem>
                         </asp:DropDownList></td>
                    </tr>
                    
                    <tr ID="r12" runat="server" >
                      <td  align="left"><span class="field-label">Promoted/Detained To Grade</span><span class="text-danger font-small">*</span></td>
         
                      <td  align="left">
                          &nbsp;<asp:DropDownList id="ddlPromoted" runat="server">
                          </asp:DropDownList></td>
                      <td align="left"><span class="field-label">Transfer Type</span><span class="text-danger font-small">*</span></td>
   
                      <td align="left"><asp:DropDownList ID="ddlTransferType" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr >
                    <td align="left"><span class="field-label">Transfered to School Name</span></td>
            
                    <td align="left"> <asp:DropDownList ID="ddlTrans_School" runat="server" >
                            </asp:DropDownList>
                            <br />
                            (If you choose other,please specify the school)<br />
                            <asp:TextBox ID="txtOthers" runat="server"></asp:TextBox></td>
                   <td align="left"><span class="field-label">Country</span></td>
     
                   <td align="left"><asp:DropDownList ID="ddlCountry" runat="server" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                   <td  align="left"><span class="field-label">Reason</span></td>
          
                   <td  align="left">  <asp:DropDownList ID="ddlReason" runat="server">
                            </asp:DropDownList></td>
                  <td align="left" ><span class="field-label">Transfer Zone</span></td>
               
                  <td  align="left"><asp:TextBox ID="txtTranZone" runat="server"></asp:TextBox></td> 
                    
                    </tr>
                    <tr>
                        <td align="left" style="width: 125px; height: 32px">
                          <span class="field-label">  Character &amp; Conduct</span></td>

                        <td align="left" >
                            <asp:TextBox ID="txtConduct" runat="server">Good</asp:TextBox></td>
                        <td align="left" >
                        </td>
                    
                        <td align="left" >
                        </td>
                    </tr>
                    <tr id="trPrint" runat="server">
                        <td align="left">
                          <span class="field-label">  LC Issue Date</span></td>
                      
                        <td align="left" >
                            <asp:TextBox ID="txtIssue" runat="server" >
                            </asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif"
                                OnClientClick="getDate(5);return false;" /><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtIssue"
                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Issue  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIssue"
                                CssClass="error" Display="Dynamic" ErrorMessage="Issue Date can not be left empty"
                                ForeColor="" ValidationGroup="groupM2">*</asp:RequiredFieldValidator></td>
                        <td align="center" colspan="2" >
                            <asp:Button ID="btnPrint" runat="server" CssClass="button" Text="Print" ValidationGroup="groupM2" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="groupM1" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                    Text="Cancel" />
                </td>
        </tr>
        <tr>
            <td  valign="bottom"><asp:HiddenField ID="hfACD_ID" runat="server" />
                <asp:HiddenField ID="hfTCM_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
                <asp:HiddenField ID="hfSTU_ID" runat="server" /><asp:HiddenField id="hfACY_DESCR"
                    runat="server"></asp:HiddenField><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFName"
                                CssClass="error" Display="None" ErrorMessage="please enter data in the field Father's name"
                                ForeColor="" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                &nbsp; &nbsp;
            </td>
        </tr>
    </table>

            </div>
        </div>
    </div>

</asp:Content>

