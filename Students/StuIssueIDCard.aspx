<%@ Page Title="" Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false"
    CodeFile="StuIssueIDCard.aspx.vb" Inherits="Students_StuIssueIDCard" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
.RadComboBoxDropDown .rcbItem>label, .RadComboBoxDropDown .rcbHovered>label, .RadComboBoxDropDown .rcbDisabled>label, .RadComboBoxDropDown .rcbLoading>label, .RadComboBoxDropDown .rcbCheckAllItems>label, .RadComboBoxDropDown .rcbCheckAllItemsHovered>label {
    display: inline;
    float: left;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border:0!important;
}

.RadComboBox_Default .rcbInner {
    padding:10px;
    border-color: #dee2da!important;
    border-radius: 6px!important;
    box-shadow: 1px 2px 5px rgba(0,0,0,0.1);
    width:80%;
    background-image: none !important;
    background-color:transparent !important;
}
.RadComboBox_Default .rcbInput {
    font-family:'Nunito', sans-serif !important;
}
.RadComboBox .rcbInput, .RadComboBox .rcbFakeInput {
    border: 0!important;
    box-shadow: none;
}
.RadComboBox_Default .rcbActionButton {
    border: 0px;
    background-image: none !important;
    height:100% !important;
    color:transparent !important;
    background-color:transparent !important;
}

    </style>
 
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <script type="text/javascript">
        function change_chk_stateg1(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;

                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CheckBar1") != -1) {
                    //                           alert(document.forms[0].elements[i].disabled)                    
                    if (document.forms[0].elements[i].disabled) {


                    } else {
                        document.forms[0].elements[i].checked = chk_state;
                        document.forms[0].elements[i].click();
                        //alert()
                    }

                }
            }
        }
    </script>

    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Issue ID Card"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table width="100%">
                    <tr>
                        <td class="matters" align="left"><span class="field-label">Business Unit</span>
                        </td>
                        <td class="matters" align="left">
                            <telerik:RadComboBox ID="ddlBUnit" RenderMode="Lightweight" runat="server" Skin="Default" AutoPostBack="True" Width="100%"
                                >
                            </telerik:RadComboBox>
                        </td>
                        <td class="matters" align="left"><span class="field-label">Academic Year</span>
                        </td>
                        <td class="matters" align="left">
                            <telerik:RadComboBox ID="ddl_acdYear"  RenderMode="Lightweight" runat="server" AutoPostBack="True" Width="100%">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left"><span class="field-label">Grade</span>
                        </td>
                        <td class="matters" align="left">
                            <telerik:RadComboBox ID="ddlgrade" RenderMode="Lightweight"  runat="server" AutoPostBack="True" Width="100%"  >
                            </telerik:RadComboBox>
                        </td>
                        <td class="matters" align="left"><span class="field-label">Approval Stages</span>
                        </td>
                        <td class="matters" align="left">
                            <telerik:RadComboBox ID="ddlStatus" RenderMode="Lightweight" runat="server" AutoPostBack="True" Width="100%">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="ALL" Value="0" />
                                    <telerik:RadComboBoxItem runat="server" Text="LOST" Value="1" />
                                    <telerik:RadComboBoxItem runat="server" Text="BLOCK" Value="2" />
                                    <telerik:RadComboBoxItem runat="server" Text="CANCELLED" Value="3" />
                                    <telerik:RadComboBoxItem runat="server" Text="ISSUED" Value="4" />
                                    <telerik:RadComboBoxItem runat="server" Text="UNBLOCKED" Value="5" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="left"><span class="field-label">Student No</span>
                        </td>
                        <td class="matters" align="left">
                            <asp:TextBox ID="txtStudNo" runat="server" EmptyMessage="Student Number">
                            </asp:TextBox>
                        </td>
                        <td class="matters" align="left"><span class="field-label">Student Name</span>
                        </td>
                        <td class="matters" align="left">
                            <asp:textbox ID="txtStudName" runat="server" EmptyMessage="Name"  >
                            </asp:textbox>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" colspan="4" align="center">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="center" colspan="4">
                            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" ValidationGroup="Save" />
                            <br />
                            <asp:Label ID="lblerror" runat="server" EnableViewState="False" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" class="matters" style="text-align:center">
                            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="100%">
                                <asp:GridView ID="gvLostIDCards" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                                     Width="100%" CssClass="table table-bordered table-row">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="h_SLCID" runat="server" Value='<%#Container.DataItem("SLC_ID")%>' />
                                                <asp:HiddenField ID="h_STU_ID" runat="server" Value='<%#Container.DataItem("SLC_STU_ID")%>' />
                                                <asp:HiddenField ID="h_bPaid" runat="server" Value='<%#Container.DataItem("PAIDSTATUS")%>' />
                                            </ItemTemplate>
                                               <ItemStyle HorizontalAlign="Center"   />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Barcode">
                                            <HeaderTemplate>
                                                Print
                                                <br />
                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_stateg1(this);"
                                                    ToolTip="Click here to select/deselect all rows" />

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <center>
                                        <asp:CheckBox ID="CheckBar1" runat="server" />
                                    </center>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center"   />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="STU_NO" HeaderText="STUDENT NO." />
                                        <asp:BoundField DataField="STU_NAME" HeaderText="STUDENT NAME" />
                                        <asp:BoundField DataField="SLC_LOGDT" HeaderText="DATE" />
                                        <asp:BoundField DataField="SLC_REMARKS" HeaderText="REMARKS" />
                                        <asp:BoundField DataField="SLC_AMOUNT" HeaderText="RATE" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="PAIDSTATUS" HeaderText="PAID" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:HiddenField ID="h_SLCSTATUS" runat="server" Value='<%#Container.DataItem("SLC_STATUS")%>' />
                                                <telerik:RadComboBox ID="dlAction" RenderMode="Lightweight" runat="server" AutoPostBack="False" SelectedValue='<%# Bind("SLC_STATUS") %>' Width="100%">
                                                    <Items>
                                                        <telerik:RadComboBoxItem runat="server" Text="LOST" Value="1" />
                                                        <telerik:RadComboBoxItem runat="server" Text="BLOCK" Value="2" />
                                                        <telerik:RadComboBoxItem runat="server" Text="CANCEL" Value="3" />
                                                        <telerik:RadComboBoxItem runat="server" Text="ISSUE" Value="4" />
                                                        <telerik:RadComboBoxItem runat="server" Text="UNBLOCK" Value="5" />
                                                    </Items>
                                                </telerik:RadComboBox>
                                            </ItemTemplate>
                                               <ItemStyle HorizontalAlign="Center"   />
                                        </asp:TemplateField>
                                    </Columns>
                                    <%--   <HeaderStyle Height="30px" CssClass="gridheader_new" />
                        <RowStyle CssClass="griditem" Height="25px" />
                        <SelectedRowStyle CssClass="griditem" />
                        <AlternatingRowStyle CssClass="griditem_alternative" />  --%>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" align="center" colspan="4" >&nbsp;
                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="button" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="HiddenShowFlag" runat="server" />

                <script type="text/javascript">
                    if (document.getElementById("<%=HiddenShowFlag.ClientID %>").value == '1') {
                        document.getElementById("<%=HiddenShowFlag.ClientID %>").value = 0
                        var sFeatures;
                        sFeatures = "dialogWidth: 800px; ";
                        sFeatures += "dialogHeight: 600px; ";

                        sFeatures += "help: no; ";
                        sFeatures += "resizable: no; ";
                        sFeatures += "scroll: yes; ";
                        sFeatures += "status: no; ";
                        sFeatures += "unadorned: no; ";

                        var qstng = "Type=STUDENT&Checked=0";
                        qstng += "&ayshow=0"
                        var result;
                        //result = window.showModalDialog('Barcode/BarcodeStudentNumber.aspx?Type=STUDENT' ,"", sFeatures);
                        result = window.open('../CardPrinter/NewApprovedCard/Barcode.aspx?' + qstng)


                    }


                </script>
            </div>
        </div>
    </div>
</asp:Content>
