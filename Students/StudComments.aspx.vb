Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports system
Partial Class Students_StudComments
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")


        'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim strSQL = "SELECT STM_ENR_COMMENTS From Student_M A INNER JOIN Student_MISC B ON A.STU_ID=B.STM_STU_ID WHERE STU_NO='" & Request.QueryString("eqsid") & "'"
        'Dim dsSQL As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        'txtComments.Text = dsSQL.Tables(0).Rows(0).Item("STM_ENR_COMMENTS").ToString().Trim()
       
        If Not IsPostBack Then
            BindGrid()
        End If



        'Catch ex As Exception
        '    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
        'End Try

    End Sub

    
    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If txtComments.Text <> "" Then
            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
            Dim transaction As SqlTransaction
            connection.Open()
            transaction = connection.BeginTransaction()
            lblMessage.Text = ""
            Try
                Dim pParms(12) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STM_STU_ID", Request.QueryString("eqsid"))
                pParms(1) = New SqlClient.SqlParameter("@STM_ENR_COMMENTS", txtComments.Text)
                pParms(2) = New SqlClient.SqlParameter("@EQS_ID", Request.QueryString("ENQID"))
                pParms(3) = New SqlClient.SqlParameter("@MSC_BSU_ID", Session("sBSUID"))


                lblMessage.Text = SqlHelper.ExecuteScalar(transaction, CommandType.StoredProcedure, "INSERT_Student_MISC", pParms)
                transaction.Commit()
                BindGrid()
                txtComments.Text = ""

            Catch ex As Exception
                transaction.Rollback()

            Finally
                connection.Close()

            End Try

        Else
            lblMessage.Text = "Please enter comments"
        End If

    End Sub


    Public Sub BindGrid()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = " SELECT STM_ENR_COMMENTS,STM_ENTRY_DATE, " & _
                        " '<span style=''color: red;''>Hide</span>' as hide, " & _
                        "  (substring(STM_ENR_COMMENTS,0,50)+ '</br><span style=''color: red;''> more... </span>')tempview1 " & _
                        " From Student_M A INNER JOIN Student_MISC B ON A.STU_ID=B.STM_STU_ID " & _
                        " WHERE STU_NO='" & Request.QueryString("eqsid") & "'"
        '" order by STM_LOG_ID desc "
        Dim str_query2 = " SELECT STM_ENR_COMMENTS,STM_ENTRY_DATE, " & _
                        " '<span style=''color: red;''>Hide</span>' as hide, " & _
                        "  (substring(STM_ENR_COMMENTS,0,50)+ '</br><span style=''color: red;''> more... </span>')tempview1 " & _
                        " From Enquiry_SchoolPrio_S A INNER JOIN Student_MISC B ON A.EQS_ID=B.STM_STU_ID AND MSC_BSU_ID='" & Session("sBSUID") & "'" & _
                        " WHERE EQS_ID='" & Request.QueryString("ENQID") & "'"


        str_query = str_query + " UNION " + str_query2 + "order by STM_ENTRY_DATE desc"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        GridInfo.DataSource = ds
        GridInfo.DataBind()


    End Sub

    Protected Sub GridInfo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridInfo.PageIndexChanging

        GridInfo.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub
End Class
