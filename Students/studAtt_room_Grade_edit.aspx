<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studAtt_room_Grade_edit.aspx.vb" Inherits="Students_studAtt_room_Grade_edit" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <script language="javascript" type="text/javascript">
        function CheckBoxCheck(obj) {


            var grid = obj.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                var currentid = inputs[i].id;
                if (inputs[i].type == "checkbox" && currentid.indexOf("chkVAtt") != -1) {
                    if (obj.checked && inputs[i] != obj && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }

        }

        function validateSave() {

            var gvchk = false
            var Othchk = false
            var atLeast = 1

            var CHK = document.getElementById("<%=chkOthGrade.ClientID%>");

            var checkbox = CHK.getElementsByTagName("input");

            var counter = 0;

            for (var i = 0; i < checkbox.length; i++) {

                if (checkbox[i].checked) {

                    Othchk = true;

                }

            }






            for (i = 0; i < chkPer.length; i++) {
                if (document.getElementById(chkPer[i]).checked == true) {
                    gvchk = true;
                }
            }

            if (gvchk == true && Othchk == true) {
                document.getElementById("<%=lblError.ClientID %>").innerHTML = "";
                return true;
            }
            else {
                document.getElementById("<%=lblError.ClientID %>").innerHTML = "Please select Grade(s) & Period(s) to be saved !!!";
                return false;
            }


        }




        function change_chk_state(chkThis) {

            var chk_state = chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkPer") != -1) {

                    if (document.forms[0].elements[i].disabled == false) {
                        document.forms[0].elements[i].checked = chk_state;

                    }
                }
            }
        }

        function chk_check() {
            var j = 0;
            for (i = 0; i < chkPer.length; i++) {
                if (document.getElementById(chkPer[i]).checked == true) {
                    j = i;
                }
            }

            for (i = 0; i < j + 1; i++) {
                document.getElementById(chkPer[i]).checked = true

            }

        }

    </script>


    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            <asp:Literal ID="ltHeader" runat="server" Text="Grade Setting"></asp:Literal>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="tbl_AddGroup" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <span style="display: block; left: 0px; float: left">
                                <div align="left">
                                    <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                                </div>

                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters" valign="bottom">
                            <table width="100%">
                                <tr id="vid" runat="server">
                                    <td align="left" colspan="2">Grade :
                                        <asp:Literal ID="ltGrade" runat="server"></asp:Literal></td>

                                </tr>
                                <tr id="aid" runat="server">
                                    <td align="left"><span class="field-label">Grades </span></td>
                                    <td align="left">
                                        <asp:CheckBoxList ID="chkOthGrade" runat="server" RepeatColumns="22" RepeatDirection="Horizontal">
                                        </asp:CheckBoxList></td>
                                </tr>
                                <tr class="title-bg-small">
                                    <td align="left" class="matters" colspan="2">Period List
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:GridView ID="gvDoc" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-row"
                                            EmptyDataText="No record added yet" Width="100%"
                                            OnPreRender="gvDoc_PreRender" EnableModelValidation="True">
                                            <RowStyle CssClass="griditem" />
                                            <EmptyDataRowStyle CssClass="gridheader" HorizontalAlign="Center" Wrap="True" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Period_Id" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPeriod_id" runat="server" Text='<%# Bind("PERIOD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Select">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" onclick="javascript:change_chk_state(this);" runat="server" __designer:wfdid="w16" ToolTip="Click here to select/deselect all rows"></asp:CheckBox>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkPer" Checked='<%# Bind("flag") %>' onclick="javascript:chk_check();" runat="server" __designer:wfdid="w15"></asp:CheckBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Periods">
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPeriod_Descr" runat="server" Text='<%# Bind("PERIOD_DESCR") %>' __designer:wfdid="w14"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Schedule">
                                                    <EditItemTemplate>
                                                        &nbsp; 
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        &nbsp;<asp:TextBox ID="txtRAPG_TIME" runat="server" Text='<%# bind("RAPG_TIME") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vertical Attendance">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkVAtt" runat="server"
                                                            onclick="javascript:CheckBoxCheck(this);" Checked='<%# Bind("RAPG_bVERTICAL_ATT") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="Khaki" />
                                            <HeaderStyle CssClass="gridheader_new" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="matters" valign="bottom" align="center">
                            <asp:Button ID="btnSave"
                                runat="server" CssClass="button" OnClick="btnSave_Click" Text="Save" ValidationGroup="groupM1" OnClientClick="return validateSave();" CausesValidation="False" /><asp:Button
                                    ID="btnCancel" runat="server" CausesValidation="False" CssClass="button" OnClick="btnCancel_Click"
                                    Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
</asp:Content>

