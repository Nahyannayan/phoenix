<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studEnroll_M.aspx.vb" Inherits="Students_studEnroll_M" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <style>
        input {
            vertical-align :middle !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        var color = '';
        function highlight(obj) {
            var rowObject = getParentRow(obj);
            var parentTable = document.getElementById("<%=gvStudEnquiry.ClientID %>");
            if (color == '') {
                color = getRowColor();
            }
            if (obj.checked) {
                rowObject.style.backgroundColor = '#f6deb2';
            }
            else {
                rowObject.style.backgroundColor = '';
                color = '';
            }
            // private method

            function getRowColor() {
                if (rowObject.style.backgroundColor == '') return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }
        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }


        function change_chk_state(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("chkSelect") != -1) {
                    //if (document.forms[0].elements[i].type=='checkbox' )
                    //if (document.forms[0].elements[i].name.search(/chkSelect/)>0) also works
                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();//fire the click event of the child element
                }
            }
        }






        function getDate(val) {
            var sFeatures;
            sFeatures = "dialogWidth: 227px; ";
            sFeatures += "dialogHeight: 252px; ";
            sFeatures += "help: no; ";
            sFeatures += "resizable: no; ";
            sFeatures += "scroll: no; ";
            sFeatures += "status: no; ";
            sFeatures += "unadorned: no; ";
            var NameandCode;
            var result;
            var nofuture = "../Accounts/calendar.aspx?nofuture=yes";
            if (val == 0) {
                result = window.showModalDialog(nofuture, "", sFeatures)

            }
            else {
                result = window.showModalDialog("../Accounts/calendar.aspx", "", sFeatures)
            }
            if (result == '' || result == undefined) {
                return false;
            }


            if (val == 1) {
                document.getElementById('<%=txtDoj.ClientID %>').value = result;
                document.getElementById('<%=txtMDoj.ClientID %>').value = result;
            }
            else if (val == 2) {
                document.getElementById('<%=txtMDoj.ClientID %>').value = result;
    }



    return false;
}


function MoveText() {
    
    document.getElementById('<%=txtMDoj.ClientID %>').value = document.getElementById('<%=txtDoj.ClientID %>').value;
        return false;
    }

    function confirm_enroll() {

        if (confirm("You are about to enroll these selected enquiries.Do you want to proceed?") == true)
            return true;
        else
            return false;

    }

    function POPUP(eqsid) {
        var sFeatures;
        sFeatures = "dialogWidth: 600px; ";
        sFeatures += "dialogHeight: 400px; ";
        sFeatures += "help: no; ";
        sFeatures += "resizable: no; ";
        sFeatures += "scroll: yes; ";
        sFeatures += "status: no; ";
        sFeatures += "unadorned: no; ";
        var NameandCode;
        var result;
        var url;
        url = '../Students/studEnroll_Popup.aspx?id=' + eqsid;
        var oWnd = radopen(url, "pop_stud");
        //                if (mode=='SE')
        //                {          
     //   result = window.showModalDialog(url, "", sFeatures);

        //                    if (result=='' || result==undefined)
        //                    {    return false; 
        //                    }   
        //                 NameandCode = result.split('___');
        //                

        //               
        //               }
    }
    function autoSizeWithCalendar(oWindow) {
        var iframe = oWindow.get_contentFrame();
        var body = iframe.contentWindow.document.body;

        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();
    }
    </script>

    <telerik:RadWindowManager ID="RadWindowManager1" ShowContentDuringLoad="false" VisibleStatusbar="false" 
        ReloadOnShow="true" runat="server" EnableShadow="true"  >
        <Windows>
            <telerik:RadWindow ID="pop_stud" runat="server" Behaviors="Close,Move" 
               OnClientAutoSizeEnd="autoSizeWithCalendar" Width="700px" Height="620px" >
            </telerik:RadWindow>
        </Windows>          
    </telerik:RadWindowManager>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>

            Enrollment
        </div>
        <div class="card-body">
            <div class="table-responsive m-auto">

                <table id="tbl_ShowScreen" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
                                ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
                                ValidationGroup="groupM1"  Style="display: inline"  />

                        </td>

                    </tr>

                    <tr>
                        <td>
                            <table align="center" cellpadding="0" cellspacing="0" width="100%">

                                <tr id="TR1" runat="server">
                                    <td align="left"><span class="field-label">Select Academic Year</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlAcademicYear" SkinID="smallcmb" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left"><span class="field-label">Payment Plan</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlPayPlan" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Date of Joining</span></td>

                                    <td align="left">
                                       <%-- <input id="txtDoj" type="text" runat="server"  onchange="MoveText();return false;" />--%>
                                      <asp:TextBox ID="txtDoj" runat="server" onchange="MoveText();return false;"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="imgBtnEnqDate" TargetControlID="txtDoj">
                                        </ajaxToolkit:CalendarExtender>
                                        <asp:ImageButton ID="imgBtnEnqDate" runat="server" ImageUrl="~/Images/calendar.gif"
                                            /><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDoj"
                                                Display="Dynamic" ErrorMessage="Enter the Joining Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator3" runat="server" ControlToValidate="txtDoj"
                                                    CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Date of join entered is not a valid date"
                                                    ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>

                                        <asp:RequiredFieldValidator ID="rfDoj" runat="server" ErrorMessage="Please enter the field Date of join" ControlToValidate="txtDoj" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>

                                    </td>


                                    <td align="left"><span class="field-label">Ministry Date of Joining</span></td>

                                    <td align="left">
                                       <%-- <input id="txtMDoj" type="text" runat="server" />--%>
                                        <asp:TextBox ID="txtMDoj" runat="server"></asp:TextBox>
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.gif" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            PopupButtonID="ImageButton1" TargetControlID="txtMDoj">
                                        </ajaxToolkit:CalendarExtender>

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMDoj"
                                            Display="Dynamic" ErrorMessage="Enter the Ministry joining Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtMDoj"
                                                CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Ministry date of join entered is not a valid date"
                                                ForeColor="" ValidationGroup="groupM1">*</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfMdoj" runat="server" ErrorMessage="Please enter the field Ministry  Date of join" ControlToValidate="txtMDoj" Display="None" ValidationGroup="groupM1"></asp:RequiredFieldValidator>

                                    </td>

                                </tr>

                                <tr>
                                    <td align="left"><span class="field-label">Transfer Type</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlTranType" runat="server">
                                        </asp:DropDownList>
                                    </td>

                                    <td align="left"><span class="field-label">Previous School Result</span></td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddlJoinResult" runat="server">
                                            <asp:ListItem Value="NEW">NEW</asp:ListItem>
                                            <asp:ListItem Value="PASS">PASS</asp:ListItem>
                                            <asp:ListItem Value="FAIL">FAIL</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                </tr>

                                <tr>
                                    <td align="center" colspan="4" valign="top">

                                        <asp:GridView ID="gvStudEnquiry" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-row" EmptyDataText="No Records Found"
                                            HeaderStyle-Height="30" PageSize="20">
                                            <Columns>

                                                <asp:TemplateField HeaderText="Available">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:highlight(this);" />
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        Select<br />
                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:change_chk_state(this);"
                                                            ToolTip="Click here to select/deselect all rows" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqsId" runat="server" Text='<%# Bind("Eqs_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="EQS_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRegDate" runat="server" Text='<%# Bind("EQS_REGNDATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="ENQ_ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqId" runat="server" Text='<%# Bind("EQM_ENQID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Enq No">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqHeader" runat="server" CssClass="gridheader_text" Text="Enq No"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnqid_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnqid_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqNo" runat="server" Text='<%# Bind("eqs_applno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>




                                                <asp:TemplateField HeaderText="ACC No">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblAcc" runat="server" CssClass="gridheader_text" Text="Acc No"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtAccSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnAcc_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnAcc_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccNo" runat="server" Text='<%# Bind("eqs_accno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>




                                                <asp:TemplateField HeaderText="Enq Date">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" CssClass="gridheader_text" Text="Enq.Date"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtEnqDate" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnEnq_Date" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnEnq_Date_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEnqDate" runat="server" Text='<%# Bind("eqm_enqdate", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Grade">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblH12" runat="server" CssClass="gridheader_text" Text="Grade"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtGrade" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="btnGrade_Search" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnGrade_Search_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrade" runat="server" Text='<%# Bind("grm_display") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Shift">
                                                    <HeaderTemplate>
                                                        Shift<br />
                                                        <asp:DropDownList ID="ddlgvShift" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvShift_SelectedIndexChanged" >
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("shf_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stream">
                                                    <HeaderTemplate>
                                                        Stream<br />
                                                        <asp:DropDownList ID="ddlgvStream" runat="server" AutoPostBack="True" CssClass="listbox"
                                                            OnSelectedIndexChanged="ddlgvStream_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStream" runat="server" Text='<%# Bind("stm_descr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Applicant Name">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblApplName" runat="server" CssClass="gridheader_text" Text="Applicant Name"></asp:Label>
                                                        <br />
                                                        <asp:TextBox ID="txtApplName" runat="server" ></asp:TextBox>
                                                        <asp:ImageButton ID="btnAppl_Name" runat="server" ImageAlign="Top" ImageUrl="~/Images/forum_search.gif" OnClick="btnAppl_Name_Click" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkApplName" CommandName="Select" OnClientClick="<%# &quot;POPUP('&quot; & Container.DataItem(&quot;EQM_ENQID&quot;) & &quot;');return false;&quot; %>" OnClick="lnkApplName_Click" runat="server" Text='<%# Bind("appl_name") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <HeaderStyle CssClass="gridheader_pop" />
                                            <RowStyle CssClass="griditem" />
                                            <SelectedRowStyle CssClass="Green" />
                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>


                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnEnroll" runat="server" Text="Enroll" CssClass="button" ValidationGroup="groupM1" TabIndex="4" OnClientClick="return confirm_enroll();" />
                                    </td>


                                </tr>
                            </table>
                            <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_6" runat="server" type="hidden" value="=" />
                            <input id="h_Selected_menu_7" runat="server" type="hidden" value="=" /><input id="h_Selected_menu_2"
                                runat="server" type="hidden" value="=" /><input id="h_Selected_menu_3" runat="server"
                                    type="hidden" value="=" /></td>
                    </tr>


                </table>

            </div>
        </div>
    </div>
</asp:Content>

