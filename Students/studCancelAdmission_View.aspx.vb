Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Collections
Imports System.Reflection
Imports System.Math
Partial Class Students_studCancelAdmission_View
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Dim smScriptManager As New ScriptManager
    '    smScriptManager = Master.FindControl("ScriptManager1")

    '    smScriptManager.EnablePartialRendering = False
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ' Dim STR As String = Encr_decrData.Decrypt("wr92h2HbWq4=")

        If Page.IsPostBack = False Then


            If isPageExpired() Then
                Response.Redirect("expired.htm")
            Else
                Session("TimeStamp") = Now.ToString
                ViewState("TimeStamp") = Now.ToString
            End If

            Try

                Dim str_sql As String = ""
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")

                'collect the url of the file to be redirected in view state

                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If

                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))

                'check for the usr_name and the menucode are valid otherwise redirect to login page

                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S100300") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else

                        Response.Redirect("~\noAccess.aspx")
                    End If

                Else
                    'calling pageright class to get the access rights


                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))

                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page

                    'disable the control buttons based on the rights
                    Call AccessRight.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    ViewState("datamode") = "add"

                    h_Selected_menu_1.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                    h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                    set_Menu_Img()
                    ddlAcademicYear = studClass.PopulateAcademicYear(ddlAcademicYear, Session("clm"), Session("sbsuid"))
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            End Try
            GridBind()


        Else


        End If
    End Sub
    Protected Sub btnSearchStuNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub btnSearchStuName_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Sub ddlgvSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub

#Region "Private Methods"



    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub set_Menu_Img()
        Dim str_Sid_img() As String
        str_Sid_img = h_Selected_menu_1.Value.Split("__")
        getid1(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_2.Value.Split("__")
        getid2(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_3.Value.Split("__")
        getid3(str_Sid_img(2))
        str_Sid_img = h_Selected_menu_4.Value.Split("__")
        getid4(str_Sid_img(2))

    End Sub
    
    Public Function getid1(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_1_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid2(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_2_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid3(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_3_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Public Function getid4(Optional ByVal p_imgsrc As String = "") As String
        If gvStudTPT.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Try
                s = gvStudTPT.HeaderRow.FindControl("mnu_4_img")
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function
    Private Sub GridBind()
        Dim str_conn = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "SELECT STU_ID,STU_NO,STU_NAME=(ISNULL(STU_FIRSTNAME,'')+' ' + ISNULL(STU_MIDNAME,'')+''+ISNULL(STU_LASTNAME,''))," _
                   & " ISNULL(GRM_DISPLAY,'') AS GRM_DISPLAY,ISNULL(SCT_DESCR,'') AS SCT_DESCR " _
                   & " FROM STUDENT_M AS A INNER JOIN GRADE_BSU_M AS B ON A.STU_GRM_ID=B.GRM_ID" _
                   & " LEFT OUTER JOIN SECTION_M AS C ON A.STU_SCT_ID=C.SCT_ID AND C.SCT_GRM_ID=A.STU_GRM_ID " _
                   & " WHERE STU_ACD_ID = " + ddlAcademicYear.SelectedValue.ToString _
                   & " AND STU_LEAVEDATE IS NULL " _
                   & " AND STU_CURRSTATUS='EN'"

        '& " AND CONVERT(datetime, ISNULL(STU_LEAVEDATE,'2100-01-01')) > CONVERT(datetime,GETDATE()) " _
        '& " AND STU_CURRSTATUS='EN'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim strFilter As String = ""
        Dim strSidsearch As String()
        Dim strSearch As String
        Dim stuNameSearch As String = ""
        Dim stunoSearch As String = ""
        Dim applySearch As String = ""
        Dim issueSearch As String = ""
        Dim pSearch As String = ""
        Dim dSearch As String = ""

        Dim ddlgvGrade As New DropDownList
        Dim ddlgvSection As New DropDownList
     
        Dim selectedGrade As String = ""
        Dim selectedSection As String = ""
        Dim selectedPick As String = ""
        Dim selectedDrop As String = ""

        Dim txtSearch As New TextBox

        If gvStudTPT.Rows.Count > 0 Then


            txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuNo")
            strSidsearch = h_Selected_menu_1.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter = GetSearchString("STU_NO", txtSearch.Text, strSearch)
            stunoSearch = txtSearch.Text

            txtSearch = New TextBox
            txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuName")
            strSidsearch = h_Selected_menu_2.Value.Split("__")
            strSearch = strSidsearch(0)
            strFilter += GetSearchString("ISNULL(STU_FIRSTNAME,'')+' '+ISNULL(STU_MIDNAME,'')+' '+ISNULL(STU_LASTNAME,' ')", txtSearch.Text, strSearch)
            stuNameSearch = txtSearch.Text


            ddlgvGrade = gvStudTPT.HeaderRow.FindControl("ddlgvGrade")
            If ddlgvGrade.Text <> "ALL" And ddlgvGrade.Text.Trim <> "" Then
                strFilter += " And grm_display='" + ddlgvGrade.Text + "'"
                selectedGrade = ddlgvGrade.Text
            End If

            ddlgvSection = gvStudTPT.HeaderRow.FindControl("ddlgvSection")
            If ddlgvSection.Text <> "ALL" And ddlgvSection.Text.Trim <> "" Then
                strFilter += " and sct_descr='" + ddlgvSection.Text + "'"
                selectedSection = ddlgvSection.Text
            End If

            If strFilter.Trim <> "" Then
                str_query = str_query + strFilter
            End If

        End If


        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvStudTPT.DataSource = ds
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvStudTPT.DataBind()
            Dim columnCount As Integer = gvStudTPT.Rows(0).Cells.Count
            gvStudTPT.Rows(0).Cells.Clear()
            gvStudTPT.Rows(0).Cells.Add(New TableCell)
            gvStudTPT.Rows(0).Cells(0).ColumnSpan = columnCount
            gvStudTPT.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvStudTPT.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        Else
            gvStudTPT.DataBind()
        End If


        Dim dt As DataTable = ds.Tables(0)

        txtSearch = New TextBox
        txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuNo")
        txtSearch.Text = stunoSearch

        txtSearch = New TextBox
        txtSearch = gvStudTPT.HeaderRow.FindControl("txtStuName")
        txtSearch.Text = stuNameSearch

        If gvStudTPT.Rows.Count > 0 Then
            ddlgvGrade = gvStudTPT.HeaderRow.FindControl("ddlgvGrade")
            ddlgvSection = gvStudTPT.HeaderRow.FindControl("ddlgvSection")
          
            Dim dr As DataRow

            ddlgvGrade.Items.Clear()
            ddlgvGrade.Items.Add("ALL")


            ddlgvSection.Items.Clear()
            ddlgvSection.Items.Add("ALL")

            For Each dr In dt.Rows
                If dr.Item(0) Is DBNull.Value Then
                    Exit For
                End If
                With dr
                    ''check for duplicate values and add to dropdownlist 
                    If ddlgvGrade.Items.FindByText(.Item(3)) Is Nothing Then
                        ddlgvGrade.Items.Add(.Item(3))
                    End If
                    If ddlgvSection.Items.FindByText(.Item(4)) Is Nothing Then
                        ddlgvSection.Items.Add(.Item(4))
                    End If
                End With

            Next

            If selectedGrade <> "" Then
                ddlgvGrade.Text = selectedGrade
            End If


            If selectedSection <> "" Then
                ddlgvSection.Text = selectedSection
            End If

        End If
        set_Menu_Img()


    End Sub
    Public Function GetSearchString(ByVal field As String, ByVal value As String, ByVal strSearch As String)
        Dim strFilter As String = ""
        If value <> "" Then
            If strSearch = "LI" Then
                strFilter = " AND " + field + " LIKE '%" & value & "%'"
            ElseIf strSearch = "NLI" Then
                strFilter = "  AND " + field + " NOT LIKE '%" & value & "%'"
            ElseIf strSearch = "SW" Then
                strFilter = " AND " + field + "  LIKE '" & value & "%'"
            ElseIf strSearch = "NSW" Then
                strFilter = " AND " + field + "  NOT LIKE '" & value & "%'"
            ElseIf strSearch = "EW" Then
                strFilter = " AND " + field + " LIKE  '%" & value & "'"
            ElseIf strSearch = "NEW" Then
                strFilter = " AND " + field + " NOT LIKE '%" & value & "'"
            End If
        End If
        Return strFilter
    End Function
#End Region


    Protected Sub gvStudTPT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStudTPT.RowCommand
        If e.CommandName = "view" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selectedRow As GridViewRow = DirectCast(gvStudTPT.Rows(index), GridViewRow)
            ViewState("datamode") = Encr_decrData.Encrypt("view")
            ViewState("MainMnu_code") = Encr_decrData.Encrypt(ViewState("MainMnu_code"))
            Dim lblStuId As Label = selectedRow.FindControl("lblStuId")
            Dim lblStu_Name As Label = selectedRow.FindControl("lblStu_Name")
            Dim lblStu_No As Label = selectedRow.FindControl("lblStu_No")

            Dim url As String = String.Format("~\Students\studCancelAdmission_M.aspx?MainMnu_code={0}&datamode={1}&stuid=" + Encr_decrData.Encrypt(lblStuId.Text) _
            & "&stuno=" + Encr_decrData.Encrypt(lblStu_No.Text) _
            & "&name=" + Encr_decrData.Encrypt(lblStu_Name.Text), ViewState("MainMnu_code"), ViewState("datamode"))
            Response.Redirect(url)
        End If
    End Sub
End Class
