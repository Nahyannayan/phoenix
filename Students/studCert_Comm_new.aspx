<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studCert_Comm_new.aspx.vb" Inherits="Students_studCert_Comm_new" Title="::::GEMS OASIS:::: Online Student Administration System::::" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">

    <%--    <link href="../cssfiles/title.css" rel="stylesheet" type="text/css" />--%>
    <style>
        /* styles for menus*/
        .chromestyle {
            width: 99%;
            font-weight: bold;
        }

            .chromestyle:after { /*Add margin between menu and rest of content in Firefox*/
                content: ".";
                display: block;
                height: 0;
                clear: both;
                visibility: hidden;
            }

        .img_left {
            border: 0;
            vertical-align: middle;
            text-align: left;
        }

        .chromestyle ul {
            border: 0px solid #1B80B6;
            width: 100%;
            height: 21px;
            background: url(chromebg.gif) center center repeat-x; /*THEME CHANGE HERE*/
            padding: 0px 0;
            margin: 0;
            text-align: center; /*set value to "left", "center", or "right"*/
        }

            .chromestyle ul li {
                display: inline;
            }

                .chromestyle ul li a {
                    color: #494949;
                    padding: 0px 7px;
                    margin: 0;
                    text-decoration: none;
                    /*border-right: 1px solid #DADADA;*/
                }

                    .chromestyle ul li a:hover {
                        background: url(chromebg-over.gif) center center repeat-x; /*THEME CHANGE HERE*/
                        text-decoration: none;
                    }

                    .chromestyle ul li a[rel]:after { /*HTML to indicate drop down link*/
                        content: "v";
                        /*content: " " url(downimage.gif); /*uncomment this line to use an image instead*/
                    }




        /* ######### Style for Drop Down Menu ######### */

        .dropmenudiv {
            position: absolute;
            top: 0;
            border: 1px solid #BBB; /*THEME CHANGE HERE*/
            border-bottom-width: 0;
            /*font: normal 12px Verdana;*/
            line-height: 10px;
            z-index: 100;
            background-color: white;
            text-align: left;
            width: 200px;
            visibility: hidden;
            /*filter: progid:DXImageTransform.Microsoft.Shadow(color=#CACACA,direction=135,strength=4); /*Add Shadow in IE. Remove if desired*/
        }


            .dropmenudiv a {
                width: auto;
                display: block;
                text-indent: 3px;
                border-bottom: 1px solid #BBB; /*THEME CHANGE HERE*/
                padding: 2px 0;
                text-decoration: none;
                text-align: left;
                font-weight: bold;
                color: black;
            }

            * html .dropmenudiv a { /*IE only hack*/
                width: 100%;
                text-decoration: none;
                text-align: left;
            }

                .dropmenudiv a:link {
                    text-decoration: none;
                    color: #1B80B6;
                    text-align: left;
                }

                .dropmenudiv a:hover { /*THEME CHANGE HERE*/
                    background-color: #F0F0F0;
                    text-decoration: #1B80B6;
                    text-align: left;
                }

                .dropmenudiv a:visited {
                    text-decoration: none;
                    color: #1B80B6;
                    text-align: left;
                }

                .dropmenudiv a:active {
                    text-decoration: none;
                    color: #1B80B6;
                    text-align: left;
                }
    </style>
    <%--<script language="javascript" src="../cssfiles/chromejs/chrome.js" type="text/javascript">
    </script>--%>

    <script language="javascript" type="text/javascript">



        function client_OnTreeNodeChecked() {
            var obj = window.event.srcElement;
            var treeNodeFound = false;
            var checkedState;
            if (obj.tagName == "INPUT" && obj.type == "checkbox") {
                var treeNode = obj;
                checkedState = treeNode.checked;
                do {
                    obj = obj.parentElement;
                } while (obj.tagName != "TABLE")
                var parentTreeLevel = obj.rows[0].cells.length;
                var parentTreeNode = obj.rows[0].cells[0];
                var tables = obj.parentElement.getElementsByTagName("TABLE");
                var numTables = tables.length
                if (numTables >= 1) {
                    for (i = 0; i < numTables; i++) {
                        if (tables[i] == obj) {
                            treeNodeFound = true;
                            i++;
                            if (i == numTables) {
                                return;
                            }
                        }
                        if (treeNodeFound == true) {
                            var childTreeLevel = tables[i].rows[0].cells.length;
                            if (childTreeLevel > parentTreeLevel) {
                                var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                                var inputs = cell.getElementsByTagName("INPUT");
                                inputs[0].checked = checkedState;
                            }
                            else {
                                return;
                            }
                        }
                    }
                }
            }
        }




        function Select_Cond() {
            var e = document.getElementById('<%= ddlCond1.clientID %>');
            var ddlTxt = e.options[e.selectedIndex].text;
            var CheckStr = ddlTxt.toUpperCase();
            var strTemp = document.getElementById('<%=h_selected_menu_1.ClientID %>').value;
            var strval = strTemp.split('__');
            var TXT2 = document.getElementById('<%=txtCond2.ClientID %>');
            var IMG1 = document.getElementById('<%=imgCalendar1.ClientID %>');
            <%--var IMG2 = document.getElementById('<%=imgCalendar2.ClientID %>');--%>
            var val = strval[0];
            //alert(strval[0]);
            if (CheckStr.indexOf('DATE') != -1) {
                //if selected type is date then do this

                if (strval[0] == 'LI') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    //IMG2.style.display = 'none';
                }
                else if (strval[0] == 'NLI') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    //IMG2.style.display = 'none';
                }
                else if (strval[0] == 'SW') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    //IMG2.style.display = 'none';
                }
                else if (strval[0] == 'NSW') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    //IMG2.style.display = 'none';
                }
                else if (strval[0] == 'EW') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    //IMG2.style.display = 'none';
                }
                else if (strval[0] == 'NEW') {
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'inline';
                    //IMG2.style.display = 'none';
                }
                else if (strval[0] == 'BET') {
                    TXT2.style.display = 'inline';
                    IMG1.style.display = 'inline';
                    //IMG2.style.display = 'inline';
                }
            }
            else {
                IMG1.style.display = 'none';
                TXT2.style.display = 'none';
                TXT2.value = '';
                //IMG2.style.display = 'none';
            }
            if (strval[0] == 'BET') {
                val = 'LI';
                var path = '../Images/operations/like.gif';

              <%--  document.getElementById('<%=getid1()%>').src = path;--%>
                document.getElementById('<%=h_selected_menu_1.ClientID %>').value = val + '__' + path;
            }
        }


        function initialLoad_Cond() {

            var e = document.getElementById('<%= ddlCond1.clientID %>');
            var ddlTxt = e.options[e.selectedIndex].text;
            var CheckStr = ddlTxt.toUpperCase();
            var strTemp = document.getElementById('<%=h_selected_menu_1.ClientID %>').value;
            var strval = strTemp.split('__');
            Select_Cond();
            test1(strval[0]);

        }



        function test1(val) {
            var TXT2 = document.getElementById('<%=txtCond2.ClientID %>');
            <%--var IMG2 = document.getElementById('<%=imgCalendar2.ClientID %>');--%>
            var IMG1 = document.getElementById('<%=imgCalendar1.ClientID %>');
            var e = document.getElementById('<%= ddlCond1.clientID %>');
            var ddlTxt = e.options[e.selectedIndex].text;
            var CheckStr = ddlTxt.toUpperCase();

            var path;
            if (val == 'LI') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                //IMG2.style.display = 'none';
                path = '../Images/operations/like.gif';
            } else if (val == 'NLI') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                //IMG2.style.display = 'none';
                path = '../Images/operations/notlike.gif';
            } else if (val == 'SW') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                //IMG2.style.display = 'none';
                path = '../Images/operations/startswith.gif';
            } else if (val == 'NSW') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                //IMG2.style.display = 'none';
                path = '../Images/operations/notstartwith.gif';
            } else if (val == 'EW') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                //IMG2.style.display = 'none';
                path = '../Images/operations/endswith.gif';
            } else if (val == 'NEW') {
                TXT2.style.display = 'none';
                TXT2.value = '';
                //IMG2.style.display = 'none';
                path = '../Images/operations/notendswith.gif';
            }
            else if (val == 'BET') {
                if (CheckStr.indexOf('DATE') != -1) {
                    TXT2.style.display = 'inline';
                    IMG1.style.display = 'inline';
                    //IMG2.style.display = 'inline';
                    path = '../Images/operations/between.gif';
                }
                else {
                    alert("Only applicable for date");
                    TXT2.style.display = 'none';
                    TXT2.value = '';
                    IMG1.style.display = 'none';
                    //IMG2.style.display = 'none';
                    val = 'LI'
                    path = '../Images/operations/like.gif';
                }
            }
            <%--document.getElementById("<%=getid1()%>").src = path;--%>
            document.getElementById("<%=h_selected_menu_1.ClientID %>").value = val + '__' + path;
            $('#dropmenu1').hide();
        }





        function change_chk_state(chkThis) {
            var ids = chkThis.id

            var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)
            var state = false
            if (chkThis.checked) {
                state = true
            }
            else {
                state = false
            }

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                    document.forms[0].elements[i].checked = state;

                }
            }
            //document.forms[0].submit()
            return false;
        }


        function uncheckall(chkThis) {
            var ids = chkThis.id
            var value = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2) + '0' //Parent
            var value1 = ids.slice(ids.indexOf("_|"), ids.indexOf("|_") + 2)  //Child
            var state = false
            if (chkThis.checked) {
                state = true
            }
            else {
                state = false
            }
            var uncheckflag = 0

            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value1) != -1) {
                    if (currentid.indexOf(value) == -1) {
                        if (document.forms[0].elements[i].checked == false) {
                            uncheckflag = 1
                        }
                    }
                }
            }

            if (uncheckflag == 1) {
                // uncheck parent
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var currentid = document.forms[0].elements[i].id;
                    if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                        document.forms[0].elements[i].checked = false;
                    }
                }
            }
            else {
                // Check parent
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    var currentid = document.forms[0].elements[i].id;
                    if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf(value) != -1) {
                        document.forms[0].elements[i].checked = true;

                    }
                }
                // document.forms[0].submit();
            }


            return false;
        }
        function change_chk_stateg(chkThis) {
            var chk_state = !chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("ch1") != -1) {

                    document.forms[0].elements[i].checked = chk_state;
                    document.forms[0].elements[i].click();
                }
            }
        }




        function Cond1(event) {
            if (event.srcElement.type == 'checkbox') {
                var childNodes = event.srcElement.parentNode.parentNode.parentNode.parentNode.childNodes[0].childNodes[0].childNodes;
                for (i = 0; i < childNodes.length; ++i) {
                    if (childNodes[i].childNodes[0].type == 'checkbox')
                        childNodes[i].childNodes[0].checked = false;
                }
                event.srcElement.checked = true;
            }
        }

        function CondTALL(chkThis) {
            var chk_state = chkThis.checked;
            for (i = 0; i < document.forms[0].elements.length; i++) {
                var currentid = document.forms[0].elements[i].id;
                if (document.forms[0].elements[i].type == "checkbox" && currentid.indexOf("CBLbusnumber") != -1) {
                    // alert(document.forms[0].elements[i].value)
                    document.forms[0].elements[i].checked = chk_state;
                    // document.forms[0].elements[i].click();
                }
            }
        }

        function CondT() {
            var listALL = document.getElementById("<%=CBbusnoselectall.ClientId%>");
            var listBox1 = document.getElementById("<%=CBLbusnumber.ClientId%>");
            var checkbox = listBox1.getElementsByTagName("input");
            var counter = 0;
            var chkLength = checkbox.length;
            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].checked) {
                    counter++;
                }
            }
            if (chkLength == counter) {
                listALL.checked = true;
            }
            else {
                listALL.checked = false;
            }
        }

        //            var label = CHK.getElementsByTagName("label"); 

        //   for (var i=0;i<checkbox.length;i++) 

        //   { 

        //       if (checkbox[i].checked) 

        //       { 

        //           alert("Selected = " + label[i].innerHTML); 

        //       } 

        //   } 



    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-users mr-3"></i>
            Customized Export To Excel
        </div>

        <div class="card-body">
            <div class="table-responsive m-auto">

                <%--  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr class="title">
                        <td align="left" style="height: 25px"></td>
                    </tr>
                </table>--%>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
                            <asp:ValidationSummary ID="vsHeader" runat="server" CssClass="error"
                                DisplayMode="SingleParagraph" ValidationGroup="cert"></asp:ValidationSummary>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">
                            <asp:UpdatePanel ID="updaTEtop" runat="server">
                                <ContentTemplate>
                                    <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <tr>
                                            <td class="title-bg" colspan="4">Part A</td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Name</span> <font class="error">*</font></td>

                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtCerti" runat="server" MaxLength="150">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="error" ValidationGroup="cert" Font-Italic="True" ErrorMessage="Certificate name required" Display="Dynamic" ControlToValidate="txtCerti">*</asp:RequiredFieldValidator></td>
                                            <td align="left" width="20%"><span class="field-label">Type</span></td>

                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddtype" runat="server" OnSelectedIndexChanged="ddtype_SelectedIndexChanged" AutoPostBack="True">
                                                    <asp:ListItem Value="Student">Student</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>

                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">School</span></td>

                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlSchool" runat="server" OnSelectedIndexChanged="ddlSchool_SelectedIndexChanged" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                            <td align="left" width="20%"><span class="field-label">Academic Year</span></td>

                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlACD_ID" runat="server" OnSelectedIndexChanged="ddlACD_ID_SelectedIndexChanged" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                        </tr>

                                        <tr id="trCurr" runat="server">
                                            <td align="left" width="20%"><span class="field-label">Curriculum</span></td>

                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlCLM_ID" runat="server" OnSelectedIndexChanged="ddlCLM_ID_SelectedIndexChanged" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr id="trshf" runat="server">
                                            <td align="left"><span class="field-label">Shift</span></td>

                                            <td align="left">
                                                <asp:DropDownList ID="ddlSHF_ID" runat="server" OnSelectedIndexChanged="ddlSHF_ID_SelectedIndexChanged" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr id="trstm" runat="server">
                                            <td align="left" width="20%"><span class="field-label">Stream</span></td>

                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlSTM_ID" runat="server" OnSelectedIndexChanged="ddlSTM_ID_SelectedIndexChanged" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">Grade &amp; Section</span></td>

                                            <td align="left" width="30%">
                                                <div class="checkbox-list">
                                                    <asp:Panel ID="plGrade" runat="server" HorizontalAlign="Left">

                                                        <asp:TreeView ID="tvGrade" runat="server" ExpandDepth="1" onclick="client_OnTreeNodeChecked();"
                                                            ShowCheckBoxes="All">
                                                            <DataBindings>
                                                                <asp:TreeNodeBinding DataMember="GradeItem" SelectAction="SelectExpand" TextField="Text" ValueField="ValueField"></asp:TreeNodeBinding>
                                                            </DataBindings>
                                                        </asp:TreeView>

                                                    </asp:Panel>
                                                </div>
                                            </td>
                                            <td align="left" width="20%"><span class="field-label">Filter By</span></td>

                                            <td align="left" width="30%">
                                                <asp:DropDownList ID="ddlFilterBy" runat="server">
                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                    <asp:ListItem Value="1">Enrolled</asp:ListItem>
                                                    <asp:ListItem Value="2">Transfer Certificate</asp:ListItem>
                                                    <asp:ListItem Value="3">Strike Off</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" width="20%"><span class="field-label">As on date</span> </td>

                                            <td align="left" width="30%">
                                                <asp:TextBox ID="txtDate" runat="server" Width="90px"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" TargetControlID="txtDate" PopupButtonID="imgCalendar" Format="dd/MMM/yyyy">
                                                </ajaxToolkit:CalendarExtender>
                                                <asp:ImageButton ID="imgCalendar" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton><br />
                                                Note: If As on date is left blank it will default to the current date. </td>
                                            <td colspan="2"></td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">

                            <table id="tbl2" width="100%" cellpadding="5" cellspacing="0">
                                <tr>
                                    <td align="left" class="title-bg">Part B</td>

                                </tr>
                                <tr>
                                    <td width="100%">
                                        <asp:DataList ID="DataListPartB" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                            ShowHeader="False" Width="100%">
                                            <EditItemStyle HorizontalAlign="Left" />
                                            <SelectedItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:HiddenField ID="HiddenCCM_ID" runat="server" Value='<%# Eval("CCM_ID") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="HiddenCondition" runat="server" Value='<%# Eval("CCM_CONDITION") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="HiddenTextField" runat="server" Value='<%# Eval("CCM_TEXT_FIELD") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="HiddenValueField" runat="server" Value='<%# Eval("CCM_VALUE_FIELD") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="HiddenFilterField" runat="server" Value='<%# bind("CCM_FILTER_FIELD") %>'></asp:HiddenField>
                                                <div width="30%">
                                                    <span class="field-label"><%#Eval("CCM_DESC")%></span><br />
                                                    <div class="checkbox-list">
                                                        <asp:Panel ID="PanelControl" runat="server" ScrollBars="Auto">
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <AlternatingItemStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="30%" />
                                            <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                        </asp:DataList>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Transport User <%#Eval("CCM_DESC")%> </span>
                                        <br />
                                        <div class="checkbox-list">
                                            <asp:Panel ID="Panel6" runat="server" ScrollBars="Auto" HorizontalAlign="Left">

                                                <asp:RadioButtonList ID="RBLbusstatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="RBLbusstatus_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">YES</asp:ListItem>
                                                    <asp:ListItem Value="2">NO</asp:ListItem>
                                                </asp:RadioButtonList>

                                            </asp:Panel>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="field-label">Transport Bus Number <%#Eval("CCM_DESC")%> </span>
                                        <br />
                                        <div class="checkbox-list">
                                            <asp:Panel ID="Panel7" runat="server" ScrollBars="Auto">

                                                <asp:CheckBox ID="CBbusnoselectall"
                                                    runat="server"
                                                    Text="ALL"></asp:CheckBox><br />
                                                <asp:CheckBoxList ID="CBLbusnumber" runat="server">
                                                </asp:CheckBoxList>

                                            </asp:Panel>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="tbl3" width="100%" cellpadding="5" cellspacing="0">
                                <tr>
                                    <td align="left" class="title-bg">Part C</td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label runat="server" ID="lblError2" CssClass="error"></asp:Label>
                                        <table width="100%" cellpadding="1" cellspacing="0">
                                            <tr>
                                                <td><span class="field-label">Condition</span></td>

                                                <td align="left" valign="middle">
                                                    <div class="checkbox-list">
                                                        <asp:CheckBoxList ID="chkCond1" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" onClick="Cond1" CellPadding="2">
                                                            <asp:ListItem>AND</asp:ListItem>
                                                            <asp:ListItem>OR</asp:ListItem>
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCond1" runat="server"></asp:DropDownList></td>
                                                <td>
                                                    <asp:TextBox ID="txtCond1" runat="server">

                                                    </asp:TextBox><asp:ImageButton ID="imgCalendar1" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                                    <asp:TextBox ID="txtCond2" runat="server"></asp:TextBox>
                                                    <%--<asp:ImageButton ID="imgCalendar2" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>--%>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/operations/like.gif" OnClientClick=" $('#dropmenu1').show(); return false;"></asp:ImageButton>

                                                    <%--<span id="Div1" class="chromestyle" style="width: 60%; display: inline;">
                                                        <ul style="display: inline;">
                                                            <li style="display: inline;">
                                                                <a href="#" rel="dropmenu1">
                                                                    <img id="mnu_1_img" alt="Menu" src="../Images/operations/like.gif" align="middle" border="0" runat="server" /></a>

                                                            </li>
                                                        </ul>
                                                    </span>--%>
                                                    <ajaxToolkit:PopupControlExtender ID="pce" runat="server" PopupControlID="dropmenu1" TargetControlID="ImageButton1"></ajaxToolkit:PopupControlExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="5">
                                                    <asp:Button ID="btnPartCAdd" runat="server" CssClass="button" Text="Add" OnClick="btnPartCAdd_Click" CausesValidation="False" /></td>

                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <asp:GridView ID="gvPartC" runat="server" AutoGenerateColumns="False"
                                                        DataKeyNames="id" EmptyDataText="No search condition added yet for Part C."
                                                        CssClass="table table-bordered table-row" Width="100%" OnRowDeleting="gvPartC_RowDeleting">
                                                        <RowStyle CssClass="griditem" Height="23px" />
                                                        <EmptyDataRowStyle CssClass="gridheader" Wrap="True" HorizontalAlign="Center" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Id" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="OPER" HeaderText="Operator" ReadOnly="True"></asp:BoundField>
                                                            <asp:BoundField DataField="COLNAME" HeaderText="Field Name"></asp:BoundField>
                                                            <asp:BoundField DataField="COND" HeaderText="Condition"></asp:BoundField>
                                                            <asp:BoundField DataField="CONDVAL1" HeaderText="Value 1"></asp:BoundField>
                                                            <asp:BoundField DataField="CONDVAL2" HeaderText="Value 2"></asp:BoundField>
                                                            <asp:BoundField DataField="STRCOND" HeaderText="CONDSTR" Visible="False"></asp:BoundField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="DeleteBtn"
                                                                        CommandArgument='<%# Eval("id") %>'
                                                                        CommandName="Delete" runat="server">
         Delete</asp:LinkButton>


                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle BackColor="Aqua" />
                                                        <HeaderStyle CssClass="gridheader_new" />
                                                        <AlternatingRowStyle CssClass="griditem_alternative" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnProcess"
                                runat="server" CssClass="button" Text="Process Info" OnClick="btnProcess_Click" ValidationGroup="cert" /><asp:Button
                                    ID="btnBack" runat="server" CausesValidation="False" CssClass="button" OnClick="btnBack_Click"
                                    Text="Cancel" />
                            <ajaxToolkit:CalendarExtender ID="cal1" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar1"
                                TargetControlID="txtCond1">
                            </ajaxToolkit:CalendarExtender>
                            <%--<ajaxToolkit:CalendarExtender ID="cal2" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgCalendar2"
                                TargetControlID="txtCond2">
                            </ajaxToolkit:CalendarExtender>--%>
                        </td>
                    </tr>
                </table>


                <input id="h_Selected_menu_1" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_2" runat="server" type="hidden" value="=" />
                <input id="h_Selected_menu_3" runat="server" type="hidden" value="=" />
                <div id="dropmenu1" class="dropmenudiv">
                    <%--style="width: 110px; left: -216px; top: -10px;"--%>
                    <a href="javascript:test1('LI');">
                        <img alt="Any where" class="img_left" src="../Images/operations/like.gif" />Any
            Where</a> <a href="javascript:test1('NLI');">
                <img alt="Not In" class="img_left" src="../Images/operations/notlike.gif" />Not
                In</a> <a href="javascript:test1('SW');">
                    <img alt="Starts With" class="img_left" src="../Images/operations/startswith.gif" />Starts
                    With</a> <a href="javascript:test1('NSW');">
                        <img alt="Like" class="img_left" src="../Images/operations/notstartwith.gif" />Not
                        Start With</a> <a href="javascript:test1('EW');">
                            <img alt="Like" class="img_left" src="../Images/operations/endswith.gif" />Ends
                            With</a> <a href="javascript:test1('NEW');">
                                <img alt="Like" class="img_left" src="../Images/operations/notendswith.gif" />Not
                                Ends With</a>
                    <a href="javascript:test1('BET');">
                        <img alt="Like" class="img_left" src="../Images/operations/between.gif" />Between Dates</a>

                </div>



            </div>
        </div>
    </div>

    <%-- <script type="text/javascript">


        cssdropdown.startchrome("Div1")


    </script>--%>
</asp:Content>

