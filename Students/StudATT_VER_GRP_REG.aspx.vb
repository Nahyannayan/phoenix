Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections

Partial Class Students_StudATT_VER_GRP_REG
    Inherits System.Web.UI.Page

    Dim Encr_decrData As New Encryption64
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        ' Try
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = True
        'smScriptManager.RegisterPostBackControl(gvInfo)
        'Catch ex As Exception

        'End Try
    End Sub
    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(Now.AddSeconds(-1))
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave1)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave2)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnCancel1)
        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnCancel2)
        If Page.IsPostBack = False Then
            Try
                Dim CurBsUnit As String = Session("sBsuid")
                Dim USR_NAME As String = Session("sUsr_name")
                'collect the url of the file to be redirected in view state
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                'get the data mode from the query string to check if in add or edit mode 
                ViewState("datamode") = "view" ' Encr_decrData.Decrypt(Request.QueryString("datamode").Replace(" ", "+"))
                'get the menucode to confirm the user is accessing the valid page
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                'if query string returns Eid  if datamode is view state
                'check for the usr_name and the menucode are valid otherwise redirect to login page
                If USR_NAME = "" Or (ViewState("MainMnu_code") <> "S059061") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                Else
                    hfDate.Value = DateTime.Now
                    'calling pageright class to get the access rights
                    ViewState("menu_rights") = AccessRight.PageRightsID(USR_NAME, CurBsUnit, ViewState("MainMnu_code"))
                    'disable the control based on the rights
                    'use content if the page is comming from master page else use me.Page
                    'disable the control buttons based on the rights
                    btnSave1.Attributes.Add("onclick", "hideButton()")
                    btnSave2.Attributes.Add("onclick", "hideButton()")
                    Call AccessRight2.setpage(Page.Master.FindControl("cphMasterpage"), ViewState("menu_rights"), ViewState("datamode"))
                    Call bindAcademic_Year()
                    btnSave1.Visible = False
                    btnSave2.Visible = False
                    btnCancel1.Visible = False
                    btnCancel2.Visible = False
                    Session("dt_VA_ATT_GROUP") = CreateDataTable()
                    Session("dt_VA_ATT_GROUP").Rows.Clear()
                    txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    divAttparam.Visible = False
                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If

    End Sub
    Private Sub bindAcademic_Year()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            ddlAcdYear.Items.Clear()

            Dim reader As SqlDataReader
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBSUID"))
            PARAM(1) = New SqlParameter("@CLM_ID", Session("CLM"))
            PARAM(2) = New SqlParameter("@INFO_TYPE", "ACD")
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "VA.GETVERTICAL_GRP_BIND", PARAM)
            While reader.Read
                ddlAcdYear.Items.Add(New ListItem(reader("ACY_DESCR"), reader("ACD_ID")))
            End While
            reader.Close()

            ddlAcdYear.ClearSelection()
            ddlAcdYear.Items.FindByValue(Session("Current_ACD_ID")).Selected = True
            ddlAcdYear_SelectedIndexChanged(ddlAcdYear, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub ddlAcdYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        BindGroup()
        bindAttparam()
    End Sub
    Private Sub BindGroup()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            ddlGroup.Items.Clear()
            Dim reader As SqlDataReader
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcdYear.SelectedValue)
            PARAM(1) = New SqlParameter("@USR_ID", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@INFO_TYPE", "GROUP")
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "VA.GETVERTICAL_GRP_BIND", PARAM)

            While reader.Read
                ddlGroup.Items.Add(New ListItem(reader("SGR_DESCR"), reader("GRP_ID")))
            End While
            reader.Close()
            ddlGroup_SelectedIndexChanged(ddlGroup, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        GETPERIODS()
        Dim bEnabled As Boolean = True
        Dim ddlGRP As String = ddlGroup.SelectedValue
        Dim ddlPRD As String = ddlPERIOD.SelectedValue
        If (ddlGRP.Contains("|1") = True) Then
            ddlPERIOD.ClearSelection()
            For Each item As ListItem In ddlPERIOD.Items
                If item.Value.ToString.Contains("|1") = True Then
                    item.Selected = True
                    bEnabled = False
                End If
            Next
        Else
            ddlPERIOD.ClearSelection()
            ddlPERIOD.SelectedIndex = 0
            ddlPERIOD.Enabled = True
        End If
        ddlPERIOD.Enabled = bEnabled
    End Sub
    Private Sub GETPERIODS()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ddlGRP As String = ddlGroup.SelectedValue
            ddlPERIOD.Items.Clear()

            Dim reader As SqlDataReader
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBSUID"))
            PARAM(1) = New SqlParameter("@USR_ID", Session("sUsr_id"))
            PARAM(2) = New SqlParameter("@GRP_ID", ddlGRP)
            PARAM(3) = New SqlParameter("@INFO_TYPE", "PERIODS")
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "VA.GETVERTICAL_GRP_BIND", PARAM)
            While reader.Read
                ddlPERIOD.Items.Add(New ListItem(reader("RAP_PERIOD_DESCR"), reader("PERIOD_ID")))
            End While
            reader.Close()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub bindAttparam()


        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ddlGRP As String = ddlGroup.SelectedValue


            Dim reader As SqlDataReader
            Dim PARAM(4) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBSUID"))
            PARAM(1) = New SqlParameter("@ACD_ID", ddlAcdYear.SelectedValue)
            PARAM(2) = New SqlParameter("@GRP_ID", ddlGRP)
            PARAM(3) = New SqlParameter("@INFO_TYPE", "ATT_PARAM_ALL")
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "VA.GETVERTICAL_GRP_BIND", PARAM)
            While reader.Read
                divAttparam.innerHTML = Convert.ToString(reader("APD_PARAM_DESCR"))
            End While
            reader.Close()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function ValidateDate() As String
        Try
            Dim CommStr As String = String.Empty
            Dim ErrorStatus As String = String.Empty
            CommStr = ""

            If txtDate.Text <> "" Then
                Dim strfDate As String = txtDate.Text
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "Attendance Date format is Invalid"
                Else
                    txtDate.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtDate.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "Attendance Date format is Invalid"
                    End If
                End If
            Else
                ErrorStatus = "-1"
                CommStr = CommStr & "Attendance Date required"

            End If

            If ErrorStatus <> "-1" Then
                Return "0"
            Else
                lblError.Text = CommStr
            End If


            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ATTENDANCE DATE", "StuAtt_registration")
            Return "-1"
        End Try

    End Function
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim Valid_flag As Boolean = True
            Dim ddlGRP As String = ddlGroup.SelectedValue
            Dim ddlPRD As String = ddlPERIOD.SelectedValue
            Dim Vertical_period As String = String.Empty
            For Each item As ListItem In ddlPERIOD.Items
                If item.Value.ToString.Contains("|1") = True Then
                    Vertical_period = item.Text
                End If
            Next
            Dim t1 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            If t1 > t2 Then
                lblError.Text = "You cannot select a day greater than today!"
                txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Valid_flag = False
            Else
                If ddlAcdYear.SelectedIndex = -1 Then
                    lblError.Text = "Academic Year not selected"
                    Valid_flag = False
                ElseIf ddlGroup.SelectedIndex = -1 Then
                    lblError.Text = "Please verify your attendance permission ! "
                    Valid_flag = False
                ElseIf ddlGRP.Contains("|1") = True And ddlPRD.Contains("|0") = True Then
                    lblError.Text = "Only vertical groups can be marked attendance for period " & Vertical_period
                    Valid_flag = False
                ElseIf ddlGRP.Contains("|0") = True And ddlPRD.Contains("|1") = True Then
                    lblError.Text = "Only vertical groups can be marked attendance for period " & Vertical_period
                    Valid_flag = False
                End If
            End If
            If Valid_flag = True Then
                If (ValidateDate() = "0") Then
                    GETROOM_ATT_PARAM()
                    GRIDBIND_ATT(False)
                End If
            End If
        Catch ex As Exception
            lblError.Text = "You cannot select a day greater than today and must be a valid date!"
            txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        End Try
    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim Valid_flag As Boolean = True
            Dim ddlGRP As String = ddlGroup.SelectedValue
            Dim ddlPRD As String = ddlPERIOD.SelectedValue
            Dim Vertical_period As String = String.Empty
            For Each item As ListItem In ddlPERIOD.Items
                If item.Value.ToString.Contains("|1") = True Then
                    Vertical_period = item.Text
                End If
            Next
            Dim t1 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Dim t2 As DateTime = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            If t1 > t2 Then
                lblError.Text = "You cannot select a day greater than today!"
                txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                Valid_flag = False
            Else
                If ddlAcdYear.SelectedIndex = -1 Then
                    lblError.Text = "Academic Year not selected"
                    Valid_flag = False
                ElseIf ddlGroup.SelectedIndex = -1 Then
                    lblError.Text = "Please verify your attendance permission ! "
                    Valid_flag = False
                ElseIf ddlGRP.Contains("|1") = True And ddlPRD.Contains("|0") = True Then
                    lblError.Text = "Only vertical groups can be marked attendance for period " & Vertical_period
                    Valid_flag = False
                ElseIf ddlGRP.Contains("|0") = True And ddlPRD.Contains("|1") = True Then
                    lblError.Text = "Only vertical groups can be marked attendance for period " & Vertical_period
                    Valid_flag = False
                End If
            End If
            If Valid_flag = True Then
                If (ValidateDate() = "0") Then
                    GETROOM_ATT_PARAM()
                    GRIDBIND_ATT(True)
                End If
            End If
        Catch ex As Exception
            lblError.Text = "You cannot select a day greater than today and must be a valid date!"
            txtDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        End Try
    End Sub
    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim SRNO As New DataColumn("SRNO", System.Type.GetType("System.Int64"))
            Dim RAL_ID As New DataColumn("RAL_ID", System.Type.GetType("System.String"))
            Dim STU_ID As New DataColumn("STU_ID", System.Type.GetType("System.String"))
            Dim STU_NO As New DataColumn("STU_NO", System.Type.GetType("System.String"))
            Dim STUDNAME As New DataColumn("STUDNAME", System.Type.GetType("System.String"))
            Dim REMARKS As New DataColumn("REMARKS", System.Type.GetType("System.String"))
            Dim APPLEAVE As New DataColumn("APPLEAVE", System.Type.GetType("System.String"))
            Dim APD_ID As New DataColumn("APD_ID", System.Type.GetType("System.String"))
            Dim DAY1 As New DataColumn("DAY1", System.Type.GetType("System.String"))
            Dim DAY2 As New DataColumn("DAY2", System.Type.GetType("System.String"))
            Dim DAY3 As New DataColumn("DAY3", System.Type.GetType("System.String"))

            Dim P1 As New DataColumn("P1", System.Type.GetType("System.String"))
            Dim P2 As New DataColumn("P2", System.Type.GetType("System.String"))
            Dim P3 As New DataColumn("P3", System.Type.GetType("System.String"))
            Dim P4 As New DataColumn("P4", System.Type.GetType("System.String"))
            Dim P5 As New DataColumn("P5", System.Type.GetType("System.String"))
            Dim P6 As New DataColumn("P6", System.Type.GetType("System.String"))

            Dim P7 As New DataColumn("P7", System.Type.GetType("System.String"))
            Dim P8 As New DataColumn("P8", System.Type.GetType("System.String"))
            Dim P9 As New DataColumn("P9", System.Type.GetType("System.String"))
            Dim P10 As New DataColumn("P10", System.Type.GetType("System.String"))
            Dim SHOW_PERIOD_COUNT As New DataColumn("SHOW_PERIOD_COUNT", System.Type.GetType("System.String"))


            dtDt.Columns.Add(SRNO)
            dtDt.Columns.Add(RAL_ID)
            dtDt.Columns.Add(STU_ID)
            dtDt.Columns.Add(STU_NO)
            dtDt.Columns.Add(STUDNAME)
            dtDt.Columns.Add(REMARKS)
            dtDt.Columns.Add(APPLEAVE)
            dtDt.Columns.Add(APD_ID)
            dtDt.Columns.Add(DAY1)
            dtDt.Columns.Add(DAY2)
            dtDt.Columns.Add(DAY3)
            dtDt.Columns.Add(P1)
            dtDt.Columns.Add(P2)
            dtDt.Columns.Add(P3)

            dtDt.Columns.Add(P4)
            dtDt.Columns.Add(P5)
            dtDt.Columns.Add(P6)

            dtDt.Columns.Add(P7)
            dtDt.Columns.Add(P8)
            dtDt.Columns.Add(P9)
            dtDt.Columns.Add(P10)
            dtDt.Columns.Add(SHOW_PERIOD_COUNT)

            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return dtDt
        End Try
    End Function
    Private Sub GRIDBIND_ATT(ByVal bEDIT As Boolean)
        Try
            ViewState("RAL_ID") = "0"
            ViewState("flag") = "0"
            ViewState("SHOW_PERIOD_COUNT") = "0"
            Dim dt As DataTable = CreateDataTable()
            Dim dr As DataRow
            dt.Rows.Clear()
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim USR_ID As String = Session("sUsr_id")
            Dim BSU_ID As String = Session("sBSUID")
            Dim GRP_ID As String = ddlGroup.SelectedValue
            Dim PRD_ID As String = ddlPERIOD.SelectedValue
            Dim AttDate As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
            Dim PARAM(10) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", BSU_ID)
            PARAM(1) = New SqlParameter("@USR_ID", USR_ID)
            PARAM(2) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(3) = New SqlParameter("@GRP_ID", GRP_ID)
            PARAM(4) = New SqlParameter("@PRD_ID", PRD_ID)
            PARAM(5) = New SqlParameter("@AttDate", AttDate)
            PARAM(6) = New SqlParameter("@bEDIT", bEDIT)
            PARAM(7) = New SqlParameter("@PERIOD_DESCR", ddlPERIOD.SelectedItem.Text)
            PARAM(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            PARAM(8).Direction = ParameterDirection.ReturnValue
            Dim AttReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "VA.STUDATT_REG_GRIDBIND", PARAM)
            Dim ReturnFlag As Integer = PARAM(8).Value
            If ReturnFlag = 0 Then
                Using AttReader
                    If AttReader.HasRows = True Then
                        ViewState("idTr") = 1
                        While AttReader.Read
                            dr = dt.NewRow
                            ViewState("RAL_ID") = Convert.ToString(AttReader("RAL_ID"))
                            dr("SRNO") = ViewState("idTr")
                            dr("RAL_ID") = Convert.ToString(AttReader("RAL_ID"))
                            dr("STU_ID") = Convert.ToString(AttReader("STU_ID"))
                            dr("STU_NO") = Convert.ToString(AttReader("STU_NO"))
                            dr("STUDNAME") = Convert.ToString(AttReader("STUDNAME"))
                            dr("REMARKS") = Convert.ToString(AttReader("REMARKS"))
                            dr("APPLEAVE") = Convert.ToString(AttReader("APPLEAVE"))
                            dr("APD_ID") = Convert.ToString(AttReader("APD_ID"))
                            dr("P1") = Convert.ToString(AttReader("P1"))
                            dr("P2") = Convert.ToString(AttReader("P2"))
                            dr("P3") = Convert.ToString(AttReader("P3"))
                            dr("P4") = Convert.ToString(AttReader("P4"))
                            dr("P5") = Convert.ToString(AttReader("P5"))
                            dr("P6") = Convert.ToString(AttReader("P6"))
                            dr("P7") = Convert.ToString(AttReader("P7"))
                            dr("P8") = Convert.ToString(AttReader("P8"))
                            dr("P9") = Convert.ToString(AttReader("P9"))
                            dr("P10") = Convert.ToString(AttReader("P10"))
                            dr("SHOW_PERIOD_COUNT") = Convert.ToString(AttReader("SHOW_PERIOD_COUNT"))
                            ViewState("SHOW_PERIOD_COUNT") = Convert.ToString(AttReader("SHOW_PERIOD_COUNT"))
                            dr("DAY1") = Convert.ToString(AttReader(19))
                            dr("DAY2") = Convert.ToString(AttReader(20))
                            dr("DAY3") = Convert.ToString(AttReader(21))
                            hfDay1.Value = Convert.ToString(AttReader.GetName(19))
                            hfDay2.Value = Convert.ToString(AttReader.GetName(20))
                            hfDay3.Value = Convert.ToString(AttReader.GetName(21))
                            dt.Rows.Add(dr)
                            ViewState("idTr") = ViewState("idTr") + 1
                        End While
                        gvInfo.DataSource = dt
                        gvInfo.DataBind()

                        If ViewState("SHOW_PERIOD_COUNT") = "0" Then
                            gvInfo.Columns(3).Visible = False 'P1
                            gvInfo.Columns(4).Visible = False 'P2
                            gvInfo.Columns(5).Visible = False 'P3
                            gvInfo.Columns(6).Visible = False 'P4
                            gvInfo.Columns(7).Visible = False 'P5
                            gvInfo.Columns(8).Visible = False 'P6
                            gvInfo.Columns(9).Visible = False 'P7
                            gvInfo.Columns(10).Visible = False 'P8
                            gvInfo.Columns(11).Visible = False 'P9
                            gvInfo.Columns(12).Visible = False 'P10
                        Else
                            Dim TOSHOWCOLUMN As Integer = 3 + CInt(ViewState("SHOW_PERIOD_COUNT")) - 1
                            For K As Integer = 3 To 12
                                If K <= TOSHOWCOLUMN Then
                                    gvInfo.Columns(K).Visible = True
                                Else
                                    gvInfo.Columns(K).Visible = False
                                End If

                            Next


                        End If

                    End If
                End Using
            ElseIf ReturnFlag = -20 Then
                ViewState("flag") = "-1"
                lblError.Text = "Please verify the date entered is a school day."
            ElseIf ReturnFlag = -30 Then
                ViewState("flag") = "-1"
                lblError.Text = "Record not available. Please verify both student date of join and the group."
            ElseIf ReturnFlag = -40 Then
                ViewState("flag") = "-1"
                lblError.Text = "Attendances is already marked for the selected date and period.Please click on Edit to update the attendance."
            ElseIf ReturnFlag = -50 Then
                ViewState("flag") = "-1"
                lblError.Text = "Attendances not marked for the selected date and period.Please click on Add to update the attendance."
            ElseIf ReturnFlag = -60 Then
                ViewState("flag") = "-1"
                lblError.Text = "Please update you attendance permission to mark attendance for the selected date and group"
            End If
            If ViewState("flag") = "0" Then
                backGround()
                setControl4Save()
            Else
                setControl4Add()
                Exit Sub
            End If
        Catch ex As Exception
        End Try
    End Sub
    Sub setControl4Save()
        ddlAcdYear.Enabled = False
        ddlGroup.Enabled = False
        ddlPERIOD.Enabled = False
        imgCalendar.Visible = False
        txtDate.Enabled = False
        btnSave1.Visible = True
        btnSave2.Visible = True
        btnCancel1.Visible = True
        btnCancel2.Visible = True
        btnAdd.Visible = False
        btnEdit.Visible = False
        gvInfo.Visible = True
        divAttparam.Visible = True
    End Sub
    Sub setControl4Add()
        ddlAcdYear.Enabled = True
        ddlGroup.Enabled = True
        ddlPERIOD.Enabled = True
        imgCalendar.Visible = True
        txtDate.Enabled = True
        btnSave1.Visible = False
        btnSave2.Visible = False
        btnCancel1.Visible = False
        btnCancel2.Visible = False
        btnAdd.Visible = True
        btnEdit.Visible = True
        gvInfo.Visible = False
        divAttparam.Visible = False

    End Sub

    Private Sub backGround()
        For i As Integer = 0 To gvInfo.Rows.Count - 1
            Dim row As GridViewRow = gvInfo.Rows(i)
            Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
            ddlStatus.Items(0).Attributes.Add("style", "background-color:#fdf4a8") 'GREEN
            ddlStatus.Items(1).Attributes.Add("style", "background-color:#feb4a8") 'PINK
            ddlStatus.Font.Name = "Verdana"
            ddlStatus.Font.Size = "8"
        Next
    End Sub
    Private Sub GETROOM_ATT_PARAM()
        Dim CONN As SqlConnection = ConnectionManger.GetOASISConnection
        Dim ds1 As New DataSet
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim BSU_ID As String = Session("sBsuid")
        Session("V_ATT_PARAM") = Nothing
        Dim PARAM(4) As SqlParameter
        PARAM(0) = New SqlParameter("@ACD_ID", ACD_ID)
        PARAM(1) = New SqlParameter("@BSU_ID", BSU_ID)
        PARAM(2) = New SqlParameter("@INFO_TYPE", "ATT_PARAM_VERT")
        PARAM(4) = New SqlParameter("@GRP_ID", ddlGroup.SelectedValue)
        ds1 = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "VA.GETVERTICAL_GRP_BIND", PARAM)
        Session("V_ATT_PARAM") = ds1

    End Sub
    Protected Sub gvInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvInfo.RowDataBound
        Try
            Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
            Dim BSU_ID As String = Session("sBsuid")
            Dim DS As DataSet = Session("V_ATT_PARAM") 'AccessStudentClass.GetATTENDANCE_ROOM_PARAM_D(ACD_ID, BSU_ID)
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ddlSTATUS As DropDownList = DirectCast(e.Row.FindControl("ddlStatus"), DropDownList)
                Dim lblAPD_ID As Label = DirectCast(e.Row.FindControl("lblAPD_ID"), Label)
                Dim lblAPPLEAVE As Label = DirectCast(e.Row.FindControl("lblAPPLEAVE"), Label)
                ddlSTATUS.Enabled = True
                ddlSTATUS.Items.Clear()
                If DS.Tables(0).Rows.Count > 0 Then
                    ddlSTATUS.DataSource = DS.Tables(0)
                    ddlSTATUS.DataTextField = "APD_PARAM_DESCR"
                    ddlSTATUS.DataValueField = "APD_ID"

                    ddlSTATUS.DataBind()

                    If Not ddlSTATUS.Items.FindByValue(lblAPD_ID.Text) Is Nothing Then
                        ddlSTATUS.ClearSelection()
                        ddlSTATUS.Items.FindByValue(lblAPD_ID.Text).Selected = True

                    ElseIf Not ddlSTATUS.Items.FindByText("PRESENT") Is Nothing Then
                        ddlSTATUS.ClearSelection()
                        ddlSTATUS.Items.FindByText("PRESENT").Selected = True
                    End If
                    If lblAPPLEAVE.Text.Trim = "APPROVED" Then
                        ddlSTATUS.Enabled = False
                    End If
                    Call backGround()
                End If
            ElseIf e.Row.RowType = DataControlRowType.Header Then

                'If ViewState("SHOW_PERIOD_COUNT") = "0" Then
                '    e.Row.Cells(5).Text = Convert.ToString(hfDay1.Value.Replace("|", " "))
                '    e.Row.Cells(6).Text = Convert.ToString(hfDay2.Value.Replace("|", " "))
                '    e.Row.Cells(7).Text = Convert.ToString(hfDay3.Value.Replace("|", " "))
                'Else
                ' Dim STARTCOUNT As Integer = 15 + CInt(ViewState("SHOW_PERIOD_COUNT"))
                e.Row.Cells(15).Text = Convert.ToString(hfDay1.Value.Replace("|", " "))
                e.Row.Cells(16).Text = Convert.ToString(hfDay2.Value.Replace("|", " "))
                e.Row.Cells(17).Text = Convert.ToString(hfDay3.Value.Replace("|", " "))






            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub gvInfo_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.Header Then

        '    Dim HeaderGrid As GridView = DirectCast(sender, GridView)
        '    Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
        '    Dim HeaderCell As New TableCell()
        '    HeaderCell.Text = ""
        '    HeaderCell.ColumnSpan = 8
        '    HeaderGridRow.Cells.Add(HeaderCell)
        '    HeaderCell = New TableCell()
        '    HeaderCell.ForeColor = Drawing.Color.Black
        '    HeaderCell.Height = 20
        '    HeaderCell.Text = "Last Three Period's Attendance History"
        '    HeaderCell.ColumnSpan = 3
        '    HeaderGridRow.Cells.Add(HeaderCell)
        '    gvInfo.Controls(0).Controls.AddAt(0, HeaderGridRow)
        'End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click, btnSave2.Click
        System.Threading.Thread.Sleep(20)
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        str_err = calltransaction(errorMessage)
        If str_err = "0" Then
            lblError.Text = "Record Saved Successfully"
            btnSave2.Visible = False
        Else
            lblError.Text = errorMessage
            Call backGround()
        End If
    End Sub
    Function calltransaction(ByRef errorMessage As String) As Integer

        Dim RAL_ID As String = ViewState("RAL_ID")
        Dim ATTDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", txtDate.Text)
        Dim USR_ID As String = Session("sUsr_id")
        Dim BSU_ID As String = Session("sBsuid")
        Dim ACD_ID As String = ddlAcdYear.SelectedItem.Value
        Dim GRP_ID As String = ddlGroup.SelectedValue
        Dim PERIOD_ID As String = ddlPERIOD.SelectedValue
        Dim STR_XML As String = String.Empty
        Dim STU_ID As String = String.Empty
        Dim APD_ID As String = String.Empty
        Dim REMARKS As String = String.Empty
        Dim AppLeave As String = String.Empty
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim status As Integer
                For i As Integer = 0 To gvInfo.Rows.Count - 1
                    Dim row As GridViewRow = gvInfo.Rows(i)
                    Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlStatus"), DropDownList)
                    STU_ID = DirectCast(row.FindControl("lblStud_ID"), Label).Text
                    REMARKS = DirectCast(row.FindControl("txtRemarks"), TextBox).Text
                    AppLeave = DirectCast(row.FindControl("lblAppLeave"), Label).Text

                    If (AppLeave <> "APPROVED") Then
                        APD_ID = ddlStatus.SelectedItem.Value
                        STR_XML += String.Format("<STUDENT STUID='{0}' APD_ID='{1}' ALS_REMARKS='{2}'> </STUDENT>", _
                                                STU_ID, APD_ID, REMARKS)
                    End If
                Next
                If STR_XML <> "" Then
                    STR_XML = "<STUDENTS>" + STR_XML + "</STUDENTS>"
                Else
                    STR_XML = "<STUDENTS></STUDENTS>"
                End If


                Dim pParms(10) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
                pParms(1) = New SqlClient.SqlParameter("@USR_ID", USR_ID)
                pParms(2) = New SqlClient.SqlParameter("@GRP_ID", GRP_ID)
                pParms(3) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                pParms(4) = New SqlClient.SqlParameter("@ATTDT", ATTDT)
                pParms(5) = New SqlClient.SqlParameter("@PERIOD_ID", PERIOD_ID)
                pParms(6) = New SqlClient.SqlParameter("@STR_XML", STR_XML)
                pParms(7) = New SqlClient.SqlParameter("@RAL_ID", RAL_ID)
                pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(8).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "VA.SAVEVERTICAL_ROOM_ATT_REG", pParms)
                status = pParms(8).Value

                If status <> 0 Then
                    calltransaction = "1"
                    errorMessage = UtilityObj.getErrorMessage(status)  '"Error in inserting new record"
                    Return "1"
                End If

 Session("V_ATT_PARAM") = Nothing

                calltransaction = "0"
                setControl4Add()


            Catch ex As Exception
                calltransaction = "1"
                errorMessage = "Error Occured While Saving."
            Finally
                If calltransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel1.Click, btnCancel2.Click
        setControl4Add()
    End Sub
    

    
   
End Class
