Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class FEEAdjustmentView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.Title = OASISConstants.Gemstitle
            Try
                h_selected_menu_1.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_2.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_3.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_5.Value = "LI__../Images/operations/like.gif"
                h_Selected_menu_4.Value = "LI__../Images/operations/like.gif"
                ViewState("MainMnu_code") = Encr_decrData.Decrypt(Request.QueryString("MainMnu_code").Replace(" ", "+"))
                If Not Request.UrlReferrer Is Nothing Then
                    ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
                End If
                Dim USR_NAME As String = Session("sUsr_name")
                If USR_NAME = "" OrElse (ViewState("MainMnu_code") <> "S100465") Then
                    If Not Request.UrlReferrer Is Nothing Then
                        Response.Redirect(Request.UrlReferrer.ToString())
                    Else
                        Response.Redirect("~\noAccess.aspx")
                    End If
                End If
                GridBind()
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblError.Text = "Request could not be processed "
            End Try
        End If
    End Sub

    Public Function getid(ByVal pImg As String, Optional ByVal p_imgsrc As String = "") As String
        If gvStudentInfo.Rows.Count > 0 Then
            Dim s As HtmlControls.HtmlImage
            Dim pControl As String

            pControl = pImg
            Try
                s = gvStudentInfo.HeaderRow.FindControl(pControl)
                If p_imgsrc <> "" Then
                    s.Src = p_imgsrc
                End If
                Return s.ClientID
            Catch ex As Exception
                Return ""
            End Try
        End If
        Return ""
    End Function

    Sub GridBind()
        Try
             Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_Sql As String = ""
            Dim ds As New DataSet
            Dim str_Filter As String = ""

            Dim lstrCondn1, lstrCondn2, lstrCondn3, lstrCondn4, lstrCondn5 As String
            Dim ddlSTU_TYP As DropDownList
            Dim larrSearchOpr() As String
            Dim lstrOpr As String
            Dim txtSearch As New TextBox

            lstrCondn1 = ""
            lstrCondn2 = ""
            lstrCondn3 = ""
            lstrCondn4 = ""
            lstrCondn5 = ""

            str_Filter = ""
            If gvStudentInfo.Rows.Count > 0 Then
                ' --- Initialize The Variables
                '   --- FILTER CONDITIONS ---
                '   -- 1   txtEmpNo
                larrSearchOpr = h_selected_menu_1.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvStudentInfo.HeaderRow.FindControl("txtStuNo")
                lstrCondn1 = Trim(txtSearch.Text)
                If (lstrCondn1 <> "") Then
                    str_Filter = str_Filter & SetCondn(lstrOpr, "( STU_NO", lstrCondn1)
                    Dim strFEE_ID As String = SetCondn(lstrOpr, "FEE_ID", lstrCondn1)
                    strFEE_ID = strFEE_ID.Replace(" AND ", " OR ")
                    str_Filter = str_Filter & strFEE_ID & ")"
                End If
                '   -- 1  txtEmpname
                larrSearchOpr = h_Selected_menu_2.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvStudentInfo.HeaderRow.FindControl("txtstudname")
                lstrCondn2 = Trim(txtSearch.Text)
                If (lstrCondn2 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "STU_NAME", lstrCondn2)

                '   -- 2  txtFrom
                larrSearchOpr = h_Selected_menu_3.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvStudentInfo.HeaderRow.FindControl("txtGrade")
                lstrCondn3 = txtSearch.Text
                If (lstrCondn3 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "GRD_SCT", lstrCondn3)

                '   -- 3   txtTDate

                larrSearchOpr = h_Selected_menu_4.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvStudentInfo.HeaderRow.FindControl("txtAcademicYear")
                lstrCondn4 = txtSearch.Text
                If (lstrCondn4 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "ACY_DESCR", lstrCondn4)

                '   -- 5  city

                larrSearchOpr = h_Selected_menu_5.Value.Split("__")
                lstrOpr = larrSearchOpr(0)
                txtSearch = gvStudentInfo.HeaderRow.FindControl("txtStatus")
                lstrCondn5 = txtSearch.Text
                If (lstrCondn5 <> "") Then str_Filter = str_Filter & SetCondn(lstrOpr, "CURR_STATUS", lstrCondn5)


            End If

            Dim vSTU_TYP As String = String.Empty
            Dim vSTU_TYP_VAL As String = String.Empty
            Try
                ddlSTU_TYP = gvStudentInfo.HeaderRow.FindControl("ddlStudType")
                vSTU_TYP = " AND STU_TYPE in (" & ddlSTU_TYP.SelectedValue & ")"
                vSTU_TYP_VAL = ddlSTU_TYP.SelectedValue
            Catch ex As Exception
                vSTU_TYP = String.Empty
            End Try

            Dim str_cond As String = " WHERE STU_BSU_ID ='" & Session("sBSUID") & "'"
            str_Sql = "SELECT STU_TYPE, STU_ID, STU_NO, STU_BSU_ID, " & _
            " STU_NAME, PARENT_NAME, PARENT_MOBILE,PARENT_EMAIL, STU_GENDER, " & _
            " ACY_DESCR, GRD_SCT, SCT_DESCR, CURR_STATUS,FEE_ID, STU_DOB,TRAN_STATUS, TRANSPORTATION, BUS_NO, TRANS_LOC, TC_ENQ_DATE ,(select  TOP 1 CASE WHEN CHARINDEX ('RF',ISNULL(rol_module_access,''),1)>0 THEN 1 ELSE 0 END " & _
" from roles_m where ROL_ID in('" & Session("sroleid") & "')) AS bREF,(select  TOP 1 CASE WHEN CHARINDEX ('RF',ISNULL(rol_module_access,''),1)>0 THEN 0 ELSE 1 END " & _
" from roles_m where ROL_ID in('" & Session("sroleid") & "')) AS bREF_Not  FROM vw_OSO_STUD_ENQ_INFO "
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_cond & vSTU_TYP & str_Filter)
            gvStudentInfo.DataSource = ds.Tables(0)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvStudentInfo.DataBind()
                Dim columnCount As Integer = gvStudentInfo.Rows(0).Cells.Count

                gvStudentInfo.Rows(0).Cells.Clear()
                gvStudentInfo.Rows(0).Cells.Add(New TableCell)
                gvStudentInfo.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudentInfo.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudentInfo.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            Else
                gvStudentInfo.DataBind()
            End If
            'gvJournal.DataBind()
            txtSearch = gvStudentInfo.HeaderRow.FindControl("txtStuNo")
            txtSearch.Text = lstrCondn1

            txtSearch = gvStudentInfo.HeaderRow.FindControl("txtstudname")
            txtSearch.Text = lstrCondn2

            txtSearch = gvStudentInfo.HeaderRow.FindControl("txtGrade")
            txtSearch.Text = lstrCondn3

            txtSearch = gvStudentInfo.HeaderRow.FindControl("txtAcademicYear")
            txtSearch.Text = lstrCondn4

            txtSearch = gvStudentInfo.HeaderRow.FindControl("txtStatus")
            txtSearch.Text = lstrCondn5

            If vSTU_TYP <> String.Empty Then
                ddlSTU_TYP = gvStudentInfo.HeaderRow.FindControl("ddlStudType")
                ddlSTU_TYP.ClearSelection()
                ddlSTU_TYP.Items.FindByValue(vSTU_TYP_VAL).Selected = True
            End If

        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub BindSubjects(ByVal stu_id As String, ByVal gvSubjects As GridView)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT PARENT_NAME, PARENT_MOBILE, STU_GENDER, FEE_ID, STU_DOB, " & _
        " TRAN_STATUS, TRANSPORTATION, BUS_NO, TRANS_LOC, TC_ENQ_DATE " & _
        " FROM vw_OSO_STUD_ENQ_INFO WHERE STU_ID = " & stu_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        gvSubjects.DataSource = ds
        gvSubjects.DataBind()
    End Sub


    Sub BindDetails(ByVal gvRow As GridViewRow)
        Dim lblFeeId As Label
        Dim lblGender As Label
        Dim lblDob As Label
        Dim lblParentName As Label
        Dim lblParentMobile As Label
        Dim lblParentEmail As Label
        Dim lblTcEnqDate As Label
        Dim lblTranStatus As Label
        Dim lblTransportation As Label
        Dim lblBusNo As Label
        Dim lblLoc As Label
        Dim gvStudInfo As GridView

        With gvRow
            lblFeeId = .FindControl("lblFeeId")
            lblGender = .FindControl("lblGender")
            lblDob = .FindControl("lblDob")
            lblParentName = .FindControl("lblParentName")
            lblParentMobile = .FindControl("lblParentMobile")
            lblParentEmail = .FindControl("lblParentEmail")
            lblTcEnqDate = .FindControl("lblTcEnqDate")
            lblTranStatus = .FindControl("lblTranStatus")
            lblTransportation = .FindControl("lblTransportation")
            lblBusNo = .FindControl("lblBusNo")
            lblLoc = .FindControl("lblLoc")

            gvStudInfo = .FindControl("gvStudInfo")
        End With

        Dim dt As New DataTable

        dt.Columns.Add("FEE_ID", GetType(String))
        dt.Columns.Add("STU_GENDER", GetType(String))
        dt.Columns.Add("STU_DOB", GetType(String))
        dt.Columns.Add("PARENT_NAME", GetType(String))
        dt.Columns.Add("PARENT_MOBILE", GetType(String))
        dt.Columns.Add("PARENT_EMAIL", GetType(String))
        dt.Columns.Add("TC_ENQ_DATE", GetType(String))
        dt.Columns.Add("TRAN_STATUS", GetType(String))
        dt.Columns.Add("TRANSPORTATION", GetType(String))
        dt.Columns.Add("BUS_NO", GetType(String))
        dt.Columns.Add("TRANS_LOC", GetType(String))

        Dim dr As DataRow

        dr = dt.NewRow

        dr.Item(0) = lblFeeId.Text
        dr.Item(1) = lblGender.Text
        dr.Item(2) = lblDob.Text
        dr.Item(3) = lblParentName.Text
        dr.Item(4) = lblParentMobile.Text
        dr.Item(5) = lblParentEmail.Text
        dr.Item(6) = lblTcEnqDate.Text
        dr.Item(7) = lblTranStatus.Text
        dr.Item(8) = lblTransportation.Text
        dr.Item(9) = lblBusNo.Text
        dr.Item(10) = lblLoc.Text

        dt.Rows.Add(dr)


        gvStudInfo.DataSource = dt
        gvStudInfo.DataBind()
    End Sub

    Protected Sub gvJournal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStudentInfo.PageIndexChanging
        gvStudentInfo.PageIndex = e.NewPageIndex
        GridBind()
    End Sub

    Protected Sub gvJournal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudentInfo.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblSTU_ID As New Label
                lblSTU_ID = e.Row.FindControl("lblSTU_ID")
                If Not lblSTU_ID Is Nothing AndAlso lblSTU_ID.Text <> "" Then
                    BindDetails(e.Row)
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GridBind()
    End Sub

    Protected Sub ddlStudType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GridBind()
    End Sub
    Protected Function GetNavigateUrl(ByVal eqsid As String, ByVal STU_ID As String) As String
        Dim str As String = "ShowWindowWithClose('studComments.aspx?eqsid=" + eqsid + "&ENQID=" + STU_ID + "', 'search', '55%', '85%');"
        Return str
    End Function
  

    Protected Sub lbtnStuNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try


            Dim lblStuNo As New Label
            Dim lblStudType As New Label
            Dim lblSTU_ID As New Label 'EQS_ID/STU_ID

            lblStuNo = TryCast(sender.FindControl("lblStuNo"), Label)
            lblStudType = TryCast(sender.FindControl("lblStudType"), Label)
            lblSTU_ID = TryCast(sender.FindControl("lblSTU_ID"), Label)
            Session("Link_Ref") = lblSTU_ID.Text & "|" & lblStudType.Text
            mdlRef.Show()

        Catch ex As Exception

        End Try
    End Sub
End Class
