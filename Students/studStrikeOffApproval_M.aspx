<%@ Page Language="VB" MasterPageFile="~/mainMasterPage.master" AutoEventWireup="false" CodeFile="studStrikeOffApproval_M.aspx.vb" Inherits="Students_studStrikeOffApproval_M" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMasterpage" runat="Server">
    <script language="javascript" type="text/javascript">
        function confirm_reject() {

            if (confirm("You are about to reject the strike off request.Do you want to proceed?") == true)
                return true;
            else
                return false;

        }
    </script>
    <div class="card mb-3">
        <div class="card-header letter-space">
            <i class="fa fa-book mr-3"></i>
            <asp:Label ID="lblTitle" runat="server" Text="Strike Off Approval"></asp:Label>
        </div>
        <div class="card-body">
            <div class="table-responsive ">
                <table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="0"
                    cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left">&nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td align="left" valign="bottom" >
                            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"
                                SkinID="error"></asp:Label></td>
                    </tr>
                    <tr>
                        <td ></td>
                    </tr>
                    <tr>
                        <td   valign="top">
                            <table align="center"  cellpadding="5" cellspacing="0"
                                style="width: 100%" >
                               
                                <tr>
                                    <td align="left"  ><span class="field-label">Student Name</span></td>
                                    
                                    <td align="left" >
                                        <asp:TextBox ID="txtName" runat="server" Enabled="False"></asp:TextBox></td>
                                    <td align="left"  ><span class="field-label">SEN</span></td>
                                   
                                    <td align="left"  >
                                        <asp:TextBox ID="txtSEN" runat="server" Enabled="False"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="20%"  ><span class="field-label">Last Attendance Date</span><span style="font-size: 8pt; color: #800000"></span>
                                    </td>
                                    
                                    <td align="left"   >
                                        <asp:TextBox ID="txtLast" runat="server" Width="99px"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td align="left"  ><span class="field-label">Recommended Date</span></td>
                                   
                                    <td align="left"  >
                                        <asp:TextBox ID="txtRec" runat="server" >
                                        </asp:TextBox></td>
                                    <td align="left"  ><span class="field-label">Remarks</span></td>
                                    
                                    <td align="left"  >
                                        <asp:TextBox ID="txtRecRemarks" runat="server" Width="238px" TextMode="MultiLine" Height="67px" SkinID="MultiText" ReadOnly="True">
                                        </asp:TextBox></td>
                                </tr>



                                <tr>
                                    <td align="left" width="20%"  ><span class="field-label">Approval Date</span></td>
                                   
                                    <td align="left"   >
                                        <asp:TextBox ID="txtAprDate" runat="server" Width="99px">
                                        </asp:TextBox>&nbsp;<asp:ImageButton ID="imgApr" runat="server" ImageUrl="~/Images/calendar.gif"></asp:ImageButton>
                                        <asp:RegularExpressionValidator ID="revApprov" runat="server" ControlToValidate="txtAprDate"
                                            CssClass="error" Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Approval  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ForeColor="" ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr>
                                    <td align="left" ><span class="field-label">Remarks</span><span style="font-size: 8pt; color: #800000"></span></td>
                                    
                                    <td align="left"  style="width: 326px" colspan="4">
                                        <asp:TextBox ID="txtRemarks" runat="server" Width="238px" TextMode="MultiLine" Height="67px" SkinID="MultiText"></asp:TextBox>
                                    </td>

                                </tr>


                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td  align="center" valign="bottom">
                            <asp:Button ID="btnAccept" runat="server" CssClass="button" Text="Accept" ValidationGroup="groupM1" />&nbsp;
                <asp:Button ID="btnReject" OnClientClick="return confirm_reject();" runat="server" CssClass="button" Text="Reject" ValidationGroup="groupM1" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="False" /></td>
                    </tr>
                    <tr>
                        <td  valign="bottom">&nbsp;&nbsp;
                <asp:HiddenField ID="hfSTK_ID" runat="server" />
                            <asp:HiddenField ID="hfSTU_ID" runat="server" />
                            &nbsp;
                &nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRec"
                    Display="None" ErrorMessage="Please enter the recommended  date" ValidationGroup="groupM1"
                    Width="23px">
                </asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfMode" runat="server" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2"
                                runat="server" CssClass="MyCalendar" Format="dd/MMM/yyyy" PopupButtonID="txtAprDate"
                                TargetControlID="txtAprDate">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" CssClass="MyCalendar"
                                Format="dd/MMM/yyyy" PopupButtonID="imgApr" TargetControlID="txtAprDate">
                            </ajaxToolkit:CalendarExtender>
                            &nbsp;
                &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

